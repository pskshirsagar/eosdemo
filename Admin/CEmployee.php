<?php

use Psi\Eos\Admin\CStatsSalesCloserEmployees;
use Psi\Eos\Admin\CStatsCsmEmployees;
use Psi\Eos\Admin\CStatsSalesMonthlyCommits;
use Psi\Eos\Admin\CStatsSalesEngineerEmployees;
use Psi\Libraries\HtmlMimeMail\CHtmlMimeMail;
use Psi\Libraries\HtmlMimeMail\CFileAttachment;
use Psi\Libraries\UtilPsHtmlMimeMail\CPsHtmlMimeMail;
use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;

class CEmployee extends CBaseEmployee {

    use Psi\Libraries\EosFoundation\TEosStoredObject;
	const ID_SYSTEM					= 1;
	const ID_ADMIN					= 2;
	const ID_DAVID_BATEMEN			= 8;
	const ID_SANDEEP_GARUD			= 61;
	const ID_PREETAM_YADAV			= 62;
	const ID_DHARMESH_SHROFF		= 189;
	const ID_ROBERT_JONES			= 267;
	const ID_TAPAN_BHATT			= 276;
	const ID_BRADLEY_CREER			= 375;
	const ID_DANIEL_CLAY			= 400;
	const ID_CHASE_HARRINGTON		= 418;
	const ID_AMITABH_PUROHIT		= 449;
	const ID_CHINMAYI_SAHU			= 563;
	const ID_DANIEL_AINSWORTH		= 655;
	const ID_ROHAN_VYAS				= 676;
	const ID_HITEKSHU_PALIWAL		= 684;
	const ID_WILLIAM_CLINTON		= 702;
	const ID_ROHITSING_PARDESHI		= 717;
	const ID_CHRIS_MARTIN			= 770;
	const ID_KYLE_BROGDON			= 900;
	const ID_PRAFULL_JAISWAL		= 906;
	const ID_THOMAS_OWENS			= 1016;
	const ID_HEATHER_AKE			= 1070;
	const ID_SUPPORT_QUEUE			= 1068;
	const ID_SAMINA_BEGUM			= 1147;
	const ID_ADAM_GUNDERSEN			= 1229;
	const ID_CHRISTINA_FERNANDES	= 1299;
	const ID_HEATHER_GAGON			= 1305;
	const ID_ANGELA_KAVANAUGH		= 1344;
	const ID_ERIC_BJORNN			= 1414;
	const ID_BRANDON_MCCLOSKEY		= 1422;
	const ID_NEETA_NETI				= 1638;
	const ID_JAIMI_SHOEMAKER		= 1673;
	const ID_GILBERT_THOMUS			= 1830;
	const ID_ROBERT_WEATHERS		= 1833;
	const ID_IT_HELP_DESK			= 1845;
	const ID_SYSTEM_MONITOR			= 1852;
	const ID_BRANDON_FISH			= 2054;
	const ID_ADMIN_HELP_DESK		= 2071;
	const ID_HR_HELP_DESK			= 2072;
	const ID_BRIAN_BLOOD			= 2099;
	const ID_BEN_TREASURE			= 2269;
	const ID_TYLER_CARTER			= 2374;
	const ID_BRANDON_BREIVIK		= 2479;
	const ID_DENNISON_HARRIS		= 2483;
	const ID_KEVIN_CARPENTER		= 2886;
	const ID_DBA					= 2890;
	const ID_MARK_ROMERO			= 2991;
	const ID_BASWESHWAR_AMLAPURE	= 3015;
	const ID_JUSTIN_COOLE			= 3668;
	const ID_DBA_HELPDESK			= 3278;
	const ID_JONATHAN_OTT			= 4297;
	const ID_KIMBERLY_MOIR			= 4327;
	const ID_ACCOUNT_HELP_DESK		= 4868;
	const ID_RECRUITMENT_HELP_DESK	= 4869;
	const ID_TRAINING_HELP_DESK		= 4870;
	const ID_JEANINE_MILLER			= 916;
	const ID_ERIC_RIOS  			= 3186;
	const ID_MANDAR_JOSHI           = 7359;
	const ID_SHRIRAM_BHANDARI		= 413;
	const ID_ALYSHA_LOW             = 5554;
	const ID_SANTOSH_HARNEKAR       = 1729;
	const ID_TAUTOMATION			= 7047;
	const ID_HEIDI_RICHARDS			= 4866;
	const ID_CLIFF_KOKER			= 7964;

	// Giving Access to Internal Training Employees for My Leasing Center Report
	const ID_TREVOR_RILEY			= 488;
	const ID_SANDEEP_CHAVAN			= 69;
	const ID_SACHIN_DURGE			= 489;
	const ID_KEVIN_CARLIN			= 521;
	const ID_AMOL_KARANDE			= 818;
	const ID_SIDDHARTH_SHAH			= 1081;
	const ID_ALICIA_MOSS			= 1179;
	const ID_JOHN_GARDNER			= 1261;
	const ID_DAN_LUDLOW				= 1274;
	const ID_ABHIJEET_SINGH			= 1336;
	const ID_STEPHANIE_ROY			= 1435;
	const ID_RYAN_TYSON				= 1614;
	const ID_DALLAS_JENSEN			= 1731;
	const ID_SHAWN_HAMMOND			= 1835;
	const ID_KAIDEN_REDD			= 1903;
	const ID_JAMIE_SPRABERRY		= 2133;
	const ID_PATRICK_CONKLING		= 2316;
	const ID_CALVIN_CRYER			= 2463;
	const ID_JEFFERY_MICHAELIS		= 2466;
	const ID_COURTNEY_WILSON		= 2552;
	const ID_DANIEL_TORRES			= 2896;
	const ID_PETER_KTESTAKIS		= 2942;
	const ID_DAVID_NORTAN			= 3033;
	const ID_GILLIAN_HOWDEN			= 3104;
	const ID_KATELYN_CHASE			= 3300;
	const ID_MATTHEW_BALL			= 3485;
	const ID_JAYLEY_WAINWRIGHT		= 3748;
	const ID_TAYLER_AARON			= 3959;
	const ID_JOSHUA_ELLINGTON		= 4737;
	const ID_ONICA_HANBY			= 5239;
	const ID_SEAN_PETERSON			= 5245;
	const ID_ALEX_HANSEN			= 5827;
	const ID_HOLLY_MATHEWS			= 5825;
	const ID_MORGAN_ESLINGER        = 4449;
	const ID_ISAAC_BRUNNER          = 5194;
	const ID_JILEESA_CHRISTENSEN    = 6299;
	const ID_BAILEE_BEALS           = 4146;
	const ID_KIM_ROBERTSHAW         = 6049;
	const ID_DOUGLAS_ALBEE          = 5140;
	const ID_ERICA_LOWDER           = 4181;
	const ID_BECKY_MORTON           = 6327;
	const ID_SEAN_ROCKWOOD          = 7172;
	const ID_MATTHEW_CARLIN         = 550;
	const ID_STEPHEN_PATTERSON      = 5178;
	const ID_JOHNATHAN_LINDSAY      = 6479;
	const ID_BRENTON_BOUDREAUX      = 3762;
	const ID_WILL_ROBERTSHAW        = 4847;

	const ID_DEBRA_MADSEN           = 3712;
	const ID_KENNEDEE_WELCH         = 5136;
	const ID_JAMES_HARDMAN          = 6651;
	const ID_BROCK_ALIUS            = 4820;
	const ID_DANIEL_COPE            = 5396;
	const ID_JAYNANNE_MEADS         = 4673;
	const ID_LOSE_VAIFOOU           = 5737;
	const ID_JUSTIN_SMITH           = 6644;
	const ID_BRITTIAN_DECKER        = 6153;
	const ID_BETH_HENLEY            = 6152;
	const ID_PAYTON_BAUM            = 6647;
	const ID_CAMILLE_LORZ           = 6722;
	const ID_SANDRA_MCCLURE         = 2879;
	const ID_CODY_ENGSTROM          = 4682;
	const ID_LAUREN_JEPPSON         = 5162;

	// Added for delinquency tab access
	const ID_LOKESH_AGRAWAL			= 163;
	const ID_BRYAN_WADE				= 404;
	const ID_GANESH_PASARKAR		= 1139;
	const ID_BLAKE_WEBSTER			= 1378;
	const ID_SOMESH_JADHAV			= 2703;

	// Added for use in Annual Pay Review
	const ID_SWATI_KUMAWAT          = 3512;
	const ID_AYUSHI_JAIN            = 5811;
	const ID_RAJNI_MASOOM           = 6487;

	// To show preetam in review cycle. currently preetam shown under christina.
	const REPORTING_MANAGER_EMPLOYEE_ID_PREETAM  = self::ID_CHRISTINA_FERNANDES;

	// Include for No-Dues Survey
	const ID_RAHUL_NAGPURE			= 119;
	const ID_ANIRUDDHA_PUROHIT		= 238;
	const ID_SANJAY_BODKE			= 680;
	const ID_SAHEBRAJ_KALAL			= 1307;
	const ID_BUSHAN_VAISHAMPAYAN	= 1426;
	const ID_RITESH_SINGH			= 2527;
	const ID_RUHINA_SHAIKH			= 2534;
	const ID_AYUSH_MEHRA			= 2882;
	const ID_SAMEER_SATARKE			= 3680;
	const ID_ADWAIT_UDGAONKAR		= 4468;
	const ID_SWATI_PANDEY			= 5619;

	// Include for SMS For immediate task
	const ID_PRAVEEN_SHETTY			= 65;
	const ID_SANJAY_SINGH			= 462;
	const ID_RATNA_GANGULY			= 822;
	const ID_PRATHMASH_AUSEKAR		= 5007;

	// Added ADD employees
	const ID_VIJAY_TOPADEBUWA		= 523;
	const ID_MANJEET_PATEL			= 623;
	const ID_ROHIT_SHUKLA			= 701;
	const ID_ASHUTOSH_DURUGKAR		= 917;
	const ID_RAKESH_PADWAL			= 1047;
	const ID_ANIL_SHERKAR			= 1048;
	const ID_DATTATRAYA_NIRMAL		= 3093;

	// Added for domain inventory
	const ID_THOMAS_BIRCH			= 175;
	const ID_PAIGE_ANSTON			= 3794;

	// Added for US Annual pay review system
	const ID_PATRICIA_BRADFORD		= 483;
	const ID_JULIA_MANOOKIN			= 764;
	const ID_RAYLENE_EGBERT			= 1095;
	const ID_KATIE_RUCH				= 1099;
	const ID_AUSTIN_LAWYER			= 1182;
	const ID_RYAN_BYRD				= 1400;
	const ID_BENSON_JATEN			= 1661;
	const ID_CLINT_RICHARDS			= 2095;
	const ID_ZANE_HALES				= 2626;
	const ID_TIFFANY_BINKS			= 3005;
	const ID_REY_GARCIA				= 3022;
	const ID_DANIEL_ROLLO			= 3139;
	const ID_IAN_RAWLINGS			= 3223;
	const ID_MARESHA_FAWSON			= 3244;
	const ID_DALE_BROWN				= 3408;
	const ID_BAILEY_ROBERTSON		= 3535;
	const ID_JAYDE_FORD				= 3871;
	const ID_SHANE_DONOVAN			= 4097;
	const ID_KYLE_ASHBY				= 4801;
	const ID_JENNIFER_MORRISON		= 4319;
	const ID_ZACK_BLEYLE			= 4770;
	const ID_AUTUMN_SMITH			= 4819;
	const ID_AUDRIC_SCHADE			= 4984;
	const ID_CHRYSTAL_BRANDAO		= 4986;
	const ID_ALYSSA_WIDDISON		= 4888;
	const ID_LISA_FAWCETT			= 4990;
	const ID_ALYSSA_DUSAGE			= 5261;
	const ID_DEREK_DAVIDSON			= 5265;
	const ID_CHELSEA_WILSON			= 5297;
	const ID_SPENCER_SUNDRUD		= 5300;
	const ID_ROBERT_CHARNLEY		= 5528;
	const ID_KEITH_VIRGIN			= 6233;
	const ID_BILLING_BILLING		= 2038;
	const ID_TYSON_ENGLAND			= 2070;
	const ID_MELIA_CRANE			= 4248;

	// added for billing request default watchlist
	const ID_LAUREN_GIFFORD			= 1176;
	const ID_MATTHEW_HAYNIE			= 2076;

	// added for team_sprint
	const ID_SUBHASH_SODNAR			= 1525;
	const ID_SIDDHANT_GAIKWAD		= 4628;
	const ID_SUDHAKAR_SEERAPU		= 4862;
	const ID_PRADNYA_NAIK			= 4882;

	// added for Bounce Note ticket Project
	const ID_PRIYANKA_SATRE			= 2202;
	const ID_SACHIN_DAMRE			= 2732;
	const ID_SOMNATH_CHOWDHURY		= 5381;

	// Added for ps lead documents updates
	const ID_SANTOSH_KHATTE			= 932;
	const ID_RAJENDRA_GAIKWAD		= 972;
	const ID_ABHISHEK_JAMWADIKAR	= 5009;
	const ID_SACHIN_SHIRSEKAR		= 5490;

	// provide ability to send email to Resident
	const ID_JEN_HEAPS				= 6008;
	const ID_CHERISH_MOORE			= 7037;
	const ID_JAMES_HARRIS			= 3311;
	const ID_KEVIN_LAMBERT			= 1321;
	const ID_ASHLEY_MOON			= 1032;
	const ID_MANOJ_JADHAV			= 3312;

	// Include Ashlee Cowley in mail when Entrata Core is present in the contract
	const ID_ASSOCIATE_ACCOUNT_MANAGEMENT = 638;
	const ID_MAC_SIMS                     = 2579;

	// Include Ashlee Cowley in mail when Entrata Core is present in the contract
	const EMAIL_ASHLEE_COWLEY							= CSystemEmail::ACOWLEY_EMAIL_ADDRESS;
	const ID_ASHLEE_COWLEY								= 2288;
	const ASHLEY_CRANOR									= 3215;

	// IT WFH policy access employees
	const ID_MUJAHIN_FULARI			= 1846;
	const ID_RAJIV_KUMAR			= 6391;

	// Providing access to new HRIS ATS
	const ID_SAM_WINTERTON	= 7811;
	const ID_EMMA_CORBETT	= 5878;
	const ID_BEN_BLAKER		= 2507;

	// added for project management
    const ID_SUDHINDRA_DESHPANDE	= 3281;
    const ID_VIKRAM_YADAV	        = 6919;
	const ID_LES_ELDREDGE           = 480;

	const ID_ACCOUNTS_RESTRICTED_USER	= 8143;

	const IS_360_REVIEW									= 1;

	const ONE_YEAR										= 1;
	const TWO_YEAR										= 2;
	const THREE_YEAR									= 3;
	const FOUR_YEAR										= 4;

	const THREE_MONTHS									= 3;
	const FOUR_MONTHS									= 4;
	const SIX_MONTHS									= 6;
	const YEAR_DAYS										= 365;
	const MONTH_DAYS									= 30;
	const MONTHS_IN_YEAR								= 12;
	const MONTH_DECEMBER								= 12;

	const LAST_ASSESSED_THREE_MONTHS					= 3;
	const LAST_ASSESSED_SIX_MONTHS						= 6;
	const EMPLOYMENT_APPLICATION_TYPE_GENERAL			= 1;
	const EMPLOYMENT_APPLICATION_STATUS_TYPE_GENERAL	= 12;
	const APRIL_REVIEW									= '-04-01';
	const OCTOBER_REVIEW								= '-10-01';

	const EMAIL_DOMAIN_XENTO							= 'xento.com';
	const SERVICE_FILE_IN								= 'client_admin_xento_domain_service_account_file.json';
	const SERVICE_FILE_US								= 'client_admin_entrata_domain_service_account_file.json';
	const GENERIC_EMAIL_SIGNATURE_IN					= '<span style="color: rgb(59, 59, 60); font-family: arial; font-size: 12px; line-height: 35px;">Full Name |&nbsp;</span><i style="color: rgb(59, 59, 60); font-family: Arial; font-size: x-small;">Designation</i><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;" /><span style="text-align: -webkit-auto; color: rgb(207, 55, 24); font-family: arial; font-size: 12px; font-weight: bold; line-height: 1.5em;">DB Xento Systems Pvt. Ltd.</span><table cellpadding="0" cellspacing="3" style="text-align: -webkit-auto; font-size: 12.8px; color: rgb(0, 0, 0); line-height: 19.05px; font-family: verdana, arial, helvetica, sans-serif; margin: 5px 0px;"><tbody><tr>	<td style="font-family: arial; margin: 0px; font-style: italic; font-size: 11px; color: rgb(207, 55, 24); padding-right: 5px;">a:</td><td style="font-family: arial; margin: 0px; font-size: 11px; color: rgb(59, 59, 60);">Wing A, Upper Ground Floor, Tower 8, SEZ, Magarpatta City, Pune - 411013, Maharashtra, India</td></tr><tr><td style="font-family: arial; margin: 0px; font-style: italic; font-size: 11px; color: rgb(207, 55, 24); padding-right: 5px;">p:</td><td style="font-family: arial; margin: 0px; font-size: 11px; color: rgb(59, 59, 60);">020-30481988</td></tr><tr><td style="font-family: arial; margin: 0px; font-style: italic; font-size: 11px; color: rgb(207, 55, 24); padding-right: 5px;">m:</td><td style="font-family: arial; margin: 0px; font-size: 11px; color: rgb(59, 59, 60);">Mobile No</td></tr><tr><td style="font-family: arial; margin: 0px; font-style: italic; font-size: 10px; color: rgb(207, 55, 24); padding-right: 5px;">w:</td><td style="font-family: arial; margin: 0px; font-size: 11px; color: rgb(59, 59, 60);"><a href="https://www.xento.com/" rel="nofollow" style="color: rgb(51, 102, 187); padding-right: 13px; background-position: 100% 50%; background-repeat: no-repeat; background-color: initial;" target="_blank">https://www.xento.com</a></td></tr></tbody></table><span style="text-align: -webkit-auto; line-height: 1.5em; color: rgb(174, 175, 177); font-family: verdana, arial, helvetica, sans-serif; font-size: 9px;">Disclaimer &amp; Privilege:<br />This e-Mail may contain proprietary, privileged and confidential information and is sent&nbsp;<g class="gr_ gr_619 gr-alert gr_gramm gr_inline_cards gr_run_anim Grammar multiReplace" data-gr-id="619" id="619" style="display: inline; color: inherit !important; font-size: inherit !important; border-bottom: 2px solid transparent; background-repeat: no-repeat; background-position: -1px calc(100% + 3px); background-image: url(&quot;data:image/svg+xml;charset=utf8,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'100%\' height=\'100%\'%3E%3Cline opacity=\'0.75\' x1=\'4\' y1=\'100%\' x2=\'100%\' y2=\'100%\' transform=\'translate(-1.5, -2.5)\' stroke-width=\'3\' stroke-linecap=\'round\' stroke=\'%23fc5454\'/%3E%3C/svg%3E&quot;); background-size: calc(100% + 1px) 100%; animation: gr__appear_critical 0.4s ease forwards;">or</g>&nbsp;the intended recipient(s) only.&nbsp;<g class="gr_ gr_633 gr-alert gr_gramm gr_inline_cards gr_run_anim Punctuation only-del replaceWithoutSep" data-gr-id="633" id="633" style="display: inline; color: inherit !important; font-size: inherit !important; border-bottom: 2px solid transparent; background-repeat: no-repeat; background-position: -1px calc(100% + 3px); background-image: url(&quot;data:image/svg+xml;charset=utf8,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'100%\' height=\'100%\'%3E%3Cline opacity=\'0.75\' x1=\'4\' y1=\'100%\' x2=\'100%\' y2=\'100%\' transform=\'translate(-1.5, -2.5)\' stroke-width=\'3\' stroke-linecap=\'round\' stroke=\'%23fc5454\'/%3E%3C/svg%3E&quot;); background-size: calc(100% + 1px) 100%; animation: gr__appear_critical 0.4s ease forwards;">If,</g>&nbsp;by an addressing or transmission error, this mail has been misdirected to you, you are requested to notify us immediately by return email message and delete this mail and its attachments. You are also hereby notified that any use, any form of reproduction, dissemination, copying, disclosure, modification, distribution and/or publication of this e-mail message, contents or its attachment(s) other than by its intended recipient(s) is strictly prohibited.</span>';
	const GENERIC_EMAIL_SIGNATURE_US					= '<div style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;"><b style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><span style="font-size: 11px; font-family: Arial; color: rgb(153, 153, 153); font-weight: normal; vertical-align: baseline; white-space: pre-wrap;">Full Name &nbsp;| <i>&nbsp;<b>Designation</b></i></span></b></div><div style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;"><b style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><span style="font-size: 13px; font-family: Arial; color: rgb(255, 0, 0); vertical-align: baseline; white-space: pre-wrap;">Entrata</span><span style="font-size: 13px; font-family: Arial; color: rgb(192, 192, 192); vertical-align: baseline; white-space: pre-wrap;"> </span><br /><br /><span style="font-size: 11px; font-family: Arial; color: rgb(255, 0, 0); font-weight: normal; font-style: italic; vertical-align: baseline; white-space: pre-wrap;">p:</span><span style="font-size: 11px; font-family: Arial; color: rgb(153, 153, 153); font-weight: normal; font-style: italic; vertical-align: baseline; white-space: pre-wrap;"> </span><span style="font-size: 11px; font-family: Arial; color: rgb(153, 153, 153); font-weight: normal; vertical-align: baseline; white-space: pre-wrap;"><a style="color: rgb(34, 34, 34);" value="+18013755522">801.375.5522 ext. Extension</a></span><br /><span style="font-size: 11px; font-family: Arial; color: rgb(255, 0, 0); font-weight: normal; font-style: italic; vertical-align: baseline; white-space: pre-wrap;">tf:</span><span style="font-size: 11px; font-family: Arial; color: rgb(153, 153, 153); font-weight: normal; font-style: italic; vertical-align: baseline; white-space: pre-wrap;"> </span><span style="font-size: 11px; font-family: Arial; color: rgb(153, 153, 153); font-weight: normal; vertical-align: baseline; white-space: pre-wrap;"><a style="color: rgb(34, 34, 34);" value="+18778269700">877.826.9700</a></span><br /><span style="font-size: 11px; font-family: Arial; color: rgb(255, 0, 0); font-weight: normal; font-style: italic; vertical-align: baseline; white-space: pre-wrap;">w: </span><a data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://' . CONFIG_COMPANY_BASE_DOMAIN . '/&amp;source=gmail&amp;ust=1498904239813000&amp;usg=AFQjCNGXPUli3T0X8Wg2GRloHTpFXf80WQ" href="http://' . CONFIG_COMPANY_BASE_DOMAIN . '/" style="color: rgb(17, 85, 204);" target="_blank"><span style="font-size: 11px; font-family: Arial; font-weight: normal; vertical-align: baseline; white-space: pre-wrap;">' . CONFIG_COMPANY_BASE_DOMAIN . '</span></a></b><br />&nbsp;</div><div style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; text-align: justify;"><span style="color: rgb(174, 175, 177); font-family: verdana, arial, helvetica, sans-serif; font-size: 9px;">Disclaimer &amp; Privilege:</span><br style="color: rgb(174, 175, 177); font-family: verdana, arial, helvetica, sans-serif; font-size: 9px;" /><span style="color: rgb(174, 175, 177); font-family: verdana, arial, helvetica, sans-serif; font-size: 9px;">This e-Mail may contain proprietary, privileged and confidential information and is sent&nbsp;or&nbsp;the intended recipient(s) only.&nbsp;If by an addressing or transmission error, this mail has been misdirected to you, you are requested to notify us immediately by return email message and delete this mail and its attachments. You are also hereby notified that any use, any form of reproduction, dissemination, copying, disclosure, modification, distribution and/or publication of this e-mail message, contents or its attachment(s) other than by its intended recipient(s) is strictly prohibited.</span></div>';
	const XENTO_HRIS_EMAIL_SIGNATURE 					= 'Team - People Operations';
	protected $m_arrobjEmployees;
	protected $m_arrobjEmployeeAddresses;
	protected $m_arrobjEmployeePhoneNumbers;
	protected $m_arrobjEmployeeVacationSchedules;
	protected $m_arrobjEmployeeHours;
	protected $m_arrobjEmployeeVacationRequests;
	protected $m_arrobjEmployeeAssessments;
	protected $m_arrobjEmployeeDependants;
	protected $m_arrobjEmployeePreferences;

	protected $m_objEmployeeAddress;
	protected $m_objEmployeePhoneNumber;
	protected $m_objEmployeeVacationSchedule;
	protected $m_objDocument;
	protected $m_objUser;

	protected $m_intUserId;
	protected $m_intDaysLate;
	protected $m_intIsTerminated;
	protected $m_intEmployeeTrainingSessionId;
	protected $m_intEmailPassword;
	protected $m_intIsAssignedToClient;
	protected $m_intCid;
	protected $m_intDepartmentId;
	protected $m_intDesignationId;
	protected $m_intEmployeeCompletedYear;
	protected $m_intEmployeeCompletedMonth;
	protected $m_intEmployeeCompletedDay;
	protected $m_intEmployeeExperienceYear;
	protected $m_intEmployeeExperienceMonth;
	protected $m_intEmployeeAssessmentId;
	protected $m_intEmployeeApplicationStatusTypeId;
	protected $m_intManagerEmployeeId;
	protected $m_intEmployeePendingResponse;
	protected $m_intManagerPendingResponse;
	protected $m_intTeamId;
	protected $m_intTpmEmployeeId;
	protected $m_intAddEmployeeId;
	protected $m_intLikeCount;
	protected $m_intPreferenceId;
	protected $m_intDeskNumber;
	protected $m_intEmailTaskUpdatesToSelf;
	protected $m_intGroupId;

	protected $m_fltRegularHoursCount;
	protected $m_fltTimeOffHoursCount;
	protected $m_fltOvertimeHoursCount;
	protected $m_fltTotalHoursCount;
	protected $m_fltDiffHoursCount;
	protected $m_fltTestScore;

	protected $m_strEmployeeBonusAmountEncrypted;
	protected $m_strEmployeeBonusDescriptionEncrypted;
	protected $m_strEmployeeRelocationAllowanceEncrypted;
	protected $m_strEmployeeHrManagerName;
	protected $m_strPayrollRemotePrimaryKey;
	protected $m_strCountryCode;
	protected $m_strEmailAddress;
	protected $m_strGmailUsername;
	protected $m_strDesignationName;
	protected $m_strDepartmentName;
	protected $m_strAttachmentFileName;
	protected $m_strTotalHoursInAPayrollPeriod;
	protected $m_strTotalApprovedHoursInAPayrollPeriod;
	protected $m_strPhoneNumber;
	protected $m_strEmergencyContactNumber;
	protected $m_strLeasingCenterStatus;
	protected $m_strAssessmentDelayedBy;
	protected $m_strUsername;
	protected $m_strPassword;
	protected $m_strTeamName;
	protected $m_strTeamLogoFilePath;
	protected $m_strLastReviewed;
	protected $m_strTagName;
	protected $m_strNote;
	protected $m_strNoteDatetime;
	protected $m_strRefreshToken;
	protected $m_strManagerEmployeeName;
	protected $m_strManagerEmailAddress;
	protected $m_strValue;
	protected $m_strTeamPreferredName;

	protected $m_boolIsSelected;
	protected $m_boolIsLikeEmployee;
	protected $m_boolPrimaryTeam;
	protected $m_boolHasSameDirector;

	public static $c_arrintDevOpsEmployeeIds = [
		self::ID_ROHIT_SHUKLA,
		self::ID_ANIL_SHERKAR,
		self::ID_DATTATRAYA_NIRMAL,
		self::ID_RAKESH_PADWAL,
		self::ID_SAMEER_SATARKE,
		self::ID_MANJEET_PATEL,
		self::ID_ASHUTOSH_DURUGKAR,
		self::ID_VIJAY_TOPADEBUWA
	];

	public static $c_arrintMyLeasingCenterPermissionedEmployeeIds = [
		self::ID_MATTHEW_CARLIN,
		self::ID_STEPHEN_PATTERSON,
		self::ID_JOHNATHAN_LINDSAY,
		self::ID_BRENTON_BOUDREAUX
	];

	public static $c_arrintHrAllowTeamPermissionedEmployeeIds = [
		self::ID_HEATHER_GAGON,
		self::ID_ANGELA_KAVANAUGH,
		self::ID_KIMBERLY_MOIR
	];

	public static $c_arrintForEmployeesForManagerEmployee = [ self::ID_DAVID_BATEMEN ];
	public static $c_arrintForDomainEmployee = [ self::ID_THOMAS_BIRCH, self::ID_PAIGE_ANSTON ];
	public static $c_arrintForSprintStakeholderEmployee = [ self::ID_SUBHASH_SODNAR, self::ID_LOKESH_AGRAWAL ];
	public static $c_arrintForLoadScrumManagementTab = [ self::ID_RYAN_BYRD, self::ID_CHRIS_MARTIN, self::ID_DHARMESH_SHROFF ];

	public static $c_arrintForBounceNoteTaskEmployees = [ self::ID_LOKESH_AGRAWAL, self::ID_PRIYANKA_SATRE, self::ID_SOMNATH_CHOWDHURY ];

	public static $c_arrintSalesAndMarketingEmployeeIds = [
		self::ID_GANESH_PASARKAR,
		self::ID_SOMESH_JADHAV
	];

	// Allowed employee Ids to view CSM and NON CSM records
	public static $c_arrintAllowedEmployeeIdsToViewDelinquency = [
		self::ID_BRIAN_BLOOD,
		self::ID_BRYAN_WADE,
		self::ID_GANESH_PASARKAR,
		self::ID_SOMESH_JADHAV,
		self::ID_SANJAY_SINGH
	];


	public static $c_arrintEmployeesOnLeave = [
		self::ID_RUHINA_SHAIKH,
		self::ID_AYUSH_MEHRA,
		self::ID_SWATI_PANDEY,
		self::ID_SAMEER_SATARKE
	];


	public static $c_arrintHrisManagerTestGroup = [
		self::ID_CHRIS_MARTIN,
		self::ID_LES_ELDREDGE,
		self::ID_BEN_BLAKER
	];

	// task id 1247851 For download the employee data department wise field Access
	public static $c_arrstrAllDepartmentFields = [
		'phone_number'      => 'Phone number',
		'email_address'     => 'Email Address',
		'country_code'      => 'Country Code',
		'department_name'   => 'Department',
		'designation'       => 'Designation',
		'director'          => 'Director Name',
		'employee_number'   => 'Employee Number',
		'gender'            => 'Gender',
		'quality_manager'   => 'QAM Name',
		'reporting_manager' => 'Reporting Manager',
		'tpm_name'          => 'TPM Name',
		'team_name'         => 'Team Name',
		'total_experience'  => 'Total Experience',
		'xento_experience'  => 'Experience with Us',
		'hr_representative' => 'HR Representative',
		'technology'        => 'Technology'
	];

	public static $c_arrstrHrDepartmentFields = [
		'agent_phone_extension_id'    => 'Agent Phone Extension',
		'birth_date'                  => 'Birth Date',
		'city'                        => 'City',
		'date_confirmed'              => 'Confirmation Date',
		'emplyee_status_name'         => 'Employee Type',
		'physical_phone_extension_id' => 'Extension',
		'gmail_username'              => 'Gmail Username',
		'date_started'                => 'Hire Date',
		'last_working_day'            => 'Last Working Day',
		'permanent_address'           => 'Permanent Address',
		'permanent_email_address'     => 'Permanent Email Address',
		'hr_primary_reason'           => 'HR\'s Comment',
		'employee_primary_reason'     => 'Primary reason for leaving',
		'resigned_on'                 => 'Resignation Date',
		'review_cycle'                => 'Review Cycle',
		'pdm_name'                    => 'SDM',
		'employee_secondry_reason'    => 'Secondary reason for leaving',
		'date_terminated'             => 'Termination Date',
		'anniversary_date'            => 'Wedding Anniversary',
		'postal_code'                 => 'Zip Code'
	];

	public static $c_arrstrAccountDepartmentFields = [
		'pf_number_encrypted'        => 'PF No',
		'payroll_remote_primary_key' => 'Payroll Number',
		'annual_bonus_potential' => 'Annual Bonus Potential'
	];

	public static $c_arrstrAdminDepartmentFields = [
		'blood_group'                   => 'Blood Group',
		'card_number'                   => 'Card Number',
		'current_address'               => 'Current Address',
		'emergency_contact_number'      => 'Emergency Contact Number',
		'using_company_transport'       => 'Is Using Company Transport',
		'magarpatta_access_card_number' => 'Magarpatta Access Card Number',
		'office_name'                   => 'Office Name',
		'state_code'                    => 'State'
	];

	public static $c_arrstrITDepartmentFields = [
		'work_station_ip_address' => 'IP Address',
		'qa_ip_address'           => 'QA IP Address',
		'vpn_ip_address'          => 'VPN IP Address',
		'date_started'            => 'Hire Date',
		'date_terminated'         => 'Termination Date',
		'emplyee_status_name'     => 'Employee Type',
		'pdm_name'                => 'SDM',
	];

	public static $c_arrstrTrainingAndDevelopmentDepartmentFields = [
		'birth_date'              => 'Birth Date',
		'blood_group'             => 'Blood Group',
		'card_number'             => 'Card Number',
		'city'                    => 'City',
		'date_confirmed'          => 'Confirmation Date',
		'emplyee_status_name'     => 'Employee Type',
		'date_started'            => 'Hire Date',
		'using_company_transport' => 'Is Using Company Transport',
		'last_working_day'        => 'Last Working Day',
		'office_name'             => 'Office Name',
		'resigned_on'             => 'Resignation Date',
		'review_cycle'            => 'Review Cycle',
		'pdm_name'                => 'SDM',
		'date_terminated'         => 'Termination Date',
		'state_code'              => 'State',
		'postal_code'             => 'Zip Code'
	];

	public static $c_arrstrCommonFields = [
		'desk_number' => 'Desk Number'
	];

	public static $c_arrstrBirthDate = [
		'birth_date' => 'Birth Date'
	];

	public static $c_arrmixListAllProductBacklogsToUsers = [
		'ID_BRANDON_MCCLOSKEY' => CEmployee::ID_BRANDON_MCCLOSKEY,
		'ID_CHRIS_MARTIN' => CEmployee::ID_CHRIS_MARTIN
	];

	public static $c_arrstrEmployeeCategory = [ 'Entrata', 'Xento', 'Leasing Center' ];

	public function __construct() {
		parent::__construct();

		$this->m_arrobjEmployees                       = [];
		$this->m_arrobjEmployeeAddresses               = [];
		$this->m_arrobjEmployeePhoneNumbers            = [];
		$this->m_arrobjEmployeeVacationSchedules       = [];
		$this->m_arrobjEmployeeHours                   = [];
		$this->m_strTotalHoursInAPayrollPeriod         = 0;
		$this->m_strTotalApprovedHoursInAPayrollPeriod = 0;
		$this->m_intEmailTaskUpdatesToSelf             = 0;

		return;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createResourceRequisition() {
		$objResourceRequisition = new CResourceRequisition();
		$objResourceRequisition->setOriginatingEmployeeId( $this->getId() );

		return $objResourceRequisition;
	}

	public function createEmployeeDependant() {
		$objEmployeeDependant = new CEmployeeDependant();
		$objEmployeeDependant->setEmployeeId( $this->getId() );

		return $objEmployeeDependant;
	}

	public function createEmployeeAddress() {
		$this->m_objEmployeeAddress = new CEmployeeAddress();
		$this->m_objEmployeeAddress->setEmployeeId( $this->m_intId );

		return $this->m_objEmployeeAddress;
	}

	public function createEmployeePhoneNumber() {
		$this->m_objEmployeePhoneNumber = new CEmployeePhoneNumber();
		$this->m_objEmployeePhoneNumber->setEmployeeId( $this->m_intId );

		return $this->m_objEmployeePhoneNumber;
	}

	public function createEmployeeDocument() {
		$objEmployeeDocument = new CPsDocument();
		$objEmployeeDocument->setEmployeeId( $this->m_intId );

		return $objEmployeeDocument;
	}

	public function createEmployeeVacationSchedule() {
		$objEmployeeVacationSchedule = new CEmployeeVacationSchedule();
		$objEmployeeVacationSchedule->setEmployeeId( $this->m_intId );

		return $objEmployeeVacationSchedule;
	}

	public function createEmployeeCard() {
		$objEmployeeCard = new CEmployeeCard();
		$objEmployeeCard->setEmployeeId( $this->getId() );
		$objEmployeeCard->setIssuedOn( 'NOW()' );

		return $objEmployeeCard;
	}

	public function createAction() {
		$objEmployeeAction = new CAction();
		$objEmployeeAction->setEmployeeId( $this->getId() );
		$objEmployeeAction->setActionDatetime( 'NOW()' );
		$objEmployeeAction->setCreatedOn( 'NOW()' );

		return $objEmployeeAction;
	}

	public function createEmployeeAssessment() {
		$objEmployeeAssessment = new CEmployeeAssessment();

		$objEmployeeAssessment->setEmployeeId( $this->getId() );
		$objEmployeeAssessment->setDepartmentId( $this->getDepartmentId() );
		$objEmployeeAssessment->setAssessmentDateTime( date( 'm/d/Y' ) );
		$objEmployeeAssessment->setManagerEmployeeId( $this->getManagerEmployeeId() );

		return $objEmployeeAssessment;
	}

	private function createTask( $intTaskTypeId, $strTitle, $strDescription, $intUserId, $intGroupId ) {
		$objTask = new CTask();
		$objTask->setDefaults();
		$objTask->setTaskTypeId( $intTaskTypeId );
		$objTask->setTitle( $strTitle );
		$objTask->setDescription( $strDescription );
		$objTask->setUserId( $intUserId );
		$objTask->setGroupId( $intGroupId );
		$objTask->setProjectManagerId( CEmployee::ID_PREETAM_YADAV );
		$objTask->setTaskStatusId( CTaskStatus::UNUSED );
		$objTask->setTaskPriorityId( CTaskPriority::HIGH );

		return $objTask;
	}

	public function createUser() {
		$objNewUser = new CUser();
		$objNewUser->setEmployeeId( $this->m_intId );

		return $objNewUser;
	}

	public function creatPsProduct() {
		$objEmployeeProduct = new CEmployeeProduct();

		$objEmployeeProduct->setEmployeeId( $this->m_intId );

		return $objEmployeeProduct;
	}

	public function createCallAgent() {
		$objCallAgent = new CCallAgent();

		$objCallAgent->setEmployeeId( $this->getId() );
		$objCallAgent->setDepartmentId( $this->getDepartmentId() );
		$objCallAgent->setOfficeId( $this->getOfficeId() );
		$objCallAgent->setAgentPhoneExtensionId( $this->getAgentPhoneExtensionId() );
		$objCallAgent->setPhysicalPhoneExtensionId( $this->getPhysicalPhoneExtensionId() );
		$objCallAgent->setCallAgentStatusTypeId( CCallAgentStatusType::LOGGED_OUT );
		$objCallAgent->setCallAgentStateTypeId( CCallAgentStateType::IDLE );
		$objCallAgent->setFullName( $this->getNameFull() );
		$objCallAgent->setDomain( CCallHelper::getSipDomainName() );
		if( CDepartment::CALL_CENTER == $this->getDepartmentId() ) {
			$objCallAgent->setStaticWrapUpTime( CAgent::WRAP_UP_TIME );
		} else {
			$objCallAgent->setStaticWrapUpTime( 0 );
		}

		return $objCallAgent;
	}

	public function createEmailAddress( $strEmailAddress, $objDatabase, $intCounter = 0 ) {
		$arrstrNewEmailAddress        = explode( '@', $strEmailAddress );
		$strEmailAddressWithoutDomain = $arrstrNewEmailAddress[0];
		if( 0 < $intCounter ) {
			$strFinalEmailAddress = $strEmailAddressWithoutDomain . sprintf( '%02d', $intCounter ) . '@xento.com';
			$strSvnUsername       = $strEmailAddressWithoutDomain . sprintf( '%02d', $intCounter );
		} else {
			$strFinalEmailAddress = $strEmailAddressWithoutDomain . '@xento.com';
			$strSvnUsername       = $strEmailAddressWithoutDomain;
		}

		$strSql   = ' WHERE email_address=\'' . $strFinalEmailAddress . '\' ';
		$intCount = CEmployees::fetchRowCount( $strSql, 'employees', $objDatabase );

		$strSql             = ' WHERE svn_username=\'' . $strSvnUsername . '\' ';
		$intCountForSvnUser = CEmployees::fetchRowCount( $strSql, 'users', $objDatabase );

		if( 0 < $intCount || 0 < $intCountForSvnUser ) {
			$intCounter++;
			$strFinalEmailAddress = $this->createEmailAddress( $strEmailAddress, $objDatabase, $intCounter );
		} else {
			$this->setEmailAddress( $strFinalEmailAddress );
		}

		return;
	}

	public function createEmployeePreference() {
		$objEmployeePreference = new CEmployeePreference();
		$objEmployeePreference->setEmployeeId( $this->getId() );

		return $objEmployeePreference;
	}

	public function createEmployeeApplication( $intCurrentUserId, $objDatabase, $objEmployeeAddress = NULL ) {

		if( false == is_numeric( $intCurrentUserId ) ) {
			return NULL;
		}

		$objEmployeeApplication = new CEmployeeApplication();

		$objEmployeeApplication->setEmploymentApplicationId( self::EMPLOYMENT_APPLICATION_TYPE_GENERAL );
		$objEmployeeApplication->setEmployeeApplicationStatusTypeId( self::EMPLOYMENT_APPLICATION_STATUS_TYPE_GENERAL );
		$objEmployeeApplication->setEmployeeId( $this->getId() );
		$objEmployeeApplication->setDesignationId( $this->getDesignationId() );
		$objEmployeeApplication->setNamePrefix( $this->getNamePrefix() );
		$objEmployeeApplication->setNameFirst( $this->getNameFirst() );
		$objEmployeeApplication->setNameMiddle( $this->getNameMiddle() );
		$objEmployeeApplication->setGender( $this->getGender() );
		$objEmployeeApplication->setNameLast( $this->getNameLast() );
		$objEmployeeApplication->setEmailAddress( $this->getEmailAddress() );

		if( false == is_null( $objEmployeeAddress ) && true == valObj( $objEmployeeAddress, 'CEmployeeAddress' ) ) {
			$objEmployeeApplication->setStreetLine1( $objEmployeeAddress->getStreetLine1() );
			$objEmployeeApplication->setStreetLine2( $objEmployeeAddress->getStreetLine2() );
			$objEmployeeApplication->setCity( $objEmployeeAddress->getCity() );
			if( true == is_numeric( $objEmployeeAddress->getCountryStateId() ) ) {
				$strStateCode = CCountryStates::fetchStateCodeById( $objEmployeeAddress->getCountryStateId(), $objDatabase );
				$objEmployeeApplication->setStateCode( $strStateCode );
			} else {
				$objEmployeeApplication->setStateCode( $objEmployeeAddress->getStateCode() );
			}
			$objEmployeeApplication->setProvince( $objEmployeeAddress->getProvince() );
			$objEmployeeApplication->setPostalCode( $objEmployeeAddress->getPostalCode() );
			$objEmployeeApplication->setCountryCode( $objEmployeeAddress->getCountryCode() );
		}

		$objEmployeeApplication->setAppliedOn( date( 'Y-m-d H:i:s' ) );
		$objEmployeeApplication->setUpdatedBy( $intCurrentUserId );
		$objEmployeeApplication->setUpdatedOn( date( 'Y-m-d H:i:s' ) );
		$objEmployeeApplication->setCreatedBy( $intCurrentUserId );
		$objEmployeeApplication->setCreatedOn( date( 'Y-m-d H:i:s' ) );

		return $objEmployeeApplication;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addEmployeeDependant( $objEmployeeDependant ) {
		$this->m_arrobjEmployeeDependants[$objEmployeeDependant->getId()] = $objEmployeeDependant;
	}

	public function addEmployeePreference( $objEmployeeDependant ) {
		$this->m_arrobjEmployeePreferences[$objEmployeeDependant->getId()] = $objEmployeeDependant;
	}

	public function addTeamEmployee( $objTeamEmployee ) {
		$this->m_arrobjTeamEmployees[$objTeamEmployee->getId()] = $objTeamEmployee;
	}

	public function addPsLeadDetail( $objPsLeadDetail ) {
		$this->m_arrobjPsLeadDetail[$objPsLeadDetail->getId()] = $objPsLeadDetail;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getEmployeeTrainingSessionId() {
		return $this->m_intEmployeeTrainingSessionId;
	}

	public function getEmployeeDependants() {
		return $this->m_arrobjEmployeeDependants;
	}

	public function getTagName() {
		return $this->m_strTagName;
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function getTpmEmployeeId() {
		return $this->m_intTpmEmployeeId;
	}

	public function getAddEmployeeId() {
		return $this->m_intAddEmployeeId;
	}

	public function getEmployeePhoneNumbers() {
		return $this->m_arrobjEmployeePhoneNumbers;
	}

	public function getEmployees() {
		return $this->m_arrobjEmployees;
	}

	public function getEmployeeAddresses() {
		return $this->m_arrobjEmployeeAddresses;
	}

	public function getEmployeeAddress() {
		return $this->m_objEmployeeAddress;
	}

	public function getEmployeeVacationSchedules() {
		return $this->m_arrobjEmployeeVacationSchedules;
	}

	public function getEmployeePhoneNumber() {
		return $this->m_objEmployeePhoneNumber;
	}

	public function getLeasingCenterStatus() {
		return $this->m_strLeasingCenterStatus;
	}

	public function getEmployeeVacationRequests() {
		return $this->m_arrobjEmployeeVacationRequests;
	}

	public function getEmployeeVacationSchedule() {
		return $this->m_objEmployeeVacationSchedule;
	}

	public function getIsTerminated() {
		return $this->m_intIsTerminated;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getPayrollId() {
		return $this->m_strPayrollRemotePrimaryKey;
	}

	public function getEmailPassword() {
		return $this->m_intEmailPassword;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getManagerEmailAddress() {
		return $this->m_strManagerEmailAddress;
	}

	public function getTaxNumber() {
		if( false == valStr( $this->m_strTaxNumberEncrypted ) ) {
			return NULL;
		}
		return preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] ) );
	}

	public function getEmployeeHours() {
		return $this->m_arrobjEmployeeHours;
	}

	public function getTotalHoursInAPayrollPeriod() {
		return $this->m_strTotalHoursInAPayrollPeriod;
	}

	public function getTotalApprovedHoursInAPayrollPeriod() {
		return $this->m_strTotalApprovedHoursInAPayrollPeriod;
	}

	public function getIsAssignedToClient() {
		return $this->m_intIsAssignedToClient;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

	public function getRegularHoursCount() {
		return ( float ) $this->m_fltRegularHoursCount;
	}

	public function getTimeOffHoursCount() {
		return ( float ) $this->m_fltTimeOffHoursCount;
	}

	public function getOvertimeHoursCount() {
		return ( float ) $this->m_fltOvertimeHoursCount;
	}

	public function getTotalHoursCount() {
		return ( float ) $this->m_fltTotalHoursCount;
	}

	public function getDiffHoursCount() {
		return $this->m_fltDiffHoursCount;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getEmergencyContactNumber() {
		return $this->m_strEmergencyContactNumber;
	}

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getEmployeeCompletedYear() {
		return $this->m_intEmployeeCompletedYear;
	}

	public function getEmployeeCompletedMonth() {
		return $this->m_intEmployeeCompletedMonth;
	}

	public function getEmployeeCompletedDay() {
		return $this->m_intEmployeeCompletedDay;
	}

	public function getEmployeeExperienceYear() {
		return $this->m_intEmployeeExperienceYear;
	}

	public function getEmployeeBonusAmountEncrypted() {
		if( false == valStr( $this->m_strEmployeeBonusAmountEncrypted ) ) {
			return NULL;
		}

		if( CCountry::CODE_USA == $this->m_strCountryCode ) {
			return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strEmployeeBonusAmountEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER, 'is_base64_encoded' => true ] );
		} else {
			return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strEmployeeBonusAmountEncrypted, CONFIG_SODIUM_KEY_EMPLOYEE_BONUS, [ 'legacy_secret_key' => CONFIG_KEY_EMPLOYEE_BONUS, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
		}
	}

	public function getEmployeeBonusDescriptionEncrypted() {
		if( false == valStr( $this->m_strEmployeeBonusDescriptionEncrypted ) ) {
			return NULL;
		}

		if( CCountry::CODE_USA == $this->m_strCountryCode ) {
			return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strEmployeeBonusDescriptionEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER, 'is_base64_encoded' => true ] );
		} else {
			return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strEmployeeBonusDescriptionEncrypted, CONFIG_SODIUM_KEY_EMPLOYEE_BONUS, [ 'legacy_secret_key' => CONFIG_KEY_EMPLOYEE_BONUS, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
		}
	}

	public function getEmployeeRelocationAllowanceEncrypted() {
		if( false == valStr( $this->m_strEmployeeRelocationAllowanceEncrypted ) ) {
			return NULL;
		}

		if( CCountry::CODE_USA == $this->m_strCountryCode ) {
			return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strEmployeeRelocationAllowanceEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER, 'is_base64_encoded' => true ] );
		} else {
			return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strEmployeeRelocationAllowanceEncrypted, CONFIG_SODIUM_KEY_EMPLOYEE_BONUS, [ 'legacy_secret_key' => CONFIG_KEY_EMPLOYEE_BONUS, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
		}
	}

	public function getEmployeeExperienceMonth() {
		return $this->m_intEmployeeExperienceMonth;
	}

	public function getAssessmentDelayedBy() {
		return $this->m_strAssessmentDelayedBy;
	}

	public function getManagerEmployeeId() {
		return $this->m_intManagerEmployeeId;
	}

	public function getManagerEmployeeName() {
		return $this->m_strManagerEmployeeName;
	}

	public function getBankAccountNumber() {
		if( false == valStr( $this->m_strBankAccountNumberEncrypted ) ) {
			return NULL;
		}
		return preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strBankAccountNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] ) );
	}

	public function getPfNumber() {
		if( false == valStr( $this->m_strPfNumberEncrypted ) ) {
			return NULL;
		}
		return preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strPfNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function getPassword() {
		return $this->m_setPassword;
	}

	public function getUser() {
		return $this->m_objUser;
	}

	public function getTeamName() {
		return $this->m_strTeamName;
	}

	public function getTeamLogoFilePath() {
		return $this->m_strTeamLogoFilePath;
	}

	public function getDaysLate() {
		return $this->m_intDaysLate;
	}

	public function getLastReviewed() {
		return $this->m_strLastReviewed;
	}

	public function getEmployeeAssessmentId() {
		return $this->m_intEmployeeAssessmentId;
	}

	public function getEmployeeApplicationStatusTypeId() {
		return $this->m_intEmployeeApplicationStatusTypeId;
	}

	public function getLikeCount() {
		return $this->m_intLikeCount;
	}

	public function getIsLikeEmployee() {
		return $this->m_boolIsLikeEmployee;
	}

	public function getBaseSalary() {
		if( false == valStr( $this->getBaseSalaryEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getBaseSalaryEncrypted(), CONFIG_SODIUM_KEY_BASE_SALARY_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_BASE_SALARY_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function getRequestedDatetime() {
		return $this->m_strNoteDatetime;
	}

	public function getEmployeePreferences() {
		return $this->m_arrobjEmployeePreferences;
	}

	public function getRefreshToken() {
		return $this->m_strRefreshToken;
	}

	public function getPreferenceId() {
		return $this->m_intPreferenceId;
	}

	public function getTeamPreferredName() {
		return $this->m_strTeamPreferredName;
	}

	public function getEmailTaskUpdatesToSelf() {
		return $this->m_intEmailTaskUpdatesToSelf;
	}

	public function getDeskNumber() {
		return $this->m_intDeskNumber;
	}

	public function getGroupId() {
		return $this->m_intGroupId;
	}

	public function getEmployeeHrManagerName() {
		return $this->m_strEmployeeHrManagerName;
	}

	public function getBonusPotential() {
		if( false == valStr( $this->m_strBonusPotentialEncrypted ) ) {
			return NULL;
		}
		$strBonusPotential = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strBonusPotentialEncrypted, CONFIG_SODIUM_KEY_PURCHASE_REQUEST_BASE_AMOUNT, [ 'legacy_secret_key' => CONFIG_KEY_PURCHASE_REQUEST_BASE_AMOUNT ] );
		return $strBonusPotential;
	}

	public function getTeamEmployee() {
		return $this->m_arrobjTeamEmployees;
	}

	public function getPsLeadDetail() {
		return $this->m_arrobjPsLeadDetail;
	}

	public function getTestScore() {
		return $this->m_fltTestScore;
	}

	public function getIsPrimaryTeam() {
		return $this->m_boolPrimaryTeam;
	}

	public function getHasSameDirector() {
		return $this->m_boolHasSameDirector;
	}

    public function getTitle() {
        return $this->m_strTitle;
    }

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['tax_number'] ) ) {
			$this->setTaxNumber( $arrmixValues['tax_number'] );
		}
		if( true == isset( $arrmixValues['is_terminated'] ) ) {
			$this->setIsTerminated( $arrmixValues['is_terminated'] );
		}
		if( true == isset( $arrmixValues['user_id'] ) ) {
			$this->setUserId( $arrmixValues['user_id'] );
		}
		if( true == isset( $arrmixValues['cid'] ) ) {
			$this->setCid( $arrmixValues['cid'] );
		}
		if( true == isset( $arrmixValues['department_id'] ) ) {
			$this->setDepartmentId( $arrmixValues['department_id'] );
		}
		if( true == isset( $arrmixValues['designation_id'] ) ) {
			$this->setDesignationId( $arrmixValues['designation_id'] );
		}
		if( true == isset( $arrmixValues['phone_number'] ) ) {
			$this->setPhoneNumber( $arrmixValues['phone_number'] );
		}
		if( true == isset( $arrmixValues['emergency_contact_number'] ) ) {
			$this->setEmergencyContactNumber( $arrmixValues['emergency_contact_number'] );
		}
		if( true == isset( $arrmixValues['leasing_center_status'] ) ) {
			$this->setLeasingCenterStatus( $arrmixValues['leasing_center_status'] );
		}
		if( true == isset( $arrmixValues['phone_number'] ) ) {
			$this->setEmployeePhoneNumber( $arrmixValues['phone_number'] );
		}
		if( true == isset( $arrmixValues['department_name'] ) ) {
			$this->setDepartmentName( $arrmixValues['department_name'] );
		}
		if( true == isset( $arrmixValues['designation_name'] ) ) {
			$this->setDesignationName( $arrmixValues['designation_name'] );
		}
		if( true == isset( $arrmixValues['date_started_year'] ) ) {
			$this->setEmployeeCompletedYear( $arrmixValues['date_started_year'] );
		}
		if( true == isset( $arrmixValues['date_started_month'] ) ) {
			$this->setEmployeeCompletedMonth( $arrmixValues['date_started_month'] );
		}
		if( true == isset( $arrmixValues['date_started_day'] ) ) {
			$this->setEmployeeCompletedDay( $arrmixValues['date_started_day'] );
		}
		if( true == isset( $arrmixValues['experience_year'] ) ) {
			$this->setEmployeeExperienceYear( $arrmixValues['experience_year'] );
		}

		if( true == isset( $arrmixValues['bonus_amount_encrypted'] ) ) {
			$this->setEmployeeBonusAmountEncrypted( $arrmixValues['bonus_amount_encrypted'] );
		}

		if( true == isset( $arrmixValues['bonus_description_encrypted'] ) ) {
			$this->setEmployeeBonusDescriptionEncrypted( $arrmixValues['bonus_description_encrypted'] );
		}

		if( true == isset( $arrmixValues['relocation_allowance_encrypted'] ) ) {
			$this->setEmployeeRelocationAllowanceEncrypted( $arrmixValues['relocation_allowance_encrypted'] );
		}

		if( true == isset( $arrmixValues['experience_month'] ) ) {
			$this->setEmployeeExperienceMonth( $arrmixValues['experience_month'] );
		}
		if( true == isset( $arrmixValues['assessment_delayed'] ) ) {
			$this->setAssessmentDelayedBy( $arrmixValues['assessment_delayed'] );
		}
		if( true == isset( $arrmixValues['bank_account_number'] ) ) {
			$this->setBankAccountNumber( $arrmixValues['bank_account_number'] );
		}
		if( true == isset( $arrmixValues['pf_number'] ) ) {
			$this->setPfNumber( $arrmixValues['pf_number'] );
		}
		if( true == isset( $arrmixValues['base_salary'] ) ) {
			$this->setBaseSalary( $arrmixValues['base_salary'] );
		}
		if( true == isset( $arrmixValues['name_first'] ) ) {
			$this->setNameFirst( $arrmixValues['name_first'] );
		}
		if( true == isset( $arrmixValues['name_middle'] ) ) {
			$this->setNameMiddle( $arrmixValues['name_middle'] );
		}
		if( true == isset( $arrmixValues['name_last'] ) ) {
			$this->setNameLast( $arrmixValues['name_last'] );
		}
		if( true == isset( $arrmixValues['username'] ) ) {
			$this->setUsername( $arrmixValues['username'] );
		}
		if( true == isset( $arrmixValues['password'] ) ) {
			$this->setPassword( $arrmixValues['password'] );
		}
		if( true == isset( $arrmixValues['country_code'] ) ) {
			$this->setCountryCode( $arrmixValues['country_code'] );
		}
		if( true == isset( $arrmixValues['team_name'] ) ) {
			$this->setTeamName( $arrmixValues['team_name'] );
		}
		if( true == isset( $arrmixValues['team_logo_file_path'] ) ) {
			$this->setTeamLogoFilePath( $arrmixValues['team_logo_file_path'] );
		}
		if( true == isset( $arrmixValues['tag_name'] ) ) {
			$this->setTagName( $arrmixValues['tag_name'] );
		}
		if( true == isset( $arrmixValues['employee_training_session_id'] ) ) {
			$this->setEmployeeTrainingSessionId( $arrmixValues['employee_training_session_id'] );
		}
		if( true == isset( $arrmixValues['days_late'] ) ) {
			$this->setDaysLate( $arrmixValues['days_late'] );
		}
		if( true == isset( $arrmixValues['last_reviewed'] ) ) {
			$this->setLastReviewed( $arrmixValues['last_reviewed'] );
		}
		if( true == isset( $arrmixValues['employee_assessment_id'] ) ) {
			$this->setEmployeeAssessmentId( $arrmixValues['employee_assessment_id'] );
		}
		if( true == isset( $arrmixValues['employee_application_status_type_id'] ) ) {
			$this->setEmployeeApplicationStatusTypeId( $arrmixValues['employee_application_status_type_id'] );
		}
		if( true == isset( $arrmixValues['team_id'] ) ) {
			$this->setTeamId( $arrmixValues['team_id'] );
		}
		if( true == isset( $arrmixValues['tpm_employee_id'] ) ) {
			$this->setTpmEmployeeId( $arrmixValues['tpm_employee_id'] );
		}
		if( true == isset( $arrmixValues['add_employee_id'] ) ) {
			$this->setAddEmployeeId( $arrmixValues['add_employee_id'] );
		}
		if( true == isset( $arrmixValues['like_count'] ) ) {
			$this->setLikeCount( $arrmixValues['like_count'] );
		}
		if( true == isset( $arrmixValues['is_like_employee'] ) ) {
			$this->setIsLikeEmployee( $arrmixValues['is_like_employee'] );
		}
		if( true == isset( $arrmixValues['note'] ) ) {
			$this->setNote( $arrmixValues['note'] );
		}
		if( true == isset( $arrmixValues['requested_datetime'] ) ) {
			$this->setRequestedDatetime( $arrmixValues['requested_datetime'] );
		}
		if( true == isset( $arrmixValues['email_task_updates_to_self'] ) ) {
			$this->setEmailTaskUpdatesToSelf( $arrmixValues['email_task_updates_to_self'] );
		}
		if( true == isset( $arrmixValues['manager_employee_name'] ) ) {
			$this->setManagerEmployeeName( $arrmixValues['manager_employee_name'] );
		}
		if( true == isset( $arrmixValues['manager_email_address'] ) ) {
			$this->setManagerEmailAddress( $arrmixValues['manager_email_address'] );
		}
		if( true == isset( $arrmixValues['group_id'] ) ) {
			$this->setGroupId( $arrmixValues['group_id'] );
		}
		if( true == isset( $arrmixValues['hr_manager_name'] ) ) {
			$this->setEmployeeHrManagerName( $arrmixValues['hr_manager_name'] );
		}
		if( true == isset( $arrmixValues['test_score'] ) ) {
			$this->setTestScore( $arrmixValues['test_score'] );
		}
		if( true == isset( $arrmixValues['manager_employee_id'] ) ) {
			$this->setManagerEmployeeId( $arrmixValues['manager_employee_id'] );
		}
		if( true == isset( $arrmixValues['desk_number'] ) ) {
			$this->setDeskNumber( $arrmixValues['desk_number'] );
		}
		if( true == isset( $arrmixValues['is_primary_team'] ) ) {
			$this->setIsPrimaryTeam( $arrmixValues['is_primary_team'] );
		}
		if( true == isset( $arrmixValues['has_same_director'] ) ) {
			$this->setHasSameDirector( $arrmixValues['has_same_director'] );
		}
		if( true == isset( $arrmixValues['team_preferred_name'] ) ) {
			$this->setTeamPreferredName( $arrmixValues['team_preferred_name'] );
		}

		return;
	}

	public function setTagName( $strTagName ) {
		$this->m_strTagName = $strTagName;
	}

	public function setTeamId( $intTeamId ) {
		$this->m_intTeamId = $intTeamId;
	}

	public function setTpmEmployeeId( $intTpmEmployeeId ) {
		$this->m_intTpmEmployeeId = $intTpmEmployeeId;
	}

	public function setAddEmployeeId( $intAddEmployeeId ) {
		$this->m_intAddEmployeeId = $intAddEmployeeId;
	}

	public function setEmployeePhoneNumbers( $arrobjEmployeePhoneNumbers ) {
		$this->m_arrobjEmployeePhoneNumbers = $arrobjEmployeePhoneNumbers;
	}

	public function setEmployees( $arrobjEmployees ) {
		$this->m_arrobjEmployees = $arrobjEmployees;
	}

	public function setEmployeeAddresses( $arrobjEmployeeAddresses ) {
		$this->m_arrobjEmployeeAddresses = $arrobjEmployeeAddresses;
	}

	public function setEmployeeAddress( $objEmployeeAddress ) {
		$this->m_objEmployeeAddress = $objEmployeeAddress;
	}

	public function setEmployeePhoneNumber( $objEmployeePhoneNumber ) {
		$this->m_objEmployeePhoneNumber = $objEmployeePhoneNumber;
	}

	public function setEmployeeVacationSchedule( $objEmployeeVacationSchedule ) {
		$this->m_objEmployeeVacationSchedule = $objEmployeeVacationSchedule;
	}

	public function setIsTerminated( $intIsTerminated ) {
		$this->m_intIsTerminated = $intIsTerminated;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setEmailPassword( $intEmailPassword ) {
		$this->m_intEmailPassword = $intEmailPassword;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', $strEmailAddress );
	}

	public function setTaxNumber( $strTaxNumber ) {
		$this->setTaxNumberEncrypted( NULL );
		$this->setTaxNumberMasked( NULL );
		if( true == valStr( $strTaxNumber ) ) {
			$this->setTaxNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( preg_replace( '/[^a-z0-9]/i', '', $strTaxNumber ), CONFIG_SODIUM_KEY_TAX_NUMBER ) );
			$this->setTaxNumberMasked( CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $strTaxNumber ) ) );
		}
	}

	public function setTotalHoursInAPayrollPeriod( $strTotalHoursInAPayrollPeriod ) {
		$this->m_strTotalHoursInAPayrollPeriod = $strTotalHoursInAPayrollPeriod;
	}

	public function setTotalApprovedHoursInAPayrollPeriod( $strTotalApprovedHoursInAPayrollPeriod ) {
		$this->m_strTotalApprovedHoursInAPayrollPeriod = $strTotalApprovedHoursInAPayrollPeriod;
	}

	public function setIsAssignedToClient( $intIsAssignedToClient ) {
		$this->m_intIsAssignedToClient = $intIsAssignedToClient;
	}

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function setRegularHoursCount( $fltRegularHours ) {
		if( 0 < $this->getOvertimeHoursCount() ) {
			$this->m_fltRegularHoursCount = $fltRegularHours - $this->getOvertimeHoursCount();
		} else {
			$this->m_fltRegularHoursCount = $fltRegularHours;
		}
	}

	public function setTimeOffHoursCount( $fltTimeOffHours ) {
		$this->m_fltTimeOffHoursCount = $fltTimeOffHours;
	}

	public function setOvertimeHoursCount( $fltHoursExpected, $fltRegularHours ) {
		$this->m_fltOvertimeHoursCount = ( float ) ( $fltRegularHours - $fltHoursExpected );
	}

	public function setTotalHoursCount() {
		$fltOvertimeHours           = ( 0 < $this->getOvertimeHoursCount() ) ? $this->getOvertimeHoursCount() : 0.00;
		$this->m_fltTotalHoursCount = ( float ) ( $this->getRegularHoursCount() + $fltOvertimeHours + $this->getTimeOffHoursCount() );
	}

	public function setDiffHoursCount( $fltHoursExpected ) {
		$this->m_fltDiffHoursCount = ( float ) ( $this->getTotalHoursCount() - $fltHoursExpected );
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->m_intDepartmentId = $intDepartmentId;
	}

	public function setDesignationId( $intDesignationId ) {
		$this->m_intDesignationId = $intDesignationId;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setEmergencyContactNumber( $strEmergencyContactNumber ) {
		$this->m_strEmergencyContactNumber = $strEmergencyContactNumber;
	}

	public function setLeasingCenterStatus( $strLeasingCenterStatus ) {
		return $this->m_strLeasingCenterStatus = $strLeasingCenterStatus;
	}

	public function setDesignationName( $strDesignationName ) {
		$this->m_strDesignationName = $strDesignationName;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setEmployeeCompletedYear( $intEmployeeCompletedYear ) {
		$this->m_intEmployeeCompletedYear = $intEmployeeCompletedYear;
	}

	public function setEmployeeCompletedMonth( $intEmployeeCompletedMonth ) {
		$this->m_intEmployeeCompletedMonth = $intEmployeeCompletedMonth;
	}

	public function setEmployeeCompletedDay( $intEmployeeCompletedDay ) {
		$this->m_intEmployeeCompletedDay = $intEmployeeCompletedDay;
	}

	public function setEmployeeExperienceYear( $intEmployeeExperienceYear ) {
		$this->m_intEmployeeExperienceYear = $intEmployeeExperienceYear;
	}

	public function setEmployeeBonusAmountEncrypted( $strEmployeeBonusAmountEncrypted ) {
		$this->m_strEmployeeBonusAmountEncrypted = $strEmployeeBonusAmountEncrypted;
	}

	public function setEmployeeBonusDescriptionEncrypted( $strEmployeeBonusDescriptionEncrypted ) {
		$this->m_strEmployeeBonusDescriptionEncrypted = $strEmployeeBonusDescriptionEncrypted;
	}

	public function setEmployeeRelocationAllowanceEncrypted( $strEmployeeRelocationAllowanceEncrypted ) {
		$this->m_strEmployeeRelocationAllowanceEncrypted = $strEmployeeRelocationAllowanceEncrypted;
	}

	public function setEmployeeExperienceMonth( $intEmployeeExperienceMonth ) {
		$this->m_intEmployeeExperienceMonth = $intEmployeeExperienceMonth;
	}

	public function setAssessmentDelayedBy( $strAssessmentDelayedBy ) {
		$this->m_strAssessmentDelayedBy = $strAssessmentDelayedBy;
	}

	public function setManagerEmployeeId( $intManagerEmployeeId ) {
		$this->m_intManagerEmployeeId = $intManagerEmployeeId;
	}

	public function setManagerEmailAddress( $strManagerEmailAddress ) {
		$this->m_strManagerEmailAddress = $strManagerEmailAddress;
	}

	public function setManagerEmployeeName( $strManagerEmployeeName ) {
		$this->m_strManagerEmployeeName = $strManagerEmployeeName;
	}

	public function setBankAccountNumber( $strBankAccountNumber ) {
		$this->setBankAccountNumberEncrypted( NULL );
		if( true == valStr( $strBankAccountNumber ) ) {
			$this->setBankAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( preg_replace( '/[^a-z0-9]/i', '', $strBankAccountNumber ), CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		}
	}

	public function setPfNumber( $strPfNumber ) {
		$this->setPfNumberEncrypted( NULL );
		if( true == valStr( $strPfNumber ) ) {
			$this->setPfNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( preg_replace( '/[^a-z0-9]/i', '', $strPfNumber ), CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		}
	}

	public function setNameMiddle( $strNameMiddle ) {
        $this->m_strNameMiddle = CStrings::strTrimDef( mb_convert_case( $strNameMiddle, MB_CASE_TITLE, 'UTF-8' ), 50, NULL, true );
	}

	public function setUsername( $strUsername ) {
		$this->m_strUsername = $strUsername;
	}

	public function setPassword( $strPassword ) {
		$this->m_setPassword = $strPassword;
	}

	public function setUser( $objUser ) {
		$this->m_objUser = $objUser;
	}

	public function setTeamName( $strTeamName ) {
		$this->m_strTeamName = $strTeamName;
	}

	public function setTeamLogoFilePath( $strTeamLogoFilePath ) {
		$this->m_strTeamLogoFilePath = $strTeamLogoFilePath;
	}

	public function setLikeCount( $intLikeCount ) {
		$this->m_intLikeCount = $intLikeCount;
	}

	public function setIsLikeEmployee( $boolIsLikeEmployee ) {
		$this->m_boolIsLikeEmployee = $boolIsLikeEmployee;
	}

	public function setBaseSalary( $strBaseSalary ) {
		$this->setBaseSalaryEncrypted( NULL );
		if( true == valStr( $strBaseSalary ) ) {
			$strBaseSalary = CStrings::strTrimDef( $strBaseSalary, 20, NULL, true );
			$this->setBaseSalaryEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strBaseSalary, CONFIG_SODIUM_KEY_BASE_SALARY_NUMBER ) );
		}
	}

	public function setNote( $strNote ) {
		$this->m_strNote = $strNote;
	}

	public function setRequestedDatetime( $strNoteDatetime ) {
		$this->m_strNoteDatetime = $strNoteDatetime;
	}

	public function setRefreshToken( $strRefreshToken ) {
		$this->m_strRefreshToken = CStrings::strTrimDef( $strRefreshToken, -1, NULL, true );
	}

	public function setPreferenceId( $intReferenceId ) {
		$this->m_intPreferenceId = $intReferenceId;
	}

	public function setValue( $strValue ) {
		$this->m_strValue = CStrings::strTrimDef( $strValue, -1, NULL, true );
	}

	public function setTeamPreferredName( $strTeamPreferredName ) {
		$this->m_strTeamPreferredName = $strTeamPreferredName;
	}

	public function setEmailTaskUpdatesToSelf( $intEmailTaskUpdatesToSelf ) {
		$this->m_intEmailTaskUpdatesToSelf = CStrings::strToIntDef( $intEmailTaskUpdatesToSelf, NULL, false );
	}

	public function setDeskNumber( $intDeskNumber ) {
		$this->m_intDeskNumber = $intDeskNumber;
	}

	public function setGroupId( $intGroupdId ) {
		$this->m_intGroupId = CStrings::strToIntDef( $intGroupdId, NULL, false );
	}

	public function setEmployeeHrManagerName( $strEmployeeHrManagerName ) {
		$this->m_strEmployeeHrManagerName = $strEmployeeHrManagerName;
	}

	public function setBonusPotential( $fltBonusPotential ) {
		$fltBonusPotential = CStrings::strTrimDef( $fltBonusPotential, 20, NULL, true );
		if( false == is_null( $fltBonusPotential ) ) {
			$this->setBonusPotentialEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $fltBonusPotential, CONFIG_SODIUM_KEY_PURCHASE_REQUEST_BASE_AMOUNT ) );
		} else {
			$this->setBonusPotentialEncrypted( NULL );
		}
	}

	public function setTestScore( $fltTestScore ) {
		$this->m_fltTestScore = round( $fltTestScore, 2 );
	}

	public function setIsPrimaryTeam( $boolPrimaryTeam ) {
		$this->m_boolPrimaryTeam = $boolPrimaryTeam;
	}

	public function setHasSameDirector( $boolHasSameDirector ) {
		$this->m_boolHasSameDirector = $boolHasSameDirector;
	}

	/**
	 * Set Functions
	 */

	public function setEmployeeTrainingSessionId( $intEmployeeTrainingSessionId ) {
		$this->m_intEmployeeTrainingSessionId = $intEmployeeTrainingSessionId;
	}

    public function setTitle( $strTitle ) {
        $this->m_strTitle = $strTitle;
    }

	/**
	 * Other Functions
	 */

	public function setDaysLate( $intDaysLate ) {
		$this->m_intDaysLate = $intDaysLate;
	}

	public function setLastReviewed( $strLastReviewed ) {
		$this->m_strLastReviewed = $strLastReviewed;
	}

	public function setEmployeeAssessmentId( $intEmployeeAssessmentId ) {
		$this->m_intEmployeeAssessmentId = $intEmployeeAssessmentId;
	}

	public function setEmployeeApplicationStatusTypeId( $intEmployeeApplicationStatusTypeId ) {
		$this->m_intEmployeeApplicationStatusTypeId = $intEmployeeApplicationStatusTypeId;
	}

	/**
	 * Other Functions
	 *
	 */

	public function loadEmployeeDateDifference( $objDatabase ) {
		$arrstrDateDifference = [];

		$objEmployeeEmploymentHistory = CEmployeeEmploymentHistories::fetchEmployeeEmploymentHistoryByEmployeeId( $this->getId(), $objDatabase );

		if( ( CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_ON_SITE == $this->getEmployeeApplicationStatusTypeId() || CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_JOINED == $this->getEmployeeApplicationStatusTypeId() ) && true == valObj( $objEmployeeEmploymentHistory, 'CEmployeeEmploymentHistory' ) ) {
			$strStartDate = new DateTime( $objEmployeeEmploymentHistory->getDateStarted() );
		} else {
			$strStartDate = new DateTime( $this->getDateStarted() );
		}

		$strEndDate = ( false == is_null( $this->getDateTerminated() ) ) ? new DateTime( $this->getDateTerminated() ) : new DateTime( date( 'm/d/Y' ) );

		$intDateDifferenceInMonths      = $strEndDate->diff( $strStartDate )->format( '%m' );
		$intDateDifferenceInYears       = $strEndDate->diff( $strStartDate )->format( '%y' );
		$arrstrDateDifference['months'] = $intDateDifferenceInMonths;
		$arrstrDateDifference['years']  = $intDateDifferenceInYears;

		return $arrstrDateDifference;
	}

	public function isValidExtension() {
		if( true == is_numeric( $this->getPhysicalPhoneExtensionId() ) && 4 == strlen( $this->getPhysicalPhoneExtensionId() ) && '0000' != $this->getPhysicalPhoneExtensionId() ) {
			return true;
		}

		return false;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchEmployeeExperienceWithXento( $objDatabase, $boolFromDownload = false ) {
		$arrstrDateDifference           = [];
		$arrstrPreviousExperienceWithUs = [];

		$arrstrDateDifference = $this->loadEmployeeDateDifference( $objDatabase );

		$arrstrPreviousExperienceWithUs = CEmployeeEmploymentHistories::fetchEmployeePreviousExperienceWithUsById( $this->getId(), $objDatabase );
		$objEmployeeEmploymentHistory   = CEmployeeEmploymentHistories::fetchEmployeeEmploymentHistoryByEmployeeId( $this->getId(), $objDatabase );

		if( ( CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_ON_SITE == $this->getEmployeeApplicationStatusTypeId() || CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_JOINED == $this->getEmployeeApplicationStatusTypeId() ) && true == valObj( $objEmployeeEmploymentHistory, 'CEmployeeEmploymentHistory' ) ) {

			if( true == valArr( $arrstrPreviousExperienceWithUs ) ) {
				foreach( $arrstrPreviousExperienceWithUs as $arrstrExperience ) {
					$arrstrDateDifference['years']  = $arrstrExperience['previous_experience_years'];
					$arrstrDateDifference['months'] = $arrstrExperience['previous_experience_months'];
				}
			}

		} else {

			if( true == valArr( $arrstrPreviousExperienceWithUs ) ) {
				foreach( $arrstrPreviousExperienceWithUs as $arrstrExperience ) {

					$arrstrDateDifference['years']  = $arrstrDateDifference['years'] + $arrstrExperience['previous_experience_years'];
					$arrstrDateDifference['months'] = $arrstrDateDifference['months'] + $arrstrExperience['previous_experience_months'];
				}
			}

			if( self::MONTHS_IN_YEAR <= $arrstrDateDifference['months'] ) {
				$arrstrDateDifference['years']  = $arrstrDateDifference['years'] + floor( $arrstrDateDifference['months'] / self::MONTHS_IN_YEAR );
				$arrstrDateDifference['months'] = ( $arrstrDateDifference['months'] % self::MONTHS_IN_YEAR );
			}
		}

		if( true == $boolFromDownload ) {
			return ( ( 0 <= $arrstrDateDifference['years'] ) ? ( $arrstrDateDifference['years'] . '.' ) : '' ) . ( ( 0 <= $arrstrDateDifference['months'] ) ? ( ( 10 == $arrstrDateDifference['months'] ) ? $arrstrDateDifference['months'] + 1 : $arrstrDateDifference['months'] ) : '' );
		} else {
			return ( ( 0 != $arrstrDateDifference['years'] ) ? ( $arrstrDateDifference['years'] . ( ( $arrstrDateDifference['years'] > 1 ) ? ' Years ' : ' Year ' ) ) : '' ) . ( ( 0 != $arrstrDateDifference['months'] ) ? ( $arrstrDateDifference['months'] . ( ( $arrstrDateDifference['months'] > 1 ) ? ' Months ' : ' Month ' ) ) : '' );
		}
	}

	// This function is written for calculating the Total Experience with Entrata when the employee will moved to US, from status Onsite Deputation.

	public function fetchEmployeeExperienceWithEntrata( $objDatabase ) {
		$arrstrDateDifference        = [];
		$arrstrExperienceWithEntrata = [];

		$arrstrDateDifference = $this->loadEmployeeDateDifference( $objDatabase );

		$arrstrExperienceWithEntrata = \Psi\Eos\Admin\CEmployees::createService()->fetchEmployeeExperienceWithEntrataById( $this->getId(), $objDatabase );

		if( true == valArr( $arrstrExperienceWithEntrata ) ) {
			foreach( $arrstrExperienceWithEntrata as $arrstrExperience ) {
				$arrstrDateDifference['years']  = $arrstrExperience['previous_experience_years'];
				$arrstrDateDifference['months'] = $arrstrExperience['previous_experience_months'];
			}

			if( self::MONTHS_IN_YEAR <= $arrstrDateDifference['months'] ) {
				$arrstrDateDifference['years']  = $arrstrDateDifference['years'] + floor( $arrstrDateDifference['months'] / self::MONTHS_IN_YEAR );
				$arrstrDateDifference['months'] = ( $arrstrDateDifference['months'] % self::MONTHS_IN_YEAR );
			}
		}

		return ( ( 0 != $arrstrDateDifference['years'] ) ? ( $arrstrDateDifference['years'] . ( ( $arrstrDateDifference['years'] > 1 ) ? ' Years ' : ' Year ' ) ) : '' ) . ( ( 0 != $arrstrDateDifference['months'] ) ? ( $arrstrDateDifference['months'] . ( ( $arrstrDateDifference['months'] > 1 ) ? ' Months ' : ' Month ' ) ) : '' );
	}

	public function fetchEmployeeTotalExperience( $objDatabase, $boolFromDownload = false ) {

		$arrstrDateDifference = $this->loadEmployeeDateDifference( $objDatabase );

		$arrstrPreviousExperienceWithUs = \Psi\Eos\Admin\CEmployeeEmploymentHistories::createService()->fetchEmployeePreviousExperienceWithUsById( $this->getId(), $objDatabase );
		$arrstrExperienceWithEntrata    = \Psi\Eos\Admin\CEmployees::createService()->fetchEmployeeExperienceWithEntrataById( $this->getId(), $objDatabase );
		$objEmployeeEmploymentHistory   = \Psi\Eos\Admin\CEmployeeEmploymentHistories::createService()->fetchEmployeeEmploymentHistoryByEmployeeId( $this->getId(), $objDatabase );
		if( ( CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_ON_SITE == $this->getEmployeeApplicationStatusTypeId() || CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_JOINED == $this->getEmployeeApplicationStatusTypeId() ) && true == valObj( $objEmployeeEmploymentHistory, 'CEmployeeEmploymentHistory' ) ) {
			if( true == valArr( $arrstrPreviousExperienceWithUs ) ) {
				foreach( $arrstrPreviousExperienceWithUs as $arrstrExperience ) {
					$intTotalExperienceInMonths = ( int ) $this->getEmployeeExperienceMonth() + $arrstrExperience['previous_experience_months'];
					$intTotalExperienceInYears  = ( int ) $this->getEmployeeExperienceYear() + $arrstrExperience['previous_experience_years'];
					if( true == valArr( $arrstrExperienceWithEntrata ) ) {
						foreach( $arrstrExperienceWithEntrata as $arrstrEntrataExperience ) {
							$intTotalExperienceInYears  = $intTotalExperienceInYears + $arrstrEntrataExperience['previous_experience_years'];
							$intTotalExperienceInMonths = $intTotalExperienceInMonths + $arrstrEntrataExperience['previous_experience_months'];
						}
					}
				}
			}

		} else {
            $intTotalExperienceInMonths = ( int ) $arrstrDateDifference['months'] + $this->getEmployeeExperienceMonth();
            $intTotalExperienceInYears  = ( int ) $arrstrDateDifference['years'] + $this->getEmployeeExperienceYear();

            if( true == valArr( $arrstrPreviousExperienceWithUs ) ) {
                foreach ( $arrstrPreviousExperienceWithUs as $arrstrPreviousExperience ) {
                    if( false == is_null( $arrstrPreviousExperience['previous_experience_months'] ) ) {
                        $intTotalExperienceInMonths += $arrstrPreviousExperience['previous_experience_months'];
                    }
                    if( false == is_null( $arrstrPreviousExperience['previous_experience_years'] ) ) {
                        $intTotalExperienceInYears += $arrstrPreviousExperience['previous_experience_years'];
                    }
                }
            }
		}

		if( self::MONTHS_IN_YEAR <= $intTotalExperienceInMonths ) {
			$intTotalExperienceInYears  = $intTotalExperienceInYears + ( int ) ( $intTotalExperienceInMonths / self::MONTHS_IN_YEAR );
			$intTotalExperienceInMonths = $intTotalExperienceInMonths % self::MONTHS_IN_YEAR;
		}

		if( true == $boolFromDownload ) {
			return ( ( 0 <= $intTotalExperienceInYears ) ? ( $intTotalExperienceInYears . '.' ) : '' ) . ( ( 0 <= $intTotalExperienceInMonths ) ? ( ( 10 == $intTotalExperienceInMonths ) ? $intTotalExperienceInMonths + 1 : $intTotalExperienceInMonths ) : '' );
		} else {
			return ( ( 0 != $intTotalExperienceInYears ) ? ( $intTotalExperienceInYears . ( ( $intTotalExperienceInYears > 1 ) ? ' Years ' : ' Year ' ) ) : '' ) . ( ( 0 != $intTotalExperienceInMonths ) ? ( $intTotalExperienceInMonths . ( ( $intTotalExperienceInMonths > 1 ) ? ' Months ' : ' Month ' ) ) : '' );
		}
	}

	public function fetchManagerEmailAddresses( $objDatabase ) {
		$strSql = 'SELECT email_address FROM employees WHERE id = ' . ( int ) $this->getManagerEmployeeId();

		$arrstrEmailAddresses = fetchData( $strSql, $objDatabase );

		$arrstrFinalEmailAddreses = [];

		if( true == valArr( $arrstrEmailAddresses ) ) {
			foreach( $arrstrEmailAddresses as $arrstrEmailAddress ) {
				if( true == CValidation::validateEmailAddresses( $arrstrEmailAddress['email_address'] ) ) {
					$arrstrFinalEmailAddreses[$arrstrEmailAddress['email_address']] = $arrstrEmailAddress['email_address'];
				}
			}
		}

		return $arrstrFinalEmailAddreses;
	}

	public function fetchLeadPersonsByIds( $arrintLeadPersonIds, $objDatabase ) {
		return \Psi\Eos\Admin\CPersons::createService()->fetchLeadPersonsByEmployeeIdByIds( $this->getId(), $arrintLeadPersonIds, $objDatabase );
	}

	public function fetchEmployeePhoneNumbers( $objDatabase ) {
		$this->m_arrobjEmployeePhoneNumbers = CEmployeePhoneNumbers::fetchEmployeePhoneNumbersByEmployeeId( $this->m_intId, $objDatabase );

		return $this->m_arrobjEmployeePhoneNumbers;
	}

	public function fetchEmployeeAddress( $objDatabase ) {
		return CEmployeeAddresses::fetchEmployeeAddressesByEmployeeId( $this->getId(), $objDatabase );
	}

	public function fetchPsDocumentByEmployeeId( $objDatabase ) {
		$this->m_objDocument = CPsDocuments::fetchPsDocumentByEmployeeId( $this->m_intId, $objDatabase );

		return $this->m_objDocument;
	}

	public function fetchEmployeeAddressByAddressTypeId( $intAddressTypeId, $objDatabase ) {
		return CEmployeeAddresses::fetchEmployeeAddressByEmployeeIdByAddressTypeId( $this->m_intId, $intAddressTypeId, $objDatabase );
	}

	public function fetchEmployeeAddressesByAddressTypeIdByAddressTypeIds( $arrintAddressTypeIds, $objDatabase ) {
		return CEmployeeAddresses::fetchEmployeeAddressesByEmployeeIdByAddressTypeIds( $this->m_intId, $arrintAddressTypeIds, $objDatabase );
	}

	public function fetchEmployeePhoneNumberByPhoneNumberTypeId( $intPhoneNumberTypeId, $objDatabase ) {
		$this->m_objEmployeePhoneNumber = CEmployeePhoneNumbers::fetchEmployeePhoneNumberByEmployeeIdByPhoneNumberTypeId( $this->m_intId, $intPhoneNumberTypeId, $objDatabase );

		return $this->m_objEmployeePhoneNumber;
	}

	public function fetchEmployeePhoneNumbersByPhoneNumberTypeIds( $arrintPhonenumbersTypeIds, $objDatabase ) {
		return CEmployeePhoneNumbers::fetchEmployeePhoneNumbersByEmployeeIdByPhoneNumberTypeIds( $this->m_intId, $arrintPhonenumbersTypeIds, $objDatabase );
	}

	public function fetchEmployeeCard( $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeCards::createService()->fetchEmployeeCardByEmployeeId( $this->getId(), $objDatabase );
	}

	public function fetchEmployeeCardData( $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeCards::createService()->fetchEmployeeCardDataByEmployeeId( $this->getId(), $objDatabase );
	}

	public function fetchUser( $objDatabase ) {
		return CUsers::fetchUserByEmployeeId( $this->getId(), $objDatabase );
	}

	public function fetchCurrentEmployeeAssessmentEmployees( $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeAssessmentEmployees::createService()->fetchCurrentEmployeeAssessmentEmployeesByEmployeeId( $this->getId(), $objDatabase );
	}

	public function fetchEmployeeVacationScheduleByEmployeeId( $objDatabase ) {

		$this->m_objEmployeeVacationSchedule = CEmployeeVacationSchedules::fetchEmployeeVacationScheduleByEmployeeIdByYear( $this->m_intId, date( 'Y' ), $objDatabase );

		return $this->m_objEmployeeVacationSchedule;
	}

	public function fetchEmployeeAssessmentById( $intEmployeeAssessmentId, $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeAssessments::createService()->fetchEmployeeAssessmentByIdByEmployeeId( $intEmployeeAssessmentId, $this->getId(), $objDatabase );
	}

	public function fetchEmployeeAssessmentByIdByManagerEmployeeId( $intEmployeeAssessmentId, $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeAssessments::createService()->fetchEmployeeAssessmentByIdByManagerEmployeeId( $intEmployeeAssessmentId, $this->getId(), $objDatabase );
	}

	public function fetchCompletedEmployeeAssessments( $objDatabase, $boolEmployee = false ) {
		return \Psi\Eos\Admin\CEmployeeAssessments::createService()->fetchCompletedEmployeeAssessmentsByEmployeeId( $this->getId(), $objDatabase, $boolEmployee );
	}

	public function fetchDesignationName( $objDatabase ) {
		$objDesignation = CDesignations::fetchDesignationById( $this->getDesignationId(), $objDatabase );

		if( true == valObj( $objDesignation, 'CDesignation' ) ) {
			return $objDesignation->getName();
		} else {
			return false;
		}
	}

	public function fetchPrimaryAddressCountryCode( $objDatabase ) {

		$objAddress = CEmployeeAddresses::fetchEmployeeAddressByEmployeeId( $this->getId(), $objDatabase, CAddressType::PRIMARY );

		if( true == valObj( $objAddress, 'CEmployeeAddress' ) ) {
			return $objAddress->getCountryCode();
		} else {
			return false;
		}
	}

	public function fetchEmployeeAssessmentEmployeeByAssessmentId( $intEmployeeAssessmentId, $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeAssessmentEmployees::createService()->fetchCurrentEmployeeAssessmentEmployeeIdByEmployeeAssessmentIdByEmployeeId( $this->getId(), $intEmployeeAssessmentId, $objDatabase );
	}

	public function fetchEmployeeDependants( $objDatabase ) {
		return CEmployeeDependants::fetchEmployeeDependantsByEmployeeId( $this->getId(), $objDatabase );
	}

	public function fetchManagerEmployees( $objDatabase ) {
		return CEmployees::fetchManagerEmployees( $this->getId(), $objDatabase );
	}

	public function fetchCallAgent( $objVoipDatabase ) {
		return CCallAgents::fetchCallAgentByEmployeeId( $this->getId(), $objVoipDatabase );
	}

	public function fetchCallAgentByEmployeeId( $objVoipDatabase ) {
		return CCallAgents::fetchCallAgentDetailsByEmployeeId( $this->getId(), $objVoipDatabase );
	}

	public function fetchEmployeePreferenceByKey( $strKey, $objDatabase ) {
		return CEmployeePreferences::fetchEmployeePreferenceByEmployeeIdByKey( $this->getId(), $strKey, $objDatabase );
	}

	public function fetchEmployeePreferenceValueByKey( $strKey, $objDatabase ) {
		return CEmployeePreferences::fetchEmployeePreferenceValueByEmployeeIdByKey( $this->getId(), $strKey, $objDatabase );
	}

	public function sendApplyEmployeeVacationRequestEmail( $arrstrApplyVacationRequestDates, $strClientAdminBaseUrl, $objUser, $objAdminDatabase, $objEmailDatabase ) {

		$arrstrHrGroupEmailAddresses = CEmployeeAddresses::fetchSimpleEmailAddressesByGroupIdByCountryCode( CGroup::HR, CCountry::CODE_INDIA, $objAdminDatabase );

		if( true == valArr( $arrstrHrGroupEmailAddresses ) ) {
			$arrstrHrGroupEmailAddresses = array_keys( $arrstrHrGroupEmailAddresses );
			$strHrGroupEmailAddresses    = implode( ',', $arrstrHrGroupEmailAddresses );
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objSmarty->assign( 'employee', $this );
		$objSmarty->assign( 'apply_vacation_request_dates', $arrstrApplyVacationRequestDates );

		$objSmarty->assign( 'base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'client_admin_base_uri', $strClientAdminBaseUrl );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );

		$strSubject = $this->getNameFirst() . ' ' . $this->getNameLast() . ' : Apply Time Off Request Reminder';

		$objSystemEmailLibrary = new CSystemEmailLibrary();
		$objSystemEmail        = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject );

		// Email for employee
		$objSmarty->assign( 'is_from_hr_tab', false );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'email_templates/employee_vacation_requests_emails/send_apply_employee_vacation_request_email.tpl', PATH_INTERFACES_ADMIN . 'user_administration/admin_email_template.tpl' );

		$objTmpSystemEmail = clone $objSystemEmail;
		$objTmpSystemEmail->setHtmlContent( $strHtmlContent );
		$objTmpSystemEmail->setToEmailAddress( $this->getEmailAddress() );
		$arrobjSystemEmails[] = $objTmpSystemEmail;

		// Email for HR group

		if( false == is_null( $strHrGroupEmailAddresses ) ) {

			$objSmarty->assign( 'is_from_hr_tab', true );

			$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'email_templates/employee_vacation_requests_emails/send_apply_employee_vacation_request_email.tpl', PATH_INTERFACES_ADMIN . 'user_administration/admin_email_template.tpl' );

			$objTmpSystemEmail = clone $objSystemEmail;
			$objTmpSystemEmail->setHtmlContent( $strHtmlContent );
			$objTmpSystemEmail->setToEmailAddress( $strHrGroupEmailAddresses );
			$arrobjSystemEmails[] = $objTmpSystemEmail;
		}

		switch( NULL ) {
			default:
				$objEmailDatabase->begin();

				if( true == valArr( $arrobjSystemEmails ) ) {
					foreach( $arrobjSystemEmails as $objSystemEmail ) {
						if( false == $objSystemEmail->insert( $objUser->getId(), $objEmailDatabase ) ) {
							$objEmailDatabase->rollback();
							break 2;
						}
					}
				}

				$objEmailDatabase->commit();
		}
	}

	public function sendEmployeeDesignationDepartmentChangeEmail( $arrstrToDesignationChangeEmailAddress, $arrstrToDepartmentChangeEmailAddress, $arrmixEmployeeDetail, $objEmailDatabase, $objDatabase ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		$boolIsSendMail  = false;
		$objSystemEmailToBUHr = NULL;
		$strBUHREmail    = '';
		$objSelectedUser = $this->fetchUser( $objDatabase );

		if( true == valObj( $objSelectedUser, 'CUser' ) && true == in_array( $this->getDesignationId(), [ CDesignation::ASSOCIATE_TECHNICAL_PRODUCT_MANAGER, CDesignation::TECHNICAL_PROJECT_MANAGER, CDesignation::QA_MANAGER, CDesignation::ASSOCIATE_QA_MANAGER ] ) ) {
			$boolIsSendMail = true;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );
		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		if( true == valStr( $arrmixEmployeeDetail['designation_changed'] ) && true == is_numeric( $arrmixEmployeeDetail['employee_previous_designation_id'] ) && true == valStr( $arrmixEmployeeDetail['designation_name'] ) ) {
			$objDesignation = CDesignations::fetchDesignationById( $arrmixEmployeeDetail['employee_previous_designation_id'], $objDatabase );
			$objSmarty->assign( 'designation', $objDesignation );

			if( true == $boolIsSendMail && true == valStr( $arrmixEmployeeDetail['bu_hr_email_address'] ) ) {
				$strBUHREmail = $arrmixEmployeeDetail['bu_hr_email_address'];

				if( true == valArr( $arrstrToDesignationChangeEmailAddress ) ) {
					$strBUHREmployeeEmail = array_search( $arrmixEmployeeDetail['bu_hr_email_address'], $arrstrToDesignationChangeEmailAddress );
					if( true == valStr( $strBUHREmployeeEmail ) ) {
						unset( $arrstrToDesignationChangeEmailAddress[$strBUHREmployeeEmail] );
					}
				}
			}
		}

		if( true == valArr( $arrstrToDesignationChangeEmailAddress ) ) {
			$strDesignationEmailAddress = implode( ',', $arrstrToDesignationChangeEmailAddress );
		}

		if( true == valArr( $arrstrToDepartmentChangeEmailAddress ) ) {
			$strDepartmentEmailAddress = implode( ',', $arrstrToDepartmentChangeEmailAddress );
		}

		if( true == valStr( $arrmixEmployeeDetail['department_changed'] ) && true == is_numeric( $arrmixEmployeeDetail['employee_previous_department_id'] ) && true == valStr( $arrmixEmployeeDetail['department_name'] ) ) {
			$objDepartment = CDepartments::fetchDepartmentById( $arrmixEmployeeDetail['employee_previous_department_id'], $objDatabase );
			$objSmarty->assign( 'department', $objDepartment );
		}

		$objSmarty->assign( 'employee_detail', $arrmixEmployeeDetail );
		$objSmarty->assign( 'base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'employee', $this );
		$objSmarty->assign( 'DEV_SUPPORT_EMAIL_ADDRESS', CSystemEmail::DEV_SUPPORT_EMAIL_ADDRESS );
		$objSmarty->assign( 'DEVSUPPORT_EMAIL_ADDRESS', CSystemEmail::DEVSUPPORT_EMAIL_ADDRESS );
		$objSmarty->assign( 'MAINTENANCE_EMAIL_ADDRESS', CSystemEmail::MAINTENANCE_EMAIL_ADDRESS );
		$objSmarty->assign( 'email_image_path', 'https://rcommon.entrata.com' . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );
		$objSmarty->assign( 'country_code', CCountry::CODE_INDIA );
		$objSmarty->assign( 'logo_url', ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'https://www.xento.com/images/xento-logo.png' : CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );

		$objSystemEmailLibrary = new CSystemEmailLibrary();

		if( true == valStr( $arrmixEmployeeDetail['designation_changed'] ) ) {

			$strHtmlContent       = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employees/profile/designation_change_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
			$strHtmlEmailContent  = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employees/profile/designation_change_email_to_hr.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
			$strHtmlEmailContentIT  = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employees/profile/designation_change_email_it.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
			$strToEmailAddress		= 'opssupport@entrata.com';
			$arrobjSystemEmails[] = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, 'Employee Designation Updated', $strHtmlContent, $strDesignationEmailAddress, $intIsSelfDestruct = 0, CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS );
			$arrintDesignationIds = [ CDesignation::TECHNICAL_PROJECT_MANAGER, CDesignation::TEAM_LEAD_QA, CDesignation::TEAM_LEAD, CDesignation::QA_MANAGER, CDesignation:: ASSOCIATE_TECHNICAL_PRODUCT_MANAGER, CDesignation::ASSOCIATE_TEAM_LEAD_DEV, CDesignation::ASSOCIATE_QA_MANAGER, CDesignation::ASSOCIATE_TEAM_LEAD ];

			if( true == in_array( $this->getDesignationId(), $arrintDesignationIds ) ) {
				$arrobjSystemEmails[] = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, 'Employee Designation Updated', $strHtmlEmailContentIT, $strToEmailAddress, $intIsSelfDestruct = 0, CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS );
			}
			if( true == $boolIsSendMail ) {
				$objSystemEmailToBUHr = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, 'Employee Designation Updated', $strHtmlEmailContent, $strBUHREmail, $intIsSelfDestruct = 0, CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS );
			}
			if( true == valObj( $objSystemEmailToBUHr, 'CSystemEmail' ) ) {
				array_push( $arrobjSystemEmails, $objSystemEmailToBUHr );
			}
		}

		if( true == valStr( $arrmixEmployeeDetail['department_changed'] ) ) {
			$strHtmlContent       = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employees/profile/department_change_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
			$arrobjSystemEmails[] = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, 'Employee Department Updated', $strHtmlContent, $strDepartmentEmailAddress, $intIsSelfDestruct = 0, CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS );
		}

		$boolIsValid = true;

		switch( NULL ) {
			default:
				$objEmailDatabase->begin();
				if( true == valArr( $arrobjSystemEmails ) ) {
					foreach( $arrobjSystemEmails as $objSystemEmail ) {
						if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
							if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
								if( false == $objSystemEmail->sendEmail( $objEmailDatabase ) ) {
									$boolIsValid = false;
									break;
								}
							}
						}
					}
				}
		}

		$objEmailDatabase->commit();

		return $boolIsValid;
	}

	public function sendEmployeeProfileUpdateEmail( $objUser, $objExistingEmployeeData, $arrstrMessages, $objEmailDatabase ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		date_default_timezone_set( 'Asia/Calcutta' );

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		$objSmarty->assign( 'base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'existing_data', $objExistingEmployeeData );
		$objSmarty->assign( 'mail_content', $strMailBody );
		$objSmarty->assign( 'messages', $arrstrMessages );
		$objSmarty->assign( 'user', $objUser );
		$objSmarty->assign( 'employee', $this );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employees/profile/employee_profile_update_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$strSubject     = 'Employee Profile Updated';

		$objSystemEmailLibrary = new CSystemEmailLibrary();
		$objSystemEmail        = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, $strHtmlContent, CSystemEmail::XENTO_HR_EMAIL_ADDRESS );

		$boolIsValid = true;

		switch( NULL ) {
			default:
				if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
					if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
						$objEmailDatabase->rollback();
						if( false == $objSystemEmail->sendEmail( $objEmailDatabase ) ) {
							$boolIsValid = false;
							break;
						}
					}
				}
		}

		$objEmailDatabase->commit();

		return $boolIsValid;
	}

	public function sendEmployeeEnableDisableEmail( $arrstrEmailAddress, $intEmployeeStatusTypeId, $objEmailDatabase, $objDatabase ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		if( false == is_numeric( $intEmployeeStatusTypeId ) || true == is_null( $intEmployeeStatusTypeId ) ) {
			return false;
		}

		if( CEmployeeStatusType::PREVIOUS == $intEmployeeStatusTypeId ) {
			$strEmployeeStatus = 'Enabled';
		} else {
			$strEmployeeStatus = 'Disabled';
		}

		$objSelectedEmployeeUser = CUsers::fetchUserByEmployeeId( $this->getId(), $objDatabase );

		if( false == valObj( $objSelectedEmployeeUser, 'CUser' ) ) {
			return false;
		} else {
			$strEmployeeUserName = $objSelectedEmployeeUser->getUserName();
		}

		$objEmployeeDepartment = CDepartments::fetchDepartmentByEmployeeId( $this->getId(), $objDatabase );

		if( false == valObj( $objEmployeeDepartment, 'CDepartment' ) ) {
			return false;
		} else {
			$strEmployeeDepartmentName = $objEmployeeDepartment->getName();
		}

		$strToEmailAddress = implode( ',', $arrstrEmailAddress );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		$objSmarty->assign( 'employee', $this );
		$objSmarty->assign( 'employee_status_type', $strEmployeeStatus );
		$objSmarty->assign( 'employee_username', $strEmployeeUserName );
		$objSmarty->assign( 'employee_department', $strEmployeeDepartmentName );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employees/profile/employee_disable_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$strSubject     = 'Employee Status Update : ' . ' ' . $this->getNameFirst() . ' ' . $this->getNameMiddle() . ' ' . $this->getNameLast() . ' is ' . $strEmployeeStatus;

		$objSystemEmailLibrary = new CSystemEmailLibrary();
		$objSystemEmail        = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, $strHtmlContent, $strToEmailAddress, $intIsSelfDestruct = 0 );

		$boolIsValid = true;

		switch( NULL ) {
			default:
				if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
					if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
						$objEmailDatabase->rollback();
						if( false == $objSystemEmail->sendEmail( $objEmailDatabase ) ) {
							$boolIsValid = false;
							break;
						}
					}
				}
		}

		$objEmailDatabase->commit();

		return $boolIsValid;
	}

	public function sendEmailOnChangeEmailAddress( $arrstrEmailAddress, $objEmailDatabase, $objDatabase ) {
		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSelectedEmployeeUser = CUsers::fetchUserByEmployeeId( $this->getId(), $objDatabase );

		if( false == valObj( $objSelectedEmployeeUser, 'CUser' ) ) {
			return false;
		} else {
			$strEmployeeUserName = $objSelectedEmployeeUser->getUserName();
		}

		$objEmployeeDepartment = CDepartments::fetchDepartmentByEmployeeId( $this->getId(), $objDatabase );

		if( false == valObj( $objEmployeeDepartment, 'CDepartment' ) ) {
			return false;
		} else {
			$strEmployeeDepartmentName = $objEmployeeDepartment->getName();
		}

		$strToEmailAddress = implode( ',', $arrstrEmailAddress );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		if( true == is_null( $this->getDesignationName() ) ) {
			$this->setDesignationName( $this->fetchDesignationName( $objDatabase ) );
		}

		if( true == is_null( $this->getCountryCode() ) ) {
			$this->setCountryCode( $this->fetchPrimaryAddressCountryCode( $objDatabase ) );
		}

		$objSmarty->assign( 'employee', $this );
		$objSmarty->assign( 'employee_department', $strEmployeeDepartmentName );
		$objSmarty->assign( 'email_change', true );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employees/profile/employee_disable_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$strSubject     = 'Employee E-mail Update : ' . ' ' . $this->getNameFirst() . ' ' . $this->getNameMiddle() . ' ' . $this->getNameLast() . ' email is ' . $this->getEmailAddress();

		$objSystemEmailLibrary = new CSystemEmailLibrary();
		$objSystemEmail        = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, $strHtmlContent, $strToEmailAddress, $intIsSelfDestruct = 0 );

		$boolIsValid = true;

		switch( NULL ) {
			default:
				if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
					if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
						$objEmailDatabase->rollback();
						if( false == $objSystemEmail->sendEmail( $objEmailDatabase ) ) {
							$boolIsValid = false;
							break;
						}
					}
				}
		}

		$objEmailDatabase->commit();

		return $boolIsValid;
	}

	public function fetchReviewingEmployeeScheduledSurveys( $objDatabase ) {
		return CScheduledSurveys::fetchScheduledSurveysByResponderReferenceId( $this->getId(), $objDatabase );
	}

	public function fetchReviewedEmployeeScheduledSurveys( $objDatabase ) {
		return CScheduledSurveys::fetchScheduledSurveysBySubjectReferenceId( $this->getId(), $objDatabase );
	}

	public function fetchSelfEvalutionScheduledSurvey( $objDatabase ) {
		return CScheduledSurveys::fetchScheduledSurveysByEmployeeId( $this->getId(), $objDatabase );
	}

	public function addEmployeeDesignationChangeOperations( $arrobjExistingRoles, $objDatabase, $boolFromScript = false ) {

		$arrintDesignationIds = [ CDesignation::TECHNICAL_PROJECT_MANAGER, CDesignation::TEAM_LEAD_QA, CDesignation::TEAM_LEAD, CDesignation::QA_MANAGER, CDesignation:: ASSOCIATE_TECHNICAL_PRODUCT_MANAGER, CDesignation::ASSOCIATE_TEAM_LEAD_DEV, CDesignation::ASSOCIATE_QA_MANAGER, CDesignation::ASSOCIATE_TEAM_LEAD ];

		$objSelectedUser = $this->fetchUser( $objDatabase );
		if( true == valObj( $objSelectedUser, 'CUser' ) && true == in_array( $this->getDesignationId(), $arrintDesignationIds ) ) {

			$strTitle       = 'Give access to ' . $this->getNameFull();
			$strDescription = '<b>Give access to ' . $this->getNameFull() . ' for following things :</b></br></br> ';

			// adding TPM to UserRole
			if( true == in_array( $this->getDesignationId(), [ CDesignation:: ASSOCIATE_TECHNICAL_PRODUCT_MANAGER, CDesignation::TECHNICAL_PROJECT_MANAGER ] ) ) {
				$objUserRole     = new CUserRole();
				$arrobjUserRoles = [];
				$arrintUserRoles = [ CRole::ALLOW_STORY_POINT_EDITING, CRole::RAPID_ACCESS, CRole::STANDARD_ACCESS ];
				if( true == valArr( $arrobjExistingRoles ) ) {
					$arrintExistingRoleIds = array_keys( $arrobjExistingRoles );
					$arrintUserRoles       = array_diff( $arrintUserRoles, $arrintExistingRoleIds );
				}
				$objUserRole->setUserId( $objSelectedUser->getId() );

				foreach( $arrintUserRoles as $intUserRole ) {
					$objCloneUserRole = clone $objUserRole;
					$objCloneUserRole->setRoleId( $intUserRole );
					$arrobjUserRoles[] = $objCloneUserRole;
				}

				foreach( $arrobjUserRoles as $objUserRole ) {
					if( true == $boolFromScript ) {
						if( false == $objUserRole->insert( SYSTEM_USER_ID, $objDatabase ) ) {
							$objDatabase->rollback();
							break;
						}
					} else {
						if( false == $objUserRole->insert( $this->m_objUser, $objDatabase ) ) {
							$objDatabase->rollback();
							break;
						}
					}
				}
				$strDescription .= '</br>- Access to execute SELECT queries on Live DR DBs</br>- Update a module in CA >> Development >> Setup >> Entrata Modules
									</br>- Inclusion in tpm@xento.com group';
			}

			if( true == in_array( $this->getDesignationId(), [ CDesignation::TECHNICAL_PROJECT_MANAGER, CDesignation::ASSOCIATE_TECHNICAL_PRODUCT_MANAGER, CDesignation::QA_MANAGER, CDesignation::ASSOCIATE_QA_MANAGER ] ) ) {
				$strDescription .= '</br>- Automation dashboard';
			}
			if( true == in_array( $this->getDesignationId(), [ CDesignation::TECHNICAL_PROJECT_MANAGER, CDesignation::ASSOCIATE_TECHNICAL_PRODUCT_MANAGER, CDesignation::TEAM_LEAD, CDesignation::ASSOCIATE_TEAM_LEAD_DEV ] ) ) {
				$strDescription .= '</br>- Code Review Group</br>- SQL deployment from CA';
			}
			if( true == in_array( $this->getDesignationId(), [ CDesignation::ASSOCIATE_TEAM_LEAD_DEV, CDesignation::TEAM_LEAD ] ) ) {
				$strDescription .= '</br>- Inclusion in teamleads@xento.com email group';
			}
			if( false == in_array( $this->getDesignationId(), [ CDesignation::TEAM_LEAD_QA, CDesignation::ASSOCIATE_TEAM_LEAD ] ) ) {
				$strDescription .= '</br>- execute_sql module access in CA';
			}

			if( true == in_array( $this->getDesignationId(), [ CDesignation::TEAM_LEAD_QA, CDesignation::ASSOCIATE_TEAM_LEAD ] ) ) {
				$strDescription .= '</br>- Inclusion in qatl@xento.com email group </br>- Inclusion in  QA TL Group of clientadmin';
			}

			$objITTask = $this->createTask( CTaskType::HELPDESK, $strTitle, $strDescription, CUser::ID_SYSTEM, CGroup::IT_SUPPORT );

			$objDatabase->begin();
			if( true == $boolFromScript ) {
				if( false == $objITTask->insert( SYSTEM_USER_ID, $objDatabase ) ) {
					$objDatabase->rollback();
				}
			} else {
				if( false == $objITTask->insert( $this->m_objUser->getId(), $objDatabase ) ) {
					$objDatabase->rollback();
				}
			}

			$objDatabase->commit();
		}

	}

	/**
	 * Validation Functions
	 *
	 */

	public function valEmployeeStatusTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intEmployeeStatusTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_status_type_id', 'Employee type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDepartmentId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intDepartmentId ) || false == is_numeric( $this->m_intDepartmentId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_id', 'Department is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOffice( $strEmployeeCountryCode = NULL, $objDatabase ) {
		$boolIsValid = true;

		if( false == isset( $this->m_intOfficeId ) || false == is_numeric( $this->m_intOfficeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_id', 'Office is required.' ) );

		} elseif( true == $strEmployeeCountryCode ) {
			$objOffice = \Psi\Eos\Admin\COffices::createService()->fetchOfficeById( $this->m_intOfficeId, $objDatabase );

			if( false == valObj( $objOffice, 'COffice' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_id', 'Invalid office.' ) );

				return $boolIsValid;
			}

			if( $strEmployeeCountryCode != $objOffice->getCountryCode() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_id', 'Office and Country does not match.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( false == isset( $this->m_strNameFirst ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name is required.' ) );
		}

		if( true == $boolIsValid && 1 >= strlen( $this->m_strNameFirst ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name should contain more than one character.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( false == isset( $this->m_strNameLast ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name is required.' ) );
		}

		if( true == $boolIsValid && 1 >= strlen( $this->m_strNameLast ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', ' Last name should contain more than one character.' ) );
		}

		return $boolIsValid;
	}

	public function valBloodGroup() {
		$boolIsValid = true;

		if( false == isset( $this->m_strBloodGroup ) || false == valStr( $this->m_strBloodGroup ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'blood_group', 'Please select blood group. ' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valBirthDate() {
		$boolIsValid = true;

		if( false == isset( $this->m_strBirthDate ) || false == CValidation::validateDate( $this->m_strBirthDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'A valid birth date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDateStarted( $objDatabase = NULL, $strPreviousTerminationDate = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_strDateStarted ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_started', 'Date started is required.' ) );
		}

		if( true == isset( $this->m_strDateStarted ) && false == CValidation::validateDate( $this->m_strDateStarted ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_started', 'Date started is not a valid date.' ) );
		}

		if( false == is_null( $this->getDateStarted() ) && false == is_null( $this->getBirthDate() ) ) {
			$strDateDifference = strtotime( $this->getBirthDate() ) - strtotime( $this->getDateStarted() );

			if( 0 <= $strDateDifference ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_started', 'Date started should be greater than birth date.' ) );
			}
		}

		// to validate start date for previous employees.

		if( true == $boolIsValid && true == valObj( $objDatabase, 'CDatabase' ) ) {

			$arrstrEmploymentEmploymentHisory = CEmployeeEmploymentHistories::fetchLastTerminationDateByEmployeeId( $this->getId(), $objDatabase );

			if( true == valStr( $arrstrEmploymentEmploymentHisory[0]['date_terminated'] ) && ( strtotime( $arrstrEmploymentEmploymentHisory[0]['date_terminated'] ) >= strtotime( $strPreviousTerminationDate ) ) ) {
				$strPreviousTerminationDate = $arrstrEmploymentEmploymentHisory[0]['date_terminated'];
			}

			if( true == valStr( $strPreviousTerminationDate ) && 0 <= strtotime( $strPreviousTerminationDate ) - strtotime( $this->getDateStarted() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_started', 'Date started should be greater than previous termination date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDateTerminated() {
		$boolIsValid = true;

		if( CEmployeeStatusType::PREVIOUS == $this->getEmployeeStatusTypeId() && false == valStr( $this->getDateTerminated() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_terminated', 'Date terminated is required.' ) );
		}

		if( true == $boolIsValid && false == is_null( $this->getDateTerminated() ) ) {
			$strDateDifference = strtotime( $this->getDateStarted() ) - strtotime( $this->getDateTerminated() );

			if( 0 <= $strDateDifference ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_terminated', 'Date terminated should be greater than date started.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDateConfirmed() {
		$boolIsValid = true;

		$objUser = $this->getUser();
		if( false == is_null( $this->getDateConfirmed() ) && false == CValidation::validateDate( $this->getDateConfirmed() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_confirmed', 'Date confirmed is not a valid date.' ) );
		}

		if( false == is_null( $this->getDateConfirmed() ) ) {
			$strDateDifference = strtotime( $this->getDateStarted() ) - strtotime( $this->getDateConfirmed() );

			if( CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_ON_SITE != $this->getEmployeeApplicationStatusTypeId() ) {
				if( CCountry::CODE_INDIA == $objUser->getEmployee()->getCountryCode() && 0 < $strDateDifference ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_confirmed', 'Date confirmation should be same or greater than date started.' ) );
				} elseif( CCountry::CODE_USA == $objUser->getEmployee()->getCountryCode() && 0 <= $strDateDifference ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_confirmed', 'Date confirmation should be greater than date started.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valAnniversaryDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getAnniversaryDate() ) && false == CValidation::validateDate( $this->getAnniversaryDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'anniversary_date', 'Anniversary date is not a valid date.' ) );
		}

		if( false == is_null( $this->getAnniversaryDate() ) ) {
			$strDateDifference = strtotime( $this->getBirthDate() ) - strtotime( $this->getAnniversaryDate() );

			if( 0 <= $strDateDifference ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'anniversary_date', 'Anniversary date should be greater than birth date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valTaxNumber( $objDatabase = NULL ) {
		$boolIsValid         = true;
		$boolIsAllowValidate = true;

		$objUser = $this->getUser();

		if( true == valObj( $objUser, 'CUser' ) && ( false == $objUser->getIsSuperUser() || ( true == valArr( $objUser->getAssociatedRoleIds() ) && false == in_array( CRole::ACCESS_SSN, $objUser->getAssociatedRoleIds() ) ) ) ) {
			$boolIsAllowValidate = false;
		}

		if( true == valObj( $objUser, 'CUser' ) && CDepartment::HR == $objUser->getEmployee()->getDepartmentId() && CCountry::CODE_INDIA == $objUser->getEmployee()->getCountryCode() ) {
			$boolIsAllowValidate = true;
		}

		if( true == $boolIsAllowValidate && 'development' != CONFIG_ENVIRONMENT && ( true == is_null( $this->getTaxNumber() ) || 0 == strlen( trim( $this->getTaxNumber() ) ) ) && ( ( CCountry::CODE_INDIA == $this->getCountryCode() && CCountry::CODE_INDIA == $this->m_objUser->getEmployee()->getCountryCode() ) || ( true == is_null( $this->getCountryCode() ) && CCountry::CODE_INDIA == $this->m_objUser->getEmployee()->getCountryCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', 'Tax number is required.' ) );

			return $boolIsValid;
		}

		if( true == $boolIsAllowValidate && false == is_null( trim( $this->getTaxNumber() ) ) && false == is_null( $objDatabase ) && 'production' == CONFIG_ENVIRONMENT ) {
			$strSql   = ' WHERE tax_number_encrypted=\'' . $this->getTaxNumberEncrypted() . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$intCount = CEmployees::fetchRowCount( $strSql, 'employees', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', 'An employee already exists with this tax number.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valEmployeeNumber( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getEmployeeNumber() ) || 0 == strlen( trim( $this->getEmployeeNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_number', 'Employee number is required.' ) );

			return $boolIsValid;
		}

		if( false == is_null( trim( $this->getEmployeeNumber() ) ) && false == is_null( $objDatabase ) ) {
			$strSql   = ' WHERE employee_number=\'' . $this->getEmployeeNumber() . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$intCount = CEmployees::fetchRowCount( $strSql, 'employees', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_number', 'An employee already exists with this employee number.' ) );
			}
		}

		return $boolIsValid;
	}

	public function checkDuplicateEmailAddress( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( preg_match( '/@.*propertysolution/', \Psi\CStringService::singleton()->strtolower( $this->m_strEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Change official email address domain name from propertysolutions to entrata.' ) );
		}

		if( true == $boolIsValid && 0 < strlen( $this->m_strEmailAddress ) ) {
			$strSql      = 'SELECT COUNT(1) FROM employees  WHERE email_address=\'' . $this->m_strEmailAddress . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$arrintCount = ( array ) fetchData( $strSql, $objDatabase );

			if( 0 < $arrintCount[0]['count'] ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Official email address already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valEmailAddress( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( 'production' != CONFIG_ENVIRONMENT ) {
			return $boolIsValid;
		}

		if( preg_match( '/@.*propertysolution/', \Psi\CStringService::singleton()->strtolower( $this->m_strEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Change official email address domain name from propertysolutions to entrata.' ) );
		}

		if( false == isset( $this->m_strEmailAddress ) || false == CValidation::validateEmailAddresses( \Psi\CStringService::singleton()->strtolower( $this->m_strEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'A valid official email address is required.' ) );
		}

		if( true == $boolIsValid ) {
			$strSql      = 'SELECT COUNT(1) FROM employees  WHERE email_address=\'' . $this->m_strEmailAddress . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$arrintCount = ( array ) fetchData( $strSql, $objDatabase );

			if( 0 < $arrintCount[0]['count'] ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Official email address already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPermanentEmailAddress( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strPermanentEmailAddress ) || false == CValidation::validateEmailAddresses( $this->m_strPermanentEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'permanent_email_address', 'A valid permanent email address is required.' ) );
		}

		if( true == $boolIsValid ) {

			$strSql   = ' WHERE permanent_email_address=\'' . $this->getPermanentEmailAddress() . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$intCount = CEmployees::fetchRowCount( $strSql, 'employees', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'permanent_email_address', 'Permanent email address already exists.' ) );
			}
		}

		if( true == $boolIsValid && true == \Psi\CStringService::singleton()->strstr( $this->m_strPermanentEmailAddress, 'xento' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'permanent_email_address', 'Domain for permanent email address should not be xento.' ) );
		}

		return $boolIsValid;

	}

	public function valGmailUsername( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strGmailUsername ) || false == CValidation::validateEmailAddresses( $this->m_strGmailUsername ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gmail_username', 'A valid username is required.' ) );
		}

		if( 'production' == CONFIG_ENVIRONMENT ) {
			$strSql = "WHERE gmail_username ='" . $this->m_strGmailUsername . "' and id<>" . $this->m_intId;

			$intCount = CEmployees::fetchRowCount( $strSql, 'employees', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gmail_username', 'Username already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valAgentPhoneExtensionId() {
		if( false == isset( $this->m_intAgentPhoneExtensionId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agent_phone_extension_id', 'Agent extension is required.' ) );

			return false;
		}

		return true;
	}

	public function valPhysicalPhoneExtensionId() {
		if( false == isset( $this->m_intPhysicalPhoneExtensionId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'physical_phone_extension_id', 'Phone extension is required.' ) );

			return false;
		}

		return true;
	}

	public function valEmployeeExtensionId() {
		if( false == isset( $this->m_intPhysicalPhoneExtensionId ) && false == isset( $this->m_intAgentPhoneExtensionId ) ) {
			return false;
		}

		return true;
	}

	public function valConflictPhysicalPhoneExtensionId( $objDatabase, $boolIsSharedPhysicalPhoneExtension = false ) {
		$boolIsValid = true;

		if( false == valObj( $objDatabase, 'CDatabase' ) || false == valId( $this->getPhysicalPhoneExtensionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'physical_phone_extension_id', 'Phone extension is required.' ) );
			return $boolIsValid;
		}

		if( false == preg_match( '/^(\d{3}(\d{1})?)?$/', $this->getPhysicalPhoneExtensionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'physical_phone_extension_id', 'Phone extension must be 3 or 4 digit valid number.' ) );
			return $boolIsValid;
		}

		if( true == valId( CEmployees::fetchRowCount( 'WHERE physical_phone_extension_id=\'' . $this->getPhysicalPhoneExtensionId() . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' ), 'employees', $objDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'physical_phone_extension_id', 'Phone extension is already occupied.' ) );
		}

		if( false == $boolIsValid && false == $boolIsSharedPhysicalPhoneExtension ) {
			return false;
		}

		return $boolIsValid;
	}

	public function valConflictAgentPhoneExtensionId( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_numeric( $this->getAgentPhoneExtensionId() ) && false == is_null( $objDatabase ) ) {
			$strSql   = ' WHERE agent_phone_extension_id=\'' . $this->getAgentPhoneExtensionId() . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$intCount = CEmployees::fetchRowCount( $strSql, 'employees', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agent_phone_extension_id', 'Agent extension is already occupied.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valWorkStationIpAddressForIt( $objDatabase, $objDeployDatabase = NULL ) {
		if( true == is_null( $this->m_strWorkStationIpAddress ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'work_station_ip_address', 'Work station IP address is required.' ) );

			return false;
		}

		if( false == $this->valWorkStationIpAddress( $objDatabase, $objDeployDatabase ) ) {
			return false;
		}

		return true;
	}

	public function valWorkStationIpAddress( $objDatabase, $objDeployDatabase = NULL ) {

		$boolIsValid = true;
		if( true == valStr( $this->getWorkStationIpAddress() ) ) {
			if( false == CValidation::validateIpAddress( $this->getWorkStationIpAddress() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'work_station_ip_address', 'Invalid work station IP address.' ) );
			} elseif( 0 < CEmployees::fetchRowCount( ' WHERE work_station_ip_address=\'' . $this->getWorkStationIpAddress() . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->getId() : '' ), 'employees', $objDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'work_station_ip_address', 'Work station IP address already exists. ' ) );
			} elseif( true == valObj( $objDeployDatabase, 'CDatabase' ) && 0 < CHardwares::fetchRowCount( ' WHERE environment_type_id IN ( ' . implode( ', ', CEnvironmentType::$c_arrintLocalEnvironmentTypes ) . ' ) AND ip_address = \'' . $this->getWorkStationIpAddress() . '\'', 'hardwares', $objDeployDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'work_station_ip_address', 'Server IP address is not allowed ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valBaseSalary() {

		if( true == is_null( $this->m_strBaseSalaryEncrypted ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_salary', 'Base salary is required.' ) );

			return false;
		}

		return true;
	}

	public function valDnsHandle( $objDatabase ) {

		if( true == valStr( $this->getDnsHandle() ) && ( true == valObj( $objDatabase, 'CDatabase' ) ) && 0 < CEmployees::fetchRowCount( ' WHERE dns_handle=\'' . $this->getDnsHandle() . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' ), 'employees', $objDatabase ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dns_handle', 'DNS handle is in use, please choose different handle.' ) );

			return false;
		}

		return true;
	}

	public function valDesignationId( $strEmployeeCountryCode = NULL, $objDatabase ) {

		if( false == is_numeric( $this->m_intDesignationId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'designation_id', 'Designation is required.' ) );

			return false;
		} elseif( true == $strEmployeeCountryCode ) {
			$objDesignation            = CDesignations::fetchDesignationById( $this->m_intDesignationId, $objDatabase );
			$strDesignationCountryCode = $objDesignation->getCountryCode();

			if( $strEmployeeCountryCode != $strDesignationCountryCode ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'designation_id', 'Designation selected is not applicable to the country in primary address.' ) );

				return false;
			}
		}

		return true;
	}

	public function valGender() {
		if( true == is_null( $this->m_strGender ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gender', 'Gender is required.' ) );

			return false;
		}

		return true;
	}

	public function valDesk() {
		$boolIsValid = true;

		if( false == isset( $this->m_intOfficeDeskId ) || false == is_numeric( $this->m_intOfficeDeskId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_desk_id', 'Desk number is required.' ) );

		}

		return $boolIsValid;
	}

	public function valResignedOn() {
		$boolIsValid = true;

		if( false == isset( $this->m_strResignedOn ) || false == valStr( $this->m_strResignedOn ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resigned_on', 'Please select resigned on date. ' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valExpectedTerminationDate() {
		$boolIsValid = true;

		if( false == isset( $this->m_strExpectedTerminationDate ) || false == valStr( $this->m_strExpectedTerminationDate ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_working_day', 'Please select last working date.' ) );
			$boolIsValid = false;
		}

        if( $this->m_intEmployeeStatusTypeId != CEmployeeStatusType::PREVIOUS && strtotime( $this->m_strExpectedTerminationDate ) < strtotime( date( 'm/d/Y' ) ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_working_day', 'Last working date must be greater than or equal to today\'s date. ' ) );
            $boolIsValid = false;
        }

		return $boolIsValid;
	}

	public function validateSalesCommitLock( $objPsLeadFilter, $objDatabase ) {

		$strSql = 'SELECT count(ssmc.id), ssmc.employee_id, e.name_full FROM stats_sales_monthly_commits ssmc JOIN employees e ON ( ssmc.employee_id = e.id) WHERE ssmc.employee_id IN ( ' . implode( ',', $objPsLeadFilter->getEmployees() ) . ' ) AND ssmc.month::DATE = DATE_TRUNC( \'month\', \'' . $objPsLeadFilter->getCloseDate() . '\'::DATE ) AND ssmc.locked_by IS NOT NULL GROUP BY ssmc.employee_id, e.name_full;';

		// Check to see if their is a locked commit for the month
		$arrstrEmployeesLockedCommits = fetchData( $strSql, $objDatabase );

		if( \Psi\Libraries\UtilFunctions\count( $arrstrEmployeesLockedCommits ) <= \Psi\Libraries\UtilFunctions\count( $objPsLeadFilter->getEmployees() ) ) {
			return true;
		}

		$strCommitMsg = '';
		foreach( $arrstrEmployeesLockedCommits as $strEmployeeLockedCommit ) {
			$strCommitMsg .= $strEmployeeLockedCommit['name_full'] . ', ';
		}

		if( 0 < strlen( $strCommitMsg ) ) {
			$strCommitMsg = rtrim( $strCommitMsg, ', ' );
			$strCommitMsg = ' for ' . $strCommitMsg;
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrEmployeesLockedCommits ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commit', 'This month\'s sales commitment has already been locked' . $strCommitMsg . '.' ) );

			return false;
		}

		return true;
	}

	public function validateSalesCommitUnlock( $arrintAssociatedRoleIds ) {

		if( false == in_array( CRole::SALES_MANAGER, $arrintAssociatedRoleIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commit', 'Only sales managers can unlock a sales commitment.' ) );

			return false;
		}

		return true;
	}

	public function validate( $strAction, $objDatabase = NULL, $strEmployeeCountryCode = NULL, $boolIsSharedPhysicalPhoneExtension = false, $strPreviousTerminationDate = NULL, $objDeployDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valOffice( $strEmployeeCountryCode, $objDatabase );
				$boolIsValid &= $this->valEmployeeStatusTypeId();
				$boolIsValid &= $this->valDepartmentId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress( $objDatabase );
				$boolIsValid &= $this->valTaxNumber( $objDatabase );

                if( CCountry::CODE_USA == $strEmployeeCountryCode ) {
                    $boolIsValid &= $this->valPermanentEmailAddress( $objDatabase );
                }

				if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {
					$boolIsValid &= $this->valBloodGroup();
					$boolIsValid &= $this->valEmployeeNumber( $objDatabase );
					$boolIsValid &= $this->valPermanentEmailAddress( $objDatabase );
					$boolIsValid &= $this->valGender();
					$boolIsValid &= $this->valDateConfirmed();
				}

				if( true == is_numeric( $this->getEmployeeStatusTypeId() ) && $this->getEmployeeStatusTypeId() != CEmployeeStatusType::SYSTEM ) {
					$boolIsValid &= $this->valBirthDate();
					$boolIsValid &= $this->valDateStarted( $objDatabase, $strPreviousTerminationDate );
				}

				$boolIsValid &= $this->valAnniversaryDate();
				$boolIsValid &= $this->valDesignationId( $strEmployeeCountryCode, $objDatabase );
				$boolIsValid &= $this->valDnsHandle( $objDatabase );
				$boolIsValid &= $this->valWorkStationIpAddress( $objDatabase );
				$boolIsValid &= $this->valDateTerminated();
				if( CCountry::CODE_USA == $strEmployeeCountryCode && $this->getDepartmentId() != CDepartment::CALL_CENTER ) {
					$boolIsValid &= $this->valConflictPhysicalPhoneExtensionId( $objDatabase, $boolIsSharedPhysicalPhoneExtension );
				}
				$boolIsValid &= $this->valConflictAgentPhoneExtensionId( $objDatabase );
				break;

			case 'validate_insert_for_hr':
				if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {
					$boolIsValid &= $this->valEmployeeNumber( $objDatabase );
					$boolIsValid &= $this->valGender();
					$boolIsValid &= $this->valTaxNumber( $objDatabase );
					$boolIsValid &= $this->valDateConfirmed();
				} elseif( CCountry::CODE_USA == $strEmployeeCountryCode && $this->getDepartmentId() != CDepartment::CALL_CENTER ) {
					$boolIsValid &= $this->valPhysicalPhoneExtensionId();
					$boolIsValid &= $this->valConflictPhysicalPhoneExtensionId( $objDatabase, $boolIsSharedPhysicalPhoneExtension );
				}
				$boolIsValid &= $this->checkDuplicateEmailAddress( $objDatabase );
				$boolIsValid &= $this->valOffice( $strEmployeeCountryCode, $objDatabase );
				$boolIsValid &= $this->valAnniversaryDate();
				$boolIsValid &= $this->valDesignationId( $strEmployeeCountryCode, $objDatabase );
				$boolIsValid &= $this->valDateTerminated();

				if( true == is_numeric( $this->getEmployeeStatusTypeId() ) && $this->getEmployeeStatusTypeId() != CEmployeeStatusType::SYSTEM ) {
					$boolIsValid &= $this->valBirthDate();
					$boolIsValid &= $this->valDateStarted( $objDatabase, $strPreviousTerminationDate );
				}
				$boolIsValid &= $this->valConflictAgentPhoneExtensionId( $objDatabase );
				break;

			case 'validate_update':
				$boolIsValid &= $this->valEmailAddress( $objDatabase );
				$boolIsValid &= $this->valDnsHandle( $objDatabase );
				$boolIsValid &= $this->valWorkStationIpAddress( $objDatabase, $objDeployDatabase );
				if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {
					$boolIsValid &= $this->valPermanentEmailAddress( $objDatabase );
				}
				break;

			case 'validate_update_gmail_user_password':
				$boolIsValid &= $this->valGmailUsername( $objDatabase );
				break;

			case 'validate_physical_phone_extension_id':
				$boolIsValid &= $this->valPhysicalPhoneExtensionId();
				break;

			case 'validate_conflict_physical_phone_extension_id':
				$boolIsValid &= $this->valConflictPhysicalPhoneExtensionId( $objDatabase, $boolIsSharedPhysicalPhoneExtension );
				break;

			case 'validate_for_it_department':
				$boolIsValid &= $this->valEmailAddress( $objDatabase );
				$boolIsValid &= $this->valWorkStationIpAddressForIt( $objDatabase, $objDeployDatabase );
				$boolIsValid &= $this->valOffice( $strEmployeeCountryCode, $objDatabase );
				break;

			case 'validate_qa_for_it_department':
				$boolIsValid &= $this->valEmailAddress( $objDatabase );
				$boolIsValid &= $this->valWorkStationIpAddressForIt( $objDatabase );
				break;

			case 'validate_for_account_department':
				$boolIsValid &= $this->valTaxNumber( $objDatabase );
				$boolIsValid &= $this->valBaseSalary();
				break;

			case 'validate_employee_personal_info':
				$boolIsValid &= $this->valEmployeeStatusTypeId();
				$boolIsValid &= $this->valDepartmentId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valDnsHandle( $objDatabase );
				$boolIsValid &= $this->valWorkStationIpAddress( $objDatabase );
				break;

			case 'validate_previous_employee':
				$boolIsValid &= $this->valDateTerminated();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmployeeStatusTypeId();
				$boolIsValid &= $this->valDepartmentId();
				$boolIsValid &= $this->valEmailAddress( $objDatabase );
				if( CEmployeeStatusType::CURRENT == $this->getEmployeeStatusTypeId() ) {
					$boolIsValid &= $this->valDateStarted();
				}
				break;

			case 'validate_employee_extension_id':
				if( CCountry::CODE_INDIA != $strEmployeeCountryCode ) {
					$boolIsValid &= $this->valEmployeeExtensionId();
				}
				$boolIsValid &= $this->valConflictPhysicalPhoneExtensionId( $objDatabase, $boolIsSharedPhysicalPhoneExtension );
				$boolIsValid &= $this->valConflictAgentPhoneExtensionId( $objDatabase );
				break;

			case 'validate_desk_shifting':
				break;

			case 'validate_for_exit_process':
				$boolIsValid &= $this->valResignedOn();
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valExpectedTerminationDate();
				}
				break;

			case 'validate_employee_basic_info':
				$boolIsValid &= $this->valEmployeeStatusTypeId();
				$boolIsValid &= $this->valDepartmentId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				break;

			case VALIDATE_DELETE:
			default:
				// no validation avaliable for delete action
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function addEmployeePhoneNumber( $objEmployeePhoneNumber ) {
		$this->m_arrobjEmployeePhoneNumbers[$objEmployeePhoneNumber->getId()] = $objEmployeePhoneNumber;
	}

	public function addEmployee( $objEmployee ) {
		$this->m_arrobjEmployees[$objEmployee->getId()] = $objEmployee;
	}

	public function addEmployeeAddress( $objEmployeeAddress ) {
		$this->m_arrobjEmployeeAddresses[$objEmployeeAddress->getId()] = $objEmployeeAddress;
	}

	public function addEmployeeVacationSchedule( $objEmployeeVacationSchedule ) {
		if( false == is_null( $objEmployeeVacationSchedule->getEmployeeId() ) ) {
			$this->m_arrobjEmployeeVacationSchedules[$objEmployeeVacationSchedule->getEmployeeId()] = $objEmployeeVacationSchedule;
		} else {
			$this->m_arrobjEmployeeVacationSchedules[] = $objEmployeeVacationSchedule;
		}
	}

	public function addEmployeeHour( $arrobjEmployeeHours ) {
		$this->m_arrobjEmployeeHours[] = $arrobjEmployeeHours;
	}

	public function emailPassword( $strPassword ) {
		$objMimeEmail = new CHtmlMimeMail();

		$objMimeEmail->setFrom( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objMimeEmail->setHtml( 'Your password is ' . $strPassword );
		$objMimeEmail->setSubject( 'Administrative Login' );

		return $objMimeEmail->send( $this->m_strEmailAddress );
	}

	public function addEmployeeVacationRequest( $objEmployeeVacationRequest ) {
		if( false == is_null( $objEmployeeVacationRequest->getEmployeeId() ) ) {
			$this->m_arrobjEmployeeVacationRequests[$objEmployeeVacationRequest->getEmployeeId()] = $objEmployeeVacationRequest;
		} else {
			$this->m_arrobjEmployeeVacationRequests[] = $objEmployeeVacationRequest;
		}
	}

	public function createAndSendClientProgressSummary( $objAdminDatabase, $arrobjClientDatabases ) {

		require_once( PATH_LIBRARIES_PSI . 'ExcelGenerator/CWriteexcelWorkbookBig.php' );
		require_once( PATH_LIBRARIES_PSI . 'ExcelGenerator/CWriteexcelWorksheet.php' );

		$boolIsValid = false;

		if( true == valArr( $arrobjClientDatabases ) ) {

			$arrstrMonthHeaders = [];

			$intTimestamp = strtotime( '-1 month', strtotime( date( 'm' ) . '/1/' . date( 'Y' ) ) );

			while( 6 > \Psi\Libraries\UtilFunctions\count( $arrstrMonthHeaders ) ) {
				$arrstrMonthHeaders[$intTimestamp] = date( 'M y', $intTimestamp );
				$intTimestamp                      = strtotime( '- 1 month', $intTimestamp );
			}

			ksort( $arrstrMonthHeaders );

			// $boolIsValid = false;

			// GENERATE EXCEL FILE

			$this->m_strAttachmentFileName = PATH_NON_BACKUP_MOUNTS_GLOBAL_EMPLOYEE_STATUS_UPDATES . date( 'Y-m-d' ) . '-' . preg_replace( '/[^a-zA-Z0-9]/', '', ( $this->getNameFirst() . '_' . $this->getNameLast() ) ) . '.xls';

			$objWorkBook = new CWriteexcelWorkbookBig( $this->m_strAttachmentFileName );

			$objWorkSheet =& $objWorkBook->addworksheet( 'Panes 1' );

			// Frozen panes
			$objWorkSheet->freeze_panes( 2, 0 ); // 1 row

			$objGreenHeader =& $objWorkBook->addformat();
			$objGreenHeader->set_color( 'white' );
			$objGreenHeader->set_align( 'center' );
			$objGreenHeader->set_align( 'vcenter' );
			$objGreenHeader->set_pattern();
			$objGreenHeader->set_fg_color( 'green' );

			$objBlueHeader =& $objWorkBook->addformat();
			$objBlueHeader->set_color( 'white' );
			$objBlueHeader->set_align( 'center' );
			$objBlueHeader->set_align( 'vcenter' );
			$objBlueHeader->set_pattern();
			$objBlueHeader->set_fg_color( 'Blue' );

			$objRedHeader =& $objWorkBook->addformat();
			$objRedHeader->set_color( 'white' );
			$objRedHeader->set_align( 'center' );
			$objRedHeader->set_align( 'vcenter' );
			$objRedHeader->set_pattern();
			$objRedHeader->set_fg_color( 'Red' );

			$objYellowHeader =& $objWorkBook->addformat();
			$objYellowHeader->set_color( 'black' );
			$objYellowHeader->set_align( 'center' );
			$objYellowHeader->set_align( 'vcenter' );
			$objYellowHeader->set_pattern();
			$objYellowHeader->set_fg_color( 'Yellow' );

			$objBlackHeader =& $objWorkBook->addformat();
			$objBlackHeader->set_color( 'white' );
			$objBlackHeader->set_align( 'center' );
			$objBlackHeader->set_align( 'vcenter' );
			$objBlackHeader->set_pattern();
			$objBlackHeader->set_fg_color( 'Black' );

			$objSuperHeader =& $objWorkBook->addformat();
			$objSuperHeader->set_color( 'black' );
			$objSuperHeader->set_align( 'center' );
			$objSuperHeader->set_align( 'vcenter' );
			$objSuperHeader->set_pattern();
			$objSuperHeader->set_fg_color( 'White' );
			$objSuperHeader->set_size( 18 );
			$objSuperHeader->set_merge( 1 );

			$objOrangeHeader =& $objWorkBook->addformat();
			$objOrangeHeader->set_color( 'black' );
			$objOrangeHeader->set_align( 'left' );
			$objOrangeHeader->set_align( 'vcenter' );
			$objOrangeHeader->set_fg_color( 'Orange' );
			$objOrangeHeader->set_size( 16 );

			$objOrangeSmallHeader =& $objWorkBook->addformat();
			$objOrangeSmallHeader->set_color( 'black' );
			$objOrangeSmallHeader->set_align( 'left' );
			$objOrangeSmallHeader->set_align( 'vcenter' );
			$objOrangeSmallHeader->set_fg_color( 'Orange' );
			$objOrangeSmallHeader->set_num_format( '#,##0' );

			$objStandardFormat =& $objWorkBook->addformat( [
				'color' => 'black',
				'size'  => 9,
				'align' => 'left'

			] );

			$objStandardFormat->set_num_format( '#,##0' );

			$objWorkBookFormat =& $objWorkBook->addformat();
			$objWorkBookFormat->set_align( 'left' );

			// Sheet 1

			$objWorkSheet->set_column( 'A:A', 50 );
			$objWorkSheet->set_column( 'B:ZZ', 11 );

			$objWorkSheet->write_row( 'B1', array_pad( [ 'Payments' ], \Psi\Libraries\UtilFunctions\count( $arrstrMonthHeaders ), '' ), $objSuperHeader );
			$objWorkSheet->write_row( 'H1', array_pad( [ 'Applications' ], \Psi\Libraries\UtilFunctions\count( $arrstrMonthHeaders ), '' ), $objSuperHeader );
			$objWorkSheet->write_row( 'N1', array_pad( [ 'Guest Cards' ], \Psi\Libraries\UtilFunctions\count( $arrstrMonthHeaders ), '' ), $objSuperHeader );
			$objWorkSheet->write_row( 'T1', array_pad( [ 'Work Orders' ], \Psi\Libraries\UtilFunctions\count( $arrstrMonthHeaders ), '' ), $objSuperHeader );

			$objWorkSheet->write( 1, 0, 'Company/Property', $objBlackHeader );

			$intCurrentColumn = 1;

			foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {
				$objWorkSheet->write( 1, $intCurrentColumn, $strMonthHeader, $objGreenHeader );
				$intCurrentColumn++;
			}

			foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {
				$objWorkSheet->write( 1, $intCurrentColumn, $strMonthHeader, $objBlueHeader );
				$intCurrentColumn++;
			}

			foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {
				$objWorkSheet->write( 1, $intCurrentColumn, $strMonthHeader, $objRedHeader );
				$intCurrentColumn++;
			}

			foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {
				$objWorkSheet->write( 1, $intCurrentColumn, $strMonthHeader, $objYellowHeader );
				$intCurrentColumn++;
			}

			$intCurrentRow = 2;

			// CHECK FOR EACH DATABASE
			foreach( $arrobjClientDatabases as $objClientDatabase ) {

				if( NULL != $objClientDatabase->open() ) {

					$arrobjClients = CClients::fetchActiveClientsByEmployeeId( $this->getId(), $objAdminDatabase );

					if( false == valArr( $arrobjClients ) ) {
						continue;
					}

					if( true == is_null( $this->getEmailAddress() ) ) {
						continue;
					}

					$arrobjProperties = \Psi\Eos\Admin\CProperties::createService()->fetchPropertiesByCids( array_keys( $arrobjClients ), $objClientDatabase );

					CObjectModifiers::createService()->nestObjects( $arrobjProperties, $arrobjClients );

					if( false == valArr( $arrobjProperties ) ) {
						continue;
					}

					$boolIsValid = true;

					$arrmixStatisticsData          = [];
					$arrmixCompanyTotalDisplayData = [];

					// LOADING STATISTICS DATA ARRAY

					if( true == valArr( $arrobjClients ) ) {
						foreach( $arrobjClients as $objClient ) {
							$arrobjNewProperties = $objClient->getProperties();

							if( false == valArr( $arrmixStatisticsData[$objClient->getCompanyName()] ) ) {
								$arrmixStatisticsData[$objClient->getCompanyName()] = [];
							}

							if( true == valArr( $arrobjNewProperties ) ) {
								foreach( $arrobjNewProperties as $objProperty ) {
									$arrmixStatisticsData[$objClient->getCompanyName()][$objProperty->getPropertyName()] = [];
								}
							}
						}
					}

					// RETRIEVING PAYMENT DATA
					$strSql = 'SELECT
									sum(payment_amount),
									cid,
									property_id,
									DATE_TRUNC ( \'month\', payment_datetime )
								FROM
									ar_payments
								WHERE
									cid IN ( ' . implode( ',', array_keys( $arrobjClients ) ) . ' )
									AND payment_datetime >= DATE_TRUNC ( \'month\', ( NOW() - INTERVAL \'6 months\' ))
									AND payment_datetime < DATE_TRUNC ( \'month\', NOW())
								GROUP BY
									cid,
									property_id,
									DATE_TRUNC ( \'month\', payment_datetime )';

					$arrmixMerchantProcessingData = fetchData( $strSql, $objClientDatabase );

					if( true == valArr( $arrmixMerchantProcessingData ) ) {
						foreach( $arrmixMerchantProcessingData as $arrmixPaymentDetails ) {

							$objProperty = $arrobjProperties[$arrmixPaymentDetails['property_id']];
							$objClient   = $arrobjClients[$arrmixPaymentDetails['cid']];

							if( false == isset ( $objProperty ) || false == isset ( $objClient ) ) {
								continue;
							}

							$arrmixStatisticsData[$objClient->getCompanyName()][$objProperty->getPropertyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixPaymentDetails['date_trunc'], 0, 10 ) )]['payments'] = $arrmixPaymentDetails['sum'];

							$arrmixCompanyTotalDisplayData[$objClient->getCompanyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixPaymentDetails['date_trunc'], 0, 10 ) )]['payments'] = ( float ) $arrmixCompanyTotalDisplayData[$objClient->getCompanyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixPaymentDetails['date_trunc'], 0, 10 ) )]['payments'] + $arrmixPaymentDetails['sum'];

							ksort( $arrmixStatisticsData[$objClient->getCompanyName()] );
							ksort( $arrmixStatisticsData[$objClient->getCompanyName()][$objProperty->getPropertyName()] );
						}
					}

					// RETRIEVING APPLICATION DATA
					$strSql = 'SELECT
									count( id ),
									cid,
									property_id,
									DATE_TRUNC ( \'month\', created_on )
								FROM
									cached_applications
								WHERE
									cid IN ( ' . implode( ',', array_keys( $arrobjClients ) ) . ' )
									AND created_on >= DATE_TRUNC ( \'month\', ( NOW() - INTERVAL \'6 months\' ))
									AND created_on < DATE_TRUNC ( \'month\', NOW())
									AND lease_interval_type_id = 1
									AND application_stage_id = ' . CApplicationStage::APPLICATION . '
									AND application_status_id = ' . CApplicationStatus::COMPLETED . '
								GROUP BY
									cid,
									property_id,
									DATE_TRUNC ( \'month\', created_on )';

					$arrmixApplicationsTotals = fetchData( $strSql, $objClientDatabase );

					if( true == valArr( $arrmixApplicationsTotals ) ) {
						foreach( $arrmixApplicationsTotals as $arrmixApplicationDetails ) {

							$objProperty = $arrobjProperties[$arrmixApplicationDetails['property_id']];
							$objClient   = $arrobjClients[$arrmixApplicationDetails['cid']];

							if( false == isset ( $objProperty ) || false == isset ( $objClient ) ) {
								continue;
							}

							$arrmixStatisticsData[$objClient->getCompanyName()][$objProperty->getPropertyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixApplicationDetails['date_trunc'], 0, 10 ) )]['applications'] = $arrmixApplicationDetails['count'];

							$arrmixCompanyTotalDisplayData[$objClient->getCompanyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixApplicationDetails['date_trunc'], 0, 10 ) )]['applications'] = ( float ) $arrmixCompanyTotalDisplayData[$objClient->getCompanyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixApplicationDetails['date_trunc'], 0, 10 ) )]['applications'] + $arrmixApplicationDetails['count'];

							ksort( $arrmixStatisticsData[$objClient->getCompanyName()] );
							ksort( $arrmixStatisticsData[$objClient->getCompanyName()][$objProperty->getPropertyName()] );
						}
					}

					// RETRIEVING GUEST CARD DATA
					$strSql = 'SELECT
									count( id ),
									cid,
									property_id,
									DATE_TRUNC ( \'month\', created_on )
								FROM
									cached_applications
								WHERE
									cid IN ( ' . implode( ',', array_keys( $arrobjClients ) ) . ' )
									AND created_on >= DATE_TRUNC ( \'month\', ( NOW() - INTERVAL \'6 months\' ))
									AND created_on < DATE_TRUNC ( \'month\', NOW())
									AND lease_interval_type_id = 2
								GROUP BY
									cid,
									property_id,
									DATE_TRUNC ( \'month\', created_on )';

					$arrmixGuestCardsTotals = fetchData( $strSql, $objClientDatabase );

					if( true == valArr( $arrmixGuestCardsTotals ) ) {
						foreach( $arrmixGuestCardsTotals as $arrmixGuestCardDetails ) {

							$objProperty = $arrobjProperties[$arrmixGuestCardDetails['property_id']];
							$objClient   = $arrobjClients[$arrmixGuestCardDetails['cid']];

							if( false == isset ( $objProperty ) || false == isset ( $objClient ) ) {
								continue;
							}

							$arrmixStatisticsData[$objClient->getCompanyName()][$objProperty->getPropertyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixGuestCardDetails['date_trunc'], 0, 10 ) )]['guest_cards'] = $arrmixGuestCardDetails['count'];

							$arrmixCompanyTotalDisplayData[$objClient->getCompanyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixGuestCardDetails['date_trunc'], 0, 10 ) )]['guest_cards'] = ( float ) $arrmixCompanyTotalDisplayData[$objClient->getCompanyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixGuestCardDetails['date_trunc'], 0, 10 ) )]['guest_cards'] + $arrmixGuestCardDetails['count'];

							ksort( $arrmixStatisticsData[$objClient->getCompanyName()] );
							ksort( $arrmixStatisticsData[$objClient->getCompanyName()][$objProperty->getPropertyName()] );
						}
					}

					// RETRIEVING MAINTENANCE REQUEST DATA
					$strSql = 'SELECT
									count( id ),
									cid,
									property_id,
									DATE_TRUNC ( \'month\', created_on )
								FROM
									maintenance_requests
								WHERE
									cid IN ( ' . implode( ',', array_keys( $arrobjClients ) ) . ' )
									AND created_on >= DATE_TRUNC ( \'month\', ( NOW() - INTERVAL \'6 months\' ))
									AND created_on < DATE_TRUNC ( \'month\', NOW())
								GROUP BY
									cid,
									property_id,
									DATE_TRUNC ( \'month\', created_on )';

					$arrmixMaintenanceRequestsTotals = fetchData( $strSql, $objClientDatabase );

					if( true == valArr( $arrmixMaintenanceRequestsTotals ) ) {
						foreach( $arrmixMaintenanceRequestsTotals as $arrmixMaintenanceRequestDetails ) {

							$objProperty = $arrobjProperties[$arrmixMaintenanceRequestDetails['property_id']];
							$objClient   = $arrobjClients[$arrmixMaintenanceRequestDetails['cid']];

							if( false == isset ( $objProperty ) || false == isset ( $objClient ) ) {
								continue;
							}

							$arrmixStatisticsData[$objClient->getCompanyName()][$objProperty->getPropertyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixMaintenanceRequestDetails['date_trunc'], 0, 10 ) )]['maintenance_requests'] = $arrmixMaintenanceRequestDetails['count'];

							$arrmixCompanyTotalDisplayData[$objClient->getCompanyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixMaintenanceRequestDetails['date_trunc'], 0, 10 ) )]['maintenance_requests'] = ( float ) $arrmixCompanyTotalDisplayData[$objClient->getCompanyName()][strtotime( \Psi\CStringService::singleton()->substr( $arrmixMaintenanceRequestDetails['date_trunc'], 0, 10 ) )]['maintenance_requests'] + $arrmixMaintenanceRequestDetails['count'];

							ksort( $arrmixStatisticsData[$objClient->getCompanyName()] );
							ksort( $arrmixStatisticsData[$objClient->getCompanyName()][$objProperty->getPropertyName()] );
						}
					}

					ksort( $arrmixStatisticsData );

					if( true == valArr( $arrmixStatisticsData ) ) {

						$intCurrentColumn = 1;

						foreach( $arrmixStatisticsData as $strCompanyName => $arrmixCompanyDetails ) {

							$objWorkSheet->write( $intCurrentRow, 0, \Psi\CStringService::singleton()->substr( $strCompanyName, 0, 25 ), $objOrangeHeader );

							foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {

								if( true == isset( $arrmixCompanyTotalDisplayData[$strCompanyName][$strMonthHeaderKey]['payments'] ) ) {
									$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, __( '{%f,0,p:1}', [ $arrmixCompanyTotalDisplayData[$strCompanyName][$strMonthHeaderKey]['payments'] ] ), $objOrangeSmallHeader );
								} else {
									$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, '-', $objOrangeSmallHeader );
								}

								$intCurrentColumn++;
							}

							foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {

								if( true == isset( $arrmixCompanyTotalDisplayData[$strCompanyName][$strMonthHeaderKey]['applications'] ) ) {
									$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, $arrmixCompanyTotalDisplayData[$strCompanyName][$strMonthHeaderKey]['applications'], $objOrangeSmallHeader );
								} else {
									$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, '-', $objOrangeSmallHeader );
								}

								$intCurrentColumn++;
							}

							foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {

								if( true == isset( $arrmixCompanyTotalDisplayData[$strCompanyName][$strMonthHeaderKey]['guest_cards'] ) ) {
									$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, $arrmixCompanyTotalDisplayData[$strCompanyName][$strMonthHeaderKey]['guest_cards'], $objOrangeSmallHeader );
								} else {
									$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, '-', $objOrangeSmallHeader );
								}

								$intCurrentColumn++;
							}

							foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {

								if( true == isset( $arrmixCompanyTotalDisplayData[$strCompanyName][$strMonthHeaderKey]['maintenance_requests'] ) ) {
									$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, $arrmixCompanyTotalDisplayData[$strCompanyName][$strMonthHeaderKey]['maintenance_requests'], $objOrangeSmallHeader );
								} else {
									$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, '-', $objOrangeSmallHeader );
								}

								$intCurrentColumn++;
							}

							$intCurrentRow++;
							$intCurrentColumn = 1;

							foreach( $arrmixCompanyDetails as $strPropertyName => $arrmixPropertyDetails ) {

								$objWorkSheet->write( $intCurrentRow, 0, $strPropertyName, $objStandardFormat );

								foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {

									if( true == isset ( $arrmixPropertyDetails[$strMonthHeaderKey]['payments'] ) ) {
										$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, __( '{%f,0,p:1}', [ $arrmixPropertyDetails[$strMonthHeaderKey]['payments'] ] ), $objStandardFormat );
									} else {
										$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, '-', $objStandardFormat );
									}

									$intCurrentColumn++;
								}

								foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {

									if( true == isset ( $arrmixPropertyDetails[$strMonthHeaderKey]['applications'] ) ) {
										$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, $arrmixPropertyDetails[$strMonthHeaderKey]['applications'], $objStandardFormat );
									} else {
										$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, '-', $objStandardFormat );
									}

									$intCurrentColumn++;
								}

								foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {

									if( true == isset( $arrmixPropertyDetails[$strMonthHeaderKey]['guest_cards'] ) ) {
										$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, $arrmixPropertyDetails[$strMonthHeaderKey]['guest_cards'], $objStandardFormat );
									} else {
										$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, '-', $objStandardFormat );
									}

									$intCurrentColumn++;
								}

								foreach( $arrstrMonthHeaders as $strMonthHeaderKey => $strMonthHeader ) {

									if( true == isset( $arrmixPropertyDetails[$strMonthHeaderKey]['maintenance_requests'] ) ) {
										$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, $arrmixPropertyDetails[$strMonthHeaderKey]['maintenance_requests'], $objStandardFormat );
									} else {
										$objWorkSheet->write( $intCurrentRow, $intCurrentColumn, '-', $objStandardFormat );
									}

									$intCurrentColumn++;
								}

								$intCurrentRow++;
								$intCurrentColumn = 1;
							}
						}

						$intCurrentRow++;

					}

					// Creating Email
					if( true == file_exists( $this->m_strAttachmentFileName ) && false == is_null( file_get_contents( $this->m_strAttachmentFileName ) ) ) {

						$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionById( CFileExtension::APPLICATION_XLS, $objClientDatabase );

						if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
							$strContentType = $objFileExtension->getMimeType();
						}
					}

					$objClientDatabase->close();
				}
			}

			if( true == $boolIsValid ) {

				$objWorkBook->close();

				// Creating Email

				require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

				$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

				$objSmarty->assign( 'employee', $this );
				$objSmarty->assign( 'server_name', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN );
				$objSmarty->assign( 'SALES_EMAIL_ADDRESS', CSystemEmail::PROPERTYSOLUTIONS_SALES_EMAIL_ADDRESS );
				$objSmarty->assign( 'image_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

				$strHtmlRequestContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/scheduled_updates/simple_employee_update.tpl' );

				$objPsHtmlMimeMail = new CPsHtmlMimeMail();
				$objPsHtmlMimeMail->setFrom( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
				$objPsHtmlMimeMail->setSubject( 'Client Progress Update' );
				$objPsHtmlMimeMail->setHtml( $strHtmlRequestContent );
				$objPsHtmlMimeMail->setHeader( 'Reply-To', CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
				$objPsHtmlMimeMail->setHeader( 'X-PS-Header-Version', '1.0' );

				if( true == file_exists( $this->m_strAttachmentFileName ) && false == is_null( file_get_contents( $this->m_strAttachmentFileName ) ) ) {

					$objAttachment = new CFileAttachment( $this->m_strAttachmentFileName, $strContentType );
					$objPsHtmlMimeMail->addAttachment( $objAttachment );
				}

				$boolIsValid = $objPsHtmlMimeMail->send( [ $this->getEmailAddress() ] );

				return $boolIsValid;
			}

		}

		return $boolIsValid;
	}

	public function encryptGmailPassword() {
		return $this->setGmailPasswordEncrypted( CEncryption::encryptText( trim( $this->getGmailPasswordEncrypted() ), CONFIG_EMPLOYEE_GMAIL_AUTHENTICATION ) );
	}

	public function acceptNewGmailPassword() {
		$this->encryptGmailPassword();
	}

	public function isManagingUsers( $objDatabase ) {

		$intCountTeamEmployees = CTeamEmployees::fetchTeamEmployeeCountByEmployeeId( $this->getId(), $objDatabase );

		if( true == is_numeric( $intCountTeamEmployees ) && 0 < $intCountTeamEmployees ) {
			return $intCountTeamEmployees;
		} else {
			return 0;
		}
	}

	public function fetchSalesCloserReportCardStats( $objDatabase ) {

		$objPsLeadFilter = new CPsLeadFilter();
		$objPsLeadFilter->setSortBy( 'booked_acv' );
		$objPsLeadFilter->setSortDirection( 'DESC' );
		$objPsLeadFilter->setCloseDate( date( 'm/01/Y' ) );
		$objPsLeadFilter->setDisplayType( 'year' );
		$objPsLeadFilter->setSalesEmployees( $this->getId() );
		$objPsLeadFilter->calclulateActionRanges( $objPsLeadFilter->getDisplayType() );

		$arrstrYearData = CStatsSalesCloserEmployees::createService()->fetchQuotaVsAcvStackRankByPsLeadFilter( $objPsLeadFilter, $objDatabase );

		$objPsLeadFilter->setDisplayType( 'quarter' );
		$objPsLeadFilter->calclulateActionRanges( $objPsLeadFilter->getDisplayType() );

		$arrstrQuarterData = CStatsSalesCloserEmployees::createService()->fetchQuotaVsAcvStackRankByPsLeadFilter( $objPsLeadFilter, $objDatabase );

		$objPsLeadFilter->setDisplayType( 'month' );
		$objPsLeadFilter->calclulateActionRanges( $objPsLeadFilter->getDisplayType() );

		$arrstrMonthData = CStatsSalesCloserEmployees::createService()->fetchQuotaVsAcvStackRankByPsLeadFilter( $objPsLeadFilter, $objDatabase );

		$arrmixDataFields = [];

		$arrmixDataFields['yearly_quota_attainment']['value'] = ( 0 < $arrstrYearData[0]['quota_attainment_percent'] ) ? webMoneyFormat( $arrstrYearData[0]['quota_attainment_percent'], 0, false ) . '% of $' . __( '{%f, 0, p:1}', [ ( $arrstrYearData[0]['annual_acv_quota'] / 1000000 ) ] ) . 'MM Attained' : 'No Quota Entered';
		$arrmixDataFields['yearly_quota_attainment']['grade'] = $arrstrYearData[0]['quota_attainment_rank'];

		$arrmixDataFields['quarterly_quota_attainment']['value'] = ( 0 < $arrstrQuarterData[0]['quota_attainment_percent'] ) ? webMoneyFormat( $arrstrQuarterData[0]['quota_attainment_percent'], 0, false ) . '% of $' . __( '{%f, 0, p:0}', [ ( $arrstrQuarterData[0]['quarterly_acv_quota'] / 1000 ) ] ) . 'K Attained' : 'No Quota Entered';
		$arrmixDataFields['quarterly_quota_attainment']['grade'] = $arrstrQuarterData[0]['quota_attainment_rank'];

		$arrmixDataFields['commit_vs_actual']['value'] = ( 0 < $arrstrQuarterData[0]['total_committed_acv'] ) ? webMoneyFormat( $arrstrQuarterData[0]['commit_vs_actual_percent'], 0, false ) . '% of $' . __( '{%f, 0, p:0}', [ ( $arrstrQuarterData[0]['total_committed_acv'] / 1000 ) ] ) . 'K Committed' : 'No Commit Entered';
		$arrmixDataFields['commit_vs_actual']['grade'] = $arrstrQuarterData[0]['commit_vs_actual_grade'];

		$arrmixDataFields['commissions_paid']['value'] = webMoneyFormat( ( $arrstrQuarterData[0]['commissions_paid'] / 1000 ), 0 ) . 'K This Quarter';
		$arrmixDataFields['commissions_paid']['grade'] = $arrstrQuarterData[0]['commissions_paid_rank'];

		$arrmixDataFields['entrata_units']['value'] = webMoneyFormat( $arrstrYearData[0]['new_entrata_units'], 0, false ) . ' This Year';
		$arrmixDataFields['entrata_units']['grade'] = $arrstrYearData[0]['new_entrata_units_rank'];

		$arrmixDataFields['demos_completed']['value'] = $arrstrQuarterData[0]['demos_completed'] . ' This Quarter';
		$arrmixDataFields['demos_completed']['grade'] = $arrstrQuarterData[0]['demos_completed_rank'];

		$arrmixDataFields['meetings']['value'] = webMoneyFormat( $arrstrQuarterData[0]['meetings'], 0, false ) . ' This Quarter';
		$arrmixDataFields['meetings']['grade'] = $arrstrQuarterData[0]['meetings_rank'];

		$arrmixDataFields['profile_completion_percent']['value'] = $arrstrMonthData[0]['profile_completion_percent'] . '% Complete';
		$arrmixDataFields['profile_completion_percent']['grade'] = $arrstrMonthData[0]['profile_completion_percent_rank'];

		$arrmixDataFields['monthly_quota_attainment']['value'] = ( 0 < $arrstrMonthData[0]['quota_attainment_percent'] ) ? webMoneyFormat( $arrstrMonthData[0]['quota_attainment_percent'], 0, false ) . '% of $' . __( '{%f, 0, p:0}', [ ( $arrstrMonthData[0]['quarterly_acv_quota'] / 1000 ) ] ) . 'K Attained' : 'No Quota Entered';
		$arrmixDataFields['monthly_quota_attainment']['grade'] = $arrstrMonthData[0]['quota_attainment_rank'];

		$arrmixDataFields['monthly_commit_vs_actual']['value'] = ( 0 < $arrstrMonthData[0]['total_committed_acv'] ) ? webMoneyFormat( $arrstrMonthData[0]['commit_vs_actual_percent'], 0, false ) . '% of $' . __( '{%f, 0, p:0}', [ ( $arrstrMonthData[0]['total_committed_acv'] / 1000 ) ] ) . 'K Committed' : 'No Commit Entered';
		$arrmixDataFields['monthly_commit_vs_actual']['grade'] = $arrstrMonthData[0]['commit_vs_actual_grade'];

		$arrmixDataFields['monthly_commissions_paid']['value'] = webMoneyFormat( ( $arrstrMonthData[0]['commissions_paid'] / 1000 ), 0 ) . 'K This Month';
		$arrmixDataFields['monthly_commissions_paid']['grade'] = $arrstrMonthData[0]['commissions_paid_rank'];

		$arrmixDataFields['monthly_entrata_units']['value'] = webMoneyFormat( $arrstrMonthData[0]['new_entrata_units'], 0, false ) . ' This Month';
		$arrmixDataFields['monthly_entrata_units']['grade'] = $arrstrMonthData[0]['new_entrata_units_rank'];

		$arrmixDataFields['monthly_demos_completed']['value'] = $arrstrMonthData[0]['demos_completed'] . ' This Month';
		$arrmixDataFields['monthly_demos_completed']['grade'] = $arrstrMonthData[0]['demos_completed_rank'];

		$arrmixDataFields['monthly_meetings']['value'] = webMoneyFormat( $arrstrMonthData[0]['meetings'], 0, false ) . ' This Month';
		$arrmixDataFields['monthly_meetings']['grade'] = $arrstrMonthData[0]['meetings_rank'];

		return $arrmixDataFields;
	}

	public function fetchSupportReportCardStats( $objDatabase ) {

		$objStdFilter             = new stdClass();
		$objStdFilter->sort_by    = NULL;
		$objStdFilter->employees  = $this->getId();
		$objStdFilter->is_manager = NULL;

		$arrstrWeeklyData = CStatsSupportEmployees::fetchSupportEmployeeStackRank( $objStdFilter, $objDatabase );

		$strAvg = ( true == $this->isManagingUsers( $objDatabase ) ) ? 'Avg ' : '';

		$arrmixDataFields = [];

		$arrmixDataFields['non_escalated_closed_total']['title'] = $strAvg . 'Tickets Closed';
		$arrmixDataFields['non_escalated_closed_total']['value'] = $strAvg . ( int ) $arrstrWeeklyData[0]['non_escalated_closed_total'] . ' Tickets Closed';
		$arrmixDataFields['non_escalated_closed_total']['grade'] = $arrstrWeeklyData[0]['non_escalated_closed_total_grade'];

		$arrmixDataFields['average_nps_score']['title'] = 'Average Ticket Rating';
		$arrmixDataFields['average_nps_score']['value'] = 'Avg ' . __( '{%f, 0, p:1}', [ $arrstrWeeklyData[0]['average_nps_score'] ] ) . ' Rating - ' . $arrstrWeeklyData[0]['nps_count'] . ' Ratings';
		$arrmixDataFields['average_nps_score']['grade'] = $arrstrWeeklyData[0]['average_nps_score_grade'];

		$arrmixDataFields['avg_days_to_close']['title'] = 'Average Days to Close';
		$arrmixDataFields['avg_days_to_close']['value'] = __( '{%f, 0, p:2}', [ $arrstrWeeklyData[0]['avg_days_to_close'] ] ) . ' Days';
		$arrmixDataFields['avg_days_to_close']['grade'] = $arrstrWeeklyData[0]['avg_days_to_close_grade'];

		$arrmixDataFields['answered_calls']['title'] = $strAvg . 'Total Calls Taken';
		$arrmixDataFields['answered_calls']['value'] = $strAvg . $arrstrWeeklyData[0]['answered_calls'] . ' Calls';
		$arrmixDataFields['answered_calls']['grade'] = $arrstrWeeklyData[0]['answered_calls_grade'];

		$arrmixDataFields['phone_avg_available_mins']['title'] = $strAvg . 'Average Available Phone Minutes';
		$arrmixDataFields['phone_avg_available_mins']['value'] = $strAvg . $arrstrWeeklyData[0]['phone_avg_available_mins'] . ' Minutes';
		$arrmixDataFields['phone_avg_available_mins']['grade'] = $arrstrWeeklyData[0]['phone_avg_available_mins_grade'];

		return $arrmixDataFields;
	}

	public function fetchCsmReportCardStats( $objDatabase ) {

		$objStdFilter             = new stdClass();
		$objStdFilter->sort_by    = NULL;
		$objStdFilter->employees  = $this->getId();
		$objStdFilter->is_manager = NULL;

		$arrstrMonthlyData = CStatsCsmEmployees::createService()->fetchCsmEmployeeStackRank( $objStdFilter, $objDatabase );

		$strAvg = ( 1 == $arrstrMonthlyData[0]['is_manager'] ) ? 'Avg ' : '';

		$arrmixDataFields = [];

		$arrmixDataFields['is_manager'] = $arrstrMonthlyData[0]['is_manager'];

		$arrmixDataFields['number_of_client_success_reviews_performed']['title'] = 'Client Success Reviews';
		$arrmixDataFields['number_of_client_success_reviews_performed']['value'] = $strAvg . $arrstrMonthlyData[0]['number_of_client_success_reviews_performed'] . ' CSRs';
		$arrmixDataFields['number_of_client_success_reviews_performed']['grade'] = $arrstrMonthlyData[0]['number_of_client_success_reviews_performed_grade'];

		$arrmixDataFields['number_of_demos_performed']['title'] = 'Demos Performed';
		$arrmixDataFields['number_of_demos_performed']['value'] = $strAvg . $arrstrMonthlyData[0]['number_of_demos_performed'] . ' Demos Performed';
		$arrmixDataFields['number_of_demos_performed']['grade'] = $arrstrMonthlyData[0]['number_of_demos_performed_grade'];

		$arrmixDataFields['upsell_revenue']['title'] = 'Upsell Revenue';
		$arrmixDataFields['upsell_revenue']['value'] = $strAvg . webMoneyFormat( $arrstrMonthlyData[0]['upsell_revenue'] ) . '';
		$arrmixDataFields['upsell_revenue']['grade'] = $arrstrMonthlyData[0]['upsell_revenue_grade'];

		$arrmixDataFields['number_of_delts']['title'] = 'DELTs';
		$arrmixDataFields['number_of_delts']['value'] = $strAvg . $arrstrMonthlyData[0]['number_of_delts'] . ' DELTs';
		$arrmixDataFields['number_of_delts']['grade'] = $arrstrMonthlyData[0]['number_of_delts_grade'];

		$arrmixDataFields['number_of_total_touches']['title'] = 'Total Touches';
		$arrmixDataFields['number_of_total_touches']['value'] = $strAvg . $arrstrMonthlyData[0]['number_of_total_touches'] . ' Touches';
		$arrmixDataFields['number_of_total_touches']['grade'] = $arrstrMonthlyData[0]['number_of_total_touches_grade'];

		$arrmixDataFields['net_promoter_score']['title'] = 'NPS';
		$arrmixDataFields['net_promoter_score']['value'] = $strAvg . $arrstrMonthlyData[0]['net_promoter_score'];
		$arrmixDataFields['net_promoter_score']['grade'] = $arrstrMonthlyData[0]['net_promoter_score_grade'];

		$arrmixDataFields['number_of_adoption_projects']['title'] = 'Adoption Projects';
		$arrmixDataFields['number_of_adoption_projects']['value'] = $strAvg . $arrstrMonthlyData[0]['number_of_adoption_projects'] . '';
		$arrmixDataFields['number_of_adoption_projects']['grade'] = $arrstrMonthlyData[0]['number_of_adoption_projects_grade'];

		$arrmixDataFields['average_profile_percent_completion']['title'] = 'Profile Completion';
		$arrmixDataFields['average_profile_percent_completion']['value'] = $strAvg . __( '{%f, 0, p:2}', [ $arrstrMonthlyData[0]['average_profile_percent_completion'] ] ) . '%';
		$arrmixDataFields['average_profile_percent_completion']['grade'] = $arrstrMonthlyData[0]['average_profile_percent_completion_grade'];

		if( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrMonthlyData ) ) {

			$arrmixDataFields['number_of_client_success_reviews_performed_manager']['value'] = 'Avg ' . $arrstrMonthlyData[1]['number_of_client_success_reviews_performed'] . ' CSRs';
			$arrmixDataFields['number_of_client_success_reviews_performed_manager']['grade'] = $arrstrMonthlyData[1]['number_of_client_success_reviews_performed_grade'];

			$arrmixDataFields['number_of_demos_performed_manager']['value'] = 'Avg ' . $arrstrMonthlyData[1]['number_of_demos_performed'] . ' Demos Performed';
			$arrmixDataFields['number_of_demos_performed_manager']['grade'] = $arrstrMonthlyData[1]['number_of_demos_performed_grade'];

			$arrmixDataFields['upsell_revenue_manager']['value'] = 'Avg ' . webMoneyFormat( $arrstrMonthlyData[1]['upsell_revenue'] ) . '';
			$arrmixDataFields['upsell_revenue_manager']['grade'] = $arrstrMonthlyData[1]['upsell_revenue_grade'];

			$arrmixDataFields['number_of_delts_manager']['value'] = 'Avg ' . $arrstrMonthlyData[1]['number_of_delts'] . ' DELTs';
			$arrmixDataFields['number_of_delts_manager']['grade'] = $arrstrMonthlyData[1]['number_of_delts_grade'];

			$arrmixDataFields['number_of_total_touches_manager']['value'] = 'Avg ' . $arrstrMonthlyData[1]['number_of_total_touches'] . ' Touches';
			$arrmixDataFields['number_of_total_touches_manager']['grade'] = $arrstrMonthlyData[1]['number_of_total_touches_grade'];

			$arrmixDataFields['net_promoter_score_manager']['value'] = 'Avg ' . $arrstrMonthlyData[1]['net_promoter_score'];
			$arrmixDataFields['net_promoter_score_manager']['grade'] = $arrstrMonthlyData[1]['net_promoter_score_grade'];

			$arrmixDataFields['detractor_followup_percent_manager']['value'] = 'Avg ' . $arrstrMonthlyData[1]['detractor_followup_percent'] . '%';
			$arrmixDataFields['detractor_followup_percent_manager']['grade'] = $arrstrMonthlyData[1]['detractor_followup_percent_grade'];

			$arrmixDataFields['number_of_adoption_projects_manager']['value'] = 'Avg ' . $arrstrMonthlyData[1]['number_of_adoption_projects'] . '';
			$arrmixDataFields['number_of_adoption_projects_manager']['grade'] = $arrstrMonthlyData[1]['number_of_adoption_projects_grade'];

			$arrmixDataFields['average_profile_percent_completion_manager']['value'] = 'Avg ' . __( '{%f, 0, p:2}', [ $arrstrMonthlyData[1]['average_profile_percent_completion'] ] ) . '%';
			$arrmixDataFields['average_profile_percent_completion_manager']['grade'] = $arrstrMonthlyData[1]['average_profile_percent_completion_grade'];
		}

		return $arrmixDataFields;
	}

	public function fetchDeveloperReportCardStats( $objDatabase ) {

		$objStdFilter             = new stdClass();
		$objStdFilter->sort_by    = NULL;
		$objStdFilter->employees  = $this->getId();
		$objStdFilter->is_manager = NULL;

		$arrstrWeeklyData = CStatsDeveloperEmployees::fetchDeveloperEmployeeStatsStackRank( $objStdFilter, $objDatabase );

		if( true == valArr( $arrstrWeeklyData ) ) {
			$arrmixDataFields = [];

			$arrmixDataFields['is_manager'] = $arrstrWeeklyData[0]['is_manager'];

			$arrmixDataFields['story_points_released']['value'] = $arrstrWeeklyData[0]['story_points_released'];
			$arrmixDataFields['story_points_released']['grade'] = $arrstrWeeklyData[0]['story_points_released_grade'];

			$arrmixDataFields['bug_severity']['value'] = $arrstrWeeklyData[0]['bug_severity'];
			$arrmixDataFields['bug_severity']['grade'] = $arrstrWeeklyData[0]['bug_severity_grade'];

			$arrmixDataFields['bugs_resolved']['value'] = $arrstrWeeklyData[0]['bugs_resolved'];
			$arrmixDataFields['bugs_resolved']['grade'] = $arrstrWeeklyData[0]['bugs_resolved_grade'];

			$arrmixDataFields['features_released']['value'] = $arrstrWeeklyData[0]['features_released'];
			$arrmixDataFields['features_released']['grade'] = $arrstrWeeklyData[0]['features_released_grade'];

			$arrmixDataFields['qa_rejections']['value'] = $arrstrWeeklyData[0]['qa_rejections'];
			$arrmixDataFields['qa_rejections']['grade'] = $arrstrWeeklyData[0]['qa_rejections_grade'];

			if( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrWeeklyData ) ) {

				$arrmixDataFields['story_points_released_manager']['value'] = $arrstrWeeklyData[1]['story_points_released'];
				$arrmixDataFields['story_points_released_manager']['grade'] = $arrstrWeeklyData[1]['story_points_released_grade'];

				$arrmixDataFields['bug_severity_manager']['value'] = $arrstrWeeklyData[1]['bug_severity'];
				$arrmixDataFields['bug_severity_manager']['grade'] = $arrstrWeeklyData[1]['bug_severity_grade'];

				$arrmixDataFields['bugs_resolved_manager']['value'] = $arrstrWeeklyData[1]['bugs_resolved'];
				$arrmixDataFields['bugs_resolved_manager']['grade'] = $arrstrWeeklyData[1]['bugs_resolved_grade'];

				$arrmixDataFields['features_released_manager']['value'] = $arrstrWeeklyData[1]['features_released'];
				$arrmixDataFields['features_released_manager']['grade'] = $arrstrWeeklyData[1]['features_released_grade'];

				$arrmixDataFields['qa_rejections_manager']['value'] = $arrstrWeeklyData[1]['qa_rejections'];
				$arrmixDataFields['qa_rejections_manager']['grade'] = $arrstrWeeklyData[1]['qa_rejections_grade'];

			}

			return $arrmixDataFields;
		}
	}

	public function fetchSalesEngineerReportCardStats( $objDatabase ) {

		$objStdFilter            = new stdClass();
		$objStdFilter->sort_by   = NULL;
		$objStdFilter->employees = $this->getId();

		$arrstrWeeklyData = CStatsSalesEngineerEmployees::createService()->fetchSalesEngineerEmployeeStatsStackRank( $objStdFilter, $objDatabase );

		$arrmixDataFields = [];

		$arrmixDataFields['is_manager'] = $arrstrWeeklyData[0]['is_manager'];

		$arrmixDataFields['demos']['value'] = $arrstrWeeklyData[0]['demos'];
		$arrmixDataFields['demos']['grade'] = $arrstrWeeklyData[0]['demos_grade'];

		$arrmixDataFields['units_demoed']['value'] = $arrstrWeeklyData[0]['units_demoed'];
		$arrmixDataFields['units_demoed']['grade'] = $arrstrWeeklyData[0]['units_demoed_grade'];

		$arrmixDataFields['total_opportunity_dollars']['value'] = $arrstrWeeklyData[0]['total_opportunity_dollars'];
		$arrmixDataFields['total_opportunity_dollars']['grade'] = $arrstrWeeklyData[0]['total_opportunity_dollars_grade'];

		$arrmixDataFields['deals_closed']['value'] = $arrstrWeeklyData[0]['deals_closed'];
		$arrmixDataFields['deals_closed']['grade'] = $arrstrWeeklyData[0]['deals_closed_grade'];

		$arrmixDataFields['deals_value']['value'] = $arrstrWeeklyData[0]['deals_value'];
		$arrmixDataFields['deals_value']['grade'] = $arrstrWeeklyData[0]['deals_value_grade'];

		return $arrmixDataFields;
	}

	public function fetchSdmReportCardStats( $objDatabase ) {

		$objStdFilter            = new stdClass();
		$objStdFilter->sort_by   = NULL;
		$objStdFilter->employees = $this->getId();

		$arrmixDataFields = [];

		$arrstrWeeklyData = CStatsSdmEmployees::fetchSdmEmployeeStatsStackRank( $objStdFilter, $objDatabase );

		$arrmixDataFields['story_points_released']['value'] = $arrstrWeeklyData[0]['story_points_released'];
		$arrmixDataFields['story_points_released']['grade'] = $arrstrWeeklyData[0]['story_points_released_grade'];

		$arrmixDataFields['bug_severity']['value'] = $arrstrWeeklyData[0]['bug_severity'];
		$arrmixDataFields['bug_severity']['grade'] = $arrstrWeeklyData[0]['bug_severity_grade'];

		$arrmixDataFields['bugs_resolved']['value'] = $arrstrWeeklyData[0]['bugs_resolved'];
		$arrmixDataFields['bugs_resolved']['grade'] = $arrstrWeeklyData[0]['bugs_resolved_grade'];

		$arrmixDataFields['features_released']['value'] = $arrstrWeeklyData[0]['features_released'];
		$arrmixDataFields['features_released']['grade'] = $arrstrWeeklyData[0]['features_released_grade'];

		return $arrmixDataFields;
	}

	public function fetchReportCardStats( $objDatabase ) {

		switch( $this->getDepartmentId() ) {
			case CDepartment::SALES:
			case CDepartment::SALES_FARMERS:
				return $this->fetchSalesCloserReportCardStats( $objDatabase );
				break;

			case CDepartment::TECHNICAL_SUPPORT:
				return;
				break;

			case CDepartment::SUCCESS_MANAGEMENT:
				return $this->fetchCsmReportCardStats( $objDatabase );
				break;

			case CDepartment::CREATIVE:
			case CDepartment::INFORMATION_SECURITY:
			case CDepartment::DEVELOPMENT:
			case CDepartment::DATABASE:
			case CDepartment::MOBILE_TECHNOLOGY:
				if( true == in_array( $this->getOfficeId(), [ COffice::TOWER_8, COffice::TOWER_9 ] ) ) {
					return $this->fetchDeveloperReportCardStats( $objDatabase );
				} elseif( true == in_array( $this->getDesignationId(), CDesignation::$c_arrintSdmDesignations ) ) {
					return $this->fetchSdmReportCardStats( $objDatabase );
				}
				break;

			case CDepartment::SALES_ENGINEER:
				return $this->fetchSalesEngineerReportCardStats( $objDatabase );
				break;

			default:
				// default action
				break;
		}

		return;
	}

	public function rebuildReportCardStats( $objAdminDatabase, $objLogsDatabase = NULL ) {
		switch( $this->getDepartmentId() ) {
			case CDepartment::SALES:
			case CDepartment::SALES_FARMERS:
				return CStatsSalesCloserEmployees::createService()->rebuildSalesCloserEmployeeStats( $this->getId(), $objAdminDatabase );

				return CStatsSalesMonthlyCommits::createService()->rebuildStatsSalesMonthlyCommits( [ $this->getId() ], NULL, $objAdminDatabase );
				break;

			case CDepartment::TECHNICAL_SUPPORT:
				return CStatsSupportEmployees::rebuildSupportEmployeeStats( $this->getId(), $objAdminDatabase );
				break;

			case CDepartment::SUCCESS_MANAGEMENT:
				return CStatsCsmEmployees::createService()->rebuildCsmEmployeeStats( $this->getId(), $objAdminDatabase );
				break;

			case CDepartment::CREATIVE:
			case CDepartment::INFORMATION_SECURITY:
			case CDepartment::DEVELOPMENT:
			case CDepartment::DATABASE:
			case CDepartment::MOBILE_TECHNOLOGY:
				if( true == in_array( $this->getOfficeId(), [ COffice::TOWER_8, COffice::TOWER_9 ] ) ) {
					return CStatsDeveloperEmployees::rebuildDeveloperEmployeeStats( $this->getId(), $objAdminDatabase, $objLogsDatabase );
				} elseif( true == in_array( $this->getDesignationId(), CDesignation::$c_arrintSdmDesignations ) ) {
					return CStatsSdmEmployees::rebuildSdmEmployeeStats( $this->getId(), $objAdminDatabase );
				}
				break;

			case CDepartment::SALES_ENGINEER:
				return CStatsSalesEngineerEmployees::createService()->rebuildSalesEngineerEmployeeStats( $this->getId(), $objAdminDatabase );
				break;

			default:
				// default action
				break;
		}

		return;
	}

	public function loadDashboardTabs( $objDatabase ) {

		// Get Left Tabs(task types according to department)
		$arrmixTaskTypeDepartments = ( array ) \Psi\Eos\Admin\CTaskTypeDepartments::createService()->fetchTaskTypeDepartmentsIdsWithTabNameByDepartmentId( $this->getDepartmentId(), $objDatabase );
		$arrmixTaskTypeDepartments = rekeyArray( 'tab_name', $arrmixTaskTypeDepartments );

		return [ 'LeftSubTabs' => $arrmixTaskTypeDepartments ];
	}

	public function deleteEmployeePreferenceByKey( $strKey, $objDatabase ) {
		fetchData( 'DELETE FROM employee_preferences WHERE key = \'' . $strKey . '\' AND employee_id = ' . $this->getId(), $objDatabase );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		$intEmployeeStatusTypeId = ( int ) $this->getOriginalValueByFieldName( 'employee_status_type_id' );

		if( 0 != $intEmployeeStatusTypeId && CEmployeeStatusType::CURRENT == $intEmployeeStatusTypeId && CEmployeeStatusType::PREVIOUS == $this->getEmployeeStatusTypeId() ) {
			$objScheduledSurvey = new CScheduledSurvey();
			$boolIsValid        &= $objScheduledSurvey->declineEmployeeSurveyData( $intCurrentUserId, $this->getId(), $objDatabase );
		}

		return $boolIsValid;
	}

	public function sendEmployeePermissionEmail( $arrobjGroups, $arrmixSvnRepository, $arrobjPsModuleDetails, $arrstrBccEmailAddresses, $arrobjRoles, $objEmailDatabase ) {
		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );
		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		$objSmarty->assign( 'employee_name', $this->getNameFirst() );
		$objSmarty->assign( 'groups', $arrobjGroups );
		$objSmarty->assign( 'base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'svn_repositories', $arrmixSvnRepository );
		$objSmarty->assign( 'new_ps_modules', $arrobjPsModuleDetails );
		$objSmarty->assign( 'logo_url', 'https://www.xento.com/images/xento-logo.png' );
		$objSmarty->assign( 'new_designation', $this->getDesignationName() );
		$objSmarty->assign( 'email_image_path', 'https://rcommon.entrata.com' . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );
		$objSmarty->assign( 'country_code', CCountry::CODE_INDIA );
		$objSmarty->assign( 'roles', '' );

		if( true == in_array( $this->getDesignationId(), [ CDesignation:: ASSOCIATE_TECHNICAL_PRODUCT_MANAGER, CDesignation::TECHNICAL_PROJECT_MANAGER ] ) ) {
			$objSmarty->assign( 'roles', ( true == valArr( $arrobjRoles ) ) ? array_diff_key( [ CRole::ALLOW_STORY_POINT_EDITING => 'Allow Story Point Editing', CRole::RAPID_ACCESS => 'Rapid Deployment Access', CRole::STANDARD_ACCESS => 'Standard Deployment Access' ], $arrobjRoles ) : [ 'Allow Story Point Editing', 'Rapid Deployment Access', 'Standard Deployment Access' ] );
		}

		$objSystemEmailLibrary = new CSystemEmailLibrary();
		$strHtmlContent        = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'it/users_and_groups/users/assignments/permission_details_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$objSystemEmail        = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, 'Notification On Permissions', $strHtmlContent, $this->getEmailAddress(), $intIsSelfDestruct = 0, CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS );
		if( true == valArr( $arrstrBccEmailAddresses ) ) {
			$strBccEmailAddresses = implode( ',', $arrstrBccEmailAddresses );
			$objSystemEmail->setBccEmailAddress( $strBccEmailAddresses );
		}

		$boolIsValid = true;

		switch( NULL ) {
			default:
				$objEmailDatabase->begin();
				if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
					if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
						if( false == $objSystemEmail->sendEmail( $objEmailDatabase ) ) {
							$boolIsValid = false;
							break;
						}
					}
				}
		}

		$objEmailDatabase->commit();

		return $boolIsValid;
	}

	public function updateGmailSignature( $strEmailSignature ) {
		try {
			if( false == valStr( $this->getEmailAddress() ) ) {
				return false;
			}
			$strEmailAddress = $this->getEmailAddress();
			$arrmixMatches   = [];

			$objGoogleClient = new Google_Client();
			if( \Psi\CStringService::singleton()->strpos( $strEmailAddress, '@entrata.com' ) !== false ) {
				putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . PATH_CONFIG . 'Keys/' . self::SERVICE_FILE_US );
			} else {
				putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . PATH_CONFIG . 'Keys/' . self::SERVICE_FILE_IN );
			}

			$objGoogleClient->setApplicationName( 'GmailAPI' );
			$objGoogleClient->setScopes( [ 'https://www.googleapis.com/auth/gmail.settings.basic', 'https://www.googleapis.com/auth/gmail.settings.sharing', 'https://www.googleapis.com/auth/admin.directory.user', 'https://www.googleapis.com/auth/admin.directory.group' ] );
			$objGoogleClient->useApplicationDefaultCredentials();

			if( \Psi\CStringService::singleton()->strpos( $strEmailAddress, '@entrata.com' ) !== false ) {
				$objGoogleClient->setAuthConfig( PATH_CONFIG . 'Keys/' . self::SERVICE_FILE_US );
			} else {
				$objGoogleClient->setAuthConfig( PATH_CONFIG . 'Keys/' . self::SERVICE_FILE_IN );
			}

			$objGoogleClient->setSubject( $strEmailAddress );
			$objGoogleClient->setAccessType( 'offline' );
			$objGoogleServiceGmail       = new Google_Service_Gmail( $objGoogleClient );
			$objGoogleServiceGmailSendAs = new Google_Service_Gmail_SendAs();

			$arrmixSignatures = ( array ) $objGoogleServiceGmail->users_settings_sendAs->listUsersSettingsSendAs( me )->getSendAs();
			foreach( $arrmixSignatures as $intKey => $strValue ) {
				preg_match( '/' . CONFIG_ENTRATA_SUFFIX . '/', $arrmixSignatures[$intKey]->sendAsEmail, $arrmixMatches );
				if( true == valArr( $arrmixMatches ) ) {
					$objGoogleServiceGmailSendAs->setIsDefault( true );
				}

				if( true == valStr( $strEmailSignature ) ) {
					$strEmailSignatureContent = $this->prepareSignature( $strEmailSignature );
				} else {
					if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
						$strEmailSignatureContent = $this->prepareSignature( self::GENERIC_EMAIL_SIGNATURE_IN );
					} else {
						$strEmailSignatureContent = $this->prepareSignature( self::GENERIC_EMAIL_SIGNATURE_US );
					}
				}

				$objGoogleServiceGmailSendAs->setSignature( $strEmailSignatureContent );
				if( true == valObj( $objGoogleServiceGmail->users_settings_sendAs->update( me, $arrmixSignatures[$intKey]->sendAsEmail, $objGoogleServiceGmailSendAs ), 'Google_Service_Gmail_SendAs' ) ) {
					continue;
				} else {
					show( 'Error : Failed to update signature.' );
				}
			}

		} catch( Exception $objException ) {
			throw new Exception( $objException->getMessage() );
		}

		return true;
	}

	public function prepareSignature( $strContent ) {
		$strContent = \Psi\CStringService::singleton()->str_ireplace( 'Full Name', ( true == valStr( $this->getPreferredName() ) ) ? $this->getPreferredName() : $this->getNameFull(), $strContent );
		$strContent = \Psi\CStringService::singleton()->str_ireplace( 'Designation', $this->getDesignationName(), $strContent );
		$strContent = \Psi\CStringService::singleton()->str_ireplace( 'Department', $this->getDepartmentName(), $strContent );
		$strContent = \Psi\CStringService::singleton()->str_ireplace( 'Mobile No', $this->getPhoneNumber(), $strContent );
		$strContent = \Psi\CStringService::singleton()->str_ireplace( 'Extension', $this->getPhysicalPhoneExtensionId(), $strContent );

		return $strContent;
	}

	public function updateImplementationConsultantEmployeeId( $intEmployeePreviousManagerId, $objAdminDatabase, $objUtilitiesDatabase ) {

		$objPropertyUtilitySetting = new CPropertyUtilitySetting();
		$objPropertyUtilitySetting->updateImplementationConsultantEmployeeId( $this->getId(), $intEmployeePreviousManagerId, $objAdminDatabase, $objUtilitiesDatabase );

		return true;

	}

	public function calculateProRataBasisLeavePool( $objEmployeeVacationSchedule, $intEmployeeId, $strEffectiveDate, $objDatabase, $boolDeductLeavePool = false, $boolTraineeConfirmation = false ) {
		$objEmployeePreferenceNew = NULL;
		$fltLeavesToAdd           = 0;
		$fltLeavesToDeduct        = 0;
		$fltOldTimeOffPool        = 0;

		$strEffectiveDateFormatted = date( 'Y-m-d', strtotime( $strEffectiveDate ) );
		$intEffectiveYear          = date( 'Y', strtotime( $strEffectiveDate ) );

		if( true == valObj( $objEmployeeVacationSchedule, 'CEmployeeVacationSchedule' ) ) {

			$fltOldTimeOffPool = ( $boolTraineeConfirmation == true ) ? 0 : $objEmployeeVacationSchedule->getHoursGranted() / CEmployeeVacationRequest::DAILY_EXPECTED_WORKING_HOURS;

			if( $strEffectiveDateFormatted > date( 'Y' ) . '-06-25' && $strEffectiveDateFormatted <= date( 'Y' ) . '-12-25' ) {
				$strKey = 'LEAVE_POOL_FIRST_HALF_' . $intEffectiveYear;
			} else {
				$intEffectiveKeyYear = ( date( 'm', strtotime( $strEffectiveDate ) ) == self::MONTH_DECEMBER ) ? $intEffectiveYear : $intEffectiveYear - 1;
				$strKey           = 'LEAVE_POOL_SECOND_HALF_' . $intEffectiveKeyYear;
			}

			$objEmployeePreference = \Psi\Eos\Admin\CEmployeePreferences::createService()->fetchEmployeePreferenceByEmployeeIdByKey( $intEmployeeId, $strKey, $objDatabase );
			if( false == valObj( $objEmployeePreference, 'CEmployeePreference' ) ) {
				$objEmployeePreferenceNew = new CEmployeePreference();
				$objEmployeePreferenceNew->setEmployeeId( $intEmployeeId );
				$objEmployeePreferenceNew->setKey( $strKey );
				$objEmployeePreferenceNew->setValue( $objEmployeeVacationSchedule->getHoursGranted() );
			}
		} else {
			$objEmployeeVacationSchedule = new CEmployeeVacationSchedule();
			$objEmployeeVacationSchedule->setEmployeeId( $intEmployeeId );
			$objEmployeeVacationSchedule->setYear( date( 'Y' ) );
			if( $strEffectiveDateFormatted > date( 'Y' ) . '-12-25' ) {
				$objEmployeeVacationSchedule->setYear( date( 'Y' ) + 1 );
			}
		}
		$strEndDate = $intEffectiveYear . '-06-30';
		if( $strEffectiveDate > $intEffectiveYear . '-06-25' && $strEffectiveDate <= $intEffectiveYear . '-12-25' ) {
			$strEndDate = $intEffectiveYear . '-12-31';
		} elseif( $strEffectiveDate > $intEffectiveYear . '-12-25' && $strEffectiveDate <= $intEffectiveYear . '-12-31' ) {
			$strEndDate = date( 'Y-m-d', strtotime( $intEffectiveYear . '-06-30 +1 Year' ) );
		}

		$arrmixInterval = Psi\Libraries\UtilDates\CDates::createService()->getDateDifference( $strEffectiveDate, $strEndDate );
		$intDays        = $arrmixInterval['days'];

		if( $arrmixInterval['months'] > 0 ) {
			$fltLeavesToAdd = $fltLeavesToDeduct = $arrmixInterval['months'] * 2;
		}

		if( $intDays > 0 ) {
			switch( $intDays ) {
				case $intDays >= 28:
					$fltLeavesToAdd    += 2.0;
					$fltLeavesToDeduct += 2.0;
					break;

				case $intDays >= 21:
					$fltLeavesToAdd    += 1.5;
					$fltLeavesToDeduct += 2.0;
					break;

				case $intDays >= 14:
					$fltLeavesToAdd    += 1.0;
					$fltLeavesToDeduct += 1.5;
					break;

				case $intDays >= 7:
					$fltLeavesToAdd    += 0.5;
					$fltLeavesToDeduct += 1.0;
					break;

				default:
					$fltLeavesToDeduct += 0.5;
			}
		}

		if( true == $boolDeductLeavePool ) {
			$fltOldTimeOffPool -= $fltLeavesToDeduct;
		} else {
			$fltOldTimeOffPool += $fltLeavesToAdd;
		}

		$objEmployeeVacationSchedule->setHoursGranted( $fltOldTimeOffPool * CEmployeeVacationRequest::DAILY_EXPECTED_WORKING_HOURS );

		return [ 'vacation_schedule' => $objEmployeeVacationSchedule, 'employee_preferences' => $objEmployeePreferenceNew ];
	}

    protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

        switch( $strVendor ) {
            case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
                if( true == is_numeric( $this->getId() ) ) {
                    if( 'id_card' == $strReferenceTag ) {
                        return $strPath = 'ID_card_'. $this->getEmployeeNumber() . '.pdf';
                    } elseif( 'large_image' == $strReferenceTag ) {
                        return $strPath = '/employees/photos/' . $this->getId() . '_large.jpg';
                    } else {
                        return $strPath = '/employees/photos/' . $this->getId() . '.jpg';
                    }
                }
                break;

            default:
                if( true == is_numeric( $this->getId() ) ) {
                    if( 'id_card' == $strReferenceTag ) {
                        $strPath = sprintf( '%d/%s%d/%02d/%02d/%d/', CClient::ID_DEFAULT, 'employees/id_cards/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() ) . 'ID_card_' . $this->getEmployeeNumber() . '.pdf';
                    } elseif( 'large_image' == $strReferenceTag ) {
                        $strPath = sprintf( '%d/%s%d/%02d/%02d/%d/', CClient::ID_DEFAULT, 'employees/photos/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() ) . $this->getId() . '_large.jpg';
                    } else {
                        $strPath = sprintf( '%d/%s%d/%02d/%02d/%d/', CClient::ID_DEFAULT, 'employees/photos/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() ) . $this->getId() . '.jpg';
                    }
                }

        }

        return $strPath;
    }

    protected function calcStorageContainer( $strVendor = NULL ) {

        switch( $strVendor ) {
            case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
                return 'admin_medias';
                break;

            default:
                return CONFIG_OSG_BUCKET_SYSTEM_EMPLOYEES;
        }
    }

    protected function createStoredObject() {
        return new \CStoredObject();
    }

    protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
        return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
    }

}

?>