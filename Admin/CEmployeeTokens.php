<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeTokens
 * Do not add any new functions to this class.
 */

class CEmployeeTokens extends CBaseEmployeeTokens {

	public static function fetchEmployeeTokens( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmployeeToken', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEmployeeTokenByToken( $strToken, $objDatabase ) {
		return self::fetchEmployeeToken( 'SELECT * FROM employee_tokens WHERE token = \'' . pg_escape_string( $strToken ) . '\'', $objDatabase );
	}

	public static function fetchValidEmployeeByToken( $strToken, $objDatabase ) {
		return self::fetchEmployeeToken( 'SELECT * FROM employee_tokens WHERE expired_on >= now() and token = \'' . pg_escape_string( $strToken ) . '\'', $objDatabase );
	}

}
?>