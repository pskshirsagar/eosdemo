<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTags
 * Do not add any new functions to this class.
 */

class CTags extends CBaseTags {

	public static function fetchPublishedTagsByActionResultId( $intActionResultId, $objDatabase ) {
		$strSql = 'SELECT * FROM tags WHERE is_published = 1 AND deleted_by IS NULL AND action_result_id = ' . ( int ) $intActionResultId . ' ORDER BY name';
		return self::fetchTags( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTagsByActionResultIdByIsPublished( $intActionResultId, $intIsPublished, $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset 	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit 	= ( int ) $intPageSize;

		$strSql = 'SELECT * FROM tags WHERE action_result_id = ' . ( int ) $intActionResultId . ' AND is_published = ' . ( int ) $intIsPublished . ' AND deleted_by IS NULL AND deleted_on IS NULL ORDER BY id DESC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchTags( $strSql, $objDatabase );
	}

	public static function fetchTagsCountByActionResultIdByIsPublished( $intActionResultId, $intIsPublished, $objDatabase ) {

		$strSql = 'SELECT count( id ) FROM tags WHERE action_result_id =' . ( int ) $intActionResultId . ' AND is_published =' . ( int ) $intIsPublished . ' AND deleted_by IS NULL';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchCountDuplicateTagsById( $intId, $strName, $intActionResultId, $boolIsPrivate, $intUserId, $objDatabase ) {

		$strSql = 'SELECT count( id ) FROM tags WHERE id <> ' . ( int ) $intId . ' AND lower( name ) = \'' . pg_escape_string( \Psi\CStringService::singleton()->strtolower( $strName ) ) . '\' AND action_result_id = ' . ( int ) $intActionResultId . ' AND deleted_by IS NULL AND is_published = 1';

		if( true == $boolIsPrivate ) {
			$strSql .= ' AND created_by = ' . ( int ) $intUserId;
		}

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchTagsByIds( $arrintTagIds, $objDatabase ) {

		if( false == valArr( $arrintTagIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						tags t
					WHERE
						t.id IN (' . implode( ',', $arrintTagIds ) . ')';

		return self::fetchTags( $strSql, $objDatabase );
	}

	public static function fetchAllTagsByActionResultId( $arrintActionResultIds, $strOrderByField, $objDatabase, $intShowDisabledData = 0 ) {

		if( false == valArr( $arrintActionResultIds ) ) return;

		$strSubSql = '';

		if( 0 == $intShowDisabledData ) {
			$strSubSql .= ' AND deleted_by IS NULL';
		}

		$strSql = 'SELECT * FROM tags WHERE action_result_id IN ( ' . implode( ',', $arrintActionResultIds ) . ' ) ' . $strSubSql . ' ORDER BY ' . $strOrderByField;

		return self::fetchTags( $strSql, $objDatabase );
	}

	public static function fetchTagsByTagNameByActionResultId( $strTagName, $intActionResultId, $objDatabase, $boolFromReport = false, $boolFromSuggestedReportTag = false ) {
		$strSql = '';

		if( true == empty( $strTagName ) ) {

			$strSql .= 't.name ~ \'[a-zA-Z]\'';
		} else {

			if( false == $boolFromSuggestedReportTag ) {
				$strSql .= 'lower( t.name ) LIKE lower( \'' . addslashes( $strTagName ) . '%\' )';
			} else {
				$arrstrTagNames = \Psi\CStringService::singleton()->preg_split( '/\s+/', $strTagName );
				foreach( $arrstrTagNames as $strTagNme ) {
					if( false == valStr( $strSql ) ) {
						$strSql .= ' ( lower( t.name ) LIKE lower( \'%' . addslashes( trim( $strTagNme ) ) . '%\' )';
					} else {
						$strSql .= ' OR lower( t.name ) LIKE lower( \'%' . addslashes( trim( $strTagNme ) ) . '%\' )';
					}
				}
				$strSql .= ' ) ';
			}
		}

		$strSql = 'SELECT
                  t.id, 
                  t.name,
                  t.action_result_id
               FROM
                  tags t
               WHERE ' . $strSql . ' AND t.action_result_id = ' . ( int ) $intActionResultId . ' AND t.is_published = 1 AND t.deleted_by IS NULL ORDER BY name';

		if( true == $boolFromReport ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return parent::fetchTags( $strSql, $objDatabase );
		}
	}

	public static function fetchTagByName( $strTagName, $objDatabase ) {

		if( true == empty( $strTagName ) ) return NULL;

		$strSql = 'SELECT
						t.id
					FROM
						tags t
					WHERE
						t.name ILIKE \'' . trim( addslashes( $strTagName ) ) . '\'
					GROUP BY
						t.id
					ORDER BY
						t.id
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublicAndPrivateTags( $intUserId, $intActionResultId, $objDatabase ) {

		$strSql = 'SELECT
						t.id,
						t.name
					FROM
						tags t
					WHERE
						( ( ( t.created_by = ' . ( int ) $intUserId . ' or t.owner_id = ' . ( int ) $intUserId . ' ) AND t.is_private IS TRUE ) OR t.is_private IS false )
						AND t.action_result_id = ' . ( int ) $intActionResultId . ' AND t.is_published = 1 AND t.deleted_by IS NULL AND t.deleted_on IS NULL 
					ORDER BY t.name asc';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTagsByKeywordByEmployeeIdByIsDisabled( $intPageNo, $intPageSize, $boolIsCount = false, $strKeyword, $intUserId, $intActionResultId, $objAdminDatabase, $boolPrivateTagAccess = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strOrderBy = '';

		if( true == $boolIsCount ) {
			$strSelectSql = 'count( DISTINCT tt.id)';
		} else {
			$strSelectSql = 'count ( ta.tag_id ),
						tt.id,
						tt.name,
						tt.description,
						tt.created_by,
						tt.owner_id,
						e.name_full as employee_name,
						tt.is_private,
						ar.name as action_result_name,
						tt.is_published';
			$strOrderBy = 'GROUP BY tt.id, tt.name, tt.description, tt.created_by, tt.is_private, tt.owner_id, e.name_full, ar.name ORDER BY tt.name';
		}

		$strSql = '	SELECT ' . $strSelectSql . '
					FROM
						tags tt
						JOIN action_type_results atr ON ( atr.action_result_id = tt.action_result_id AND atr.action_type_id = ' . CActionType::TAG . ' )
				 		LEFT JOIN tags_associations ta ON ( ta.tag_id = tt.id )
						LEFT JOIN users u ON ( CASE WHEN tt.owner_id IS NOT NULL THEN u.id = tt.owner_id ELSE u.id = tt.created_by END )
						LEFT JOIN employees e ON ( e.id = u.employee_id )
						LEFT JOIN action_results ar ON ( ar.id = atr.action_result_id )
					WHERE
				';

		if( false == empty( $strKeyword ) ) {
			$strSql .= ' (tt.name ILIKE \'%' . pg_escape_string( $strKeyword ) . '%\' OR tt.description ILIKE \'%' . pg_escape_string( $strKeyword ) . '%\') AND ';
		}

		if( NULL != $intUserId ) {
			$strSql .= ' ( tt.created_by = ' . ( int ) $intUserId . ' OR tt.owner_id = ' . ( int ) $intUserId . ' ) AND ';
		}

		if( NULL != $intActionResultId ) {
			$strSql .= ' tt.action_result_id = ' . ( int ) $intActionResultId . ' AND ';
		}

		if( false == $boolPrivateTagAccess ) {
			$strSql .= ' tt.is_private = false AND ';
		}

		$strSql .= ' 1=1 AND deleted_on IS NULL ' . $strOrderBy;

		if( true == $boolIsCount ) {
			return fetchData( $strSql, $objAdminDatabase );
		} else {

			$strSql .= ' OFFSET
							' . ( int ) $intOffset . '
						LIMIT
							' . ( int ) $intLimit;

			return self::fetchTags( $strSql, $objAdminDatabase );
		}
	}

	public static function fetchTagsNameByTaskIds( $arrintTaskIds, $objDatabase, $intUserId = NULL ) {
		$strWhere = '';

		if( false == valArr( $arrintTaskIds ) ) return NULL;

		if( valId( $intUserId ) ) {
			$strWhere = ' AND ( tg.is_private = false or ( tg.is_private = true AND (tg.owner_id = ' . ( int ) $intUserId . ' or tg.created_by = ' . ( int ) $intUserId . ' ) ) )';
		}
		$strSql = 'SELECT 
						array_to_string ( array_agg ( DISTINCT tg.name ), \',\' ) AS task_tag_name,
						t.id as task_id
					FROM
						tags tg
						JOIN tags_associations ta ON ( ta.tag_id = tg.id )
						JOIN tasks t ON ( t.id = ta.task_id )
					WHERE t.id IN(' . implode( ',', $arrintTaskIds ) . ') ' . $strWhere . '
						GROUP BY t.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTaskTagsByActionResultId( $intTaskTag, $intUserId, $objDatabase ) {

		$strSql = 'SELECT
						id,
						name
					FROM
						tags
					WHERE
						action_result_id =' . ( int ) $intTaskTag . '
						AND ( is_private = false or ( is_private = true AND ( created_by = ' . ( int ) $intUserId . ' or owner_id = ' . ( int ) $intUserId . ' ) ) )
						AND is_published = 1';

		return self::fetchTags( $strSql, $objDatabase );
	}

}
?>