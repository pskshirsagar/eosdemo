<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEcsMessages
 * Do not add any new functions to this class.
 */

class CEcsMessages extends CBaseEcsMessages {

	public static function fetchSearchedMessages( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ecs_messages
					WHERE
						( msg_body ILIKE\'%' . implode( '%\' AND msg_body ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR received_from ILIKE \'%' . implode( '%\' AND received_from ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
						AND deleted_on IS NULL AND deleted_by IS NULL';

		return self::fetchEcsMessages( $strSql, $objDatabase );

	}

	public static function fetchEcsMessagesById( $intMessageId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						ecs_messages
				    WHERE
						deleted_by IS NULL
						AND id = ' . ( int ) $intMessageId;

		return self:: fetchEcsMessage( $strSql, $objDatabase );
	}

	 public static function fetchEcsMessageById( $intMessageId, $objDatabase ) {

	 	$strSql = 'SELECT * FROM ecs_messages WHERE id = ' . ( int ) $intMessageId;

		return self:: fetchEcsMessage( $strSql, $objDatabase );
	 }

	 public static function fetchAllEcsMessagesCount( $objDatabase ) {

	 	return self::fetchEcsMessageCount( ' WHERE deleted_by IS NULL', $objDatabase );
	 }

	 public static function fetchAllPaginatedMessages( $intPageNo, $intPageSize, $objDatabase ) {

	 	$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

	 	$strSql = 'SELECT
	                   *
	                FROM
	                    ecs_messages
	 				WHERE
						deleted_by IS NULL
	                ORDER BY id desc';

	 	$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

	 	return self::fetchEcsMessages( $strSql, $objDatabase );
	 }

	 public static function fetchEcsMessagesByMessageStatus( $strStatus, $objDatabase ) {

	 	$strSql = 'SELECT
						*
					FROM
						ecs_messages
				    WHERE
						message_status = \'' . $strStatus . '\'
						AND deleted_by IS NULL';

	 	return self:: fetchEcsMessages( $strSql, $objDatabase );
	 }

}
?>