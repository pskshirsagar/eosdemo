<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeVacationSchedules
 * Do not add any new functions to this class.
 */

class CEmployeeVacationSchedules extends CBaseEmployeeVacationSchedules {

	public static function fetchEmployeeVacationScheduleByEmployeeIdByYear( $intEmployeeId, $intYear, $objDatabase ) {
		return self::fetchEmployeeVacationSchedule( 'SELECT * FROM employee_vacation_schedules WHERE employee_id = ' . ( int ) $intEmployeeId . ' AND year = \'' . ( int ) $intYear . '\' ORDER BY id DESC LIMIT 1', $objDatabase );
	}

	public static function fetchAllEmployeeVacationSchedulesByYear( $intYear, $objDatabase ) {
		return self::fetchEmployeeVacationSchedules( 'SELECT * FROM employee_vacation_schedules WHERE year = \'' . ( int ) $intYear . '\' ORDER BY id', $objDatabase );
	}

	public static function fetchEmployeeVacationSchedulesByYearByEmployeeIds( $intYear, $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						employee_vacation_schedules
					WHERE
						year = \'' . ( int ) $intYear . '\'
						AND employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
					ORDER BY id';

		return parent::fetchEmployeeVacationSchedules( $strSql, $objDatabase );
	}

	public static function fetchHoursGrantedByEmployeeIdByYear( $intEmployeeId, $intYear, $objDatabase ) {
		if( false == valId( $intEmployeeId ) || false == valId( $intYear ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						CAST ( hours_granted AS float ) / ' . CEmployeeVacationSchedule::PER_DAY_ON_FLOOR_HOURS . ' AS days
					FROM
						employee_vacation_schedules
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND year = ' . ( int ) $intYear;

		$arrfltOutput = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrfltOutput ) ) {
			return $arrfltOutput[0]['days'];
		}

		return NULL;
	}

}
?>