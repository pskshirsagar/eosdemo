<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSvnFiles
 * Do not add any new functions to this class.
 */

class CSvnFiles extends CBaseSvnFiles {

	public static function fetchSvnFilesByIds( $arrintIds, $objAdminDatabase, $boolIsTaskId=false ) {
		if( false == valArr( $arrintIds ) ) return NULL;
		$strSql = 'SELECT * FROM svn_files WHERE id IN ( ' . implode( ',', $arrintIds ) . ' )';

		if( true == $boolIsTaskId ) {
			$strSql .= ' AND task_id IS NULL';
		}

		$strSql .= ' ORDER BY id ASC';

		return parent::fetchSvnFiles( $strSql, $objAdminDatabase );
	}

	public static function fetchUnassociatedPublishedSvnFiles( $objAdminDatabase ) {
		$strSql = 'SELECT
						sf.*
					FROM svn_files sf
						LEFT JOIN task_svn_files tsf ON ( sf.id = tsf.svn_file_id )
					WHERE
						tsf.id IS NULL
						AND sf.is_published = 1
						AND ( position(\'RFR.\' in sf.file_name) = 1 )
						AND sf.database_type_id <> ' . CDatabaseType::CONNECT . '
						ORDER BY sf.database_type_id ASC, sf.file_name DESC';

		return parent::fetchSvnFiles( $strSql, $objAdminDatabase );
	}

	public static function fetchConflictingSvnFileCount( $objSvnFile, $objAdminDatabase ) {

		$intId = ( false == is_numeric( $objSvnFile->getId() ) ) ? 0 : $objSvnFile->getId();

		$strWhereSql = ' WHERE file_name = \'' . addslashes( $objSvnFile->getFileName() ) . '\' AND database_type_id = ' . ( int ) $objSvnFile->getDatabaseTypeId() . ' AND id <> ' . ( int ) $intId . ' LIMIT 1 ';

		return self::fetchSvnFileCount( $strWhereSql, $objAdminDatabase );
	}

	public static function getSearchCriteria( $objSvnFilesFilter ) {

		$arrstrWhereParameters = array();

		// Create SQL parameters.
		$intTaskId = $objSvnFilesFilter->getTaskId();
		( false == isset( $objSvnFilesFilter ) || true == empty( $intTaskId ) ) ?  false : array_push( $arrstrWhereParameters, 'sf.task_id=' . $objSvnFilesFilter->getTaskId() );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			return $arrstrWhereParameters;
		} else {
			return NULL;
		}
	}

    public static function fetchSvnFilesByDatabaseDeploymentTypeIdByDatabaseTypeIdsByClusterIdBySvnFilePrefix( $intDatabaseDeploymentTypeId, $arrintDatabaseTypeIds, $intClusterId, $strFilePrefix, $objDatabase, $boolIsIncludeTimeInterval = true ) {

        $strCondition = '';

        if( false == is_null( $strFilePrefix ) ) {
            $strCondition = ' AND position( \'' . $strFilePrefix . '\' in file_name) = 1 ';
        }

        $strTimeIntervalCondition = ( true == $boolIsIncludeTimeInterval ) ? ' AND created_on > now() - interval \'1 year\' ' : '';

        $strSql = 'SELECT
						*
					FROM svn_files
					WHERE
						database_type_id IN( ' . implode( ',', $arrintDatabaseTypeIds ) . ' )
						AND is_published = 1 ' . $strCondition . '
						AND deployment_type_id = ' . ( int ) $intDatabaseDeploymentTypeId . $strTimeIntervalCondition . '
						ORDER BY database_type_id ';

        return parent::fetchSvnFiles( $strSql, $objDatabase );
    }

	public static function fetchUnassociatedPublishedSvnFilesByDatabaseTypeIds( $arrintDatabaseTypeIds, $objAdminDatabase ) {
		$strSql = 'SELECT
						sf.*
					FROM svn_files sf
						LEFT JOIN task_svn_files tsf ON ( sf.id = tsf.svn_file_id )
					WHERE
						tsf.id IS NULL
						AND	sf.database_type_id IN ( ' . implode( ',', $arrintDatabaseTypeIds ) . ' )
						AND sf.is_published = 1
						AND ( position(\'RFR.\' in sf.file_name) = 1 )
						ORDER BY sf.database_type_id ASC, sf.file_name DESC';

		return parent::fetchSvnFiles( $strSql, $objAdminDatabase );
	}

	public static function fetchSvnFilesByFileNameByDatabaseTypeIdByDeploymentTypeId( $strFileName, $intDatabaseTypeId, $intDeploymentTypeId = NULL, $objDatabase ) {

		$strDeploymentTypeCondition = ( false == is_null( $intDeploymentTypeId ) ) ? ' AND deployment_type_id = ' . ( int ) $intDeploymentTypeId : '';
		$strSql = '
					SELECT
						*
					FROM
						svn_files
					WHERE
						is_published = 1
						AND file_name LIKE \'' . $strFileName . '\'
						AND database_type_id = ' . ( int ) $intDatabaseTypeId .
						$strDeploymentTypeCondition . '
						ORDER BY id
					LIMIT 1';

		return self::fetchSvnFile( $strSql, $objDatabase );
	}

	public static function fetchSvnFilesByFileName( $strFileName, $objDatabase ) {
		return self::fetchSvnFile( 'SELECT * FROM svn_files WHERE file_name LIKE \'' . $strFileName . '\' ORDER BY id DESC LIMIT 1', $objDatabase );
	}

	public static function fetchSearchSvnFilesByFileNameByLimit( $strFileName, $intLimit, $objDatabase ) {
		return self::fetchSvnFiles( 'SELECT * FROM svn_files WHERE file_name ILIKE \'%' . trim( $strFileName ) . '%\' LIMIT ' . ( int ) $intLimit, $objDatabase );
	}

	public static function fetchSvnFilesByFileNameByDatabaseTypeIdByDeploymentTypeIdByIsPublished( $strFileName, $intDatabaseTypeId, $intDeploymentTypeId, $intIsPublished, $objDatabase ) {

		$strSql = '
					SELECT
						*
					FROM
						svn_files
					WHERE
						is_published = ' . ( int ) $intIsPublished . '
						AND file_name LIKE \'' . $strFileName . '\'
						AND database_type_id = ' . ( int ) $intDatabaseTypeId . '
						AND deployment_type_id = ' . ( int ) $intDeploymentTypeId . '
						ORDER BY id
					LIMIT 1';

		return self::fetchSvnFile( $strSql, $objDatabase );
	}

	public static function fetchAllSvnFiles( $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    svn_files sf
					WHERE
						sf.is_published = 1
					ORDER BY id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTaskDetailsBySvnFileIds( $arrintSvnFileIds, $objAdminDatabase, $intTaskTypeId = NULL ) {

		if( false == valArr( $arrintSvnFileIds ) ) return NULL;

		$strTaskTypeCondition = ( false == is_null( $intTaskTypeId ) ) ? ' AND task_type_id = ' . ( int ) $intTaskTypeId : '';

		$strSql = 'SELECT
					s.id,
					t.id task_id,
					t.task_type_id
				FROM
					svn_files s
				LEFT JOIN tasks t ON ( t.id = s.task_id )
				WHERE s.id IN ( ' . implode( ',', $arrintSvnFileIds ) . ' )' . $strTaskTypeCondition;

		return fetchData( $strSql, $objAdminDatabase );
	}

}
?>