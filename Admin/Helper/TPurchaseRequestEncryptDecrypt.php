<?php
use Psi\Libraries\Cryptography\CCrypto;

trait TPurchaseRequestEncryptDecrypt {

	// returns double encrypted data if the $strOldEncryptionKey is not NULL else returns single encrypted amount.

	public function getEncryptedAmount( $strAmount, $strOldEncryptionKey = NULL ) {

		if( false == valStr( $strAmount ) ) {
			return $strAmount;
		}

		if( true == valStr( $strOldEncryptionKey ) ) {
			return CCrypto::createService()->encrypt( $strAmount, CONFIG_SODIUM_KEY_PURCHASE_REQUEST_DOUBLE_ENCRYPTION );
		} else {
			return CCrypto::createService()->encrypt( $strAmount, CONFIG_SODIUM_KEY_PURCHASE_REQUEST_BASE_AMOUNT );
		}
	}

	// returns double decrypted data if the $strOldEncryptionKey is not NULL else returns single decrypted amount.

	public function getDecryptedAmount( $strAmount, $strOldEncryptionKey = NULL ) {

		if( false == valStr( $strAmount ) ) {
			return $strAmount;
		}

		if( true == valStr( $strOldEncryptionKey ) ) {
			return CCrypto::createService()->decrypt( $strAmount, CONFIG_SODIUM_KEY_PURCHASE_REQUEST_DOUBLE_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_PURCHASE_REQUEST_DOUBLE_ENCRYPTION ] );
		} else {
			return CCrypto::createService()->decrypt( $strAmount, CONFIG_SODIUM_KEY_PURCHASE_REQUEST_BASE_AMOUNT, [ 'legacy_secret_key' => CONFIG_KEY_PURCHASE_REQUEST_BASE_AMOUNT ] );
		}
	}

}

?>