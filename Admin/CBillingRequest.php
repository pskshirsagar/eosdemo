<?php

use Psi\Eos\Admin\CPsLeads;
Use Psi\Eos\Admin\CPsDocuments;

class CBillingRequest extends CBaseBillingRequest {

	const OSG_PS_LEADS_PATH = 'documents/ps_leads/';
	protected $m_strCompanyName;
	protected $m_strBillingRequestType;

	/**
	 * Create Functions
	 *
	 */

	public function setDefaults() {
		$this->setBillingRequestStatusTypeId( CBillingRequestStatusType::UNUSED );
	}

	public function createBillingRequestDetail() {
		$objBillingRequestDetail = new CBillingRequestDetail();
		$objBillingRequestDetail->setBillingRequestId( $this->getId() );
		return $objBillingRequestDetail;

	}

	public function createDefaultStakeHolder( $arrintStakeholderEmployeeIds ) {

		$objStakeholder = new CStakeholder();
		$objStakeholder->setStakeholderTypeId( CStakeholderType::BILLING_REQUEST );
		$objStakeholder->setStakeholderReferenceId( $this->getId() );
		foreach( $arrintStakeholderEmployeeIds as $intEmployeeId ) {
			$objCloneStakeholder = clone $objStakeholder;
			$objCloneStakeholder->setEmployeeId( $intEmployeeId );
			$arrobjStakeholderLists[] = $objCloneStakeholder;
		}

		return $arrobjStakeholderLists;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getBillingRequestType() {
		return $this->m_strBillingRequestType;
	}

	/**
	 * Set Functions
	 */

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setBillingRequestType( $strBillingRequestType ) {
		$this->m_strBillingRequestType = $strBillingRequestType;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) ) {
			$this->setCompanyName( $arrmixValues['company_name'] );
		}

		if( true == isset( $arrmixValues['billing_request_type'] ) ) {
			$this->setBillingRequestType( $arrmixValues['billing_request_type'] );
		}
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valCid() {
		$boolIsValid = true;
		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Company Name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valBillingRequestTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getBillingRequestTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Request Type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valBillingRequestStatusTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getBillingRequestStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Request Type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valReasonForDelay() {
		$boolIsValid = true;
		if( false == valStr( $this->getReasonForDelay() ) ) {
			$boolIsValid = false;
			$strErrorMsg = CBillingRequestType::DELAYED_BILLING == $this->getBillingRequestTypeId() ? 'Reason for Delay is required.' : 'Reason for Suspend is required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', $strErrorMsg ) );
		}
		return $boolIsValid;
	}

	public function valPersonId() {
		$boolIsValid = true;
		if( !valId( $this->getPersonId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'person_id', 'Company Contact is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valBillingRequestTypeId();
				$boolIsValid &= $this->valBillingRequestStatusTypeId();
				$boolIsValid &= $this->valPersonId();
				$boolIsValid &= $this->valReasonForDelay();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return ( bool ) $boolIsValid;
	}

	/**
	 *  Extra Functions
	 *
	 */

	public function generateDocumentSignature( $strSignatureText, $objAdminDatabase, $objObjectStorageGateway, $boolIsEntrata = false, $strCustomerDocumentTitle = NULL ) {

		$arrintClientPsLeadIds	= CPsLeads::createService()->fetchPsLeadIdsByCids( [ $this->getCid() ], $objAdminDatabase );
		$strDestinationPath		= PATH_MOUNTS_DOCUMENTS_PS_LEADS . $arrintClientPsLeadIds[$this->getCid()]['id'] . '/';

		$objPsDocument	= CPsDocuments::createService()->fetchPsDocumentById( $this->getPsDocumentId(), $objAdminDatabase );
		$strFileName	= $objPsDocument->getFileName();
		$strFileNameWithPath = $strDestinationPath . $strFileName;
		$boolIsFileFromS3 = false;

		if( !CFileIo::fileExists( $strFileNameWithPath ) ) {
			$boolIsFileFromS3 = true;
			$strFileNameWithPath = $objPsDocument->downloadObject( $objObjectStorageGateway, $objPsDocument->getFileName(), $strDispositionType, true );
			if( !$strFileNameWithPath ) {
				return false;
			}
		}

		try {
			$objZendPdf			= Zend_Pdf::load( $strFileNameWithPath );

			$intLastPageIndex	= \Psi\Libraries\UtilFunctions\count( $objZendPdf->pages ) - 1;
			$objZendPdfPage		= $objZendPdf->pages[$intLastPageIndex];

			$resImage = imagecreatefrompng( PATH_COMMON . '/images/lease_execution/esign_initials.png' );
			// Allocate A Color For The Text
			$resWhite		= imagecolorallocate( $resImage, 255, 255, 255 );
			$resGrey		= imagecolorallocate( $resImage, 128, 128, 128 );
			$resColorBlack	= imagecolorallocate( $resImage, 0, 0, 0 );

			// Set Path to Font File
			$strFontFile			= PATH_LIBRARY_PSI_FONTS . 'SWENSON.TTF';
			$strSignatureImagePath	= CONFIG_CACHE_DIRECTORY . 'sign_' . time() . '.png';

			imagettftext( $resImage, $intSize = 20, $intAngle = 0, $intX = 25, $intY = 52, $resColorBlack, $strFontFile, $strSignatureText );

			$strFontFile = PATH_LIBRARY_PSI_FONTS . 'arial_narrow.ttf';

			imagettftext( $resImage, $intSize = 10, $intAngle = 0, $intX = 31, $intY = 82, $resWhite, $strFontFile, getRemoteIpAddress() );
			imagettftext( $resImage, $intSize = 10, $intAngle = 0, $intX = 10, $intY = 22, $resGrey, $strFontFile, date( 'm/d/y h:i A' ) );
			imageinterlace( $resImage, false );
			imagepng( $resImage, $strSignatureImagePath );

			$objFont			= Zend_Pdf_Font::fontWithName( Zend_Pdf_Font:: FONT_HELVETICA );
			$objZendPdfImage	= Zend_Pdf_Image::imageWithPath( $strSignatureImagePath );

			if( $boolIsEntrata ) {
				$objZendPdfPage->drawImage( $objZendPdfImage, 105, 30, 265, 100 );
				$objZendPdfPage->setFont( $objFont, 9 )->drawText( $strSignatureText, 105, 147 );
				$objZendPdfPage->setFont( $objFont, 9 )->drawText( CBillingRequestType::$c_arrintBillingRequestTypes[$this->getBillingRequestTypeId()], 105, 109 );
				$objZendPdfPage->setFont( $objFont, 9 )->drawText( $strCustomerDocumentTitle, 105, 128 );
			} else {
				$objZendPdfPage->drawImage( $objZendPdfImage, 395, 30, 555, 100 );
				$objZendPdfPage->setFont( $objFont, 9 )->drawText( $strSignatureText, 395, 147 );
				$objZendPdfPage->setFont( $objFont, 9 )->drawText( CBillingRequestType::$c_arrintBillingRequestTypes[$this->getBillingRequestTypeId()], 395, 109 );
				$objZendPdfPage->setFont( $objFont, 9 )->drawText( __( 'Corporate Counsel' ), 395, 128 );
			}

			$objZendPdf->save( $strFileNameWithPath );

			if( $boolIsFileFromS3 && file_exists( $strFileNameWithPath ) ) {
				$strBaseFileName = basename( $strFileNameWithPath );
				$strTemFilePath = str_replace( $strBaseFileName, '', $strFileNameWithPath );
				rename( $strTemFilePath . $strBaseFileName, $strTemFilePath . $objPsDocument->getFileName() );
				$strFileNameWithPath = $strTemFilePath . $objPsDocument->getFileName();
				if( !$objPsDocument->uploadObject( $objObjectStorageGateway, $strFileNameWithPath ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Failed to upload outage attachment.' ) );
					return false;
				}

				$objStoredObject		= $objPsDocument->fetchStoredObject( $objAdminDatabase );
				if( valObj( $objStoredObject, CStoredObject::class ) ) {
					$objPsDocument->insertOrUpdateStoredObject( CUser::ID_BILLING_BILLING, $objAdminDatabase, $objStoredObject, $objPsDocument->getFileName() );
				}
			}

			imagedestroy( $resImage );
			if( false == is_null( $strSignatureImagePath ) && false == is_dir( $strSignatureImagePath ) && true == CFileIo::fileExists( $strSignatureImagePath ) ) {
				CFileIo::deleteFile( $strSignatureImagePath );
			}

			return true;
		} catch( Exception $objException ) {
			trigger_error( sprintf( 'Unable to draw signature image: Error: %s ;', $objException->getMessage() ), E_USER_WARNING );

			return false;
		}
	}

	public function addBillingRequestAction( $arrmixActionData, $objAdminDatabase ) {

		if( false == valArr( $arrmixActionData ) ) {
			return false;
		}

		$objAction = new CAction();
		$objAction->setActionTypeId( $arrmixActionData['action_type'] );
		$objAction->setActionDescription( $arrmixActionData['description'] );
		$objAction->setEmployeeId( $arrmixActionData['employee_id'] );
		$objAction->setPsLeadId( $arrmixActionData['ps_lead_id'] );
		$objAction->setNotes( $arrmixActionData['notes'] . ( ( CActionType::BILLING_REQUEST_NOTE != $arrmixActionData['action_type'] ) ? $objAction->buildEmployeeDescription( $objAdminDatabase ) : '' ) );

		if( false == $objAction->validate( VALIDATE_INSERT, false ) || false == $objAction->insert( $arrmixActionData['user_id'], $objAdminDatabase ) ) {
			$this->addErrorMsgs( $objAction->getErrorMsgs() );
			return false;
		}

		$objActionReference = new CActionReference();
		$objActionReference->setActionReferenceTypeId( CActionReferenceType::BILLING_REQUEST );
		$objActionReference->setReferenceNumber( $this->getId() );
		$objActionReference->setActionId( $objAction->getId() );

		if( false == $objActionReference->validate( VALIDATE_INSERT ) || false == $objActionReference->insert( $arrmixActionData['user_id'], $objAdminDatabase ) ) {
			$this->addErrorMsgs( $objActionReference->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function createBillingRequestTask( $strDescription, $objAdminDatabase ) {

		$objTask = new CTask();
		$intTaskId = $objTask->fetchNextId( $objAdminDatabase );
		$objTask->setId( $intTaskId );
		$objTask->setDescription( $strDescription );
		$objTask->setTaskPriorityId( CTaskPriority::NORMAL );
		$objTask->setTaskStatusId( CTaskStatus::UNUSED );
		$objTask->setTaskTypeId( CTaskType::ACCOUNTING );
		$objTask->setUserId( CUser::ID_BILLING_BILLING );

		$this->setTaskId( $intTaskId );

		return $objTask;

	}

	public function createBillingRequestTaskAttachment( $objTask, $objAdminDatabase, $objObjectStorageGateway = NULL ) {

		$arrintClientPsLeadIds	= CPsLeads::createService()->fetchPsLeadIdsByCids( [ $this->getCid() ], $objAdminDatabase );

		$objPsDocument		= CPsDocuments::createService()->fetchPsDocumentById( $this->getPsDocumentId(), $objAdminDatabase );

		$objTaskAttachment		= $objTask->createTaskAttachment();
		$objTaskAttachment->setFileName( $objPsDocument->getFileName() );

		if( false == is_dir( $objTaskAttachment->getFullTaskAttachmentPath() ) && false == CFileIo::recursiveMakeDir( $objTaskAttachment->getFullTaskAttachmentPath() ) ) {
			trigger_error( 'Couldn\'t create directory(' . $objTaskAttachment->getFullTaskAttachmentPath() . ' ). ', E_USER_WARNING );
		}

		$intTaskAttachmentId = $objTaskAttachment->fetchNextId( $objAdminDatabase );

		$objTaskAttachment->setUrl( $intTaskAttachmentId . '_' . $objTaskAttachment->getFileName() );

		$strFilePath = $objPsDocument->downloadObject( $objObjectStorageGateway, $objPsDocument->getFileName(), '', true );
		if( !$strFilePath ) {
			trigger_error( 'Contract document is not present or has been deleted.', E_USER_WARNING );
			exit;
		}

		CFileIo::copyFile( $strFilePath, $objTaskAttachment->getFullTaskAttachmentPath() . $objTaskAttachment->getUrl() );

		return $objTaskAttachment;
	}

}
?>