<?php

class CInterviewType extends CBaseInterviewType {

	const GD				= 1;
	const TECH				= 2;
	const HR				= 3;
	const TELEPHONIC		= 4;
	const SKYPE_INTERVIEW	= 5;

	const TELEPHONIC_INTERVIEW		= 'Telephonic';
	const SKYPE_INTERVIEW_TYPE		= 'Skype Interview';
	const GOOGLE_MEET_INTERVIEW		= 'Google Meet Interview';

	public static function loadSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'INTERVIEW_TYPE_GD', 	self::GD );
		$objSmarty->assign( 'INTERVIEW_TYPE_TECH',	self::TECH );
		$objSmarty->assign( 'INTERVIEW_TYPE_HR', 	self::HR );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

	switch( $strAction ) {
		case VALIDATE_INSERT:
		case VALIDATE_UPDATE:
		case VALIDATE_DELETE:
			break;

		default:
			// no case for default
			break;
	}

	return $boolIsValid;
	}
}
?>