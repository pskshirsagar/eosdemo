<?php

class CSemSetting extends CBaseSemSetting {

	protected $m_fltAmountDue;

	public function __construct() {
		parent::__construct();

		$this->m_fltAmountDue = 0;

		return;
	}

	public function getAmountDue() {
		return $this->m_fltAmountDue;
	}

	public function setAmountDue( $fltAmountDue ) {
		$this->m_fltAmountDue = $fltAmountDue;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['amount_due'] ) ) $this->setAmountDue( $arrValues['amount_due'] );

		return;
	}

	public function valAccountId( $arrobjProperties = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getAccountId() ) ) {
			$boolIsValid = false;
			$strMessage = ( true == valArr( $arrobjProperties ) && true == isset( $arrobjProperties[$this->getPropertyId()] ) ? ' for ' . $arrobjProperties[$this->getPropertyId()]->getPropertyName():'' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Account is required ' . $strMessage . '.' ) );
		}

		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', 'Frequency id is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrobjProperties = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAdBudget();
				$boolIsValid &= $this->valAccountId( $arrobjProperties );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function fetchBalance( $objDatabase ) {

		$strSql = 'SELECT
						sum( transaction_amount )
					FROM
						sem_transactions
					WHERE 1 = 1 ';

		if( true == is_numeric( $this->getPropertyId() ) ) {
			$strSql .= ' AND property_id = ' . ( int ) $this->getPropertyId() . ' ';
		} elseif( true == is_numeric( $this->getSemAdGroupId() ) ) {
			$strSql .= ' AND sem_ad_group_id = ' . ( int ) $this->getSemAdGroupId() . ' AND property_id IS NULL ';
		}

		$arrfltBalance = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrfltBalance[0]['sum'] ) ) {
			return ( float ) $arrfltBalance[0]['sum'];

		} else {
			return 0;
		}
	}

	public function fetchSemPropertyDetail( $objDatabase ) {
		return CSemPropertyDetails::fetchSemPropertyDetailByPropertyId( $this->getPropertyId(), $objDatabase );
	}

	public function createSemBudget( $objDatabase = NULL, $fltBillingAmount = NULL ) {

		$objSemBudget = new CSemBudget();
		$objSemBudget->setCid( $this->getCid() );
		$objSemBudget->setAccountId( $this->getAccountId() );
		$objSemBudget->setSemAdGroupId( $this->getSemAdGroupId() );
		$objSemBudget->setPropertyId( $this->getPropertyId() );

		// Budget Date should be the current month
		if( CFrequency::MONTHLY == $this->getFrequencyId() ) {
			$objSemBudget->setBudgetDate( date( 'm' ) . '/1/' . date( 'Y' ) );

		} elseif( CFrequency::DAILY == $this->getFrequencyId() ) {
			$objSemBudget->setBudgetDate( date( 'm' ) . '/' . date( 'd' ) . '/' . date( 'Y' ) );
		}

		$fltBudget = 0;

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			if( true == is_null( $fltBillingAmount ) ) {
				// Find the unused or overused balance by summing the sem_transactions table
				$fltAccountBalance = $this->fetchBalance( $objDatabase );

				// [SG] WE should prorate the Ad Budget amount but not the balance if any.
				if( CFrequency::MONTHLY == $this->getFrequencyId() && 0 < $this->getAdBudget() ) {
					$fltProratedAdBudget = ( $this->getAdBudget() / date( 't' ) ) * ( ( date( 't' ) - date( 'd' ) ) + 1 );
				} else {
					$fltProratedAdBudget = $this->getAdBudget();
				}

				// If balance is greater than or equal to zero
				// [SG] If balace is positive means client owe us money , so we need to bill for overages + ad budget
				if( 0 <= $fltAccountBalance ) {
					$fltBillingAmount = $fltProratedAdBudget + $fltAccountBalance;
					$fltBudget 		  = $fltProratedAdBudget;

				// If balance is less than zero
				} else {

					// If balance is less than new budget
					// [SG]If balance is negative means they have credit , so we need to bill for adbudget - credit
					if( abs( $fltAccountBalance ) <= $fltProratedAdBudget ) {

						$fltBillingAmount = $fltProratedAdBudget - abs( $fltAccountBalance );
						$fltBudget = $fltProratedAdBudget;

					// If balance is greater than new budget
					} else {
						$fltBillingAmount = 0;
						$fltBudget = abs( $fltAccountBalance );
					}
				}

				$objSemBudget->setMonthlyBudget( $this->getAdBudget() );
				$objSemBudget->setRemainingBudget( $fltBudget );

			} else {
				$objSemBudget->setMonthlyBudget( $fltBillingAmount );
				$objSemBudget->setRemainingBudget( $fltBillingAmount );
			}

			$objSemBudget->setBillingAmount( $fltBillingAmount );

			if( CFrequency::MONTHLY == $this->getFrequencyId() ) {
				$intDaysRemainingInMonth 	= ( date( 't' ) - date( 'd' ) ) + 1;

				$objSemBudget->setDailyRemainingBudget( $fltBudget / $intDaysRemainingInMonth );

			} else {
				$objSemBudget->setDailyRemainingBudget( $fltBudget );
			}

			$this->setLastPostedOn( $objSemBudget->getBudgetDate() );
		}

		return $objSemBudget;
	}

}
?>