<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeSalaryTypes
 * Do not add any new functions to this class.
 */

class CEmployeeSalaryTypes extends CBaseEmployeeSalaryTypes {

	public static function fetchEmployeeSalaryTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmployeeSalaryType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEmployeeSalaryType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmployeeSalaryType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllEmployeeSalaryTypes( $objDatabase ) {
		return CEmployeeSalaryTypes::fetchEmployeeSalaryTypes( 'SELECT * FROM employee_salary_types', $objDatabase );
	}

}
?>