<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportPermissions
 * Do not add any new functions to this class.
 */

class CReportPermissions extends CBaseReportPermissions {

	public static function fetchReportPermissionsByCompanyReportIdByPrimaryOwner( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportPermission( sprintf( 'SELECT * FROM report_permissions WHERE company_report_id = %d', ( int ) $intCompanyReportId ) . ' AND is_primary_owner = true', $objDatabase );
	}

	public static function fetchReportPermissionsByCompanyReportIdBySecondaryOwner( $intCaReportId, $objDatabase ) {
		return self::fetchReportPermissions( sprintf( 'SELECT * FROM report_permissions WHERE company_report_id = %d', ( int ) $intCaReportId ) . ' AND is_secondary_owner = true AND deleted_by IS NULL', $objDatabase );
	}

	public static function fetchReportPermissionsByCompanyReportIdBySecondaryOwnerEmloyeeId( $intCompanyReportId, $arrintEmployeeIds, $objAdminDatabase ) {

		if( false == is_numeric( $intCompanyReportId ) || false == valArr( array_filter( $arrintEmployeeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						report_permissions AS rp
					WHERE
						rp.company_report_id = ' . ( int ) $intCompanyReportId . '
						AND rp.is_secondary_owner = true 
						AND rp.employee_id IN ( ' . implode( ',', array_filter( $arrintEmployeeIds ) ) . ' ) ';

		return parent::fetchReportPermissions( $strSql, $objAdminDatabase );
	}

	public static function fetchReportDepartmentsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportPermissions( sprintf( 'SELECT * FROM report_permissions WHERE company_report_id = %d', ( int ) $intCompanyReportId ) . ' AND department_id IS NOT NULL AND deleted_by IS NULL', $objDatabase );
	}

	public static function fetchReportDepartmentPermissionByCompanyReportIdByDepartmentId( $intCompanyReportId, $arrintDepartmentIds, $objAdminDatabase ) {
		if( false == is_numeric( $intCompanyReportId ) || false == valArr( array_filter( $arrintDepartmentIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						report_permissions AS rp
					WHERE
						rp.company_report_id = ' . ( int ) $intCompanyReportId . '
						AND rp.department_id IN ( ' . implode( ',', array_filter( $arrintDepartmentIds ) ) . ' ) ';

		return parent::fetchReportPermissions( $strSql, $objAdminDatabase );
	}

	public static function fetchReportDepartmentsByCompanyReportIdFByGRoup( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportPermissions( sprintf( 'SELECT * FROM report_permissions WHERE company_report_id = %d', ( int ) $intCompanyReportId ) . ' AND group_id IS NOT NULL AND deleted_by IS NULL', $objDatabase );
	}

	public static function fetchReportGroupPermissionByCompanyReportIdByGroupId( $intCompanyReportId, $arrintGroupIds, $objAdminDatabase ) {
		if( false == is_numeric( $intCompanyReportId ) || false == valArr( array_filter( $arrintGroupIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						report_permissions AS rp
					WHERE
						rp.company_report_id = ' . ( int ) $intCompanyReportId . '
						AND rp.group_id IN ( ' . implode( ',', array_filter( $arrintGroupIds ) ) . ' ) ';

		return parent::fetchReportPermissions( $strSql, $objAdminDatabase );
	}

	public static function fetchReportEmployeeIdsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportPermissions( sprintf( 'SELECT * FROM report_permissions WHERE company_report_id = %d', ( int ) $intCompanyReportId ) . ' AND is_primary_owner = false AND is_secondary_owner =false AND deleted_by IS NULL ', $objDatabase );
	}

	public static function fetchReportGroupPermissionByCompanyReportIdByEmployeeId( $intCompanyReportId, $arrintEmployeeIds, $objAdminDatabase ) {
		if( false == is_numeric( $intCompanyReportId ) || false == valArr( array_filter( $arrintEmployeeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						report_permissions AS rp
					WHERE
						rp.company_report_id = ' . ( int ) $intCompanyReportId . '
						AND is_primary_owner = false 
						AND is_secondary_owner =false
						AND rp.employee_id IN ( ' . implode( ',', array_filter( $arrintEmployeeIds ) ) . ' ) ';

		return parent::fetchReportPermissions( $strSql, $objAdminDatabase );
	}

	public static function fetchSharedReportDetailsByCompanyReportIdByGroupByDepartmentByEmployee( $intCompanyReportId, $boolGroups = false, $boolDepartments = false, $boolEmployees = false, $objDatabase ) {
		$strSqlCondition = '';

		if( false == is_numeric( $intCompanyReportId ) ) return NULL;

		if( true == $boolGroups ) {
			$strSqlCondition = ' AND department_id is not null';
		} elseif( true == $boolDepartments ) {
			$strSqlCondition = ' AND employee_id is not null';
		} elseif( true == $boolEmployees ) {
			$strSqlCondition = ' AND group_id is not null';
		}

		$strSql = 'SELECT 
					* 
				FROM 
					report_permissions 
				WHERE 
					company_report_id = ' . ( int ) $intCompanyReportId . ' 
					AND is_primary_owner = false 
					AND is_secondary_owner = false 
					AND deleted_by is NULL' . $strSqlCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function updateSharedReportPermissionsByIds( $arrintIds, $intEmployeeId, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		if( false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'UPDATE 
					report_permissions 
				SET deleted_on = NOW ( ), 
					deleted_by=' . $intEmployeeId . ', 
					updated_by = ' . ( int ) $intEmployeeId . ', 
					updated_on = NOW ( ) 
				WHERE id in ( ' . implode( ',', $arrintIds ) . ' )';

		if( false == is_resource( $objDatabase->execute( $strSql ) ) ) {
			return false;
		}

		return true;
	}

	public static function fetchReportPermissionsByEmployeeIds( $arrintEmployeeIds, $boolPrimary, $objDatabase ) {

		$strSelectClause = 'rp.*';
		if( false == $boolPrimary ) {
			$strSelectClause = 'rp1.*';
			$strJoinClause = 'JOIN report_permissions rp1 ON ( rp.company_report_id = rp1.company_report_id and rp1.is_secondary_owner = true AND rp1.deleted_by IS NULL )
     							JOIN employees e ON ( rp1.employee_id = e.id and e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ')';
		}

		$strSql = 'SELECT
						' . $strSelectClause . '
					FROM
						company_reports cr
						JOIN report_permissions rp ON( rp.company_report_id = cr.id AND rp.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND rp.is_primary_owner = true )
						' . $strJoinClause . '
					WHERE
						cr.deleted_by IS NULL AND
						rp.deleted_by IS NULL';

		return parent::fetchReportPermissions( $strSql, $objDatabase );
	}

	public static function fetchSharedReportsByEmployeeIdByGroupIdsByDepartmentId( $intEmployeeId, $arrintGroupIds, $intDepartmentId, $intLimit, $intOffset, $strSortBy, $intSortOrder, $objDatabase ) {

		$strLimit = '';
		if( true == is_numeric( $intLimit ) ) {
			$strLimit = ' limit ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;
		}

		$strWhereClauseSubQuery = '';
		if( true == valArr( $arrintGroupIds ) ) {
			$strWhereClauseSubQuery .= ' OR rp.group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )';
		}
		if( true == valId( $intDepartmentId ) ) {
			$strWhereClauseSubQuery .= ' OR rp.department_id =' . ( int ) $intDepartmentId;
		}

		$strOrderClause = 'ORDER BY cr.id, rp.created_on DESC';

		if( true == valStr( $strSortBy ) ) {
			$strSortDirection = ( 1 == $intSortOrder ) ? ' DESC' : ' ASC';
			switch( $strSortBy ) {
				case 'name':
					$strOrderClause = ' ORDER BY cr.id ' . $strSortDirection . ' , cr.name' . $strSortDirection;
					break;

				case 'departments':
					$strOrderClause = ' ORDER BY cr.id ' . $strSortDirection . ', departments' . $strSortDirection;
					break;

				case 'tags':
					$strOrderClause = ' ORDER BY cr.id ' . $strSortDirection . ', tags' . $strSortDirection;
					break;

				case 'shared_by':
					$strOrderClause = ' ORDER BY cr.id ' . $strSortDirection . ', shared_by' . $strSortDirection;
					break;

				default:
					$strOrderClause = ' ORDER BY cr.id ' . $strSortDirection . ', rp.created_on DESC';
					break;
			}
		}

		$strSql = '	SELECT
						DISTINCT ON (cr.id)
						cr.id,
						cr.name,
						cr.url,
						cr.description,
						cr.is_shared_access,
						cr.ps_module_id,
						array_to_string(array_agg(DISTINCT (d.name)), \', \') as departments,
						array_to_string(array_agg(DISTINCT (rda.department_id)), \', \') as department_ids,
						(
						 SELECT array_to_string(array_agg(t.name), \', \')
						 FROM tags_associations ta
							JOIN tags As t ON (t.id = ta.tag_id)
						 WHERE ta.company_report_id = cr.id
						) as tags,
						3 AS permission,
						( SELECT count( a.id ) FROM actions a JOIN action_references ar ON (ar.action_id = a.id  AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' ) where ar.reference_number = cr.id and action_result_id=' . CAction::ACTION_RESULT_VIEW . ') as views,
						( SELECT count( a.id ) FROM actions a JOIN action_references ar ON (ar.action_id = a.id  AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' ) where ar.reference_number = cr.id and action_result_id=' . CAction::ACTION_RESULT_DOWNLOAD . ') as downloads,
						( SELECT count( a.id ) FROM actions a JOIN action_references ar ON (ar.action_id = a.id  AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' ) where ar.reference_number = cr.id and action_result_id=' . CAction::ACTION_RESULT_FAVORITE . ') as favorites,
						( SELECT count( rc.id ) FROM report_comments rc where rc.company_report_id = cr.id ) as comments,
						( select count( a.id ) from actions a JOIN action_references ar ON (ar.action_id = a.id AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' ) where ar.reference_number = cr.id AND employee_id = ' . ( int ) $intEmployeeId . ' AND action_result_id=' . CAction::ACTION_RESULT_FAVORITE . ' ) as fav_report,
						( select e.preferred_name from employees e JOIN report_permissions rp1 ON (e.id = rp1.employee_id) WHERE cr.id = rp1.company_report_id AND rp1.is_primary_owner = true ) As owner,
						e.preferred_name as shared_by,
						rp.created_on
					from
						report_permissions rp
						RIGHT JOIN company_reports cr ON (rp.company_report_id = cr.id)
						JOIN users u ON(u.id = rp.created_by)
						JOIN employees e ON (e.id = u.employee_id)
						LEFT JOIN report_department_associations rda ON ( rda.company_report_id = cr.id )
						LEFT JOIN departments AS d ON ( rda.department_id = d.id )
						WHERE cr.deleted_by IS NULL
							AND cr.is_published = true
							AND cr.ps_module_id IS NULL AND ( ( ( rp.employee_id = ' . ( int ) $intEmployeeId . $strWhereClauseSubQuery . ' )
							AND rp.is_primary_owner = false AND
							rp.is_secondary_owner = false) OR
							NOT EXISTS (
									SELECT id
									FROM report_permissions
									WHERE company_report_id = cr.id
									) )
					Group BY
						cr.id, e.preferred_name, rp.created_on '
				  . $strOrderClause . $strLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSeconadryOwnersReportPermissionByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rp.*
					FROM
						report_permissions rp
						JOIN company_reports cr ON( rp.company_report_id = cr.id AND rp.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND rp.is_secondary_owner = true )
					WHERE
						rp.deleted_by IS NULL';

		return parent::fetchReportPermissions( $strSql, $objDatabase );
	}

}
?>