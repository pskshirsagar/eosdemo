<?php

class CFeedbackType extends CBaseFeedbackType {

	const CROSS_PRODUCT_REQUEST = 1;
	const FEEDBACK              = 2;
	const NON_URGENT_BUG        = 3;
	const TECHNICAL_DEBT        = 4;

	public static $c_arrstrFeedbackTypeIds = [
		self::CROSS_PRODUCT_REQUEST,
		self::FEEDBACK,
		self::NON_URGENT_BUG,
		self::TECHNICAL_DEBT,
	];

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>