<?php

class CEmployeeDocument extends CBaseEmployeeDocument {
	use Psi\Libraries\EosFoundation\TEosStoredObject;

	private $m_strDocumentCategoryName;
	protected $m_intCid;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNote() {
		if( ( true == $this->getPartiallyApproved() || true == valStr( $this->getDeniedBy() ) ) && false == valStr( $this->getNote() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note is required.' ) );
			return false;
		}
		return true;
	}

	public function valEmployeeDocumentTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getEmployeeDocumentTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_document_type_id', __( 'Document type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeDocumentCategoryId() {
		$boolIsValid = true;

		if( false == valId( $this->getEmployeeDocumentCategoryId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_document_category_id', __( 'Document Category is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		if( false == valId( $this->getEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFileExtensionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPartiallyApproved() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			$boolIsValid &= $this->valNote();
			case 'validate_employee_document':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valEmployeeDocumentTypeId();
				$boolIsValid &= $this->valEmployeeDocumentCategoryId();
				$boolIsValid &= $this->valEmployeeId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setDocumentCategoryName( $strDocumentCategoryName ) {
		$this->m_strDocumentCategoryName = $strDocumentCategoryName;
	}

	public function getDocumentCategoryName() {
		return $this->m_strDocumentCategoryName;
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true === isset( $arrmixValues['document_category_name'] ) ) {
			$this->setDocumentCategoryName( $arrmixValues['document_category_name'] );
		}
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		$strPath = '';

		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				if( CEmployeeDocumentType::ID_EMPLOYEE_POLICY_DOCUMENTS == $this->getEmployeeDocumentTypeId() ) {
					$strPath = 'employees/' . $this->getEmployeeId() . '/' . $this->getName();
				}
				break;

			default:
				if( CEmployeeDocumentType::ID_EMPLOYEE_POLICY_DOCUMENTS == $this->getEmployeeDocumentTypeId() || CEmployeeDocumentType::ID_REVIEW_DOCUMENTS == $this->getEmployeeDocumentTypeId() ) {
					$strPath = sprintf( '%d/%s/%d/%d/%d/%d/%02d/%02d/%d/', CClient::ID_DEFAULT, 'employee_documents', $this->getEmployeeId(), $this->getEmployeeDocumentTypeId(), $this->getEmployeeDocumentCategoryId(), date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() );
					$strPath .= $this->getName();
				}
				if( CEmployeeDocumentType::ID_EMPLOYEE_INVESTMENT_DOCUMENTS == $this->getEmployeeDocumentTypeId() ) {
					$intDate		= $this->getCreatedOn();
					$strPath = 'employee_documents/' . $this->getEmployeeId() . '/' . $this->getEmployeeDocumentTypeId() . '/' . date( 'Y/m/d', strtotime( $intDate ) ) . '/' . $this->getId() . '_' . $this->getName();
				}
				break;
		}

		return $strPath;
	}

	protected function calcStorageContainer( $strVendor = NULL ) {
		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				if( CEmployeeDocumentType::ID_EMPLOYEE_POLICY_DOCUMENTS == $this->getEmployeeDocumentTypeId() ) {
					return PATH_MOUNTS_DOCUMENTS;
				}
				break;

			default:
				if( CEmployeeDocumentType::ID_EMPLOYEE_POLICY_DOCUMENTS == $this->getEmployeeDocumentTypeId() || CEmployeeDocumentType::ID_REVIEW_DOCUMENTS == $this->getEmployeeDocumentTypeId() || CEmployeeDocumentType::ID_EMPLOYEE_INVESTMENT_DOCUMENTS == $this->getEmployeeDocumentTypeId() ) {
					return CConfig::get( 'OSG_BUCKET_SYSTEM_EMPLOYEES' );
				}
				break;
		}

		return CConfig::get( 'OSG_BUCKET_SYSTEM_DOCUMENTS' );
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

	public function fetchFileExtension( $objDatabase ) {

		if( false == is_null( $this->getName() ) ) {
			$arrstrFileParts = pathinfo( $this->getName() );
			if( true == valArr( $arrstrFileParts ) && true == array_key_exists( 'extension', $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
				return \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
			}
		}

		return false;
	}

	public function downloadObject( $objObjectStorageGateway, $strFileName, $strDispositionType ) {

		if( false == valObj( $objObjectStorageGateway, 'CProxyObjectStorageGateway' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to get object storage gateway.' ) ) );
			return false;
		}

		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest();
		$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixStorageArgs );

		if( $arrobjObjectStorageResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not present or has been deleted.' ) ) );
			return false;
		}

		$objFileExtension = $this->fetchFileExtension( $this->m_objDatabase );
		$strContentType = ' application/pdf';
		if( valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strContentType = $objFileExtension->getMimeType();
		}

		header( 'Pragma:public' );
		header( 'Expires:0' );
		header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control:public' );
		header( 'Content-Description:File Transfer' );
		header( 'Content-Type: ' . $strContentType );
		header( 'Content-Disposition: ' . $strDispositionType . '; filename="' . $strFileName . '"' );
		echo $arrobjObjectStorageResponse['data'];
		exit();
	}

}
