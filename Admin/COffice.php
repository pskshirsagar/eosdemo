<?php

class COffice extends CBaseOffice {

	const TOWER_8                   = 1;
	const TOWER_9                   = 2;
	const US_HQ_FLOOR_ONE           = 3;
	const US_HQ_FLOOR_TWO           = 4;
	const US_HQ_FLOOR_THREE         = 5;
	const US_HQ_FLOOR_FOUR          = 6;
	const US_PROVO                  = 7;
	const US_DALLAS                 = 8;
	const US_HOME_OFFICE            = 9;
	const US_WFH_LEASING_CENTER     = 10;
	const US_INDEPENDENT_CONTRACTOR = 11;

	const IN_OFFICE = 1;
	const US_OFFICE = 2;

	const PUNE_TOWER_8 = 'Pune - Tower 8';
	const PUNE_TOWER_9 = 'Pune - Tower 9';

	const HQ_FLOOR1 = 'HQ Floor 1';
	const HQ_FLOOR2 = 'HQ Floor 2';
	const HQ_FLOOR3 = 'HQ Floor 3';
	const HQ_FLOOR4 = 'HQ Floor 4';

	public static $c_arrstrOfficeInitials = array(
		self::TOWER_8                   => 'T8',
		self::TOWER_9                   => 'T9',
		self::US_HQ_FLOOR_ONE           => 'HQ-F1',
		self::US_HQ_FLOOR_TWO           => 'HQ-F2',
		self::US_HQ_FLOOR_THREE         => 'HQ-F3',
		self::US_HQ_FLOOR_FOUR          => 'HQ-F4',
		self::US_PROVO                  => 'P',
		self::US_DALLAS                 => 'D',
		self::US_HOME_OFFICE            => 'HO',
		self::US_WFH_LEASING_CENTER     => 'WLC',
		self::US_INDEPENDENT_CONTRACTOR => 'IC'
	);

	public static $c_arrintAllOfficeIds = array(
		self::TOWER_8,
		self::TOWER_9,
		self::US_HQ_FLOOR_ONE,
		self::US_HQ_FLOOR_TWO,
		self::US_HQ_FLOOR_THREE,
		self::US_HQ_FLOOR_FOUR,
		self::US_PROVO,
		self::US_DALLAS,
		self::US_HOME_OFFICE,
		self::US_WFH_LEASING_CENTER,
		self::US_INDEPENDENT_CONTRACTOR
	);

	public static $c_arrintIndiaOfficeIds = [
		self::TOWER_8,
		self::TOWER_9
	];

	public static $c_arrintSwagStoreExcludeOfficeIds = [
		self::US_PROVO,
		self::US_WFH_LEASING_CENTER,
		self::US_INDEPENDENT_CONTRACTOR
	];

	public static $c_arrintUSOfficeIds = array(
		self::US_HQ_FLOOR_ONE,
		self::US_HQ_FLOOR_TWO,
		self::US_HQ_FLOOR_THREE,
		self::US_HQ_FLOOR_FOUR,
		self::US_PROVO,
		self::US_DALLAS,
		self::US_HOME_OFFICE,
		self::US_WFH_LEASING_CENTER,
		self::US_INDEPENDENT_CONTRACTOR
	);

	public static $c_arrintUSDeskLocaterOfficeIds = array(
		self::US_HQ_FLOOR_ONE,
		self::US_HQ_FLOOR_TWO,
		self::US_HQ_FLOOR_THREE,
		self::US_HQ_FLOOR_FOUR
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			default:
		}

		return $boolIsValid;
	}

}

?>