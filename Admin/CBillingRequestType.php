<?php

class CBillingRequestType extends CBaseBillingRequestType {

	const DELAYED_BILLING 	= 1;
	const SUSPENDED_BILLING = 2;

	public static $c_arrintBillingRequestTypes = array(
		self::DELAYED_BILLING => 'Delayed Billing',
		self::SUSPENDED_BILLING => 'Suspended Billing',
	);

	public function valId() {
		return true;
	}

	public function valName() {
		return true;
	}

	public function valDescription() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valOrderNumber() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>