<?php

class CStatsPsLead extends CBaseStatsPsLead {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsLeadId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEntrataCoreCustomer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrencyCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewProducts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostProducts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNetProductChange() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCumulativeProducts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubscriptionRevenue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionRevenue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOneTimeRevenue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWriteOffRevenue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalRevenue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuarterRevenueGrowthPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCredits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostRevenueUnhappy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostRevenueUnimplemented() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostRevenueTransferProperty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostRevenueManagement() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalLostRevenue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewLogoUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewProductUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewPropertyUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewTransferPropertyUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostUnitsUnhappy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostUnitsUnimplemented() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostUnitsTransferProperty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostUnitsManagement() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalLostUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNetUnitChange() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCumulativeUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRevenuePerUnit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewLogoSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewProductSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewPropertySubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewRenewalSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewTransferPropertySubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewActiveSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewAdjustedSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewActiveAdjustedSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewLogoTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewProductTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewPropertyTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewRenewalTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewTransferPropertyTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewActiveTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewAdjustedTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewActiveAdjustedTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewActiveAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostSubscriptionAcvUnhappy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostSubscriptionAcvUnimplemented() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostSubscriptionAcvTransferProperty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostSubscriptionAcvManagement() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalLostSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostTransactionAcvUnhappy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostTransactionAcvUnimplemented() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostTransactionAcvTransferProperty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostTransactionAcvManagement() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalLostTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalLostAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostAcvUnhappy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostAcvUnimplemented() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostAcvTransferProperty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLostAcvManagement() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNetAcvChange() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCumulativeAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMcvPerUnit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotNewSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotNewTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotPendingSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotPendingTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotWonSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotWonTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotLostSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotLostTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotOpenCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnimplementedOpenDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnimplementedUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnimplementedSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnimplementedTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnimplementedAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementedUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementedSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementedTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementedAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewLogoImplementation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewProductImplementation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewPropertyImplementation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewRenewalImplementation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewTransferPropertyImplementation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNewImplementation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnbilledOpenDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnbilledAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnbilledUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBilledAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBilledUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemosScheduled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemoedUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemosCompleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemosForgotten() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommissionsScheduled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommissionsPaid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommittedAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSupportNps() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientExecutiveNps() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOverallNps() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSupportNpsSubmissions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientExecutiveNpsSubmissions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOverallNpsSubmissions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewBugSeverity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewBugs() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClosedBugs() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOpenBugs() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewFeatureRequests() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClosedFeatureRequests() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOpenFeatureRequests() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewSupportTickets() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClosedSupportTickets() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOpenSupportTickets() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientImpactfulReleasedTasks() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReleasedTasksWithoutStoryPoints() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStoryPointsReleased() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>