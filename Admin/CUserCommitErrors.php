<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserCommitErrors
 * Do not add any new functions to this class.
 */

class CUserCommitErrors extends CBaseUserCommitErrors {

	public static function fetchMonthlyUserCommitErrorsByEmployeeId( $intEmployeeId, $strMonth, $objDatabase ) {

		$strSql = 'SELECT
					  *
					FROM
					  user_commit_errors
					WHERE
					  employee_id = ' . ( int ) $intEmployeeId . '
					  AND month = \'' . $strMonth . '\'
					ORDER BY id';

		return self::fetchUserCommitErrors( $strSql, $objDatabase );
	}

	public static function fetchUserCommitErrorsForLastTwoMonthsByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strCurrentMonth	= '01' . '-' . date( 'm' ) . '-' . date( 'Y' );
		$strLastMonth 		= date( 'd-m-Y', strtotime( $strCurrentMonth . '-1 months' ) );

		$strSql = 'SELECT
						user_commit_check_type_id, SUM(error_count) as error_count
					FROM
						user_commit_errors
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
					AND
						month IN (\'' . $strCurrentMonth . '\', \'' . $strLastMonth . '\')
					GROUP BY
						user_commit_check_type_id
					ORDER BY
						user_commit_check_type_id';

		return self::fetchCachedObjects( $strSql, 'CUserCommitError', $objDatabase, 'user_commit_error_count_' . $intEmployeeId, 43200 );
	}

	public static function fetchUserCommitErrorsCountsByEmployeeId( $intEmployeeId, $strMonths, $objDatabase ) {

	$strSql = 'SELECT
						sub.employee_id AS employee_id,
						sub.month AS month,
						SUM(sub.code_sniffer) AS code_sniffer,
						SUM(sub.company_pattern_management) AS company_pattern_management,
						SUM(sub.security) AS security
					FROM
						(
							SELECT
								uce.employee_id,
								uce.month,
								(CASE
									WHEN uce.user_commit_check_type_id = ' . CUserCommitCheckType::CODE_SNIFFER . ' THEN uce.error_count
									ELSE 0
								END) AS Code_Sniffer,
								(CASE
									WHEN uce.user_commit_check_type_id = ' . CUserCommitCheckType::SECURITY . ' THEN uce.error_count
									ELSE 0
								END) AS Security,
								(CASE
									WHEN uce.user_commit_check_type_id = ' . CUserCommitCheckType::MANAGE_COMPANY_PATTERNS . ' THEN uce.error_count
									ELSE 0
								END) AS Company_Pattern_Management
							FROM
								user_commit_errors uce
							WHERE
								uce.employee_id = ' . ( int ) $intEmployeeId . ' AND
								uce.month IN (' . implode( ',', $strMonths ) . ')
							ORDER BY
								month
						)sub
					GROUP BY
						sub.employee_id, sub.month
					ORDER BY
						month ASC';

		return fetchData( $strSql, $objDatabase );
	}

}
?>