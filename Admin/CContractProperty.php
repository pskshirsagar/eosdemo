<?php

use Psi\Eos\Admin\CContractProperties;
use Psi\Eos\Admin\CContractPropertyPricings;
use Psi\Eos\Admin\CContractTerminationRequests;
use Psi\Eos\Admin\CPsProductOptions;
use Psi\Eos\Admin\CContracts;
use Psi\Eos\Connect\CDatabases;
use Psi\Eos\Entrata\CPropertyMerchantAccounts;
use Psi\Eos\Entrata\CPropertyPreferences;
use Psi\Eos\Voip\CPropertyCallSettings;
use Psi\Eos\Entrata\CPropertyGroupDocuments;
use Psi\Eos\Entrata\CCompanyApplications;
use Psi\Eos\Entrata\CPropertyIntegrationDatabases;
use Psi\Eos\Admin\CProperties;
use Psi\Eos\Admin\CPsProducts;
use Psi\Eos\Admin\CContractProducts;
use Psi\Eos\Entrata\CScheduledPayments;

/**
 * @method getDataServicesChargeRate() Gets the rate that we're charging for data services using TEosDetails::__call
 * @method setDataServicesChargeRate( $fltRate )
 */
class CContractProperty extends CBaseContractProperty {

	protected $m_arrintPsProductIds;

	protected $m_strPropertyName;
	protected $m_strContractDatetime;
	protected $m_strContractStartDate;
	protected $m_strCompanyName;
	protected $m_strProductName;
	protected $m_strPsProductShortName;
	protected $m_strContractRequestDate;
	protected $m_strChargeMemo;
	protected $m_strAccountName;
	protected $m_strBundleProductName;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strCity;
	protected $m_strPostalCode;
	protected $m_strStateCode;
	protected $m_strContractTerminationRequestStatus;

	protected $m_fltTotalSetUpAmount;
	protected $m_fltTotalMonthlyRecurringAmount;
	protected $m_fltAssociatedChargeTotal;
	protected $m_fltAssociatedChargeMonthly;
	protected $m_fltUnitTransactionalAverage;

	protected $m_intPropertyCount;
	protected $m_intIsTransactional;
	protected $m_intNumberOfUnits;
	protected $m_intContractTypeId;
	protected $m_intEntityId;
	protected $m_intCompanyChargesAmount;
	protected $m_intChargeCodeId;
	protected $m_intTotal;
	protected $m_intActiveContractPropertyId;

	protected $m_intBundledPsProductId;
	protected $m_intContractProductBucketId;

	protected $m_boolApplyProductSetUpAmount;
	protected $m_boolIsManagerial;
	protected $m_boolIsChargeInvoicedOrBatched;

	protected $m_objCompanyCharge;
	protected $m_objImplementationTransaction;
	protected $m_objTrainingTransaction;
	protected $m_intPackageProductId;
	protected $m_intPropertyTypeId;

	/**
	 * Get Functions
	 *
	 */

	public function getActiveContractPropertyId() {
		return $this->m_intActiveContractPropertyId;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getAssociatedChargeTotal() {
		return $this->m_fltAssociatedChargeTotal;
	}

	public function getAssociatedChargeMonthly() {
		return $this->m_fltAssociatedChargeMonthly;
	}

	public function getPsProductIds() {
		return $this->m_arrintPsProductIds;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getContractDatetime() {
		return $this->m_strContractDatetime;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getProductName() {
		return $this->m_strProductName;
	}

	public function getTotalSetUpAmount() {
		return $this->m_fltTotalSetUpAmount;
	}

	public function getTotalMonthlyRecurringAmount() {
		return $this->m_fltTotalMonthlyRecurringAmount;
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function getPsProductShortName() {
		return $this->m_strPsProductShortName;
	}

	public function getIsTransactional() {
		return $this->m_intIsTransactional;
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function getCompanyCharge() {
		return $this->m_objCompanyCharge;
	}

	public function getContractTypeId() {
		return $this->m_intContractTypeId;
	}

	public function getContractStartDate() {
		return $this->m_strContractStartDate;
	}

	public function getContractTerminationRequestDate() {
		return $this->m_strContractRequestDate;
	}

	public function getCompanyChargesAmount() {
		return $this->m_intCompanyChargesAmount;
	}

	public function getChargeMemo() {
		return $this->m_strChargeMemo;
	}

	public function getBoolApplyProductSetUpAmount() {
		return $this->m_boolApplyProductSetUpAmount;
	}

	public function getImplementationTransaction() {
		return $this->m_objImplementationTransaction;
	}

	public function getTrainingTransaction() {
		return $this->m_objTrainingTransaction;
	}

	public function getIsManagerial() {
		return $this->m_boolIsManagerial;
	}

	public function getBundleProductName() {
		return $this->m_strBundleProductName;
	}

	public function getChargeCodeId() {
		return $this->m_intChargeCodeId;
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function getIsChargeInvoicedOrBatched() {
		return $this->m_boolIsChargeInvoicedOrBatched;
	}

	public function getBundledPsProductId() {
		return $this->m_intBundledPsProductId;
	}

	public function getContractProductBucketId() {
		return $this->m_intContractProductBucketId;
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getTotal() {
		return $this->m_intTotal;
	}

	public function getContractTerminationRequestStatus() {
		return $this->m_strContractTerminationRequestStatus;
	}

	public function getPackageProductId() {
		return $this->m_intPackageProductId;
	}

	public function getPropertyTypeId() {
		return $this->m_intPropertyTypeId;
	}

	public function getUnitTransactionalAverage() {
		return $this->m_fltUnitTransactionalAverage;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) && 0 < strlen( trim( $arrmixValues['property_name'] ) ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['short_name'] ) && 0 < strlen( trim( $arrmixValues['short_name'] ) ) ) {
			$this->setPsProductShortName( $arrmixValues['short_name'] );
		}
		if( true == isset( $arrmixValues['is_transactional'] ) && 0 < strlen( trim( $arrmixValues['is_transactional'] ) ) ) {
			$this->setIsTransactional( $arrmixValues['is_transactional'] );
		}
		if( true == isset( $arrmixValues['total_set_up_amount'] ) ) {
			$this->setTotalSetUpAmount( $arrmixValues['total_set_up_amount'] );
		}
		if( true == isset( $arrmixValues['total_monthly_recurring_amount'] ) ) {
			$this->setTotalMonthlyRecurringAmount( $arrmixValues['total_monthly_recurring_amount'] );
		}
		if( true == isset( $arrmixValues['contract_datetime'] ) ) {
			$this->setContractDatetime( $arrmixValues['contract_datetime'] );
		}
		if( true == isset( $arrmixValues['company_name'] ) ) {
			$this->setCompanyName( $arrmixValues['company_name'] );
		}
		if( true == isset( $arrmixValues['product_name'] ) ) {
			$this->setProductName( $arrmixValues['product_name'] );
		}
		if( true == isset( $arrmixValues['property_count'] ) ) {
			$this->setPropertyCount( $arrmixValues['property_count'] );
		}
		if( true == isset( $arrmixValues['number_of_units'] ) ) {
			$this->setNumberOfUnits( $arrmixValues['number_of_units'] );
		}
		if( true == isset( $arrmixValues['contract_type_id'] ) ) {
			$this->setContractTypeId( $arrmixValues['contract_type_id'] );
		}
		if( true == isset( $arrmixValues['contract_start_date'] ) ) {
			$this->setContractStartDate( $arrmixValues['contract_start_date'] );
		}
		if( true == isset( $arrmixValues['associated_charge_total'] ) ) {
			$this->setAssociatedChargeTotal( $arrmixValues['associated_charge_total'] );
		}
		if( true == isset( $arrmixValues['associated_charge_monthly'] ) ) {
			$this->setAssociatedChargeMonthly( $arrmixValues['associated_charge_monthly'] );
		}
		if( true == isset( $arrmixValues['contract_termination_request_date'] ) ) {
			$this->setContractTerminationRequestDate( $arrmixValues['contract_termination_request_date'] );
		}
		if( true == isset( $arrmixValues['company_charges_amount'] ) ) {
			$this->setCompanyChargesAmount( $arrmixValues['company_charges_amount'] );
		}
		if( true == isset( $arrmixValues['charge_memo'] ) ) {
			$this->setChargeMemo( $arrmixValues['charge_memo'] );
		}
		if( true == isset( $arrmixValues['apply_product_set_up_amount'] ) ) {
			$this->setBoolApplyProductSetUpAmount( $arrmixValues['apply_product_set_up_amount'] );
		}
		if( true == isset( $arrmixValues['active_contract_property_id'] ) ) {
			$this->setActiveContractPropertyId( $arrmixValues['active_contract_property_id'] );
		}
		if( true == isset( $arrmixValues['account_name'] ) ) {
			$this->setAccountName( $arrmixValues['account_name'] );
		}
		if( true == isset( $arrmixValues['is_managerial'] ) ) {
			$this->setIsManagerial( $arrmixValues['is_managerial'] );
		}
		if( true == isset( $arrmixValues['bundle_product_name'] ) ) {
			$this->setBundleProductName( $arrmixValues['bundle_product_name'] );
		}
		if( true == isset( $arrmixValues['charge_code_id'] ) ) {
			$this->setChargeCodeId( $arrmixValues['charge_code_id'] );
		}
		if( true == isset( $arrmixValues['entity_id'] ) ) {
			$this->setEntityId( $arrmixValues['entity_id'] );
		}
		if( true == isset( $arrmixValues['bundled_ps_product_id'] ) ) {
			$this->setBundledPsProductId( $arrmixValues['bundled_ps_product_id'] );
		}
		if( true == isset( $arrmixValues['contract_product_bucket_id'] ) ) {
			$this->setContractProductBucketId( $arrmixValues['contract_product_bucket_id'] );
		}
		if( true == isset( $arrmixValues['total'] ) ) {
			$this->setTotal( $arrmixValues['total'] );
		}
		if( true == isset( $arrmixValues['contract_termination_request_status'] ) ) {
			$this->setContractTerminationRequestStatus( $arrmixValues['contract_termination_request_status'] );
		}
		if( true == isset( $arrmixValues['package_product_id'] ) ) {
			$this->setPackageProductId( $arrmixValues['package_product_id'] );
		}
		if( true == isset( $arrmixValues['property_type_id'] ) ) {
			$this->setPropertyTypeId( $arrmixValues['property_type_id'] );
		}
		if( true == isset( $arrmixValues['unit_transactional_average'] ) ) {
			$this->setUnitTransactionalAverage( $arrmixValues['unit_transactional_average'] );
		}
	}

	public function setAssociatedChargeTotal( $fltAssociatedChargeTotal ) {
		$this->m_fltAssociatedChargeTotal = $fltAssociatedChargeTotal;
	}

	public function setAssociatedChargeMonthly( $fltAssociatedChargeMonthly ) {
		$this->m_fltAssociatedChargeMonthly = $fltAssociatedChargeMonthly;
	}

	public function setPsProductIds( $arrintPsProductIds ) {
		$this->m_arrintPsProductIds = $arrintPsProductIds;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPsProductShortName( $strPsProductShortName ) {
		$this->m_strPsProductShortName = $strPsProductShortName;
	}

	public function setActiveContractPropertyId( $intActiveContractPropertyId ) {
		$this->m_intActiveContractPropertyId = $intActiveContractPropertyId;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setContractDatetime( $strContractDatetime ) {
		$this->m_strContractDatetime = CStrings::strTrimDef( $strContractDatetime );
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = CStrings::strTrimDef( $strCompanyName );
	}

	public function setProductName( $strProductName ) {
		$this->m_strProductName = CStrings::strTrimDef( $strProductName );
	}

	public function setTotalSetUpAmount( $fltTotalSetUpAmount ) {
		$this->m_fltTotalSetUpAmount = $fltTotalSetUpAmount;
	}

	public function setContractStartDate( $strContractStartDate ) {
		$this->m_strContractStartDate = $strContractStartDate;
	}

	public function setTotalMonthlyRecurringAmount( $fltTotalMonthlyRecurringAmount ) {
		$this->m_fltTotalMonthlyRecurringAmount = $fltTotalMonthlyRecurringAmount;
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function setIsTransactional( $intIsTransactional ) {
		$this->m_intIsTransactional = $intIsTransactional;
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->m_intNumberOfUnits = $intNumberOfUnits;
	}

	public function setCompanyCharge( $objCompanyCharge ) {
		$this->m_objCompanyCharge = $objCompanyCharge;
	}

	public function setContractTypeId( $intContractTypeId ) {
		$this->m_intContractTypeId = $intContractTypeId;
	}

	public function setContractTerminationRequestDate( $strContractRequestDate ) {
		$this->m_strContractRequestDate = $strContractRequestDate;
	}

	public function setCompanyChargesAmount( $intCompanyChargesAmount ) {
		$this->m_intCompanyChargesAmount = $intCompanyChargesAmount;
	}

	public function setChargeMemo( $strChargeMemo ) {
		$this->m_strChargeMemo = $strChargeMemo;
	}

	public function setBoolApplyProductSetUpAmount( $boolApplyProductSetupAmount ) {
		$this->m_boolApplyProductSetUpAmount = $boolApplyProductSetupAmount;
	}

	public function setImplementationTransaction( $objImplementationTransaction ) {
		$this->m_objImplementationTransaction = $objImplementationTransaction;
	}

	public function setTrainingTransaction( $objTrainingTransaction ) {
		$this->m_objTrainingTransaction = $objTrainingTransaction;
	}

	public function setIsManagerial( $boolIsManagerial ) {
		$this->m_boolIsManagerial = $boolIsManagerial;
	}

	public function setBundleProductName( $strBundleProductName ) {
		$this->m_strBundleProductName = $strBundleProductName;
	}

	public function setChargeCodeId( $intChargeCodeId ) {
		$this->m_intChargeCodeId = $intChargeCodeId;
	}

	public function setEntityId( $intEntityId ) {
		$this->m_intEntityId = $intEntityId;
	}

	public function setIsChargeInvoicedOrBatched( $boolIsChargeInvoicedOrBatched ) {
		$this->m_boolIsChargeInvoicedOrBatched = $boolIsChargeInvoicedOrBatched;
	}

	public function setBundledPsProductId( $intBundledPsProductId ) {
		$this->m_intBundledPsProductId = CStrings::strToIntDef( $intBundledPsProductId );
	}

	public function setContractProductBucketId( $intContractProductBucketId ) {
		$this->m_intContractProductBucketId = CStrings::strToIntDef( $intContractProductBucketId );
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = $strStreetLine1;
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = $strStreetLine2;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = $strCity;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setTotal( $intTotal ) {
		$this->m_intTotal = $intTotal;
	}

	public function setContractTerminationRequestStatus( $strContractTerminationRequestStatus ) {
		$this->m_strContractTerminationRequestStatus = $strContractTerminationRequestStatus;
	}

	public function setPackageProductId( $intPackageProductId ) {
		$this->m_intPackageProductId = $intPackageProductId;
	}

	public function setPropertyTypeId( $intPropertyTypeId ) {
		$this->m_intPropertyTypeId = $intPropertyTypeId;
	}

	public function setUnitTransactionalAverage( $fltUnitTransactionalAverage ) {
		$this->m_fltUnitTransactionalAverage = $fltUnitTransactionalAverage;
	}

	/**
	 * Other Functions
	 *
	 */

	public function createContractProductOption() {

		$objContractProductOption = new CContractProductOption();
		$objContractProductOption->setContractId( $this->getContractId() );
		$objContractProductOption->setCurrencyCode( $this->getCurrencyCode() );
		$objContractProductOption->setPropertyId( $this->getPropertyId() );
		$objContractProductOption->setPsProductId( $this->getPsProductId() );
		$objContractProductOption->setCid( $this->getCid() );

		return $objContractProductOption;
	}

	public function createDefaultContractPropertyPricing( $fltRecurringAmt, $fltMonthlyChangeAmt, $objContractDocument, $strEffectiveBillingDate, $strPPCStartDate = NULL, $objDetails = NULL ) {
		$objContractPropertyPricing = new CContractPropertyPricing();
		$objContractPropertyPricing->setPrice( $fltRecurringAmt );
		$objContractPropertyPricing->setMonthlyChangeAmount( $fltMonthlyChangeAmt );
		$objContractPropertyPricing->setCid( $this->getCid() );
		$objContractPropertyPricing->setPropertyId( $this->getPropertyId() );
		$objContractPropertyPricing->setPsProductId( $this->getPsProductId() );
		$objContractPropertyPricing->setContractPropertyId( $this->getId() );
		$objContractPropertyPricing->setStartDate( $strEffectiveBillingDate );
		$objContractPropertyPricing->setEndDate( NULL );
		$objContractPropertyPricing->setContractDocumentId( ( true == valObj( $objContractDocument, 'CContractDocument' ) ) ? $objContractDocument->getId() : NULL );
		if( true == valStr( $strPPCStartDate ) ) {
			$objContractPropertyPricing->setDetailsField( [], $objDetails );
		}
		$objContractPropertyPricing->setIsReviewed( 'false' );
		return $objContractPropertyPricing;
	}

	public function createCompanyCharge( $objPsProduct ) {

		if( false == valObj( $objPsProduct, 'CPsProduct' ) ) {
			return false;
		}

		if( 0 >= $objPsProduct->getRecurringChargeCodeId() ) {
			return false;
		}

		if( true == is_null( $this->getRecurringAccountId() ) ) {
			return false;
		}

		$objCompanyCharge = new CCompanyCharge();
		$objCompanyCharge->setCid( $this->getCid() );
		$objCompanyCharge->setAccountId( $this->getRecurringAccountId() );
		$objCompanyCharge->setChargeCodeId( $objPsProduct->getRecurringChargeCodeId() );
		$objCompanyCharge->setChargeStartDate( 'NOW()' );
		$objCompanyCharge->setChargeAmount( $this->getMonthlyRecurringAmount() );
		$objCompanyCharge->setChargeMemo( '# Recurring charges for ' . $objPsProduct->getName() );
		$objCompanyCharge->setInternalNotes( '#CP ' . $this->getId() . '# Recurring charges for ' . $objPsProduct->getName() );

		return $objCompanyCharge;
	}

	public function createTransaction( $objPsProduct, $objAccount, $objClient, $intNextRecurringChargePostDate = NULL, $intMonthlyRecurringAmount = 0 ) {

		if( true == is_null( $intNextRecurringChargePostDate ) && ( false == valObj( $objPsProduct, 'CPsProduct' ) || false == valObj( $objAccount, 'CAccount' ) || ( false == valObj( $objClient, 'CClient' ) ) ) ) {
			return false;
		}

		$objTransaction = new CTransaction();
		$objTransaction->setCid( $this->getCid() );
		$objTransaction->setContractPropertyId( $this->getId() );
		$objTransaction->setPropertyId( $this->getPropertyId() );

		if( false == is_null( $intNextRecurringChargePostDate ) ) {
			$objTransaction->setEntityId( $this->getEntityId() );
			$objTransaction->setAccountId( $this->getRecurringAccountId() );
			$objTransaction->setChargeCodeId( $this->getChargeCodeId() );
			$objTransaction->setMemo( 'Recurring charges for ' . $this->getProductName() . ' for ' . $this->getPropertyName() );
			$objTransaction->setBundlePsProductId( $this->getBundlePsProductId() );
			$objTransaction->setTransactionDatetime( date( 'm/d/Y', $intNextRecurringChargePostDate ) );
			$objTransaction->setTransactionAmount( $intMonthlyRecurringAmount );
			$objTransaction->setPostMonth( date( 'm/d/Y', $intNextRecurringChargePostDate ) );
		} else {
			$objTransaction->setAccountId( $objAccount->getId() );
			$objTransaction->setCurrencyCode( $objAccount->getCurrencyCode() );
			$objTransaction->setEntityId( $objClient->getEntityId() );
			$objTransaction->setMemo( 'Transaction charges for ' . $objPsProduct->getName() );
		}

		return $objTransaction;
	}

	public function createContractPropertyTermination( $intContractTerminationReasonId ) {

		$objContractPropertyTermination = new CContractPropertyTermination();

		$objContractPropertyTermination->setCid( $this->getCid() );
		$objContractPropertyTermination->setContractId( $this->getContractId() );
		$objContractPropertyTermination->setPropertyId( $this->getPropertyId() );
		$objContractPropertyTermination->setBundlePsProductId( $this->getBundlePsProductId() );
		$objContractPropertyTermination->setPsProductId( $this->getPsProductId() );
		$objContractPropertyTermination->setContractTerminationRequestId( $this->getContractTerminationRequestId() );
		$objContractPropertyTermination->setContractTerminationReasonId( $intContractTerminationReasonId );
		$objContractPropertyTermination->setTerminationDate( $this->getTerminationDate() );
		$objContractPropertyTermination->setDeactivationDate( $this->getDeactivationDate() );

		return $objContractPropertyTermination;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valContractId() {
		$boolIsValid = true;

		if( true == is_null( $this->getContractId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_id', __( 'Contract is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', __( 'Ps product is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMonthlyRecurringAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getMonthlyRecurringAmount() ) || false == is_numeric( $this->m_fltMonthlyRecurringAmount ) || 15 < \Psi\CStringService::singleton()->strlen( $this->getMonthlyRecurringAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'monthly_recurring_amount', __( 'Valid monthly recurring amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valImplementationAmount() {

		$boolIsValid = true;

		if( true == is_null( $this->getImplementationAmount() ) || false == is_numeric( $this->m_fltImplementationAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'implementation_amount', __( 'Valid set-up amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMonthlyRecurringAmountLessThanOne() {
		$boolIsValid = true;

		// allow 0 or greater than 1 i.e. 0.5
		if( false == is_null( $this->getMonthlyRecurringAmount() ) && true == is_numeric( $this->m_fltMonthlyRecurringAmount ) && 0 == $this->m_fltMonthlyRecurringAmount ) {
			return $boolIsValid;
		}

		if( false == is_null( $this->getMonthlyRecurringAmount() ) && true == is_numeric( $this->m_fltMonthlyRecurringAmount ) && ( 0 != $this->m_fltMonthlyRecurringAmount && 1 > $this->m_fltMonthlyRecurringAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'monthly_recurring_amount', 'Monthly recurring amount can not be less than $1.' ) );
		}

		return $boolIsValid;
	}

	public function valSetUpAmountLessThanOne() {
		$boolIsValid = true;

		// allow 0 or greater than 1 i.e. 0.5
		if( false == is_null( $this->getImplementationAmount() ) && true == is_numeric( $this->m_fltImplementationAmount ) && 0 == $this->m_fltImplementationAmount ) {
			return $boolIsValid;
		}

		if( false == is_null( $this->getImplementationAmount() ) && true == is_numeric( $this->m_fltImplementationAmount ) && ( 0 != $this->m_fltImplementationAmount && 1 > $this->m_fltImplementationAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'implementation_amount', 'Set-up amount can not be less than $1.' ) );
		}

		return $boolIsValid;
	}

	public function valTerminationDate( $boolIsUnterminate = false ) {
		$boolIsValid = true;

		if( true == $boolIsUnterminate ) {

			if( false == is_null( $this->getTerminationDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_date', 'Termination date is not required.' ) );
			}

			return $boolIsValid;
		}

		if( true == is_null( $this->getTerminationDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_date', 'Valid termination date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyContractProductUniqueness( $objDatabase ) {
		$boolIsValid = true;

		if( true == valObj( $objDatabase, 'CDatabase' ) && 0 != CContractProperties::createService()->fetchConflictingContractPropertyByPsProductIdByPropertyIdByContractIdBySafeContractPropertyId( $this->getPsProductId(), $this->getPropertyId(), $this->getContractId(), $this->getId(), $objDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'The property / product combination set already exists on this contract.' ) ) );
		}

		return $boolIsValid;

	}

	public function valPropertyContractProductUniquenessByCid( $objDatabase ) {
		$boolIsValid = true;
		if( true == valObj( $objDatabase, 'CDatabase' ) && 0 != CContractProperties::createService()->fetchConflictingContractPropertyByPsProductIdByPropertyIdByCid( $this->getPsProductId(), $this->getPropertyId(), $this->getCid(), $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'The property / product combination set already exists.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valContractId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valMonthlyRecurringAmount();
				$boolIsValid &= $this->valImplementationAmount();
				// $boolIsValid &= $this->valMonthlyRecurringAmountLessThanOne();
				// $boolIsValid &= $this->valSetUpAmountLessThanOne();
				$boolIsValid &= $this->valPropertyContractProductUniqueness( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_termination':
				$boolIsValid &= $this->valTerminationDate();
				break;

			case 'validate_untermination':
				$boolIsValid &= $this->valTerminationDate( $boolIsUnterminate = true );
				break;

			case 'contract_renew':
				$boolIsValid &= $this->valMonthlyRecurringAmount();
				$boolIsValid &= $this->valImplementationAmount();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPsProductId();
				// $boolIsValid &= $this->valMonthlyRecurringAmountLessThanOne();
				// $boolIsValid &= $this->valSetUpAmountLessThanOne();
				break;

			case 'property_product_assignment':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valContractId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valMonthlyRecurringAmount();
				$boolIsValid &= $this->valImplementationAmount();
				$boolIsValid &= $this->valPropertyContractProductUniqueness( $objDatabase );
				$boolIsValid &= $this->valPropertyContractProductUniquenessByCid( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getUpdatedBy() ) || true == is_null( $this->getUpdatedOn() ) ) {
			$this->setUpdatedOn( 'NOW()' );
		}

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getUpdatedBy() ) || true == is_null( $this->getUpdatedOn() ) ) {
			$this->setUpdatedOn( 'NOW()' );
		}

		return parent::insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function createStandardQuarterTransactions( $strInvoiceBatchDateTime = NULL, $arrobjContractPropertyPricings = NULL ) {

		$arrobjTransactions 		 = [];
		$strBillingDate = ( false == is_null( $strInvoiceBatchDateTime ) ) ? date( 'm/d/Y', strtotime( $strInvoiceBatchDateTime ) ) : date( 'm/d/Y' );
		$strNextQuarterDate = date( 'm/d/Y', strtotime( '+3 months', strtotime( self::getStandardQuarterDate( $strBillingDate ) ) ) );

		$strNextLastPostedOnTimestamp 	= strtotime( date( 'm/1/Y', strtotime( $this->getBillingStartDate() ) ) );
		$strBillingStartDate 			= date( 'm/d/Y', strtotime( $this->getBillingStartDate() ) );

		if( false == is_null( $this->getLastPostedOn() ) ) {
			$strBillingStartDate 	= NULL;
			$strLastPostedOnTimeStamp = strtotime( $this->getLastPostedOn() );
			$strNextLastPostedOnTimestamp = strtotime( date( 'm/1/Y', strtotime( '+1 month', $strLastPostedOnTimeStamp ) ) );
		}

		while( $strNextLastPostedOnTimestamp < strtotime( $strNextQuarterDate ) ) {

			$objTransaction = $this->createTransaction( NULL, NULL, NULL, $strNextLastPostedOnTimestamp, $this->getMonthlyRecurringAmount() );

			$objTransaction->setPostMonth( self::getStandardQuarterDate( date( 'm/d/Y', $strNextLastPostedOnTimestamp ), false, $strBillingStartDate ) );

			if( true == valArr( $arrobjContractPropertyPricings ) ) {
				foreach( $arrobjContractPropertyPricings as $objContractPropertyPricing ) {
					if( strtotime( $objContractPropertyPricing->getStartDate() ) <= $strNextLastPostedOnTimestamp && ( strtotime( $objContractPropertyPricing->getEndDate() ) >= $strNextLastPostedOnTimestamp || true == is_null( $objContractPropertyPricing->getEndDate() ) ) ) {
						$objTransaction->setTransactionAmount( $objContractPropertyPricing->getPrice() );
					}
				}
			}

			$arrobjTransactions[] = $objTransaction;

			$this->setLastPostedOn( date( 'm/d/Y', $strNextLastPostedOnTimestamp ) );

			$strNextLastPostedOnTimestamp = strtotime( '+1 month', $strNextLastPostedOnTimestamp );
		}

		return $arrobjTransactions;
	}

	public function createTransactions( $strInvoiceBatchDateTime = NULL, $arrobjContractPropertyPricings = NULL ) {

		$arrobjTransactions 	= [];
		$strBillingDate = ( false == is_null( $strInvoiceBatchDateTime ) ) ? strtotime( date( 'm/1/Y', strtotime( $strInvoiceBatchDateTime ) ) ) : strtotime( date( 'm/1/Y' ) );

		// If the next date this client should be charged falls after the current date, return NULL.
		// OR If the current date is less than the begin date, return null.
		if( $strBillingDate < strtotime( \Psi\CStringService::singleton()->substr( $this->m_strBillingStartDate, 0, 11 ) ) ) {
			return [];
		}

		if( true == is_null( $this->getLastPostedOn() ) ) {

			$intStartDate 					= strtotime( date( 'm/1/Y', strtotime( $this->m_strBillingStartDate ) ) );
			$intNextRecurringChargePostDate	= $intStartDate;

			switch( $this->getFrequencyId() ) {
				case CFrequency::YEARLY:
					while( $intNextRecurringChargePostDate <= $strBillingDate ) {
						$intNextRecurringChargePostDate = strtotime( '+1 year', $intNextRecurringChargePostDate );
					}
					break;

				case CFrequency::SEMI_ANNUALLY:
					while( $intNextRecurringChargePostDate <= $strBillingDate ) {
						$intNextRecurringChargePostDate = strtotime( '+6 months', strtotime( self::getStandardSemiAnnualDate( $strBillingDate ) ) );
					}
					break;

				case CFrequency::QUARTERLY:
					while( $intNextRecurringChargePostDate <= $strBillingDate ) {
						$intNextRecurringChargePostDate = strtotime( '+3 months', strtotime( self::getStandardQuarterDate( $strBillingDate ) ) );
					}
					break;

				case CFrequency::MONTHLY:
					while( $intNextRecurringChargePostDate <= $strBillingDate ) {
						$intNextRecurringChargePostDate = strtotime( '+1 month', $intNextRecurringChargePostDate );
					}
					break;

				default:
					trigger_error( 'A recurring charge with no charge period was found.' );
					break;
			}

			$intNextRecurringChargePostDate = strtotime( '-1 month', $intNextRecurringChargePostDate );

		} else {

			$intNextRecurringChargePostDate	= strtotime( \Psi\CStringService::singleton()->substr( $this->m_strLastPostedOn, 0, 11 ) );

			$intStartDate           = strtotime( '+1 month', strtotime( substr( $this->m_strLastPostedOn, 0, 11 ) ) );

			switch( $this->getFrequencyId() ) {
				case CFrequency::YEARLY:
					while( $intNextRecurringChargePostDate < $strBillingDate ) {
						$intNextRecurringChargePostDate = strtotime( '+1 year', $intNextRecurringChargePostDate );
					}
					break;

				case CFrequency::SEMI_ANNUALLY:
					while( $intNextRecurringChargePostDate < $strBillingDate ) {
						$intNextRecurringChargePostDate = strtotime( '+6 months', $intNextRecurringChargePostDate );
					}
					break;

				case CFrequency::QUARTERLY:
					while( $intNextRecurringChargePostDate < $strBillingDate ) {
						$intNextRecurringChargePostDate = strtotime( '+3 months', $intNextRecurringChargePostDate );
					}
					break;

				case CFrequency::MONTHLY:
					while( $intNextRecurringChargePostDate < $strBillingDate ) {
						$intNextRecurringChargePostDate = strtotime( '+1 month', $intNextRecurringChargePostDate );
					}
					break;

				default:
					trigger_error( 'A recurring charge with no charge period was found.' );
					break;
			}
		}

		$strPostMonthDate = date( 'm/d/Y', $intStartDate );
		// Now the $intNextRecurringChargePostDate date is one period greater than the last_posted_on date.
		while( $intStartDate <= $intNextRecurringChargePostDate && ( false == isset( $this->m_strTerminationDate ) || ( true == isset( $this->m_strTerminationDate ) && $intStartDate <= strtotime( \Psi\CStringService::singleton()->substr( $this->m_strTerminationDate, 0, 11 ) ) ) ) ) {

			$objTransaction = $this->createTransaction( NULL, NULL, NULL, $intStartDate, $this->getMonthlyRecurringAmount() );

			$this->setLastPostedOn( date( 'm/d/Y', $intStartDate ) );
			switch( $this->getFrequencyId() ) {
				case CFrequency::YEARLY:
					$objTransaction->setPostMonth( $strPostMonthDate );
					break;

				case CFrequency::SEMI_ANNUALLY:
					$objTransaction->setPostMonth( self::getStandardSemiAnnualDate( date( 'm/d/Y', $intStartDate ), false, date( 'm/d/Y', strtotime( $this->getBillingStartDate() ) ) ) );
					break;

				case CFrequency::MONTHLY:
					break;

				case CFrequency::QUARTERLY:
					$objTransaction->setPostMonth( self::getStandardQuarterDate( date( 'm/d/Y', $intStartDate ), false, date( 'm/d/Y', strtotime( $this->getBillingStartDate() ) ) ) );
					break;

				default:
					trigger_error( 'A recurring charge with no charge period was found.' );
					break;
			}

			$intPrice = 0;
			if( true == valArr( $arrobjContractPropertyPricings ) ) {
				foreach( $arrobjContractPropertyPricings as $objContractPropertyPricing ) {
					if( false == valArr( $objContractPropertyPricing ) && strtotime( $objContractPropertyPricing->getStartDate() ) <= $intStartDate && ( strtotime( $objContractPropertyPricing->getEndDate() ) >= $intStartDate || true == is_null( $objContractPropertyPricing->getEndDate() ) ) ) {
						$objTransaction->setTransactionAmount( $objContractPropertyPricing->getPrice() );
						$intPrice = $objContractPropertyPricing->getPrice();
					}
				}
			}

			if( 0 != $intPrice ) {
				$arrobjTransactions[] = $objTransaction;
			}

			$intStartDate = strtotime( '+1 month', $intStartDate );

		}

		return $arrobjTransactions;
	}

	public static function getStandardQuarterDate( $strDate, $boolLastPostedOnNull = false, $strBillingStartDate = NULL ) {
		$strDate  = ( is_numeric( $strDate ) ) ? $strDate : strtotime( $strDate );
		$intMonth = date( 'm', $strDate );
		$intYear  = date( 'Y', $strDate );

		if( true == $boolLastPostedOnNull ) {
			return date( 'm/d/Y', $strDate );
		}

		if( $intMonth >= 1 && $intMonth <= 3 ) {
			$strStandardQuarterDate = date( 'm/d/Y', mktime( 0, 0, 0, 1, 1, $intYear ) );
		} elseif( $intMonth >= 4 && $intMonth <= 6 ) {
			$strStandardQuarterDate = date( 'm/d/Y', mktime( 0, 0, 0, 4, 1, $intYear ) );
		} elseif( $intMonth >= 7 && $intMonth <= 9 ) {
			$strStandardQuarterDate = date( 'm/d/Y', mktime( 0, 0, 0, 7, 1, $intYear ) );
		} else {
			$strStandardQuarterDate = date( 'm/d/Y', mktime( 0, 0, 0, 10, 1, $intYear ) );
		}

		if( true == valStr( $strBillingStartDate ) && strtotime( $strBillingStartDate ) > strtotime( $strStandardQuarterDate ) ) {
			return date( 'm/d/Y', strtotime( $strBillingStartDate ) );
		}
		return $strStandardQuarterDate;
	}

	public static function getStandardSemiAnnualDate( $strDate, $boolLastPostedOnNull = false, $strBillingStartDate = NULL ) {
		$strDate  = ( is_numeric( $strDate ) ) ? $strDate : strtotime( $strDate );
		$intMonth = date( 'm', $strDate );
		$intYear  = date( 'Y', $strDate );

		if( true == $boolLastPostedOnNull ) {
			return date( 'm/d/Y', $strDate );
		}

		if( $intMonth >= 1 && $intMonth <= 6 ) {
			$strStandardSemiAnnualDate = date( 'm/d/Y', mktime( 0, 0, 0, 1, 1, $intYear ) );
		} else {
			$strStandardSemiAnnualDate = date( 'm/d/Y', mktime( 0, 0, 0, 7, 1, $intYear ) );
		}

		if( true == valStr( $strBillingStartDate ) && strtotime( $strBillingStartDate ) > strtotime( $strStandardSemiAnnualDate ) ) {
			return date( 'm/d/Y', strtotime( $strBillingStartDate ) );
		}

		return $strStandardSemiAnnualDate;
	}

	// this function is used on view_contract_billing_preferences.tpl file...

	public static function getLastPostedOnDate( $strDate ) {

		$intDay		= date( 'd', strtotime( $strDate ) );
		$intMonth	= date( 'm', strtotime( $strDate ) );
		$intYear	= date( 'Y', strtotime( $strDate ) );

		$intMonth++;

		if( 12 < $intMonth ) {
			$intMonth = 1;
			$intYear++;
		}

		return date( 'm/d/Y', mktime( 0, 0, 0, $intMonth, $intDay, $intYear ) );
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchContract( $objAdminDatabase ) {
		return CContracts::createService()->fetchContractById( $this->getContractId(), $objAdminDatabase );
	}

	public function fetchContractProductOptions( $objAdminDatabase ) {
		return \Psi\Eos\Admin\CContractProductOptions::createService()->fetchContractProductOptionsByContractPropertyId( $this->getId(), $objAdminDatabase );
	}

	public function fetchPsProductOptions( $objDatabase ) {
		return CPsProductOptions::createService()->fetchPsProductOptionsByPsProductId( $this->getPsProductId(), $objDatabase );
	}

	public function getCalculatedNewBillingStartDateByEffectiveDate( $strNewEffectiveBillingDate ) {
		$strCalculatedNewBillingStartDate = NULL;
		$strNewEffectiveBillingDate = strtotime( $strNewEffectiveBillingDate );

		if( false == is_null( $this->getLastPostedOn() ) ) {
			$strLastPostedOn = strtotime( ( new DateTime( $this->getLastPostedOn() ) )->format( 'Y-m-01' ) );

			$intNumberOfMonth = 1;
			// if last posted on is not in sync with frequency
			$intMonthNumber				= date_parse_from_format( 'Y-m-d', date( 'Y-m-01', $strLastPostedOn ) )['month'];
			// sync non standard cycle last_posted on change ( Dec, March, Jun, Sept );
			while( ( CFrequency::QUARTERLY == $this->getFrequencyId() && false == in_array( $intMonthNumber, [ 12, 3, 6, 9 ] ) ) || ( CFrequency::SEMI_ANNUALLY == $this->getFrequencyId() && false == in_array( $intMonthNumber, [ 12, 6 ] ) ) ) {
				$strLastPostedOn = strtotime( '+1 month', $strLastPostedOn );
				$intMonthNumber				= date_parse_from_format( 'Y-m-d', date( 'Y-m-01', $strLastPostedOn ) )['month'];
			}

			switch( $this->getFrequencyId() ) {
				case CFrequency::MONTHLY:
					$strCalculatedNewBillingStartDate = $strNewEffectiveBillingDate;
					if( false == ( $strCalculatedNewBillingStartDate > $strLastPostedOn ) ) {
						// if effective date is less than last posted set start date as next month of last_posted_on
						$strCalculatedNewBillingStartDate = strtotime( '+1 month', $strLastPostedOn );
					}
					break;

				case CFrequency::QUARTERLY:
					$intNumberOfMonth = 3;
					break;

				case CFrequency::SEMI_ANNUALLY:
					$intNumberOfMonth = 6;
					break;

				case CFrequency::YEARLY:
					if( $strNewEffectiveBillingDate <= $strLastPostedOn ) {
						$strCalculatedNewBillingStartDate = strtotime( '+1 month', $strLastPostedOn );
					} else {
						$strCalculatedNewBillingStartDate = $strNewEffectiveBillingDate;
					}
					break;

				default:
					return false;
					break;
			}

			if( $this->getFrequencyId != CFrequency::MONTHLY && $this->getFrequencyId != CFrequency::YEARLY ) {
				// find next year billing date based on selected date and last posted on
				$strNextStartDate = strtotime( '+1 month', $strLastPostedOn );
				if( $strNewEffectiveBillingDate <= $strLastPostedOn ) {
					$strCalculatedNewBillingStartDate = $strNextStartDate;
				} else {
					$strCalculatedNewBillingStartDate = $strNextStartDate;
					$strNextEndDate = strtotime( '+' . ( $intNumberOfMonth - 1 ) . ' month', $strCalculatedNewBillingStartDate );
					while( $strCalculatedNewBillingStartDate < $strNewEffectiveBillingDate && false == ( ( $strCalculatedNewBillingStartDate < $strNextStartDate ) && $strCalculatedNewBillingStartDate <= $strNextEndDate ) ) {
						$strCalculatedNewBillingStartDate = strtotime( '+' . $intNumberOfMonth . ' month', $strCalculatedNewBillingStartDate );
						$strNextStartDate			= $strCalculatedNewBillingStartDate;
						$strNextEndDate				= strtotime( '+' . ( $intNumberOfMonth - 1 ) . ' month', $strCalculatedNewBillingStartDate );
					}
				}
			}
		} else {
			// in case of last_posted on NULL calculate next cycle based on effective billing date
			$intMonthNumber	= date_parse_from_format( 'Y-m-d', date( 'Y-m-01', $strNewEffectiveBillingDate ) )['month'];
			switch( $this->getFrequencyId() ) {
				case CFrequency::MONTHLY:
					$strCalculatedNewBillingStartDate = $strNewEffectiveBillingDate;
					break;

				case CFrequency::QUARTERLY:
					if( true == in_array( $intMonthNumber, CInvoice::$c_arrintQuarterMonthsIds ) ) {
						$strCalculatedNewBillingStartDate = $strNewEffectiveBillingDate;
					} else {
						$strCalculatedNewBillingStartDate = $strNewEffectiveBillingDate;
						while( false == in_array( $intMonthNumber, CInvoice::$c_arrintQuarterMonthsIds ) ) {
							$strCalculatedNewBillingStartDate = strtotime( '+1 month', $strCalculatedNewBillingStartDate );
							$intMonthNumber                   = date( 'm', $strCalculatedNewBillingStartDate );
						}
					}
					break;

				case CFrequency::SEMI_ANNUALLY:
					if( true == in_array( $intMonthNumber, CInvoice::$c_arrintSemiAnnualMonthsIds ) ) {
						$strCalculatedNewBillingStartDate = strtotime( $strNewEffectiveBillingDate );
					} else {
						$strCalculatedNewBillingStartDate = $strNewEffectiveBillingDate;
						while( false == in_array( $intMonthNumber, CInvoice::$c_arrintSemiAnnualMonthsIds ) ) {
							$strCalculatedNewBillingStartDate	= strtotime( '+1 month', $strCalculatedNewBillingStartDate );
							$intMonthNumber						= date( 'm', $strCalculatedNewBillingStartDate );
						}
					}
					break;

				case CFrequency::YEARLY:
					$strCalculatedNewBillingStartDate = $strNewEffectiveBillingDate;
					break;

				default:
					return false;
					break;
			}
		}

		return date( 'Y-m-01', $strCalculatedNewBillingStartDate );
	}

	public function createContractPropertyTerminationEventForEntrataCore( $arrmixContractTerminationEvents, $objConnectDatabase, $intDefaultDatabaseUserTypeId = CDatabaseUserType::PS_PROPERTYMANAGER ) {

		$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $this->getCid(), $objConnectDatabase, $intDefaultDatabaseUserTypeId );

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			// TODO: Undefined methods.
			$this->addSessionErrorMsg( 'Database Error: Failed to load client database.' );
			$this->displayApplicationErrorMessage();
			return;
		}

		$arrmixContractTerminationEvents = $this->createContractPropertyTerminationEventForAutomaticPostScheduleCharges( $this->getCid(), $this->getPropertyId(), $arrmixContractTerminationEvents, $objClientDatabase );

		return $arrmixContractTerminationEvents;
	}

	public function createContractPropertyTerminationEventForResidentPay( $arrmixContractTerminationEvents, $objConnectDatabase, $intDefaultDatabaseUserTypeId = CDatabaseUserType::PS_PROPERTYMANAGER ) {

		$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $this->getCid(), $objConnectDatabase, $intDefaultDatabaseUserTypeId );

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			// TODO: Undefined methods.
			$this->addSessionErrorMsg( __( 'Database Error: Failed to load client database.' ) );
			$this->displayApplicationErrorMessage();
			return;
		}

		$objClientDatabase->open();

		// Keeping log for Default Merchant Account
		$arrmixOldPropertyMerchantAccountSetting	= [];
		$objPropertyMerchantAccount				= CPropertyMerchantAccounts::createService()->fetchDefaultPropertyMerchantAccountByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );

		if( true == valObj( $objPropertyMerchantAccount, 'CPropertyMerchantAccount' ) ) {
			$arrmixOldPropertyMerchantAccountSetting['table_name']					= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_DEFAULT_MERCHANT_ACCOUNT];
			$arrmixOldPropertyMerchantAccountSetting['company_merchant_account_id']	= $objPropertyMerchantAccount->getCompanyMerchantAccountId();
			$arrmixOldPropertyMerchantAccountSetting['property_id']					= $objPropertyMerchantAccount->getPropertyId();
			$arrmixOldPropertyMerchantAccountSetting['disabled_by']					= $objPropertyMerchantAccount->getDisabledBy();
			$arrmixOldPropertyMerchantAccountSetting['disabled_on']					= $objPropertyMerchantAccount->getDisabledOn();
		}

		if( true == valArr( $arrmixOldPropertyMerchantAccountSetting ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::TERMINATE_DEFAULT_MERCHANT_ACCOUNT]['EVENT_PARAMETERS'] = json_encode( $arrmixOldPropertyMerchantAccountSetting );
		}

		// Keeping log for Charge Code Specific Merchant Accounts
		$arrobjPropertyMerchantAccounts	= CPropertyMerchantAccounts::createService()->fetchPropertyMerchantAccountsByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objClientDatabase, false, true );

		if( true == valArr( $arrobjPropertyMerchantAccounts ) ) {
			foreach( $arrobjPropertyMerchantAccounts AS $intPropertyMerchantAccountId => $objPropertyMerchantAccount ) {
				$arrintArCodeAccounts[$objPropertyMerchantAccount->getArCodeId()] = $objPropertyMerchantAccount->getCompanyMerchantAccountId();
			}

			if( true == valArr( $arrintArCodeAccounts ) ) {
				$arrmixOldMerchantAccoutDetails['accounts']   = $arrintArCodeAccounts;
				$arrmixOldMerchantAccoutDetails['table_name'] = CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_CHARGE_CODE_SPECIFIC_MERCHANT_ACCOUNT];
				$arrmixOldMerchantAccoutDetails['property_id'] = $objPropertyMerchantAccount->getPropertyId();
			}

			if( true == valArr( $arrmixOldMerchantAccoutDetails ) ) {
				$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::TERMINATE_CHARGE_CODE_SPECIFIC_MERCHANT_ACCOUNT]['EVENT_PARAMETERS'] = json_encode( $arrmixOldMerchantAccoutDetails );
			}
		}

		// Keeping log To Disable Resident Pay
		$arrmixOldPropertyPreferencesDetails			= [];
		$arrmixOldPropertyPreferencesDetails['value']	= '0';

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'DISABLE_RESIDENTPORTAL_RESIDENTPAY', $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			$arrmixOldPropertyPreferencesDetails['value'] = $objPropertyPreference->getValue();
		}

		$arrmixOldPropertyPreferencesDetails['table_name'] = CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_DISABLE_RESIDENT_PAY];
		$arrmixOldPropertyPreferencesDetails['property_id']	= $this->getPropertyId();
		$arrmixOldPropertyPreferencesDetails['key']			= CPropertySettingKey::$c_arrstrPropertySettingKey[CPropertySettingKey::DISABLE_RESIDENT_PAY];

		if( true == valArr( $arrmixOldPropertyPreferencesDetails ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::TERMINATE_DISABLE_RESIDENT_PAY]['EVENT_PARAMETERS'] = json_encode( $arrmixOldPropertyPreferencesDetails );
		}

		// Keeping log To Enable Rent Reminders
		$arrmixOldPropertyPreferencesDetails			= [];
		$arrmixOldPropertyPreferencesDetails['value']	= '0';

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'DONT_SEND_RENT_REMINDERS', $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			$arrmixOldPropertyPreferencesDetails['value'] = $objPropertyPreference->getValue();
		}

		$arrmixOldPropertyPreferencesDetails['table_name']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_ENABLE_RENT_REMINDER];
		$arrmixOldPropertyPreferencesDetails['property_id']	= $this->getPropertyId();
		$arrmixOldPropertyPreferencesDetails['key']			= CPropertySettingKey::$c_arrstrPropertySettingKey[CPropertySettingKey::DONT_SEND_RENT_REMINDERS];

		if( true == valArr( $arrmixOldPropertyPreferencesDetails ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::TERMINATE_ENABLE_RENT_REMINDER]['EVENT_PARAMETERS'] = json_encode( $arrmixOldPropertyPreferencesDetails );
		}

		$objClientDatabase->close();

		return $arrmixContractTerminationEvents;
	}

	public function createContractPropertyTerminationEventForAutomaticPostScheduleCharges( $intCid, $intPropertyId, $arrmixContractTerminationEvents, $objClientDatabase ) {

		$objClientDatabase->open();

		// Keeping log for Automatically Post Scheduled Charges
		$arrmixOldPropertyChargeSettings	= [];
		$objPropertyChargeSetting			= \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingDetailsByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase );

		if( true == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) {
			$arrmixOldPropertyChargeSettings['table_name']					= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_AUTOMATIC_POST_SCHEDULED_CHARGES];
			$arrmixOldPropertyChargeSettings['auto_post_scheduled_charges']	= $objPropertyChargeSetting->getAutoPostScheduledCharges();
			$arrmixOldPropertyChargeSettings['property_id']					= $objPropertyChargeSetting->getPropertyId();
		}

		if( true == valArr( $arrmixOldPropertyChargeSettings ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::TERMINATE_AUTOMATIC_POST_SCHEDULED_CHARGES]['EVENT_PARAMETERS'] = json_encode( $arrmixOldPropertyChargeSettings );
		}

		$objClientDatabase->close();

		return $arrmixContractTerminationEvents;

	}

	public function createContractPropertyTerminationEventForIlsPortal( $arrmixContractTerminationEvents, $objConnectDatabase, $intDefaultDatabaseUserTypeId = CDatabaseUserType::PS_PROPERTYMANAGER ) {

		$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $this->getCid(), $objConnectDatabase, $intDefaultDatabaseUserTypeId );

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			// TODO: Undefined methods.
			$this->addSessionErrorMsg( 'Database Error: Failed to load client database.' );
			$this->displayApplicationErrorMessage();
			return;
		}

		$objClientDatabase->open();

		// Keeping log for Disable ILS Portal
		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByCidByPropertyIdByKey( $this->getCid(), $this->getPropertyId(), 'DISABLE_ILSPORTAL', $objClientDatabase );

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			$arrmixOldPropertyPreferencesDetails['property_id']					= $objPropertyPreference->getPropertyId();
			$arrmixOldPropertyPreferencesDetails['key']							= $objPropertyPreference->getKey();
			$arrmixOldPropertyPreferencesDetails['value']						= $objPropertyPreference->getValue();
		} else {
			$arrmixOldPropertyPreferencesDetails['property_id']					= $this->getPropertyId();
			$arrmixOldPropertyPreferencesDetails['key']							= CPropertySettingKey::$c_arrstrPropertySettingKey[CPropertySettingKey::DISABLE_ILSPORTAL];
			$arrmixOldPropertyPreferencesDetails['value']						= '0';
		}

		$arrmixOldPropertyPreferencesDetails['table_name'] = CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_DISABLE_ILS_PORTAL];

		if( true == valArr( $arrmixOldPropertyPreferencesDetails ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::TERMINATE_DISABLE_ILS_PORTAL]['EVENT_PARAMETERS'] = json_encode( $arrmixOldPropertyPreferencesDetails );
		}

		$objClientDatabase->close();

		return $arrmixContractTerminationEvents;
	}

	public function createContractPropertyTerminationEventForLeasingCenter( $arrmixContractTerminationEvents, $objConnectDatabase, $objVoidDatabase, $intDefaultDatabaseUserTypeId = CDatabaseUserType::PS_PROPERTYMANAGER ) {

		$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $this->getCid(), $objConnectDatabase, $intDefaultDatabaseUserTypeId );

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			// TODO: Undefined methods.
			$this->addSessionErrorMsg( __( 'Database Error: Failed to load client database.' ) );
			$this->displayApplicationErrorMessage();
			return;
		}

		$objClientDatabase->open();
		$objVoidDatabase->open();

		// Keeping log for Disable Leasing center for setting Lead Chats - Redirect to Leasing Center
		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByCidByPropertyIdByKey( $this->getCid(), $this->getPropertyId(), 'LEAD_CHAT_ROUTING_CONDITION', $objClientDatabase );

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			$arrmixOldLeasingCenterDetails['LEAD_CHAT_ROUTING_CONDITION']['key']		= $objPropertyPreference->getKey();
			$arrmixOldLeasingCenterDetails['LEAD_CHAT_ROUTING_CONDITION']['value']		= $objPropertyPreference->getValue();
		} else {
			$arrmixOldLeasingCenterDetails['LEAD_CHAT_ROUTING_CONDITION']['key']		= CPropertySettingKey::$c_arrstrPropertySettingKey[CPropertySettingKey::LEAD_CHAT_ROUTING_CONDITION];
			$arrmixOldLeasingCenterDetails['LEAD_CHAT_ROUTING_CONDITION']['value']		= 'NONE';
		}

		$arrmixOldLeasingCenterDetails['LEAD_CHAT_ROUTING_CONDITION']['table_name']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_DISABLE_LEASING_CENTER][CPropertySettingKey::LEAD_CHAT_ROUTING_CONDITION];

		$objPropertyCallSetting = CPropertyCallSettings::createService()->fetchPropertyCallSettingByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objVoidDatabase );

		if( true == valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$arrmixOldLeasingCenterDetails[$this->getPropertyId()]['lead_routing_condition']					= $objPropertyCallSetting->getLeadRoutingCondition();
			$arrmixOldLeasingCenterDetails[$this->getPropertyId()]['maintenance_routing_condition']				= $objPropertyCallSetting->getMaintenanceRoutingCondition();
			$arrmixOldLeasingCenterDetails[$this->getPropertyId()]['maintenance_emergency_routing_condition']	= $objPropertyCallSetting->getMaintenanceEmergencyRoutingCondition();
		} else {
			$arrmixOldLeasingCenterDetails[$this->getPropertyId()]['lead_routing_condition']					= 'NONE';
			$arrmixOldLeasingCenterDetails[$this->getPropertyId()]['maintenance_routing_condition']				= 'NONE';
			$arrmixOldLeasingCenterDetails[$this->getPropertyId()]['maintenance_emergency_routing_condition']	= 'NONE';
		}

		$arrmixOldLeasingCenterDetails['property_id']							= $this->getPropertyId();
		$arrmixOldLeasingCenterDetails[$this->getPropertyId()]['table_name']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_DISABLE_LEASING_CENTER][CPropertySettingKey::PROPERTY_CALL_SETTINGS_LEAD_ROUTING_CONDITION];

		if( true == valArr( $arrmixOldLeasingCenterDetails ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::TERMINATE_DISABLE_LEASING_CENTER]['EVENT_PARAMETERS'] = json_encode( $arrmixOldLeasingCenterDetails );
		}

		$objVoidDatabase->close();
		$objClientDatabase->close();

		return $arrmixContractTerminationEvents;
	}

	public function createDefaultContractPropertyTerminationEvent( $arrmixContractTerminationEvents, $arrmixDeactivateWebsiteIds, $objClientDatabase ) {

		$objClientDatabase->open();

		// Keeping log for Document Template.
		$arrobjPropertyGroupDocuments = CPropertyGroupDocuments::createService()->fetchPropertyGroupDocumentTemplateDetailsByPropertyIdByCidByDocumentTypeIdByDocumentSubTypeId( $this->getPropertyId(), $this->getCid(), CDocumentType::MERGE_DOCUMENT, CDocumentSubType::DOCUMENT_TEMPLATE, CDocumentAssociationType::DOCUMENT_TEMPLATE, $objClientDatabase );

		if( true == valArr( $arrobjPropertyGroupDocuments ) ) {
			$arrintDocumentIds = array_keys( rekeyObjects( 'DocumentId', $arrobjPropertyGroupDocuments ) );
			if( true == valArr( $arrintDocumentIds ) ) {
				$arrintAssociateDocumentTemplateIds = CPropertyGroupDocuments::createService()->fetchPropertyCountByDocumentIds( $arrintDocumentIds, $objClientDatabase );
				if( true == valArr( $arrintAssociateDocumentTemplateIds ) ) {

					$arrintAssociateDocumentTemplateIds = rekeyArray( 'document_id', $arrintAssociateDocumentTemplateIds );
					$arrintDocumentIds = [];

					foreach( $arrintAssociateDocumentTemplateIds As $intDocumentId => $arrintDocumentDetails ) {
						$arrintDocumentIds[$intDocumentId] = $arrintDocumentDetails['count'];
					}

					$arrmixOldPropertyGroupDocuments['document_ids'] = $arrintDocumentIds;
				}
			}

			$arrmixOldPropertyGroupDocuments['table_name']['remove_association']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_DOCUMENT_TEMPLATE]['remove_association'];
			$arrmixOldPropertyGroupDocuments['table_name']['deactivate_template']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_DOCUMENT_TEMPLATE]['deactivate_template'];
			$arrmixOldPropertyGroupDocuments['property_id']							= $this->getPropertyId();

			if( true == valArr( $arrmixOldPropertyGroupDocuments ) ) {
				$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::TERMINATE_DOCUMENT_TEMPLATE]['EVENT_PARAMETERS'] = json_encode( $arrmixOldPropertyGroupDocuments );
			}
		}

		// Keeping log for Document Packets.
		$arrobjPropertyGroupDocuments = CPropertyGroupDocuments::createService()->fetchPropertyGroupDocumentTemplateDetailsByPropertyIdByCidByDocumentTypeIdByDocumentSubTypeId( $this->getPropertyId(), $this->getCid(), CDocumentType::PACKET, NULL, CDocumentAssociationType::PACKET, $objClientDatabase );

		if( true == valArr( $arrobjPropertyGroupDocuments ) ) {
			$arrintDocumentIds = array_keys( rekeyObjects( 'DocumentId', $arrobjPropertyGroupDocuments ) );
			if( true == valArr( $arrintDocumentIds ) ) {
				$arrintAssociateDocumentTemplateIds = CPropertyGroupDocuments::createService()->fetchPropertyCountByDocumentIds( $arrintDocumentIds, $objClientDatabase );
				if( true == valArr( $arrintAssociateDocumentTemplateIds ) ) {

					$arrintAssociateDocumentTemplateIds = rekeyArray( 'document_id', $arrintAssociateDocumentTemplateIds );
					$arrintDocumentIds = [];

					foreach( $arrintAssociateDocumentTemplateIds As $intDocumentId => $arrintDocumentDetails ) {
						$arrintDocumentIds[$intDocumentId] = $arrintDocumentDetails['count'];
					}

					$arrmixOldPropertyGroupDocuments['document_ids'] = $arrintDocumentIds;
				}
			}

			$arrmixOldPropertyGroupDocuments['table_name']['remove_association']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_DOCUMENT_PACKETS]['remove_association'];
			$arrmixOldPropertyGroupDocuments['table_name']['deactivate_template']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_DOCUMENT_PACKETS]['deactivate_template'];
			$arrmixOldPropertyGroupDocuments['property_id']							= $this->getPropertyId();

			if( true == valArr( $arrmixOldPropertyGroupDocuments ) ) {
				$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::TERMINATE_DOCUMENT_PACKETS]['EVENT_PARAMETERS'] = json_encode( $arrmixOldPropertyGroupDocuments );
			}
		}

		// Keeping log for terminating properties of website template
		$arrobjWebsiteProperties = \Psi\Eos\Entrata\CWebsiteProperties::createService()->fetchWebsitePropertiesByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objClientDatabase );

		if( true == valArr( $arrobjWebsiteProperties ) ) {

			$arrmixWebsiteLogs = [];
			foreach( $arrobjWebsiteProperties AS $intWebsitePropertyId => $objWebsiteProperties ) {
				$arrmixWebsiteLogs['website_properties'][$objWebsiteProperties->getWebsiteId()]['PP'] = $objWebsiteProperties->getHideOnProspectPortal();
				$arrmixWebsiteLogs['website_properties'][$objWebsiteProperties->getWebsiteId()]['RP'] = $objWebsiteProperties->getHideOnResidentPortal();
				$arrmixWebsiteLogs['website_properties'][$objWebsiteProperties->getWebsiteId()]['JP'] = $objWebsiteProperties->getHideOnJobPortal();
				$arrmixWebsiteLogs['website_properties'][$objWebsiteProperties->getWebsiteId()]['MP'] = $objWebsiteProperties->getHideOnMobilePortal();
				$arrmixWebsiteLogs['website_properties'][$objWebsiteProperties->getWebsiteId()]['MPCP'] = $objWebsiteProperties->getHideOnMobilePortalCorporatePage();
				$arrmixWebsiteLogs['website_properties'][$objWebsiteProperties->getWebsiteId()]['info_button'] = $objWebsiteProperties->getHideMoreInfoButton();
				$arrmixWebsiteLogs['website_properties'][$objWebsiteProperties->getWebsiteId()]['SAFO'] = $objWebsiteProperties->getShowApplicationFeeOption();
			}

			$arrobjAllWebsiteDomains = \Psi\Eos\Entrata\CWebsiteDomains::createService()->fetchWebsitesDomainsByPropertyIdsByWebsiteIdByCid( [ $this->getPropertyId() ], $this->getCid(), $objClientDatabase );
			if( true == valArr( $arrobjAllWebsiteDomains ) ) {

				foreach( $arrobjAllWebsiteDomains AS $intWebsiteDomainId => $objWebsiteDomain ) {
					$arrmixWebsiteLogs['website_domains'][$objWebsiteDomain->getWebsiteId()][$intWebsiteDomainId] = $objWebsiteDomain->getWebsiteDomain();
				}
			}
		}

		$arrmixWebsiteLogs['table_name']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::TERMINATE_PROPERTY_WEBSITES]['website'];
		$arrmixWebsiteLogs['property_id']	= $this->getPropertyId();

		if( true == valArr( $arrmixWebsiteLogs ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::TERMINATE_PROPERTY_WEBSITES]['EVENT_PARAMETERS'] = json_encode( $arrmixWebsiteLogs );
		}

		// Keeping log for deactivating websites.
		$arrmixWebsiteLogs = [];
		if( false == empty( $arrmixDeactivateWebsiteIds[$this->getPropertyId()]['website_id'] ) ) {
			$arrintDeactivateWebsiteIds = explode( ',', $arrmixDeactivateWebsiteIds[$this->getPropertyId()]['website_id'] );

			if( true == valArr( $arrintDeactivateWebsiteIds ) ) {
				$arrmixWebsiteLogs['table_name']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::DEACTIVATE_WEBSITES];
				$arrmixWebsiteLogs['property_id']	= $this->getPropertyId();
				$arrmixWebsiteLogs['website_ids']	= $arrintDeactivateWebsiteIds;
			}
		}

		if( true == valArr( $arrmixWebsiteLogs ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::DEACTIVATE_WEBSITES]['EVENT_PARAMETERS'] = json_encode( $arrmixWebsiteLogs );
		}

		// Keeping log to disassociate Rental Applications Documents Settings
		$arrobjCompanyApplications = CCompanyApplications::createService()->fetchCompanyApplicationsByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objClientDatabase );

		if( true == valArr( $arrobjCompanyApplications ) && true == valArr( array_keys( $arrobjCompanyApplications ) ) ) {
			$arrmixRentalApplicationLogs['ids']			= array_keys( $arrobjCompanyApplications );
			$arrmixRentalApplicationLogs['table_name']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::DISASSOCIATE_RENTAL_APPLICATION_DOCUMENT];
			$arrmixRentalApplicationLogs['property_id']	= $this->getPropertyId();
		}

		if( true == valArr( $arrmixRentalApplicationLogs ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::DISASSOCIATE_RENTAL_APPLICATION_DOCUMENT]['EVENT_PARAMETERS'] = json_encode( $arrmixRentalApplicationLogs );
		}

		// Keeping log to disassociate Property integration.
		$arrobjPropertyIntegrationDatabases = CPropertyIntegrationDatabases::createService()->fetchPropertyIntegrationDatabasesByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objClientDatabase );

		if( true == valArr( $arrobjPropertyIntegrationDatabases ) ) {

			$arrobjPropertyIntegrationDatabases = rekeyObjects( 'IntegrationDatabaseId', $arrobjPropertyIntegrationDatabases );

			if( true == valArr( $arrobjPropertyIntegrationDatabases ) ) {
				$objProperty = \Psi\Eos\Admin\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );

				if( true == valObj( $objProperty, 'CProperty' ) ) {
					$arrmixPropertyIntegrationLogs['remote_primary_key'] = $objProperty->getRemotePrimaryKey();
					$arrmixPropertyIntegrationLogs['lookup_code']        = $objProperty->getLookupCode();
				}

				$arrmixPropertyIntegrationLogs['database_id']	= array_keys( $arrobjPropertyIntegrationDatabases );
				$arrmixPropertyIntegrationLogs['table_name']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::DISASSOCIATE_PROPERTY_INTEGRATION]['database_ids'];
				$arrmixPropertyIntegrationLogs['property_id']	= $this->getPropertyId();
			}
		}

		if( true == valArr( $arrmixPropertyIntegrationLogs ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::DISASSOCIATE_PROPERTY_INTEGRATION]['EVENT_PARAMETERS'] = json_encode( $arrmixPropertyIntegrationLogs );
		}

		// Keeping log to remove recurring payments.
		$arrobjScheduledPayments = CScheduledPayments::createService()->fetchScheduledPaymentsbyCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objClientDatabase );

		if( true == valArr( $arrobjScheduledPayments ) ) {
			$arrmixScheduledPaymentDetails['ids']			= array_keys( $arrobjScheduledPayments );
			$arrmixScheduledPaymentDetails['table_name']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::REMOVE_RESIDENT_RECURRING_PAYMENTS];
			$arrmixScheduledPaymentDetails['property_id']	= $this->getPropertyId();
		}

		if( true == valArr( $arrmixScheduledPaymentDetails ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::REMOVE_RESIDENT_RECURRING_PAYMENTS]['EVENT_PARAMETERS'] = json_encode( $arrmixScheduledPaymentDetails );
		}

		// Keeping log to disable property.
		if( true == valId( $this->getPropertyId() ) ) {
			$arrmixPropertyDetails['table_name']	= CContractTerminationEventType::$c_arrstrTerminationEventsTables[CContractTerminationEventType::REMOVE_RESIDENT_RECURRING_PAYMENTS];
			$arrmixPropertyDetails['property_id']	= $this->getPropertyId();
		}

		if( true == valArr( $arrmixPropertyDetails ) ) {
			$arrmixContractTerminationEvents[$this->getId()][CContractTerminationEventType::DISABLE_PROPERTY]['EVENT_PARAMETERS'] = json_encode( $arrmixPropertyDetails );
		}

		$objClientDatabase->close();

		return $arrmixContractTerminationEvents;
	}

	public function setMinimumBillingStartDate( $strNewBillingDate, $strOldBillingDate, $strSecondLastBillingDate ) {
		$arrstrBillingDates = array_filter( array( $strNewBillingDate, $strOldBillingDate, $strSecondLastBillingDate ) );
		$arrstrConvertedBillingDates = array_filter( array( strtotime( $strNewBillingDate ), strtotime( $strOldBillingDate ), strtotime( $strSecondLastBillingDate ) ) );
		\Psi\CStringService::singleton()->asort( $arrstrConvertedBillingDates );
		$strOldestDate = current( $arrstrConvertedBillingDates );
		foreach( $arrstrBillingDates as $strDate ) {
			if( $strOldestDate == strtotime( $strDate ) ) {
				$strContractPropertyBillingDate = $strDate;
				break;
			}
		}
		$this->setBillingStartDate( $strContractPropertyBillingDate );
	}

	public function applyContractPropertyPricing( $fltNewPrice, $intUserId, $objDatabase, $strTerminationDate = NULL, $arrmixDiscountDetails = NULL ) {

		$objInsertContractPropertyPricing		= NULL;

		$arrobjInsertContractPropertyPricings	= [];
		$arrobjUpdateContractPropertyPricings	= [];
		$arrobjDeleteContractPropertyPricings	= [];

		$arrstrFieldsToUpdate = [
			'start_date',
			'end_date',
			'price',
			'monthly_change_amount',
			'is_discount_update',
			'details'
		];

		$fltNewPrice = round( $fltNewPrice, 2 );

		$objLatestContractPropertyPricing		= CContractPropertyPricings::createService()->fetchLatestContractPropertyPricingByContractPropertyId( $this->getId(), $objDatabase );
		$objSecondLastContractPropertyPricing	= CContractPropertyPricings::createService()->fetchSecondLastContractPropertyPricingByContractPropertyId( $this->getId(), $objDatabase );

		if( valObj( $objLatestContractPropertyPricing, 'CContractPropertyPricing' ) ) {

			$fltCurrentPrice = $objLatestContractPropertyPricing->getPrice();

			if( $fltNewPrice == $fltCurrentPrice ) {
				return true;
			}

			$objStartDate		= new DateTime( $objLatestContractPropertyPricing->getStartDate() );
			$objLastDate		= new DateTime( date( 'Y-m-t' ) );

			$strNewStartDate = date( 'Y-m-01', strtotime( '+1 month' ) );

			if( $strTerminationDate ) {
				$strNewStartDate = date( 'Y-m-01', strtotime( '+1 month', strtotime( $strTerminationDate ) ) );
			}

			$strEndDate = date( 'Y-m-t', strtotime( '-1 month', strtotime( $strNewStartDate ) ) );

			if( 0 == $objLatestContractPropertyPricing->getPrice() || valStr( $this->getLastPostedOn() ) || ( !valId( $this->getPackageProductId() ) && valId( $this->getBundlePsProductId() ) ) ) {

				if( valStr( $this->getLastPostedOn() ) ) {
					$strNewStartDate	= date( 'Y-m-01', strtotime( '+1 month', strtotime( $this->getLastPostedOn() ) ) );
					$strEndDate			= date( 'Y-m-t', strtotime( '-1 month', strtotime( $strNewStartDate ) ) );
					$objLastDate		= new DateTime( $this->getLastPostedOn() );
				}

				if( $objStartDate > $objLastDate ) {

					$objLatestContractPropertyPricing->setStartDate( $strNewStartDate );
					$objLatestContractPropertyPricing->setEndDate( NULL );
					$objLatestContractPropertyPricing->setPrice( $fltNewPrice );

					$fltMonthlyChangeAmount = $fltNewPrice;

					if( valObj( $objSecondLastContractPropertyPricing, 'CContractPropertyPricing' ) ) {
						$fltMonthlyChangeAmount = round( $fltNewPrice - $objSecondLastContractPropertyPricing->getPrice(), 2 );

						if( $objSecondLastContractPropertyPricing->getPrice() == $fltNewPrice ) {
							$objSecondLastContractPropertyPricing->setEndDate( NULL );
							$arrobjDeleteContractPropertyPricings[] = $objLatestContractPropertyPricing;
						} else {
							$objSecondLastContractPropertyPricing->setEndDate( $strEndDate );
						}

						$arrobjUpdateContractPropertyPricings[] = $objSecondLastContractPropertyPricing;
					}

					$objLatestContractPropertyPricing->setMonthlyChangeAmount( $fltMonthlyChangeAmount );

					$arrmixDetails = [ 'is_reviewed' => 'false' ];

					if( valArr( $arrmixDiscountDetails ) ) {
						$objLatestContractPropertyPricing->setIsDiscountUpdate( $arrmixDiscountDetails['is_discount_update'] );
						$arrmixDetails['discount_note'] = $arrmixDiscountDetails['discount_note'];
					}

					$objLatestContractPropertyPricing->setDetails( ( object ) array_merge( ( array ) $objLatestContractPropertyPricing->getDetails(), $arrmixDetails ) );

					$arrobjUpdateContractPropertyPricings[] = $objLatestContractPropertyPricing;

				} else {

					$objLatestContractPropertyPricing->setEndDate( $strEndDate );

					$objInsertContractPropertyPricing = new CContractPropertyPricing();
					$objInsertContractPropertyPricing->setCid( $objLatestContractPropertyPricing->getCid() );
					$objInsertContractPropertyPricing->setPropertyId( $objLatestContractPropertyPricing->getPropertyId() );
					$objInsertContractPropertyPricing->setPsProductId( $objLatestContractPropertyPricing->getPsProductId() );
					$objInsertContractPropertyPricing->setContractPropertyId( $objLatestContractPropertyPricing->getContractPropertyId() );
					$objInsertContractPropertyPricing->setStartDate( $strNewStartDate );
					$objInsertContractPropertyPricing->setEndDate( NULL );
					$objInsertContractPropertyPricing->setPrice( $fltNewPrice );
					$objInsertContractPropertyPricing->setMonthlyChangeAmount( round( $objInsertContractPropertyPricing->getPrice() - $objLatestContractPropertyPricing->getPrice(), 2 ) );

					$arrmixDetails = [ 'is_reviewed' => 'false' ];

					if( valArr( $arrmixDiscountDetails ) ) {
						$objInsertContractPropertyPricing->setIsDiscountUpdate( $arrmixDiscountDetails['is_discount_update'] );
						$arrmixDetails['discount_note'] = $arrmixDiscountDetails['discount_note'];
					}

					$objInsertContractPropertyPricing->setDetails( ( object ) array_merge( ( array ) $objLatestContractPropertyPricing->getDetails(), $arrmixDetails ) );

					$arrobjInsertContractPropertyPricings[] = $objInsertContractPropertyPricing;
					$arrobjUpdateContractPropertyPricings[] = $objLatestContractPropertyPricing;
				}

			} else {
				$objLatestContractPropertyPricing->setPrice( $fltNewPrice );

				$fltMonthlyChangeAmount = $fltNewPrice;

				if( valObj( $objSecondLastContractPropertyPricing, 'CContractPropertyPricing' ) ) {
					$fltMonthlyChangeAmount = round( $fltNewPrice - $objSecondLastContractPropertyPricing->getPrice(), 2 );
					$objSecondLastContractPropertyPricing->setEndDate( $strEndDate );
				}

				$objLatestContractPropertyPricing->setMonthlyChangeAmount( $fltMonthlyChangeAmount );

				$arrmixDetails = [ 'is_reviewed' => 'false' ];

				if( valArr( $arrmixDiscountDetails ) ) {
					$objLatestContractPropertyPricing->setIsDiscountUpdate( $arrmixDiscountDetails['is_discount_update'] );
					$arrmixDetails['discount_note'] = $arrmixDiscountDetails['discount_note'];
				}

				$objLatestContractPropertyPricing->setDetails( ( object ) array_merge( ( array ) $objLatestContractPropertyPricing->getDetails(), $arrmixDetails ) );

				$arrobjUpdateContractPropertyPricings[] = $objLatestContractPropertyPricing;
			}

		} else {
			$this->setMonthlyRecurringAmount( $fltNewPrice );
		}

		if( valArr( $arrobjInsertContractPropertyPricings ) && !CContractPropertyPricings::createService()->bulkInsert( $arrobjInsertContractPropertyPricings, $intUserId, $objDatabase ) ) {
			return false;
		}

		if( valArr( $arrobjUpdateContractPropertyPricings ) && !CContractPropertyPricings::createService()->bulkUpdate( $arrobjUpdateContractPropertyPricings, $arrstrFieldsToUpdate, $intUserId, $objDatabase ) ) {
			return false;
		}

		if( valArr( $arrobjDeleteContractPropertyPricings ) && !CContractPropertyPricings::createService()->bulkDelete( $arrobjDeleteContractPropertyPricings, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function applyPackageProductDiscount( $objDatabase, $boolTerminate = true ) {

		if( !valId( $this->getPackageProductId() ) ) {
			return false;
		}

		$boolBundle = $this->getBundlePsProductId() ? true : false;

		$arrmixDiscountDetails = ( array ) CContractProperties::createService()->fetchDiscountDetailsByContractPropertyId( $this->getId(), $objDatabase, $boolBundle );

		$arrintDependentProductIds = array_unique( array_filter( array_keys( rekeyArray( 'dependent_product_id', $arrmixDiscountDetails ) ) ) );

		// No products are dependent on this contract property.
		if( !valArr( $arrintDependentProductIds ) ) {
			return false;
		}

		$fltDiscountSum		= 0;
		$fltFinalDiscount	= 0;

		$arrobjDependentContractProperties = ( array ) CContractProperties::createService()->fetchActiveContractPropertiesByPropertyIdByPackageProductIdByPsProductIds( $this->getPropertyId(), $this->getPackageProductId(), $arrintDependentProductIds, $objDatabase );

		$arrmixRekeyedDiscounts = rekeyArray( 'dependent_product_id', $arrmixDiscountDetails );

		foreach( $arrobjDependentContractProperties as $objDependentContractProperty ) {
			if( isset( $arrmixRekeyedDiscounts[$objDependentContractProperty->getPsProductId()] ) ) {
				$fltDiscountSum += $arrmixRekeyedDiscounts[$objDependentContractProperty->getPsProductId()]['recurring_discount_percentage'];
			}
		}

		if( $fltDiscountSum <= 100 ) {
			// If the addition is greater than 100 then it's invalid discount. Consider 0 discount in that case.
			$fltFinalDiscount = $fltDiscountSum;
		}

		$objPsProduct	= CPsProducts::createService()->fetchPsProductById( $this->getPsProductId(), $objDatabase );
		$objProperty	= CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( !valObj( $objPsProduct, 'CPsProduct' ) || !valObj( $objProperty, 'CProperty' ) ) {
			return false;
		}

		// Fetch payment type, min/max units details stored in contract_products.
		$arrmixContractProductDetails = [];

		$objContractProduct = CContractProducts::createService()->fetchPackageContractProductByContractPropertyId( $this->getId(), $objDatabase );

		if( valObj( $objContractProduct, 'CContractProduct' ) ) {
			$arrmixContractProductDetails = json_decode( $objContractProduct->getDetails(), true );
		}

		$intPaymentTypeId	= isset( $arrmixContractProductDetails['payment_type_id'] ) ? $arrmixContractProductDetails['payment_type_id'] : CContractProduct::PAYMENT_TYPE_PER_UNIT;
		$intMinUnits		= isset( $arrmixContractProductDetails['min_units'] ) ? $arrmixContractProductDetails['min_units'] : 1;
		$intMaxUnits		= isset( $arrmixContractProductDetails['max_units'] ) ? $arrmixContractProductDetails['max_units'] : 1;

		// Decide number of units based on the payment type of the line item / bundle.
		switch( $intPaymentTypeId ) {

			case CContractProduct::PAYMENT_TYPE_PER_PROPERTY:
				$intNumberOfUnits = 1;
				break;

			case CContractProduct::PAYMENT_TYPE_MIN_MAX:
				$intNumberOfUnits = $objProperty->getNumberOfUnits() <= $intMinUnits ? $intMinUnits : ( $objProperty->getNumberOfUnits() >= $intMaxUnits ? $intMaxUnits : $objProperty->getNumberOfUnits() );
				break;

			default:
				$intNumberOfUnits = $objProperty->getNumberOfUnits();
		}

		// Recurring amount without discount
		$fltPlainRecurringAmount = $intNumberOfUnits * $objPsProduct->getUnitMonthlyRecurring();

		// New recurring amount = Plain recurring amount - discounted recurring amount.
		$fltNewRecurringAmount = $fltPlainRecurringAmount - ( $fltPlainRecurringAmount * $fltFinalDiscount / 100 );

		$objPsProduct = CPsProducts::createService()->fetchPsProductById( $this->getPsProductId(), $objDatabase );

		$strDiscountNote = sprintf( '%f%% discount applied after %s of product %s.', $fltFinalDiscount, ( $boolTerminate ? 'termination' : 'reactivation' ), $objPsProduct->getName() );

		$arrmixDiscountDetails = [
			'is_discount_update' => true,
			'discount_note' => $strDiscountNote
		];

		$strTerminationDate = NULL;

		if( valId( $this->getContractTerminationRequestId() ) ) {
			$objContractTerminationRequest = CContractTerminationRequests::createService()->fetchContractTerminationRequestById( $this->getContractTerminationRequestId(), $objDatabase );

			if( valObj( $objContractTerminationRequest, 'CContractTerminationRequest' ) ) {
				$strTerminationDate = $objContractTerminationRequest->getTerminationDate();
			}
		}

		if( !$this->applyContractPropertyPricing( $fltNewRecurringAmount, CUser::ID_SYSTEM, $objDatabase, $strTerminationDate, $arrmixDiscountDetails ) ) {
			return false;
		}

		if( !valId( $this->getOriginalContractPropertyId() ) ) {
			$this->setMonthlyChangeAmount( $fltNewRecurringAmount );
		}

		return true;
	}

	public static function applyPackageProductDataToProperty( $intPropertyId, $intPackageProductId, $arrobjContractProducts, $objContract ) {

		foreach( $arrobjContractProducts as $objContractProduct ) {
			$objContractProperty = $objContract->createContractProperty();
			$objContractProperty->setPropertyId( $intPropertyId );
			$objContractProperty->setPsProductId( $objContractProduct->getPsProductId() );
			if( true == valId( $intPackageProductId ) ) {
				$objContractProperty->setPackageProductId( $intPackageProductId );
			} else {
				$objContractProperty->setPackageProductId( $objContractProduct->getPackageProductId() );
			}
			if( true == valId( $objContractProduct->getBundlePsProductId() ) ) {
				$objContractProperty->setBundlePsProductId( $objContractProduct->getBundlePsProductId() );
			}

			if( CContractStatusType::CONTRACT_APPROVED == $objContract->getContractStatusTypeId() && false == is_null( $objContract->getCloseDate() ) ) {
				$objContractProperty->setCloseDate( $objContract->getCloseDate() );
			}

			if( CContractType::RENEWAL == $objContract->getContractTypeId() && true == is_null( $objContractProperty->getImplementationEndDate() ) && true == is_null( $objContractProperty->getImplementedOn() ) ) {
				$objContractProperty->setImplementationEndDate( date( 'm/d/Y', strtotime( '+60 days' ) ) );
			} elseif( CContractStatusType::CONTRACT_APPROVED == $objContract->getContractStatusTypeId() && false == is_null( $objContract->getContractStartDate() ) ) {
				$objContractProperty->setImplementationEndDate( date( 'm/d/Y', strtotime( $objContract->getContractStartDate() . '+60 days' ) ) );
			}

			$objContractProperty->setChargeDatetime( 'NOW()' );
			$objContractProperty->setEffectiveDate( 'NOW()' );
			$objContractProperty->setBillingStartDate( $objContract->getContractStartDate() );

			if( CContractStatusType::CONTRACT_APPROVED == $objContract->getContractStatusTypeId() ) {
				$objContractProperty->setChargeDatetime( $objContract->getContractStartDate() );
				$objContractProperty->setEffectiveDate( $objContract->getContractStartDate() );
			}
			$arrobjContractProperties[] = $objContractProperty;
		}

		return $arrobjContractProperties;

	}

}
?>