<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePayrolls
 * Do not add any new functions to this class.
 */

class CEmployeePayrolls extends CBaseEmployeePayrolls {

	public static function fetchPaginatedEmployeePayrollsByEmployeeIds( $arrintEmployeeIds, $arrstrEmployeeCompensationFilter, $objDatabase, $boolIsForCount = false, $boolIsForDownload = false ) {
		if( false == valArr( $arrintEmployeeIds ) || true == empty( $arrstrEmployeeCompensationFilter['viewer_employee_id'] ) ) return NULL;

		$strOrderByField 	= ( false == empty( $arrstrEmployeeCompensationFilter['order_by_field'] ) ) ? $arrstrEmployeeCompensationFilter['order_by_field'] : '';
		$strOrderByType		= ( false == empty( $arrstrEmployeeCompensationFilter['order_by_type'] ) ) ? $arrstrEmployeeCompensationFilter['order_by_type'] : '';
		$strComma 		 	= ( false == empty( $strOrderByField ) ) ? ',' : '';

		$intOffset			= ( false == $boolIsForCount && false == empty( $arrstrEmployeeCompensationFilter['page_no'] ) && false == empty( $arrstrEmployeeCompensationFilter['page_size'] ) ) ? $arrstrEmployeeCompensationFilter['page_size'] * ( $arrstrEmployeeCompensationFilter['page_no'] - 1 ) : 0;
		$intLimit			= ( false == $boolIsForCount && true == isset( $arrstrEmployeeCompensationFilter['page_size'] ) ) ? $arrstrEmployeeCompensationFilter['page_size'] : '';
		$strSelectClause 	= ( false == $boolIsForCount ) ? ' ep.*, gross_pay_comp.encrypted_amount AS gross_pay_encrypted, hourly_pay_comp.encrypted_amount AS hourly_pay_rate_encrypted' : ' count( ep.id ) ';
		$strWhereClause		= 'ep.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		$strOrderByClause	= ( false == $boolIsForCount ) ? ' ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . $strComma . ' e.name_full, ep.payroll_period_id DESC' : '';
		$strOrderByClause	 = ( true == $boolIsForDownload ) ? ' ORDER BY ep.payroll_period_id DESC' : $strOrderByClause;

		$strWhereClause	.= ( false == empty( $arrstrEmployeeCompensationFilter['department_ids'] ) && true == valArr( array_filter( $arrstrEmployeeCompensationFilter['department_ids'] ) ) ) ? ' AND e.department_id IN ( ' . implode( ',', $arrstrEmployeeCompensationFilter['department_ids'] ) . ') ' : '';
		$strWhereClause	.= ( true == isset( $arrstrEmployeeCompensationFilter['dates'] ) && 1 == $arrstrEmployeeCompensationFilter['dates'] && false == empty( $arrstrEmployeeCompensationFilter['single_date'] ) ) ? ' AND ep.end_date BETWEEN \'' . $arrstrEmployeeCompensationFilter['single_date'] . ' 00:00:00 \' AND \'' . $arrstrEmployeeCompensationFilter['single_date'] . ' 23:59:59 \'' : '';
		$strWhereClause	.= ( true == valArr( $arrstrEmployeeCompensationFilter['payroll_period_ids'] ) ) ? ' AND ep.payroll_period_id IN( ' . implode( ',', $arrstrEmployeeCompensationFilter['payroll_period_ids'] ) . ')' : '';

		$strWhereClause	.= ( false == $arrstrEmployeeCompensationFilter['show_terminated'] ) ? ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT : ' ';

		$strSql = ' SELECT
						' . $strSelectClause . '
					FROM
						employee_payrolls ep
						JOIN employee_encryption_associations gross_pay_comp ON ( ep.gross_pay_employee_encryption_association_id = gross_pay_comp.id AND gross_pay_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND gross_pay_comp.viewer_employee_id = ' . ( int ) $arrstrEmployeeCompensationFilter['viewer_employee_id'] . ' )
						LEFT JOIN employee_encryption_associations hourly_pay_comp ON ( ep.hourly_pay_employee_encryption_association_id = hourly_pay_comp.id AND hourly_pay_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND hourly_pay_comp.viewer_employee_id = ' . ( int ) $arrstrEmployeeCompensationFilter['viewer_employee_id'] . ' )
						JOIN employees e ON ( ep.employee_id = e.id )
						JOIN departments d ON( d.id = e.department_id )
					WHERE
						' . $strWhereClause . $strOrderByClause . '
					OFFSET ' . ( int ) $intOffset;

		if( true == $boolIsForCount ) {
			$arrintEmployeePayrollCount = fetchData( $strSql, $objDatabase );
			return ( true == valArr( $arrintEmployeePayrollCount ) ) ? $arrintEmployeePayrollCount[0]['count'] : 0;
		} else {
			if( 0 < $intLimit && false == $boolIsForDownload ) $strSql .= ' LIMIT ' . ( int ) $intLimit;
			return self::fetchEmployeePayrolls( $strSql, $objDatabase );
		}
	}

	public static function fetchEmployeePayrollsByDepartmentIds( $arrstrEmployeeCompensationFilter, $strCountryCode, $objDatabase ) {

		$strSelectClause 	= 'ep.employee_id, gross_pay_comp.encrypted_amount AS gross_pay_encrypted, ep.payroll_period_id';
		$strWhereClause		= 'ea.country_code = \'' . $strCountryCode . '\' AND ea.address_type_id = ' . CAddressType::PRIMARY;
		$strWhereClause		 .= ( false == empty( $arrstrEmployeeCompensationFilter['department_ids'] ) && true == valArr( array_filter( $arrstrEmployeeCompensationFilter['department_ids'] ) ) ) ? ' AND	e.department_id IN ( ' . implode( ',', $arrstrEmployeeCompensationFilter['department_ids'] ) . ') ' : '';
		$strWhereClause		 .= ( true == valArr( $arrstrEmployeeCompensationFilter['payroll_period_ids'] ) ) ? ' AND ep.payroll_period_id IN( ' . implode( ',', $arrstrEmployeeCompensationFilter['payroll_period_ids'] ) . ' )' : '';
		$strWhereClause	.= ( false == $arrstrEmployeeCompensationFilter['show_terminated'] ) ? ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT : ' ';

		$strSql = ' SELECT
						' . $strSelectClause . '
					FROM
						employee_payrolls ep
						LEFT JOIN employee_encryption_associations gross_pay_comp ON( gross_pay_comp.id = ep.gross_pay_employee_encryption_association_id AND gross_pay_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND gross_pay_comp.viewer_employee_id = ' . ( int ) $arrstrEmployeeCompensationFilter['viewer_employee_id'] . ')
						LEFT JOIN employees e ON ( ep.employee_id = e.id )
						JOIN employee_addresses ea ON ( e.id = ea.employee_id )
					WHERE
						' . $strWhereClause . ' ORDER BY ep.payroll_period_id';

		return	$arrstrEmployeePayrolls = fetchData( $strSql, $objDatabase );

	}

	public static function fetchEmployeePayrollsByPayrollPeriodId( $intPayrollPeriodId, $objAdminDatabase ) {

		if( true == empty( $intPayrollPeriodId ) ) return NULL;

		$strSql = 'SELECT * FROM employee_payrolls ep WHERE ep.payroll_period_id = ' . ( int ) $intPayrollPeriodId;

		return self::fetchEmployeePayrolls( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeePayrollDetailsById( $intEmployeePayrollId, $intViewerEmployeeId, $objDatabase ) {
		if( true == empty( $intEmployeePayrollId ) ) return NULL;

		$strSql = ' SELECT ep.*,
							gross_pay_comp.encrypted_amount AS gross_pay_encrypted,
							hourly_pay_comp.encrypted_amount AS hourly_pay_rate_encrypted
					 FROM
						employee_payrolls ep
						LEFT JOIN employee_encryption_associations gross_pay_comp ON( gross_pay_comp.id = ep.gross_pay_employee_encryption_association_id AND gross_pay_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND gross_pay_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						LEFT JOIN employee_encryption_associations hourly_pay_comp ON( hourly_pay_comp.id = ep.hourly_pay_employee_encryption_association_id AND hourly_pay_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND hourly_pay_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
					WHERE ep.id = ' . ( int ) $intEmployeePayrollId;

		return self::fetchEmployeePayroll( $strSql, $objDatabase );
	}

	public static function fetchEmployeePayrollByIdByEmployeeId( $intPayrollId, $intEmployeeId, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						employee_payrolls
					WHERE
						payroll_id	= ' . ( int ) $intPayrollId . '
						AND employee_id	= ' . ( int ) $intEmployeeId . '
					LIMIT 1';

		return self::fetchEmployeePayroll( $strSql, $objDatabase );
	}

	public static function fetchEmployeePayrollsByPayrollPeriodIdEmployeeIds( $intPayrollPeriodId, $arrintEmployeeIds, $objDatabase, $arrstrFields = NULL, $boolFetchArray = false ) {

		$strWhereSql = ( true == valArr( $arrintEmployeeIds ) ) ? ' AND ep.employee_id IN( ' . implode( ',', $arrintEmployeeIds ) . ' )' : '';

		$strSelect = ( true == valArr( $arrstrFields ) ) ? implode( ',', $arrstrFields ) : ' * ';

		$strSql = ' SELECT ' .
						$strSelect . '
					FROM
						employee_payrolls ep
					WHERE
						ep.payroll_period_id = ' . ( int ) $intPayrollPeriodId . $strWhereSql;

		if( true == $boolFetchArray ) {
			return ( array ) fetchData( $strSql, $objDatabase );
		}

		return ( array ) parent::fetchEmployeePayrolls( $strSql, $objDatabase );
	}

	public static function fetchUnReviewedEmployeePayrollsCountByPayrollId( $intPayrollId, $objDatabase, $boolCheckException = false, $arrintEmployeeIds = NULL ) {

		$strWhereSql = ( true == valArr( $arrintEmployeeIds ) ) ? ' AND ep.employee_id IN( ' . implode( ',', $arrintEmployeeIds ) . ' )' : '';

		if( true == $boolCheckException ) {
			$strWhereSql .= ' AND ( ep.supporting_data ->> \'exceptions\' )::TEXT IS NOT NULL';
		}
		// for time being puting hard coded employeeids to verify payrolls issues.

		$strSql = ' SELECT
						count( 1 )
					FROM
						employee_payrolls ep
					WHERE
						ep.reviewed_by IS NULL
						AND ep.payroll_id = ' . ( int ) $intPayrollId . $strWhereSql;

		$arrstrResult = fetchData( $strSql, $objDatabase );
		return $arrstrResult[0]['count'];
	}

	public static function fetchEmployeePayrollDetailsByPayrollId( $intPayrollId, $objDatabase, $strReportName = NULL, $arrmixFilter = array(), $boolIscount = false ) {

		$strJoinSql = '';
		$strWhereSql = '';

		switch( $strReportName ) {
			case 'weekend_working':
				$strSelectSql 	= ', ep.extra_working_days AS extra_working_days_count,
									ep.supporting_data ->> \'extra_working_days\' AS extra_working_days';

				$strWhereSql 	= ' AND ep.extra_working_days > 0';
				break;

			case 'full_and_final_settlement_report':
				$strSelectSql 	= ', e.date_terminated AS last_working_date,
									ep.unpaid_days AS LOP,
									ep.present_days + ep.paid_days AS paid_days,
									( ep.supporting_data ->> \'leaves\' )::json ->> \'closing_leave_pool\' AS leave_encashment';

				$strJoinSql .= ' JOIN payrolls p ON p.id = ep.payroll_id ';

				$strWhereSql 	= ' AND e.date_terminated BETWEEN p.begin_date AND p.end_date';
				break;

			case 'retention_bonous_on_hold':
				$strSelectSql = ', \'Resigned On \' || to_char(e.resigned_on, \'DD, Mon YYYY\') as resigned_on';

				$strJoinSql .= ' JOIN payrolls p ON p.id = ep.payroll_id ';

				$strWhereSql 	= ' AND e.resigned_on BETWEEN p.begin_date AND p.end_date';
				break;

			case 'attendance':
				$strSelectSql = ', ( ep.supporting_data ->> \'leaves\' )::json ->> \'opening_leave_pool\' AS opening_leave_pool,
									( ep.supporting_data ->> \'leaves\' )::json ->> \'closing_leave_pool\' AS closing_leave_pool,
									( ep.supporting_data ->> \'leaves\' )::json ->> \'special_leave\' AS special_leaves,
									( ep.supporting_data ->> \'leaves\' )::json ->> \'paid\' AS paid_leave_details,
									( ep.supporting_data ->> \'leaves\' )::json ->> \'unpaid\' AS unpaid_leave_details,
									ep.paid_days As paid_leaves,
									ep.unpaid_days AS lop,
									e.date_started AS date_started,
									e.date_terminated AS date_terminated';

				$strJoinSql .= ' JOIN payrolls p ON p.id = ep.payroll_id ';
				break;

			default:
				$strSelectSql 	= '';
				$strWhereSql 	= '';
		}

		$arrmixSearchResult  = self::getSearchData( $arrmixFilter );
		$strWhereSql 		.= $arrmixSearchResult['where_sql'];
		$strJoinSql 		.= $arrmixSearchResult['join_sql'];

		$strOrderBySql = '';

		if( true == $boolIscount ) {
			$strSelectSql = ' count( e.id ) ';

		} else {
			$strSelectSql = ' e.employee_number as employee_id,
							COALESCE ( e.preferred_name, e.name_full ) as employee_name ' . $strSelectSql;

			$strOrderBySql = ' ORDER BY e.employee_number';
		}

		$strSql = ' SELECT '
						. $strSelectSql . '
					FROM
						employee_payrolls ep
						JOIN employees e ON ( e.id = ep.employee_id ) ' .
						$strJoinSql . '
					WHERE
						ep.payroll_id = ' . ( int ) $intPayrollId . $strWhereSql . $strOrderBySql;

		$arrstrResult = fetchData( $strSql, $objDatabase );
		if( true == $boolIscount ) {
			return ( true == valArr( $arrstrResult ) ) ? $arrstrResult[0]['count'] : 0;
		}

		return $arrstrResult;
	}

	public static function fetchEmployeePayrollsDesignationChangeReportByPayrollIdByDate( $intPayrollId, $strDate, $objDatabase, $arrmixFilter = array(), $boolIscount = false ) {

		$arrmixSearchResult = self::getSearchData( $arrmixFilter );
		$strWhereSql 		= $arrmixSearchResult['where_sql'];
		$strJoinSql 		= $arrmixSearchResult['join_sql'];

		if( true == $boolIscount ) {
			$strSelectSql = ' count( e.id ) ';

		} else {
			$strSelectSql = ' e.employee_number as employee_id,
								COALESCE ( e.preferred_name, e.name_full ) AS employee_name,
								d.name AS new_designation,
								d2.name AS old_designation ';

			$strOrderBySql = ' ORDER BY
								e.employee_number';
		}

		$strSql = ' with ep_old as (
						SELECT
							old_ep.employee_id,
							old_ep.designation_id
						FROM
							(
								SELECT
									p.id
								FROM
									payrolls p
								WHERE
									p.end_date < \'' . $strDate . '\'
								ORDER BY
									p.end_date DESC
								LIMIT
									1
							) p2
							JOIN employee_payrolls old_ep ON old_ep.payroll_id = p2.id
					)
					SELECT ' .
						$strSelectSql . '
					FROM
						employee_payrolls ep
						JOIN employees e ON e.id = ep.employee_id
						LEFT JOIN ep_old ON ( ep_old.employee_id = ep.employee_id )
						LEFT JOIN designations d ON ( d.id = ep.designation_id )
						LEFT JOIN designations d2 ON ( d2.id = ep_old.designation_id )' .
						$strJoinSql . '
					WHERE
						ep_old.designation_id <> ep.designation_id
						AND ep.payroll_id = ' . ( int ) $intPayrollId . $strWhereSql . $strOrderBySql;

		$arrstrResult = fetchData( $strSql, $objDatabase );
		if( true == $boolIscount ) {
			return ( true == valArr( $arrstrResult ) ) ? $arrstrResult[0]['count'] : 0;
		}
		return $arrstrResult;
	}

	public static function getSearchData( $arrmixFilter ) {

		$strWhereSql 	= '';
		$strJoinSql 	= '';

		if( true == isset( $arrmixFilter['hr_representative_id'] ) ) {

			$strJoinSql .= ' LEFT OUTER JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
							LEFT OUTER JOIN teams t ON ( t.id = te.team_id )';

			if( true == valId( $arrmixFilter['hr_representative_id'] ) ) {

				$strWhereSql .= ' AND ( t.hr_representative_employee_id =  ' . ( int ) $arrmixFilter['hr_representative_id'] . ' ) ';

			} elseif( 'other' == $arrmixFilter['hr_representative_id'] ) {

				// condition for other HR => Employees who don't have any HR assigned or HR is not in India
				$strWhereSql .= ' AND (
										t.hr_representative_employee_id IS NULL
										OR
										t.hr_representative_employee_id NOT IN (
											SELECT
												DISTINCT ON ( e.id ) e.id
											FROM
												employees e
												JOIN teams t ON ( e.id = t.hr_representative_employee_id )
												JOIN employee_addresses AS ea ON ( e.id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\')
											WHERE
												ea.country_code = \'' . CCountry::CODE_INDIA . '\'
												AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
										)
									)';
			}

		}

		if( true == isset( $arrmixFilter['search_data'] ) && true == valArr( $arrmixFilter['search_data'] ) ) {
			$strWhereSql .= ' AND ( e.name_full ILIKE \'%' . implode( '%\' AND name_full ILIKE \'%', $arrmixFilter['search_data'] ) . '%\' )';
		}

		return array( 'where_sql' => $strWhereSql, 'join_sql' => $strJoinSql );
	}

}
?>