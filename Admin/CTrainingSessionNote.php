<?php

class CTrainingSessionNote extends CBaseTrainingSessionNote {

	protected $m_strNameFull;

	/**
	 * Get Functions
	 *
	 */

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_full'] ) )				$this->setNameFull( $arrmixValues['name_full'] );
		return;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valNote( $boolIsReason = false ) {
		$boolIsValid = true;
		if( true == is_null( $this->getNote() ) ) {
			$boolIsValid = false;

			if( false == $boolIsReason ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Training note required.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Reason is required.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNote();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_attendance_reason':
				$boolIsValid &= $this->valNote( $boolIsReason = true );
				break;

			default:
				// no validation avaliable for delete action
				break;
		}

		return $boolIsValid;
	}

}
?>