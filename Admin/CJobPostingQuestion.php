<?php

class CJobPostingQuestion extends CBaseJobPostingQuestion {

	const FORMER_EMPLOYEE_QUESTION_USA = 1;

	protected $m_strQuestionTypeName;

	/**
	 * Get Functions
	 *
	 */

	public function getQuestionTypeName() {
		return $this->m_strQuestionTypeName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setQuestionTypeName( $strQuestionTypeName ) {
		$this->m_strQuestionTypeName = $strQuestionTypeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['type_name'] ) )	$this->setQuestionTypeName( $arrmixValues['type_name'] );

		return;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valQuestionTypeId() {
		$boolIsValid = true;

		// since datatype of field is integer and if we pass string it returns 0 so need to check with 0
		if( false == isset( $this->m_intQuestionTypeId ) && ( true == is_null( $this->getQuestionTypeId() ) || 0 == $this->getQuestionTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question_type_id', ' Please select question type.' ) );
		}

		return $boolIsValid;
	}

	public function valQuestion() {
		$boolIsValid = true;

		if( false == isset( $this->m_strQuestion ) && false == valStr( $this->getQuestion() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', ' Question is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valQuestionTypeId();
				$boolIsValid &= $this->valQuestion();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}
}
?>