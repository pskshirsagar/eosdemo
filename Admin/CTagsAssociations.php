<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTagsAssociations
 * Do not add any new functions to this class.
 */

class CTagsAssociations extends CBaseTagsAssociations {

	public static function fetchTagAssociationsByTaskId( $intTaskId, $intActionResultId, $objDatabase, $intUserId = NULL ) {
		$strWhere = '';

		if( false == is_numeric( $intTaskId ) ) return NULL;

		if( valId( $intUserId ) ) {
			$strWhere = ' AND ( t.is_private = false or ( t.is_private = true AND ( t.created_by = ' . ( int ) $intUserId . ' or t.owner_id = ' . ( int ) $intUserId . ' ) ) ) ';
		}

		$strSql = 'SELECT
						tag_id,
						name,
						ta.created_by
					FROM
						tags_associations ta
						JOIN tags t ON ( t.id = ta.tag_id )
					WHERE
						ta.task_id = ' . ( int ) $intTaskId . ' AND t.action_result_id = ' . ( int ) $intActionResultId . ' AND t.is_published = 1 ' . $strWhere . ' ORDER BY ta.id ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTasksByTagId( $intTagId, $objDatabase ) {
		if( false == is_numeric( $intTagId ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT t.id as task_id,
						t.title
					FROM
						tags_associations ta
						JOIN tasks t ON ( t.id = ta.task_id)
					WHERE
						ta.tag_id = ' . ( int ) $intTagId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTagAssociationByTagIdAndTaskId( $intTagId, $intTaskId, $intActionResultId, $objDatabase ) {
		if( false == is_numeric( $intTagId ) || false == is_numeric( $intTaskId ) ) return NULL;

		$strSql = 'SELECT
						ta.*
					FROM
						tags_associations ta
						JOIN tags t ON ( t.id = ta.tag_id )
					WHERE
						ta.tag_id = ' . ( int ) $intTagId . '
						AND ta.task_id = ' . ( int ) $intTaskId . '
						AND t.action_result_id = ' . ( int ) $intActionResultId . ' AND t.is_published = 1';

		return parent::fetchTagsAssociation( $strSql, $objDatabase );
	}

	public static function fetchTagsAssociationssByReportIdByTagIds( $intCompanyReportId, $arrintTagsIds, $objAdminDatabase ) {

		if( false == is_numeric( $intCompanyReportId ) || false == valArr( array_filter( $arrintTagsIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						tags_associations AS ta
					WHERE
						ta.company_report_id = ' . ( int ) $intCompanyReportId . '
						AND ta.tag_id IN ( ' . implode( ',', array_filter( $arrintTagsIds ) ) . ' ) ';

		return parent::fetchTagsAssociations( $strSql, $objAdminDatabase );
	}

}
?>