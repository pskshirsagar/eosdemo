<?php

class CSmsResponsePhoneNumber extends CBaseSmsResponsePhoneNumber {

	public function valCellNumber() {
		$boolIsValid = true;

		if( 0 == strlen( $this->getCellNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', 'Phone number cannot be empty.' ) );
		} else {
			$strPhoneNumber = preg_replace( '/[^0-9]/', '', $this->getCellNumber() );

			if( 10 != strlen( $strPhoneNumber ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', 'Not a valid phone number' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCellNumber();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>