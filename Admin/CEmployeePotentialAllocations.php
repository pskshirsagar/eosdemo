<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePotentialAllocations
 * Do not add any new functions to this class.
 */

class CEmployeePotentialAllocations extends CBaseEmployeePotentialAllocations {

	public static function fetchEmployeePotentialAllocationByEmployeeIdByPayReviewYear( $intEmployeeId, $strPayReviewYear, $objDatabase ) {
		if( false == is_numeric( $intEmployeeId ) || false == valStr( $strPayReviewYear ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM employee_potential_allocations WHERE employee_id = ' . ( int ) $intEmployeeId . ' AND EXTRACT( YEAR FROM pay_review_year ) = ' . $strPayReviewYear;
		return self::fetchEmployeePotentialAllocation( $strSql, $objDatabase );
	}

	public static function fetchEmployeePayAllocationDetailsByEmployeeIdByViewerEmployeeIdByPayReviewYear( $intEmployeeId, $intViewerEmployeeId, $strPayReviewYear, $objDatabase ) {

		if( true == empty( $intViewerEmployeeId ) || true == empty( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						eea.encrypted_amount AS manager_encrypted_amount,
						epa.employee_pay_status_type_id AS status_type_id,
						epst.name AS pay_status_name,
						epa.employee_id as manager_employee_id,
						epa.details
					FROM
						employee_potential_allocations epa
						LEFT JOIN employee_encryption_associations eea ON ( eea.id = epa.potential_increase_encryption_association_id AND eea.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::ANNUAL_PAY_REVIEW . ' AND eea.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						JOIN employee_pay_status_types epst ON ( epst.id = epa.employee_pay_status_type_id)
					WHERE
						epa.employee_id = ' . ( int ) $intEmployeeId . '
						AND EXTRACT( YEAR FROM epa.pay_review_year ) = ' . $strPayReviewYear;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeePotentialAllocationByEmployeeIdsByPayReviewYear( $arrintEmployeeIds, $strPayReviewYear, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) || false == valStr( $strPayReviewYear ) ) {
			return NULL;
		}

		$strSql = '	SELECT 
						id,
						employee_id,
						employee_pay_status_type_id,
						manager_updated_by,
						manager_updated_on,
						details
					FROM employee_potential_allocations
					WHERE employee_id IN ( ' . sqlIntImplode( $arrintEmployeeIds ) . ' )
						 AND EXTRACT( YEAR FROM pay_review_year ) = ' . $strPayReviewYear;

		return self::fetchEmployeePotentialAllocations( $strSql, $objDatabase );
	}

}
?>