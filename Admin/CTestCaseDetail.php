<?php

class CTestCaseDetail extends CBaseTestCaseDetail {

	const SQM_FIRST_TAB		= 1;
	const SQM_SECOND_TAB	= 2;

	const AUDIT_REQUIRED	= 1;
	const APPROVAL_REQUIRED	= 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldTestCaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedTaskIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskAcceptanceCriteriaIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQaManagerEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTestCaseExecutionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTestCaseProductFolderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewerEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreconditions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTestData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCanBeAutomated() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAutomationStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoadTestStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScriptName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoadTestCandidate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsArchive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>