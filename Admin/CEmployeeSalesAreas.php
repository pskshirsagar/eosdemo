<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeSalesAreas
 * Do not add any new functions to this class.
 */

class CEmployeeSalesAreas extends CBaseEmployeeSalesAreas {

	public static function fetchEmployeeSalesAreasBySalesAreaIdByEmployeeIds( $intSalesAreaId, $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return;

		$strSql = 'SELECT * FROM employee_sales_areas where sales_area_id =' . ( int ) $intSalesAreaId . ' AND employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ORDER BY created_on DESC';

		return self::fetchEmployeeSalesAreas( $strSql, $objDatabase );
	}

}
?>