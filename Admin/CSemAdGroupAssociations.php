<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdGroupAssociations
 * Do not add any new functions to this class.
 */

class CSemAdGroupAssociations extends CBaseSemAdGroupAssociations {

	public static function fetchSemAdGroupAssociationsByByPropertyIdByCid( $intPropertyId, $intCid, $objAdminDatbase ) {

		$strSql = 'SELECT * FROM sem_ad_group_associations WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchSemAdGroupAssociations( $strSql, $objAdminDatbase );
	}

}
?>