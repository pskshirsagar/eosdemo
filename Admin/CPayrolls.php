<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPayrolls
 * Do not add any new functions to this class.
 */

class CPayrolls extends CBasePayrolls {

	public static function fetchCurrentPayroll( $objDatabase, $strDate = NULL ) {
		if( true == is_null( $strDate ) ) {
			$strDate = date( 'm/d/Y' );
		}

		$strSql = ' SELECT
						*
					FROM
						payrolls
					WHERE
						end_date >= \'' . $strDate . '\'
						AND begin_date <= \'' . $strDate . '\'
					ORDER BY
						id DESC
					LIMIT 1';

		return self::fetchPayroll( $strSql, $objDatabase );
	}

	public static function fetchAllPayrolls( $objDatabase, $boolFetchUnApprovedPayrolls = true ) {

		$strWhereSql = ( false == $boolFetchUnApprovedPayrolls ) ? ' WHERE approved_by IS NOT NULL ' : '';

		$strSql = ' SELECT
						id,
						begin_date,
						end_date,
						approved_by
					FROM
						payrolls ' .
					$strWhereSql . '
					ORDER BY
						id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPayrollByPayrollPeriodId( $intPayrollPeriodId, $objDatabase ) {
		return self::fetchPayroll( sprintf( 'SELECT * FROM payrolls WHERE payroll_period_id = %d LIMIT 1', ( int ) $intPayrollPeriodId ), $objDatabase );
	}

	public static function fetchPayrollByPayrollPeriodIds( $arrintPayrollPeriodIds, $objDatabase ) {
		if( false == valArr( $arrintPayrollPeriodIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						*
					FROM
						payrolls
					WHERE
						payroll_period_id IN ( ' . sqlIntImplode( $arrintPayrollPeriodIds ) . ' )';

		return self::fetchPayrolls( $strSql, $objDatabase );
	}

}
?>