<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCertificationLevelTypes
 * Do not add any new functions to this class.
 */

class CCertificationLevelTypes extends CBaseCertificationLevelTypes {

	public static function fetchCertificationLevelTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CCertificationLevelType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCertificationLevelType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CCertificationLevelType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllCertificationLevelTypes( $objDatabase ) {

		$strSql = 'SELECT * FROM certification_level_types';

		return parent::fetchCertificationLevelTypes( $strSql, $objDatabase );
	}
}
?>