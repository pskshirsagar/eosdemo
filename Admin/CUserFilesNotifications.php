<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserFilesNotifications
 * Do not add any new functions to this class.
 */

class CUserFilesNotifications extends CBaseUserFilesNotifications {

	public static function fetchUserFilesNotificationByIds( $intIds, $objDatabase ) {
		$strSql = 'SELECT * FROM user_files_notifications WHERE id IN (' . $intIds . ')';
		return self::fetchUserFilesNotifications( $strSql, $objDatabase );
	}

	public static function fetchAllUserFilesNotification( $objDatabase ) {
		return parent::fetchUserFilesNotifications( 'SELECT * FROM user_files_notifications', $objDatabase );
	}

	public static function fetchFilePathByUserId( $intUserId, $objDatabase ) {
		$strSql = 'SELECT file_name, svn_repository_id FROM user_files_notifications WHERE user_id = ' . $intUserId;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedUserFilesNotifications( $intPageNo, $intPageSize, $objDatabase, $intUserId = NULL ) {
		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;
		$strWhere	= ( false == is_null( $intUserId ) ) ? 'WHERE user_id = ' . $intUserId : '';
		$strSql 	= 'SELECT * FROM user_files_notifications
						' . $strWhere . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchUserFilesNotifications( $strSql, $objDatabase );
	}

	public static function fetchUserFilesNotificationsCount( $objDatabase, $intUserId = NULL ) {

		$strWhere	= ( false == is_null( $intUserId ) ) ? 'WHERE user_id = ' . $intUserId : '';
		$strSql = 'SELECT count(id) FROM user_files_notifications ' . $strWhere;
		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		return 0;

	}

}
?>