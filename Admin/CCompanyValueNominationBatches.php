<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyValueNominationBatches
 * Do not add any new functions to this class.
 */

class CCompanyValueNominationBatches extends CBaseCompanyValueNominationBatches {

	public static function fetchCurrentCompanyValueNominationBatch( $objDatabase ) {
		$strSql = '
			SELECT *
			FROM company_value_nomination_batches
			WHERE CURRENT_DATE BETWEEN start_date AND end_date
			ORDER BY id DESC
			LIMIT 1;
		';

		return self::fetchCompanyValueNominationBatch( $strSql, $objDatabase );
	}

	public static function fetchCurrentCompanyValueNominationBatchId( $objDatabase ) {

		$objCompanyValueNominationBatch = self::fetchCurrentCompanyValueNominationBatch( $objDatabase );

		if( false == valObj( $objCompanyValueNominationBatch, 'CCompanyValueNominationBatch' ) ) {
			return NULL;
		}

		return $objCompanyValueNominationBatch->getId();
	}

}
?>