<?php

class CEmployeeApplicationResponse extends CBaseEmployeeApplicationResponse {

	public function valAnswer( $strQuestion ) {
		$boolIsValid = true;
		if( true == is_null( $this->getAnswer() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer', 'Answer required for ' . $strQuestion ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $strQuestion ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valAnswer( $strQuestion );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>