<?php

class CPsAssetPurchaseSource extends CBasePsAssetPurchaseSource {

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == ( is_null( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Source name is required.' ) );

			return $boolIsValid;
		}

		if( ( false == preg_match( '/^[a-zA-Z0-9- ]{2,50}$/', $this->getName() ) ) || ( true == is_numeric( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'A valid source name is required.' ) );

			return $boolIsValid;
		}

		$intCount = \Psi\Eos\Admin\CPsAssetPurchaseSources::createService()->fetchPsAssetPurchaseSourceCount( 'WHERE deleted_by IS NULL AND deleted_on IS NULL AND name LIKE ' . "'" . $this->getName() . "'", $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Source name already exists!' ) );

			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		$intCount = \Psi\Eos\Admin\CPsAssets::createService()->fetchPsAssetCount( 'WHERE deleted_by IS NULL AND deleted_on IS NULL AND ps_asset_purchase_sources_id = ' . $this->getId(), $objDatabase );

		if( 0 < $intCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Failed to delete the source, it is being associated with assets.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}

?>