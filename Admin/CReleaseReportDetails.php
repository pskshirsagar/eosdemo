<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseReportDetails
 * Do not add any new functions to this class.
 */

class CReleaseReportDetails extends CBaseReleaseReportDetails {

	public static function fetchReleaseReportDetailsByTaskReleaseIds( $arrintTaskReleaseIds, $objDatabase ) {

		if( false == valArr( $arrintTaskReleaseIds ) ) {
			return false;
		}
		$strTaskReleaseIds = implode( ', ', $arrintTaskReleaseIds );
		$strSql = 'SELECT
						*
					FROM
						release_report_details
					WHERE
						task_release_id IN ( ' . $strTaskReleaseIds . ' ) ORDER BY task_release_id';

		$arrobjReleaseReportDetails = self::fetchReleaseReportDetails( $strSql, $objDatabase );
		$arrobjReleaseReportDetails = rekeyObjects( 'TaskReleaseId', $arrobjReleaseReportDetails );

		return $arrobjReleaseReportDetails;
	}

}
?>