<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsImplementationEmployees
 * Do not add any new functions to this class.
 */

class CStatsImplementationEmployees extends CBaseStatsImplementationEmployees {

	public static function rebuildImplementationEmployeeStats( $intImplementationEmployeeId, $objDatabase ) {

		if( true == is_numeric( $intImplementationEmployeeId ) ) {
			$strImplementationEmployeeCondition = ' = ' . ( int ) $intImplementationEmployeeId;
		} else {
			$strImplementationEmployeeCondition = ' <> 0 ';
		}

		$objDatabase->begin();
		if( false === fetchData( self::buildImplementationEmployeeStatsInsertSql( $strImplementationEmployeeCondition ), $objDatabase ) ) {
			$objDatabase->rollback();
			return false;
		}
		$objDatabase->commit();

		$strSql = self::buildImplementationEmployeeContractsSql( $strImplementationEmployeeCondition );

		return fetchData( $strSql, $objDatabase );
	}

	public static function buildImplementationEmployeeStatsInsertSql( $strImplementationEmployeeCondition ) {

		$strSql = '
					DELETE FROM stats_implementation_employees WHERE employee_id' . $strImplementationEmployeeCondition . ';

					INSERT INTO
						public.stats_implementation_employees ( id, employee_id, week, created_by, created_on )

						SELECT
							nextval ( \'stats_implementation_employees_id_seq\' ) AS id,
							ie.employee_id,
							d.week,
							1 AS created_by,
							NOW () AS created_on
						FROM
							(
								SELECT DISTINCT
									e.id employee_id,
									e.date_started,
									COALESCE( e.date_terminated, now() )::date date_terminated
								FROM
									contracts c
									JOIN contract_properties cp ON ( cp.contract_id = c.id )
									JOIN contract_details cd ON ( cd.contract_id = c.id )
									JOIN clients mc ON ( mc.id = c.cid )
										AND mc.company_status_type_id IN ( ' . implode( ', ', CCompanyStatusType::$c_arrintClosedCompanyStatusTypeIds ) . ' )
									LEFT JOIN contract_employees ce ON ( c.id = ce.contract_id AND ce.contract_employee_type_id = ' . CContractEmployeeType::IMPLEMENTATION . ' )
									LEFT JOIN users u ON u.id = cp.implemented_by
									JOIN employees e ON ( e.id = COALESCE( cp.implementation_employee_id, ce.employee_id, u.employee_id ) )
										AND e.employee_status_type_id NOT IN (' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ' )
									LEFT JOIN contract_termination_requests ctr ON ctr.id = cp.contract_termination_request_id
									LEFT JOIN contract_renewals cr ON ( c.id = cr.old_contract_id )
								WHERE
									c.contract_status_type_id IN ( ' . implode( ', ', CContractStatusType::$c_arrintCompletedContractStatusTypeIds ) . ' )
									AND ( COALESCE( cp.implemented_on, now() ) BETWEEN cp.close_date AND now() )
									AND COALESCE( ctr.contract_termination_reason_id, 0 ) != ' . CContractTerminationReason::DUPLICATE_CONTRACT . '
									AND cr.id IS NULL
									AND cp.renewal_contract_property_id IS NULL
									AND cp.is_last_contract_record = TRUE
							) AS ie
							JOIN ( SELECT DISTINCT DATE_TRUNC ( \'week\', d.date ) AS week
									 FROM dates d
								) d ON ( DATE_TRUNC ( \'week\', d.week ) <= DATE_TRUNC ( \'week\', NOW ( ) ) )
						WHERE
							( d.week BETWEEN ie.date_started AND ie.date_terminated )
							AND ie.employee_id' . $strImplementationEmployeeCondition . ';
				';

		return $strSql;
	}

	public static function buildImplementationEmployeeContractsSql( $strImplementationEmployeeCondition ) {

		$strSql = 'UPDATE
						stats_implementation_employees sie
					SET
						average_days_to_implement = data.average_days_to_implement,
						implementation_weight_score_total = data.implementation_weight_score_total,
						property_products_implemented = data.property_products_implemented,
						implemented_acv = data.implemented_acv,
						unimplemented_acv = data.unimplemented_acv
					FROM (
							WITH cp AS (
										SELECT *
										FROM
											(
												SELECT
													e.id employee_id,
													cd.implementation_weight_score,
													cp.implemented_on,
													cp.close_date,
													c.contract_status_type_id,
													cp.contract_termination_request_id,
													date_trunc( \'week\', cp.implemented_on )::DATE AS implementation_week,
													( EXTRACT(epoch FROM AGE( cp.implemented_on, cp.close_date ) ) / 86400 ) + 1 days_to_implement,
													( cp.monthly_recurring_amount + cp.transactional_amount ) * 12 contract_acv,
													ROW_NUMBER() OVER( PARTITION BY cp.ps_product_id, cp.property_id ORDER BY cp.close_date DESC ) rank
											FROM
												contracts c
												JOIN contract_properties cp ON ( cp.contract_id = c.id )
												JOIN contract_details cd ON ( cd.contract_id = c.id )
												JOIN clients mc ON ( mc.id = c.cid )
												  AND mc.company_status_type_id IN ( ' . implode( ', ', CCompanyStatusType::$c_arrintClosedCompanyStatusTypeIds ) . ' )
												LEFT JOIN contract_employees ce ON ( c.id = ce.contract_id AND ce.contract_employee_type_id = ' . CContractEmployeeType::IMPLEMENTATION . ' )
												LEFT JOIN users u ON u.id = cp.implemented_by
												JOIN employees e ON ( e.id = COALESCE( cp.implementation_employee_id, ce.employee_id, u.employee_id ) )
													AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ' )
												LEFT JOIN contract_termination_requests ctr ON ctr.id = cp.contract_termination_request_id
												LEFT JOIN contract_renewals cr ON ( c.id = cr.old_contract_id )
											WHERE
												c.contract_status_type_id IN ( ' . implode( ', ', CContractStatusType::$c_arrintCompletedContractStatusTypeIds ) . ' )
												AND ( COALESCE( cp.implemented_on, now() ) BETWEEN cp.close_date AND now() )
												AND COALESCE( ctr.contract_termination_reason_id, 0 ) != ' . CContractTerminationReason::DUPLICATE_CONTRACT . '
												AND cr.id IS NULL
												AND cp.renewal_contract_property_id IS NULL
												AND cp.is_last_contract_record = TRUE
												AND cp.commission_bucket_id <> ' . CCommissionBucket::TRANSFER_PROPERTY . '
											) all_cp
										WHERE rank = 1
										)

							SELECT COALESCE( unimplemented.employee_id, implemented.employee_id ) employee_id,
								COALESCE( unimplemented.week, implemented.implementation_week ) AS week,
								COALESCE( implemented.average_days_to_implement, 0 ) average_days_to_implement,
								COALESCE( implemented.implementation_weight_score_total, 0 ) implementation_weight_score_total,
								COALESCE( implemented.property_products_implemented, 0 ) property_products_implemented,
								COALESCE( implemented.implemented_acv, 0 ) implemented_acv,
								COALESCE( unimplemented.unimplemented_acv, 0 ) unimplemented_acv
							FROM
								(
									SELECT employee_id,
										week,
										SUM( contract_acv ) unimplemented_acv
									FROM
									(
										SELECT *
										FROM (
											SELECT DISTINCT date_trunc( \'week\', d.date )::DATE AS week
											FROM dates d
											WHERE d.date BETWEEN ( SELECT MIN( cp.close_date ) FROM cp ) AND now()
											) d
											LEFT JOIN cp ON cp.close_date + INTERVAL \'60 days\' <= d.week
												AND cp.implemented_on IS NULL
										WHERE cp.contract_termination_request_id IS NULL
											AND cp.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
									) unimplemented
									GROUP BY employee_id,
										week
								) unimplemented
								FULL JOIN
								(
									SELECT employee_id,
										implementation_week,
										AVG( days_to_implement ) average_days_to_implement,
										SUM( implementation_weight_score ) implementation_weight_score_total,
										SUM( contract_acv ) implemented_acv,
										COUNT(*) property_products_implemented
									FROM cp
									WHERE cp.implemented_on IS NOT NULL
									GROUP BY employee_id,
										implementation_week
								) implemented ON implemented.employee_id = unimplemented.employee_id
									AND implemented.implementation_week = unimplemented.week
						) data
					WHERE
						sie.employee_id = data.employee_id
						AND sie.week = data.week
						AND sie.employee_id' . $strImplementationEmployeeCondition . ';
					';

		return $strSql;
	}

}
?>