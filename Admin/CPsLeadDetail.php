<?php
use Psi\Eos\Admin\CPsLeadDetails;

class CPsLeadDetail extends CBasePsLeadDetail {

	protected $m_strClientName;
	protected $m_intEmployeeId;
	protected $m_intKeyClientRank;
	protected $m_intIsKeyClient;

	const BLUE		= 1;
	const GREEN		= 2;
	const YELLOW	= 3;
	const RED		= 4;

	const HIGH			= 1;
	const MEDIUM_HIGH	= 2;
	const MEDIUM		= 3;
	const MEDIUM_LOW	= 4;
	const LOW			= 5;

	public static $c_arrstrClientRiskStatusIcons = [
		self::BLUE		=> 'blue',
		self::GREEN		=> 'green',
		self::YELLOW	=> 'yellow',
		self::RED		=> 'red'
	];

	public static $c_arrstrKeyClientRanks = [
		self::HIGH			=> 'high',
		self::MEDIUM_HIGH	=> 'medium-high',
		self::MEDIUM		=> 'medium',
		self::MEDIUM_LOW	=> 'medium-low',
		self::LOW			=> 'low'
	];

	public static $c_arrstrKeyClientRiskTitles = [
		self::BLUE		=> 'Promoter',
		self::GREEN		=> 'Low Risk Client',
		self::YELLOW	=> 'Medium Risk Client',
		self::RED		=> 'High Risk Client'
	];

	public static $c_arrstrClientRiskStatusTitles = [
		self::BLUE		=> 'Corporate NPS of at least 50.',
		self::GREEN		=> 'Default Status; a client resides at Low Risk until they qualify for Promoter status or things are bad enough for them to be in Medium or High categories.',
		self::YELLOW	=> 'Expresses frustration over an outage, i.e. This is becoming really frustrating. Multiple releases that impact the client negatively. Main point of contact changes; previous MPOC is still with company. Account that has been escalated to a manager.',
		self::RED		=> 'Our main point of contact leaves the company. An internal champion (other than our MPOC) leaves the company. The client is getting purchased by another company. Client tells us that they are evaluating other potential providers.'
	];

	/**
	 * Create Functions
	 */

	public function createEmployeeAssociation() {

		$objEmployeeAssociation = new CEmployeeAssociation();
		$objEmployeeAssociation->setPsLeadId( $this->getPsLeadId() );
		$objEmployeeAssociation->setAssociationDatetime( date( 'm/d/Y H:i:s' ) );

		return $objEmployeeAssociation;
	}

	/**
	 * Get Functions
	 */

	public function getTaxNumber() {
		if( false == valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_CLIENT_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CLIENT_TAX_NUMBER ] );
	}

	public function getTaxNumberMasked() {
		$strTaxNumber = $this->getTaxNumber();
		$intStringLength = strlen( $strTaxNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strTaxNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getKeyClientRank() {
		return $this->m_intKeyClientRank;
	}

	public function getIsKeyClient() {
		return $this->m_intIsKeyClient;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['tax_number'] ) ) $this->setTaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['tax_number'] ) : $arrstrValues['tax_number'] );
		if( true == isset( $arrstrValues['client_name'] ) ) $this->setClientName( $arrstrValues['client_name'] );
		if( true == isset( $arrstrValues['employee_id'] ) ) $this->setEmployeeId( $arrstrValues['employee_id'] );
		if( true == isset( $arrstrValues['key_client_rank'] ) ) $this->setKeyClientRank( $arrstrValues['key_client_rank'] );
		if( true == isset( $arrstrValues['is_key_client'] ) ) $this->setIsKeyClient( $arrstrValues['is_key_client'] );

	}

	public function setTaxNumber( $strTaxNumber ) {
		if( true == valStr( $strTaxNumber ) ) {
			$this->setTaxNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strTaxNumber, CONFIG_SODIUM_KEY_CLIENT_TAX_NUMBER ) );
		}
	}

	public function setDefaults() {
		$this->m_intInternalTaskIntervalDays	= 7;
		$this->m_intCompanyTaskIntervalDays		= 4;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setKeyClientRank( $intKeyClientRank ) {
		$this->m_intKeyClientRank = $intKeyClientRank;
	}

	public function setIsKeyClient( $intIsKeyClient ) {
		$this->m_intIsKeyClient = $intIsKeyClient;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchClient( $objDatabase ) {
		$objClient = \Psi\Eos\Admin\CClients::createService()->fetchClientById( $this->getCid(), $objDatabase );

		return $objClient;
	}

	public function fetchSalesEmployee( $objDatabase ) {
		return CEmployees::fetchEmployeeById( $this->getSalesEmployeeId(), $objDatabase );
	}

	public function fetchSupportEmployee( $objDatabase ) {
		return CEmployees::fetchEmployeeById( $this->getSupportEmployeeId(), $objDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function valSalesEmployeeId( $boolIsRequired = true ) {
		$boolIsValid = true;

		if( false == is_numeric( $this->getSalesEmployeeId() ) && true == $boolIsRequired ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sales_employee_id', 'Sales closer is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSupportEmployeeId( $boolIsRequired = true ) {
		$boolIsValid = true;

		if( false == is_numeric( $this->getSupportEmployeeId() ) && true == $boolIsRequired ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'support_employee_id', 'CSM is required.' ) );
		}

		return $boolIsValid;
	}

	public function valImplementationEmployeeId( $objDatabase = NULL, $boolIsRequired = true, $objUser = NULL, $objEmployee = NULL ) {
		$boolIsValid = true;

		if( false == is_numeric( $this->getImplementationEmployeeId() ) && true == $boolIsRequired ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'implementation_employee_id', 'Implementation is required.' ) );
		}

		$objPsLeadDetail = CPsLeadDetails::createService()->fetchPsLeadDetailById( $this->getId(), $objDatabase );

		if( false == in_array( $objEmployee->getDepartmentId(), CDepartment::$c_arrintImplementationDepartmentIds ) && false == $objUser->getIsSuperUser() && false == $objUser->getIsAdministrator() && 0 != $this->getImplementationEmployeeId() && $objPsLeadDetail->getImplementationEmployeeId() != $this->getImplementationEmployeeId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'implementation_employee_id', 'You are not authorized to update implementation manager.' ) );
		}

		return $boolIsValid;
	}

	public function valTrainingEmployeeId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == isset( $objDatabase ) && 0 != $this->getTrainingEmployeeId() ) {
			$intEmployeeCount = CEmployees::fetchCurrentEmployeeCountByEmployeeId( $this->getTrainingEmployeeId(), $objDatabase );

			if( 0 == $intEmployeeCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'training_employee_id', 'Unauthorized trainer is assigned.' ) );
			}
		}

		return $boolIsValid;

	}

	public function valExecutiveSponsorId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == isset( $objDatabase ) && 0 != $this->getExecutiveSponsorId() ) {
			$intEmployeeCount = CEmployees::fetchCurrentEmployeeCountByEmployeeId( $this->getExecutiveSponsorId(), $objDatabase );

			if( 0 == $intEmployeeCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'training_employee_id', 'Unauthorized executive sponsor is assigned.' ) );
			}
		}

		return $boolIsValid;

	}

	public function valSeoEmployeeId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == isset( $objDatabase ) && 0 != $this->getSeoEmployeeId() ) {
			$intEmployeeCount = CEmployees::fetchActiveEmployeeCountByDepartmentIdsByEmployeeId( CDepartment::$c_arrintSEOEmployeeDepartmentIds, $this->getSeoEmployeeId(), $objDatabase );

			if( 0 == $intEmployeeCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'seo_employee_id', 'Unauthorized SEO employee is assigned.' ) );
			}
		}
		return $boolIsValid;

	}

	public function valTerminationNotes() {

		$boolIsValid = true;
		if( true == is_null( $this->getTerminationNotes() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_reason', 'Termination reason is required.' ) );
		}
		return $boolIsValid;
	}

	public function valContractTerminationReasonId() {
		$boolIsValid = true;
		if( true == is_null( $this->getContractTerminationReasonId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_termination_reason_id', 'Termination reason type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valUtilityBillingEmployeeId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == isset( $objDatabase ) && 0 != $this->getUtilityBillingEmployeeId() ) {
			$intEmployeeCount = CEmployees::fetchCurrentEmployeeCountByEmployeeId( $this->getUtilityBillingEmployeeId(), $objDatabase );

			if( 0 == $intEmployeeCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_billing_employee_id', 'Unauthorized utility billing employee is assigned.' ) );
			}
		}

		return $boolIsValid;

	}

	public function valPsLeadStageReasonId( $intPsLeadStageId ) {
		$boolIsValid = true;
			if( valId( $intPsLeadStageId ) && false == valId( $this->getPsLeadStageReasonId() ) && \CPsLeadStage::PROSPECTING_HOLD == $intPsLeadStageId ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_lead_stage_reason_id', 'Lead stage reason required.' ) );
			}

		return $boolIsValid;
	}

	public function validate( $strAction, $objUser = NULL, $objEmployee = NULL, $objDatabase = NULL, $intPsLeadStageId = NULL, $intContractStatusTypeId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_update_for_company_status_client':
			$boolIsValid &= $this->valSalesEmployeeId();
			$boolIsValid &= $this->valImplementationEmployeeId();
			$boolIsValid &= $this->valSupportEmployeeId();
				break;

			case 'validate_termination':
			$boolIsValid &= $this->valTerminationNotes();
			$boolIsValid &= $this->valContractTerminationReasonId();
				break;

			case 'validate_update_assignment_on_client_page':
			$boolIsValid &= $this->valSalesEmployeeId( false );
			$boolIsValid &= $this->valImplementationEmployeeId( $objDatabase, false, $objUser, $objEmployee );
			$boolIsValid &= $this->valSupportEmployeeId( false );
			$boolIsValid &= $this->valTrainingEmployeeId( $objDatabase );
			$boolIsValid &= $this->valExecutiveSponsorId( $objDatabase );
			$boolIsValid &= $this->valSeoEmployeeId( $objDatabase );
			$boolIsValid &= $this->valUtilityBillingEmployeeId( $objDatabase );
				break;

			case 'validate_update_insert_on_client_page':
				$boolIsValid &= $this->valPsLeadStageReasonId( $intPsLeadStageId );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid = true;

		if( true == is_numeric( $this->getSalesEmployeeId() ) ) {
			// Create an employee associations log
			$objEmployee = CEmployees::fetchEmployeeById( $this->getSalesEmployeeId(), $objDatabase );
			$objSalesEmployeeAssociation = $this->createEmployeeAssociation();
			$objSalesEmployeeAssociation->setDepartmentId( $objEmployee->getDepartmentId() );
			$objSalesEmployeeAssociation->setEmployeeId( $this->getSalesEmployeeId() );

			$boolIsValid &= $objSalesEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( true == is_numeric( $this->getSupportEmployeeId() ) ) {
			// Create an employee associations log
			$objAccountManagementEmployeeAssociation = $this->createEmployeeAssociation();
			$objAccountManagementEmployeeAssociation->setDepartmentId( CDepartment::SUCCESS_MANAGEMENT );
			$objAccountManagementEmployeeAssociation->setEmployeeId( $this->getSupportEmployeeId() );

			$boolIsValid &= $objAccountManagementEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( true == is_numeric( $this->getImplementationEmployeeId() ) ) {
			// Create an employee associations log
			$objEmployee = CEmployees::fetchEmployeeById( $this->getImplementationEmployeeId(), $objDatabase );
			$objImplementationManagerEmployeeAssociation = $this->createEmployeeAssociation();
			$objImplementationManagerEmployeeAssociation->setDepartmentId( $objEmployee->getDepartmentId() );
			$objImplementationManagerEmployeeAssociation->setEmployeeId( $this->getImplementationEmployeeId() );

			$boolIsValid &= $objImplementationManagerEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( true == is_numeric( $this->getLeasingCenterEmployeeId() ) ) {
			// Create an employee associations log
			$objLeasingCenterEmployeeAssociation = $this->createEmployeeAssociation();
			$objLeasingCenterEmployeeAssociation->setDepartmentId( CDepartment::CALL_CENTER );
			$objLeasingCenterEmployeeAssociation->setEmployeeId( $this->getLeasingCenterEmployeeId() );

			$boolIsValid &= $objLeasingCenterEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		$boolIsValid = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid = true;

		$objOriginalPsLeadDetail = CPsLeadDetails::createService()->fetchPsLeadDetailById( $this->getId(), $objDatabase );

		if( true == valObj( $objOriginalPsLeadDetail, 'CPsLeadDetail' ) && $objOriginalPsLeadDetail->getSalesEmployeeId() != $this->getSalesEmployeeId() && true == is_numeric( $this->getSalesEmployeeId() ) ) {
			// Create an employee associations log
			$objEmployee = CEmployees::fetchEmployeeById( $this->getSalesEmployeeId(), $objDatabase );
			$objSalesEmployeeAssociation = $this->createEmployeeAssociation();
			$objSalesEmployeeAssociation->setDepartmentId( $objEmployee->getDepartmentId() );
			$objSalesEmployeeAssociation->setEmployeeId( $this->getSalesEmployeeId() );

			$boolIsValid &= $objSalesEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( true == valObj( $objOriginalPsLeadDetail, 'CPsLeadDetail' ) && $objOriginalPsLeadDetail->getSupportEmployeeId() != $this->getSupportEmployeeId() && true == is_numeric( $this->getSupportEmployeeId() ) ) {
			// Create an employee associations log
			$objAccountManagementEmployeeAssociation = $this->createEmployeeAssociation();
			$objAccountManagementEmployeeAssociation->setDepartmentId( CDepartment::SUCCESS_MANAGEMENT );
			$objAccountManagementEmployeeAssociation->setEmployeeId( $this->getSupportEmployeeId() );

			$boolIsValid &= $objAccountManagementEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( true == valObj( $objOriginalPsLeadDetail, 'CPsLeadDetail' ) && $objOriginalPsLeadDetail->getImplementationEmployeeId() != $this->getImplementationEmployeeId() && true == valId( $this->getImplementationEmployeeId() ) ) {
			// Create an employee associations log
			$objEmployee = CEmployees::fetchEmployeeById( $this->getImplementationEmployeeId(), $objDatabase );
			$objImplementationManagerAssociation = $this->createEmployeeAssociation();
			$objImplementationManagerAssociation->setDepartmentId( $objEmployee->getDepartmentId() );
			$objImplementationManagerAssociation->setEmployeeId( $this->getImplementationEmployeeId() );

			$boolIsValid &= $objImplementationManagerAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( true == valObj( $objOriginalPsLeadDetail, 'CPsLeadDetail' ) && $objOriginalPsLeadDetail->getBillingEmployeeId() != $this->getBillingEmployeeId() && true == valId( $this->getBillingEmployeeId() ) ) {
			// Create an employee associations log
			$objBillingManagerAssociation = $this->createEmployeeAssociation();
			$objBillingManagerAssociation->setDepartmentId( CDepartment::ACCOUNTING );
			$objBillingManagerAssociation->setEmployeeId( $this->getBillingEmployeeId() );

			$boolIsValid &= $objBillingManagerAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( true == valObj( $objOriginalPsLeadDetail, 'CPsLeadDetail' ) && $objOriginalPsLeadDetail->getSeoEmployeeId() != $this->getSeoEmployeeId() && true == valId( $this->getSeoEmployeeId() ) ) {
			// Create an employee associations log
			$objSeoManagerAssociation = $this->createEmployeeAssociation();
			$objSeoManagerAssociation->setDepartmentId( CDepartment::SEO_SERVICES );
			$objSeoManagerAssociation->setEmployeeId( $this->getSeoEmployeeId() );

			$boolIsValid &= $objSeoManagerAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( true == valObj( $objOriginalPsLeadDetail, 'CPsLeadDetail' ) && $objOriginalPsLeadDetail->getLeasingCenterEmployeeId() != $this->getLeasingCenterEmployeeId() && true == valId( $this->getLeasingCenterEmployeeId() ) ) {
			// Create an employee associations log
			$objLeasingCenterEmployeeAssociation = $this->createEmployeeAssociation();
			$objLeasingCenterEmployeeAssociation->setDepartmentId( CDepartment::CALL_CENTER );
			$objLeasingCenterEmployeeAssociation->setEmployeeId( $this->getLeasingCenterEmployeeId() );

			$boolIsValid &= $objLeasingCenterEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( valObj( $objOriginalPsLeadDetail, 'CPsLeadDetail' ) && valId( $this->getTrainingEmployeeId() ) && $objOriginalPsLeadDetail->getTrainingEmployeeId() != $this->getTrainingEmployeeId() ) {

			$objTrainingEmployeeAssociation = $this->createEmployeeAssociation();
			$objTrainingEmployeeAssociation->setDepartmentId( CDepartment::TRAINING );
			$objTrainingEmployeeAssociation->setEmployeeId( $this->getTrainingEmployeeId() );

			$boolIsValid &= $objTrainingEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( valObj( $objOriginalPsLeadDetail, 'CPsLeadDetail' ) && valId( $this->getUtilityBillingEmployeeId() ) && $objOriginalPsLeadDetail->getUtilityBillingEmployeeId() != $this->getUtilityBillingEmployeeId() ) {

			$objUtilityBillingEmployeeAssociation = $this->createEmployeeAssociation();
			$objUtilityBillingEmployeeAssociation->setDepartmentId( CDepartment::UTILITY_BILLING );
			$objUtilityBillingEmployeeAssociation->setEmployeeId( $this->getUtilityBillingEmployeeId() );

			$boolIsValid &= $objUtilityBillingEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( valObj( $objOriginalPsLeadDetail, 'CPsLeadDetail' ) && valId( $this->getUtilityManagementEmployeeId() ) && $objOriginalPsLeadDetail->getUtilityManagementEmployeeId() != $this->getUtilityManagementEmployeeId() ) {

			$objUtilityManagementEmployeeAssociation = $this->createEmployeeAssociation();
			$objUtilityManagementEmployeeAssociation->setDepartmentId( CDepartment::UTILITY_MANAGEMENT );
			$objUtilityManagementEmployeeAssociation->setEmployeeId( $this->getUtilityManagementEmployeeId() );

			$boolIsValid &= $objUtilityManagementEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		if( valObj( $objOriginalPsLeadDetail, 'CPsLeadDetail' ) && valId( $this->getUtilityManagerId() ) && $objOriginalPsLeadDetail->getUtilityManagerId() != $this->getUtilityManagerId() ) {

			$objUtilityManagementEmployeeAssociation = $this->createEmployeeAssociation();
			$objUtilityManagementEmployeeAssociation->setDepartmentId( CDepartment::UTILITY_MANAGEMENT );
			$objUtilityManagementEmployeeAssociation->setEmployeeId( $this->getUtilityManagerId() );

			$boolIsValid &= $objUtilityManagementEmployeeAssociation->insert( $intCurrentUserId, $objDatabase );
		}

		$boolIsValid &= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		return $boolIsValid;
	}

}
?>