<?php

class CVirtualForeignKeyLog extends CBaseVirtualForeignKeyLog {

	protected $m_strClusterName;
	protected $m_strDataBaseName;

	/**
	* Get Functions
	*
	*/

	public function getClusterName() {
		return $this->m_strClusterName;
	}

	public function getDataBaseName() {
		return $this->m_strDataBaseName;
	}

	public function getReadableInvalidValues() {
		return preg_replace( '/"(\d+),(\d+)"/', '($1,$2)', str_replace( '","', '", "', $this->getInvalidValues() ) );
	}

	/**
	* Set Functions
	*
	*/

	public function setClusterName( $strClusterName ) {
		$this->m_strClusterName = $strClusterName;
	}

	public function setDataBaseName( $strDataBaseName ) {
		$this->m_strDataBaseName = $strDataBaseName;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}
}
?>