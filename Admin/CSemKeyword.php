<?php

class CSemKeyword extends CBaseSemKeyword {

	protected $m_fltAvgCostPerClick;
	protected $m_intSemKeywordAssociationId;
	protected $m_strStateCode;
	protected $m_boolIsPublished;

	/**
	 * Set Functions
	 */

	public function getAvgCostPerClick() {
		return $this->m_fltAvgCostPerClick;
	}

	public function getSemKeywordAssociationId() {
		return $this->m_intSemKeywordAssociationId;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	/**
	 * Set Functions
	 */

	public function setAvgCostPerClick( $fltAvgCostPerClick ) {
		$this->m_fltAvgCostPerClick = round( $fltAvgCostPerClick, 2 );
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setSemKeywordAssociationId( $intSemKeywordAssociationId ) {
		$this->m_intSemKeywordAssociationId = $intSemKeywordAssociationId;
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->m_boolIsPublished = $boolIsPublished;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
 		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['avg_cost_per_click'] ) ) $this->setAvgCostPerClick( $arrmixValues['avg_cost_per_click'] );
		if( true == isset( $arrmixValues['sem_keyword_association_id'] ) ) $this->setSemKeywordAssociationId( $arrmixValues['sem_keyword_association_id'] );
		if( true == isset( $arrmixValues['state_code'] ) ) $this->setStateCode( $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['is_published'] ) ) $this->setIsPublished( $arrmixValues['is_published'] );
		return;
	}

	public function delete( $intUserId, $objAdminDatabase, $boolReturnSqlOnly = false ) {
		$boolIsValid = true;

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objAdminDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Validate Functions
	 */

	public function valKeywords( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getKeywords() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keywords', 'Keyword is required.' ) );
		} elseif( 0 < CSemKeywords::fetchSemKeywordCount( 'WHERE id <>' . ( int ) $this->getId() . ' AND sem_ad_group_id = ' . ( int ) $this->getSemAdGroupId() . ' AND lower( keywords ) = lower(\'' . trim( addslashes( $this->getKeywords() ) ) . '\')', $objAdminDatabase ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keywords', $this->getKeywords() . ' is already being used.', 319 ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valKeywords( $objAdminDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createSemKeywordAssociation() {

		$objSemKeywordAssociation = new CSemKeywordAssociation();
		$objSemKeywordAssociation->setSemKeywordId( $this->getId() );

		return $objSemKeywordAssociation;
	}

	public function createSemKeywordSource() {

		$objSemKeywordSource = new CSemKeywordSource();
		$objSemKeywordSource->setSemKeywordId( $this->getId() );

		return $objSemKeywordSource;
	}

	/**
	 * Other Functions
	 */

	public function fetchSemKeywordSourceBySemSourceId( $intSemSourceId, $objAdminDatabase ) {
		return CSemKeywordSources::fetchSemKeywordSourceBySemKeywordIdBySemAdGroupIdBySemSourceId( $this->getId(), $this->getSemAdGroupId(), $intSemSourceId, $objAdminDatabase );
	}

	public function fetchSemKeywordAssociations( $objAdminDatabase ) {
		return CSemKeywordAssociations::fetchSemKeywordAssociationsBySemKeywordId( $this->getId(), $objAdminDatabase );
	}

	public function fetchSemKeywordSources( $objAdminDatabase ) {
		return CSemKeywordSources::fetchSemKeywordSourcesBySemKeywordId( $this->getId(), $objAdminDatabase );
	}

}
?>