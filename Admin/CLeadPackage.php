<?php

class CLeadPackage extends CBaseLeadPackage {

	protected $m_intPackagePropertyCount;
	protected $m_intPackageUnitCount;
	protected $m_strLeadPackageId;
	protected $m_strLeadPackageTitle;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsLeadId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle( $objDatabase ) {

		if( !valStr( trim( $this->getTitle() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Package Title is required.' ) );
			return false;
		}

		if( valObj( $objDatabase, 'CDatabase' ) ) {
			$arrobjLeadPackages = \Psi\Eos\Admin\CLeadPackages::createService()->fetchLeadPackagesByPsLeadIdByTitle( $this->getPsLeadId(), $this->getTitle(), $objDatabase, $this->getId() );
			if( valArr( $arrobjLeadPackages ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Package Title already exists.' ) );
				return false;
			}
		}

		return true;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $arrmixPackage = NULL, $arrmixDiscounts = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valTitle( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_request_data':
				$boolIsValid = $this->validateRequestData( $arrmixPackage, $arrmixDiscounts, $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function validateRequestData( $arrmixPackage, $arrmixDiscounts, $objDatabase ) {

		// Package details validation

		if( !valStr( trim( $arrmixPackage['title'] ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Package Title is required.' ) );
		}

		if( !valId( $arrmixPackage['property_count'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Number of Properties are required.' ) );
		}

		if( !valId( $arrmixPackage['unit_count'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Number of Units are required.' ) );
		}

		if( $arrmixPackage['unit_count'] < $arrmixPackage['property_count'] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Number of Units should be greater than or equal to the Number of Properties.' ) );
		}

		if( !isset( $arrmixPackage['line_items'] ) && !isset( $arrmixPackage['bundles'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Minimum one product or bundle is required in the package.' ) );
		}

		if( valArr( $this->getErrorMsgs() ) ) {
			return false;
		}

		// Products tab validation

		$arrintAllPsProductIds = [];

		$arrobjPsProducts			= ( array ) \Psi\Eos\Admin\CPsProducts::createService()->fetchPackagePsProducts( $objDatabase );
		$arrmixProductDependencies	= ( array ) \Psi\Eos\Admin\CProductDependencies::createService()->fetchProductsDependencies( $objDatabase );

		$arrintProductDependency	= ( isset( $arrmixProductDependencies['product_dependency'] ) && valArr( $arrmixProductDependencies['product_dependency'] ) ? $arrmixProductDependencies['product_dependency'] : [] );
		$arrboolBundleDependency	= ( isset( $arrmixProductDependencies['bundle_dependency'] ) && valArr( $arrmixProductDependencies['bundle_dependency'] ) ? $arrmixProductDependencies['bundle_dependency'] : [] );

		if( isset( $arrmixPackage['line_items'] ) && valArr( $arrmixPackage['line_items'] ) ) {

			foreach( $arrmixPackage['line_items'] as $intIndex => $intPsProductId ) {

				if( !valId( $intPsProductId ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Select a product in Line Items or remove the empty row added.' ) );
					continue;
				}

				if( isset( $arrmixPackage['payment_types'][$intIndex] ) && CContractProduct::PAYMENT_TYPE_MIN_MAX == $arrmixPackage['payment_types'][$intIndex] ) {
					if( !valId( $arrmixPackage['payment_min_units'][$intIndex] ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Min Units are required.' ) );
					}

					if( !valId( $arrmixPackage['payment_max_units'][$intIndex] ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Max Units are required.' ) );
					}

					if( $arrmixPackage['payment_min_units'][$intIndex] > $arrmixPackage['payment_max_units'][$intIndex] ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Min Units should be less than or equal to the Max Units.' ) );
					}
				}

				$arrintAllPsProductIds[] = $intPsProductId;

				if( array_key_exists( $intPsProductId, $arrintProductDependency ) && valId( $arrintProductDependency[$intPsProductId] ) && !in_array( $arrintProductDependency[$intPsProductId], $arrmixPackage['line_items'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'You are adding "%s" in Line Items, you also need to add "%s" in Line items.', $arrobjPsProducts[$intPsProductId]->getName(), $arrobjPsProducts[$arrintProductDependency[$intPsProductId]]->getName() ) ) );
				}
			}
		}

		if( isset( $arrmixPackage['bundles'] ) && valArr( $arrmixPackage['bundles'] ) ) {

			foreach( $arrmixPackage['bundles'] as $arrmixBundleDetails ) {

				if( !valstr( $arrmixBundleDetails['name'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Bundle Name is required.' ) );
					continue;
				}

				$strBundleName = trim( $arrmixBundleDetails['name'] );

				if( !isset( $arrmixBundleDetails['product_ids'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Minimum one product is required in "%s".', $strBundleName ) ) );
					continue;
				}

				if( isset( $arrmixBundleDetails['type'] ) && CContractProduct::PAYMENT_TYPE_MIN_MAX == $arrmixBundleDetails['type'] ) {
					if( !valId( $arrmixBundleDetails['min_units'] ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Min Units are required.' ) );
					}

					if( !valId( $arrmixBundleDetails['max_units'] ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Max Units are required.' ) );
					}

					if( $arrmixBundleDetails['min_units'] > $arrmixBundleDetails['max_units'] ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Min Units should be less than or equal to the Max Units.' ) );
					}
				}

				if( valArr( $arrmixBundleDetails['product_ids'] ) ) {

					foreach( $arrmixBundleDetails['product_ids'] as $intPsProductId ) {

						if( !valId( $intPsProductId ) ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Select a product in "%s" or remove the empty row added.', $strBundleName ) ) );
							continue;
						}

						if( valObj( $arrobjPsProducts[$intPsProductId], 'CPsProduct' ) && !$arrobjPsProducts[$intPsProductId]->getAllowInBundle() ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Product "%s" is not allowed in "%s".', $arrobjPsProducts[$intPsProductId]->getName(), $strBundleName ) ) );
							continue;
						}

						$arrintAllPsProductIds[] = $intPsProductId;

						if( array_key_exists( $intPsProductId, $arrboolBundleDependency ) && true == $arrboolBundleDependency[$intPsProductId] && !in_array( $arrintProductDependency[$intPsProductId], $arrmixBundleDetails['product_ids'] ) ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'You are adding "%s" in "%s", you also need to add "%s" in the bundle.', $arrobjPsProducts[$intPsProductId]->getName(), $strBundleName, $arrobjPsProducts[$arrintProductDependency[$intPsProductId]]->getName() ) ) );
						}
					}
				}
			}
		}

		if( valArr( $arrintAllPsProductIds ) ) {

			$arrintRepeatedPsProductIds = array_count_values( $arrintAllPsProductIds );

			array_walk( $arrintRepeatedPsProductIds, function ( $intRepeatedCount, $intPsProductId ) use ( &$arrobjPsProducts ) {

				if( 1 < $intRepeatedCount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Product "%s" is repeated in the package.', ( valObj( $arrobjPsProducts[$intPsProductId], 'CPsProduct' ) ? $arrobjPsProducts[$intPsProductId]->getName() : NULL ) ) ) );
				}
			} );
		}

		if( valArr( $this->getErrorMsgs() ) ) {
			return false;
		}

		// Discounts tab validation

		if( valArr( $arrmixDiscounts ) ) {

			$arrmixLineItemsTotal	= [];
			$arrmixBundlesTotal		= [];

			foreach( $arrmixDiscounts as $intIndex => $arrmixDiscount ) {

				if( !valId( $arrmixDiscount['applied_product_id'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Select a product or remove the empty discount row added.' ) );
				}

				if( !is_numeric( $arrmixDiscount['setup_percentage'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Setup percentage are required.' ) );
				}

				if( $arrmixDiscount['setup_percentage'] > 100 || $arrmixDiscount['setup_percentage'] < 0 ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Valid Setup percentage are required.' ) );
				}

				if( !is_numeric( $arrmixDiscount['recurring_percentage'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Recurring percentage are required.' ) );
				}

				if( $arrmixDiscount['recurring_percentage'] > 100 || $arrmixDiscount['recurring_percentage'] < 0 ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Valid Recurring percentage are required.' ) );
				}

				if( 'line_item' == $arrmixDiscount['applied_product_type'] ) {
					$arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup']		= $arrmixDiscount['setup_percentage'] + ( isset( $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup'] ) ? $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup'] : 0 );
					$arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring']	= $arrmixDiscount['recurring_percentage'] + ( isset( $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring'] ) ? $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring'] : 0 );

					$arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup_available']		= ( isset( $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup_available'] ) ? $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup_available'] : $arrmixDiscount['setup_available'] );
					$arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring_available']	= ( isset( $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring_available'] ) ? $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring_available'] : $arrmixDiscount['recurring_available'] );
				}

				if( 'bundle' == $arrmixDiscount['applied_product_type'] ) {
					$arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup']		= $arrmixDiscount['setup_percentage'] + ( isset( $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup'] ) ? $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup'] : 0 );
					$arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring']	= $arrmixDiscount['recurring_percentage'] + ( isset( $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring'] ) ? $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring'] : 0 );

					$arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup_available']		= ( isset( $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup_available'] ) ? $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup_available'] : $arrmixDiscount['setup_available'] );
					$arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring_available']	= ( isset( $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring_available'] ) ? $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring_available'] : $arrmixDiscount['recurring_available'] );
				}
			}

			if( valArr( $this->getErrorMsgs() ) ) {
				return false;
			}

		}

		return ( $this->getErrorMsgs() ? false : true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		// Custom Data
		if( isset( $arrmixValues['package_property_count'] ) ) {
			$this->setPackagePropertyCount( trim( $arrmixValues['package_property_count'] ) );
		}

		if( isset( $arrmixValues['package_unit_count'] ) ) {
			$this->setPackageUnitCount( trim( $arrmixValues['package_unit_count'] ) );
		}

		if( true == isset( $arrmixValues['title'] ) && 0 < strlen( trim( $arrmixValues['title'] ) ) ) {
			$this->setLeadPackageTitle( $arrmixValues['title'] );
		}
		if( true == isset( $arrmixValues['id'] ) ) $this->setLeadPackageId( $arrmixValues['id'] );
	}

	public function setPackagePropertyCount( $intPackagePropertyCount ) {
		$this->m_intPackagePropertyCount = $intPackagePropertyCount;
	}

	public function setPackageUnitCount( $intPackageUnitCount ) {
		$this->m_intPackageUnitCount = $intPackageUnitCount;
	}

	public function setLeadPackageId( $intLeadPackageId ) {
		$this->m_strLeadPackageId = $intLeadPackageId;
	}

	public function setLeadPackageTitle( $strTitle ) {
		$this->m_strLeadPackageTitle = $strTitle;
	}

	public function getPackagePropertyCount() {
		return $this->m_intPackagePropertyCount;
	}

	public function getPackageUnitCount() {
		return $this->m_intPackageUnitCount;
	}

	public function getLeadPackageTitle() {
		return $this->m_strLeadPackageTitle;
	}

	public function getLeadPackageId() {
		return $this->m_strLeadPackageId;
	}

}
?>