<?php

class COkrKeyResultLog extends CBaseOkrKeyResultLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOkrKeyResultId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOkrKeyResultStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOwnerEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKeyResultDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProgress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOnTrack() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeliveryDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDeleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>