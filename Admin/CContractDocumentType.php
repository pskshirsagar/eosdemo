<?php

class CContractDocumentType extends CBaseContractDocumentType {

	const SIGNED_CONTRACT		= 1;
	const CUSTOM_WORK_ADDENDUM	= 2;
	const REDLINED_CONTRACT		= 3;
	const BASE_DOCUMENT_WORD	= 4;
	const BASE_DOCUMENT_PDF		= 5;
	const INITIAL_CONTRACT		= 6;
	const OTHER					= 7;
	const CONSULTATION_SUMMARY	= 8;
	const CLOSE_OUT_SUMMARY		= 9;
	const IMPLEMENTATION		= 10;
	const BILLING_REQUEST		= 11;
	const CONTRACT_PRICING		= 12;
	const E_SIGN_CONTRACT		= 13;

	public static $c_arrintClientSummaryDocumentTypes = [
		self::CONSULTATION_SUMMARY,
		self::CLOSE_OUT_SUMMARY,
		self::IMPLEMENTATION
	];

	public static $c_arrintEntrataContractDocumentTypeIds = [
		self::INITIAL_CONTRACT,
		self::REDLINED_CONTRACT,
		self::SIGNED_CONTRACT,
		self::E_SIGN_CONTRACT
	];

	public static $c_arrintSignedContractDocumentTypeIds = [
		self::SIGNED_CONTRACT,
		self::E_SIGN_CONTRACT
	];

}
?>