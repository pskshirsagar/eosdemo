<?php

class CAdEnvironment extends CBaseAdEnvironment {

	const RESIDENT_WORKS_AD 	= 1;
	const MANAGER_EMAILS 		= 2;
	const RESIDENT_EMAILS 		= 3;
	const PROSPECT_EMAILS 		= 4;
	const ADMINISTRATOR_EMAILS 	= 5;
	const INVOICE_AD 			= 6;
}
?>