<?php

class CTrainingSessionCompanyGoal extends CBaseTrainingSessionCompanyGoal {

	public function valTrainingSessionId() {
		$boolIsValid = true;
		if( true == is_null( $this->getTrainingSessionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'training session id', 'training session is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCompanyGoalId() {
		$boolIsValid = true;
		if( true == is_null( $this->getCompanyGoalId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company goal id', 'company goal is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTrainingSessionId();
				$boolIsValid &= $this->valCompanyGoalId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>