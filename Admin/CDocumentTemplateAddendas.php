<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplateAddendas
 * Do not add any new functions to this class.
 */

class CDocumentTemplateAddendas extends CBaseDocumentTemplateAddendas {

	public static function fetchNonArchivedDocumentTemplateAddendasByLeaseDocumentTemplateId( $intLeaseDocumentTemplateId, $objDatabase ) {
		$strSql = 'SELECT * FROM document_template_addendas WHERE lease_document_template_id = ' . ( int ) $intLeaseDocumentTemplateId . ' AND archived_on IS NULL ORDER BY order_num';
		return self::fetchDocumentTemplateAddendas( $strSql, $objDatabase );
	}

}
?>