<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingRecruiters
 * Do not add any new functions to this class.
 */

class CJobPostingRecruiters extends CBaseJobPostingRecruiters {

	public static function fetchJobPostingRecruitersDetailByJobPostingId( $intJobPostingId, $objDatabase ) {
		if( false == is_numeric( $intJobPostingId ) ) return NULL;

		$strSql = 'SELECT
						jpr.*,
						e.email_address,
						e.preferred_name
					FROM
						job_posting_recruiters jpr
						LEFT JOIN employees e ON ( jpr.recruiter_employee_id = e.id )
					WHERE
						jpr.job_posting_id = ' . ( int ) $intJobPostingId . '';

		return fetchData( $strSql, $objDatabase );
	}

}
?>