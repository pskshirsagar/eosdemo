<?php

class CPayrollPeriod extends CBasePayrollPeriod {

	public function valBeginDate( $objDatabase, $intPayrollPeriodId, $boolIsUpdate ) {
		$boolIsValid = true;

		if( true == is_null( $this->getBeginDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_date', 'Please enter start date.' ) );
		}

		if( false == $boolIsUpdate ) {
			$objPayrollPeriod = CPayrollPeriods::fetchLatestPayrollPeriodByCountryCode( CCountry::CODE_INDIA, $objDatabase );
		} else {
			$objPayrollPeriod = CPayrollPeriods::fetchPreviousPayrollPeriodByIdByCountryCode( $intPayrollPeriodId, CCountry::CODE_INDIA, $objDatabase );
		}

		if( true == valObj( $objPayrollPeriod, 'CPayrollPeriod' ) && true == valStr( $this->getBeginDate() ) && strtotime( $objPayrollPeriod->getEndDate() ) >= strtotime( $this->getBeginDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_date', 'Start date should be greater than previous payroll period\'s end date.' ) );
		}

		return $boolIsValid;
	}

	public function valEndDate( $objDatabase, $intPayrollPeriodId, $boolIsUpdate ) {
		$boolIsValid = true;

		if( true == is_null( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Please enter end date.' ) );
		}

		if( false == is_null( $this->getBeginDate() ) && false == is_null( $this->getEndDate() ) ) {
			if( strtotime( $this->getBeginDate() ) > strtotime( $this->getEndDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date should be greater than start date.' ) );
			}
		}

		if( true == $boolIsUpdate ) {
			$objNextPayrollPeriod = CPayrollPeriods::fetchNextPayrollPeriodByIdByCountryCode( $intPayrollPeriodId, CCountry::CODE_INDIA, $objDatabase );

			if( true == valObj( $objNextPayrollPeriod, 'CPayrollPeriod' ) && true == valStr( $this->getEndDate() ) && $objNextPayrollPeriod->getBeginDate() <= $this->getEndDate() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date should be less than next payroll period\'s start date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDeletePayroll( $objDatabase ) {

		$boolIsValid = true;

		$arrobjEmployeePayrolls = CEmployeePayrolls::fetchEmployeePayrollsByPayrollPeriodId( $this->getId(), $objDatabase );

		if( true == valArr( $arrobjEmployeePayrolls ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'You can not delete this payroll period because its used in employee_payrolls' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $intPayrollPeriodId = NULL,  $boolIsUpdate = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBeginDate( $objDatabase, $intPayrollPeriodId, $boolIsUpdate );
				$boolIsValid &= $this->valEndDate( $objDatabase, $intPayrollPeriodId, $boolIsUpdate );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeletePayroll( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function sendInternalPayrollPeriodNotification( $intPayrollPeriodId, $arrstrPreviousPayrollPeriod, $objUser, $objEmailDatabase ) {

		$strHtmlContent = '<dl><dt><strong>Hello Team,</strong></dt></dl>';

		if( true == is_numeric( $intPayrollPeriodId ) && true == valArr( $arrstrPreviousPayrollPeriod ) ) {
			$strHtmlContent .= '
								<dl><dt>HR team updated Payroll period.</dt></dl>
								<dl><dt style="width:80px;float:left"><b>Updated By: </b></dt><dd><b>' . $objUser->getEmployee()->getNameFull() . '</b></dd></dl>
								<dl><dt><b>Current</b></dt></dl>
								<dl><dt style="width:80px;float:left"><b>Strat Date: </b></dt><dd>' . $this->getBeginDate() . '</dd></dl>
								<dl><dt style="width:80px;float:left"><b>End Date: </b></dt><dd>' . $this->getEndDate() . '</dd></dl>
								<dl><dt><b>Previous</b></dt></dl>
								<dl><dt  style="width:80px;float:left"><b>Strat Date: </b></dt><dd>' . $arrstrPreviousPayrollPeriod['begin_date'] . '</dd></dl>
								<dl><dt  style="width:80px;float:left"><b>End Date: </b></dt><dd>' . $arrstrPreviousPayrollPeriod['end_date'] . '</dd></dl>';
		} elseif( false == is_numeric( $intPayrollPeriodId ) ) {
			$strHtmlContent .= '
								<dl><dt>HR team added new Payroll period.</dt></dl>
								<dl><dt style="width:80px;float:left"><b>Added By: </b></dt><dd><b>' . $objUser->getEmployee()->getNameFull() . '</b></dd></dl>
								<dl><dt style="width:80px;float:left"><b>Strat Date: </b></dt><dd>' . $this->getBeginDate() . '</dd></dl>
								<dl><dt style="width:80px;float:left"><b>End Date: </b></dt><dd>' . $this->getEndDate() . '</dd></dl>';

		} else {
			return false;
		}
		$strHtmlContent .= '<dl><dt></dt></dl>';

		$strToEmailAddress = 'mnaik@xento.com, mjadhav01@xento.com, psatre@xento.com, apawar03@xento.com';

		$strSubject = 'Payroll Period';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'content', $strHtmlContent );
		$objSmarty->assign( 'logo_url',				CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, $strHtmlContent, $strToEmailAddress, $intIsSelfDestruct = 0 );

		if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
			return false;
		}
		return true;
	}

}
?>