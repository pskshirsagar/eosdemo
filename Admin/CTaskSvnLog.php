<?php

class CTaskSvnLog extends CBaseTaskSvnLog {

	protected $m_intManagerId;

	public function setManagerId( $intManagerId ) {
		$this->m_intManagerId = $intManagerId;
	}

	public function getManagerId() {
		return $this->m_intManagerId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['manager_id'] ) ) {
			$this->setManagerId( $arrmixValues['manager_id'] );
		}
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>