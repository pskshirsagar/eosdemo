<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CRootCauseTypes
 * Do not add any new functions to this class.
 */

class CRootCauseTypes extends CBaseRootCauseTypes {

	public static function fetchRootCauseTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRootCauseType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchRootCauseType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRootCauseType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllRootCauses( $objDatabase, $intTaskTypeId = NULL ) {
		$strWhere = ' rca.task_type_id IS NULL';
		if( valId( $intTaskTypeId ) ) {
			$strWhere = ' rca.task_type_id = ' . ( int ) $intTaskTypeId;
		}
		$strSql = 'SELECT rct.* 
					FROM root_cause_types rct 
							LEFT JOIN root_cause_associations rca ON (rct.id = rca.root_cause_type_id) 
							WHERE  
							' . $strWhere . ' ORDER BY ' . $objDatabase->getCollateSort( 'rct.name' );
		return self::fetchRootCauseTypes( $strSql, $objDatabase );
	}

	public static function fetchRootCauseTypesByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return;

		$strSql = 'SELECT id,
						name
					FROM root_cause_types
					WHERE id IN ( ' . implode( ',', $arrintIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

}
?>