<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsProductModules
 * Do not add any new functions to this class.
 */

class CPsProductModules extends CBasePsProductModules {

	public static function fetchPsProductModulesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchPsProductModules( 'SELECT * FROM ps_product_modules WHERE ps_product_id = ' . ( int ) $intPsProductId . ' ORDER BY name ASC', $objDatabase );
	}

	public static function fetchPsProductModulesByIds( $arrintPsProductModuleIds, $objDatabase ) {
		if( false == valArr( $arrintPsProductModuleIds ) ) return NULL;
		$strSql = 'SELECT * FROM ps_product_modules WHERE id IN ( ' . implode( ',', $arrintPsProductModuleIds ) . ' ) ORDER BY name ASC';
		return self::fetchPsProductModules( $strSql, $objDatabase );
	}

	public static function writePsProductModulesToFileSystem( $objAdminDatabase ) {

		$arrstrPsProductModules = array();
		$arrstrPsProductModules	= fetchData( 'SELECT 	ppm.id,
									lower( ppm.name ) as name,
									ppm.ps_product_id,
									ppm.ps_product_option_id,
									ppm.dev_user_id,
									e.email_address
								FROM
									ps_product_modules ppm
									JOIN users u ON ( u.id = ppm.dev_user_id )
									JOIN employees e ON ( u.employee_id = e.id)', $objAdminDatabase );

		if( false == valArr( $arrstrPsProductModules ) ) {
			return false;
		}

		$arrstrPsProductModulesFinal = array();

		foreach( $arrstrPsProductModules as $strPsProductModule ) {
			$arrstrPsProductModulesFinal[\Psi\CStringService::singleton()->strtolower( $strPsProductModule['name'] . '_' . $strPsProductModule['ps_product_id'] )] = $strPsProductModule;
		}

		$strFileName = 'ps_product_modules.txt';

		CFileIo::recursiveMakeDir( PATH_NON_BACKUP_MOUNTS_GLOBAL_PS_PRODUCT_MODULES );

		if( !$resHandle = CFileIo::fileOpen( PATH_NON_BACKUP_MOUNTS_GLOBAL_PS_PRODUCT_MODULES . $strFileName, 'w' ) ) {
			return false;
		}

		// Write $somecontent to our opened file.
		if( false === CFileIo::writeTextFile( PATH_NON_BACKUP_MOUNTS_GLOBAL_PS_PRODUCT_MODULES . $strFileName, serialize( $arrstrPsProductModulesFinal ) ) ) {
			return false;
		}

		fclose( $resHandle );

		return $arrstrPsProductModules;
	}

	public static function loadPsProductModulesFromFileSystem() {

		$arrstrPsProductModules = array();

		if( false === file_exists( PATH_NON_BACKUP_MOUNTS_GLOBAL_PS_PRODUCT_MODULES . 'ps_product_modules.txt' ) ) {
			return $arrstrPsProductModules;
		}

		return unserialize( file_get_contents( PATH_NON_BACKUP_MOUNTS_GLOBAL_PS_PRODUCT_MODULES . 'ps_product_modules.txt' ) );
	}

	public static function getPsProdcutModules() {

		$arrstrPsProductModules = false;

		// Get serialzed array from Zend Shared Memory Cache
		$arrstrPsProductModules = CCache::fetchObject( 'cachedPsProductModules' );

		if( false === $arrstrPsProductModules ) {

			$arrstrPsProductModules = self::loadPsProductModulesFromFileSystem();

			// Storing data into cache for 1 hour
			CCache::storeObject( 'cachedPsProductModules', $arrstrPsProductModules, 3600 );
		}

		return $arrstrPsProductModules;
	}

	public static function fetchSearchedPsProductModules( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						ppm.*
					FROM
						ps_product_modules AS ppm
						JOIN ps_products AS ps ON ( ppm.ps_product_id = ps.id )
					WHERE
						ppm.deleted_by IS NULL
						AND ppm.is_published = 1
						AND ppm.name ILIKE \'%' . implode( '%\' AND ps.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
											OR ps.name ILIKE \'%' . implode( '%\' AND ppm.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
											order by id LIMIT 10';

		return self::fetchPsProductModules( $strSql, $objDatabase );

	}

	public static function fetchPaginatedPsProductModules( $intUserId, $intPageNo, $intPageSize, $objPsProductModulesFilter, $strOrderByField = NULL, $strOrderByType = NULL, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY ppm.updated_on DESC';
		}

		$strSql = 'SELECT
						ppm.*
					FROM ps_product_modules as ppm
						LEFT OUTER JOIN users as u ON ( ppm.dev_user_id = u.id )
						JOIN ps_products as ps ON ( ppm.ps_product_id = ps.id )
					WHERE
						ppm.deleted_by is NULL
						AND ppm.is_published = 1';

		if( true == valStr( $objPsProductModulesFilter->getUsers() ) ) {
			$strSql .= ' AND ( ppm.dev_user_id IN (' . $objPsProductModulesFilter->getUsers() . ') OR ppm.qa_user_id IN (' . $objPsProductModulesFilter->getUsers() . ' ) )';
		}

		if( true == valStr( $objPsProductModulesFilter->getPsProducts() ) && false == valStr( $objPsProductModulesFilter->getPsProductOptions() ) ) {
			$strSql .= ' AND ppm.ps_product_id IN ( ' . $objPsProductModulesFilter->getPsProducts() . ' ) AND ppm.ps_product_option_id IS NULL ';
		} elseif( true == valStr( $objPsProductModulesFilter->getPsProducts() ) && true == valStr( $objPsProductModulesFilter->getPsProductOptions() ) ) {
			$strSql .= ' AND ( ppm.ps_product_id IN ( ' . $objPsProductModulesFilter->getPsProducts() . ' ) OR ppm.ps_product_option_id IN ( ' . $objPsProductModulesFilter->getPsProductOptions() . ' ) ) ';
		} elseif( false == valStr( $objPsProductModulesFilter->getPsProducts() ) && true == valStr( $objPsProductModulesFilter->getPsProductOptions() ) ) {
			$strSql .= ' AND ppm.ps_product_option_id IN ( ' . $objPsProductModulesFilter->getPsProductOptions() . ' ) ';
		}

		$strSql .= $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
									LIMIT ' . $intLimit;

		return self::fetchPsProductModules( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPsProductModulesCount( $intUserId, $objPsProductModulesFilter, $objDatabase ) {

		$arrstrWhereParameters = array();
		$strKeyword = $objPsProductModulesFilter->getKeyword();

		( false == isset( $objPsProductModulesFilter ) || true == empty( $strKeyword ) ) ? false : array_push( $arrstrWhereParameters, "(name ILIKE '%" . $objPsProductModulesFilter->getKeyword() . "%' OR description ILIKE '%" . $objPsProductModulesFilter->getKeyword() . "%')" );

		$strSql = ' WHERE deleted_by is NULL AND is_published = 1 ';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			$strSql .= 'AND ';
			$strSql .= implode( ' AND ', $arrstrWhereParameters );
		}

		if( true == valStr( $objPsProductModulesFilter->getPsProducts() ) && false == valStr( $objPsProductModulesFilter->getPsProductOptions() ) ) {
			$strSql .= ' AND ps_product_id IN ( ' . $objPsProductModulesFilter->getPsProducts() . ' ) AND ps_product_option_id IS NULL ';
		} elseif( true == valStr( $objPsProductModulesFilter->getPsProducts() ) && true == valStr( $objPsProductModulesFilter->getPsProductOptions() ) ) {
			$strSql .= ' AND ( ps_product_id IN ( ' . $objPsProductModulesFilter->getPsProducts() . ' ) OR ps_product_option_id IN ( ' . $objPsProductModulesFilter->getPsProductOptions() . ' ) ) ';
		} elseif( false == valStr( $objPsProductModulesFilter->getPsProducts() ) && true == valStr( $objPsProductModulesFilter->getPsProductOptions() ) ) {
			$strSql .= ' AND ps_product_option_id IN ( ' . $objPsProductModulesFilter->getPsProductOptions() . ' ) ';
		}

		if( true == valStr( $objPsProductModulesFilter->getUsers() ) ) {
			$strSql .= ' AND ( dev_user_id IN (' . $objPsProductModulesFilter->getUsers() . ') OR qa_user_id IN (' . $objPsProductModulesFilter->getUsers() . ' ) )';
		}

		return self::fetchPsProductModuleCount( $strSql, $objDatabase );
	}

	public static function fetchPublishedPsProductModuleByPsProductIdByNameByFilePath( $intPsProductId, $strName, $strFilePath, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ps_product_modules
					WHERE
						ps_product_id = ' . ( int ) $intPsProductId . '
						AND name = \'' . CStrings::strTrimDef( addslashes( $strName ) ) . '\'
						AND file_path = \'' . trim( addslashes( $strFilePath ) ) . '\'
						AND is_published = 1
					LIMIT 1';

		return self::fetchPsProductModule( $strSql, $objDatabase );
	}

	public static function fetchPublishedPsProductModuleByApplicationLayerNameByNameByFilePath( $strApplicationLayerName, $strName, $strFilePath, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ps_product_modules
					WHERE
						application_layer_name = \'' . $strApplicationLayerName . '\'
						AND name = \'' . CStrings::strTrimDef( addslashes( $strName ) ) . '\'
						AND file_path = \'' . trim( addslashes( $strFilePath ) ) . '\'
						AND is_published = 1
					LIMIT 1';

		return self::fetchPsProductModule( $strSql, $objDatabase );
	}

	public static function fetchPsProductModulesByUserId( $intUserId, $objDatabase ) {

		if( false == is_numeric( $intUserId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ps_product_modules
					WHERE
						' . ( int ) $intUserId . ' IN ( dev_user_id, qa_user_id )';

		return self::fetchPsProductModules( $strSql, $objDatabase );
	}

	public static function fetchActivePsProductModules( $objDatabase ) {

		$strSql = ' SELECT
						ppm.application_layer_name,
						ppm.file_path,
						ppm.dev_user_id,
						ppm.qa_user_id
					FROM
						ps_product_modules as ppm
						LEFT OUTER JOIN users as u ON (ppm.dev_user_id = u.id)
						JOIN ps_products as ps ON (ppm.ps_product_id = ps.id)
					WHERE
						ppm.deleted_by is NULL AND
						ppm.is_published = 1 AND
						ppm.deprecated_on IS NULL
					ORDER BY
						ppm.updated_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

}
?>