<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestGroups
 * Do not add any new functions to this class.
 */

class CTestGroups extends CBaseTestGroups {

	public static function fetchTestsIdsbyAssignmentIds( $objTestsFilter, $objDatabase ) {

		$strWhere = '';

		if( true == valArr( $objTestsFilter->getTeamIds() ) )
			$strWhere .= 'team_id IN ( ' . implode( ',', $objTestsFilter->getTeamIds() ) . ' ) ';

		if( true == valArr( $objTestsFilter->getDepartmentIds() ) ) {
			if( true == empty( $strWhere ) ) {
				$strWhere .= '';
			} else {
				$strWhere .= 'OR';
			}
			$strWhere .= ' department_id IN ( ' . implode( ',', $objTestsFilter->getDepartmentIds() ) . ' ) ';
		}

		if( true == valArr( $objTestsFilter->getOfficeIds() ) ) {
			if( true == empty( $strWhere ) ) {
				$strWhere .= '';
			} else {
				$strWhere .= 'OR';
			}
			$strWhere .= ' office_id IN ( ' . implode( ',', $objTestsFilter->getOfficeIds() ) . ' ) ';
		}

		if( true == valArr( $objTestsFilter->getDesignationIds() ) ) {
			if( true == empty( $strWhere ) ) {
				$strWhere .= '';
			} else {
				$strWhere .= 'OR';
			}
			$strWhere .= ' designation_id IN ( ' . implode( ',', $objTestsFilter->getDesignationIds() ) . ' ) ';
		}

		if( true == valArr( $objTestsFilter->getGroupIds() ) ) {
			if( true == empty( $strWhere ) ) {
				$strWhere .= '';
			} else {
				$strWhere .= 'OR';
			}
			$strWhere .= ' group_id IN ( ' . implode( ',', $objTestsFilter->getGroupIds() ) . ' ) ';
		}

		if( true == valArr( $objTestsFilter->getJobPostingIds() ) ) {
			if( true == empty( $strWhere ) ) {
				$strWhere .= '';
			} else {
				$strWhere .= 'OR';
			}
			$strWhere .= ' ps_website_job_posting_id IN ( ' . implode( ',', $objTestsFilter->getJobPostingIds() ) . ' ) ';
		}

		$strSql = 'SELECT DISTINCT
						test_id
				   FROM
						test_groups
				   WHERE
						' . $strWhere;

		return array_keys( rekeyArray( 'test_id', fetchData( $strSql, $objDatabase ) ) );
	}

	public static function fetchTestGroupsAssignmentsCountByTestId( $intTestId, $objDatabase ) {

		$strSql = 'SELECT
						count( DISTINCT group_id ) AS groups,
						count( DISTINCT department_id ) AS departments,
						count( DISTINCT office_id ) AS	offices,
						count( DISTINCT designation_id ) AS designations,
						count( DISTINCT team_id ) as teams,
						count( DISTINCT ps_website_job_posting_id ) as published_jobs,
						count( DISTINCT training_session_id ) as training_sessions
				   FROM
						test_groups
				   WHERE
						(
						group_id IS NOT NULL
						OR department_id IS NOT NULL
						OR office_id IS NOT NULL
						OR designation_id IS NOT NULL
						OR team_id IS NOT NULL
						OR ps_website_job_posting_id IS NOT NULL
						OR training_session_id IS NOT NULL
						)
						AND test_id =' . ( int ) $intTestId;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0] ) ) return $arrintResponse[0];
		return 0;
	}

	public static function fetchTestGroupsByPsWebsiteJobPostingIdsWithOutCodingTest( $arrintPsWebsiteJobPostingIds, $objDatabase, $boolDelete = false ) {

		if( true == $boolDelete ) {
			$strCondition = ' AND t.deleted_by IS NULL AND t.deleted_on IS NULL';
		}

		$intTestQuestionTypeId = CTestQuestionType::CODING_TEST;

		$strSql = 'SELECT
						tg.*,
						t.name AS test_name
					FROM
						test_groups tg
						JOIN tests t ON ( t.id = tg.test_id )
					WHERE
						t.is_published = 1 ' . $strCondition . '
						AND tg.ps_website_job_posting_id IN ( ' . implode( ',', array_filter( $arrintPsWebsiteJobPostingIds ) ) . ')
						AND tg.test_id NOT IN (
												SELECT
													t.id
												FROM
													test_groups tg
													JOIN tests t ON ( t.id = tg.test_id )
													LEFT JOIN test_question_associations tqa ON ( tqa.test_id = t.id )
													LEFT JOIN test_questions tq ON ( tq.id = tqa.test_question_id )
													LEFT JOIN test_question_template_associations tqta ON tqta.test_id = t.id
													LEFT JOIN test_templates tt ON ( tt.id = tqta.test_template_id )
													LEFT JOIN test_question_templates tqt ON ( tt.id = tqt.test_template_id )
												WHERE
													t.is_published = 1
													AND tg.ps_website_job_posting_id IN ( ' . implode( ',', array_filter( $arrintPsWebsiteJobPostingIds ) ) . ')
													AND ( tq.id IS NOT NULL OR tqt.id IS NOT NULL )
													AND ( CASE
															WHEN tq.id IS NOT NULL THEN tq.test_question_type_id = ' . ( int ) $intTestQuestionTypeId . '
															ELSE TRUE
														  END )
													AND ( CASE
															WHEN tqt.id IS NOT NULL THEN tqt.test_question_type_id = ' . ( int ) $intTestQuestionTypeId . '
															ELSE TRUE
														  END )
						)';

		return self::fetchTestGroups( $strSql, $objDatabase );
	}

	public static function fetchTestGroupsByTestIdByFieldIds( $intTestId, $arrintFieldIds, $strFieldName, $objDatabase ) {
		$strSql = 'SELECT * FROM test_groups WHERE ' . $strFieldName . ' IN ( ' . implode( ',', $arrintFieldIds ) . ' ) AND test_id = ' . ( int ) $intTestId;
		return CTestGroups::fetchTestGroups( $strSql, $objDatabase );
	}

	public static function fetchTestGroupsByCertificationIds( $arrintCertificationIds, $objDatabase ) {
		if( false == valArr( $arrintCertificationIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						*
					FROM
						test_groups
					WHERE
						certification_id IN ( ' . implode( ',', $arrintCertificationIds ) . ' )';

		return parent::fetchTestGroups( $strSql, $objDatabase );
	}

	public static function fetchTestGroupDataByTestIdByEmployeeIds( $intTestId, $arrintEmployeeIds, $objDatabase ) {

		if( false == is_numeric( $intTestId ) || false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						tg.*,
						ec.employee_id
					FROM
						test_groups tg
						JOIN employee_certifications ec ON ( tg.certification_id = ec.certification_id )
					WHERE
						tg.test_id = ' . ( int ) $intTestId . '
						AND employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND tg.certification_id IS NOT NULL ';

		return self::fetchTestGroups( $strSql, $objDatabase );
	}

}
?>