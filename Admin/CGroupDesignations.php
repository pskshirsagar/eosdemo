<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupDesignations
 * Do not add any new functions to this class.
 */

class CGroupDesignations extends CBaseGroupDesignations {

	public static function fetchDesignationIdsByGroupId( $intGroupId, $objDatabase ) {
		if( true == is_null( $intGroupId ) ) return NULL;

		$strSql = 'SELECT
						designation_id
					FROM
						group_designations
					WHERE
						group_id  = ' . ( int ) $intGroupId;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGroupDesignationData( $intUserId, $intDesignationId, $objDatabase ) {

		if( true == is_null( $intUserId ) && true == is_null( $intDesignationId ) ) return NULL;

		$strSql = 'SELECT
						user_id,
						group_id
					FROM
						user_groups
					WHERE
						user_id = ' . ( int ) $intUserId . '
						AND deleted_by IS NULL
						AND group_id IN (
									SELECT
										group_id
									FROM
										group_designations
									WHERE
										designation_id = ' . ( int ) $intDesignationId . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGroupDesignationsData( $arrintUserIds, $arrintDesignationIds, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) && false == valArr( $arrintDesignationIds ) ) return NULL;

		$strSql = ' SELECT
						ug.user_id,
						ug.group_id
					FROM
						user_groups ug
						JOIN group_designations gd ON ( ug.group_id = gd.group_id )
					WHERE
						ug.user_id IN (' . implode( ',', $arrintUserIds ) . ')
						AND ug.deleted_by IS NULL
						AND gd.designation_id IN (' . implode( ',', $arrintDesignationIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGroupDesignationsByDesignationIds( $arrintDesignationIds, $objDatabase ) {
		if( false == valArr( $arrintDesignationIds ) ) return NULL;

		$strSql = 'SELECT gd.* FROM group_designations gd JOIN groups g ON( gd.group_id = g.id AND g.deleted_by IS NULL ) WHERE gd.designation_id IN (' . implode( ',', $arrintDesignationIds ) . ')';

		return self::fetchGroupDesignations( $strSql, $objDatabase );
	}

	public static function fetchGroupDesignationsDetails( $objDatabase ) {

		$strSql = 'SELECT
						gd.designation_id AS designation_id,
						g.id AS group_id,
						g.active_directory_guid
					FROM
						group_designations gd
						JOIN groups g ON ( g.id = gd.group_id );';

		return fetchData( $strSql, $objDatabase );
	}

}
?>