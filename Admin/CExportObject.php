<?php

class CExportObject extends CBaseExportObject {

	const CLIENT					= 'Client';
	const INVOICE					= 'Invoice';
	const CREDIT_MEMO				= 'Credit Memo';
	const PAYMENT					= 'Payment';
	const DEPOSIT					= 'Deposit';
	const RETURN_PAYMENT			= 'Return Payment';
	const PAYMENT_TO_CREDIT_MEMO	= 'Payment To Credit_Memo';
	const RETURN_PAYMENT_TO_INVOICE = 'Return_Payment To Invoice';
	const GENERAL_ERROR			    = 'General Error';

	public static $c_arrstrDuplicateNetsuiteErrorCodes = [ 'DUP_CSTM_FIELD', 'DUP_CSTM_RCRD', 'DUP_ENTITY', 'DUP_CSTM_LIST', 'DUP_NAME', 'DUP_RCRD' ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportPartnerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportObjectType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportObjectTypeKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['response_details'] ) && false == is_null( $arrstrValues['response_details'] ) ) {
			$this->setResponseDetails( $arrstrValues['response_details'] );
		}

		if( true == isset( $arrstrValues['is_taxable'] ) && false == is_null( $arrstrValues['is_taxable'] ) ) {
			$this->setIsTaxable( $arrstrValues['is_taxable'] );
		}
	}

	public function toArray() {
		$arrstrReturn = parent::toArray();
		$arrstrReturn['details'] = CStrings::jsonToStrDef( $this->getDetails() );
		return $arrstrReturn;
	}

}
?>