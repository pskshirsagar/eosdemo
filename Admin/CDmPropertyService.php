<?php

class CDmPropertyService extends CBaseDmPropertyService {

	const ENTRATA		= 'Entrata';
	const THIRD_PARTY	= '3rd Party';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDmPropertyServiceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceUtilizationProjectLink() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRegionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validateInsertOrUpdate() {
		$boolIsValid = false;

		if( !valId( $this->getCid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Please select valid client.' ) );
			return $boolIsValid;
		}

		if( !valId( $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Please select valid property.' ) );
			return $boolIsValid;
		}

		if( !in_array( $this->getWebsiteType(), [ self::ENTRATA, self::THIRD_PARTY ] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_type', 'Please select valid website type.' ) );
			return $boolIsValid;
		}

		if( !valStr( $this->getServiceUtilizationProjectLink() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_utilization_project_link', 'Please enter valid URL.' ) );
			return $boolIsValid;
		}

		if( !CValidation::checkUrl( $this->getServiceUtilizationProjectLink(), false ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_utilization_project_link', 'Please enter valid URL.' ) );
			return $boolIsValid;
		}

		if( !valStr( $this->getWebsiteUrl() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_url', 'Please enter valid URL.' ) );
			return $boolIsValid;
		}

		if( !CValidation::checkUrl( $this->getWebsiteUrl(), false ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_url', 'Please enter valid URL.' ) );
			return $boolIsValid;
		}

		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_insert_or_update':
				$boolIsValid = $this->validateInsertOrUpdate();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
