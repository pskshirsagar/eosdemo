<?php

class CCompanyValueNomination extends CBaseCompanyValueNomination {

	public function valCompanyValueId() {
		if( false == valIntArr( $this->m_arrintCompanyValueId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_value_id', 'Please select one or more Company Values.' ) );
			return false;
		}
		return true;
	}

	public function valEmployeeId() {
		if( false == valId( $this->m_intEmployeeId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Please select an employee.' ) );
			return false;
		}
		return true;
	}

	public function valNominatingEmployeeId() {
		if( false == valId( $this->m_intNominatingEmployeeId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'nominating_employee_id', 'Nominating Employee ID is required.' ) );
			return false;
		}
		return true;
	}

	public function valCompanyValueNominationBatchId() {
		if( false == valId( $this->m_intCompanyValueNominationBatchId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_value_nomination_batch_id', 'Nomination Batch ID is required.' ) );
			return false;
		}
		return true;
	}

	public function valSpiel() {
		if( false == valStr( $this->m_strSpiel ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'spiel', 'Please enter details.' ) );
			return false;
		}
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valCompanyValueId();
				$boolIsValid &= $this->valNominatingEmployeeId();
				$boolIsValid &= $this->valCompanyValueNominationBatchId();
				$boolIsValid &= $this->valSpiel();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>