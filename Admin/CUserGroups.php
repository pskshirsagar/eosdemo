<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserGroups
 * Do not add any new functions to this class.
 */

class CUserGroups extends CBaseUserGroups {

	public static function fetchAllUserGroups( $objDatabase ) {
		$strSql = 'SELECT * FROM user_groups WHERE deleted_by IS NULL';

		return self::fetchUserGroups( $strSql, $objDatabase );
	}

	public static function fetchUserGroupByGroupIdUserId( $intUserId, $intGroupId, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON ( user_groups.user_id ) *
					FROM
						user_groups
					WHERE
						group_id = ' . ( int ) $intGroupId . '
						AND user_id = ' . ( int ) $intUserId . '
						AND deleted_by IS NULL';

		return self::fetchUserGroup( $strSql, $objDatabase );
	}

	public static function fetchUserGroupByUserIdsByGroupId( $arrintUsers, $intGroupId, $objDatabase ) {

		if( false == valArr( $arrintUsers ) ) {
			return NULL;
		}
		if( true == is_null( $intGroupId ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						*
					FROM
						user_groups
					WHERE
						group_id = ' . ( int ) $intGroupId . '
						AND user_id IN ( ' . implode( ',', $arrintUsers ) . ' )';

		return self::fetchUserGroups( $strSql, $objDatabase );
	}

	public static function fetchUserGroupsCountByUserId( $intUserId, $objDatabase ) {

		$strSql   = 'WHERE user_id =' . ( int ) $intUserId . ' AND deleted_by IS NULL';
		$intCount = CUserGroups::fetchRowCount( $strSql, 'user_groups', $objDatabase );

		return $intCount;
	}

	public static function fetchUsersByGroupIds( $arrintGroupsIds, $objDatabase ) {

		$strSql = 'SELECT
						ug.user_id,ug.group_id
				   FROM
						user_groups AS ug
						JOIN users u on ug.user_id = u.id
						JOIN employees e on u.employee_id = e.id
						JOIN employee_addresses AS ea ON u.employee_id = ea.employee_id';

		$strSql .= ' WHERE ug.group_id IN ( ' . implode( ',', $arrintGroupsIds ) . ' ) AND ug.deleted_by IS NULL AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return self::fetchUserGroups( $strSql, $objDatabase );
	}

	public static function fetchAllUserGroupsCountForAllGroupsByGroupTypeId( $intGroupTypeId, $objDatabase ) {

		$strSql = 'SELECT
						count ( ug.id ),
						g.id
					FROM
						user_groups AS ug
						JOIN groups AS g ON ug.group_id = g.id
						JOIN users AS u ON ug.user_id = u.id
					WHERE
						g.group_type_id = ' . ( int ) $intGroupTypeId . '
						AND ug.deleted_by IS NULL
						AND u.is_disabled = 0
					GROUP BY
						g.id';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchUserGroupsByGroupIdByUserIds( $intGroupId, $arrintUserIds, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						user_groups
					WHERE
						group_id = ' . ( int ) $intGroupId . '
						AND user_id IN ( ' . implode( ',', $arrintUserIds ) . ' )
						AND deleted_by IS NULL';

		return self::fetchUserGroups( $strSql, $objDatabase );
	}

	public static function fetchUserGroupsByUserIdByGroupIds( $arrintGroupIds, $intUserId, $objDatabase ) {
		if( false == valArr( $arrintGroupIds ) ) {
			return NULL;
		}

				$strSql = 'SELECT *
							FROM 
								user_groups
							WHERE user_id = ' . ( int ) $intUserId . '
								AND group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )
								AND deleted_by IS NULL';

		return self::fetchUserGroups( $strSql, $objDatabase );
	}

	public static function fetchUserGroupsByUserIdsByGroupIds( $arrintUserIds, $arrintGroupIds, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) || false == valArr( $arrintGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						*
					FROM 
						user_groups
					WHERE 
						user_id IN ( ' . implode( ',', $arrintUserIds ) . ' )
						AND group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )
						AND deleted_on IS NOT NULL';
		return self::fetchUserGroups( $strSql, $objDatabase );
	}

	public static function fetchEnabledUsersByGroupId( $intGroupId, $objDatabase, $strCountryCode = NULL ) {

		$strWhere         = '';
		$strJoinCondition = '';

		if( true == valStr( $strCountryCode ) ) {
			$strWhere         = 'AND o.country_code = \'' . $strCountryCode . '\'';
			$strJoinCondition = 'LEFT JOIN employees e ON e.id = u.employee_id
						LEFT JOIN offices o ON o.id = e.office_id';
		}

		$strSql = 'SELECT
						ug.*
					FROM
						user_groups ug
						LEFT JOIN users u ON ug.user_id = u.id
						' . $strJoinCondition . '
					WHERE
						ug.group_id = ' . ( int ) $intGroupId . $strWhere . ' ' . '
						AND ug.deleted_by IS NULL
						AND u.is_disabled = 0';

		return self::fetchUserGroups( $strSql, $objDatabase );
	}

	public static function fetchUserIdsByGroupId( $intGruopId, $objDatabase ) {

		$strSql = 'SELECT
						 user_id
					FROM
						user_groups
					WHERE
						group_id = ' . ( int ) $intGruopId . '
						AND deleted_by IS NULL
					ORDER BY
						user_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUserGroupsByGroupIds( $arrintGroupIds, $objDatabase ) {
		if( false == valArr( $arrintGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						user_groups
					WHERE
						group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )
						AND deleted_by IS NULL';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchEnabledUsersCountByGroupId( $intGroupId, $objDatabase ) {

		$strSql = 'SELECT
						count ( ug.id )
					FROM
						user_groups AS ug
						JOIN users AS u ON ug.user_id = u.id
					WHERE
						ug.group_id = ' . ( int ) $intGroupId . '
						AND ug.deleted_by IS NULL
						AND u.is_disabled = 0';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchActiveDirectoryEnabledUserGroupsByUserId( $intUserId, $objDatabase ) {

		$strSql = ' SELECT
						ug.*,
						g.active_directory_guid
					FROM
						user_groups ug
						JOIN groups g ON ( g.id = ug.group_id )
					WHERE
						g.active_directory_guid IS NOT NULL
						AND ug.user_id =' . ( int ) $intUserId . '
						AND ug.deleted_by IS NULL';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchActiveDirectoryEnabledUserGroupsByGroupId( $intGroupId, $objDatabase ) {

		$strSql = ' SELECT
						ug.*,
						u.active_directory_guid
					FROM
						user_groups ug
						JOIN users u ON ( u.id = ug.user_id )
					WHERE
						u.active_directory_guid IS NOT NULL
						AND ug.group_id =' . ( int ) $intGroupId . '
						AND ug.deleted_by IS NULL';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchUserGroupIdsByUserId( $intUserId, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT group_id
					FROM
						user_groups
					WHERE
						user_id = ' . ( int ) $intUserId . '
						AND deleted_by IS NULL';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return array_keys( rekeyArray( 'group_id', $arrintResponse ) );
	}

	public static function fetchUserGroupIdsWithEmployeeDetailsByUserId( $intUserId, $objDatabase ) {

		$strSql = ' SELECT
						ug.group_id,
						emp.id AS employee_id,
						emp.preferred_name
					FROM
						user_groups ug
						JOIN users u ON ( u.id = ug.user_id )
						JOIN employees emp ON ( emp.id = u.employee_id )
					WHERE
						ug.user_id = ' . ( int ) $intUserId . '
						AND ug.deleted_by IS NULL';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchUserGroupsByUserIdByGroupTypeId( $intUserId, $intGroupTypeId, $objDatabase ) {
		if( false == is_numeric( $intUserId ) || false == is_numeric( $intGroupTypeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						ug.*
					FROM
						groups g
						JOIN user_groups ug ON ( g.id = ug.group_id AND ug.deleted_by IS NULL )
					WHERE
						g.group_type_id = ' . ( int ) $intGroupTypeId . '
						AND user_id = ' . ( int ) $intUserId;

		return self::fetchUserGroups( $strSql, $objDatabase );
	}

	public static function fetchUserGroupByUserIdsByGroupIds( $arrintUserIds, $arrintGroupIds, $objDatabase ) {
		if( ( false == valArr( $arrintUserIds ) ) || ( false == valArr( $arrintGroupIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
				   FROM
					   	user_groups
				   WHERE
						user_id IN ( ' . implode( ',', $arrintUserIds ) . ' )
						AND group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )
						AND deleted_by IS NULL';

		return self::fetchUserGroup( $strSql, $objDatabase );
	}

	public static function fetchUserGroupsCountByGroupTypeId( $intGroupTypeId, $objDatabase ) {

		if( false == isset( $intGroupTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					count ( ug.id )
				FROM
					user_groups AS ug
					JOIN groups AS g ON ug.group_id = g.id
					JOIN users AS u ON ug.user_id = u.id
				WHERE
					g.group_type_id = ' . ( int ) $intGroupTypeId . '
					AND ug.deleted_by IS NULL
					AND u.is_disabled = 0';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchUserGroupDetailsByUserGroupId( $intGroupId, $objAdminDatabase ) {

		$strSql = 'SELECT
						e.name_first as first_name,
						e.name_last as last_name,
						\'91\' || epn.phone_number as number
					FROM
						user_groups ug
						JOIN users u ON ( u.id = ug.user_id )
						JOIN employees e ON ( e.id = u.employee_id )
						JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id )
					WHERE
						ug.group_id = ' . ( int ) $intGroupId . '
						AND ug.deleted_by IS NULL
						AND u.is_disabled = 0
						AND phone_number_type_id = ' . CPhoneNumberType::PRIMARY;

		return fetchdata( $strSql, $objAdminDatabase );
	}

	public static function fetchUserGroupsByUserId( $intUserId, $objDatabase, $boolCheckDeletedBy = true ) {
		if( false == is_numeric( $intUserId ) ) {
			return NULL;
		}

		$strWhereClause = ( true == $boolCheckDeletedBy ) ? ' AND deleted_by IS NULL' : '';

		$strSql = 'SELECT *
					FROM 
						user_groups
					WHERE 
						user_id = ' . ( int ) $intUserId . $strWhereClause;

		return self::fetchUserGroups( $strSql, $objDatabase );
	}

	public static function fetchUserGroupsByGroupId( $intGroupId, $objDatabase, $boolCheckDeletedBy = true ) {
		if( false == is_numeric( $intGroupId ) ) {
			return NULL;
		}

		$strWhereClause = ( true == $boolCheckDeletedBy ) ? ' AND deleted_by IS NULL' : '';

		$strSql = 'SELECT *
					FROM 
						user_groups
					WHERE 
						group_id = ' . ( int ) $intGroupId . $strWhereClause;

		return self::fetchUserGroups( $strSql, $objDatabase );
	}

	public static function fetchUserGroupsDataByGroupIds( $arrintGroupIds, $objDatabase ) {

		if( false == valArr( $arrintGroupIds ) ) return NULL;

		$strSql = 'SELECT
						ug.*,
						u.is_disabled,
						e.designation_id,
						e.id AS employee_id,
						e.preferred_name AS employee_name
					FROM
						user_groups ug
						LEFT JOIN users u ON ( ug.user_id = u.id )
						LEFT JOIN employees e ON ( u.employee_id = e.id )
					WHERE
						u.is_disabled = 0
						AND ug.group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

}

?>