<?php

class CEmployeeExternalTool extends CBaseEmployeeExternalTool {

	protected $m_strExternalToolName;
	protected $m_strExternalToolUrl;

	public function setExternalToolName( $strExternalToolName ) {
		$this->m_strExternalToolName = $strExternalToolName;
	}

	public function setExternalToolUrl( $strExternalToolUrl ) {
		$this->m_strExternalToolUrl = $strExternalToolUrl;
	}

	public function getExternalToolName() {
		return $this->m_strExternalToolName;
	}

	public function getExternalToolUrl() {
		return $this->m_strExternalToolUrl;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['external_tool_name'] ) ) 	$this->setExternalToolName( $arrmixValues['external_tool_name'] );
		if( true == isset( $arrmixValues['external_tool_url'] ) ) 	$this->setExternalToolUrl( $arrmixValues['external_tool_url'] );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExternalToolId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>