<?php

class CEmployeeEncryptionAssociation extends CBaseEmployeeEncryptionAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewerEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEncryptedAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEncryptedAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		// $intCurrentUserId this variable is not in use will remove later.
		$intCurrentUserId = NULL;
		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
					  public.employee_encryption_associations
					SET ';

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' reference_id = ' . $this->sqlReferenceId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlReferenceId() ) != $this->getOriginalValueByFieldName( 'reference_id' ) ) {
			$arrstrOriginalValueChanges['reference_id'] = $this->sqlReferenceId();
			$strSql .= ' reference_id = ' . $this->sqlReferenceId() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' encrypted_amount = ' . $this->sqlEncryptedAmount() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlEncryptedAmount() ) != $this->getOriginalValueByFieldName( 'encrypted_amount' ) ) {
			$arrstrOriginalValueChanges['encrypted_amount'] = $this->sqlEncryptedAmount();
			$strSql .= ' encrypted_amount = ' . $this->sqlEncryptedAmount() . ',';
			$boolUpdate = true;
		}

		$strSql = \Psi\CStringService::singleton()->substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . '
						AND viewer_employee_id = ' . ( int ) $this->sqlViewerEmployeeId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		// $intCurrentUserId this variable is not in use will remove later.
		$intCurrentUserId = NULL;

		$strSql = 'DELETE FROM public.employee_encryption_associations WHERE id = ' . ( int ) $this->sqlId() . ' AND viewer_employee_id = ' . ( int ) $this->sqlViewerEmployeeId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>