<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeEncryptionPermissions
 * Do not add any new functions to this class.
 */

class CEmployeeEncryptionPermissions extends CBaseEmployeeEncryptionPermissions {

	public static function fetchEmployeeEncryptionPermissionByEmployeeIdByReferenceTypeIdByEncryptionSystemTypeId( $intEmployeeId, $intReferenceTypeId, $intEncryptionSystemTypeId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						employee_encryption_permissions eep
					WHERE
						eep.employee_id = ' . ( int ) $intEmployeeId . '
						AND eep.reference_type_id = ' . ( int ) $intReferenceTypeId . '
						AND eep.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId;

		return self::fetchEmployeeEncryptionPermissions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEncryptionPermissionsByCountryCodeByEncryptionSystemTypeId( $strCountryCode, $intEncryptionSystemTypeId, $objDatabase, $boolWithReadAccess = true, $boolIsConsiderBoth = false, $boolEncryptionKey = false, $boolViewEmployeeEncryptionPermissions = false ) {

		$strWhereCondition = ( true == $boolWithReadAccess ) ? ' eep.has_read_access = 1 ' : ' eep.has_read_access = 0 ';
		$strWhereCondition = ( true == $boolIsConsiderBoth ) ? ' 1 = 1 ' : $strWhereCondition;

		if( false == empty( $intEncryptionSystemTypeId ) ) {
			$strWhereCondition .= ' AND eep.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId;
		}
		if( $boolEncryptionKey ) {
			$strWhereCondition .= ' AND e.public_key IS NOT NULL ';
		}

		$strSelect = '';
		if( true == $boolViewEmployeeEncryptionPermissions ) {
			$strSelect = ', CASE WHEN eep.employee_id IN(
				SELECT DISTINCT (u.employee_id)
				FROM users u
					JOIN employee_addresses ea ON (u.employee_id = ea.employee_id)
					JOIN user_groups ug ON (u.id = ug.user_id AND ug.deleted_by IS NULL)
				WHERE
					ug.group_id = ' . CGroup::ANNUAL_REVIEW_MANAGERS . ' AND
					ea.country_code = \'' . $strCountryCode . '\'
				ORDER BY u.employee_id
			)
			OR
			eep.employee_id IN (
				SELECT
					distinct( e.id )
				FROM
					employees e
					LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id )
				WHERE
					e.department_id IN ( ' . implode( ',', [ CDepartment::HR, CDepartment::ACCOUNTING, CDepartment::ACCOUNTS ] ) . ' )
					AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' )  AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . ' AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
				ORDER BY
					e.id 
			) THEN \'Review Manager\' 
			ELSE
				\'Reporting Manager\' 
			END 
				AS manager_type';
		}

		$strSql = 'SELECT
						eep.employee_id,
						eep.reference_type_id,
						eep.encryption_system_type_id,
						eep.has_read_access,
						eep.updated_by,
						eep.created_by,
						e.preferred_name as name_full,
						array_to_string( array_agg ( CASE WHEN ' . $strWhereCondition . ' THEN eep.reference_id ELSE 0 END ), \'' . ',' . '\' ) AS read_reference
						' . $strSelect . '
					FROM
						employee_encryption_permissions eep
						JOIN employees e ON ( eep.employee_id = e.id  )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' ) AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						' . $strWhereCondition . '
					GROUP BY
						eep.employee_id,
						e.preferred_name,
						eep.encryption_system_type_id,
						eep.reference_type_id,
						eep.has_read_access,
						eep.updated_by,
						eep.created_by
					ORDER BY
						eep.updated_by, e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEncryptionPermissionsByEmployeeIdsByReferenceTypeIdByEncryptionSystemTypeId( $arrintEmployeeIds, $intReferenceTypeId, $objAdminDatabase, $boolIsReadAccess = 1, $intEncryptionSystemTypeId ) {
		if( false == valArr( $arrintEmployeeIds ) || true == empty( $intReferenceTypeId ) ) return NULL;

		$strSql = ' SELECT
						eep.employee_id,
						eep.reference_id,
						eep.reference_type_id
					FROM
						employee_encryption_permissions eep
					WHERE
						eep.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND eep.has_read_access = ' . $boolIsReadAccess . '
						AND eep.reference_type_id = ' . ( int ) $intReferenceTypeId . '
						AND eep.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId;

		$arrstrTempEmployeeEncryptionPermissions = fetchData( $strSql, $objAdminDatabase );

		$arrstrEmployeeEncryptionPermissions = array();

		if( true == valArr( $arrstrTempEmployeeEncryptionPermissions ) ) {
			foreach( $arrstrTempEmployeeEncryptionPermissions as $arrstrEmployeeEncryptionPermission ) {
				$arrstrEmployeeEncryptionPermissions[$arrstrEmployeeEncryptionPermission['reference_id']][] = $arrstrEmployeeEncryptionPermission['employee_id'];
			}
		}

		return $arrstrEmployeeEncryptionPermissions;
	}

	public static function fetchEmployeeEncryptionPermissionByEmployeeIdsByReferenceTypeIdByEncryptionSystemTypeIdByHasReadAccess( $arrintEmployeeIds, $intReferenceTypeId, $intEncryptionSystemTypeId, $objDatabase, $boolHasReadAccess = true ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strWhereCondition = ( true == $boolHasReadAccess ) ? ' AND eep.has_read_access = 1'  : ' AND eep.has_read_access = 0';

		$strSql = 'SELECT
						*
					FROM
						employee_encryption_permissions eep
					WHERE
						eep.employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )
						AND eep.reference_type_id = ' . ( int ) $intReferenceTypeId . '
						AND eep.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId . $strWhereCondition;

		return self::fetchEmployeeEncryptionPermissions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEncryptionPermissionByEmployeeIdsByReferenceIdByReferenceTypeIdByEncryptionSystemTypeIdByHasReadAccess( $arrintEmployeeIds, $intReferenceId, $intReferenceTypeId, $intEncryptionSystemTypeId, $objDatabase, $boolHasReadAccess = true ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strWhereCondition = ( true == $boolHasReadAccess ) ? ' AND eep.has_read_access = 1'  : ' AND eep.has_read_access = 0';

		$strSql = 'SELECT
						*
					FROM
						employee_encryption_permissions eep
					WHERE
						eep.employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )
						AND eep.reference_id = ' . ( int ) $intReferenceId . '
						AND eep.reference_type_id = ' . ( int ) $intReferenceTypeId . '
						AND eep.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId . $strWhereCondition;

		return self::fetchEmployeeEncryptionPermissions( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeEncryptionPermissionsPermissionsByWhosEmployeeIdsByEncryptionSystemTypeId( $arrintEmployeeIds, $strCountryCode, $objAdminDatabase, $boolHasReadAccess = true, $intEncryptionSystemTypeId ) {
		if( false == valArr( $arrintEmployeeIds ) || true == empty( $strCountryCode ) ) return NULL;

		$strWhereCondition = ( true == $boolHasReadAccess ) ? ' AND eep.has_read_access = 1'  : ' AND eep.has_read_access = 0';

		$strSql = ' SELECT
						e.id As data_employee_id,
						eep.employee_id,
						e1.public_key
					FROM
						employees e
						JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN employee_encryption_permissions eep ON ( te.team_id = eep.reference_id AND eep.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId . ' AND eep.reference_type_id = ' . CPermissionReferenceType::TEAM . ' )
						JOIN employees e1 ON ( e1.id = eep.employee_id AND e1.public_key IS NOT NULL )
						JOIN employee_addresses ea ON ( ea.employee_id = e1.id AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' ) AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e.id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' ) ' . $strWhereCondition;

		$arrstrTempEmployeeEncryptionPermissions = fetchData( $strSql, $objAdminDatabase );

		$arrstrEmployeeEncryptionPermissions = array();

		if( true == valArr( $arrstrTempEmployeeEncryptionPermissions ) ) {
			foreach( $arrstrTempEmployeeEncryptionPermissions as $arrstrEmployeeEncryptionPermission ) {
				$arrstrEmployeeEncryptionPermissions[$arrstrEmployeeEncryptionPermission['data_employee_id']][$arrstrEmployeeEncryptionPermission['employee_id']] = $arrstrEmployeeEncryptionPermission['public_key'];
			}
		}

		return $arrstrEmployeeEncryptionPermissions;
	}

	public static function fetchDepartmentEmployeeEncryptionPermissionsByWhosEmployeeIdsByEncryptionSystemTypeId( $arrintEmployeeIds, $strCountryCode, $objAdminDatabase, $boolHasReadAccess = true, $intEncryptionSystemTypeId ) {
		if( false == valArr( $arrintEmployeeIds ) || true == empty( $strCountryCode ) ) return NULL;

		$strWhereCondition = ( true == $boolHasReadAccess ) ? ' AND eep.has_read_access = 1'  : ' AND eep.has_read_access = 0';

		$strSql = ' SELECT
						e.id As data_employee_id,
						eep.employee_id,
						e1.public_key
					FROM
						employees e
						JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN employee_encryption_permissions eep ON ( e.department_id = eep.reference_id AND eep.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId . ' AND eep.reference_type_id = ' . CPermissionReferenceType::DEPARTMENT . ' )
						JOIN employees e1 ON ( e1.id = eep.employee_id AND e1.public_key IS NOT NULL )
						JOIN employee_addresses ea ON ( ea.employee_id = e1.id AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' ) AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e.id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' ) ' . $strWhereCondition;

		$arrstrTempEmployeeEncryptionPermissions = fetchData( $strSql, $objAdminDatabase );

		$arrstrEmployeeEncryptionPermissions = array();

		if( true == valArr( $arrstrTempEmployeeEncryptionPermissions ) ) {
			foreach( $arrstrTempEmployeeEncryptionPermissions as $arrstrEmployeeEncryptionPermission ) {
				$arrstrEmployeeEncryptionPermissions[$arrstrEmployeeEncryptionPermission['data_employee_id']][$arrstrEmployeeEncryptionPermission['employee_id']] = $arrstrEmployeeEncryptionPermission['public_key'];
			}
		}

		return $arrstrEmployeeEncryptionPermissions;
	}

	public static function fetchEmployeesEncryptionPermissionsByWhosEmployeeIdsByEncryptionSystemTypeId( $arrintEmployeeIds, $objAdminDatabase, $boolHasReadAccess = true, $intEncryptionSystemTypeId ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strWhereCondition = ( true == $boolHasReadAccess ) ? ' AND eep.has_read_access = 1'  : ' AND eep.has_read_access = 0';

		$strSql = ' SELECT
						e.id As data_employee_id,
						eep.employee_id,
						e1.public_key
					FROM
						employees e
						JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN employee_encryption_permissions eep ON ( e.id = eep.reference_id AND eep.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId . ' AND eep.reference_type_id = ' . CPermissionReferenceType::EMPLOYEE . ' )
						JOIN employees e1 ON ( e1.id = eep.employee_id AND e1.public_key IS NOT NULL )
					WHERE
						e.id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' ) ' . $strWhereCondition;

		$arrstrTempEmployeeEncryptionPermissions = fetchData( $strSql, $objAdminDatabase );

		$arrstrEmployeeEncryptionPermissions = array();

		if( true == valArr( $arrstrTempEmployeeEncryptionPermissions ) ) {
			foreach( $arrstrTempEmployeeEncryptionPermissions as $arrstrEmployeeEncryptionPermission ) {
				$arrstrEmployeeEncryptionPermissions[$arrstrEmployeeEncryptionPermission['data_employee_id']][$arrstrEmployeeEncryptionPermission['employee_id']] = $arrstrEmployeeEncryptionPermission['public_key'];
			}
		}

		return $arrstrEmployeeEncryptionPermissions;
	}

	public static function fetchEmployeeEncryptionPermissionsByEmployeeIdsByReferenceTypeId( $arrintEmployeeIds, $intReferenceTypeId, $objDatabase, $boolHasReadAccess = true ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strWhereCondition = ( true == $boolHasReadAccess ) ? ' AND eep.has_read_access = 1'  : ' AND eep.has_read_access = 0';

		$strSql = 'SELECT
						*
					FROM
						employee_encryption_permissions eep
					WHERE
						eep.employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )
						AND eep.reference_type_id = ' . ( int ) $intReferenceTypeId . $strWhereCondition;

		return self::fetchEmployeeEncryptionPermissions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEncryptionPermissionByEmployeeIdsByReferenceTypeIdByEncryptionSystemTypeIdsByHasReadAccess( $arrintEmployeeIds, $intReferenceTypeId, $arrintEncryptionSystemTypeIds, $objDatabase, $boolHasReadAccess = true ) {
		if( false == valArr( $arrintEmployeeIds ) || false == valId( $intReferenceTypeId ) || false == valArr( $arrintEncryptionSystemTypeIds ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == $boolHasReadAccess ) ? ' AND eep.has_read_access = 1'  : ' AND eep.has_read_access = 0';

		$strSql = 'SELECT
						*
					FROM
						employee_encryption_permissions eep
					WHERE
						eep.employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )
						AND eep.reference_type_id = ' . ( int ) $intReferenceTypeId . '
						AND eep.encryption_system_type_id IN (' . sqlIntImplode( $arrintEncryptionSystemTypeIds ) . ')' . $strWhereCondition;

		return self::fetchEmployeeEncryptionPermissions( $strSql, $objDatabase );
	}

}
?>