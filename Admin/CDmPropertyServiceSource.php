<?php

class CDmPropertyServiceSource extends CBaseDmPropertyServiceSource {

	const GOOGLE		= 1;
	const BING			= 2;
	const FACEBOOK		= 3;
	const SIMPLI_FI		= 4;

	public static $c_arrintDmServiceSourceIds = [
		self::GOOGLE		=> 'Google',
		self::BING			=> 'Bing',
		self::FACEBOOK		=> 'Facebook',
		self::SIMPLI_FI		=> 'Simpli.fi'
	];

	public function valValue() {

		$boolIsValid = true;

		if( empty( $this->m_strValue ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Please enter DM Property service source value.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_value':
				$boolIsValid = $this->valValue();
				break;

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
