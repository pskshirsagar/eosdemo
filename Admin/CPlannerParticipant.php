<?php

class CPlannerParticipant extends CBasePlannerParticipant {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPlannerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmployeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIfAlreadyExist( $objDatabase ) {
    	$boolIsValid = true;

    	$intCount = CPlannerParticipants::fetchRowCount( 'WHERE planner_id =' . $this->getPlannerId() . ' AND employee_id =' . $this->getEmployeeId(), 'planner_participants', $objDatabase );

    	if( 0 < $intCount )
    		$boolIsValid = false;

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valIfAlreadyExist( $objDatabase );
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>