<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAttendanceRemarks
 * Do not add any new functions to this class.
 */

class CEmployeeAttendanceRemarks extends CBaseEmployeeAttendanceRemarks {

	public static function fetchEmployeeAttendanceRemarkByEmployeeIdByDate( $intEmployeeId, $strDate, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						employee_attendance_remarks
					WHERE
						employee_id  = ' . ( int ) $intEmployeeId . '
						AND requested_datetime = \'' . $strDate . '\'
					LIMIT 1';

		return self::fetchEmployeeAttendanceRemark( $strSql, $objDatabase );
	}

	public static function fetchEmployeeAttendanceRemarksByEmployeeIdByDateRange( $intEmployeeId, $strBeginDate, $strEndDate, $objDatabase ) {
		$strSql = 'SELECT
						id,
						employee_id,
						note,
						date(requested_datetime) as requested_datetime
					FROM
						employee_attendance_remarks
					WHERE
						employee_id  = ' . ( int ) $intEmployeeId . '
						AND requested_datetime  BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' ';

		return self::fetchEmployeeAttendanceRemarks( $strSql, $objDatabase );
	}
}
?>