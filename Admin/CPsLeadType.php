<?php

class CPsLeadType extends CBasePsLeadType {

	const FEE_MANAGER						= 1;
	const OWNER_OR_OPERATOR					= 2;
	const OWNS_AND_FEE_MANAGES				= 3;
	const OWNER_OR_OUTSOURCES_MANAGEMENT	= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

}
?>