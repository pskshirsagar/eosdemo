<?php

class CTaskReason extends CBaseTaskReason {
	const DUPLICATE								= 1;
	const DUPLICATE_CANCEL_REASON_FOR_FEATURE	= 12;
	const DATAFIX								= 9;
	const TESTED_ON_ANOTHER_TASK				= 10;
	const NOT_TESTABLE							= 11;

	const PRODUCT_MANAGEMENT_VERIFICATION_TASK	= 30;
	const PRODUCT_MANAGEMENT_TECHNICAL_TASK		= 31;
	const PRODUCT_MANAGEMENT_DATA_FIX			= 32;
	const PRODUCT_MANAGEMENT_LIMITED_SCOPE		= 33;
	const PRODUCT_MANAGEMENT_NEEDED				= 34;

	public static $c_arrmixProductManagementReleaseNoteNonNeededOptions = [
		self::PRODUCT_MANAGEMENT_VERIFICATION_TASK	=> 'Verification Task',
		self::PRODUCT_MANAGEMENT_TECHNICAL_TASK		=> 'Technical Task',
		self::PRODUCT_MANAGEMENT_DATA_FIX			=> 'Data Fix',
		self::PRODUCT_MANAGEMENT_LIMITED_SCOPE		=> 'Limited Scope'
	];

	public static $c_arrmixProductManagementReleaseNoteNonNeededMessages = [
		self::PRODUCT_MANAGEMENT_VERIFICATION_TASK	=> 'is a verification task',
		self::PRODUCT_MANAGEMENT_TECHNICAL_TASK		=> 'is a technical task',
		self::PRODUCT_MANAGEMENT_DATA_FIX			=> 'is a data fix',
		self::PRODUCT_MANAGEMENT_LIMITED_SCOPE		=> 'has Limited Scope'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskReasonTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>