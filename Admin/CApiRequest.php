<?php

class CApiRequest extends CBaseApiRequest {

	protected $m_strTitle;
	protected $m_boolIsCompanyEmployee;
	protected $m_boolTermsAndConditions;
	protected $m_boolIsDocumentUploaded;
	protected $m_arrobjApiRequestIps;
	protected $m_strRequestedIp;

	public function createApiRequestIp() {

		$objApiRequestIp = new CApiRequestIp();
		$objApiRequestIp->setApiRequestId( $this->getId() );
		$objApiRequestIp->setCid( $this->getCid() );

		return $objApiRequestIp;
	}

	/**
	 * Get Functions
	 */

	public function getIsCompanyEmployee() {
		return $this->m_boolIsCompanyEmployee;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getTermsAndConditions() {
		return $this->m_boolTermsAndConditions;
	}

	public function getIsDocumentUploaded() {
		return $this->m_boolIsDocumentUploaded;
	}

	public function getApiRequestIps() {
		return $this->m_arrobjApiRequestIps;
	}

	public function getRequestedIp() {
		return $this->m_strRequestedIp;
	}

	public function addApiRequestIp( $objApiRequestIp ) {
		$this->m_arrobjApiRequestIps[] = $objApiRequestIp;
	}

	/**
	 * Set Functions
	 */

	public function setIsCompanyEmployee( $boolIsCompanyEmployee ) {
		$this->m_boolIsCompanyEmployee = $boolIsCompanyEmployee;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setTermsAndConditions( $boolTermsAndConditions ) {
		$this->m_boolTermsAndConditions = $boolTermsAndConditions;
	}

	public function setIsDocumentUploaded( $boolIsDocumentUploaded ) {
		$this->m_boolIsDocumentUploaded = $boolIsDocumentUploaded;
	}

	public function setRequestedIp( $strRequestedIp ) {
		$this->m_strRequestedIp = $strRequestedIp;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['terms_and_conditions'] ) )	$this->setTermsAndConditions( $arrmixValues['terms_and_conditions'] );
		if( true == isset( $arrmixValues['title'] ) )					$this->setTitle( $arrmixValues['title'] );
		if( true == isset( $arrmixValues['requested_ip'] ) )			$this->setRequestedIp( $arrmixValues['requested_ip'] );
		return;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPreExistingApiRequestIpsByIpAddresses( $arrstrIpAddresses, $objDatabase ) {
		return \Psi\Eos\Admin\CApiRequestIps::createService()->fetchPreExistingApiRequestIpsByIpAddressesByCid( $arrstrIpAddresses, $this->getCid(), $objDatabase );
	}

	/**
	 * Valiate Functions
	 */

	public function valCompanyName() {
		$boolIsValid = true;

		if( false == valStr( $this->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', 'Company Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( false == valStr( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Name first is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( false == valStr( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Name last is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTermsAndConditions() {
		$boolIsValid = true;

		if( 1 == $this->getIsCompanyEmployee() && 1 != $this->getTermsAndConditions() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'terms_and_conditions', 'You must agree to Terms & Conditions.' ) );
		}

		return $boolIsValid;
	}

	public function valDocumentUploaded() {
		$boolIsValid = true;

		if( 1 != $this->getIsCompanyEmployee() && 1 != $this->getIsDocumentUploaded() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'terms_and_conditions', 'You must upload the Terms & Use document.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {

		$boolIsValid = true;
		$strPhoneNumber	= $this->getPhoneNumber();

		if( false == valStr( $strPhoneNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
			return $boolIsValid;
		}

		$intLength = strlen( $strPhoneNumber );

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number must be between 10 and 15 characters.' ) );
			$boolIsValid = false;
		} elseif( true == isset( $strPhoneNumber ) && false == ( CValidation::validateFullPhoneNumber( $strPhoneNumber, false ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Valid Phone number is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {

		$boolIsValid = true;

		if( false == valStr( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is required.' ) );
			return $boolIsValid;
		} elseif( 1 != ( CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address does not have valid format.', '' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valTermsAndConditions();
				$boolIsValid &= $this->valDocumentUploaded();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>