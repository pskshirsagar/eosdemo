<?php

class CSemAdStatusType extends CBaseSemAdStatusType {

	const APPROVED			= 1;
	const DISAPPROVED		= 2;
	const UNCHECKED			= 3;

	public static $c_arrmixSemAdStatusTypeStrToId = array(
		'APPROVED' => self::APPROVED,
		'DISAPPROVED' => self::DISAPPROVED,
		'UNCHECKED' => self::UNCHECKED
	);

}
?>