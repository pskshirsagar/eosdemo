<?php

class CEmployeeApplicationContactType extends CBaseEmployeeApplicationContactType {

	const EMAIL_ADVERTISEMENT 		= 1;
	const NEWSLETTER 				= 2;
	const DIRECT_MAIL   			= 3;
	const INBOUND_SMS 				= 4;
	const OUTBOUND_SMS 				= 5;
	const WEBSITE_NEWSLETTER 		= 6;
	const WEBSITE_INFO 				= 7;
	const EMAIL_INBOUND 			= 8;
	const EMAIL_OUTBOUND			= 9;
	const CALL_OUTBOUND 			= 10;
	const CALL_INBOUND				= 11;
	const MASS_EMAIL				= 12;
	const IN_PERSON_VISIT			= 13;
	const XENTO_WEBSITE_VISIT		= 14;
	const TECHNICAL_TEST_EMAIL		= 15;
}
?>