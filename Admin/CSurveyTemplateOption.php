<?php

class CSurveyTemplateOption extends CBaseSurveyTemplateOption {

	const FIVE_NUMERIC_WEIGHT = 5;

	public function valOptionName() {
		$boolIsValid = true;

		if( false == valStr( $this->getOptionName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'option_name', 'Answer choice is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valOptionName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>