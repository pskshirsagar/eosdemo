<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStorageTypes
 * Do not add any new functions to this class.
 */

class CStorageTypes extends CBaseStorageTypes {

	public static function fetchStorageTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CStorageType::class, $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchStorageType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CStorageType::class, $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllStorageTypes( $objDatabase ) {
		return self::fetchStorageTypes( sprintf( 'SELECT * FROM storage_types ORDER BY id' ), $objDatabase );
	}

	public static function fetchStorageTypeByName( $strName,  $objDatabase ) {

		$strSql = 'SELECT * FROM storage_types WHERE name ILIKE \'' . $strName . '\'';

		return self::fetchStorageType( $strSql, $objDatabase );
	}

}
?>