<?php

use Psi\Eos\Admin\CDmBundles;

class CDmBundle extends CBaseDmBundle {

	const ONGOING	= 'Ongoing';
	const SETUP		= 'Setup';

	public function valName( $objAdminDatabase ) {
		$boolIsValid = false;

		if( !valStr( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please enter valid DM Bundle name.' ) );
			return $boolIsValid;
		}

		if( 2 >= strlen( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'DM Bundle name should contain at least 3 letters.' ) );
			return $boolIsValid;
		}

		if( valObj( $objAdminDatabase, 'CDatabase' ) ) {
			if( valId( $this->getId() ) ) {
				$objDmBundle = CDmBundles::createService()->fetchDmBundleById( $this->getId(), $objAdminDatabase );
				if( !valObj( $objDmBundle, 'CDmBundle' ) ) {
					return $boolIsValid;
				} elseif( \Psi\CStringService::singleton()->strtolower( $this->m_strName ) == \Psi\CStringService::singleton()->strtolower( $objDmBundle->getName() ) ) {
					return true;
				}
			}
			$boolIsValid = !$this->isDmBundleNameExist( $this->m_strName, $objAdminDatabase );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_name':
				$boolIsValid = $this->valName( $objAdminDatabase );
				break;

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function isDmBundleNameExist( $strName, $objAdminDatabase ) {

		$boolIsValid		= false;
		$intExistNameCount	= CDmBundles::createService()->fetchDmBundleCountByName( $strName, $objAdminDatabase );

		if( 0 < $intExistNameCount ) {
			$boolIsValid = true;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'DM Bundle name already exists' ) );
		}

		return $boolIsValid;
	}

}
?>
