<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaymentProcessingFees
 * Do not add any new functions to this class.
 */

class CPaymentProcessingFees extends CBasePaymentProcessingFees {

	public static function fetchAllPaymentProcessingFeeDetails( $objDatabase, $objPagination = NULL, $boolIsCount = false ) {

		$strSql = 'SELECT
						ppf.id,ppf.client_id,ppf.payment_type_id,ppf.percentage
					FROM
						payment_processing_fees ppf
					ORDER BY
						ppf.updated_on DESC';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) && false == $boolIsCount ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . '
						 LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		$arrmixResult = self::fetchPaymentProcessingFees( $strSql, $objDatabase );
		return ( true == $boolIsCount ) ? \Psi\Libraries\UtilFunctions\count( $arrmixResult ) : $arrmixResult;
	}

	public static function fetchPaymentProcessingFeesByClientIdsAndPaymentTypeId( $objDatabase, $arrintClientIds, $intPaymentTypeId ) {

		if( true == is_null( $intPaymentTypeId ) || false == valArr( $arrintClientIds ) ) return NULL;

		$strSql = 'SELECT
						c.id,
						c.company_name,
						pt.name AS payment_type
					FROM
						payment_processing_fees ppf
						LEFT JOIN clients c ON ( c.id = ppf.client_id )
						LEFT JOIN payment_types pt ON ( pt.id = ppf.payment_type_id )
					WHERE
						ppf.client_id IN ( ' . implode( ',', $arrintClientIds ) . ' )
						AND ppf.payment_type_id = ' . ( int ) $intPaymentTypeId;

		return fetchData( $strSql, $objDatabase );

	}

	public static function deletePaymentProcessingFeeById( $intPaymentProcessingFeesId, $objDatabase ) {

		if( true == is_null( $intPaymentProcessingFeesId ) ) {
			return NULL;
		}

		$strSql = 'DELETE
					FROM
						payment_processing_fees
					WHERE
						id = ' . ( int ) $intPaymentProcessingFeesId;

		return false == is_null( $objDatabase->execute( $strSql ) );

	}

	public static function fetchPaymentProcessingFeeDetailsById( $intPaymentProcessingFeesId, $objDatabase ) {

		if( true == is_null( $intPaymentProcessingFeesId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						payment_processing_fees ppf
					WHERE
						ppf.id =' . ( int ) $intPaymentProcessingFeesId;

		$arrmixValues = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrmixValues ) && true == isset( $arrmixValues[0] ) ) {
			return $arrmixValues[0];
		} else {
			return array();
		}

	}

	public static function fetchPaymentProcessingFeeWithPaymentTypeByCid( $intCid, $objDatabase ) {

		if( true == is_null( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ppf.id,
						ppf.client_id,
						ppf.payment_type_id,
						ppf.percentage,
						pt.name
					FROM payment_processing_fees ppf
						 LEFT JOIN payment_types pt ON ( ppf.payment_type_id = pt.id )
					WHERE
						ppf.client_id = ' . ( int ) $intCid . ';';

		return fetchData( $strSql, $objDatabase );
	}

}
?>