<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInterviewTypes
 * Do not add any new functions to this class.
 */

class CInterviewTypes extends CBaseInterviewTypes {

	public static function fetchInterviewTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CInterviewType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchInterviewTypesByIsPublished( $objDatabase ) {
		return self::fetchInterviewTypes( 'SELECT * FROM interview_types WHERE is_published = 1 ORDER BY order_num', $objDatabase );
	}

	public static function fetchInterviewType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CInterviewType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>