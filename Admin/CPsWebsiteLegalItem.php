<?php

use Psi\Eos\Admin\CPsWebsiteLegalItems;
use Psi\Libraries\ExternalFileUpload\CFileUpload;
use Psi\Eos\Admin\CFileExtensions;

class CPsWebsiteLegalItem extends CBasePsWebsiteLegalItem {

	use Psi\Libraries\EosFoundation\TEosStoredObject;

	protected $m_intCid;

	/**
	 * Validate Functions
	 *
	 */

	public function valDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Date is required. ' ) );
		} elseif( false == CValidation::validateDate( $this->getDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Date is not a valid. ' ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {

		$boolIsValid = true;

		if( false == valStr( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valSeoHandle( $objDatabase ) {

		$boolIsValid = true;

		if( false == valStr( $this->getSeoHandle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'seo_handle', 'Seo handle is required. ' ) );
		} elseif( false == preg_match( '/^[a-z0-9_]+$/iD', $this->getSeoHandle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'seo_handle', 'Seo handle contains invalid characters.' ) );
		} else {
			$intPsWebsiteLegalItemcount = CPsWebsiteLegalItems::createService()->fetchPsWebsiteLegalItemCountBySeoHandle( $this->getId(), $this->getSeoHandle(), $objDatabase );
			if( 0 < $intPsWebsiteLegalItemcount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'seo_handle', 'Seo handle is already in use. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valContent() {
		$boolIsValid = true;

		if( false == valStr( $this->getContent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'content', 'Content is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valSeoHandle( $objDatabase );
				$boolIsValid &= $this->valDate();
				$boolIsValid &= $this->valContent();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objDatabase = NULL, $boolReturnSqlOnly = false ) {

		if( !parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( !$this->insertorUpdateStoredObject( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase = NULL, $boolReturnSqlOnly = false ) {

		if( !parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( !$this->insertorUpdateStoredObject( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function buildFilePath() {
		return 'legal_items/' . $this->getId() . '/';
	}

	public function getPsWebsiteLegalItemAttachmentsRelativePath() {
		return 'legal_items/' . $this->getId() . '/';
	}

	public function createPsWebsiteLegalItemAttachment( $intCurrentUserId, $intPsWebsiteLegalItemId, $strUploadedFileName ) {

		$objPsWebsiteLegalItemAttachment = new CPsWebsiteLegalItemAttachment();

		$objPsWebsiteLegalItemAttachment->setPsWebsiteLegalItemId( $intPsWebsiteLegalItemId );
		$objPsWebsiteLegalItemAttachment->setFileName( $strUploadedFileName );
		$objPsWebsiteLegalItemAttachment->setFilePath( $this->getPsWebsiteLegalItemAttachmentsRelativePath() );
		$objPsWebsiteLegalItemAttachment->setUpdatedBy( $intCurrentUserId );
		$objPsWebsiteLegalItemAttachment->setUpdatedOn( 'NOW()' );
		$objPsWebsiteLegalItemAttachment->setCreatedBy( $intCurrentUserId );
		$objPsWebsiteLegalItemAttachment->setCreatedOn( 'NOW()' );

		return $objPsWebsiteLegalItemAttachment;
	}

	public function getUniqueFileName( $strOldFileName ) {

		$strFileInfo = pathinfo( CFileUpload::cleanFilename( $strOldFileName ) );
		$strFileName = strtotime( 'now' ) . '_' . $strFileInfo['filename'] . '.' . $strFileInfo['extension'];
		return $strFileName;
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function uploadObject( $objObjectStorageGateway, $strUploadPath ) {

		if( !valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( __( 'Failed to get IObjectStorage' ), E_USER_WARNING );
			return false;
		}

		$arrmixGatewayRequest = $this->createPutGatewayRequest( [ 'data' => CFileIo::fileGetContents( $strUploadPath ) ] );

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( $arrmixGatewayRequest );

		if( $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to put storage object.' ) ) );
			return false;
		}

		$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse );

		return true;
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

		switch( $strVendor ) {

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				$strPath = 'ps_website/' . $this->getImagePath();
				break;

			default:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), 'legal_item_image/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() );
		}

		return $strPath . $this->getImageFileName();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		switch( $strVendor ) {

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return 'Global/';

			default:
				return CONFIG_OSG_BUCKET_DOCUMENTS;

		}

	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

	public function getDocumentFromCloud( $objObjectStorageGateway ) {

		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'outputFile' => 'temp' ] );
		return $objObjectStorageGateway->getObject( $arrmixStorageArgs );

	}

	public function downloadObject( $objObjectStorageGateway, $strFileName, $strDispositionType, $boolIsFullPath = false ) {

		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'outputFile' => 'temp', 'mountSystem' => 'NON_BACKUP_MOUNTS' ] );

		$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixStorageArgs );
		if( $arrobjObjectStorageResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not present or has been deleted.' ) ) );
			return false;
		}

		$strFullPath = $arrobjObjectStorageResponse['outputFile'];

		if( $boolIsFullPath ) {
			return $strFullPath;
		}

		$arrstrFileInfo = pathinfo( $strFullPath );

		if( valArr( $arrstrFileInfo ) && !is_null( $arrstrFileInfo['extension'] ) ) {
			$objFileExtension = CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileInfo['extension'], $this->m_objDatabase );
		}

		header( 'Pragma:public' );
		header( 'Expires:0' );
		header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control:public' );
		header( 'Content-Description:File Transfer' );
		header( 'Content-type: ' . $objFileExtension->getMimeType() );
		header( 'Content-Disposition: ' . $strDispositionType . '; filename="' . $strFileName . '"' );
		echo CFileIo::fileGetContents( $strFullPath );
		exit();
	}

	public function deleteObject( $objObjectStorageGateway, $intUserId ) {

		$arrmixGatewayRequest         = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest();
		$objDeleteObjectResponse      = $objObjectStorageGateway->deleteObject( $arrmixGatewayRequest );

		if( valObj( $objDeleteObjectResponse, 'CRouterObjectStorageGateway' ) && $objDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to remove the file.' ) );
			return false;
		}

		if( !$this->deleteStoredObject( $intUserId, $this->m_objDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>