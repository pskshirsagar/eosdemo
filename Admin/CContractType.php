<?php

class CContractType extends CBaseContractType {

	const STANDARD			= 1;
	const RENEWAL			= 2;
	const AMSI				= 3;
	const SMALL_PROPERTY	= 4;
    const SYSTEM_CONTRACT	= 5;

}
?>