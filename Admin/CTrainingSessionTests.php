<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingSessionTests
 * Do not add any new functions to this class.
 */

class CTrainingSessionTests extends CBaseTrainingSessionTests {

	public static function fetchTrainingSessionTestsByTrainingSessionIds( $arrintTrainingSessionIds, $objDatabase ) {
		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;
		return self::fetchTrainingSessionTests( 'SELECT * FROM training_session_tests WHERE training_session_id IN ( ' . implode( ',', $arrintTrainingSessionIds ) . ' )', $objDatabase );
	}

}
?>