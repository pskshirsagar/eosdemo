<?php

class CCompanyValueNominationWinner extends CBaseCompanyValueNominationWinner {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyValueId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyValueNominationBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>