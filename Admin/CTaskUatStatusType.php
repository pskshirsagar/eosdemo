<?php

class CTaskUatStatusType extends CBaseTaskUatStatusType {

	const UAT_READY			= 1;
	const PASSED			= 2;
	const FAILED			= 3;
	const UNABLE_TO_PERFORM	= 4;
	const NOT_NEEDED		= 5;

	public static $c_arrmixTaskUATStatuses = [
		self::UAT_READY         => 'UAT Ready',
		self::PASSED            => 'Passed',
		self::FAILED            => 'Failed',
		self::UNABLE_TO_PERFORM => 'Unable To Perform',
		self::NOT_NEEDED        => 'Not Needed'
	];

	public static $c_arrintNoteRequiredUATStatuses = [
		self::FAILED,
		self::UNABLE_TO_PERFORM,
		self::NOT_NEEDED,
	];

	public static $c_arrintUATStatusesForFollowUpNeeded = [
		self::FAILED,
		self::UNABLE_TO_PERFORM
	];

	public static function getTaskUatStatusTypes() {
		$arrstrTaskUatStatusTypes[self::UAT_READY]         = [ 'id' => self::UAT_READY, 'name' => 'UAT Ready' ];
		$arrstrTaskUatStatusTypes[self::PASSED]            = [ 'id' => self::PASSED, 'name' => 'Passed' ];
		$arrstrTaskUatStatusTypes[self::FAILED]            = [ 'id' => self::FAILED, 'name' => 'Failed' ];
		$arrstrTaskUatStatusTypes[self::UNABLE_TO_PERFORM] = [ 'id' => self::UNABLE_TO_PERFORM, 'name' => 'Unable To Perform' ];
		$arrstrTaskUatStatusTypes[self::NOT_NEEDED]        = [ 'id' => self::NOT_NEEDED, 'name' => 'Not Needed' ];

		return $arrstrTaskUatStatusTypes;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>