<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyPricings
 * Do not add any new functions to this class.
 */

class CCompanyPricings extends CBaseCompanyPricings {

	public static function fetchCompanyPricingByCidByCompanyPricingTypeId( $intCid, $intCompanyPricingTypeId, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM company_pricings WHERE cid = ' . ( int ) $intCid . ' AND company_pricing_type_id = ' . ( int ) $intCompanyPricingTypeId . ' LIMIT 1';
		return self::fetchCompanyPricing( $strSql, $objAdminDatabase );
	}

	 public static function fetchCompanyPricingsByCidByAccountId( $intCid, $intAccountId, $objDatabase, $boolIsShowDisabledData = false ) {

		$strSql = 'SELECT cp.*, cpt.name as company_pricing_type_name
					FROM
						company_pricings cp
						JOIN company_pricing_types cpt ON ( cpt.id = cp.company_pricing_type_id )
					WHERE
						cp.cid = ' . ( int ) $intCid;

		$strSql .= ( false == is_null( $intAccountId ) && 0 < $intAccountId ) ? ' AND cp.account_id = ' . ( int ) $intAccountId : ' AND cp.account_id IS NULL ';
		$strSql .= ( false == $boolIsShowDisabledData ) ? ' AND cp.deleted_on IS NULL ' : ' ';
		$strSql .= ' ORDER BY cpt.name';

		return self::fetchCompanyPricings( $strSql, $objDatabase );
	 }

	public static function fetchCompanyPricingByCidByPropertyIdByCompanyPricingTypeId( $intCid, $intPropertyId, $intCompanyPricingTypeId, $objDatabase ) {

		$strSql = 'SELECT cp.*
					FROM
						company_pricings cp
						JOIN company_pricing_types cpt ON ( cpt.id = cp.company_pricing_type_id )
					WHERE
						cp.cid = ' . ( int ) $intCid . '
						AND ( cp.property_id = ' . ( int ) $intPropertyId . ' OR cp.property_id IS NULL )
						AND company_pricing_type_id = ' . ( int ) $intCompanyPricingTypeId . '
						AND cp.deleted_on IS NULL
					ORDER BY
						property_id ASC
					LIMIT 1';

		return self::fetchCompanyPricing( $strSql, $objDatabase );
	}

	public static function fetchCompanyPricingByCidByPropertyIds( $intCid, $arrintPropertyIds, $objAdminDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT cp.*
					FROM
						company_pricings cp
					WHERE
						cp.cid = ' . ( int ) $intCid . '
						AND ( cp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						AND cp.deleted_on IS NULL ';

		return self::fetchCompanyPricings( $strSql, $objAdminDatabase );
	}

	public static function fetchCompanyPricingsByCid( $intCid, $objAdminDatabase, $boolIsShowDisabledData = false ) {

		$strSql = 'SELECT cp.*, cpt.name as company_pricing_type_name
					FROM
						company_pricings cp
						JOIN company_pricing_types cpt ON ( cpt.id = cp.company_pricing_type_id )
					WHERE
						cp.cid = ' . ( int ) $intCid . '
						AND ( cp.property_id IS NULL )';

		$strSql .= ( false == $boolIsShowDisabledData ) ? ' AND cp.deleted_on IS NULL ' : ' ';
		$strSql .= ' ORDER BY cpt.name';

		return self::fetchCompanyPricings( $strSql, $objAdminDatabase );
	}

}
?>