<?php

class CTestSuiteType extends CBaseTestSuiteType {

	const UNIT_TEST = 1;

	/**
	 * @param $strTestSuiteType
	 * @return int
	 * @throws \Exception
	 */
	public static function getTestSuiteTypeIdByName( $strTestSuiteType ) : int {
		switch( $strTestSuiteType ) {
			case 'Unit Test':
			case 'unit test':
			case 'unit':
				$intTestSuiteTypeId = self::UNIT_TEST;
				break;

			default:
				throw new UnexpectedValueException( 'Undefined test suite type: ' . $strTestSuiteType );
		}

		return $intTestSuiteTypeId;
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>