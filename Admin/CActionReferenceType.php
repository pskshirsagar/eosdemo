<?php

class CActionReferenceType extends CBaseActionReferenceType {

	const CONTRACT 					= 1;
	const PROPERTY 					= 2;
	const SALES_ENGINEER_DEMO 		= 3;
	const PERSON 					= 4;
	const PRODUCT 					= 5;
	const DAILER_DEMO 				= 6;
	const TASK 						= 7;
	const EMPLOYEE 					= 8;
	const DELENQUENCY_PROPERTY		= 9;
	const DELENQUENCY_LEVEL_TYPE	= 10;
	const SUCESS_REVIEW_REFERENCE	= 11;
	const BILLING_REQUEST 			= 12;
	const REPORT		 			= 13;
	const CONTRACT_DRAFT		 	= 14;
	const COMPANY_PAYMENT_FAILURE   = 15;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>