<?php

class CClientName extends CBaseClientName {

	protected $m_boolRemove;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['remove'] ) )	$this->setRemove( $arrmixValues['remove'] );

		return;
	}

	public function setRemove( $boolRemove ) {
		$this->m_boolRemove = $boolRemove;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getRemove() {
		return $this->m_boolRemove;
	}

	public function valClientNameTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getClientNameTypeId() ) || false == is_numeric( $this->getClientNameTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_name_type_id', 'Name type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valClientName() {
		$boolIsValid = true;

		if( false == valStr( $this->getClientName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_name', 'Name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valStartDatetime() {
		$boolIsValid = true;

		if( false == valStr( $this->getStartDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_datetime', 'Start date is required. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valClientNameTypeId();
				$boolIsValid &= $this->valClientName();
				$boolIsValid &= $this->valStartDatetime();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>