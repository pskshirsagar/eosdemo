<?php

class CPsJobPostingStatus extends CBasePsJobPostingStatus {

	const NEW_STATUS 	= 1;
	const IN_PROCESS 	= 2;
	const ON_HOLD 		= 3;
	const SELECTED		= 4;
	const REJECTED 		= 5;
	const HOLD			= 'On Hold';
	const APPROVE		= 'Approve';
	const REJECT		= 'Reject';
	const NEW			= 'New';

	public static $c_arrmixPsJobPostingStatuses = array(
		self::NEW_STATUS 	=> array( 'name' => 'New' ),
		self::IN_PROCESS 	=> array( 'name' => 'In Process' ),
		self::ON_HOLD 		=> array( 'name' => 'On Hold' ),
		self::SELECTED 		=> array( 'name' => 'Selected' ),
		self::REJECTED 		=> array( 'name' => 'Rejected' )
	);

	public static $c_arrmixPsJobPostingStatusTypes = array(
		'New'			=> self::NEW_STATUS,
		'In Process'	=> self::IN_PROCESS,
		'On Hold'		=> self::ON_HOLD,
		'Selected'		=> self::SELECTED,
		'Rejected'		=> self::REJECTED
	);

	public static $c_arrintOfferExtentedStatusIds = array(
		self::IN_PROCESS,
		self::REJECTED
	);

	public static $c_arrintOfferAcceptedStatusIds = array(
		self::SELECTED,
		self::REJECTED
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>