<?php

class CPurchaseRequest extends CBasePurchaseRequest {
	use TPurchaseRequestEncryptDecrypt;

	const IN_PROCESS 			= 1;
	const DENIED 	 			= 2;
	const APPROVED 	 			= 3;
	const PENDING_BY_MANAGER	= 6;
	const ALLOCATED 			= 7;
	const HIRING_IN_PROCESS		= 8;
	const DENIED_BY_MANAGER		= 9;
	const APPROVED_BY_MANAGER	= 10;

	const TAGGED_PR_ID				= 1;
	const UNTAGGED_PR_ID			= 2;
	const APPROVED_PR_ID			= 3;
	const UNAPPROVED_PR_ID			= 4;
	const PR_ON_HOLD_PR_ID			= 18;
	const WORK_IN_PROGRESS_PR_ID	= 17;

	const DEFAULT_FINAL_APPROVER_FOR_INDIA = CEmployee::ID_PREETAM_YADAV;
	const DEFAULT_FINAL_APPROVER		   = CEmployee::ID_CHASE_HARRINGTON;

	const PURCHASE_REQUEST_OPTION_FILL_POSITION		= 1;
	const PURCHASE_REQUEST_OPTION_HOLD_POSITION		= 2;
	const PURCHASE_REQUEST_OPTION_REMOVE_POSITION	= 3;

	public static $c_arrstrPRPositionOptions = [
		self::PURCHASE_REQUEST_OPTION_FILL_POSITION		=> 'Fill Position',
		self::PURCHASE_REQUEST_OPTION_HOLD_POSITION		=> 'Hold Position',
		self::PURCHASE_REQUEST_OPTION_REMOVE_POSITION	=> 'Remove Position'
	];

	public static $c_arrstrPRFilterData = [
		self::TAGGED_PR_ID				=> 'Tagged PRs',
		self::UNTAGGED_PR_ID			=> 'Untagged PRs',
		self::APPROVED_PR_ID			=> 'Approved PRs',
		self::UNAPPROVED_PR_ID			=> 'Unapproved PRs',
		self::PR_ON_HOLD_PR_ID			=> 'On Hold PRs',
		self::WORK_IN_PROGRESS_PR_ID	=> 'Work In Progress PRs'
	];

	// This is related to new hire and employee_application related status.
	public static $c_arrstrCustomNewHirePrStatuses = [
		'1' => 'Approved',
		'2' => 'Work in progress',
		'3' => 'Candidate Tagged',
		'4' => 'Closed',
		'5' => 'On Hold'
	];

	protected $m_intRequestRequirementTypeId;
	protected $m_intReplacedEmployeeId;
	protected $m_intPurchaseRequestDesignationId;
	protected $m_intYearlyWageNew;
	protected $m_intYearlyWageOld;
	protected $m_intHourlyRateOld;
	protected $m_intHourlyRateNew;
	protected $m_intResourceRequisitionId;
	protected $m_intRecentApproverOrderNum;
	protected $m_intApproverOrderNum;
	protected $m_intProductId;

	protected $m_fltBaseAmount;
	protected $m_boolIsDeleted;
	protected $m_boolIsEditedButtonDisplay;
	protected $m_boolIsApprovedByManager;
	protected $m_boolIsFinalApproved;
	protected $m_boolIsRequestCompleted;
	protected $m_boolIsDeniedByManager;
	protected $m_boolIsAssociatedPurchaseRequest = false;

	protected $m_strReplacedEmployees;
	protected $m_strRequirementType;
	protected $m_strCountryCode;
	protected $m_strPurchaseRequestStatus;
	protected $m_strEmployeeName;
	protected $m_strPsAssetTypeName;
	protected $m_strDesignationNameOld;
	protected $m_strDesignationNameNew;
	protected $m_strPurchaseRequestTypeName;
	protected $m_strRequestedByEmployeeName;
	protected $m_strPurchaseRequestTooltipStatus;
	protected $m_strPayrollRemotePrimaryKey;
	protected $m_strApprovalsApprovedOn;
	protected $m_strApprovalsDeniedOn;
	protected $m_strRecentApproverEmployeeName;
	protected $m_strAllocatedResourceName;
	protected $m_strTeamName;
	protected $m_strDesignation;
	protected $m_intDepartmentId;

	/**
	* Set Functions
	*
	*/

 	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['purchase_request_status'] ) ) 			$this->setpurchaseRequestStatus( $arrstrValues['purchase_request_status'] );
		if( true == isset( $arrstrValues['employee_name'] ) ) 						$this->setEmployeeName( $arrstrValues['employee_name'] );
		if( true == isset( $arrstrValues['ps_asset_type_name'] ) ) 					$this->setPsAssetTypeName( $arrstrValues['ps_asset_type_name'] );
		if( true == isset( $arrstrValues['purchase_request_type_name'] ) ) 			$this->setPurchaseRequestTypeName( $arrstrValues['purchase_request_type_name'] );
		if( true == isset( $arrstrValues['designation_name_old'] ) ) 				$this->setDesignationNameOld( $arrstrValues['designation_name_old'] );
		if( true == isset( $arrstrValues['designation_name_new'] ) ) 				$this->setDesignationNameNew( $arrstrValues['designation_name_new'] );
		if( true == isset( $arrstrValues['requested_by_employee_name'] ) ) 			$this->setRequestedByEmployeeName( $arrstrValues['requested_by_employee_name'] );
		if( true == isset( $arrstrValues['replaced_employee_id'] ) ) 				$this->setReplacedEmployeeId( $arrstrValues['replaced_employee_id'] );
		if( true == isset( $arrstrValues['request_requirement_type_id'] ) ) 		$this->setRequestRequirementTypeId( $arrstrValues['request_requirement_type_id'] );
		if( true == isset( $arrstrValues['country_code'] ) ) 						$this->setCountryCode( $arrstrValues['country_code'] );
		if( true == isset( $arrstrValues['yearly_wage_new'] ) )						$this->setYearlyWageNew( $arrstrValues['yearly_wage_new'] );
		if( true == isset( $arrstrValues['yearly_wage_old'] ) )						$this->setYearlyWageOld( $arrstrValues['yearly_wage_old'] );
		if( true == isset( $arrstrValues['hourly_rate_old'] ) )						$this->setHourlyRateOld( $arrstrValues['hourly_rate_old'] );
		if( true == isset( $arrstrValues['hourly_rate_new'] ) )						$this->setHourlyRateNew( $arrstrValues['hourly_rate_new'] );
		if( true == isset( $arrstrValues['resource_requisition_id'] ) )				$this->setResourceRequisitionId( $arrstrValues['resource_requisition_id'] );
		if( true == isset( $arrstrValues['approvals_approved_on'] ) )				$this->setApprovalsApprovedOn( $arrstrValues['approvals_approved_on'] );
		if( true == isset( $arrstrValues['approvals_denied_on'] ) )					$this->setApprovalsDeniedOn( $arrstrValues['approvals_denied_on'] );
		if( true == isset( $arrstrValues['recent_approver_employee_name'] ) )		$this->setRecentApproverEmployeeName( $arrstrValues['recent_approver_employee_name'] );
		if( true == isset( $arrstrValues['recent_approver_order_num'] ) )			$this->setRecentApproverOrderNum( $arrstrValues['recent_approver_order_num'] );
		if( true == isset( $arrstrValues['approved_order_num'] ) )					$this->setApproverOrderNum( $arrstrValues['approved_order_num'] );
		if( true == isset( $arrstrValues['allocated_resource_name'] ) )				$this->setAllocatedResourceName( $arrstrValues['allocated_resource_name'] );
		if( true == isset( $arrstrValues['product_id'] ) )							$this->setProductId( $arrstrValues['product_id'] );
		if( true == isset( $arrstrValues['designation'] ) )							$this->setDesignation( $arrstrValues['designation'] );
		if( true == isset( $arrstrValues['department_id'] ) )						$this->setDepartmentId( $arrstrValues['department_id'] );
		if( true == isset( $arrstrValues['team_name'] ) )							$this->setTeamName( $arrstrValues['team_name'] );
	}

	public function setPurchaseRequestStatus( $strPurchaseRequestStatus ) {
		return $this->m_strPurchaseRequestStatus = $strPurchaseRequestStatus;
	}

 	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = $strEmployeeName;
	}

	public function setRequestedByEmployeeName( $strRequestedByEmployeeName ) {
		$this->m_strRequestedByEmployeeName = $strRequestedByEmployeeName;
	}

	public function setPurchaseRequestTooltipStatus( $strPurchaseRequestTooltipStatus ) {
		$this->m_strPurchaseRequestTooltipStatus = $strPurchaseRequestTooltipStatus;
	}

	public function setIsApprovedByManager( $boolIsApprovedByManager ) {
		$this->m_boolIsApprovedByManager = $boolIsApprovedByManager;
	}

	public function setIsFinalApproved( $boolIsFinalApproved ) {
		$this->m_boolIsFinalApproved = $boolIsFinalApproved;
	}

	public function setIsRequestCompleted( $boolIsRequestCompleted ) {
		$this->m_boolIsRequestCompleted = $boolIsRequestCompleted;
	}

	public function setRequestRequirementTypeId( $intRequestRequirementTypeId ) {
		$this->m_intRequestRequirementTypeId = $intRequestRequirementTypeId;
	}

	public function setReplacedEmployeeId( $intReplacedEmployeeId ) {
		$this->m_intReplacedEmployeeId = $intReplacedEmployeeId;
	}

	public function setIsDeniedByManager( $boolIsDeniedByManager ) {
		$this->m_boolIsDeniedByManager = $boolIsDeniedByManager;
	}

	public function setPsAssetTypeName( $strPsAssetTypeName ) {
		$this->m_strPsAssetTypeName = $strPsAssetTypeName;
	}

	public function setPurchaseRequestTypeName( $strPurchaseRequestTypeName ) {
		$this->m_strPurchaseRequestTypeName = $strPurchaseRequestTypeName;
	}

	public function setDesignationNameOld( $strDesignationNameOld ) {
		$this->m_strDesignationNameOld = $strDesignationNameOld;
	}

	public function setDesignationNameNew( $strDesignationNameNew ) {
		$this->m_strDesignationNameNew = $strDesignationNameNew;
	}

	public function setBaseAmountDollars( $strBaseAmountDollars ) {
		$strBaseAmountDollars = CStrings::strTrimDef( $strBaseAmountDollars, 20, NULL, true );
		$this->setBaseAmountDollarsEncrypted( $this->getEncryptedAmount( $strBaseAmountDollars ) );
	}

	public function setAnnualBonusPotential( $strAnnualBonusPotential ) {
		$strAnnualBonusPotential = CStrings::strTrimDef( $strAnnualBonusPotential, 20, NULL, true );
		$this->setAnnualBonusPotentialEncrypted( $this->getEncryptedAmount( $strAnnualBonusPotential ) );
	}

	public function setNewAmountDollars( $strNewAmountDollars ) {
		$strNewAmountDollars = CStrings::strTrimDef( $strNewAmountDollars, 20, NULL, true );
		$this->setNewAmountDollarsEncrypted( $this->getEncryptedAmount( $strNewAmountDollars ) );
	}

	public function setBaseAmountDollarsDoubleEncrypted( $strDoubleEncryptionKey = NULL ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			$this->setBaseAmountDollarsEncrypted( $this->getEncryptedAmount( $this->m_strBaseAmountDollarsEncrypted, md5( $strDoubleEncryptionKey ) ) );
		}
	}

	public function setNewAmountDollarsDoubleEncrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			$this->setNewAmountDollarsEncrypted( $this->getEncryptedAmount( $this->m_strNewAmountDollarsEncrypted, md5( $strDoubleEncryptionKey ) ) );
		}
	}

	public function setHourlyRateOld( $fltHourlyRateOld ) {
		$fltHourlyRateOld = CStrings::strTrimDef( $fltHourlyRateOld, 20, NULL, true );
		$this->setHourlyRateOldEncrypted( $this->getEncryptedAmount( $fltHourlyRateOld ) );
	}

	public function setHourlyRateNew( $fltHourlyAmountNew ) {

		$fltHourlyAmountNew = CStrings::strTrimDef( $fltHourlyAmountNew, 20, NULL, true );

		$this->setHourlyRateNewEncrypted( $this->getEncryptedAmount( $fltHourlyAmountNew ) );

	}

	public function setHourlyRateOldDoubleEncrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			$this->setHourlyRateOldEncrypted( $this->getEncryptedAmount( $this->m_strHourlyRateOldEncrypted, md5( $strDoubleEncryptionKey ) ) );
		}
	}

 	public function setHourlyRateNewDoubleEncrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			$this->setHourlyRateNewEncrypted( $this->getEncryptedAmount( $this->m_strHourlyRateNewEncrypted, md5( $strDoubleEncryptionKey ) ) );
		}
	}

	public function setApprovedAmountDollars( $strApprovedAmountDollars ) {
		$strApprovedAmountDollars = CStrings::strTrimDef( $strApprovedAmountDollars, 20, NULL, true );
		$this->setApprovedAmountDollarsEncrypted( $this->getEncryptedAmount( $strApprovedAmountDollars ) );
	}

	public function setApprovedAmountDollarsDoubleEncrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			$this->setApprovedAmountDollarsEncrypted( $this->getEncryptedAmount( $this->m_strApprovedAmountDollarsEncrypted, md5( $strDoubleEncryptionKey ) ) );
		}
	}

	public function setHourlyRateApproved( $fltHourlyRateApproved ) {
		$fltHourlyRateApproved = CStrings::strTrimDef( $fltHourlyRateApproved, 20, NULL, true );
		$this->setHourlyRateApprovedEncrypted( $this->getEncryptedAmount( $fltHourlyRateApproved ) );
	}

	public function setHourlyRateApprovedDoubleEncrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			$this->setHourlyRateApprovedEncrypted( $this->getEncryptedAmount( $this->m_strHourlyRateApprovedEncrypted, md5( $strDoubleEncryptionKey ) ) );
		}
	}

 	public function setBaseAmount( $fltBaseAmount ) {
		$this->m_fltBaseAmount = CStrings::strToFloatDef( $fltBaseAmount, NULL, false, 2 );
	}

	public function setIsDeleted( $boolIsDeletedFlag ) {
		$this->m_boolIsDeleted = $boolIsDeletedFlag;
	}

 	public function setIsEditedButtonDisplay( $boolEditedButtonDisplay ) {
		$this->m_boolIsEditedButtonDisplay = $boolEditedButtonDisplay;
	}

	public function setReplacedEmployees( $strReplacedEmployees ) {
		$this->m_strReplacedEmployees = $strReplacedEmployees;
	}

 	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

	public function setRequirementType( $strRequirementType ) {
		$this->m_strRequirementType = $strRequirementType;
	}

	public function setNewAmount( $fltNewAmount ) {
		$this->m_fltNewAmount = CStrings::strToFloatDef( $fltNewAmount, NULL, false, 2 );
	}

	public function setYearlyWageNew( $intYearlyWageNew ) {
		$this->m_intYearlyWageNew = $intYearlyWageNew;
	}

	public function setYearlyWageOld( $intYearlyWageOld ) {
 		$this->m_intYearlyWageOld = $intYearlyWageOld;
	}

	public function setPayrollId( $strPayrollRemotePrimaryKey ) {
		$this->m_strPayrollRemotePrimaryKey = $strPayrollRemotePrimaryKey;
	}

	public function setResourceRequisitionId( $intResourceRequisitionId ) {
		$this->m_intResourceRequisitionId = $intResourceRequisitionId;
	}

	public function setApprovalsApprovedOn( $strApprovalsApprovedOn ) {
		$this->m_strApprovalsApprovedOn = $strApprovalsApprovedOn;
	}

	public function setApprovalsDeniedOn( $strApprovalsDeniedOn ) {
		$this->m_strApprovalsDeniedOn = $strApprovalsDeniedOn;
	}

	public function setRecentApproverEmployeeName( $strRecentApproverEmployeeName ) {
		$this->m_strRecentApproverEmployeeName = $strRecentApproverEmployeeName;
	}

	public function setRecentApproverOrderNum( $intRecentApproverOrderNum ) {
		$this->m_intRecentApproverOrderNum = $intRecentApproverOrderNum;
	}

	public function setApproverOrderNum( $intApproverOrderNum ) {
		$this->m_intApproverOrderNum = $intApproverOrderNum;
	}

	public function setAllocatedResourceName( $strAllocatedResourceName ) {
		$this->m_strAllocatedResourceName = $strAllocatedResourceName;
	}

	public function setProductId( $intProductId ) {
		$this->m_intProductId = $intProductId;
	}

	public function setDesignation( $strDesignation ) {
		$this->m_strDesignation = $strDesignation;
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->m_intDepartmentId = $intDepartmentId;
	}

	public function setTeamName( $strTeamName ) {
		$this->m_strTeamName = $strTeamName;
	}

	/**
	* Get functions
	*
	*/

	public function getPurchaseRequestStatus() {
		return $this->m_strPurchaseRequestStatus;
	}

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	public function getRequestedByEmployeeName() {
		return $this->m_strRequestedByEmployeeName;
	}

	public function getPurchaseRequestTooltipStatus() {
		return $this->m_strPurchaseRequestTooltipStatus;
	}

	public function getIsApprovedByManager() {
		return $this->m_boolIsApprovedByManager;
	}

	public function getIsFinalApproved() {
		return $this->m_boolIsFinalApproved;
	}

	public function getIsRequestCompleted() {
		return $this->m_boolIsRequestCompleted;
	}

	public function getRequestRequirementTypeId() {
		return $this->m_intRequestRequirementTypeId;
	}

	public function getReplacedEmployeeId() {
		return $this->m_intReplacedEmployeeId;
	}

	public function getIsDeniedByManager() {
		return $this->m_boolIsDeniedByManager;
	}

	public function getPsAssetTypeName() {
		return $this->m_strPsAssetTypeName;
	}

	public function getPurchaseRequestTypeName() {
		return $this->m_strPurchaseRequestTypeName;
	}

	public function getDesignationNameOld() {
		return $this->m_strDesignationNameOld;
	}

	public function getDesignationNameNew() {
		return $this->m_strDesignationNameNew;
	}

	public function getBaseAmountDollars() {
		return $this->getDecryptedAmount( $this->m_strBaseAmountDollarsEncrypted );
	}

	public function getAnnualBonusPotential() {
		return $this->getDecryptedAmount( $this->m_strAnnualBonusPotentialEncrypted );
	}

	public function getNewAmountDollars() {
		return $this->getDecryptedAmount( $this->m_strNewAmountDollarsEncrypted );
	}

	public function getAnnualBonusPotentialDoubleDecrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			return $this->getNewAmountDollar( $this->m_strAnnualBonusPotentialEncrypted, $strDoubleEncryptionKey );
		}
	}

	public function getBaseAmountDollarsDoubleDecrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			return $this->getNewAmountDollar( $this->m_strBaseAmountDollarsEncrypted, $strDoubleEncryptionKey );
		}
	}

	public function getHistoryBonusAmountDollarsDoubleDecrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			return $this->getBonusHistoryAmountDollar( $this->m_strBaseAmountDollarsEncrypted, $strDoubleEncryptionKey );
		}
	}

	public function getNewAmountDollarsDoubleDecrypted( $strDoubleEncryptionKey ) {

		if( false == empty( $strDoubleEncryptionKey ) ) {
			return $this->getNewAmountDollar( $this->m_strNewAmountDollarsEncrypted, $strDoubleEncryptionKey );
		}
	}

	private function getNewAmountDollar( $strAmountDollarsEncrypted, $strDoubleEncryptionKey ) {
		$strFirstLevelDecryptedNewAmountDollars 	= $this->getDecryptedAmount( $strAmountDollarsEncrypted, md5( $strDoubleEncryptionKey ) );
		return $this->getDecryptedAmount( $strFirstLevelDecryptedNewAmountDollars );
	}

	private function getBonusHistoryAmountDollar( $strAmountDollarsEncrypted, $strDoubleEncryptionKey ) {
		$strFirstLevelDecryptedNewAmountDollars 	= $this->getDecryptedAmount( $strAmountDollarsEncrypted, $strDoubleEncryptionKey );
		return $this->getDecryptedAmount( $strFirstLevelDecryptedNewAmountDollars );
	}

	public function getHourlyRateOld() {
		return $this->getDecryptedAmount( $this->m_strHourlyRateOldEncrypted );
	}

	public function getHourlyRateNew() {
		return $this->getDecryptedAmount( $this->m_strHourlyRateNewEncrypted );
	}

	public function getHourlyRateOldDoubleDecrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			return $this->getNewAmountDollar( $this->m_strHourlyRateOldEncrypted, $strDoubleEncryptionKey );
		}
	}

	public function getHourlyRateNewDoubleDecrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			return $this->getNewAmountDollar( $this->m_strHourlyRateNewEncrypted, $strDoubleEncryptionKey );
		}
	}

	public function getApprovedAmountDollars() {
		return $this->getDecryptedAmount( $this->m_strApprovedAmountDollarsEncrypted );
	}

	public function getApprovedAmountDollarsDoubleDecrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			return $this->getNewAmountDollar( $this->m_strApprovedAmountDollarsEncrypted, $strDoubleEncryptionKey );
		}
	}

	public function getApprovedHistoryBonusAmountDollarsDoubleDecrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			return $this->getBonusHistoryAmountDollar( $this->m_strApprovedAmountDollarsEncrypted, $strDoubleEncryptionKey );
		}
	}

	public function getHourlyRateApproved() {
		return $this->getDecryptedAmount( $this->m_strHourlyRateApprovedEncrypted );
	}

	public function getHourlyRateApprovedDoubleDecrypted( $strDoubleEncryptionKey ) {
		if( false == empty( $strDoubleEncryptionKey ) ) {
			return $this->getNewAmountDollar( $this->m_strHourlyRateApprovedEncrypted, $strDoubleEncryptionKey );
		}
	}

	public function getBaseAmount() {
		return $this->m_fltBaseAmount;
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function getIsEditedButtonDisplay() {
		return $this->m_boolIsEditedButtonDisplay;
	}

	public function getReplacedEmployees() {
		return $this->m_strReplacedEmployees;
	}

	public function getRequirementType() {
		return $this->m_strRequirementType;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getNewAmount() {
		return $this->m_fltNewAmount;
	}

	public function getHourlyAmountOld() {
		return $this->m_intHourlyRateOld;
	}

	public function getHourlyAmountNew() {
		return $this->m_intHourlyRateNew;
	}

	public function getYearlyWageNew() {
		return $this->m_intYearlyWageNew;
	}

	public function getYearlyWageOld() {
		return $this->m_intYearlyWageOld;
	}

	public function getPayrollId() {
		return $this->m_strPayrollRemotePrimaryKey;
	}

	public function getResourceRequisitionId() {
		return $this->m_intResourceRequisitionId;
	}

	public function getApprovalsApprovedOn() {
		return $this->m_strApprovalsApprovedOn;
	}

	public function getApprovalsDeniedOn() {
		return $this->m_strApprovalsDeniedOn;
	}

	public function getRecentApproverEmployeeName() {
		return $this->m_strRecentApproverEmployeeName;
	}

	public function getRecentApproverOrderNum() {
		return $this->m_intRecentApproverOrderNum;
	}

	public function getApproverOrderNum() {
		return $this->m_intApproverOrderNum;
	}

	public function getAllocatedResourceName() {
		return $this->m_strAllocatedResourceName;
	}

	public function getProductId() {
		return $this->m_intProductId;
	}

	public function getDesignation() {
		return $this->m_strDesignation;
	}
	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function getTeamName() {
		return $this->m_strTeamName;
	}

	public function getAmountDoubleDecrypted( $strAmountEncrypted, $strSecretKey ) {
		$strAmountDecrypt 	= $this->getDecryptedAmount( $strAmountEncrypted, $strSecretKey );
		return $this->getDecryptedAmount( $strAmountDecrypt );
	}

	/**
	* Validate functions
	*
	*/

	public function valEmployeeId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intEmployeeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNewAmount() {
		$boolIsValid = true;
		if( 0 >= ( $this->m_fltNewAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Amount is missing.' ) );
		}
		return $boolIsValid;
	}

	public function valBaseAmount() {
		$boolIsValid = true;
		if( 0 >= ( $this->m_fltBaseAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Current Compensation is missing.' ) );
		}
		return $boolIsValid;
	}

	public function valDesignationIdOld() {
		$boolIsValid = true;

		if( true == empty( $this->m_intDesignationIdOld ) || false == is_numeric( $this->m_intDesignationIdOld ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'designation_id_old', 'Current designation is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDesignationIdNew() {
		$boolIsValid = true;

		if( true == empty( $this->m_intDesignationIdNew ) || false == is_numeric( $this->m_intDesignationIdNew ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'designation_id_new', 'New Designation is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsAssetTypeId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intPsAssetTypeId ) || false == is_numeric( $this->m_intPsAssetTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_type_id', 'Asset type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCurrentSalaryTypeId( $boolIsFromNewHire = false ) {
		$boolIsValid = true;

		if( true == empty( $this->m_intCurrentSalaryTypeId ) || false == is_numeric( $this->m_intCurrentSalaryTypeId ) ) {
			$boolIsValid = false;
			$strMessage = ( true == $boolIsFromNewHire ) ? 'Previous Employee Salary type is required.' : 'Salary type is required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_salary_type_id', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valProposedSalaryTypeId( $boolIsFromNewHire = false, $boolIsReplacement = false, $strUserCountryCode = '' ) {
		$boolIsValid = true;

		if( true == empty( $this->m_intProposedSalaryTypeId ) || false == is_numeric( $this->m_intProposedSalaryTypeId ) ) {
			$boolIsValid = false;
			if( true == $boolIsFromNewHire && true == $boolIsReplacement && CCountry::CODE_USA == $strUserCountryCode ) {
				$strMessage = 'Replacement salary type is required.';
			} elseif( true == $boolIsFromNewHire ) {
				$strMessage = 'Salary type is required.';
			} else {
				$strMessage = 'Proposed salary type is required.';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'proposed_salary_type_id', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valPurchaseRequestId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intPurchaseRequestId ) || false == is_numeric( $this->m_intPurchaseRequestId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_request_id_new_hire', 'Please associate Approved New Hire request.' ) );
		}
		return $boolIsValid;
	}

	public function valBonusTypeId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intBonusTypeId ) || false == is_numeric( $this->m_intBonusTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bonus_type_id', 'Bonus type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valBonus( $strSecretKeyEntered ) {
		$boolIsValid = true;

		$intBaseAmount = ( 1 == $this->getIsReencrypted() ) ? $this->getBaseAmountDollarsDoubleDecrypted( $strSecretKeyEntered ) : $this->getBaseAmountDollars();

		if( 0 >= $intBaseAmount && CBonusType::PAID_DAY_OFF != $this->getBonusTypeId() ) {

			if( 0 > $intBaseAmount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bonus', 'Valid Bonus is required.' ) );
			} elseif( true == empty( $intBaseAmount ) || 0 == $intBaseAmount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bonus', 'Bonus is required.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valAmount( $strSecretKeyEntered ) {
		$boolIsValid = true;

		$intBaseAmount = ( 1 == $this->getIsReencrypted() ) ? $this->getBaseAmountDollarsDoubleDecrypted( $strSecretKeyEntered ) : $this->getBaseAmountDollars();

		if( true == empty( $intBaseAmount ) || 0 >= $intBaseAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Valid amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function valApprovedAmount( $strSecretKeyEntered ) {
		$boolIsValid = true;

		$intApprovedAmount = ( 1 == $this->getIsReencrypted() ) ? $this->getApprovedAmountDollarsDoubleDecrypted( $strSecretKeyEntered ) : $this->getApprovedAmountDollars();

		if( CBonusType::PAID_DAY_OFF != $this->getBonusTypeId() ) {
			if( 0 > $intApprovedAmount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approved_amount', 'Valid approved amount is required.' ) );
			} elseif( true == empty( $intApprovedAmount ) || 0 == $intApprovedAmount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approved_amount', 'Approved amount is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valEstimatedHoursOld( $boolIsFromNewHire = false ) {
		$boolIsValid = true;

		if( true == empty( $this->m_intEstimatedHoursOld ) ) {
			$boolIsValid = false;
			$strMessage = ( true == $boolIsFromNewHire ) ? 'Previous estimated hours required.' : 'Estimated hours required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_hours_old', $strMessage ) );
		} elseif( 0 > $this->m_intEstimatedHoursOld ) {
			$boolIsValid = false;
			$strMessage = ( true == $boolIsFromNewHire ) ? 'Valid previous estimated hours required.' : 'Valid estimated hours required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_hours_old', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valEstimatedHoursNew( $boolIsFromNewHire = false, $boolIsReplacement = false, $strUserCountryCode = '' ) {
		$boolIsValid = true;

		if( true == empty( $this->m_intEstimatedHoursNew ) ) {
			$boolIsValid = false;
			if( true == $boolIsFromNewHire && true == $boolIsReplacement && CCountry::CODE_USA == $strUserCountryCode ) {
				$strMessage = 'Replacement estimated hours required.';
			} elseif( true == $boolIsFromNewHire ) {
				$strMessage = 'Estimated hours required.';
			} else {
				$strMessage = 'Proposed estimated hours required.';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_hours_new', $strMessage ) );
		} elseif( 0 >= $this->m_intEstimatedHoursNew ) {
			$boolIsValid = false;
			if( true == $boolIsFromNewHire && true == $boolIsReplacement && CCountry::CODE_USA == $strUserCountryCode ) {
				$strMessage = 'Replacement estimated hours required.';
			} elseif( true == $boolIsFromNewHire ) {
				$strMessage = 'Estimated hours required.';
			} else {
				$strMessage = 'Proposed estimated hours required.';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_hours_new', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valHourlyRateOldEncrypted( $strSecretKeyEntered, $boolIsFromNewHire = false, $boolKeyEncrypted = false ) {
		$boolIsValid = true;

		if( 1 == $this->getIsReencrypted() && false == empty( $strSecretKeyEntered ) ) {
			$intHourlyAmount = ( true == $boolKeyEncrypted ) ? $this->getAmountDoubleDecrypted( $this->getHourlyRateOldEncrypted(), $strSecretKeyEntered ) : $this->getHourlyRateOldDoubleDecrypted( $strSecretKeyEntered );
		} else {
			$intHourlyAmount = $this->getHourlyRateOld();
		}

		if( true == empty( $this->m_strHourlyRateOldEncrypted ) ) {
			$boolIsValid = false;
			$strMessage = ( true == $boolIsFromNewHire ) ? 'Previous hourly amount is required.' : 'Hourly amount is required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_rate_old', $strMessage ) );
		} elseif( 0 >= $intHourlyAmount ) {
			$boolIsValid = false;
			$strMessage = ( true == $boolIsFromNewHire ) ? 'Previous Valid hourly amount is required.' : 'Valid hourly amount is required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_rate_old', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valHourlyRateNewEncrypted( $strSecretKeyEntered, $boolIsFromNewHire = false, $boolIsReplacement = false, $strUserCountryCode = '', $boolKeyEncrypted = false ) {
		$boolIsValid 		= true;

		if( 1 == $this->getIsReencrypted() && false == empty( $strSecretKeyEntered ) ) {
			$intHourlyAmountNew = ( true == $boolKeyEncrypted ) ? $this->getAmountDoubleDecrypted( $this->getHourlyRateNewEncrypted(), $strSecretKeyEntered ) : $this->getHourlyRateNewDoubleDecrypted( $strSecretKeyEntered );
		} else {
			$intHourlyAmountNew = $this->getHourlyRateNew();
		}

		if( true == empty( $this->m_strHourlyRateNewEncrypted ) ) {
			$boolIsValid = false;
			if( true == $boolIsFromNewHire && true == $boolIsReplacement && CCountry::CODE_USA == $strUserCountryCode ) {
				$strMessage = 'Replacement hourly amount required. ';
			} elseif( true == $boolIsFromNewHire ) {
				$strMessage = 'Hourly amount required. ';
			} else {
				$strMessage = 'Proposed hourly amount is required. ';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_rate_new', $strMessage ) );
		} elseif( 0 >= $intHourlyAmountNew ) {

			$boolIsValid = false;
			if( true == $boolIsFromNewHire && true == $boolIsReplacement && CCountry::CODE_USA == $strUserCountryCode ) {
				$strMessage = 'Replacement valid hourly amount required. ';
			} elseif( true == $boolIsFromNewHire ) {
				$strMessage = 'Valid hourly amount required. ';
			} else {
				$strMessage = 'Valid proposed hourly is amount required. ';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_rate_old', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valAnnualBonusPotentialEncrypted( $strSecretKeyEntered, $boolKeyEncrypted = false ) {
		$boolIsValid = true;
		if( CPurchaseRequestType::NEW_HIRE == $this->getPurchaseRequestTypeId() ) {
			if( 1 == $this->getIsReencrypted() ) {
				$intBonusAmount = ( true == $boolKeyEncrypted ) ? $this->getAmountDoubleDecrypted( $this->getAnnualBonusPotentialEncrypted(), $strSecretKeyEntered ) : $this->getAnnualBonusPotentialDoubleDecrypted( $strSecretKeyEntered );
			} else {
				$intBonusAmount = $this->getAnnualBonusPotential();
			}
		} else {
			$intBonusAmount = $this->getAnnualBonusPotential();
		}

		if( false == empty( $intBonusAmount ) && 0 > $intBonusAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'annual_bonus_potential', 'Valid annual bonus potential is required.' ) );
		}

		return $boolIsValid;
	}

	public function valReason() {
		$boolIsValid = true;

		$strErrorMessage = 'Reason is required. ';
		if( CPurchaseRequestType::ASSET_PURCHASE == $this->getPurchaseRequestTypeId() ) {
			$strErrorMessage = 'Reason/Specification is required. ';
		} else if( CPurchaseRequestType::TRAVEL == $this->getPurchaseRequestTypeId() ) {
			$strErrorMessage = 'Business purpose is required. ';
		}
		if( true == empty( $this->m_strReason ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reason', $strErrorMessage ) );
		}
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;

		$strErrorMessage = 'Note is required. ';
		if( true == empty( $this->m_strNotes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', $strErrorMessage ) );
		}
		return $boolIsValid;
	}

	public function valReopenReason() {
		$boolIsValid = true;

		if( true == empty( $this->m_strReopenedReason ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reason', 'Reason is required for reopening request.' ) );
		}
		return $boolIsValid;
	}

	public function valTeamId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intTeamId ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reason', 'Team is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;

		if( CBonusType::PAID_DAY_OFF != $this->getBonusTypeId() ) {
			if( true == empty( $this->m_strEffectiveDate ) || ( strtotime( $this->m_strEffectiveDate ) < date( 'm/d/Y', time() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', ' Effective date is required. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;

		if( true == empty( $this->m_strCountryCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', 'Please choose Position for.' ) );
		}

		return $boolIsValid;
	}

	public function valYearlyWage( $strSecretKeyEntered, $boolIsFromNewHire = false, $boolKeyEncrypted = false ) {
		$boolIsValid = true;

		if( 1 == $this->getIsReencrypted() && false == empty( $strSecretKeyEntered ) ) {
			$intBaseAmount = ( true == $boolKeyEncrypted ) ? $this->getAmountDoubleDecrypted( $this->getBaseAmountDollarsEncrypted(), $strSecretKeyEntered ) : $this->getBaseAmountDollarsDoubleDecrypted( $strSecretKeyEntered );
		} else {
			$intBaseAmount = $this->getBaseAmountDollars();
		}

		if( true == empty( $intBaseAmount ) && CPurchaseRequestType::COMP_CHANGE != $this->getPurchaseRequestTypeId() ) {
			$boolIsValid = false;
			$strMessage = ( true == $boolIsFromNewHire ) ? 'Previous employee yearly wage is required.' : 'Yearly wage is required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'yearly_wage', $strMessage ) );
		} elseif( 0 >= $intBaseAmount ) {
			$boolIsValid = false;
			$strMessage = ( true == $boolIsFromNewHire ) ? 'Valid previous employee yearly wage is required.' : 'Valid current yearly wage is required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'yearly_wage', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valYearlyWageNew( $strSecretKeyEntered, $boolIsFromNewHire = false, $boolIsReplacement = false, $strUserCountryCode = '', $boolEmployeeBonus = false, $boolKeyEncrypted = false ) {
		$boolIsValid = true;

		if( 1 == $this->getIsReencrypted() && false == empty( $strSecretKeyEntered ) ) {
			$intYearlyWageNew = ( true == $boolKeyEncrypted ) ? $this->getAmountDoubleDecrypted( $this->getNewAmountDollarsEncrypted(), $strSecretKeyEntered ) : $this->getNewAmountDollarsDoubleDecrypted( $strSecretKeyEntered );
		} else {
			$intYearlyWageNew = $this->getYearlyWageNew();
		}

		if( 0 > $intYearlyWageNew ) {
			$boolIsValid = false;
			if( true == $boolIsFromNewHire && true == $boolIsReplacement && CCountry::CODE_USA == $strUserCountryCode && false == $boolEmployeeBonus ) {
				$strMessage = 'Replacement valid yearly wage is required. ';
			} elseif( true == $boolIsFromNewHire ) {
				$strMessage = 'Valid yearly wage is required. ';
			} else {
				$strMessage = 'Valid proposed ' . ( ( false == $boolEmployeeBonus ) ? 'yearly wage' : 'bonus' ) . ' is required. ';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'yearly_wage', $strMessage ) );
		} else if( ( false == valId( $intYearlyWageNew ) || 0 == $intYearlyWageNew ) && false == $boolEmployeeBonus ) {
			$boolIsValid = false;
			if( true == $boolIsFromNewHire && true == $boolIsReplacement && CCountry::CODE_USA == $strUserCountryCode ) {
				$strMessage = 'Replacement yearly wage is required. ';
			} elseif( true == $boolIsFromNewHire ) {
				$strMessage = 'Yearly wage is required. ';
			} else {
				$strMessage = 'Proposed yearly wage is required. ';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'yearly_wage', $strMessage ) );
		} else if( true == $boolEmployeeBonus && ( false == valId( $intYearlyWageNew ) || 1 > $intYearlyWageNew ) ) {
			$boolIsValid = false;
			$strMessage = 'Proposed bonus is required.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'yearly_wage', $strMessage ) );
		}
		return $boolIsValid;
	}

	public function valTravelDate() {
		$boolIsValid = true;

		$arrmixDetails = json_decode( json_encode( $this->getDetails(), true ), true );
		if( true == empty( $arrmixDetails['travel']['start_date'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', ' Start date is required. ' ) );
		} else if( true == empty( $arrmixDetails['travel']['end_date'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', ' End date is required. ' ) );
		} else if( strtotime( $arrmixDetails['travel']['end_date'] ) < strtotime( $arrmixDetails['travel']['start_date'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', ' End date should be greater than start date. ' ) );
		}

		return $boolIsValid;
	}

	public function valDestinationCountry() {
		$boolIsValid = true;

		if( true == empty( $this->m_strDestinationCountry ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'destination_country', ' Destination country is required.' ) );
		}
		return $boolIsValid;
	}

	public function valTechnologyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsAmountRequired = false, $strSecretKeyEntered = NULL, $boolNotesRequired = false, $boolIgnoreProposedSalaryTypeId = false, $boolKeyEncrypted = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_insert_new_hire':
				$boolIsValid &= $this->valTeamId();
				$boolIsValid &= $this->valReason();
				$boolIsValid &= $this->valAnnualBonusPotentialEncrypted( $strSecretKeyEntered );
				if( true == $boolIsAmountRequired && CCountry::CODE_USA == $this->getCountryCode() ) {
					// Conditional validation
					$boolIsValid &= $this->validateConditionalPurchaseRequest( $strSecretKeyEntered );
				}
				break;

			case 'validate_update_new_hire':
				$boolIsValid &= $this->valReason();
				$boolIsValid &= $this->valTeamId();
				$boolIsValid &= $this->valAnnualBonusPotentialEncrypted( $strSecretKeyEntered );
				break;

			case 'validate_insert_comp_change':
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valDesignationIdNew();
				$boolIsValid &= $this->valProposedSalaryTypeId();
				$boolIsValid &= $this->valEffectiveDate();
				$boolIsValid &= $this->valReason();
				$boolIsValid &= $this->valDesignationIdOld();
				$boolIsValid &= $this->valAnnualBonusPotentialEncrypted( $strSecretKeyEntered );
				if( true == $boolIsAmountRequired ) {
					// Conditional validation
					$boolIsValid &= $this->validateConditionalPurchaseRequest( $strSecretKeyEntered );
				}

				if( CEmployeeSalaryType::HOURLY_BASED_EMPLOYEE == $this->m_intProposedSalaryTypeId ) {
					if( CCountry::CODE_INDIA == $this->m_strCountryCode ) {
						$boolIsValid &= $this->valEstimatedHoursNew();
						$boolIsValid &= $this->valEstimatedHoursOld();
					}
					$boolIsValid &= $this->valHourlyRateNewEncrypted( $strSecretKeyEntered );
				} else {
					$boolIsValid &= $this->valYearlyWageNew( $strSecretKeyEntered );
				}

				$boolIsValid &= $this->valApprovedAmount( $strSecretKeyEntered );
				break;

			case 'validate_insert_update_comp_change':
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valDesignationIdNew();
				$boolIsValid &= $this->valEffectiveDate();
				$boolIsValid &= $this->valReason();
				$boolIsValid &= $this->valDesignationIdOld();

				if( true == $boolIsAmountRequired ) {
					$boolIsValid &= $this->validateConditionalPurchaseRequest( $strSecretKeyEntered );
				}

				if( CEmployeeSalaryType::HOURLY_BASED_EMPLOYEE == $this->getProposedSalaryTypeId() ) {

					$boolIsValid &= $this->valHourlyRateNewEncrypted( $strSecretKeyEntered );
				} else {
					$boolIsValid &= $this->valYearlyWageNew( $strSecretKeyEntered );
				}

				$boolIsValid &= $this->valApprovedAmount( $strSecretKeyEntered );
				break;

			case 'validate_insert_employee_bonus':
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valBonusTypeId();
				$boolIsValid &= $this->valBonus( $strSecretKeyEntered );
				$boolIsValid &= $this->valEffectiveDate();
				$boolIsValid &= $this->valReason();
				break;

			case 'validate_insert_asset_purchase':
				$boolIsValid &= $this->valPsAssetTypeId();
				if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
					$boolIsValid &= $this->valEmployeeId();
				}
				$boolIsValid &= $this->valEffectiveDate();

				if( true == $boolIsAmountRequired ) {
				    $boolIsValid &= $this->valAmount( $strSecretKeyEntered );
				}

				$boolIsValid &= $this->valReason();
				break;

			case 'validate_update_employee_bonus':
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valBonus( $strSecretKeyEntered );
				$boolIsValid &= $this->valApprovedAmount( $strSecretKeyEntered );
				$boolIsValid &= $this->valEffectiveDate();
				$boolIsValid &= $this->valReason();
				break;

			case 'validate_update_asset_purchase':
				$boolIsValid &= $this->valPsAssetTypeId();
				$boolIsValid &= $this->valEffectiveDate();

				if( true == $boolIsAmountRequired ) {
				    $boolIsValid &= $this->valAmount( $strSecretKeyEntered );
				    $boolIsValid &= $this->valApprovedAmount( $strSecretKeyEntered );
				}

				$boolIsValid &= $this->valReason();
				break;

			case 'validate_reopen_new_hire':
				$boolIsValid &= $this->valReopenReason();
				break;

			case 'validate_bulk_insert_comp_change':
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valDesignationIdOld();
				$boolIsValid &= $this->valProposedSalaryTypeId();
				$boolIsValid &= $this->valEffectiveDate();
				$boolIsValid &= $this->valDesignationIdNew();
				$boolIsValid &= $this->valReason();
				$boolIsValid &= $this->valAnnualBonusPotentialEncrypted( $strSecretKeyEntered );

				if( CEmployeeSalaryType::HOURLY_BASED_EMPLOYEE == $this->m_intProposedSalaryTypeId ) {
					if( CCountry::CODE_INDIA == $this->m_strCountryCode ) {
						$boolIsValid &= $this->valEstimatedHoursNew();
					}
					$boolIsValid &= $this->valHourlyRateNewEncrypted( $strSecretKeyEntered );
				}

				if( CEmployeeSalaryType::SALARIED_EXEMPT_EMPLOYEE == $this->m_intProposedSalaryTypeId || CEmployeeSalaryType::SALARIED_NON_EXEMPT_EMPLOYEE == $this->m_intProposedSalaryTypeId ) {
					$boolIsValid &= $this->valYearlyWageNew( $strSecretKeyEntered );
				}

				$boolIsValid &= $this->validateConditionalPurchaseRequest( $strSecretKeyEntered );
				break;

			case 'validate_insert_or_update_employee_bonus':
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valBonusTypeId();
				$boolIsValid &= $this->valYearlyWageNew( $strSecretKeyEntered, false, false, '', true, $boolKeyEncrypted );
				$boolIsValid &= $this->valEffectiveDate();
				$boolIsValid &= $this->valReason();
				$boolIsValid &= $this->valAnnualBonusPotentialEncrypted( $strSecretKeyEntered, $boolKeyEncrypted );
				break;

			case 'VALIDATE_IMPORT':
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valEffectiveDate();
				$boolIsValid &= $this->valNewAmount();
				$boolIsValid &= $this->valBaseAmount();
				break;

			case 'VALIDATE_US_NEW_HIRE':
				$boolIsValid &= $this->valTeamId();
				$boolIsValid &= $this->valReason();

				if( CEmployeeSalaryType::HOURLY_BASED_EMPLOYEE == $this->m_intProposedSalaryTypeId ) {
					$boolIsValid &= $this->valHourlyRateNewEncrypted( $strSecretKeyEntered, true, false, '', $boolKeyEncrypted );
				} else {
					$boolIsValid &= $this->valYearlyWageNew( $strSecretKeyEntered, true, false, '', false, $boolKeyEncrypted );
				}

				if( true == $boolNotesRequired ) {
					$boolIsValid &= $this->valNotes();
				}
				break;

            case 'VALIDATE_ON_HOLD_FILL_POSITION':
				$boolIsValid &= $this->valEffectiveDate();
				break;

			case 'validate_insert_hris_comp_change':
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valReason();

				if( false == $boolIgnoreProposedSalaryTypeId ) {
					$boolIsValid &= $this->valProposedSalaryTypeId();
				}
				$boolIsValid &= $this->valDesignationIdNew();

				$boolIsValid &= $this->valDesignationIdOld();
				if( true == $boolIsAmountRequired ) {
					// Conditional validation
					$boolIsValid &= $this->validateConditionalPurchaseRequest( $strSecretKeyEntered, $boolKeyEncrypted );
				}

				if( CEmployeeSalaryType::HOURLY_BASED_EMPLOYEE == $this->m_intProposedSalaryTypeId ) {
					$boolIsValid &= $this->valHourlyRateNewEncrypted( $strSecretKeyEntered, false, false, '', $boolKeyEncrypted );
				} else {
					$boolIsValid &= $this->valYearlyWageNew( $strSecretKeyEntered, true, false, '', false, $boolKeyEncrypted );
				}

				$boolIsValid &= $this->valEffectiveDate();
				break;

			case 'validate_insert_or_update_travel_purchase_request':
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valDestinationCountry();
				$boolIsValid &= $this->valTravelDate();
				$boolIsValid &= $this->valReason();
				break;

			default:
				// Default Case
				break;
		}

		return $boolIsValid;
	}

	public function validateConditionalPurchaseRequest( $strSecretKeyEntered, $boolKeyEncrypted = false ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valCurrentSalaryTypeId();

		switch( $this->m_intCurrentSalaryTypeId ) {
			case CEmployeeSalaryType::HOURLY_BASED_EMPLOYEE:
				if( CCountry::CODE_INDIA == $this->m_strCountryCode ) {
					$boolIsValid &= $this->valEstimatedHoursOld();
				}
				$boolIsValid &= $this->valHourlyRateOldEncrypted( $strSecretKeyEntered, false, $boolKeyEncrypted );
				break;

			case CEmployeeSalaryType::SALARIED_EXEMPT_EMPLOYEE:
			case CEmployeeSalaryType::SALARIED_NON_EXEMPT_EMPLOYEE:
				$boolIsValid &= $this->valYearlyWage( $strSecretKeyEntered, false, $boolKeyEncrypted );
				break;

			default:
				// Default Case
				break;
		}

		return $boolIsValid;
	}

	public function validateConditionalPreviousEmployeePurchaseRequestNewHire( $strSecretKeyEntered ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valCurrentSalaryTypeId( true );

		switch( $this->m_intCurrentSalaryTypeId ) {
			case CEmployeeSalaryType::HOURLY_BASED_EMPLOYEE:
				if( CCountry::CODE_INDIA == $this->m_strCountryCode ) {
					$boolIsValid &= $this->valEstimatedHoursOld( true );
				}
				$boolIsValid &= $this->valHourlyRateOldEncrypted( $strSecretKeyEntered, true );
				break;

			case CEmployeeSalaryType::SALARIED_EXEMPT_EMPLOYEE:
			case CEmployeeSalaryType::SALARIED_NON_EXEMPT_EMPLOYEE:
				$boolIsValid &= $this->valYearlyWage( $strSecretKeyEntered, true );
				break;

			default:
				// Default Case
				break;
		}
		return $boolIsValid;
	}

	public function valConditionalPurchaseRequestProposedAmount( $strSecretKeyEntered, $boolIsReplacement = false, $strUserCountryCode = '' ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valProposedSalaryTypeId( true, $boolIsReplacement, $strUserCountryCode );
		switch( $this->m_intProposedSalaryTypeId ) {
			case CEmployeeSalaryType::HOURLY_BASED_EMPLOYEE:
				$boolIsValid &= $this->valHourlyRateNewEncrypted( $strSecretKeyEntered, true, $boolIsReplacement, $strUserCountryCode );
				if( CCountry::CODE_INDIA == $this->m_strCountryCode ) {
					$boolIsValid &= $this->valEstimatedHoursNew( true, $boolIsReplacement, $strUserCountryCode );
				}
				break;

			case CEmployeeSalaryType::SALARIED_EXEMPT_EMPLOYEE:
			case CEmployeeSalaryType::SALARIED_NON_EXEMPT_EMPLOYEE:
				$boolIsValid &= $this->valYearlyWageNew( $strSecretKeyEntered, true, $boolIsReplacement, $strUserCountryCode );
				break;

			default:
				// Default Case
				break;
		}
		return $boolIsValid;
	}

	public function validateActionOnPurchaseRequest( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'approve':
			case 'deny':
			case 'delete':

				if( true == is_null( $this->getNotes() ) || '' == $this->getNotes() ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Reason/Note is required.' ) );
				}
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	/**
	* Other functions
	*
	*/

	public function getPurchaseRequestDesignationId() {
		return $this->m_intPurchaseRequestDesignationId;
	}

	public function setPurchaseRequestDesignationId( $intPurchaseRequestDesignationId ) {
		$this->m_intPurchaseRequestDesignationId = $intPurchaseRequestDesignationId;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsAssociatedPurchaseRequest = false ) {

		$this->m_boolIsAssociatedPurchaseRequest = $boolIsAssociatedPurchaseRequest;

		if( false == $this->m_boolIsAssociatedPurchaseRequest && false == in_array( $this->getPurchaseRequestStatusTypeId(), [ CPurchaseRequestStatusType::ID_HOLD, CPurchaseRequestStatusType::ID_PENDING_BY_HR, CPurchaseRequestStatusType::ID_REMOVED, CPurchaseRequestStatusType::ID_PENDING_FOR_REMOVE_FROM_HR ] ) ) {
			$this->updatePurchaseRequestStatusTypeId( $objDatabase, $strUpdateType = 'update', $boolRequirementType = NULL, $boolIsAssociatedPurchaseRequest );
		}
		if( false == parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		return true;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false,$boolRequirementType = NULL ) {

		$intPurchaseRequestDesignationId = $this->getPurchaseRequestDesignationId();
		if( false == in_array( $this->getPurchaseRequestStatusTypeId(), [ CPurchaseRequestStatusType::ID_HOLD, CPurchaseRequestStatusType::ID_PENDING_BY_HR, CPurchaseRequestStatusType::ID_REMOVED, CPurchaseRequestStatusType::ID_PENDING_FOR_REMOVE_FROM_HR ] ) ) {
			$this->updatePurchaseRequestStatusTypeId( $objDatabase, $strUpdateType = 'insert', $boolRequirementType, $boolIsAssociatedPurchaseRequest = false, $intPurchaseRequestDesignationId );
		}

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function insertOrUpdatePurchaseRequest( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == valId( $this->getId() ) ) {
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public static function fetchFinalApproverByEmployeeId( $intEmployeeId, $intPurchaseRequestTypeId, $objDatabase,$boolRequirementType = NULL, $intDesignationId = NULL, $intAssetTypeId = NULL ) {

		if( $intPurchaseRequestTypeId == CPurchaseRequestType::ASSET_PURCHASE ) {

			$objEmployeeAddresses 	  = CEmployeeAddresses::fetchEmployeeAddressByEmployeeId( $intEmployeeId, $objDatabase );

			if( CCountry::CODE_INDIA == $objEmployeeAddresses->getCountryCode() ) {
				$intFinalApproverEmployeeId = self::DEFAULT_FINAL_APPROVER_FOR_INDIA;
			} elseif( false == is_null( $intAssetTypeId ) ) {
				$objPsAssetType = \Psi\Eos\Admin\CPsAssetTypes::createService()->fetchPsAssetTypeById( $intAssetTypeId, $objDatabase );
				if( true == $objPsAssetType->getIsAutoApprove() ) {
					$intFinalApproverEmployeeId = $objEmployeeAddresses->getEmployeeId();
				} elseif( false == is_null( $objPsAssetType->getApproverEmployeeId() ) ) {
					$intFinalApproverEmployeeId = $objPsAssetType->getApproverEmployeeId();
				}
			}

			return $intFinalApproverEmployeeId;
		}
		// For new hire and comp change, while inserting new request, we are getting designation directly. From that designation we are fetching approver id
		if( ( false == is_null( $intDesignationId ) ) && ( $intPurchaseRequestTypeId == CPurchaseRequestType::NEW_HIRE || $intPurchaseRequestTypeId == CPurchaseRequestType::COMP_CHANGE ) ) {
			$intDesignationId = $intDesignationId;
		} else {
			$objEmployee 	  = CEmployees::fetchEmployeeById( $intEmployeeId, $objDatabase );
			if( true == valObj( $objEmployee, 'CEmployee' ) ) {
				$intDesignationId = $objEmployee->getDesignationId();
			}
		}

		$objDesignation = CDesignations::fetchDesignationById( $intDesignationId, $objDatabase );
		if( false == valObj( $objDesignation, 'CDesignation' ) ) {
			 return self::DEFAULT_FINAL_APPROVER;
		}
		 // If the request is of type comp change then check comp_change_approver_id
		 // If the request is of type new hire - replacement then check replacement_approver_id
		 // Else check approver_employee_id

		if( ( $intPurchaseRequestTypeId == CPurchaseRequestType::EMPLOYEE_BONUS ) && ( false == is_null( $objDesignation->getBonusApproverEmployeeId() ) ) ) {
			$intFinalApproverEmployeeId = $objDesignation->getBonusApproverEmployeeId();
		} elseif( ( $intPurchaseRequestTypeId == CPurchaseRequestType::COMP_CHANGE ) && ( false == is_null( $objDesignation->getCompChangeApproverId() ) ) ) {
			$intFinalApproverEmployeeId = $objDesignation->getCompChangeApproverId();
		} elseif( ( $intPurchaseRequestTypeId == CPurchaseRequestType::NEW_HIRE ) && ( $boolRequirementType == CResourceRequisition::NEW_HIRE_REPLACEMENT ) && ( false == is_null( $objDesignation->getReplacementApproverId() ) ) ) {
			$intFinalApproverEmployeeId = $objDesignation->getReplacementApproverId();
		} elseif( false == is_null( $objDesignation->getApproverEmployeeId() ) ) {
			$intFinalApproverEmployeeId = $objDesignation->getApproverEmployeeId();
		} elseif( $objDesignation->getCountryCode() == 'IN' ) {
			$intFinalApproverEmployeeId = self::DEFAULT_FINAL_APPROVER_FOR_INDIA;
		} else {
			$intFinalApproverEmployeeId = self::DEFAULT_FINAL_APPROVER;
		}

		return $intFinalApproverEmployeeId;
	}

	public function updatePurchaseRequestStatusTypeId( $objDatabase, $strUpdateType = NULL, $boolRequirementType = NULL, $boolIsAssociatedPurchaseRequest = false, $intPurchaseRequestDesignationId = NULL ) {

		$this->m_boolIsAssociatedPurchaseRequest = $boolIsAssociatedPurchaseRequest;

		$objRequestedByUser		= CUsers::fetchUserDetailsById( $this->getRequestedBy(), $objDatabase );
		$intRequestedEmployeeId = $objRequestedByUser->getEmployeeId();

		if( false == is_null( $this->getEmployeeId() ) ) {
			$intEmployeeId = $this->getEmployeeId();
		} elseif( false == is_null( $this->getRequestedBy() ) ) {
			$intEmployeeId = $intRequestedEmployeeId;
		}

		// Get the final approver based on employee's designation

		$intFinalApproverEmployeeId = self::fetchFinalApproverByEmployeeId( $intEmployeeId, $this->getPurchaseRequestTypeId(), $objDatabase, $boolRequirementType, $intPurchaseRequestDesignationId, $this->getPsAssetTypeId() );

		if( $strUpdateType == 'insert' ) {

			if( true == is_null( $this->getApproverEmployeeId() ) ) {
				$this->setApproverEmployeeId( $intFinalApproverEmployeeId );
			}

			// Making the status as Approved if request is added by final approver

			if( $intFinalApproverEmployeeId == $intRequestedEmployeeId && CPurchaseRequestStatusType::ID_HOLD != $this->getPurchaseRequestStatusTypeId() ) {
				$this->setApprovedOn( 'NOW()' );
				$this->setPurchaseRequestStatusTypeId( CPurchaseRequestStatusType::ID_APPROVED );
				$this->setNotes( 'This request has been automatically approved.' );
			}

		} else {
			if( $this->getPurchaseRequestTypeId() == CPurchaseRequestType::NEW_HIRE && false == is_null( $this->getApprovedOn() ) ) {

				$arrintRequestStatus = CPurchaseRequests::fetchNewHireRequestStatusForResourceAllocation( $this->getId(), $objDatabase );
				$arrintRequestStatus = array_shift( $arrintRequestStatus );

				if( $arrintRequestStatus['is_allocated'] == 1 ) {
					$this->setPurchaseRequestStatusTypeId( CPurchaseRequestStatusType::ID_ALLOCATED );
				} elseif( false == in_array( $this->getPurchaseRequestStatusTypeId(), [ CPurchaseRequestStatusType::ID_WORK_IN_PROGRESS, CPurchaseRequestStatusType::ID_PR_ON_HOLD ] ) ) {
					$this->setPurchaseRequestStatusTypeId( CPurchaseRequestStatusType::ID_APPROVED );
				}
			} else {
				if( false == is_null( $this->getDeniedOn() ) ) {
					$this->setPurchaseRequestStatusTypeId( CPurchaseRequestStatusType::ID_DENIED );
				} elseif( false == is_null( $this->getApprovedOn() ) ) {
					$this->setPurchaseRequestStatusTypeId( CPurchaseRequestStatusType::ID_APPROVED );
				} else {
					if( false == is_null( $this->getPurchaseRequestStatusTypeId() ) ) {
						$this->setPurchaseRequestStatusTypeId( $this->getPurchaseRequestStatusTypeId() );
					} else {
						$this->setPurchaseRequestStatusTypeId( CPurchaseRequestStatusType::ID_PENDING_BY_MANAGER );
					}
				}
			}
		}
	}

	public function updateApproverEmployeeId( $strTableName,  $arrintSelectedIds, $intCurrentUserId, $objDatabase, $intApproverEmployeeId = NULL ) {

		if( false == valStr( $strTableName ) ) return NULL;
		if( false == valArr( $arrintSelectedIds ) ) return NULL;

		if( 'designations' == $strTableName ) {
			if( false == is_numeric( $intApproverEmployeeId ) ) return NULL;
			$strSql = 'UPDATE
							designations AS d
						SET
							comp_change_approver_id = CASE
															WHEN d.comp_change_approver_id = ' . ( int ) $intApproverEmployeeId . '
															THEN ' . CEmployee::ID_DAVID_BATEMEN . '
															ELSE d.comp_change_approver_id
													 END,
							replacement_approver_id = CASE
															WHEN d.replacement_approver_id = ' . ( int ) $intApproverEmployeeId . '
															THEN ' . CEmployee::ID_DAVID_BATEMEN . '
															ELSE d.replacement_approver_id
													 END,
							updated_by = ' . ( int ) $intCurrentUserId . ',updated_on = \'NOW()\'
					 WHERE d.id IN ( ' . implode( ',', $arrintSelectedIds ) . ' );';
		} else {
			$strSql = 'UPDATE ' . $strTableName . ' set approver_employee_id = ' . CEmployee::ID_DAVID_BATEMEN . ', updated_by = ' . ( int ) $intCurrentUserId . ',updated_on = \'NOW()\'  WHERE id IN ( ' . implode( ',', $arrintSelectedIds ) . ' );';
		}
		return $this->executeSql( $strSql, $this, $objDatabase );
	}

}
?>