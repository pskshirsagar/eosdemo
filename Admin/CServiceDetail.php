<?php

class CServiceDetail extends CBaseServiceDetail {

	protected $m_strServiceName;

	public function __construct() {
		parent::__construct();
		// FIXME remove this static declaration once UI implemented at CA which allows user to select specific version.
		$this->m_intMinorApiVersionId = CApiVersion::TMP_DEFAULT_MINOR_API_VERSION;
		return;
	}

	/**
	 * Get Functions
	 */

	public function getServiceName() {
		return $this->m_strServiceName;
	}

	/**
	 * Set Functions
	 */

	public function setServiceName( $strServiceName ) {
		$this->m_strServiceName = $strServiceName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['web_service_name'] ) )	$this->setServiceName( $arrmixValues['web_service_name'] );

		return;
	}

	/**
	 * Validate Functions
	 */

	public function valKey() {

		$boolIsValid = true;

		if( true == is_null( $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key is required.' ) );
		}

		return $boolIsValid;

	}

	public function valValue() {

		$boolIsValid = true;

		if( true == is_null( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Value is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valValue();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = parent::update( $intCurrentUserId, $objDatabase, true );
		if( false == valStr( $strSql ) ) {
			return false;
		}
		$strSql = rtrim( $strSql, ';' );
		$strSql .= ' AND minor_api_version_id = ' . ( int ) $this->sqlMinorApiVersionId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = rtrim( parent::delete( $intCurrentUserId, $objDatabase, true ), ';' );
		$strSql .= ' AND minor_api_version_id = ' . ( int ) $this->sqlMinorApiVersionId() . ';';
		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>