<?php

class CCommonTextField extends CBaseCommonTextField {
	const COMMON_INFORMATION_BENEFITS = 'BENEFITS';
	const COMMON_INFORMATION_PHYSICAL_REQUIREMENTS = 'PHYSICAL_REQUIREMENTS';
	const COMMON_INFORMATION_ABOUT_US = 'ABOUT_US';

	public static $c_arrstrJobPostingCommonInformation = [
		self::COMMON_INFORMATION_BENEFITS,
		self::COMMON_INFORMATION_PHYSICAL_REQUIREMENTS,
		self::COMMON_INFORMATION_ABOUT_US
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepartmentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setRequiredData( $arrmixData ) {
		$this->setKey( $arrmixData['key'] );
		$this->setCountryCode( $arrmixData['country_code'] );
		$this->setDepartmentId( $arrmixData['department_id'] );
	}

}
?>