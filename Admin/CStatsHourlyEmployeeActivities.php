<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsHourlyEmployeeActivities
 * Do not add any new functions to this class.
 */

class CStatsHourlyEmployeeActivities extends CBaseStatsHourlyEmployeeActivities {

	public static function fetchStatsHourlyEmployeeActivities( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CStatsHourlyEmployeeActivity', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchStatsHourlyEmployeeActivity( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CStatsHourlyEmployeeActivity', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function rebuildStatsHourlyEmployeeActivities( $intEmployeeId, $objDatabase ) {

		if( true == is_numeric( $intEmployeeId ) ) {
			$strEmployeeCondition = ' = ' . ( int ) $intEmployeeId;
		} else {
			$strEmployeeCondition = ' <> 0 ';
		}

		$objDatabase->begin();
		if( false === fetchData( self::buildEmployeeActivitiesInsertSql( $strEmployeeCondition ), $objDatabase ) ) {
			$objDatabase->rollback();
			return false;
		}
		$objDatabase->commit();

		$strSql = self::buildEmailActivityInsertSql( $strEmployeeCondition );
		$strSql .= self::buildClientAdminActivityInsertSql( $strEmployeeCondition );

		return fetchData( $strSql, $objDatabase );
	}

	public static function buildEmployeeActivitiesInsertSql( $strEmployeeCondition ) {

		$strSql = '
				DELETE FROM stats_hourly_employee_activities WHERE employee_id ' . $strEmployeeCondition . ' AND DATE_TRUNC( \'month\', hour )::DATE >= DATE_TRUNC( \'month\', ( NOW() - INTERVAL \'1 month\' ) )::DATE;

				INSERT INTO
					public.stats_hourly_employee_activities ( id, employee_id, HOUR, created_by, created_on )
				SELECT
					nextval ( \'stats_hourly_employee_activities_id_seq\' ) AS id,
					e.id AS employee_id,
					( d.date::timestamp + ( s.hour * INTERVAL \'1 hour\' ) ) AS HOUR,
					1 AS created_by,
					NOW ( ) AS created_on
				FROM
					employees e
					JOIN dates d ON ( e.date_started::DATE <= d.date AND COALESCE ( e.date_terminated::DATE, NOW ( ) ::DATE ) >= d.date )
					JOIN
					(
						SELECT
							generate_series AS HOUR
						FROM
							generate_series ( 0, 23 )
					) AS s ON ( 1 = 1 )
				WHERE
					e.id ' . $strEmployeeCondition . '
					AND DATE_TRUNC( \'month\', d.date )::DATE >= DATE_TRUNC( \'month\', ( NOW() - INTERVAL \'1 month\' ) )::DATE
				GROUP BY
					e.id,
					( d.date::timestamp + ( s.hour * INTERVAL \'1 hour\' ) );';

		return $strSql;
	}

	public static function buildEmailActivityInsertSql( $strEmployeeCondition ) {

		$strSql = '
			UPDATE
				public.stats_hourly_employee_activities shea
			SET
				internal_emails_sent = sub.internal_emails_sent,
				external_emails_sent = sub.external_emails_sent
			FROM
				(
					SELECT
						from_employee_id AS employee_id,
						DATE_TRUNC ( \'hour\', log_datetime ) AS hour_datetime,
						SUM ( CASE WHEN to_employee_id IS NOT NULL THEN 1 ELSE 0 END ) AS internal_emails_sent,
						SUM ( CASE WHEN to_employee_id IS NULL THEN 1 ELSE 0 END ) AS external_emails_sent
					FROM
						email_logs
					WHERE
						from_employee_id ' . $strEmployeeCondition . '
						AND DATE_TRUNC ( \'month\', log_datetime ) >= DATE_TRUNC( \'month\', ( NOW() - INTERVAL \'1 month\' ) )::DATE
					GROUP BY
						from_employee_id,
						DATE_TRUNC ( \'hour\', log_datetime )
				) AS sub
			WHERE
				sub.employee_id = shea.employee_id
				AND DATE_TRUNC ( \'hour\', sub.hour_datetime ) = DATE_TRUNC ( \'hour\', shea.hour )
				AND DATE_TRUNC ( \'month\', sub.hour_datetime ) >= DATE_TRUNC( \'month\', ( NOW() - INTERVAL \'1 month\' ) )::DATE
				AND shea.employee_id ' . $strEmployeeCondition . '
				AND ( sub.internal_emails_sent <> shea.internal_emails_sent
					OR sub.external_emails_sent <> shea.external_emails_sent );

			UPDATE
				public.stats_hourly_employee_activities shea
			SET
				internal_emails_received = sub.internal_emails_received
			FROM
				(
					SELECT
						to_employee_id AS employee_id,
						DATE_TRUNC ( \'hour\', log_datetime ) AS hour_datetime,
						COUNT ( * ) AS internal_emails_received
					FROM
						email_logs
					WHERE
						to_employee_id IS NOT NULL
						AND to_employee_id ' . $strEmployeeCondition . '
						AND DATE_TRUNC ( \'month\', log_datetime ) >= DATE_TRUNC( \'month\', ( NOW() - INTERVAL \'1 month\' ) )::DATE
					GROUP BY
						to_employee_id,
						DATE_TRUNC ( \'hour\', log_datetime )
				) AS sub
			WHERE
				sub.employee_id = shea.employee_id
				AND DATE_TRUNC ( \'hour\', sub.hour_datetime ) = DATE_TRUNC ( \'hour\', shea.hour )
				AND DATE_TRUNC ( \'month\', sub.hour_datetime ) >= DATE_TRUNC( \'month\', ( NOW() - INTERVAL \'1 month\' ) )::DATE
				AND shea.employee_id ' . $strEmployeeCondition . '
				AND sub.internal_emails_received <> shea.internal_emails_received;
		';

		return $strSql;
	}

	public static function buildClientAdminActivityInsertSql( $strEmployeeCondition ) {

		$strSql = '
			UPDATE
				public.stats_hourly_employee_activities shea
			SET
				has_admin_activity = COALESCE ( sub.has_admin_activity, 0 ),
				total_admin_clicks = COALESCE ( sub.total_admin_clicks, 0 ),
				admin_minutes_active = LEAST( COALESCE ( DATE_PART ( \'minutes\', sub.admin_minutes_active ), 0 ), 60 )
			FROM
				(
				    SELECT
						u.employee_id,
						d.hour,
						1 AS has_admin_activity,
						SUM ( CASE
								WHEN DATE_TRUNC ( \'hour\', ual.login_datetime ) = d.hour THEN COALESCE ( ual.click_count, 0 )
								ELSE 0
							END ) AS total_admin_clicks,
						SUM ( CASE
								WHEN ual.last_click IS NULL THEN INTERVAL \'0 minutes\'
								WHEN DATE_TRUNC ( \'hour\', ual.login_datetime ) <> d.hour AND DATE_TRUNC ( \'hour\', ual.last_click ) <> ( d.hour ) THEN INTERVAL \'60 minutes\'
								WHEN DATE_TRUNC ( \'hour\', ual.login_datetime ) = d.hour THEN LEAST ( ual.last_click, ( d.hour + INTERVAL \'1 hour\' ) ) - ual.login_datetime
								WHEN DATE_TRUNC ( \'hour\', ual.last_click ) = d.hour THEN ( ual.last_click - GREATEST ( d.hour, ual.login_datetime ) )
							END ) AS admin_minutes_active
					FROM
						user_authentication_logs ual
						JOIN users u ON ( ual.user_id = u.id )
						JOIN
						(
							SELECT
								( d.date + s.hour ) AS HOUR
							FROM
								dates d
								JOIN
								(
									SELECT
										( generate_series::BIGINT * INTERVAL \'1 hour\' ) AS HOUR
									FROM
										generate_series ( 0, 23 )
								) AS s ON ( 1 = 1 )
							WHERE
								d.month_start_date >= DATE_TRUNC( \'month\', ( NOW() - INTERVAL \'1 month\' ) )::DATE
								AND d.month_start_date <= DATE_TRUNC ( \'month\', NOW () ) ::DATE
						) AS d ON ( DATE_TRUNC ( \'hour\', ual.login_datetime ) <= d.hour AND DATE_TRUNC ( \'hour\', ual.last_click ) >= d.hour )
					WHERE
						u.employee_id ' . $strEmployeeCondition . '
						AND ual.login_datetime >= DATE_TRUNC( \'month\', ( NOW() - INTERVAL \'1 month\' ) )::DATE
					GROUP BY
						u.employee_id,
						d.hour
				) AS sub
			WHERE
				sub.employee_id = shea.employee_id
				AND sub.hour = shea.hour
				AND shea.hour >= DATE_TRUNC( \'month\', ( NOW() - INTERVAL \'1 month\' ) )::DATE
				AND shea.employee_id ' . $strEmployeeCondition . '
				AND ( 
					sub.has_admin_activity <> shea.has_admin_activity
					OR sub.total_admin_clicks <> shea.total_admin_clicks
					OR LEAST( DATE_PART ( \'minutes\', sub.admin_minutes_active ), 60 ) <> shea.admin_minutes_active
				);
		';

		return $strSql;
	}
}
?>