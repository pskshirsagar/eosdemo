<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskApprovals
 * Do not add any new functions to this class.
 */

class CTaskApprovals extends CBaseTaskApprovals {

	public static function fetchTaskApprovalByTaskIdByTaskApprovalTypeId( $objDatabase, $intTaskId, $intTaskApprovalTypeid ) {

		if( false == $intTaskId && false == $intTaskApprovalTypeid ) {
			return false;
		}

		$strSql = 'SELECT 
						ta.id,
						ta.task_id,
						ta.approved_by_employee_id,
						ta.approved_on,
						e.preferred_name,
						u.id as user_id,
						d.name as department_name
					FROM
						task_approvals ta
						JOIN employees e ON ( e.id = ta.approved_by_employee_id )
						JOIN users u ON ( e.id = u.employee_id )
						JOIN departments d ON ( d.id = e.department_id)
					WHERE
						task_id = ' . ( int ) $intTaskId . '
						AND approval_type_id= ' . ( int ) $intTaskApprovalTypeid;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchTaskApprovalsByTaskIdByTaskApprovalTypeIdByEmployeeIds( $objDatabase, $intTaskId, $intTaskApprovalTypeId,  $arrintReviewerIds ) {

		if( false == valArr( $arrintReviewerIds ) ) return false;

		if( false == is_numeric( $intTaskId ) || false == is_numeric( $intTaskApprovalTypeId ) ) return false;

		$strSql = 'SELECT
		                *
		             FROM
		                task_approvals
		             Where
		                task_id = ' . ( int ) $intTaskId . '
		                AND approval_type_id = ' . ( int ) $intTaskApprovalTypeId . '
		                AND approved_by_employee_id IN ( ' . implode( ',', $arrintReviewerIds ) . ' )';

		return self::fetchTaskApprovals( $strSql, $objDatabase );
	}

	public static function fetchCurrentTaskApprovalByTaskIdByTaskApprovalTypeId( $intTaskId, $intTaskApprovalTypeid, $objDatabase ) {

		if( false == is_numeric( $intTaskId ) || false == is_numeric( $intTaskApprovalTypeid ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						*
					FROM
						task_approvals
					WHERE
						task_id = ' . ( int ) $intTaskId . '
						AND approval_type_id= ' . ( int ) $intTaskApprovalTypeid . '
					ORDER BY id DESC
					LIMIT 1';

		return self::fetchTaskApproval( $strSql, $objDatabase );
	}

}
?>