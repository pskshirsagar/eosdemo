<?php

class CDocumentTemplateOrder extends CBaseDocumentTemplateOrder {

	protected $m_strClientName;
	protected $m_strPropertyName;
	protected $m_strDocumentTemplateName;
	protected $m_strAccountName;

	public function setClientName( $strCompanyName ) {
		$this->m_strClientName = CStrings::strTrimDef( $strCompanyName, NULL, false );
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, NULL, false );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function setDocumentTemplateName( $strDocumentTemplateName ) {
		$this->m_strDocumentTemplateName = CStrings::strTrimDef( $strDocumentTemplateName, NULL, false );
	}

	public function getDocumentTemplateName() {
		return $this->m_strDocumentTemplateName;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = CStrings::strTrimDef( $strAccountName, NULL, false );
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['client_name'] ) ) $this->setClientName( $arrValues['client_name'] );
		if( true == isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );
		if( true == isset( $arrValues['document_template_name'] ) ) $this->setDocumentTemplateName( $arrValues['document_template_name'] );
		if( true == isset( $arrValues['account_name'] ) ) $this->setAccountName( $arrValues['account_name'] );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>