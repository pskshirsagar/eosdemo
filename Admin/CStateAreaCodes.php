<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStateAreaCodes
 * Do not add any new functions to this class.
 */

class CStateAreaCodes extends CBaseStateAreaCodes {

	public static function fetchStateAreaCode( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CStateAreaCode', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchStateAreaCodeByAreaCode( $strAreaCode, $objDatabase ) {
		$strSql = 'SELECT * FROM state_area_codes WHERE area_code = \'' . trim( addslashes( $strAreaCode ) ) . '\' LIMIT 1';

		return self::fetchStateAreaCode( $strSql, $objDatabase );
	}
}
?>