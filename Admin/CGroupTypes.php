<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupTypes
 * Do not add any new functions to this class.
 */

class CGroupTypes extends CBaseGroupTypes {

	public static function fetchGroupTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGroupType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchGroupType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGroupType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPublishedGroupTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM group_types WHERE is_published = 1';

		return self::fetchGroupTypes( $strSql, $objDatabase );
	}
}
?>