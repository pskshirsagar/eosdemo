<?php

use Psi\Eos\Admin\CPsProducts;
use Psi\Eos\Admin\CPsProductModules;
use Psi\Eos\Admin\CPsProductCompetitors;

class CPsProduct extends CBasePsProduct {

	protected $m_arrobjPsProductModules;
	protected $m_arrobjPsProductVersions;
	protected $m_arrobjPsWebsiteProductPages;
	protected $m_arrobjTasks;
	protected $m_arrobjPsProductCompetitors;
	protected $m_arrobjPsProductOptions;
	protected $m_arrobjChargeCodes;

	protected $m_arrstrModulePath;
	protected $m_arrstrModuleUsers;
	protected $m_arrstrProductNames;

	protected $m_boolIsSelected;

	protected $m_intIsAssociated;
	protected $m_intProductBundleId;
	protected $m_intIsBundle;

	const ENTRATA					= 1;
	const PROSPECT_PORTAL			= 2;
	const RESIDENT_PORTAL			= 3;
	const RESIDENT_PAY				= 4;
	const CLIENT_ADMIN				= 5;
	const LOBBY_DISPLAY				= 6;
	const API_SERVICES				= 7; // Removed API_SERVICES
	const ENTRATA_COM               = 8;
	const GMAIL_EMAIL				= 9;
	const VACANCY					= 10;
	const SCRIPT_LAYER				= 11;
	const RESIDENT_PAY_DESKTOP		= 12;
	const PMS_INTEGRATION			= 14;
	const ILS_PORTAL				= 18;
	const GUEST_CARD_PARSING		= 19;
	const BLOG						= 22;
	const SEO_SERVICES				= 25;
	const RESIDENT_PAY_REVERSALS	= 26;
	const LEASE_EXECUTION			= 27;
	const MESSAGE_CENTER			= 28;
	const MOBILE_PORTAL				= 30;
	const CRAIGSLIST_POSTING		= 31;
	const SITE_TABLET				= 34;
	const CALL_TRACKER				= 36;
	const RESIDENT_UTILITY			= 38;
	const RESIDENT_INSURE			= 39;
	const CUSTOM_DEVELOPMENT		= 40;
	const SCREENING					= 41;
	const PRICING					= 42;
	const LEAD_MANAGEMENT			= 43;
	const PRINT_BROCHURE			= 44;
	const LEASING_CENTER			= 46;
	const PARCEL_ALERT				= 47;
	const CALL_ANALYSIS				= 49;
	const CALL_LEAD_COMMUNICATION	= 50;
	const SEM_SERVICES				= 51;
	const SEO_DASHBOARD				= 52;
	const RESIDENT_VERIFY			= 53;
	const MARKETING_TEMPLATE		= 54;
	const LEAD_ALERT				= 55;
	const INSPECTION_MANAGER		= 56;
	const FACEBOOK_INTEGRATION		= 37;
	const ENTRATA_PAAS				= 58;
	const OWNER_PORTAL				= 60;
	const JOBS						= 61;
	const ENTRATA_AFFORDABLE		= 62;
	const DOMAIN_RENEWAL			= 64;
	const VENDOR_ACCESS				= 65;
	const MISCALLANEOUS				= 66;
	const ONLINE_APPLICATION		= 2;
	const GATEWAY					= 69;
	const MERCHANT_ENROLLMENT		= 70;
	const CORPORATE_PORTAL			= 71;
	const EMPLOYEE_PORTAL			= 72;
	const VOIP						= 73;
	const ADMIN_SERVICES			= 74;
	const SERVICES					= 75;
	const LEASE_EDITOR				= 77;
	const SYSTEM					= 78;
	const INVOICE_PROCESSING		= 751;
	const MAINTENANCE				= 82;
	const ENTRATA_FACILITIES_APP	= 21981;
	const REPUTATION_ADVISOR		= 2035;
	const RESIDENT_SUPPORT_PREMIUM  = 2985;
	const DOCUMENT_MANAGEMENT		= 3395;
	const CHECK_SCANNING			= 12;
	const UTILITY_EXPENSE_MANAGEMENT = 4078;
	const BILL_PAY					= 14803;
	const STUDENT_HOUSING			= 412;
	const PPC_SERVICES				= 51;
	const SNIPPET					= 18508;
	const JOB_COSTING				= 8292;
	const MEDIA_LIBRARY				= 3323;
	const ENTRATAMATION				= 20612;
	const SMS						= 1892;
	const ENTRATA_TABLET			= 1382;
	const MASTER_POLICY 			= 23972;
	const ENTRATA_COMMERCIAL		= 28186;
	const DATA_FIX		            = 2037;
	const FACEBOOK_MARKETPLACE		= 31695;
	const MARKETING_HUB				= 31701;

	const ENTRATA_MILITARY			= 19357;
	const ENTRATA_FACILITIES		= 82;
	const ENTRATA_INSIGHTS			= 29276;
	const HISTORICAL_ACCESS         = 28524;
	const CONSULTING_SERVICES       = 31685;
	const PRICING_STUDENT           = 31687;

	const ENTRATA_ACCOUNTING     = 31696;

	// for setupverification report
	const PS_ONLINE_APPLICATION		= 14491;

	const CHAT                      = 1890;
	const RESERVATION_HUB           = 31689;
	const ADVANCED_BUDGETING        = 28841;

	// This is for temp use.
	const ENTRATA_CLOUD_DATABASE = 4599;
	const STR_ALL_PAYMENT_SOURCE_CONSTANT = 'ALL_SOURCE';

	const ENTRATA_HELP_DESK = 31683;

	const AGENT_ACCESS = 31691;

	const ENTRATA_DATA_SERVICES = 31692;

	const ENTRATA_MOBILE_MAINTENANCE = 31693;

	const ENTRATAFI = 31698;

	const RESIDENT_MAINTENANCE = 1891;
	const RESIDENT_SYNC = 3325;
	const RESIDENT_WORKS = 3321;
	const VENDOR_ACCESS_ENTRATA_ACCOUNTING = 17292;
	const TRANSMISSIONS = 63;
	const ADMIN_SYNC = 1889;
	const COMMON = 3322;

	public static $c_arrintEntrataCoreARProductIds = [
		self::BILL_PAY,
		self::DATA_FIX,
		self::ENTRATA_AFFORDABLE,
		self::ENTRATA_COMMERCIAL,
		self::ENTRATA,
		self::ENTRATA_MILITARY,
		self::ENTRATA_TABLET
	];

	public static $c_arrintEntrataCoreAPProductIds = [
		self::INVOICE_PROCESSING,
		self::VENDOR_ACCESS,
		self::JOB_COSTING,
		self::VENDOR_ACCESS_ENTRATA_ACCOUNTING,
		self::ENTRATAMATION,
		self::ENTRATA_INSIGHTS
	];

	public static $c_arrintMarketingAndPricingProductIds = [
		self::API_SERVICES,
		self::BLOG,
		self::CALL_ANALYSIS,
		self::CALL_TRACKER,
		self::CRAIGSLIST_POSTING,
		self::CUSTOM_DEVELOPMENT,
		self::DOMAIN_RENEWAL,
		self::PRICING,
		self::FACEBOOK_INTEGRATION,
		self::GUEST_CARD_PARSING,
		self::ILS_PORTAL,
		self::LEASING_CENTER,
		self::LOBBY_DISPLAY,
		self::MARKETING_TEMPLATE,
		self::MEDIA_LIBRARY,
		self::PRINT_BROCHURE,
		self::PROSPECT_PORTAL,
		self::REPUTATION_ADVISOR,
		self::SEO_SERVICES,
		self::SITE_TABLET,
		self::SNIPPET,
		self::SYSTEM,
		self::TRANSMISSIONS,
		self::VOIP
	];

	public static $c_arrintLeasingProductIds = [
		self::ADMIN_SERVICES,
		self::ADMIN_SYNC,
		self::CHAT,
		self::CLIENT_ADMIN,
		self::COMMON,
		self::DOCUMENT_MANAGEMENT,
		self::ENTRATA_PAAS,
		self::GMAIL_EMAIL,
		self::LEAD_ALERT,
		self::CALL_LEAD_COMMUNICATION,
		self::LEAD_MANAGEMENT,
		self::LEASE_EDITOR,
		self::LEASE_EXECUTION,
		self::MASTER_POLICY,
		self::PMS_INTEGRATION,
		self::SCREENING
	];

	public static $c_arrintResidentsProductIds = [
		self::CHECK_SCANNING,
		self::CORPORATE_PORTAL,
		self::EMPLOYEE_PORTAL,
		self::ENTRATA_FACILITIES,
		self::ENTRATA_FACILITIES_APP,
		self::INSPECTION_MANAGER,
		self::MERCHANT_ENROLLMENT,
		self::MESSAGE_CENTER,
		self::MISCALLANEOUS,
		self::PARCEL_ALERT,
		self::RESIDENT_INSURE,
		self::RESIDENT_MAINTENANCE,
		self::RESIDENT_PAY,
		self::RESIDENT_PAY_REVERSALS,
		self::RESIDENT_SUPPORT_PREMIUM,
		self::RESIDENT_UTILITY,
		self::RESIDENT_PORTAL,
		self::RESIDENT_SYNC,
		self::RESIDENT_WORKS,
		self::UTILITY_EXPENSE_MANAGEMENT
	];

	const ENTRATA_FLEXIBLE = 31697;

	public static $c_arrintSeoProductIds = [
		CPsProduct::SEO_SERVICES,
		CPsProduct::PPC_SERVICES,
		CPsProduct::REPUTATION_ADVISOR
	];

	public static $c_arrintLeadRelatedProducts	= [
		CPsProduct::CALL_TRACKER,
		CPsProduct::CRAIGSLIST_POSTING,
		CPsProduct::GUEST_CARD_PARSING,
		CPsProduct::LEASING_CENTER,
		CPsProduct::ILS_PORTAL,
		CPsProduct::MESSAGE_CENTER,
		CPsProduct::MOBILE_PORTAL,
		CPsProduct::PMS_INTEGRATION,
		CPsProduct::PROSPECT_PORTAL,
		CPsProduct::ENTRATA_PAAS,
		CPsProduct::FACEBOOK_INTEGRATION,
		CPsProduct::SITE_TABLET,
		CPsProduct::VACANCY,
		CPsProduct::LEAD_MANAGEMENT,
		CPsProduct::API_SERVICES,
		CPsProduct::RESIDENT_INSURE,
		CPsProduct::FACEBOOK_MARKETPLACE,
		CPsProduct::MARKETING_HUB
	];

	public static $c_arrintOnlinePsProductIds = [
		self::GUEST_CARD_PARSING,
		self::VACANCY,
		self::CRAIGSLIST_POSTING,
		self::FACEBOOK_INTEGRATION,
		self::ILS_PORTAL,
		self::PROSPECT_PORTAL
	];

	public static $c_arrintMobilePsProductIds = [
		self::SITE_TABLET,
		self::ENTRATAMATION
	];

	public static $c_arrintReleaseNotesForNonSaleableProducts = [
		CPsProduct::API_SERVICES,
		CPsProduct::BLOG,
		CPsProduct::ENTRATA_PAAS,
		CPsProduct::MAINTENANCE,
		CPsProduct::MARKETING_TEMPLATE
	];

	public static $c_arrintResidentPortalPsProducts = [
		CPsProduct::RESIDENT_PAY,
		CPsProduct::RESIDENT_PORTAL,
		CPsProduct::RESIDENT_UTILITY,
		CPsProduct::MOBILE_PORTAL,
		CPsProduct::MESSAGE_CENTER,
		CPsProduct::RESIDENT_INSURE,
		CPsProduct::FACEBOOK_INTEGRATION,
		CPsProduct::ENTRATA,
		CPsProduct::LEASE_EXECUTION,
		CPSProduct::INSPECTION_MANAGER,
		CPsProduct::PARCEL_ALERT,
		CPsProduct::RESIDENT_SUPPORT_PREMIUM,
	    CPsProduct::PROSPECT_PORTAL,
		CPsProduct::ENTRATAFI
	];

	public static $c_arrintNonSelfProvisionablePsProducts = [
		CPsProduct::RESIDENT_PAY,
		CPsProduct::RESIDENT_PAY_DESKTOP,
		CPsProduct::LEASING_CENTER,
		CPsProduct::SEM_SERVICES,
		CPsProduct::RESIDENT_VERIFY,
		CPsProduct::RESIDENT_UTILITY,
		CPsProduct::RESIDENT_INSURE,
		CPsProduct::INVOICE_PROCESSING,
		CPsProduct::MISCALLANEOUS,
		CPsProduct::CUSTOM_DEVELOPMENT,
		CPsProduct::DOMAIN_RENEWAL,
		CPsProduct::GMAIL_EMAIL
	];

	public static $c_arrintSystemImplementedPsProducts	= [
		CPsProduct::DOMAIN_RENEWAL,
		CPsProduct::CUSTOM_DEVELOPMENT,
		CPsProduct::GMAIL_EMAIL
	];

	public static $c_arrintNonSelfImplementedPsProducts = [
		CPsProduct::RESIDENT_VERIFY,
		CPsProduct::RESIDENT_UTILITY,
		CPsProduct::RESIDENT_INSURE,
		CPsProduct::LEASING_CENTER,
		CPsProduct::SEO_SERVICES,
		CPsProduct::ENTRATA,
		CPsProduct::LEASE_EXECUTION,
		CPSProduct::INSPECTION_MANAGER,
		CPSProduct::PRICING,
		CPSProduct::REPUTATION_ADVISOR,
		CPSProduct::RESIDENT_SUPPORT_PREMIUM,
		CPSProduct::VENDOR_ACCESS,
		CPSProduct::UTILITY_EXPENSE_MANAGEMENT,
		CPSProduct::INVOICE_PROCESSING,
		CPSProduct::SEM_SERVICES
	];

	public static $c_arrintContactPointsPermissionedPsProductIds = [
		CPsProduct::CALL_TRACKER,
		CPsProduct::CRAIGSLIST_POSTING,
		CPsProduct::ENTRATA,
		CPsProduct::FACEBOOK_INTEGRATION,
		CPsProduct::ILS_PORTAL,
		CPsProduct::LEAD_MANAGEMENT,
		CPsProduct::LEASING_CENTER,
		CPsProduct::MESSAGE_CENTER,
		CPsProduct::PROSPECT_PORTAL,
		CPsProduct::SITE_TABLET,
		CPsProduct::RESIDENT_INSURE,
		CPsProduct::DOCUMENT_MANAGEMENT,
		CPsProduct::ENTRATA_COMMERCIAL,
		CPsProduct::PARCEL_ALERT
	];

	public static $c_arrintSqlTimeoutProductIds = [
		CPsProduct::ENTRATA,
		CPsProduct::PROSPECT_PORTAL,
		CPsProduct::CLIENT_ADMIN
	];

	public static $c_arrintLegalEntityProductIds = [
		CPsProduct::ENTRATA,
		CPsProduct::RESIDENT_VERIFY,
		CPsProduct::RESIDENT_INSURE
	];

	public static $c_strContactPointsPsProductNames = [
		CPsProduct::ENTRATA => 'Entrata',
		CPsProduct::SITE_TABLET => 'Site Tablet',
		CPsProduct::LEASING_CENTER => 'Leasing Center',
		CPsProduct::PROSPECT_PORTAL => 'Prospect Portal',
		CPsProduct::ILS_PORTAL => 'ILS portal',
		CPsProduct::CRAIGSLIST_POSTING => 'Craigslist Posting',
		CPsProduct::API_SERVICES => 'API',
		CPsProduct::CALL_TRACKER => 'Call Tracker'
	];

	public static $c_arrintDefaultProductsForRegions = [

		self::ENTRATA		            => 'Entrata Core',
		self::RESIDENT_PAY              => 'Resident Pay',
		//	self::ENTRATA                   => 'Entrata',
		self::RESIDENT_PORTAL			=> 'Resident Portal',
		self::PROSPECT_PORTAL			=> 'Prospect Portal',
		self::PARCEL_ALERT				=> 'Parcel Alert',
		self::LEASE_EXECUTION			=> 'Lease Execution',
		self::LEAD_MANAGEMENT			=> 'Lead Management',
		self::DOCUMENT_MANAGEMENT       => 'Document Management',
		self::CALL_TRACKER				=> 'Call Tracking',
		self::MESSAGE_CENTER			=> 'Message Center',
		self::LEASING_CENTER			=> 'Leasing Center',
		self::ENTRATA_COMMERCIAL        => 'Entrata Commercial',
		self::CHAT                      => 'Chat',
		self::RESERVATION_HUB           => 'ReservationHub',
		//	self::TENANT_PORTAL             => 'Tenant Portal',
		//	self::AGENT_ACCESS_GLOBAL       => 'Agentaccessglobal'
		self::AGENT_ACCESS              => 'Agent Access'
	];

	public static $c_arrintRuUemProductIds = [
		CPsProduct::RESIDENT_UTILITY,
		CPsProduct::UTILITY_EXPENSE_MANAGEMENT
	];

	public static $c_arrintRuUemShortProductNames = [
		self::RESIDENT_UTILITY           => 'RU',
		self::UTILITY_EXPENSE_MANAGEMENT => 'UEM'
	];

	public static $c_arrintRuUemIpProductIds = [
		CPsProduct::RESIDENT_UTILITY,
		CPsProduct::UTILITY_EXPENSE_MANAGEMENT,
		CPsProduct::INVOICE_PROCESSING
	];

	public static $c_arrintCorporateEntityProductIds = [
		self::ENTRATA,
		self::ADVANCED_BUDGETING,
		self::JOB_COSTING,
		self::INVOICE_PROCESSING,
		self::VENDOR_ACCESS,
		self::ENTRATA_ACCOUNTING,
	];

	public static $c_arrintBillingRequestIgnoredPsProductIds = [
		CPsProduct::CONSULTING_SERVICES,
		CPsProduct::HISTORICAL_ACCESS,
		CPsProduct::OWNER_PORTAL,
		CPsProduct::EMPLOYEE_PORTAL,
		CPsProduct::DOCUMENT_MANAGEMENT,
		CPsProduct::VENDOR_ACCESS,
		CPsProduct::BILL_PAY,
		CPsProduct::DOMAIN_RENEWAL,
		CPsProduct::CUSTOM_DEVELOPMENT,
		CPsProduct::MISCALLANEOUS,
		CPsProduct::RESIDENT_INSURE,
		CPsProduct::RESIDENT_VERIFY,
		CPsProduct::MASTER_POLICY
	];

	public static $c_arrintBillingRequestExcludeTransactionalPsProductIds = [
		self::RESIDENT_UTILITY,
		self::UTILITY_EXPENSE_MANAGEMENT,
		self::INVOICE_PROCESSING
	];

	public static $c_arrintCorporatePsProductIds = [
		self::ADVANCED_BUDGETING,
		self::JOB_COSTING,
		self::INVOICE_PROCESSING,
		self::VENDOR_ACCESS,
		self::ENTRATA_ACCOUNTING,
	];

	/**
	 * Add Functions
	 */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getProductName( $intProductId ) {
		$this->loadProductNames();
		return $this->m_arrstrProductNames[$intProductId] ?? '';
	}

	public function getProductNames() {
		$this->loadProductNames();

		return $this->m_arrstrProductNames;
	}

	private function loadProductNames() {
		if( false == empty( $this->m_arrstrProductNames ) ) {
			return;
		}

		$this->m_arrstrProductNames = [
			self::ENTRATA					=> 'Entrata Core',
			self::PROSPECT_PORTAL			=> 'Prospect Portal',
			self::RESIDENT_PORTAL			=> 'Resident Portal',
			self::RESIDENT_PAY				=> 'Resident Pay',
			self::CLIENT_ADMIN				=> 'Client Admin',
			self::LOBBY_DISPLAY				=> 'Lobby Display',
			self::API_SERVICES				=> 'API Services',
			self::ENTRATA_COM				=> 'Entrata.com',
			self::GMAIL_EMAIL				=> 'GMail Email',
			self::VACANCY					=> 'Vacancy.com',
			self::RESIDENT_PAY_DESKTOP		=> 'Resident Pay Desktop',
			self::PMS_INTEGRATION			=> 'Integration',
			self::ILS_PORTAL				=> 'ILS Portal',
			self::GUEST_CARD_PARSING		=> 'Guest Card Parsing',
			self::BLOG						=> 'Blog',
			self::SEO_SERVICES				=> 'SEO Services',
			self::RESIDENT_PAY_REVERSALS	=> 'ResidentPay Reversals',
			self::LEASE_EXECUTION			=> 'Lease Execution',
			self::MESSAGE_CENTER			=> 'Message Center',
			self::MOBILE_PORTAL				=> 'Mobile Portal',
			self::CRAIGSLIST_POSTING		=> 'CraigsList Posting',
			self::SITE_TABLET				=> 'Site Tablet',
			self::CALL_TRACKER				=> 'Call Tracking',
			self::RESIDENT_UTILITY			=> 'Resident Utility',
			self::RESIDENT_INSURE			=> 'Resident Insure',
			self::CUSTOM_DEVELOPMENT		=> 'Custom Development',
			self::LEAD_MANAGEMENT			=> 'Lead Management',
			self::PRINT_BROCHURE			=> 'Print Brochure',
			self::LEASING_CENTER			=> 'Leasing Center',
			self::PARCEL_ALERT				=> 'Parcel Alert',
			self::CALL_ANALYSIS				=> 'CallAnalysis',
			self::CALL_LEAD_COMMUNICATION	=> 'Lead Communication',
			self::SEM_SERVICES				=> 'SEM Services',
			self::SEO_DASHBOARD				=> 'SEO Dashboard',
			self::SCREENING					=> 'Screening',
			self::PRICING					=> 'Pricing',
			self::RESIDENT_VERIFY			=> 'Resident verify',
			self::MARKETING_TEMPLATE		=> 'Marketing Template',
			self::LEAD_ALERT				=> 'Lead Alert',
			self::INSPECTION_MANAGER		=> 'Inspection Manager',
			self::FACEBOOK_INTEGRATION		=> 'Facebook Integration',
			self::ENTRATA_PAAS				=> 'Entrata PaaS',
			self::OWNER_PORTAL				=> 'Owner Portal',
			self::JOBS						=> 'Jobs',
			self::VENDOR_ACCESS				=> 'Vendor Access',
			self::INVOICE_PROCESSING		=> 'Invoice Processing',
			self::PS_ONLINE_APPLICATION		=> 'Online Application',
			// self::CHECK_SCANNING			=> 'Check Scanning',
			self::SNIPPET 					=> 'Snippet',
			self::BILL_PAY					=> 'Bill Pay',
			self::ENTRATA_MILITARY			=> 'Entrata Military',
			self::ENTRATA_AFFORDABLE		=> 'Entrata Affordable',
			self::ENTRATA_FACILITIES_APP	=> 'Entrata Facilities App',
			self::ENTRATAMATION             => 'Entratamation',
			self::ENTRATA_MOBILE_MAINTENANCE => 'Mobile Maintenance',
			self::FACEBOOK_MARKETPLACE       => 'Facebook Marketplace',
			self::AGENT_ACCESS              => 'Agent Access',
			self::JOB_COSTING                => 'Job Costing',
			self::RESERVATION_HUB           => 'Reservation Hub',
			self::MARKETING_HUB				=> 'Marketing Strategy Hub'
		];
	}

	public function addTask( $objTask ) {
		$this->m_arrobjTasks[$objTask->getId()] = $objTask;
	}

	public function addChargeCode( $objChargeCode ) {
		$this->m_arrobjChargeCodes[$objChargeCode->getId()] = $objChargeCode;
	}

	public function addPsProductOption( $objPsProductOption ) {
		$this->m_arrobjPsProductOptions[$objPsProductOption->getId()] = $objPsProductOption;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_selected'] ) ) {
			$this->setIsSelected( $arrmixValues['is_selected'] );
		}
		if( true == isset( $arrmixValues['is_associated'] ) ) {
			$this->setIsAssociated( $arrmixValues['is_associated'] );
		}
		if( true == isset( $arrmixValues['set_up_amount'] ) ) {
			$this->setSetUpAmount( $arrmixValues['set_up_amount'] );
		}
		if( true == isset( $arrmixValues['monthly_recurring_amount'] ) ) {
			$this->setMonthlyRecurringAmount( $arrmixValues['monthly_recurring_amount'] );
		}

		if( true == isset( $arrmixValues['product_bundle_id'] ) ) {
			$this->setProductBundleId( $arrmixValues['product_bundle_id'] );
		}

		if( true == isset( $arrmixValues['is_bundle'] ) ) {
			$this->setIsBundle( $arrmixValues['is_bundle'] );
		}

		return;
	}

	public function setSeoHandle( $strSeoHandle ) {
		$this->m_strSeoHandle = $strSeoHandle;
	}

	public function setProductBundleId( $intProductBundleId ) {
		$this->m_intProductBundleId = $intProductBundleId;
	}

	public function setTasks( $arrobjTasks ) {
		$this->m_arrobjTasks = $arrobjTasks;
	}

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function setPsProductCompetitors( $arrobjPsProductCompetitors ) {
		$this->m_arrobjPsProductCompetitors = $arrobjPsProductCompetitors;
	}

	public function setPsProductOptions( $arrobjPsProductOptions ) {
		$this->m_arrobjPsProductOptions = $arrobjPsProductOptions;
	}

	public function setIsAssociated( $intIsAssociated ) {
		$this->m_intIsAssociated = $intIsAssociated;
	}

	public function setIsBundle( $intIsBundle ) {
		$this->m_intIsBundle = $intIsBundle;
	}

	/**
	 * Get Functions
	 */

	public function getPsProductOptions() {
		return $this->m_arrobjPsProductOptions;
	}

	public function getPsProductModules() {
		return $this->m_arrobjPsProductModules;
	}

	public function getPsProductVersions() {
		return $this->m_arrobjPsProductVersions;
	}

	public function getPsWebsiteProductPages() {
		return $this->m_arrobjPsWebsiteProductPages;
	}

	public function getSeoHandle() {
		return $this->m_strSeoHandle;
	}

	public function getProductBundleId() {
		return $this->m_intProductBundleId;
	}

	public function getTasks() {
		return $this->m_arrobjTasks;
	}

	public function getPsProductCompetitors() {
		return $this->m_arrobjPsProductCompetitors;
	}

	public function getChargeCodes() {
		return $this->m_arrobjChargeCodes;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

	public function getCastPropertyMonthlyRecurring() {
		return ( true == is_null( $this->m_fltPropertyMonthlyRecurring ) ) ? 0 : $this->m_fltPropertyMonthlyRecurring;
	}

	public function getCastUnitMonthlyRecurring() {
		return ( true == is_null( $this->m_fltUnitMonthlyRecurring ) ) ? 0 : $this->m_fltUnitMonthlyRecurring;
	}

	public function getIsAssociated() {
		return $this->m_intIsAssociated;
	}

	public function getIsBundle() {
		return $this->m_intIsBundle;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPsProductModules( $objDatabase ) {
		return CPsProductModules::createService()->fetchPsProductModulesByPsProductId( $this->m_intId, $objDatabase );
	}

	public function fetchPsProductCompetitors( $objAdminDatabase ) {
		return CPsProductCompetitors::createService()->fetchPsProductCompetitorsByPsProductsIds( [ $this->m_intId ], $objAdminDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function valName( $objDatabase = NULL, $boolValidateDuplicateName = true, $boolAllowRoundBrackets = false ) {
		$boolIsValid = true;

		$strMatchString = '/^[.a-zA-Z0-9&\s]+$/';
		if( true == $boolAllowRoundBrackets ) {
			$strMatchString = '/^[-.a-zA-Z0-9()&\s]+$/';
		}

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} elseif( false == \Psi\CStringService::singleton()->preg_match( $strMatchString, $this->getName() ) ) {
			  $boolIsValid = false;
			  $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name should be in alphanumeric.' ) );
		}

		if( true == $boolValidateDuplicateName && true == $boolIsValid && true == valId( $this->getPsProductTypeId() ) ) {

			$strSql = ' WHERE name ilike \'' . trim( addslashes( $this->m_strName ) ) . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$strSql .= 'AND CASE
								WHEN ' . $this->getPsProductTypeId() . ' = ' . CPsProductType::CUSTOM_BUNDLE . '
								THEN ps_product_type_id = ' . $this->getPsProductTypeId() . '
									AND ps_lead_id = ' . ( int ) $this->getPsLeadId() . '
								ELSE ps_lead_id IS NULL
						END;';

			$intCount = CPsProducts::createService()->fetchRowCount( $strSql, 'ps_products', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' Product name already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valShortName() {
		$boolIsValid = true;

		if( true == is_null( $this->getShortName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'short_name', 'Short name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_type_id', 'Product type is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolValidateDuplicateName = true, $boolAllowRoundBrackets = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase, $boolValidateDuplicateName );
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valPsProductTypeId();
				$boolIsValid &= $this->valShortName();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_bundle':
				$boolIsValid &= $this->valName( $objDatabase, $boolValidateDuplicateName, $boolAllowRoundBrackets );
				$boolIsValid &= $this->valRecurringAmount();
				$boolIsValid &= $this->valPropertySetup();
				$boolIsValid &= $this->valMinMaxUnits();
				break;

			case 'validate_stand_alone_bundle':
				$boolIsValid &= $this->valName( $objDatabase, $boolValidateDuplicateName );
				$boolIsValid &= $this->valPropertySetup();
				$boolIsValid &= $this->valMinMaxUnits();
				$boolIsValid &= $this->checkDuplicateBundle( $objDatabase );
				break;

			default:
				// Default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valPsProductBundleName( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) || false == \Psi\CStringService::singleton()->preg_match( '/^[.a-zA-Z0-9&\s]+$/', $this->getName() ) ) {
			return false;
		}

		$strSql = ' WHERE
						name ilike \'' . trim( addslashes( $this->getName() ) ) . '\' 
						AND ps_product_type_id = ' . CPsProductType::CUSTOM_BUNDLE . '
						AND ps_lead_id = ' . ( int ) $this->getPsLeadId() . ';';

		$intCount = CPsProducts::createService()->fetchRowCount( $strSql, 'ps_products', $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Bundle name already exists.' ) );
		}

		return $boolIsValid;
	}

	public function checkDuplicatePsProductBundle( $arrintPsProductIds, $objDatabase ) {

		if( false == valArr( $arrintPsProductIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						count( bundle_ps_product_id ) as count,
						max( existing_bundle_name ) as existing_bundle_name
					FROM (
							SELECT
								DISTINCT ON( pr.bundle_ps_product_id )
								pr.bundle_ps_product_id,
								p.name as existing_bundle_name,
								SUM( CASE WHEN pr.ps_product_id IN( ' . implode( ',', $arrintPsProductIds ) . ' ) THEN 1 ELSE 0 END ) OVER ( PARTITION BY pr.bundle_ps_product_id ) as matched_count,
								COUNT( pr.id ) OVER ( PARTITION BY pr.bundle_ps_product_id ) as total_ps_products
							FROM
								ps_product_relationships pr
								JOIN ps_products p ON pr.bundle_ps_product_id = p.id
							WHERE
								pr.ps_lead_id = ' . ( int ) $this->getPsLeadId() . '
						) as sub
					WHERE
						matched_count = total_ps_products
						AND total_ps_products=' . \Psi\Libraries\UtilFunctions\count( $arrintPsProductIds ) . ';';

		$arrmixValues	= fetchData( $strSql, $objDatabase );
		$intCount 		= ( true == isset( $arrmixValues[0] ) ) ? ( int ) $arrmixValues[0]['count'] : 0;

		if( 0 < $intCount ) {
			if( 1 < $intCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' <br /> Multiple bundles already exists with same products.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' <br /> Bundle "' . $arrmixValues[0]['existing_bundle_name'] . '" already exists with same products.' ) );
			}

			return false;
		}

		return true;
	}

	/**
	 * Other Functions
	 */

	public function addPsProductVersion( $objPsProductVersion ) {
		$this->m_arrobjPsProductVersions[$objPsProductVersion->getId()] = $objPsProductVersion;
	}

	public function addPsWebsitePage( $objPsWebsiteProductPage ) {
		$this->m_arrobjPsWebsiteProductPages[$objPsWebsiteProductPage->getId()] = $objPsWebsiteProductPage;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createPsProductOption() {
		$objPsProductOption = new CPsProductOption();
		$objPsProductOption->setPsProductId( $this->getId() );
		return $objPsProductOption;
	}

	public function assignModulePaths() {
		require_once( PATH_APPLICATION_ADMIN . 'Admin.defines.php' );
		require_once( PATH_APPLICATION_RESIDENTSYNC . 'ResidentSync.defines.php' );
		require_once( PATH_APPLICATION_PS_WEBSITE . 'PsWebsite.defines.php' );
		require_once( PATH_APPLICATION_VACANCY . 'Vacancy.defines.php' );
		require_once( PATH_APPLICATION_PARCEL_ALERT . 'ParcelAlert.defines.php' );
		$this->m_arrstrModulePath = [
			self::ENTRATA              => PATH_RESIDENT_WORKS_APPLICATION . 'AssignApplicationModules.php',
			self::PROSPECT_PORTAL      => PATH_PROSPECT_PORTAL_APPLICATION . 'AssignApplicationModules.php',
			self::RESIDENT_PORTAL      => PATH_RESIDENT_PORTAL_APPLICATION . 'AssignApplicationModules.php',
			self::RESIDENT_PAY         => NULL,
			self::CLIENT_ADMIN         => PATH_ADMIN_APPLICATION . 'AssignApplicationModules.php',
			self::LOBBY_DISPLAY        => NULL,
			self::API_SERVICES         => PATH_RESIDENTSYNC_APPLICATION . 'AssignApplicationModules.php',
			self::ENTRATA_COM          => PATH_PS_WEBSITE_APPLICATION . 'AssignApplicationModules.php',
			self::GMAIL_EMAIL          => NULL,
			self::VACANCY              => PATH_VACANCY_APPLICATION . 'AssignApplicationModules.php',
			self::RESIDENT_PAY_DESKTOP => NULL,
			self::PMS_INTEGRATION      => NULL,
			self::ILS_PORTAL           => PATH_ILS_PORTAL_APPLICATION . 'AssignApplicationModules.php',
			self::GUEST_CARD_PARSING   => NULL,
			self::BLOG                 => NULL,
			self::SEO_SERVICES         => NULL,
			self::LEASE_EXECUTION      => NULL,
			self::MESSAGE_CENTER       => NULL,
			self::CRAIGSLIST_POSTING		=> NULL,
			self::SITE_TABLET				=> NULL,
			self::CALL_TRACKER				=> NULL,
			self::FACEBOOK_INTEGRATION		=> NULL,
			self::RESIDENT_UTILITY			=> NULL,
			self::RESIDENT_INSURE			=> NULL,
			self::CUSTOM_DEVELOPMENT		=> NULL,
			self::LEAD_MANAGEMENT			=> NULL,
			self::PRINT_BROCHURE			=> NULL,
			self::LEASING_CENTER			=> NULL,
			self::PARCEL_ALERT				=> PATH_PARCEL_ALERT_APPLICATION . 'AssignApplicationModules.php',
			self::SEM_SERVICES				=> NULL
		];

		return $this->m_arrstrModulePath;
	}

	public function assignModuleUsers() {
		require_once( PATH_APPLICATION_ADMIN . 'Admin.defines.php' );

		$this->m_arrstrModuleUsers = [
			self::ENTRATA              => CUser::ID_RUJUTA_GADRE,
			self::PROSPECT_PORTAL      => CUser::ID_SANDEEP_CHAVAN,
			self::RESIDENT_PORTAL      => CUser::ID_YUSUF_POONAWALA,
			self::RESIDENT_PAY         => NULL,
			self::CLIENT_ADMIN         => CUser::ID_LOKESH_AGRAWAL,
			self::LOBBY_DISPLAY        => NULL,
			self::API_SERVICES         => CUser::ID_SANDEEP_CHAVAN,
			self::ENTRATA_COM          => CUser::ID_TAPAN_BHATT,
			self::GMAIL_EMAIL          => NULL,
			self::VACANCY              => CUser::ID_RAHUL_WAGHMARE,
			self::RESIDENT_PAY_DESKTOP => NULL,
			self::PMS_INTEGRATION      => NULL,
			self::ILS_PORTAL           => CUser::ID_NETAJI_WAKDE,
			self::GUEST_CARD_PARSING   => NULL,
			self::BLOG                 => NULL,
			self::SEO_SERVICES         => NULL,
			self::LEASE_EXECUTION      => NULL,
			self::MESSAGE_CENTER       => NULL,
			self::CRAIGSLIST_POSTING   => NULL,
			self::SITE_TABLET          => NULL,
			self::CALL_TRACKER				=> NULL,
			self::FACEBOOK_INTEGRATION		=> NULL,
			self::RESIDENT_UTILITY			=> NULL,
			self::RESIDENT_INSURE			=> NULL,
			self::CUSTOM_DEVELOPMENT		=> NULL,
			self::LEAD_MANAGEMENT			=> NULL,
			self::PRINT_BROCHURE			=> NULL,
			self::LEASING_CENTER       => NULL,
			self::PARCEL_ALERT         => CUser::ID_SACHINKUMAR_DURGE,
			self::SEM_SERVICES         => NULL
		];

		return $this->m_arrstrModuleUsers;
	}

	public static function loadPsProductNameById( $intPsProductId ) {
		$arrstrPsProductNames = CPsProduct::createService()->getProductNames();
		$strName = ( true == array_key_exists( $intPsProductId, $arrstrPsProductNames ) ) ? $arrstrPsProductNames[$intPsProductId] : 'Unnamed';
		return $strName;
	}

	public static function loadPsProductsByIds( $arrintPsProductIds ) {
		if( false == valArr( $arrintPsProductIds ) ) {
			return [];
		}

		$arrstrPsProductNames = CPsProduct::createService()->getProductNames();
		$arrobjPsProducts = [];

		foreach( $arrintPsProductIds as $intPsProductId ) {

			$objPsProduct = new CPsProduct();
			$objPsProduct->setId( $intPsProductId );

			$strName = ( true == array_key_exists( $intPsProductId, $arrstrPsProductNames ) ) ? $arrstrPsProductNames[$intPsProductId] : 'Unnamed';

			$objPsProduct->setName( $strName );
			$arrobjPsProducts[$strName] = $objPsProduct;
		}

		ksort( $arrobjPsProducts );

		return rekeyObjects( 'Id', $arrobjPsProducts );
	}

	public static function hasPortals( $arrintPsProductIds ) {
		$arrintAllowedPsProductIds = [ CPsProduct::ILS_PORTAL, CPsProduct::SITE_TABLET, CPsProduct::RESIDENT_PORTAL, CPsProduct::PROSPECT_PORTAL, CPsProduct::ENTRATA, CPsProduct::MOBILE_PORTAL, CPsProduct::LEAD_MANAGEMENT, CPsProduct::LEASE_EXECUTION ];

		if( true == valArr( $arrintPsProductIds ) && 0 < \Psi\Libraries\UtilFunctions\count( array_intersect( array_keys( $arrintPsProductIds ), $arrintAllowedPsProductIds ) ) ) {
			return true;
		}

		return false;
	}

	public static function hasPay( $arrintPsProductIds ) {
		$arrintAllowedPsProductIds = [ CPsProduct::RESIDENT_PAY, CPsProduct::RESIDENT_PAY_DESKTOP ];

		if( true == valArr( $arrintPsProductIds ) && 0 < \Psi\Libraries\UtilFunctions\count( array_intersect( array_keys( $arrintPsProductIds ), $arrintAllowedPsProductIds ) ) ) {
			return true;
		}

		return false;
	}

	public function validatePsProductPricingDiscounts( $strSetupDiscountType, $strMonthlyDiscountType ) {

		// @TODO: Remove below line once we've to make Sales Opportunity Redesign project live.
		return true;

		$boolIsValid = true;

		if( $strSetupDiscountType && $strMonthlyDiscountType ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Monthly and Setup discounts are required and should be nonzero.' ) );
		} elseif( $strMonthlyDiscountType ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Monthly discounts are required and should be nonzero.' ) );
		} elseif( $strSetupDiscountType ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Setup discounts are required and should be nonzero.' ) );
		}
		return $boolIsValid;
	}

}
?>