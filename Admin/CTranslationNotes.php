<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTranslationNotes
 * Do not add any new functions to this class.
 */

class CTranslationNotes extends CBaseTranslationNotes {

	public static function fetchUnbatchedTranslationNotesByLocaleCode( $strLocale, $objDatabase ) {
		$strSql = sprintf( 'SELECT
									    tn.*,
									    tk.key as translation_key,
									    t.value as translation_value
									FROM
									    translation_notes tn
									    JOIN translation_keys tk ON tn.translation_key_id = tk.id
										JOIN translations t ON tk.id = t.translation_key_id AND tn.locale_code = t.locale_code
										WHERE
											( tk.details IS NULL OR  tk.details ? %s = FALSE )
											AND tn.locale_code = %s
											AND tn.details#>>%s IS NULL',
							pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ),
							pg_escape_literal( $objDatabase->getHandle(), $strLocale ),
							pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,sent_on}' ) );

		return self::fetchTranslationNotes( $strSql, $objDatabase );
	}

	public static function fetchPendingTranslationBatchesByVendor( $strVendorName, $objDatabase ) {
		$strSql = sprintf( '
			SELECT DISTINCT 
				tn.details#>>%s AS vendor_batch_id,
			    tn.details#>>%s AS interchange_format,
				tn.locale_code
			FROM
				translation_notes tn
                JOIN translation_keys tk ON ( tn.translation_key_id = tk.id )
			WHERE
				( tk.details IS NULL OR tk.details ? %s = FALSE )
				AND tn.details#>>%s = %s
				AND tn.details->%s ? %s
				AND tn.details->%s ? %s IS FALSE
				AND tn.locale_code != %s
				AND tk.deleted_by IS NULL
				AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_batch_id}' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,interchange_format}' ),
			pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ),
			pg_escape_literal( $objDatabase->getHandle(), 'vendor_batch_id' ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ),
			pg_escape_literal( $objDatabase->getHandle(), 'received_on' ),
			pg_escape_literal( $objDatabase->getHandle(), CLocale::ENGLISH_UNITED_STATES ) );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPendingFeedbackTranslationsByVendor( $strVendorName, $objDatabase ) {
		$strSql = sprintf( '
			SELECT
				tn.*
			FROM
				translation_notes tn
			WHERE
				tn.details#>>%s = %s
				AND tn.details->%s ? %s
				AND tn.details->%s ? %s IS FALSE
				AND tn.locale_code != %s',
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ),
			pg_escape_literal( $objDatabase->getHandle(), 'vendor_batch_id' ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ),
			pg_escape_literal( $objDatabase->getHandle(), 'received_on' ),
			pg_escape_literal( $objDatabase->getHandle(), CLocale::ENGLISH_UNITED_STATES ) );

		return self::fetchTranslationNotes( $strSql, $objDatabase );
	}

	public function fetchTranslationNotesByTranslationKeyIdByLocaleCode( $intTranslationKeyId, $strLocale, $objDatabase ) {
		$strSql = sprintf( ' SELECT
										e.name_full as employee_name,
										tn.*
									FROM
									    translation_notes tn
									    JOIN users u ON tn.created_by = u.id
									    JOIN employees e ON u.employee_id = e.id
									WHERE
										tn.translation_key_id = %s
										AND tn.locale_code = %s
									ORDER BY
										tn.id DESC',
			pg_escape_literal( $objDatabase->getHandle(), $intTranslationKeyId ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocale ) );

		return self::fetchTranslationNotes( $strSql, $objDatabase );
	}

	public static function fetchLatestTranslationNoteByTranslationKeyIdByLocaleCode( $intTranslationKeyId, $strLocale, $objDatabase ) {
		$strSql = sprintf( ' SELECT
										tn.*
									FROM
									    translation_notes tn
									WHERE
										tn.translation_key_id = %s
										AND tn.locale_code = %s
									ORDER BY
										tn.id DESC 
									LIMIT 1',
			pg_escape_literal( $objDatabase->getHandle(), $intTranslationKeyId ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocale ) );

		return self::fetchTranslationNote( $strSql, $objDatabase );
	}

	public static function fetchLatestTranslationNoteByTranslationKeyIdsByLocaleCode( $arrintTranslationKeyIds, $strLocale, $objDatabase ) {
		$strSql = sprintf( '
			SELECT
				DISTINCT ON (tn.translation_key_id, tn.locale_code)
			    tn.*
			FROM
			    translation_notes tn
			WHERE
			    tn.translation_key_id IN (' . implode( ', ', $arrintTranslationKeyIds ) . ')
			    AND tn.locale_code = %s
			ORDER BY
			    tn.translation_key_id, tn.locale_code, tn.id DESC',
		pg_escape_literal( $objDatabase->getHandle(), $strLocale ) );

		return parent::fetchTranslationNotes( $strSql, $objDatabase );
	}

}
?>