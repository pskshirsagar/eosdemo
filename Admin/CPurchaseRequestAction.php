<?php

class CPurchaseRequestAction extends CBasePurchaseRequestAction {

	const PROMOTE	= 'Promote';
	const SPLIT		= 'Split';
	const MERGE		= 'Merge';
	const HOLD		= 'Hold';
	const FILL		= 'Fill';
	const REMOVE	= 'Remove';
	const REOPEN	= 'Reopen';

	public function valPurchaseRequestId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPurchaseRequestId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_request_id', 'Purchase Request Id is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valActionType() {
		$boolIsValid = true;

		if( false == in_array( $this->getActionType(), array_merge( [ 'Promote', 'Split', 'Merge', 'Hold', 'Fill', 'Remove' ], CPurchaseRequestActionType::$c_arrstrIndianPurchaseRequestActionTypes ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'action_type', 'Action type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPurchaseRequestId();
				$boolIsValid &= $this->valActionType();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>