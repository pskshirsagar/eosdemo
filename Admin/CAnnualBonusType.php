<?php

class CAnnualBonusType extends CBaseAnnualBonusType {

	const ANNUAL		= '1';
	const SEMIANNUAL	= '2';
	const QUARTERLY		= '3';
	const MONTHLY		= '4';

	const BONUS_TYPE_ANNUAL 	= 'Annual';
	const BONUS_TYPE_SEMIANNUAL = 'Semi Annual';
	const BONUS_TYPE_QUARTERLY 	= 'Quarterly';
	const BONUS_TYPE_MONTHLY 	= 'Monthly';

	public static $c_arrstrAnnualBonusType = array(
		'1' => self::BONUS_TYPE_ANNUAL,
		'2' => self::BONUS_TYPE_SEMIANNUAL,
		'3' => self::BONUS_TYPE_QUARTERLY,
		'4' => self::BONUS_TYPE_MONTHLY
	);

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
		}

		return $boolIsValid;
	}

}
?>