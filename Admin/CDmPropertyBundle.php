<?php

class CDmPropertyBundle extends CBaseDmPropertyBundle {

	const ALL			= 0;
	const PPC_DMC		= 1;
	const ONLY_PPC		= 2;
	const ONLY_DMC		= 3;
	const OPPORTUNITIES	= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>