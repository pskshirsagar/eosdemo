<?php

class CTestSubmissionAnswer extends CBaseTestSubmissionAnswer {

	protected $m_strTempFileName;
	protected $m_strFileName;

	/**
	 * Get Functions
	 *
	 */

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valTestQuestionId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_intTestQuestionId ) || 0 == $this->m_intTestQuestionId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_question_id', 'Question is compulsary.' ) );
		}

		if( true == $boolIsValid && false == is_null( $objDatabase ) ) {
			$strSql = ' WHERE id=\'' . $this->m_intTestQuestionId . '\'';
			$intCount = CTestQuestions::fetchRowCount( $strSql, 'test_questions', $objDatabase );

			if( 0 == $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_question_id', 'No such Question exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valTestAnswerOptionId( $intTestQuestionTypeId, $intIsOptional ) {
		$boolIsValid = true;

		if( CTestQuestionType::MULTIPLE_CHOICE == $intTestQuestionTypeId || CTestQuestionType::MULTISELECT_ANSWERS == $intTestQuestionTypeId ) {
			if( 1 != $intIsOptional ) {
				if( true == is_null( $this->getTestAnswerOptionId() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_answer_option_id', 'Select an option.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valSubmissionAnswer( $intTestQuestionTypeId, $intIsOptional ) {
		$boolIsValid = true;

		if( CTestQuestionType::OPEN_ANSWER == $intTestQuestionTypeId || CTestQuestionType::TRUE_OR_FALSE == $intTestQuestionTypeId ) {
			if( 1 != $intIsOptional ) {
				if( true == is_null( $this->getSubmissionAnswer() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'submission_answer', 'Question is compulsary.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valTestSubmissionId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTestSubmissionId() ) || false == is_numeric( $this->getTestSubmissionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'submission_answer', 'Failed to load test submission details.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intTestQuestionTypeId = NULL, $intIsOptional = NULL, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTestQuestionId( $objDatabase );
				$boolIsValid &= $this->valTestSubmissionId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchTestQuestion( $objDatabase ) {
		return CTestQuestions::fetchTestQuestionById( $this->getTestQuestionId(), $objDatabase );
	}

	/**
	 * Other Functions
	 *
	 */

	public function buildAttachFileData( $strInputTypeFileName, $arrstrExtensions, $strPath, $objDatabase ) {

		if( false == valArr( $arrstrExtensions ) ) $arrstrExtensions = array( 'jpg', 'jpeg' );

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) $this->setId( $this->fetchNextId( $objDatabase ) );

		if( false == isset( $_FILES[$strInputTypeFileName]['name'] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Allowed file extensions are: ' . implode( ', ', $arrstrExtensions ) . '.' ) );
			return false;
		}

		$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );

		$strExtension = ( isset( $arrstrFileInfo['extension'] ) ) ? \Psi\CStringService::singleton()->strtolower( $arrstrFileInfo['extension'] ) : NULL;

		if( false == is_null( $strExtension ) && false == in_array( $strExtension, $arrstrExtensions ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Allowed file extensions are: ' . implode( ', ', $arrstrExtensions ) . '.' ) );
			return false;
		}

		$strFileName = $this->getId() . '_' . date( 'Ymdhis' ) . '.' . $strExtension;

		$strMimeType = '';
		$objFileExtension = \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Please select a file to upload.' ) );
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) $strMimeType = $objFileExtension->getMimeType();

		$this->setFilePath( $strPath );

		return true;
	}

	public function uploadFileDocument() {

		require_once( PATH_LIBRARIES_PSI . 'CFileIo.class.php' );

		$boolIsValid = true;

		$strUploadPath 	= PATH_MOUNTS . $this->getFilePath();

		if( false == is_dir( $strUploadPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Failed to create directory path.' ) );
				$boolIsValid = false;
			}
		}

		if( false == move_uploaded_file( $this->getTempFileName(), $strUploadPath . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Please select a file to upload.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>