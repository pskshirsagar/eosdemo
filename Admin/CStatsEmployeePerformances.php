<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsEmployeePerformances
 * Do not add any new functions to this class.
 */

class CStatsEmployeePerformances extends CBaseStatsEmployeePerformances {

	public static function fetchStatsEmployeePerformances( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CStatsEmployeePerformance', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchStatsEmployeePerformance( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CStatsEmployeePerformance', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>