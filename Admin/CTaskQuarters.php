<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskQuarters
 * Do not add any new functions to this class.
 */

class CTaskQuarters extends CBaseTaskQuarters {

	public static function fetchAllTaskQuarters( $objDatabase ) {
		$strSql = 'SELECT * FROM task_quarters';
		return self::fetchTaskQuarters( $strSql, $objDatabase );
	}
}
?>