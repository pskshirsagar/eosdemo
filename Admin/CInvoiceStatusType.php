<?php

class CInvoiceStatusType extends CBaseInvoiceStatusType {

	const PENDING_REVIEW 				= 1;
	const REVIEWED 						= 2;
	const INITIAL_BILLING_SUCCESSFUL	= 3;
	const INITIAL_BILLING_FAILED 		= 4;

	const UTILITY_BILLING_PENDING 		= 5;
 	const SALES_TAX_PENDING 			= 6;
}
?>