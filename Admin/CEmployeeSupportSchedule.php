<?php

class CEmployeeSupportSchedule extends CBaseEmployeeSupportSchedule {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDevEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQaEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBeginDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductOptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>