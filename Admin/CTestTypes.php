<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestTypes
 * Do not add any new functions to this class.
 */

class CTestTypes extends CBaseTestTypes {

	public static function fetchTestTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTestType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTestType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTestType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedTestTypes( $objDatabase ) {
		return self::fetchTestTypes( 'SELECT * FROM test_types WHERE is_published = 1 ORDER BY order_num', $objDatabase );
	}
}
?>