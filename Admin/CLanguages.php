<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CLanguages
 * Do not add any new functions to this class.
 */

class CLanguages extends CBaseLanguages {

	public static function fetchLanguages( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CLanguage', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchLanguage( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CLanguage', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>