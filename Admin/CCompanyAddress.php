<?php

class CCompanyAddress extends CBaseCompanyAddress {

	protected $m_strCompanyName;

	/**
	 * Get Functions
	 *
	 */

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = CStrings::strTrimDef( $strCompanyName, 100, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valPostalCode() {
		$boolIsValid = true;

		if( true == isset( $this->m_strPostalCode ) ) {

			if( preg_match( '/^[0-9]+$/', $this->m_strPostalCode ) )  return $boolIsValid;

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Invalid Postal Code' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valPostalCode();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>