<?php

class CCompanyPricing extends CBaseCompanyPricing {

	protected $m_strCompanyPricingTypeName;

	/**
	 * Get Functions
	 *
	 */

	public function getCompanyPricingTypeName() {
		return $this->m_strCompanyPricingTypeName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setCompanyPricingTypeName( $strCompanyPricingTypeName ) {
		$this->m_strCompanyPricingTypeName = $strCompanyPricingTypeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_pricing_type_name'] ) ) $this->setCompanyPricingTypeName( $arrmixValues['company_pricing_type_name'] );
	}

	/**
	 * Val Functions
	 *
	 */

	public function valCompanyPricingTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyPricingTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_pricing_type_id', 'Company pricing type is required for ' . CCompanyPricingType::getPricingTypeNameById( $this->getCompanyPricingTypeId() ) . '.' ) );
		}

		return $boolIsValid;
	}

	public function valAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Account is required for ' . CCompanyPricingType::getPricingTypeNameById( $this->getCompanyPricingTypeId() ) . '.' ) );
		}

		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount is required for ' . CCompanyPricingType::getPricingTypeNameById( $this->getCompanyPricingTypeId() ) . '.' ) );
			return $boolIsValid;
		}

		if( 0 >= $this->m_fltAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount must be valid for ' . CCompanyPricingType::getPricingTypeNameById( $this->getCompanyPricingTypeId() ) . '.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valCompanyPricingTypeId();
				$boolIsValid &= $this->valAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		return true;
	}

}
?>