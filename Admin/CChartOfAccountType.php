<?php

class CChartOfAccountType extends CBaseChartOfAccountType {

	const ACCOUNT_TYPE_ACCOUNTS_PAYABLE 		= 1;
	const ACCOUNT_TYPE_ACCOUNTS_RECEIVABLE 		= 2;
	const ACCOUNT_TYPE_BANK 					= 3;
	const ACCOUNT_TYPE_EXPENSE 					= 4;
	const ACCOUNT_TYPE_INCOME 					= 5;
	const ACCOUNT_TYPE_LONG_TERM_LIABILITY 		= 6;
	const ACCOUNT_TYPE_CURRENT_ASSET 			= 7;
	const ACCOUNT_TYPE_CURRENT_LIABILITY 		= 8;

}
?>