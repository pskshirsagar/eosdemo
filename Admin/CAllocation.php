<?php

class CAllocation extends CBaseAllocation {

	protected $m_strChargeCreditTransaction;

	/**
	 * get Functions
	 *
	 */

	public function getChargeCreditTransaction() {
		return $this->m_strChargeCreditTransaction;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['charge_credit_transaction'] ) ) {
			$this->setChargeAccountTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['charge_credit_transaction'] ) : $arrmixValues['charge_credit_transaction'] );
		}
	}

	public function setDefaults() {
		$this->setAllocationDatetime( date( 'm/d/Y H:i:s' ) );
	}

	public function setChargeCreditTransaction( $strChargeCreditTransaction ) {
		$this->m_strChargeCreditTransaction = $strChargeCreditTransaction;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );

			if( false == $this->insertReverseAllocation( $intCurrentUserId, $objDatabase ) ) {
				return false;
			}

			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function insertReverseAllocation( $intCurrentUserId, $objDatabase ) {
		$boolIsValid = true;

		$objReverseAllocation = clone $this;
		$objReverseAllocation->setId( $objReverseAllocation->fetchNextId( $objDatabase ) );
		$objReverseAllocation->setAllocationAmount( $this->getAllocationAmount() * -1 );
		$objReverseAllocation->setAllocationDatetime( 'NOW()' );
		$objReverseAllocation->setAllocationId( $this->getId() );
		$objReverseAllocation->setExportBatchId( NULL );
		$objReverseAllocation->setCreatedBy( $intCurrentUserId );
		$objReverseAllocation->setCreatedOn( 'NOW()' );

		if( false == $objReverseAllocation->insert( $intCurrentUserId, $objDatabase ) ) {
			$boolIsValid = false;
		}

		$this->setAllocationId( $objReverseAllocation->getId() );
		return $boolIsValid;
	}

}
?>