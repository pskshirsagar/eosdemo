<?php

class CChargeCode extends CBaseChargeCode {

	const PAYMENT_RECEIVED							= 1;
	const RETURN_FEE								= 9;
	const RETURN_ADJUSTMENT							= 10;
	const ADJUSTMENT								= 11;
	const CC_CONVENIENCE_FEE						= 13;
	const ACH_CONVENIENCE_FEE						= 14;
	const CHECK										= 16;
	const PAYMENT_REVERSAL_FEE						= 17;
	const PROSECT_PORTAL_HOSTING					= 19;
	const ACH_MANAGEMENT_FEE						= 21;
	const CC_MANAGEMENT_FEE							= 22;
	const WORKS_HOSTING								= 23;
	const ENTRATA_SUPPORT							= 23;
	const PAY_HOSTING								= 24;
	const ACH_MANAGEMENT_FAILED_FEE					= 25;
	const CC_MANAGEMENT_FAILED_FEE					= 26;
	const PHOTOGRAPHY								= 29;
	const SEARCH_ENGINE_OPTIMIZATION				= 30;
	const WORKS_SETUP								= 31;
	const PORTAL_SETUP								= 32;
	const PAY_SETUP									= 33;
	const DOMAIN_NAME_RENEWAL						= 34;
	const REFRIGERATOR_MAGNETS						= 35;
	const CHECK_21_FEE								= 37;
	const DISTRIBUTION_FEE							= 38;
	const CHECK_SCANNERS							= 39;
	const WHITE_FENCE								= 40;
	const BAD_DEBT									= 41;
	const ILS_PORTAL								= 42;
	const EMAIL_SET_UP								= 43;
	const CUSTOM_DEVELOPMENT						= 44;
	const RESIDENT_PORTAL_HOSTING					= 45;
	const RESIDENT_PORTAL_SET_UP					= 46;
	const SMS_MONTHLY_FEES							= 47;
	const SMS_PER_ITEM_FEES							= 48;
	const ILS_PORTAL_SET_UP							= 51;
	const VACANCY_BUDGET							= 52;
	const MOBILE_WEBSITES							= 53;
	const MONTHLY_EMAIL_HOSTING						= 55;
	const MARKETING_MATERIALS						= 56;
	const FLOOR_PLANS_3D							= 57;
	const MONTHLY_CHECK_SCANNING_FEE				= 58;
	const SITE_TABLET_SET_UP						= 59;
	const SITE_TABLET_RECURRING						= 60;
	const INTEGRATION_FEES							= 61;
	const MOBILE_WEBSITES_SET_UP					= 62;
	const BLOG_SET_UP								= 63;
	const BLOG_HOSTING								= 64;
	const SOCIAL_MEDIA_SET_UP						= 65;
	const SOCIAL_MEDIA_HOSTING						= 66;
	const SEO_SET_UP								= 67;
	const UTILITY_POSTAGE_FEES						= 68;
	const UTILITY_BILLING_FEES						= 69;
	const UTILITY_MOVE_IN_FEES						= 70;
	const UTILITY_TERMINATION_FEES					= 71;
	const UTILITY_VCR_PER_UNIT_FEES					= 72;
	const UTILITY_VCR_RECOVERY_FEES					= 73;
	const UTILITY_LATE_FEES							= 74;
	const DISTRIBUTION_FAILURE_FEE					= 75;
	const CREDIT_CARD_CHARGE_BACK					= 76;
	const MESSAGE_CENTER_SET_UP						= 77;
	const MESSAGE_CENTER_RECURRING					= 78;
	const LOBBY_DISPLAY_SET_UP						= 79;
	const LOBBY_DISPLAY_RECURRING					= 80;
	const PARCEL_ALERT_SET_UP						= 81;
	const PARCEL_ALERT_RECURRING					= 82;
	const CALL_TRACKER_SET_UP_FEE					= 83;
	const CALL_TRACKER_RECURRING					= 84;
	const CALL_TRACKER_PER_MINUTE					= 85;
	const RESIDENT_LEASE_SET_UP						= 86;
	const RESIDENT_LEASE_RECURRING					= 87;
	const MARKEL_RI_PREMIUMS						= 88;
	const CRAIGS_LIST_SET_UP_FEE					= 89;
	const CRAIGS_LIST_RECURRING						= 90;
	const BILL_PAY_PER_ITEM_FEE						= 91;
	const BILL_PAY_MONTHLY_FEE						= 92;
	const BILL_PAY_SET_UP_FEE						= 93;
	const UTILITY_SET_UP_FEE						= 94;
	const UTILITY_SALES_TAX							= 95;
	const UTILITY_EQUIPMENT_FEES					= 98;
	const LATE_FEE									= 99;
	const MAIL_INVOICE_FEE							= 100;
	const VOIP_FEE									= 101;
	const SUPPORT_FEE								= 102;
	const MARKEL_RI_CONVENIENCE_FEE					= 103;
	const MARKEL_RI_TAX								= 104;
	const RI_RETURN_ADJUSTMENT						= 105;
	const LEAD_MANAGEMENT_SET_UP					= 106;
	const LEAD_MANAGEMENT_RECURRING					= 107;
	const CHARITY_DONATION							= 108;
	const WRAPPER_PLUG_IN							= 109;
	const PORTAL_WRAPPER_SET_UP						= 110;
	const CALL_ANALYSIS_RECURRING					= 111;
	const CALL_ANALYSIS_SET_UP						= 112;
	const UTILITY_BILL_PER_UNIT_FEES				= 113;
	const LEASING_CENTER_RECURRING					= 114;
	const LEASING_CENTER_SET_UP						= 115;
	const SMS_SET_UP								= 116;
	const REVENUE_MANAGEMENT_SET_UP					= 117;
	const REVENUE_MANAGEMENT_RECURRING				= 118;
	const RESIDENT_VERIFY_LLC_RECURRING				= 119;
	const RESIDENT_VERIFY_FEES						= 120;
	const RESIDENT_VERIFY_SET_UP					= 121;
	const LEASING_LIBRARY_RECURRING					= 124;
	const LEASING_LIBRARY_SET_UP					= 125;
	const BALANCE_MOVEMENT							= 133;
	const PPC_SET_UP								= 135;
	const PPC_RECURRING								= 136;
	const VACANCY_COM_TRAINING						= 137;
	const BLOG_TRAINING								= 138;
	const MESSAGE_CENTER_TRAINING					= 139;
	const SITE_TABLET_TRAINING						= 140;
	const RESIDENT_INSURE_TRAINING					= 141;
	const ILS_PORTAL_TRAINING						= 142;
	const LOBBY_DISPLAY_TRAINING					= 143;
	const PARCEL_ALERT_TRAINING						= 144;
	const RESIDENT_PAY_TRAINING						= 145;
	const LEASING_CENTER_TRAINING					= 146;
	const RESIDENT_PAY_DESKTOP_TRAINING				= 147;
	const SEO_SERVICES_TRAINING						= 148;
	const LEASE_EXECUTION_TRAINING					= 149;
	const CRAIGSLIST_POSTING_TRAINING				= 150;
	const LEAD_MANAGEMENT_TRAINING					= 151;
	const ENTRATA_TRAINING							= 152;
	const PRICING_PORTAL_TRAINING					= 153;
	const RESIDENTUTILITY_TRAINING					= 154;
	const ONLINE_APPLICATION_TRAINING				= 155;
	const SEM_SERVICES_TRAINING						= 156;
	const SEO_DASHBOARD_TRAINING					= 157;
	const RESIDENT_VERIFY_TRAINING					= 158;
	const FACEBOOK_INTEGRATION_TRAINING				= 159;
	const CALL_ANALYSIS_TRAINING					= 161;
	const PROSPECT_PORTAL_TRAINING					= 162;
	const RESIDENT_PORTAL_TRAINING					= 163;
	const CALL_TRACKING_TRAINING					= 164;
	const GMAIL_EMAIL_TRAINING						= 165;
	const CUSTOM_DEVELOPMENT_TRAINING				= 166;
	const ACH_CONVENIENCE_FEE_CLEARING 				= 167;
	const CC_CONVENIENCE_FEE_CLEARING 				= 168;
	const OLD_ACH_CONVENIENCE_FEE_CLEARING 			= 169;
	const OLD_CC_CONVENIENCE_FEE_CLEARING 			= 170;
	const INSPECTION_MANGER_SET_UP					= 173;
	const INSPECTION_MANGER_RECURRING				= 174;
	const RIVERSTONE_MONEY_GRAM_RECURRING 			= 175;
	const PPC_ENGINE_RECURRING						= 176;
	const INOVICE_PROCESSING_TRANSACTION			= 179;
	const ACH_REBATE_FEE_CLEARING					= 180;
	const CC_REBATE_FEE_CLEARING					= 181;
	const UNDERWRITING_RESERVES						= 182;
	const SALES_TAX									= 183;
	const SALES_TAX_ADJUSTMENT						= 187;
	const SUB_METERING_LABOR						= 188;
	const SUB_METERING_PARTS						= 189;
	const REPUTATION_ADVISER_RECURRING				= 190;
	const REPUTATION_ADVISER_SET_UP					= 191;
	const CUSTOM_RP_MOBILE_APP_RECURRING			= 197;
	const CUSTOM_RP_MOBILE_APP_SET_UP				= 198;
	const CONSULTING_ONSITE_TRAINING				= 199;
	const ENTRATA_JOB_COSTING_RECURRING				= 200;
	const VENDOR_ACCESS								= 201;
	const UTILITY_EXPENSE_MANAGEMENT_IMPLEMENTATION	= 202;
	const UTILITY_EXPENSE_MANAGEMENT_RECURRING		= 203;
	const UTILITY_EXPENSE_MANAGEMENT_TRANSACTIONAL	= 204;
	const RESIDENT_SECURE							= 206;
	const RESIDENT_SECURE_CARRIER_LIABILITY			= 207;
	const RESIDENT_SECURE_REV_SHARE_LIABILITY		= 208;
	const FINANCE_CHARGE_FEES						= 210;
	const ENTRATA_DATA_SERVICES						= 205;
	const VENDOR_ACCESS_CREDIT                      = 211;
	const VENDOR_ACCESS_CRIMINAL                    = 212;
	const VENDOR_ACCESS_EMPLOYEE                    = 213;
	const VENDOR_ACCESS_ADMIN_FEE                   = 214;
	const RI_PROVIDER_LIABILITY_AMOUNT              = 215;
	const KEMPER_CARRIER_LIABILITY					= 216;
	const QBE_CARRIER_LIABILITY						= 217;
	const QBE_COMMISSION_RECEIVABLE					= 218;
	const BILL_PAY_ACH_MANAGEMENT_FEE 				= 223;
	const BILL_PAY_CHECK_MANAGEMENT_FEE				= 224;
	const BILL_PAY_ACH_RETURN_FEE 					= 225;
	const BILL_PAY_ACH_REVERSE_FEE 					= 226;
	const PSI_EARNED_PREMIUM                        = 220;
	const MASTER_POLICY_PREMIUM                     = 227;
	const ACQUIRE_PROCESSING_FEE                    = 233;
    const BAH_PAYMENT                               = 1103;
	const RESIDENT_PAY_ACCOUNT_VERIFICATION			= 236;
	const SITE_PLAN									= 244;
	const TEMPLATE_LOGO								= 245;
	const CUSTOM_LOGO								= 246;
	const UTILITY_SUBSIDY_BILLING_FEE				= 251;
	const UNAUTHORIZED_RETURN_FEE					= 235;
	const ENTRATA_DATA_SERVICES_FEES                = 205;
	const MASTER_POLICY_LIABILITY                   = 228;
	const MASTER_POLICY_COMMISSION                  = 229;
	const MASTER_POLICY_ADMIN_FEE					= 260;
	const MASTER_POLICY_AR					        = 275;

	const PAD_CONVENIENCE_FEE						= 281;
	const PAD_CONVENIENCE_FEE_CLEARING				= 282;
	const PAD_MANAGEMENT_FEE						= 279;
	const PAD_REBATE_FEE_CLEARING					= 280;

	const PPC_ENGINE_REGION							= 287;
	const PPC_ENGINE_BRAND							= 288;

	const DEFERRED_REVENUE							= 99999; // Note: This is not a real charge code

	public static $c_arrintSetUpChargeCodes = [
		self::WORKS_SETUP,
		self::PORTAL_SETUP,
		self::PAY_SETUP,
		self::RESIDENT_PORTAL_SET_UP,
		self::ILS_PORTAL_SET_UP,
		self::SITE_TABLET_SET_UP,
		self::MOBILE_WEBSITES_SET_UP,
		self::BLOG_SET_UP,
		self::SOCIAL_MEDIA_SET_UP,
		self::SEO_SET_UP,
		self::MESSAGE_CENTER_SET_UP,
		self::LOBBY_DISPLAY_SET_UP,
		self::PARCEL_ALERT_SET_UP,
		self::CALL_TRACKER_SET_UP_FEE,
		self::RESIDENT_LEASE_SET_UP,
		self::CRAIGS_LIST_SET_UP_FEE,
		self::BILL_PAY_SET_UP_FEE,
		self::UTILITY_SET_UP_FEE,
		self::LEAD_MANAGEMENT_SET_UP,
		self::PORTAL_WRAPPER_SET_UP,
		self::CALL_ANALYSIS_SET_UP,
		self::LEASING_CENTER_SET_UP,
		self::REVENUE_MANAGEMENT_SET_UP,
		self::LEASING_LIBRARY_SET_UP,
		self::RESIDENT_VERIFY_SET_UP,
		self::PPC_SET_UP,
		self::EMAIL_SET_UP,
		self::SMS_SET_UP,
		self::INSPECTION_MANGER_SET_UP,
		self::REPUTATION_ADVISER_SET_UP
	];

	public static $c_arrintRecurringChargeGroups = [
		self::PROSECT_PORTAL_HOSTING,
		self::PAY_HOSTING,
		self::ILS_PORTAL,
		self::RESIDENT_PORTAL_HOSTING,
		self::MOBILE_WEBSITES,
		self::MONTHLY_CHECK_SCANNING_FEE,
		self::SOCIAL_MEDIA_HOSTING,
		self::LOBBY_DISPLAY_RECURRING,
		self::CALL_TRACKER_RECURRING,
		self::RESIDENT_LEASE_RECURRING,
		self::CRAIGS_LIST_RECURRING,
		self::BILL_PAY_MONTHLY_FEE,
		self::LEAD_MANAGEMENT_RECURRING,
		self::CALL_ANALYSIS_RECURRING,
		self::SEARCH_ENGINE_OPTIMIZATION,
		self::SITE_TABLET_RECURRING,
		self::LEASING_CENTER_RECURRING,
		self::MESSAGE_CENTER_RECURRING,
		self::DOMAIN_NAME_RENEWAL,
		self::MONTHLY_EMAIL_HOSTING,
		self::RIVERSTONE_MONEY_GRAM_RECURRING,
		self::SMS_MONTHLY_FEES,
		self::BLOG_HOSTING,
		self::PARCEL_ALERT_RECURRING,
		self::VOIP_FEE,
		self::REVENUE_MANAGEMENT_RECURRING,
		self::LEASING_LIBRARY_RECURRING,
		self::WORKS_HOSTING,
		self::RESIDENT_VERIFY_LLC_RECURRING,
		self::INSPECTION_MANGER_RECURRING,
		self::PPC_RECURRING,
		self::PPC_ENGINE_RECURRING,
		self::REPUTATION_ADVISER_RECURRING
	];

	// Define the charge codes the have detail such as the eft charge codes.
	public static $c_arrintChargeCodesWithDetail = [
		self::ACH_CONVENIENCE_FEE_CLEARING,
		self::ACH_CONVENIENCE_FEE,
		self::ACH_MANAGEMENT_FEE,
		self::CC_CONVENIENCE_FEE,
		self::CC_CONVENIENCE_FEE_CLEARING,
		self::CC_MANAGEMENT_FEE,
		self::CHARITY_DONATION,
		self::CHECK_21_FEE,
		self::CREDIT_CARD_CHARGE_BACK,
		self::DISTRIBUTION_FEE,
		self::DISTRIBUTION_FAILURE_FEE,
		self::LATE_FEE,
		self::PAYMENT_REVERSAL_FEE,
		self::RETURN_FEE,
		self::CC_MANAGEMENT_FAILED_FEE,
		self::ACH_REBATE_FEE_CLEARING,
		self::CC_REBATE_FEE_CLEARING,
		self::BILL_PAY_CHECK_MANAGEMENT_FEE,
		self::BILL_PAY_ACH_REVERSE_FEE,
		self::BILL_PAY_ACH_RETURN_FEE,
		self::BILL_PAY_ACH_MANAGEMENT_FEE
	];

	public static $c_arrintNonGaapChargeCodes = [
		CChargeCode::PAYMENT_RECEIVED,
		CChargeCode::CHARITY_DONATION,
		CChargeCode::RETURN_ADJUSTMENT,
		CChargeCode::OLD_ACH_CONVENIENCE_FEE_CLEARING,
		CChargeCode::OLD_CC_CONVENIENCE_FEE_CLEARING,
		CChargeCode::ACH_CONVENIENCE_FEE_CLEARING,
		CChargeCode::CC_CONVENIENCE_FEE_CLEARING,
		CChargeCode::SALES_TAX,
		CChargeCode::SALES_TAX_ADJUSTMENT
	];

	public static $c_arrintUtilityChargeCodes = [
		self::UTILITY_POSTAGE_FEES,
		self::UTILITY_BILLING_FEES,
		self::UTILITY_MOVE_IN_FEES,
		self::UTILITY_TERMINATION_FEES,
		self::UTILITY_BILL_PER_UNIT_FEES,
		self::UTILITY_LATE_FEES,
		self::UTILITY_VCR_RECOVERY_FEES,
		self::UTILITY_VCR_PER_UNIT_FEES,
		self::UTILITY_SALES_TAX,
		self::UTILITY_SET_UP_FEE,
		self::UTILITY_EQUIPMENT_FEES,
		self::RESIDENTUTILITY_TRAINING,
		self::UTILITY_EXPENSE_MANAGEMENT_IMPLEMENTATION,
		self::UTILITY_EXPENSE_MANAGEMENT_RECURRING,
		self::UTILITY_EXPENSE_MANAGEMENT_TRANSACTIONAL,
		self::UTILITY_SUBSIDY_BILLING_FEE
	];

	public static $c_arrintIgnoreDelinquencyChargeCodes = [
		self::UTILITY_LATE_FEES,
		self::LATE_FEE
	];

	public static $c_arrintInsuranceReversalChargeCodeIds = [
		self::MARKEL_RI_CONVENIENCE_FEE,
		self::MARKEL_RI_PREMIUMS,
		self::MARKEL_RI_TAX,
		self::RI_PROVIDER_LIABILITY_AMOUNT,
		self::PSI_EARNED_PREMIUM,
		self::PSI_EARNED_PREMIUM,
		self::RESIDENT_SECURE_CARRIER_LIABILITY,
		self::RESIDENT_SECURE
	];

	public static $c_arrintResidentInsureChargeCodeIds = [
		self::MARKEL_RI_PREMIUMS,
		self::MARKEL_RI_CONVENIENCE_FEE,
		self::MARKEL_RI_TAX,
		self::RI_RETURN_ADJUSTMENT
	];

	public static $c_arrintPPCEngineSpendChargeCodeIds = [
		self::PPC_ENGINE_RECURRING,
		self::PPC_ENGINE_REGION,
		self::PPC_ENGINE_BRAND
	];

	public static $c_arrintMPARChargeCodeIds = [
		self::MASTER_POLICY_LIABILITY,
		self::MASTER_POLICY_COMMISSION
	];

	public static $c_arrintSubMeteringChargeCodeIds = [
		self::SUB_METERING_LABOR,
		self::SUB_METERING_PARTS
	];

	protected $m_strDebitAccountName;
	protected $m_strCreditAccountName;
	protected $m_intDebitGlAccountNumber;
	protected $m_intCreditGlAccountNumber;
	protected $m_intCreditAccountSecondaryNumber;
	protected $m_intDebitAccountSecondaryNumber;
	protected $m_strProductName;
	protected $m_strRevenueTypeName;

	/**
	*Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['debit_account_name'] ) ) {
			$this->setDebitAccountName( $arrmixValues['debit_account_name'] );
		}
		if( true == isset( $arrmixValues['credit_account_name'] ) ) {
			$this->setCreditAccountName( $arrmixValues['credit_account_name'] );
		}
		if( true == isset( $arrmixValues['debit_gl_account_number'] ) ) {
			$this->setDebitGlAccountNumber( $arrmixValues['debit_gl_account_number'] );
		}
		if( true == isset( $arrmixValues['credit_gl_account_number'] ) ) {
			$this->setCreditGlAccountNumber( $arrmixValues['credit_gl_account_number'] );
		}
		if( true == isset( $arrmixValues['credit_account_secondary_number'] ) ) {
			$this->setCreditAccountSecondaryNumber( $arrmixValues['credit_account_secondary_number'] );
		}
		if( true == isset( $arrmixValues['debit_account_secondary_number'] ) ) {
			$this->setDebitAccountSecondaryNumber( $arrmixValues['debit_account_secondary_number'] );
		}
		if( true == isset( $arrmixValues['product_name'] ) ) {
			$this->setProductName( $arrmixValues['product_name'] );
		}
		if( true == isset( $arrmixValues['revenue_type_name'] ) ) {
			$this->setRevenueTypeName( $arrmixValues['revenue_type_name'] );
		}
	}

	public function setDebitAccountName( $strDebitAccountName ) {
		$this->m_strDebitAccountName = $strDebitAccountName;
	}

	public function setCreditAccountName( $strCreditAccountName ) {
		$this->m_strCreditAccountName = $strCreditAccountName;
	}

	public function setDebitGlAccountNumber( $intDebitGlAccountNumber ) {
		$this->m_intDebitGlAccountNumber = $intDebitGlAccountNumber;
	}

	public function setCreditGlAccountNumber( $intCreditGlAccountNumber ) {
		$this->m_intCreditGlAccountNumber = $intCreditGlAccountNumber;
	}

	public function setCreditAccountSecondaryNumber( $intCreditAccountSecondaryNumber ) {
		$this->m_intCreditAccountSecondaryNumber = $intCreditAccountSecondaryNumber;
	}

	public function setDebitAccountSecondaryNumber( $intDebitAccountSecondaryNumber ) {
		$this->m_intDebitAccountSecondaryNumber = $intDebitAccountSecondaryNumber;
	}

	public function setProductName( $strProductName ) {
		$this->m_strProductName = $strProductName;
	}

	public function setRevenueTypeName( $strRevenueTypeName ) {
		$this->m_strRevenueTypeName = $strRevenueTypeName;
	}

	/**
	*Get Functions
	*
	*/

	public function getDebitAccountName() {
		return $this->m_strDebitAccountName;
	}

	public function getCreditAccountName() {
		return $this->m_strCreditAccountName;
	}

	public function getDebitGlAccountNumber() {
		return $this->m_intDebitGlAccountNumber;
	}

	public function getCreditGlAccountNumber() {
		return $this->m_intCreditGlAccountNumber;
	}

	public function getCreditAccountSecondaryNumber() {
		return $this->m_intCreditAccountSecondaryNumber;
	}

	public function getDebitAccountSecondaryNumber() {
		return $this->m_intDebitAccountSecondaryNumber;
	}

	public function getProductName() {
		return $this->m_strProductName;
	}

	public function getRevenueTypeName() {
		return $this->m_strRevenueTypeName;
	}

	/**
	*Validation Functions
	*
	*/

	public function valChargeCodeTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getChargeCodeTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_type_id', 'Charge code type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valCreditChartOfAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCreditChartOfAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_chart_of_account_id', 'Credit chart of account is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valDebitChartOfAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDebitChartOfAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_chart_of_account_id', 'Debit chart of account is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valRevenueTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getRevenueTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'revenue_type_id', 'Revenue type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valRefundablePercent() {
		$boolIsValid = true;

		$fltRefundablePercent = ( 0 < $this->getRefundablePercent() ) ? $this->getRefundablePercent() * 100 : $this->getRefundablePercent();

		if( 100 < $fltRefundablePercent ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'refundable_percent', 'Refundable percent should be less/equal to 100. ' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionPercent() {
		$boolIsValid = true;

		$fltCommissionPercent = ( 0 < $this->getCommissionPercent() ) ? $this->getCommissionPercent() * 100 : $this->getCommissionPercent();

		if( 100 < $fltCommissionPercent ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_percent', 'Commission percent should be less/equal to 100. ' ) );
		}

		return $boolIsValid;
	}

	public function valCostPercent() {
		$boolIsValid = true;

		$fltCostPercent = ( 0 < $this->getCostPercent() ) ? $this->getCostPercent() * 100 : $this->getCostPercent();

		if( 100 < $fltCostPercent ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cost_percent', 'Cost percent should be less/equal to 100. ' ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceCategoryId() {
		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceCategoryId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_category_id', 'Invoice Category is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valSalesTaxCategoryId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSalesTaxCategoryId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sales_tax_category_id', 'Sales Tax Category is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateChargeCodeName( $objAdminDatabase ) {
		$boolIsValid = true;
		if( false == is_null( $this->getName() ) ) {
			$intChargeCodeCount = \Psi\Eos\Admin\CChargeCodes::createService()->fetchConflictingChargeCodeCountByNameById( $this->getName(), $this->getId(), $objAdminDatabase );

			if( 0 < $intChargeCodeCount ) {
				$this->addErrorMsg( new CErrorMsg( NULL, 'name', 'Name already exists.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDuplicateChargeCodeName( $objAdminDatabase );
				$boolIsValid &= $this->valChargeCodeTypeId();
				$boolIsValid &= $this->valCreditChartOfAccountId();
				$boolIsValid &= $this->valDebitChartOfAccountId();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valRevenueTypeId();
				$boolIsValid &= $this->valRefundablePercent();
				$boolIsValid &= $this->valCommissionPercent();
				$boolIsValid &= $this->valCostPercent();
				$boolIsValid &= $this->valInvoiceCategoryId();
				$boolIsValid &= $this->valSalesTaxCategoryId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setNextOrderNum( $objDatabase ) {

		$objDataset = $objDatabase->createDataset();

			$strSql = "SELECT nextval('charge_codes_id_seq'::text) AS id";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to get next value' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrintValues = $objDataset->fetchArray();
			$this->setOrderNum( $arrintValues['id'] );
			$this->setId( $arrintValues['id'] );

			$objDataset->cleanup();

		return true;
	}

	public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {

		if( true == isset( $arrmixRequestForm['commission_percent'] ) ) {
			$arrmixRequestForm['commission_percent']	= ( 0 < $arrmixRequestForm['commission_percent'] ) ? $arrmixRequestForm['commission_percent'] / 100 : NULL;
		}
		if( true == isset( $arrmixRequestForm['cost_percent'] ) ) {
			$arrmixRequestForm['cost_percent'] = ( 0 < $arrmixRequestForm['cost_percent'] ) ? $arrmixRequestForm['cost_percent'] / 100 : NULL;
		}
		if( true == isset( $arrmixRequestForm['refundable_percent'] ) ) {
			$arrmixRequestForm['refundable_percent'] = ( 0 < $arrmixRequestForm['refundable_percent'] ) ? $arrmixRequestForm['refundable_percent'] / 100 : NULL;
		}
		parent::applyRequestForm( $arrmixRequestForm, $arrmixFormFields );
	}

}

?>
