<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationHistories
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationHistories extends CBaseEmployeeApplicationHistories {

	public static function fetchEmployeeApplicationHistoriesByEmployeeApplicationCompanyIds( $arrintCompanyIds, $objDatabase ) {
		if( false == valArr( $arrintCompanyIds ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						employee_application_histories
					WHERE
						employee_application_company_id IN ( ' . implode( ',', $arrintCompanyIds ) . ' )';

		return self::fetchEmployeeApplicationHistories( $strSql, $objDatabase );
	}

	public static function fetchEmployeeApplicationHistoriesByEmployeeApplicationIds( $arrintEmployeeApplicationIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeApplicationIds ) ) return NULL;

		$strSql = 'SELECT * FROM employee_application_histories WHERE employee_application_id IN ( ' . implode( ',', $arrintEmployeeApplicationIds ) . ')';

		return self::fetchEmployeeApplicationHistories( $strSql, $objDatabase );
	}

	public static function fetchEmployeeApplicationHistoryDetailsByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		if( false == is_numeric( $intEmployeeApplicationId ) ) return NULL;

		$strSql = 'SELECT
						eah.employee_application_id,
						eac.company_name,
						eah.designation,
						EXTRACT(YEAR FROM age( COALESCE ( eah.end_date, NOW ( ) ), eah.start_date ) ) as year,
						EXTRACT(MONTH FROM age( COALESCE ( eah.end_date, NOW ( ) ), eah.start_date ) ) as month,
						eah.reason_for_leaving
					FROM
						employee_application_histories eah
						LEFT JOIN employee_application_companies eac ON ( eac.id = eah.employee_application_company_id )
					WHERE
						eah.employee_application_id = ' . ( int ) $intEmployeeApplicationId;
		return fetchData( $strSql, $objDatabase );
	}

}
?>