<?php

use Psi\Libraries\HtmlMimeMail\CHtmlMimeMail;
use Psi\Libraries\UtilHash\CHash;

class CEntity extends CBaseEntity {

	protected $m_boolIsLoggedIn;

	protected $m_strOldPassword;
	protected $m_strPassword;
	protected $m_strPasswordConfirm;

	protected $m_arrobjInsurancePolicies;

	const LOGIN_FROM_CLIENT_ADMIN 		= 1;
	const LOGIN_FROM_PROSPECT_PORTAL 	= 2;
	const LOGIN_FROM_RESIDENT_PORTAL 	= 3;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsLoggedIn = false;

		return;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createInsurancePolicy() {

		$objInsurancePolicy = new CInsurancePolicy();
		$objInsurancePolicy->setEntityId( $this->getId() );

		return $objInsurancePolicy;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function getPasswordConfirm() {
		return $this->m_strPasswordConfirm;
	}

	public function getIsLoggedIn() {
		return $this->m_boolIsLoggedIn;
	}

	public function getTaxNumber() {
		return  preg_replace( '/[^a-z0-9]/i', '', CEncryption::decryptText( $this->m_strTaxNumberEncrypted, CONFIG_KEY_TAX_NUMBER ) );
	}

	public function getTaxNumberMasked() {
		if( 4 == strlen( $this->getTaxNumber() ) ) {
			return $this->getTaxNumber();
		} else {
			return CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $this->getTaxNumber() ) );
		}
	}

	public function getOldPassword() {
		return $this->m_strOldPassword;
	}

	public function getInsurancePolicies() {
		return $this->m_arrobjInsurancePolicies;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['password'] ) ) 			$this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['password'] ) : $arrstrValues['password'] );
		if( true == isset( $arrstrValues['password_confirm'] ) ) 	$this->setPasswordConfirm( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['password_confirm'] ) : $arrstrValues['password_confirm'] );
		if( true == isset( $arrstrValues['old_password'] ) ) 		$this->setOldPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['old_password'] ) : $arrstrValues['old_password'] );

		return;
	}

	public function setPassword( $strPassword ) {
		$this->m_strPassword = $strPassword;
	}

	public function setPasswordConfirm( $strPasswordConfirm ) {
		$this->m_strPasswordConfirm = $strPasswordConfirm;
	}

	public function setTaxNumber( $strPlainTaxNumber ) {
		if( true == \Psi\CStringService::singleton()->stristr( $strPlainTaxNumber, '****' ) ) return;

		$this->setTaxNumberEncrypted( CEncryption::encryptText( preg_replace( '/[^a-z0-9]/i', '', $strPlainTaxNumber ), CONFIG_KEY_TAX_NUMBER ) );
		$this->setTaxNumberMasked( CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $strPlainTaxNumber ) ) );
	}

	public function setTaxNumberMasked( $strTaxNumberMasked ) {
		$this->m_strTaxNumberMasked = CStrings::strTrimDef( $strTaxNumberMasked, 240, NULL, true );
	}

	public function setIsLoggedIn( $boolIsLoggedIn ) {
		$this->m_boolIsLoggedIn = $boolIsLoggedIn;
	}

	public function setOldPassword( $strOldPassword ) {
		$this->m_strOldPassword = $strOldPassword;
	}

	public function setInsurancePolicies( $arrobjInsurancePolicies ) {
		$this->m_arrobjInsurancePolicies = $arrobjInsurancePolicies;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addInsurancePolicy( $objInsurancePolicy ) {
		$this->m_arrobjInsurancePolicies[$objInsurancePolicy->getId()] = $objInsurancePolicy;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchInsurancePolicy( $objAdminDatabase ) {
		return \Psi\Eos\Insurance\CInsurancePolicies::createService()->fetchInsurancePolicyByEntityId( $this->getId(), $objAdminDatabase );
	}

	public function fetchInsurancePolicyById( $intId, $objAdminDatabase ) {
		return \Psi\Eos\Insurance\CInsurancePolicies::createService()->fetchInsurancePolicyByEntityIdById( $this->getId(), $intId, $objAdminDatabase );
	}

	public function fetchInsurancePolicyByInsurancePolicyId( $intInsurancePolicyId, $objInsurancePortalDatabase ) {
		return \Psi\Eos\Insurance\CInsurancePolicies::createService()->fetchInsurancePolicyByIdByEntityId( $intInsurancePolicyId, $this->getId(), $objInsurancePortalDatabase );
	}

	public function fetchAccounts( $objInsurancePortalDatabase ) {
		return \Psi\Eos\Admin\CAccounts::createService()->fetchAccountsByEntityId( $this->getId(), $objInsurancePortalDatabase );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valCompanyName() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', 'Company name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name is required.' ) );

		} elseif( false == preg_match( "/^[A-Za-z0-9-_.\",'\s]+$/", $this->getNameFirst() ) || true == \Psi\CStringService::singleton()->stristr( $this->getNameFirst(), '"' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name should be set of alphabets.' ) );

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameFirst, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameFirst, 'Content-type' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name cannot contain @ signs or email headers.' ) );
		}

		return $boolIsValid;
	}

	public function valNameMiddle() {
		$boolIsValid = true;

		if( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameMiddle, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameMiddle, 'Content-type' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_middle', 'Middle name cannot contain @ signs or email headers.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name is required.' ) );

		} elseif( false == preg_match( "/^[A-Za-z0-9-_.\",'\s]+$/", $this->getNameLast() ) || true == \Psi\CStringService::singleton()->stristr( $this->getNameLast(), '"' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name should be set of alphabets.' ) );

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameLast, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameLast, 'Content-type' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name cannot contain @ signs or email headers.' ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine1( $boolIsMailingAddress = false ) {
		$boolIsValid = true;

		$strFieldName 	= 'street_line1';
		$strMessage 	= 'Mailing street is required.';

		if( true == $boolIsMailingAddress ) {
			$strFieldName 	= 'mailing_street_line1';
			$strMessage 	= 'Mailing street address is required.';
		}

		if( true == is_null( $this->getStreetLine1() ) || 0 == strlen( $this->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode( $boolIsMailingAddress = false ) {
		$boolIsValid = true;

		$strZipCode = $this->getPostalCode();

		$strFieldName 	 = 'postal_code';
		$strMessage 	 = 'Zip code is required.';
		$strValidMessage = 'Zip code is not valid.';

		if( true == $boolIsMailingAddress ) {
			$strFieldName 	 = 'mailing_postal_code';
			$strMessage 	 = 'Mailing zip code is required.';
			$strValidMessage = 'Mailing zip code is not valid.';

		}

		if( true == is_null( $strZipCode ) || 0 == strlen( $strZipCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
		}

		if( false == is_null( $strZipCode ) && 0 < strlen( $strZipCode ) ) {
			$strZipCodeCheck = '/(^\d{5}$)|(^\d{5}-\d{4}$)/';

			if( preg_match( $strZipCodeCheck, $strZipCode ) !== 1 ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName,  $strValidMessage ) );
			}
		}

		return $boolIsValid;
	}

	public function valCity( $boolIsMailingAddress = false ) {
		$boolIsValid = true;

		$strFieldName 	= 'city';
		$strMessage 	= 'City is required.';

		if( true == $boolIsMailingAddress ) {
			$strFieldName 	= 'mailing_city';
			$strMessage 	= 'Mailing city is required.';
		}

		if( true == is_null( $this->getCity() ) || 0 == strlen( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
		}

		return $boolIsValid;
	}

	public function valStateCode( $boolIsMailingAddress = false ) {
		$boolIsValid = true;

		$strFieldName 	= 'state_code';
		$strMessage 	= 'State code is required.';

		if( true == $boolIsMailingAddress ) {
			$strFieldName 	= 'mailing_state_code';
			$strMessage 	= 'Mailing state code is required.';
		}

		if( true == is_null( $this->getStateCode() ) || 0 == strlen( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber( $boolIsRequired = false ) {

		$boolIsValid = true;

		$intLength = strlen( $this->m_strPhoneNumber );

		$strSectionName = 'Phone';

		if( true == $boolIsRequired ) {
			if( true == is_null( $this->m_strPhoneNumber ) || 0 == $intLength ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', $strSectionName . ' number is required.' ) );
			}
		}

		if( false == is_null( $this->m_strPhoneNumber ) && 0 < $intLength && ( 15 >= $intLength ) ) {
			$strPhonenumberCheck = '/^(\([0-9]{3}\))\s([0-9]{3})-([0-9]{4})$/';

			if( preg_match( $strPhonenumberCheck, $this->m_strPhoneNumber ) !== 1 ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Valid ' . $strSectionName . ' number is required.', 3 ) );
			}
		} elseif( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', $strSectionName . ' number length must be between 10 to 15.', 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valUsername( $objDatabase = NULL, $boolUsernameRequired = false, $boolAllowDuplicate = false ) {

		$boolIsValid = true;

		if( true == isset( $this->m_strUsername ) && 0 < strlen( $this->m_strUsername ) || true == $boolUsernameRequired ) {

			if( false == isset( $this->m_strUsername ) || 0 == strlen( trim( $this->m_strUsername ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Email is required.' ) );
				return $boolIsValid;

			} elseif( false == CValidation::validateEmailAddresses( $this->m_strUsername ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Email does not appear to be valid.', 1 ) );
				return $boolIsValid;
			}

			// Make sure it does not already exist in the entity table
			if( false == $boolAllowDuplicate && false == is_null( $objDatabase ) ) {
				$objPreExistingEntity = CEntities::fetchPreExistingEntityByUsername( $this->m_strUsername, $objDatabase, $this->m_intId );

				if( true == valObj( $objPreExistingEntity, 'CEntity' ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Email is already being used.', 2 ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valPassword() {

		$boolIsValid = true;

		if( true == is_null( $this->getPassword() ) || ( 0 == strlen( $this->getPassword() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
		}

		if( true == is_null( $this->getPasswordConfirm() ) || ( 0 == strlen( $this->getPasswordConfirm() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_confirm', 'Confirm Password is required.' ) );
		}

		if( true == $boolIsValid && 6 > strlen( $this->m_strPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password must be at least 6 characters long.' ) );
		}

		if( true == $boolIsValid && ( trim( $this->m_strPassword ) != trim( $this->m_strPasswordConfirm ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password and confirmation password do not match.' ) );
		}

		return $boolIsValid;
	}

	public function valChangePassword() {

		$boolIsValid = true;

		if( true == is_null( $this->getOldPassword() ) || ( 0 == strlen( $this->getOldPassword() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_old', 'Old Password is required.' ) );
		}

		$strPassword = CHash::createService()->hashEntity( $this->getOldPassword() );

		if( $strPassword != $this->getPasswordEncrypted() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_old', 'Current password is not valid.' ) );
		}

		if( true == is_null( $this->getPassword() ) || ( 0 == strlen( $this->getPassword() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
		}

		if( true == is_null( $this->getPasswordConfirm() ) || ( 0 == strlen( $this->getPasswordConfirm() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_confirm', 'Confirm Password is required.' ) );
		}

		if( true == $boolIsValid && 6 > strlen( $this->m_strPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password must be at least 6 characters long.' ) );
		}

		if( true == $boolIsValid && ( trim( $this->m_strPassword ) != trim( $this->m_strPasswordConfirm ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password and confirmation password do not match.' ) );
		}

		return $boolIsValid;
	}

	public function valMailingStreetLine1() {
		$boolIsValid = true;

		$strFieldName 	= 'mailing_street_line1';
		$strMessage 	= 'Mailing street is required.';

		if( true == is_null( $this->getMailingStreetLine1() ) || 0 == strlen( $this->getMailingStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
		}

		return $boolIsValid;
	}

	public function valMailingCity() {
		$boolIsValid = true;

		$strFieldName 	= 'mailing_city';
		$strMessage 	= 'Mailing city is required.';

		if( true == is_null( $this->getMailingCity() ) || 0 == strlen( $this->getMailingCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
		}

		return $boolIsValid;
	}

	public function valMailingStateCode() {
		$boolIsValid = true;

		$strFieldName 	= 'mailing_state_code';
		$strMessage 	= 'Mailing state code is required.';

		if( true == is_null( $this->getMailingStateCode() ) || 0 == strlen( $this->getMailingStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
		}

		return $boolIsValid;
	}

	public function valMailingPostalCode() {
		$boolIsValid = true;

		$strZipCode = $this->getMailingPostalCode();

		$strFieldName 	 = 'mailing_postal_code';
		$strMessage 	 = 'Mailing zip code is required.';
		$strValidMessage = 'Mailing zip code is not valid.';

		if( true == is_null( $strZipCode ) || 0 == strlen( $strZipCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
		}

		if( false == is_null( $strZipCode ) && 0 < strlen( $strZipCode ) ) {
			$strZipCodeCheck = '/(^\d{5}$)|(^\d{5}-[a-zA-Z0-9]{4}$)/';
			if( preg_match( $strZipCodeCheck, $strZipCode ) !== 1 ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName,  $strValidMessage ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameMiddle();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber( false );
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valUsername( $objDatabase, true, false );
				$boolIsValid &= $this->valPassword();
				break;

			case 'validate_insert_client':
				$boolIsValid &= $this->valCompanyName();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameMiddle();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber( true );
				$boolIsValid &= $this->valStreetLine1( $boolIsMailingAddress = true );
				$boolIsValid &= $this->valPostalCode( $boolIsMailingAddress = true );
				$boolIsValid &= $this->valCity( $boolIsMailingAddress = true );
				$boolIsValid &= $this->valStateCode( $boolIsMailingAddress = true );
				break;

			case 'validate_address_info':
				$boolIsValid &= $this->valStreetLine1( $boolIsMailingAddress = true );
				$boolIsValid &= $this->valPostalCode( $boolIsMailingAddress = true );
				$boolIsValid &= $this->valCity( $boolIsMailingAddress = true );
				$boolIsValid &= $this->valStateCode( $boolIsMailingAddress = true );
				break;

			case 'validate_user_info':
				$boolIsValid &= $this->valUsername( $objDatabase, true, false );
				break;

			case 'validate_user_password':
				$boolIsValid &= $this->valPassword();
				break;

			case 'validate_entity_change_password':
				$boolIsValid &= $this->valChangePassword();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function encryptPassword() {
		$this->m_strPasswordEncrypted = CHash::createService()->hashEntity( $this->getPassword() );
	}

	public function login( $objDatabase, $boolIsPasswordRequired = true ) {

		if( true == $boolIsPasswordRequired ) {
			$objEntity = CEntities::fetchActiveEntityByUsernameByPassword( $this->getUsername(), $this->getPasswordEncrypted(), $objDatabase );
		} else {
			$objEntity = CEntities::fetchPreExistingEntityByUsername( $this->getUsername(), $objDatabase );
		}

		if( true == valObj( $objEntity, 'CEntity' ) ) {
			$this->m_boolIsLoggedIn = true;
			$this->m_intId = $objEntity->getId();
		} else {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', 'Invalid username or password.', 304 ) );
			return false;
		}

		return true;
	}

	public function verifyNewPasswordsMatch( $arrstrPasswords ) {
		if( false == valArr( $arrstrPasswords ) ) {
			trigger_error( 'Invalid Request: Missing password array - CEntityr', E_USER_ERROR );
		}

		$strPassword1 = $arrstrPasswords[1];
		$strPassword2 = $arrstrPasswords[2];

		if( 6 > strlen( $strPassword1 ) ) {
			$this->addErrorMsg( new CErrorMsg( 303, 'password', 'New password must be at least 6 characters long.' ) );
			return false;
		}

		if( trim( $strPassword1 ) != trim( $strPassword2 ) ) {
			$this->addErrorMsg( new CErrorMsg( 300, 'password', 'New password and re-entered new password do not match.' ) );
			return false;
		}

		return true;
	}

	public function sendCreateEntityPasswordLink( $strSecureBaseName, $objEmailDatabase ) {

		$strHtmlContent			= $this->buildHtmlCreateEntityPasswordLinkEmailContent( $strSecureBaseName );
		$strSubject				= 'ResidentInsure Password Reset Confirmation';

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PROFILE_UPDATE, $strSubject, $strHtmlContent, $strToEmailAddress = $this->getUsername() );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
			return $this->sendPsHtmlMimeEmail( $strSubject, $strHtmlContent );
		}

		return true;
	}

	public function buildHtmlCreateEntityPasswordLinkEmailContent( $strSecureBaseName ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_INSURANCE_PORTAL, false );
		$objCrypto = \Psi\Libraries\Cryptography\CCrypto::createService();

		$strCreateEntityPasswordLink = $strSecureBaseName . '?module=forgot_password&action=create_entity_password&entity[key]=' . $objCrypto->encryptUrl( $this->getId(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) . '&expire_date=' . $objCrypto->encryptUrl( date( 'Y-m-d' ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD );

		$objSmarty->assign( 'entity', 						$this );
		$objSmarty->assign( 'create_entity_password_link', 	$strCreateEntityPasswordLink );

		$objSmarty->assign( 'path_base_uri', 				CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'image_url',	                CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strHtmlRequestContent = $objSmarty->nestedFetch( PATH_INTERFACES_INSURANCE_PORTAL . 'common/forgot_password_email.tpl' );

		return $strHtmlRequestContent;
	}

	public function buildHtmlConfirmAccountEmailContent( $strSecureBaseName ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_INSURANCE_PORTAL, false );

		$strLoginLink = $strSecureBaseName . '?module=enroll&action=create_entity_password&entity[key]=' . \Psi\Libraries\Cryptography\CCrypto::createService()->encryptUrl( $this->getId(), CONFIG_SODIUM_KEY_ID );

		$objSmarty->assign( 'entity', 		$this );
		$objSmarty->assign( 'login_link', 	$strLoginLink );

		$objSmarty->assign( 'path_base_uri', 		    CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'SUPPORT_EMAIL_ADDRESS', 	CSystemEmail::SUPPORT_EMAIL_ADDRESS );
		$objSmarty->assign( 'image_url',	            CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strHtmlRequestContent = $objSmarty->nestedFetch( PATH_INTERFACES_INSURANCE_PORTAL . 'common/welcome.tpl' );

		return $strHtmlRequestContent;
	}

	public function checkIsSameAddress( $objInsurancePolicy ) {

		if( false == valObj( $objInsurancePolicy, 'CInsurancePolicy' ) ) {
			return false;
		}

		if( 0 != \Psi\CStringService::singleton()->strcasecmp( $this->getStreetLine1(), $objInsurancePolicy->getStreetLine1() ) ) {
			return false;
		}

		if( 0 != \Psi\CStringService::singleton()->strcasecmp( $this->getStreetLine2(), $objInsurancePolicy->getStreetLine2() ) ) {
			return false;
		}

		if( 0 != \Psi\CStringService::singleton()->strcasecmp( $this->getStreetLine3(), $objInsurancePolicy->getStreetLine3() ) ) {
			return false;
		}

		if( 0 != \Psi\CStringService::singleton()->strcasecmp( $this->getPostalCode(), $objInsurancePolicy->getPostalCode() ) ) {
			return false;
		}

		if( 0 != \Psi\CStringService::singleton()->strcasecmp( $this->getCity(), $objInsurancePolicy->getCity() ) ) {
			return false;
		}

		if( 0 != \Psi\CStringService::singleton()->strcasecmp( $this->getStateCode(), $objInsurancePolicy->getStateCode() ) ) {
			return false;
		}

		return true;
	}

	public function sendPsHtmlMimeEmail( $strSubject, $strHtmlContent, $strToEmailAddress = NULL ) {
		if( false == valStr( $strToEmailAddress ) ) {
			$strToEmailAddress = $this->getUsername();
		}

		$arrstrEmailAddress = CEmail::createEmailArrayFromString( $strToEmailAddress );

		$objMimeEmail = new CHtmlMimeMail();

		$objMimeEmail->setSubject( $strSubject );
		$objMimeEmail->setHtml( $strHtmlContent );
		$objMimeEmail->setFrom( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		return $objMimeEmail->send( $arrstrEmailAddress );
	}

}
?>