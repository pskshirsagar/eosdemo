<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CHandbooks
 * Do not add any new functions to this class.
 */

class CHandbooks extends CBaseHandbooks {

	public static function fetchAllHandbooks( $objAdminDatabase, $arrintHandbookPagination = array() ) {
		$strPagination = '';
		if( true == valArr( $arrintHandbookPagination ) ) {
			$intOffset = ( true == isset( $arrintHandbookPagination['page_no'] ) && true == isset( $arrintHandbookPagination['page_size'] ) ) ? $arrintHandbookPagination['page_size'] * ( $arrintHandbookPagination['page_no'] - 1 ) : 0;
			$intLimit  = ( int ) $arrintHandbookPagination['page_size'];
			$strPagination = ' OFFSET ' . $intOffset . ' LIMIT ' . $intLimit . ' ';
		}

		$strSql = ' SELECT
						*
					FROM
						handbooks
					ORDER BY created_on DESC' . $strPagination;

		return self::fetchHandbooks( $strSql, $objAdminDatabase );
	}
}
?>