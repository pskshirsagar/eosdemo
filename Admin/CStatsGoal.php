<?php

class CStatsGoal extends CBaseStatsGoal {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatGoalTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGoalValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>