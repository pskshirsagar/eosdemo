<?php

use Psi\Eos\Admin\CCommissionBatches;
use Psi\Eos\Admin\CCommissions;

class CCommissionBatch extends CBaseCommissionBatch {

	public function setDefaults() {

		$this->setBatchTotal( 0 );
		$this->setBatchDatetime( date( 'Y/m/d', mktime( 0, 0, 0, date( 'm' ), 1, date( 'Y' ) ) ) );

		return;
	}

	public function addBatchedCommissionAmount( $fltCommissionAmount ) {
		$this->m_fltBatchTotal += $fltCommissionAmount;
	}

	public function setNextId( $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		if( false == isset( $this->m_intId ) ) {
			$strSql = "SELECT nextval('commission_batches_id_seq'::text) AS id";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post commission batch record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrmixValues = $objDataset->fetchArray();

			$this->setId( $arrmixValues['id'] );

			$objDataset->cleanup();
		}
	}

	public function valBatchDatetime( $objDatabase ) {

		$boolIsValid = true;

		if( false == CValidation::validateDate( $this->getBatchDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'batch_datetime', 'Valid batch date is required.' ) );
		} else {
			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$strWher = ' WHERE to_char( batch_datetime, \'mm\' ) = \'' . date( 'm', strtotime( $this->getBatchDatetime() ) ) . '\'
						 AND  to_char( batch_datetime, \'yyyy\' ) = \'' . date( 'Y', strtotime( $this->getBatchDatetime() ) ) . '\'' . $strSqlCondition;
			$intCount = CCommissionBatches::createService()->fetchCommissionBatchCount( $strWher, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'batch_datetime', 'Commissions for this month were already posted.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valBatchDatetime( $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function fetchBatchedCommissionSummary( $objDatabase ) {

		return CCommissions::createService()->fetchBatchedCommissionSummaryByCommissionBatchId( $this->getId(), $objDatabase );
	}

	public function updateUnpaidCommissionsByCommissionRecipientStructures( $arrstrCommissionRecipientStructures, $objDatabase ) {
		return CCommissions::createService()->updateUnpaidCommissionsByCommissionBatchIdByBatchDatetimeByCommissionRecipientStructures( $this->getId(), $this->getBatchDatetime(), $arrstrCommissionRecipientStructures, $objDatabase );
	}

	public function generateComissionBatchSummaryByCommissionRecipientId( $intCommissionRecipientId = NULL, $intCid = NULL, $objDatabase ) {

		$strSql = '
			SELECT
				sub.commission_recipient_id,
				sub.commission_batch_id,
				sub.company_name,
				sub.cid,
				sub.last_contract_id,
				sub.charge_code_name,
				sub.charge_code_id,
				sub.commission_currency_code,
				sub.transaction_currency_code,
				sum( sub.commission_amount ) AS batch_total,
				sum( sub.transaction_amount ) AS transaction_total,
				count( sub.id ) AS commission_count,
				sum( sub.front_load_count ) AS front_load_count
			FROM (
				SELECT
					c.id,
					cl.id AS cid,
					c.commission_amount,
					c.commission_recipient_id,
					c.commission_batch_id,
					cl.company_name,
					t.transaction_amount,
					fl.front_load_count,
					sub2.id AS last_contract_id,
					cc.name AS charge_code_name,
					cc.id AS charge_code_id,
					c.currency_code AS commission_currency_code,
					t.currency_code AS transaction_currency_code
				FROM
					commissions c
					JOIN commission_recipients cr ON ( cr.id = c.commission_recipient_id )
					JOIN charge_codes cc ON ( c.charge_code_id = cc.id )
					JOIN clients cl ON ( cl.id = c.cid )
					LEFT JOIN transactions t ON ( t.id = c.transaction_id )
					LEFT JOIN (
						SELECT
							cid,
							charge_code_id,
							count( id ) AS front_load_count
						FROM
							commissions
						WHERE
							commission_batch_id = ' . ( int ) $this->getId() . '
				            ' . ( false == is_null( $intCommissionRecipientId ) ? ' AND commission_recipient_id = ' . ( int ) $intCommissionRecipientId : ' ' ) . '
				            ' . ( false == is_null( $intCid ) ? ' AND cid = ' . ( int ) $intCid : ' ' ) . '
							AND commission_front_load_id IS NOT NULL
						GROUP BY
							cid,
							charge_code_id
					) fl ON ( fl.cid = c.cid AND fl.charge_code_id = c.charge_code_id )
					LEFT JOIN (
						SELECT
							c.id,
							c.cid,
							c.sales_employee_id,
							rank() OVER ( PARTITION BY c.sales_employee_id, c.cid ORDER BY c.id DESC )
						FROM
							contracts c
						WHERE
							c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
					) AS sub2 ON ( sub2.cid = cl.id AND sub2.sales_employee_id = cr.employee_id AND sub2.rank = 1 )
					WHERE
						c.commission_batch_id = ' . ( int ) $this->getId() . '
			            ' . ( false == is_null( $intCommissionRecipientId ) ? ' AND c.commission_recipient_id = ' . ( int ) $intCommissionRecipientId : ' ' ) . '
			            ' . ( false == is_null( $intCid ) ? ' AND c.cid = ' . ( int ) $intCid : ' ' ) . '
						AND commission_payment_id IS NULL
				UNION
				SELECT
				    c.id,
				    0 AS cid,
				    c.commission_amount,
				    c.commission_recipient_id,
				    c.commission_batch_id,
				    \'Product Commissions\' AS company_name,
				    0 AS transaction_amount,
				    0 AS front_load_count,
				    NULL AS last_contract_id,
				    c.commission_memo AS charge_code_name,
					NULL AS charge_code_id,
					c.currency_code AS commission_currency_code,
					NULL AS transaction_currency_code
				FROM
				    commissions c
				    JOIN commission_recipients cr ON cr.id = c.commission_recipient_id
				    JOIN commission_rate_associations cra ON cra.id = c.commission_rate_association_id
				    JOIN commission_rates crt ON crt.id = cra.commission_rate_id AND crt.commission_structure_id = ' . CCommissionStructure::PRODUCT_COMMISSION . '
				WHERE
					c.commission_payment_id IS NULL
				    AND c.commission_batch_id = ' . ( int ) $this->getId() . '
				    ' . ( false == is_null( $intCommissionRecipientId ) ? ' AND c.commission_recipient_id = ' . ( int ) $intCommissionRecipientId : ' ' ) . '
				    AND c.deleted_on IS NULL
				    AND cr.deleted_on IS NULL
			) sub
			GROUP BY
				sub.commission_recipient_id,
				sub.commission_batch_id,
				sub.company_name,
				sub.charge_code_name,
				sub.cid,
				sub.charge_code_id,
				sub.last_contract_id,
				sub.commission_currency_code,
				sub.transaction_currency_code
			ORDER BY
				sub.company_name,
				sub.charge_code_name';

		return fetchData( $strSql, $objDatabase );
	}

	public function generateComissionBatchSummaryRowDetailByCommissionRecipientIdByCidByChargeCodeId( $intCommissionRecipientId, $intCid, $intChargeCodeId, $objPagination, $objDatabase ) {

		$intLimit		= ( int ) $objPagination->getPageSize();
		$intOffset		= ( 0 < $objPagination->getPageNo() ) ? $intLimit * ( $objPagination->getPageNo() - 1 ) : 0;

		$strSql = 'SELECT * FROM (
							 SELECT c.id,
							 		t.cid AS cid,
							 		t.account_id AS account_id,
									c.commission_front_load_id,
									t.id AS transaction_id,
									c.reversed_commission_id,
									c.commission_datetime,
									c.commission_amount,
									c.is_manual_adjustment,
									t.transaction_amount,
									t.transaction_datetime,
									t.cost_amount,
									cs.name as structure_name,
									a.account_name,
									CASE WHEN cc.cost_percent IS NULL THEN 0 ELSE cc.cost_percent END AS cost_percent,
									c.commission_memo,
									t.memo
							FROM commissions c
									JOIN transactions t ON ( t.id = c.transaction_id )
									LEFT JOIN charge_codes cc ON ( cc.id = c.charge_code_id )
									LEFT JOIN accounts a ON ( a.id = t.account_id )
									LEFT JOIN commission_structures cs ON ( c.commission_structure_id = cs.id )
							WHERE
									c.commission_batch_id = ' . ( int ) $this->getId() . '
									AND c.commission_recipient_id = ' . ( int ) $intCommissionRecipientId . '
									AND c.cid = ' . ( int ) $intCid . '
									AND c.charge_code_id = ' . ( int ) $intChargeCodeId . '
									AND c.is_manual_adjustment = 0

							UNION

							SELECT c.id,
									c.cid AS cid,
									c.account_id AS account_id,
									c.commission_front_load_id,
									NULL AS transaction_id,
									c.reversed_commission_id,
									c.commission_datetime,
									c.commission_amount,
									c.is_manual_adjustment,
									NULL AS transaction_amount,
									NULL AS transaction_datetime,
									NULL AS cost_amount,
									NULL AS structure_name,
									a.account_name,
									CASE WHEN cc.cost_percent IS NULL THEN 0 ELSE cc.cost_percent END AS cost_percent,
									c.commission_memo,
									NULL AS memo
							FROM commissions c
									JOIN charge_codes cc ON ( cc.id = c.charge_code_id )
									LEFT JOIN accounts a ON ( a.id = c.account_id )
							WHERE
									c.commission_batch_id = ' . ( int ) $this->getId() . '
									AND c.commission_recipient_id = ' . ( int ) $intCommissionRecipientId . '
									AND c.cid = ' . ( int ) $intCid . '
									AND c.charge_code_id = ' . ( int ) $intChargeCodeId . '
									AND c.is_manual_adjustment = 1 ) as sub_query1
					ORDER BY
						commission_datetime DESC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit . ';';

		return fetchData( $strSql, $objDatabase );
	}

	public function generateComissionBatchSummaryRowDetailCountByCommissionRecipientIdByCidByChargeCodeId( $intCommissionRecipientId, $intCid, $intChargeCodeId, $objDatabase ) {

		$strSql = ' SELECT ( ( SELECT count( c.id )
							FROM commissions c
									JOIN transactions t ON ( t.id = c.transaction_id )
							WHERE
									c.commission_batch_id = ' . ( int ) $this->getId() . '
									AND c.commission_recipient_id = ' . ( int ) $intCommissionRecipientId . '
									AND c.cid = ' . ( int ) $intCid . '
									AND c.charge_code_id = ' . ( int ) $intChargeCodeId . '
									AND c.is_manual_adjustment = 0 ) +

							( SELECT count(c.id)
									FROM commissions c
									WHERE
										c.commission_batch_id = ' . ( int ) $this->getId() . '
										AND c.commission_recipient_id = ' . ( int ) $intCommissionRecipientId . '
										AND c.cid = ' . ( int ) $intCid . '
										AND c.charge_code_id = ' . ( int ) $intChargeCodeId . '
										AND c.is_manual_adjustment = 1 ) ) AS count';

		$arrstrCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrstrCount[0]['count'] ) ) ? $arrstrCount[0]['count'] : 0;
	}

	public function fetchBatchTotal( $objDatabase ) {

		$strSql = 'SELECT
						sum(commission_amount) as batch_total
					FROM
						commissions
					WHERE
						commission_batch_id = ' . ( int ) $this->getId() . '
						AND commission_payment_id IS NULL
						AND deleted_on IS NULL';

		$arrstrResult = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrstrResult[0]['batch_total'] ) ) {
			$this->m_fltBatchTotal = $arrstrResult[0]['batch_total'];
		} else {
			$this->m_fltBatchTotal = 0;
		}
	}

	public function generateComissionBatchDetailByCommissionRecipientId( $intCommissionRecipientId, $objDatabase ) {

		$strSql = 'SELECT * FROM (
							 SELECT c.id,
									c.commission_front_load_id,
									c.commission_datetime,
									c.commission_amount,
									c.is_manual_adjustment,
									t.transaction_amount,
									t.transaction_datetime,
									t.cost_amount,
									cs.name as structure_name,
									mc.company_name as company_name,
									cc.name as charge_code_name,
									a.account_name,
									CASE WHEN cc.cost_percent IS NULL THEN 0 ELSE cc.cost_percent END AS cost_percent,
									c.commission_memo,
									t.memo,
									string_agg( DISTINCT cp.contract_id::VARCHAR, \',\' ) AS contract_id,
									p.property_name
							FROM commissions c
									JOIN transactions t ON ( t.id = c.transaction_id )
									LEFT JOIN clients mc ON ( mc.id = c.cid )
									LEFT JOIN contract_properties cp ON ( cp.id = t.contract_property_id AND cp.cid = t.cid  AND cp.renewal_contract_property_id IS NULL AND cp.is_last_contract_record = TRUE )
									LEFT JOIN properties p ON ( t.property_id = p.id AND t.cid = p.cid )
									LEFT JOIN charge_codes cc ON ( cc.id = c.charge_code_id )
									LEFT JOIN accounts a ON ( a.id = t.account_id )
									LEFT JOIN commission_structures cs ON ( c.commission_structure_id = cs.id )
							WHERE
									c.commission_batch_id = ' . ( int ) $this->getId() . '
									AND c.commission_recipient_id = ' . ( int ) $intCommissionRecipientId . '
									AND c.is_manual_adjustment = 0
							GROUP BY
									c.id,
									c.commission_front_load_id,
									c.commission_datetime,
									c.commission_amount,
									c.is_manual_adjustment,
									t.transaction_amount,
									t.transaction_datetime,
									t.cost_amount,
									cs.name,
									mc.company_name,
									cc.name,
									a.account_name,
									cc.cost_percent,
									c.commission_memo,
									t.memo,
									p.property_name
							UNION

							SELECT c.id,
									c.commission_front_load_id,
									c.commission_datetime,
									c.commission_amount,
									c.is_manual_adjustment,
									NULL AS transaction_amount,
									NULL AS transaction_datetime,
									NULL AS cost_amount,
									NULL AS structure_name,
									mc.company_name as company_name,
									a.account_name,
									cc.name as charge_code_name,
									CASE WHEN cc.cost_percent IS NULL THEN 0 ELSE cc.cost_percent END AS cost_percent,
									c.commission_memo,
									NULL AS memo,
									NULL AS contract_id,
									NULL AS property_name
								FROM
									commissions c
									JOIN charge_codes cc ON ( cc.id = c.charge_code_id )
									LEFT JOIN clients mc ON ( mc.id = c.cid )
									LEFT JOIN accounts a ON ( a.id = c.account_id )
								WHERE
									c.commission_batch_id = ' . ( int ) $this->getId() . '
									AND c.commission_recipient_id = ' . ( int ) $intCommissionRecipientId . '
									AND c.is_manual_adjustment = 1 ) as sub_query1
					ORDER BY
						commission_datetime DESC;';

		return fetchData( $strSql, $objDatabase );
	}

}
?>