<?php

class CReportChangeHistory extends CBaseReportChangeHistory {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportChangeDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastPostedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>