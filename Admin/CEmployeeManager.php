<?php

class CEmployeeManager extends CBaseEmployeeManager {

	protected $m_strManagerFullName;
	protected $m_strMemberFullName;
	protected $m_strDesignationName;

	/**
	 * Get Functions
	 */

	public function getManagerFullName() {
		return $this->m_strManagerFullName;
	}

	public function getMemberFullName() {
		return $this->m_strMemberFullName;
	}

	/**
	 * Set Functions
	 */

	public function setManagerFullName( $strManagerFullName ) {
		$this->m_strManagerFullName = $strManagerFullName;
	}

	public function setMemberFullName( $strMemberFullName ) {
		$this->m_strMemberFullName = $strMemberFullName;
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>