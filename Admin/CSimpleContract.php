<?php

class CSimpleContract extends CBaseSimpleContract {

	protected $m_intDaysLeft;

	/**
	 * Get Functions
	 */

	public function getDaysLeft() {
		return $this->m_intDaysLeft;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['days_left'] ) && 0 < strlen( trim( $arrstrValues['days_left'] ) ) ) {
			$this->setDaysLeft( $arrstrValues['days_left'] );
		}
	}

	/**
	 * Set Functions
	 */

	public function setDaysLeft( $intDaysLeft ) {
		$this->m_intDaysLeft = $intDaysLeft;
	}

	/**
	 * Other Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Ps Product id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valExpireOn( $boolIsRequired = false ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && false == valStr( $this->getExpireOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expire_on', 'Expire on is required.' ) );
		}

		if( true == valStr( $this->getExpireOn() ) ) {
			$mixValidatedExpireOn = CValidation::checkDateFormat( $this->getExpireOn(), $boolFormat = true );
			if( false === $mixValidatedExpireOn ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expire_on', 'Expire on must be in mm/dd/yyyy format.' ) );
			} else {
				$this->setExpireOn( date( 'm/d/Y', strtotime( $mixValidatedExpireOn ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valExpireOn( $boolIsRequired = false );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default break
				break;
		}

		return $boolIsValid;
	}

}
?>