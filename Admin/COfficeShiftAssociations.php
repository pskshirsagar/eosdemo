<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COfficeShiftAssociations
 * Do not add any new functions to this class.
 */

class COfficeShiftAssociations extends CBaseOfficeShiftAssociations {

	public static function fetchOfficeShiftAssociationsByFilter( $arrmixFilter, $objDatabase, $boolFetchAllShift = false, $boolReturnObjects = false ) {
		if( false == valArr( $arrmixFilter['department_ids'] ) ) return false;

		$strWhereClause = '';
		if( true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strWhereClause .= ' AND t.hr_representative_employee_id = ' . ( int ) $arrmixFilter['hr_representative_employee_id'];
		}

		if( true == valArr( $arrmixFilter['office_shift_ids'] ) && false == valStr( $arrmixFilter['search_filter'] ) && false == $boolFetchAllShift ) {
			$strWhereClause .= ' AND ( osa.office_shift_id IN ( ' . implode( ',', $arrmixFilter['office_shift_ids'] ) . ' ) OR osa.office_shift_id IS NULL )';
		}

		$strJoinCondition = ( ( false == $boolReturnObjects ) ? 'LEFT ' : ' ' ) . 'JOIN office_shift_associations osa ON ( ' . ( ( true == $arrmixFilter['is_employee_wise'] ) ? 'e.id = osa.employee_id' : 't.id = osa.team_id' ) . ' )';

		if( true == $arrmixFilter['is_employee_wise'] ) {
			$strSelectClause 	= ( ( false == $boolReturnObjects ) ? ' DISTINCT ON ( e.id ) ' : ' ' ) . 'e.id as employee_id, e.preferred_name as employee_name, e1.preferred_name as manager_name, ';
			$strWhereClause 	.= ( true == valStr( $arrmixFilter['search_filter'] ) ) ? ' AND lower( e.preferred_name ) like \'%' . \Psi\CStringService::singleton()->strtolower( trim( $arrmixFilter['search_filter'] ) ) . '%\'' : '';
			$strJoinCondition 	.= 'JOIN employees e1 ON ( e.reporting_manager_id = e1.id )';
		} else {
			$strSelectClause 	= ( ( false == $boolReturnObjects ) ? ' DISTINCT ON ( t.id ) ' : ' ' ) . 't.id as team_id, t.name as team_name, e2.preferred_name as employee_name, e3.preferred_name AS manager_name, ';
			$strWhereClause 	.= ( true == valStr( $arrmixFilter['search_filter'] ) ) ? ' AND lower( t.name ) like \'%' . \Psi\CStringService::singleton()->strtolower( trim( $arrmixFilter['search_filter'] ) ) . '%\'' : '';
			$strJoinCondition 	.= ' JOIN employees e2 ON ( e2.id = te.employee_id AND e2.department_id IN ( ' . implode( ',', $arrmixFilter['department_ids'] ) . ' ) )
									JOIN employees e3 ON ( e3.id = e.reporting_manager_id )';
		}

		if( true == $arrmixFilter['check_deleted_by'] ) {
			$strWhereClause .= ' AND osa.deleted_by IS NULL';
		}

		if( true == $boolReturnObjects ) {
			$strSelectClause .= ' osa.id, osa.deleted_by, osa.deleted_on,';
		}

		$strSql = 'SELECT
						' . $strSelectClause . '
						osa.office_shift_id,
						osa.is_compensatory_off,
						osa.is_paid_holiday,
						osa.deleted_by
					FROM
						employees e
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN teams t ON ( te.team_id = t.id )
						' . $strJoinCondition . '
					WHERE
						ea.country_code = \'' . CCountry::CODE_INDIA . '\'
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.department_id IN ( ' . implode( ',', $arrmixFilter['department_ids'] ) . ' )
						' . $strWhereClause;

		return ( false == $boolReturnObjects ) ? fetchData( $strSql, $objDatabase ) : self::fetchOfficeShiftAssociations( $strSql, $objDatabase );
	}

	public static function fetchOfficeShiftAssociationsByDepartmentIdsByTeamIds( $objDatabase, $arrintDepartmentIds = NULL, $arrintTeamIds = NULL, $intOfficeShiftId = NULL, $intEmployeeId = NULL, $boolFromLams = false, $boolCompensatoryOff = false, $boolDeleted = false ) {
		$strWhereClause	 = '1 = 1';

		if( false == $boolDeleted ) {
			$strWhereClause .= ' AND osa.deleted_by IS NULL';
		}

		if( true == valArr( $arrintTeamIds ) && true == is_numeric( $intEmployeeId ) ) {
			$strWhereClause .= ' AND ( te.team_id IN ( ' . implode( ',', $arrintTeamIds ) . ' ) OR te.Employee_id = ' . ( int ) $intEmployeeId . ')';
		} elseif( true == valArr( $arrintTeamIds ) && false == is_numeric( $intEmployeeId ) ) {
			$strWhereClause .= ' AND te.team_id IN ( ' . implode( ',', $arrintTeamIds ) . ' )';
		} elseif( true == is_numeric( $intEmployeeId ) ) {
			$strWhereClause .= ' AND te.employee_id = ' . ( int ) $intEmployeeId;
		}

		if( true == valArr( $arrintDepartmentIds ) ) {
			$strJoinClause	= ' ';
			$strWhereClause .= ' AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )';
		}

		if( true == is_numeric( $intOfficeShiftId ) ) {
			$strWhereClause .= ' AND osa.office_shift_id = ' . ( int ) $intOfficeShiftId;
		}

		if( true == $boolCompensatoryOff ) {
			$strWhereClause .= ' AND osa.is_compensatory_off IS TRUE';
		}

		if( true == $boolFromLams ) {
			$strLimitClause = ' LIMIT 1';
		}

		$strSql = 'SELECT
						DISTINCT osa.id,
						osa.employee_id,
						osa.team_id,
						osa.office_shift_id,
						osa.is_compensatory_off,
						osa.is_paid_holiday,
						osa.deleted_on::DATE,
						osa.created_on::DATE,
						e.department_id
					FROM
						office_shift_associations osa
						JOIN team_employees te ON ( osa.team_id = te.team_id OR osa.employee_id = te.employee_id )
    					JOIN teams t ON ( te.team_id = t.id )
    					JOIN employees e ON ( e.id = te.employee_id )
					WHERE
						' . $strWhereClause . '
					GROUP BY
						osa.id,
						osa.team_id,
						osa.created_on,
						e.department_id
					ORDER BY
						osa.created_on::DATE'
					. $strLimitClause;

		return fetchData( $strSql, $objDatabase );
	}

}
?>