<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskTypes
 * Do not add any new functions to this class.
 */

class CTaskTypes extends CBaseTaskTypes {

	public static function fetchAllTaskTypes( $objDatabase, $boolFromTaskType = false ) {

		$strJoin = ( true == $boolFromTaskType ) ? ' LEFT JOIN task_default_assignments tda on tda.task_type_id = tt.id AND ( tda.deleted_by IS NULL AND tda.deleted_on IS NULL )' : '';
		$strSelect = ( true == $boolFromTaskType ) ? ' ,tda.employee_id ' : '';

		$strSql = 'SELECT tt.*' . $strSelect . ' FROM task_types tt' . $strJoin . ' WHERE tt.id != ' . CTaskType::ENTRATA_TEST_CASE . ' ORDER BY order_num';

		return self::fetchTaskTypes( $strSql, $objDatabase );
	}

	public static function fetchEnabledTaskTypes( $objDatabase, $boolFromTaskType = false ) {

		$strJoin = ( true == $boolFromTaskType ) ? ' LEFT JOIN task_default_assignments tda on tda.task_type_id = tt.id AND ( tda.deleted_by IS NULL AND tda.deleted_on IS NULL )' : '';
		$strSelect = ( true == $boolFromTaskType ) ? ' ,tda.employee_id ' : '';

		$strSql = 'SELECT tt.*' . $strSelect . ' FROM task_types tt ' . $strJoin . ' WHERE disabled_by IS NULL AND tt.id != ' . CTaskType::ENTRATA_TEST_CASE . ' ORDER BY order_num';

		return self::fetchTaskTypes( $strSql, $objDatabase );
	}

	public static function fetchAllParentTaskTypes( $objDatabase ) {

		$strSql = 'SELECT * FROM task_types WHERE parent_task_type_id IS NULL AND id != ' . CTaskType::ENTRATA_TEST_CASE . ' ORDER BY ' . $objDatabase->getCollateSort( 'name' );

		return self::fetchTaskTypes( $strSql, $objDatabase );
	}

	public static function fetchEnabledParentTaskTypes( $objDatabase, $boolIsIncludeSeleniumTaskType = false, $boolIsExcludeToDoType = true, $boolIsExcludeOutageTaskType = false, $boolExcludeSchemaChangeTaskType = false, $boolIncludeHelpDeskType = false, $boolIncludeDefectTaskType = false ) {

		$strTaskType = ( true == $boolExcludeSchemaChangeTaskType ) ? CTaskType::SUSPICION . ', ' . CTaskType::SCHEMA_CHANGE : CTaskType::SUSPICION;
		$strTaskType .= ', ' . CTaskType::ENTRATA_TEST_CASE;
		$strUnion = '';

		if( $boolIncludeHelpDeskType ) {
			$strUnion = ' UNION SELECT * FROM task_types WHERE id = ' . \CTaskType::HELPDESK;
		}

		if( $boolIncludeDefectTaskType ) {
			$strUnion .= ' UNION SELECT * FROM task_types WHERE id = ' . \CTaskType::DEFECT;
		}

		$strSql = ' SELECT
						*
					FROM
						task_types
					WHERE
						' . ( ( true == $boolIsExcludeToDoType ) ? ' id != ' . CTaskType::TO_DO . ' AND ' : '' ) . '
						' . ( ( true == $boolIsExcludeOutageTaskType ) ? ' id != ' . CTaskType::OUTAGE . ' AND ' : '' ) . '
						id NOT IN (' . $strTaskType . ' ) AND
						disabled_by IS NULL
						AND ( parent_task_type_id IS NULL ' . ( ( true == $boolIsIncludeSeleniumTaskType ) ? ' OR id IN( ' . CTaskType::TEST_CASES . ', ' . CTaskType::AUTOMATION . ', ' . CTaskType::RP_FEEDBACK . ' )' : '' ) . '
								)
					' . $strUnion . '
					ORDER BY ' . $objDatabase->getCollateSort( 'name' );

		return parent::fetchTaskTypes( $strSql, $objDatabase );
	}

	public static function fetchTaskTypeById( $intTaskTypeId, $objDatabase ) {

		$strSql = 'SELECT * FROM task_types WHERE id = ' . ( int ) $intTaskTypeId;

		return self::fetchTaskType( $strSql, $objDatabase );
	}

	public static function fetchTaskTypeByIds( $arrintTaskTypeIds, $objDatabase ) {

		if( false == valArr( $arrintTaskTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM task_types WHERE id IN ( ' . implode( ',', $arrintTaskTypeIds ) . ' ) ORDER BY ' . $objDatabase->getCollateSort( 'name' );

		return self::fetchTaskTypes( $strSql, $objDatabase );
	}

	public static function fetchTaskTypesByParentTaskTypeId( $intParentTaskTypeId, $objDatabase ) {

		$strSql = 'SELECT * FROM task_types WHERE disabled_by IS NULL AND parent_task_type_id = ' . ( int ) $intParentTaskTypeId . ' ORDER BY id';

		return self::fetchTaskTypes( $strSql, $objDatabase );
	}

	public static function fetchTaskTypeByTaskId( $intTaskId, $objDatabase ) {

		$strSql = '	SELECT
						tt.*
					FROM
						task_types tt
						JOIN tasks t ON ( t.task_type_id = tt.id )
					WHERE
						t.id = ' . ( int ) $intTaskId;

		return self::fetchTaskType( $strSql, $objDatabase );
	}

	public static function fetchSubTaskTypesByTaskTypeIds( $arrintTaskTypeIds, $arrintTaskTypeForSubTaskTypeIds = NULL, $objDatabase ) {
		if( false == valArr( $arrintTaskTypeIds ) ) return NULL;
		$strWhereJoin = '';
		if( true == valArr( $arrintTaskTypeForSubTaskTypeIds ) ) {
			$strWhereJoin .= 'AND t.id IN ( ' . sqlIntImplode( $arrintTaskTypeForSubTaskTypeIds ) . ' )';
		}
		$strSql = ' select t.id as parent_task_type_id, t.name as parent_name,tt.id as child_task_type_id, tt.name as child_name from task_types t LEFT JOIN task_types tt ON ( tt.parent_task_type_id = t.id AND tt.disabled_by IS NULL ' . $strWhereJoin . ' ) where t.id in ( ' . implode( ',', $arrintTaskTypeIds ) . ') ORDER BY ' . $objDatabase->getCollateSort( 't.name' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompetingTaskTypeCountByNameById( $strName, $intId, $objDatabase ) {
		if( false == is_null( $intId ) && true == is_numeric( $intId ) ) {
			return self::fetchTaskTypeCount( 'WHERE name = \'' . addslashes( trim( $strName ) ) . '\' AND id!=' . ( int ) $intId, $objDatabase );
		} else {
			return self::fetchTaskTypeCount( 'WHERE name = \'' . addslashes( trim( $strName ) ) . '\' ', $objDatabase );
		}
	}

	// This function nests sub task types under their parent tasks.
	// The function is located here because it is needed in more than one module.

	public static function reorderTaskTypes( $arrobjTaskTypes ) {

		if( true == valArr( $arrobjTaskTypes ) ) {
			foreach( $arrobjTaskTypes as $objTaskType ) {

				if( true == is_numeric( $objTaskType->getParentTaskTypeId() ) ) {
					if( true == isset( $arrobjTaskTypes[$objTaskType->getParentTaskTypeId()] ) ) {
						$arrobjTaskTypes[$objTaskType->getParentTaskTypeId()]->addTaskType( $objTaskType );
					}

					unset( $arrobjTaskTypes[$objTaskType->getId()] );
				}
			}
		}

		return $arrobjTaskTypes;
	}

	public static function fetchConflictingSupportTaskTypeCount( $strName, $intId, $objAdminDatabase ) {

		$strWhereSql = ' WHERE
							is_support = 1
						AND
							name = \'' . addslashes( $strName ) . '\'
						AND
							id <> ' . ( int ) $intId;

		return self::fetchTaskTypeCount( $strWhereSql, $objAdminDatabase );
	}

	public static function fetchOpenTaskTypesByCidOrPsLeadIdByTaskTypeIds( $objPsLead, $arrintTaskTypeIds, $intUserId, $boolShowCompletedTasks, $objDatabase ) {

		$strSql = 'SELECT
						tt.id,
						tt.name AS task_type_name,
						count( DISTINCT( CASE WHEN (';

		if( false == is_null( $objPsLead ) && false == is_null( $objPsLead->getCid() ) ) {

			$strSql .= ' tc.cid = ' . $objPsLead->getCid() . ' OR ';

			$strSql .= ' t.ps_lead_id = ' . $objPsLead->getId() . ' ) THEN t.id ELSE NULL END ) ) as task_count
						 FROM
							task_types tt
							LEFT JOIN tasks t ON ( tt.id = t.task_type_id AND t.deleted_on IS NULL AND t.deleted_by IS NULL AND t.task_type_id IN ( ' . implode( ',', $arrintTaskTypeIds ) . ' )';

			if( false == is_null( $objPsLead ) && false == is_null( $objPsLead->getCid() ) ) {
				$strSql .= ' AND (
										CASE
											WHEN ( t.task_type_id = ' . CTaskType::TO_DO . ' ) THEN t.user_id = ' . ( int ) $intUserId . ' ELSE 1 = 1
										END
										)';
			}

			if( true == $boolShowCompletedTasks ) {
				$strSql .= ' AND ( CASE
											WHEN tt.id = ' . CTaskType::TO_DO . ' THEN t.task_status_id != ' . CTaskStatus::UNUSED . '
										ELSE t.task_status_id NOT IN ( ' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' )
										END )
									 )';
			} else {
				$strSql .= ' AND t.task_status_id NOT IN ( ' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' ) )';
			}

			$strSql .= ' LEFT JOIN task_companies tc ON ( t.id = tc.task_id AND tc.task_note_id IS NULL )';
		} else {

			$strSql .= ' t.ps_lead_id = ' . $objPsLead->getId() . ' ) THEN t.id ELSE NULL END ) ) as task_count
						FROM
							task_types tt
							LEFT JOIN tasks t ON ( tt.id = t.task_type_id AND t.deleted_on IS NULL AND t.deleted_by IS NULL AND t.task_type_id IN ( ' . implode( ',', $arrintTaskTypeIds ) . ' ) AND ( t.user_id = ' . ( int ) $intUserId . ' OR t.task_type_id <> ' . CTaskType::TO_DO . ')';

			$strSql .= ( true == $boolShowCompletedTasks ) ? ' AND t.task_status_id IN ( ' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' ) )' : ' AND t.task_status_id NOT IN ( ' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' ) )';

		}

		$strSql .= 'WHERE
						tt.id IN ( ' . implode( ',', $arrintTaskTypeIds ) . ' )';

		$strSql .= 'GROUP BY
							tt.id,
							tt.name
					ORDER BY
							' . $objDatabase->getCollateSort( 'tt.name' ) . ' DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTaskTypesCountByDepartmentIdByUserIdByEmployeeIdByTaskRelationShipId( $intDepartmentId, $intUserId, $intEmployeeId, $intTaskRelationshipTypeId, $arrintReleaseIds, $arrintTeamEmployeesUserIds, $arrstrToDoFilter, $objDatabase, $arrintNextReleaseIds = NULL ) {

		$strSubSql = ' SELECT
							DISTINCT t.id,
							t.task_type_id,
							ttd.order_num
						FROM
							task_type_departments ttd
							JOIN task_types tt ON ttd.task_type_id = tt.id AND tt.id NOT IN ( ' . CTaskType::HELPDESK . ', ' . CTaskType::RP_FEEDBACK . ', ' . CTaskType::SCHEMA_CHANGE . ', ' . CTaskType::ENTRATA_TEST_CASE . ' )
							JOIN tasks t ON t.deleted_by IS NULL AND tt.id = t.task_type_id AND t.task_status_id NOT IN ( ' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ', ' . CTaskStatus::CLOSED . ', ' . CTaskStatus::REJECT . ' ) ';

		$strFromClause 								= '';
		$strWhereClauseCondition 					= ' WHERE ttd.department_id = ' . ( int ) $intDepartmentId;
		$strWhereClauseConditionTaskReleaseIdIsNull = '';
		$strWhereClauseConditionForToDo				= '';
		$strFromClauseForToDo						= '';
		$strSubSqlForToDo							= '';
		$strWhereClauseConditionExceptToDo			= '';

		$boolIsReleaseFilterSet = false;
		if( true == valArr( $arrintReleaseIds ) ) {
			$boolIsReleaseFilterSet = true;
		}

		if( true == $boolIsReleaseFilterSet || CTaskRelationshipType::MY_TEAM == $intTaskRelationshipTypeId ) {

			if( true == $boolIsReleaseFilterSet ) {
				$intNullKey = array_search( 'NULL', $arrintReleaseIds );

				if( false !== $intNullKey ) {
					$strWhereClauseConditionTaskReleaseIdIsNull = ' OR t.task_release_id IS NULL ';
				}
			}

			if( ( CTaskRelationshipType::MY_TEAM == $intTaskRelationshipTypeId || true == valArr( $arrstrToDoFilter ) ) && CUser::ID_DBA != $intUserId && false == in_array( $intUserId, CUser::$c_arrintHelpdeskUsers ) ) {

				$strWhereClauseConditionExceptToDo .= ( true == $boolIsReleaseFilterSet ) ? ' AND t.task_type_id <> ' . CTaskType::TO_DO . ' AND ' : ' AND t.task_type_id <> ' . CTaskType::TO_DO;
				$strWhereClauseConditionExceptToDo .= ( true == $boolIsReleaseFilterSet ? ' ( ( t.task_release_id IN (' . implode( ',', $arrintReleaseIds ) . ' ) OR t.task_standard_release_id IN (' . implode( ',', $arrintReleaseIds ) . ' ) ) ' : '' );
				$strWhereClauseConditionExceptToDo .= ( true == $boolIsReleaseFilterSet ) ? $strWhereClauseConditionTaskReleaseIdIsNull . ' ) ' : $strWhereClauseConditionTaskReleaseIdIsNull;
			} else {
				$strWhereClauseConditionExceptToDo .= ' AND ( t.task_type_id= ' . CTaskType::TO_DO;
				if( true == valArr( $arrintNextReleaseIds ) ) {
					$strWhereClauseConditionExceptToDo .= ( true == $boolIsReleaseFilterSet ? ' OR ( CASE
                                    WHEN t.task_type_id = ' . CTaskType::TEST_CASES . ' THEN  ( t.task_release_id IN (' . implode( ',', $arrintNextReleaseIds ) . ') OR t.task_standard_release_id IN (' . implode( ',', $arrintNextReleaseIds ) . ') ) ELSE ( t.task_release_id IN (' . implode( ',', $arrintReleaseIds ) . ') OR t.task_standard_release_id IN (' . implode( ',', $arrintReleaseIds ) . ') ) END ) ' : '' );
				} else {
					$strWhereClauseConditionExceptToDo .= ( true == $boolIsReleaseFilterSet ? ' OR ( t.task_release_id IN (' . implode( ',', $arrintReleaseIds ) . ') OR t.task_standard_release_id IN (' . implode( ',', $arrintReleaseIds ) . ') ) ' : '' );
				}
				$strWhereClauseConditionExceptToDo .= $strWhereClauseConditionTaskReleaseIdIsNull . ' ) ';

			}

			$strWhereClauseConditionForToDo .= ' AND t.task_type_id = ' . CTaskType::TO_DO;

		}

		if( true == valArr( $arrstrToDoFilter ) ) {
			if( ( true == array_key_exists( 'last_days_filter', $arrstrToDoFilter ) && false == is_null( $arrstrToDoFilter['last_days_filter'] ) ) && 0 != $arrstrToDoFilter['last_days_filter'] ) {
				$strWhereClauseConditionForToDo .= ' AND ( DATE(t.created_on) >= ( DATE( \'' . date( 'Y-m-d' ) . '\') - INTERVAL\'' . $arrstrToDoFilter['last_days_filter'] . 'day\' ) AND DATE( t.created_on ) <= DATE(\'' . date( 'Y-m-d' ) . '\') )';
			}

			if( ( true == array_key_exists( 'due_days_filter', $arrstrToDoFilter ) && false == is_null( $arrstrToDoFilter['due_days_filter'] ) ) && 0 != $arrstrToDoFilter['due_days_filter'] ) {
				$strWhereClauseConditionForToDo .= ' AND ( DATE(t.due_date) <= ( DATE( \'' . date( 'Y-m-d' ) . '\') + INTERVAL\'' . $arrstrToDoFilter['due_days_filter'] . 'day\' ) AND DATE(t.due_date) >= DATE(\'' . date( 'Y-m-d' ) . '\') )';
			}

			if( ( true == array_key_exists( 'due_days_filter', $arrstrToDoFilter ) && false == is_null( $arrstrToDoFilter['due_days_filter'] ) ) && 0 != $arrstrToDoFilter['due_days_filter'] ) {
				$strWhereClauseConditionForToDo .= ' AND ( DATE(t.due_date) <= ( DATE( \'' . date( 'Y-m-d' ) . '\') + INTERVAL\'' . $arrstrToDoFilter['due_days_filter'] . 'day\' ) AND DATE(t.due_date) >= DATE(\'' . date( 'Y-m-d' ) . '\') )';
			}

		}

		if( CUser::ID_DBA == $intUserId || true == in_array( $intUserId, CUser::$c_arrintHelpdeskUsers ) ) {
			$strWhereClauseCondition .= ' AND t.user_id = ' . ( int ) $intUserId;

		} else {

			switch( $intTaskRelationshipTypeId ) {

				case CTaskRelationshipType::CREATED_BY_ME:
					$strWhereClauseCondition .= ' AND t.created_by = ' . ( int ) $intUserId;
					break;

				case CTaskRelationshipType::I_AM_STAKEHOLDER:
					$strWhereClauseCondition .= ' AND t.task_type_id != (' . CTaskType::TO_DO . ') AND tw.user_id = ' . ( int ) $intUserId;
					$strFromClause .= ' LEFT JOIN task_watchlists tw ON ( t.id = tw.task_id )';
					break;

				case CTaskRelationshipType::MY_TEAM:
					if( true == valArr( $arrintTeamEmployeesUserIds ) ) {
						$strWhereClauseCondition .= ' AND t.user_id IN (' . implode( ',', $arrintTeamEmployeesUserIds ) . ')';
						$strFromClause .= ' LEFT JOIN task_watchlists tw ON ( t.id = tw.task_id )';
						$strFromClauseForToDo .= ' JOIN survey_tasks st ON ( t.id = st.task_id ) ';
					}
					break;

				case CTaskRelationshipType::I_AM_DEVELOPER:
					$strWhereClauseCondition	.= ' AND t.task_type_id != (' . CTaskType::TO_DO . ')';
					$strFromClause 				.= ' INNER JOIN task_details td ON ( t.id = td.task_id AND td.developer_employee_id = ' . ( int ) $intEmployeeId . ' ) ';
					break;

				case CTaskRelationshipType::I_AM_QA:
					$strWhereClauseCondition	.= ' AND t.task_type_id != (' . CTaskType::TO_DO . ')';
					$strFromClause				.= ' INNER JOIN task_details td ON ( t.id = td.task_id AND td.qa_employee_id = ' . ( int ) $intEmployeeId . ' ) ';
					break;

				case CTaskRelationshipType::I_AM_DESIGNER:
					$strWhereClauseCondition	.= ' AND t.task_type_id != (' . CTaskType::TO_DO . ')';
					$strFromClause				.= ' INNER JOIN task_details td ON ( t.id = td.task_id AND td.developer_employee_id = ' . ( int ) $intEmployeeId . ' ) ';
					break;

				case CTaskRelationshipType::ASSIGNED_TO_ME:
				default:
					$strWhereClauseCondition .= ' AND t.user_id = ' . ( int ) $intUserId;
					break;
			}
		}

		$strSubSql .= 'LEFT JOIN task_references tr on ( t.id = tr.task_id AND tr.deleted_by IS NULL )
				LEFT JOIN employee_assessments as ea ON ( ea.id = tr.reference_number )';
		$strWhereClauseCondition .= ' AND ( ( ea.completed_on IS NULL AND tr.reference_number IS NULL ) OR ( ea.completed_on IS NULL AND tr.reference_number IS NOT NULL AND tr.task_reference_type_id !=' . CTaskReferenceType::REVIEW_GOALS . ' ) OR ( ea.completed_on IS NOT NULL AND tr.reference_number IS NOT NULL ) )';

		if( CTaskRelationshipType::MY_TEAM == $intTaskRelationshipTypeId || true == valArr( $arrstrToDoFilter ) ) {
			$strSubSqlExceptToDo = $strSubSql . $strFromClause . $strWhereClauseCondition . $strWhereClauseConditionExceptToDo;
			$strSubSqlForToDo = $strSubSql . $strFromClauseForToDo . $strWhereClauseCondition . $strWhereClauseConditionForToDo;
			$strFinalSubSql = ' ( ' . $strSubSqlExceptToDo . ' ) UNION ( ' . $strSubSqlForToDo . ' ) ';
		} else {
			$strFinalSubSql = $strSubSql . $strFromClause . $strWhereClauseCondition . $strWhereClauseConditionExceptToDo;
		}

		$strSql = 'SELECT
						task_type_id,
						order_num,
						count ( task_type_id ) as task_total
					FROM
						( ' . $strFinalSubSql . ' ) AS sub_query
					GROUP BY
						task_type_id,
						order_num
					ORDER BY
						order_num';

		$arrmixData = fetchData( sprintf( $strSql, ( int ) $intDepartmentId ), $objDatabase );

		return rekeyArray( 'task_type_id', $arrmixData );

	}

	public static function fetchNonAssociatedTaskTypesByUserIdByTaskRelationShipIdByDepartmentId( $intUserId, $intTaskRelationshipTypeId, $intDepartmentId, $objDatabase, $arrintLCEmployeeIds ) {
		$strWhereCondition = '';
		if( true == valArr( $arrintLCEmployeeIds ) ) {
			$strWhereCondition = ' AND t.user_id NOT IN( ' . implode( ',', $arrintLCEmployeeIds ) . ' )';
		}

		$strSql = 'SELECT
					task_type_id,
					name as tab_name,
					count(task_type_id) task_total
					FROM
						( SELECT
							DISTINCT t.id,
							tt.name,
							t.task_type_id
						 FROM
							tasks t
							JOIN task_types tt ON t.deleted_by IS NULL 
								AND tt.id NOT IN ( ' . CTaskType::HELPDESK . ',' . CTaskType::SUSPICION . ',' . CTaskType::TASK_DATA_REQUEST . ',' . CTaskType::RP_FEEDBACK . ',' . CTaskType::SCHEMA_CHANGE . ', ' . CTaskType::ENTRATA_TEST_CASE . ' ) 
								AND tt.id = t.task_type_id 
								AND t.task_status_id NOT IN( ' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ', ' . CTaskStatus::CLOSED . ', ' . CTaskStatus::REJECT . ' )' . $strWhereCondition;

		$strFromClause 				= '';
		$strWhereClauseCondition 	= ' WHERE t.task_type_id NOT IN ( SELECT ttd.task_type_id FROM task_type_departments ttd WHERE ttd.department_id = ' . ( int ) $intDepartmentId . ' ) ';

		switch( $intTaskRelationshipTypeId ) {

			case CTaskRelationshipType::CREATED_BY_ME:
				$strWhereClauseCondition .= ' AND t.created_by = ' . ( int ) $intUserId;
				break;

			case CTaskRelationshipType::I_AM_STAKEHOLDER:
				$strWhereClauseCondition .= ' AND tw.user_id = ' . ( int ) $intUserId;
				$strFromClause .= ' LEFT JOIN task_watchlists tw ON ( t.id = tw.task_id )';
				break;

			case CTaskRelationshipType::ASSIGNED_TO_ME:
			default:
				$strWhereClauseCondition .= ' AND t.user_id = ' . ( int ) $intUserId;
				break;
		}

		$strSql .= $strFromClause . $strWhereClauseCondition . ') as sub_query GROUP BY task_type_id, name ORDER BY task_type_id ASC';
		return rekeyArray( 'task_type_id', fetchData( $strSql, $objDatabase ) );

	}

	public static function fetchToDoTaskCountByDepartmentIdByUserIdByEmployeeIdByTaskRelationShipId( $intDepartmentId, $intUserId, $intTaskRelationshipTypeId, $arrintReleaseIds, $arrintTeamEmployeesUserIds,$arrstrToDoFilter,$boolShowCompleted, $objDatabase ) {

		$strSubSql = 'SELECT
							DISTINCT t.id,
							t.task_type_id,
							dtt.order_num
						FROM
							task_type_departments dtt
							JOIN task_types tt ON ( tt.id = dtt.task_type_id )
							JOIN tasks t ON t.deleted_by IS NULL AND tt.id = t.task_type_id ' . ( false == $boolShowCompleted ? ' AND t.task_status_id NOT IN ( ' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' ) ' : ' AND t.task_status_id IN ( ' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' ) ' );

		$strFromClause 								= '';
		$strWhereClauseCondition 					= ' WHERE dtt.department_id = ' . ( int ) $intDepartmentId;
		$strWhereClauseConditionTaskReleaseIdIsNull = '';

		if( true == valArr( $arrintReleaseIds ) ) {

			$intNullKey = array_search( 'NULL', $arrintReleaseIds );

			if( false !== $intNullKey ) {
				unset( $arrintReleaseIds[$intNullKey] );
				$strWhereClauseConditionTaskReleaseIdIsNull = ' OR t.task_release_id IS NULL ';
			}

			$strWhereClauseCondition .= ' AND t.task_type_id= ' . CTaskType::TO_DO;
			$strWhereClauseCondition .= ( true == valArr( $arrintReleaseIds ) ? ' AND ( t.task_release_id IN (' . implode( ',', $arrintReleaseIds ) . ') ' : '' );
			$strWhereClauseCondition .= $strWhereClauseConditionTaskReleaseIdIsNull . ' ) ';

		}
		if( true == valArr( $arrstrToDoFilter ) ) {
			if( ( true == array_key_exists( 'last_days_filter', $arrstrToDoFilter ) && false == is_null( $arrstrToDoFilter['last_days_filter'] ) ) && 0 != $arrstrToDoFilter['last_days_filter'] ) {
				$strWhereClauseCondition .= 'AND ( DATE(t.created_on) >= ( DATE( \'' . date( 'Y-m-d' ) . '\') - INTERVAL\'' . $arrstrToDoFilter['last_days_filter'] . 'day\' ) AND DATE( t.created_on ) <= DATE(\'' . date( 'Y-m-d' ) . '\') )';
			}

			if( ( true == array_key_exists( 'due_days_filter', $arrstrToDoFilter ) && false == is_null( $arrstrToDoFilter['due_days_filter'] ) ) && 0 != $arrstrToDoFilter['due_days_filter'] ) {
				$strWhereClauseCondition .= 'AND ( DATE(t.due_date) <= ( DATE( \'' . date( 'Y-m-d' ) . '\') + INTERVAL\'' . $arrstrToDoFilter['due_days_filter'] . 'day\' ) AND DATE(t.due_date) >= DATE(\'' . date( 'Y-m-d' ) . '\') )';
			}

			if( ( true == array_key_exists( 'due_days_filter', $arrstrToDoFilter ) && false == is_null( $arrstrToDoFilter['due_days_filter'] ) ) && 0 != $arrstrToDoFilter['due_days_filter'] ) {
				$strWhereClauseCondition .= 'AND ( DATE(t.due_date) <= ( DATE( \'' . date( 'Y-m-d' ) . '\') + INTERVAL\'' . $arrstrToDoFilter['due_days_filter'] . 'day\' ) AND DATE(t.due_date) >= DATE(\'' . date( 'Y-m-d' ) . '\') )';
			}

		}

		switch( $intTaskRelationshipTypeId ) {

			case CTaskRelationshipType::CREATED_BY_ME:
				$strWhereClauseCondition .= ' AND t.created_by = ' . ( int ) $intUserId;
				break;

			case CTaskRelationshipType::MY_TEAM:
				if( true == valArr( $arrintTeamEmployeesUserIds ) ) {
					$strWhereClauseCondition .= ' AND t.user_id IN (' . implode( ',', $arrintTeamEmployeesUserIds ) . ')';
				}

				$strFromClause .= ' JOIN survey_tasks st ON ( t.id = st.task_id ) ';
				break;

			case CTaskRelationshipType::ASSIGNED_TO_ME:
			default:
				$strWhereClauseCondition .= ' AND t.user_id = ' . ( int ) $intUserId;
				break;
		}

		$strWhereClauseCondition .= ' AND ( ( ea.completed_on IS NULL AND tr.reference_number IS NULL ) OR ( ea.completed_on IS NULL AND tr.reference_number IS NOT NULL AND tr.task_reference_type_id !=' . CTaskReferenceType::REVIEW_GOALS . ' )  OR ( ea.completed_on IS NOT NULL AND tr.reference_number IS NOT NULL ) )';
		$strFromClause .= ' LEFT JOIN task_references tr ON ( t.id = tr.task_id AND tr.deleted_by IS NULL )
          LEFT JOIN employee_assessments ea ON ea.id = tr.reference_number ';

		$strSubSql .= $strFromClause . $strWhereClauseCondition;
		$strSql = ' SELECT
							task_type_id,
							order_num,
							count ( task_type_id ) AS task_total
						FROM ( ' . $strSubSql . ' ) as subQury GROUP BY task_type_id, order_num ORDER BY order_num ASC';

		$arrintData = fetchData( sprintf( $strSql, ( int ) $intDepartmentId ), $objDatabase );

		return rekeyArray( 'task_type_id', $arrintData );
	}

	public static function fetchUsersByParentTaskTypeId( $intTaskTypeId, $objDatabase ) {

		if( false == is_numeric( $intTaskTypeId ) ) return [];

		$strSql = 'SELECT 
						tt.id,
						tda.country_code,
						u.id as user_id
				   FROM task_types tt 
				   JOIN task_default_assignments tda ON tda.task_type_id = tt.id
				   JOIN users u ON u.employee_id = tda.employee_id
				   WHERE tt.disabled_by IS NULL
				   AND tda.deleted_by IS NULL
				   AND tda.deleted_on IS NULL
				   AND tt.parent_task_type_id = ' . ( int ) $intTaskTypeId;

		return fetchData( $strSql, $objDatabase );
	}

}
?>