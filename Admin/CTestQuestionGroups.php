<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestionGroups
 * Do not add any new functions to this class.
 */

class CTestQuestionGroups extends CBaseTestQuestionGroups {

	public static function fetchAllQuestionGroups( $objDatabase ) {
		return self::fetchTestQuestionGroups( 'SELECT * FROM test_question_groups ORDER BY name', $objDatabase );
	}
}
?>