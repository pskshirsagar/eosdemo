<?php

use Psi\Eos\Admin\CPersons;
use Psi\Eos\Admin\CPsProducts;
use Psi\Eos\Admin\CActionCategories;
use Psi\Eos\Admin\CActionResults;

class CAction extends CBaseAction {

	const PROMOTER		= '1 Promoter';
	const LOW_RISK		= '2 Low Risk';
	const MEDIUM_RISK	= '3 Medium Risk';
	const HIGH_RISK		= '4 High Risk';
	const ACTION_RESULT_FAVORITE	= 156;
	const ACTION_RESULT_VIEW		= 154;
	const ACTION_RESULT_DOWNLOAD	= 155;

	protected $m_intDepartmentId;
	protected $m_strActionResultName;
	protected $m_strEmployeeName;

	/**
	 * Get Functions
	 *
	 */

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function getActionsCount() {
		return $this->m_intActionsCount;
	}

	public function getDecryptedNote() {
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strNotes, CONFIG_SODIUM_KEY_EMPLOYEE_NOTE, [ 'legacy_secret_key' => CONFIG_KEY_EMPLOYEE_NOTE, 'is_base64_encoded' => true ] );
	}

	public function getActionResultName() {
		return $this->m_strActionResultName;
	}

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setDepartmentId( $intDepartmentId ) {
		$this->m_intDepartmentId = $intDepartmentId;
	}

	public function setActionsCount( $intActionsCount ) {
		$this->m_intActionsCount = $intActionsCount;
	}

	public function setEncryptedNote( $strNote ) {
		$this->m_strNotes = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $strNote, NULL, NULL, true ), CONFIG_SODIUM_KEY_EMPLOYEE_NOTE );
	}

	public function setActionResultName( $strActionResultName ) {
		$this->m_strActionResultName = $strActionResultName;
	}

	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = CStrings::strTrimDef( $strEmployeeName, NULL, false );
	}

	/**
	 * Setter Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['department_id'] ) )	$this->setDepartmentId( $arrmixValues['department_id'] );
		if( true == isset( $arrmixValues['actions_count'] ) )	$this->setActionsCount( $arrmixValues['actions_count'] );
		if( true == isset( $arrmixValues['note_type_name'] ) )	$this->setActionResultName( $arrmixValues['note_type_name'] );
		if( true == isset( $arrmixValues['employee_name'] ) )	$this->setEmployeeName( $arrmixValues['employee_name'] );

	}

	public function createTrainingTrainerAssociation() {
		$objTrainingTrainerAssociation = new CTrainingTrainerAssociation();
		$objTrainingTrainerAssociation->setActionId( $this->getId() );

		return $objTrainingTrainerAssociation;
	}

	public function createActionReference() {
		$objActionReference = new CActionReference();
		$objActionReference->setActionId( $this->getId() );

		return $objActionReference;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valActionTypeId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intActionTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'action_type_id', 'Action type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valActionResultId( $strMessage ) {
		$boolIsValid = true;

		if( true == empty( $this->m_intActionResultId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'action_result_id', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valNoteTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getActionResultId() ) && true == is_null( $this->getActionCategoryId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContractId( $boolIsForOpportunity = false ) {
		$boolIsValid = true;
		$strMessage	 = NULL;

		if( true == is_null( $this->getContractId() ) ) {

			if( false == $boolIsForOpportunity ) {
				$strMessage = 'Contract id is required.';
			} else {
				$strMessage = 'Opportunity is required.';
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_id', $strMessage ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPsLeadEventId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intPsLeadEventId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_lead_event_id', 'Event is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intEmployeeId ) ) {
			$strMessage = ( CActionType::DEMO == $this->m_intActionTypeId ) ? 'Sales Closer is required.' : 'Employee is required.';

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', $strMessage ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valPrimaryReference() {
		$boolIsValid = true;

		switch( $this->m_intActionTypeId ) {
			case CActionType::TAG:
				if( true == empty( $this->m_intPrimaryReference ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_reference', 'Tag is required.' ) );
				}
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	public function valActionDatetime() {
		$boolIsValid = true;

		if( true == empty( $this->m_strActionDatetime ) || 10 > strlen( $this->m_strActionDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'action_datetime', 'Contact date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;

		if( CActionResult::DELINQUENCY_REASON == $this->getActionResultId() ) {

			if( true == is_null( $this->getNotes() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Reason is required.' ) );
			}
		} else {
			if( true == is_null( $this->getNotes() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valProjectedEndDate() {
		$boolIsValid = true;

		if( true == empty( $this->m_strProjectedEndDate ) || 10 > strlen( $this->m_strProjectedEndDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'projected_end_date', 'Projected end date is required.' ) );
		} elseif( strtotime( $this->m_strProjectedEndDate ) < strtotime( $this->m_strActionDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'projected_end_date', 'Projected end date should be greater than start date.' ) );
		}

		return $boolIsValid;
	}

	public function valProjectTitle() {
		$boolIsValid = true;

	 	if( true == is_null( $this->getProjectTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'project_title', 'Project Title is required.' ) );
		}

		return $boolIsValid;
	}

	public function valExpectedGains() {
		$boolIsValid = true;

		if( true == is_null( $this->getExpectedGains() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expected_gains', 'Expected Gains is required.' ) );
		}

		return $boolIsValid;
	}

	public function valActualGains() {
		$boolIsValid = true;

		if( true == is_null( $this->getActualGains() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_gains', 'Actual Gains is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCurrentProductAdoption() {
		$boolIsValid = true;

		if( true == is_null( $this->getCurrentProductAdoption() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_product_adoption', 'Product Adoption is required.' ) );
		}
		return $boolIsValid;
	}

	public function valImplementationDelayTypeId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intImplementationDelayTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'implementation_delay_type_id', 'Delay Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valActionDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getActionDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'action_description', 'Action Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		$arrstrActionDetails = $this->getDetails();

		if( !valStr( $arrstrActionDetails->implementation_training ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'implementation_training', 'Training category is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsFromEmployee = false, $boolISNoteRequired = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valActionTypeId();

				if( true == $boolIsFromEmployee ) {
					$boolIsValid &= $this->valNoteTypeId();
					$boolIsValid &= $this->valNotes();
				}
				break;

			case VALIDATE_UPDATE:
				if( true == $boolIsFromEmployee ) {
					$boolIsValid &= $this->valNoteTypeId();
					$boolIsValid &= $this->valNotes();
				}

				if( true == $boolISNoteRequired ) {
					$boolIsValid &= $this->valNotes();
				}
				break;

			case 'amex_batch_insert':
				$boolIsValid &= $this->valProjectTitle();
				$boolIsValid &= $this->valActionDatetime();
				$boolIsValid &= $this->valProjectedEndDate();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_delinquency_dashboard':
				$boolIsValid &= $this->valNotes();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getCondensedActionDate() {
		return date( 'm/d/y', strtotime( $this->getActionDatetime() ) );
	}

	public function loadIconName() {

		switch( $this->getActionTypeId() ) {

			case CActionType::CALL:
			case CActionType::CALL:
				return 'phone-icon';
				break;

			case CActionType::NEWSLETTER:
			case CActionType::MASS_EMAIL:
			case CActionType::NEWSLETTER:
			case CActionType::DEMO:
				return 'newsletter-icon';
				break;

			case CActionType::EMAIL:
			case CActionType::EMAIL:
				return 'email-icon';
				break;

			case CActionType::DEMO:
				return 'demo-icon';
				break;

			case CActionType::MEETING:
				return 'cal-icon';
				break;

			case CActionType::DIRECT_MAIL:
				return 'mail-icon';
				break;

			case CActionType::SUPPORT_PEEP_EMAIL:
			case CActionType::SALES_PEEP_EMAIL:
				return 'peep';
				break;

			case CActionType::ENTRATA_AD:
				return 'ad-email-icon';
				break;

			case CActionType::FOLLOW_UP:
				return 'follow-up-icon';
				break;

			default:
				return 'Symbols-Critical-icon';
				break;

		}

		return 'Symbols-Critical-icon';
	}

	public function customValidateByActionType( $arrobjActionReferences = array(), $intActionResultId, $boolIsActiveContract = false, $intActionCategoryId = NULL ) {

		$boolIsValid 					= true;
		$intActionTypeId 				= $this->getActionTypeId();
		$arrobjTempActionReferences 	= rekeyObjects( 'ActionReferenceTypeId', $arrobjActionReferences );

		$arrintEmployeeValidatationTypes 		= array(
			CActionType::NOTE,
			CActionType::EMAIL,
			CActionType::CALL,
			CActionType::DEMO,
			CActionType::MEETING,
			CActionType::FOLLOW_UP,
			CActionType::ENTRATA_AD,
			CActionType::SALES_PEEP_EMAIL,
			CActionType::SUPPORT_PEEP_EMAIL,
			CActionType::OFFICE_VISIT,
			CActionType::MAILER,
			CActionType::SOCIAL_MEDIA,
			CActionType::EVENT,
			CActionType::TRAINING
		);

		$arrintActionDatetimeValidationTypes	= array(
			CActionType::DEMO,
			CActionType::FOLLOW_UP,
			CActionType::SUCCESS_REVIEW,
			CActionType::ADOPTION,
			CActionType::OFFICE_VISIT,
			CActionType::MAILER,
			CActionType::SOCIAL_MEDIA,
			CActionType::EVENT,
			CActionType::TRAINING
		);

		$arrintActionNoteValidationTypes		= array(
			CActionType::NOTE,
			CActionType::DEMO,
			CActionType::SUCCESS_REVIEW,
			CActionType::ADOPTION,
			CActionType::SUPPORT_FLAG,
			CActionType::OFFICE_VISIT,
			CActionType::MAILER,
			CActionType::SOCIAL_MEDIA,
			CActionType::EVENT,
			CActionType::TRAINING
		);

		$arrintActionTagValidationTypes			= array( CActionType::TAG );

		$arrintActionOpportunityValidationTypes	= array(
			CActionType::DEMO,
			CActionType::FOLLOW_UP
		);

		$arrintSalesEngineerValidationTypes 	= array();
		$arrintPersonDemoValidationTypes 		= array(
			CActionType::DEMO,
			CActionType::EMAIL,
			CActionType::SUCCESS_REVIEW,
			CActionType::CALL,
			CActionType::FOLLOW_UP,
			CActionType::ENTRATA_AD,
			CActionType::SALES_PEEP_EMAIL,
			CActionType::SUPPORT_PEEP_EMAIL,
			CActionType::OFFICE_VISIT,
			CActionType::MAILER,
			CActionType::SOCIAL_MEDIA,
			CActionType::EVENT
		);

		$arrintPsProductValidationTypes 		= array(
			CActionType::DEMO,
			CActionType::ADOPTION,
			CActionType::TRAINING
		);

		$arrintActionDetailsValidationTypes		= array(
			CActionType::TRAINING
		);

		$strActionResultValidationMessage = 'Action Result is required.';
		$arrintActionResultValidationTypes		= array(
			CActionType::NOTE,
			CActionType::CALL,
			CActionType::MEETING,
			CActionType::EVENT,
			CActionType::TRAINING
		);

		if( $intActionTypeId == CActionType::EVENT ) {
			$strActionResultValidationMessage = 'Event type is required.';
		}

		if( $intActionTypeId == CActionType::TRAINING ) {
			$strActionResultValidationMessage = 'Training type is required.';
		}

		$arrintProjectedEndDateValidationTypes		= array( CActionType::ADOPTION );
		$arrintProjectTitleValidationTypes			= array( CActionType::ADOPTION );

		$arrintImplementationDelayValidationTypes	= array();
		$arrintActionEventValidationTypes			= array();

		if( $intActionCategoryId == CActionCategory::ID_COMPLAINT ) {
			unset( $arrintEmployeeValidatationTypes[array_search( CActionType::NOTE, $arrintEmployeeValidatationTypes )] );
		}

		if( $intActionCategoryId == CActionCategory::ID_IMPLEMENTATION && true == $boolIsActiveContract ) {
			$arrintImplementationDelayValidationTypes[] = CActionType::NOTE;
			$arrintPsProductValidationTypes[] 			= CActionType::NOTE;
		}

		if( $intActionResultId == CActionResult::EVENT ) {
			$arrintActionEventValidationTypes = array( CActionType::MEETING );
		}

		// Custom validatation
		if( true == in_array( $intActionTypeId, $arrintActionNoteValidationTypes ) ) 			$boolIsValid &= $this->valNotes();
		if( true == in_array( $intActionTypeId, $arrintEmployeeValidatationTypes ) ) 			$boolIsValid &= $this->valEmployeeId();
		if( true == in_array( $intActionTypeId, $arrintActionEventValidationTypes ) ) 			$boolIsValid &= $this->valPsLeadEventId();
		if( true == in_array( $intActionTypeId, $arrintActionDatetimeValidationTypes ) ) 		$boolIsValid &= $this->valActionDatetime();
		if( true == in_array( $intActionTypeId, $arrintActionTagValidationTypes ) ) 			$boolIsValid &= $this->valPrimaryReference();
		if( true == in_array( $intActionTypeId, $arrintActionOpportunityValidationTypes ) ) 	$boolIsValid &= $this->valContractId( true );
		if( true == in_array( $intActionTypeId, $arrintActionResultValidationTypes ) && CActionType::NOTE !== $this->getActionTypeId() )	$boolIsValid &= $this->valActionResultId( $strActionResultValidationMessage );
		if( true == in_array( $intActionTypeId, $arrintProjectedEndDateValidationTypes ) )	 	$boolIsValid &= $this->valProjectedEndDate();
		if( true == in_array( $intActionTypeId, $arrintProjectTitleValidationTypes ) ) 			$boolIsValid &= $this->valProjectTitle();
		if( true == in_array( $intActionTypeId, $arrintImplementationDelayValidationTypes ) ) 	$boolIsValid &= $this->valImplementationDelayTypeId();
		if( true == in_array( $intActionTypeId, $arrintActionDetailsValidationTypes ) ) 		$boolIsValid &= $this->valDetails();

		if( false == is_numeric( $this->getActionResultId() ) && true == in_array( $this->getActionTypeId(), array( CActionType::CALL, CActionType::FOLLOW_UP ) ) ) $boolIsValid &= $this->valNotes();

		if( true == in_array( $intActionTypeId, $arrintSalesEngineerValidationTypes ) ) {
			if( true == empty( $arrobjTempActionReferences[CActionReferenceType::SALES_ENGINEER_DEMO] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sales_engineer_ids', 'Sales Engineer is required.' ) );
				$boolIsValid &= false;
			}
		}

		if( true == in_array( $intActionTypeId, $arrintPersonDemoValidationTypes ) ) {
			if( true == empty( $arrobjTempActionReferences[CActionReferenceType::PERSON] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'demo_person_ids', 'Person is required.' ) );
				$boolIsValid &= false;
			}
		}

		if( true == in_array( $intActionTypeId, $arrintPsProductValidationTypes ) ) {
			if( true == empty( $arrobjTempActionReferences[CActionReferenceType::PRODUCT] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_ids', 'Product is required.' ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function buildActionDescription( $objDatabase, $arrobjActionReferences = NULL, $objPsLeadDetail = NULL, $objCompanyPreference = NULL, $objAction = NULL ) {
		if( true == is_numeric( $this->getActionTypeId() ) ) {

			$arrobjAdoptionActivityStatus = array();

			$strActionDescription = '';
			$objActionType = CActionTypes::fetchActionTypeById( $this->getActionTypeId(), $objDatabase );

			if( true == valObj( $objActionType, 'CActionType' ) ) {
				$strActionDescription = '<strong>' . $objActionType->getName() . '</strong>';

				switch( $this->getActionTypeId() ) {
					case CActionType::NOTE:
					case CActionType::ENTRATA_AD:
						$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
						$strActionDescription .= $this->buildPersonDescription( $objDatabase );
						$strActionDescription .= $this->buildActionReferenceDescription( $arrobjActionReferences, $objDatabase );
						$strActionDescription .= $this->buildResultDescription( $objDatabase );
						$strActionDescription .= $this->buildImplementationDelayDescription( $objDatabase );
						break;

					case CActionType::DEMO:
					case CActionType::DELINQUENCY:
					case CActionType::CALL:
					case CActionType::FOLLOW_UP:
					case CActionType::EMAIL:
					case CActionType::SALES_PEEP_EMAIL:
					case CActionType::SUPPORT_PEEP_EMAIL:
						$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
						$strActionDescription .= $this->buildPersonDescription( $objDatabase );
						$strActionDescription .= $this->buildActionReferenceDescription( $arrobjActionReferences, $objDatabase );
						$strActionDescription .= $this->buildResultDescription( $objDatabase );
						break;

					case CActionType::SUCCESS_REVIEW:
						$strActionDescription .= $this->buildActionReferenceDescription( $arrobjActionReferences, $objDatabase );
						break;

					case CActionType::ADOPTION:
						$arrobjAdoptionActivityStatus = CActionResults::createService()->fetchAdoptionActivityStatus( $objDatabase );

						$strStatus = ( true == valArr( $arrobjAdoptionActivityStatus ) ) ? $arrobjAdoptionActivityStatus[$this->getActionResultId()]->getName() : '-';

						$strActionDescription .= ' ' . $this->getProjectTitle() . $this->buildEmployeeDescription( $objDatabase ) . ' &#124; <strong>Status:</strong> ' . $strStatus . ' <\br>';
						$strActionDescription .= $this->buildActionReferenceDescription( $arrobjActionReferences, $objDatabase );
						break;

					case CActionType::TAG:
						if( true == is_numeric( $this->getPrimaryReference() ) ) {
							$objTag = CTags::fetchTagById( $this->getPrimaryReference(), $objDatabase );

							if( true == valObj( $objTag, 'CTag' ) ) {
								$strActionDescription .= ' ' . $objTag->getName() . ' Associated ';
							}
						}
						break;

					case CActionType::MEETING:
					case CActionType::EVENT:
					case CActionType::TRAINING:
						$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
						$strActionDescription .= $this->buildActionReferenceDescription( $arrobjActionReferences, $objDatabase );
						$strActionDescription .= $this->buildEventDescription( $objDatabase );
						$strActionDescription .= $this->buildResultDescription( $objDatabase, $objAction );
						break;

					case CActionType::RISK_STATUS:
						$strActionDescription .= ' changed ';
						$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
						$strActionDescription .= $this->getRiskStatusIcon( $objPsLeadDetail );
						break;

					case CActionType::SUPPORT_FLAG:
						$strActionDescription .= ' Added ';
						$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
						break;

					case CActionType::EMPLOYEE:
						$strActionDescription .= ' People List Downloaded ';
						$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
						break;

					case CActionType::RENEWAL_STATUS:
						if( $this->getActionResultId() == CActionResult::SEND_INITIAL_LETTER ) {
							$strActionDescription .= ' Initial letter draft saved successfully ';
							$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
							$strActionDescription .= ' . ';

						} elseif( $this->getActionResultId() == CActionResult::WAIVE_INITIAL_LETTER ) {
							$strActionDescription .= ' Send Initial Letter Status Marked as Waived ';
							$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
							$strActionDescription .= ' . ';
						} elseif( $this->getActionResultId() == CActionResult::SEND_INITIAL_LETTER_COMPLETE ) {

							$strMessage = $this->getActionDescription();
							$strActionDescription .= ' Send Initial Letter emailed ';
							$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
							$strActionDescription .= $this->buildActionReferenceDescription( $arrobjActionReferences, $objDatabase );
							$strActionDescription = trim( $strActionDescription, ',' );
							$strActionDescription .= '. ';
							$strActionDescription .= '<br/><br/>' . $strMessage;

						} elseif( $this->getActionResultId() == CActionResult::APPROVE_FOUR_PERCENTAGE ) {
							$strActionDescription .= ' Increase Approved ';
							$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
							$strActionDescription .= ' . ';

						} elseif( $this->getActionResultId() == CActionResult::WAIVE_FOUR_PERCENTAGE ) {
							$strReasonForWaive		= $this->buildWaiveIncreaseDescription();
							$strActionDescription .= ' Waived with reason <strong>' . $strReasonForWaive . '</strong> ';
							$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
							$strActionDescription .= ' . ';

						} elseif( $this->getActionResultId() == CActionResult::GENERATE_FINAL_LETTER ) {
							$strActionDescription .= ' Final Renewal letter draft saved successfully ';
							$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
							$strActionDescription .= ' . ';
						} elseif( $this->getActionResultId() == CActionResult::EMAIL_LETTER_COMPLETE ) {

							$strMessage = $this->getActionDescription();
							$strActionDescription .= ' Final Letter emailed ';
							$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
							$strActionDescription .= $this->buildActionReferenceDescription( $arrobjActionReferences, $objDatabase );
							$strActionDescription = trim( $strActionDescription, ',' );
							$strActionDescription .= '. ';
							$strActionDescription .= '<br/><br/>' . $strMessage;

						} elseif( $this->getActionResultId() == CActionResult:: UPDATE_CHARGES ) {
							$strActionDescription .= ' Charges Updated ';
							$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
						}
						break;

					case CActionType::SETTING_PROFILES:
						if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) ) {

							$strSettingTemplateStatus = ( true == $objCompanyPreference->getValue() ) ? 'On' : 'Off';

							$strActionDescription .= ' updated to <strong>' . \Psi\CStringService::singleton()->strtoupper( $strSettingTemplateStatus ) . '</strong>';
							$strActionDescription .= $this->buildEmployeeDescription( $objDatabase ) . '.';
						}
						break;

					case CActionType::OFFICE_VISIT:
					case CActionType::MAILER:
					case CActionType::SOCIAL_MEDIA:
						$strActionDescription .= $this->buildEmployeeDescription( $objDatabase );
						$strActionDescription .= $this->buildActionReferenceDescription( $arrobjActionReferences, $objDatabase );
						break;

					default:
						// Default case
						break;
				}

				$this->setActionDescription( $strActionDescription );
			}
		}
	}

	public function buildEmployeeDescription( $objDatabase ) {
		$intEmployeeId = $this->getEmployeeId();

		if( true == is_numeric( $intEmployeeId ) ) {
			$objEmployee = CEmployees::fetchEmployeeById( $intEmployeeId, $objDatabase );

			$strEmployeeName = ( true == is_null( $objEmployee->getPreferredName() ) ) ? $objEmployee->getNameFirst() : $objEmployee->getPreferredName();

			if( true == valObj( $objEmployee, 'CEmployee' ) ) return ' by <strong>' . $strEmployeeName . '</strong>';
		}
	}

	public function buildPersonDescription( $objDatabase ) {
		$intPersonId = $this->getPersonId();

		if( true == is_numeric( $intPersonId ) ) {
			$objPerson = CPersons::createService()->fetchPersonById( $intPersonId, $objDatabase );

			if( true == valObj( $objPerson, 'CPerson' ) ) {
				$strPersonName = ( true == $objPerson->getIsPrimary() && true == is_null( $objPerson->getNameFirst() ) ) ? 'Primary Contact' : $objPerson->getNameFirst();

				return ' to <strong>' . $strPersonName . '</strong>';
			}
		}
	}

	public function buildEventDescription( $objDatabase ) {
		$intPsLeadEventId = $this->getPsLeadEventId();

		if( true == is_numeric( $intPsLeadEventId ) ) {
			$objPsLeadEvent = \Psi\Eos\Admin\CPsLeadEvents::createService()->fetchPsLeadEventById( $intPsLeadEventId, $objDatabase );

			if( true == valObj( $objPsLeadEvent, 'CPsLeadEvent' ) ) return ' for event <strong>' . $objPsLeadEvent->getName() . '</strong>';
		}
	}

	public function buildResultDescription( $objDatabase, $objAction = NULL ) {

		$intActionResultId = $this->getActionResultId();

		if( valId( $intActionResultId ) ) {
			$objActionResult = CActionResults::createService()->fetchActionResultById( $intActionResultId, $objDatabase );

			if( valObj( $objActionResult, 'CActionResult' ) && CActionType::TRAINING == $this->getActionTypeId() ) {
				$arrmixDetails = objectToArray( $objAction->getDetails() );
				$strTrainingDetails = ' [' . $objActionResult->getName() . '';

				if( valStr( $arrmixDetails['training_location'] ) ) {
					$strTrainingDetails .= ', Location : ' . $arrmixDetails['training_location'] . '';
				}

				if( valStr( $arrmixDetails['training_session'] ) ) {
					$strTrainingDetails .= ', Session : ' . $arrmixDetails['training_session'] . '';
				}

				if( valStr( $arrmixDetails['implementation_training'] ) ) {
					$strTrainingDetails .= ', Category Of Training : ' . $arrmixDetails['implementation_training'];
				}
				return $strTrainingDetails . ']';
			} elseif( valObj( $objActionResult, 'CActionResult' ) ) {
				return ' [' . $objActionResult->getName() . ']';
			}
		}

		if( valId( $this->getActionCategoryId() ) ) {
			$objActionCategory = CActionCategories::createService()->fetchActionCategoryById( $this->getActionCategoryId(), $objDatabase );
			if( valObj( $objActionCategory, 'CActionCategory' ) ) {
				return ' [' . $objActionCategory->getName() . ']';
			}
		}
	}

	public function buildImplementationDelayDescription( $objDatabase ) {
		$intImplementationDelayTypeId = $this->getImplementationDelayTypeId();

		if( true == is_numeric( $intImplementationDelayTypeId ) ) {
			$objImplementationDelayType = \Psi\Eos\Admin\CImplementationDelayTypes::createService()->fetchImplementationDelayTypeById( $intImplementationDelayTypeId, $objDatabase );

			if( true == valObj( $objImplementationDelayType, 'CImplementationDelayType' ) ) return ' <strong>Delay Reason:</strong> ' . $objImplementationDelayType->getName();
		}
	}

	private function buildActionReferenceDescription( $arrobjActionReferences, $objDatabase ) {
		if( true == valArr( $arrobjActionReferences ) ) {
			$strDescription				= '';
			$arrintPersonIds 			= array();
			$arrintSalesEngineerIds 	= array();
			$arrintPsProductIds			= array();
			$arrintPropertyIds			= array();

			foreach( $arrobjActionReferences as $objActionReference ) {
				switch( $objActionReference->getActionReferenceTypeId() ) {
					case CActionReferenceType::PERSON:
						$arrintPersonIds[] = $objActionReference->getReferenceNumber();
						break;

					case CActionReferenceType::SALES_ENGINEER_DEMO:
						$arrintSalesEngineerIds[] = $objActionReference->getReferenceNumber();
						break;

					case CActionReferenceType::PRODUCT:
						$arrintPsProductIds[] = $objActionReference->getReferenceNumber();
						break;

					case CActionReferenceType::PROPERTY:
						$arrintPropertyIds[] = $objActionReference->getReferenceNumber();
						break;

					default:
						// Default case
						break;
				}
			}

			if( true == valArr( $arrintPersonIds ) ) {
				$arrobjPersons = CPersons::createService()->fetchPersonsByIds( $arrintPersonIds, $objDatabase );

				if( true == valArr( $arrobjPersons ) ) {
					$intCount = 1;
					$strDescription .= ' to ';

					foreach( $arrobjPersons as $objPerson ) {
						if( false == is_null( $objPerson->getNameFirst() ) || false == is_null( $objPerson->getNameLast() ) ) {
							if( 1 < $intCount ) $strDescription .= ', ';
							$strDescription .= ' ' . $objPerson->getNameFirst() . ' ' . $objPerson->getNameLast();

							$intCount++;
						} elseif( true == $objPerson->getIsPrimary() && true == is_null( $objPerson->getNameFirst() ) ) {
							if( 1 < $intCount ) $strDescription .= ', ';
							$strDescription .= ' Primary Contact ';

							$intCount++;
						}
					}
				}
			}

			if( true == valArr( $arrintSalesEngineerIds ) ) {
				$arrobjSalesEngineers = CEmployees::fetchEmployeesByIds( $arrintSalesEngineerIds, $objDatabase );

				if( true == valArr( $arrobjSalesEngineers ) ) {
					$intCount = 1;
					$strDescription .= ' <strong> Sales Engineers: </strong>';

					foreach( $arrobjSalesEngineers as $objSalesEngineer ) {
						if( 1 < $intCount ) $strDescription .= ', ';
						$strDescription .= $objSalesEngineer->getPreferredName();

						$intCount++;
					}
				}
			}

			if( true == valArr( $arrintPsProductIds ) ) {
				$arrobjPsProducts = CPsProducts::createService()->fetchPsProductsByIds( $arrintPsProductIds, $objDatabase );

				if( true == valArr( $arrobjPsProducts ) ) {
					$intCount = 1;
					$strDescription .= ' <strong> Ps Products: </strong>';

					foreach( $arrobjPsProducts as $objPsProduct ) {
						if( 1 < $intCount ) $strDescription .= ', ';
						$strDescription .= $objPsProduct->getName();

						$intCount++;
					}
				}
			}

			if( true == valArr( $arrintPropertyIds ) ) {
				$arrmixProperties = \Psi\Eos\Admin\CProperties::createService()->fetchPropertiesByIds( $arrintPropertyIds, $objDatabase );
				if( true == valArr( $arrmixProperties ) ) {
					$intCount = 1;
					$strDescription .= '</br> <strong> Properties:</strong> ,';

					foreach( $arrmixProperties as $arrmixProperty ) {
						if( 1 < $intCount ) $strDescription .= ', ';
						$strDescription .= $arrmixProperty['property_name'];
						$intCount++;
					}

				}
			}
			return $strDescription . ',';
		}
	}

	public function buildWaiveIncreaseDescription() {
		$intWaiveResultId = $this->getPrimaryReference();

		switch( $intWaiveResultId ) {

			case CActionResult::BILLING_ISSUES:
				$strReasonForWaive = ' Billing Issues ';
				break;

			case CActionResult::IN_PILOT:
				$strReasonForWaive = ' In pilot ';
				break;

			case CActionResult::MARK_AS_COMPLETED:
				$strReasonForWaive = ' Mark as Completed ';
				break;

			case CActionResult::NEW_SALE_IN_PROGRESS:
				$strReasonForWaive = ' New Sale in Progress ';
				break;

			case CActionResult::NEW_SALE_ADDED_PROPERTIES:
				$strReasonForWaive = ' New Sales - Added Properties ';
				break;

			case CActionResult::OTHER:
				$strReasonForWaive = ' Other ';
				break;

			case CActionResult::RECENTLY_ADDED_PRODUCTS:
				$strReasonForWaive = ' Recently Added Products ';
				break;

			case CActionResult::RECENTLY_ADDED_PROPERTIES:
				$strReasonForWaive = ' Recently Added Properties ';
				break;

			case CActionResult::SHOPPING_COMPETITORS:
				$strReasonForWaive = ' Shopping Competitors ';
				break;

			case CActionResult::TERMINATING:
				$strReasonForWaive = ' Terminating ';
				break;

			case CActionResult::UNSATISFIED_WITH_IMPLEMENTATION:
				$strReasonForWaive = ' Unsatisfied with Implementation ';
				break;

			case CActionResult::UNSATISFIED_WITH_PRODUCTS_SERVICES:
				$strReasonForWaive = ' Unsatisfied with Products/Services ';
				break;

			default:
				$strReasonForWaive = '';
				break;
		}

		return $strReasonForWaive;
	}

	/**
	 * Other Functions
	 *
	 */

	public function sendPersonEmailAlert( $intUserId,  $arrobjMultipleObjects, $strUploadPath=NULL ) {

		$objSystemEmail 				= ( true == isset( $arrobjMultipleObjects['system_email'] ) && true == valObj( $arrobjMultipleObjects['system_email'], 'CSystemEmail' ) ) ? $arrobjMultipleObjects['system_email']:'';
		$arrobjEmailAttachments			= ( true == isset( $arrobjMultipleObjects['email_attachments'] ) ) ? $arrobjMultipleObjects['email_attachments'] : '';
		$arrobjSystemEmailAttachments 	= ( true == isset( $arrobjMultipleObjects['system_email_attachments'] ) ) ? $arrobjMultipleObjects['system_email_attachments'] : '';
		$objAdminDatabase				= ( true == isset( $arrobjMultipleObjects['admin_db'] ) && true == valObj( $arrobjMultipleObjects['admin_db'], 'CDatabase' ) ) ? $arrobjMultipleObjects['admin_db']:'';
		$objEmailDatabase   			= ( true == isset( $arrobjMultipleObjects['email_db'] ) && true == valObj( $arrobjMultipleObjects['email_db'], 'CDatabase' ) ) ? $arrobjMultipleObjects['email_db']:'';

		switch( NULL ) {
		 	default:
				$boolIsValid 			= true;
				$arrobjErrorMessages 	= NULL;

				// Validate action
				$boolIsValid &= $this->validate( VALIDATE_INSERT );

				if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) $boolIsValid &= $objSystemEmail->validate( VALIDATE_INSERT );

				if( false == $boolIsValid ) {
					$arrobjErrorMessages[] = $this->getErrorMsgs();
					if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) $arrobjErrorMessages[] = $objSystemEmail->getErrorMsgs();
					break;
				}

				$objAdminDatabase->begin();
				$objEmailDatabase->begin();

				// Insert System Email
				if( true == valObj( $objSystemEmail, 'CSystemEmail' ) && false == is_null( $objSystemEmail ) && false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
					$arrobjErrorMessages[] = $objSystemEmail->getErrorMsgs();
					$objEmailDatabase->rollback();
					break;
				}

				// Insert Email Attachments
				if( true == valArr( $arrobjEmailAttachments ) ) {

					foreach( $arrobjEmailAttachments as $objEmailAttachment ) {
						if( false == valObj( $objEmailAttachment, 'CEmailAttachment' ) || false == $objEmailAttachment->insert( $intUserId, NULL, $objEmailDatabase, $strUploadPath . $objEmailAttachment->getFileName() ) ) {
							$arrobjErrorMessages[] = $objEmailAttachment->getErrorMsgs();
								break 2;
						}
					}
				}

				// Insert System Email Attachments
				if( true == valArr( $arrobjSystemEmailAttachments ) ) {
					foreach( $arrobjSystemEmailAttachments as $objSystemEmailAttachment ) {
						if( false == valObj( $objSystemEmailAttachment, 'CSystemEmailAttachment' ) || false == $objSystemEmailAttachment->insert( $intUserId, $objEmailDatabase ) ) {
							continue;
						}
					}
				}

			// Insert Action
			if( false == $this->insert( $intUserId, $objAdminDatabase ) ) {
				$arrobjErrorMessages[] = $this->getErrorMsgs();
				$objAdminDatabase->rollback();
				break;
			}

			$objAdminDatabase->commit();
			$objEmailDatabase->commit();
		}

		if( true == valArr( $arrobjErrorMessages ) ) {
			$strMessage = '';
			$boolIsValidInsert = false;
			foreach( $arrobjErrorMessages as $arrobjMessages ) {
				if( true == valArr( $arrobjMessages ) ) {
					foreach( $arrobjMessages as $objMessage ) {
						$strMessage .= $objMessage->getMessage() . ' ';
					}
				}
			}
		}

		if( false == $boolIsValid ) return $strMessage;
	}

	private function getRiskStatusIcon( $objPsLeadDetail ) {

		if( false == valObj( $objPsLeadDetail, 'CPsLeadDetail' ) || false == $objPsLeadDetail->getClientRiskStatus() ) return $strKeyRiskStatusIcon = '';

		$intKeyClientRank = $objPsLeadDetail->getKeyClientRank();

		if( CPsLeadDetail::HIGH == $intKeyClientRank ) {
			$strKeyClass = 'high ';
		} elseif( CPsLeadDetail::MEDIUM_HIGH == $intKeyClientRank ) {
			$strKeyClass = 'medium-high ';
		} elseif( CPsLeadDetail::MEDIUM == $intKeyClientRank ) {
			$strKeyClass = 'medium ';
		} elseif( CPsLeadDetail::MEDIUM_LOW == $intKeyClientRank ) {
			$strKeyClass = 'medium-low ';
		} elseif( CPsLeadDetail::LOW == $intKeyClientRank ) {
			$strKeyClass = 'low ';
		} else {
			$strKeyClass = ' off ';
		}

		switch( $objPsLeadDetail->getClientRiskStatus() ) {
			case Cpsleaddetail::BLUE:
				$strKeyClass .= ' blue ' . $strKeyClass;
				break;

			case Cpsleaddetail::GREEN:
				$strKeyClass .= ' green ' . $strKeyClass;
				break;

			case Cpsleaddetail::YELLOW:
				$strKeyClass .= ' yellow ' . $strKeyClass;
				break;

			case Cpsleaddetail::RED:
				$strKeyClass .= ' red ' . $strKeyClass;
				break;

			default:
				$strKeyClass .= '';
				break;
		}

		$strKeyRiskStatusIcon = '<div class="risk-status default-cursor ' . $strKeyClass . ' js-risk-status"><div class="margin10-top"><span>' . $intKeyClientRank . '</span></div></div>';
		return $strKeyRiskStatusIcon;
	}

}
?>