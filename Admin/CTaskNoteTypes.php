<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskNoteTypes
 * Do not add any new functions to this class.
 */

class CTaskNoteTypes extends CBaseTaskNoteTypes {

	public static function fetchTaskNoteTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CTaskNoteType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchTaskNoteType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CTaskNoteType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>