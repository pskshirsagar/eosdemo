<?php

class CTestSuiteResult extends CBaseTestSuiteResult {

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valTestSuiteId() {
		if( !valId( $this->getTestSuiteId() ) ) {
			throw new UnexpectedValueException( __( 'Test Suite ID missing' ) );
		}
	}

	public function valTestSuiteTypeId() {
		if( !valId( $this->getTestSuiteTypeId() ) ) {
			throw new UnexpectedValueException( __( 'Test Suite Type Id missing' ) );
		}
	}

	public function valResultDatetime() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	/**
	 * @param $strAction
	 * @throws \UnexpectedValueException
	 */
	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$this->valTestSuiteId();
				$this->valTestSuiteTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setResults( $arrmixResults ) {
		$this->setDetailsField( 'test_suite_results', $arrmixResults );
	}

}

?>