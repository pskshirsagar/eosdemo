<?php

class CNewTeamEmployee extends CBaseNewTeamEmployee {

	protected $m_strTeamName;
	protected $m_strNameFull;
	protected $m_strCountryCode;
	protected $m_strEmailAddress;
	protected $m_strDepartmentName;
	protected $m_strDesignationName;
	protected $m_strManagerEmailAddress;

	protected $m_intUserId;
	protected $m_intIsProjectManager;
	protected $m_intEmployeeStatusTypeId;
	protected $m_intSdmEmployeeId;
	protected $m_intHrRepresentativeEmployeeId;
	protected $m_intDesignationId;

	protected $m_boolViewCpa;
	protected $m_boolHasTeam;
	protected $m_strRecommendedBy;
	protected $m_boolEligibleToRecommend;
	protected $m_boolAlreadyAdded;

	/**
	 * Get Functions
	 */

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getManagerEmailAddress() {
		return $this->m_strManagerEmailAddress;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getIsProjectManager() {
		return $this->m_intIsProjectManager;
	}

	public function getTeamName() {
		return $this->m_strTeamName;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getEmployeeStatusTypeId() {
		return $this->m_intEmployeeStatusTypeId;
	}

	public function getHrRepresentativeEmployeeId() {
		return $this->m_intHrRepresentativeEmployeeId;
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function getViewCpa() {
		return $this->m_boolViewCpa;
	}

	public function getHasTeam() {
		return $this->m_boolHasTeam;
	}

	public function getRecommendedBy() {
		return $this->m_strRecommendedBy;
	}

	public function getEligibleToRecommend() {
		return $this->m_boolEligibleToRecommend;
	}

	public function getAlreadyAdded() {
		return $this->m_boolAlreadyAdded;
	}

	public function getSdmEmployeeId() {
		return $this->m_intSdmEmployeeId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['department_name'] ) ) 			$this->setDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['designation_name'] ) ) 			$this->setDesignationName( $arrmixValues['designation_name'] );
		if( true == isset( $arrmixValues['name_full'] ) ) 					$this->setNameFull( $arrmixValues['name_full'] );
		if( true == isset( $arrmixValues['user_id'] ) ) 					$this->setUserId( $arrmixValues['user_id'] );
		if( true == isset( $arrmixValues['email_address'] ) ) 				$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['manager_email_address'] ) )		$this->setManagerEmailAddress( $arrmixValues['manager_email_address'] );
		if( true == isset( $arrmixValues['is_project_manager'] ) )			$this->setIsProjectManager( $arrmixValues['is_project_manager'] );
		if( true == isset( $arrmixValues['team_name'] ) )					$this->setTeamName( $arrmixValues['team_name'] );
		if( true == isset( $arrmixValues['country_code'] ) )				$this->setCountryCode( $arrmixValues['country_code'] );
		if( true == isset( $arrmixValues['employee_status_type_id'] ) )		$this->setEmployeeStatusTypeId( $arrmixValues['employee_status_type_id'] );
		if( true == isset( $arrmixValues['hr_representative_employee_id'] ) )	$this->setHrRepresentativeEmployeeId( $arrmixValues['hr_representative_employee_id'] );
		if( true == isset( $arrmixValues['view_cpa'] ) )						$this->setViewCpa( $arrmixValues['view_cpa'] );
		if( true == isset( $arrmixValues['has_team'] ) )						$this->setHasTeam( $arrmixValues['has_team'] );
		if( true == isset( $arrmixValues['recommended_by'] ) )					$this->setRecommendedBy( $arrmixValues['recommended_by'] );
		if( true == isset( $arrmixValues['eligible_to_recommend'] ) )			$this->setEligibleToRecommend( $arrmixValues['eligible_to_recommend'] );
		if( true == isset( $arrmixValues['already_added'] ) )			        $this->setAlreadyAdded( $arrmixValues['already_added'] );
		if( true == isset( $arrmixValues['sdm_employee_id'] ) )                 $this->setSdmEmployeeId( $arrmixValues['sdm_employee_id'] );
		if( true == isset( $arrmixValues['designation_id'] ) )                 $this->setDesignationId( $arrmixValues['designation_id'] );
		return;
	}

	public function setDesignationName( $strDesignationName ) {
		$this->m_strDesignationName = $strDesignationName;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setManagerEmailAddress( $strManagerEmailAddress ) {
		$this->m_strManagerEmailAddress = $strManagerEmailAddress;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setIsProjectManager( $intIsProjectManager ) {
		$this->m_intIsProjectManager = $intIsProjectManager;
	}

	public function setTeamName( $strTeamName ) {
		$this->m_strTeamName = $strTeamName;
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

	public function setEmployeeStatusTypeId( $intEmployeeStatusTypeId ) {
		$this->m_intEmployeeStatusTypeId = $intEmployeeStatusTypeId;
	}

	public function setHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId ) {
		$this->m_intHrRepresentativeEmployeeId = $intHrRepresentativeEmployeeId;
	}

	public function setDesignationId( $intDesignationId ) {
		$this->m_intDesignationId = $intDesignationId;
	}

	public function setViewCpa( $boolViewCpa ) {
		$this->m_boolViewCpa = $boolViewCpa;
	}

	public function setHasTeam( $boolHasTeam ) {
		$this->m_boolHasTeam = $boolHasTeam;
	}

	public function setRecommendedBy( $strRecommendedBy ) {
		$this->m_strRecommendedBy = $strRecommendedBy;
	}

	public function setEligibleToRecommend( $strEligibleToRecommend ) {
		$this->m_boolEligibleToRecommend = $strEligibleToRecommend;
	}

	public function setAlreadyAdded( $strAlreadyAdded ) {
		$this->m_boolAlreadyAdded = $strAlreadyAdded;
	}

	public function setSdmEmployeeId( $intSdmEmployeeId ) {
		$this->m_intSdmEmployeeId = CStrings::strToIntDef( $intSdmEmployeeId, NULL, false );
	}

	/**
	 * Validation Functions
	 */

	public function valEmployeeId() {
		$boolValid = true;

		if( true == is_null( $this->getEmployeeId() ) && false == is_numeric( $this->getTeamId() ) ) {
			$boolValid = false;
		}

		return $boolValid;
	}

	public function validate( $strAction ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolValid &= $this->valEmployeeId();
				break;

			case VALIDATE_UPDATE:
				$boolValid &= $this->valEmployeeId();
				break;

			case VALIDATE_DELETE:
				$boolValid &= $this->valEmployeeId();
				break;

			default:
				$boolValid = false;
				break;
		}

		return $boolValid;
	}

}
?>