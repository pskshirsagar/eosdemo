<?php

class CDmOffPageLogin extends CBaseDmOffPageLogin {

	const DM_GOOGLE           = 1;
	const DM_FACEBOOK         = 2;
	const DM_TWITTER          = 3;
	const DM_HOOTSUITE        = 4;
	const DM_YEXT             = 5;
	const DM_LIVE_YOUTUBE_URL = 6;
	const DM_OTHER_LOGIN_TYPE = 7;

	const DM_CLIENT_LOGIN                = 1;
	const DM_PROPERTY_LOGIN              = 2;
	const DM_CITATION_LOGIN              = 3;
	const DM_ACCOUNT_ACCESSIBILITY_LOGIN = 4;
	const DM_OTHER_LOGIN                 = 5;

	const DM_FACEBOOK_ACCESS_LEVEL_ADMIN     = 'Admin';
	const DM_FACEBOOK_ACCESS_LEVEL_NON_ADMIN = 'Non-Admin';
	const DM_FACEBOOK_ACCESS_LEVEL_NONE      = 'None';

	public static $c_arrintDmOffPageLoginTypes = [
		self::DM_GOOGLE,
		self::DM_FACEBOOK,
		self::DM_TWITTER,
		self::DM_HOOTSUITE,
		self::DM_YEXT,
		self::DM_LIVE_YOUTUBE_URL,
		self::DM_OTHER_LOGIN_TYPE
	];

	public static $c_arrintDmOffPageLoginCategories = [
		self::DM_CLIENT_LOGIN,
		self::DM_PROPERTY_LOGIN,
		self::DM_CITATION_LOGIN,
		self::DM_ACCOUNT_ACCESSIBILITY_LOGIN,
		self::DM_OTHER_LOGIN
	];

	public static $c_arrstrLoginAccessLevels = [
		self::DM_FACEBOOK_ACCESS_LEVEL_ADMIN,
		self::DM_FACEBOOK_ACCESS_LEVEL_NON_ADMIN,
		self::DM_FACEBOOK_ACCESS_LEVEL_NONE,
	];

	public static function encrypt( $strPlainText ) {
		if( false == valStr( $strPlainText ) ) {
			return NULL;
		}

		return base64_encode( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainText, CONFIG_KEY_LOGIN_USERNAME, [ 'provider' => \Psi\Libraries\Cryptography\CCryptoProviderFactory::CRYPTO_PROVIDER_OPENSSL ] ) );
	}

	public static function decrypt( $strEncryptedText ) {
		if( false == valStr( $strEncryptedText ) ) {
			return NULL;
		}

		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( base64_decode( $strEncryptedText ), CONFIG_KEY_LOGIN_USERNAME, [ 'provider' => \Psi\Libraries\Cryptography\CCryptoProviderFactory::CRYPTO_PROVIDER_OPENSSL ] );
	}

	public function valUsername() {
		$boolIsValid = true;

		if( valStr( $this->getUsername() ) ) {

			$strUserName = self::decrypt( $this->getUsername() );

			if( !CValidation::validateEmailAddresses( $strUserName ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Please enter a valid email address.' ) );

				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public static function bulkDecrypt( $arrmixLoginCredentials ) {
		if( !valArr( $arrmixLoginCredentials ) ) {
			return [];
		}

		foreach( $arrmixLoginCredentials as $intKey => $arrmixLoginCredential ) {

			if( valStr( $arrmixLoginCredential['username'] ) ) {
				$arrmixLoginCredentials[$intKey]['username'] = self::decrypt( $arrmixLoginCredential['username'] );
			}

			if( valStr( $arrmixLoginCredential['password'] ) ) {
				$arrmixLoginCredentials[$intKey]['password'] = self::decrypt( $arrmixLoginCredential['password'] );
			}
		}

		return $arrmixLoginCredentials;
	}

	public function validateAccountAccessibilityLogin() {

		$boolIsValid = false;

		if( !in_array( $this->getDmOffPageLoginTypeId(), self::$c_arrintDmOffPageLoginTypes ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dm_off_page_login_type_id', 'Invalid Login Type.' ) );

			return $boolIsValid;
		}

		if( !in_array( $this->getDmLoginCategoryId(), self::$c_arrintDmOffPageLoginCategories ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dm_login_category_id', 'Invalid Login Category.' ) );

			return $boolIsValid;
		}

		if( valStr( $this->getLoginAccessLevel() ) && !in_array( $this->getLoginAccessLevel(), self::$c_arrstrLoginAccessLevels ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'login_access_level', 'Invalid login access level.' ) );

			return $boolIsValid;
		}

		if( valStr( $this->getUrl() ) && !CValidation::checkUrl( $this->getUrl() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Please enter valid URL.' ) );

			return $boolIsValid;
		}

		return true;
	}

	public function valUrl() {
		$boolIsValid = true;

		if( valStr( $this->getUrl() ) && !CValidation::checkUrl( $this->getUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Please enter valid url.' ) );

			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intDmLoginCategoryId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				if( self::DM_CITATION_LOGIN == $intDmLoginCategoryId ) {
					$boolIsValid &= $this->valUrl();
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_account_accessibility_login':
				$boolIsValid = $this->validateAccountAccessibilityLogin();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>
