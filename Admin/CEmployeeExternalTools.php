<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeExternalTools
 * Do not add any new functions to this class.
 */

class CEmployeeExternalTools extends CBaseEmployeeExternalTools {

	public static function fetchAllPaginatedEmployeeExternalTools( $intPageNo, $intPageSize, $objDatabase,$strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strOrderBy = '';

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY e.preferred_name';
		}

		$strSql = 'SELECT
						Distinct ( e.id ),e.*

					FROM
						employees e
						JOIN employee_external_tools eet ON eet.employee_id = e. id
						JOIN external_tools et ON eet.external_tool_id = et.id
					Where
						et.is_published = true
						AND eet.deleted_by IS NULL
						AND eet.deleted_on IS NULL
						AND et.deleted_by IS NULL
						AND et.deleted_on IS NULL'
				. $strOrderBy . '
						OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return CEmployees::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesCountByExternalTools( $objDatabase ) {

		$strSql = 'SELECT
						Count(Distinct (e.id))
					FROM
						employee_external_tools eet
						JOIN external_tools et ON eet.external_tool_id = et.id
						JOIN employees e ON eet.employee_id = e.id
					Where
						et.is_published = true
						AND eet.deleted_by IS NULL
						AND eet.deleted_on IS NULL';

		$arrintResponse = fetchdata( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchEmployeesByExternalTools( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = '	SELECT e.*,
						u.id as user_id,
						d.name AS department_name
					FROM
						employees e
						JOIN users u ON (e.id = u.employee_id)
						LEFT JOIN departments d ON ( e.department_id = d.id )
					WHERE
						e.employee_status_type_id = 1 AND
						( e.date_started IS NULL OR
						e.date_terminated IS NULL OR
						NOW() < e.date_terminated )
						AND e.id NOT IN (	SELECT
												DISTINCT (eet.employee_id)
											FROM
												external_tools et
												JOIN employee_external_tools eet ON ( et.id = eet.external_tool_id )
											Where
												et.deleted_by IS NULL
												AND	et.deleted_on IS NULL
												AND eet.deleted_by IS NULL
												AND	eet.deleted_on IS NULL )
						AND ( e.preferred_name ILIKE E\'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_full ILIKE E\'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
					ORDER BY e.preferred_name';

		return CEmployees::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchActiveExternalToolsByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = '	SELECT
						eet.external_tool_id,
						eet.employee_id,
						et.name as external_tool_name,
						et.url as external_tool_url
					FROM
						employee_external_tools eet
						LEFT JOIN external_tools et ON ( et.id = eet.external_tool_id )
					WHERE
						eet.employee_id = ' . ( int ) $intEmployeeId . ' AND
						eet.deleted_on IS NULL
					ORDER BY
						et.name ASC';

			return self::fetchEmployeeExternalTools( $strSql, $objDatabase );
	}

	public static function fetchActiveExternalToolsByEmployeeIds( $arrintEmployeeId, $objDatabase ) {
		$strSql = '	SELECT
						*
					FROM
						employee_external_tools
					WHERE
						employee_id IN (' . implode( ', ', $arrintEmployeeId ) . ') AND
						deleted_by IS NULL AND
						deleted_on IS NULL';

		return self::fetchEmployeeExternalTools( $strSql, $objDatabase );
	}

	public static function fetchEmployeeExternalToolByEmployeeIdsByExternalToolIds( $arrintEmployeeId, $arrintExternalToolIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeId ) || false == valArr( $arrintExternalToolIds ) ) return NULL;

		$strSql = 'SELECT *
					FROM
						employee_external_tools
					WHERE
						employee_id IN (' . implode( ', ', $arrintEmployeeId ) . ')
					    AND external_tool_id IN (' . implode( ', ', $arrintExternalToolIds ) . ')';

		return self::fetchEmployeeExternalTools( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchEmployeeByExternalTool( $arrstrFilteredExplodedSearch,$objDatabase ) {

		$strSql = 'SELECT
						Distinct ( e.id ),
						e.*,
						d.name as department_name,
						dg.name as designation_name
					FROM
						employees e
						JOIN employee_external_tools eet ON eet.employee_id = e. id
						JOIN external_tools et ON eet.external_tool_id = et.id
						LEFT JOIN departments d ON ( e.department_id = d.id )
						LEFT JOIN designations dg ON ( e.designation_id = dg.id )
					WHERE
						et.is_published = true
						AND eet.deleted_by IS NULL
						AND eet.deleted_on IS NULL
						AND ( e.preferred_name ILIKE E\'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_full ILIKE E\'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
						ORDER BY e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeAssociatedExternalToolsCount( $objDatabase, $arrintExternalToolId = NULL ) {

		$strWhereConditation = '';
		if( true == valArr( $arrintExternalToolId ) ) {
			$strWhereConditation = 'AND et.id IN (' . implode( ', ', $arrintExternalToolId ) . ')';
		}

			$strSql = ' SELECT
							et.id,
							count( eet.employee_id )
						FROM
							external_tools et
							LEFT JOIN employee_external_tools eet ON ( et.id = eet.external_tool_id AND eet.deleted_on IS NULL )
						WHERE
							et.deleted_by IS NULL
							AND et.deleted_on IS NULL
							' . $strWhereConditation . '
							Group by et.id Order by name ASC';

			return fetchData( $strSql, $objDatabase );
	}
}
?>