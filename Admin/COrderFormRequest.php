<?php

use Psi\Eos\Admin\CTransactions;
use Psi\Libraries\ExternalFileUpload\CFileUpload;

class COrderFormRequest extends CBaseOrderFormRequest {

	protected $m_strFilePath;

	const ORDER_FORM_PAYMENT_OPTION_MAIL_BY_CHECK	= 2;
	const ORDER_FORM_REVISION_COUNT					= 3;
	const CUSTOM_TEMPLATE_ADD_FILES_COUNT			= 2;
	const FLOOR_PLAN_ADD_FILES_COUNT				= 6;
	const TEMPLATE_LOGO_ORDER_FORM_AMOUNT			= 25;
	const DOMAIN_ORDER_TRANSFER_FORM_AMOUNT			= 34.95;
	const CHECK_SCANNER_SHIPPING_COST				= 50;
	const FLOOR_PLAN_PACKAGE_AMOUNT					= 125;
	const CUSTOM_LOGO_AMOUNT						= 150;
	const SITE_PLAN_ORDER_FORM_AMOUNT				= 250;
	const NEW_MIN_CHECK_SCANNER_LIMIT				= 5000;
	const ACTIVE_ORDERS								= 0;
	const FULFILLED_ORDERS							= 1;
	const CC_SERVICE_PERCENT_AMOUNT					= 3.75;
	const FLOOR_PLAN_REQUIRED_COUNT_BY_EXT			= 3;
	const FLOOR_PLAN_ADD_REVIEW_FILES_COUNT			= 3;
	const MAX_NUMBER_OF_PDF_PAGES					= 9;
	const FLOOR_PLAN_TEMPLATE_PREFIX				= 'floor-plan-';
	const FLOOR_PLAN_FIRST							= 1;
	const ORDER_FORM_REOPEN_MAX_DAYS				= 60;
	const DOMAIN_ORDER_FORM_REORDER_AMOUNT			= 10;
	const MAX_ORDER_FORM_REVISION_REQUEST_DAYS		= 30;
	const SEND_REVISION_REMINDER_EMAIL_DAYS			= 20;

	public static $c_arrintTestClientIds = [ CClient::ID_DB_XENTO_SYSTEMS, CClient::ID_OZONE_ENTERPRISES, CClient::ID_DEV_TEST ];

	/**
	 * Get Functions
	 *
	 */
	public function getFilePath() {
		return $this->m_strFilePath;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['file_path'] ) ) {
			$this->setFilePath( $arrmixValues['file_path'] );
		}

	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	/**
	 * Other Functions
	 *
	 */

	public function createOrderFormTransactions( $intCompanyPaymentId, $objClient, $objInvoice, $arrmixPaymentData, $arrmixPropertyAmountDetails, $objAdminDatabase, $arrmixOrderFormDetails, $boolIsPaymentTransaction = true ) {
		$arrobjOrderFormTransactions = [];
		$strSalesTaxStatus = NULL;

		$intChargeCodeId = COrderFormType::$c_arrintOrderFormChargeCodeIds[$this->getOrderFormTypeId()];
		$strOrderFormTransactionMemo = COrderFormType::$c_arrintOrderFormNames[$this->getOrderFormTypeId()] . ' Order Form Request Charges';

		$objDefaultTransaction = new CTransaction();
		$objDefaultTransaction->setDefaults();
		$objDefaultTransaction->setAccountId( $arrmixOrderFormDetails['account_id'] );
		$objDefaultTransaction->setCid( $objClient->getId() );
		$objDefaultTransaction->setEntityId( $objClient->getEntityId() );
		$objDefaultTransaction->setInvoiceId( $objInvoice->getId() );

		if( false == empty( $arrmixPaymentData['sub_total'] ) && 0 != $arrmixPaymentData['sub_total'] ) {
			$arrmixPropertyNames = \Psi\Eos\Admin\CProperties::createService()->fetchPropertyNamesByPropertyIdsByCid( array_keys( ( array ) $arrmixPropertyAmountDetails ), $objClient->getId(), $objAdminDatabase );
			foreach( ( array ) $arrmixPropertyAmountDetails as $intPropertyId => $fltAmount ) {
				$strMemoString = $strOrderFormTransactionMemo;
				if( 0 == $intPropertyId ) {
					$intPropertyId = NULL;
				} else {
					if( true == valArr( $arrmixPropertyNames ) ) {
						$strMemoString = $strOrderFormTransactionMemo . ' For ' . $arrmixPropertyNames[$intPropertyId];
					}
				}
				$objOrderFormTransaction = clone $objDefaultTransaction;
				$objOrderFormTransaction->setId( $objOrderFormTransaction->fetchNextId( $objAdminDatabase ) );
				$objOrderFormTransaction->setChargeCodeId( $intChargeCodeId );
				$objOrderFormTransaction->setPropertyId( $intPropertyId );
				$objOrderFormTransaction->setTransactionAmount( $fltAmount );
				$objOrderFormTransaction->setMemo( $strMemoString );
				$arrobjOrderFormTransactions[] = $objOrderFormTransaction;
			}
		}

		$objInvoice->setTransactions( $arrobjOrderFormTransactions );

		if( false == empty( $arrmixPaymentData['shipping_amount'] ) && 0 != $arrmixPaymentData['shipping_amount'] ) {
			$objOrderFormShipmentTransaction = clone $objDefaultTransaction;
			$objOrderFormShipmentTransaction->setId( $objOrderFormShipmentTransaction->fetchNextId( $objAdminDatabase ) );
			$objOrderFormShipmentTransaction->setChargeCodeId( $intChargeCodeId );
			$objOrderFormShipmentTransaction->setTransactionAmount( $arrmixPaymentData['shipping_amount'] );
			$objOrderFormShipmentTransaction->setMemo( 'Expedited Shipping Fee' );
			$arrobjOrderFormTransactions[] = $objOrderFormShipmentTransaction;
		}

		if( false == empty( $arrmixPaymentData['service_fee_amount'] ) && 0 != round( $arrmixPaymentData['service_fee_amount'] ) ) {
			$objOrderFormShipmentTransaction = clone $objDefaultTransaction;
			$objOrderFormShipmentTransaction->setId( $objOrderFormShipmentTransaction->fetchNextId( $objAdminDatabase ) );
			$objOrderFormShipmentTransaction->setChargeCodeId( CChargeCode::FINANCE_CHARGE_FEES );
			$objOrderFormShipmentTransaction->setTransactionAmount( $arrmixPaymentData['service_fee_amount'] );
			$objOrderFormShipmentTransaction->setMemo( 'Credit card processing fees for company payment Id ' . strval( $intCompanyPaymentId ) );
			$arrobjOrderFormTransactions[] = $objOrderFormShipmentTransaction;
		}

		if( true == $boolIsPaymentTransaction && CPaymentType::CHECK != $arrmixOrderFormDetails['payment_type_id'] ) {
			$objOrderFormPaymentTransaction = clone $objDefaultTransaction;
			$objOrderFormPaymentTransaction->setId( $objOrderFormPaymentTransaction->fetchNextId( $objAdminDatabase ) );
			$objOrderFormPaymentTransaction->setChargeCodeId( CChargeCode::PAYMENT_RECEIVED );
			$objOrderFormPaymentTransaction->setCompanyPaymentId( $intCompanyPaymentId );
			$objOrderFormPaymentTransaction->setTransactionAmount( -1 * abs( $arrmixOrderFormDetails['grand_amount'] ) );
			$objOrderFormPaymentTransaction->setMemo( COrderFormType::$c_arrintOrderFormNames[$this->getOrderFormTypeId()] . ' Order Form Payment Received' );

			$arrobjOrderFormTransactions[] = $objOrderFormPaymentTransaction;
		}

		if( true == valArr( $arrobjOrderFormTransactions ) && false == CTransactions::createService()->bulkInsert( $arrobjOrderFormTransactions, ENTRATA_USER_ID, $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();
			return [ 'status' => false ];
		}

		$strSalesTaxStatus = [ 'status' => true ];
		if( false == in_array( $this->getOrderFormTypeId(), COrderFormType::$c_arrintOrderFormWithNoSalesTax ) ) {
			$strSalesTaxStatus = [ 'status' => $objInvoice->calculateAndProcessInvoiceSalesTax( $objAdminDatabase, ENTRATA_USER_ID, $boolIsOnlyCalculate = false ) ];
		}

		if( true == $boolIsPaymentTransaction && true == $arrmixOrderFormDetails['is_payment_option'] ) {
			self::allocatePayment( $objInvoice, $objOrderFormPaymentTransaction->getId(), $objAdminDatabase );
		}

		return $strSalesTaxStatus;
	}

	protected static function allocatePayment( $objInvoice, $intCreditTransactionId, $objAdminDatabase ) {

		$arrobjOrderFormTransactions = CTransactions::createService()->fetchUnallottedTransactionsByCidByAccountIdByInvoiceIds( $objInvoice->getCid(), $objInvoice->getAccountId(), array( $objInvoice->getId() ), $objAdminDatabase );

		// create negative transaction in transactions table if payment type is not mail by check
		if( true == valArr( $arrobjOrderFormTransactions ) ) {
			foreach( $arrobjOrderFormTransactions as $intTransctionId => $objOrderFormTransaction ) {

				if( 0 != round( $objOrderFormTransaction->getTransactionAmount(), 2 ) ) {
					// create allocation table tuples
					$objAllocation = new CAllocation();
					$objAllocation->setCid( $objInvoice->getCid() );
					$objAllocation->setAccountId( $objInvoice->getAccountId() );
					$objAllocation->setChargeTransactionId( $objOrderFormTransaction->getId() );
					$objAllocation->setCreditTransactionId( $intCreditTransactionId );
					$objAllocation->setAllocationDatetime( 'NOW()' );
					$objAllocation->setAllocationAmount( $objOrderFormTransaction->getTransactionAmount() );

					$arrobjAllocations[] = $objAllocation;
				}
			}

			if( true == valArr( $arrobjAllocations ) && false == \Psi\Eos\Admin\CAllocations::createService()->bulkInsert( $arrobjAllocations, ENTRATA_USER_ID, $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();
				return [ 'status' => false ];
			}
		}
	}

	public function calculateSalesTax( $arrmixPropertyAndAmount, $objAdminDatabase, $objAccount, $boolIsPropertySpecificTax = false ) {

		// As we don't have any property association for custom logo, so we are by passing sales tax calculation step
		if( true == in_array( $this->getOrderFormTypeId(), COrderFormType::$c_arrintOrderFormWithNoSalesTax ) ) {
			return [ 'status' => true, 'data' => [ 'InvoiceSubTotal' => $arrmixPropertyAndAmount, 'TotalTaxApplied' => 0 ] ];
		}

		$arrobjTransactions = [];

		// we are calculating sales tax by dummy invoice id
		$objInvoice = new CInvoice();
		$objInvoice->setInvoiceNumber( CInvoice::INVOICE_REFERENCE_NUMBER );
		$objInvoice->setInvoiceDate( date( 'Y-m-d' ) );

		$objTransaction = new CTransaction();
		$fltSubTotal	= 0;

		foreach( $arrmixPropertyAndAmount as $intPropertyId => $intTransactionAmount ) {
			$objTempTransaction = clone $objTransaction;
			$objTempTransaction->setId( $intPropertyId );
			$objTempTransaction->setChargeCodeId( COrderFormType::$c_arrintOrderFormChargeCodeIds[$this->getOrderFormTypeId()] );
			$objTempTransaction->setTransactionAmount( $intTransactionAmount );
			$objTempTransaction->setPropertyId( $intPropertyId );
			$fltSubTotal += $intTransactionAmount;

			$arrobjTransactions[] = $objTempTransaction;
		}

		$objInvoice->setAccount( $objAccount );
		$objInvoice->setTransactions( $arrobjTransactions );

		$arrmixResponse = $objInvoice->calculateAndProcessInvoiceSalesTax( $objAdminDatabase, ENTRATA_USER_ID, $boolIsOnlyCalculate = true, false, false, false, $boolIsPropertySpecificTax );

		if( true == valArr( $arrmixResponse ) && false == $arrmixResponse['success'] ) {
			return [ 'status' => false, 'messages' => 'Unable to calculate sales tax. ' ];
		}

		return [ 'status' => true, 'data' => \Psi\Eos\Admin\CSalesTaxTransactions::createService()->prepareSalesTaxTransactionDetails( $arrmixResponse, $fltSubTotal ) ];
	}

	public function managePaymentOption( $objClient, $objAccount, $objInvoice, $intPaymentTypeId, $arrmixCompanyPayments, $arrmixPaymentOptionDetails, $arrmixPaymentData, $arrmixPropertyAmountDetails, $objAdminDatabase, $objClientDatabase, $intUserId ) {

		$arrmixOrderFormDetails =
			[
				'cid'				=> $objClient->getId(),
				'account_id'		=> $objAccount->getId(),
				'payment_type_id'	=> $intPaymentTypeId,
				'payment_status_type_id'	=> CPaymentStatusType::PENDING,
				'invoice_id'		=> $objInvoice->getId(),
				'sales_tax_amount'  => $arrmixPaymentData['sales_tax_amount'],
				'service_fee_amount' => $arrmixPaymentData['service_fee_amount'],
				'grand_amount'		=> $arrmixPaymentData['grand_amount'],
				'is_payment_option' => false
			];

		$objCompanyPayment = new CCompanyPayment();

		if( true == in_array( $intPaymentTypeId, array_merge( CPaymentType::$c_arrintCreditCardPaymentTypes, [ CPaymentType::ACH ] ) ) ) {
			if( false == $objCompanyPayment->setOrderFormCompanyPaymentData( $arrmixOrderFormDetails, $arrmixCompanyPayments, $arrmixPaymentOptionDetails, $intPaymentTypeId, $this->getOrderFormTypeId(), $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();
				return [ 'status' => false, 'messages' => 'Invalid Payament option, please try again later. ' ];
			}

			$strValidate = ( CPaymentType::ACH == $objCompanyPayment->getPaymentTypeId() ) ? 'post' : 'card_payment';

			if( false == $objCompanyPayment->validate( $strValidate, $objAdminDatabase, ENTRATA_USER_ID, NULL ) ) {
				$objAdminDatabase->rollback();
				return [ 'status' => false, 'messages' => getConsolidatedErrorMsg( $objCompanyPayment ) ];
			}

			$arrmixOrderFormDetails['is_payment_option'] = true;
		}

		$this->setInvoiceId( $objInvoice->getId() );

		// called postpayment() function to verify payment & then perform required operations
		$boolPostPaymentResult = $objCompanyPayment->postRpcPayment( $intUserId, $objAdminDatabase );
		if( true == $boolPostPaymentResult ) {
			$objCompanyPayment->setInvoiceId( $objInvoice->getId() );
			if( false == $objCompanyPayment->update( $intUserId, $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();
				return [ 'status' => false, 'messages' => 'failed to update company payments details, please try after some time. ' ];
			}

			$objInvoice->setCompanyPaymentId( $objCompanyPayment->getId() );
			if( false == $objInvoice->update( $intUserId, $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();
				return [ 'status' => false, 'messages' => 'failed to update invoice details, please try after some time. ' ];
			}
		} else {
			if( true == valObj( $objClient, 'CClient' ) && CCompanyStatusType::CLIENT == $objClient->getCompanyStatusTypeId() ) {
				$this->sendPaymentFailureNotificationEmail( $arrmixPaymentData, $objAccount, $objInvoice, $intUserId, $objAdminDatabase, $objClientDatabase );
			}
			$this->setOrderFormStatusTypeId( COrderFormStatusType::AWAITING_PAYMENT );
		}

		$arrmixSalesTaxTransactions = $this->createOrderFormTransactions( $objCompanyPayment->getId(), $objClient, $objInvoice, $arrmixPaymentData, $arrmixPropertyAmountDetails, $objAdminDatabase, $arrmixOrderFormDetails, $boolPostPaymentResult );

		if( false == $arrmixSalesTaxTransactions['status'] ) {
			return [ 'status' => false, 'messages' => 'Failed to insert transactions details. ' ];
		} else {
			return [ 'status' => true, 'messages' => 'Payment details created successfully. ' ];
		}
	}

	public function sendPaymentFailureNotificationEmail( $arrmixPaymentData, $objAccount, $objInvoice, $intUserId, $objAdminDatabase, $objClientDatabase ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objClient = CClients::fetchClientById( $this->getCid(), $objAdminDatabase );

		$objSmarty 						= new CPsSmarty( PATH_INTERFACES_ENTRATA, false );

		$objSmarty->assign( 'order_form_request_id', $this->getId() );
		$objSmarty->assign( 'order_form_type', COrderFormType::$c_arrintOrderFormNames[$this->getOrderFormTypeId()] );
		$objSmarty->assign( 'payment_amount', ( $arrmixPaymentData['grand_amount'] + $arrmixPaymentData['shipping_amount'] ) );
		$objSmarty->assign( 'account_id', $objAccount->getId() );
		$objSmarty->assign( 'invoice_id', $objInvoice->getId() );
		$objSmarty->assign( 'company_name', $objClient->getCompanyName() );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );
		$objSmarty->assign( 'company_url', 'http://' . $objClient->getRwxDomain() . '.' . CONFIG_COMPANY_BASE_DOMAIN );

		// Mail Body.
		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_ENTRATA . 'app_store/order_forms/order_form_payment_failure_notification.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$arrstrEmailIds	= [];

		// Fetch email addresses of users to trigger emails.

		$objCompanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeDetailsByCompanyUserIdByCid( $intUserId, $this->getCid(), $objClientDatabase );
		$arrstrEmailIds[] = ( true == valstr( $objCompanyEmployee->getEmailAddress() ) ) ? $objCompanyEmployee->getEmailAddress() : $objAccount->getBilltoEmailAddress();
		$strCcEmailId = $objAccount->getBilltoEmailAddress();

		if( true == valArr( $arrstrEmailIds ) ) {
			$objEmailDatabase 		= CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::INTERNAL_CLIENT_SERVICES );
			$strSubject 			= 'Order form payment failed for ' . COrderFormType::$c_arrintOrderFormNames[$this->getOrderFormTypeId()] . ' request';
			$objSystemEmailLibrary	= new CSystemEmailLibrary();
			$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::COMPANY_PAYMENT_ERROR, $strSubject, $strHtmlContent, $strToEmailAddress = implode( ', ', $arrstrEmailIds ), $intIsSelfDestruct = 0, $strFromEmailAddress = CSystemEmail::PROPERTYSOLUTIONS_BILLING_EMAIL_ADDRESS );
			$objSystemEmail->setCcEmailAddress( $strCcEmailId );

			if( false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
				trigger_error( 'System email failed to insert.', E_USER_ERROR );
				return false;
			}
		}
		return true;
	}

	public function convertPdfToJpeg() {
		// When we have multiple pages pdf file, we have to merge them all so converting pdf to jpeg and then merging jpeg images to one. We are allowing maximum 8 pages pdfs.
		$objImage = new Imagick();
		$objImage->pingImage( $this->getFilePath() );
		if( $objImage->getNumberImages() < self::MAX_NUMBER_OF_PDF_PAGES ) {
			$arrstrConvertedJpgImages = $this->convertPdfFileToJpegImages( $this->getFilePath() );
			if( true == valArr( $arrstrConvertedJpgImages ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrConvertedJpgImages ) ) {
				$strMergedImage = $this->mergedJpgImages( $arrstrConvertedJpgImages );
				if( NULL == $strMergedImage ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Unable to upload file.' ) );
					return false;
				}
				imagejpeg( $strMergedImage, $this->getFilePath() . '.jpeg' );
			}
		} else {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Unable to upload File. Maximum 8 pages of PDF is allowed for upload. ' ) );
			return false;
		}
		$strJpegFilePath = $this->getFilePath() . '.jpeg';
		$strImageFilePath = ( true == file_exists( $strJpegFilePath ) ) ? $strJpegFilePath : $this->getFilePath();
		return $strImageFilePath;
	}

	private function checkImageFilesExist( $arrstrImageFiles ) {
		$arrstrImageFilePaths = [];
		foreach( $arrstrImageFiles as $strImageFile ) {
			if( true == file_exists( $strImageFile ) && true == is_file( $strImageFile ) && 0 < filesize( $strImageFile ) ) {
				$arrstrImageFilePaths[] = $strImageFile;
			}
		}
		return $arrstrImageFilePaths;
	}

	public function convertEpsToJpeg() {

		$objImage = new Imagick();
		$objImage->readimage( $this->getFilePath() );
		$objImage->setImageFormat( 'jpeg' );
		$objImage->writeImage( $this->getFilePath() . '.jpeg' );
		$strJpegFilePath = $this->getFilePath() . '.jpeg';
		$strImageFilePath = ( true == file_exists( $strJpegFilePath ) ) ? $strJpegFilePath : $this->getFilePath();

		return $strImageFilePath;
	}

	public function uploadOrderFormRequestDetailFiles( $strFileInputName ) {

		$objUpload = new CFileUpload();

		if( false == is_dir( $this->getFilePath() ) && false == CFileIo::recursiveMakeDir( $this->getFilePath() ) ) {
			return [ 'status' => false, 'messages' => 'Could not create directory(' . $this->getFilePath() . ' ).' ];
		}

		$arrstrUploadedFiles = $objUpload->upload( $strFileInputName, $this->getFilePath() );

		return [ 'status' => true, 'data' => $arrstrUploadedFiles ];
	}

	public function uploadOrderFormRequestDetailObject( $strFileInputName, $objObjectStorageGateway, $intCid, $intPsLeadId ) {
		$arrstrUploadedFiles = [];
		$arrobjObjectStorageGatewayResponse = [];

		foreach( $_FILES[$strFileInputName]['name'] as $strIndex => $strFileName ) {
			if( '' != $strFileName ) {
				$objUploadOrderFormRequestDetail = new COrderFormRequestDetail();
				$objUploadOrderFormRequestDetail->buildFileData( $strFileInputName, $intCid, $intPsLeadId, $strIndex );
				$boolIsUploaded = $objUploadOrderFormRequestDetail->uploadObject( $objObjectStorageGateway );
				if( $boolIsUploaded ) {
					$arrobjObjectStorageGatewayResponse[$strIndex] = $objUploadOrderFormRequestDetail->getObjectStorageGatewayResponse();
					if( $objUploadOrderFormRequestDetail->getUploadFileName() ) $arrstrUploadedFiles[$strIndex] = $objUploadOrderFormRequestDetail->getUploadFileName();
				} else {
					return [ 'status' => false, 'messages' => 'Failed to upload file' ];
				}
			}
		}
		return [ 'status' => true, 'data' => $arrstrUploadedFiles, 'stored_object' => $arrobjObjectStorageGatewayResponse ];
	}

	private function convertPdfFileToJpegImages( $strFilePath ) {

		$strImageFilePath = $strFilePath;

		if( false == file_exists( $strImageFilePath ) ) return;

		$objImage = new Imagick();
		$objImage->readImage( $strFilePath );
		$intPages = $objImage->getNumberImages();

		$objSystemCommand 	= new CSystemCommand();
		$objSystemCommand->setExecutionTimeout( 30 );

		$strCommand = 'convert -density 150 ' . $strFilePath . ' ' . $strImageFilePath . '.jpeg';

		if( true != $objSystemCommand->execute( $strCommand ) ) {
			$strImageFilePath = NULL;
		}

		for( $intStart = 0; $intStart < $intPages; $intStart++ ) {
			$strImageFile = NULL;
			if( 1 == $intPages ) {
				$strImageFile = $strImageFilePath . '.jpeg';
			} else {
				$strImageFile = $strImageFilePath . '-' . $intStart . '.jpeg';
			}

			if( NULL != $strImageFile && true == file_exists( $strImageFile ) ) {
				 $arrstrImageNames[] = $strImageFile;
			}
		}

		return $arrstrImageNames;
	}

	private function mergedJpgImages( $arrstrFileImages ) {

		if( false == valArr( $arrstrFileImages ) ) {
			return NULL;
		}

		$arrstrImages = $this->checkImageFilesExist( $arrstrFileImages );

		if( false == valArr( $arrstrImages ) ) {
			return NULL;
		}

		$arrstrImageData = [];
		$intImageCount = \Psi\Libraries\UtilFunctions\count( $arrstrImages );
		$fltWidth = ceil( sqrt( $intImageCount ) );
		$fltHeight = floor( sqrt( $intImageCount / 2 ) );
		$arrfltWidths = [];
		$arrfltHeights = [];
		for( $intCnt = 0; $intCnt < $intImageCount; $intCnt++ ) {
			$arrstrImageData[$intCnt] = getimagesize( $arrstrImages[$intCnt] );
			$boolIsFile = false;
			for( $intCntWidth = 0; $intCntWidth < $intCnt; $intCntWidth++ ) {
				if( $arrstrImageData[$arrfltWidths[$intCntWidth]][0] < $arrstrImageData[$intCnt][0] ) {
					$arrintWidth  = $intCntWidth > 0 ? array_slice( $arrfltWidths, $intCntWidth - 1, $intCnt ) : [];
					$arrfltWidths  = array_merge( $arrintWidth, [ $intCnt ], array_slice( $arrfltWidths, $intCntWidth ) );
					$boolIsFile = true;
					break;
				}
			}

			if( true == $boolIsFile ) {
				$arrfltWidths[$intCnt] = $intCnt;
			}
			$boolIsFile = false;
			for( $intCntWidth = 0; $intCntWidth < $intCnt; $intCntWidth++ ) {
				if( $arrstrImageData[$arrfltHeights[$intCntWidth]][1] < $arrstrImageData[$intCnt][1] ) {
					$arrintWidth  = $intCntWidth > 0 ? array_slice( $arrfltHeights, $intCntWidth - 1, $intCnt ) : [];
					$arrfltHeights  = array_merge( $arrintWidth, [ $intCnt ], array_slice( $arrfltHeights, $intCntWidth ) );
					$boolIsFile = true;
					break;
				}
			}
			if( true == $boolIsFile ) {
				$arrfltHeights[$intCnt] = $intCnt;
			}
		}

		$intWidth = 0;
		for( $intCnt = 0; $intCnt < $fltWidth; $intCnt++ ) {
			$intWidth += $arrstrImageData[$arrfltWidths[$intCnt]][0];
		}

		$intHeight = 0;
		for( $intCnt = 0; $intCnt < $fltHeight; $intCnt++ ) {
			$intHeight += $arrstrImageData[$arrfltHeights[$intCnt]][1];
		}

		$strMergedImage = imagecreatetruecolor( $intWidth, $intHeight );

		$intWidthCount = 0;
		$intWidthStartFrom = 0;
		$intHeightStartFrom = 0;
		for( $intCnt = 0; $intCnt < $intImageCount; $intCnt++ ) {
			$resSourceImage = imagecreatefromjpeg( $arrstrImages[$intCnt] );

			if( false == $resSourceImage ) {
				return NULL;
			}
			$boolIsTrue = imagecopyresampled( $strMergedImage, $resSourceImage, $intWidthStartFrom, $intHeightStartFrom, 0, 0, $arrstrImageData[$intCnt][0], $arrstrImageData[$intCnt][1], $arrstrImageData[$intCnt][0], $arrstrImageData[$intCnt][1] );

			if( false == $boolIsTrue ) {
				return NULL;
			}
			$intWidthCount++;
			if( $intWidthCount == $fltWidth ) {
				$intWidthStartFrom = 0;
				$intHeightStartFrom += $arrstrImageData[$arrfltHeights[0]][1];
				$intWidthCount       = 0;
			} else {
				$intWidthStartFrom += $arrstrImageData[$intCnt][0];
			}
		}

		return $strMergedImage;
	}

	public function valCheckSignedDate( $boolIsReorderOrderFormRequest ) {
		$boolIsValid = true;

		if( true == is_null( $this->getSignedDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Sign Date is required. ' ) );
			return $boolIsValid;
		}

		// if merchant services order form of status is awaiting_signature then don't compare it with today's date
		if( false == is_null( $this->getOrderFormStatusTypeId() ) && COrderFormStatusType::AWAITING_SIGNATURE == $this->getOrderFormStatusTypeId() ) {
			return $boolIsValid;
		}

		if( strtotime( date( 'Y-m-d' ) ) > strtotime( $this->getSignedDate() ) && false == $boolIsReorderOrderFormRequest ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Sign Date should be greater than or equal today\'s date. ' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valcheckSignedName() {

		$boolIsValid = true;

		if( true == is_null( $this->getSignedName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Sign Name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valIsAcknowledgeTerms() {

		$boolIsValid = true;

		if( !$this->getIsAcknowledgeTerms() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please acknowledge to agree the Revisions terms.' ) );
		}

		return $boolIsValid;
	}

	public function valElectronicSign() {

		$boolIsValid = true;

		if( true == is_null( $this->getElectronicSignIpAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Electronic Sign Ip Address is required. ' ) );
		}

		return $boolIsValid;
	}

	public function validateOrderFormRequest( $boolIsReorderOrderFormRequest ) {
		$boolIsValid = true;

		switch( $this->getOrderFormTypeId() ) {
			case COrderFormType::CHECK_SCANNER:
			case COrderFormType::DOMAIN_ORDER_TRANSFER:
			case COrderFormType::DISTRIBUTION_TYPE_CHANGE_REQUEST:
			case COrderFormType::CHECK_SCANNING_MAX_PAYMENT_INCREASE_REQUEST:
				$boolIsValid &= $this->valcheckSignedName();
				$boolIsValid &= $this->valCheckSignedDate( $boolIsReorderOrderFormRequest );
				$boolIsValid &= $this->valElectronicSign();
				break;

			case COrderFormType::FLOOR_PLAN_PACKAGE:
			case COrderFormType::TEMPLATE_LOGO:
			case COrderFormType::CUSTOM_LOGO:
			case COrderFormType::SITE_PLAN:
				$boolIsValid &= $this->valcheckSignedName();
				$boolIsValid &= $this->valIsAcknowledgeTerms();
				$boolIsValid &= $this->valCheckSignedDate( $boolIsReorderOrderFormRequest );
				$boolIsValid &= $this->valElectronicSign();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsReorderOrderFormRequest = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->validateOrderFormRequest( $boolIsReorderOrderFormRequest );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createCompanyCharges( $objOrderFormRequestDetail, $intAccountId ) {
		$strStartDate = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm' ) + 1, 1, date( 'Y' ) ) );
		$objCompanyCharge = new CCompanyCharge();
		$objCompanyCharge->setCid( $this->getCid() );
		$objCompanyCharge->setAccountId( $intAccountId );
		$objCompanyCharge->setChargeCodeId( CChargeCode::DOMAIN_NAME_RENEWAL );
		$objCompanyCharge->setPropertyId( $objOrderFormRequestDetail->getPropertyId() );
		$objCompanyCharge->setChargePeriod( CFrequency::YEARLY );
		$objCompanyCharge->setChargeStartDate( $strStartDate );
		$objCompanyCharge->setChargeAmount( self::DOMAIN_ORDER_TRANSFER_FORM_AMOUNT );
		$objCompanyCharge->setChargeMemo( 'URL: ' . $objOrderFormRequestDetail->getDomainUrl() );
		$objCompanyCharge->setChargeEndDate( NULL );
		$objCompanyCharge->setCommissionStartDate( $strStartDate );

		return $objCompanyCharge;
	}

	public function sendNotificationEmailToBillingTeam( $intUserId, $arrintPropertyIds, $objAdminDatabase ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objClient = CClients::fetchClientById( $this->getCid(), $objAdminDatabase );
		$arrstrPropertyNames = \Psi\Eos\Admin\CProperties::createService()->fetchPropertyNamesByIdsByCid( $arrintPropertyIds, $this->getCid(), $objAdminDatabase );
		if( valArr( $arrstrPropertyNames ) ) {
			$strPropertyNames = current( $arrstrPropertyNames )['property_names'];
		}

		$objSmarty 	= new CPsSmarty( PATH_INTERFACES_ENTRATA, false );

		$objSmarty->assign( 'ps_lead_id', $objClient->getPsLeadId() );
		$objSmarty->assign( 'company_name', $objClient->getCompanyName() );
		$objSmarty->assign( 'property_names', $strPropertyNames );
		$objSmarty->assign( 'order_form_request_id', $this->getId() );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );
		$objSmarty->assign( 'company_url', 'http://' . $_SERVER['HTTP_HOST'] );
		$objSmarty->assign( 'is_reordered', ( COrderFormStatusType::REORDER == $this->getOrderFormStatusTypeId() ) );

		// Mail Body.
		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/dashboard/order_forms/domain_order_transfer_create_charge_notification.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		// Fetch email addresses of users to trigger emails.

		$objEmailDatabase 		= CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::INTERNAL_CLIENT_SERVICES );
		$strSubject 			= ' Domain Name Renewal charge created for ' . $objClient->getCompanyName();
		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_TO_COMPANY_ACCOUNTING, $strSubject, $strHtmlContent, CSystemEmail::PROPERTYSOLUTIONS_BILLING_EMAIL_ADDRESS );

		if( false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
			trigger_error( 'System email failed to insert.', E_USER_ERROR );
			return false;
		}
		return true;
	}

	public function sendDomainOrderRequestNotificationToStakeHolders( $intUserId, $arrintPropertyIds, $objAdminDatabase ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objClient = CClients::fetchClientById( $this->getCid(), $objAdminDatabase );

		if( false == valObj( $objClient, 'CClient' ) ) {
			return false;
		}

		$arrstrPropertyNames = \Psi\Eos\Admin\CProperties::createService()->fetchPropertyNamesByIdsByCid( $arrintPropertyIds, $this->getCid(), $objAdminDatabase );
		$strPropertyNames = NULL;

		if( true == valArr( $arrstrPropertyNames ) ) {
			$strPropertyNames = current( $arrstrPropertyNames )['property_names'];
		}

		$objSmarty 	= new CPsSmarty( PATH_INTERFACES_ENTRATA, false );

		$objSmarty->assign( 'ps_lead_id', $objClient->getPsLeadId() );
		$objSmarty->assign( 'company_name', $objClient->getCompanyName() );
		$objSmarty->assign( 'property_names', $strPropertyNames );
		$objSmarty->assign( 'order_form_request_id', $this->getId() );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );
		$objSmarty->assign( 'CONFIG_COMPANY_BASE_DOMAIN', CONFIG_COMPANY_BASE_DOMAIN );
		$objSmarty->assign( 'is_reorder', ( COrderFormStatusType::REORDER == $this->getOrderFormStatusTypeId() ) );

		$strHtmlContent        = $objSmarty->nestedFetch( PATH_INTERFACES_ENTRATA . 'app_store/order_forms/' . COrderFormType::$c_arrintOrderFormFolderNames[$this->getOrderFormTypeId()] . '/domain_order_transfer_create_request_notification.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$objEmailDatabase      = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::INTERNAL_CLIENT_SERVICES );
		if( COrderFormStatusType::REORDER == $this->getOrderFormStatusTypeId() ) {
			$strSubject            = ' Reorder request for Domain Order is submitted by ' . $objClient->getCompanyName();
		} else {
			$strSubject = ' Domain order form request is submitted for ' . $objClient->getCompanyName();
		}

		$objSystemEmailLibrary = new CSystemEmailLibrary();
		$objSystemEmail        = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::COMPANY_EMPLOYEE, $strSubject, $strHtmlContent, CSystemEmail::DOMAINSUPPORT_EMAIL_ADDRESS );

		if( false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
			trigger_error( 'System email failed to insert.', E_USER_ERROR );

			return false;
		}

		return true;
	}

	public function getPdfToImageFilePath( $strFilePath ) {
		if( 'pdf' == pathinfo( $strFilePath, PATHINFO_EXTENSION ) ) {
			$strJpegFileName = $strFilePath . '.jpeg';
			// In case of PDF file we are showing jpeg file, so while loading the view if jpeg file is already present then we will load that file else will convert pdf to jpeg & then show it
			if( true == CFileIo::fileExists( $strJpegFileName ) ) {
				$strFilePath = $strJpegFileName;
			} else {
				$this->setFilePath( $strFilePath );
				$strFilePath = $this->convertPdfToJpeg();
			}
		}
		return $strFilePath;
	}

}
?>
