<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportSpreadsheets
 * Do not add any new functions to this class.
 */

class CReportSpreadsheets extends CBaseReportSpreadsheets {

	public static function fetchReportSpreadsheetByCompanyReportId( $intCompanyReportId, $objDatabase ) {

		if( false == is_numeric( $intCompanyReportId ) ) {
			return false;
		}

		$strSql = 'SELECT 
						rs.*
					FROM
						report_spreadsheets rs
					JOIN report_details rd ON ( rd.report_spreadsheet_id = rs.id AND rd.is_current_version = true AND rd.report_data_type_id = ' . CReportDataType::SPREADSHEET . '  )
					WHERE rs.company_report_id = ' . ( int ) $intCompanyReportId;

		$arrmixReportDetail = fetchData( $strSql, $objDatabase );
		return $arrmixReportDetail[0];

	}

}
?>