<?php

class CTestTemplate extends CBaseTestTemplate {

	const ID_TEMPLATE_PHP_NINJA			= 59;
	const ID_TEMPLATE_PHP_SAMURAI		= 63;
	const ID_TEMPLATE_PHP_JEDI			= 64;

	public static $c_arrintPhpCertificationTemplates = [
		self::ID_TEMPLATE_PHP_NINJA,
		self::ID_TEMPLATE_PHP_SAMURAI,
		self::ID_TEMPLATE_PHP_JEDI
	];

	/**
	 * Validation Functions
	 *
	 */

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		if( true == $boolIsValid && true == isset( $objDatabase ) ) {

			$strSql = ' WHERE name = \'' . trim( addslashes( $this->m_strName ) ) . '\' AND id!=' . ( int ) $this->m_intId;
			$intCount = CTests::fetchRowCount( $strSql, 'test_templates', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is already in use.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valTestLevelTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTestLevelTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_level_type_id', 'Template level is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valTestLevelTypeId( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

}
?>