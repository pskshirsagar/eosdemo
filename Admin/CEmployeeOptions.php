<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeOptions
 * Do not add any new functions to this class.
 */

class CEmployeeOptions extends CBaseEmployeeOptions {

	public static function fetchPaginatedEmployeeOptions( $arrstrEmployeeOptionsFilter, $objAdminDatabase, $boolIsForCount = false ) {

		if( true == empty( $arrstrEmployeeOptionsFilter['viewer_employee_id'] ) ) return NULL;

		if( true == isset( $arrstrEmployeeOptionsFilter['order_by_field'] ) ) {

			switch( $arrstrEmployeeOptionsFilter['order_by_field'] ) {

				case 'employee_name':
					$strOrderByField = ' employee_name';
					break;

				case 'vesting_schedule_name':
					$strOrderByField = ' vesting_schedule_name ';
					break;

				case 'option_datetime':
					$strOrderByField = ' eo.option_datetime ';
					break;

				case 'grant_date':
					$strOrderByField = ' eo.grant_date ';
					break;

				case 'vesting_commencement_date':
					$strOrderByField = ' eo.vesting_commencement_date ';
					break;

				case 'final_vesting_date':
					$strOrderByField = ' eo.final_vesting_date ';
					break;

				default:
					$strOrderByField = ' employee_name ';
					break;
			}
		}

		$strOrderByField  	= ( false == empty( $strOrderByField ) ) ? $strOrderByField : 'employee_name';
		$strOrderByType  	= ( false == empty( $arrstrEmployeeOptionsFilter['order_by_type'] ) ) ? $arrstrEmployeeOptionsFilter['order_by_type'] : 'ASC';
		$intOffset			= ( false == $boolIsForCount && false == empty( $arrstrEmployeeOptionsFilter['page_no'] ) && false == empty( $arrstrEmployeeOptionsFilter['page_size'] ) ) ? $arrstrEmployeeOptionsFilter['page_size'] * ( $arrstrEmployeeOptionsFilter['page_no'] - 1 ) : 0;
		$intLimit			= ( false == $boolIsForCount && true == isset( $arrstrEmployeeOptionsFilter['page_size'] ) ) ? $arrstrEmployeeOptionsFilter['page_size'] : '';

		$strSelectClause	= ( false == $boolIsForCount ) ? 'eo.*, e.name_full AS employee_name, vs.name AS vesting_schedule_name, options.encrypted_values AS option_encrypted, exercise_price.encrypted_values AS exercise_price_encrypted' : 'eo.id, e.name_full AS employee_name, vs.name AS vesting_schedule_name';

		$strSql = ' SELECT
						' . $strSelectClause . '
					FROM
						employee_options eo
						JOIN vesting_schedules vs ON( vs.id = eo.vesting_schedule_id)
						LEFT JOIN employee_option_associations options ON( options.id = eo.employee_option_association_id AND options.viewer_employee_id = ' . ( int ) $arrstrEmployeeOptionsFilter['viewer_employee_id'] . ' )
						LEFT JOIN employee_option_associations exercise_price ON( exercise_price.id = eo.exercise_price_association_id AND exercise_price.viewer_employee_id = ' . ( int ) $arrstrEmployeeOptionsFilter['viewer_employee_id'] . ' )
						JOIN employees e ON( e.id = eo.employee_id)
					ORDER BY
						' . $strOrderByField . ' ' . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( true == $boolIsForCount ) {
			$arrintVestingSchedulesCount = fetchData( $strSql, $objAdminDatabase );

			return ( true == valArr( $arrintVestingSchedulesCount ) ) ? \Psi\Libraries\UtilFunctions\count( $arrintVestingSchedulesCount ) : 0;
		}

		$strSql .= ( 0 < $intLimit ) ? ' LIMIT ' . ( int ) $intLimit : '';

		return self::fetchEmployeeOptions( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeOptionByIdByViewerEmployeeId( $intEmployeeOptionId, $intViewerEmployeeId, $objAdminDatabase ) {

		$strSql = ' SELECT
						eo.*,
						options.encrypted_values AS option_encrypted,
						exercise_price.encrypted_values AS exercise_price_encrypted
					FROM
						employee_options eo
						LEFT JOIN employee_option_associations options ON( options.id = eo.employee_option_association_id AND options.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						LEFT JOIN employee_option_associations exercise_price ON( exercise_price.id = eo.exercise_price_association_id AND exercise_price.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
					WHERE
						eo.id = ' . ( int ) $intEmployeeOptionId;

		return self::fetchEmployeeOption( $strSql, $objAdminDatabase );
	}
}
?>