<?php

class CTeamSprintDetail extends CBaseTeamSprintDetail {

	const MIN_REMAINING_HOURS	= 0;
	const MAX_REMAINING_HOURS	= 50;
	const SCROLL_TASK_COUNT	= 7;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTeamSprintId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemainingHours() {
		$boolIsValid = true;
		if( true == is_numeric( $this->getRemainingHours() ) && ( self::MIN_REMAINING_HOURS > $this->getRemainingHours() || self::MAX_REMAINING_HOURS < $this->getRemainingHours() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'remaining_hours', 'Remaining Hours must be between 0 to 50.' ) );
		}
		return $boolIsValid;
	}

	public function valRemainingStoryPoints() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRemainingHours();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>