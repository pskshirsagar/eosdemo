<?php

class CContractCommit extends CBaseContractCommit {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsLeadId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSalesCloserEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCloseDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCloseLiklihood() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommitMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAcvCommitted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledCloseMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommitDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>