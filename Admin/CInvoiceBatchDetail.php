<?php

class CInvoiceBatchDetail extends CBaseInvoiceBatchDetail {

	public function valId() {
		return true;
	}

	public function valInvoiceBatchId() {
		return true;
	}

	public function valTrack1BilledAmount() {
		return true;
	}

	public function valTrack2BilledAmount() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>