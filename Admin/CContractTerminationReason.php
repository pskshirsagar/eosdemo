<?php

class CContractTerminationReason extends CBaseContractTerminationReason {

	protected $m_arrstrContractTerminationReasons;

	const DUPLICATE_CONTRACT 				= 3;
	const FAILED_UNDERWRITING				= 5;
	const LOST_MANAGEMENT					= 6;
	const DISSATISFIED_WITH_PRODUCT			= 9;
	const SOLD_ASSET_MANAGEMENT_LOST		= 10;
	const DISSATISFIED_WITH_SERVICE			= 11;
	const FAILED_PILOT						= 14;
	const BUDGETARY_CONSTRAINTS				= 15;
	const OTHER								= 16;
	const INTEGRATION_ISSUE					= 17;
	const TRANSFER_PROPERTY					= 24;
	const NON_PAYMENT						= 25;
	const LITIGATION						= 26;
	const SOLD_ASSET_MANAGEMENT_RETAINED	= 27;
	const BUNDLE_OR_NO_INTENT_TO_USE		= 28;
	const REBUNDLE_REPRICE					= 29;
	const NO_REASON_GIVEN					= 30;

	public static $c_arrintDirectContractTerminationReasons = [ self::LOST_MANAGEMENT, self::SOLD_ASSET_MANAGEMENT_LOST ];
	public static $c_arrintIgnoreContractTerminationReasons = [ self::DUPLICATE_CONTRACT, self::TRANSFER_PROPERTY ];

	public static $c_arrintExcludeContractTerminationReasonsOnAppStore = [
		self::FAILED_UNDERWRITING,
		self::DUPLICATE_CONTRACT,
		self::FAILED_PILOT,
		self::NON_PAYMENT,
		self::LITIGATION,
		self::TRANSFER_PROPERTY,
		self::BUNDLE_OR_NO_INTENT_TO_USE,
		self::REBUNDLE_REPRICE,
		self::NO_REASON_GIVEN
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getContractTerminationReasons() {
		$this->loadContractTerminationReasons();

		return $this->m_arrstrContractTerminationReasons;
	}

	private function loadContractTerminationReasons() {
		if( false == empty( $this->m_arrstrContractTerminationReasons ) ) {
			return;
		}

		$this->m_arrstrContractTerminationReasons = [
			self::DUPLICATE_CONTRACT		        => __( 'Duplicate Contract' ),
			self::FAILED_UNDERWRITING		        => __( 'Failed Underwriting' ),
			self::LOST_MANAGEMENT			        => __( 'Loss of management' ),
			self::DISSATISFIED_WITH_PRODUCT	        => __( 'Dissatisfied with product(s)' ),
			self::SOLD_ASSET_MANAGEMENT_LOST        => __( 'Sale of property (management lost)' ),
            self::SOLD_ASSET_MANAGEMENT_RETAINED	=> __( 'Sale of property (management retained)' ),
			self::DISSATISFIED_WITH_SERVICE	        => __( 'Dissatisfied with service' ),
			self::FAILED_PILOT				        => __( 'Failed Pilot' ),
			self::BUDGETARY_CONSTRAINTS		        => __( 'Budgetary constraints' ),
			self::OTHER						        => __( 'Other' ),
			self::TRANSFER_PROPERTY			        => __( 'Transfer Property' ),
			self::INTEGRATION_ISSUE			        => __( 'Integration Issue' ),
			self::LITIGATION				        => __( 'Litigation' ),
			self::NON_PAYMENT				        => __( 'Non Payment' ),
			self::BUNDLE_OR_NO_INTENT_TO_USE		=> __( 'Bundle/No Intent to Use' ),
			self::REBUNDLE_REPRICE					=> __( 'Rebundle/Reprice' ),
			self::NO_REASON_GIVEN					=> __( 'No Reason Given' )
		];
	}

	public static $c_arrstrContractTerminationReasons = [
		self::DUPLICATE_CONTRACT		        => 'Duplicate Contract',
		self::FAILED_UNDERWRITING		        => 'Failed Underwriting',
		self::LOST_MANAGEMENT			        => 'Loss of management',
		self::DISSATISFIED_WITH_PRODUCT	        => 'Dissatisfied with product(s)',
		self::SOLD_ASSET_MANAGEMENT_LOST        => 'Sale of property (management lost)',
        self::SOLD_ASSET_MANAGEMENT_RETAINED	=> 'Sale of property (management retained)',
		self::DISSATISFIED_WITH_SERVICE	        => 'Dissatisfied with service',
		self::FAILED_PILOT				        => 'Failed Pilot',
		self::BUDGETARY_CONSTRAINTS		        => 'Budgetary constraints',
		self::OTHER						        => 'Other',
		self::TRANSFER_PROPERTY			        => 'Transfer Property',
		self::INTEGRATION_ISSUE			        => 'Integration Issue',
		self::LITIGATION				        => 'Litigation',
		self::NON_PAYMENT				        => 'Non Payment',
		self::BUNDLE_OR_NO_INTENT_TO_USE		=> 'Bundle/No Intent to Use',
		self::REBUNDLE_REPRICE					=> 'Rebundle/Reprice',
		self::NO_REASON_GIVEN					=> 'No Reason Given'
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>