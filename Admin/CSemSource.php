<?php

class CSemSource extends CBaseSemSource {

	const GOOGLE		= 1;
	const BING 			= 2;
	const YAHOO 		= 3;
	const VACANCY 		= 4;

	/**
	 * Fetch Functions
	 */

	public function fetchSemAccounts( $objAdminDatabase ) {
		return CSemAccounts::fetchSemAccountsBySemSourceId( $this->getId(), $objAdminDatabase );
	}

	public function fetchRandomSemAccount( $objAdminDatabase ) {
		return CSemAccounts::fetchRandomSemAccountBySemSourceId( $this->getId(), $objAdminDatabase );
	}

	public function fetchSemCampaignSources( $objAdminDatabase ) {
		return CSemCampaignSources::fetchSemCampaignSourcesBySemSourceId( $this->getId(), $objAdminDatabase );
	}

	public function fetchSemTransactionsByDate( $strTransactionDate, $objAdminDatabase ) {
		return CSemTransactions::fetchSemTransactionsBySemSourceIdByDate( $this->getId(), $strTransactionDate, $objAdminDatabase );
	}

	public function fetchSemKeywordSourcesBySemKeywordIds( $arrintSemKeywordIds, $objAdminDatabase ) {

		if( false == valArr( $arrintSemKeywordIds ) ) return NULL;

		return CSemKeywordSources::fetchSemKeywordSourcesBySemSourceIdBySemKeywordIds( $this->getId(), $arrintSemKeywordIds, $objAdminDatabase );
	}

	/**
	 * Create Functions
	 */

	public function createSemController( $objAdminDatabase ) {

		switch( $this->getId() ) {

			case CSemSource::GOOGLE:
				$objSemController = new CGoogleSemController();
				break;

			case CSemSource::BING:
				return  NULL;
				break;

			case CSemSource::YAHOO:
				return NULL;
				break;

			default:
				$boolIsValid = false;
				break;
		}

		$objSemController->setSemSource( $this );
		$objSemController->setAdminDatabase( $objAdminDatabase );

		return  $objSemController;
	}
}
?>