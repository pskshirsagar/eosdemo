<?php

class CPersonContactType extends CBasePersonContactType {

	const MANAGER				= 2;
	const OWNER					= 3;
	const MAINTENANCE   		= 4;
	const ACCOUNTING			= 5;
	const IMPLEMENTATION		= 6;
	const FAILED_SETTLEMENT		= 7;
	const OTHER					= 8;
	const MARKETING				= 9;
	const TECHNICAL				= 10;
	const ADDITIONAL_MERCHANT	= 11;
	const OUTAGE_SMS			= 12;
	const OUTAGE_EMAIL			= 13;
	const BLOCK_MASS_EMAIL		= 14;
	const ADVERTISEMENT			= 15;
	const SALES_CALL 			= 16;
	const EMAIL 				= 17;
	const POPUP 				= 18;
	const PHONE 				= 19;
	const INTEGRATION_OUTAGE	= 20;
	const RED_SHOE_CLUB			= 21;
	const KEY_CONTACTS			= 22;
	const TERMINATION_EMAIL		= 23;

	public static $c_arrintCorporateContactTypes = array( self::ACCOUNTING, self::ADDITIONAL_MERCHANT, self::ADVERTISEMENT, self::BLOCK_MASS_EMAIL, self::FAILED_SETTLEMENT, self::IMPLEMENTATION, self::MAINTENANCE, self::MARKETING, self::OWNER, self::OUTAGE_EMAIL, self::OUTAGE_SMS, self::SALES_CALL, self::TECHNICAL, self::EMAIL );

	// Allows a not in condition to return the corporate contacts that aren't primary
	public static $c_arrintAntiNonPrimaryCorporateContacts = array( self::OTHER, self::MANAGER );
	public static $c_arrintActivePersonContactTypeIds	   = array( self::OWNER, self::MAINTENANCE, self::ACCOUNTING, self::IMPLEMENTATION, self::FAILED_SETTLEMENT, self::MARKETING, self::TECHNICAL, self::ADDITIONAL_MERCHANT, self::OUTAGE_SMS, self::OUTAGE_EMAIL, self::BLOCK_MASS_EMAIL, self::ADVERTISEMENT, self::SALES_CALL, self::EMAIL );

	public static $c_arrintSelectedPersonContactTypeIds    = array( self::OWNER, self::MAINTENANCE, self::ACCOUNTING, self::IMPLEMENTATION, self::FAILED_SETTLEMENT, self::MARKETING, self::TECHNICAL, self::ADDITIONAL_MERCHANT, self::OUTAGE_SMS, self::OUTAGE_EMAIL, self::BLOCK_MASS_EMAIL, self::ADVERTISEMENT, self::SALES_CALL, self::EMAIL );

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>