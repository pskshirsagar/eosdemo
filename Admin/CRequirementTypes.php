<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CRequirementTypes
 * Do not add any new functions to this class.
 */

class CRequirementTypes extends CBaseRequirementTypes {

	public static function fetchAllRequirementTypesByCountryCode( $strCountryCode, $objDatabase ) {

		$strWhereClause = ( CCountry::CODE_INDIA == $strCountryCode ) ? ' WHERE id <> ' . CRequirementType::VISA_PETITION : '';
		$strSql			= 'SELECT * FROM requirement_types' . $strWhereClause;

		return self::fetchRequirementTypes( $strSql, $objDatabase );
	}

}
?>