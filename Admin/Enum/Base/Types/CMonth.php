<?php

class CMonth {

	const JANUARY   = 'January';
	const FEBRUARY  = 'February';
	const MARCH     = 'March';
	const APRIL     = 'April';
	const MAY       = 'May';
	const JUNE      = 'June';
	const JULY      = 'July';
	const AUGUST    = 'August';
	const SEPTEMBER = 'September';
	const OCTOBER   = 'October';
	const NOVEMBER  = 'November';
	const DECEMBER  = 'December';

	public static $c_arrstrMonths = [
		self::JANUARY,
		self::FEBRUARY,
		self::MARCH,
		self::APRIL,
		self::MAY,
		self::JUNE,
		self::JULY,
		self::AUGUST,
		self::SEPTEMBER,
		self::OCTOBER,
		self::NOVEMBER,
		self::DECEMBER
	];
}
?>