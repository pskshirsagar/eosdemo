<?php

class CCompensationGradeValue {

	const D         = 'D';
	const C         = 'C';
	const B         = 'B';
	const A         = 'A';
	const APLUS     = 'A+';
	const APLUSPLUS = 'A++';

	public static $c_arrstrCompensationGradeValues = [
		self::D,
		self::C,
		self::B,
		self::A,
		self::APLUS
	];
}
?>