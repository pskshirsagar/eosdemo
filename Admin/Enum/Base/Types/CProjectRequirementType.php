<?php

class CProjectRequirementType {

	const FEEDBACK = 'Feedback';
	const CROSS_PRODUCT_REQUEST = 'Cross Product Request';
	const NON_URGENT_BUG = 'Non-Urgent Bug';
	const TECHNICAL_DEBT = 'Technical Debt';

	public static $c_arrstrProjectRequirementTypes	= [
		'1' => 'Feedback',
		'2' => 'Cross Product Request',
		'3' => 'Non-Urgent Bug',
		'4' => 'Technical Debt'
	];

}
?>