<?php

class CProjectRequirementSourceType {

	const CLIENT = 'Client';
	const INTERNAL = 'Internal';

	public static $c_arrstrProjectRequirementSourceTypes	= [
		self::CLIENT,
		self::INTERNAL
	];
}
?>