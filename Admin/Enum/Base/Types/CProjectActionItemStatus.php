<?php

class CProjectActionItemStatus {

	const NEW = 'New';
	const IN_PROGRESS = 'In Progress';
	const READY_FOR_REVIEW = 'Ready for Review';
	const CANCELLED = 'Cancelled';
	const COMPLETED = 'Completed';

}
?>