<?php

class CRecruitmentDriveType {

	const SCHEDULED = 'Scheduled';
	const WALK_IN = 'Walk In';
	const MIXED = 'Mixed';

}
?>