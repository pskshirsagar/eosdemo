<?php

class CDataPrivacyRequestStatusType {
	const NEW = 'New';
	const IN_PROGRESS = 'In Progress';
	const COMPLETED = 'Completed';
	const DENIED = 'Denied';
	const ARCHIVED = 'Archived';
	const WAITING_CLIENT_REVIEW = 'Waiting Client Review';
}

?>
