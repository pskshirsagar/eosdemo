<?php

class CPurchaseRequestActionType {

	const REMOVE			= 'Remove';
	const MERGE				= 'Merge';
	const HOLD				= 'Hold';
	const FILL				= 'Fill';
	const PROMOTE			= 'Promote';
	const SPLIT				= 'Split';
	const INTERNAL_TOGGLE	= 'Internal Toggle';
	const EXTERNAL_TOGGLE	= 'External Toggle';
	const REOPEN			= 'Reopen';
	const PR_ON_HOLD		= 'PR On Hold';
	const WORK_IN_PROGRESS	= 'Work In Progress';
	const OFFER_DECLINED	= 'Offer Declined';
	const PR_CHANGED		= 'PR Changed';
	const OFFER_EXTENDED	= 'Offer Extended';
	const OFFER_ACCEPTED	= 'Offer Accepted';
	const ALLOCATED			= 'Allocated';

	public static $c_arrstrIndianPurchaseRequestActionTypes = [
		self::WORK_IN_PROGRESS,
		self::OFFER_DECLINED,
		self::PR_CHANGED,
		self::INTERNAL_TOGGLE,
		self::EXTERNAL_TOGGLE,
		self::REOPEN,
		self::PR_ON_HOLD,
		self::OFFER_EXTENDED,
		self::OFFER_ACCEPTED,
		self::ALLOCATED
	];

	public static $c_arrstrPurchaseRequestCloseActionTypes = [
		self::PR_ON_HOLD,
		self::OFFER_EXTENDED,
		self::OFFER_ACCEPTED,
		self::ALLOCATED
	];

	public static $c_arrstrPurchaseRequestOpenActionTypes = [
		self::WORK_IN_PROGRESS,
		self::OFFER_DECLINED,
		self::PR_CHANGED,
		self::REOPEN
	];

	public static $c_arrstrPurchaseRequestToggleActionTypes = [
		self::INTERNAL_TOGGLE,
		self::EXTERNAL_TOGGLE
	];
}
?>