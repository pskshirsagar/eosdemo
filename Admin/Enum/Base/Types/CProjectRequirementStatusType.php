<?php

class CProjectRequirementStatusType {

	const DISCOVERY = 'Discovery';
	const STRATEGY_READY = 'Strategy_Ready';
	const IN_STRATEGY = 'In_Strategy';
	const COMPLETED = 'Completed';

}
?>