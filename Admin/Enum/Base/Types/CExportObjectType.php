<?php

class CExportObjectType {

	const CLIENT = 'Client';
	const INVOICE = 'Invoice';
	const PAYMENT = 'Payment';
	const DEPOSIT = 'Deposit';
	const ALLOCATION = 'Allocation';
	const CHARGE_CODE = 'Charge Code';
	const CREDIT_MEMO = 'Credit Memo';
	const RETURN_PAYMENT = 'Return Payment';
	const PAYMENT_TO_CREDIT_MEMO = 'Payment To Credit_Memo';
	const RETURN_PAYMENT_TO_INVOICE = 'Return Payment To Invoice';
	const TAX_CODE = 'Tax Code';
	const PRODUCT = 'Product';
	const CHART_OF_ACCOUNT = 'Chart Of Account';
	const GENERAL_ERROR = 'General Error';

}
?>