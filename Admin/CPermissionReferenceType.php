<?php

class CPermissionReferenceType extends CBasePermissionReferenceType {

	const DEPARTMENT	= 1;
	const TEAM			= 2;
	const EMPLOYEE		= 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>