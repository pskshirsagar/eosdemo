<?php

use Psi\Eos\Admin\CInvoices;
use Psi\Eos\Admin\CTransactions;
use Psi\Eos\Admin\CAccounts;

class CCompanyPayment extends CBaseCompanyPayment {

	const VALIDATE_POST 			= 'post';
	const VALIDATE_RETURN_FEE_POST 	= 'return_fee_post';
	const VALIDATE_CARD_PAYMENT 	= 'card_payment';

	const VISA_AND_MASTERCARD_CUT_OFF_HOUR			= 23;
	const VISA_AND_MASTERCARD_CUT_OFF_MINUTE		= 59;

	const DISCOVER_CUT_OFF_HOUR						= 22;
	const DISCOVER_CUT_OFF_MINUTE					= 59;

	const AMEX_CUT_OFF_HOUR							= 15;
	const AMEX_CUT_OFF_MINUTE						= 59;

	/**
	 * The following constants are used in messages sent to the EFT Payment system
	 */
	const COMPANY_PAYMENT_EFT_TYPE_ID				= 3;
	const COMPANY_PAYMENT_SERVICE_CLIENT_ID			= 4;

	protected $m_objClient;
	protected $m_objInvoice;
	protected $m_objAccount;
	protected $m_objTransaction;

	protected $m_intChargeCodeId;
	protected $m_intSelected;
	protected $m_intIsIntermediaryAccount;
	protected $m_intDuplicateCounter;
	protected $m_intPsLeadId;
	protected $m_intInvoiceNumber;

	protected $m_strClientName;
	protected $m_strAuthToken;
	protected $m_strPaymentTypeName;
	protected $m_strPaymentStatusTypeName;
	protected $m_strCheckAccountTypeName;
	protected $m_strAccountName;
	protected $m_strBilltoCountryName;
	protected $m_strCompanyPaymentIds;
	protected $m_strCheckConfirmAccountNumberEncrypted;

	protected $m_arrobjCompanyPaymentTransactions;

	protected $m_fltReversedAmount;
	protected $m_fltInvoiceAmount;
	protected $m_fltAccountBalance;
	protected $m_fltCheckAmount;

	protected $m_boolIsElectronic;
	protected $m_boolUseCustomBankAccount;

	public function __construct() {
		parent::__construct();
		$this->m_fltReversedAmount = 0;
		$this->m_boolUseCustomBankAccount = false;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getSelected() {
		return $this->m_intSelected;
	}

	public function getCompanyPaymentTransactions() {
		return $this->m_arrobjCompanyPaymentTransactions;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getIsIntermediaryAccount() {
		return $this->m_intIsIntermediaryAccount;
	}

	public function getCid() {

		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient->getId();
		} else {
			return parent::getCid();
		}
	}

	public function getPaymentDate() {
		if( true == isset( $this->m_strPaymentDatetime ) ) {
			$intPaymentDatetime = strtotime( $this->m_strPaymentDatetime );
		if( 0 <= $intPaymentDatetime ) {
			return date( 'm/d/Y', $intPaymentDatetime );
		}
		}
	return NULL;
	}

	public function getChargeCodeId() {
		return $this->m_intChargeCodeId;
	}

	public function getDuplicateCounter() {
		return $this->m_intDuplicateCounter;
	}

	public function getInvoiceId() {
		return $this->m_intInvoiceId;
	}

	public function getAccount() {
		return $this->m_objAccount;
	}

	public function getCcCardNumber() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_CC_CARD_NUMBER ];
		return( true == isset( $this->m_strCcCardNumberEncrypted ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCcCardNumberEncrypted, CONFIG_SODIUM_KEY_CC_CARD_NUMBER, $arrmixOptions ) : NULL;
	}

	public function getCheckAccountNumber() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ];
		return( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, $arrmixOptions ) : NULL;
	}

	public function getCheckConfirmAccountNumber() {
		$arrmixOptions = [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ];
		return( true == isset( $this->m_strCheckConfirmAccountNumberEncrypted ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCheckConfirmAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, $arrmixOptions ) : NULL;
	}

	public function getCcCardNumberMasked() {
		if( false == valStr( $this->getCcCardNumber() ) ) {
			return NULL;
		}

		$strCcCardNumber = $this->getCcCardNumber();
		$intStringLength = \Psi\CStringService::singleton()->strlen( $strCcCardNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCcCardNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCheckAccountNumberMasked() {
		if( false == valStr( $this->getCheckAccountNumber() ) ) {
			return NULL;
		}

		$strCheckAccountNumber = $this->getCheckAccountNumber();
		$intStringLength = \Psi\CStringService::singleton()->strlen( $strCheckAccountNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getAuthToken() {
		return $this->m_strAuthToken;
	}

	public function getPaymentTypeName() {
		return $this->m_strPaymentTypeName;
	}

	public function getPaymentStatusTypeName() {
		return $this->m_strPaymentStatusTypeName;
	}

	public function getCheckAccountTypeName() {
		return $this->m_strCheckAccountTypeName;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getBilltoCountryName() {
		return $this->m_strBilltoCountryName;
	}

	public function getReversedAmount() {
		return $this->m_fltReversedAmount;
	}

	public function getInvoiceAmount() {
		return $this->m_fltInvoiceAmount;
	}

	public function getAccountBalance() {
		return $this->m_fltAccountBalance;
	}

	public function getCheckAmount() {
		return $this->m_fltCheckAmount;
	}

	public function getIsElectronic() {
		return $this->m_boolIsElectronic;
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function getCompanyPaymentIds() {
		return $this->m_strCompanyPaymentIds;
	}

	public function getInvoiceNumber() {
		return $this->m_intInvoiceNumber;
	}

	public function getUseCustomBankAccount() {
		return $this->m_boolUseCustomBankAccount;
	}

	public function getCheckBankNumber() {
		if( !valStr( $this->getCheckRoutingNumber() ) ) {
			return NULL;
		}

		if( 8 == strlen( $this->getCheckRoutingNumber() ) ) {
			$this->setCheckRoutingNumber( '0' . $this->getCheckRoutingNumber() );
		}

		return substr( $this->getCheckRoutingNumber(), 0, 4 );
	}

	public function getCheckBankTransitNumber() {
		if( !valStr( $this->getCheckRoutingNumber() ) ) {
			return NULL;
		}

		if( 8 == strlen( $this->getCheckRoutingNumber() ) ) {
			$this->setCheckRoutingNumber( '0' . $this->getCheckRoutingNumber() );
		}

		return substr( $this->getCheckRoutingNumber(), 4 );
	}

	/**
	 * Set Functions
	 *
	 */

	public function setDefaults() {

		$this->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
		$this->setPaymentDatetime( date( 'm/d/Y H:i:s' ) );

		$this->setBilltoIpAddress( getRemoteIpAddress() );
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['cc_card_number'] ) ) {
			$this->setCcCardNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['cc_card_number'] ) : $arrstrValues['cc_card_number'] );
		}
		if( true == isset( $arrstrValues['check_account_number'] ) ) {
			$this->setCheckAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['check_account_number'] ) : $arrstrValues['check_account_number'] );
		}
		if( true == isset( $arrstrValues['check_routing_number'] ) ) {
			$this->setCheckRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['check_routing_number'] ) : $arrstrValues['check_routing_number'] );
		}
		if( true == isset( $arrstrValues['charge_code_id'] ) ) {
			$this->setChargeCodeId( $arrstrValues['charge_code_id'] );
		}
		if( true == isset( $arrstrValues['invoice_id'] ) ) {
			$this->setInvoiceId( $arrstrValues['invoice_id'] );
		}
		if( true == isset( $arrstrValues['payment_date'] ) ) {
			$this->setPaymentDatetime( $arrstrValues['payment_date'] );
		}
		if( true == isset( $arrstrValues['is_intermediary_account'] ) ) {
			$this->setIsIntermediaryAccount( $arrstrValues['is_intermediary_account'] );
		}
		if( true == isset( $arrstrValues['company_name'] ) ) {
			$this->setClientName( $arrstrValues['company_name'] );
		}
		if( true == isset( $arrstrValues['payment_type_name'] ) ) {
			$this->setPaymentTypeName( $arrstrValues['payment_type_name'] );
		}
		if( true == isset( $arrstrValues['payment_status_type_name'] ) ) {
			$this->setPaymentStatusTypeName( $arrstrValues['payment_status_type_name'] );
		}
		if( true == isset( $arrstrValues['reversed_amount'] ) ) {
			$this->setReversedAmount( $arrstrValues['reversed_amount'] );
		}
		if( true == isset( $arrstrValues['invoice_amount'] ) ) {
			$this->setInvoiceAmount( $arrstrValues['invoice_amount'] );
		}
		if( true == isset( $arrstrValues['check_amount'] ) ) {
			$this->setCheckAmount( $arrstrValues['check_amount'] );
		}
		if( true == isset( $arrstrValues['ps_lead_id'] ) ) {
			$this->setPsLeadId( $arrstrValues['ps_lead_id'] );
		}
		if( true == isset( $arrstrValues['check_account_type_name'] ) ) {
			$this->setCheckAccountTypeName( $arrstrValues['check_account_type_name'] );
		}
		if( true == isset( $arrstrValues['account_name'] ) ) {
			$this->setAccountName( $arrstrValues['account_name'] );
		}
		if( true == isset( $arrstrValues['country_name'] ) ) {
			$this->setBilltoCountryName( $arrstrValues['country_name'] );
		}
		if( true == isset( $arrstrValues['company_payment_ids'] ) ) {
			$this->setCompanyPaymentIds( $arrstrValues['company_payment_ids'] );
		}
		if( true == isset( $arrstrValues['auth_token'] ) ) {
			$this->setAuthToken( $arrstrValues['auth_token'] );
		}
		if( true == isset( $arrstrValues['invoice_number'] ) ) {
			$this->setInvoiceNumber( $arrstrValues['invoice_number'] );
		}
	}

	public function addCompanyPaymentTransaction( $objCompanyPaymentTransaction ) {
		$this->m_arrobjCompanyPaymentTransactions[$objCompanyPaymentTransaction->getId()] = $objCompanyPaymentTransaction;
	}

	public function setSelected( $intSelected ) {
		$this->m_intSelected = $intSelected;
	}

	public function setAccount( $objAccount ) {
		$this->m_objAccount = $objAccount;
	}

	public function setInvoiceId( $intInvoiceId ) {
		$this->m_intInvoiceId = CStrings::strToIntDef( $intInvoiceId, NULL, false );
	}

	public function setIsIntermediaryAccount( $intIsIntermediaryAccount ) {
		$this->m_intIsIntermediaryAccount = $intIsIntermediaryAccount;
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		if( false == valStr( $strCheckAccountNumber ) || true == \Psi\CStringService::singleton()->stristr( $strCheckAccountNumber, 'X' ) ) {
			return;
		}
		$strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 20, NULL, true );
		$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
		$this->setCheckAccountNumberBindex( \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $strCheckAccountNumber, CONFIG_SODIUM_KEY_BLIND_INDEX ) );
	}

	public function setCheckConfirmAccountNumber( $strCheckConfirmAccountNumber ) {
		if( false == valStr( $strCheckConfirmAccountNumber ) || true == \Psi\CStringService::singleton()->stristr( $strCheckConfirmAccountNumber, 'X' ) ) {
			return;
		}
		$strCheckConfirmAccountNumber = CStrings::strTrimDef( $strCheckConfirmAccountNumber, 20, NULL, true );
		$this->setCheckConfirmAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckConfirmAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

	public function setCheckConfirmAccountNumberEncrypted( $strCheckConfirmAccountNumberEncrypted ) {
		$this->m_strCheckConfirmAccountNumberEncrypted = CStrings::strTrimDef( $strCheckConfirmAccountNumberEncrypted, 240, NULL, true );
	}

	public function setCcCardNumber( $strCcCardNumber ) {
		if( false == valStr( $strCcCardNumber ) ) {
			return;
		}

		$strCcCardNumber = CStrings::strTrimDef( $strCcCardNumber, 20, NULL, true );
		$this->setCcCardNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCcCardNumber, CONFIG_SODIUM_KEY_CC_CARD_NUMBER ) );
	}

	public function setChargeCodeId( $intChargeCodeId ) {
		$this->m_intChargeCodeId = CStrings::strToIntDef( $intChargeCodeId, NULL, true );
	}

	public function setCid( $intCid ) {
		parent::setCid( $intCid );
		// If we are setting the client id directly we unbind the
		// client object to avoid any concurrency issues
		$this->m_objClient = NULL;
	}

	public function setClient( $objClient ) {
		if( false == valObj( $objClient, 'CClient' ) ) {
			trigger_error( 'Incompatible types: \'CClient\' and \'' . get_class( $objClient ) . '\' - CCompanyMerchantAccount::setClient()', E_USER_ERROR );
		return false;
		}

		$this->m_objClient = $objClient;
		return true;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setAuthToken( $strAuthToken ) {
		$this->m_strAuthToken = $strAuthToken;
	}

	public function setPaymentTypeName( $strPaymentTypeName ) {
		$this->m_strPaymentTypeName = $strPaymentTypeName;
	}

	public function setPaymentStatusTypeName( $strPaymentStatusTypeName ) {
		$this->m_strPaymentStatusTypeName = $strPaymentStatusTypeName;
	}

	public function setCheckAccountTypeName( $strCheckAccountTypeName ) {
		$this->m_strCheckAccountTypeName = $strCheckAccountTypeName;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setBilltoCountryName( $strBilltoCountryName ) {
		$this->m_strBilltoCountryName = $strBilltoCountryName;
	}

	public function setReversedAmount( $fltReversedAmount ) {
	$this->m_fltReversedAmount = $fltReversedAmount;
	}

	public function setInvoiceAmount( $fltInvoiceAmount ) {
		$this->m_fltInvoiceAmount = $fltInvoiceAmount;
	}

	public function setAccountBalance( $fltAccountBalance ) {
		$this->m_fltAccountBalance = $fltAccountBalance;
	}

	public function setCheckAmount( $fltCheckAmount ) {
		$this->m_fltCheckAmount = $fltCheckAmount;
	}

	public function setIsElectronic( $boolIsElectronic ) {
	$this->m_boolIsElectronic = $boolIsElectronic;
	}

	public function setPsLeadId( $intPsLeadId ) {
		return $this->m_intPsLeadId = $intPsLeadId;
	}

	public function setCompanyPaymentIds( $strCompanyPaymentIds ) {
		$this->m_strCompanyPaymentIds = $strCompanyPaymentIds;
	}

	public function setInvoiceNumber( $intInvoiceNumber ) {
		$this->m_intInvoiceNumber = $intInvoiceNumber;
	}

	public function setUseCustomBankAccount( $boolUseCustomBankAccount ) {
		$this->m_boolUseCustomBankAccount = $boolUseCustomBankAccount;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createCompanyPaymentTransaction( $objGateway = NULL ) {

		$objCompanyPaymentTransaction = new CCompanyPaymentTransaction();
		$objCompanyPaymentTransaction->setDefaults();

		$objCompanyPaymentTransaction->setCid( $this->getCid() );
		$objCompanyPaymentTransaction->setAccountId( $this->getAccountId() );
		$objCompanyPaymentTransaction->setCompanyPaymentId( $this->getId() );
		$objCompanyPaymentTransaction->setTransactionAmount( $this->getPaymentAmount() );
		$objCompanyPaymentTransaction->setGatewayResponseCode( -1 );
		$objCompanyPaymentTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );

		if( CPaymentType::CHECK_21 == $this->m_intPaymentTypeId || CMerchantGateway::FIRST_REGIONAL_ACH == $this->getMerchantGatewayId() || CMerchantGateway::ZIONS_ACH == $this->getMerchantGatewayId() ) {
				$objCompanyPaymentTransaction->setGatewayResponseText( 'ACH or Check21 Event' );
		} elseif( true == isset( $objGateway ) ) {

			$objCompanyPaymentTransaction->setGatewayResponseCode( $objGateway->getGatewayResponseCode() );
			$objCompanyPaymentTransaction->setGatewayResponseText( $objGateway->getGatewayResponseText() );
			$objCompanyPaymentTransaction->setGatewayResponseReasonCode( $objGateway->getGatewayResponseReasonCode() );
			$objCompanyPaymentTransaction->setGatewayResponseReasonText( $objGateway->getGatewayResponseReasonText() );
			$objCompanyPaymentTransaction->setGatewayTransactionNumber( $objGateway->getGatewayTransactionNumber() );
			$objCompanyPaymentTransaction->setGatewayApprovalCode( $objGateway->getGatewayApprovalCode() );
			$objCompanyPaymentTransaction->setGatewayAvsResultCode( $objGateway->getGatewayAvsResultCode() );
			$objCompanyPaymentTransaction->setGatewayAvsResultText( $objGateway->getGatewayAvsResultText() );
			$objCompanyPaymentTransaction->setGatewayRawData( $objGateway->getGatewayRawData() );
		}

	return $objCompanyPaymentTransaction;
	}

	/**
	 * Get or Fetch Functions
	 *
	 */

	public function getOrFetchInvoice( $objDatabase ) {

		if( true == valObj( $this->m_objInvoice, 'CInvoice' ) ) {
			return $this->m_objInvoice;
		} else {
			return $this->fetchInvoice( $objDatabase );
		}
	}

 	public function getOrFetchAccount( $objDatabase ) {

		if( true == valObj( $this->m_objAccount, 'CAccount' ) ) {
			return $this->m_objAccount;
		} else {
			$this->m_objAccount = $this->fetchAccount( $objDatabase );
			return $this->m_objAccount;
		}
 	}

 	public function getOrFetchTransaction( $objDatabase ) {

		if( true == valObj( $this->m_objTransaction, 'CTransaction' ) ) {
			return $this->m_objTransaction;
		} else {
			$this->m_objTransaction = $this->fetchTransaction( $objDatabase );
			return $this->m_objTransaction;
		}
 	}

 	/**
 	 * Fetch Functions
 	 *
 	 */

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->getCid(), $objDatabase );
	}

	public function fetchInvoice( $objDatabase ) {
		$this->m_objInvoice = CInvoices::createService()->fetchInvoiceById( $this->m_intInvoiceId, $objDatabase );
		return $this->m_objInvoice;
	}

	public function fetchAccount( $objDatabase ) {
		return CAccounts::createService()->fetchAccountById( $this->getAccountId(), $objDatabase );
	}

	public function fetchTransaction( $objDatabase ) {
		$objTransaction = CTransactions::createService()->fetchTransactionByCidByCompanyPaymentId( $this->getCid(), $this->getId(), $objDatabase );
		return $objTransaction;
	}

	public function fetchSemTransaction( $objDatabase ) {
		return CSemTransactions::fetchSemTransactionByCompanyPaymentId( $this->getId(), $objDatabase );
	}

	public function fetchAuthCode( $arrintPaymentTransactionTypeIds, $objDatabase ) {
		return \Psi\Eos\Payment\CCompanyPaymentTransactions::createService()->fetchCompanyPaymentAuthCodeByCompanyPaymentIdByPaymentTransactionTypeIds( $this->getId(), $arrintPaymentTransactionTypeIds, $objDatabase );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required for company payment.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valAccountId( $objDatabase, $boolIsBulkAddChecks = false ) {

		$boolIsValid = true;

		if( true == is_null( $this->m_intAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Account or invoice number is required for payment to post.<br/>' ) );

		} elseif( true == isset( $objDatabase ) ) {
			$objAccount = CAccounts::createService()->fetchAccountByIdAndCid( $this->m_intAccountId, $this->m_intCid, $objDatabase );

			if( false == valObj( $objAccount, 'CAccount' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Account does not belong to this client.<br/>' ) );
			} elseif( true == $boolIsBulkAddChecks && true == valObj( $objAccount, 'CAccount' ) && false == in_array( $objAccount->getAccountTypeId(), CAccountType::$c_arrintAllowedAccountTypeIds ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Only primary, standard and master policy accounts are allowed.<br/>' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPaymentTypeId( $strAction = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_intPaymentTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type_id', 'Payment type is required for company payment.<br/>' ) );
		}

		if( VALIDATE_DELETE == $strAction ) {
			if( true == ( CPaymentTypes::isElectronicPayment( $this->m_intPaymentTypeId ) ) && false == in_array( $this->m_objAccount->getAccountTypeId(), CAccountType::$c_arrintInsuranceAccountTypeIds ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type_id', 'Electronic payments may not be deleted.<br/>' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPaymentStatusTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intPaymentStatusTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_status_type_id', 'Payment status is required for company payment.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strPaymentDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_datetime', 'Payment date is required for company payment.<br/>' ) );
		}

		$intPaymentDatetime = strtotime( $this->m_strPaymentDatetime );
		if( false == $intPaymentDatetime ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_datetime', 'Payment date is invalid.<br/>' ) );
		}

	return $boolIsValid;
	}

	public function valPaymentAmount( $intUserId, $arrintAssociatedRoleIds ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltPaymentAmount ) || 0 == ( float ) $this->m_fltPaymentAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', 'Payment Amount is required for company payment.<br/>' ) );
			return $boolIsValid;
		}

		if( $this->m_fltPaymentAmount < 0 && false == in_array( $intUserId, [ CUser::ID_SYSTEM, CUser::ID_ADMIN ] ) && false == in_array( CRole::ENTRATA_BANK_ACCOUNT_SIGNER, ( array ) $arrintAssociatedRoleIds ) ) {
			if( false == is_numeric( $this->m_fltPaymentAmount ) ) {
				trigger_error( 'User Id #' . $intUserId . ' attempting to disburse negative Payment Amount. Potential fraud attempt occurring.  If you did this by accident, please contact an administrator immediately.', E_USER_WARNING );
			}
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', 'You are not authorized to enter a negative payment.<br/>' ) );
			return $boolIsValid;
		}

		if( ( ( double ) 100000000000.0 ) <= ( double ) $this->m_fltPaymentAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', 'Payment Amount is out of range.<br/>' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPaymentMemo() {
		$boolIsValid = true;

		if( true == is_null( $this->getPaymentMemo() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'memo', 'Memo is required.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valChargeCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intChargeCodeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_id', 'A payment charge code is required for company payment.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoStreetLine1() {
		$boolIsValid = true;

		if( ( true == CPaymentTypes::isElectronicPayment( $this->m_intPaymentTypeId ) ) && ( true == is_null( $this->m_strBilltoStreetLine1 ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_street_line1', 'Address is required for payment.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoCity() {
		$boolIsValid = true;

		if( ( true == CPaymentTypes::isElectronicPayment( $this->m_intPaymentTypeId ) ) && ( true == is_null( $this->m_strBilltoCity ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_city', 'City is required for payment.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoStateCode() {
		$boolIsValid = true;

		if( ( true == CPaymentTypes::isElectronicPayment( $this->m_intPaymentTypeId ) ) && ( true == is_null( $this->m_strBilltoStateCode ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_state_code', 'State is required for payment.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoPostalCode() {
		$boolIsValid = true;

		if( ( true == CPaymentTypes::isElectronicPayment( $this->m_intPaymentTypeId ) ) && ( true == is_null( $this->m_strBilltoPostalCode ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code', 'ZIP code is required for payment.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoCountryCode() {
		$boolIsValid = true;

		if( ( true == CPaymentTypes::isElectronicPayment( $this->m_intPaymentTypeId ) ) && ( true == is_null( $this->m_strBilltoCountryCode ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_country_code', 'Billing country is required for company payment.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valAuthToken() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strAuthToken, 32 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'auth_token', 'Auth token is not valid.', 621 ) );
		}

		return $boolIsValid;
	}

	public function valSecureReferenceNumber() {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_intSecureReferenceNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', 'Card number is not valid.', 621 ) );
		}

		return $boolIsValid;
	}

	public function valAuthTokenOrSecureReferenceNumber() {
		$boolIsValid = true;

		if( ( true == valStr( $this->m_strAuthToken, 32 ) && false == valStr( $this->getCcCardNumberMasked() ) ) || ( false == valStr( $this->m_strAuthToken, 32 ) && false == is_numeric( $this->m_intSecureReferenceNumber ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', 'Auth token or Card number is not valid.', 621 ) );
		}

		return $boolIsValid;
	}

	public function valCcExpDateMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strCcExpDateMonth ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_month', 'Card expiration month is required for payment.' ) );
		}

		return $boolIsValid;
	}

	public function valCcExpDateYear() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strCcExpDateYear ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_year', 'Card expiration year is required for payment.' ) );
		}

		return $boolIsValid;
	}

	public function valCcExpDateMonthYear() {
		$boolIsValid = true;

		if( $this->m_strCcExpDateMonth < date( 'm' ) && $this->m_strCcExpDateYear == date( 'Y' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_year', 'Card expiration month & year should be greater than or equal to current month and year.' ) );
		}
		return $boolIsValid;
	}

	public function valCcNameOnCard() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strCcNameOnCard ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_name_on_card', 'Name on card is required for payment.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valCheckAccountTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intCheckAccountTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_type_id', 'Check account type is required.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valCheckAccountNumber() {
		$boolIsValid = true;
		if( true == is_null( $this->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', 'Account number is required for payment.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valCheckRoutingNumber( $objPaymentDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number is required for payment.<br/>' ) );
		} elseif( 0 !== \Psi\CStringService::singleton()->preg_match( '/[^0-9]/', $this->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be a number.<br/>' ) );
		} elseif( 9 != \Psi\CStringService::singleton()->strlen( $this->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be 9 digits long.<br/>' ) );
		}

		if( false == is_null( $objPaymentDatabase ) ) {

			$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->getCheckRoutingNumber(), $objPaymentDatabase );

			if( true == is_null( $objFedAchParticipant ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number was not found.<br/>' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCheckAccountAndRoutingNumber() {
		$boolIsValid = true;

		if( true == $this->isElectronicPayment() && true == $this->isAchPayment() && ( true == is_null( $this->getCheckAccountNumber() ) || true == is_null( $this->getCheckRoutingNumber() ) ) ) {

			$strMessage = 'Check Account Number or Routing Number is NULL at :' . $this->getPaymentDatetime() . ' For Mgmt Company : ' . $this->getCid() . ' For Account : ' . $this->getAccountName;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Check Account Number or Routing Number is NULL <br/>' ) );
			trigger_error( 'Company Payment :: ' . $strMessage, E_USER_WARNING );

			$arrstrEmailAddresses = [ CSystemEmail::XENTO_RPARDESHI_EMAIL_ADDRESS, CSystemEmail::AREID_EMAIL_ADDRESS, CSystemEmail::RJENSEN_EMAIL_ADDRESS ];
			// Try to send an email to the Emergency team
			$objPaymentEmailer = new CPaymentEmailer();
			$strSubject = 'Company Payment :: Check Account Number / Routing Number is NULL';

			$objPaymentEmailer->emailEmergencyTeam( $strMessage, $strSubject, $arrstrEmailAddresses );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valCheckNumber( $boolIsBulkAddChecks = false ) {
		$boolIsValid = true;

		if( CPsProduct::VACANCY != $this->getPsProductId() && CPaymentType::CHECK == $this->m_intPaymentTypeId && true == is_null( $this->m_strCheckNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_number', 'Check number is required.<br/>' ) );
		}

		if( true == $boolIsBulkAddChecks ) {
			if( false == valId( $this->m_strCheckNumber ) && CPaymentType::STR_ACH != $this->m_strCheckNumber ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_number', 'Invalid check number.<br/>' ) );
			}
		}

		return $boolIsValid;
	}

	public function valMasterPolicyAccounts( $arrintAccountTypes ) {
		$boolIsValid = true;

		if( CAccountType::MASTER_POLICY == $arrintAccountTypes[$this->getAccountId()] && \Psi\Libraries\UtilFunctions\count( array_unique( $arrintAccountTypes ) ) > 1 ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_id', 'Master policy check needs to be added to it\'s own batch.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckNameOnAccount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strCheckNameOnAccount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ach_name_on_account', 'Account holder&#39;s name is required.<br/>' ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceId( $objDatabase, $boolIsBulkAddChecks = false ) {
		$boolIsValid = true;

		if( 0 < strlen( $this->m_intInvoiceId ) && ( false == is_numeric( $this->m_intInvoiceId ) || 0 == $this->m_intInvoiceId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_id', 'Invoice Id must be a number greater than zero.<br/>' ) );
		}

	if( 0 < strlen( $this->m_intInvoiceId ) && true == valObj( $objDatabase, 'CDatabase' ) ) {
		$objInvoice = $this->fetchInvoice( $objDatabase );

		if( true == $boolIsBulkAddChecks ) {
			if( false == valObj( $objInvoice, 'CInvoice' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_id', 'Invoice Id entered is not a valid invoice.<br/>' ) );
			} elseif( false == is_null( $this->m_intCid ) && true == valObj( $objInvoice, 'CInvoice' ) && $objInvoice->getCid() != $this->m_intCid ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_id', 'Invoice number entered belong to a different client.' ) );
			} elseif( false == is_null( $this->m_intAccountId ) && true == valObj( $objInvoice, 'CInvoice' ) && $objInvoice->getAccountId() != $this->m_intAccountId ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_id', 'Invoice number belong to different account.<br/><br/>' ) );
			} elseif( true == $boolIsBulkAddChecks && true == valObj( $objInvoice, 'CInvoice' ) ) {
				$objAccount = CAccounts::createService()->fetchAccountById( $objInvoice->getAccountId(), $objDatabase );

				if( true == valObj( $objAccount, 'CAccount' ) && true == in_array( $objAccount->getAccountTypeId(), CAccountType::$c_arrintExcludeBulkCheckAccountTypeIds ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_id', 'Invoice belong to Resident account are not allowed.<br/>' ) );
				}
			}
		} else {

			if( false == valObj( $objInvoice, 'CInvoice' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_id', 'Invoice Id entered is not a valid invoice.<br/>' ) );
			} elseif( false == is_null( $this->m_intCid ) && true == valObj( $objInvoice, 'CInvoice' ) && $objInvoice->getCid() != $this->m_intCid ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_id', 'Invoice number entered belong to a different client.<br/>' ) );
			} elseif( false == is_null( $this->m_intAccountId ) && true == valObj( $objInvoice, 'CInvoice' ) && $objInvoice->getAccountId() != $this->m_intAccountId ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_id', 'Invoice number belong to different account.<br/>' ) );
			}

		}
	}

	return $boolIsValid;
	}

	public function valDepositId( $strAction, $objDatabase ) {

		if( false == isset( $objDatabase ) ) {
			trigger_error( 'Database must be sent to the validate update function on a company payment.', E_USER_ERROR );
			exit;
		}

		$objOldCompanyPayment = \Psi\Eos\Admin\CCompanyPayments::createService()->fetchCompanyPaymentById( $this->m_intId, $objDatabase );

		if( false == isset( $objOldCompanyPayment ) ) {
			trigger_error( 'Old company payment failed to load.', E_USER_ERROR );
			exit;
		}

		if( $objOldCompanyPayment->getPaymentAmount() != $this->m_fltPaymentAmount && false == is_null( $objOldCompanyPayment->getDepositId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You cannot edit the payment amount on a payment that is part of a deposit. You must delete the deposit before editing the payment amount.<br/>', NULL ) );
			return false;

		} elseif( $strAction == VALIDATE_DELETE && false == is_null( $objOldCompanyPayment->getDepositId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You cannot delete a payment that is part of a deposit.  You must first delete the deposit.<br/>', NULL ) );
			return false;
		}

		return true;
	}

	public function valIntermediaryProcessingBankAccountId( $objPaymentDatabase ) {

		// In Split Payment database process as we have removed the trigger func_assert_intermediary_processing_bank_account to validate processing bank account adding code here to validate the same
		if( false == is_null( $this->getIntermediaryProcessingBankAccountId() ) ) {
			if( false == valObj( $objPaymentDatabase, 'CDatabase' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to load payment database.' ) );
				return false;
			}

			$strSql = 'SELECT id
						FROM
							public.processing_bank_accounts  pba
						WHERE pba.id = ' . ( int ) $this->getIntermediaryProcessingBankAccountId() . ' AND
						( pba.cid = ' . ( int ) $this->getCid() . ' OR pba.cid IS NULL )';

			$objIntermediaryProcessingBankAccount = \Psi\Eos\Payment\CProcessingBankAccounts::createService()->fetchProcessingBankAccount( $strSql, $objPaymentDatabase );

			if( false == valObj( $objIntermediaryProcessingBankAccount, 'CProcessingBankAccount' ) ) {
			 	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to post company payment. Processing bank account invalid.<br/>' ) );
				return false;
			}
		}

		 return true;
	}

	public function validate( $strAction, $objDatabase = NULL, $intUserId = NULL, $objPaymentDatabase = NULL, $boolIsBulkAddChecks = false, $arrintAccountTypes = NULL ) {
		$boolIsValid = true;
		$arrintAssociatedRoleIds = [];

		// checking user role Association with "Entrata Bank Account Signer" role

		if( true == valId( $intUserId ) && true == valObj( $objDatabase, 'CDatabase' ) && CDatabaseType::ADMIN == $objDatabase->getDatabaseTypeId() ) {

			$arrintUserId = CUsers::fetchUserIdByUserIdByRoleIdWithIsSuperUser( $intUserId, CRole::ENTRATA_BANK_ACCOUNT_SIGNER, $objDatabase );
			if( true == valArr( $arrintUserId ) ) {
				$arrintAssociatedRoleIds[] = CRole::ENTRATA_BANK_ACCOUNT_SIGNER;
			}
		}

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDepositId( $strAction, $objDatabase );

			case self::VALIDATE_POST:

				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAccountId( $objDatabase, $boolIsBulkAddChecks );
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount( $intUserId, $arrintAssociatedRoleIds );
				$boolIsValid &= $this->valBilltoStreetLine1();
				$boolIsValid &= $this->valBilltoCity();
				$boolIsValid &= $this->valBilltoStateCode();
				$boolIsValid &= $this->valBilltoPostalCode();
				$boolIsValid &= $this->valCheckNumber( $boolIsBulkAddChecks );
				if( true == $this->isAchPayment() ) {
					$boolIsValid &= $this->valCheckNameOnAccount();
					$boolIsValid &= $this->valCheckRoutingNumber( $objPaymentDatabase );
					$boolIsValid &= $this->valCheckAccountNumber();
					$boolIsValid &= $this->valCheckAccountTypeId();
				}
				if( true == $this->isCreditCardPayment() ) {
					$boolIsValid &= $this->valCcExpDateMonth();
					$boolIsValid &= $this->valCcExpDateYear();
					$boolIsValid &= $this->valCcNameOnCard();
					$boolIsValid &= $this->valAuthTokenOrSecureReferenceNumber();
				}
				$boolIsValid &= $this->valInvoiceId( $objDatabase, $boolIsBulkAddChecks );

				if( true == $boolIsBulkAddChecks ) {
					$boolIsValid &= $this->valMasterPolicyAccounts( $arrintAccountTypes );
				}
				break;

			case self::VALIDATE_RETURN_FEE_POST:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAccountId( $objDatabase );
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount( $intUserId, $arrintAssociatedRoleIds );
				$boolIsValid &= $this->valCheckNumber();
				if( true == $this->isAchPayment() ) {
					$boolIsValid &= $this->valCheckAccountTypeId();
					$boolIsValid &= $this->valCheckAccountNumber();
					$boolIsValid &= $this->valCheckRoutingNumber( $objPaymentDatabase );
					$boolIsValid &= $this->valCheckNameOnAccount();
				}
				if( true == $this->isCreditCardPayment() ) {
					$boolIsValid &= $this->valCcExpDateMonth();
					$boolIsValid &= $this->valCcExpDateYear();
					$boolIsValid &= $this->valCcNameOnCard();
					$boolIsValid &= $this->valAuthTokenOrSecureReferenceNumber();
				}
				$boolIsValid &= $this->valInvoiceId( $objDatabase );
				break;

			case self::VALIDATE_CARD_PAYMENT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAccountId( $objDatabase );
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount( $intUserId, $arrintAssociatedRoleIds );
				$boolIsValid &= $this->valAuthTokenOrSecureReferenceNumber();
				$boolIsValid &= $this->valCcNameOnCard();
				$boolIsValid &= $this->valCcExpDateMonth();
				$boolIsValid &= $this->valCcExpDateYear();
				$boolIsValid &= $this->valCcExpDateMonthYear();
				$boolIsValid &= $this->valBilltoStreetLine1();
				$boolIsValid &= $this->valBilltoCity();
				$boolIsValid &= $this->valBilltoStateCode();
				$boolIsValid &= $this->valBilltoPostalCode();
				$boolIsValid &= $this->valInvoiceId( $objDatabase );
				break;

			case VALIDATE_DELETE:
			$boolIsValid &= $this->valPaymentTypeId( $strAction );
			$boolIsValid &= $this->valDepositId( $strAction, $objDatabase );
				break;

			default:
				// deafult case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Database Functions
	 *
	 */

	/**
	 * Asynchronous payment posting. Responses are received by the EFT payment finalization consumer,
	 * which will set the correct payment status and create a transaction. See the below methods to
	 * add additional post-capture/reversal handling.
	 * @see CCompanyPaymentProcesses::finalizeAuthCapture()
	 * @see CCompanyPaymentProcesses::finalizeReverse()
	 *
	 * @param int $intUserId
	 * @param CDatabase $objDatabase
	 * @param CCompanyPayment|null $objOriginalCompanyPayment
	 * @param bool $boolReprocessPayment
	 * @return bool
	 */
	public function postAsyncPayment( $intUserId, $objDatabase, $objOriginalCompanyPayment = NULL, $boolReprocessPayment = false ) {
		return $this->postPayment( $intUserId, $objDatabase, $objOriginalCompanyPayment, $boolReprocessPayment, true );
	}

	/**
	 * Traditional, rpc payment posting. Code waits for a response via RabbitMQ, then returns. Expects calling
	 * code to create transactions.
	 *
	 * @param int $intUserId
	 * @param CDatabase $objDatabase
	 * @param CCompanyPayment|null $objOriginalCompanyPayment
	 * @param bool $boolReprocessPayment
	 * @return bool
	 */
	public function postRpcPayment( $intUserId, $objDatabase, $objOriginalCompanyPayment = NULL, $boolReprocessPayment = false ) {
		return $this->postPayment( $intUserId, $objDatabase, $objOriginalCompanyPayment, $boolReprocessPayment, false );
	}

	/**
	 * @deprecated Use postRpcPayment or postAsyncPayment
	 * @see CCompanyPayment::postRpcPayment()
	 * @see CCompanyPayment::postAsyncPayment()
	 *
	 * @param int $intUserId
	 * @param CDatabase $objDatabase
	 * @param null|CCompanyPayment $objOriginalCompanyPayment
	 * @param bool $boolReprocessPayment
	 * @param bool $boolUseAsynchronousPost
	 * @return bool
	 */
	public function postPayment( $intUserId, $objDatabase, $objOriginalCompanyPayment = NULL, $boolReprocessPayment = false, $boolUseAsynchronousPost = false ) {

		if( false == $this->validateAndInsertPayment( $boolReprocessPayment, $intUserId, $objDatabase ) ) {
			return false;
		}

		// For non-electronic payments no more processing required. Return immediately.
		if( false == $this->isElectronicPayment() ) {
			return true;
		}

		// Only CC and ACH electronic payments accepted
		if( false == $this->isCreditCardPayment() && false == $this->isAchPayment() && false == $this->isAftPayment() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to post electronic company payment. Only credit card or echeck payments accepted.' ) );
			return false;
		}

		if( true == $this->handleEftTesting( $objOriginalCompanyPayment, $intUserId, $objDatabase ) ) {
			return true;
		}

		$boolIsTransaction = false;
		if( true == $objDatabase->getIsTransaction() ) {
			$objDatabase->commit();
			$boolIsTransaction = true;
		}

		// DO NOT USE TRANSLATION, MULTI-BYTE, OR __() FUNCTIONS HERE!! This line is critical and is not compatible with those changes.
		$intPaymentAmount = ( int ) str_replace( [ '.' ], [ '' ], number_format( $this->getPaymentAmount(), 2, '.', '' ) );
		/** flip the sign on the amount.  Negative amounts are credits to Entrata/debits from clients.  Positive amounts are debits to Entrata/credits to clients */
		$intPaymentAmount = - 1 * $intPaymentAmount;

		$boolIsReversal = false;
		$boolAuthCaptureNegativeOverride = $this->checkNegativePaymentOverride( $intUserId, $objOriginalCompanyPayment, $objDatabase );

		// if the amount is negative it should be a reversal.  Except when it's on a clearing or intermediary account!
		if( $this->m_fltPaymentAmount < 0 && false == $boolAuthCaptureNegativeOverride ) {
			if( false == valObj( $objOriginalCompanyPayment, 'CCompanyPayment' ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Negative electronic payments are not permitted. Please reverse an existing company payment instead.' ) );
				return false;
			}
			$objPaymentMessage = $this->buildReverseMessage( $intUserId, $intPaymentAmount, $objOriginalCompanyPayment, $objDatabase );
			$boolIsReversal = true;
		} else {
			$objPaymentMessage = $this->buildAuthCaptureMessage( $intUserId, $intPaymentAmount, $objDatabase );
		}

		$strQueue = $objPaymentMessage::PAYMENTS_RPC_OPERATION_EFT;
		if( true == $boolUseAsynchronousPost ) {
			$strQueue = $objPaymentMessage::PAYMENTS_SAGA_OPERATION_EFT;
			$arrmixPaymentInfo = $objPaymentMessage->getPaymentInfo();
			$arrmixPaymentInfo['create_transactions'] = true;
			$objPaymentMessage->setPaymentInfo( $arrmixPaymentInfo );
		}

		$objAmqpMessageFactory 	= \Psi\Libraries\Queue\Factory\CAmqpMessageFactory::createService();
		$objMessageSender 		= \Psi\Libraries\Queue\Factory\CMessageSenderFactory::createService()->createSimpleSender( $objAmqpMessageFactory, $strQueue, [], NULL, \Psi\Libraries\Queue\CAmqpConnection::PAYMENTS_VHOST );

		try {
			if( true == $boolUseAsynchronousPost ) {
				$objMessageSender->send( $objPaymentMessage );
				$objMessageSender->close();

				return true;
			}

			$objMessageSender->sendRpc( $objPaymentMessage, [], 90 );
		} catch( \Exception $objException ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $objException->getMessage() ) );
			return false;
		}

		$arrstrResponses = json_decode( $objMessageSender->getResponse(), true );
		$boolResult = CStrings::strToBool( $arrstrResponses['operation_status'] );
		$arrstrErrorMessages = $arrstrResponses['error_msgs'];
		$objMessageSender->close();

		if( true == $boolResult ) {
			$this->setPaymentStatusTypeId( CPaymentStatusType::CAPTURED );

			if( true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
				$this->setBatchedOn( date( 'Y-m-d' ) );
			}

			if( true == $boolIsReversal ) {
				$objOriginalCompanyPayment->setPaymentStatusTypeId( CPaymentStatusType::REVERSED );
				$objOriginalCompanyPayment->update( $intUserId, $objDatabase );

				$objCompanyPaymentTransaction = $this->createCompanyPaymentTransaction();
				$objCompanyPaymentTransaction->setId( \Psi\Eos\Payment\CCompanyPaymentTransactions::createService()->fetchNextId( $objDatabase ) );
				$objCompanyPaymentTransaction->setPaymentTransactionTypeId( CPaymentTransactionType::CREDIT );
				$objCompanyPaymentTransaction->setGatewayResponseCode( CLitleGateway::RESPONSE_CODE_APPROVED );
				if( false == $objCompanyPaymentTransaction->insert( $intUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objCompanyPaymentTransaction->getErrorMsgs() );
				}

				$objCompanyPaymentTransaction->setCompanyPaymentId( $objOriginalCompanyPayment->getId() );
				$objCompanyPaymentTransaction->setId( \Psi\Eos\Payment\CCompanyPaymentTransactions::createService()->fetchNextId( $objDatabase ) );
				if( false == $objCompanyPaymentTransaction->insert( $intUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objCompanyPaymentTransaction->getErrorMsgs() );
				}
			}
		} else {
			$this->setPaymentStatusTypeId( CPaymentStatusType::DECLINED );
			if( false == is_null( $objOriginalCompanyPayment ) ) {
				$objOriginalCompanyPayment->setPaymentStatusTypeId( CPaymentStatusType::CAPTURED );
			}

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, implode( ' ', $arrstrErrorMessages ) ) );

			if( true == $this->isCreditCardPayment() ) {
				$objAccount 	= $this->getOrFetchAccount( $objDatabase );

				if( false == in_array( $objAccount->getAccountTypeId(), CAccountType::getInsuranceAccountTypeIds() ) && false == $this->applyReturnFee( $intUserId, $objDatabase ) ) {
					$boolResult = false;
				}
			}
		}

		$objDatabase->begin();

		if( false == $this->update( $intUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to update company payment ' . $this->getId() . ' to ' . CPaymentStatusType::getTypeNameByTypeId( $this->getPaymentStatusTypeId() ) . ' status' ) );
			$boolResult = false;
		}

		// Commit company payment and payment transaction
		$objDatabase->commit();

		if( true == $boolIsTransaction ) {
			$objDatabase->begin();
		}

		return $boolResult;
	}

	protected function checkNegativePaymentOverride( $intUserId, $objOriginalCompanyPayment, $objDatabase ) {

		// if the amount isn't negative or it's a reversal of an existing payment return false because it should be a reversal
		if( 0 <= $this->m_fltPaymentAmount || false == is_null( $objOriginalCompanyPayment ) ) {
			return false;
		}

		$intAccountTypeId = $this->getOrFetchAccount( $objDatabase )->getAccountTypeId();
		$arrintPermittedAccountTypes = [ CAccountType::CLEARING, CAccountType::INTERMEDIARY ];

		// negative auth_capture only permitted on clearing and intermediary accounts, unless created manually by Will
		if( CUser::ID_WESTON_BELL == $intUserId || true == in_array( $intAccountTypeId, $arrintPermittedAccountTypes ) ) {
			return true;
		}

		return false;
	}

	protected function validateAndInsertPayment( $boolReprocessPayment, $intUserId, $objDatabase ) {
		if( CPaymentStatusType::PENDING != $this->getPaymentStatusTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to post company payment. Invalid payment status.' ) );
			return false;
		}

		$this->setPaymentStatusTypeId( CPaymentStatusType::CAPTURING );

		// For non-electronic payments set payment status to received
		if( false == $this->isElectronicPayment() ) {
			$this->clearAchData();
			$this->clearCcData();
			$this->setPaymentStatusTypeId( CPaymentStatusType::RECEIVED );
		}

		if( false === $boolReprocessPayment ) {
			// Save company payment
			if( false == $this->valCheckAccountAndRoutingNumber() ) {
				return false;
			}

			if( false == $this->insert( $intUserId, $objDatabase ) ) {
				$this->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
				return false;
			}
		}

		return true;
	}

	/**
	 * @param CCompanyPayment $objOriginalCompanyPayment
	 * @param int $intUserId
	 * @param CDatabase $objDatabase
	 * @return bool
	 */
	protected function handleEftTesting( $objOriginalCompanyPayment, $intUserId, $objDatabase ) {
		// if we're in production, or the eft system is enabled, return false so we continue executing
		if( 'production' == CONFIG_ENVIRONMENT || ( true == defined( 'CONFIG_ENABLE_EFT_PAYMENT_SYSTEM' ) && true == CONFIG_ENABLE_EFT_PAYMENT_SYSTEM ) ) {
			return false;
		}

		// short circuit functionality for dev and stage when the new payment system is not available
		$this->setPaymentStatusTypeId( CPaymentStatusType::CAPTURED );
		$this->update( $intUserId, $objDatabase );
		if( true == valObj( $objOriginalCompanyPayment, 'CCompanyPayment' ) ) {
			$objOriginalCompanyPayment->setPaymentStatusTypeId( CPaymentStatusType::REVERSED );
			$objOriginalCompanyPayment->update( $intUserId, $objDatabase );
			$objCompanyPaymentTransaction = $this->createCompanyPaymentTransaction();
			$objCompanyPaymentTransaction->setId( \Psi\Eos\Payment\CCompanyPaymentTransactions::createService()->fetchNextId( $objDatabase ) );
			$objCompanyPaymentTransaction->setPaymentTransactionTypeId( CPaymentTransactionType::CREDIT );
			$objCompanyPaymentTransaction->setGatewayResponseCode( CLitleGateway::RESPONSE_CODE_APPROVED );
			$objCompanyPaymentTransaction->insert( $intUserId, $objDatabase );

			$objCompanyPaymentTransaction->setCompanyPaymentId( $objOriginalCompanyPayment->getId() );
			$objCompanyPaymentTransaction->setId( \Psi\Eos\Payment\CCompanyPaymentTransactions::createService()->fetchNextId( $objDatabase ) );
			$objCompanyPaymentTransaction->insert( $intUserId, $objDatabase );
		}

		return true;
	}

	/**
	 * @param int $intUserId
	 * @param int $intPaymentAmount
	 * @param CDatabase $objDatabase
	 * @return \Psi\Libraries\PaymentQueue\Message\Payments\Operations\CAuthCapture
	 */
	protected function buildAuthCaptureMessage( $intUserId, $intPaymentAmount, $objDatabase ) {
		$objPaymentMessage = \Psi\Libraries\PaymentQueue\Message\Payments\Operations\CAuthCapture::create();

		$objPaymentMessage->setEftTypeId( self::COMPANY_PAYMENT_EFT_TYPE_ID )
			->setEftDatetime( $this->getPaymentDatetime() )
			->setCid( $this->getCid() )
			->setRemotePaymentNumber( $this->getId() )
			->setServiceClientId( self::COMPANY_PAYMENT_SERVICE_CLIENT_ID )
			->setUserId( $intUserId )
			->setIsTesting( 'production' != CONFIG_ENVIRONMENT ? true : false );
		$objPaymentMessage
			->setCurrencyCode( $this->getCurrencyCode() )
			->setCountryCode( $this->getBilltoCountryCode() )
			->setMerchantGatewayId( $this->getMerchantGatewayId() )
			->setProcessingBankAccountId( $this->getPsProcessingBankAccountId() )
			->setPaymentMediumId( CPaymentMedium::WEB )
			->setPaymentTypeId( $this->getPaymentTypeId() )
			->setPaymentStatusTypeId( $this->getPaymentStatusTypeId() )
			->setAmount( $intPaymentAmount )
			->setTotalAmount( $intPaymentAmount )
			->setReceiptDateTime( $this->getPaymentDatetime() )
			->setBilltoCompanyName( $this->getOrFetchAccount( $objDatabase )->getBilltoNameFull() )
			->setBilltoNameFirst( $this->getOrFetchAccount( $objDatabase )->getBilltoNameFirst() )
			->setBilltoNameLast( $this->getOrFetchAccount( $objDatabase )->getBilltoNameLast() )
			->setBilltoStreetLine1( $this->getBilltoStreetLine1() )
			->setBilltoStreetLine2( $this->getBilltoStreetLine2() )
			->setBilltoCity( $this->getBilltoCity() )
			->setBilltoProvince( $this->getBilltoProvince() )
			->setBilltoStateCode( $this->getBilltoStateCode() )
			->setBilltoPostalCode( $this->getBilltoPostalCode() )
			->setBilltoCountryCode( $this->getBilltoCountryCode() )
			->setCcCardNumberEncrypted( $this->getCcCardNumberEncrypted() )
			->setCcExpDateMonth( $this->getCcExpDateMonth() )
			->setCcExpDateYear( $this->getCcExpDateYear() )
			->setCcNameOnCard( $this->getCcNameOnCard() )
			->setSecureReferenceNumber( $this->getSecureReferenceNumber() )
			->setAuthToken( $this->getAuthToken() )
			->setCheckAccountTypeId( $this->getCheckAccountTypeId() )
			->setCheckDate( $this->getCheckDate() )
			->setCheckNumber( $this->getCheckNumber() )
			->setCheckPayableTo( $this->getCheckPayableTo() )
			->setCheckBankName( $this->getCheckBankName() )
			->setCheckNameOnAccount( $this->getCheckNameOnAccount() )
			->setCheckAccountNumberEncrypted( $this->getCheckAccountNumberEncrypted() )
			->setCheckAccountNumberMasked( $this->getCheckAccountNumberMasked() )
			->setCheckRoutingNumber( $this->getCheckRoutingNumber() )
			->setPhoneAuthRequired( false )
			->setCheckIsMoneyOrder( false )
			->setCheckIsConverted( false )
			->setIsReconPrepped( false )
			->setEftMemo( $this->getPaymentMemo() )
			->setAccountTypeId( $this->getOrFetchAccount( $objDatabase )->getAccountTypeId() )
			->setRemoteReferenceNumber1( $this->getAccountId() )
			->setRemoteReferenceNumber2( $this->getOrFetchAccount( $objDatabase )->getAccountTypeId() )
			->setIsCardOnFile( true )
			->setPaymentInfo( [ 'charge_code_id' => $this->getChargeCodeId() ] );

		if( CPaymentType::ACH == $this->getPaymentTypeId() && false == $this->getUseCustomBankAccount() ) {
			$objPaymentMessage->setInstructionProcessingBankAccountId( $this->getOrFetchAccount( $objDatabase )->getProcessingBankAccountId() );
		}

		return $objPaymentMessage;
	}

	/**
	 * @param int $intUserId
	 * @param int $intPaymentAmount
	 * @param CCompanyPayment $objOriginalCompanyPayment
	 * @param CDatabase $objDatabase
	 * @return \Psi\Libraries\PaymentQueue\Message\Payments\Operations\CReverse
	 */
	protected function buildReverseMessage( $intUserId, $intPaymentAmount, $objOriginalCompanyPayment, $objDatabase ) {
		// DO NOT USE TRANSLATION, MULTI-BYTE, OR __() FUNCTIONS HERE!! This line is critical and is not compatible with those changes.
		$intOriginalPaymentAmount = ( int ) str_replace( [ '.' ], [ '' ], number_format( $this->getPaymentAmount(), 2, '.', '' ) );
		$boolIsPartial = false;
		if( abs( $intPaymentAmount ) != abs( $intOriginalPaymentAmount ) ) {
			$boolIsPartial = true;
		}

		// the eft system expects adjusting amounts to be negative.
		if( 0 < $intPaymentAmount ) {
			$intPaymentAmount = - 1 * $intPaymentAmount;
		}

		$objPaymentMessage = \Psi\Libraries\PaymentQueue\Message\Payments\Operations\CReverse::create();

		$objPaymentMessage->setCid( $this->getCid() )
			->setEftId( NULL )
			->setRemotePaymentNumber( $objOriginalCompanyPayment->getId() )
			->setServiceClientId( self::COMPANY_PAYMENT_SERVICE_CLIENT_ID )
			->setUserId( $intUserId )
			->setEftTypeId( self::COMPANY_PAYMENT_EFT_TYPE_ID )
			->setClusterId( NULL );
		$objPaymentMessage->setNote( $this->getPaymentMemo() )
			->setAdjustingPaymentAmount( $intPaymentAmount )
			->setWaiveReversalFees( false )
			->setIsReversalImmediately( true )
			->setIsPartialReversal( $boolIsPartial )
			->setIsTesting( 'production' != CONFIG_ENVIRONMENT ? true : false )
			->setAccountTypeId( $this->getOrFetchAccount( $objDatabase )->getAccountTypeId() );

		$arrmixPaymentInfo = $this->toArray();
		$arrmixPaymentInfo['account_type_id'] = $this->getOrFetchAccount( $objDatabase )->getAccountTypeId();
		$arrmixPaymentInfo['new_company_payment_id'] = $this->getId();
		$arrmixPaymentInfo['charge_code_id'] = $this->getChargeCodeId();

		$objPaymentMessage->setPaymentInfo( $arrmixPaymentInfo );

		return $objPaymentMessage;
	}

	/**
	 * Other Functions
	 *
	 */

	public function generateReturnFee() {
		$fltReturnFee 	  = 0;
		$fltPaymentAmount = abs( $this->m_fltPaymentAmount );

		if( $fltPaymentAmount >= 100 ) {
			$fltReturnFee = 25.00;
		} elseif( ( $fltPaymentAmount >= 50 ) && ( $fltPaymentAmount < 100 ) ) {
			$fltReturnFee = 10.00;
		} elseif( $fltPaymentAmount < 50 ) {
			$fltReturnFee = 5.00;
		}

		return $fltReturnFee;
	}

	public function isElectronicPayment() {

		if( true == is_null( $this->m_intPaymentTypeId ) ) {
			return false;
		}
		return CPaymentTypes::isElectronicPayment( $this->m_intPaymentTypeId );
	}

	public function isCreditCardPayment() {

		if( true == is_null( $this->m_intPaymentTypeId ) ) {
			return false;
		}
		return CPaymentTypes::isCreditCardPayment( $this->m_intPaymentTypeId );
	}

	public function isAchPayment() {

		if( true == is_null( $this->m_intPaymentTypeId ) ) {
			return false;
		}
		return CPaymentTypes::isAchPayment( $this->m_intPaymentTypeId );
	}

	public function isAftPayment() {
		if( true == is_null( $this->m_intPaymentTypeId ) ) {
			return false;
		}
		return CPaymentTypes::isAftPayment( $this->m_intPaymentTypeId );
	}

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				if( true == valObj( $objErrorMsg, 'CErrorMsg' ) ) {
					$this->addErrorMsg( $objErrorMsg );
				}
			}
		}
	}

	public function detachCompanyInvoice( $intUserId, $objDatabase ) {

		// Remove the payment id from any invoice that has it as the company_payment_id
		// The reason this is done, is it indicates that the invoice really wasn't billed successfully so we can reattempt the billing.
		$objInvoice 		= $this->getOrFetchInvoice( $objDatabase );
		$objAccount 	= $this->getOrFetchAccount( $objDatabase );

		if( true == valObj( $objInvoice, 'CInvoice' ) ) {
			$objInvoice->setCompanyPaymentId( NULL );
			$this->m_intInvoiceId = NULL;

			if( false == $objInvoice->update( $intUserId, $objDatabase ) ) {
				trigger_error( 'Invoice failed to update.', E_USER_WARNING );
			}

			// Check to make sure the account for which we are applying the return should not be a intermediary account.
			if( false == in_array( $objAccount->getAccountTypeId(), array_merge( [ CAccountType::INTERMEDIARY ], CAccountType::getInsuranceAccountTypeIds() ) ) && false == $this->applyReturnFee( $intUserId, $objDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function insertCompanyPaymentTransaction( $intPaymentTransactionTypeId, $strGatewayResponseText, $strTransactionMemo, $intUserId, $objDatabase ) {

		// Create a company_payment_transaction to describe the action of the Return
		$objCompanyPaymentTransaction = $this->createCompanyPaymentTransaction( NULL );
		$objCompanyPaymentTransaction->setPaymentTransactionTypeId( $intPaymentTransactionTypeId );
		$objCompanyPaymentTransaction->setGatewayResponseText( $strGatewayResponseText );
		$objCompanyPaymentTransaction->setTransactionMemo( $strTransactionMemo );

		// Save the company payment transaction
		if( false == $objCompanyPaymentTransaction->validate( VALIDATE_INSERT ) || false == $objCompanyPaymentTransaction->insert( $intUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $objCompanyPaymentTransaction->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function unallocateAndOffset( $intUserId, $objDatabase ) {

		// Create an offsetting adjustment, and allocate the offset adjustment to the original payment.
		$this->getOrFetchTransaction( $objDatabase );

	// Check that the payment is indeed ACH
		if( false == valObj( $this->m_objTransaction, 'CTransaction' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Original payment transaction could not be found.', NULL ) );
			return false;
		}

		$objAccount = $this->getOrFetchAccount( $objDatabase );

		if( false == in_array( $objAccount->getAccountTypeId(), CAccountType::getInsuranceAccountTypeIds() ) ) {
			if( false == $this->m_objTransaction->reverseWithPayment( $intUserId, $objDatabase, $objAccount, $this->getId() ) ) {
				trigger_error( 'Payment failed to reverse. Payment Id: ' . $this->getId(), E_USER_WARNING );
				return false;
			}
		} else {
			// If this is a Resident Insure Payment we need to handle the return differently.
			if( false == CResidentInsurePaymentProcesses::reverseReturnedPayment( $intUserId, $this->m_objTransaction, $objAccount, $this, $objDatabase ) ) {
				trigger_error( 'Error processing Resident Insure payment return. Payment Id: ' . $this->getId(), E_USER_WARNING );
				return false;
			}
		}

		$objAccount->autoAllocate( $intUserId, $objDatabase );

		return true;
	}

	/**
	 * PROCESSING A VACANCY.COM RETURN
	 *
	 */

	// 1. See if a vacancy.com return.
	// 2. Insert reverse sem transaction entry for the payment transaction whose payment got return.
	// 3. Increment return count on sem_setting.
	// 4. Deactivate Account?
	// 5. Email Client.
	// 6. Email Vacancy.com Team.
	// 7. Charge a client a $25 fee.

	public function processVacancyReturn( $intUserId, $objDatabase ) {

		$objSemBudget 		= CSemBudgets::fetchSemBudgetById( $this->getReferenceNumber(), $objDatabase );
		$objSemTransaction 	= $this->fetchSemTransaction( $objDatabase );
		$objSemSetting 		= $objSemBudget->fetchSemSetting( $objDatabase );

		if( true == valObj( $objSemTransaction, 'CSemTransaction' ) && true == valObj( $objSemBudget, 'CSemBudget' ) && true == valObj( $objSemSetting, 'CSemSetting' ) ) {
			$objSemTransaction->setId( NULL );
			$objSemTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
			$objSemTransaction->setTransactionAmount( -1 * $objSemTransaction->getTransactionAmount() );
			$objSemTransaction->setIsReconciled( 1 );

			if( false == $objSemTransaction->insert( $intUserId, $objDatabase ) ) {
				trigger_error( 'Sem transaction failed to insert.', E_USER_ERROR );
				exit;
			}

			// Deactivate sem setting.
			$objSemPropertyDetail = $objSemSetting->fetchSemPropertyDetail( $objDatabase );

			if( false == valObj( $objSemPropertyDetail, 'CSemPropertyDetail' ) ) {
				trigger_error( 'Unable to load property detail.', E_USER_ERROR );
				exit;
			}

			$objSemPropertyDetail->setSemPropertyStatusTypeId( CSemPropertyStatusType::DISABLED );
			$objSemSetting->setPaymentFailures( ( int ) $objSemSetting->getPaymentFailures() + 1 );

			if( false == $objSemSetting->update( $intUserId, $objDatabase ) ) {
				trigger_error( 'Sem setting failed to update.', E_USER_ERROR );
				exit;
			}

			if( false == $objSemPropertyDetail->update( $intUserId, $objDatabase ) ) {
				trigger_error( 'Sem property detail failed to delete.', E_USER_ERROR );
				exit;
			}

		$objFeeSemTransaction = $objSemBudget->createSemTransaction();
			$objFeeSemTransaction->setCompanyPaymentId( $this->getId() );
			$objFeeSemTransaction->setTransactionAmount( 25.00 );
			$objFeeSemTransaction->setIpAddress( '127.0.0.1' );
		$objFeeSemTransaction->setSemBudgetId( NULL );

			if( false == $objFeeSemTransaction->insert( $intUserId, $objDatabase ) ) {
				trigger_error( 'Sem transaction object failed to insert.', E_USER_ERROR );
				exit;
			}
		}

		return true;
	}

	public function processResidentReturnReversalLogic( $intUserId, $objAdminDatabase, $objPaymentDatabase ) {

		// ******************************************************************************************************
		// Company payments with a NRADR_id set represent ACH return fees.
		//  * Update original NRADR that spawned this ACH return fee company payment
		//  * Immediately offset the return adjustment charge with a bad debt credit transaction
		//  * Create a manual allocation transaction to allocate this credit to the return adjustment charge
		// ******************************************************************************************************

		$objOriginalNachaReturnAddendaDetailRecord = \Psi\Eos\Payment\CNachaReturnAddendaDetailRecords::createService()->fetchNachaReturnAddendaDetailRecordById( $this->getNachaReturnAddendaDetailRecordId(), $objPaymentDatabase );

		if( false == valObj( $objOriginalNachaReturnAddendaDetailRecord, 'CNachaReturnAddendaDetailRecord' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'The original NRADR could not be retrieve from the database.', NULL ) );
			return false;
		}

		$objOriginalNachaReturnAddendaDetailRecord->setLastReturnFeeFailedOn( date( 'm/d/Y H:i:s' ) );

		if( false == $objOriginalNachaReturnAddendaDetailRecord->update( $intUserId, $objPaymentDatabase ) ) {
			trigger_error( 'The original NRADR failed to update.', E_USER_WARNING );
			return false;
		}

		// Create an offsetting adjustment, and allocate the offset adjustment to the original payment.
		$this->getOrFetchTransaction( $objAdminDatabase );

		// Check that the payment is indeed ACH
		if( false == valObj( $this->m_objTransaction, 'CTransaction' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Original payment transaction could not be found.', NULL ) );

			return false;
		}

		// Now we are going to load the original charge associated with this payment, and reverse it.
		$arrobjAllocations = \Psi\Eos\Admin\CAllocations::createService()->fetchAllocationsByCreditTransactionId( $this->m_objTransaction->getId(), $objAdminDatabase );

		if( false == valArr( $arrobjAllocations, 1 ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Multiple allocations exist for resident return fee.', NULL ) );
			return false;
		}

		$objAllocation = array_shift( $arrobjAllocations );

		$intChargeTransactionId = $objAllocation->getChargeTransactionId();

		$objAssociatedChargeTransaction = CTransactions::createService()->fetchTransactionByCidById( $this->getCid(), $intChargeTransactionId, $objAdminDatabase );

		$objAccount = $this->getOrFetchAccount( $objAdminDatabase );

		// Check that the payment is indeed ACH
		if( false == valObj( $objAssociatedChargeTransaction, 'CTransaction' ) ) {

			// Eventually this manual code should be able to be removed.  Probably after 8/1/2011, it's totally safe to
			// change this condition so false just returns false, because it should never happen.

			$objCreditTransaction = $objAccount->createTransaction();
			$objCreditTransaction->setDefaults();
			$objCreditTransaction->setCompanyPaymentId( $this->m_intId );
			$objCreditTransaction->setChargeCodeId( CChargeCode::RETURN_FEE );
			$objCreditTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
			$objCreditTransaction->setTransactionAmount( -1 * $this->m_fltPaymentAmount );
			$objCreditTransaction->setMemo( 'Writing off this returned ACH return fee as bad debt. Company Payment # ' . $this->m_intId );

			// Post the adjustment charge/transaction on the company ledger
			if( false == $objCreditTransaction->validate( VALIDATE_INSERT ) || false == $objCreditTransaction->postCharge( $intUserId, $objAdminDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Could not post an Bad Debt credit on the company ledger for this payment.', NULL ) );
				return false;
			}
		} else {

			// If we found an associated charge, we have to reverse it.
			if( false == $objAssociatedChargeTransaction->reverseWithCharge( $intUserId, $objAdminDatabase, $objAccount, CChargeCode::RETURN_FEE ) ) {
				trigger_error( 'Transaction allocation failed to delete.', E_USER_WARNING );
				return false;
			}
		}

		return true;
	}

	public function processReturn( $intUserId, $objAdminDatabase, $objPaymentDatabase, $objEmailDatabase ) {

		$objAccount = $this->getOrFetchAccount( $objAdminDatabase );

	// Check that the payment is indeed ACH
		if( CPaymentType::ACH != $this->m_intPaymentTypeId ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Cannot continue processing. Non-ACH type payment (Type ID ' . $this->getPaymentTypeId() . ') found.', NULL ) );
			return false;
		}

		// Is the payment status currently in a valid state ( i.e. PAYMENT_STATUS_TYPE_CAPTURED )
		if( CPaymentStatusType::CAPTURED != $this->m_intPaymentStatusTypeId && CPaymentStatusType::RETURNING != $this->m_intPaymentStatusTypeId && CPaymentStatusType::REVERSED != $this->m_intPaymentStatusTypeId ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Payment status type (ID ' . $this->m_intPaymentStatusTypeId . ') is not "Captured" or "Returning". Processing will not continue.', NULL ) );
			return false;
		}

		// Check if this payment already has an NSF date; if so, we do not want to reprocess
		if( false == is_null( $this->m_strReturnedOn ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'This payment already has an returned on date(' . $this->m_strReturnedOn . '). Cannot process an return more than once.', NULL ) );
			return false;
		}

		// If this is the payment is associated to a company_invoices record, we should detach it.
		if( false == $this->detachCompanyInvoice( $intUserId, $objAdminDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Payment failed to detach from company invoice.' ) );
			return false;
		}

		// If this is the payment is associated to a company_invoices record, we should detach it.
		if( false == $this->insertCompanyPaymentTransaction( CPaymentTransactionType::RECALL, 'Return Processed', 'Return received and processed.', $intUserId, $objAdminDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Payment failed to detach from company invoice.' ) );
			return false;
		}

		// Set return date
		$this->setReturnedOn( date( 'm/d/Y' ) );

		// Update status type
		$this->setPaymentStatusTypeId( CPaymentStatusType::RECALL );

		// We ding residents who we can't debit from successfully.  If those fines fail to post, we reverse off the charges.
		// This needs to be done before the allocation is deleted in unallocateAndOffset
		if( false == is_null( $this->getNachaReturnAddendaDetailRecordId() ) && 0 < $this->getNachaReturnAddendaDetailRecordId() ) {
			$this->processResidentReturnReversalLogic( $intUserId, $objAdminDatabase, $objPaymentDatabase );
		}

		// Unassociating payment allocations in netsuite
		$arrmixAllocationsData = [];
		if( true == valId( $this->getDepositId() ) ) {
			require_once PATH_APPLICATION_CLIENT_ADMIN . 'Library/NetSuiteService/CNetSuiteServiceLibrary.class.php';
			$objNetSuiteServiceLibrary = new CNetSuiteServiceLibrary();
			$arrmixAllocationsData     = $objNetSuiteServiceLibrary->loadAllocationsDataByExportObjectTypeKey( $this->getId(), $objAdminDatabase );
		}

		// We always de-allocate all current charges to this payment and reallocate them to an NSF adjustment.
		if( false == $this->unallocateAndOffset( $intUserId, $objAdminDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Payment failed to unallocate and offset.' ) );
			return false;
		}

		// For payments for Vacancy.com, when a return happens, we have to process special logic in sem_transactions table.
		if( CPsProduct::VACANCY == $this->getPsProductId() && true == is_numeric( $this->getReferenceNumber() ) ) {
			if( false == $this->processVacancyReturn( $intUserId, $objAdminDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to process Vacancy.com return.' ) );
				return false;
			}
		}

		if( false == $this->update( $intUserId, $objAdminDatabase ) ) {
			trigger_error( 'Returning company payment failed to update.', E_USER_WARNING );
			return false;
		}

		// Check to make sure the account for which we are sending email should not be a intermediary account also.
		if( false == in_array( $objAccount->getAccountTypeId(),  array_merge( array( CAccountType::INTERMEDIARY ), CAccountType::getInsuranceAccountTypeIds() ) ) && ( true == is_null( $this->getNachaReturnAddendaDetailRecordId() ) || 0 >= $this->getNachaReturnAddendaDetailRecordId() ) ) {
			$this->sendReturnNotificationEmails( $intUserId, $objAdminDatabase, $objEmailDatabase );
		}

		if( $this->getReturnTypeId() == CReturnType::ACCOUNT_CLOSED || $this->getReturnTypeId() == CReturnType::NO_ACCOUNT_OR_UNABLE_TO_LOCATE_ACCOUNT ) {
			$objAccount->setIsDisabled( true );
			if( false == $objAccount->update( $intUserId, $objAdminDatabase ) ) {
				trigger_error( 'Account failed to update .', E_USER_WARNING );
				return false;
			}

		}

		if( true == valArr( $arrmixAllocationsData ) ) {
			$objNetSuiteServiceLibrary->unallocateCompanyPayments( $arrmixAllocationsData, $intUserId, $objAdminDatabase );
		}

		return true;
	}

	public function processRevertCompanyPayment( $objUser, $objCompanyPayment, $objTransaction, $objDatabase ) {

		$objAccount = $objCompanyPayment->getOrFetchAccount( $objDatabase );

		if( true == CPaymentTypes::isElectronicPayment( $objCompanyPayment->getPaymentTypeId() ) && false == in_array( $objAccount->getAccountTypeId(), CAccountType::$c_arrintInsuranceAccountTypeIds ) ) {
			trigger_error( 'Company payment must be Nonelectronic.', E_USER_ERROR );
			exit;
		}

		$boolIsInvoiceUpdateRequired			= false;
		$boolIsExportedOrInvoicedOrDeposited	= false;

		if( true == is_null( $objCompanyPayment->getInvoiceId() ) && true == is_null( $objTransaction->getExportBatchId() ) && true == is_null( $objCompanyPayment->getDepositId() ) ) {
			// Company Payment is Not Invoiced or Not Exported or Not Deposited
			// Hard delete the Company Payment
			// Hard Delete the Company Payment & Allocation Transaction
			$boolIsExportedOrInvoicedOrDeposited = false;
		} else {
			// Company Payment is Invoiced or Exported
			// Create a new offsetting company_payment record with payment_amount = (payment_amount * -1)
			// Offset the original payment transaction with a new payment transaction (with transaction_amount * -1).Associate the offsetting company_payment to this new record.
			$boolIsExportedOrInvoicedOrDeposited = true;

			if( false == valObj( $objAccount, 'CAccount' ) ) {
				trigger_error( 'Account failed to load for #' . $objCompanyPayment->getId(), E_USER_ERROR );
			}

			if( false == is_null( $objCompanyPayment->getInvoiceId() ) ) {

				$objInvoice = $objCompanyPayment->fetchInvoice( $objDatabase );

				if( true == valObj( $objInvoice, 'CInvoice' ) && $objCompanyPayment->getId() == $objInvoice->getCompanyPaymentId() ) {

					$objInvoice->setCompanyPaymentId( NULL );

					$boolIsInvoiceUpdateRequired = true;
				}
			}
		}

		if( false == $boolIsExportedOrInvoicedOrDeposited ) {

			if( false == $objCompanyPayment->validate( VALIDATE_DELETE, $objDatabase ) ) {
				return false;
			}

			if( false == $objTransaction->validate( 'delete_payment', $objDatabase ) ) {
				$this->addErrorMsgs( $objTransaction->getErrorMsgs() );
				return false;
			}

			if( false == $objTransaction->deleteAllocations( $objDatabase ) ) {
				return false;
			}

			if( false == $objTransaction->delete( $objUser->getId(), $objDatabase ) ) {
				return false;
			}

			if( true == in_array( $objAccount->getAccountTypeId(), CAccountType::$c_arrintInsuranceAccountTypeIds ) && false == $this->deleteCompanyPaymentTransaction( $objCompanyPayment->getId(), $objDatabase ) ) {
				return false;
			}

			if( false == $objCompanyPayment->delete( $objUser->getId(), $objDatabase ) ) {
				return false;
			}

		} elseif( true == $boolIsExportedOrInvoicedOrDeposited ) {

			if( true == $boolIsInvoiceUpdateRequired && false == $objInvoice->validate( VALIDATE_UPDATE ) ) {
				return false;
			}

			if( true == $boolIsInvoiceUpdateRequired && false == $objInvoice->update( $objUser->getId(), $objDatabase ) ) {
				return false;
			}

			$arrmixAllocationsData = [];
			if( false == is_null( $objCompanyPayment->getDepositId() ) ) {
				require_once PATH_APPLICATION_CLIENT_ADMIN . 'Library/NetSuiteService/CNetSuiteServiceLibrary.class.php';
				$objNetSuiteServiceLibrary	= new CNetSuiteServiceLibrary();
				$arrmixAllocationsData		= $objNetSuiteServiceLibrary->loadAllocationsDataByExportObjectTypeKey( $objCompanyPayment->getId(), $objDatabase );
			}

			if( false == $objTransaction->reverseWithPayment( $objUser->getId(), $objDatabase, $objAccount, $objCompanyPayment->getId() ) ) {
				return false;
			}

			if( true == valArr( $arrmixAllocationsData ) ) {
				$objNetSuiteServiceLibrary->unallocateCompanyPayments( $arrmixAllocationsData, $objUser->getId(), $objDatabase );
			}
		}

		return true;
	}

	/**
	 * Here we need to notify the company and Entrata Accounting of the return.
	 *
	 *
	 */

	public function deleteCompanyPaymentTransaction( $intPaymentId, $objDatabase ) {

		$strSql = 'DELETE FROM company_payment_transactions
					WHERE
						 company_payment_id = ' . ( int ) $intPaymentId;

		$objDataset = $objDatabase->createDataset();

		if( ( false == $objDataset->execute( $strSql ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to DROP Company payment transaction.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		return true;
	}

	public function sendReturnNotificationEmails( $intUserId, $objDatabase, $objEmailDatabase ) {

		$objClient 	= $this->fetchClient( $objDatabase );
		$objAccount				= $this->fetchAccount( $objDatabase );
		$arrobjPaymentTypes		= CPaymentTypes::fetchAllPaymentTypes( $objDatabase );
		$arrobjReturnTypes		= CReturnTypes::fetchAllReturnTypes( $objDatabase );

		$objInvoice = $this->fetchInvoice( $objDatabase );
		if( true == valObj( $objInvoice, 'CInvoice' ) ) {
			$this->setInvoiceNumber( $objInvoice->getInvoiceNumber() );
		}

		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objSmarty->assign( 'company_payment', 		$this );
		$objSmarty->assign( 'payment_types', 		$arrobjPaymentTypes );
		$objSmarty->assign( 'client',				$objClient );
		$objSmarty->assign( 'account',				$objAccount );

		$objSmarty->assign( 'invoice_method_id_track2', CInvoiceMethod::TRACK_2 );
		$objSmarty->assign( 'title',					'Electronic Payment Return' );
		$objSmarty->assign( 'message',					'Your payment in the amount of $' . __( '{%f, 0, p:2}', [ $this->getPaymentAmount() ] ) . ' returned.' );
		$objSmarty->assign( 'logo_url',				    CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );

		$strClientHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/company_payments/company_payments_email.tpl' );

		if( false == is_null( $this->getReturnTypeId() ) && true == valArr( $arrobjReturnTypes ) && true == array_key_exists( $this->getReturnTypeId(), $arrobjReturnTypes ) ) {
			$objSmarty->assign( 'secondary_message',			'Reason Given: ' . $arrobjReturnTypes[$this->getReturnTypeId()]->getName() );
		} else {
			$objSmarty->assign( 'secondary_message', ' ' );
		}

		$strPsHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/company_payments/company_payments_email.tpl' );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::COMPANY_PAYMENT_ERROR, $strSubject = 'Payment Failure', $strClientHtmlEmailOutput, $strToEmailAddress = NULL, $intIsSelfDestruct = 0, $strFromEmailAddress = CSystemEmail::PROPERTYSOLUTIONS_BILLING_EMAIL_ADDRESS );

		$objPropertySolutionsSystemEmail = clone $objSystemEmail;
		$objPropertySolutionsSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objPropertySolutionsSystemEmail->setHtmlContent( $strPsHtmlEmailOutput );

		switch( $objAccount->getInvoiceMethodId() ) {
			case CInvoiceMethod::TRACK_2:
				$objPropertySolutionsSystemEmail->setToEmailAddress( CSystemEmail::MERCHANTSERVICES_EMAIL_ADDRESS );
				break;

			case CInvoiceMethod::TRACK_1:
			default:
				$objPropertySolutionsSystemEmail->setToEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_BILLING_EMAIL_ADDRESS );
				break;
		}

		// This below code of if condition is just for safer side. When this is tested on live and become stable, we can remove it.
		if( true == is_null( $objPropertySolutionsSystemEmail->getToEmailAddress() ) || '' == trim( $objPropertySolutionsSystemEmail->getToEmailAddress() ) ) {
			$objPropertySolutionsSystemEmail->setToEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_BILLING_EMAIL_ADDRESS );
		}

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objPropertySolutionsSystemEmail->setCid( $objClient->getId() );
		}

		if( false == $objPropertySolutionsSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
			trigger_error( 'System email failed to insert.', E_USER_WARNING );
		}

		if( false == is_null( $objAccount->getBilltoEmailAddress() ) ) {

			$objClientSystemEmail = clone $objSystemEmail;
			$objClientSystemEmail->setToEmailAddress( $objAccount->getBilltoEmailAddress() . ',' . CSystemEmail::RETURNS_EMAIL_ADDRESS );

			if( false == $objClientSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
				trigger_error( 'System email failed to insert.', E_USER_WARNING );
			}
		}

		if( CPsProduct::VACANCY == $this->getPsProductId() && true == is_numeric( $this->getReferenceNumber() ) ) {

			$objVacancySystemEmail = clone $objSystemEmail;
			$objVacancySystemEmail->setSubject( 'Vacancy.com Payment Failure' );
			$objVacancySystemEmail->setToEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_BILLING_EMAIL_ADDRESS . ',' . CSystemEmail::JJONES_EMAIL_ADDRESS );

			if( false == $objVacancySystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
				trigger_error( 'System email failed to insert.', E_USER_WARNING );
			}
		}
	}

	public function applyReturnFee( $intUserId, $objDatabase ) {

		$objAccount = $this->getOrFetchAccount( $objDatabase );

		$fltRerurnFee = $this->generateReturnFee();

		if( true == isset( $fltRerurnFee ) && 0 < $fltRerurnFee ) {
			$objTransaction = $objAccount->createTransaction();
			$objTransaction->setDefaults();
			$objTransaction->setCompanyPaymentId( $this->m_intId );
			$objTransaction->setChargeCodeId( CChargeCode::RETURN_FEE );
			$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
			$objTransaction->setTransactionAmount( $this->generateReturnFee() );
			$objTransaction->setMemo( 'Return fee charge for Company Payment # ' . $this->m_intId );

			// Post the adjustment charge/transaction on the company ledger
			if( false == $objTransaction->validate( VALIDATE_INSERT ) || false == $objTransaction->postCharge( $intUserId, $objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Could not post an Return fee charge on the company ledger for this payment.', NULL ) );
				return false;
			}
		}

		return true;
	}

	public function populatePaymentTransaction( $objTransaction ) {

		$objTransaction->setCid( $this->getCid() );
		$objTransaction->setAccountId( $this->getAccountId() );
		$objTransaction->setChargeCodeId( CChargeCode::PAYMENT_RECEIVED );
		$objTransaction->setCompanyPaymentId( $this->getId() );
		$objTransaction->setTransactionAmount( -1 * $this->getPaymentAmount() );
		$objTransaction->setTransactionDatetime( $this->getPaymentDatetime() );
		$objTransaction->setMemo( $this->getPaymentMemo() );
	}

	public function populateChargeTransaction( $objTransaction ) {

		$objTransaction->setCid( $this->getCid() );
		$objTransaction->setAccountId( $this->getAccountId() );
		$objTransaction->setTransactionAmount( $this->getPaymentAmount() );
		$objTransaction->setTransactionDatetime( $this->getPaymentDatetime() );
		$objTransaction->setMemo( $this->getPaymentMemo() );
	}

	public function populateAllocation( $objAllocation ) {

		$objAllocation->setCid( $this->getCid() );
		$objAllocation->setAccountId( $this->getAccountId() );
		$objAllocation->setAllocationAmount( abs( $this->getPaymentAmount() ) );
		$objAllocation->setAllocationDatetime( $this->getPaymentDatetime() );
	}

	public function loadDefaultAmountDue( $objDatabase, $arrintAccountIds = NULL ) {

		if( false == valArr( $arrintAccountIds, 'CAccount' ) ) {
			$arrintAccountIds = [ $this->fetchAccount( $objDatabase )->getId() ];
		}

		$this->m_fltPaymentAmount = CTransactions::createService()->fetchAccountBalanceByAccountIds( $arrintAccountIds, $objDatabase );

		if( $this->m_fltPaymentAmount < 0 ) {
			$this->m_fltPaymentAmount = 0;
		}

		return $this->m_fltPaymentAmount;
	}

	public function clearAchData() {
		$this->m_strCheckRoutingNumber					= NULL;
		$this->m_strCheckAccountNumberEncrypted 		= NULL;
		$this->m_strCheckConfirmAccountNumberEncrypted	= NULL;
		$this->m_strCheckNameOnAccount					= NULL;
		$this->m_strCheckBankName 						= NULL;
		$this->m_intCheckAccountTypeId					= NULL;
	}

	public function clearCcData() {
		$this->m_strCcCardNumberEncrypted 			= NULL;
		$this->m_strCcExpDateMonth					= NULL;
		$this->m_strCcExpDateYear					= NULL;
		$this->m_strCcNameOnCard 					= NULL;
	}

	public function setCompanyPaymentData( $intInvoiceNumber = NULL, $intAccountId = NULL, $arrmixAccountsDataByInvoice = NULL, $arrmixAccountsDataByAccount = NULL ) {
		$arrmixAccountsData = [];

		if( true == valId( $intInvoiceNumber ) && true == valArr( $arrmixAccountsDataByInvoice[$intInvoiceNumber] ) ) {
			$arrmixAccountsData = $arrmixAccountsDataByInvoice[$intInvoiceNumber];
			$this->setAccountId( $arrmixAccountsData['account_id'] );
			$this->setInvoiceNumber( $intInvoiceNumber );
		} elseif( true == valId( $intAccountId ) && true == valArr( $arrmixAccountsDataByAccount[$intAccountId] ) ) {
			$arrmixAccountsData = $arrmixAccountsDataByAccount[$intAccountId];
			$this->setAccountId( $intAccountId );
			$this->setInvoiceNumber( $arrmixAccountsData['invoice_number'] );
		}

		if( true == valArr( $arrmixAccountsData ) ) {
			$this->setAccountName( $arrmixAccountsData['account_name'] );
			$this->setClientName( $arrmixAccountsData['company_name'] );
			$this->setAccountBalance( $arrmixAccountsData['amount_due'] );
		} elseif( true == valId( $this->getAccountId() ) || true == valId( $this->getInvoiceNumber() ) ) {
			$this->setClientName( 'Invalid Invoice Number / Account #' );
			$this->setAccountName( '-' );
		}
	}

	public function setOrderFormCompanyPaymentData( $arrmixOrderFormDetails, $arrmixCompanyPaymentDetails, $arrmixPaymentOptionDetails, $intPaymentOptionId, $intOrderFormTypeId, $objAdminDatabase ) {

		$this->setId( $this->fetchNextId( $objAdminDatabase ) );
		$this->setAuthToken( $arrmixCompanyPaymentDetails['auth_token'] );
		$this->setChargeCodeId( COrderFormType::$c_arrintOrderFormChargeCodeIds[$intOrderFormTypeId] );
		$this->setCid( $arrmixOrderFormDetails['cid'] );
		$this->setAccountId( $arrmixOrderFormDetails['account_id'] );
		$this->setPaymentTypeId( $arrmixOrderFormDetails['payment_type_id'] );
		$this->setPaymentStatusTypeId( $arrmixOrderFormDetails['payment_status_type_id'] );
		$this->setInvoiceAmount( $arrmixOrderFormDetails['grand_amount'] );
		$this->setPaymentDatetime( date( 'Y-m-d H:i:s' ) );
		$this->setPaymentAmount( $arrmixOrderFormDetails['grand_amount'] );

		switch( $intPaymentOptionId ) {
			case CPaymentType::VISA:
			case CPaymentType::MASTERCARD:
			case CPaymentType::DISCOVER:
			case CPaymentType::AMEX:
				$this->setCcNameOnCard( $arrmixCompanyPaymentDetails['cc_name_on_card'] );
				if( false == is_null( $arrmixCompanyPaymentDetails['cc_card_number'] ) ) {
					$this->setCcCardNumber( $arrmixCompanyPaymentDetails['cc_card_number'] );
				}
				// setting data for credit card details
				if( true == valStr( $this->getCcCardNumberEncrypted() ) || false == valStr( $this->getSecureReferenceNumber() ) ) {
					$arrstrResponse = CPsSecureLibrary::getSecureReferenceNumberFromAuthToken( $arrmixCompanyPaymentDetails['auth_token'] );

					if( true == valArr( $arrstrResponse ) && true == array_key_exists( 'secure_reference_number', $arrstrResponse ) ) {
						$this->setSecureReferenceNumber( $arrstrResponse['secure_reference_number'] );
					}
				}
				$this->setCcExpDateMonth( date( 'm', strtotime( $arrmixCompanyPaymentDetails['expiration_date'] ) ) );
				$this->setCcExpDateYear( date( 'Y', strtotime( $arrmixCompanyPaymentDetails['expiration_date'] ) ) );
				$this->setBilltoStreetLine1( $arrmixPaymentOptionDetails['billto_street_line1'] );
				$this->setBilltoCity( $arrmixPaymentOptionDetails['billto_city'] );
				$this->setBilltoStateCode( $arrmixPaymentOptionDetails['billto_state_code'] );
				$this->setBilltoPostalCode( $arrmixPaymentOptionDetails['billto_postal_code'] );
				break;

			case CPaymentType::ACH:
				$this->setCheckNameOnAccount( $arrmixCompanyPaymentDetails['check_name_on_account'] );
				$this->setCheckAccountTypeId( $arrmixCompanyPaymentDetails['check_account_type_id'] );
				$this->setCheckRoutingNumber( $arrmixCompanyPaymentDetails['check_routing_number'] );
				$this->setCheckAccountNumber( $arrmixCompanyPaymentDetails['check_account_number_encrypted'] );
				$this->setCheckBankName( $arrmixCompanyPaymentDetails['check_bank_name'] );

				$this->setBilltoStreetLine1( $arrmixPaymentOptionDetails['billto_street_line1'] );
				$this->setBilltoCity( $arrmixPaymentOptionDetails['billto_city'] );
				$this->setBilltoStateCode( $arrmixPaymentOptionDetails['billto_state_code'] );
				$this->setBilltoPostalCode( $arrmixPaymentOptionDetails['billto_postal_code'] );
				break;

			case CPaymentType::CHECK:
				return true;
				break;

			default:
				return true;
				break;
		}
		return true;

	}

}
?>
