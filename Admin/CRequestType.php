<?php

class CRequestType extends CBaseRequestType {

	const REIMBURSEMENT				= 1;
	const AMEX						= 2;
	const REIMBURSEMENT_AND_AMEX	= 0;
	const INDIAN_REIMBURSEMENT  	= 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>