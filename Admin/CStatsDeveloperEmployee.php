<?php

class CStatsDeveloperEmployee extends CBaseStatsDeveloperEmployee {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case self::VALIDATE_INSERT:
			case self::VALIDATE_UPDATE:
			case self::VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}
}
?>