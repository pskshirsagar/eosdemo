<?php

class CContractProductStakeholder extends CBaseContractProductStakeholder {

	protected $m_intUserId;
	protected $m_strProductName;
	protected $m_strEmployeeName;

	/**
	 * Get Functions
	 *
	 */

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getProductName() {
		return $this->m_strProductName;
	}

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setProductName( $strProductName ) {
		$this->m_strProductName = $strProductName;
	}

	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = $strEmployeeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['user_id'] ) && 0 < strlen( trim( $arrmixValues['user_id'] ) ) ) {
			$this->setUserId( $arrmixValues['user_id'] );
		}

		if( true == isset( $arrmixValues['product_name'] ) && 0 < strlen( trim( $arrmixValues['product_name'] ) ) ) {
			$this->setProductName( $arrmixValues['product_name'] );
		}

		if( true == isset( $arrmixValues['employee_name'] ) && 0 < strlen( trim( $arrmixValues['employee_name'] ) ) ) {
			$this->setEmployeeName( $arrmixValues['employee_name'] );
		}

		if( isset( $arrmixValues['preferred_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strPreferredName', trim( stripcslashes( $arrmixValues['preferred_name'] ) ) );
		} elseif( isset( $arrmixValues['preferred_name'] ) ) {
			$this->setPreferredName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['preferred_name'] ) : $arrmixValues['preferred_name'] );
		}

		if( isset( $arrmixValues['name'] ) && $boolDirectSet ) {
			$this->set( 'm_strName', trim( stripcslashes( $arrmixValues['name'] ) ) );
		} elseif( isset( $arrmixValues['name'] ) ) {
			$this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['name'] ) : $arrmixValues['name'] );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProductOptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAttentionNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setPreferredName( $strPreferredName ) {
		$this->set( 'm_strPreferredName', CStrings::strTrimDef( $strPreferredName, 100, NULL, true ) );
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

}
?>