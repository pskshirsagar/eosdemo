<?php

class CTaskCategory extends CBaseTaskCategory {

	const HARDWARE              = 1;
	const APPLICATION_SOFTWARE  = 2;
	const PC_PERFORMANCE        = 3;
	const DEV_ENVIRONMENT       = 4;
	const PERMISSION_ACCESS     = 5;
	const INTERNET_NETWORK      = 6;
	const OPERATING_SYSTEM      = 7;
	const ANTIVIRUS             = 8;
	const OTHER                 = 9;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>