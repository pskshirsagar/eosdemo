<?php
use Psi\Eos\Admin\CPersons;
use Psi\Eos\Admin\CClients;
use Psi\Eos\Connect\CDatabases;
use Psi\Eos\Admin\CEmployees;

class CContractDraft extends CBaseContractDraft {

	protected $m_intPsLeadId;
	protected $m_intPropertyCount;
	protected $m_intProductCount;
	protected $m_intSalesEmployeeManagerEmployeeId;

	protected $m_strPsDocumentFileName;
	protected $m_strPsDocumentTitle;
	protected $m_strNote;
	protected $m_strClientName;
	protected $m_strContractName;
	protected $m_strSalesEmployeeNameFirst;
	protected $m_strSalesEmployeeNameLast;
	protected $m_strCompanyName;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ps_lead_id'] ) )							$this->setPsLeadId( $arrmixValues['ps_lead_id'] );
		if( true == isset( $arrmixValues['ps_document_file_name'] ) )				$this->setPsDocumentFileName( $arrmixValues['ps_document_file_name'] );
		if( true == isset( $arrmixValues['ps_document_title'] ) )					$this->setPsDocumentTitle( $arrmixValues['ps_document_title'] );
		if( true == isset( $arrmixValues['note'] ) )								$this->setNote( $arrmixValues['note'] );
		if( true == isset( $arrmixValues['client_name'] ) )							$this->setClientName( $arrmixValues['client_name'] );
		if( true == isset( $arrmixValues['contract_name'] ) )						$this->setContractName( $arrmixValues['contract_name'] );
		if( true == isset( $arrmixValues['property_count'] ) )						$this->setPropertyCount( $arrmixValues['property_count'] );
		if( true == isset( $arrmixValues['product_count'] ) )						$this->setProductCount( $arrmixValues['product_count'] );
		if( true == isset( $arrmixValues['sales_employee_name_first'] ) )			$this->setSalesEmployeeNameFirst( $arrmixValues['sales_employee_name_first'] );
		if( true == isset( $arrmixValues['sales_employee_name_last'] ) )			$this->setSalesEmployeeNameLast( $arrmixValues['sales_employee_name_last'] );
		if( true == isset( $arrmixValues['sales_employee_manager_employee_id'] ) )	$this->setSalesEmployeeManagerEmployeeId( $arrmixValues['sales_employee_manager_employee_id'] );
		if( true == isset( $arrmixValues['company_name'] ) )						$this->setCompanyName( $arrmixValues['company_name'] );
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->m_intPsLeadId = $intPsLeadId;
	}

	public function setPsDocumentFileName( $strPsDocumentFileName ) {
		$this->m_strPsDocumentFileName = $strPsDocumentFileName;
	}

	public function setPsDocumentTitle( $strPsDocumentTitle ) {
		$this->m_strPsDocumentTitle = $strPsDocumentTitle;
	}

	public function setNote( $strNote ) {
		$this->m_strNote = $strNote;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setContractName( $strContractName ) {
		$this->m_strContractName = $strContractName;
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function setProductCount( $intProductCount ) {
		$this->m_intProductCount = $intProductCount;
	}

	public function setSalesEmployeeNameFirst( $strSalesEmployeeNameFirst ) {
		$this->m_strSalesEmployeeNameFirst = $strSalesEmployeeNameFirst;
	}

	public function setSalesEmployeeNameLast( $strSalesEmployeeNameLast ) {
		$this->m_strSalesEmployeeNameLast = $strSalesEmployeeNameLast;
	}

	public function setSalesEmployeeManagerEmployeeId( $intSalesEmployeeManagerEmployeeId ) {
		$this->m_intSalesEmployeeManagerEmployeeId = $intSalesEmployeeManagerEmployeeId;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function getPsDocumentFileName() {
		return $this->m_strPsDocumentFileName;
	}

	public function getPsDocumentTitle() {
		return $this->m_strPsDocumentTitle;
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getContractName() {
		return $this->m_strContractName;
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function getProductCount() {
		return $this->m_intProductCount;
	}

	public function getSalesEmployeeNameFirst() {
		return $this->m_strSalesEmployeeNameFirst;
	}

	public function getSalesEmployeeNameLast() {
		return $this->m_strSalesEmployeeNameLast;
	}

	public function getSalesEmployeeManagerEmployeeId() {
		return $this->m_intSalesEmployeeManagerEmployeeId;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractDraftStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactPersonId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubject() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsUrgent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSentOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientSignedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientSignedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCounterSignedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCounterSignedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {

		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolValid &= $this->validateContractDraft();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolValid = false;
				break;
		}

		return $boolValid;
	}

	public function validateContractDraft() {

		$boolValid = true;

		if( false == valStr( $this->getSubject() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Subject is required.' ) );
		}

		if( false == valId( $this->getPsDocumentId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Ps Document is required.' ) );
		}

		if( false == valId( $this->getContractDraftStatusId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Contract Draft Status is required.' ) );
		}

		if( false == valId( $this->getSalesEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'RVP is required.' ) );
		}

		return $boolValid;
	}

	public function insertContractDraftAction( $arrmixActionDetails, $intEmployeeId, $intUserId, $objAdminDatabase, $boolContractDraftNote = false ) {

		if( false == valId( $this->getId() ) ) {
			return false;
		}

		$objAction = new CAction();
		$objAction->setActionTypeId( CActionType::NOTE );
		$objAction->setActionDatetime( date( 'm/d/Y H:i:s' ) );
		$objAction->setPsLeadId( $this->getPsLeadId() );
		$objAction->setContractId( $this->getContractId() );
		$objAction->setEmployeeId( $intEmployeeId );
		$objAction->setActionDescription( $arrmixActionDetails['action_description'] );
		$objAction->setNotes( $arrmixActionDetails['notes'] );
		$objAction->setActionCategoryId( CActionCategory::ID_SALES );

		if( valStr( $arrmixActionDetails['is_note'] ) ) {
			$objAction->setActionTypeId( CActionType::CONTRACT_DRAFT_NOTE );
			$objAction->setPrimaryReference( $this->getId() );
		}

		if( false == $objAction->validate( VALIDATE_INSERT ) || false == $objAction->insert( $intUserId, $objAdminDatabase ) ) {
			$this->addErrorMsgs( $objAction->getErrorMsgs() );
			return false;
		}

		if( true == $boolContractDraftNote ) {
			// This reference will be used to fetch contract draft note.
			$objActionReferenceForContractDraft = new CActionReference();
			$objActionReferenceForContractDraft->setActionReferenceTypeId( CActionReferenceType::CONTRACT_DRAFT );
			$objActionReferenceForContractDraft->setReferenceNumber( $this->getId() );
			$objActionReferenceForContractDraft->setActionId( $objAction->getId() );

			if( false == $objActionReferenceForContractDraft->validate( VALIDATE_INSERT ) || false == $objActionReferenceForContractDraft->insert( $intUserId, $objAdminDatabase ) ) {
				$this->addErrorMsgs( $objActionReferenceForContractDraft->getErrorMsgs() );
				return false;
			}
		}

		if( true == valId( $this->getContactPersonId() ) ) {
			// This reference will be used to fetch the contact person.
			$objActionReferenceForContractPerson = new CActionReference();
			$objActionReferenceForContractPerson->setActionReferenceTypeId( CActionReferenceType::PERSON );
			$objActionReferenceForContractPerson->setReferenceNumber( $this->getContactPersonId() );
			$objActionReferenceForContractPerson->setActionId( $objAction->getId() );

			if( false == $objActionReferenceForContractPerson->validate( VALIDATE_INSERT ) || false == $objActionReferenceForContractPerson->insert( $intUserId, $objAdminDatabase ) ) {
				$this->addErrorMsgs( $objActionReferenceForContractPerson->getErrorMsgs() );
				return false;
			}
		}

		return true;
	}

	public function sendContractReadyToSignEmail( $intUserId, $objAdminDatabase, $objEmailDatabase ) {

		$objPerson = CPersons::createService()->fetchPersonById( $this->getContactPersonId(), $objAdminDatabase );
		$objEmployee = CEmployees::createService()->fetchEmployeeById( $this->getSalesEmployeeId(), $objAdminDatabase );
		$objReportingManagerEmployee = CEmployees::createService()->fetchReportingManagerByEmployeeId( $this->getSalesEmployeeId(), $objAdminDatabase );

		if( !valObj( $objPerson, 'CPerson' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_draft_person_id', 'Unable to load the contact person.' ) );
			return false;
		}

		if( !valStr( $objPerson->getEmailAddress() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_draft_person_id', 'The contact person does not have an email address.' ) );
			return false;
		}

		$objClient = CClients::createService()->fetchClientById( $objPerson->getCid(), $objAdminDatabase );

		if( !valObj( $objClient, 'CClient' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_draft_cid', 'Unable to load the client.' ) );
			return false;
		}

		$objLocaleContainer = CLocaleContainer::createService();
		$objConnectDatabase = CDatabases::createService()->createConnectDatabase();

		if( valObj( $objConnectDatabase, 'CDatabase' ) ) {

			$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $objPerson->getCid(), $objConnectDatabase );

			if( valObj( $objClientDatabase, 'CDatabase' ) ) {

				$objClientDatabase->open();
				$objLocaleContainer->initializeByClient( $objClient, $objClientDatabase );

				if( valId( $objPerson->getCompanyUserId() ) ) {
					$objCompanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByCompanyUserIdByCid( $objPerson->getCompanyUserId(), $objPerson->getCid(), $objClientDatabase );

					if( valObj( $objCompanyEmployee, 'CCompanyEmployee' ) && valStr( $objCompanyEmployee->getPreferredLocaleCode() ) ) {
						$objLocaleContainer->setLocaleCode( $objCompanyEmployee->getPreferredLocaleCode() );
					}
				}

				$objClientDatabase->close();
			}

			$objConnectDatabase->close();
		}

		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$arrmixTemplateParameters = [];

		$arrmixTemplateParameters['CONFIG_COMPANY_BASE_DOMAIN']	= CONFIG_COMPANY_BASE_DOMAIN;
		$arrmixTemplateParameters['logo_url']					= CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG;
		$arrmixTemplateParameters['person']						= $objPerson;
		$arrmixTemplateParameters['company_name']				= $objClient->getCompanyName();
		$arrmixTemplateParameters['contract_request']			= $this->getSubject() ?? NULL;
		$arrmixTemplateParameters['contract_draft_sign_url']	= CConfig::get( 'secure_host_prefix' ) . $objClient->getRwxDomain() . CConfig::get( 'rwx_login_suffix' ) . '/?module=new_dashboardxxx&contract_draft_id=' . $this->getId();

		$objRenderTemplate		= new CRenderTemplate( 'sales/contract_drafts/contract_ready_to_sign_email.tpl', PATH_INTERFACES_CLIENT_ADMIN, false, $arrmixTemplateParameters );
		$strHtmlContent			= $objRenderTemplate->nestedFetchTemplate( PATH_INTERFACES_CLIENT_ADMIN . 'sales/contract_drafts/contract_ready_to_sign_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::COMPANY_EMPLOYEE,  __( 'Contract ready to sign - {%s, 0}', [ $objClient->getCompanyName() ] ), $strHtmlContent, $objPerson->getEmailAddress(), 0, CSystemEmail::CONTRACTS_EMAIL_ADDRESS );

		$arrstrCcEmailAddresses = [];
		if( valObj( $objEmployee, 'CEmployee' ) ) {
			$arrstrCcEmailAddresses[] = $objEmployee->getEmailAddress();
		}

		if( valObj( $objReportingManagerEmployee, 'CEmployee' ) ) {
			$arrstrCcEmailAddresses[] = $objReportingManagerEmployee->getEmailAddress();
		}

		if( valArr( $arrstrCcEmailAddresses ) ) {
			$objSystemEmail->setCcEmailAddress( implode( ', ', $arrstrCcEmailAddresses ) );
		}

		return ( $objSystemEmail->validate( VALIDATE_INSERT ) && $objSystemEmail->insert( $intUserId, $objEmailDatabase ) );
	}

}
