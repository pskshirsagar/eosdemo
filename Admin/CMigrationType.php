<?php

class CMigrationType extends CBaseMigrationType {
	const ENTRATA_WORKBOOK	= 1;
	const SIM				= 2;
	const SYSTEM_WORKBOOK	= 3;

	const STR_ENTRATA_WORKBOOK	= 'Entrata Workbook';
	const STR_SIM				= 'S.I.M';
	const STR_SYSTEM_WORKBOOK	= 'System Workbook';

	public static $c_arrintMigrationTypeIds = [ self::ENTRATA_WORKBOOK, self::SIM, self::SYSTEM_WORKBOOK ];

	const C_ARRSTRMIGRATIONTYPES = [
		self::ENTRATA_WORKBOOK	=> 'Entrata Workbook',
		self::SIM				=> 'S.I.M',
		self::SYSTEM_WORKBOOK	=> 'System Workbook'
	];

}
?>