<?php

class CStatsPilot extends CBaseStatsPilot {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsLeadId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProductName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTransactionalAverage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCloseDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractEnteredPipelineOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractWonOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractLostOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractTerminatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPilotLastActiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTerminationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubscriptionAcvUsd() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionAcvUsd() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsWon() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>