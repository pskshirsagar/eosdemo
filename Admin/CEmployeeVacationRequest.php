<?php

class CEmployeeVacationRequest extends CBaseEmployeeVacationRequest {

		protected $m_intIsApproved;
		protected $m_intApprovedEmployeeVacationRequestHoursCount;
		protected $m_intHalfDayHours;
		protected $m_intApprovedEmployeeCompensatoryOffsCount;

		protected $m_arrstrCompensatoryOffs;
		protected $m_arrstrManagerEmailAddresses;

		protected $m_arrobjTeamEmployees;

		// Restricting compensatory off facility until 31st Jan 2013
		const COMPENSATORY_OFF_END_DATE									= '1/31/2013';
		const DAILY_EXPECTED_WORKING_HOURS								= 8;
		const DAILY_EXPECTED_HALF_DAY_HOURS								= 4;
		const VACATION_HOURS_FOR_HALF_DAY_LEAVE							= 4;
		const DAY_OF_WEEK_SATURDAY										= 6;
		const DAY_OF_WEEK_SUNDAY										= 0;
		const EXCEPTIONAL_WORKING_DAY									= '09/17/2016';
		const VACATION_TYPE_MARRIAGE_LEAVES_WORKING_HOURS 				= 32;
		const VACATION_TYPE_MARRIAGE_ANNIVERSARY_LEAVES_WORKING_HOURS 	= 8;
		const VACATION_TYPE_BIRTHDAY_LEAVES_WORKING_HOURS 	= 8;
		const PER_DAY_SECONDS											= 86400;
		const WEEKEND_WORKING_DAYS										= 2;
		const VACATION_TYPE_PATERNITY_LEAVES_WORKING_HOURS 				= 24;
		const LEAVE_POOL_HALF_YEAR_DATE									= 25;
		const VACATION_HOURS_OF_MONTH									= 150;
		const VACATION_TYPE_OPTIONAL_HOLIDAY_WORKING_HOURS 				= 16;
		const VACATION_TYPE_COVID_HOME_ISOLATION_WORKING_HOURS 			= 40;
		const VACATION_TYPE_COVID_HOSPITALIZATION_WORKING_HOURS 		= 80;

		const FIRST_HALF_BEGIN_TIME 	= '8:00:00';
		const FIRST_HALF_END_TIME		= '12:00:00';
		const SECOND_HALF_BEGIN_TIME	= '13:00:00';
		const SECOND_HALF_END_TIME		= '17:00:00';

		/**
		 * Set Functions
		 *
		 */

		public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

			parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
			if( true == isset( $arrmixValues['is_approved'] ) ) $this->setIsApproved( $arrmixValues['is_approved'] );
			if( true == isset( $arrmixValues['approved_employee_vacation_requests_hours_count'] ) ) $this->setApprovedEmployeeVacationRequestHoursCount( $arrmixValues['approved_employee_vacation_requests_hours_count'] );
			if( true == isset( $arrmixValues['half_day'] ) ) $this->setHalfDayHours( $arrmixValues['half_day'] );
			if( true == isset( $arrmixValues['approved_employee_compensatory_offs_count'] ) ) $this->setApprovedEmployeeCompensatoryOffsCount( $arrmixValues['approved_employee_compensatory_offs_count'] );
			if( true == isset( $arrmixValues['preferred_name'] ) ) $this->setPreferredName( $arrmixValues['preferred_name'] );
			return;
		}

		public function setIsApproved( $intIsApproved ) {
			$this->m_intIsApproved = $intIsApproved;
		}

		public function setApprovedEmployeeVacationRequestHoursCount( $intApprovedEmployeeVacationRequestHoursCount ) {
			$this->m_intApprovedEmployeeVacationRequestHoursCount = $intApprovedEmployeeVacationRequestHoursCount;
		}

		public function setHalfDayHours( $intHalfDayHours ) {
			$this->m_intHalfDayHours = $intHalfDayHours;
		}

		public function setCompensatoryOffs( $arrstrCompensatoryOffs ) {
			$this->m_arrstrCompensatoryOffs = $arrstrCompensatoryOffs;
		}

		public function setApprovedEmployeeCompensatoryOffsCount( $intApprovedEmployeeCompensatoryOffsCount ) {
			$this->m_intApprovedEmployeeCompensatoryOffsCount = $intApprovedEmployeeCompensatoryOffsCount;
		}

		public function setDefaultsForVacationRequestAddedByHr( $strDate ) {

			$this->setBeginDatetime( $strDate . ' 08:00:00' );
			$this->setEndDatetime( $strDate . ' 17:00:00' );
			$this->setVacationTypeId( CVacationType::UNSCHEDULED );
			$this->setScheduledVacationHours( self::DAILY_EXPECTED_WORKING_HOURS );
			$this->setRequest( 'Time off added by HR' );

			return;
		}

		public function setDefaultsForLateMarksVacationRequestAddedByHr( $intEmployeeId, $intCurrentUserId ) {

			$this->setIsPaid( 0 );
			$this->setEmployeeId( $intEmployeeId );
			$this->setRequest( 'Time off added by HR for latemarks.' );
			$this->setVacationTypeId( CVacationType::UNSCHEDULED );
			$this->setScheduledVacationHours( CEmployeeVacationRequest::VACATION_HOURS_FOR_HALF_DAY_LEAVE );
			$this->setIsApproved( 1 );
			$this->setApprovedBy( $intCurrentUserId );
			$this->setApprovedOn( 'NOW()' );

			return;
		}

		public function setPreferredName( $strPreferredName ) {
			$this->m_strPreferredName = $strPreferredName;
		}

		/**
		 * Get Functions
		 *
		 */

		public function getIsApproved() {
			return $this->m_intIsApproved;
		}

		public function getApprovedEmployeeVacationRequestHoursCount() {
			return $this->m_intApprovedEmployeeVacationRequestHoursCount;
		}

		public function getHalfDayHours() {
			return $this->m_intHalfDayHours;
		}

		public function getCompensatoryOffs() {
			return $this->m_arrstrCompensatoryOffs;
		}

		public function getApprovedEmployeeCompensatoryOffsCount() {
			return $this->m_intApprovedEmployeeCompensatoryOffsCount;
		}

		public function getPreferredName() {
			return $this->m_strPreferredName;
		}

		/**
		 * Validate Functions
		 *
		 */

		public function valEmployeeId() {
			$boolIsValid = true;

			if( true == is_null( $this->getEmployeeId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Name is required.' ) );
			}

			return $boolIsValid;
		}

		public function valVacationTypeId( $objDatabase = NULL ) {
			$boolIsValid = true;

			if( false == is_null( $this->getEmployeeId() ) && true == is_null( $this->getVacationTypeId() ) ) {
				$boolIsValid = false;
				$strMessage = 'Time off type is required.';
				if( CVacationType::WEEKEND_WORKING == $this->getVacationTypeId() ) {
					$strMessage = 'Working type is required.';
				}
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vacation_type_id', $strMessage ) );
			}

			if( false == is_null( $this->getEmployeeId() ) && false == is_null( $this->getVacationTypeId() ) && true == valObj( $objDatabase, 'CDatabase' ) ) {
				$objEmployeeWeekend			= new CEmployeeWeekend();
				$arrstrEmployeeWeekendDays	= $objEmployeeWeekend->loadEmployeeWeekends( date( 'Y', strtotime( $this->getBeginDatetime() ) ), $objDatabase, array( $this->getEmployeeId() ) );

				if( $this->getVacationTypeId() == CVacationType::COMPENSATORY_OFF && ( true == valArr( $arrstrEmployeeWeekendDays ) && ( true == in_array( date( 'm/d/Y', strtotime( $this->getBeginDatetime() ) ), $arrstrEmployeeWeekendDays['weekends'] ) || ( false == in_array( date( 'm/d/Y', strtotime( $this->getBeginDatetime() ) ), $arrstrEmployeeWeekendDays['swap_days'] ) && true == in_array( date( 'w', strtotime( $this->getBeginDatetime() ) ), array( self::DAY_OF_WEEK_SATURDAY, self::DAY_OF_WEEK_SUNDAY ) ) ) ) ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vacation_type_id', 'Compensatory off is not applicable on weekend.' ) );
				}

				if( ( date( 'm/d/Y', strtotime( $this->getBeginDatetime() ) ) == date( 'm/d/Y', strtotime( $this->getEndDatetime() ) ) ) && true == valArr( $arrstrEmployeeWeekendDays ) && true == in_array( date( 'm/d/Y', strtotime( $this->getBeginDatetime() ) ), $arrstrEmployeeWeekendDays['weekends'] ) && false == in_array( $this->getVacationTypeId(), array( CVacationType::WEEKEND_WORKING, CVacationType::WORK_FROM_HOME, CVacationType::COMPENSATORY_OFF ) ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vacation_type_id', 'Leave is not applicable.' ) );
				}
			}

			return $boolIsValid;
		}

		public function valBeginDatetime() {
			$boolIsValid = true;
			$strErrorMsg = 'Begin date is required.';
			if( CVacationType::OPTIONAL_HOLIDAY == $this->getVacationTypeId() ) {
				$strErrorMsg = 'Date is required.';
			}
			if( true == is_null( $this->getBeginDatetime() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_datetime', $strErrorMsg ) );
			}

			return $boolIsValid;
		}

		public function valEndDatetime() {
			$boolIsValid = true;

			$strBeginDate = strtotime( $this->getBeginDatetime() );
			$strEndDate   = strtotime( $this->getEndDatetime() );

			if( true == is_null( $this->getEndDatetime() ) && CVacationType::OPTIONAL_HOLIDAY != $this->getVacationTypeId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', 'End date is required.' ) );
			} elseif( ( $strEndDate - $strBeginDate ) <= 0 && CVacationType::OPTIONAL_HOLIDAY != $this->getVacationTypeId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', 'End date should be greater than or equal to begin date.' ) );
			}

			return $boolIsValid;
		}

		public function valScheduledVacationHours( $strEmployeeCountryCode = NULL, $boolIsUSRequest = false ) {
			$boolIsValid = true;

			if( ( true == is_null( $this->getScheduledVacationHours() ) || 0 == $this->getScheduledVacationHours() ) && true == valStr( $this->getBeginDatetime() ) ) {
				$boolIsValid = false;
				if( CCountry::CODE_USA == $strEmployeeCountryCode && false == $boolIsUSRequest ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_vacation_hours', 'Scheduled time off hours is required.' ) );
				} elseif( CCountry::CODE_USA == $strEmployeeCountryCode && true == $boolIsUSRequest ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_vacation_hours', 'Leave is not applicable on paid holidays/weekends. Please change begin/end date.' ) );
				} else {
					if( CVacationType::WEEKEND_WORKING != $this->getVacationTypeId() ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_vacation_hours', 'Leave is not applicable.' ) );
					}
				}
			}

			return $boolIsValid;
		}

		public function valScheduledVacationTimeOffHours( $strEmployeeCountryCode = NULL, $boolIsUSRequest = false ) {
			$boolIsValid = true;

			if( ( true == is_null( $this->getScheduledVacationHours() ) || 0 == $this->getScheduledVacationHours() ) && true == valStr( $this->getBeginDatetime() ) ) {
				$boolIsValid = false;
				if( CCountry::CODE_USA == $strEmployeeCountryCode && false == $boolIsUSRequest ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_vacation_hours', 'You are not currently scheduled during the selected hours.' ) );
				}
			}

			return $boolIsValid;
		}

		public function valRequest( $strEmployeeCountryCode=NULL ) {
			$boolIsValid = true;

			if( true == is_null( $this->getRequest() ) ) {
				$boolIsValid = false;

				if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request', 'Reason is required.' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request', 'Time off request is required.' ) );
				}
			}

			return $boolIsValid;
		}

		public function  valScheduledLeave( $strEmployeeCountryCode ) {
			$boolIsValid = true;
			if( CCountry::CODE_INDIA == $strEmployeeCountryCode && $this->getVacationTypeId() == CVacationType::SCHEDULED && strtotime( getConvertedDateTimeByTimeZoneName( $this->getBeginDatetime(), 'm/d/Y', 'MDT' ) ) < strtotime( getConvertedDateTimeByTimeZoneName( $this->getCreatedOn(), 'm/d/Y', 'MDT' ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Previous date is not allowed for Scheduled leaves.' ) );
			}
			return $boolIsValid;
		}

        public function valUnscheduledLeave( $strEmployeeCountryCode ) {
            $boolIsValid = true;
            if( CCountry::CODE_INDIA == $strEmployeeCountryCode && $this->getVacationTypeId() == CVacationType::UNSCHEDULED && strtotime( getConvertedDateTimeByTimeZoneName( $this->getBeginDatetime(), 'm/d/Y', 'MDT' ) ) > strtotime( getConvertedDateTimeByTimeZoneName( $this->getCreatedOn(), 'm/d/Y', 'MDT' ) ) ) {
                $boolIsValid = false;
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Future date is not allowed for Unscheduled leaves.' ) );
            }
            return $boolIsValid;
        }

		public function valCompensatoryOffs() {
			$boolIsValid = true;

			if( true == valArr( $this->getCompensatoryOffs() ) ) {

				foreach( $this->getCompensatoryOffs() as $strCompensatoryOff ) {
					if( strtotime( date( 'm/d/Y', strtotime( $this->getBeginDatetime() ) ) ) > strtotime( date( 'm/d/Y', strtotime( '+6 month', strtotime( $strCompensatoryOff ) ) ) ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, date( 'd M, Y', strtotime( $strCompensatoryOff ) ) . ' Compensatory Off requested expires on ' . date( 'd M, Y', strtotime( '+6 month', strtotime( $strCompensatoryOff ) ) ) ) );
					}
				}
			}

		return $boolIsValid;
		}

		public function valCompensatoryOffsWithBeginDate() {
			$boolIsValid = true;

			if( true == valArr( $this->getCompensatoryOffs() ) ) {

				foreach( $this->getCompensatoryOffs() as $strCompensatoryOff ) {
					if( false == is_null( $this->getBeginDatetime() ) && strtotime( date( 'm/d/Y', strtotime( $this->getBeginDatetime() ) ) ) < strtotime( date( 'm/d/Y', strtotime( $strCompensatoryOff ) ) ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Requested Begin Date ' . date( 'd M, Y', strtotime( $this->getBeginDatetime() ) ) . ' should be greater than compensatory Off requested date ' . date( 'd M, Y', strtotime( $strCompensatoryOff ) ) ) );
					}
				}
			}

			return $boolIsValid;
		}

		public function valRepetativeLeave( $objDatabase ) {
			$boolIsValid = true;

			if( false == is_null( $this->getEmployeeId() ) && false == is_null( $this->getEndDatetime() ) && false == is_null( $this->getBeginDatetime() ) ) {

				$strCondition = '';

				if( false == is_null( $this->getId() ) ) {
					$strCondition = 'id != ' . ( int ) $this->getId() . ' AND ';
				}
				$strSql	= 'WHERE ' . $strCondition . 'employee_id = ' . $this->getEmployeeId() . ' AND end_datetime >= \'' . $this->getBeginDatetime() . '\' AND begin_datetime <= \'' . $this->getEndDatetime() . '\' AND denied_by IS NULL AND deleted_by IS NULL AND vacation_type_id = ' . CVacationType::WEEKEND_WORKING;

				$intCountEmployeeWeekendWorkingRequests = CEmployeeVacationRequests::fetchEmployeeVacationRequestCount( $strSql, $objDatabase );

				if( 0 < $intCountEmployeeWeekendWorkingRequests ) {
					$boolIsValid = false;
					if( date( 'm/d/Y', strtotime( $this->getBeginDatetime() ) ) == date( 'm/d/Y', strtotime( $this->getEndDatetime() ) ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vacation_request_type', 'Weekend Working Request is already added for date ' . date( 'm/d/Y', strtotime( $this->getBeginDatetime() ) ) . '.' ) );
					} else {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vacation_request_type', 'Weekend Working Request is already added for dates between ' . date( 'm/d/Y', strtotime( $this->getBeginDatetime() ) ) . ' and ' . date( 'm/d/Y', strtotime( $this->getEndDatetime() ) ) . '.' ) );
					}
				}
			}
			return $boolIsValid;
		}

		public function validate( $strAction, $strEmployeeCountryCode = NULL, $boolIsUSRequest = false, $boolIsRepetativeLeave = false, $objDatabase = NULL ) {

			$boolIsValid = true;

			switch( $strAction ) {
				case VALIDATE_INSERT:
				case VALIDATE_UPDATE:
					$boolIsValid &= $this->valEmployeeId();
					$boolIsValid &= $this->valVacationTypeId( $objDatabase );
					$boolIsValid &= $this->valBeginDatetime();
					$boolIsValid &= $this->valEndDatetime();
					if( true == $boolIsValid ) $boolIsValid &= $this->valScheduledVacationHours( $strEmployeeCountryCode, $boolIsUSRequest );
					$boolIsValid &= $this->valRequest( $strEmployeeCountryCode );
					if( true == $boolIsValid && true == $boolIsRepetativeLeave ) $boolIsValid &= $this->valRepetativeLeave( $objDatabase );
					$boolIsValid &= $this->valScheduledLeave( $strEmployeeCountryCode );
                    $boolIsValid &= $this->valUnscheduledLeave( $strEmployeeCountryCode );
					// $boolIsValid &= $this->valCompensatoryOffs();
					// $boolIsValid &= $this->valCompensatoryOffsWithBeginDate();
					break;

				case VALIDATE_DELETE:
					break;

				case 'validate_scheduled_time_off':
					$boolIsValid &= $this->valEmployeeId();
					$boolIsValid &= $this->valVacationTypeId( $objDatabase );
					$boolIsValid &= $this->valBeginDatetime();
					$boolIsValid &= $this->valEndDatetime();
					if( true == $boolIsValid ) $boolIsValid &= $this->valScheduledVacationTimeOffHours( $strEmployeeCountryCode );
					$boolIsValid &= $this->valRequest( $strEmployeeCountryCode );
					if( true == $boolIsValid && true == $boolIsRepetativeLeave ) $boolIsValid &= $this->valRepetativeLeave( $objDatabase );
					$boolIsValid &= $this->valScheduledLeave( $strEmployeeCountryCode );
					break;

				default:
					// default case
					$boolIsValid = true;
					break;
			}

			return $boolIsValid;
		}

		/**
		* Other Functions
		*
		*/

		public function delete( $intUserId, $objAdminDatabase, $boolReturnSqlOnly = false ) {

			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( ' NOW() ' );
			$this->setUpdatedBy( $intUserId );
			$this->setUpdatedOn( '  NOW() ' );
			if( $this->update( $intUserId, $objAdminDatabase, $boolReturnSqlOnly ) ) {
				return true;
			}
		}

		public function sendEmployeeVacationRequestEmails( $arrobjEmployeeVacationRequests, $arrintEmployeeIds, $objUser, $objDatabase, $objEmailDatabase, $arrmixEmployeeVacationSchedules = NULL, $boolIsScript = false ) {

			if( true == valArr( $arrobjEmployeeVacationRequests ) && true == valArr( $arrintEmployeeIds ) ) {

				$arrobjSystemEmails = [];

				$this->m_arrobjTeamEmployees = \Psi\Eos\Admin\CTeamEmployees::createService()->fetchTeamEmployeesByPrimaryTeam( $objDatabase );
				$this->m_arrobjTeamEmployees = rekeyObjects( 'EmployeeId', $this->m_arrobjTeamEmployees );

				$arrobjEmployees = \Psi\Eos\Admin\CEmployees::createService()->fetchEmployeesByIds( $arrintEmployeeIds, $objDatabase );

				foreach( $arrobjEmployeeVacationRequests as $objEmployeeVacationRequest ) {

					$objEmployee = $arrobjEmployees[$objEmployeeVacationRequest->getEmployeeId()];

					$arrstrToSendEmailAddresses = $arrstrEmployeeManagerEmailAddresses = [];

					if( false == is_null( $objEmployeeVacationRequest->getEmployeeId() ) ) {
						$arrobjManagerEmployees      = $objEmployee->fetchManagerEmployees( $objDatabase );
						$intManagerCount             = \Psi\Libraries\UtilFunctions\count( $arrobjManagerEmployees );
						$intEmployeeIsProjectManager = $objEmployee->getIsProjectManager();
						$objReportingManager         = \Psi\Eos\Admin\CEmployees::createService()->fetchReportingManagerByEmployeeId( $objEmployee->getId(), $objDatabase );

						if( false == valObj( $objReportingManager, 'CEmployee' ) ) {
							$this->displayMessage( '', 'Failed to load manager.', '/?module=employees-new' );
							exit;
						}

						if( 2 < \Psi\Libraries\UtilFunctions\count( $arrobjManagerEmployees ) ) {
							foreach( $arrobjManagerEmployees as $intKey => $objEmployee ) {
								if( $objEmployee->getId() == CEmployee::ID_DAVID_BATEMEN || ( 3 != $intManagerCount && CEmployee::ID_CHASE_HARRINGTON == $objEmployee->getId() ) || ( 4 < $intManagerCount && 27 >= $objEmployeeVacationRequest->getScheduledVacationHours() && CDesignation::DIRECTOR_OF_QUALITY == $objEmployee->getDesignationId() ) ) {
									unset( $arrobjManagerEmployees[$intKey] );
								}
							}
						}

						if( 1 == $intEmployeeIsProjectManager ) {
							foreach( $arrobjManagerEmployees as $objManagerEmployee ) {
								if( false == valObj( $this->m_arrobjTeamEmployees[$objEmployee->getId()], 'CTeamEmployee' ) || ( true == valObj( $this->m_arrobjTeamEmployees[$objEmployee->getId()], 'CTeamEmployee' ) && $this->m_arrobjTeamEmployees[$objEmployee->getId()]->getManagerEmployeeId() == $objManagerEmployee->getId() ) || CEmployee::ID_DAVID_BATEMEN != $objManagerEmployee->getId() ) {
									$arrstrEmployeeManagerEmailAddresses[$objManagerEmployee->getId()] = $objManagerEmployee->getEmailAddress();
								}
							}
						} else {
							$arrobjManagerEmployees = array_reverse( $arrobjManagerEmployees );

							foreach( $arrobjManagerEmployees as $objManagerEmployee ) {
								if( false == valObj( $this->m_arrobjTeamEmployees[$objEmployee->getId()], 'CTeamEmployee' ) || ( true == valObj( $this->m_arrobjTeamEmployees[$objEmployee->getId()], 'CTeamEmployee' ) && $this->m_arrobjTeamEmployees[$objEmployee->getId()]->getManagerEmployeeId() == $objManagerEmployee->getId() ) || CEmployee::ID_DAVID_BATEMEN != $objManagerEmployee->getId() ) {
									$arrstrEmployeeManagerEmailAddresses[$objManagerEmployee->getId()] = $objManagerEmployee->getEmailAddress();
								}
							}
						}

						if( CDepartment::QA != $objEmployee->getDepartmentId() && 3 < \Psi\Libraries\UtilFunctions\count( $arrobjManagerEmployees ) && true == valObj( $objEmployee, 'CEmployee' ) ) {
							if( false == is_null( getArrayElementByKey( CEmployee::ID_PREETAM_YADAV, $arrstrEmployeeManagerEmailAddresses ) ) ) {
								unset( $arrstrEmployeeManagerEmailAddresses[CEmployee::ID_PREETAM_YADAV] );
							}
						} else {
							if( $objReportingManager->getId() != CEmployee::ID_PREETAM_YADAV && CDepartment::QA != $objEmployee->getDepartmentId() && 24 >= $objEmployeeVacationRequest->getScheduledVacationHours() ) {
								if( false == is_null( getArrayElementByKey( CEmployee::ID_PREETAM_YADAV, $arrstrEmployeeManagerEmailAddresses ) ) ) {
									unset( $arrstrEmployeeManagerEmailAddresses[CEmployee::ID_PREETAM_YADAV] );
								}
							} else {
								if( $objReportingManager->getId() == CEmployee::ID_PRATHMASH_AUSEKAR && CDepartment::QA == $objEmployee->getDepartmentId() && 24 >= $objEmployeeVacationRequest->getScheduledVacationHours() && false == is_null( getArrayElementByKey( CEmployee::ID_PREETAM_YADAV, $arrstrEmployeeManagerEmailAddresses ) ) ) {
									unset( $arrstrEmployeeManagerEmailAddresses[CEmployee::ID_PREETAM_YADAV] );
								} elseif( $objReportingManager->getId() != CEmployee::ID_PREETAM_YADAV && $objReportingManager->getId() != CEmployee::ID_PRATHMASH_AUSEKAR && CDepartment::QA == $objEmployee->getDepartmentId() ) {
									if( 3 < \Psi\Libraries\UtilFunctions\count( $arrobjManagerEmployees ) && 24 < $objEmployeeVacationRequest->getScheduledVacationHours() ) {
										unset( $arrstrEmployeeManagerEmailAddresses[CEmployee::ID_PREETAM_YADAV] );
									} else {
										unset( $arrstrEmployeeManagerEmailAddresses[CEmployee::ID_PRATHMASH_AUSEKAR] );
										unset( $arrstrEmployeeManagerEmailAddresses[CEmployee::ID_PREETAM_YADAV] );
									}
								}
							}
						}

						if( CDepartment::QA != $objEmployee->getDepartmentId() && 3 < \Psi\Libraries\UtilFunctions\count( $arrobjManagerEmployees ) && true == valObj( $objEmployeeVacationRequest, 'CEmployeeVacationRequest' ) && false == is_null( $objEmployeeVacationRequest->getScheduledVacationHours() ) && 24 >= $objEmployeeVacationRequest->getScheduledVacationHours() ) {
							$objAddEmployee = \Psi\Eos\Admin\CEmployees::createService()->fetchAddEmployeeByEmployeeId( $objEmployeeVacationRequest->getEmployeeId(), $objDatabase );

							if( true == valObj( $objAddEmployee, 'CEmployee' ) && false == is_null( getArrayElementByKey( $objAddEmployee->getId(), $arrstrEmployeeManagerEmailAddresses ) ) ) {
								unset( $arrstrEmployeeManagerEmailAddresses[$objAddEmployee->getId()] );
							}
						}

						if( CCountry::CODE_INDIA == $objUser->getEmployee()->getCountryCode() ) {
							$arrstrHrGroupEmailAddresses = [ CSystemEmail::XENTO_HR_EMAIL_ADDRESS ];
						}

						if( array_key_exists( CEmployee::ID_RYAN_BYRD, $arrstrEmployeeManagerEmailAddresses ) ) {
							unset( $arrstrEmployeeManagerEmailAddresses[CEmployee::ID_RYAN_BYRD] );
						}

						if( $objReportingManager->getId() != CEmployee::ID_CHRIS_MARTIN ) {
							unset( $arrstrEmployeeManagerEmailAddresses[CEmployee::ID_CHRIS_MARTIN] );
						}

						if( true == valArr( $arrstrEmployeeManagerEmailAddresses ) && true == valArr( $arrstrHrGroupEmailAddresses ) ) {
							$arrstrToSendEmailAddresses = array_merge( $arrstrEmployeeManagerEmailAddresses, $arrstrHrGroupEmailAddresses );
						}

						$arrmixScriptValues = [];
						if( true == $boolIsScript ) {
							$objVacationType                    = \Psi\Eos\Admin\CVacationTypes::createService()->fetchVacationTypeById( $objEmployeeVacationRequest->getVacationTypeId(), $objDatabase );
							$arrmixLeavesStatus                 = \Psi\Eos\Admin\CEmployees::createService()->fetchEmployeeNameByEmployeeVacationRequestIds( [ $objEmployeeVacationRequest->getId() ], $objDatabase );
							$arrmixScriptValues['vacationType'] = $objVacationType;
							$arrmixScriptValues['leavesStatus'] = $arrmixLeavesStatus;
							$strSubject                         = 'Re: Approved: ' . $objEmployee->getNameFirst() . ' ' . $objEmployee->getNameLast() . ':';
							$strSubject                         .= ' Time Off Request Response for ' . ( $objEmployeeVacationRequest->getScheduledVacationHours() / self::DAILY_EXPECTED_WORKING_HOURS ) . ( ( $objEmployeeVacationRequest->getScheduledVacationHours() <= self::DAILY_EXPECTED_WORKING_HOURS ) ? ' Day' : ' Days' );
						} else {
							$strSubject = $objEmployee->getPreferredName() . ' : Application for Time Off Request';
						}

						$strHtmlEmailOutput = $this->buildHtmlVacationRequestEmailContent( $objEmployeeVacationRequest, $objEmployee, $objUser, $arrmixEmployeeVacationSchedules, $arrmixScriptValues );

						$strEmailAddresses  = NULL;

						$objSystemEmailLibrary = new CSystemEmailLibrary();
						$objSystemEmail        = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, addslashes( $strHtmlEmailOutput ) );

						$objSystemEmailTemp		= clone $objSystemEmail;
						$objSystemEmailTemp->setToEmailAddress( $objEmployee->getEmailAddress() );
						$arrobjSystemEmails[] = $objSystemEmailTemp;

						foreach( $arrstrToSendEmailAddresses as $strToSendEmailAddress ) {
							if( false == is_null( $strToSendEmailAddress ) && $objEmployee->getEmailAddress() != $strToSendEmailAddress ) {
								$strEmailAddresses .= $strToSendEmailAddress . ',';
							}
						}

						if( true == valStr( $strEmailAddresses ) ) {
							$objSystemEmailTemp = clone $objSystemEmail;
							$objSystemEmailTemp->setToEmailAddress( \Psi\CStringService::singleton()->substr( $strEmailAddresses, 0, -1 ) );
							$arrobjSystemEmails[] = $objSystemEmailTemp;
						}
					}
				}

				switch( NULL ) {
					default:

						if( true == valArr( $arrobjSystemEmails ) ) {

							$objEmailDatabase->begin();

							foreach( $arrobjSystemEmails as $objSystemEmail ) {
								if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
									if( false == $objSystemEmail->insert( $objUser->getId(), $objEmailDatabase ) ) {
										$objEmailDatabase->rollback();
										break 2;
									}
								}
							}

							$objEmailDatabase->commit();
						}
				}
			}
		}

		public function buildHtmlVacationRequestEmailContent( $objEmployeeVacationRequest, $objEmployee, $objUser = NULL, $arrmixEmployeeVacationSchedules = NULL, $arrmixScriptValues = NULL ) {

			require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

			$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

			$objSmarty->assign( 'employee', 						$objEmployee );
			$objSmarty->assign( 'employee_vacation_request',	 	$objEmployeeVacationRequest );
			$objSmarty->assign( 'current_user',						$objUser );
			$objSmarty->assign( 'employee_vacation_schedule',		$arrmixEmployeeVacationSchedules );
			$objSmarty->assign( 'COUNTRY_CODE_USA',					 CCountry::CODE_USA );
			$objSmarty->assign( 'COUNTRY_CODE_INDIA',				 CCountry::CODE_INDIA );
			$objSmarty->assign( 'base_uri',							CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
			$objSmarty->assign( 'logo_url',  						CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );

			if( true == isset( $arrmixEmployeeVacationSchedules[$objEmployee->getId()] ) && true == isset( $arrmixEmployeeVacationSchedules[$objEmployee->getId()]['paid_approved_count'] ) && true == isset( $arrmixEmployeeVacationSchedules[$objEmployee->getId()]['lop_count'] ) ) {
				$objSmarty->assign( 'employee_paid_vacation_hours_count', $arrmixEmployeeVacationSchedules[$objEmployee->getId()]['paid_approved_count'] );
				$objSmarty->assign( 'employee_LOP_hours_count', $arrmixEmployeeVacationSchedules[$objEmployee->getId()]['lop_count'] );
				$objSmarty->assign( 'bool_partial_lop', !is_null( $arrmixEmployeeVacationSchedules[$objEmployee->getId()]['new_end_date'] ) );
			}

			if( true == valArr( $arrmixScriptValues ) ) {
				$objSmarty->assign( 'vacation_type',				$arrmixScriptValues['vacationType'] );
				$objSmarty->assign( 'boolIsResponse',				true );
				$objSmarty->assign( 'employee_name',				$objEmployee->getPreferredName() );
				$objSmarty->assign( 'leaves_status',				$arrmixScriptValues['leavesStatus'] );
				$objSmarty->assign( 'boolIsScript',					true );

				return $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employees/employee_vacation_requests/india_employee_vacation_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
			} else {
				return $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employees_attendance_report/employee_vacation_request_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
			}
		}

		public function loadManagerEmailAddresses( $intEmployeeId, $intVacationRequestEmployeeId ) {

			if( false == array_key_exists( $intEmployeeId, $this->m_arrobjTeamEmployees ) ) return;

			$this->m_arrstrManagerEmailAddresses[$intVacationRequestEmployeeId][$this->m_arrobjTeamEmployees[$intEmployeeId]->getEmailAddress()] = $this->m_arrobjTeamEmployees[$intEmployeeId]->getEmailAddress();

			if( true == $this->m_arrobjTeamEmployees[$intEmployeeId]->getIsProjectManager() && $intEmployeeId != $intVacationRequestEmployeeId ) {
				return;
			}

			$this->loadManagerEmailAddresses( $this->m_arrobjTeamEmployees[$intEmployeeId]->getManagerEmployeeId(), $intVacationRequestEmployeeId );
		}

		public function calculateEmployeesVacationRequestsDetailsCount( $arrintEmployeeIds, $objDatabase ) {

			if( false == valArr( $arrintEmployeeIds ) ) {
				return NULL;
			}

			$arrmixAllEmployeeVacationDetails	= [];
			$intHalfYear						= 06;

			if( date( 'm' ) <= $intHalfYear && strtotime( date( 'm/d/Y' ) ) <= strtotime( date( $intHalfYear . '/' . self::LEAVE_POOL_HALF_YEAR_DATE . '/Y' ) ) ) {
				$strHalfYearlyBeginDatetime = date( 'Y-m-d H:i:s', strtotime( '' . date( 'Y' ) - 1 . '-12-26' . ' 00:00:00' ) );
				$strHalfYearlyEndDatetime   = date( 'Y-m-d H:i:s', strtotime( '' . date( 'Y' ) . '-06-25' . ' 23:59:00' ) );
			} else {
				$strHalfYearlyBeginDatetime = date( 'Y-m-d H:i:s', strtotime( '' . date( 'Y' ) . '-06-26' . ' 00:00:00' ) );
				$strHalfYearlyEndDatetime   = date( 'Y-m-d H:i:s', strtotime( '' . date( 'Y' ) . '-12-25' . ' 23:59:00' ) );
			}

			$arrobjEmployeeVacationSchedules = \Psi\Eos\Admin\CEmployeeVacationSchedules::createService()->fetchEmployeeVacationSchedulesByYearByEmployeeIds( date( 'Y' ), $arrintEmployeeIds, $objDatabase );
			$arrobjEmployeeVacationSchedules = rekeyObjects( 'EmployeeId', $arrobjEmployeeVacationSchedules );

			if( true == valArr( $arrobjEmployeeVacationSchedules ) ) {
				$objEmployeeWeekend                = new CEmployeeWeekend();
				$arrmixPaidHolidays                = \Psi\Eos\Admin\CPaidHolidays::createService()->fetchPaidHolidaysByCountryCode( CCountry::CODE_INDIA, $objDatabase );
				$arrmixAllEmployeeVacationRequests = \Psi\Eos\Admin\CEmployeeVacationRequests::createService()->fetchDetailEmployeeVacationRequestsByEmployeeIdsByDateRange( $arrintEmployeeIds, $strHalfYearlyBeginDatetime, $strHalfYearlyEndDatetime, CCountry::CODE_INDIA, $objDatabase );
				$arrmixAllEmployeeVacationRequests = rekeyArray( 'employee_id', $arrmixAllEmployeeVacationRequests, false, true, true );

				foreach( $arrobjEmployeeVacationSchedules as $intEmployeeId => $objEmployeeVacationSchedule ) {

					$arrmixAllEmployeeVacationDetails[$intEmployeeId]['total_count']         = $objEmployeeVacationSchedule->getHoursGranted();
					$arrmixAllEmployeeVacationDetails[$intEmployeeId]['paid_approved_count'] = 0;
					$arrmixAllEmployeeVacationDetails[$intEmployeeId]['lop_count']           = 0;
					$arrmixAllEmployeeVacationDetails[$intEmployeeId]['trainee_count']       = 0;
					$arrmixAllEmployeeVacationDetails[$intEmployeeId]['new_end_date']        = NULL;
					$arrmixAllEmployeeVacationDetails[$intEmployeeId]['old_request']         = NULL;
					$arrmixAllEmployeeVacationDetails[$intEmployeeId]['new_request']         = NULL;
					$arrmixAllEmployeeVacationDetails[$intEmployeeId]['vacation_schedule']   = $objEmployeeVacationSchedule;

					if( true == valArr( $arrmixAllEmployeeVacationRequests ) && !empty( $arrmixAllEmployeeVacationRequests[$intEmployeeId] ) ) {
						foreach( $arrmixAllEmployeeVacationRequests[$intEmployeeId] as $arrmixEmployeeVacationRequest ) {

							$arrstrEmployeeWeekendDays = ( array ) $objEmployeeWeekend->loadEmployeeWeekends( date( 'Y', strtotime( $arrmixEmployeeVacationRequest['begin_datetime'] ) ), $objDatabase, [ $intEmployeeId ] );
							if( ( true == valArr( $arrstrEmployeeWeekendDays['weekends'] ) && true == in_array( date( 'm/d/Y', strtotime( $arrmixEmployeeVacationRequest['date'] ) ), $arrstrEmployeeWeekendDays['weekends'] ) ) || ( true == valArr( $arrstrEmployeeWeekendDays['swap_days'] ) && false == in_array( date( 'm/d/Y', strtotime( $arrmixEmployeeVacationRequest['date'] ) ), $arrstrEmployeeWeekendDays['swap_days'] ) && true == in_array( date( 'w', strtotime( $arrmixEmployeeVacationRequest['date'] ) ), [ self::DAY_OF_WEEK_SATURDAY, self::DAY_OF_WEEK_SUNDAY ] ) ) || ( false == valArr( $arrstrEmployeeWeekendDays['weekends'] ) && true == in_array( date( 'w', strtotime( $arrmixEmployeeVacationRequest['date'] ) ), [ self::DAY_OF_WEEK_SATURDAY, self::DAY_OF_WEEK_SUNDAY ] ) ) || true == array_key_exists( date( 'm/d/Y', strtotime( $arrmixEmployeeVacationRequest['date'] ) ), $arrmixPaidHolidays ) ) {
								continue;
							}

							if( true == in_array( $arrmixEmployeeVacationRequest['vacation_type_id'], CVacationType::$c_arrintLeavePoolVacationTypes ) ) {
								// Calculate all non special time off(s) ( Deductible from time off pool )
								if( strtotime( $arrmixEmployeeVacationRequest['date'] ) >= strtotime( $strHalfYearlyBeginDatetime ) && strtotime( $arrmixEmployeeVacationRequest['date'] ) <= strtotime( $strHalfYearlyEndDatetime ) ) {
									if( $arrmixEmployeeVacationRequest['vacation_category_type'] == CVacationCategoryType::LOP ) {
										$arrmixAllEmployeeVacationDetails[$intEmployeeId]['lop_count'] += $arrmixEmployeeVacationRequest['vacation_hours'];
									} elseif( $arrmixEmployeeVacationRequest['vacation_category_type'] == CVacationCategoryType::TRAINEE ) {
										$arrmixAllEmployeeVacationDetails[$intEmployeeId]['trainee_count'] += $arrmixEmployeeVacationRequest['vacation_hours'];
									} else {
										$arrmixAllEmployeeVacationDetails[$intEmployeeId]['paid_approved_count'] += $arrmixEmployeeVacationRequest['vacation_hours'];
									}
								}
							}

						}
						if( $arrmixAllEmployeeVacationDetails[$intEmployeeId]['total_count'] < $arrmixAllEmployeeVacationDetails[$intEmployeeId]['paid_approved_count'] ) {
							$intLopDueToBalance                                                      = $arrmixAllEmployeeVacationDetails[$intEmployeeId]['paid_approved_count'] - $arrmixAllEmployeeVacationDetails[$intEmployeeId]['total_count'];
							$arrmixAllEmployeeVacationDetails[$intEmployeeId]['paid_approved_count'] -= $intLopDueToBalance;
							$arrmixAllEmployeeVacationDetails[$intEmployeeId]['lop_count']           += $intLopDueToBalance;
						}
					}
				}
			}

			return $arrmixAllEmployeeVacationDetails;
		}

		public function loadEmployeeVactionRequestDates( $strDateFrom, $strDateTo ) {

			$arrstrDateRange = array();

			$intFromDate = strtotime( $strDateFrom );
			$intToDate	 = strtotime( $strDateTo );

			if( $intToDate >= $intFromDate ) {

				array_push( $arrstrDateRange, date( 'Y-m-d', $intFromDate ) );

				while( $intFromDate < $intToDate ) {
					$intFromDate += self::PER_DAY_SECONDS; // add 24 hours
					array_push( $arrstrDateRange, date( 'Y-m-d', $intFromDate ) );
				}
			}

			return $arrstrDateRange;
		}

	public function loadEmployeeCompensatoryOffs( $intEmployeeId, $objAdminDatabase, $boolReturnAvailedCompensatoryHours = false, $boolUnusedRequest = false ) {
		$objTeam							= CTeams::fetchPrimaryTeamByEmployeeId( $intEmployeeId, $objAdminDatabase );
		$strCompensatoryOffBeginDate		= date( 'm/d/Y', strtotime( '-2 months' ) );
		if( true == valObj( $objTeam, 'CTeam' ) ) {
			$arrmixOfficeShiftAssociations		= COfficeShiftAssociations::fetchOfficeShiftAssociationsByDepartmentIdsByTeamIds( $objAdminDatabase, NULL, array( $objTeam->getId() ), NULL, $intEmployeeId, false, true );
		}
		if( true == valArr( $arrmixOfficeShiftAssociations ) ) {
			$arrobjCompensatoryOffEmployeeHours = rekeyObjects( 'date', ( array ) \Psi\Eos\Admin\CEmployeeHours::createService()->fetchCompensatoryOffEmployeeHoursByEmployeeIdByDate( $intEmployeeId, $strCompensatoryOffBeginDate, $objAdminDatabase, NULL, $boolUnusedRequest ) );
			$arrmixPaidHolidays	= array_keys( rekeyArray( 'date', \Psi\Eos\Admin\CPaidHolidays::createService()->fetchOptionalPaidHolidaysByCountryCodeByDateRange( $strCompensatoryOffBeginDate, date( 'm/d/Y' ), CCountry::CODE_INDIA, $objAdminDatabase, true, $intEmployeeId ) ) );
			if( true == valArr( $arrobjCompensatoryOffEmployeeHours ) ) {
				foreach( $arrobjCompensatoryOffEmployeeHours as $strDate => $objCompensatoryOffEmployeeHour ) {
					if( 't' == $objCompensatoryOffEmployeeHour->getIsOptional() && false == in_array( $strDate, $arrmixPaidHolidays ) ) {
						unset( $arrobjCompensatoryOffEmployeeHours[$strDate] );
					}
				}
			}

			$arrobjPaidHolidays = rekeyObjects( 'Date', \Psi\Eos\Admin\CPaidHolidays::createService()->fetchPaidHolidaysByDateRangeByCountryCode( $strCompensatoryOffBeginDate, date( 'm/d/Y' ), CCountry::CODE_INDIA, $objAdminDatabase, true ) );
			if( true == valArr( $arrobjPaidHolidays ) ) {
				foreach( $arrobjPaidHolidays as $strDate => $objPaidHoliday ) {
					if( 't' == $objPaidHoliday->getIsOptional() && false == in_array( $strDate, $arrmixPaidHolidays ) ) {
						unset( $arrobjPaidHolidays[$strDate] );
					}
				}
			}
			$arrstrPaidHolidays = array_keys( ( array ) $arrobjPaidHolidays );
			if( true == valArr( $arrstrPaidHolidays ) ) {
				$objEmployeeWeekend = new CEmployeeWeekend();
				$arrstrEmployeeWeekends = $objEmployeeWeekend->loadEmployeeWeekends( date( 'Y' ), $objAdminDatabase, [ $intEmployeeId ] );
				$arrstrEmployeeCompensatoryOffData = ( array ) rekeyArray( 'paid_off_date', \Psi\Eos\Admin\CEmployeeVacationRequests::createService()->fetchEmployeeCompensatoryOffByEmployeeIdByPaidHolidayDates( $intEmployeeId, $arrstrPaidHolidays, $objAdminDatabase ) );

				$objCompensatoryOffEmployeeHour = new CEmployeeHour();
				$objCompensatoryOffEmployeeHour->setEmployeeId( $intEmployeeId );

				foreach( $arrstrPaidHolidays as $strPaidHoliday ) {
					if( ( true == valArr( $arrstrEmployeeWeekends ) && true == in_array( $strPaidHoliday, $arrstrEmployeeWeekends['weekends'] ) ) || ( false == valArr( $arrstrEmployeeWeekends ) && true == in_array( date( 'w', strtotime( $strPaidHoliday ) ), [ self::DAY_OF_WEEK_SATURDAY, self::DAY_OF_WEEK_SUNDAY ] ) ) ) {
						$objCompensatoryOffEmployeeHourClone = clone $objCompensatoryOffEmployeeHour;
						$objCompensatoryOffEmployeeHourClone->setDate( $strPaidHoliday );

						if( true == valArr( $arrstrEmployeeCompensatoryOffData[$strPaidHoliday] ) ) {
							if( CEmployeeHour::MIN_REQUIRED_ONFLOOR_HOURS_FOR_FULL_DAY > $arrstrEmployeeCompensatoryOffData[$strPaidHoliday]['scheduled_vacation_hours'] ) {
								$objCompensatoryOffEmployeeHourClone->setIsHalfDayCompensatoryOff( true );
							} else {
								continue;
							}
						}
						$arrobjCompensatoryOffEmployeeHours[$strPaidHoliday] = $objCompensatoryOffEmployeeHourClone;
					}
					if( 't' == $arrmixOfficeShiftAssociations[0]['is_paid_holiday'] && false == in_array( date( 'm/d', strtotime( $strPaidHoliday ) ), CPaidHoliday::$c_arrintNationalHolidays ) ) {
						unset( $arrobjCompensatoryOffEmployeeHours[$strPaidHoliday] );
					}
				}
			}
			$arrstrNationalHolidays = CPaidHoliday::$c_arrintNationalHolidays;
			array_walk( $arrstrNationalHolidays, function ( &$strNationalHolidayDate, $intKey ) {
				$strNationalHolidayDate = $strNationalHolidayDate . '/' . date( 'Y' );
			} );
			$arrobjNationalHolidayEmployeeHours = ( array ) rekeyArray( 'paid_off_date', \Psi\Eos\Admin\CEmployeeVacationRequests::createService()->fetchEmployeeCompensatoryOffByEmployeeIdByPaidHolidayDates( $intEmployeeId, $arrstrNationalHolidays, $objAdminDatabase ) );

			if( true == valArr( $arrobjNationalHolidayEmployeeHours ) ) {
				foreach( $arrobjNationalHolidayEmployeeHours as $strDate => $objCompensatoryOffEmployeeHour ) {

					if( true == in_array( $strDate, $arrstrNationalHolidays ) ) {
						$arrstrNationalHolidays = array_filter( $arrstrNationalHolidays, function( $e ) use ( $strDate ) {
							return ( $e !== $strDate );
						} );
					}
				}
			}
			if( true == valArr( $arrstrNationalHolidays ) ) {
				$objCompensatoryOffEmployeeHour    = new CEmployeeHour();
				$objCompensatoryOffEmployeeHour->setEmployeeId( $intEmployeeId );
				foreach( $arrstrNationalHolidays as $strNationalHoliday ) {

					if( strtotime( $strCompensatoryOffBeginDate ) <= strtotime( $strNationalHoliday ) && strtotime( date( 'm/d/Y' ) ) >= strtotime( $strNationalHoliday ) ) {
						if( false == in_array( $strNationalHoliday, $arrstrPaidHolidays ) ) {
							if( ( true == in_array( date( 'w', strtotime( $strNationalHoliday ) ), [self::DAY_OF_WEEK_SATURDAY, self::DAY_OF_WEEK_SUNDAY] ) ) ) {
								$objCompensatoryOffEmployeeHourClone = clone $objCompensatoryOffEmployeeHour;
								$objCompensatoryOffEmployeeHourClone->setDate( $strNationalHoliday );
								// We need to restict on  the  comp off for the entire Devops team and Tushar, Dattatray N & Varun from DBA Team.
								if( ( false == in_array( $intEmployeeId, [683, 3093, 7908] ) ) && ( true == valObj( $objTeam, 'CTeam' ) && ( false == in_array( $objTeam->getId(), [1104, 844, 326, 607, 129, 710, 65] ) ) ) ) {
										$arrobjCompensatoryOffEmployeeHours[$strNationalHoliday] = $objCompensatoryOffEmployeeHourClone;
								}
							}
							if( false == in_array( date( 'm/d', strtotime( $strNationalHoliday ) ), CPaidHoliday::$c_arrintNationalHolidays ) ) {
								unset( $arrobjNationalHolidayEmployeeHours[$strNationalHoliday] );
							}
						}
					}
				}
			}
			$arrstrAvailedCompensatoryOffDates	= array_keys( $arrobjCompensatoryOffEmployeeHours );

			if( true == $boolReturnAvailedCompensatoryHours ) {
				$intAvailedCompensatoryOffHours = 0;

				foreach( $arrstrAvailedCompensatoryOffDates as $strSelectedCompensatoryOffDate ) {
					if( true == valObj( $arrobjCompensatoryOffEmployeeHours[$strSelectedCompensatoryOffDate], 'CEmployeeHour' ) ) {
						$intAvailedCompensatoryOffHours += ( true == $arrobjCompensatoryOffEmployeeHours[$strSelectedCompensatoryOffDate]->getIsHalfDayCompensatoryOff() ) ? self::DAILY_EXPECTED_HALF_DAY_HOURS : CEmployeeVacationRequest::DAILY_EXPECTED_WORKING_HOURS;
					}
				}
			}
		}

		return ( false == $boolReturnAvailedCompensatoryHours ) ? $arrobjCompensatoryOffEmployeeHours : $intAvailedCompensatoryOffHours;
	}

	public function loadEmployeeCompensatoryOffRequests( $intEmployeeVacationScheduledHours, $arrmixRequest, $arrobjCompensatoryOffEmployeeHours, $objDatabase, $arrmixPaidHolidays, $arrstrPaidHolidays, $intEmployeeId ) {

		if( true == valArr( $arrobjCompensatoryOffEmployeeHours ) ) {
			$intAvailedCompensatoryOffHours    = 0;
			$arrstrAvailedCompensatoryOffDates = array_keys( $arrobjCompensatoryOffEmployeeHours );

			$arrstrEmployeeCompensatoryOffData = ( array ) rekeyArray( 'paid_off_date', \Psi\Eos\Admin\CEmployeeVacationRequests::createService()->fetchEmployeeCompensatoryOffByEmployeeIdByPaidHolidayDates( $intEmployeeId, $arrstrPaidHolidays, $objDatabase, true ) );
			foreach( $arrstrAvailedCompensatoryOffDates as $intKey => $strSelectedCompensatoryOffDate ) {
				if( true == valArr( $arrstrEmployeeCompensatoryOffData ) && true == array_key_exists( $strSelectedCompensatoryOffDate, $arrstrEmployeeCompensatoryOffData ) ) {
					if( false == valId( $arrstrEmployeeCompensatoryOffData[$strSelectedCompensatoryOffDate]['denied_by'] ) ) {
						unset( $arrstrAvailedCompensatoryOffDates[$intKey] );
					}
				}
			}
			foreach( $arrstrAvailedCompensatoryOffDates as $intKey => $strSelectedCompensatoryOffDate ) {
				$arrstrAvailedCompensatoryOffDates[$intKey] = strtotime( $strSelectedCompensatoryOffDate );

				if( $arrstrAvailedCompensatoryOffDates[$intKey] < strtotime( date( 'm/d/Y', strtotime( '-2 month', strtotime( $arrmixRequest['begin_date'] ) ) ) ) ) {
					unset( $arrstrAvailedCompensatoryOffDates[$intKey] );
				}

				if( true == valObj( $arrobjCompensatoryOffEmployeeHours[$strSelectedCompensatoryOffDate], 'CEmployeeHour' ) ) {
					if( true == $arrobjCompensatoryOffEmployeeHours[$strSelectedCompensatoryOffDate]->getIsHalfDayCompensatoryOff() && CEmployeeVacationRequest::DAILY_EXPECTED_HALF_DAY_HOURS < $this->getScheduledVacationHours() ) unset( $arrstrAvailedCompensatoryOffDates[$intKey] );
					$intAvailedCompensatoryOffHours += ( true == $arrobjCompensatoryOffEmployeeHours[$strSelectedCompensatoryOffDate]->getIsHalfDayCompensatoryOff() ) ? self::DAILY_EXPECTED_HALF_DAY_HOURS : CEmployeeVacationRequest::DAILY_EXPECTED_WORKING_HOURS;
				}
			}
			\Psi\CStringService::singleton()->sort( $arrstrAvailedCompensatoryOffDates );

			$arrstrAvailedCompensatoryOffDates	= array_values( $arrstrAvailedCompensatoryOffDates );

			foreach( $arrstrAvailedCompensatoryOffDates as $intKey => $strSelectedCompensatoryOffDate ) {
				$arrstrAvailedCompensatoryOffDates[$intKey] = date( 'm/d/Y', $strSelectedCompensatoryOffDate );
			}

			$arrobjEmployeeVacationRequests		= array();
			if( $intEmployeeVacationScheduledHours <= $intAvailedCompensatoryOffHours ) {

				$objEmployeeWeekend			= new CEmployeeWeekend();
				$arrstrEmployeeWeekendDays	= $objEmployeeWeekend->loadEmployeeWeekends( date( 'Y', strtotime( $arrmixRequest['begin_date'] ) ), $objDatabase, array( $this->getEmployeeId() ) );

				if( ( true == valArr( $arrstrEmployeeWeekendDays['weekends'] ) && true == in_array( date( 'm/d/Y', strtotime( $this->getBeginDatetime() ) ), $arrstrEmployeeWeekendDays['weekends'] ) ) || true == in_array( date( 'w', strtotime( $this->getBeginDatetime() ) ), array( self::DAY_OF_WEEK_SATURDAY, self::DAY_OF_WEEK_SUNDAY ) ) ) {
					$arrmixRequest['begin_date'] = $arrmixRequest['end_date'];
				}

				$intScheduledVacationHours = ( 'second_half' == $arrmixRequest['begin_date_half_day'] || ( strtotime( $arrmixRequest['begin_date'] ) == strtotime( $arrmixRequest['end_date'] ) && 'first_half' == $arrmixRequest['begin_date_half_day'] ) ) ? self::DAILY_EXPECTED_HALF_DAY_HOURS : self::DAILY_EXPECTED_WORKING_HOURS;

				$this->setBeginDatetime( $arrmixRequest['begin_date'] . ' ' . ( ( 'second_half' == $arrmixRequest['begin_date_half_day'] ) ? self::SECOND_HALF_BEGIN_TIME : self::FIRST_HALF_BEGIN_TIME ) );
				$this->setEndDatetime( $arrmixRequest['begin_date'] . ' ' . ( ( strtotime( $arrmixRequest['begin_date'] ) == strtotime( $arrmixRequest['end_date'] ) && 'first_half' == $arrmixRequest['begin_date_half_day'] ) ? self::FIRST_HALF_END_TIME : self::SECOND_HALF_END_TIME ) );
				$this->setScheduledVacationHours( $intScheduledVacationHours );
				$this->setPaidOffDate( $arrstrAvailedCompensatoryOffDates[0] );
				$arrobjEmployeeVacationRequests[] = $this;
				if( strtotime( $arrmixRequest['begin_date'] ) < strtotime( $arrmixRequest['end_date'] ) ) {
					$arrstrDates = $this->loadEmployeeVactionRequestDates( $arrmixRequest['begin_date'], $arrmixRequest['end_date'] );
					$intEmployeeVacationScheduledHours -= $intScheduledVacationHours;
					foreach( $arrstrDates as $intIndex => $strDate ) {
						if( ( true == valArr( $arrstrEmployeeWeekendDays['weekends'] ) && true == in_array( date( 'm/d/Y', strtotime( $strDate ) ), $arrstrEmployeeWeekendDays['weekends'] ) ) || ( true == valArr( $arrstrEmployeeWeekendDays['swap_days'] ) && false == in_array( date( 'm/d/Y', strtotime( $strDate ) ), $arrstrEmployeeWeekendDays['swap_days'] ) && true == in_array( date( 'w', strtotime( $strDate ) ), [ self::DAY_OF_WEEK_SATURDAY, self::DAY_OF_WEEK_SUNDAY ] ) ) || ( false == valArr( $arrstrEmployeeWeekendDays['weekends'] ) && true == in_array( date( 'w', strtotime( $strDate ) ), [ self::DAY_OF_WEEK_SATURDAY, self::DAY_OF_WEEK_SUNDAY ] ) ) || false == is_null( getArrayElementByKey( date( 'm/d/Y', strtotime( $strDate ) ), $arrmixPaidHolidays ) ) ) {
							unset( $arrstrDates[$intIndex] );
						}
					}
					\Psi\CStringService::singleton()->sort( $arrstrDates );
					foreach( $arrstrDates as $intIndex => $strDate ) {
						if( 0 == $intIndex ) continue;
						if( 0 <= $intEmployeeVacationScheduledHours ) {
							$objEmployeeVacationRequest = clone $this;
							$objEmployeeVacationRequest->setId( NULL );
							$objEmployeeVacationRequest->setBeginDatetime( $strDate . ' ' . ( ( self::DAILY_EXPECTED_WORKING_HOURS <= $intEmployeeVacationScheduledHours ) ? self::FIRST_HALF_BEGIN_TIME : ( ( strtotime( $strDate ) == strtotime( $arrmixRequest['end_date'] ) && 'first_half' == $arrmixRequest['end_date_half_day'] ) ? self::FIRST_HALF_BEGIN_TIME : self::SECOND_HALF_BEGIN_TIME ) ) );
							$objEmployeeVacationRequest->setEndDatetime( $strDate . ' ' . ( ( self::DAILY_EXPECTED_WORKING_HOURS <= $intEmployeeVacationScheduledHours ) ? self::SECOND_HALF_END_TIME : self::FIRST_HALF_END_TIME ) );
							$objEmployeeVacationRequest->setScheduledVacationHours( ( self::DAILY_EXPECTED_WORKING_HOURS <= $intEmployeeVacationScheduledHours ) ? self::DAILY_EXPECTED_WORKING_HOURS : self::DAILY_EXPECTED_HALF_DAY_HOURS );
							$objEmployeeVacationRequest->setPaidOffDate( $arrstrAvailedCompensatoryOffDates[$intIndex] );

							$arrobjEmployeeVacationRequests[] = $objEmployeeVacationRequest;
						}
					}
				}
			}
		}

		$arrmixEmployeeVacationRequest['employee_vacation_requests'] 		= $arrobjEmployeeVacationRequests;
		$arrmixEmployeeVacationRequest['availed_compensatory_off_hours']	= $intAvailedCompensatoryOffHours;
		$arrmixEmployeeVacationRequest['availed_compensatory_off_dates']	= $arrstrAvailedCompensatoryOffDates;
		return $arrmixEmployeeVacationRequest;
	}

	public function getQuarterDateRange( $strMonth ) {
		$intQuarter = ceil( $strMonth / 3 );
		switch( $intQuarter ) {
			case 1:
				return array( 'begin_date' => getConvertedDateTime( date( '01/01/Y' ), 'm/d/Y' ), 'end_date' => getConvertedDateTime( date( '03/t/Y' ), 'm/d/Y' ) );
				break;
			case 2:
				return array( 'begin_date' => getConvertedDateTime( date( '04/01/Y' ), 'm/d/Y' ), 'end_date' => getConvertedDateTime( date( '06/t/Y' ), 'm/d/Y' ) );
				break;
			case 3:
				return array( 'begin_date' => getConvertedDateTime( date( '07/01/Y' ), 'm/d/Y' ), 'end_date' => getConvertedDateTime( date( '09/t/Y' ), 'm/d/Y' ) );
				break;
			case 4:
				return array( 'begin_date' => getConvertedDateTime( date( '10/01/Y' ), 'm/d/Y' ), 'end_date' => getConvertedDateTime( date( '12/t/Y' ), 'm/d/Y' ) );
				break;
			default:
				return false;
		}
	}

}
?>