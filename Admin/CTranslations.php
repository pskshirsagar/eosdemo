<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTranslations
 * Do not add any new functions to this class.
 */

class CTranslations extends CBaseTranslations {

	public static function fetchTranslationValue( $strApplicationName, $strNamespace, $strLocaleCode, $strTranslationKey, $boolAcceptMachineTranslated, $objDatabase ) {
		return self::fetchTranslationValues( $strApplicationName, $strNamespace, $strLocaleCode, [ $strTranslationKey ], $boolAcceptMachineTranslated, $objDatabase );
	}

	public static function fetchTranslationValues( $strApplicationName, $strNamespace, $strLocaleCode, $arrstrTranslationKeys, $boolAcceptMachineTranslated, $objDatabase ) {

		array_walk( $arrstrTranslationKeys, function( &$strValue ) use ( $objDatabase ) {
			$strValue = pg_escape_literal( $objDatabase->getHandle(), trim( $strValue ) );
		} );

		$strSelectValue = $boolAcceptMachineTranslated
			? sprintf( '
					CASE 
                        WHEN t.details#>>%s IS NOT NULL AND util.util_to_bytea(t.value)::text !~* E\'^\\\\Bx(c2a0)+$\' THEN t.value 
                        WHEN talias.value IS NOT NULL AND util.util_to_bytea(talias.value)::text !~* E\'^\\\\Bx(c2a0)+$\' THEN talias.value
                        WHEN tfuzzy.value IS NOT NULL AND util.util_to_bytea(tfuzzy.value)::text !~* E\'^\\\\Bx(c2a0)+$\' THEN tfuzzy.value 
                        WHEN t.details#>>%s IS NOT NULL THEN t.details#>>%s
                        ELSE tk.key
                    END',
				pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,received_on}' ),
				pg_escape_literal( $objDatabase->getHandle(), '{machine_translation_vendor,received_on}' ),
				pg_escape_literal( $objDatabase->getHandle(), '{machine_translation_vendor,value}' ) )
			: sprintf( '
					CASE 
                        WHEN t.details#>>%s IS NOT NULL AND util.util_to_bytea(t.value)::text !~* E\'^\\\\Bx(c2a0)+$\' THEN t.value 
                        WHEN talias.value IS NOT NULL AND util.util_to_bytea(talias.value)::text !~* E\'^\\\\Bx(c2a0)+$\' THEN talias.value
                        WHEN tfuzzy.value IS NOT NULL AND util.util_to_bytea(tfuzzy.value)::text !~* E\'^\\\\Bx(c2a0)+$\' THEN tfuzzy.value 
                        ELSE tk.key
                    END',
				pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,received_on}' ) );

		$strSql = sprintf( '
				SELECT
					tk.id,
					tk.key,
					%s AS value
				FROM
					translation_keys AS tk
					JOIN locales AS l ON l.locale_code = %s
					LEFT JOIN translations AS t ON t.translation_key_id = tk.id AND t.locale_code = %s
					LEFT JOIN translations AS talias ON talias.translation_key_id = tk.id AND talias.locale_code = l.details->>%s
					LEFT JOIN LATERAL (
						SELECT
							t1.value,
							t1.translation_key_id
						FROM
							translations AS t1
						WHERE
							t1.translation_key_id = tk.id
							AND t1.locale_code <> %s
							AND t1.approved_on IS NOT NULL
							AND t.approved_on IS NULL
							AND t1.locale_code ILIKE %s
						LIMIT 1
					) AS tfuzzy ON tfuzzy.translation_key_id = tk.id
				WHERE
					tk.application_name = %s
					AND tk.namespace = %s
					AND tk.key IN ( %s )
					AND tk.deleted_by IS NULL
					AND tk.deleted_on IS NULL',
			$strSelectValue,
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_alias' ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
			pg_escape_literal( $objDatabase->getHandle(), CLocaleContainer::createService()->normalizeLocale( $strLocaleCode, [ 'format_lang_only' => true, 'all_lowercase' => true ] ) . '_%' ),
			pg_escape_literal( $objDatabase->getHandle(), $strApplicationName ),
			pg_escape_literal( $objDatabase->getHandle(), $strNamespace ),
			implode( ',', $arrstrTranslationKeys ) );

		$arrmixData = fetchData( $strSql, $objDatabase ) ?? [];

		$arrmixTempData = [];
		$boolIsI18nFeedbackModeOn = checkIsI18nFeedbackModeOn();

		foreach( $arrmixData as $mixData ) {
			$arrstrValues[$mixData['id']] = $mixData['value'];
			if( true == $boolIsI18nFeedbackModeOn ) {
				$arrmixTempData['"' . $mixData['id'] . '"'] = strip_tags( json_encode( $mixData ) );
			}
		}
		if( true == $boolIsI18nFeedbackModeOn ) {
			$objContainer = \Psi\Libraries\Container\CDependencyContainer::getInstance()->getContainer();
			$objContainer['translation_feedback_data'] = array_merge( $objContainer['translation_feedback_data'] ?? [], $arrmixTempData );
		}
		return $arrstrValues ?? [];
	}

	public static function fetchAllTranslationValues( $strApplicationName, $strNamespace, $strLocaleCode, $boolAcceptMachineTranslated, $objDatabase ) {

		$strSelectValue = $boolAcceptMachineTranslated
			? sprintf( '
					CASE 
                        WHEN t.details#>>%s IS NOT NULL AND util.util_to_bytea(t.value)::text !~* E\'^\\\\Bx(c2a0)+$\' THEN t.value 
                        WHEN t.details#>>%s IS NOT NULL THEN t.details#>>%s
                        ELSE tk.key
                    END',
				pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,received_on}' ),
				pg_escape_literal( $objDatabase->getHandle(), '{machine_translation_vendor,received_on}' ),
				pg_escape_literal( $objDatabase->getHandle(), '{machine_translation_vendor,value}' ) )
			: 't.value';

		$strSql = sprintf( '
				SELECT
					tk.id,
					%s AS value
				FROM
					translation_keys AS tk
					JOIN translations AS t ON t.translation_key_id = tk.id
				WHERE
					tk.application_name = %s
					AND tk.namespace = %s
					AND t.locale_code = %s
					AND tk.deleted_by IS NULL
					AND tk.deleted_on IS NULL',
			$strSelectValue,
			pg_escape_literal( $objDatabase->getHandle(), $strApplicationName ),
			pg_escape_literal( $objDatabase->getHandle(), $strNamespace ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ) );

		$arrmixData = fetchData( $strSql, $objDatabase ) ?? [];

		foreach( $arrmixData as $mixData ) {
			$arrstrValues[$mixData['id']] = $mixData['value'];
		}

		return $arrstrValues ?? [];
	}

	public static function fetchTranslationByLocaleCodeByTranslationKeyId( $strLocaleCode, $intTranslationKeyId, $objDatabase ) {

		$strSql = sprintf( '
				SELECT
					t.*
				FROM
					translations AS t
				WHERE
					t.locale_code = %s
					AND t.translation_key_id = %d',
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
			pg_escape_string( $objDatabase->getHandle(), ( int ) $intTranslationKeyId ) );

		return parent::fetchTranslation( $strSql, $objDatabase );
	}

	public static function fetchUnbatchedTranslationsByLocaleCode( $strLocaleCode, $objDatabase ) {

		$strSql = sprintf( '
				SELECT
					tdst.*,
					tk.key AS translation_key,
					tsrc.translation_key_id,
					%s AS locale_code,
					tsrc.value as default_value,
					COALESCE( tdst.value, tsrc.value ) as value
				FROM
					translation_keys AS tk
					JOIN translations AS tsrc ON
						tk.id = tsrc.translation_key_id
						AND tsrc.locale_code = %s
					LEFT JOIN translations AS tdst ON
						tk.id = tdst.translation_key_id
						AND tdst.locale_code = %s
				WHERE
					tk.is_system
					AND ( tk.details IS NULL OR tk.details ? %s = FALSE )
					AND tsrc.approved_on IS NOT NULL
					AND ( tdst.id IS NULL OR tdst.details IS NULL OR tdst.details ? %s = FALSE )
					AND tk.deleted_by IS NULL
					AND tk.deleted_on IS NULL',
				pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
				pg_escape_literal( $objDatabase->getHandle(), CLocale::ENGLISH_UNITED_STATES ),
				pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
				pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ),
				pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ) );

		return parent::fetchTranslations( $strSql, $objDatabase );
	}

	public static function fetchUnbatchedMachineTranslationsByLocaleCode( $strLocaleCode, $objDatabase ) {

		$strSql = sprintf( '
				SELECT
					tdst.*,
					tk.key AS translation_key,
					tsrc.translation_key_id,
					%s AS locale_code,
					tsrc.value as default_value,
					COALESCE( tdst.value, tsrc.value ) as value
				FROM
					translation_keys AS tk
					JOIN translations AS tsrc ON
						tk.id = tsrc.translation_key_id
						AND tsrc.locale_code = %s
					LEFT JOIN translations AS tdst ON
						tk.id = tdst.translation_key_id
						AND tdst.locale_code = %s
				WHERE
					tk.is_system
					AND ( tk.details IS NULL OR tk.details ? %s = FALSE )
					AND tsrc.approved_on IS NOT NULL
					AND ( tdst.id IS NULL OR tdst.details IS NULL OR tdst.details ? %s = FALSE )
					AND tk.deleted_by IS NULL
					AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
			pg_escape_literal( $objDatabase->getHandle(), CLocale::ENGLISH_UNITED_STATES ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
			pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ),
			pg_escape_literal( $objDatabase->getHandle(), 'machine_translation_vendor' ) );

		return parent::fetchTranslations( $strSql, $objDatabase );
	}

	public static function fetchPendingTranslationBatchesByVendor( $strVendorName, $objDatabase ) {

		$strSql = sprintf( '
			SELECT DISTINCT 
				t.details#>>%s AS vendor_batch_id,
			    t.details#>>%s AS interchange_format,
				t.locale_code
			FROM
				translations AS t
			    JOIN translation_keys AS tk ON t.translation_key_id = tk.id
			WHERE
				( tk.details IS NULL OR tk.details ? %s = FALSE )
				AND t.approved_on IS NULL
				AND t.details#>>%s = %s
				AND t.details->%s ? %s
				AND t.locale_code != %s
				AND tk.deleted_by IS NULL
				AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_batch_id}' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,interchange_format}' ),
			pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ),
			pg_escape_literal( $objDatabase->getHandle(), 'vendor_batch_id' ),
			pg_escape_literal( $objDatabase->getHandle(), CLocale::ENGLISH_UNITED_STATES ) );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPendingTranslationsByVendor( $strVendorName, $objDatabase ) {

		$strSql = sprintf( '
			SELECT
				tk.key AS translation_key,
				t.details#>>%s AS vendor_batch_id,
				t.*
			FROM
				translations AS t
				JOIN translation_keys AS tk
					ON t.translation_key_id = tk.id
			WHERE
				t.approved_on IS NULL
				AND t.details#>>%s = %s
				AND t.details->%s ? %s
				AND t.locale_code != %s
				AND ( tk.details IS NULL OR tk.details ? %s = FALSE )
				AND tk.deleted_by IS NULL
				AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_batch_id}' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ),
			pg_escape_literal( $objDatabase->getHandle(), 'vendor_batch_id' ),
			pg_escape_literal( $objDatabase->getHandle(), CLocale::ENGLISH_UNITED_STATES ),
			pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ) );

		return parent::fetchTranslations( $strSql, $objDatabase );
	}

	public static function fetchDatabaseDependentNotDeployedDatabaseTranslations( $objDatabase, $arrstrLocaleCodes = [], $strFullyDeployedFlagName = 'fully_completed' ) {

		$strSql = "
				SELECT
					t.id,
					t.translation_key_id,
					tk.key as translation_key,
					t.locale_code,
					CASE WHEN t.details#>>'{translation_vendor,received_on}' IS NOT NULL AND util.util_to_bytea(t.value)::text !~* E'^\\\\Bx(c2a0)+$' THEN t.value ELSE '' END as value,
					t.details#>>'{machine_translation_vendor,value}' AS machine_value,
					t.details || (tk.details - 'dependent_files') AS details
				FROM
					translations AS t
					JOIN translation_keys AS tk ON 
						t.translation_key_id = tk.id
				WHERE
					CASE 
						WHEN t.details#>>'{translation_vendor,received_on}' IS NOT NULL THEN TRUE
						WHEN t.details#>>'{machine_translation_vendor,received_on}' IS NOT NULL THEN TRUE
						ELSE FALSE
					END					
					AND NOT COALESCE(t.details#>>'{database_deployments," . $strFullyDeployedFlagName . "}', 'f')::BOOL 
					AND tk.details ? 'dependent_databases'
					AND tk.deleted_by IS NULL
					AND tk.deleted_on IS NULL";

		if( true == valArr( $arrstrLocaleCodes ) ) {
			$strSql .= ' AND t.locale_code IN ( \'' . implode( "','", $arrstrLocaleCodes ) . '\' )';
		}

		$strSql .= ' ORDER BY t.updated_on LIMIT 10000';

		return parent::fetchTranslations( $strSql, $objDatabase );
	}

	public static function fetchAllTranslationByLocaleCodeByTranslationKeyId( $strLocaleCode, $intTranslationKeyId, $objDatabase ) {

		$strSql = 'SELECT
	                    tk.key AS default_value,
	                    t.*
					FROM
					    translation_keys AS tk
					    JOIN translations AS t ON t.translation_key_id = tk.id
					WHERE
					    t.translation_key_id = ' . ( int ) $intTranslationKeyId . '
					    AND t.locale_code = \'' . $strLocaleCode . '\'';

		return parent::fetchTranslation( $strSql, $objDatabase );
	}

	public static function fetchTranslatedLocaleCodesByTranslationKeyId( $intTranslationKeyId, $objDatabase ) {

		$strSql = 'SELECT
	                    t.locale_code
					FROM
					    translations t
					WHERE
					    t.translation_key_id = ' . ( int ) $intTranslationKeyId . '
					    AND t.locale_code != \'' . CLocale::ENGLISH_UNITED_STATES . '\'';

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * @param $arrintTranslationKeyIds
	 * @param $strLocaleCode
	 * @param $objDatabase
	 * @return \CTranslation[]
	 */
	public static function fetchTranslationsByTranslationKeyIdsByLocaleCode( $arrintTranslationKeyIds, $strLocaleCode, $objDatabase ) {

		$strSql = 'SELECT 
						t.*
					FROM
						translations t
					WHERE
						t.translation_key_id IN (' . implode( ', ', $arrintTranslationKeyIds ) . ')
						AND t.locale_code = \'' . $strLocaleCode . '\'';

		return parent::fetchTranslations( $strSql, $objDatabase );
	}

	public static function fetchUploadedTranslationStatsByVendorByLocaleCode( $strVendorName, $strLocaleCode, $objDatabase ) {
		$strSql = sprintf( '
						SELECT 
							(t.details#>>%s)::date as sent_on,
							count(DISTINCT t.details#>>%s) as total_documents,
							array_to_string( array_agg( tk.key ), \'--~~--\' ) as words
						FROM translations t
							JOIN translation_keys tk ON tk.id = t.translation_key_id
						WHERE
							t.details#>>%s = %s
							AND t.locale_code IN (%s)
							AND (t.details#>>%s)::date > NOW() - INTERVAL %s
							AND tk.deleted_by IS NULL
							AND tk.deleted_on IS NULL
						GROUP BY
							sent_on
						ORDER BY 
							sent_on',
						pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,sent_on}' ),
						pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,batch_title}' ),
						pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
						pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
						pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
						pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,sent_on}' ),
						pg_escape_literal( $objDatabase->getHandle(), '60 days' ) );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnApprovedTranslationsByVendorBatchIdByLocaleCode( $strVendorBatchId, $strLocaleCode, $objDatabase, $strVendorName = CLocale::TRANSLATION_VENDOR_LINGOTEK ) {

		$strSql = sprintf( '
			SELECT
				tk.key AS translation_key,
				t.*
			FROM
				translations AS t
				JOIN translation_keys AS tk
					ON t.translation_key_id = tk.id
			WHERE
				t.approved_on IS NULL
				AND t.details#>>%s = %s
				AND t.details#>>%s = %s
				AND t.locale_code = %s
				AND tk.deleted_by IS NULL
				AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_batch_id}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorBatchId ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ) );

		return parent::fetchTranslations( $strSql, $objDatabase );
	}

	public static function fetchCompletedTranslationStatsByVendorByLocaleCode( $strVendorName, $strLocaleCode, $objDatabase ) {
		$strSql = sprintf( '
						SELECT 
							(t.details#>>%s)::date as received_on,
							count(DISTINCT t.details#>>%s) as total_documents,
							array_to_string( array_agg( tk.key ), \'--~~--\' ) as words
						FROM translations t
							JOIN translation_keys tk ON tk.id = t.translation_key_id
						WHERE
							t.details#>>%s = %s
							AND t.locale_code IN (%s)
							AND (t.details#>>%s)::date > NOW() - INTERVAL %s
							AND tk.deleted_by IS NULL
							AND tk.deleted_on IS NULL
						GROUP BY
							received_on
						ORDER BY 
							received_on',
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,received_on}' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,batch_title}' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,received_on}' ),
			pg_escape_literal( $objDatabase->getHandle(), '60 days' ) );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTranslationKeysByValue( $strValue, $strLocaleCode, $objDatabase ) {

		$strSql = sprintf( '
					SELECT
						tk.id,
						tk.key,
						t.value
					FROM
						translation_keys AS tk
						JOIN translations AS t ON t.translation_key_id = tk.id
					WHERE
						( lower( t.value ) = %1$s OR lower( tk.key ) = %1$s )
						AND t.locale_code = %2$s
						AND tk.deleted_by IS NULL
						AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), $strValue ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ) );

		$arrmixData = fetchData( $strSql, $objDatabase ) ?? [];
		return $arrmixData ?? [];
	}

	public static function fetchPendingFeedbackTranslationsByVendor( $strVendorName, $objDatabase ) {
		$strSql = sprintf( '
			SELECT
				DISTINCT tn.details#>>%s AS vendor_batch_id,
				tk.key AS translation_key,
				t.*
		    FROM
		        translation_notes tn
		        JOIN translations t ON tn.translation_key_id = t.translation_key_id AND tn.locale_code = t.locale_code
		        JOIN translation_keys tk ON t.translation_key_id = tk.id
		    WHERE
		        ( tk.details IS NULL OR tk.details ? %s = FALSE )
		        AND tn.details #>> %s = %s
		        AND tn.details -> %s ? %s
		        AND tn.details -> %s ? %s IS FALSE
		        AND tk.deleted_by IS NULL
				AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_batch_id}' ),
			pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ),
			pg_escape_literal( $objDatabase->getHandle(), 'vendor_batch_id' ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ),
			pg_escape_literal( $objDatabase->getHandle(), 'received_on' ) );

		return self::fetchTranslations( $strSql, $objDatabase );
	}

	public static function fetchUnbatchedDocumentsByLocaleCode( $strLocaleCode, $objDatabase ) {

		$strSql = sprintf( '
				SELECT
					tdst.*,
					tk.key AS translation_key,
					tk.details AS translation_key_details,
					tsrc.translation_key_id,
					%s AS locale_code,
					tsrc.value as default_value,
					COALESCE( tdst.value, tsrc.value ) as value
				FROM
					translation_keys AS tk
					JOIN translations AS tsrc ON
						tk.id = tsrc.translation_key_id
						AND tsrc.locale_code = %s
					LEFT JOIN translations AS tdst ON
						tk.id = tdst.translation_key_id
						AND tdst.locale_code = %s
				WHERE
					tk.is_system
					AND tk.details ? %s = TRUE
					AND ( tk.details ? %s = TRUE AND ( tk.details -> %s )::jsonb ? %s )
					AND tsrc.approved_on IS NOT NULL
					AND ( tdst.id IS NULL OR tdst.details IS NULL OR tdst.details ? %s = FALSE )
					AND tk.deleted_by IS NULL
					AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
			pg_escape_literal( $objDatabase->getHandle(), CLocale::ENGLISH_UNITED_STATES ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
			pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ),
			pg_escape_literal( $objDatabase->getHandle(), 'request_locale_codes' ),
			pg_escape_literal( $objDatabase->getHandle(), 'request_locale_codes' ),
			pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ) );

		return parent::fetchTranslations( $strSql, $objDatabase );
	}

	public static function fetchPendingDocumentTranslationBatchesByVendor( $strVendorName, $objDatabase ) {

		$strSql = sprintf( '
			SELECT DISTINCT 
				t.details#>>%s AS vendor_batch_id,
			    t.details#>>%s AS interchange_format,
				t.locale_code
			FROM
				translations AS t
                JOIN translation_keys AS tk ON t.translation_key_id = tk.id
			WHERE
				tk.details ? %s = TRUE
				AND t.approved_on IS NULL
				AND t.details#>>%s = %s
				AND t.details->%s ? %s
				AND t.locale_code != %s
				AND tk.deleted_by IS NULL
                AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_batch_id}' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,interchange_format}' ),
			pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ),
			pg_escape_literal( $objDatabase->getHandle(), 'vendor_batch_id' ),
			pg_escape_literal( $objDatabase->getHandle(), CLocale::ENGLISH_UNITED_STATES ) );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPendingDocumentTranslationsByVendor( $strVendorName, $objDatabase ) {

		$strSql = sprintf( '
			SELECT
				tk.key AS translation_key,
				tk.details AS translation_key_details,
				t.details#>>%s AS vendor_batch_id,
				t.*
			FROM
				translations AS t
				JOIN translation_keys AS tk
					ON t.translation_key_id = tk.id
			WHERE
				t.approved_on IS NULL
				AND t.details#>>%s = %s
				AND t.details->%s ? %s
				AND t.locale_code != %s
				AND ( tk.details IS NOT NULL
                    AND tk.details ? %s = TRUE )
				AND tk.deleted_by IS NULL
				AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_batch_id}' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
			pg_escape_literal( $objDatabase->getHandle(), 'translation_vendor' ),
			pg_escape_literal( $objDatabase->getHandle(), 'vendor_batch_id' ),
			pg_escape_literal( $objDatabase->getHandle(), CLocale::ENGLISH_UNITED_STATES ),
			pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ) );

		return parent::fetchTranslations( $strSql, $objDatabase );
	}

	public static function fetchUndeployedDocumentTranslationBatchesByVendor( $strVendorName, $objDatabase ) {

		$strSql = sprintf( '
			SELECT
			    tk.key AS translation_key,
			    tk.details -> %s AS translation_key_details,
			    t.details #>> %s AS vendor_batch_id,
			    t.*
			FROM
			    translations AS t
			    JOIN translation_keys AS tk ON t.translation_key_id = tk.id
			WHERE
			    t.approved_on IS NOT NULL
			    AND t.details #>> %s = %s
			    AND t.locale_code != %s
			    AND ( t.details ? %s = TRUE
			    AND t.details #>> %s IS NULL )
			    AND ( tk.details IS NOT NULL
			    AND tk.details ? %s = TRUE )
			    AND tk.deleted_by IS NULL
			    AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_batch_id}' ),
			pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,vendor_name}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strVendorName ),
			pg_escape_literal( $objDatabase->getHandle(), CLocale::ENGLISH_UNITED_STATES ),
			pg_escape_literal( $objDatabase->getHandle(), 'object_result' ),
			pg_escape_literal( $objDatabase->getHandle(), '{object_result, message_deployed_on}' ),
			pg_escape_literal( $objDatabase->getHandle(), 'dependent_objects' ) );

		return parent::fetchTranslations( $strSql, $objDatabase );
	}

}
?>
