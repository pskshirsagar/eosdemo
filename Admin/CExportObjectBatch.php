<?php

class CExportObjectBatch extends CBaseExportObjectBatch {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportObjectBatchStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportPartnerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExecutedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>