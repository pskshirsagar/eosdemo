<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePreferences
 * Do not add any new functions to this class.
 */

class CEmployeePreferences extends CBaseEmployeePreferences {

	public static function fetchEmployeePreferenceByEmployeeIdByKey( $intEmployeeId, $strKey, $objDatabase ) {
		$strSql = 'SELECT * FROM employee_preferences WHERE employee_id = ' . ( int ) $intEmployeeId . ' AND key = \'' . $strKey . '\'';
		return self::fetchEmployeePreference( $strSql, $objDatabase );
	}

	public static function fetchEmployeePreferenceByEmployeeIdByKeys( $intEmployeeId, $arrstrKeys, $objDatabase ) {

		$strSql = 'SELECT
					*
					FROM
						employee_preferences
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
					AND
						key IN ( \'' . implode( '\', \'', $arrstrKeys ) . '\' )';
		return self::fetchEmployeePreferences( $strSql, $objDatabase );
	}

	public static function fetchEmployeePreferenceByEmployeeIdsByKey( $arrintEmployeeIds, $strKey, $intLoginUserId, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;
		$strCondition = ( NULL != $intLoginUserId ) ? ' AND created_by = ' . ( int ) $intLoginUserId : '';

		$strSql = 'SELECT
					*
					FROM
						employee_preferences
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
					AND
						key = \'' . $strKey . '\'' . $strCondition;

		return self::fetchEmployeePreferences( $strSql, $objDatabase );
	}

	public static function fetchEmployeePreferenceByValuesByKeyByLoginUserId( $arrintEmployeeIds, $strKey, $intLoginUserId, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						ep.id, ep.employee_id,
						ep.key, ep.value, ep.refresh_token,
						e.email_address,
						e1.preferred_name
					FROM
						employee_preferences ep
						JOIN employees e ON e.id = ep.employee_id
						JOIN users u ON u.id = ep.created_by
						JOIN employees e1 ON u.employee_id = e1.id
					WHERE
						key = \'' . $strKey . '\'' . '
						AND CAST( ep.value as INTEGER ) IN ( ' . implode( ',', $arrintEmployeeIds ) . ')
						AND ep.created_by = ' . ( int ) $intLoginUserId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllEnableEmployeePreferencesByKeyByValues( $strKey, $arrintValue, $objDatabase ) {

		if( false == valArr( $arrintValue ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						employee_preferences
					WHERE
						key = \'' . $strKey . '\'
						AND refresh_token IS NULL
						AND CAST( value as INTEGER ) IN ( ' . implode( ',', $arrintValue ) . ' )';

		return self::fetchEmployeePreferences( $strSql, $objDatabase );
	}

	public static function fetchAllEnableEmployeePreferencesWithEmailByKeyByValue( $strKey, $arrintValue, $objDatabase, $boolIsRefreshToken = true, $boolRecommendation ) {

		if( true == $boolRecommendation && false == valArr( $arrintValue ) ) return NULL;

		$strSubSql = ( true == $boolIsRefreshToken ) ? ' AND refresh_token IS NOT NULL' : '';

		$strCondition = '';
		$strCondition = ( true == $boolRecommendation ) ? 'AND CAST( ep.value as INTEGER ) IN ( ' . implode( ',', $arrintValue ) . ' )' : 'AND value IN ( ' . $arrintValue . ' )';

		$strSql = 'SELECT
						ep.id, ep.employee_id,
						ep.key, ep.value, ep.refresh_token,
						e.email_address,
						e1.preferred_name
					FROM
						employee_preferences ep
						JOIN employees e ON e.id = ep.employee_id
						JOIN users u ON u.id = ep.created_by
						JOIN employees e1 ON u.employee_id = e1.id
					WHERE
						key = \'' . $strKey . '\'' . $strCondition . $strSubSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeePreferencesByKey( $strKey, $objDatabase ) {

		return self::fetchEmployeePreferences( 'SELECT * FROM employee_preferences WHERE key=\'' . $strKey . '\'', $objDatabase );
	}

	public static function fetchEmployeePreferenceValueByEmployeeIdByKey( $intEmployeeId, $strKey, $objDatabase ) {

		$strSql = 'SELECT
						ep.value
					FROM
						employee_preferences ep
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
					AND
						key = \'' . $strKey . '\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeePreferenceData( $objDatabase ) {

		$strSql = 'SELECT
						em.id as employee_id,
						us.id as user_id,
						em.email_address as email_address,
						us.svn_username as svn_username,
						em.name_full,
						emp.value as zendesk_user_id
					FROM
						users us
						JOIN employees em ON (us.employee_id = em.id )
						LEFT JOIN employee_preferences emp ON ( em.id = emp.employee_id )
					WHERE
						emp.key = \'ZENDESK_USER_ID\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUserIdByZendeskUserId( $intZendeskUserId, $objDatabase ) {

		$strSql = 'SELECT
						u.id
					FROM
						users u
						JOIN employee_preferences ep ON ( u.employee_id = ep.employee_id )
					WHERE
						ep.value = \'' . ( int ) $intZendeskUserId . '\'';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['id'] ) ) return $arrintResponse[0]['id'];
	}

	public static function fetchEmployeePreferencesEmployeeIdByKeyByUserIds( $strKey, $arrintUserIds, $boolVar, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) ) return NULL;

		$strCondition = ( true == $boolVar ) ? " AND ep.refresh_token = 'true'" : '';

		$strSql = 'SELECT
				ep.employee_id,
				ep.value,
				e.preferred_name,
				ep.created_by,
				ep.refresh_token,
				e1.preferred_name as recommended_by
		FROM
				employee_preferences ep
				JOIN employees e ON e.id = ep.employee_id
				JOIN users u ON u.id = ep.created_by
				JOIN employees e1 ON e1.id = u.employee_id
		WHERE
			ep.created_by IN ( ' . implode( ',', $arrintUserIds ) . ' )
		    AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
		    AND ep.key = \'' . $strKey . '\'' . $strCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeePreferencesByEmployeeIdByKeys( $intEmployeeId, $objDatabase, $boolSignPolicy = false, $boolFetchObject = false ) {

		$arrstrEmployeePreferences	= [ CEmployeePreference::ASSET_ID_FOR_VPN_POLICY_SIGNED, CEmployeePreference::ASSET_ID_FOR_HARDWARE_ALLOCATION_FORM_SIGNED ];
		$strCondition				= '';
		if( true == $boolSignPolicy ) {
			$strDate 					= '2020-03-15';
			$arrstrEmployeePreferences	= CEmployeePreference::$c_arrstrEmployeeAssetPreferences;
			$strCondition 				= ' AND created_on::DATE >= \'' . $strDate . ' \'::DATE';

		}

			$strSql = 'SELECT
						*
						FROM
							employee_preferences
						WHERE
							employee_id = ' . ( int ) $intEmployeeId . '
						AND
							key IN ( \'' . implode( '\', \'', $arrstrEmployeePreferences ) . '\' )
							' . $strCondition;

		if( true == $boolFetchObject ) {
			return self::fetchEmployeePreferences( $strSql, $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchEmployeePreferencesByEmployeeIdsByKeys( $arrintEmployeeIds, $arrstrKeys, $objDatabase ) {

		if( false == ( $arrintEmployeeIds = getIntValuesFromArr( $arrintEmployeeIds ) ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						employee_preferences
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND key IN ( \'' . implode( '\', \'', $arrstrKeys ) . '\' ) ';

		return self::fetchEmployeePreferences( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeePreferencesByKeys( $arrstrKeys, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						employee_id,
						key,
						value
					FROM
						employee_preferences
					WHERE
						key IN ( \'' . implode( '\', \'', $arrstrKeys ) . '\' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeePreferencesByEmployeeIdsByKeyByValue( $arrintEmployeeIds, $arrstrKeys, $objDatabase ) {

		if( false == ( $arrintEmployeeIds = getIntValuesFromArr( $arrintEmployeeIds ) ) || false == valArr( $arrstrKeys ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						employee_preferences
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND key IN ( \'' . implode( '\', \'', $arrstrKeys ) . '\' )
						AND value::INTEGER = 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>