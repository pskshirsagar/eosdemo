<?php

class CTransportVehicle extends CBaseTransportVehicle {

	public function valVehicleNumber( $objDatabase ) {
		$boolIsValid = true;
		if( false == valStr( $this->m_strVehicleNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_number', 'Vehicle Number is required.' ) );
		} else {
			$strVehicleNumber = $this->m_strVehicleNumber;

			if( true == preg_match( '/[^a-zA-Z0-9\s]+/', $strVehicleNumber ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_number', ' Vehicle Number must be alphanumeric.' ) );
			} else {
				$strWhere = 'WHERE vehicle_number = \'' . pg_escape_string( $strVehicleNumber ) . '\' and id <> ' . $this->m_intId;
				$intCount = \Psi\Eos\Admin\CTransportVehicles::createService()->fetchRowCount( $strWhere, 'transport_vehicles', $objDatabase );
				if( 0 < $intCount ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vehicle_number', ' Vehicle Number already exists.' ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function valDriverName() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strDriverName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'driver_name', ' Driver Name is required.' ) );
		} elseif( true == preg_match( '/[^a-zA-Z0-9\s]+/', $this->m_strDriverName ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'driver_name', ' Driver Name must be alphanumeric.' ) );
		}

		return $boolIsValid;
	}

	public function valDriverContactNumber( $objDatabase ) {
		$boolIsValid = true;
		if( false == valStr( $this->m_strDriverContactNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'driver_contact_number', 'Driver Contact Number is required.' ) );
		} elseif( false == ctype_digit( $this->m_strDriverContactNumber ) || 10 !== mb_strlen( $this->m_strDriverContactNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'driver_contact_number', 'Please enter valid Driver Contact Number.' ) );
		} else {
			$strWhere = 'WHERE driver_contact_number = \'' . $this->m_strDriverContactNumber . '\' and id <> ' . $this->m_intId;
			$intCount = \Psi\Eos\Admin\CTransportVehicles::createService()->fetchRowCount( $strWhere, 'transport_vehicles', $objDatabase );
			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'driver_contact_number', ' Driver Contact Number already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsPublished( $objDatabase ) {
		$boolIsValid = true;
		if( false == $this->m_boolIsPublished ) {
			$strWhere = 'WHERE transport_vehicle_id = ' . $this->m_intId . ' AND deleted_by IS NULL AND deleted_on IS NULL';
			$intCount = \Psi\Eos\Admin\CTransportRoutes::createService()->fetchRowCount( $strWhere, 'transport_routes', $objDatabase );
			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', 'Cannot unpublish vehicle as it is associated with an active route.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valVehicleNumber( $objAdminDatabase );
				$boolIsValid &= $this->valDriverName();
				$boolIsValid &= $this->valDriverContactNumber( $objAdminDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>