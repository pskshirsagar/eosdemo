<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CFtpVendors
 * Do not add any new functions to this class.
 */

class CFtpVendors extends CBaseFtpVendors {

	public static function fetchFtpVendors( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CFtpVendor', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchFtpVendor( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CFtpVendor', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchCachedAllFtpVendors( $objAdminDatabase ) {
		$strSql = 'SELECT * FROM ftp_vendors fv WHERE fv.is_published = 1 ORDER BY order_num';
		return self::fetchFtpVendors( $strSql, $objAdminDatabase );
	}
}
?>