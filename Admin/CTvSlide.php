<?php

class CTvSlide extends CBaseTvSlide {

	protected $m_strFilePath;

	const TEMPLATE_TEXT										= 1;
	const TEMPLATE_IMAGE									= 2;
	const TEMPLATE_TEXT_AND_IMAGE							= 3;
	const TEMPLATE_QUARTER									= 4;
	const TEMPLATE_IMPLEMENTATION_IMAGE						= 5;

	const NPS_TOTALS_FOR_MONTH_TO_DATE						= 1;
	const NPS_TOTALS_FOR_LAST_FIFTEEN_DAYS					= 2;
	const NPS_TOTALS_FOR_THIS_MONTH							= 3;
	const NPS_TOTALS_FOR_YESTERDAY							= 4;
	const ABANDONED_CALLS_RATE_GOAL							= 5;
	const OPEN_SUPPORT_TASKS_GOAL							= 6;
	const NPS_TEN_HIGHLIGHT									= 7;
	const ACTIONABLE_SUPPORT_TASKS_GOAL						= 8;
	const NPS_TOP_TEN										= 9;
	const NPS_TEN_LAST_COMMENT								= 10;
	const TOTAL_OPEN_TASKS									= 11;
	const OPEN_TASKS_ONE_MONTH_AGO_VERSUS_TODAY				= 12;
	const OPEN_TASKS_ONE_WEEK_AGO_VERSUS_TODAY				= 13;
	const COMPLETED_TASKS_GOAL								= 14;
	const PHONE_ABANDON_MONTH_TO_DATE						= 15;
	const PHONE_ABANDON_ROLLING_THIRTY_DAYS					= 16;
	const PHONE_ABANDON_YESTERDAY							= 17;
	const PHONE_ABANDON_WEEKEND								= 18;
	const PHONE_TOP_THREE_AGENT								= 19;
	const TOP_AGENT_FEWEST_ACTIONABLE_TICKETS				= 20;
	const TOP_AGENT_MOST_CLIENT_FACING_TASK_UPDATES			= 21;
	const TOP_AGENT_MOST_COMPLETED_TASKS_IN_THIS_LAST_WEEK	= 22;
	const TOP_AGENT_MOST_COMPLETED_TASKS_IN_THIS_LAST_MONTH = 23;
	const TASKS_IN_QUEUE									= 24;
	const UNASSIGNED_URGENT_IMMEDIATE_TASKS					= 25;

	public static $c_arrintTemplateIds = [
		self::TEMPLATE_QUARTER,
		self::TEMPLATE_IMPLEMENTATION_IMAGE
	];

	/**
	 * Get Functions
	 *
	 */

	public function getFilePath() {

		$this->m_strFilePath = PATH_MOUNTS_TV_SLIDES . '/';

		if( false == is_dir( $this->m_strFilePath ) && false == CFileIo::recursiveMakeDir( $this->m_strFilePath ) ) {
			return false;
		}

		return $this->m_strFilePath;
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valId() {
		return true;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == empty( $this->m_strTitle ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required.' ) );
		}
		return $boolIsValid;
	}

	public function valTemplateTypeId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intTemplateTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'template_type_id', 'Template is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCustomText() {
		$boolIsValid = true;
		$strCustomText = str_replace( '&nbsp;', '', $this->m_strCustomText );
		$this->m_strCustomText = preg_replace( '/[\n]+/', '', $this->m_strCustomText );

		if( self::TEMPLATE_IMAGE != $this->m_intTemplateTypeId && ( true == empty( $strCustomText ) || false == valStr( $strCustomText ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_text', 'Custom text is required.' ) );

		} elseif( ( self::TEMPLATE_TEXT == $this->m_intTemplateTypeId && 350 < strlen( strip_tags( $this->m_strCustomText ) ) ) || ( self::TEMPLATE_TEXT_AND_IMAGE == $this->m_intTemplateTypeId && 250 < strlen( strip_tags( $this->m_strCustomText ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_text', 'Custom text message is too large to display.' ) );
		}
		return $boolIsValid;
	}

	public function valCustomImageName() {
		$boolIsValid = true;

		if( self::TEMPLATE_TEXT != $this->m_intTemplateTypeId && true == empty( $this->m_strCustomImageName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_image', 'Custom image is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCustomImageSize() {
		$boolIsValid = true;

		if( self::TEMPLATE_TEXT != $this->m_intTemplateTypeId && true == empty( $this->m_strCustomImageName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_image', 'Slide image is required.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valStartDateTime() {
		$boolIsValid = true;

		if( true == empty( $this->m_strStartDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_datetime', 'Start datetime is required.' ) );
		}
		return $boolIsValid;
	}

	public function valGoalDateTime() {
		$boolIsValid = true;

		if( true == empty( $this->m_strStartDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'goal_datetime', 'Goal datetime is required.' ) );
		}
		return $boolIsValid;
	}

	public function valEndDateTime() {
		$boolIsValid = true;

		if( true == empty( $this->m_strEndDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', 'End datetime is required.' ) );
		}
		return $boolIsValid;
	}

	public function valTargetGoal() {
		$boolIsValid = true;

		if( true == empty( $this->m_fltTargetGoal ) || ( false == is_numeric( $this->m_fltTargetGoal ) && self::COMPLETED_TASKS_GOAL != $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'target_goal', 'Target goal is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $boolStartEndDaterequired = false, $boolGoalDaterequired = false, $boolShowImageUploadSlides = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();
				if( true == $this->getIsCustom() ) {
					$boolIsValid &= $this->valTemplateTypeId();
					$boolIsValid &= $this->valCustomText();
					$boolIsValid &= $this->valCustomImageName();
				}

				if( true == $boolShowImageUploadSlides ) {
					$boolIsValid &= $this->valCustomImageSize();
				}

				if( true == $boolStartEndDaterequired ) {
					$boolIsValid &= $this->valStartDateTime();
					$boolIsValid &= $this->valEndDateTime();
					$boolIsValid &= $this->valTargetGoal();
				}

				if( true == $boolGoalDaterequired ) {
					$boolIsValid &= $this->valGoalDateTime();
					$boolIsValid &= $this->valTargetGoal();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>
