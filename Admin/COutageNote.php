<?php

class COutageNote extends CBaseOutageNote {

	use Psi\Libraries\EosFoundation\TEosStoredObject;

	protected $m_intCid;

	protected $m_strTitle;
	protected $m_strFileName;
	protected $m_strUrl;

	/**
	 * Get Functions
	 */

	public function getCid() {
		return $this->m_intCid;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	/**
	 * Set Functions
	 */

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOutageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOutageNote() {
		$boolIsValid = true;
		if( true == is_null( $this->getOutageNote() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'outage_note', 'Outage note is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsFromSlackbot() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valOutageNote();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		return CClient::ID_DEFAULT . '/outages/outage_note/' . date( 'Y/m/d' ) . '/' . $this->getId() . '/' . $this->getTitle();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {
		return CConfig::get( 'OSG_BUCKET_SYSTEM_DOCUMENTS' );
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( CClient::ID_DEFAULT, $this->getId(), self::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

}
?>