<?php

class CTranslation extends CBaseTranslation {

	private $m_strVendorBatchId;
	private $m_strTranslationKey;
	private $m_strDefaultValue;
	private $m_strMachineValue;
	private $m_strNote;
	private $m_strTranslationKeyDetails;

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrValues['vendor_batch_id'] ) && $boolDirectSet ) {
			$this->set( 'm_strVendorBatchId', trim( $arrValues['vendor_batch_id'] ) );
		} elseif( isset( $arrValues['vendor_batch_id'] ) ) {
			$this->setVendorBatchId( $arrValues['vendor_batch_id'] );
		}

		if( isset( $arrValues['translation_key'] ) && $boolDirectSet ) {
			$this->set( 'm_strTranslationKey', trim( $arrValues['translation_key'] ) );
		} elseif( isset( $arrValues['translation_key'] ) ) {
			$this->setTranslationKey( $arrValues['translation_key'] );
		}

		if( true == isset( $arrValues['default_value'] ) ) {
			$this->setDefaultValue( $arrValues['default_value'] );
		}
		if( true == isset( $arrValues['note'] ) ) {
			$this->setDefaultValue( $arrValues['note'] );
		}
		if( true == isset( $arrValues['machine_value'] ) ) {
			$this->setMachineValue( $arrValues['machine_value'] );
		}

		if( true == isset( $arrValues['translation_key_details'] ) ) {
			$this->setTranslationKeyDetails( $arrValues['translation_key_details'] );
		}

		$this->setAllowDifferentialUpdate( true );
	}

	public function setVendorBatchId( $strVendorBatchId ) {
		$this->m_strVendorBatchId = $strVendorBatchId;
	}

	public function getVendorBatchId() {
		return $this->m_strVendorBatchId;
	}

	public function setTranslationKey( $strTranslationKey ) {
		$this->m_strTranslationKey = $strTranslationKey;
	}

	public function setDefaultValue( $strDefaultValue ) {
		$this->m_strDefaultValue = $strDefaultValue;
	}

	public function setNote( $strNote ) {
		$this->m_strNote = $strNote;
	}

	public function setMachineValue( $strMachineValue ) {
		$this->m_strMachineValue = $strMachineValue;
	}

	public function setTranslationKeyDetails( $strTranslationKeyDetails ) {
		$this->m_strTranslationKeyDetails = $strTranslationKeyDetails;
	}

	public function getTranslationKey() {
		return $this->m_strTranslationKey;
	}

	public function getDefaultValue() {
		return $this->m_strDefaultValue;
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function getMachineValue() {
		return $this->m_strMachineValue;
	}

	public function getTranslationKeyDetails() {
		return $this->m_strTranslationKeyDetails;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTranslationKeyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLocaleCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
