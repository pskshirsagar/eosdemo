<?php

class CEmployeeTaxAmount extends CBaseEmployeeTaxAmount {
	const LANDLORD_PAN_REQUIREMENT_AMOUNT = 8333;

	const SPINE_REPORT_TYPE_A = 'A';
	const SPINE_REPORT_TYPE_B = 'B';
	const SPINE_REPORT_TYPE_C = 'C';
	const SPINE_REPORT_TYPE_D = 'D';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProposedAmountEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedAmountEncrypted() {
		if( false === valStr( $this->getApprovedAmountEncrypted() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approved_amount_encrypted', 'Problem facing while encrypting Approved Amount' ) );
			return false;
		}
		return true;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valApprovedAmountEncrypted();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>