<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COfficeShiftEmployees
 * Do not add any new functions to this class.
 */

class COfficeShiftEmployees extends CBaseOfficeShiftEmployees {

	public static function fetchLatestOfficeShiftEmployeeByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = ' SELECT *, 
							CASE WHEN office_shift_id IN ( ' . implode( ',', COfficeShift::$c_arrintBufferTimeOfficeShifts ) . ' )
							THEN office_start_time + interval \'' . COfficeShift::SHIFT_BUFFER_HOURS . ' hour\'
							ELSE office_start_time
							END AS office_start_time_label 
						FROM 
							office_shift_employees 
						WHERE 
							shift_request_type = \'' . COfficeShiftEmployee::SHIFT_CHANGE . '\'  
							AND employee_id = ' . ( int ) $intEmployeeId . ' 
							AND approved_by IS NOT NULL 
							AND denied_by IS NULL 
						ORDER BY id DESC 
						LIMIT 1';

		return parent::fetchOfficeShiftEmployee( $strSql, $objDatabase );
	}

	public static function fetchPreviousOfficeShiftEmployeeByIdByEmployeeId( $intId, $intEmployeeId, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						office_shift_employees
					WHERE
						shift_request_type = \'' . COfficeShiftEmployee::SHIFT_CHANGE . '\'
						AND employee_id = ' . ( int ) $intEmployeeId . '
						AND id < ' . ( int ) $intId . '
					ORDER BY
						id DESC
					LIMIT
						1';

		return parent::fetchOfficeShiftEmployee( $strSql, $objDatabase );
	}

	public static function fetchNextOfficeShiftEmployeeByIdByEmployeeId( $intId, $intEmployeeId, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						office_shift_employees
					WHERE
						shift_request_type = \'' . COfficeShiftEmployee::SHIFT_CHANGE . '\'
						AND employee_id = ' . ( int ) $intEmployeeId . '
						AND id > ' . ( int ) $intId . '
						ORDER BY
						id ASC
					LIMIT
						1';

		return parent::fetchOfficeShiftEmployee( $strSql, $objDatabase );
	}

	public static function fetchOfficeShiftEmployeeById( $intId, $objDatabase ) {

		$strSql = ' SELECT
						*, 
						CASE WHEN office_shift_id IN ( ' . implode( ',', COfficeShift::$c_arrintBufferTimeOfficeShifts ) . ' )
							THEN office_start_time + interval \'' . COfficeShift::SHIFT_BUFFER_HOURS . ' hour\'
							ELSE office_start_time
						END AS office_start_time_label
					FROM
						office_shift_employees
					WHERE
						shift_request_type = \'' . COfficeShiftEmployee::SHIFT_CHANGE . '\'
						AND id = ' . ( int ) $intId;

		return parent::fetchOfficeShiftEmployee( $strSql, $objDatabase );
	}

	public static function fetchOfficeShiftEmployeesByEmployeeId( $intEmployeeId, $objDatabase, $strSortOrder = 'ASC' ) {

		$strSql = ' SELECT
						*
					FROM
						office_shift_employees
					WHERE
						shift_request_type = \'' . COfficeShiftEmployee::SHIFT_CHANGE . '\'
						AND employee_id = ' . ( int ) $intEmployeeId . '
					ORDER BY
						id ' . $strSortOrder;

		return parent::fetchOfficeShiftEmployees( $strSql, $objDatabase );
	}

	public static function fetchOfficeShiftEmployeesWithIsSupportReleaseByEmployeeIdsByTypeByDate( $arrintEmployeeIds, $strType, $strBeginDate, $strEndDate, $objDatabase, $strLocation = NULL, $boolShowAll = false ) {

		$strCondition	= ( 'dashboard' == $strLocation ) ? ' AND ose.approved_by IS NULL' :'';
		$strCondition 	.= ( 'dashboard' == $strLocation ) ? ' AND ose.denied_by IS NULL' :'';
		$strCondition 	.= ( true == valStr( $strBeginDate ) && true == valStr( $strEndDate ) ) ? ' AND ose.start_date::DATE BETWEEN \'' . $strBeginDate . '\'::DATE AND \'' . $strEndDate . '\'::DATE ' : NULL;
		$strCondition 	.= ( 'lams' != $strLocation || false == $boolShowAll ) ? ' And ose.employee_id IN (' . implode( ',', $arrintEmployeeIds ) . ' )' : '';

		$strSql = ' SELECT
						ose.*,
						os.name,
						e.preferred_name as employee_name,
						CASE
						WHEN e.department_id = ' . CDepartment::QA . '  THEN (
																SELECT
																	e.preferred_name
																FROM
																	employees e
																WHERE
																	e.id = t.qam_employee_id and ose.approved_by IS NULL
						)
						 WHEN (e.department_id = ' . CDepartment::CREATIVE . ' OR e.department_id = ' . CDepartment::DEVELOPMENT . ')  THEN ( SELECT
																	e.preferred_name
																FROM
																	employees e
																WHERE
																	e.id = t.tpm_employee_id  and ose.approved_by IS NULL)
						ELSE (SELECT
																	e.preferred_name
																FROM
																	employees e
																	LEFT JOIN employees e1 ON ( e1.id = ose.employee_id )
																WHERE
																	e.id = e1.reporting_manager_id and ose.approved_by IS NULL)
						
						END AS pending_from
					FROM
						office_shift_employees ose
						JOIN office_shifts os ON ( os.id = ose.office_shift_id )
						JOIN employees e ON (e.id = ose.employee_id)
						LEFT JOIN team_employees te ON ( te.employee_id = ose.employee_id AND te.is_primary_team = 1 )
						LEFT JOIN teams as t ON ( t.id = te.team_id AND t.deleted_by IS NULL )
					WHERE
						ose.shift_request_type = \'' . $strType . '\'
						AND ose.deleted_by IS NULL
						AND ose.deleted_on IS NULL
						' . $strCondition . ' ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchOfficeShiftEmployeesByIdsWithIsSupportReleaseByIdByType( $arrintEmployeeIds, $strType, $objDatabase ) {

		$strSql = ' SELECT
						*
						FROM
						office_shift_employees
						WHERE
						shift_request_type = \'' . $strType . '\'
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND id IN (' . implode( ',', $arrintEmployeeIds ) . ' ) ';

		return parent::fetchOfficeShiftEmployees( $strSql, $objDatabase );
	}

	public static function fetchOfficeShiftEmployeesByDate( $strDate, $objDatabase, $arrintEmployeeIds = NULL, $boolIsFromPayrollReport = false ) {

		if( false == valStr( $strDate ) ) return NULL;

		$strIsFromPayrollReport = ( false == $boolIsFromPayrollReport ) ? ' AND ose.start_date <= DATE( \'' . $strDate . '\' ) ' : ' AND ose.approved_by IS NOT NULL AND ose.deleted_by IS NULL ';

		$strEmployeeIdsCondition = ( true == valArr( $arrintEmployeeIds ) ) ? ' AND ose.employee_id IN (\'' . implode( '\', \'', $arrintEmployeeIds ) . '\' )' : '';

		$strSql = ' SELECT
						ose.employee_id,
						ose.office_shift_id,
						CASE
							WHEN ose.office_start_time IS NULL THEN os.office_start_time::time
							ELSE ose.office_start_time::time
						END,
						ose.start_date,
						ose.end_date,
						os.office_start_time AS shift_start_time,
						os.name
					FROM
						office_shift_employees ose
						JOIN office_shifts os ON ( os.id = ose.office_shift_id )
					WHERE
						ose.shift_request_type IN ( \'' . COfficeShiftEmployee::SHIFT_CHANGE . '\',\'' . COfficeShiftEmployee::RELEASE_SUPPORT . '\')
						AND ( ose.end_date IS NULL OR ose.end_date >= DATE( \'' . $strDate . '\' ) )
						' . $strIsFromPayrollReport . $strEmployeeIdsCondition . '
					ORDER BY
						ose.start_date';

		return fetchData( $strSql, $objDatabase );
	}

}
?>