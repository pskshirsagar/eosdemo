<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeHours
 * Do not add any new functions to this class.
 */

class CEmployeeHours extends CBaseEmployeeHours {

	// FUNTION RETURNS SUM OF THE DAILY HOURS FOR ALL THE EMPLOYEES FOR THE PARTICULAR DATE RANGE(PAYROLL PERIOD)

	public static function fetchAllEmployeeHoursByDateRange( $strBeginDate, $strEndDate, $objDatabase ) {

		$strSql = 'SELECT
							to_char( ( EXTRACT( EPOCH FROM SUM( end_datetime - begin_datetime )::interval ) || \' second\' )::"interval", \'HH24:MI:SS\' ) AS total_daily_hours,
							date, employee_id, payroll_period_id, is_manually_added
					FROM
							employee_hours
					WHERE
							date >= \'' . $strBeginDate . '\'
							AND date <= \'' . $strEndDate . '\'
					GROUP BY
							date, employee_id, payroll_period_id,is_manually_added
					ORDER BY
							date DESC';

		return self::fetchEmployeeHours( $strSql, $objDatabase );
	}

	public static function fetchAllOpenEmployeeHours( $strBeginDate, $strEndDate, $objDatabase ) {

		$strSql = 'SELECT	begin_datetime, end_datetime,id, employee_id, date, payroll_period_id,is_manually_added

					FROM
							employee_hours
					WHERE
							date >= \'' . $strBeginDate . '\'
							AND date <= \'' . $strEndDate . '\'
							AND	end_datetime IS NULL AND begin_datetime IS NOT NULL
					ORDER BY
							date DESC';

		return self::fetchEmployeeHours( $strSql, $objDatabase );
	}

	public static function fetchTotalWeeklyEmployeeWorkingHoursByDateByManagerIds( $strBeginDate, $strEndDate, $arrintManagerEmployeeIds = NULL, $objDatabase ) {

		$strSql = ' SELECT
						query.employee_id,
						query.employee_name,
						query.team_name,
						query.team_id,
						query.manager_employee_id,
						to_char( sum( query.time_difference ), \'HH24:MI:SS\' ) as time_difference
					FROM (
							SELECT
								distinct (e.id) AS employee_id,
								e.name_full AS employee_name,
								e.reporting_manager_id AS manager_employee_id,
								( CASE
									WHEN eh.date BETWEEN \' ' . $strBeginDate . ' \' AND \' ' . $strEndDate . ' \'
									THEN sum( eh.end_datetime - eh.begin_datetime )
								END ) as time_difference,
								t.id AS team_id,
								t.name AS team_name
							FROM employees e
								LEFT JOIN team_employees te ON (e.id = te.employee_id)
								LEFT JOIN teams AS t ON (te.team_id = t.id)
								LEFT JOIN employee_hours eh on (eh.employee_id = e.id)
							WHERE e.reporting_manager_id IN ( ' . implode( ',', $arrintManagerEmployeeIds ) . ' )
								AND	e.employee_status_type_id = 1
								AND	date_terminated IS NULL
							GROUP BY e.id, e.name_full,	t.id, t.name, e.reporting_manager_id, eh.date
							ORDER BY employee_name
						) as query
					GROUP BY query.employee_id, query.employee_name,query.team_id,query.team_name,query.manager_employee_id
					ORDER BY query.employee_name';

		return fetchData( $strSql, $objDatabase );
	}

	// FUNCTION RETURNS SUM OF THE DAILY HOURS (UNBATCHED) FOR THE PARTICULAR EMPLOYEE GROUPED BY DATE

	public static function fetchUnbatchedEmployeeHoursByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						to_char( ( EXTRACT( EPOCH FROM SUM( end_datetime - begin_datetime )::interval ) || \' second\' )::"interval", \'HH24:MI\' ) AS total_daily_hours,
						date, employee_id,payroll_period_id,sum( is_manually_added ) as is_manually_added
					FROM
						employee_hours
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND payroll_period_id IS NULL
						AND begin_datetime::date >= \'01/01/2011\'
					GROUP BY
						date, employee_id, payroll_period_id
					ORDER BY
						date DESC';

		return self::fetchEmployeeHours( $strSql, $objDatabase );
	}

	// FUNTION RETURNS ALL THE DAILY HOURS RECORDS FOR THE EMPLOYEE FOR A PARTICULAR DATE

	public static function fetchEmployeeHoursByDateByEmployeeId( $intEmployeeId, $strDate, $objDatabase ) {

		 if( false == valStr( $strDate ) ) return NULL;

		$strSql = 'SELECT
						id,
						to_char( ( EXTRACT( EPOCH FROM ( end_datetime - begin_datetime )::interval ) || \' second\' )::"interval", \'HH24:MI:SS\' ) AS time_difference,
						begin_datetime,
						end_datetime,id,
						employee_id,
						date,
						payroll_period_id,
						is_manually_added,
						CASE WHEN is_manager_approved IS NULL THEN \'\' ELSE is_manager_approved::text END as is_manager_approved,
						employee_hour_type_id,
						updated_by,
						created_by,
						proposed_time::TIMESTAMP
					FROM
						employee_hours
					WHERE
						date = \'' . $strDate . '\'
						AND employee_id = \'' . ( int ) $intEmployeeId . '\'
					ORDER BY
						( CASE
							WHEN ( begin_datetime IS NOT NULL ) THEN begin_datetime
							ELSE end_datetime
						END ),
						id';
		return self::fetchEmployeeHours( $strSql, $objDatabase );
	}

	// static function fetcheS THE LATEST EMPLOYEE HOUR RECORD FOR THE PARTICULAR EMPLOYEE BY EMPLOYEE ID AND BY DATE
	// THE OBJECT RETURNED IS USED WHEN EMPLOYEE TIME OUTS AND THE DATABASE RECORD IS UPDATED FOR THE END_DATETIME

	public static function fetchLatestEmployeeHourByEmployeeId( $intEmployeeId, $objDatabase, $boolManuallyAdded = false ) {

		$strSql = ' SELECT
							*
					FROM
							employee_hours
					WHERE
							employee_id = ' . ( int ) $intEmployeeId;

		$strSql .= ( false == $boolManuallyAdded ) ? ' AND is_manually_added = 0 ' : ' AND is_manager_approved = 1 ';

		$strSql .= ' ORDER BY
							( CASE
								WHEN ( begin_datetime IS NOT NULL ) THEN begin_datetime
								ELSE end_datetime
							END ) DESC,
							id DESC
					 LIMIT 1';

		return self::fetchEmployeeHour( $strSql, $objDatabase );
	}

	// FUNTION RETURNS SUM OF THE DAILY HOURS FOR THE EMPLOYEE LOGGED IN FOR THE PARTICULAR DATE (CURRENT DAY)

	public static function fetchTotalDailyEmployeeHoursByDateByEmployeeId( $strDate, $intEmployeeId, $objDatabase, $intEmployeeHourTypeId = NULL ) {

		$strSql = ' SELECT
							to_char( ( EXTRACT( EPOCH FROM SUM( end_datetime - begin_datetime )::interval ) || \' second\' )::"interval", \'HH24:MI:SS\' ) AS total_daily_hours
					FROM
							employee_hours
					WHERE
							date = \' ' . $strDate . ' \' AND employee_id = ' . ( int ) $intEmployeeId;

		if( false == is_null( $intEmployeeHourTypeId ) ) {
			$strSql .= ' AND employee_hour_type_id = ' . ( int ) $intEmployeeHourTypeId;
		}

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['total_daily_hours'] ) ) return $arrintResponse[0]['total_daily_hours'];
		return 0;

	}

	// FUNTION RETURNS ALL THE UNBATECHED HOURS FOR A PARTICULAR EMPLOYEE TO SHOW IN THE VIEW UNBATCHED HOURS POPUP

	public static function fetchAllUnbatchedEmployeeHoursByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						to_char( ( EXTRACT( EPOCH FROM ( end_datetime - begin_datetime )::interval ) || \' second\' )::"interval", \'HH24:MI\' ) AS time_difference,
						begin_datetime,
						end_datetime,
						id,
						employee_id,
						DATE,
						payroll_period_id,
						is_manually_added
					FROM
						employee_hours
					WHERE
						payroll_period_id IS NULL
						AND employee_id = \'' . ( int ) $intEmployeeId . '\'
					ORDER BY
						( CASE
							WHEN ( begin_datetime IS NOT NULL ) THEN begin_datetime
							ELSE end_datetime
						END ),
						id ';

		return self::fetchEmployeeHours( $strSql, $objDatabase );
	}

	public static function fetchCurrentWeeksUnbatchedEmployeeHoursByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						to_char( ( EXTRACT( EPOCH FROM SUM( end_datetime - begin_datetime )::interval ) || \' second\' )::"interval", \'HH24:MI\' ) AS total_daily_hours,
						date,
						employee_id,
						payroll_period_id,
						SUM( is_manually_added ) AS is_manually_added
					FROM
						employee_hours
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND	payroll_period_id IS NULL
						AND begin_datetime >= date_trunc( \'WEEK\', current_date )
					GROUP BY
						date, employee_id, payroll_period_id
					ORDER BY
						date DESC';

		return self::fetchEmployeeHours( $strSql, $objDatabase );
	}

	public static function fetchEmployeeHoursByEmployeeVacationRequestIds( $arrintEmployeeVacationRequestIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeVacationRequestIds ) ) return NULL;

		$strSql = 'SELECT * FROM employee_hours WHERE employee_vacation_request_id IN ( ' . implode( ',', $arrintEmployeeVacationRequestIds ) . ' )';

		return self::fetchEmployeeHours( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeeWorkingHoursByDateByManagerId( $strBeginDate, $strEndDate, $intManagerEmployeeId, $objDatabase, $arrstrAttendanceFilter ) {

	 	if( true == empty( $intManagerEmployeeId ) || false == is_numeric( $intManagerEmployeeId ) ) return NULL;

		$intOffset 				= ( true == isset( $arrstrAttendanceFilter['page_no'] ) && true == isset( $arrstrAttendanceFilter['page_size'] ) ) ? $arrstrAttendanceFilter['page_size'] * ( $arrstrAttendanceFilter['page_no'] - 1 ) : 0;
		$intLimit 				= ( true == isset( $arrstrAttendanceFilter['page_size'] ) ) ? $arrstrAttendanceFilter['page_size'] : '';
		$boolIsShowAll			= ( true == isset( $arrstrAttendanceFilter['show_all'] ) && true == $arrstrAttendanceFilter['show_all'] ) ? true : false;
		$strOrderByField 		= ' name_full ';
		$strOrderByType			= ' ASC';

		if( true == valArr( $arrstrAttendanceFilter ) ) {
			if( true == isset( $arrstrAttendanceFilter['order_by_field'] ) ) {

				switch( $arrstrAttendanceFilter['order_by_field'] ) {

					case 'employee_name':
						$strOrderByField = ' e.preferred_name ';
						break;

					case 'team_name':
						$strOrderByField = ' team_name';
						break;

					default:
						$strOrderByField = ' e.preferred_name ';
						break;
				}
			}

			if( true == isset( $arrstrAttendanceFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrAttendanceFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}
		}

		$strSelectClause 	= ' e.reporting_manager_id AS manager_employee_id ';
		$strFromClause		= ' employees e ';
		$strJoinClause		= ' LEFT JOIN team_employees te ON ( e.id = te.employee_id ) LEFT JOIN teams t ON ( te.team_id = t.id ) ';
		$strWhereClause 	= ' AND te.is_primary_team = 1 ';
		$strGroupByClause	= ' e.reporting_manager_id ';
		$strRecursiveSql 	= ' WITH RECURSIVE all_employees( employee_id, team_id, manager_employee_id, depth ) AS (
									SELECT
										te.employee_id,
										te.team_id,
										e.reporting_manager_id AS manager_employee_id,
										1
									FROM
										team_employees te
										JOIN employees e ON ( te.employee_id = e.id )
									WHERE
										e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
										AND te.is_primary_team = 1
										AND te.employee_id <> ' . ( int ) $intManagerEmployeeId . '
									UNION ALL
									SELECT
										te.employee_id,
										te.team_id,
										e.reporting_manager_id AS manager_employee_id,
										al.depth + 1
									FROM
										all_employees al
										JOIN employees e ON ( e.reporting_manager_id = al.employee_id )
										JOIN team_employees te ON ( te.employee_id = e.id )
									WHERE
										te.is_primary_team = 1
										AND te.employee_id <> ' . ( int ) $intManagerEmployeeId . '
								)';

		if( true == is_null( $arrstrAttendanceFilter['employee_id'] ) || true == empty( $arrstrAttendanceFilter['employee_id'] ) ) {
			if( true == valArr( $arrstrAttendanceFilter['dev_qa_employee_ids'] ) ) {
				$strWhereClause	.= ' AND ( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN ( ' . implode( ',', $arrstrAttendanceFilter['dev_qa_employee_ids'] ) . ' ) )';
			} else {
				$strWhereClause	.= 'AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;
			}
		}

		if( true == $boolIsShowAll ) {
			$strSelectClause 	= ' al.manager_employee_id ';
			$strFromClause		= ' all_employees al ';
			$strJoinClause		= ' JOIN employees e ON ( al.employee_id = e.id ) LEFT JOIN teams t ON ( al.team_id = t.id ) ';
			$strGroupByClause	= ' al.manager_employee_id ';
			$strWhereClause 	= '';
		}

		if( false == is_null( $arrstrAttendanceFilter['employee_id'] ) && false == empty( $arrstrAttendanceFilter['employee_id'] ) ) {
			$strWhereClause .= ' AND e.id = ' . $arrstrAttendanceFilter['employee_id'];
		}

		if( true == valArr( $arrstrAttendanceFilter['qam_employee_ids'] ) ) {
			$strWhereClause	.= ' OR e.id IN (' . implode( ',', $arrstrAttendanceFilter['qam_employee_ids'] ) . ')';
		}

	 	$strSql = ' SELECT
						e.id AS employee_id,
	 					MAX( eh.end_datetime) AS end_datetime,
						MAX( eh.begin_datetime) AS begin_datetime,
						e.preferred_name AS employee_name,
						t.id AS team_id,
						t.name AS team_name,
						d.name AS designation,
	 					d.country_code,
	 					to_char( ( EXTRACT( EPOCH FROM ( SUM ( eh.end_datetime - eh.begin_datetime ) )::interval ) || \' second\' )::"interval", \'HH24:MI:SS\' ) AS worked_hours,
	 					' . $strSelectClause . '
					FROM
						' . $strFromClause . $strJoinClause . '
						LEFT JOIN designations d ON ( e.designation_id = d.id )
	 					LEFT JOIN employee_hours eh ON ( e.id = eh.employee_id AND eh.date BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id <> ' . ( int ) $intManagerEmployeeId . '
						' . $strWhereClause . '
					GROUP BY
						e.id,
						t.id,
						t.name,
						d.name,
						d.country_code,
						e.preferred_name,
						' . $strGroupByClause . '
					 ORDER BY
							' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		if( true == $boolIsShowAll ) $strSql = $strRecursiveSql . $strSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPresentEmployeesCountByMonthByYearByCountryCode( $strBeginDatetime, $strEndDatetime, $strCountryCode, $objDatabase ) {

		$strJoinCondition  = '';
		$strWhereCondition = '';

		if( false == valStr( $strBeginDatetime ) && false == valStr( $strEndDatetime ) ) return NULL;

		if( CCountry::CODE_USA == $strCountryCode ) {
				$strJoinCondition = ' LEFT JOIN departments d ON ( d.id = e.department_id ) ';

				$strWhereCondition = ' AND d.id = ' . CDepartment::CALL_CENTER;
		}

		$strSql = ' SELECT
						COUNT ( DISTINCT ( eh.employee_id ) ) AS total_present_employee,
						EXTRACT ( DAY FROM eh.date ) AS DAY
					FROM
						employee_hours eh
						LEFT JOIN employee_addresses ea ON ( eh.employee_id = ea.employee_id )
						LEFT JOIN employees e ON ( e.id = eh.employee_id ) ' . $strJoinCondition . '
					WHERE
						eh.date BETWEEN date_trunc( \'day\', \'' . $strBeginDatetime . '\'::date ) AND date_trunc( \'day\', \'' . $strEndDatetime . '\'::date )
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\'
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )' . $strWhereCondition . '
					GROUP BY
						eh.date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPresentEmployeesByDateByCountryCode( $strDate, $strCountryCode, $objDatabase, $intDepartmentId = NULL, $strOrderByField = NULL, $strOrderByType = NULL, $strFilteredExplodedSearch = NULL ) {

		if( false == valStr( $strDate ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT eh1.employee_id,
						eh1.first_swipe,
						e.preferred_name AS name_full,
						e.employee_number,
						est.name As employee_status,
						e.email_address,
						epn.phone_number,
						de.name as designation,
						d.name,
						last_swipe,
						total_on_floor_hour::TIME,
						ear.note,
						ear.expected_in_time,
						evr.approved_on,
						evr.denied_on,
						evr.id as vacation_request_id,
						evr.request as reason
					FROM
						(
							SELECT
								DISTINCT eh.employee_id,
								eh.employee_hour_type_id,
								SUM( eh.end_datetime - eh.begin_datetime ) OVER ( PARTITION BY eh.employee_id, eh.date,eh.employee_hour_type_id ) AS total_on_floor_hour,
								MIN( eh.begin_datetime ) OVER ( PARTITION BY eh.employee_id, eh.date ) AS first_swipe,
								MAX( eh.end_datetime ) OVER ( PARTITION BY eh.employee_id, eh.date ) AS last_swipe
							FROM
								employee_hours AS eh
								JOIN employee_addresses AS ea ON ( eh.employee_id = ea.employee_id )
							WHERE
								date_trunc ( \'day\', eh.date ) = DATE( \' ' . $strDate . ' \' )
								AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
								AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\'
						) AS eh1
						LEFT JOIN employees AS e ON ( eh1.employee_id = e.id )
						LEFT JOIN employee_phone_numbers AS epn ON ( eh1.employee_id = epn.employee_id AND epn.phone_number_type_id = \'' . CPhoneNumberType::PRIMARY . '\' )
						LEFT JOIN departments as d ON ( e.department_id = d.id )
						LEFT JOIN designations as de ON ( e.designation_id = de.id )
						LEFT JOIN employee_status_types est ON ( e.employee_status_type_id = est.id )
						LEFT JOIN employee_attendance_remarks AS ear ON ( e.id = ear.employee_id AND ear.requested_datetime = \' ' . $strDate . ' \' )
						LEFT JOIN employee_vacation_requests AS evr ON ( e.id = evr.employee_id AND evr.vacation_type_id = ' . CVacationType::WEEKEND_WORKING . ' AND evr.begin_datetime::date = \' ' . $strDate . '\' AND evr.deleted_by IS NULL )
					WHERE
						eh1.employee_hour_type_id = \'' . CEmployeeHourType::FLOOR . '\'
						AND	e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= 'AND d.id= ' . ( int ) $intDepartmentId . ' ';
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSql .= ' AND ( name_full ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR e.email_address ILIKE \'%' . $strFilteredExplodedSearch . '%\' )';
		}

		$strSql .= 'ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPresentEmployeesCountByDateBYCountryCode( $strDate, $strCountryCode, $objDatabase, $intDepartmentId = NULL, $strFilteredExplodedSearch = NULL ) {

			if( false == valStr( $strDate ) ) return NULL;

		$strSql = 'SELECT
						COUNT( DISTINCT eh.employee_id )
					FROM
						employee_hours eh
						LEFT JOIN employees e ON ( eh.employee_id = e.id )
						LEFT JOIN employee_addresses ea ON ( eh.employee_id = ea.employee_id )
						LEFT JOIN departments as d ON ( e.department_id = d.id )
					WHERE
						eh.date = \' ' . $strDate . ' \'
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\'
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= 'AND d.id= ' . ( int ) $intDepartmentId;
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSql .= ' AND ( name_full ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR e.email_address ILIKE \'%' . $strFilteredExplodedSearch . '%\' )';
		}

		$arrintPresentEmployeesCount = fetchData( $strSql, $objDatabase );

		return $arrintPresentEmployeesCount[0]['count'];
	}

	public static function fetchEmployeeMissingSwipesByDateRange( $intEmployeeId, $strBeginDate, $strEndDate, $objDatabase ) {

		if( false == valStr( $strBeginDate ) || false == valStr( $strEndDate ) || false == is_numeric( $intEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						DISTINCT EXTRACT ( DAY FROM ( DATE ) ) AS day,
						EXTRACT ( WEEK FROM ( DATE ) + \'1 day\'::interval ) AS week,
						date
					FROM
						employee_hours
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND DATE BETWEEN \'' . $strBeginDate . '\'
						AND \'' . $strEndDate . '\'
						AND ( end_datetime IS NULL
						OR begin_datetime IS NULL )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDailyEmployeeHoursByDateByEmployeeIdByEmployeeHourTypeId( $strDate, $intEmployeeId, $objDatabase ) {

		if( false == valStr( $strDate ) || false == is_numeric( $intEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						 DISTINCT employee_hour_type_id,
						 DATE,
						 to_char( ( EXTRACT( EPOCH FROM ( SUM ( end_datetime - begin_datetime ) OVER ( PARTITION BY DATE, employee_hour_type_id ))::interval ) || \' second\' )::"interval", \'HH24:MI:SS\' ) AS hours
					 FROM
						 employee_hours
					 WHERE
						 employee_id = ' . ( int ) $intEmployeeId . '
						 AND DATE = \'' . $strDate . '\'
						 AND employee_hour_type_id <> ' . CEmployeeHourType::GAMING_ROOM . '
					ORDER BY
						 employee_hour_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPreviousWorkingDayEmployeeHoursByDateByEmployeeIds( $arrintEmployeeIds, $strDate, $strCountryCode, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) || false == valStr( $strDate ) ) return NULL;

		$intDayOfWeekSaturday 	= 6;
		$intDayOfWeekSunday 	= 0;

		$strSql = ' SELECT
						employee_hours.employee_id,
						employee_hours.date,
						employee_hours.previous_day_out_time,
						employee_hours.previous_day_on_floor_hours
					FROM
						(
							SELECT
								DISTINCT employee_id,
								DATE,
								employee_hour_type_id,
								MAX ( end_datetime ) OVER ( PARTITION BY employee_id ) AS previous_day_out_time,
								to_char ( ( EXTRACT ( EPOCH FROM( SUM ( end_datetime - begin_datetime ) OVER ( PARTITION BY employee_id, employee_hour_type_id ) ) ::interval ) || \'second\' ) ::"interval", \'HH24:MI:SS\' ) AS previous_day_on_floor_hours
							FROM
								employee_hours
							WHERE
								employee_id IN ( \'' . implode( '\',\'', $arrintEmployeeIds ) . '\' )
								AND DATE =
									(
										SELECT
											MAX ( eh.date )
										FROM
											employee_hours AS eh
										WHERE
											DATE < \'' . $strDate . '\'
											AND eh.DATE NOT IN (
															SELECT
																ph.DATE
															FROM
																paid_holidays AS ph
															WHERE
																country_code = \'' . $strCountryCode . '\'
																AND is_optional IS FALSE
															)
											AND EXTRACT ( \'dow\' FROM eh.date ) NOT IN ( ' . ( int ) $intDayOfWeekSaturday . ', ' . ( int ) $intDayOfWeekSunday . ' )
									)
						) AS employee_hours
					WHERE
						employee_hours.employee_hour_type_id = ' . CEmployeeHourType::FLOOR . '
					ORDER BY
						employee_hours.employee_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function updateEmployeeHoursByPayrollPeriodIdByCountryCode( $intPayrollPeriod, $strCountryCode, $objDatabase ) {

		if( false == is_numeric( $intPayrollPeriod ) ) return NULL;

		$strSql = 'UPDATE
						employee_hours AS eh
					SET
						payroll_period_id = ' . ( int ) $intPayrollPeriod . '
					FROM
						payroll_periods pp,
						employee_addresses ea
					WHERE
						eh.date BETWEEN pp.begin_date
						AND pp.end_date
						AND eh.employee_id = ea.employee_id
						AND ea.country_code = \'' . $strCountryCode . '\'
						AND pp.id = ' . ( int ) $intPayrollPeriod;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchExtraWorkingDaysByMonthByYearByCountryCode( $strBeginDate, $strEndDate, $strCountryCode, $objDatabase, $arrintEmployeeIds = NULL ) {

		if( false == valStr( $strBeginDate ) || false == valStr( $strEndDate ) ) return NULL;

		if( true == valArr( $arrintEmployeeIds ) ) {
			$strEmployeeIdsCondition = 'AND eh.employee_id IN (\'' . implode( '\', \'', $arrintEmployeeIds ) . '\' )';
		} else {
			$strEmployeeIdsCondition = '';
		}

		$intDayOfWeekSaturday			= 6;
		$intDayOfWeekSunday				= 0;

		$strSql = 'SELECT
						employee_hours.employee_id,
						COUNT ( CASE
									WHEN ( ( EXTRACT ( \'dow\' FROM employee_hours.DATE ) IN ( \'' . ( int ) $intDayOfWeekSaturday . '\', \'' . ( int ) $intDayOfWeekSunday . '\' ) )
																OR ( employee_hours.DATE IN (
																					SELECT
																						DATE
																					FROM
																						paid_holidays
																					WHERE
																						country_code = \'' . addslashes( $strCountryCode ) . '\'
																						AND is_optional IS FALSE
																						AND DATE BETWEEN \'' . $strBeginDate . '\'
																						AND \'' . $strEndDate . '\'
																				)
																	)
										) AND DATE BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' AND employee_hours.total_on_floor_hour >= \'' . CEmployeeSwipe::MIN_REQUIRED_ONFLOOR_HOURS_FOR_PRESENT . '\'
									THEN employee_hours.employee_id
								END
							) AS extra_working_days
					FROM
						(
							SELECT
								DISTINCT eh.employee_id,
								SUM ( eh.end_datetime - eh.begin_datetime ) OVER ( PARTITION BY eh.employee_id, eh.date ) AS total_on_floor_hour,
								MIN ( eh.begin_datetime ) OVER ( PARTITION BY eh.employee_id, eh.date ) AS first_swipe,
								eh.date
							FROM
								employee_hours AS eh
								JOIN employee_addresses AS ea ON ( eh.employee_id = ea.employee_id )
							WHERE
								eh.date BETWEEN date_trunc ( \'day\', \'' . $strBeginDate . '\' ::date ) AND date_trunc ( \'day\', \'' . $strEndDate . '\' ::date )
								AND eh.employee_hour_type_id = \'' . CEmployeeHourType::FLOOR . '\'
								AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
								AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\'
								' . $strEmployeeIdsCondition . '
						) AS employee_hours
					GROUP BY
						employee_hours.employee_id
					ORDER BY
						employee_hours.employee_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchBatchedPayrollPeriodIdsByCountryCode( $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT payroll_period_id
					FROM
						employee_hours AS eh
					WHERE
						eh.employee_id IN (
											SELECT
												DISTINCT eh.employee_id
											FROM
												employee_hours AS eh
												JOIN employee_addresses AS ea ON ( eh.employee_id = ea.employee_id ) AND ea.country_code = \'' . $strCountryCode . '\'
					)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesAverageOnFloorHoursByCountryCode( $objDatabase, $strCountryCode, $intEmployeeId = NULL, $boolIsDailyTotalOnFloorHours = false ) {

		$intDayOfWeekSaturday 	= 6;
		$intDayOfWeekSunday 	= 0;

		$strEmployeeIdCondition = ( true == is_numeric( $intEmployeeId ) ) ? ' AND eh.employee_id = ' . $intEmployeeId : '';

		$strSql = 'SELECT
						DISTINCT eh.employee_id,
						eh.date,
						SUM ( eh.end_datetime - eh.begin_datetime ) OVER ( PARTITION BY eh.date, eh.employee_id ) AS daily_total_on_floor_hours
					FROM
						employee_hours eh
						JOIN employee_addresses AS ea ON ( eh.employee_id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\' AND ea.country_code = \'' . $strCountryCode . '\' AND eh.employee_hour_type_id = \'' . CEmployeeHourType::FLOOR . '\' )
					WHERE
						eh.date BETWEEN \'' . date( 'm/d/Y', strtotime( '-3 months - 1 days' ) ) . '\' AND \'' . date( 'm/d/Y', strtotime( '-1 days' ) ) . '\'
						AND eh.date NOT IN ( SELECT date FROM paid_holidays WHERE country_code = \'' . $strCountryCode . '\' AND is_optional IS FALSE AND date BETWEEN \'' . date( 'm/d/Y', strtotime( '-3 months - 1 days' ) ) . '\' AND \'' . date( 'm/d/Y', strtotime( '-1 days' ) ) . '\' )
						AND EXTRACT ( \'dow\' FROM eh.date ) NOT IN (' . ( int ) $intDayOfWeekSaturday . ', ' . ( int ) $intDayOfWeekSunday . ' )
						' . $strEmployeeIdCondition . '
					ORDER BY
						eh.date';

		if( false == $boolIsDailyTotalOnFloorHours ) {
			$strSql = 'SELECT
							to_char ( avg ( employee_hours_data.daily_total_on_floor_hours ), \'HH24:MI\' ) AS average_on_floor_hours
						FROM
							( ' . $strSql . ' ) AS employee_hours_data';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPresentEmployeesByDateByDepartmentIdByTeamId( $strDate, $intDepartmentId = NULL, $intTeamId = NULL, $strOrderByField, $strOrderByType, $objDatabase, $strCountryCode = NULL, $strFilteredExplodedSearch = NULL, $arrmixFilter = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( false == valStr( $strDate ) ) return NULL;

		$strJoinCondition		= '';

		if( true == is_numeric( $intTeamId ) || true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strJoinCondition = ' LEFT JOIN team_employees as te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
								  LEFT JOIN teams as t ON ( t.id = te.team_id )';
		}

		$strSql = 'SELECT
						DISTINCT eh1.employee_id,
						eh1.first_swipe,
						e.preferred_name,
						e.employee_number,
						e.email_address,
						epn.phone_number,
						de.name as designation,
						d.name,
						last_swipe,
						total_on_floor_hour::TIME,
						d.name as department
					FROM
						(
							SELECT
								DISTINCT eh.employee_id,
								eh.employee_hour_type_id,
								SUM( eh.end_datetime - eh.begin_datetime ) OVER ( PARTITION BY eh.employee_id, eh.date,eh.employee_hour_type_id ) AS total_on_floor_hour,
								MIN( eh.begin_datetime ) OVER ( PARTITION BY eh.employee_id, eh.date ) AS first_swipe,
								MAX( eh.end_datetime ) OVER ( PARTITION BY eh.employee_id, eh.date ) AS last_swipe
							FROM
								employee_hours AS eh
								JOIN employee_addresses AS ea ON ( eh.employee_id = ea.employee_id )
							WHERE
								date_trunc ( \'day\', eh.date ) = DATE( \' ' . $strDate . ' \' )
								AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
								AND ea.country_code = \'' . $strCountryCode . '\'
						) AS eh1
						LEFT JOIN employees AS e ON ( eh1.employee_id = e.id )
						LEFT JOIN employee_phone_numbers AS epn ON ( eh1.employee_id = epn.employee_id AND epn.phone_number_type_id = \'' . CPhoneNumberType::PRIMARY . '\' )
						LEFT JOIN departments as d ON ( e.department_id = d.id )
						LEFT JOIN designations as de ON ( e.designation_id = de.id )' . $strJoinCondition;
		$strSql .= ' WHERE
						eh1.employee_hour_type_id = \'' . CEmployeeHourType::FLOOR . '\'
						AND	e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= ' AND d.id = ' . ( int ) $intDepartmentId;
		}

		if( true == is_numeric( $intTeamId ) ) {
			$strSql .= ' AND t.id = ' . ( int ) $intTeamId;
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSql .= ' AND ( e.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR e.email_address ILIKE \'%' . $strFilteredExplodedSearch . '%\' )';
		}

		if( true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strSql .= ' AND t.deleted_by IS NULL' . ( ( true == is_numeric( $arrmixFilter['add_employee_id'] ) ) ? ' AND t.add_employee_id = ' . ( int ) $arrmixFilter['add_employee_id'] : ' AND t.hr_representative_employee_id = ' . ( int ) $arrmixFilter['hr_representative_employee_id'] );
		}

		$strSql .= ' ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesWeeklyAverageHoursByEmployeeIdsByDateRangeByCountryCode( $arrintTeamEmployeeIds, $strStartDate, $strEndDate, $strCountryCode, $objDatabase ) {

		if( false == valArr( $arrintTeamEmployeeIds ) || false == valStr( $strStartDate ) || false == valStr( $strEndDate ) || false == valStr( $strCountryCode ) ) return NULL;

		$intDayOfWeekSaturday	= 6;
		$intDayOfWeekSunday		= 0;

		$strSql = 'SELECT
						DISTINCT ( date_trunc ( \'WEEK\', employee_hour.date ) ) ::date AS start_date,
						( date_trunc ( \'WEEK\', employee_hour.date ) + \'6 days\' ) ::date AS end_date,
						employee_hour.employee_hour_type_id,
						TO_CHAR ( ( ( EXTRACT ( EPOCH FROM SUM ( employee_hour.total_daily_employee_hours ) OVER ( PARTITION BY ( date_trunc ( \'WEEK\', employee_hour.date ) ) ) ::interval ) / ' . \Psi\Libraries\UtilFunctions\count( $arrintTeamEmployeeIds ) . ' ) || \'second\' ) ::interval, \'HH24.MI\' ) AS average_total_weekly_employee_hours,
						TO_CHAR ( ( ( EXTRACT ( EPOCH FROM SUM ( employee_hour.total_daily_employee_hours ) OVER ( PARTITION BY ( date_trunc ( \'WEEK\', employee_hour.date ) ), employee_hour.employee_hour_type_id ) ::interval ) / ' . \Psi\Libraries\UtilFunctions\count( $arrintTeamEmployeeIds ) . ' ) || \'second\' ) ::interval, \'HH24.MI\' ) AS average_weekly_employee_hours
					FROM
						(
							SELECT
								date,
								employee_hour_type_id,
								SUM ( end_datetime - begin_datetime ) AS total_daily_employee_hours
							FROM
								employee_hours
							WHERE
								date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
								AND date NOT IN (
												 SELECT
													 DATE
												 FROM
													 paid_holidays
												 WHERE
													 country_code = \'' . addslashes( $strCountryCode ) . '\'
													 AND is_optional IS FALSE
													 AND DATE BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
												)
								AND EXTRACT ( \'dow\' FROM date ) NOT IN ( ' . ( int ) $intDayOfWeekSaturday . ', ' . ( int ) $intDayOfWeekSunday . ' )
								AND employee_id IN ( \'' . implode( '\', \'', $arrintTeamEmployeeIds ) . '\' )
							GROUP BY
								date,
								employee_hour_type_id
						) employee_hour
					ORDER BY
						start_date,
						employee_hour.employee_hour_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesSwipesByDateRangeByEmployeeIds( $arrintTeamEmployeeIds, $strStartDate, $strEndDate, $strCountryCode, $objDatabase ) {

		if( false == valArr( $arrintTeamEmployeeIds ) || false == valStr( $strStartDate ) || false == valStr( $strEndDate ) || false == valStr( $strCountryCode ) ) return NULL;

		$intDayOfWeekSaturday	= 6;
		$intDayOfWeekSunday		= 0;

		$strSql = 'SELECT
					eh.employee_id,
					MIN ( eh.begin_datetime::time ) AS first_swipe,
					MAX ( eh.end_datetime::time ) AS last_swipe,
					eh.date
				FROM
					employee_hours AS eh
					JOIN employee_addresses AS ea ON ( eh.employee_id = ea.employee_id AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\' AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\' AND eh.DATE BETWEEN DATE ( \' ' . $strStartDate . ' \' ) AND DATE ( \' ' . $strEndDate . ' \' ) )
				WHERE
					eh.employee_id IN ( \'' . implode( '\', \'', $arrintTeamEmployeeIds ) . '\' )
					AND EXTRACT ( \'dow\' FROM eh.DATE ) NOT IN ( \'' . ( int ) $intDayOfWeekSaturday . '\', \'' . ( int ) $intDayOfWeekSunday . '\' )
					AND eh.date NOT IN (
										SELECT
											DATE
										FROM
											paid_holidays
										WHERE
											country_code = \'' . addslashes( $strCountryCode ) . '\'
											AND is_optional IS FALSE
											AND DATE BETWEEN DATE( \' ' . $strStartDate . ' \' )
											AND DATE( \' ' . $strEndDate . ' \' )
										)
				GROUP BY
					eh.employee_id,
					eh.date
				ORDER BY
					eh.employee_id,
					eh.date';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeeWorkingHoursByDateByPdmEmployeeId( $strBeginDate, $strEndDate, $intSdmEmployeeId, $objDatabase,	$arrstrAttendanceFilter, $arrstrFilteredExplodedSearch = NULL, $boolIsFromTimeOff = false, $intVacationTypeId = NULL ) {

		$intOffset				= ( true == isset( $arrstrAttendanceFilter['page_no'] ) && true == isset( $arrstrAttendanceFilter['page_size'] ) ) ? $arrstrAttendanceFilter['page_size'] * ( $arrstrAttendanceFilter['page_no'] - 1 ) : 0;
		$intLimit				= ( true == isset( $arrstrAttendanceFilter['page_size'] ) ) ? $arrstrAttendanceFilter['page_size'] : '';
		$strOrderByField		= ' e.name_full ';
		$strOrderByType			= ' ASC';
		$strCondition			= ( true == is_numeric( $intVacationTypeId ) ) ? ' AND evr.vacation_type_id = ' . ( int ) $intVacationTypeId : ' AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';

		if( true == valArr( $arrstrAttendanceFilter ) ) {
			if( true == isset( $arrstrAttendanceFilter['order_by_field'] ) ) {

				switch( $arrstrAttendanceFilter['order_by_field'] ) {

					case 'team_name':
						$strOrderByField = ' team_name';
						break;

					default:
						$strOrderByField = ' e.preferred_name ';
						break;
				}
			}

			if( true == isset( $arrstrAttendanceFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrAttendanceFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}
		}

		if( true == $boolIsFromTimeOff ) {
			$strJoin = 'LEFT JOIN employee_vacation_schedules evs ON ( evs.employee_id = e.id AND evs.year = EXTRACT ( YEAR FROM NOW ( ) ) )
						LEFT JOIN employee_vacation_requests evr ON ( evr.employee_id = evs.employee_id AND evr.deleted_on IS NULL AND evr.denied_on IS NULL AND evr.begin_datetime BETWEEN DATE \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' AND evr.is_paid IS TRUE' . $strCondition . ' )';

			$strSelect = 'd.name AS designation_name,
						MAX ( evr.actual_vacation_hours / 8::FLOAT ) AS actual_vacation_hours,
						( evs.hours_granted / 8 ) ::FLOAT AS total_leaves_alloted,
						SUM ( CASE
									 WHEN evr.approved_on IS NOT NULL THEN evr.scheduled_vacation_hours
									 ELSE NULL
								END
							) / 8::FLOAT AS total_used,
						SUM ( CASE
									WHEN evr.begin_datetime >= NOW ( ) THEN evr.scheduled_vacation_hours
									ELSE NULL
								END
							) / 8::FLOAT AS scheduled_future';

			$strGroup = 'evs.hours_granted,';
		} else {
			$strJoin = 'LEFT JOIN employee_hours eh ON ( e.id = eh.employee_id AND eh.date BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' )';

			$strSelect = 'd.name AS designation,
						to_char( ( EXTRACT( EPOCH FROM ( SUM ( eh.end_datetime - eh.begin_datetime ) )::interval ) || \' second\' )::"interval", \'HH24:MI:SS\' ) AS worked_hours,
						MAX ( eh.end_datetime ) AS end_datetime,
						MAX ( eh.begin_datetime ) AS begin_datetime';
			$strGroup = '';
		}

		$strSearchCondition = '';

		if( false == is_null( $arrstrFilteredExplodedSearch ) ) {
			$strSearchCondition = 'AND ( e.name_first ILIKE \'%' . implode( '%\' AND e.name_first ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
									OR e.name_last ILIKE \'%' . implode( '%\' AND e.name_last ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
									OR e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
									OR e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
									OR t.name ILIKE \'%' . implode( '%\' AND t.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )';
		}

		$strSql = 'SELECT
						e.id AS employee_id,
						e.id,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.email_address,
						e.name_full AS employee_name,
						MAX( t.name ) AS team_name,
						d.country_code,
						t.sdm_employee_id AS manager_employee_id, ' . $strSelect . '
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN employees e2 ON ( te.employee_id = e2.id )
						LEFT JOIN employees e ON ( e2.reporting_manager_id = e.id )
						LEFT JOIN employees e1 ON ( t.sdm_employee_id = e1.id )
						LEFT JOIN designations d ON ( e.designation_id = d.id )	' . $strJoin . '
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id <> ' . ( int ) $intSdmEmployeeId . '
						AND t.deleted_by IS NULL
						AND t.sdm_employee_id = ' . ( int ) $intSdmEmployeeId . ' ' . $strSearchCondition . '
						AND e.id NOT IN (SELECT DISTINCT employee_id from team_employees where is_primary_team = 1 AND team_id IN (select id from teams where sdm_employee_id = ' . ( int ) $intSdmEmployeeId . ' AND deleted_by IS NULL))
					GROUP BY
						e.id,
						e.name_full,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.email_address,
						t.sdm_employee_id,
						d.name,
						t.sdm_employee_id, ' . $strGroup . '
						d.country_code
					ORDER BY
						' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( true == is_numeric( $intLimit ) ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeWorkingHoursCountByDateByPdmEmployeeId( $strBeginDate, $strEndDate, $intPdmEmployeeId, $objDatabase, $boolIsFromTimeOff = false ) {

		if( true == $boolIsFromTimeOff ) {
			$strJoin = 'LEFT JOIN employee_vacation_schedules evs ON ( evs.employee_id = e.id AND evs.year = EXTRACT ( YEAR FROM NOW ( ) ) )
						LEFT JOIN employee_vacation_requests evr ON ( evr.employee_id = evs.employee_id AND evr.deleted_on IS NULL AND evr.denied_on IS NULL AND evr.begin_datetime BETWEEN DATE \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' AND evr.is_paid IS TRUE )';

		} else {
			$strJoin = 'LEFT JOIN employee_hours eh ON ( e.id = eh.employee_id AND eh.date BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' )';

		}

		$strSql = 'SELECT
						count( DISTINCT e2.reporting_manager_id )
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN employees e2 ON ( te.employee_id = e2.id )
						LEFT JOIN employees e ON ( e2.reporting_manager_id = e.id )
						LEFT JOIN employees e1 ON ( t.sdm_employee_id = e1.id )	' . $strJoin . '
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id <> ' . ( int ) $intPdmEmployeeId . '
						AND t.deleted_by IS NULL
						AND ( e2.reporting_manager_id = t.tpm_employee_id or e2.reporting_manager_id = t.qam_employee_id )
						AND t.sdm_employee_id = ' . ( int ) $intPdmEmployeeId;

		$arrintTeamEmployeeCount = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintTeamEmployeeCount[0]['count'] ) ) return $arrintTeamEmployeeCount[0]['count'];
	}

	public static function fetchEmployeeHoursByDateRange( $strBeginDate, $strEndDate, $intPreviousDate, $objDatabase ) {

		if( false == valStr( $strBeginDate ) || false == valStr( $strEndDate ) ) return NULL;

		$strSql = 'SELECT
						t.employee_id,
						t.manager_email_address,
						t.manager_employee_id,
						t.DAY,
						t.end_datetime,
						t.begin_datetime,
						t.difference,
						t.total_daily_hours,
						t.employee_manager_name,
						t.employee_name
					FROM
						(
							SELECT
								eh.employee_id,
								e1.name_full as employee_name,
								e.name_full as employee_manager_name,
								e.email_address AS manager_email_address,
								e.id AS manager_employee_id,
								EXTRACT ( DAY
							FROM
								DATE ( DATE ) ) AS DAY,
								eh.end_datetime,
								eh.begin_datetime,
								( eh.end_datetime - eh.begin_datetime )::TIME AS Difference,
								sum ( ( eh.end_datetime - eh.begin_datetime )::TIME ) OVER ( PARTITION BY eh.employee_id, e.email_address, e.id, EXTRACT ( DAY
							FROM
								DATE ( DATE ) ) ) AS total_daily_hours
							FROM
								employee_hours eh
								LEFT JOIN employees e1 ON ( eh.employee_id = e1.id )
								LEFT JOIN employees e ON ( e1.reporting_manager_id = e.id )
								LEFT JOIN employee_addresses ea1 ON ( eh.employee_id = ea1.employee_id AND ea1.address_type_id = 1 )
							WHERE
								DATE BETWEEN \'' . $strBeginDate . '\'
								AND \'' . $strEndDate . '\'
								AND EXTRACT ( DAY
							FROM
								DATE ( DATE ) ) = ' . ( int ) $intPreviousDate . '
								AND ea1.country_code = \'' . CCountry::CODE_USA . '\'
						) t
					WHERE
						t.total_daily_hours >= \' 12:00:00 \'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompleteEmployeeHoursByDateRange( $strBeginDate, $strEndDate, $objDatabase ) {

		if( false == valStr( $strBeginDate ) || false == valStr( $strEndDate ) ) return NULL;

		$strSql = 'SELECT
						sub_query.employee_name,
						sub_query.manager_name,
						sub_query.department_name,
						sub_query.salary_type,
						sub_query.clock_in_date,
						sub_query.is_manually_added,
						sub_query.is_manager_approved,
						sub_query.total_time
					FROM
						(
							SELECT
								employee_id,
								manager_email_address,
								manager_employee_id,
								clock_in_date,
								employee_name,
								salary_type,
								department_name,
								manager_name,
								is_manually_added,
								is_manager_approved,
								total_time
							FROM
								(
									SELECT
										eh.employee_id,
										e1.name_full as employee_name,
										e.name_full AS manager_name,
										es.name as salary_type,
										d.name as department_name,
										e.email_address AS manager_email_address,
										e.id AS manager_employee_id,
										eh.end_datetime,
										eh.begin_datetime,
										eh.date as clock_in_date,
										to_char( ( EXTRACT( EPOCH FROM ( end_datetime - begin_datetime )::interval ) || \' second\' )::"interval", \'HH24:MI:SS\' ) AS total_time,
										eh.is_manually_added AS is_manually_added,
										eh.is_manager_approved AS is_manager_approved
									FROM
										employee_hours eh
										LEFT JOIN employees e ON ( e1.reporting_manager_id = e.id )
										LEFT JOIN employees e1 ON ( eh.employee_id = e1.id )
										LEFT JOIN departments d ON ( e1.department_id = d.id )
										LEFT JOIN employee_addresses ad ON ( ad.employee_id = e1.id AND ad.address_type_id = 1 )
										LEFT JOIN employee_salary_types es ON ( e1.employee_salary_type_id = es.id )
									WHERE
										DATE BETWEEN \'' . $strBeginDate . '\'
										AND \'' . $strEndDate . '\'
										AND ad.country_code = \'' . CCountry::CODE_USA . '\'
							) t
						GROUP BY
							employee_id,
							manager_email_address,
							manager_employee_id,
							clock_in_date,
							employee_name,
							salary_type,
							department_name,
							manager_name,
							is_manually_added,
							is_manager_approved,
							total_time
						) as sub_query
						WHERE
							sub_query.is_manually_added = 1
							AND sub_query.is_manager_approved = 0
						ORDER BY sub_query.clock_in_date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestEmployeeHourWithNoEndDateTimeByEmployeeId( $intEmployeeId, $objDatabase, $boolManuallyAdded = false ) {

		$strSql = ' SELECT
							*
					FROM
							employee_hours
					WHERE
							employee_id = ' . ( int ) $intEmployeeId . ' AND end_datetime IS NULL';

		$strSql .= ( false == $boolManuallyAdded ) ? ' AND is_manually_added = 0 ' : '';

		$strSql .= ' ORDER BY
							( CASE
								WHEN ( begin_datetime IS NOT NULL ) THEN begin_datetime
								ELSE end_datetime
							END ) DESC,
							id DESC
					 LIMIT 1';

		return self::fetchEmployeeHour( $strSql, $objDatabase );
	}

	public static function fetchEmployeesAverageOnFloorHoursByEmployeeId( $strCountryCode, $intEmployeeId = NULL, $strFromDate, $strToDate, $objDatabase, $boolIsDailyTotalOnFloorHours = false ) {

		$strWhereClause .= ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND s.response_datetime > \'' . $strFromDate . '\' AND s.response_datetime < \'' . $strToDate . '\'': '';

		$intDayOfWeekSaturday 	= 6;
		$intDayOfWeekSunday 	= 0;

		$strEmployeeIdCondition = ( true == is_numeric( $intEmployeeId ) ) ? ' AND eh.employee_id = ' . $intEmployeeId : '';

		$strSql = 'SELECT
						DISTINCT eh.employee_id,
						eh.date,
						SUM ( eh.end_datetime - eh.begin_datetime ) OVER ( PARTITION BY eh.date, eh.employee_id ) AS daily_total_on_floor_hours
					FROM
						employee_hours eh
						JOIN employee_addresses AS ea ON ( eh.employee_id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\' AND ea.country_code = \'' . $strCountryCode . '\' AND eh.employee_hour_type_id = \'' . CEmployeeHourType::FLOOR . '\' )
					WHERE
						eh.date BETWEEN \'' . $strFromDate . '\' AND \'' . $strToDate . '\'
						AND eh.date NOT IN ( SELECT date FROM paid_holidays WHERE country_code = \'' . $strCountryCode . '\' AND is_optional IS FALSE AND date BETWEEN \'' . $strFromDate . '\' AND \'' . $strToDate . '\' )
						AND EXTRACT ( \'dow\' FROM eh.date ) NOT IN (' . ( int ) $intDayOfWeekSaturday . ', ' . ( int ) $intDayOfWeekSunday . ' )
						' . $strEmployeeIdCondition . '
					ORDER BY
						eh.date';

		if( false == $boolIsDailyTotalOnFloorHours ) {
			$strSql = 'SELECT
							to_char ( avg ( employee_hours_data.daily_total_on_floor_hours ), \'HH24:MI\' ) AS average_on_floor_hours
						FROM
							( ' . $strSql . ' ) AS employee_hours_data';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMissedEmployeeHoursReportByManagerEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = ' WITH RECURSIVE all_employees( employee_id, depth ) AS (
					SELECT
						DISTINCT e.id AS employee_id,
						1
					FROM
						employees e
					WHERE
						e.reporting_manager_id = ' . ( int ) $intEmployeeId . '
					UNION ALL
					SELECT
						DISTINCT e.id AS employee_id,
						al.depth + 1
					FROM
						employees e,
						all_employees al
					WHERE
						e.reporting_manager_id = al.employee_id )
					SELECT
						employee_hour_id,
						employee_id,
						manager_email_address,
						manager_employee_id,
						clock_in_date,
						employee_name,
						manager_name,
						is_manually_added,
						is_manager_approved,
						total_time,
						end_datetime,
						begin_datetime,
						proposed_time
					FROM
						(
							SELECT
								eh.id as employee_hour_id,
								eh.employee_id,
								e1.name_full as employee_name,
								e.name_full AS manager_name,
								e.email_address AS manager_email_address,
								e.id AS manager_employee_id,
								eh.end_datetime,
								eh.begin_datetime,
								eh.date as clock_in_date,
								to_char( ( EXTRACT( EPOCH FROM ( end_datetime - begin_datetime )::interval ) || \' second\' )::"interval", \'HH24:MI:SS\' ) AS total_time,
								eh.is_manually_added AS is_manually_added,
								eh.is_manager_approved AS is_manager_approved,
								eh.proposed_time as proposed_time
							FROM
								employee_hours eh
								LEFT JOIN employees e2 ON ( e2.id = eh.employee_id )
								LEFT JOIN employees e ON ( e2.reporting_manager_id = e.id )
								LEFT JOIN all_employees al ON ( eh.employee_id = al.employee_id )
								LEFT JOIN employees e1 ON ( al.employee_id = e1.id )
								LEFT JOIN departments d ON ( e1.department_id = d.id )
								LEFT JOIN employee_addresses ad ON ( ad.employee_id = al.employee_id AND ad.address_type_id = 1 )
								LEFT JOIN employee_salary_types es ON ( e1.employee_salary_type_id = es.id )
							WHERE
								( ( eh.is_manually_added = 1 AND eh.is_manager_approved = 0 ) OR eh.end_datetime IS NULL )
								AND ad.country_code = \'' . CCountry::CODE_USA . '\'
								AND al.depth <= 2
							) t
					GROUP BY
						employee_id,
						manager_email_address,
						manager_employee_id,
						clock_in_date,
						employee_name,
						manager_name,
						is_manually_added,
						is_manager_approved,
						total_time,
						end_datetime,
						begin_datetime,
						proposed_time,
						employee_hour_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMissedEmployeeHoursCountReportByManagerEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'WITH RECURSIVE all_employees( employee_id, depth ) AS (
					SELECT
						DISTINCT e.id AS employee_id,
						1
					FROM
						employees e
					WHERE
						e.reporting_manager_id = ' . ( int ) $intEmployeeId . '
					UNION ALL
					SELECT
						DISTINCT e.id AS employee_id,
						al.depth + 1
					FROM
						employees e,
						all_employees al
					WHERE
						e.reporting_manager_id = al.employee_id )
					SELECT
						count( DISTINCT eh.employee_id )
					FROM
						employee_hours eh
						LEFT JOIN employees e2 ON ( e2.id = eh.employee_id )
						LEFT JOIN employees e ON ( e2.reporting_manager_id = e.id )
						LEFT JOIN all_employees al ON ( eh.employee_id = al.employee_id )
						LEFT JOIN employees e1 ON ( eh.employee_id = al.employee_id )
						LEFT JOIN departments d ON ( e1.department_id = d.id )
						LEFT JOIN employee_addresses ad ON ( ad.employee_id = al.employee_id AND ad.address_type_id = 1 )
						LEFT JOIN employee_salary_types es ON ( e1.employee_salary_type_id = es.id )
					WHERE
						( ( eh.is_manually_added = 1 AND eh.is_manager_approved = 0 ) OR eh.end_datetime IS NULL )
						AND ad.country_code = \'' . CCountry::CODE_USA . '\'
						AND al.depth <= 2';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
	}

	public static function fetchEmployeeHoursByEmployeeIdByBeginDateTime( $intEmployeeId, $strBeginDateTimeFrom, $strBeginDateTimeTo, $objAdminDatabase ) {
		if( false == valId( $intEmployeeId ) || false == valStr( $strBeginDateTimeFrom ) || false == valStr( $strBeginDateTimeTo ) ) {
			return false;
		}
		$strSql = ' SELECT
						*
					FROM
						employee_hours
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . ' 
						AND begin_datetime BETWEEN \'' . $strBeginDateTimeFrom . '\' AND \'' . $strBeginDateTimeTo . '\'';

		return self::fetchEmployeeHours( $strSql, $objAdminDatabase );

	}

	public static function fetchEmployeeHoursByIds( $arrintEmployeeHourIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeHourIds ) ) return [];

		$strSql = 'SELECT * FROM employee_hours WHERE id IN ( ' . implode( ',', $arrintEmployeeHourIds ) . ' )';

		return self::fetchEmployeeHours( $strSql, $objDatabase );
	}

    public static function fetchMissingSwipesAndEmployeesHoursByDate( $strBeginDate, $objDatabase ) {

        if( false == valStr( $strBeginDate ) ) {
            return NULL;
        }

        $strSql = 'SELECT
                      emp.id,
                      vacation.begin_datetime AS begin_leave,
                      vacation.end_datetime AS end_leave,
                      shift.office_start_time,
                      (
                        SELECT
                          to_char( ( EXTRACT( EPOCH FROM SUM( hours.end_datetime - hours.begin_datetime )::interval ) || \' second\' )::"interval", \'HH24:MI:SS\' )
                        FROM
                          employee_hours AS hours
                        WHERE
                          hours.date = \'' . $strBeginDate . '\'
                        AND emp.id = hours.employee_id
                        GROUP BY
                            hours.date, hours.employee_id
                      ) AS total_daily_hours,
                      (
                        SELECT
                            DISTINCT EXTRACT ( DAY FROM ( DATE ) ) AS day
                        FROM
                            employee_hours
                        WHERE
                            emp.id = employee_id
                        AND date = \'' . $strBeginDate . '\'
                        AND ( end_datetime IS NULL
                        OR begin_datetime IS NULL )
                      ) AS missing_swipe
                  FROM
                    employees AS emp
                  JOIN employee_addresses as emp_address ON (emp.id = emp_address.employee_id AND ' . CAddressType::PRIMARY . ' = emp_address.address_type_id AND \'IN\' = emp_address.country_code)
                  LEFT JOIN employee_vacation_requests AS vacation ON (emp.id = vacation.employee_id AND begin_datetime <= \'' . $strBeginDate . ' 23:59:59\' AND end_datetime >= \'' . $strBeginDate . ' 00:00:00\')
                  LEFT JOIN office_shift_employees AS shift ON (emp.id = shift.employee_id AND shift.start_date = \'' . $strBeginDate . '\')
                  WHERE
                    ( emp.date_terminated IS NULL OR NOW() < emp.date_terminated )';

	    return fetchData( $strSql, $objDatabase );
    }

}

?>