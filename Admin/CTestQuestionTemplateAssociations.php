<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestionTemplateAssociations
 * Do not add any new functions to this class.
 */

class CTestQuestionTemplateAssociations extends CBaseTestQuestionTemplateAssociations {

	public static function fetchTestQuestionTemplateAssociationCountByTestId( $intTestId, $objDatabase ) {
		return self::fetchTestQuestionTemplateAssociationCount( 'WHERE test_id = ' . ( int ) $intTestId, $objDatabase );
	}

	public static function fetchTestQuestionAssociationTemplateIds( $objDatabase ) {
		$arrintResponse = array_keys( rekeyArray( 'test_template_id', fetchData( 'SELECT DISTINCT test_template_id FROM test_question_template_associations', $objDatabase ) ) );

		if( true == isset( $arrintResponse ) ) return $arrintResponse;
		return NULL;
	}

	public static function fetchAssociatedTestTeplateNameByTestId( $intTestId, $objDatabase ) {
		$strSql = 'SELECT
						tt.id,
						tt.name
					FROM
						test_templates tt
						JOIN test_question_template_associations tqta ON( tt.id = tqta.test_template_id )
					WHERE
						tqta.test_id =' . ( int ) $intTestId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssociatedTestTemplateDetailsByTestId( $intTestId, $objDatabase ) {
		$strSql = 'SELECT
						tt.id as test_template_id,
						tt.test_level_type_id,
						tqta.no_of_questions
					FROM
						test_templates tt
						LEFT JOIN test_question_template_associations tqta ON ( tt.id = tqta.test_template_id )
					WHERE
						tqta.test_id =' . ( int ) $intTestId;

		return fetchData( $strSql, $objDatabase );
	}
}
?>
