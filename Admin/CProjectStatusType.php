<?php

class CProjectStatusType extends CBaseProjectStatusType {
	const DISCOVERY			= 1;
	const STRATEGY_READY	= 2;
	const IN_STRATEGY		= 3;
	const COMPLETED			= 4;
	const DEV_READY			= 5;

	public static $c_arrmixProjectStatuses = [
		self::DISCOVERY      => 'Discovery',
		self::STRATEGY_READY => 'Strategy Ready',
		self::IN_STRATEGY    => 'In Strategy',
		self::DEV_READY      => 'Dev Ready',
		self::COMPLETED      => 'Completed'
	];

	public static $c_arrintProjectStatusesForSettingRank = [ self::STRATEGY_READY, self::IN_STRATEGY, self::DEV_READY ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>