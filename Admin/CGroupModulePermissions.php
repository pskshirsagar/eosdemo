<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupModulePermissions
 * Do not add any new functions to this class.
 */

class CGroupModulePermissions extends CBaseGroupModulePermissions {

	public static function fetchGroupModulePermissionsByUserId( $intUserId, $objDatabase ) {

		$strSql = 'SELECT
						gemp.module_id,
						max( gemp.has_read_access ) AS has_read_access,
						max( gemp.has_write_access) AS has_write_access
					FROM
						group_module_permissions gemp
						JOIN user_groups ug ON ( ug.group_id = gemp.group_id AND ug.user_id =  ' . ( int ) $intUserId . ' AND ug.deleted_by IS NULL )
					GROUP BY
						gemp.module_id';

		return self::fetchGroupModulePermissions( $strSql, $objDatabase );
	}

}
?>