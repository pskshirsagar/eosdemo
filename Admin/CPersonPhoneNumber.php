<?php

class CPersonPhoneNumber extends CBasePersonPhoneNumber {

	const MAX_NUMBER_OF_PHONE_NUMBERS = 6;

	public function valPhoneNumber() {
		$boolIsValid = true;

		if( !valStr( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
		}

		if( valStr( $this->getPhoneNumber() ) && !valId( $this->getPhoneNumberTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number type is not associated with phone number ' . $this->getPhoneNumber() . '.' ) );
		}

		if( valStr( $this->getPhoneNumber() ) && !$this->validatePhoneNumber() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Invalid phone number:' . $this->getPhoneNumber() . '.' ) );
		}

		return $boolIsValid;
	}

	public function valOutageNotificationStatus( $arrintPersonContactTypes ) {
		$boolIsValid = true;

		if( in_array( CPersonContactType::OUTAGE_SMS, $arrintPersonContactTypes ) && CPhoneNumberType::MOBILE == $this->getPhoneNumberTypeId() && !valStr( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Mobile number required for opting Outage SMS. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrintPersonContactTypes = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				$boolIsValid &= $this->valPhoneNumber();

				if( valArr( $arrintPersonContactTypes ) ) {
					$boolIsValid &= $this->valOutageNotificationStatus( $arrintPersonContactTypes );
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function validatePhoneNumber() {
		require_once PATH_PHP_LIBRARIES . 'Psi/Internationalization/PhoneNumber/CPhoneNumber.class.php';

		$arrmixOptions				= [];
		$arrmixOptions['extension']	= valStr( $this->getExtension() ) ? $this->getExtension() : NULL;

		$objPhoneNumber = new \i18n\CPhoneNumber( $this->getPhoneNumber(), $arrmixOptions );

		return $objPhoneNumber->isValid();
	}

	public function validatePhoneNumbers( $arrobjPersonPhoneNumbers, $intCompanyStatusTypeId ) {

		$boolIsValid = true;
		$boolIsMobileNumber = false;
		if( true == valArr( $arrobjPersonPhoneNumbers ) ) {
			foreach( $arrobjPersonPhoneNumbers as $objPersonPhoneNumber ) {

				if( CPhoneNumberType::MOBILE == $objPersonPhoneNumber->getPhoneNumberTypeId() ) {
					$boolIsMobileNumber = true;
					if( false == $objPersonPhoneNumber->validate( VALIDATE_INSERT ) ) {
						$this->addErrorMsgs( $objPersonPhoneNumber->getErrorMsgs() );
						$boolIsValid = false;
					}
				} elseif( false == $objPersonPhoneNumber->validate( VALIDATE_INSERT ) ) {
					$this->addErrorMsgs( $objPersonPhoneNumber->getErrorMsgs() );
					$boolIsValid = false;
				}
			}
		}

		if( false == $boolIsMobileNumber && CCompanyStatusType::CLIENT == $intCompanyStatusTypeId ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number2', 'Mobile number is required. ' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>