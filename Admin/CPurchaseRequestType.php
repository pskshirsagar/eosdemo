<?php

class CPurchaseRequestType extends CBasePurchaseRequestType {

	const NEW_HIRE					= 1;
	const COMP_CHANGE				= 2;
	const EMPLOYEE_BONUS			= 3;
	const ASSET_PURCHASE			= 5;
	const TRAVEL        			= 6;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public static $c_arrintPurchaseRequestTypeIds = [
		self::NEW_HIRE,
		self::COMP_CHANGE,
		self::EMPLOYEE_BONUS,
		self::ASSET_PURCHASE
	];

	// This is related to new hire custom types.
	public static $c_arrstrCustomNewHirePrTypes = [
		't' => 'Internal Hire',
		'f' => 'External Hire'
	];

	public static function loadTemplateConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['NEW_HIRE']		    = self::NEW_HIRE;
		$arrmixTemplateParameters['COMP_CHANGE']		= self::COMP_CHANGE;
		$arrmixTemplateParameters['EMPLOYEE_BONUS']	    = self::EMPLOYEE_BONUS;
		$arrmixTemplateParameters['ASSET_PURCHASE']	    = self::ASSET_PURCHASE;

		return $arrmixTemplateParameters;
	}

}
?>