<?php

class CCategory extends CBaseCategory {

	const RATE_THE_PROFILE				= 1;
	const RATE_THE_COMMUNICATION		= 2;
	const FLAGS							= 3;
	const POOR_PROFILE					= 4;
	const AVERAGE_PROFILE				= 5;
	const GOOD_PROFILE_MATCH			= 6;
	const VERY_GOOD_PROFILE_MATCH		= 7;
	const EXCELLENT_PROFILE_MATCH		= 8;
	const POOR_COMMUNICATIONS			= 9;
	const AVERAGE_COMMUNICATIONS		= 10;
	const GOOD_PROILE_COMMUNICATIONS	= 11;
	const VERY_GOOD_COMMUNICATIONS		= 12;
	const EXCELLENT_COMMUNICATIONS		= 13;
	const HIGH_CURRENT_SALARY			= 14;
	const UNDERPAID						= 15;
	const EXPECTATIONS_VERY_HIGH		= 16;
	const STAYS_FAR						= 17;
	const STAYS_VERY_NEAR				= 18;
	const NOT_INTERESTED				= 19;
	const NOT_LOOKING_FOR_A_CHANGE		= 20;
	const NOT_READY_TO_RELOCATE_TO_PUNE	= 21;
	const READY_TO_RELOCATED_TO_PUNE	= 22;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCategoryTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>