<?php

class CPersonRole extends CBasePersonRole {

	const CEO								= 1;
	const PRESIDENT							= 2;
	const CFO								= 3;
	const MANAGING_DIRECTOR					= 4;
	const CONTROLLER						= 5;
	const OWNER_PARTNER_PRINCIPAL			= 6;
	const SENIOR_VICE_PRESIDENT				= 7;
	const VICE_PRESIDENT					= 8;
	const REGIONAL							= 9;
	const ASSET_MANAGER						= 10;
	const MARKETING_DIRECTOR				= 11;
	const OPERATIONS_DIRECTOR_MANAGER		= 12;
	const CIO								= 13;
	const IT_MANAGER						= 14;
	const ACCOUNTANT						= 15;
	const PROJECT_MANAGER					= 16;
	const PROPERTY_MANAGER					= 17;
	const ASSIS_PROPERTY_MANAGER			= 18;
	const LEASING_AGENT						= 19;
	const DIRECTOR_OF_PROPERTY_MANAGEMENT	= 20;
	const DIRECTOR_OF_ACCOUNTING			= 21;
	const OTHER								= 22;
	const COO								= 23;
	const MAINTENANCE						= 24;
	const SEO_CONTACT						= 25;

	public static $c_arrintAccountPlanPersonRoleIds = [
		self::CEO,
		self::PRESIDENT,
		self::CFO,
		self::MANAGING_DIRECTOR,
		self::OWNER_PARTNER_PRINCIPAL,
		self::SENIOR_VICE_PRESIDENT,
		self::VICE_PRESIDENT,
		self::REGIONAL,
		self::MARKETING_DIRECTOR,
		self::OPERATIONS_DIRECTOR_MANAGER,
		self::CIO,
		self::DIRECTOR_OF_PROPERTY_MANAGEMENT,
		self::DIRECTOR_OF_ACCOUNTING,
		self::OTHER
	];

	public static $c_arrintCorporateRoles = [
		self::CEO,
		self::PRESIDENT,
		self::CFO,
		self::MANAGING_DIRECTOR,
		self::CONTROLLER,
		self::OWNER_PARTNER_PRINCIPAL,
		self::SENIOR_VICE_PRESIDENT,
		self::VICE_PRESIDENT,
		self::REGIONAL,
		self::ASSET_MANAGER,
		self::MARKETING_DIRECTOR,
		self::OPERATIONS_DIRECTOR_MANAGER,
		self::CIO,
		self::IT_MANAGER,
		self::ACCOUNTANT,
		self::PROJECT_MANAGER,
		self::DIRECTOR_OF_PROPERTY_MANAGEMENT,
		self::DIRECTOR_OF_ACCOUNTING,
		self::COO
	];

	public static $c_arrintPropertyRoles = [
		CPersonRole::PROPERTY_MANAGER,
		CPersonRole::ASSIS_PROPERTY_MANAGER,
		CPersonRole::LEASING_AGENT,
		CPersonRole::OTHER,
		CPersonRole::MAINTENANCE
	];

	public static $c_arrstrAllPersonRoles = [
		self::CEO								=> 'CEO',
		self::PRESIDENT							=> 'President',
		self::CFO								=> 'CFO',
		self::MANAGING_DIRECTOR					=> 'Managing Director',
		self::CONTROLLER						=> 'Controller',
		self::OWNER_PARTNER_PRINCIPAL			=> 'Owner/Partner/Principal',
		self::SENIOR_VICE_PRESIDENT				=> 'Senior Vice President',
		self::VICE_PRESIDENT					=> 'Vice President',
		self::REGIONAL							=> 'Regional',
		self::ASSET_MANAGER						=> 'Asset Manager',
		self::MARKETING_DIRECTOR				=> 'Marketing Director',
		self::OPERATIONS_DIRECTOR_MANAGER		=> 'Operations Director/Manager',
		self::CIO								=> 'CIO',
		self::IT_MANAGER						=> 'IT Manager',
		self::ACCOUNTANT						=> 'Accountant',
		self::PROJECT_MANAGER					=> 'Project Manager',
		self::PROPERTY_MANAGER					=> 'Property Manager',
		self::ASSIS_PROPERTY_MANAGER			=> 'Assistant Property Manager',
		self::LEASING_AGENT						=> 'Leasing Agent',
		self::DIRECTOR_OF_PROPERTY_MANAGEMENT	=> 'Director Of Property Management',
		self::DIRECTOR_OF_ACCOUNTING			=> 'Director Of Accounting',
		self::OTHER								=> 'Other',
		self::COO								=> 'COO',
		self::MAINTENANCE						=> 'Maintenance'
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>