<?php

class CContractEmployeeType extends CBaseContractEmployeeType {

	const CLIENT_EXECUTIVE	= 1;
	const IMPLEMENTATION	= 2;
	const FARMER			= 3;
	const RESPONSIBLE		= 5;
	const TRAINER			= 6;
	const SALES_ENGINEER	= 7;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>