<?php

class CTransportRequestStatus extends CBaseTransportRequestStatus {

	const UNUSED		= 1;
	const ON_HOLD		= 2;
	const APPROVED		= 3;
	const MODIFIED		= 4;
	const CANCELLED		= 5;
	const DISCONTINUED	= 6;
	const REVIEWING_DISCONTINUATION = 7;

	public static $c_arrintTransportRequestActiveStatusTypes = [
		self::UNUSED,
		self::ON_HOLD,
		self::APPROVED,
		self::MODIFIED,
		self::REVIEWING_DISCONTINUATION
	];

	public static $c_arrstrAdminRequestStatus = [
		self::APPROVED 	=> 'Approved',
		self::ON_HOLD 	=> 'On Hold',
		self::CANCELLED => 'Cancelled'
	];

	public static $c_arrintInactiveRequestStatus = [
		self::UNUSED,
		self::ON_HOLD,
		self::CANCELLED
	];

	public static $c_arrintActiveRequestStatuses = [
		self::APPROVED,
		self::REVIEWING_DISCONTINUATION,
		self::MODIFIED
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>