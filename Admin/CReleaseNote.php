<?php

class CReleaseNote extends CBaseReleaseNote {

	const DATE_OLD_RELEASE_NOTE		= '2017-12-06';

	protected $m_strReleaseNoteTypeName;
	protected $m_strKeyContactEmployeeName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReleaseNoteTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intReleaseNoteTypeId ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'release_note_type_id', 'Release note type is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intEmployeeId ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', ' Key Contact is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valPathway() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstruction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( false == valStr( $this->getTitle() ) || 0 == \Psi\CStringService::singleton()->strlen( \Psi\CStringService::singleton()->preg_replace( '/\s|&nbsp;/', '', $this->getTitle() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', ' Title is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( false == valStr( $this->getDescription() ) || 0 == \Psi\CStringService::singleton()->strlen( \Psi\CStringService::singleton()->preg_replace( '/\s|&nbsp;/', '', $this->getDescription() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', ' Description is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valAction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInternalNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function getreleaseNoteTypename() {
		return $this->m_strReleaseNoteTypeName;
	}

	public function getKeyContactEmployeeName() {
		return $this->m_strKeyContactEmployeeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['release_note_type_name'] ) ) 							$this->setReleaseNoteTypeName( $arrmixValues['release_note_type_name'] );
		if( true == isset( $arrmixValues['key_contact_employee_name'] ) ) 						$this->setKeyContactEmployeeName( $arrmixValues['key_contact_employee_name'] );
	}

	public function setReleaseNoteTypeName( $strReleaseNoteTypeName ) {
		$this->m_strReleaseNoteTypeName = $strReleaseNoteTypeName;
	}

	public function setKeyContactEmployeeName( $strKeyContactEmployeeName ) {
		$this->m_strKeyContactEmployeeName = $strKeyContactEmployeeName;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valReleaseNoteTypeId();
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			case 'product_management_release_note':
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valReleaseNoteTypeId();
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valDescription();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>