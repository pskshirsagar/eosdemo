<?php

class CPurchaseApproval extends CBasePurchaseApproval {

	protected $m_strEmployeeName;
	protected $m_strApprovalStatus;

	public function setEmployeeName( $strEmployeeName ) {

		$this->m_strEmployeeName = $strEmployeeName;
	}

	public function getEmployeeName() {

		return $this->m_strEmployeeName;
	}

	public function setApprovalStatus( $strApprovalStatus ) {

		$this->m_strApprovalStatus = $strApprovalStatus;
	}

	public function getApprovalStatus() {

		return $this->m_strApprovalStatus;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['employee_name'] ) ) $this->setEmployeeName( $arrmixValues['employee_name'] );
		if( true == isset( $arrmixValues['approval_status'] ) ) $this->setApprovalStatus( $arrmixValues['approval_status'] );
		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>