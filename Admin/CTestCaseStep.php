<?php

class CTestCaseStep extends CBaseTestCaseStep {
	use Psi\Libraries\EosFoundation\TEosStoredObject;

	protected $m_intCid;

	protected $m_strTitle;
	protected $m_strFileName;
	protected $m_strUrl;

	/**
	 * Get Functions
	 */

	public function getCid() {
		return $this->m_intCid;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	/**
	 * Set Functions
	 */

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	public function setUrl( $strUrl ) {
		$this->m_strUrl = $strUrl;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_id', 'Task Id is required' ) );
		}

		return $boolIsValid;
	}

	public function valStep() {
		$boolIsValid = true;

		if( true == is_null( $this->getStep() ) || 0 == strlen( trim( $this->getStep() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Step is required.', NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valResult() {
		$boolIsValid = true;

		if( true == is_null( $this->getResult() ) || 0 == strlen( trim( $this->getResult() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Result is required.', NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTaskId();
				$boolIsValid &= $this->valStep();
				$boolIsValid &= $this->valResult();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		return CClient::ID_DEFAULT . '/entrata_test/testcase_steps/' . date( 'Y/m/d' ) . '/' . $this->getId() . '/' . $this->getUrl();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {
		return CConfig::get( 'OSG_BUCKET_SYSTEM_DOCUMENTS' );
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( CClient::ID_DEFAULT, $this->getId(), self::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

}
?>