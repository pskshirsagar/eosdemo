<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEncryptionSystemTypes
 * Do not add any new functions to this class.
 */

class CEncryptionSystemTypes extends CBaseEncryptionSystemTypes {

	const EMPLOYEE_COMPENSATIONS = 1;
	const ANNUAL_PAY_REVIEW = 2;

	public static function fetchEncryptionSystemTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEncryptionSystemType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEncryptionSystemType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEncryptionSystemType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllEncryptionSystemTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						encryption_system_types
					ORDER BY
						name';

		return self::fetchEncryptionSystemTypes( $strSql, $objDatabase );
	}

}
?>