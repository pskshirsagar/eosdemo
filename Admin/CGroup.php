<?php

class CGroup extends CBaseGroup {

	const DEVELOPMENT						= 1;
	const SUPPORT							= 2;
	const SALES								= 3;
	const MERCHANT							= 4;
	const ACCOUNTING						= 5;
	const HR								= 7;
	const QA								= 8;
	const VACANCY							= 9;
	const INSURANCE							= 10;
	const UTILITY_BILLING					= 11;
	const IT 								= 12;
	const MARKETING							= 14;
	const LEGAL								= 16;
	const UX								= 17;
	const REVENUE_MANAGEMENT				= 18;
	const SALES_ADMIN						= 19;
	const PRODUCT_MANAGERS					= 20;
	const TRAINING							= 21;
	const PHP_TPM							= 22;
	const PHP_TL							= 23;
	const PHP_ATL							= 24;
	const INTERVIEWERS						= 49;
	const WEBSITE_TEMPLATES					= 66;
	const WEBSITE_TEMPLATES_DEV				= 1350;
	const QA_TL								= 72;
	const DBA								= 78;
	const SECURITY_TEAM_STAKEHOLDERS 		= 142;
	const XPLODE							= 190;
	const HR_MANAGER						= 30;
	const TECHNICAL_WRITERS					= 249;
	const TEST_AUTOMATION	 				= 260;
	const US_RECRUITER 						= 261;
	const WEBSITE_TEMPLATE_SLOTS			= 285;
	const SALES_MANAGEMENT					= 297;
	const STORY_POINTS_AUDITORS				= 352;
	const SECURITY_QUIZ_GRADERS				= 354;
	const INFORMATION_SECURITY				= 519;
	const QAM								= 608;
	const IMPLEMENTATION_PORTAL_SETUP		= 679;
	const KNOWLEDGE_BASE_CONTRIBUTOR 		= 52;
	const PHP_ENGINEERS						= 422;
	const IOS_ENGINEERS						= 424;
	const DOTNET_ENGINEERS					= 428;
	const CONTRACT_RENEWALS					= 792;
	const DIRECTORS_OF_DEVELOPMENT			= 797;
	const LIVE_IT							= 735;
	const DEPLOY							= 436;
	const INDIA_RECRUITMENT					= 840;
	const QA_MENTOR							= 890;
	const AMEX_ASSISTED						= 938;
	const ADMIN 							= 606;
	const DEPLOY_ECS						= 835;
	const ANNUAL_REVIEW_MANAGERS			= 1203;
	const INDIAN_REIMBURSEMENT_ACCOUNTANTS	= 1285;
	const INDIAN_INTERNET_USERS 			= 1286;
	const OUTAGE_ALERT_SMS_INDIA			= 1298;
	const ARCHITECTS_US						= 1216;
	const DIRECTORS_US						= 1217;
	const PRINCIPAL_ENGINEERS				= 530;
	const TECHNICAL_MANAGER					= 25;
	const DIRECTORS							= 439;
	const DIGITAL_MARKETING_CONSULTING		= 1901;

	const DBA_SUPPORT 								= 1313;
	const DEVOPS_SUPPORT 							= 1314;
	const IT_SUPPORT 								= 1315;
	const ACCOUNTING_FULL_ACCESS 					= 1340;
	const SUSPICIOUS_ACTIVITY_SUPPORT 				= 1582;
	const DEVOPS_CAB_SUPPORT 						= 1390;
	const DBA_CAB_SUPPORT 							= 1389;
	const IT_CAB_SUPPORT 							= 1388;
	const INFORMATION_SECURITY_SUPPORT 				= 1712;
	const RESIDENT_VERIFY_SCREENING_PACKAGE_GROUP 	= 1751;
	const ORDER_FORMS 								= 1797;
	const CONTRACT_DRAFTS 							= 1801;
	const CONTRACT_PACKAGE_DISCOUNT_APPROVALS		= 2058;
	const SIMPLIMENTATION_PORTAL_ENTRATA_ACCESS		= 1812;
	const INTERNATIONALIZATION_LANGUAGE_SETTING		= 1813;
	const MIGRATION_WORKGROUP						= 1817;
	const MIGRATION_BACK_OFFICE						= 1940;
	const SIMPLIMENTATION_PORTAL_PRIMARY_USERS		= 1820;
	const RVC_ADMIN_USERS							= 1844;
	const XENTO_TRAINING_CONTENT_MANAGERS			= 1851;
	const US_PR_APPROVERS                           = 1862;
	const US_PR_EMAIL_RECEIVERS                     = 1863;
	const MILITARY_APPROVED_CITIZENS                = 1907;

	const LMS_FACILITATOR							= 1908;
	const ALLOW_SEND_RESIDENT_MAIL                  = 1957;
	const SVP_LIST_ATS								= 1968;
	const LEARNING_CENTER							= 2002;
	const ALLOW_SCRAP_IT_ASSET						= 2031;
	const CONTRACT_SYSTEM_FULL_ACCESS				= 2029;

	const UNIVERSAL_GROUP 							= 2032;
	const LEARNING_CENTER_TRAINERS					= 2046;
	const PRODUCT_MANAGEMENT_TOOL                   = 2042;
	const MASS_EMAIL								= 2060;
	const XENTO_TAX_REVIEW_MANAGER					= 2050;
	const ALLOW_TEAM_MEMBER_ACCESS					= 2063;
	const CCPA_USERS                                = 2067;

	public static $c_arrintHelpdeskGroups	= array(
		self::DBA_SUPPORT				    => 'DBA Support',
		self::DEVOPS_SUPPORT			    => 'Devops Support',
		self::IT_SUPPORT				    => 'IT Support',
		self::INFORMATION_SECURITY_SUPPORT  => 'Information Security Support'
	);

	public static $c_arrintHelpdeskCABGroups	= array(
		self::DBA_CAB_SUPPORT				=> 'DBA CAB Support',
		self::DEVOPS_CAB_SUPPORT			=> 'Devops CAB Support',
		self::IT_CAB_SUPPORT				=> 'IT CAB Support'
	);

	public static $c_arrintAssociatedHelpdeskCABGroup	= array(
		self::DBA_SUPPORT				=> self::DBA_CAB_SUPPORT,
		self::DEVOPS_SUPPORT			=> self::DEVOPS_CAB_SUPPORT,
		self::IT_SUPPORT				=> self::IT_CAB_SUPPORT
	);

	public static $c_arrintTotalHelpdeskGroups	= array(
		self::DBA_SUPPORT				=> 'DBA Support',
		self::DEVOPS_SUPPORT			=> 'Devops Support',
		self::IT_SUPPORT				=> 'IT Support',
		self::DBA_CAB_SUPPORT			=> 'DBA CAB Support',
		self::DEVOPS_CAB_SUPPORT		=> 'Devops CAB Support',
		self::IT_CAB_SUPPORT			=> 'IT CAB Support',
		self::INFORMATION_SECURITY_SUPPORT  => 'Information Security Support'
	);

	// Array of groups which are having add, edit and delete permissions for folders files owner associations module.
	public static $c_arrintFolderFileOwnerAssociationsUserGroupIds = array(
		self::ARCHITECTS_US,
		self::DIRECTORS_US,
		self::PHP_ATL,
		self::PHP_TL,
		self::PHP_TPM,
		self::PRINCIPAL_ENGINEERS,
		self::QAM,
		self::QA_MENTOR,
		self::QA_TL,
		self::SECURITY_TEAM_STAKEHOLDERS,
		self::TECHNICAL_MANAGER
	);

	// Array of groups which are having access to approve/deny employee vacation requests.
	public static $c_arrintLeaveAccessUserGroupIds = array(
		self::QAM,
		self::PHP_TPM
	);

	protected $m_arrobjUsers;
	protected $m_arrobjUserGroups;
	protected $m_arrobjGroupPermissions;
	protected $m_intAssociatedUsersCount;
	protected $m_boolIsActiveDirectory;
	private $m_strLdapOu;

	/**
	 * Create Functions
	 */

	public function createUserGroup( $intUserId ) {

		$objUserGroup = new CUserGroup();
		$objUserGroup->setGroupId( $this->m_intId );
		$objUserGroup->setUserId( $intUserId );

		return $objUserGroup;
	}

	public function createDbWatchGroup( $intDbWatchId ) {

		$objDbWatchGroup = new CDbWatchGroup();
		$objDbWatchGroup->setGroupId( $this->m_intId );
		$objDbWatchGroup->setDbWatchId( $intDbWatchId );

		return $objDbWatchGroup;
	}

	public function createScriptGroup( $intScriptId ) {

		$objScriptGroup = new CScriptGroup();
		$objScriptGroup->setGroupId( $this->m_intId );
		$objScriptGroup->setScriptId( $intScriptId );

		return $objScriptGroup;
	}

	public function getAssociatedUsersCount() {
		return $this->m_intAssociatedUsersCount;
	}

	public function createGroupPermission( $intPsModuleId ) {

		$objGroupPermission = new CGroupPermission();
		$objGroupPermission->setGroupId( $this->getId() );
		$objGroupPermission->setPsModuleId( $intPsModuleId );

		return $objGroupPermission;
	}

	public function createGroupModulePermission( $intModuleId ) {

		$objGroupModulePermission = new CGroupModulePermission();
		$objGroupModulePermission->setGroupId( $this->getId() );
		$objGroupModulePermission->setModuleId( $intModuleId );

		return $objGroupModulePermission;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchUsers( $objDatabase ) {

		$this->m_arrobjUsers = CUsers::fetchUsersByGroupId( $this->m_intId, $objDatabase );

		return $this->m_arrobjUsers;
	}

	public function fetchUserGroups( $objDatabase ) {

		$this->m_arrobjUserGroups = CUserGroups::fetchUserGroupsByGroupId( $this->m_intId, $objDatabase );

		return $this->m_arrobjUserGroups;
	}

	public function fetchGroupPermissions( $objDatabase ) {

		$this->m_arrobjGroupPermissions = CGroupPermissions::fetchGroupPermissionsByGroupId( $this->m_intId, $objDatabase );

		return $this->m_arrobjGroupPermissions;
	}

	public function fetchDbWatchGroups( $objDatabase ) {

		return CDbWatchGroups::fetchDbWatchGroupsByGroupId( $this->m_intId, $objDatabase );
	}

	public function fetchScriptGroups( $objDatabase ) {

		return CScriptGroups::fetchScriptGroupsByGroupId( $this->m_intId, $objDatabase );
	}

	public function setGroupSequence( $objAdminDatabase ) {
		if( false == is_numeric( $this->getId() ) ) return NULL;

		$strSql = 'SELECT setval( \'groups_id_seq\', ' . ( int ) $this->getId() . ', FALSE )';
		return fetchData( $strSql, $objAdminDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == empty( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Group name is required. ' ) );
		} elseif( $this->m_strName != strip_tags( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_name', 'Valid group name is required. ' ) );
			return $boolIsValid;
		}

		if( true == $boolIsValid && true == isset( $objDatabase ) ) {

			$strSql 	= ' WHERE name ILIKE E' . $this->sqlName() . ' AND group_type_id = ' . $this->getGroupTypeId() . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$intCount 	= CGroups::fetchRowCount( $strSql, 'groups', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Group name ' . $this->m_strName . ' already exist. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valGroupTypeId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_intGroupTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'group_type_id', 'A valid group type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valLocation() {
		$boolIsValid = true;

		if( true == empty( $this->m_strLocation ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'location', 'Location is required.' ) );
		}
		return $boolIsValid;
	}

	public function valGroupName() {
		$boolIsValid = true;

		if( preg_match( '/^[a-zA-Z0-9_]*$/', $this->m_strName ) ) {
			$boolIsValid = true;
		} else {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Only characters, numbers and underscore( _ ) are allowed in group name.' ) );
		}
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( true == isset( $this->m_strEmailAddress ) && false == CValidation::validateEmailAddresses( \Psi\CStringService::singleton()->strtolower( $this->m_strEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'A valid email address is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		$intCount = CTestGroups::fetchTestGroupCount( 'WHERE group_id = ' . $this->getId(), $objDatabase );

		if( 0 != $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $this->m_strName . ' cannot be deleted because test group(s) depends on it.' ) );

		}

		return $boolIsValid;
	}

	public function valDependantGroupUsers( $objDatabase ) {

		$boolValid = true;

		$arrobjEnabledUsers = CUserGroups::fetchEnabledUsersByGroupId( $this->getId(), $objDatabase );

		if( true == valArr( $arrobjEnabledUsers ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $this->m_strName . ' group cannot be deleted because active users are associated with this group.' ) );

		}

		return $boolValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valEmailAddress();
			if( CGroupType::ECS == $this->m_intGroupTypeId ) {
				$boolIsValid &= $this->valGroupName();
				$boolIsValid &= $this->valLocation();
			}
				$boolIsValid &= $this->valGroupTypeId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				$boolIsValid &= $this->valDependantGroupUsers( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['associated_users_count'] ) )		$this->setAssociatedUsersCount( trim( $arrmixValues['associated_users_count'] ) );
		if( true == isset( $arrmixValues['is_active_directory'] ) ) {
			$this->setIsActiveDirectory( $arrmixValues['is_active_directory'] );
		} else {
			$boolIsActiveDirectoryGroup = ( false == is_null( $this->getActiveDirectoryGuid() ) ) ? true : false;
			$this->setIsActiveDirectory( $boolIsActiveDirectoryGroup );
		}

		return;
	}

	/**
	 * If you are making any changes in loadSmartyConstants function then
	 * please make sure the same changes would be applied to loadTemplateConstants function also.
	 */

	public static function loadSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'GROUP_DEVELOPMENT', 			self::DEVELOPMENT );
		$objSmarty->assign( 'GROUP_SUPPORT', 				self::SUPPORT );
		$objSmarty->assign( 'GROUP_SALES', 					self::SALES );
		$objSmarty->assign( 'GROUP_MERCHANT', 				self::MERCHANT );
		$objSmarty->assign( 'GROUP_ACCOUNTING', 			self::ACCOUNTING );
		$objSmarty->assign( 'GROUP_MARKETING', 				self::MARKETING );
		$objSmarty->assign( 'GROUP_HR', 					self::HR );
		$objSmarty->assign( 'GROUP_QA', 					self::QA );
		$objSmarty->assign( 'GROUP_VACANCY', 				self::VACANCY );
		$objSmarty->assign( 'GROUP_INSURANCE', 				self::INSURANCE );
		$objSmarty->assign( 'GROUP_UTILITY_BILLING', 		self::UTILITY_BILLING );
		$objSmarty->assign( 'GROUP_IT', 					self::IT );
		$objSmarty->assign( 'IT_SUPPORT', 					self::IT_SUPPORT );
		$objSmarty->assign( 'GROUP_SALES_ADMIN', 			self::SALES_ADMIN );
	}

	public static $c_arrintGroups = array(
		'GROUP_DEVELOPMENT'		=> self::DEVELOPMENT,
		'GROUP_SUPPORT'			=> self::SUPPORT,
		'GROUP_SALES'			=> self::SALES,
		'GROUP_MERCHANT'		=> self::MERCHANT,
		'GROUP_ACCOUNTING'		=> self::ACCOUNTING,
		'GROUP_MARKETING'		=> self::MARKETING,
		'GROUP_HR'				=> self::HR,
		'GROUP_QA'				=> self::QA,
		'GROUP_VACANCY'			=> self::VACANCY,
		'GROUP_INSURANCE'		=> self::INSURANCE,
		'GROUP_UTILITY_BILLING'	=> self::UTILITY_BILLING,
		'GROUP_IT'				=> self::IT,
		'GROUP_SALES_ADMIN'		=> self::SALES_ADMIN
	);

	public function setAssociatedUsersCount( $intAssociatedUsersCount ) {
		$this->m_intAssociatedUsersCount = $intAssociatedUsersCount;
	}

	public function setIsActiveDirectory( $boolIsActiveDirectory ) {
		$this->m_boolIsActiveDirectory = $boolIsActiveDirectory;
	}

	public function setLdapOu( $strLdapOu ) {
		$this->m_strLdapOu = $strLdapOu;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getIsActiveDirectory() {
		return $this->m_boolIsActiveDirectory;
	}

	public function getLdapOu() {
		return $this->m_strLdapOu;
	}

	/**
	 * Other Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == $boolReturnSqlOnly ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} elseif( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( ( 'production' == CONFIG_ENVIRONMENT || ( true == isset( $_COOKIE['LDAP_ENABLED'] ) ) ) && true == $this->getIsActiveDirectory() ) {
			require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdapGroup.class.php' );

			$objClientAdminLdapGroup = new CClientAdminLdapGroup();
			$boolIsValid 			 = $objClientAdminLdapGroup->syncToActiveDirectory( $this, $objDatabase );

			// Update Group only to update GUID
			if( true == $boolIsValid && false == is_null( $this->getActiveDirectoryGuid() ) ) {
				$boolIsValid 	= parent::update( $intCurrentUserId, $objDatabase );
				if( false == $boolIsValid ) {
					// To rollback from AD
					$objClientAdminLdapGroup->deleteGroupFormActiveDirectory( $this );
					return false;
				}
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ldap_error', $objClientAdminLdapGroup->getErrorMessage() . '. To resolve this either contact system administration team OR please uncheck Active Directory Group to unlink this group from Active Directory.' ) );
				return false;
			}
		}

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getIsActiveDirectory() ) {
			$this->setActiveDirectoryGuid( NULL );
		}

		if( true == $boolReturnSqlOnly ) {
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} elseif( false == parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( ( 'production' == CONFIG_ENVIRONMENT || ( true == isset( $_COOKIE['LDAP_ENABLED'] ) ) ) && true == $this->getIsActiveDirectory() ) {
			require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdapGroup.class.php' );

			$objClientAdminLdapGroup = new CClientAdminLdapGroup();

			$strActiveDirectoryGuid = $this->getActiveDirectoryGuid();

			try {
				$boolIsValid = $objClientAdminLdapGroup->syncToActiveDirectory( $this, $objDatabase );
				// Update Group only to update GUID
				if( true == $boolIsValid && false == is_null( $this->getActiveDirectoryGuid() ) ) {
					if( true == is_null( $strActiveDirectoryGuid ) ) {
						$boolIsValid = parent::update( $intCurrentUserId, $objDatabase );
					}
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ldap_error', $objClientAdminLdapGroup->getErrorMessage() . '. To resolve this either contact system administration team OR please uncheck Active Directory Group to unlink this group from Active Directory.' ) );

					return false;
				}
			} catch( \Exception $objException ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ldap_error', $objException->getMessage() ) );

				return false;
			}
		}

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		if( false == parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( ( 'production' == CONFIG_ENVIRONMENT || ( true == isset( $_COOKIE['LDAP_ENABLED'] ) ) ) && false == is_null( $this->getActiveDirectoryGuid() ) ) {
			require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdapGroup.class.php' );

			$objClientAdminLdapGroup 	= new CClientAdminLdapGroup();
			$boolIsValid = $objClientAdminLdapGroup->deleteGroupFormActiveDirectory( $this );
			if( false == $boolIsValid ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ldap_error', $objClientAdminLdapGroup->getErrorMessage() . '. To resolve this either contact system administration team OR please uncheck Active Directory Group to unlink this group from Active Directory.' ) );
				return false;
			}
		}

		return true;
	}

}

?>