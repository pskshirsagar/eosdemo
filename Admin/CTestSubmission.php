<?php

class CTestSubmission extends CBaseTestSubmission {

	const ZONE_GREEN		= 75;
	const TOTAL_PERCENTAGE 	= 100;

	protected $m_strName;
	protected $m_intTestTypeId;

	protected $m_boolIsAppeared;
	protected $m_objEmailAttachment;
	protected $m_strTestName;
	protected $m_intTestUserId;
	protected $m_intGraderEmployeeId;
	protected $m_fltTestPercentage;
	protected $m_strNameFull;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strGraderNameFirst;
	protected $m_strGraderNameLast;
	protected $m_strTestZone;
	protected $m_boolIsHideScore;

	/**
	 * Get Functions
	 *
	 */

	public function getTestName() {
		return $this->m_strTestName;
	}

	public function getTestTypeId() {
		return $this->m_intTestTypeId;
	}

	public function getTestUserId() {
		return $this->m_intTestUserId;
	}

	public function getTestPercentage() {
		return $this->m_fltTestPercentage;
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getGraderEmployeeId() {
		return $this->m_intGraderEmployeeId;
	}

	public function getGraderNameFirst() {
		return $this->m_strGraderNameFirst;
	}

	public function getGraderNameLast() {
		return $this->m_strGraderNameLast;
	}

	public function getTestZone() {
		return $this->m_strTestZone;
	}

	public function getIsHideScore() {
		return $this->m_boolIsHideScore;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['test_name'] ) ) $this->setTestName( trim( $arrmixValues['test_name'] ) );
		if( true == isset( $arrmixValues['test_type_id'] ) ) $this->setTestTypeId( trim( $arrmixValues['test_type_id'] ) );
		if( true == isset( $arrmixValues['name_full'] ) ) $this->setNameFull( trim( $arrmixValues['name_full'] ) );
		if( true == isset( $arrmixValues['name_first'] ) ) $this->setNameFirst( trim( $arrmixValues['name_first'] ) );
		if( true == isset( $arrmixValues['name_last'] ) ) $this->setNameLast( trim( $arrmixValues['name_last'] ) );
		if( true == isset( $arrmixValues['grader_employee_id'] ) ) $this->setGraderEmployeeId( trim( $arrmixValues['grader_employee_id'] ) );
		if( true == isset( $arrmixValues['grader_name_first'] ) ) $this->setGraderNameFirst( trim( $arrmixValues['grader_name_first'] ) );
		if( true == isset( $arrmixValues['grader_name_last'] ) ) $this->setGraderNameLast( trim( $arrmixValues['grader_name_last'] ) );
		if( true == isset( $arrmixValues['is_hide_score'] ) ) $this->setIsHideScore( trim( stripcslashes( $arrmixValues['is_hide_score'] ) ) );
		return;
	}

	public function setTestName( $strTestName ) {
		$this->m_strTestName = $strTestName;
	}

	public function setTestTypeId( $intTestTypeId ) {
		$this->m_intTestTypeId = $intTestTypeId;
	}

	public function setTestUserId( $intTestUserId ) {
		$this->m_intTestUserId = $intTestUserId;
	}

	public function setTestPercentage( $fltTestPercentage ) {
		$this->m_fltTestPercentage = $fltTestPercentage;
	}

	public function setTestZone( $strTestZone ) {
		$this->m_strTestZone = $strTestZone;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setGraderEmployeeId( $intGraderEmployeeId ) {
		$this->m_intGraderEmployeeId = $intGraderEmployeeId;
	}

	public function setGraderNameFirst( $strGraderNameFirst ) {
		$this->m_strGraderNameFirst = $strGraderNameFirst;
	}

	public function setGraderNameLast( $strGraderNameLast ) {
		$this->m_strGraderNameLast = $strGraderNameLast;
	}

	public function setIsHideScore( $boolIsHideScore ) {
		$this->set( 'm_boolIsHideScore', CStrings::strToBool( $boolIsHideScore ) );
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchTestById( $objDatabase ) {
		return CTests::fetchTestById( $this->m_intTestId, $objDatabase );
	}

	public function fetchUserSubmittedTestQuestions( $objDatabase ) {
		return CTestQuestions::fetchUserSubmittedTestQuestions( $this->getId(), $objDatabase );
	}

	public function fetchTestSubmissionAnswersByTestId( $objDatabase ) {
		return CTestSubmissionAnswers::fetchTestSubmissionAnswersByTestId( $this->m_intTestId, $this->getId(), $objDatabase );
	}

	public function fetchQuestionOptionsByTestId( $objDatabase ) {
		return CTestAnswerOptions::fetchQuestionOptionsByTestId( $this->m_intTestId, $objDatabase );
	}

	public function fetchCandidateEmployeeByEmployeeId( $objDatabase ) {
		return CEmployees::fetchEmployeeById( $this->getEmployeeId(), $objDatabase );
	}

	public function fetchTestSubmissionAndQuestions( $objAdminDatabase ) {
		return CTestSubmissionAnswers::fetchSimpleTestSubmissionAndQuestionsByTestSubmissionId( $this->getId(), $objAdminDatabase );
	}

	/**
	 * Other Functions
	 *
	 */

	public function sendTestSubmissionGradingEmail( $boolIsMailSendReportingManager, $boolIsMailSendEmployee, $boolIsMailSendTrainingDept, $strHtmlEmailOutput, $objAdminDatabase, $objEmailDatabase, $objTest ) {

		$arrstrToEmailAddresses = array();
		$objEmployee			= $this->fetchCandidateEmployeeByEmployeeId( $objAdminDatabase );

		if( true == $boolIsMailSendReportingManager ) {
			$objEmployeeReportingManager = CEmployees::fetchReportingManagerByEmployeeId( $objEmployee->getId(), $objAdminDatabase );
			$arrstrToEmailAddresses[0] = $objEmployeeReportingManager->getEmailAddress();
		}

		if( true == $boolIsMailSendEmployee ) $arrstrToEmailAddresses[] = $objEmployee->getEmailAddress();
		if( true == $boolIsMailSendTrainingDept ) $arrstrToEmailAddresses[] = CSystemEmail::XENTO_TRAINING_EMAIL_ADDRESS;

		if( false == valArr( $arrstrToEmailAddresses ) ) return false;

		$strSubject	= 'Test Name: ' . $objTest->getName() . ', Given By ' . \Psi\CStringService::singleton()->str_pad( ( string ) $objEmployee->getNameFull(), 9, '0', STR_PAD_LEFT );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, $strHtmlEmailOutput, $strToEmailAddress = implode( ',', $arrstrToEmailAddresses ) );

		switch( NULL ) {
			default:
				if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) return false;
		}

		return true;
	}

	public function updateAndLoadTestSubmissionScore( $objEmployeeApplication, $objAdminDatabase ) {

		$arrintTestSummary = array();

		$arrintTestAnswersAndQuestions = $this->fetchTestSubmissionAndQuestions( $objAdminDatabase );

		$intTotalPoints = 0;
		$intMaxPoints = 0;

		foreach( $arrintTestAnswersAndQuestions as $arrintTestAnswerAndQuestion ) {
			$intTotalPoints += $arrintTestAnswerAndQuestion['points_granted'];
			$intMaxPoints += $arrintTestAnswerAndQuestion['maximum_points'];
		}

		$this->setScore( $intTotalPoints );
		$this->setGradedBy( SYSTEM_USER_ID );
		$this->setGradedOn( 'NOW' );

		$objAdminDatabase->begin();

		if( false == $objEmployeeApplication->update( SYSTEM_USER_ID, $objAdminDatabase, $boolResumeRequired = false ) ) {
			$objAdminDatabase->rollback();
			return $arrintTestSummary;
		}

		if( false == $this->update( SYSTEM_USER_ID, $objAdminDatabase, $boolResumeRequired = false ) ) {
			$objAdminDatabase->rollback();
			return $arrintTestSummary;
		}

		$objAdminDatabase->commit();

		$arrintTestSummary['test_id'] = $this->getTestId();
		$arrintTestSummary['test_submission_id'] = $this->getId();
		$arrintTestSummary['test_submission_score'] = $intTotalPoints;
		$arrintTestSummary['test_max_score'] = $intMaxPoints;

		return $arrintTestSummary;
	}

	public function updateTestSubmissionScore( $objAdminDatabase ) {

		$arrintTestAnswersAndQuestions = $this->fetchTestSubmissionAndQuestions( $objAdminDatabase );

		$intTotalPoints = 0;
		$intMaxPoints = 0;

		foreach( $arrintTestAnswersAndQuestions as $arrintTestAnswerAndQuestion ) {
			$intTotalPoints += $arrintTestAnswerAndQuestion['points_granted'];
			$intMaxPoints += $arrintTestAnswerAndQuestion['maximum_points'];
		}

		$this->setScore( ( int ) $intTotalPoints . ' / ' . ( int ) $intMaxPoints );
		$this->setGradedBy( SYSTEM_USER_ID );
		$this->setGradedOn( 'NOW' );

		$objAdminDatabase->begin();

		if( false == $this->update( SYSTEM_USER_ID, $objAdminDatabase, $boolResumeRequired = false ) ) {
			$objAdminDatabase->rollback();
			return false;
		}

		$objAdminDatabase->commit();

		return true;
	}

	public function createTestSubmission( $intTestId, $intEmployeeApplicationid ) {
		$this->setTestId( $intTestId );
		$this->setEmployeeApplicationId( $intEmployeeApplicationid );
	}

}
?>