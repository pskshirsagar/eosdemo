<?php

class CCompanyPaymentFailure extends CBaseCompanyPaymentFailure {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFailedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsReviewed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createAction() {
		$objAction = new CAction();
		$objAction->setPrimaryReference( $this->getId() );
		$objAction->setActionTypeId( \CActionType::COMPANY_PAYMENT_FAILURE );
		return $objAction;
	}

}
?>