<?php

/**
 * Class CGsuiteActivityType
 *
 * The following fields are stored using TEosDetails
 * @method string getScope() Returns the api scope used for this activity type
 * @method string getApplicationName() Returns the application name used for this activity type
 * @method string getEventName() Returns the event name used for this activity type
 */
class CGsuiteActivityType extends CBaseGsuiteActivityType {

	const LOGIN = 1;
	const CALENDAR_CREATE = 2;
	const CALENDAR_DELETE = 3;
	const CALENDAR_MODIFY = 4;
	const DRIVE_CREATE = 5;
	const DRIVE_EDIT = 6;
	const DRIVE_VIEW = 7;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
