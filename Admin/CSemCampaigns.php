<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemCampaigns
 * Do not add any new functions to this class.
 */

class CSemCampaigns extends CBaseSemCampaigns {

	public static function fetchAllSemCampaigns( $objAdminDatabase ) {
		return self::fetchSemCampaigns( 'SELECT * FROM sem_campaigns ORDER BY name', $objAdminDatabase );
	}

	public static function fetchSemCampaignByStateCode( $strStateCode, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM sem_campaigns WHERE state_code = \'' . \Psi\CStringService::singleton()->strtoupper( addslashes( $strStateCode ) ) . '\'';
		return self::fetchSemCampaign( $strSql, $objAdminDatabase );
	}

}
?>