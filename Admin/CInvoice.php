<?php

use Psi\Eos\Admin\CSalesTaxTransactions;
use Psi\Eos\Admin\CInvoices;
use Psi\Eos\Admin\CAds;
use Psi\Eos\Admin\CRemittanceAddresses;
use Psi\Eos\Admin\CCompanyPayments;
use Psi\Libraries\LuhnAlgorithm\CNumbers;

use Psi\Libraries\ExternalPrinceWrapper\CPrinceFactory;
use Psi\Libraries\UtilSuccessMsg\CSuccessMsg;

class CInvoice extends CBaseInvoice {

	use TEosDetails;

	const JANUARY			= 1;
	const APRIL				= 4;
	const JULY				= 7;
	const OCTOBER			= 10;

	const LOOKUP_TYPE_ADD_PURCHASE_ORDER_PROPERTY		= 'add_purchase_order_property';
	const LOOKUP_TYPE_UNIT_OF_MEASURES					= 'unit_of_measures';
	const LOOKUP_TYPE_BULK_INVOICE_VENDOR				= 'bulk_invoice_vendor';
	const MIN_CHECK_DIGIT_REQUIRED						= 5000000;
	const INVOICE_REFERENCE_NUMBER						= 100000;
	const INVOICE_FOOTER_FOR_CANADA						= 'Entrata Canada, Inc. is a wholly owned Canadian subsidiary of Entrata, Inc.';

	public static $c_arrintQuarterMonthsIds = [ self::JANUARY, self::APRIL, self::JULY, self::OCTOBER ];
	public static $c_arrintSemiAnnualMonthsIds = [ self::JANUARY, self::JULY ];

	protected $m_fltAmountDue;
	protected $m_fltInvoiceTotal;
	protected $m_fltNewChargesTotal;
	protected $m_fltTotalTaxApplied;
	protected $m_fltNewPaymentsTotal;
	protected $m_fltNewSetUpChargesTotal;
	protected $m_fltAppliedPaymentsTotal;
	protected $m_fltPastDueInvoicesTotal;
	protected $m_fltSalesTaxPaymentsTotal;
	protected $m_fltUnappliedPaymentsTotal;
	protected $m_fltNewUtilityChargesTotal;
	protected $m_fltUtilityInvoicesSubTotal;
	protected $m_fltSalesTaxNewChargesTotal;
	protected $m_fltNewRecurringChargesTotal;
	protected $m_fltSalesTaxSetupChargesTotal;
	protected $m_fltSalesTaxUtilityChargesTotal;
	protected $m_fltSalesTaxRecurringChargesTotal;

	protected $m_objAd;
	/** @var \CClient */
	protected $m_objClient;
	/** @var CAccount */
	protected $m_objAccount;
	protected $m_objAdImage;
	protected $m_objPsDocument;
	protected $m_objUtilityBatch;
	/** @var CCompanyPayment */
	protected $m_objCompanyPayment;

	protected $m_arrobjChargeCodes;
	protected $m_arrobjSuccessMsgs;
	protected $m_arrobjPaymentTypes;
	protected $m_arrobjPastDueInvoices;
	protected $m_arrobjAccountAccountGroups;

	protected $m_arrmixInvoicePages;
	protected $m_arrmixUnpaidInvoices;
	protected $m_arrmixLatestCompanyPayments;
	protected $m_arrmixAccountActivityDetails;
	protected $m_arrmixPaymentsAndCreditsApplied;
	protected $m_arrmixUnappliedPaymentsAndCredits;

	protected $m_strCompanyName;
	protected $m_strAccountName;
	protected $m_strInvoiceStatus;
	protected $m_strFtpInvoiceFilename;
	protected $m_strLastPaymentDatetime;
	protected $m_strDetails;
	protected $m_strCurrencySymbol;
	protected $m_jsonDetails;

	protected $m_arrstrStyles;
	protected $m_arrstrPropertyUtilityTypeRateGroupedData;
	protected $m_arrstrTransactions;
	protected $m_arrstrCCHTransactionIds;

	protected $m_intYVal;
	protected $m_intInvoicesFtpd;
	protected $m_intAccountTypeId;
	protected $m_intInvoicePageNo;
	protected $m_intAccountRegionId;
	protected $m_intReportInstanceId;
	protected $m_intIsIntermediaryAccount;
	protected $m_intInvoiceTransmissionTypeId;
	protected $m_intFtpVendorId;

	protected $m_boolHasLoadedAccountAccountGroups;

	public function __construct() {
		parent::__construct();

		$this->m_fltAmountDue = 0;
	}

	/*
	 * Get Or Fetch Functions
	 *
	 */

	public function getOrFetchUtilityBatch( $objUtilitiesDatabase ) {

		if( false == valObj( $this->m_objUtilityBatch, 'CUtilityBatch' ) ) {
			$this->m_objUtilityBatch = $this->fetchUtilityBatch( $objUtilitiesDatabase );
		}

		return $this->m_objUtilityBatch;
	}

	public function getOrFetchChargeCodes( $objAdminDatabase ) {
		if( false == valArr( $this->m_arrobjChargeCodes ) ) {
			$this->m_arrobjChargeCodes = $this->fetchChargeCodes( $objAdminDatabase );
		}

		return $this->m_arrobjChargeCodes;
	}

	public function getOrFetchPropertyUtilityTypeRateGroupedData( $arrintUtilityBatchIds, $objAdminDatabase, $objUtilitiesDatabase ) {

		if( false == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData ) ) {
			$arrstrPropertyUtilityTypeRateGroupedData = \Psi\Eos\Utilities\CPropertyUtilityTypeRates::createService()->fetchGroupedPropertyUtilityTypeRateInvoiceDataByUtilityBatchIds( $arrintUtilityBatchIds, $objAdminDatabase, $objUtilitiesDatabase );

			if( true == valObj( $this->m_objUtilityBatch, 'CUtilityBatch' ) && true == valArr( $arrstrPropertyUtilityTypeRateGroupedData ) && true == array_key_exists( $this->m_objUtilityBatch->getId(), $arrstrPropertyUtilityTypeRateGroupedData ) ) {
				$this->m_arrstrPropertyUtilityTypeRateGroupedData = $arrstrPropertyUtilityTypeRateGroupedData[$this->m_objUtilityBatch->getId()];
			}
		}

		return $this->m_arrstrPropertyUtilityTypeRateGroupedData;
	}

	public function getOrFetchAccount( $objAdminDatabase ) {
		if( false == valObj( $this->m_objAccount, 'CAccount' ) ) {
			$this->fetchAccount( $objAdminDatabase );
		}

		return $this->m_objAccount;
	}

	public function getOrFetchClient( $objAdminDatabase ) {
		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->fetchClient( $objAdminDatabase );
		}

		return $this->m_objClient;
	}

	/*
	 * Get Functions
	 *
	 */

	public function getPaymentTypes() {
		return $this->m_arrobjPaymentTypes;
	}

	public function getNewChargesTotal() {
		return ( false == is_null( $this->m_fltNewChargesTotal ) ) ? $this->m_fltNewChargesTotal : 0;
	}

	public function getNewRecurringChargesTotal() {
		return ( false == is_null( $this->m_fltNewRecurringChargesTotal ) ) ? $this->m_fltNewRecurringChargesTotal : 0;
	}

	public function getNewSetUpChargesTotal() {
		return ( false == is_null( $this->m_fltNewSetUpChargesTotal ) ) ? $this->m_fltNewSetUpChargesTotal : 0;
	}

	public function getNewPaymentsTotal() {
		return ( false == is_null( $this->m_fltNewPaymentsTotal ) ) ? $this->m_fltNewPaymentsTotal : 0;
	}

	public function getAccountRegionId() {
		return $this->m_intAccountRegionId;
	}

	public function getAmountDue() {
		return $this->m_fltAmountDue;
	}

	public function getAccount() {
		return $this->m_objAccount;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getTransactions() {
		return $this->m_arrstrTransactions;
	}

	public function getCompanyPayment() {
		return $this->m_objCompanyPayment;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getIsIntermediaryAccount() {
		return $this->m_intIsIntermediaryAccount;
	}

	public function getAccountTypeId() {
		return $this->m_intAccountTypeId;
	}

	public function getSuccessMsgs() {
		return $this->m_arrobjSuccessMsgs;
	}

	public function getInvoiceAmount() {
		return ( false == is_null( $this->m_fltInvoiceAmount ) ) ? $this->m_fltInvoiceAmount : 0;
	}

	public function getChargeCodes() {
		return $this->m_arrobjChargeCodes;
	}

	public function getFtpInvoiceFilename() {
		return $this->m_strFtpInvoiceFilename;
	}

	public function getPastDueInvoices() {
		return $this->m_arrobjPastDueInvoices;
	}

	public function getLastPaymentDatetime() {
		return $this->m_strLastPaymentDatetime;
	}

	public function getInvoicesFtpd() {
		return $this->m_intInvoicesFtpd;
	}

	public function getInvoiceStatus() {
		return $this->m_strInvoiceStatus;
	}

	public function getSalesTaxRecurringChargesTotal() {
		return $this->m_fltSalesTaxRecurringChargesTotal;
	}

	public function getSalesTaxSetupChargesTotal() {
		return $this->m_fltSalesTaxSetupChargesTotal;
	}

	public function getSalesTaxNewChargesTotal() {
		return $this->m_fltSalesTaxNewChargesTotal;
	}

	public function getSalesTaxUtilityChargesTotal() {
		return $this->m_fltSalesTaxUtilityChargesTotal;
	}

	public function getSalesTaxPaymentsTotal() {
		return $this->m_fltSalesTaxPaymentsTotal;
	}

	public function getNewUtilityChargesTotal() {
		return $this->m_fltNewUtilityChargesTotal;
	}

	public function getInvoiceTotal() {
		return $this->m_fltInvoiceTotal;
	}

	public function getPastDueInvoicesTotal() {
		return $this->m_fltPastDueInvoicesTotal;
	}

	public function getAppliedPaymentsTotal() {
		return $this->m_fltAppliedPaymentsTotal;
	}

	public function getUnappliedPaymentsTotal() {
		return $this->m_fltUnappliedPaymentsTotal;
	}

	public function getLatestCompanyPaymentsData() {
		return $this->m_arrmixLatestCompanyPayments;
	}

	public function getUtilityInvoicesSubTotal() {
		return ( false == is_null( $this->m_fltUtilityInvoicesSubTotal ) ) ? $this->m_fltUtilityInvoicesSubTotal : 0;
	}

	public function getInvoiceTransmissionTypeId() {
		return $this->m_intInvoiceTransmissionTypeId;
	}

	public function getFtpVendorId() {
		return $this->m_intFtpVendorId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['new_charges_total'] ) ) {
			$this->setNewChargesTotal( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['new_charges_total'] ) : $arrmixValues['new_charges_total'] );
		}
		if( true == isset( $arrmixValues['new_set_up_charges_total'] ) ) {
			$this->setNewChargesTotal( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['new_set_up_charges_total'] ) : $arrmixValues['new_set_up_charges_total'] );
		}
		if( true == isset( $arrmixValues['amount_due'] ) ) {
			$this->setAmountDue( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['amount_due'] ) : $arrmixValues['amount_due'] );
		}
		if( true == isset( $arrmixValues['company_name'] ) ) {
			$this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['company_name'] ) : $arrmixValues['company_name'] );
		}
		if( true == isset( $arrmixValues['account_name'] ) ) {
			$this->setAccountName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_name'] ) : $arrmixValues['account_name'] );
		}
		if( true == isset( $arrmixValues['is_intermediary_account'] ) ) {
			$this->setIsIntermediaryAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_intermediary_account'] ) : $arrmixValues['is_intermediary_account'] );
		}
		if( true == isset( $arrmixValues['account_region_id'] ) ) {
			$this->setAccountRegionId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_region_id'] ) : $arrmixValues['account_region_id'] );
		}
		if( true == isset( $arrmixValues['account_type_id'] ) ) {
			$this->setAccountTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_type_id'] ) : $arrmixValues['account_type_id'] );
		}
		if( true == isset( $arrmixValues['last_payment_datetime'] ) ) {
			$this->setLastPaymentDatetime( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['last_payment_datetime'] ) : $arrmixValues['last_payment_datetime'] );
		}
		if( true == isset( $arrmixValues['invoices_ftpd'] ) ) {
			$this->setInvoicesFtpd( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['invoices_ftpd'] ) : $arrmixValues['invoices_ftpd'] );
		}
		if( true == isset( $arrmixValues['invoice_status'] ) ) {
			$this->setInvoiceStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['invoice_status'] ) : $arrmixValues['invoice_status'] );
		}
		if( true == isset( $arrmixValues['details'] ) ) {
			$this->setDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['details'] ) : $arrmixValues['details'] );
		}
		if( true == isset( $arrmixValues['invoice_transmission_type_id'] ) ) {
			$this->setInvoiceTransmissionTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['invoice_transmission_type_id'] ) : $arrmixValues['invoice_transmission_type_id'] );
		}
		if( true == isset( $arrmixValues['ftp_vendor_id'] ) ) {
			$this->setFtpVendorId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['ftp_vendor_id'] ) : $arrmixValues['ftp_vendor_id'] );
		}
	}

	public function setHasLoadedAccountAccountGroups( $boolHasLoadedAccountAccountGroups ) {
		$this->m_boolHasLoadedAccountAccountGroups = $boolHasLoadedAccountAccountGroups;
	}

	public function setDefaults() {

		$this->m_intInvoiceStatusTypeId = CInvoiceStatusType::PENDING_REVIEW;
	}

	public function setAmountDue( $fltAmountDue ) {
		$this->m_fltAmountDue = $fltAmountDue;
	}

	public function setChargeCodes( $arrobjChargeCodes ) {
		$this->m_arrobjChargeCodes = $arrobjChargeCodes;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setIsIntermediaryAccount( $intIsIntermediaryAccount ) {
		$this->m_intIsIntermediaryAccount = $intIsIntermediaryAccount;
	}

	public function setAccountTypeId( $intAccountTypeId ) {
		$this->m_intAccountTypeId = $intAccountTypeId;
	}

	public function setClient( $objClient ) {
		$this->m_objClient = $objClient;
	}

	public function setTransactions( $arrstrTransactions ) {
		$this->m_arrstrTransactions = $arrstrTransactions;
	}

	public function setAccount( $objAccount ) {
		$this->m_objAccount = $objAccount;
	}

	public function setPaymentTypes( $arrobjPaymentTypes ) {
		$this->m_arrobjPaymentTypes = $arrobjPaymentTypes;
	}

	public function setAccountRegionId( $intAccountRegionId ) {
		$this->m_intAccountRegionId = $intAccountRegionId;
	}

	public function setNewChargesTotal( $fltNewChargesTotal ) {
		$this->m_fltNewChargesTotal = $fltNewChargesTotal;
	}

	public function setNewRecurringChargesTotal( $fltNewSetUpChargesTotal ) {
		$this->m_fltNewRecurringChargesTotal = $fltNewSetUpChargesTotal;
	}

	public function setNewSetUpChargesTotal( $fltNewRecurringChargesTotal ) {
		$this->m_fltNewSetUpChargesTotal = $fltNewRecurringChargesTotal;
	}

	public function setNewPaymentsTotal( $fltNewPaymentsTotal ) {
		$this->m_fltNewPaymentsTotal = $fltNewPaymentsTotal;
	}

	public function setNewUtilityChargesTotal( $fltNewUtilityChargesTotal ) {
		$this->m_fltNewUtilityChargesTotal = $fltNewUtilityChargesTotal;
	}

	public function setUtilityBatch( $objUtilityBatch ) {
		$this->m_objUtilityBatch = $objUtilityBatch;
	}

	public function setPsDocument( $objPsDocument ) {
		$this->m_objPsDocument = $objPsDocument;
	}

	public function setAd( $objAd ) {
		$this->m_objAd = $objAd;
	}

	public function setAdImage( $objAdImage ) {
		$this->m_objAdImage = $objAdImage;
	}

	public function setPropertyUtilityTypeRateGroupedData( $arrstrPropertyUtilityTypeRateGroupedData ) {
		$this->m_arrstrPropertyUtilityTypeRateGroupedData = $arrstrPropertyUtilityTypeRateGroupedData;
	}

	public function setFtpInvoiceFilename( $strFtpInvoiceFilename ) {
		$this->m_strFtpInvoiceFilename = $strFtpInvoiceFilename;
	}

	public function setPastDueInvoices( $arrobjPastDueInvoices ) {
		$this->m_arrobjPastDueInvoices = $arrobjPastDueInvoices;
	}

	public function setLastPaymentDatetime( $strLastPaymentDatetime ) {
		$this->m_strLastPaymentDatetime = $strLastPaymentDatetime;
	}

	public function setInvoicesFtpd( $intInvoicesFtpd ) {
		$this->m_intInvoicesFtpd = $intInvoicesFtpd;
	}

	public function setInvoiceStatus( $strInvoiceStatus ) {
		$this->m_strInvoiceStatus = $strInvoiceStatus;
	}

	public function setSalesTaxRecurringChargesTotal( $fltSalesTaxRecurringChargesTotal ) {
		$this->m_fltSalesTaxRecurringChargesTotal = $fltSalesTaxRecurringChargesTotal;
	}

	public function setSalesTaxUtilityChargesTotal( $fltSalesTaxUtilityChargesTotal ) {
		$this->m_fltSalesTaxUtilityChargesTotal = $fltSalesTaxUtilityChargesTotal;
	}

	public function setSalesTaxSetupChargesTotal( $fltSalesTaxSetupChargesTotal ) {
		$this->m_fltSalesTaxSetupChargesTotal = $fltSalesTaxSetupChargesTotal;
	}

	public function setSalesTaxNewChargesTotal( $fltSalesTaxNewChargesTotal ) {
		$this->m_fltSalesTaxNewChargesTotal = $fltSalesTaxNewChargesTotal;
	}

	public function setSalesTaxPaymentsTotal( $fltSalesTaxPaymentsTotal ) {
		$this->m_fltSalesTaxPaymentsTotal = $fltSalesTaxPaymentsTotal;
	}

	public function addTotalTaxApplied( $fltTotalTaxApplied ) {
		$this->m_fltTotalTaxApplied += $fltTotalTaxApplied;
	}

	public function addCCHTransactionId( $strCCHTransactionId ) {
		$this->m_arrstrCCHTransactionIds[] = $strCCHTransactionId;
	}

	public function setLatestCompanyPaymentsData( $arrmixLatestCompanyPayments ) {
		$this->m_arrmixLatestCompanyPayments = $arrmixLatestCompanyPayments;
	}

	public function setUtilityInvoicesSubTotal( $fltUtilityInvoicesSubTotal ) {
		$this->m_fltUtilityInvoicesSubTotal = $fltUtilityInvoicesSubTotal;
	}

	public function setInvoiceTransmissionTypeId( $intInvoiceTransmissionTypeId ) {
		$this->m_intInvoiceTransmissionTypeId = CStrings::strToIntDef( $intInvoiceTransmissionTypeId, NULL, false );
	}

	public function setFtpVendorId( $intFtpVendorId ) {
		$this->m_intFtpVendorId = CStrings::strToIntDef( $intFtpVendorId );;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchChargeCodes( $objDatabase ) {

		$this->m_arrobjChargeCodes = \Psi\Eos\Admin\CChargeCodes::createService()->fetchAllChargeCodes( $objDatabase );

		return $this->m_arrobjChargeCodes;
	}

	public function fetchUtilityBatch( $objDatabase ) {

		$this->m_objUtilityBatch = \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchUtilityBatchById( $this->getUtilityBatchId(), $objDatabase );

		return $this->m_objUtilityBatch;
	}

	public function fetchAccount( $objDatabase ) {
		$this->m_objAccount = \Psi\Eos\Admin\CAccounts::createService()->fetchAccountById( $this->getAccountId(), $objDatabase );

		return $this->m_objAccount;
	}

	public function fetchClient( $objDatabase ) {
		$this->m_objClient = \Psi\Eos\Admin\CClients::createService()->fetchClientById( $this->getCid(), $objDatabase );

		return $this->m_objClient;
	}

	public function fetchAdjustmentSalesTaxTransactionCount( $objDatabase ) {
		return CSalesTaxTransactions::createService()->fetchAdjustmentSalesTaxTransactionCountByInvoiceId( $this->getId(), $objDatabase );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAccountId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Account id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceNumber() {
		$boolIsValid = true;

		if( false == isset( $this->m_intInvoiceNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_number', 'Invoice number is required.' ) );
		}

		if( self::MIN_CHECK_DIGIT_REQUIRED < $this->m_intId ) {

			// split out check digits from invoice_number and verify separately
			$intAccountNumber = $this->m_intAccountId . \Psi\CStringService::singleton()->substr( $this->m_intInvoiceNumber, -1, 1 );
			$intInvoiceNumber = \Psi\CStringService::singleton()->substr( $this->m_intInvoiceNumber, 0, -1 );

			if( false == CNumbers::createService()->verifyLuhn( $intAccountNumber ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_number', 'Invoice number failed checksum for account ID.' ) );
			}

			if( false == CNumbers::createService()->verifyLuhn( $intInvoiceNumber ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_number', 'Invoice number failed checksum for invoice ID.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valInvoiceDate() {
		$boolIsValid = true;

		if( false == isset( $this->m_strInvoiceDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_date', 'Invoice date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valClient() {

		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client', 'client was not loaded.' ) );
			return $boolIsValid;
		}

		return true;
	}

	public function valAccount() {

		if( false == valObj( $this->m_objAccount, 'CAccount' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Account was not loaded.' ) );
			return $boolIsValid;
		}

		return true;
	}

	public function valPositiveInvoiceAmountOnNormalAccount() {

		if( false == valObj( $this->m_objAccount, 'CAccount' ) ) {
			return false;
		}

		if( 0 >= bccomp( $this->m_fltInvoiceAmount, 0, 2 ) && CAccountType::INTERMEDIARY != $this->m_objAccount->getAccountTypeId() && CAccountType::CLEARING != $this->m_objAccount->getAccountTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_amount', 'Invoice amount was <= zero dollars and the account was not an intermediary OR clearing account.' ) );
			return $boolIsValid;
		}

		return true;
	}

	public function valNonZeroInvoiceAmountOnIntermediaryAccountOrClearingAccount() {

		if( false == valObj( $this->m_objAccount, 'CAccount' ) ) {
			return false;
		}

		if( 0 == bccomp( $this->m_fltInvoiceAmount, 0, 2 ) && ( CAccountType::INTERMEDIARY == $this->m_objAccount->getAccountTypeId() || CAccountType::CLEARING == $this->m_objAccount->getAccountTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_amount', 'Invoice amount was zero dollars on an intermediary OR clearing account.' ) );
			return $boolIsValid;
		}

		return true;
	}

	public function valInvoiceStatusTypeId() {
		$boolIsValid = true;

		if( false == is_null( $this->getInvoiceStatusTypeId() ) && false == in_array( $this->getInvoiceStatusTypeId(), [ CInvoiceStatusType::SALES_TAX_PENDING ] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_batch_id', 'Invoice can be deleted only when invoice status type is Sales Tax Pending.' ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceBatchId() {
		$boolIsValid = true;

		if( false == is_null( $this->getInvoiceBatchId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_batch_id', 'Invoice cannot be deleted when it belongs to an invoice batch.' ) );
		}

		return $boolIsValid;
	}

	public function valMailedOn() {
		$boolIsValid = true;

		if( false == is_null( $this->getMailedOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mailed_on', 'Invoice cannot be deleted when it has already been mailed.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailedOn() {
		$boolIsValid = true;

		if( false == is_null( $this->getEmailedOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'emailed_on', 'Invoice cannot be deleted when it has already been emailed.' ) );
		}

		return $boolIsValid;
	}

	public function valFtpdOn() {
		$boolIsValid = true;

		if( false == is_null( $this->getFtpdOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ftpd_on', 'Invoice cannot be deleted when ftpd is set.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valInvoiceNumber();
				$boolIsValid &= $this->valInvoiceDate();
				break;

			case 'post_invoice_payment':
				$boolIsValid &= $this->valAccount();
				$boolIsValid &= $this->valClient();
				$boolIsValid &= $this->valPositiveInvoiceAmountOnNormalAccount();
				$boolIsValid &= $this->valNonZeroInvoiceAmountOnIntermediaryAccountOrClearingAccount();
				break;

			case 'send_email':
				$boolIsValid &= $this->m_objAccount->validate( 'validate_billing_email' );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valInvoiceBatchId();
				$boolIsValid &= $this->valInvoiceStatusTypeId();
				$boolIsValid &= $this->valMailedOn();
				$boolIsValid &= $this->valEmailedOn();
				$boolIsValid &= $this->valFtpdOn();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * @param \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $objCurrentWorksheet
	 * @param \CDatabase          $objAdminDatabase
	 * @throws \PhpOffice\PhpSpreadsheet\Exception
	 */
	public function loadStandardTransactionXlsxGrid( $objCurrentWorksheet, $objAdminDatabase ) {

		$intCurrentRowNumber = $objCurrentWorksheet->getHighestRow();

		// row Blank
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		// start charges section
		// row  Charges Posted During Period
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );

		if( 0 == $this->getIsCreditMemo() ) {
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Recurring Charges' );
		}

		// display transaction data

		$intSerialNumber					= 1;
		$intNonRecurringTransactionsIndex 	= 0;

		$boolChargeDataExists				= false;
		$boolRecurringChargesDataExists 	= false;
		$boolRecurringChargesAvailable 		= false;
		$boolSetupChargeDataExists			= false;
		$arrmixNonRecurringTransactions 	= [];
		$arrintSalesTaxTransactions 		= [];
		$arrmixSetUpCharges					= [];
		$arrmixUtilityTransactions 			= [];
		$arrintUtilityTransactionCount 		= [];

		$arrintUtilityChargeCodes = [ CChargeCode::UTILITY_BILLING_FEES => CChargeCode::UTILITY_BILLING_FEES, CChargeCode::UTILITY_MOVE_IN_FEES => CChargeCode::UTILITY_MOVE_IN_FEES ];

		$arrobjSalesTaxTransactions = CSalesTaxTransactions::createService()->fetchSalesTaxTransactionsByInvoiceId( $this->getId(), $objAdminDatabase );

		if( true == valArr( $arrobjSalesTaxTransactions ) ) {
			$arrobjSalesTaxTransactions = rekeyObjects( 'TransactionId', $arrobjSalesTaxTransactions );
			$arrintSalesTaxTransactions	= array_flip( array_keys( $arrobjSalesTaxTransactions ) );
		}

		if( true == valArr( $this->m_arrstrTransactions ) ) {

			// row  Charges Posted During Period Heading
			$intCurrentRowNumber++;

			if( 0 == $this->getIsCreditMemo() ) {
				$objCurrentWorksheet->mergeCells( 'C' . $intCurrentRowNumber . ':' . 'E' . $intCurrentRowNumber );
				$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, 'Date' );
				$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, 'Description' );
				$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, 'Subtotal' );
				$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, 'Sales Tax' );
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, 'Amount' );

				$intCurrentRowNumber++;
			}

			foreach( $this->m_arrstrTransactions as $arrstrTransaction ) {

				if( true == is_null( $arrstrTransaction['company_payment_id'] ) ) {

					if( true == in_array( $arrstrTransaction['charge_code_id'], CChargeCode::$c_arrintRecurringChargeGroups ) && 0 == $this->getIsCreditMemo() ) {
						// row  Charges Posted During Period Details

						$boolRecurringChargesDataExists = true;
						$fltTotalAmount 				= 0;
						$fltSalesTax					= 0;
						// show_bundle_pricing
						if( false == is_null( $arrstrTransaction['bundle_ps_product_id'] ) ) {

							$boolRecurringChargesAvailable = true;
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['bundle_ps_product_id']	= $arrstrTransaction['bundle_ps_product_id'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['transaction_amount']	= $arrstrTransaction['transaction_amount'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['transaction_datetime']	= $arrstrTransaction['transaction_datetime'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['name']					= $arrstrTransaction['name'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['charge_code'][$intNonRecurringTransactionsIndex]	= $this->m_arrobjChargeCodes[$arrstrTransaction['charge_code_id']]->getName();
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['memo'][$intNonRecurringTransactionsIndex]			= $arrstrTransaction['memo'];

							if( true == valArr( $arrintSalesTaxTransactions ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTransactions ) ) {
								if( true == isset( $arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] ) ) {
									$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax']	+= $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
								} else {
									$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] = $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
								}
							}

						} else {

							$objCurrentWorksheet->mergeCells( 'C' . $intCurrentRowNumber . ':' . ( 'E' . $intCurrentRowNumber ) );
							$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );
							$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, \Psi\CStringService::singleton()->substr( $arrstrTransaction['transaction_datetime'], 0, 10 ) );
							if( false == is_null( $arrstrTransaction['memo'] ) ) {
								$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, \Psi\CStringService::singleton()->substr( $arrstrTransaction['memo'], 0, 100 ) );
							} else {
								$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, $this->m_arrobjChargeCodes[$arrstrTransaction['charge_code_id']]->getName() );
							}

							$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

							if( 0 > $arrstrTransaction['transaction_amount'] ) {
								$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, '( ' . __( '{%m, 0, p:2}', [ ( - 1 ) * $arrstrTransaction['transaction_amount'] ] ) . ' )' );
							} else {
								$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, ' ' . __( '{%m, 0, p:2}', [ $arrstrTransaction['transaction_amount'] ] ) );
							}

							if( true == valArr( $arrintSalesTaxTransactions ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTransactions ) && 0 != $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied() ) {
								$objCurrentWorksheet->getStyle( 'G' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );
								$fltSalesTax = $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
								$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, ' ' . __( '{%m, 0, p:2}', [ $fltSalesTax ] ) );
							} else {
								$objCurrentWorksheet->getStyle( 'G' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );
								$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, ' - ' );
							}

							$fltTotalAmount = $arrstrTransaction['transaction_amount'] + $fltSalesTax;

							$objCurrentWorksheet->getStyle( 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

							if( 0 > $fltTotalAmount ) {
								$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, '( ' . __( '{%m, 0, p:2}', [ ( - 1 ) * $fltTotalAmount ] ) . ' )' );
							} else {
								$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' ' . __( '{%m, 0, p:2}', [ $fltTotalAmount ] ) );
							}

							$intCurrentRowNumber++;
						}

						$intNonRecurringTransactionsIndex++;

					} elseif( true == in_array( $arrstrTransaction['charge_code_id'], CChargeCode::$c_arrintSetUpChargeCodes ) && 0 == $this->getIsCreditMemo() ) {

						$boolSetupChargeDataExists = true;
						// show_bundle_pricing
						if( false == is_null( $arrstrTransaction['bundle_ps_product_id'] ) ) {

							$boolRecurringChargesAvailable = true;
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['bundle_ps_product_id']	= $arrstrTransaction['bundle_ps_product_id'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['transaction_amount']	= $arrstrTransaction['transaction_amount'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['transaction_datetime']	= $arrstrTransaction['transaction_datetime'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['name']					= $arrstrTransaction['name'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['charge_code'][$intNonRecurringTransactionsIndex]	= $this->m_arrobjChargeCodes[$arrstrTransaction['charge_code_id']]->getName();
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['memo'][$intNonRecurringTransactionsIndex]			= $arrstrTransaction['memo'];

							if( true == valArr( $arrintSalesTaxTransactions ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTransactions ) ) {
								if( true == isset( $arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] ) ) {
									$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax']	+= $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
								} else {
									$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] = $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
								}
							}
						} else {

							$arrmixSetUpCharges[$intNonRecurringTransactionsIndex]['id'] 					= $arrstrTransaction['id'];
							$arrmixSetUpCharges[$intNonRecurringTransactionsIndex]['charge_code_id'] 		= $arrstrTransaction['charge_code_id'];
							$arrmixSetUpCharges[$intNonRecurringTransactionsIndex]['item_count'] 			= $arrstrTransaction['item_count'];
							$arrmixSetUpCharges[$intNonRecurringTransactionsIndex]['transaction_datetime'] 	= \Psi\CStringService::singleton()->substr( $arrstrTransaction['transaction_datetime'], 0, 10 );
							$arrmixSetUpCharges[$intNonRecurringTransactionsIndex]['charge_code']  			= $this->m_arrobjChargeCodes[$arrstrTransaction['charge_code_id']]->getName();
							$arrmixSetUpCharges[$intNonRecurringTransactionsIndex]['memo'] 					= is_null( $arrstrTransaction['memo'] ) ? '': \Psi\CStringService::singleton()->substr( $arrstrTransaction['memo'], 0, 100 );
							$arrmixSetUpCharges[$intNonRecurringTransactionsIndex]['transaction_amount']   	= $arrstrTransaction['transaction_amount'];

							if( true == valArr( $arrintSalesTaxTransactions ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTransactions ) ) {
								$arrmixSetUpCharges[$intNonRecurringTransactionsIndex]['sales_tax']	= $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
							}
						}
						$intNonRecurringTransactionsIndex++;

					} elseif( true == in_array( $arrstrTransaction['charge_code_id'], CChargeCode::$c_arrintUtilityChargeCodes ) && 0 == $this->getIsCreditMemo() ) {
						// Utility section
						$strKey = $arrstrTransaction['charge_code_id'] . '~' . $arrstrTransaction['memo'];

						$intTempNonRecurringTransactionsIndex = $intNonRecurringTransactionsIndex;

						if( true == array_key_exists( $arrstrTransaction['charge_code_id'], $arrintUtilityChargeCodes ) && true == array_key_exists( $strKey, $arrintUtilityTransactionCount ) ) {
							$intNonRecurringTransactionsIndex = $arrintUtilityTransactionCount[$strKey];
							$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['item_count'] 			+= $arrstrTransaction['item_count'];
							$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['transaction_amount']   	+= $arrstrTransaction['transaction_amount'];

							if( true == valArr( $arrintSalesTaxTransactions ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTransactions ) ) {
								$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['sales_tax']			+= $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
								$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['sales_tax_state']	+= $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getStateOrProvince();
							}

						} else {
							$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['item_count'] 			= $arrstrTransaction['item_count'];
							$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['transaction_amount']   	= $arrstrTransaction['transaction_amount'];
						}

						$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['id'] 					= $arrstrTransaction['id'];
						$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['property_id'] 			= $arrstrTransaction['property_id'];
						$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['charge_code_id'] 		= $arrstrTransaction['charge_code_id'];
						$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['transaction_datetime'] 	= \Psi\CStringService::singleton()->substr( $arrstrTransaction['transaction_datetime'], 0, 10 );
						$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['charge_code']  			= $this->m_arrobjChargeCodes[$arrstrTransaction['charge_code_id']]->getName();
						$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['memo'] 					= is_null( $arrstrTransaction['memo'] ) ? '': \Psi\CStringService::singleton()->substr( $arrstrTransaction['memo'], 0, 100 );

						if( true == valArr( $arrintSalesTaxTransactions ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTransactions ) ) {
							$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['sales_tax']			= $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
							$arrmixUtilityTransactions[$intNonRecurringTransactionsIndex]['sales_tax_state']	= $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getStateOrProvince();
						}

						$arrintUtilityTransactionCount[$strKey] = $intNonRecurringTransactionsIndex;

						$intNonRecurringTransactionsIndex = $intTempNonRecurringTransactionsIndex;

					} else {
						// show_bundle_pricing
						if( false == is_null( $arrstrTransaction['bundle_ps_product_id'] ) ) {

							$boolRecurringChargesAvailable = true;
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['bundle_ps_product_id']	= $arrstrTransaction['bundle_ps_product_id'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['transaction_amount']	= $arrstrTransaction['transaction_amount'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['transaction_datetime']	= $arrstrTransaction['transaction_datetime'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['name']					= $arrstrTransaction['name'];
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['charge_code'][$intNonRecurringTransactionsIndex]	= $this->m_arrobjChargeCodes[$arrstrTransaction['charge_code_id']]->getName();
							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['memo'][$intNonRecurringTransactionsIndex]			= $arrstrTransaction['memo'];

							if( true == valArr( $arrintSalesTaxTransactions ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTransactions ) ) {
								if( true == isset( $arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] ) ) {
									$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax']	+= $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
								} else {
									$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] = $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
								}
							}

						} else {
							$arrmixNonRecurringTransactions[$intNonRecurringTransactionsIndex]['id'] 					= $arrstrTransaction['id'];
							$arrmixNonRecurringTransactions[$intNonRecurringTransactionsIndex]['charge_code_id'] 		= $arrstrTransaction['charge_code_id'];
							$arrmixNonRecurringTransactions[$intNonRecurringTransactionsIndex]['item_count'] 			= $arrstrTransaction['item_count'];
							$arrmixNonRecurringTransactions[$intNonRecurringTransactionsIndex]['transaction_datetime'] 	= \Psi\CStringService::singleton()->substr( $arrstrTransaction['transaction_datetime'], 0, 10 );
							$arrmixNonRecurringTransactions[$intNonRecurringTransactionsIndex]['charge_code']  			= $this->m_arrobjChargeCodes[$arrstrTransaction['charge_code_id']]->getName();
							$arrmixNonRecurringTransactions[$intNonRecurringTransactionsIndex]['memo'] 					= is_null( $arrstrTransaction['memo'] ) ? '': ' : ' . \Psi\CStringService::singleton()->substr( $arrstrTransaction['memo'], 0, 100 );
							$arrmixNonRecurringTransactions[$intNonRecurringTransactionsIndex]['transaction_amount']   	= $arrstrTransaction['transaction_amount'];

							if( true == valArr( $arrintSalesTaxTransactions ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTransactions ) ) {
								$arrmixNonRecurringTransactions[$intNonRecurringTransactionsIndex]['sales_tax']	= $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
							}
						}

						$intNonRecurringTransactionsIndex++;
					}
				}
			}
		}

		// Row for bundle Price in Invoices:-
		if( true == $boolRecurringChargesAvailable && true == valArr( $arrstrBundles ) ) {

			$fltTotalBundleCharges = 0;
			$fltSalesTaxForBundles = 0;

			foreach( $arrstrBundles as $arrstrBundle ) {

				$fltTotalBundleCharges = 0;

				$objCurrentWorksheet->mergeCells( 'C' . $intCurrentRowNumber . ':' . ( 'E' . $intCurrentRowNumber ) );
				$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );
				$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, \Psi\CStringService::singleton()->substr( $arrstrBundle['transaction_datetime'], 0, 10 ) );
				$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, \Psi\CStringService::singleton()->substr( $arrstrBundle['name'], 0, 10 ) );

				$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );
				$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, ' ' . __( '{%m, 0, p:2}', [ $arrstrBundle['transaction_amount'] ] ) );

				// sales tax
				$objCurrentWorksheet->getStyle( 'G' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

				if( true == isset( $arrstrBundle['sales_tax'] ) && 0 != $arrstrBundle['sales_tax'] ) {
					$fltSalesTaxForBundles = $arrstrBundle['sales_tax'];
					$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, '' . __( '{%m, 0, p:2}', [ $fltSalesTaxForBundles ] ) );
				} else {
					$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, ' - ' );
				}

				$fltTotalBundleCharges += $fltSalesTaxForBundles + $arrstrBundle['transaction_amount'];

				$objCurrentWorksheet->getStyle( 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' ' . __( '{%m, 0, p:2}', [ $fltTotalBundleCharges ] ) );

				$intCurrentRowNumber++;

				foreach( $arrstrBundle['memo'] as $strMemo ) {
					$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, $strMemo );
					$intCurrentRowNumber++;
				}
			}
		}

		if( false == $boolRecurringChargesDataExists && 0 == $this->getIsCreditMemo() ) {
			// row No charges posted
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_style_italic' ) );
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'No recurring charges available for this period.' );
		}

		// row  Blank
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		// row New Charges
		if( true == $boolRecurringChargesDataExists && 0 == $this->getIsCreditMemo() ) {
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Total Recurring Charges:' );

			$fltSalesTaxRecurringChargesTotal = $this->getSalesTaxRecurringChargesTotal() + $this->getNewRecurringChargesTotal();

			if( 0 > $fltSalesTaxRecurringChargesTotal ) {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' ( ' . __( '{%m, 0, p:2}', [ ( - 1 ) * $fltSalesTaxRecurringChargesTotal ] ) . ' )' );
			} else {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $fltSalesTaxRecurringChargesTotal ] ) );
			}
		}

		// row  Blank
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		// Set up charges section
		if( true == valArr( $arrmixSetUpCharges ) ) {
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Setup Charges' );

			$intCurrentRowNumber++;

			$objCurrentWorksheet->mergeCells( 'C' . $intCurrentRowNumber . ':' . 'E' . $intCurrentRowNumber );
			$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, 'Date' );
			$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, 'Description' );
			$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, 'Subtotal' );
			$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, 'Sales Tax' );
			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, 'Amount' );

			$intSerialNumber = 1;

			foreach( $arrmixSetUpCharges as $arrmixSetUpCharge ) {

				$fltTotalSetupCharges 		= 0;
				$fltSalesTaxOfSetUpCharges  = 0;

				// row  Charges Posted During Period Details
				$intCurrentRowNumber++;

				$objCurrentWorksheet->mergeCells( 'C' . $intCurrentRowNumber . ':' . 'E' . $intCurrentRowNumber );
				$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );
				$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, \Psi\CStringService::singleton()->substr( $arrmixSetUpCharge['transaction_datetime'], 0, 10 ) );

				if( false == is_null( $arrmixSetUpCharge['memo'] ) ) {
					$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, $arrmixSetUpCharge['charge_code'] . \Psi\CStringService::singleton()->substr( $arrmixSetUpCharge['memo'], 0, 100 ) );
				} else {
					$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, $this->m_arrobjChargeCodes[$arrmixSetUpCharge['charge_code_id']]->getName() );
				}

				$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );

				if( 0 > $arrmixSetUpCharge['transaction_amount'] ) {
					$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, '( ' . __( '{%m, 0, p:2}', [ ( - 1 ) * $arrmixSetUpCharge['transaction_amount'] ] ) . ' )' );
				} else {
					$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, ' ' . __( '{%m, 0, p:2}', [ $arrmixSetUpCharge['transaction_amount'] ] ) );
				}

				$objCurrentWorksheet->getStyle( 'G' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

				if( true == isset( $arrmixSetUpCharge['sales_tax'] ) && 0 != $arrmixSetUpCharge['sales_tax'] ) {
					$fltSalesTaxOfSetUpCharges = $arrmixSetUpCharge['sales_tax'];
					$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, ' ' . __( '{%m, 0, p:2}', [ $fltSalesTaxOfSetUpCharges ] ) );
				} else {
					$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, ' - ' );
				}

				$fltTotalSetupCharges += $arrmixSetUpCharge['transaction_amount'] + $fltSalesTaxOfSetUpCharges;

				$objCurrentWorksheet->getStyle( 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

				if( 0 > $fltTotalSetupCharges ) {
					$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, '( ' . __( '{%m, 0, p:2}', [ ( - 1 ) * $fltTotalSetupCharges ] ) . ' )' );
				} else {
					$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' ' . __( '{%m, 0, p:2}', [ $fltTotalSetupCharges ] ) );
				}
			}

			// row  Blank
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

			if( true == $boolSetupChargeDataExists && 0 == $this->getIsCreditMemo() ) {
				$intCurrentRowNumber++;
				$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
				$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
				$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
				$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Total Setup Charges:' );

				$fltSalesTaxSetupChargesTotal = $this->getSalesTaxSetupChargesTotal() + $this->getNewSetUpChargesTotal();

				if( 0 > $fltSalesTaxSetupChargesTotal ) {
					$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber,  ' ( ' . __( '{%m, 0, p:2}', [ ( - 1 ) * $fltSalesTaxSetupChargesTotal ] ) . ' )' );
				} else {
					$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $fltSalesTaxSetupChargesTotal ] ) );
				}
			}
		}

		// row  Blank
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		// start charges section
		// row  Charges Posted During Period
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );

		if( 1 == $this->getIsCreditMemo() ) {
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Credits Issued' );
		} else {
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Other Charges' );
		}

		if( true == valArr( $arrmixNonRecurringTransactions ) ) {

			// row  Charges Posted During Period Heading
			$intCurrentRowNumber++;

			$objCurrentWorksheet->mergeCells( 'C' . $intCurrentRowNumber . ':' . 'E' . $intCurrentRowNumber );
			$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, 'Date' );
			$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, 'Description' );
			$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, 'Subtotal' );
			$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, 'Sales Tax' );
			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, 'Amount' );

			$intSerialNumber = 1;
			$fltTotalAmount = 0;
			$boolChargeDataExists = true;

			foreach( $arrmixNonRecurringTransactions as $arrmixNonRecurringTransaction ) {

				$fltTotalOtherCharges 	   = 0;
				$fltSalesTaxOfOtherCharges = 0;

				// row  Charges Posted During Period Details
				$intCurrentRowNumber++;

				$objCurrentWorksheet->mergeCells( 'C' . $intCurrentRowNumber . ':' . 'E' . $intCurrentRowNumber );
				$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );
				$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, \Psi\CStringService::singleton()->substr( $arrmixNonRecurringTransaction['transaction_datetime'], 0, 10 ) );

				if( false == is_null( $arrmixNonRecurringTransaction['memo'] ) ) {
					$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, $arrmixNonRecurringTransaction['charge_code'] . \Psi\CStringService::singleton()->substr( $arrmixNonRecurringTransaction['memo'], 0, 100 ) );
				} else {
					$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, $this->m_arrobjChargeCodes[$arrmixNonRecurringTransaction['charge_code_id']]->getName() );
				}

				$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

				if( 0 < $arrmixNonRecurringTransaction['transaction_amount'] ) {
					$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, __( '{%m, 0, p:2}', ' ' . $arrmixNonRecurringTransaction['transaction_amount'] ) );
				} else {
					$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, '( ' . __( '{%m, 0, p:2}', [ ( - 1 ) * $arrmixNonRecurringTransaction['transaction_amount'] ] ) . ' )' );
				}

				$objCurrentWorksheet->getStyle( 'G' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

				if( true == isset( $arrmixNonRecurringTransaction['sales_tax'] ) && 0 != $arrmixNonRecurringTransaction['sales_tax'] ) {
					$fltSalesTaxOfOtherCharges = $arrmixNonRecurringTransaction['sales_tax'];
					$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, __( '{%m, 0, p:2}', $fltSalesTaxOfOtherCharges ) );
				} else {
					$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, ' - ' );
				}

				$fltTotalOtherCharges += $arrmixNonRecurringTransaction['transaction_amount'] + $fltSalesTaxOfOtherCharges;

				$objCurrentWorksheet->getStyle( 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

				if( 0 > $fltTotalOtherCharges ) {
					$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, '( ' . __( '{%m, 0, p:2}', [ ( - 1 ) * $fltTotalOtherCharges ] ) . ' )' );
				} else {
					$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', $fltTotalOtherCharges ) );
				}

				$fltTotalAmount += $fltTotalOtherCharges;
			}

			// row  Blank
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

			// row New Charges
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );

			if( 1 == $this->getIsCreditMemo() ) {
				$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Total Credits Issued:' );
			} else {
				$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Total Other Charges:' );
			}

			if( 0 > $fltTotalAmount ) {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' ( ' . __( '{%m, 0, p:2}', [ ( -1 ) * $fltTotalAmount ] ) . ' )' );
			} else {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $fltTotalAmount ] ) );
			}

		}

		// row Utility Charge
		if( true == valArr( $arrmixUtilityTransactions ) && 0 == $this->getIsCreditMemo() ) {

			// row  Blank
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );

			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Resident Utility Charges' );

			$intCurrentRowNumber++;

			$objCurrentWorksheet->mergeCells( 'C' . $intCurrentRowNumber . ':' . 'E' . $intCurrentRowNumber );
			$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, 'Date' );
			$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, 'Description' );
			$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, 'Subtotal' );
			$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, 'Sales Tax' );
			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, 'Amount' );

			$intSerialNumber = 1;
			$fltTotalAmount = 0;
			$boolChargeDataExists = true;

			foreach( $arrmixUtilityTransactions as $arrmixUtilityTransaction ) {

				$fltTotalUtilityCharges 	   = 0;
				$fltSalesTaxOfOtherCharges = 0;
				$intCurrentRowNumber++;

				$objCurrentWorksheet->mergeCells( 'C' . $intCurrentRowNumber . ':' . 'E' . $intCurrentRowNumber );
				$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );
				$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, \Psi\CStringService::singleton()->substr( $arrmixUtilityTransaction['transaction_datetime'], 0, 10 ) );

				if( false == is_null( $arrmixUtilityTransaction['memo'] ) ) {
					$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, $arrmixUtilityTransaction['charge_code'] . \Psi\CStringService::singleton()->substr( $arrmixUtilityTransaction['memo'], 0, 100 ) );
				} else {
					$objCurrentWorksheet->setCellValue( 'C' . $intCurrentRowNumber, $this->m_arrobjChargeCodes[$arrmixUtilityTransaction['charge_code_id']]->getName() );
				}

				$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

				if( 0 < $arrmixUtilityTransaction['transaction_amount'] ) {
					$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, __( '{%m, 0, p:2}', ' ' . $arrmixUtilityTransaction['transaction_amount'] ) );
				} else {
					$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, '( ' . __( '{%m, 0, p:2}', [ ( - 1 ) * $arrmixUtilityTransaction['transaction_amount'] ] ) . ' )' );
				}

				$objCurrentWorksheet->getStyle( 'G' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

				if( true == isset( $arrmixUtilityTransaction['sales_tax'] ) && 0 != $arrmixUtilityTransaction['sales_tax'] ) {
					$fltSalesTaxOfOtherCharges = $arrmixUtilityTransaction['sales_tax'];
					$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, $fltSalesTaxOfOtherCharges );
				} else {
					$objCurrentWorksheet->setCellValue( 'G' . $intCurrentRowNumber, ' - ' );
				}

				$fltTotalUtilityCharges += $arrmixUtilityTransaction['transaction_amount'] + $fltSalesTaxOfOtherCharges;

				$objCurrentWorksheet->getStyle( 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );

				if( 0 > $fltTotalUtilityCharges ) {
					$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, '( ' . __( '{%m, 0, p:2}', [ ( - 1 ) * $fltTotalUtilityCharges ] ) . ' )' );
				} else {
					$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', $fltTotalUtilityCharges ) );
				}

				$fltTotalAmount += $fltTotalUtilityCharges;
			}

			// row  Blank
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

			// row New Charges
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );

			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Total Utility Charges:' );

			if( 0 > $fltTotalAmount ) {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' ( ' . __( '{%m, 0, p:2}', [ ( -1 ) * $fltTotalAmount ] ) . ' )' );
			} else {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $fltTotalAmount ] ) );
			}
		}
		// end Utility Charge

		if( false == $boolChargeDataExists ) {
			// row No charges posted
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_style_italic' ) );
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'No charges posted during period.' );
		}

		return $objCurrentWorksheet;
	}

	/**
	 * @param \CDatabase          $objAdminDatabase
	 * @param \CDatabase          $objUtilitiesDatabase
	 * @param \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $objCurrentWorksheet
	 * @throws \PhpOffice\PhpSpreadsheet\Exception
	 */
	public function loadUtilityBillingTransactionXlsxGrid( $objAdminDatabase, $objUtilitiesDatabase, $objCurrentWorksheet ) {

		$intCurrentRowNumber = $objCurrentWorksheet->getHighestRow();

		$this->m_objUtilityBatch = $this->getOrFetchUtilityBatch( $objUtilitiesDatabase );

		if( false == valObj( $this->m_objUtilityBatch, 'CUtilityBatch' ) ) {
			trigger_error( 'Utility batch failed to load.', E_USER_ERROR );
		}

		// This query loads all the transaction data we need to generate the invoice.
		$this->m_arrstrPropertyUtilityTypeRateGroupedData = $this->getOrFetchPropertyUtilityTypeRateGroupedData( [ $this->m_objUtilityBatch->getId() ], $objAdminDatabase, $objUtilitiesDatabase );

		$arrstrBillingDates = $arrintPropertyUtilityTypeRateIds = [];
		if( true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['BillingAndUsage'] ) ) {
			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['BillingAndUsage'] as $intPropertyUtilityTypeRateId => $arrstrPropertyUtilityTypeRateData ) {
				$intPropertyUtilityTypeId = $arrstrPropertyUtilityTypeRateData['property_utility_type_id'];
				$arrintPropertyUtilityTypeRateIds[$intPropertyUtilityTypeRateId] = $intPropertyUtilityTypeId;

				if( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) {
					$arrstrBillingDates['start_date'][] = strtotime( $arrstrPropertyUtilityTypeRateData['billing_start_date'] );
				}
				if( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) {
					$arrstrBillingDates['end_date'][] = strtotime( $arrstrPropertyUtilityTypeRateData['billing_end_date'] );
				}
			}
		}

		$boolMultipleLeaseBucket = false;
		if( true == valArr( $arrintPropertyUtilityTypeRateIds ) ) {
			foreach( $arrintPropertyUtilityTypeRateIds as $intPropertyUtilityTypeRateId => $intPropertyUtilityTypeId ) {
				if( false == ( '' == $intPropertyUtilityTypeId ) ) {
					$arrintPropertyUtilityTypeRateIds = array_keys( $arrintPropertyUtilityTypeRateIds, $intPropertyUtilityTypeId );
					if( 1 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyUtilityTypeRateIds ) ) {
						// there is multiple lease bucket for one property utility type
						$boolMultipleLeaseBucket = true;
					}
				}
			}
		}

		$this->m_arrobjChargeCodes = $this->getOrFetchChargeCodes( $objAdminDatabase );

		$objCurrentWorksheet->freezePane( 'D1' );

		// row  Total Units , Occupied Units:
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'D' . $intCurrentRowNumber );
		$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'D' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'border_outline' ) );
		$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'border_outline' ) );
		$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Total Units: ' . $this->m_objUtilityBatch->getTotalUnits() );

		// row  Residence Charges, Property Charges
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'I' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'red_white_title' ) );
		$objCurrentWorksheet->mergeCells( 'I' . $intCurrentRowNumber . ':' . 'L' . $intCurrentRowNumber );
		$objCurrentWorksheet->setCellValue( 'I' . $intCurrentRowNumber, 'RESIDENT CHARGES' );
		$objCurrentWorksheet->mergeCells( 'M' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber );
		$objCurrentWorksheet->setCellValue( 'M' . $intCurrentRowNumber, 'PROPERTY CHARGES' );

		// row Billing Usages Heading
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'B' . $intCurrentRowNumber . ':' . 'C' . $intCurrentRowNumber );
		$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
		$objCurrentWorksheet->mergeCells( 'I' . $intCurrentRowNumber . ':' . 'J' . $intCurrentRowNumber );
		$objCurrentWorksheet->mergeCells( 'M' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_center' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'border_top' ) );
		$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, 'Services' );

		if( true == $boolMultipleLeaseBucket ) {
			$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
			$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, 'Lease Period' );
			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, 'Usage Period' );
		} else {
			$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
			$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, 'Service Period' );
		}

		$objCurrentWorksheet->setCellValue( 'I' . $intCurrentRowNumber, 'Utilities' );
		$objCurrentWorksheet->setCellValue( 'K' . $intCurrentRowNumber, 'Billing Fee' );
		$objCurrentWorksheet->setCellValue( 'L' . $intCurrentRowNumber, 'Total' );
		$objCurrentWorksheet->setCellValue( 'M' . $intCurrentRowNumber, 'Billing Fee' );

		$fltOverallResidentTotal	= 0;
		$fltOverallPropertyTotal	= 0;
		$intSerialNumber			= 1;

		$strStartDate = $strEndDate = NULL;
		if( true == array_key_exists( 'start_date', $arrstrBillingDates ) ) {
			$arrstrDate = getArrayElementByKey( 'start_date', $arrstrBillingDates );
			\Psi\CStringService::singleton()->sort( $arrstrDate );
			$strStartDate = date( 'm/d/Y', array_shift( $arrstrDate ) );

			$arrstrDate = getArrayElementByKey( 'end_date', $arrstrBillingDates );
			\Psi\CStringService::singleton()->sort( $arrstrDate );
			$strEndDate = date( 'm/d/Y', array_pop( $arrstrDate ) );

		}

		// Add Billing & Usage Fees
		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['BillingAndUsage'] ) && true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['BillingAndUsage'] ) ) {

			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['BillingAndUsage'] as $intPropertyUtilityTypeRateId => $arrstrPropertyUtilityTypeRateData ) {

				$strLeasePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) ? date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) : 'All';
				$strLeasePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) ? ' - ' . date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) : ' - All';

				if( true == is_null( $strStartDate ) ) {
					$strStartDate = date( 'm/d/Y', strtotime( '-1 month', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) ) + ( 3600 * 24 ) );
					$strEndDate	  = $this->m_objUtilityBatch->getDistributionDeadline();
				}

				$strUsagePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) ? date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) : date( 'n/j', strtotime( $strStartDate ) );
				$strUsagePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) ? ' - ' . date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) : ' - ' . date( 'n/j', strtotime( $strEndDate ) );

				// row Billing & UsageDetail
				$intCurrentRowNumber++;
				$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
				$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );

				$objCurrentWorksheet->mergeCells( 'B' . $intCurrentRowNumber . ':' . 'C' . $intCurrentRowNumber );
				$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, $arrstrPropertyUtilityTypeRateData['name'] );

				if( true == $boolMultipleLeaseBucket ) {
					$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
					$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, $strLeasePeriod );
					$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, $strUsagePeriod );
				} else {
					$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
					$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, $strUsagePeriod );
				}
				// Here we need to find out the following:
				// 1. How much utility usage (sum estimated and actual)
				// 2. How much in billing fees

				$fltTotalResidentBillingFees 	= 0;
				$fltTotalPropertyBillingFees 	= 0;
				$fltTotalUtilityConsumption 	= 0;
				$intTotalTransactionCount 		= 0;

				// Loop on the utility transaction types in the array and summarize billing fees along with estimated and actual utility usage
				if( true == valArr( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] ) ) {

					foreach( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] as $intUtilityTransactionTypeId => $arrstrUtilityTransactionTypeData ) {

						if( true == valArr( $arrstrUtilityTransactionTypeData ) ) {

							foreach( $arrstrUtilityTransactionTypeData as $intTransactionId => $arrstrTransactionData ) {

								switch( $intUtilityTransactionTypeId ) {

									case CUtilityTransactionType::ESTIMATED_USAGE:
									case CUtilityTransactionType::ACTUAL_USAGE:
									case CUtilityTransactionType::BASE_FEE:

										// Estimated and actual usage charges should never have corresponding transaction ids.  If it does, throw an error:

										if( true == is_numeric( $intTransactionId ) ) {
											trigger_error( 'Estimated and actual utility transactions should never have associated transactions.', E_USER_ERROR );
										}

										$fltTotalUtilityConsumption += ( float ) $arrstrTransactionData['utility_transaction_sum'];
										break;

									case CUtilityTransactionType::BILLING_FEE:
										$intTotalTransactionCount += ( float ) $arrstrTransactionData['utility_transaction_count'];
										$fltTotalResidentBillingFees += ( float ) $arrstrTransactionData['utility_transaction_sum'];

										// if( false == isset( $arrstrTransactionData['total_transactions_sum'] ) ) {
										// trigger_error( 'Looks like there was an error posting transactions during the utility invoice batch posting.  Billing fee utility transactions should always have accompanying transaction sums.', E_USER_ERROR );
										// }

										$fltTotalPropertyBillingFees += ( true == isset( $arrstrTransactionData['total_transactions_sum'] ) ) ? ( float ) $arrstrTransactionData['total_transactions_sum'] : 0;
										break;

									default:
										trigger_error( 'Erroneous utility transaction type for usage and billing fees (cound happen if a invoice gets wrongly associated to a utility_batch).', E_USER_ERROR );
										break;
								}
							}
						}
					}
				}

				// Find out the property and resident cost per bill

				$objCurrentWorksheet->mergeCells( 'I' . $intCurrentRowNumber . ':' . 'J' . $intCurrentRowNumber );
				$objCurrentWorksheet->mergeCells( 'M' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber );
				$objCurrentWorksheet->getStyle( 'I' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
				$objCurrentWorksheet->setCellValueExplicit( 'K' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentBillingFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
				$objCurrentWorksheet->setCellValueExplicit( 'I' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalUtilityConsumption ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
				$objCurrentWorksheet->setCellValueExplicit( 'L' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentBillingFees + ( float ) $fltTotalUtilityConsumption ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
				$objCurrentWorksheet->setCellValueExplicit( 'M' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalPropertyBillingFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );

				$fltOverallResidentTotal += ( ( float ) $fltTotalResidentBillingFees + ( float ) $fltTotalUtilityConsumption );
				$fltOverallPropertyTotal += ( float ) $fltTotalPropertyBillingFees;
			}
		}

		// Add Move-in & Termination Fees
		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['MoveInAndTermination'] ) && true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['MoveInAndTermination'] ) ) {
			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['MoveInAndTermination'] as $intPropertyUtilityTypeRateId => $arrstrPropertyUtilityTypeRateData ) {

				$strLeasePeriod	= ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) ? date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) : 'All';
				$strLeasePeriod	.= ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) ? ' - ' . date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) : ' - All';

				if( true == is_null( $strStartDate ) ) {
					$strStartDate = date( 'm/d/Y', strtotime( '-1 month', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) ) + ( 3600 * 24 ) );
					$strEndDate	  = $this->m_objUtilityBatch->getDistributionDeadline();
				}

				$strUsagePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) ? date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) : date( 'n/j', strtotime( $strStartDate ) );
				$strUsagePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) ? ' - ' . date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) : ' - ' . date( 'n/j', strtotime( $strEndDate ) );

				if( true == valArr( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] ) ) {

					foreach( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] as $intUtilityTransactionTypeId => $arrstrUtilityTransactionTypeData ) {

						switch( $intUtilityTransactionTypeId ) {

							case CUtilityTransactionType::MOVE_IN_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['MoveInAndTermination'] ) ) ? ' Move-in Fees ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'Move-in Fees';
								break;

							case CUtilityTransactionType::TERMINATION_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['MoveInAndTermination'] ) ) ? ' Termination Fees ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'Termination Fees';
								break;

							default:
								trigger_error( 'Erroneous utility transaction type for move in and termination fees.', E_USER_ERROR );
								break;
						}

						// Summarize the charge groupings (should only be one)
						$fltTotalResidentFees 			= 0;
						$fltTotalPropertyFees 			= 0;
						$intTotalTransactionCount 		= 0;

						if( true == valArr( $arrstrUtilityTransactionTypeData ) ) {

							foreach( $arrstrUtilityTransactionTypeData as $intTransactionId => $arrstrTransactionData ) {

								$fltTotalResidentFees 		= ( float ) $arrstrTransactionData['utility_transaction_sum'];
								$fltTotalPropertyFees 		= ( true == isset( $arrstrTransactionData['total_transactions_sum'] ) ) ? ( float ) $arrstrTransactionData['total_transactions_sum'] : 0;
								$intTotalTransactionCount 	= ( float ) $arrstrTransactionData['utility_transaction_count'];
							}
						}

						// row Details
						$intCurrentRowNumber++;
						$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
						$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );

						$objCurrentWorksheet->mergeCells( 'B' . $intCurrentRowNumber . ':' . 'C' . $intCurrentRowNumber );
						$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, $strName );

						if( true == $boolMultipleLeaseBucket ) {
							$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
							$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, $strLeasePeriod );
							$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, $strUsagePeriod );
						} else {
							$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
							$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, $strUsagePeriod );
						}

						// Find out the property and resident cost per bill

						$fltTotalPropertyFees = ( 0 == $fltTotalPropertyFees ) ? '-' : __( '{%f, 0, p:2}', [ ( float ) $fltTotalPropertyFees ] );

						$objCurrentWorksheet->mergeCells( 'I' . $intCurrentRowNumber . ':' . 'J' . $intCurrentRowNumber );
						$objCurrentWorksheet->mergeCells( 'M' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber );
						$objCurrentWorksheet->getStyle( 'I' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
						$objCurrentWorksheet->setCellValueExplicit( 'K' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
						$objCurrentWorksheet->setCellValueExplicit( 'I' . $intCurrentRowNumber, '-', PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
						$objCurrentWorksheet->setCellValueExplicit( 'L' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
						$objCurrentWorksheet->setCellValueExplicit( 'M' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalPropertyFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );

						$fltOverallResidentTotal += ( float ) $fltTotalResidentFees;
						$fltOverallPropertyTotal += ( float ) $fltTotalPropertyFees;
					}
				}
			}
		}

		// Add VCR Charges
		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] ) && true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] ) ) {

			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] as $intPropertyUtilityTypeRateId => $arrstrPropertyUtilityTypeRateData ) {

				$strLeasePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) ? date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) : 'All';
				$strLeasePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) ? ' - ' . date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) : ' - All';

				$strUsagePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) ? date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) : date( 'n/j', strtotime( '-1 month', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) ) + ( 3600 * 24 ) );
				$strUsagePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) ? ' - ' . date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) : ' - ' . date( 'n/j', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) );

				if( true == valArr( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] ) ) {

					foreach( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] as $intUtilityTransactionTypeId => $arrstrUtilityTransactionTypeData ) {

						switch( $intUtilityTransactionTypeId ) {

							case CUtilityTransactionType::VCR_USAGE_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] ) ) ? ' VCR Usage Fees ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'VCR Usage Fees';
								break;

							case CUtilityTransactionType::VCR_SERVICE_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] ) ) ? ' VCR Penalties ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'VCR Penalties';
								break;

							case CUtilityTransactionType::VCR_LATE_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] ) ) ? ' VCR Late Fees ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'VCR Late Fees';
								break;

							default:
								trigger_error( 'Erroneous utility transaction type for vcr fees.', E_USER_ERROR );
								break;
						}

						// Summarize the charge groupings (should only be one)
						$fltTotalResidentFees 			= 0;
						$fltTotalPropertyFees 			= 0;
						$intTotalTransactionCount 		= 0;

						if( true == valArr( $arrstrUtilityTransactionTypeData ) ) {

							foreach( $arrstrUtilityTransactionTypeData as $intTransactionId => $arrstrTransactionData ) {

								$fltTotalResidentFees 			= ( float ) $arrstrTransactionData['utility_transaction_sum'];
								$fltTotalPropertyFees 			= ( true == isset( $arrstrTransactionData['total_transactions_sum'] ) ) ? ( float ) $arrstrTransactionData['total_transactions_sum'] : 0;
								$intTotalTransactionCount 		= ( float ) $arrstrTransactionData['utility_transaction_count'];
							}
						}

						// row  VCR Charge Details
						$intCurrentRowNumber++;
						$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
						$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );

						$objCurrentWorksheet->mergeCells( 'B' . $intCurrentRowNumber . ':' . 'C' . $intCurrentRowNumber );
						$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, $strName );

						if( true == $boolMultipleLeaseBucket ) {
							$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
							$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, $strLeasePeriod );
							$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, $strUsagePeriod );
						} else {
							$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
							$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, $strUsagePeriod );
						}

						// Find out the property and resident cost per bill

						$fltTotalPropertyFees = ( 0 == $fltTotalPropertyFees ) ? '-' : __( '{%f, 0, p:2}', [ ( float ) $fltTotalPropertyFees ] );

						$objCurrentWorksheet->mergeCells( 'I' . $intCurrentRowNumber . ':' . 'J' . $intCurrentRowNumber );
						$objCurrentWorksheet->mergeCells( 'M' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber );
						$objCurrentWorksheet->getStyle( 'I' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );

						if( $intUtilityTransactionTypeId == CUtilityTransactionType::VCR_USAGE_FEE ) {

							$objCurrentWorksheet->setCellValueExplicit( 'K' . $intCurrentRowNumber, '-', PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
							$objCurrentWorksheet->setCellValueExplicit( 'I' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
						} else {

							$objCurrentWorksheet->setCellValueExplicit( 'K' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltResidentCostPerTransaction ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
							$objCurrentWorksheet->setCellValueExplicit( 'I' . $intCurrentRowNumber, '-', PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
						}

						$objCurrentWorksheet->setCellValueExplicit( 'L' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
						$objCurrentWorksheet->setCellValueExplicit( 'M' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalPropertyFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );

						$fltOverallResidentTotal += ( float ) $fltTotalResidentFees;
						$fltOverallPropertyTotal += ( float ) $fltTotalPropertyFees;
					}
				}
			}
		}

		// Add VCR Analysis Fees here. (comes right from transactions table)
		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'][CChargeCode::UTILITY_VCR_PER_UNIT_FEES] ) && 0 != ( float ) $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'][CChargeCode::UTILITY_VCR_PER_UNIT_FEES] ) {

			$strUsagePeriod = date( 'n/j', strtotime( '-1 month', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) ) + ( 3600 * 24 ) );
			$strUsagePeriod .= ' - ' . date( 'n/j', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) );

			// row  VCR Analysis Fees Details
			$intCurrentRowNumber++;
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );

			$objCurrentWorksheet->mergeCells( 'B' . $intCurrentRowNumber . ':' . 'C' . $intCurrentRowNumber );
			$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, 'VCR Analysis Fees' );

			if( true == $boolMultipleLeaseBucket ) {
				$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
				$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, '-' );
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, $strUsagePeriod );
			} else {
				$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
				$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, $strUsagePeriod );
			}

			$objCurrentWorksheet->mergeCells( 'I' . $intCurrentRowNumber . ':' . 'J' . $intCurrentRowNumber );
			$objCurrentWorksheet->mergeCells( 'M' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'I' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
			$objCurrentWorksheet->setCellValueExplicit( 'K' . $intCurrentRowNumber, '-', PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
			$objCurrentWorksheet->setCellValueExplicit( 'I' . $intCurrentRowNumber, '-', PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
			$objCurrentWorksheet->setCellValueExplicit( 'L' . $intCurrentRowNumber, '-', PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
			$objCurrentWorksheet->setCellValueExplicit( 'M' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'][CChargeCode::UTILITY_VCR_PER_UNIT_FEES] ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );

			$fltOverallPropertyTotal += ( float ) $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'][CChargeCode::UTILITY_VCR_PER_UNIT_FEES];
		}

		// Add Late Fees here.
		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['LateFees'] ) && true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['LateFees'] ) ) {

			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['LateFees'] as $intPropertyUtilityTypeRateId => $arrstrPropertyUtilityTypeRateData ) {

				$strLeasePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) ? date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) : 'All';
				$strLeasePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) ? ' - ' . date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) : ' - All';

				$strUsagePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) ? date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) : date( 'n/j', strtotime( '-1 month', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) ) + ( 3600 * 24 ) );
				$strUsagePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) ? ' - ' . date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) : ' - ' . date( 'n/j', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) );

				if( true == valArr( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] ) ) {

					foreach( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] as $intUtilityTransactionTypeId => $arrstrUtilityTransactionTypeData ) {

						switch( $intUtilityTransactionTypeId ) {

							case CUtilityTransactionType::LATE_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['LateFees'] ) ) ? ' Late Fees ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'Late Fees';
								break;

							default:
								trigger_error( 'Erroneous utility transaction type for late fees.', E_USER_ERROR );
								break;
						}

						// Summarize the charge groupings (should only be one)
						$fltTotalResidentFees 			= 0;
						$fltTotalPropertyFees 			= 0;
						$intTotalTransactionCount 		= 0;

						if( true == valArr( $arrstrUtilityTransactionTypeData ) ) {

							foreach( $arrstrUtilityTransactionTypeData as $intTransactionId => $arrstrTransactionData ) {

								$fltTotalResidentFees 		= ( float ) $arrstrTransactionData['utility_transaction_sum'];
								$fltTotalPropertyFees 		= ( true == isset( $arrstrTransactionData['total_transactions_sum'] ) ) ? ( float ) $arrstrTransactionData['total_transactions_sum'] : 0;
								$intTotalTransactionCount 	= ( float ) $arrstrTransactionData['utility_transaction_count'];
							}
						}

						// row  Late Fess Details
						$intCurrentRowNumber++;
						$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
						$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );

						$objCurrentWorksheet->mergeCells( 'B' . $intCurrentRowNumber . ':' . 'C' . $intCurrentRowNumber );
						$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, $strName );

						if( true == $boolMultipleLeaseBucket ) {
							$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
							$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, $strLeasePeriod );
							$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, $strUsagePeriod );
						} else {
							$objCurrentWorksheet->mergeCells( 'D' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
							$objCurrentWorksheet->setCellValue( 'D' . $intCurrentRowNumber, $strUsagePeriod );
						}

						// Find out the property and resident cost per bill

						$fltTotalPropertyFees = ( 0 == $fltTotalPropertyFees ) ? '-' : __( '{%f, 0, p:2}', [ ( float ) $fltTotalPropertyFees ] );

						$objCurrentWorksheet->mergeCells( 'I' . $intCurrentRowNumber . ':' . 'J' . $intCurrentRowNumber );
						$objCurrentWorksheet->mergeCells( 'M' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber );
						$objCurrentWorksheet->getStyle( 'I' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );

						if( $intUtilityTransactionTypeId == CUtilityTransactionType::VCR_USAGE_FEE ) {

							$objCurrentWorksheet->setCellValueExplicit( 'K' . $intCurrentRowNumber, '-', PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
							$objCurrentWorksheet->setCellValueExplicit( 'I' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
						} else {

							$objCurrentWorksheet->setCellValueExplicit( 'K' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
							$objCurrentWorksheet->setCellValueExplicit( 'I' . $intCurrentRowNumber, '-', PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );
						}

						$objCurrentWorksheet->setCellValueExplicit( 'L' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );

						$objCurrentWorksheet->setCellValueExplicit( 'M' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ ( float ) $fltTotalPropertyFees ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );

						$fltOverallResidentTotal += ( float ) $fltTotalResidentFees;
						$fltOverallPropertyTotal += ( float ) $fltTotalPropertyFees;
					}
				}
			}
		}

		// Now we need to add on any extra fees that are posted to the account, grouped by charge code
		$intLoopCount = 0;

		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'] ) && true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'] ) ) {

			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'] as $intChargeCodeId => $fltChargeAmount ) {

				if( false == in_array( $intChargeCodeId, [ CChargeCode::UTILITY_VCR_PER_UNIT_FEES, CChargeCode::PAYMENT_RECEIVED ] ) ) {

					// row  Extra Fees Details
					$intCurrentRowNumber++;
					$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
					$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $intSerialNumber++ );

					$objCurrentWorksheet->mergeCells( 'B' . $intCurrentRowNumber . ':' . 'C' . $intCurrentRowNumber );
					$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, truncate( $this->m_arrobjChargeCodes[$intChargeCodeId]->getName(), 60 ) );

					$objCurrentWorksheet->mergeCells( 'I' . $intCurrentRowNumber . ':' . 'J' . $intCurrentRowNumber );
					$objCurrentWorksheet->mergeCells( 'M' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber );
					$objCurrentWorksheet->getStyle( 'I' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
					$objCurrentWorksheet->setCellValueExplicit( 'M' . $intCurrentRowNumber, __( '{%f, 0, p:2}', [ $fltChargeAmount ] ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );

					$fltOverallPropertyTotal += ( float ) $fltChargeAmount;
				}
			}
		}

		// Add the total line
		// row  Total Line
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
		$objCurrentWorksheet->mergeCells( 'I' . $intCurrentRowNumber . ':' . 'K' . $intCurrentRowNumber );
		$objCurrentWorksheet->mergeCells( 'M' . $intCurrentRowNumber . ':' . 'N' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
		$objCurrentWorksheet->getStyle( 'L' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->getStyle( 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'O' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'border_bottom' ) );
		$objCurrentWorksheet->setCellValue( 'I' . $intCurrentRowNumber, 'Total' );
		$objCurrentWorksheet->setCellValue( 'L' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $fltOverallResidentTotal ] ) );
		$objCurrentWorksheet->setCellValue( 'M' . $intCurrentRowNumber, 'Total' );
		$objCurrentWorksheet->setCellValue( 'O' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $fltOverallPropertyTotal ] ) );

		// Include summary line with large text
		// row  summary line
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		// row Total Resident Charges
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 2 ) )->applyFromArray( $this->getStyle( 'border_outline' ) );
		$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Total Resident Charges' );
		$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $fltOverallResidentTotal ] ) );

		// row Total Property Charges
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Total Property Charges' );
		$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $fltOverallPropertyTotal ] ) );

		// row  Net Operating Income
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_color_red' ) );
		$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Net Operating Income' );
		$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ ( float ) $fltOverallResidentTotal - ( float ) $fltOverallPropertyTotal ] ) );

		return $objCurrentWorksheet;
	}

	public function addTransaction( $arrstrTransaction ) {
		if( true == is_numeric( $arrstrTransaction['id'] ) ) {
			$this->m_arrstrTransactions[$arrstrTransaction['id']] = $arrstrTransaction;
		} else {
			// Adding UN as the prefix in the key for transaction with no ID. So that this will be always unique and will never get overwritten by TransactionId. [SRB]
			$this->m_arrstrTransactions['UN' . \Psi\Libraries\UtilFunctions\count( $this->m_arrstrTransactions )] = $arrstrTransaction;
		}
	}

	public function addSuccessMsg( $objSuccessMsg ) {
		$this->m_arrobjSuccessMsgs[] = $objSuccessMsg;
	}

	public function generateInvoiceNumber() {
		$intInvoiceNumber = NULL;

		if( false == isset( $this->m_intId ) ) {
			trigger_error( 'Programming Error: You must fetch Id from invoices_id_seq sequence before attempting to generate an invoice number', E_USER_WARNING );
		} elseif( false == isset( $this->m_intAccountId ) ) {
			trigger_error( 'Programming Error: You must set the Account ID before attempting to generate an invoice number', E_USER_WARNING );
		} else {
			$intInvoiceNumber = self::calculateInvoiceNumber( $this->m_intId, $this->m_intAccountId );
		}

		return $intInvoiceNumber;
	}

	public function setInvoiceNumber( $intInvoiceNumber = NULL ) {

		// Always call generateInvoiceNumber when trying to set the invoice number, unless it's being called from setValues in the base class
		$arrmixBacktrace = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 2 );
		if( self::MIN_CHECK_DIGIT_REQUIRED < $this->m_intId && ( false == isset( $arrmixBacktrace[1] ) || 'CBaseInvoice' != $arrmixBacktrace[1]['class'] || 'setValues' != $arrmixBacktrace[1]['function'] ) ) {
			$intInvoiceNumber = $this->generateInvoiceNumber();
		}

		parent::setInvoiceNumber( $intInvoiceNumber );
	}

	public function allocateTransactions( $objDatabase ) {

		$strBatchDate 	= date( 'm/d/Y', strtotime( $this->getInvoiceDate() ) );

		// This sql updates the transactions for this invoice and sets the company invoice id on each of those transactions
		$strSql = 'UPDATE transactions
					SET invoice_id = ' . ( int ) $this->m_intId . '
					FROM accounts a
					WHERE
						transactions.cid = a.cid
						AND transactions.account_id = a.id
						AND transactions.invoice_id IS NULL
						AND transactions.cid = ' . $this->getCid() . '
						AND transactions.account_id = ' . $this->getAccountId() . '
						AND ( DATE_TRUNC( \'day\', transactions.transaction_datetime ) <= \'' . $strBatchDate . '\' OR DATE_TRUNC( \'day\', transactions.post_month ) <= \'' . $strBatchDate . '\')';

		$objDataset = $objDatabase->createDataset();

		if( true == $objDataset->execute( $strSql ) ) {
			return true;

		}

		return false;
	}

	public function rebuildTotals( $objAdminDatabase ) {

		$this->setNewChargesTotal( 0 );
		$this->setNewSetUpChargesTotal( 0 );
		$this->setNewPaymentsTotal( 0 );

		$this->setNewRecurringChargesTotal( 0 );
		$this->setSalesTaxNewChargesTotal( 0 );
		$this->setSalesTaxRecurringChargesTotal( 0 );
		$this->setSalesTaxSetupChargesTotal( 0 );
		$this->setSalesTaxPaymentsTotal( 0 );
		$this->setNewUtilityChargesTotal( 0 );

		$arrstrBundles					= [];
		$arrstrSetUpBundles				= [];
		$arrstrOtherBundles				= [];
		$arrintSalesTaxTansactionKeys	= [];
		$arrstrUtlityBundles			= [];
		$arrstrTransactions 			= $this->getTransactions();
		$arrintFrequencyIds = [ CFrequency::QUARTERLY, CFrequency::SEMI_ANNUALLY, CFrequency::YEARLY ];

		$arrobjSalesTaxTransactions = CSalesTaxTransactions::createService()->fetchNonAdjustmentSalesTaxTransactionsByInvoiceId( $this->getId(), $objAdminDatabase );

		if( true == valArr( $arrobjSalesTaxTransactions ) ) {
			$arrobjSalesTaxTransactions = rekeyObjects( 'TransactionId', $arrobjSalesTaxTransactions );
			$arrintSalesTaxTansactionKeys = array_flip( array_keys( $arrobjSalesTaxTransactions ) );
		}

		if( true == valArr( $arrstrTransactions ) ) {
			foreach( $arrstrTransactions as $arrstrTransaction ) {
				if( true == is_null( $arrstrTransaction['company_payment_id'] ) ) {
					if( false == is_null( $arrstrTransaction['bundle_ps_product_id'] ) ) {

						if( true == in_array( $arrstrTransaction['charge_code_id'], CChargeCode::$c_arrintRecurringChargeGroups ) && 0 == $this->getIsCreditMemo() ) {

							if( false == isset( $arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] ) ) {
								$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] = 0;
							}

							$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] += $arrstrTransaction['amount'];

							if( false == isset( $arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] ) ) {
								$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] = 0;
							}

							if( true == valArr( $arrintSalesTaxTansactionKeys ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTansactionKeys ) ) {

								if( true == in_array( $arrstrTransaction['frequency_id'], $arrintFrequencyIds ) ) {
									if( CFrequency::QUARTERLY == $arrstrTransaction['frequency_id'] ) {
										$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] += ( $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied() ) * $arrstrTransaction['transaction_count'];
									} elseif( CFrequency::SEMI_ANNUALLY == $arrstrTransaction['frequency_id'] ) {
										$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] += ( $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied() ) * $arrstrTransaction['transaction_count'];
									} elseif( CFrequency::YEARLY == $arrstrTransaction['frequency_id'] ) {
										$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] += ( $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied() ) * $arrstrTransaction['transaction_count'];
									}
								} else {
									$arrstrBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] += $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
								}
							}

						} elseif( true == in_array( $arrstrTransaction['charge_code_id'], CChargeCode::$c_arrintSetUpChargeCodes ) && 0 == $this->getIsCreditMemo() ) {

							if( false == isset( $arrstrSetUpBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] ) ) {
								$arrstrSetUpBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] = 0;
							}

							$arrstrSetUpBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] += $arrstrTransaction['amount'];

							if( false == isset( $arrstrSetUpBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] ) ) {
								$arrstrSetUpBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] = 0;
							}

							if( true == valArr( $arrintSalesTaxTansactionKeys ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTansactionKeys ) ) {
								$arrstrSetUpBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] += $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();
							}

						} elseif( true == in_array( $arrstrTransaction['charge_code_id'], CChargeCode::$c_arrintUtilityChargeCodes ) && 0 == $this->getIsCreditMemo() ) {
							// Do not sum up the Utility charges in other. Total Utlity Charges code is written in utlity charges section.
							if( false == isset( $arrstrUtlityBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] ) ) {
								$arrstrUtlityBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] = 0;
							}

							$arrstrUtlityBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] += $arrstrTransaction['amount'];

						} else {

							if( false == isset( $arrstrOtherBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] ) ) {
								$arrstrOtherBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] = 0;
							}

							$arrstrOtherBundles[$arrstrTransaction['bundle_ps_product_id']]['amount'] += $arrstrTransaction['amount'];

							if( false == isset( $arrstrOtherBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] ) ) {
								$arrstrOtherBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] = 0;
							}

							if( true == valArr( $arrintSalesTaxTansactionKeys ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTansactionKeys ) ) {
								$arrstrOtherBundles[$arrstrTransaction['bundle_ps_product_id']]['sales_tax'] += $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied() * $arrstrTransaction['transaction_count'];
							}
						}

					} else {

						if( false == is_null( $arrstrTransaction['charge_code_id'] ) && true == in_array( $arrstrTransaction['charge_code_id'], CChargeCode::$c_arrintRecurringChargeGroups ) && 0 == $this->getIsCreditMemo() ) {

							$this->setNewRecurringChargesTotal( $this->getNewRecurringChargesTotal() + $arrstrTransaction['transaction_amount'] );

							if( true == valArr( $arrintSalesTaxTansactionKeys ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTansactionKeys ) ) {

								$fltSalesTax = $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied();

								if( true == in_array( $arrstrTransaction['frequency_id'], $arrintFrequencyIds ) ) {
									if( CFrequency::QUARTERLY == $arrstrTransaction['frequency_id'] ) {
										$this->setSalesTaxRecurringChargesTotal( $this->getSalesTaxRecurringChargesTotal() + ( $fltSalesTax * $arrstrTransaction['transaction_count'] ) );
									} elseif( CFrequency::SEMI_ANNUALLY == $arrstrTransaction['frequency_id'] ) {
										$this->setSalesTaxRecurringChargesTotal( $this->getSalesTaxRecurringChargesTotal() + ( $fltSalesTax * $arrstrTransaction['transaction_count'] ) );
									} elseif( CFrequency::YEARLY == $arrstrTransaction['frequency_id'] ) {
										$this->setSalesTaxRecurringChargesTotal( $this->getSalesTaxRecurringChargesTotal() + ( $fltSalesTax * $arrstrTransaction['transaction_count'] ) );
									}
								} else {
									$this->setSalesTaxRecurringChargesTotal( $this->getSalesTaxRecurringChargesTotal() + $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied() );
								}
							}

						} elseif( false == is_null( $arrstrTransaction['charge_code_id'] ) && true == in_array( $arrstrTransaction['charge_code_id'], CChargeCode::$c_arrintSetUpChargeCodes ) && 0 == $this->getIsCreditMemo() ) {

							$this->setNewSetUpChargesTotal( $this->getNewSetUpChargesTotal() + $arrstrTransaction['transaction_amount'] );

							if( true == valArr( $arrintSalesTaxTansactionKeys ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTansactionKeys ) ) {
								$this->setSalesTaxSetupChargesTotal( $this->getSalesTaxSetupChargesTotal() + $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied() );
							}

						} elseif( true == in_array( $arrstrTransaction['charge_code_id'], CChargeCode::$c_arrintUtilityChargeCodes ) && 0 == $this->getIsCreditMemo() ) {
							// Do not sum up the Utility charges in other charges. Total Utlity Charges code is written in utlity charges section.
							$this->setNewUtilityChargesTotal( $this->getNewUtilityChargesTotal() + $arrstrTransaction['transaction_amount'] );

							if( true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTansactionKeys ) ) {
								$this->setSalesTaxUtilityChargesTotal( $this->getSalesTaxUtilityChargesTotal() + $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied() );
							}
						} else {

							$this->setNewChargesTotal( $this->getNewChargesTotal() + $arrstrTransaction['transaction_amount'] );

							if( true == valArr( $arrintSalesTaxTansactionKeys ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTansactionKeys ) ) {
								$this->setSalesTaxNewChargesTotal( $this->getSalesTaxNewChargesTotal() + $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied() ) * $arrstrTransaction['transaction_count'];
							}
						}
					}
				} else {

					$this->setNewPaymentsTotal( $this->getNewPaymentsTotal() + $arrstrTransaction['transaction_amount'] );

					if( true == valArr( $arrintSalesTaxTansactionKeys ) && true == array_key_exists( $arrstrTransaction['id'], $arrintSalesTaxTansactionKeys ) ) {
						$this->setSalesTaxPaymentsTotal( $this->getSalesTaxPaymentsTotal() + $arrobjSalesTaxTransactions[$arrstrTransaction['id']]->getTotalTaxApplied() );
					}
				}
			}

			if( true == valArr( $arrstrBundles ) ) {
				foreach( $arrstrBundles as $arrstrBundle ) {
					$this->setNewRecurringChargesTotal( $this->getNewRecurringChargesTotal() + $arrstrBundle['amount'] );
					$this->setSalesTaxRecurringChargesTotal( $this->getSalesTaxRecurringChargesTotal() + $arrstrBundle['sales_tax'] );
				}
			}

			if( true == valArr( $arrstrSetUpBundles ) ) {
				foreach( $arrstrSetUpBundles as $arrstrSetUpBundle ) {
					$this->setNewSetUpChargesTotal( $this->getNewSetUpChargesTotal() + $arrstrSetUpBundle['amount'] );
					$this->setSalesTaxSetupChargesTotal( $this->getSalesTaxSetupChargesTotal() + $arrstrSetUpBundle['sales_tax'] );
				}
			}

			if( true == valArr( $arrstrOtherBundles ) ) {
				foreach( $arrstrOtherBundles as $arrstrOtherBundle ) {
					$this->setNewChargesTotal( $this->getNewChargesTotal() + $arrstrOtherBundle['amount'] );
					$this->setSalesTaxNewChargesTotal( $this->getSalesTaxNewChargesTotal() + $arrstrOtherBundle['sales_tax'] );
				}
			}
		}
	}

	/**
	 * @param FPDF $objPdf
	 * @param int  $intPaymentDivHeight
	 */

	public function loadUtilityBillingTransactionData( $objAdminDatabase, $objUtilitiesDatabase ) {
		$this->m_objUtilityBatch = $this->getOrFetchUtilityBatch( $objUtilitiesDatabase );

		if( false == valObj( $this->m_objUtilityBatch, 'CUtilityBatch' ) ) {
			trigger_error( 'Utility batch failed to load.', E_USER_ERROR );
		}

		$arrstrPropertyNames = \Psi\Eos\Admin\CProperties::createService()->fetchPropertyNamesByPropertyIdsByCid( [ $this->m_objUtilityBatch->getPropertyId() ], $this->m_objUtilityBatch->getCid(), $objAdminDatabase );

		// This query loads all the transaction data we need to generate the invoice.
		$this->m_arrstrPropertyUtilityTypeRateGroupedData = $this->getOrFetchPropertyUtilityTypeRateGroupedData( [ $this->m_objUtilityBatch->getId() ], $objAdminDatabase, $objUtilitiesDatabase );

		$arrstrBillingDates = $arrintPropertyUtilityTypeRateIds = [];

		if( true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['BillingAndUsage'] ) ) {
			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['BillingAndUsage'] as $intPropertyUtilityTypeRateId => $arrstrPropertyUtilityTypeRateData ) {
				$intPropertyUtilityTypeId = $arrstrPropertyUtilityTypeRateData['property_utility_type_id'];
				$arrintPropertyUtilityTypeRateIds[$intPropertyUtilityTypeRateId] = $intPropertyUtilityTypeId;

				if( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) {
					$arrstrBillingDates['start_date'][] = strtotime( $arrstrPropertyUtilityTypeRateData['billing_start_date'] );
				}
				if( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) {
					$arrstrBillingDates['end_date'][] = strtotime( $arrstrPropertyUtilityTypeRateData['billing_end_date'] );
				}
			}
		}

		$boolMultipleLeaseBucket = false;

		if( true == valArr( $arrintPropertyUtilityTypeRateIds ) ) {
			foreach( $arrintPropertyUtilityTypeRateIds as $intPropertyUtilityTypeRateId => $intPropertyUtilityTypeId ) {
				if( false == ( '' == $intPropertyUtilityTypeId ) ) {
					$arrintPropertyUtilityTypeRateIds = array_keys( $arrintPropertyUtilityTypeRateIds, $intPropertyUtilityTypeId );

					if( 1 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyUtilityTypeRateIds ) ) {
						// there is multiple lease bucket for one property utility type
						$boolMultipleLeaseBucket = true;
					}
				}
			}
		}

		$fltOverallResidentTotal = 0;
		$fltOverallPropertyTotal = 0;
		$arrmixTransactionData = [];
		$arrmixResidentChargesData = [];
		$arrmixResidentBillingFees = [];
		$arrmixUtilityServiceRateData = [];
		$strStartDate = $strEndDate = NULL;

		if( true == array_key_exists( 'start_date', $arrstrBillingDates ) ) {
			$arrstrDate = getArrayElementByKey( 'start_date', $arrstrBillingDates );
			\Psi\CStringService::singleton()->sort( $arrstrDate );
			$strStartDate = date( 'm/d/Y', array_shift( $arrstrDate ) );

			$arrstrDate = getArrayElementByKey( 'end_date', $arrstrBillingDates );
			\Psi\CStringService::singleton()->sort( $arrstrDate );
			$strEndDate = date( 'm/d/Y', array_pop( $arrstrDate ) );
		}

		// Add Billing & Usage Fees
		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['BillingAndUsage'] ) && true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['BillingAndUsage'] ) ) {
			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['BillingAndUsage'] as $intPropertyUtilityTypeRateId => $arrstrPropertyUtilityTypeRateData ) {

				$strLeasePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) ? date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) : 'All';
				$strLeasePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) ? ' - ' . date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) : ' - All';

				if( true == is_null( $strStartDate ) ) {
					$strStartDate = date( 'm/d/Y', strtotime( '-1 month', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) ) + ( 3600 * 24 ) );
					$strEndDate	  = $this->m_objUtilityBatch->getDistributionDeadline();
				}

				$strUsagePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) ? date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) : date( 'n/j', strtotime( $strStartDate ) );
				$strUsagePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) ? ' - ' . date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) : ' - ' . date( 'n/j', strtotime( $strEndDate ) );

				$arrmixUtilityServiceRateData['name'] = $arrstrPropertyUtilityTypeRateData['name'];
				$arrmixUtilityServiceRateData['usage_period'] = $strUsagePeriod;

				if( true == $boolMultipleLeaseBucket ) {
					$arrmixUtilityServiceRateData['lease_period'] = $strLeasePeriod;
				}

				// Here we need to find out the following:
				// 1. How much utility usage (sum estimated and actual)
				// 2. How much in billing fees

				$fltTotalResidentBillingFees 	= 0;
				$fltTotalPropertyBillingFees 	= 0;
				$fltTotalUtilityConsumption 	= 0;

				// Loop on the utility transaction types in the array and summarize billing fees along with estimated and actual utility usage
				if( true == valArr( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] ) ) {
					foreach( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] as $intUtilityTransactionTypeId => $arrstrUtilityTransactionTypeData ) {
						if( true == valArr( $arrstrUtilityTransactionTypeData ) ) {
							foreach( $arrstrUtilityTransactionTypeData as $intTransactionId => $arrstrTransactionData ) {

								switch( $intUtilityTransactionTypeId ) {
									case CUtilityTransactionType::ESTIMATED_USAGE:
									case CUtilityTransactionType::ACTUAL_USAGE:
									case CUtilityTransactionType::BASE_FEE:
										// Estimated and actual usage charges should never have corresponding transaction ids.  If it does, throw an error:

										if( true == is_numeric( $intTransactionId ) ) {
											trigger_error( 'Estimated and actual utility transactions should never have associated transactions.', E_USER_ERROR );
										}

										$fltTotalUtilityConsumption += ( float ) $arrstrTransactionData['utility_transaction_sum'];
										break;

									case CUtilityTransactionType::BILLING_FEE:
										$fltTotalResidentBillingFees += ( float ) $arrstrTransactionData['utility_transaction_sum'];
										$fltTotalPropertyBillingFees += ( true == isset( $arrstrTransactionData['total_transactions_sum'] ) ) ? ( float ) $arrstrTransactionData['total_transactions_sum'] : 0;
										break;

									default:
										trigger_error( 'Erroneous utility transaction type for usage and billing fees (cound happen if a invoice gets wrongly associated to a utility_batch).', E_USER_ERROR );
										break;
								}
							}
						}
					}
				}

				// Find out the property and resident cost per bill
				$arrmixResidentChargesData['utility_consumption']   = __( '{%f, 0, p:2}', [ ( float ) $fltTotalUtilityConsumption ] );
				$arrmixResidentChargesData['resident_billing_fees'] = __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentBillingFees ] );
				$arrmixResidentChargesData['total_billing_fees'] = ( float ) $fltTotalResidentBillingFees + ( float ) $fltTotalUtilityConsumption;
				$arrmixResidentBillingFees['property_billing_fees'] = __( '{%f, 0, p:2}', [ $fltTotalPropertyBillingFees ] );

				$fltOverallResidentTotal += ( ( float ) $fltTotalResidentBillingFees + ( float ) $fltTotalUtilityConsumption );
				$fltOverallPropertyTotal += ( float ) $fltTotalPropertyBillingFees;

				$arrmixUtilityTransactionData['services'][]			= $arrmixUtilityServiceRateData;
				$arrmixUtilityTransactionData['resident_charges'][]	= $arrmixResidentChargesData;
				$arrmixUtilityTransactionData['resident_billing'][]	= $arrmixResidentBillingFees;
				$arrmixUtilityTransactionData['utility_rates'][]	= $arrmixResidentChargesData;
				$arrmixUtilityTransactionData['billing_fees'][]		= $arrmixResidentBillingFees;
			}
		}

		// Add Move-in & Termination Fees
		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['MoveInAndTermination'] ) && true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['MoveInAndTermination'] ) ) {
			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['MoveInAndTermination'] as $intPropertyUtilityTypeRateId => $arrstrPropertyUtilityTypeRateData ) {

				$strLeasePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) ? date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) : 'All';
				$strLeasePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) ? ' - ' . date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) : ' - All';

				if( true == is_null( $strStartDate ) ) {
					$strStartDate = date( 'm/d/Y', strtotime( '-1 month', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) ) + ( 3600 * 24 ) );
					$strEndDate	  = $this->m_objUtilityBatch->getDistributionDeadline();
				}

				$strUsagePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) ? date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) : date( 'n/j', strtotime( $strStartDate ) );
				$strUsagePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) ? ' - ' . date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) : ' - ' . date( 'n/j', strtotime( $strEndDate ) );

				if( true == valArr( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] ) ) {
					foreach( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] as $intUtilityTransactionTypeId => $arrstrUtilityTransactionTypeData ) {

						switch( $intUtilityTransactionTypeId ) {
							case CUtilityTransactionType::MOVE_IN_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['MoveInAndTermination'] ) ) ? ' Move-in Fees ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'Move-in Fees';
								break;

							case CUtilityTransactionType::TERMINATION_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['MoveInAndTermination'] ) ) ? ' Termination Fees ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'Termination Fees';
								break;

							default:
								trigger_error( 'Erroneous utility transaction type for move in and termination fees.', E_USER_ERROR );
								break;
						}

						// Summarize the charge groupings (should only be one)
						$fltTotalResidentFees 		 = 0;
						$fltTotalPropertyFees 		 = 0;

						if( true == valArr( $arrstrUtilityTransactionTypeData ) ) {
							foreach( $arrstrUtilityTransactionTypeData as $intTransactionId => $arrstrTransactionData ) {
								$fltTotalResidentFees 		 = ( float ) $arrstrTransactionData['utility_transaction_sum'];
								$fltTotalPropertyFees 		 = ( true == isset( $arrstrTransactionData['total_transactions_sum'] ) ) ? ( float ) $arrstrTransactionData['total_transactions_sum'] : 0;
							}
						}

						// Find out the property and resident cost per bill
						$fltTotalPropertyFees = ( 0 == $fltTotalPropertyFees ) ? '-' : __( '{%f, 0, p:2}', [ ( float ) $fltTotalPropertyFees ] );

						$arrmixUtilityServiceRateData['name'] = $strName;
						$arrmixUtilityServiceRateData['usage_period'] = $strUsagePeriod;

						if( true == $boolMultipleLeaseBucket ) {
							$arrmixUtilityServiceRateData['lease_period']	 = $strUsagePeriod;
						}
						$arrmixResidentChargesData['utility_consumption']	 = '-';
						$arrmixResidentChargesData['resident_billing_fees']	 = __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] );
						$arrmixResidentChargesData['total_billing_fees']	 = __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] );
						$arrmixResidentBillingFees['property_billing_fees']	 = $fltTotalPropertyFees;
						$arrmixUtilityTransactionData['utility_rates'][]	 = $arrmixResidentChargesData;
						$arrmixUtilityTransactionData['services'][]			 = $arrmixUtilityServiceRateData;
						$arrmixUtilityTransactionData['billing_fees'][]		 = $arrmixResidentBillingFees;
					}
				}
			}
		}

		// Add VCR Charges
		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] ) && true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] ) ) {
			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] as $intPropertyUtilityTypeRateId => $arrstrPropertyUtilityTypeRateData ) {

				$strLeasePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) ? date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) : 'All';
				$strLeasePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) ? ' - ' . date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) : ' - All';

				$strUsagePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) ? date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) : date( 'n/j', strtotime( '-1 month', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) ) + ( 3600 * 24 ) );
				$strUsagePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) ? ' - ' . date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) : ' - ' . date( 'n/j', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) );

				if( true == valArr( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] ) ) {
					foreach( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] as $intUtilityTransactionTypeId => $arrstrUtilityTransactionTypeData ) {

						switch( $intUtilityTransactionTypeId ) {

							case CUtilityTransactionType::VCR_USAGE_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] ) ) ? ' VCR Usage Fees ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'VCR Usage Fees';
								break;

							case CUtilityTransactionType::VCR_SERVICE_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] ) ) ? ' VCR Penalties ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'VCR Penalties';
								break;

							case CUtilityTransactionType::VCR_LATE_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['Vcr'] ) ) ? ' VCR Late Fees ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'VCR Late Fees';
								break;

							default:
								trigger_error( 'Erroneous utility transaction type for vcr fees.', E_USER_ERROR );
								break;
						}

						// Summarize the charge groupings (should only be one)
						$fltTotalResidentFees 		 = 0;
						$fltTotalPropertyFees 		 = 0;

						if( true == valArr( $arrstrUtilityTransactionTypeData ) ) {
							foreach( $arrstrUtilityTransactionTypeData as $intTransactionId => $arrstrTransactionData ) {
								$fltTotalResidentFees 		 = ( float ) $arrstrTransactionData['utility_transaction_sum'];
								$fltTotalPropertyFees 		 = ( true == isset( $arrstrTransactionData['total_transactions_sum'] ) ) ? ( float ) $arrstrTransactionData['total_transactions_sum'] : 0;
							}
						}

						$arrmixUtilityServiceRateData['name'] = $strName;
						$arrmixUtilityServiceRateData['usage_period'] = $strUsagePeriod;

						if( true == $boolMultipleLeaseBucket ) {
							$arrmixUtilityServiceRateData['lease_period'] = $strUsagePeriod;
						}
						$arrmixResidentChargesData['utility_consumption']	 = '-';
						$arrmixResidentChargesData['resident_billing_fees']	 = __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] );
						$arrmixResidentChargesData['total_billing_fees']	 = __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] );
						$arrmixResidentBillingFees['property_billing_fees']	 = $fltTotalPropertyFees;
						$arrmixUtilityTransactionData['utility_rates'][]	 = $arrmixResidentChargesData;
						$arrmixUtilityTransactionData['services'][]			 = $arrmixUtilityServiceRateData;
						$arrmixUtilityTransactionData['billing_fees'][]		 = $arrmixResidentBillingFees;
					}
				}
			}
		}

		// Add VCR Analysis Fees here. (comes right from transactions table)
		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'][CChargeCode::UTILITY_VCR_PER_UNIT_FEES] ) && 0 != ( float ) $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'][CChargeCode::UTILITY_VCR_PER_UNIT_FEES] ) {

			$strUsagePeriod = date( 'n/j', strtotime( '-1 month', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) ) + ( 3600 * 24 ) );
			$strUsagePeriod .= ' - ' . date( 'n/j', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) );

			$arrmixUtilityServiceRateData['name'] = 'VCR Analysis Fees';
			$arrmixUtilityServiceRateData['usage_period'] = $strUsagePeriod;

			if( true == $boolMultipleLeaseBucket ) {
				$arrmixUtilityServiceRateData['lease_period'] = $strUsagePeriod;
			}
			$arrmixResidentChargesData['utility_consumption']	 = '-';
			$arrmixResidentChargesData['resident_billing_fees']	 = '-';
			$arrmixResidentChargesData['total_billing_fees']	 = '-';
			$arrmixResidentBillingFees['property_billing_fees']	 = __( '{%f, 0, p:2}', [ ( float ) $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'][CChargeCode::UTILITY_VCR_PER_UNIT_FEES] ] );
			$arrmixUtilityTransactionData['utility_rates'][]	 = $arrmixResidentChargesData;
			$arrmixUtilityTransactionData['services'][]			 = $arrmixUtilityServiceRateData;
			$arrmixUtilityTransactionData['billing_fees'][]		 = $arrmixResidentBillingFees;
		}

		// Add Late Fees here.
		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['LateFees'] ) && true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['LateFees'] ) ) {
			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['LateFees'] as $intPropertyUtilityTypeRateId => $arrstrPropertyUtilityTypeRateData ) {

				$strLeasePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) ? date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_start_date_criteria'] ) ) : 'All';
				$strLeasePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) ? ' - ' . date( 'm/d/Y', strtotime( $arrstrPropertyUtilityTypeRateData['lease_end_date_criteria'] ) ) : ' - All';

				$strUsagePeriod = ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) ? date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_start_date'] ) ) : date( 'n/j', strtotime( '-1 month', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) ) + ( 3600 * 24 ) );
				$strUsagePeriod .= ( false == is_null( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) ? ' - ' . date( 'n/j', strtotime( $arrstrPropertyUtilityTypeRateData['billing_end_date'] ) ) : ' - ' . date( 'n/j', strtotime( $this->m_objUtilityBatch->getDistributionDeadline() ) );

				if( true == valArr( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] ) ) {
					foreach( $arrstrPropertyUtilityTypeRateData['utility_transaction_types'] as $intUtilityTransactionTypeId => $arrstrUtilityTransactionTypeData ) {

						switch( $intUtilityTransactionTypeId ) {

							case CUtilityTransactionType::LATE_FEE:
								$strName = ( 1 < \Psi\Libraries\UtilFunctions\count( $this->m_arrstrPropertyUtilityTypeRateGroupedData['LateFees'] ) ) ? ' Late Fees ( ' . $arrstrPropertyUtilityTypeRateData['name'] . ')' : 'Late Fees';
								break;

							default:
								trigger_error( 'Erroneous utility transaction type for late fees.', E_USER_ERROR );
								break;
						}

						// Summarize the charge groupings (should only be one)
						$fltTotalResidentFees 		= 0;
						$fltTotalPropertyFees 		= 0;

						if( true == valArr( $arrstrUtilityTransactionTypeData ) ) {
							foreach( $arrstrUtilityTransactionTypeData as $intTransactionId => $arrstrTransactionData ) {
								$fltTotalResidentFees 		= ( float ) $arrstrTransactionData['utility_transaction_sum'];
								$fltTotalPropertyFees 		= ( true == isset( $arrstrTransactionData['total_transactions_sum'] ) ) ? ( float ) $arrstrTransactionData['total_transactions_sum'] : 0;
							}
						}

						// Find out the property and resident cost per bill

						$fltTotalPropertyFees = ( 0 == $fltTotalPropertyFees ) ? '-' : __( '{%f, 0, p:2}', [ ( float ) $fltTotalPropertyFees ] );
						if( $intUtilityTransactionTypeId == CUtilityTransactionType::VCR_USAGE_FEE ) {
							$fltTotalResidentFees = __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] );
						}

						$arrmixUtilityServiceRateData['name'] = $strName;
						$arrmixUtilityServiceRateData['usage_period'] = $strUsagePeriod;

						if( true == $boolMultipleLeaseBucket ) {
							$arrmixUtilityServiceRateData['lease_period'] = $strUsagePeriod;
						}
						$arrmixResidentChargesData['utility_consumption']	 = '-';
						$arrmixResidentChargesData['resident_billing_fees']	 = __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] );
						$arrmixResidentChargesData['total_billing_fees']	 = __( '{%f, 0, p:2}', [ ( float ) $fltTotalResidentFees ] );
						$arrmixResidentBillingFees['property_billing_fees']	 = $fltTotalPropertyFees;
						$arrmixUtilityTransactionData['utility_rates'][]	 = $arrmixResidentChargesData;
						$arrmixUtilityTransactionData['services'][]			 = $arrmixUtilityServiceRateData;
						$arrmixUtilityTransactionData['billing_fees'][]		 = $arrmixResidentBillingFees;
					}
				}
			}
		}

		$this->m_arrobjChargeCodes = $this->getOrFetchChargeCodes( $objAdminDatabase );

		if( true == isset( $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'] ) && true == valArr( $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'] ) ) {
			foreach( $this->m_arrstrPropertyUtilityTypeRateGroupedData['ExtraCharges'] as $intChargeCodeId => $fltChargeAmount ) {

				if( false == in_array( $intChargeCodeId, [ CChargeCode::UTILITY_VCR_PER_UNIT_FEES, CChargeCode::PAYMENT_RECEIVED ] ) ) {

					$arrmixUtilityServiceRateData['name'] = truncate( $this->m_arrobjChargeCodes[$intChargeCodeId]->getName(), 60 );
					$arrmixUtilityServiceRateData['usage_period'] = '-';

					if( true == $boolMultipleLeaseBucket ) {
						$arrmixUtilityServiceRateData['lease_period']	 = '-';
					}
					$arrmixResidentChargesData['utility_consumption']	 = '-';
					$arrmixResidentChargesData['resident_billing_fees']	 = '-';
					$arrmixResidentChargesData['total_billing_fees']	 = '-';
					$arrmixResidentBillingFees['property_billing_fees']	 = __( '{%f, 0, p:2}', [ $fltChargeAmount ] );
					$arrmixUtilityTransactionData['utility_rates'][]	 = $arrmixResidentChargesData;
					$arrmixUtilityTransactionData['services'][]			 = $arrmixUtilityServiceRateData;
					$arrmixUtilityTransactionData['billing_fees'][]		 = $arrmixResidentBillingFees;

				}
			}
		}

		$strStartDate = $strEndDate = NULL;
		if( true == array_key_exists( 'start_date', $arrstrBillingDates ) ) {
			$arrstrDate = getArrayElementByKey( 'start_date', $arrstrBillingDates );
			\Psi\CStringService::singleton()->sort( $arrstrDate );
			$strStartDate = date( 'm/d/Y', array_shift( $arrstrDate ) );

			$arrstrDate = getArrayElementByKey( 'end_date', $arrstrBillingDates );
			\Psi\CStringService::singleton()->sort( $arrstrDate );
			$strEndDate = date( 'm/d/Y', array_pop( $arrstrDate ) );

		}

		$arrmixUtilityTransactionData['billing_term'] = 'Net 30';
		$intInvoiceDate = strtotime( $this->getInvoiceDate() );
		$intPrevMonthDate = strtotime( '-1 month', $intInvoiceDate );

		$strStartDate = date( 'm/01/Y', $intPrevMonthDate );
		$strEndDate = date( 'm/t/Y', $intPrevMonthDate );

		$arrmixUtilityTransactionData['billing_period']				 = ( string ) $strStartDate . ' - ' . ( string ) $strEndDate;
		$arrmixUtilityTransactionData['property_name']				 = array_pop( $arrstrPropertyNames );
		$arrmixUtilityTransactionData['utility_summary_image']		 = PATH_WWW_ROOT . 'Admin/images/utilities/NOI_high_resolution_150dpi.jpg';
		$arrmixUtilityTransactionData['total_units']				 = $this->m_objUtilityBatch->getTotalUnits();
		$arrmixUtilityTransactionData['bool_multiple_lease_bucket']	 = $boolMultipleLeaseBucket;
		$arrmixUtilityTransactionData['transaction_data']			 = $arrmixTransactionData;

		return $arrmixUtilityTransactionData;

	}

	/**
	 * @param $objAdminDatabase
	 * @param $objUtilitiesDatabase
	 */
	public function generatePdfInvoice( $objAdminDatabase, $objUtilitiesDatabase, $strFileName = NULL ) {

		$strHtmlContent = $this->buildHtmlInvoice( $objAdminDatabase, $objUtilitiesDatabase );

		$strTitle = 'Invoice';
		if( ( int ) $this->getIsCreditMemo() ) {
			$strTitle = 'Credit';
		}
		$strTitle .= ' Number ' . \Psi\CStringService::singleton()->str_pad( ( string ) $this->getInvoiceNumber(), 9, '0', STR_PAD_LEFT );

		$objPdfDocument = CPrinceFactory::createPrince();
		$objPdfDocument->setPDFTitle( $strTitle );
		$objPdfDocument->setBaseURL( PATH_COMMON );
		$objPdfDocument->setHTML( true );
		$objPdfDocument->setJavaScript( true );
		header( 'Content-type: application/pdf' );

		if( !isset( $strFileName ) ) {
			$objPdfDocument->convert_string_to_passthru( $strHtmlContent );
		} else {
			$strTempFileNamePath = CONFIG_CACHE_DIRECTORY . $strFileName;
			$objPdfDocument->convert_string_to_file( $strHtmlContent, $strTempFileNamePath );

			// Download generated PDF
			header( 'Content-Disposition:attachment; filename=' . $strFileName );
			$resFilePointer = CFileIo::fileOpen( $strTempFileNamePath, 'r' );
			fpassthru( $resFilePointer );
		}
	}

	public function loadStyles() {

		$this->m_arrstrStyles['align_horizontal_center'] = [
			'alignment' => [
				'horizontal' => PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			]
		];
		$this->m_arrstrStyles['align_horizontal_left'] = [
			'alignment' => [
				'horizontal' => PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
			]
		];
		$this->m_arrstrStyles['align_horizontal_right'] = [
			'alignment' => [
				'horizontal' => PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			]
		];
		$this->m_arrstrStyles['align_vertical_center']	= [
			'alignment' => [
				'verticle' => PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			]
		];
		$this->m_arrstrStyles['background'] = [
			'fill' => [
				'fillType' => PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				'color' => [ 'rgb' => 'E1E0F7' ],
			]
		];
		$this->m_arrstrStyles['background_end_of_invoice'] = [
			'fill' => [
				'fillType' => PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				'color' => [ 'rgb' => 'C0C0C0' ],
			]
		];
		$this->m_arrstrStyles['font_size_largest'] = [
			'font' => [
				'size' => '35',
			]
		];
		$this->m_arrstrStyles['font_normal'] = [
			'font' => [
				'bold' => false,
			]
		];
		$this->m_arrstrStyles['font_bold'] = [
			'font' => [
				'bold' => true,
			]
		];
		$this->m_arrstrStyles['font_italic'] = [
			'font' => [
				'italic' => true,
			]
		];
		$this->m_arrstrStyles['font_color_red'] = [
			'font' => [
				'color' => [ 'rgb' => 'FF0000' ],
			]
		];
		$this->m_arrstrStyles['border_all_borders'] = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				]
			]
		];
		$this->m_arrstrStyles['border_outline'] = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				]
			]
		];
		$this->m_arrstrStyles['border_top'] = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				]
			]
		];
		$this->m_arrstrStyles['border_right'] = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				]
			]
		];
		$this->m_arrstrStyles['border_bottom'] = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				]
			]
		];
		$this->m_arrstrStyles['border_left'] = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				]
			]
		];
		$this->m_arrstrStyles['red_white_title'] = [
			'fill' => [
				'fillType' => PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				'color' => [ 'rgb' => 'FF0000' ],
			],
			'font' => [
				'bold'  => true,
				'color' => [ 'rgb' => 'FFFFFF' ],
			],
			'alignment' => [
				'horizontal' => PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			]
		];
	}

	public function getStyle( $strStyleName ) {

		if( true == isset( $this->m_arrstrStyles[$strStyleName] ) ) {
			return $this->m_arrstrStyles[$strStyleName];
		}
		return [];
	}

	/**
	 * @param \CDatabase $objAdminDatabase
	 * @param \CDatabase $objUtilitiesDatabase
	 * @return \PhpOffice\PhpSpreadsheet\Spreadsheet
	 * @throws \PhpOffice\PhpSpreadsheet\Exception
	 */
	public function generateXlsx( $objAdminDatabase, $objUtilitiesDatabase ) {

		$this->rebuildTotals( $objAdminDatabase );

		$objPHPExcel = new PhpOffice\PhpSpreadsheet\Spreadsheet();
		$objPHPExcel->setActiveSheetIndex( 0 );

		$objCurrentWorksheet = $objPHPExcel->getActiveSheet()->setTitle( 'Invoice' );

		$this->loadStyles();

		$objCurrentWorksheet->getColumnDimension( 'A' )->setWidth( 5 );
		$objCurrentWorksheet->getColumnDimension( 'B' )->setWidth( 12 );
		$objCurrentWorksheet->getColumnDimension( 'C' )->setWidth( 20 );
		$objCurrentWorksheet->getColumnDimension( 'D' )->setWidth( 20 );
		$objCurrentWorksheet->getColumnDimension( 'E' )->setWidth( 25 );
		$objCurrentWorksheet->getColumnDimension( 'F' )->setWidth( 20 );
		$objCurrentWorksheet->getColumnDimension( 'G' )->setWidth( 15 );
		$objCurrentWorksheet->getColumnDimension( 'H' )->setWidth( 21 );
		$objCurrentWorksheet->getColumnDimension( 'I' )->setWidth( 12 );
		$objCurrentWorksheet->getColumnDimension( 'J' )->setWidth( 12 );
		$objCurrentWorksheet->getColumnDimension( 'K' )->setWidth( 12 );
		$objCurrentWorksheet->getColumnDimension( 'L' )->setWidth( 12 );
		$objCurrentWorksheet->getColumnDimension( 'M' )->setWidth( 12 );
		$objCurrentWorksheet->getColumnDimension( 'N' )->setWidth( 12 );
		$objCurrentWorksheet->getColumnDimension( 'O' )->setWidth( 12 );

		// These should all be preloaded unless this client quit. We only set current companies by default.
		if( false == isset( $this->m_objAccount ) ) {
			$this->m_objAccount = $this->fetchAccount( $objAdminDatabase );
		}

		if( false == $this->m_boolHasLoadedAccountAccountGroups ) {
			$this->m_arrobjAccountAccountGroups = $this->m_objAccount->getOrFetchAccountAccountGroups( $objAdminDatabase );
		} else {
			$this->m_arrobjAccountAccountGroups = $this->m_objAccount->getAccountAccountGroups( $objAdminDatabase );
		}

		$this->m_arrobjAccountAccountGroups = rekeyObjects( 'AccountGroupId', $this->m_arrobjAccountAccountGroups );

		if( false == isset( $this->m_objClient ) ) {
			$this->m_objClient = $this->fetchClient( $objAdminDatabase );
		}

		CLocaleContainer::createService()->setCurrencyCode( $this->getCurrencyCode() );

		$strDueDateText 	= 'Invoice Due Date';
		$strDueDateValue	= $this->getInvoiceDueDate();

		// if the bill is debited automatically, change 'Total Amount Due' to 'Amount to be auto debited'
		if( $this->m_objAccount->getPaymentTypeId() < CPaymentType::ACH ) {
			$strAmountDueText = 'Total Amount';
		} else {
			$strAmountDueText = 'Amount To Be Withdrawn';
			$this->setInvoiceDueDate( $this->getInvoiceDate() );
		}

		$objPHPExcel->getProperties()
			->setLastModifiedBy( CONFIG_COMPANY_NAME )
			->SetCreator( CONFIG_COMPANY_NAME )
			->SetTitle( 'Invoice Number ' . \Psi\CStringService::singleton()->str_pad( ( string ) $this->getInvoiceNumber(), 9, '0', STR_PAD_LEFT ) );
		$objPHPExcel->getDefaultStyle()
			->getFont()
			->setName( 'Arial' )
			->setSize( 10 )
			->applyFromArray( $this->getStyle( 'align_vertical_center' ) );

		$strFooterText	= '';
		$strAddressText	= '';

		if( $this->m_objAccount->getPaymentTypeId() < CPaymentType::ACH ) {
			$strFooterText = 'Mail payments to: ';
		}

		// get remittance address using currency code or bill to country code
		$arrmixRemittanceAddresses = CRemittanceAddresses::createService()->fetchRemittanceAddressByCurrencyCode( $this->getCurrencyCode(), NULL, $objAdminDatabase );

		$strMailToPaymentPoBox = '';
		$strLogoImageHeight = 82;
		$intLogoImageRowCount = 4;

		if( CAccountType::MASTER_POLICY == $this->m_objAccount->getAccountTypeId() ) {
			$strLogoImageFilePath  = PATH_WWW_ROOT . 'Admin/images/logos/' . CONFIG_ENTRATA_LOGO_PREFIX . 'invoice_logo_bold.jpg';
			$strMailToPaymentPoBox .= 'PO Box 1055, Lehi, UT 84043-1055';
		} else {
			$strLogoImageFilePath	= PATH_WWW_ROOT . 'Admin/images/logos/' . CONFIG_ENTRATA_LOGO_PREFIX . 'ps_logo_footer.gif';
			$strLogoImageHeight = 70;
			$intLogoImageRowCount = 3;
			if( true == valArr( $arrmixRemittanceAddresses ) ) {
				$strMailToPaymentPoBox = $arrmixRemittanceAddresses['street_address'] . ', ' . $arrmixRemittanceAddresses['mailing_address'] . ' ' . $arrmixRemittanceAddresses['postal_code'];
				$strMailToPaymentPoBox = str_replace( '<br>', PHP_EOL, $strMailToPaymentPoBox );
			}
		}

		$strAddressText = $strMailToPaymentPoBox;

		$strFooterText .= $strAddressText;

		if( CCurrency::CURRENCY_CODE_CAD == $this->getCurrencyCode() ) {
			$strFooterText = self::INVOICE_FOOTER_FOR_CANADA;
		}

		$intCurrentRowNumber	= 1;
		$intLogoImageRow		= 1;
		$strLogoImageColumn		= 'A';

		$objDrawing = new PhpOffice\PhpSpreadsheet\Worksheet\Drawing();

		$objDrawing->setPath( $strLogoImageFilePath )
			->setCoordinates( $strLogoImageColumn . $intLogoImageRow )
			->setResizeProportional( false )
			->setWidth( 340 )
			->setHeight( $strLogoImageHeight )
			->setWorksheet( $objPHPExcel->getActiveSheet() );

		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'D' . ( $intCurrentRowNumber + $intLogoImageRowCount ) );

		$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'H' . $intCurrentRowNumber )->getNumberFormat()->setFormatCode( PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
		$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 4 ) )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'E' . ( $intCurrentRowNumber + 1 ) )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->setCellValueExplicit( 'H' . $intCurrentRowNumber, \Psi\CStringService::singleton()->str_pad( ( string ) $this->getInvoiceNumber(), 9, '0', STR_PAD_LEFT ), PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING );

		if( 0 == $this->getIsCreditMemo() ) {
			$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, 'Invoice Number' );
		} else {
			$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, 'Credit Memo' );
		}

		// row Account Number
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
		$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, 'Account Number' );
		$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, $this->m_objAccount->getAccountNumber() );

		// row Invoice Date
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );

		if( 1 == $this->getIsCreditMemo() ) {
			$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, 'Credit Date' );
		} else {
			$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, 'Invoice Date' );
		}

		$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, $this->getInvoiceDate() );

		// row Due Date Text
		if( 0 == $this->getIsCreditMemo() ) {
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
			$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, $strDueDateText );
			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, $strDueDateValue );
		}

		// row Blank
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
		if( CAccountType::MASTER_POLICY != $this->m_objAccount->getAccountTypeId() ) {
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'D' . ( $intCurrentRowNumber + 2 ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'D' . ( $intCurrentRowNumber + 2 ) )->applyFromArray( $this->getStyle( 'align_horizontal_left' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'D' . ( $intCurrentRowNumber + 2 ) )->applyFromArray( $this->getStyle( 'font_bold' ) );
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $strMailToPaymentPoBox );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber, $strMailToPaymentPoBox )->getAlignment()->setWrapText( true );
		}

		// row Billing Period
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, 'Billing Period' );

		if( true == is_numeric( $this->getUtilityBatchId() ) ) {
			$intInvoiceDate = strtotime( $this->getInvoiceDate() );
			$intPrevMonthDate = strtotime( '-1 month', $intInvoiceDate );

			$strStartDate = date( 'm/01/Y', $intPrevMonthDate );
			$strEndDate = date( 'm/t/Y', $intPrevMonthDate );

			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ( string ) $strStartDate . ' - ' . ( string ) $strEndDate );
		} else {
			$strStartDate = strtotime( $this->getInvoiceDate() );

			switch( $this->m_objAccount->getInvoicePeriodId() ) {
				case CInvoicePeriod::FUTURE_1_MONTH:
					$strEndDate = strtotime( '+1 month', $strStartDate );
					$strEndDate = strtotime( '-1 day', $strEndDate );
					break;

				case CInvoicePeriod::FUTURE_1_QUARTER:
					$strEndDate = strtotime( '+3 months', $strStartDate );
					$strEndDate = strtotime( '-1 day', $strEndDate );
					break;

				case CInvoicePeriod::FUTURE_6_MONTHS:
					$strEndDate = strtotime( '+6 months', $strStartDate );
					$strEndDate = strtotime( '-1 day', $strEndDate );
					break;

				case CInvoicePeriod::FUTURE_1_YEAR:
					$strEndDate = strtotime( '+1 year', $strStartDate );
					$strEndDate = strtotime( '-1 day', $strEndDate );
					break;

				case CInvoicePeriod::TRAILING_1_MONTH:
					$strEndDate = strtotime( $this->getInvoiceDate() );
					$strStartDate = strtotime( '-1 month', $strEndDate );
					$strStartDate = strtotime( '+1 day', $strStartDate );
					break;

				default:
					// default
					break;
			}

			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, date( 'm/d/Y', $strStartDate ) . ' - ' . date( 'm/d/Y', $strEndDate ) );
		}

		$intTotalSalesTaxAmount = $this->getSalesTaxNewChargesTotal() + $this->getSalesTaxRecurringChargesTotal() + $this->getSalesTaxSetupChargesTotal() + $this->getSalesTaxPaymentsTotal() + $this->getSalesTaxUtilityChargesTotal();

		// row Amount Due
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );
		$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'border_bottom' ) );
		$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, $strAmountDueText );

		$strTotal = $this->getNewChargesTotal() + $this->getNewRecurringChargesTotal() + $this->getNewSetUpChargesTotal() + $intTotalSalesTaxAmount;

		$strCurrencyCode = '';
		if( $this->getCurrencyCode() != CCurrency::CURRENCY_CODE_USD ) {
			$strCurrencyCode = ' ' . $this->getCurrencyCode();
		}

		if( 0 > $strTotal ) {
			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' ( ' . __( '{%m, 0, p:2}', [ ( -1 ) * $strTotal ] ) . $strCurrencyCode . ' )' );
		} else {
			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $strTotal ] ) . $strCurrencyCode );
		}

		// row Blank
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		// row Billing Address
		$intCurrentRowNumber++;
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 4 ) )->applyFromArray( $this->getStyle( 'border_outline' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 4 ) )->applyFromArray( $this->getStyle( 'font_bold' ) );
		$objCurrentWorksheet->getStyle( 'D' . $intCurrentRowNumber . ':' . 'D' . ( $intCurrentRowNumber + 4 ) )->applyFromArray( $this->getStyle( 'border_right' ) );
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'D' . ( $intCurrentRowNumber + 4 ) );

		$strBillingAddress		= '';
		$boolCareOfSign	= false;

		if( 'Primary Account' != $this->m_objAccount->getAccountName() ) {
			$strBillingAddress		= $this->m_objAccount->getAccountName();
			$boolCareOfSign	= true;
		}

		if( true == $boolCareOfSign ) {
			$strBillingAddress .= PHP_EOL . 'C/O ' . $this->m_objClient->getCompanyName();
		} else {
			$strBillingAddress .= PHP_EOL . $this->m_objClient->getCompanyName();
		}

		$strBillingAddress .= PHP_EOL . $this->m_objAccount->getBilltoStreetLine1();

		if( false == is_null( $this->m_objAccount->getBilltoStreetLine2() ) ) {
			$strBillingAddress .= PHP_EOL . $this->m_objAccount->getBilltoStreetLine2();
		}

		if( false == is_null( $this->m_objAccount->getBilltoStreetLine3() ) ) {
			$strBillingAddress .= PHP_EOL . $this->m_objAccount->getBilltoStreetLine3();
		}

		$strBillingAddress .= PHP_EOL . $this->m_objAccount->getBilltoCity() . ' ' . $this->m_objAccount->getBilltoStateCode() . ' ' . $this->m_objAccount->getBilltoPostalCode();
		$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $strBillingAddress );

		$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 4 ) )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );

		if( CCurrency::CURRENCY_CODE_CAD == $this->getCurrencyCode() ) {
			$intCurrentRowNumber += 5;
		} else {
			// Show "Auto pay" text if necessary
			$strDoNotPay = '';
			if( $this->m_objAccount->getPaymentTypeId() >= CPaymentType::ACH ) {

				if( CAccountType::UTILITY_BILLING == $this->m_objAccount->getAccountTypeId() ) {
					$strDoNotPay = \Psi\CStringService::singleton()->strtoupper( 'Do Not Pay' );
				} else {
					$strDoNotPay = \Psi\CStringService::singleton()->strtoupper( 'Auto Pay' );
				}

				$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 2 ) );
				$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_size_largest' ) );
				$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, $strDoNotPay );

				// row Auto Pay
				$intCurrentRowNumber += 3;
				$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 1 ) );

				$strDoNotPay = 'Payment will be automatically withdrawn ';
				if( CPaymentType::ACH == $this->m_objAccount->getPaymentTypeId() ) {
					$strDoNotPay .= PHP_EOL . 'from account ' . $this->m_objAccount->getCheckAccountNumberMasked() . ' on ' . $this->getInvoiceDueDate() . '.';
				} elseif( true == in_array( $this->m_objAccount->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
					$strDoNotPay .= PHP_EOL . 'from CC ' . $this->m_objAccount->getCcCardNumberMasked() . ' on ' . $this->getInvoiceDueDate() . '.';
				}

				$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_normal' ) );
				$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, $strDoNotPay );
				$intCurrentRowNumber += 2;
			} elseif( $this->m_objAccount->getPaymentTypeId() < CPaymentType::ACH ) {

				$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
				$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 4 ) )->applyFromArray( $this->getStyle( 'font_normal' ) );
				$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_style_italic' ) );

				// Build amount enclosed area
				if( true == valArr( $this->m_arrobjAccountAccountGroups ) && true == array_key_exists( CAccountGroup::TYPE_RIVERSTONE, $this->m_arrobjAccountAccountGroups ) ) {
					$strDoNotPay = 'Make checks payable to Property Solutions USA'; // Riverstone needs to remain Property Solutions USA[SRB]
				} else {
					$strDoNotPay = 'Make checks payable to \'' . CONFIG_COMPANY_NAME . '\'';
				}
				$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, $strDoNotPay );
				// row Amount Enclosed
				$intCurrentRowNumber++;
				$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
				$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
				$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );
				$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, 'Amount Enclosed' );
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, '--' );
				$intCurrentRowNumber += 4;
			}
		}

		// row Blank
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		if( CCurrency::CURRENCY_CODE_CAD == $this->getCurrencyCode() ) {
			$strEftPaymentSentToDetails = 'EFT Payments Sent To:' . PHP_EOL . 'Institutional ID/Routing Number: 000100040' . PHP_EOL . 'Account Number: 1735441' . PHP_EOL . 'SWIFT: BOFMCAM2' . PHP_EOL . 'GST Registration Number: 733238315 RT0001' . PHP_EOL . 'Email billing inquiries to ' . CSystemEmail::ENTRATA_BILLING_EMAIL_ADDRESS . '.';

			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 4 ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 4 ) )->applyFromArray( $this->getStyle( 'font_style_italic' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . ( $intCurrentRowNumber + 4 ) )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, $strEftPaymentSentToDetails );
			$intCurrentRowNumber += 3;
		} else {
			// Mail payments
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_style_italic' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Mail payments to : ' . $strMailToPaymentPoBox );

			// row  Billing Inquiries Email
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_style_italic' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
			$objCurrentWorksheet->setCellValue( 'A' . $intCurrentRowNumber, 'Email billing inquiries to ' . CSystemEmail::ENTRATA_BILLING_EMAIL_ADDRESS . '.' );
		}

		// row  Blank
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		if( true == is_numeric( $this->getUtilityBatchId() ) ) {

			$objCurrentWorksheet = $this->loadUtilityBillingTransactionXlsxGrid( $objAdminDatabase, $objUtilitiesDatabase, $objCurrentWorksheet );
			$intCurrentRowNumber = $objCurrentWorksheet->getHighestRow();
		} else {

			$objCurrentWorksheet = $this->loadStandardTransactionXlsxGrid( $objCurrentWorksheet, $objAdminDatabase );
			$intCurrentRowNumber = $objCurrentWorksheet->getHighestRow();
		}

		// row  Blank
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		if( true == is_null( $this->getUtilityBatchId() ) ) {
			// subtotal
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'F' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
			$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );
			$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'border_bottom' ) );
			$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, 'Sub Total:' );

			$intTotalChargesAmount = $this->getNewChargesTotal() + $this->getNewRecurringChargesTotal() + $this->getNewSetUpChargesTotal();

			if( 0 > $intTotalChargesAmount ) {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' ( ' . __( '{%m, 0, p:2}', [ ( -1 ) * $intTotalChargesAmount ] ) . ' )' );
			} else {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $intTotalChargesAmount ] ) );
			}

			// Total Sales tax
			$intCurrentRowNumber++;
			$objCurrentWorksheet->mergeCells( 'F' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
			$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
			$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );
			$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
			$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'border_bottom' ) );
			$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, 'Total Sales Tax:' );

			if( 0 == $intTotalSalesTaxAmount ) {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' - ' );
			} else if( 0 > $intTotalSalesTaxAmount ) {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' ( ' . __( '{%m, 0, p:2}', [ ( -1 ) * $intTotalSalesTaxAmount ] ) . ' )' );
			} else {
				$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $intTotalSalesTaxAmount ] ) );
			}
		}

		// row Amount Due
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'F' . $intCurrentRowNumber . ':' . 'G' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background' ) );
		$objCurrentWorksheet->getStyle( 'F' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'font_bold' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'border_bottom' ) );
		$objCurrentWorksheet->setCellValue( 'F' . $intCurrentRowNumber, $strAmountDueText . ':' );

		if( 0 > $strTotal ) {
			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, ' ( ' . __( '{%m, 0, p:2}', [ ( -1 ) * $strTotal ] ) . $strCurrencyCode . ' )' );
		} else {
			$objCurrentWorksheet->setCellValue( 'H' . $intCurrentRowNumber, __( '{%m, 0, p:2}', [ $strTotal ] ) . $strCurrencyCode );
		}

		// row Blank
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		// row Footer Text
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'B' . $intCurrentRowNumber . ':' . 'D' . $intCurrentRowNumber );
		$objCurrentWorksheet->mergeCells( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'E' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'align_horizontal_right' ) );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'border_outline' ) );
		$objCurrentWorksheet->setCellValue( 'B' . $intCurrentRowNumber, 'www.' . CONFIG_COMPANY_BASE_DOMAIN );
		$objCurrentWorksheet->setCellValue( 'E' . $intCurrentRowNumber, $strFooterText );

		// row Blank
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );

		// row end_of_invoice
		$intCurrentRowNumber++;
		$objCurrentWorksheet->mergeCells( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber );
		$objCurrentWorksheet->getStyle( 'A' . $intCurrentRowNumber . ':' . 'H' . $intCurrentRowNumber )->applyFromArray( $this->getStyle( 'background_end_of_invoice' ) );

		return $objPHPExcel;
	}

	public function buildHtmlInvoice( $objAdminDatabase, $objUtilitiesDatabase ) {

		$arrmixOutstandingItems = [];
		$arrmixAccountActivityDetails = [];
		$boolHasDetail = false;
		$strAutoPayDate = '';

		// These should all be pre-loaded unless this client quit. We only set current companies by default.
		if( false == valObj( $this->m_objAccount, 'CAccount' ) ) {
			$this->m_objAccount = $this->fetchAccount( $objAdminDatabase );
		}

		if( false == $this->m_boolHasLoadedAccountAccountGroups ) {
			$this->m_arrobjAccountAccountGroups = $this->m_objAccount->getOrFetchAccountAccountGroups( $objAdminDatabase );
		} else {
			$this->m_arrobjAccountAccountGroups = $this->m_objAccount->getAccountAccountGroups( $objAdminDatabase );
		}

		$this->m_arrobjAccountAccountGroups = rekeyObjects( 'AccountGroupId', $this->m_arrobjAccountAccountGroups );

		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->m_objClient = $this->fetchClient( $objAdminDatabase );
		}

		$strTitle = 'Invoice';
		if( true == ( int ) $this->getIsCreditMemo() ) {
			$strTitle = 'Credit';
		}
		$strTitle .= ' Number ' . \Psi\CStringService::singleton()->str_pad( ( string ) $this->getInvoiceNumber(), 9, '0', STR_PAD_LEFT );

		if( false == $this->m_objAccount->getPaymentTypeId() < CPaymentType::ACH ) {
			$this->setInvoiceDueDate( $this->getInvoiceDate() );
		}

		// According to new invoice we should show only the total of the current charges, previous balance and past due invoice will not be considered.
		$intTotalSalesTaxAmount = $this->getSalesTaxNewChargesTotal() + $this->getSalesTaxRecurringChargesTotal() + $this->getSalesTaxSetupChargesTotal() + $this->getSalesTaxPaymentsTotal();
		$this->m_fltInvoiceTotal = $this->getNewChargesTotal() + $this->getNewRecurringChargesTotal() + $this->getNewSetUpChargesTotal() + $intTotalSalesTaxAmount + $this->getNewUtilityChargesTotal();

		if( false == ( int ) $this->getIsCreditMemo() ) {
			$strInvoiceDate 	= date( 'm/d/Y', strtotime( $this->getInvoiceDate() ) );
			$fltTotalAmountDue 	= $this->m_objAccount->fetchAccountBalanceByDate( $strInvoiceDate, $objAdminDatabase );

			// On the track 2 invoices, under "Auto Pay" the statement says we will debit their account on the 10th.  We will debit on the 11th  not the 10th. task id : 421033

			if( $this->m_objAccount->getInvoiceMethodId() == CInvoiceMethod::TRACK_2 ) {

				$strFirstDayOfMonth	 = date( 'm/01/Y', strtotime( $this->getInvoiceDate() ) );
				$strLastDayOfMonth	 = date( 'm/t/Y', strtotime( $this->getInvoiceDate() ) );
				$strTrackTwoDate	 = date( 'm/10/Y', strtotime( $this->getInvoiceDate() ) );

				if( $this->getInvoiceDate() >= $strFirstDayOfMonth && $this->getInvoiceDate() < $strTrackTwoDate ) {
					$strAutoPayDate	 = date( 'm/11/Y', strtotime( $this->getInvoiceDate() ) );

				} elseif( $strTrackTwoDate < $this->getInvoiceDate() && $this->getInvoiceDate() <= $strLastDayOfMonth ) {
					$objDateTime	 = new DateTime( $this->getInvoiceDate() );
					$strAutoPayDate	 = $objDateTime->setDate( $objDateTime->format( 'Y' ), $objDateTime->format( 'm' ), 11 )->modify( 'next month' );
					$strAutoPayDate	 = $strAutoPayDate->format( 'm/d/Y' );
				} else {
					$strAutoPayDate	 = date( 'm/d/Y', strtotime( ' +1 day', strtotime( $this->getInvoiceDueDate() ) ) );
				}
			} else {
				$strAutoPayDate = $this->getInvoiceDueDate();
			}
		}

		$arrmixInvoiceTransactionsData = CInvoices::createService()->fetchInvoiceTransactionsByInvoiceId( $this->getId(), $objAdminDatabase );

		$boolIsMasterPolicyAccount = ( CAccountType::MASTER_POLICY == $this->m_objAccount->getAccountTypeId() ) ? true : false;

		$strPoBoxCode          = '';
		$strMailToPaymentPoBox = '';
		$strBillingEmail = 'Email billing inquiries to ' . CSystemEmail::ENTRATA_BILLING_EMAIL_ADDRESS;
		if( true == $boolIsMasterPolicyAccount ) {
			$strPoBoxCode = 'P.O. BOX 477 - Lehi, UT 84043';
			$strMailToPaymentPoBox = 'P.O. BOX 477, Lehi, UT 84043';
			$strChecksPayable = 'Make checks payable to \'Property Solutions Insurance Agency LLC\'';
		} else {
			// get remittance address using currency code or bill to country code
			$arrmixRemittanceAddresses = CRemittanceAddresses::createService()->fetchRemittanceAddressByCurrencyCode( $this->getCurrencyCode(), NULL, $objAdminDatabase );

			if( true == valArr( $arrmixRemittanceAddresses ) ) {
				$strPoBoxCode = $strMailToPaymentPoBox = $arrmixRemittanceAddresses['street_address'] . ', ' . $arrmixRemittanceAddresses['mailing_address'] . ' ' . $arrmixRemittanceAddresses['postal_code'];
			}

			if( true == valArr( $this->m_arrobjAccountAccountGroups ) && true == array_key_exists( CAccountGroup::TYPE_RIVERSTONE, $this->m_arrobjAccountAccountGroups ) ) {
				$strChecksPayable = 'Make checks payable to Property Solutions USA';
			} else {
				$strChecksPayable = 'Make checks payable to \'' . CONFIG_COMPANY_NAME . '\'';
			}
		}

		if( CPaymentType::ACH > $this->m_objAccount->getPaymentTypeId() ) {
			$strMailToPaymentPoBox = 'Mail payments to: ' . $strMailToPaymentPoBox;
		}

		if( CCurrency::CURRENCY_CODE_CAD == $this->getCurrencyCode() ) {
			$strMailToPaymentPoBox = self::INVOICE_FOOTER_FOR_CANADA;
		}

		if( false == $boolIsMasterPolicyAccount ) {
			if( true == valArr( $arrmixInvoiceTransactionsData ) ) {
				foreach( $arrmixInvoiceTransactionsData as $arrmixTransaction ) {
					if( CChargeCode::RESIDENT_VERIFY_FEES == $arrmixTransaction['charge_code_id'] || $arrmixTransaction['item_count'] > 0 ) {
						$boolHasDetail = true;
					}
				}
			}

			$arrstrBundles					 = [];
			$arrstrBundlesFrequencyCharges 	 = [];
			$arrintFrequencyIds = [ CFrequency::QUARTERLY, CFrequency::SEMI_ANNUALLY, CFrequency::YEARLY ];
			$arrintUtilityChargeCodes = [ CChargeCode::UTILITY_BILLING_FEES => CChargeCode::UTILITY_BILLING_FEES, CChargeCode::UTILITY_MOVE_IN_FEES => CChargeCode::UTILITY_MOVE_IN_FEES ];

			// grouping of charges based on bundle, frequency or charge codes
			if( true == valArr( $arrmixInvoiceTransactionsData ) ) {
				foreach( $arrmixInvoiceTransactionsData as $arrmixTransaction ) {

					// to show back billing bundle changes bundle id grouped with post month
					$strBundleKey = $arrmixTransaction['bundle_ps_product_id'] . strtotime( $arrmixTransaction['post_month'] );
					if( true == ( $arrmixTransaction['invoice_category'] == 'Recurring' ) && 0 == $this->getIsCreditMemo() ) {

						if( false == is_null( $arrmixTransaction['bundle_ps_product_id'] ) ) {

							$arrstrBundles['Recurring'][$strBundleKey]['bundle_ps_product_id']	 = $arrmixTransaction['bundle_ps_product_id'];
							$arrstrBundles['Recurring'][$strBundleKey]['transaction_amount']	 = $arrmixTransaction['total'];
							$arrstrBundles['Recurring'][$strBundleKey]['cid']					 = $arrmixTransaction['cid'];
							$arrstrBundles['Recurring'][$strBundleKey]['account_id']			 = $arrmixTransaction['account_id'];

							if( true == in_array( $arrmixTransaction['frequency_id'], $arrintFrequencyIds ) ) {
								$arrstrBundles['Recurring'][$strBundleKey]['date'] = \Psi\CStringService::singleton()->substr( $arrmixTransaction['post_month'] ? $arrmixTransaction['post_month'] : $this->getInvoiceDate(), 0, 10 ) . ' to ' . \Psi\CStringService::singleton()->substr( $arrmixTransaction['date'], 0, 10 );
							} else {
								$arrstrBundles['Recurring'][$strBundleKey]['date'] = $arrmixTransaction['date'];
							}

							$arrstrBundles['Recurring'][$strBundleKey]['transaction_datetime']	 = $arrmixTransaction['date'];
							$arrstrBundles['Recurring'][$strBundleKey]['invoice_category']		 = $arrmixTransaction['invoice_category'];
							$arrstrBundles['Recurring'][$strBundleKey]['tax_state']				 = $arrmixTransaction['tax_state'];
							$arrstrBundles['Recurring'][$strBundleKey]['name']					 = $arrmixTransaction['product_name'];
							if( false == valArr( $arrstrBundles['Recurring'][$strBundleKey]['memo'] ) || false == in_array( $arrmixTransaction['memo'], $arrstrBundles['Recurring'][$strBundleKey]['memo'] ) ) {
								$arrstrBundles['Recurring'][$strBundleKey]['memo'][] = $arrmixTransaction['memo'];
							}
							if( false == isset( $arrstrBundles['Recurring'][$strBundleKey]['amount'] ) || true == is_null( $arrstrBundles['Recurring'][$strBundleKey]['amount'] ) ) {
								$arrstrBundles['Recurring'][$strBundleKey]['total'] = 0;
							}
							if( true == isset( $arrmixTransaction['sub_total'] ) && false == is_null( $arrmixTransaction['sub_total'] ) ) {
								$arrstrBundles['Recurring'][$strBundleKey]['sub_total'] += $arrmixTransaction['sub_total'];
							}

							if( true == isset( $arrmixTransaction['total'] ) && false == is_null( $arrmixTransaction['total'] ) ) {
								$arrstrBundles['Recurring'][$strBundleKey]['total'] += $arrmixTransaction['total'];
							}
							if( true == isset( $arrmixTransaction['sales_tax'] ) && false == is_null( $arrmixTransaction['sales_tax'] ) ) {
								$arrstrBundles['Recurring'][$strBundleKey]['sales_tax'] += $arrmixTransaction['sales_tax'];
							}
						} else {

							if( true == in_array( $arrmixTransaction['frequency_id'], $arrintFrequencyIds ) ) {
								$strFrequencyBundleKey = $arrmixTransaction['account_id'] . '_' . $arrmixTransaction['property_id'] . '_' . $arrmixTransaction['charge_code_id'];
                                $strInvoiceToDate = strtotime( $arrmixTransaction['date'] );

                                if( false == is_null( $arrmixTransaction['company_charge_id'] ) ) {
                                    $strFrequencyBundleKey = $arrmixTransaction['line_item_id'];
                                    switch( $arrmixTransaction['frequency_id'] ) {
                                        case CFrequency::YEARLY:
                                            $strInvoiceToDate = strtotime( $arrmixTransaction['date'] . '+11 months' );
                                            break;

                                        case CFrequency::SEMI_ANNUALLY:
                                            $strInvoiceToDate = strtotime( $arrmixTransaction['date'] . '+5 months' );
                                            break;

                                        case CFrequency::QUARTERLY:
                                            $strInvoiceToDate = strtotime( $arrmixTransaction['date'] . '+2 months' );
                                            break;

                                        default:
                                            $this->displayPageMessage( 'A recurring charge with no frequency was found.' );
                                            break;
                                    }
                                }

								$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['date']					 = \Psi\CStringService::singleton()->substr( $arrmixTransaction['post_month'] ? $arrmixTransaction['post_month'] : $arrmixTransaction['date'], 0, 10 ) . ' to ' . ( $arrmixTransaction['post_month'] ? $arrmixTransaction['date'] : date( 'm/d/Y', $strInvoiceToDate ) );
								$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['transaction_amount']	 = $arrmixTransaction['total'];
								$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['cid']					 = $arrmixTransaction['cid'];
								$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['account_id']			 = $arrmixTransaction['account_id'];
								$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['charge_code_id']		 = $arrmixTransaction['charge_code_id'];
								$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['invoice_category']		 = $arrmixTransaction['invoice_category'];
								$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['tax_state']			 = $arrmixTransaction['tax_state'];
								if( false == isset( $arrstrBundles['Recurring'][$strFrequencyBundleKey]['amount'] ) || true == is_null( $arrstrBundles['Recurring'][$strBundleKey]['amount'] ) ) {
									$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['total'] = 0;
								}
								if( true == isset( $arrmixTransaction['sub_total'] ) && false == is_null( $arrmixTransaction['sub_total'] ) ) {
									$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['sub_total'] += $arrmixTransaction['sub_total'];
								}

								if( true == isset( $arrmixTransaction['total'] ) && false == is_null( $arrmixTransaction['total'] ) ) {
									$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['total'] += $arrmixTransaction['total'];
								}
								if( true == isset( $arrmixTransaction['sales_tax'] ) && false == is_null( $arrmixTransaction['sales_tax'] ) ) {
									$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['sales_tax'] += $arrmixTransaction['sales_tax'];
								}

								if( false == valArr( $arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['memo'] ) || false == in_array( $arrmixTransaction['memo'], $arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['memo'] ) ) {
									$arrstrBundlesFrequencyCharges['Recurring'][$strFrequencyBundleKey]['memo'] = $arrmixTransaction['memo'];
								}
							} else {
								$arrmixInvoiceTransactions[] = $arrmixTransaction;
							}
						}

					} elseif( true == ( $arrmixTransaction['invoice_category'] == 'Set Up' ) && 0 == $this->getIsCreditMemo() ) {

						if( false == is_null( $arrmixTransaction['bundle_ps_product_id'] ) ) {
							$arrstrBundles['setup'][$strBundleKey]['bundle_ps_product_id']	 = $arrmixTransaction['bundle_ps_product_id'];
							$arrstrBundles['setup'][$strBundleKey]['transaction_amount']	 = $arrmixTransaction['total'];
							$arrstrBundles['setup'][$strBundleKey]['cid']					 = $arrmixTransaction['cid'];
							$arrstrBundles['setup'][$strBundleKey]['account_id']			 = $arrmixTransaction['account_id'];
							$arrstrBundles['setup'][$strBundleKey]['charge_code_id']		 = $arrmixTransaction['charge_code_id'];
							$arrstrBundles['setup'][$strBundleKey]['date']					 = $arrmixTransaction['date'];
							$arrstrBundles['setup'][$strBundleKey]['transaction_datetime']	 = $arrmixTransaction['date'];
							$arrstrBundles['setup'][$strBundleKey]['invoice_category']		 = $arrmixTransaction['invoice_category'];
							$arrstrBundles['setup'][$strBundleKey]['tax_state']				 = $arrmixTransaction['tax_state'];
							$arrstrBundles['setup'][$strBundleKey]['name']					 = $arrmixTransaction['product_name'];
							$arrstrBundles['setup'][$strBundleKey]['memo'][]				 = $arrmixTransaction['memo'];

							if( false == isset( $arrstrBundles['setup'][$strBundleKey]['amount'] ) || true == is_null( $arrstrBundles['setup'][$strBundleKey]['amount'] ) ) {
								$arrstrBundles['setup'][$strBundleKey]['total'] = 0;
							}
							if( true == isset( $arrmixTransaction['sub_total'] ) && false == is_null( $arrmixTransaction['sub_total'] ) ) {
								$arrstrBundles['setup'][$strBundleKey]['sub_total'] += $arrmixTransaction['sub_total'];
							}

							if( true == isset( $arrmixTransaction['total'] ) && false == is_null( $arrmixTransaction['total'] ) ) {
								$arrstrBundles['setup'][$strBundleKey]['total'] += $arrmixTransaction['total'];
							}
							if( true == isset( $arrmixTransaction['sales_tax'] ) && false == is_null( $arrmixTransaction['sales_tax'] ) ) {
								$arrstrBundles['setup'][$strBundleKey]['sales_tax'] += $arrmixTransaction['sales_tax'];
							}

						} else {
							$arrmixInvoiceTransactions[] = $arrmixTransaction;
						}
					} elseif( true == ( $arrmixTransaction['invoice_category'] == 'Other' ) && 1 == $this->getIsCreditMemo() ) {
						if( false == is_null( $arrmixTransaction['bundle_ps_product_id'] ) ) {

							$arrstrBundles['Other'][$strBundleKey]['bundle_ps_product_id']	 = $arrmixTransaction['bundle_ps_product_id'];
							$arrstrBundles['Other'][$strBundleKey]['transaction_amount']	 = $arrmixTransaction['total'];
							$arrstrBundles['Other'][$strBundleKey]['cid']					 = $arrmixTransaction['cid'];
							$arrstrBundles['Other'][$strBundleKey]['account_id']			 = $arrmixTransaction['account_id'];
							$arrstrBundles['Other'][$strBundleKey]['charge_code_id']		 = $arrmixTransaction['charge_code_id'];
							$arrstrBundles['Other'][$strBundleKey]['date']					 = $arrmixTransaction['date'];
							$arrstrBundles['Other'][$strBundleKey]['transaction_datetime']	 = $arrmixTransaction['date'];
							$arrstrBundles['Other'][$strBundleKey]['invoice_category']		 = $arrmixTransaction['invoice_category'];
							$arrstrBundles['Other'][$strBundleKey]['tax_state']				 = $arrmixTransaction['tax_state'];
							$arrstrBundles['Other'][$strBundleKey]['name']					 = $arrmixTransaction['product_name'];
							$arrstrBundles['Other'][$strBundleKey]['memo'][]				 = $arrmixTransaction['memo'];

							if( false == isset( $arrstrBundles['Other'][$strBundleKey]['amount'] ) || true == is_null( $arrstrBundles['Other'][$strBundleKey]['amount'] ) ) {
								$arrstrBundles['Other'][$strBundleKey]['total'] = 0;
							}
							if( true == isset( $arrmixTransaction['sub_total'] ) && false == is_null( $arrmixTransaction['sub_total'] ) ) {
								$arrstrBundles['Other'][$strBundleKey]['sub_total'] += $arrmixTransaction['sub_total'];
							}

							if( true == isset( $arrmixTransaction['total'] ) && false == is_null( $arrmixTransaction['total'] ) ) {
								$arrstrBundles['Other'][$strBundleKey]['total'] += $arrmixTransaction['total'];
							}
							if( true == isset( $arrmixTransaction['sales_tax'] ) && false == is_null( $arrmixTransaction['sales_tax'] ) ) {
								$arrstrBundles['Other'][$strBundleKey]['sales_tax'] += $arrmixTransaction['sales_tax'];
							}

						} else {
							$arrmixInvoiceTransactions[] = $arrmixTransaction;
						}

					} elseif( true == ( $arrmixTransaction['invoice_category'] == 'Utility' ) && 0 == $this->getIsCreditMemo() ) {
						$strKey = $arrmixTransaction['charge_code_id'] . '~' . $arrmixTransaction['memo'];

						if( true == in_array( $arrmixTransaction['charge_code_id'], $arrintUtilityChargeCodes ) ) {

							$arrstrBundles['Utility'][$strKey]['bundle_ps_product_id']	 = $arrmixTransaction['bundle_ps_product_id'];
							$arrstrBundles['Utility'][$strKey]['transaction_amount']	 = $arrmixTransaction['total'];
							$arrstrBundles['Utility'][$strKey]['cid']					 = $arrmixTransaction['cid'];
							$arrstrBundles['Utility'][$strKey]['account_id']			 = $arrmixTransaction['account_id'];
							$arrstrBundles['Utility'][$strKey]['charge_code_id']		 = $arrmixTransaction['charge_code_id'];
							$arrstrBundles['Utility'][$strKey]['date']					 = $arrmixTransaction['date'];
							$arrstrBundles['Utility'][$strKey]['transaction_datetime']	 = $arrmixTransaction['date'];
							$arrstrBundles['Utility'][$strKey]['invoice_category']		 = $arrmixTransaction['invoice_category'];
							$arrstrBundles['Utility'][$strKey]['tax_state']				 = $arrmixTransaction['tax_state'];
							$arrstrBundles['Utility'][$strKey]['name']					 = $arrmixTransaction['product_name'];
							$arrstrBundles['Utility'][$strKey]['memo']					 = $arrmixTransaction['memo'];
							$arrstrBundles['Utility'][$strKey]['property_id']			 = $arrmixTransaction['property_id'];

							if( false == isset( $arrstrBundles['Utility'][$strKey]['amount'] ) || true == is_null( $arrstrBundles['Utility'][$strKey]['amount'] ) ) {
								$arrstrBundles['Utility'][$strKey]['total'] = 0;
							}
							if( true == isset( $arrmixTransaction['item_count'] ) && false == is_null( $arrmixTransaction['item_count'] ) ) {
								$arrstrBundles['Utility'][$strKey]['item_count'] += $arrmixTransaction['item_count'];
							}
							if( true == isset( $arrmixTransaction['sub_total'] ) && false == is_null( $arrmixTransaction['sub_total'] ) ) {
								$arrstrBundles['Utility'][$strKey]['sub_total'] += $arrmixTransaction['sub_total'];
							}

							if( true == isset( $arrmixTransaction['total'] ) && false == is_null( $arrmixTransaction['total'] ) ) {
								$arrstrBundles['Utility'][$strKey]['total'] += $arrmixTransaction['total'];
							}
							if( true == isset( $arrmixTransaction['sales_tax'] ) && false == is_null( $arrmixTransaction['sales_tax'] ) ) {
								$arrstrBundles['Utility'][$strKey]['sales_tax'] += $arrmixTransaction['sales_tax'];
							}
						} else {
							$arrmixInvoiceTransactions[] = $arrmixTransaction;
						}

					} else {
						$arrmixInvoiceTransactions[] = $arrmixTransaction;

					}
				}
			}

			if( true == valArr( $arrstrBundlesFrequencyCharges ) && true == valArr( $arrstrBundlesFrequencyCharges['Recurring'] ) ) {
				foreach( $arrstrBundlesFrequencyCharges['Recurring'] as $strBundleKey => $arrstrBundle ) {
					$arrmixInvoiceTransactions[] = $arrstrBundle;
				}
			}
			if( true == valArr( $arrstrBundles ) && true == valArr( $arrstrBundles['Recurring'] ) ) {
				krsort( $arrstrBundles['Recurring'] );
				foreach( $arrstrBundles['Recurring'] as $strBundleKey => $arrstrBundle ) {
					$arrmixInvoiceTransactions[] = $arrstrBundle;
				}
			}
			if( true == valArr( $arrstrBundles ) && true == valArr( $arrstrBundles['setup'] ) ) {
				foreach( $arrstrBundles['setup'] as $strBundleKey => $arrstrBundle ) {
					$arrmixInvoiceTransactions[] = $arrstrBundle;
				}
			}
			if( true == valArr( $arrstrBundles ) && true == valArr( $arrstrBundles['Other'] ) ) {
				krsort( $arrstrBundles['Other'] );
				foreach( $arrstrBundles['Other'] as $strBundleKey => $arrstrBundle ) {
					$arrmixInvoiceTransactions[] = $arrstrBundle;
				}
			}
			if( true == valArr( $arrstrBundles ) && true == valArr( $arrstrBundles['Utility'] ) ) {
				foreach( $arrstrBundles['Utility'] as $strBundleKey => $arrstrBundle ) {
					$arrmixInvoiceTransactions[] = $arrstrBundle;
				}
			}

			if( true == ( int ) $this->getIsCreditMemo() ) {
				$arrmixInvoiceTransactions['Other'] = $arrmixInvoiceTransactions;
			} else {
				$arrmixInvoiceTransactions = rekeyArray( 'invoice_category', $arrmixInvoiceTransactions, false, true, true );
			}

			if( true == $boolHasDetail && true == valArr( $arrmixInvoiceTransactions ) && true == valArr( $arrmixInvoiceTransactions['Other'] ) ) {
				foreach( $arrmixInvoiceTransactions['Other'] as $intKey => $arrmixTransaction ) {
					$arrmixInvoiceTransactions['Other'][$intKey]['DetailLink']	= $this->getInvoiceDetailLink( $arrmixTransaction, $objAdminDatabase, $boolNewInvoice = true );
				}
			}

			if( true == valArr( $arrmixInvoiceTransactions ) && true == valArr( $arrmixInvoiceTransactions['Utility'] ) ) {
				$strStartDate 	= date( 'm/01/Y', strtotime( '-1 month', strtotime( $this->getInvoiceDate() ) ) );
				$strEndDate 	= date( 'm/t/Y', strtotime( '-1 month', strtotime( $this->getInvoiceDate() ) ) );
				foreach( $arrmixInvoiceTransactions['Utility'] as $intKey => $arrmixTransaction ) {
					if( CChargeCode::UTILITY_BILLING_FEES == $arrmixTransaction['charge_code_id'] ) {
						$arrmixInvoiceTransactions['Utility'][$intKey]['memo'] = $arrmixTransaction['memo'] . ' ( ' . $strStartDate . ' to ' . $strEndDate . ')';
					}
					$arrmixInvoiceTransactions['Utility'][$intKey]['DetailLink'] = $this->getUtilityReportLink( $arrmixTransaction['property_id'], $this->getInvoiceDate() );
				}
			}

		} else if( true == valArr( $arrmixInvoiceTransactionsData ) ) {

			$arrstrBundles				= [];
			$arrmixInvoiceTransactions	= [];

			foreach( $arrmixInvoiceTransactionsData as $arrmixTransaction ) {

				if( false == is_null( $arrmixTransaction['bundle_ps_product_id'] ) ) {

					if( true == in_array( $arrmixTransaction['bundle_ps_product_id'], [ CProductBundle::PRIOR_MONTH_ENROLLMENTS, CProductBundle::PRIOR_MONTH_CANCELLATIONS ] ) ) {
						$strProductName = $arrmixTransaction['product_name'] . ' - ' . date( 'm/Y', strtotime( '-1 month', strtotime( $arrmixTransaction['date'] ) ) );
					} elseif( CProductBundle::CURRENT_MONTH_RENEWALS == $arrmixTransaction['bundle_ps_product_id'] ) {
						$strProductName = $arrmixTransaction['product_name'] . ' - ' . date( 'm/Y', strtotime( $arrmixTransaction['date'] ) );
					} else {
						$strProductName = $arrmixTransaction['product_name'];
					}

					$arrstrBundles[$arrmixTransaction['bundle_ps_product_id']]['memo']		= $strProductName;
					$arrstrBundles[$arrmixTransaction['bundle_ps_product_id']]['date']		= $arrmixTransaction['date'];
					$arrstrBundles[$arrmixTransaction['bundle_ps_product_id']]['sub_total']	+= $arrmixTransaction['sub_total'];
					$arrstrBundles[$arrmixTransaction['bundle_ps_product_id']]['sales_tax']	+= $arrmixTransaction['sales_tax'];

					if( true == $arrmixTransaction['is_show_details'] ) {
						$arrstrBundles[$arrmixTransaction['bundle_ps_product_id']]['memo_details'][] = $arrmixTransaction['memo'];
					}

				} else {
					$arrmixInvoiceTransactions[] = $arrmixTransaction;
				}
			}

			if( true == valArr( $arrstrBundles ) ) {

				foreach( $arrstrBundles as $arrstrBundle ) {
					$arrmixInvoiceTransactions[] = $arrstrBundle;
				}
			}

			$arrmixInvoiceTransactionsData = $arrmixInvoiceTransactions;
		}

		if( true == is_numeric( $this->getUtilityBatchId() ) ) {
			$arrmixUtilityTransactionData = $this->loadUtilityBillingTransactionData( $objAdminDatabase, $objUtilitiesDatabase );
		} else {
			if( false == $boolIsMasterPolicyAccount ) {
				$arrmixAccountActivityDetails = CInvoices::createService()->fetchAccountActivitiesByInvoiceId( $this->getId(), $objAdminDatabase );
			}
			// invoice due information
			$arrmixOutstandingItems = CInvoices::createService()->fetchOutstandingItemsByInvoiceId( $this->getId(), $objAdminDatabase );
		}

		$arrstrSalesTaxDetails = [];
		if( true == $this->getIsNonBilledSalesTax() || CCountry::CODE_CANADA == $this->getAccount()->getBilltoCountryCode() ) {
			$arrstrSalesTaxDetails = \Psi\Eos\Admin\CSalesTaxTransactionDetails::createService()->fetchSalesTaxTransactionDetailsForNonBilledSalesTaxInvoice( $this->getId(), $objAdminDatabase );
		}

		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		$strFormat = CCountry::CODE_USA == $this->m_objAccount->getBilltoCountryCode() ? \Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressFormat::NATIONAL_FORMAT : \Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressFormat::INTERNATIONAL_FORMAT;
		$arrstrFormattedAddress = $objPostalAddressService->format( $this->m_objAccount->getPostalAddress(), [], $strFormat );
		$objCurrency = \Psi\Eos\Admin\CCurrencies::createService()->fetchCurrencyByCurrencyCode( $this->getCurrencyCode(), $objAdminDatabase );

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$arrmixTemplateParameters = [];

		$arrmixTemplateParameters['invoice'] = $this;
		$arrmixTemplateParameters['account_total'] = $fltTotalAmountDue;
		$arrmixTemplateParameters['account'] = $this->m_objAccount;
		$arrmixTemplateParameters['client'] = $this->m_objClient;
		$arrmixTemplateParameters['invoice_transactions'] = $arrmixInvoiceTransactions;
		$arrmixTemplateParameters['account_activity'] = $arrmixAccountActivityDetails;
		$arrmixTemplateParameters['outstanding_items'] = $arrmixOutstandingItems;
		$arrmixTemplateParameters['company_base_domain'] = CONFIG_COMPANY_BASE_DOMAIN;
		$arrmixTemplateParameters['invoice_logo'] = CONFIG_COMMON_PATH . '/images/resident_invoice/invoice_logo.svg';

		if( true == $boolIsMasterPolicyAccount ) {
			$arrmixTemplateParameters['invoice_logo'] = CONFIG_COMMON_PATH . '/images/logos/' . CONFIG_ENTRATA_LOGO_PREFIX . 'ri_logo.png';
			$arrmixTemplateParameters['invoice_transactions'] = $arrmixInvoiceTransactionsData;
		}

		$arrmixTemplateParameters['invoice_bar_code'] = CONFIG_COMMON_PATH . '/images/resident_invoice/linear_0.png';
		$arrmixTemplateParameters['po_box_code'] = $strPoBoxCode;
		$arrmixTemplateParameters['billing_email_enquiry'] = $strBillingEmail;
		$arrmixTemplateParameters['make_payable_checks'] = $strChecksPayable;
		$arrmixTemplateParameters['mail_to_payment_box_code'] = $strMailToPaymentPoBox;
		$arrmixTemplateParameters['title'] = $strTitle;
		$arrmixTemplateParameters['has_detail'] = $boolHasDetail;
		$arrmixTemplateParameters['auto_pay_date'] = $strAutoPayDate;

		$arrmixTemplateParameters['is_master_policy'] = $boolIsMasterPolicyAccount;
		$arrmixTemplateParameters['sales_tax_details'] = $arrstrSalesTaxDetails;
		$arrmixTemplateParameters['CURRENCY_CODE_EUR'] = CCurrency::CURRENCY_CODE_EUR;
		$arrmixTemplateParameters['CURRENCY_CODE_USD'] = CCurrency::CURRENCY_CODE_USD;
		$arrmixTemplateParameters['CURRENCY_CODE_CAD'] = CCurrency::CURRENCY_CODE_CAD;
		$arrmixTemplateParameters['account_billing_address'] = $arrstrFormattedAddress;
		$arrmixTemplateParameters['currency_symbol'] = $objCurrency->getSymbol();

		$arrmixTemplateParameters['payment_type_ach']			= CPaymentType::ACH;
		$arrmixTemplateParameters['payment_type_cc']			= CPaymentType::$c_arrintCreditCardPaymentTypes;

		CCountry::assignTemplateConstants( $arrmixTemplateParameters );

		if( true == valId( $this->getUtilityBatchId() ) ) {

			$arrmixTemplateParameters['utility_transaction_data'] = $arrmixUtilityTransactionData;

			$objRenderTemplate		= new CRenderTemplate( 'ps_lead/accounting/invoices/utility_invoice.tpl', PATH_INTERFACES_CLIENT_ADMIN, false, $arrmixTemplateParameters );

			$strHtmlInvoiceContent	= $objRenderTemplate->fetchTemplate( PATH_INTERFACES_CLIENT_ADMIN . 'ps_lead/accounting/invoices/utility_invoice.tpl' );

		} else {

			$objRenderTemplate		= new CRenderTemplate( 'ps_lead/accounting/invoices/invoice.tpl', PATH_INTERFACES_CLIENT_ADMIN, false, $arrmixTemplateParameters );

			$strHtmlInvoiceContent	= $objRenderTemplate->fetchTemplate( PATH_INTERFACES_CLIENT_ADMIN . 'ps_lead/accounting/invoices/invoice.tpl' );

		}

		return $strHtmlInvoiceContent;
	}

	public function buildHtmlPaymentErrorEmailContent() {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$arrmixTemplateParameters['invoice']					= $this;
		$arrmixTemplateParameters['client']						= $this->m_objClient;
		$arrmixTemplateParameters['account']					= $this->m_objAccount;
		$arrmixTemplateParameters['payment_types']				= $this->m_arrobjPaymentTypes;
		$arrmixTemplateParameters['company_payment']			= $this->m_objCompanyPayment;
		$arrmixTemplateParameters['base_url']					= CONFIG_COMMON_PATH;
		$arrmixTemplateParameters['CONFIG_COMPANY_NAME']		= CConfig::get( 'company_name' );
		$arrmixTemplateParameters['CONFIG_ENTRATA_LOGO_PREFIX']	= CONFIG_ENTRATA_LOGO_PREFIX;
		$arrmixTemplateParameters['HTTP_COMPANY_BASE_URL']		= CONFIG_INSECURE_HOST_PREFIX . 'www.' . CONFIG_COMPANY_BASE_DOMAIN;

		$objRenderTemplate 	= new CRenderTemplate( PATH_INTERFACES_CLIENT_ADMIN . 'ps_lead/accounting/invoices/payment_posting_error_email.tpl', PATH_INTERFACES_CLIENT_ADMIN, false, $arrmixTemplateParameters );
		return $objRenderTemplate->nestedFetchTemplate( PATH_INTERFACES_CLIENT_ADMIN . 'ps_lead/accounting/invoices/payment_posting_error_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . '_common/layouts/ps_email.tpl' );

	}

	// $intCompanyPaymentId this parameter in postInvoicePayment for checking if company payment is created,
	// but failed to create transaction then it will add only transaction to corresponding company_payment. task id : 704655

	public function postInvoicePayment( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $boolUseAccountBalance = true, $intCompanyPaymentId = NULL, $intInvoiceBatchId = NULL ) {
		$boolIsValid = true;

		$fltPaymentAmount = $this->m_fltInvoiceAmount;

		if( true == $boolUseAccountBalance ) {
			$fltPaymentAmount = $this->getAccount()->fetchPaymentAccountBalance( $objDatabase );
		}

		if( 0 == bccomp( $fltPaymentAmount, 0, 2 ) ) {
			// Nothing to post.
			return true;
		}

		// make sure that this payment is either 1.) a positive amount or 2.) associated to an intermediary/clearing account
		if( -1 == bccomp( $fltPaymentAmount, 0, 2 ) && CAccountType::INTERMEDIARY != $this->m_objAccount->getAccountTypeId() && CAccountType::CLEARING != $this->m_objAccount->getAccountTypeId() ) {
			// Nothing to post.
			return true;
		}

		// If company payment is created and failed to create transaction then create transaction on existing company payment object do not add new company payment for same invoice.

		if( true == valId( $intCompanyPaymentId ) ) {
			$this->m_objCompanyPayment = CCompanyPayments::createService()->fetchCompanyPaymentById( $intCompanyPaymentId, $objDatabase );
		} else {
			$this->m_objCompanyPayment = $this->m_objAccount->createCompanyPayment( $objDatabase, $objPaymentDatabase );
			$this->m_objCompanyPayment->setId( $this->m_objCompanyPayment->fetchNextId( $objDatabase ) );
			$this->m_objCompanyPayment->setPaymentAmount( $fltPaymentAmount );
			$this->m_objCompanyPayment->setCheckNumber( $this->m_intId );
			$this->m_objCompanyPayment->setBilltoCountryCode( 'US' );
			$this->m_objCompanyPayment->setChargeCodeId( CChargeCode::PAYMENT_RECEIVED );
			$this->m_objCompanyPayment->setInvoiceId( $this->m_intId );
			$this->m_objCompanyPayment->setInvoiceBatchId( $intInvoiceBatchId );
		}

		if( CPaymentType::ACH == $this->m_objCompanyPayment->getPaymentTypeId() ) {
			$objAccountType = \Psi\Eos\Admin\CAccountTypes::createService()->fetchAccountTypeById( $this->m_objAccount->getAccountTypeId(), $objDatabase );

			if( true == valObj( $objAccountType, 'CAccountType' ) ) {
				$this->m_objCompanyPayment->setPsProcessingBankAccountId( $objAccountType->getPsProcessingBankAccountId() );
			}
		}

		if( CPaymentType::AFT == $this->m_objCompanyPayment->getPaymentTypeId() ) {
			$this->m_objCompanyPayment->setPsProcessingBankAccountId( CProcessingBankAccount::BANK_OF_MONTREAL_REVENUE );
		}

		if( true == valArr( $this->m_objCompanyPayment->getErrorMsgs() ) || ( true == is_null( $intCompanyPaymentId ) && false == $this->m_objCompanyPayment->postRpcPayment( $intCompanyUserId, $objDatabase ) ) ) {

			$boolIsValid = false;
			$arrobjErrorMsgs = $this->m_objCompanyPayment->getErrorMsgs();
			$strEmailMessage = 'Payment for :' . $this->m_objClient->getCompanyName() . ' :: Account: ' . $this->m_objAccount->getAccountName() . ' failed to bill.';

			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				$strEmailMessage .= '\n\nError Given:\n\n' . $objErrorMsg->getMessage() . ' :: ';
			}

			$this->addErrorMsgs( $arrobjErrorMsgs );

			$this->sendAdministrativePaymentPostingFailureEmail();
			$this->m_intInvoiceStatusTypeId = CInvoiceStatusType::INITIAL_BILLING_FAILED;

		} else {

			$objTransaction = $this->m_objAccount->createTransaction();
			$this->m_objCompanyPayment->populatePaymentTransaction( $objTransaction, $objDatabase );

			if( false == $objTransaction->postPayment( $intCompanyUserId, $objDatabase ) ) {
				return false;
			}

			$this->setCompanyPaymentId( $this->m_objCompanyPayment->getId() );
			$this->m_intInvoiceStatusTypeId = CInvoiceStatusType::INITIAL_BILLING_SUCCESSFUL;
			$this->addSuccessMsg( new CSuccessMsg( NULL, NULL, 'Initial Billing Succeeded', NULL ) );
		}

		if( false == $this->update( $intCompanyUserId, $objDatabase ) ) {
			trigger_error( 'company invoice failed to update.', E_USER_WARNING );
		}

		return $boolIsValid;
	}

	public function sendAdministrativePaymentPostingFailureEmail() {

		$strEmailBody = $this->buildHtmlPaymentErrorEmailContent();

		$strSubject 			= 'Payment Post Failure';
		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_TO_COMPANY_ACCOUNTING, $strSubject, $strEmailBody, CSystemEmail::ENTRATA_BILLING_EMAIL_ADDRESS );
		$objEmailDatabase 		= CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::INTERNAL_SALES_ACCOUNTING );

		return ( true == $objSystemEmail->validate( VALIDATE_INSERT ) && true == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) );

	}

	public function getInvoiceDetailLink( $arrmixTransaction, $objAdminDatabase, $boolNewInvoice = false ) {
		$strURL 	= '';
		if( true == valArr( $arrmixTransaction ) ) {
			// check to see if this is a charge code that can have detail.
			if( true == in_array( $arrmixTransaction['charge_code_id'], CChargeCode::$c_arrintChargeCodesWithDetail ) ) {

				$strURL = 'http://' . $this->m_objClient->getRwxDomain() . '.' . CONFIG_COMPANY_BASE_DOMAIN;

				if( 'stage' == CConfig::get( 'environment' ) ) {
					$strURL = 'https://' . $this->m_objClient->getRwxDomain() . '.' . basename( $_SERVER['HTTP_HOST'] );
				} elseif( 'production' == CConfig::get( 'environment' ) ) {
					$strURL = 'http://' . $this->m_objClient->getRwxDomain() . '.' . CONFIG_COMPANY_BASE_DOMAIN;
				}

				$strURL .= '/?module=psi_invoice_detailxxx&action=view_invoice_details';
				$strURL .= '&client[id]=' . $this->getCid();
				$strURL .= '&invoice[id]=' . $this->getId();
				if( $boolNewInvoice == true ) {
					$strURL .= '&transaction[id]=' . $arrmixTransaction['line_item_id'];
				} else {
					$strURL .= '&transaction[id]=' . $arrmixTransaction['id'];
				}
				$strURL .= '';
			} elseif( $arrmixTransaction['charge_code_id'] == CChargeCode::RESIDENT_VERIFY_FEES ) {
				$strPropertyIdUrlArguments = '';
				// Check for an associated contract property id for this transactions.
				if( true == isset( $arrmixTransaction['property_id'] ) && '' != $arrmixTransaction['property_id'] ) {
					// Get the property id from the contract property

					$strPropertyIdUrlArguments = '%26report_filter[property_group_ids][]=' . $arrmixTransaction['property_id'];
				}

				// If we do not have a property id on the transaction let's check the Account Properties
				if( '' == $strPropertyIdUrlArguments ) {
					$intCount = 0;
					// Get property id(s) for this account.
					$objAccount = $this->getAccount();
					if( valObj( $objAccount, 'CAccount' ) ) {
						$arrobjAccountProperties = $objAccount->fetchAccountProperties( $objAdminDatabase );
						if( valArr( $arrobjAccountProperties ) ) {
							foreach( $arrobjAccountProperties as $objAccountProperty ) {
								$strPropertyIdUrlArguments .= '%26report_filter[property_ids][]=' . $objAccountProperty->getPropertyId();
								if( 20 < $intCount++ ) {
									break;
								} // We don't want to make the URL too long so limit this to 20 properties.
							}
						}
					}
				}

				$strURL = 'http://' . $this->m_objClient->getRwxDomain() . '.' . CONFIG_COMPANY_BASE_DOMAIN;

				if( 'stage' == CConfig::get( 'environment' ) ) {
					$strURL = 'https://' . $this->m_objClient->getRwxDomain() . '.' . basename( $_SERVER['HTTP_HOST'] );
				} elseif( 'production' == CConfig::get( 'environment' ) ) {
					$strURL = 'http://' . $this->m_objClient->getRwxDomain() . '.' . CONFIG_COMPANY_BASE_DOMAIN;
				}

				$strURL .= '/?module=leads_report_tabxxx&load_default_report_id=' . ( int ) CDefaultReport::SCREENING_BILLING_DETAILS;
				if( $boolNewInvoice == true ) {
					if( false == valId( $this->m_intReportInstanceId ) ) {
						$objClientDatabase = $this->m_objClient->loadDatabase( CDatabaseUserType::PS_PROPERTYMANAGER );
						if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {
							$this->m_intReportInstanceId = \Psi\Eos\Entrata\CReportInstances::createService()->fetchReportInstanceByCidByDefaultReportIdByName( $this->m_objClient->getId(), CDefaultReport::SCREENING_BILLING_DETAILS, CReport::SCREENING_BILLING_DETAILS_REPORT, $objClientDatabase );
							$objClientDatabase->close();
						}
					}

					$strURL .= '&load_large_dialog=%3Fmodule%3Dreportsxxx%26report_id=' . $this->m_intReportInstanceId . '%26name=screening_billing_details';
					$strURL .= '%26report_filter[date]=' . $arrmixTransaction['invoice_date'] . '%26report_filter[period][period_type]=date';
					$strURL .= '%26report_filter[period][date]=' . date_format( new DateTime( $arrmixTransaction['date'] ), 'm/d/y' );
				} else {
					$strURL .= '%26report_filter[billing_month]=' . $arrmixTransaction['transaction_datetime'];
				}
				$strURL .= '%26report_filter[screening_type_ids][]=1%26report_filter[screening_type_ids][]=2%26report_filter[screening_type_ids][]=3%26report_filter[screening_type_ids][]=4%26report_filter[screening_type_ids][]=5%26report_filter[screening_type_ids][]=6%26report_filter[screening_type_ids][]=7%26report_filter[screening_type_ids][]=8';
				$strURL .= $strPropertyIdUrlArguments;

			}
		}

		return $strURL;
	}

	// calculate and process sales tax.

	public function calculateAndProcessInvoiceSalesTax( $objAdminDatabase, $intUserId, $boolOnlyCalculate = false, $boolForcefullyCalculate = false, $boolBackTax = false, $boolClientAdminOnly = false, $boolIsPropertySpecificTax = false ) {

		$objSalesTaxLibrary = CSalesTaxLibrary::createLibrary( $this );
		if( true == valObj( $objSalesTaxLibrary, 'CSalesTaxLibrary' ) ) {
			$objResponse = $objSalesTaxLibrary->calculateAndProcessInvoiceSalesTax( $this, $objAdminDatabase, $intUserId, $boolOnlyCalculate, $boolForcefullyCalculate, $boolBackTax, $boolClientAdminOnly, $boolIsPropertySpecificTax );
			if( false == $objResponse ) {
				$this->addErrorMsgs( $objSalesTaxLibrary->getErrorMsgs() );
				return false;
			}
			return $objResponse;
		}

		return false;

	}

	// Recalculate sales tax.
	// 1.Recalculate only if sum of all related sales_tax_transactions is equal to zero.
	// 2.call CalculateRequest and parse the return results from the calculate request results and record them in the sales_tax_transactions table with the is_adjustment flag set to 1
	// 3.a new transaction should be posted to the account using the SALES_TAX_ADJUSTMENT charge code

	public function recalculateAndProcessInvoiceSalesTax( $intUserId, $objAdminDatabase ) {

		$objSalesTaxLibrary = CSalesTaxLibrary::createLibrary( $this );
		if( true == valObj( $objSalesTaxLibrary, 'CSalesTaxLibrary' ) ) {
			$objResponse = $objSalesTaxLibrary->recalculateAndProcessInvoiceSalesTax( $this, $objAdminDatabase, $intUserId );
			if( false == $objResponse ) {
				$this->addErrorMsgs( $objSalesTaxLibrary->getErrorMsgs() );
				return false;
			}
			return $objResponse;
		}

		return false;

	}

	public function applySalesTaxAmount( $objAdminDatabase, $boolIsReCalculate = false ) {

		if( true == is_null( $this->getId() ) ) {
			return false;
		}
		// updating invoice amount by its transactions' amount except payments...
		$strSubUpdateStatusSql = ( false == $boolIsReCalculate ) ? ',invoice_status_type_id = ' . CInvoiceStatusType::PENDING_REVIEW : '';

		$strSql = ' UPDATE invoices
					SET invoice_amount = (
					    SELECT sum( transaction_amount ) as total_amount
					    FROM transactions t
					    WHERE
					        t.invoice_id = ' . ( int ) $this->getId() . '
					        AND t.charge_code_id <> ' . CChargeCode::PAYMENT_RECEIVED . '
					) ' . $strSubUpdateStatusSql . ' 
                    WHERE id = ' . ( int ) $this->getId() . ';';

		$objAdminDatabase->execute( $strSql );
		return true;
	}

	public function loadTransactions( $objAdminDatabase ) {

		$arrobjTransactions = $this->getTransactions();

		if( false == valArr( $this->getTransactions() ) ) {
			$arrobjTransactions = \Psi\Eos\Admin\CTransactions::createService()->fetchTransactionsByInvoiceId( $this->getId(), $objAdminDatabase, $boolIsExcludeSalesTax = true );
			$this->setTransactions( $arrobjTransactions );
		}

		return $arrobjTransactions;
	}

	public function loadSalesTaxTransactions( $objAdminDatabase ) {

		$arrobjSalesTaxtransactions = CSalesTaxTransactions::createService()->fetchSalesTaxTransactionsByInvoiceIdByTransactionIds( $this->getId(), array_filter( array_keys( $this->getTransactions() ) ), $objAdminDatabase );

		return $arrobjSalesTaxtransactions;
	}

	public function loadSalesTaxAccountPropertyAddress( $objAdminDatabase, $boolIsPropertySpecificTax = false ) {

		$arrobjSalesTaxPropertyAddresses = [];

		$arrintPropertyIds = array_filter( array_keys( rekeyObjects( 'PropertyId', $this->getTransactions() ) ), 'is_numeric' );

		if( true == valArr( $arrintPropertyIds ) ) {
			$arrobjPropertyAddresses = \Psi\Eos\Admin\CPropertyAddresses::createService()->fetchPropertyAddressesByPropertyIdsByTransactionIds( $arrintPropertyIds, array_keys( $this->getTransactions() ), $objAdminDatabase, $boolIsPropertySpecificTax );

			if( true == $arrobjPropertyAddresses ) {
				foreach( $arrobjPropertyAddresses as $objPropertyAddress ) {

					// If property address is valid then add it.
					if( false == $boolIsPropertySpecificTax ) {
							$arrobjSalesTaxPropertyAddresses[$objPropertyAddress->getTransactionId()] = $objPropertyAddress;
					} else {
						$arrobjSalesTaxPropertyAddresses[$objPropertyAddress->getPropertyId()] = $objPropertyAddress;
					}
				}
			}
		}

		$objAccount = $this->getOrFetchAccount( $objAdminDatabase );

		// If property addresses are Invalid and account address also invalid then return false.
		if( false == valArr( $arrobjSalesTaxPropertyAddresses ) && true == valObj( $objAccount, 'CAccount' ) && CSalesTaxResultType::FAILED_ALL_VALIDATIONS == $objAccount->getSalesTaxResultTypeId() ) {
			return false;
		}

		return $arrobjSalesTaxPropertyAddresses;
	}

	public function fetchAllocations( $objAdminDatabase, $boolExcludePayments = false ) {
		return \Psi\Eos\Admin\CAllocations::createService()->fetchAllocationsByInvoiceId( $this->getId(), $objAdminDatabase, $boolExcludePayments );
	}

	public function fetchCompanyPayments( $objAdminDatabase ) {
		return CCompanyPayments::createService()->fetchCompanyPaymentsByInvoiceId( $this->getId(), $objAdminDatabase );
	}

	public function getUtilityReportLink( $intPropertyId, $strPostMonth ) {

		$strURL = 'http://' . $this->getClient()->getRwxDomain() . '.' . CONFIG_COMPANY_BASE_DOMAIN;

		return $strURL . '/?module=utilities_report_tabxxx&load_large_dialog=' . urlencode( '?module=reportsxxx&name=resident_utility_billing_details&report_id=&drilldown=1&report_filter[property_group_ids][]=' . ( int ) $intPropertyId . '&report_filter[utility_batch_due_date][]=' . $strPostMonth );
	}

	public function getCalculatedInvoiceAmount( $objAdminDatabase ) {

		$strSql = '
				SELECT
				  	SUM( t.transaction_amount )
				FROM
					transactions t
				WHERE
					t.invoice_id = ' . $this->getId() . '
					AND t.charge_code_id <> ' . CChargeCode::PAYMENT_RECEIVED . ';';

		$arrstrData = fetchData( $strSql, $objAdminDatabase );

		return $arrstrData[0]['sum'];
	}

	public function setPendingReviewStatusForInvoice( $objAdminDatabase ) {
		$strSql = 'UPDATE invoices
						SET invoice_amount = ( SELECT sum( transaction_amount ) as total_amount
													FROM transactions t WHERE t.invoice_id = ' . ( int ) $this->getId() . ' AND t.charge_code_id <> ' . CChargeCode::PAYMENT_RECEIVED . ' ),
							invoice_status_type_id = ' . CInvoiceStatusType::PENDING_REVIEW . '
													 WHERE id = ' . ( int ) $this->getId() . ';';
		$objAdminDatabase->execute( $strSql );
	}

	/**
	 * @param $intInvoiceNumber
	 * @return int
	 */
	public static function getInvoiceIdFromInvoiceNumber( $intInvoiceNumber ) {
		if( self::MIN_CHECK_DIGIT_REQUIRED < $intInvoiceNumber ) {
			// remove last two check digits
			$intInvoiceId = ( int ) \Psi\CStringService::singleton()->substr( $intInvoiceNumber, 0, -2 );

			if( self::MIN_CHECK_DIGIT_REQUIRED > $intInvoiceId ) {
				// trigger_error( 'Invalid invoice number', E_USER_WARNING );
				$intInvoiceId = NULL;
			}
			return $intInvoiceId;
		}

		return $intInvoiceNumber;
	}

	public static function calculateInvoiceNumber( $intInvoiceId, $intAccountId ) {
		$boolIsValid = true;

		if( false == valId( $intInvoiceId ) ) {
			trigger_error( 'Invoice ID is required to calculate the invoice number.', E_USER_WARNING );
			$boolIsValid = false;
		}

		if( false == valId( $intAccountId ) ) {
			trigger_error( 'Account ID is required to calculate the invoice number.', E_USER_WARNING );
			$boolIsValid = false;
		}

		if( false == $boolIsValid ) {
			return NULL;
		}

		if( self::MIN_CHECK_DIGIT_REQUIRED < $intInvoiceId ) {

			$intInvoiceIdCheckDigit = CNumbers::createService()->calculateLuhn( $intInvoiceId );
			$intAccountIdCheckDigit = CNumbers::createService()->calculateLuhn( $intAccountId );

			$intInvoiceNumber = $intInvoiceId . $intInvoiceIdCheckDigit . $intAccountIdCheckDigit;

		} else {
			$intInvoiceNumber = $intInvoiceId;
		}

		return $intInvoiceNumber;
	}

	public function createDefaultOrderFormRequestInvoice( $intAccountId, $fltAmount, $intClientId, $objAdminDatabase ) {
		$this->setId( $this->fetchNextId( $objAdminDatabase ) );
		$this->setAccountId( $intAccountId );
		$this->setCid( $intClientId );
		$this->setInvoiceStatusTypeId( CInvoiceStatusType::PENDING_REVIEW );
		$this->setInvoiceNumber( $this->generateInvoiceNumber() );
		$this->setInvoiceDate( date( 'm/d/Y' ) );
		$this->setInvoiceDueDate( date( 'm/d/Y', strtotime( '30 days' ) ) );
		$this->setInvoiceAmount( $fltAmount );
		$this->setNewChargesTotal( $fltAmount );
	}

	public function checkSalesTaxEligibility( $objDatabase, $intCurrentUserId ) {

		$strAccountCountryCode = $this->getOrFetchAccount( $objDatabase )->getBilltoCountryCode();
		/** @var CTransaction[] $arrobjTransactions */
		$arrobjTransactions = $this->getTransactions();

		// check that transactions have country codes that match the invoice currency
		if( true == valArr( $arrobjTransactions ) ) {
			$arrobjCountries = \Psi\Eos\Admin\CCountries::createService()->fetchCountriesByCurrencyCode( $this->m_strCurrencyCode, $objDatabase );
			$arrstrValidCountryCodes = extractUniqueFieldValuesFromObjects( 'getCode', $arrobjCountries );
			$arrobjProperties = \Psi\Eos\Admin\CProperties::createService()->fetchPropertiesByPropertyIdsByCid( extractUniqueFieldValuesFromObjects( 'getPropertyId', $arrobjTransactions ), $this->getCid(), $objDatabase );
			foreach( $arrobjTransactions as $objTransaction ) {
				if( true == isset( $arrobjProperties[$objTransaction->getPropertyId()] ) ) {
					$arrstrCountries[] = $arrobjProperties[$objTransaction->getPropertyId()]->getCountryCode();
				} else {
					$arrstrCountries[] = $strAccountCountryCode;
				}
			}

			// If none of the transactions have countries that match the invoice currency, we are done
			if( false == array_intersect( $arrstrValidCountryCodes, $arrstrCountries ) ) {
				return false;
			}
		}

		// check invoice against sales tax currency/country table
		$strInvoiceCurrencyCode = $this->getCurrencyCode();
		$objSalesTaxSettings = \Psi\Eos\Admin\CSalesTaxSettings::createService()->fetchSalesTaxSettingByCountryCodeByCurrencyCode( $strAccountCountryCode, $strInvoiceCurrencyCode, $objDatabase );
		if( false == valObj( $objSalesTaxSettings, 'CSalesTaxSetting' ) ) {
			// no settings means it passes
			return true;
		}

		if( true == $objSalesTaxSettings->getIsBlockedSalesTax() ) {
			return false;
		}

		if( true == $objSalesTaxSettings->getIsNonBilledSalesTax() ) {
			$this->setIsNonBilledSalesTax( true );

			// if the invoice has been created, update the settings
			if( true == valId( $this->getId() ) ) {
				$this->update( $intCurrentUserId, $objDatabase );
			}
		}

		return true;
	}

}
?>
