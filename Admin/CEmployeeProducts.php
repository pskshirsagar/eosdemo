<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeProducts
 * Do not add any new functions to this class.
 */

class CEmployeeProducts extends CBaseEmployeeProducts {

	public static function fetchEmployeeProductsByEmployeeIdByProductIds( $intEmployeeId, $arrintProductIds, $objDatabase ) {

		if( false == valArr( $arrintProductIds ) ) return NULL;

		$strSql = 'SELECT * FROM employee_products where employee_id =' . ( int ) $intEmployeeId . ' AND ps_product_id IN ( ' . implode( ',', $arrintProductIds ) . ' ) ORDER BY created_on DESC';

		return self::fetchEmployeeProducts( $strSql, $objDatabase );
	}
}
?>