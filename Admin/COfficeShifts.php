<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COfficeShifts
 * Do not add any new functions to this class.
 */

class COfficeShifts extends CBaseOfficeShifts {

	public static function fetchAllOfficeShifts( $objDatabase ) {

		$strSql = ' SELECT * FROM office_shifts ORDER BY id';

		return parent::fetchOfficeShifts( $strSql, $objDatabase );
	}

	public static function fetchDefaultOfficeShift( $objDatabase ) {

		$strSql = 'SELECT * FROM office_shifts WHERE default_shift = 1';

		return parent::fetchOfficeShift( $strSql, $objDatabase );
	}

	public static function fetchAllWeekendOfficeShifts( $objDatabase ) {

		$strSql = 'SELECT
						id,
						name,
						office_start_time,
						CASE WHEN id IN ( ' . implode( ',', COfficeShift::$c_arrintBufferTimeOfficeShifts ) . ' )
							THEN office_start_time + interval \'' . COfficeShift::SHIFT_BUFFER_HOURS . ' hour\'
							ELSE office_start_time
						END AS office_start_time_label,
						required_hours
					FROM
						office_shifts
					WHERE
						default_shift <> 1
						AND is_support IS TRUE
						AND deleted_by IS NULL
					ORDER BY
						order_num';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchOfficeShiftsByIsSupport( $boolIsSupport, $objDatabase ) {

		$strWhereClause = ' WHERE is_support ' . ( ( false == $boolIsSupport ) ? 'IS FALSE ' : 'IS TRUE ' );

		$strSql = ' SELECT
						id,
						name,
						CASE WHEN id IN ( ' . implode( ',', COfficeShift::$c_arrintBufferTimeOfficeShifts ) . ' )
							THEN to_char( office_start_time + interval \'' . COfficeShift::SHIFT_BUFFER_HOURS . ' hour\', \'HH24:MI:SS\' )
							ELSE to_char( office_start_time, \'HH24:MI:SS\' )
						END AS office_start_time,
						CASE WHEN id IN ( ' . COfficeShift::MORNING_SHIFT . ', ' . COfficeShift::EVENING_SHIFT . ', ' . COfficeShift::NIGHT_SHIFT . ' )
								THEN ( office_start_time + interval \'1 hour\' * ( required_hours - ' . COfficeShift::SHIFT_BUFFER_HOURS . ' ) )::time
							WHEN id = ' . COfficeShift::GENERAL_SHIFT . ' 
								THEN ( office_start_time + interval \'1 hour\' * ( required_hours + ' . COfficeShift::SHIFT_BUFFER_HOURS . ' ) )::time
								ELSE ( office_start_time + interval \'1 hour\' * required_hours )::time
						END AS office_end_time,
						encrypted_shift_amount,
						is_show_in_payroll,
						details
					FROM
						office_shifts
					' . $strWhereClause . '
					ORDER BY
						id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchOfficeShiftsByTeamIds( $arrintTeamIds, $objDatabase ) {

		if( true == valArr( $arrintTeamIds ) ) {
			$strWhereCondition = ' OR osa.team_id IN (' . implode( ',', $arrintTeamIds ) . ') ';
		}

		$strSql = ' SELECT
						DISTINCT os.id,
						os.name,
						to_char( os.office_start_time, \'HH24:MI:SS\' ) as office_start_time,
						CASE WHEN os.id IN ( ' . implode( ',', COfficeShift::$c_arrintBufferTimeOfficeShifts ) . ' )
							THEN to_char( os.office_start_time + interval \'' . COfficeShift::SHIFT_BUFFER_HOURS . ' hour\', \'HH24:MI:SS\' )
							ELSE to_char( os.office_start_time, \'HH24:MI:SS\' )
						END AS office_start_time_label,
						CASE WHEN os.id IN ( ' . COfficeShift::MORNING_SHIFT . ', ' . COfficeShift::EVENING_SHIFT . ', ' . COfficeShift::NIGHT_SHIFT . ' )
								THEN ( os.office_start_time + interval \'1 hour\' * ( os.required_hours - ' . COfficeShift::SHIFT_BUFFER_HOURS . ' ) )::time
							WHEN os.id = ' . COfficeShift::GENERAL_SHIFT . ' 
								THEN ( os.office_start_time + interval \'1 hour\' * ( os.required_hours + ' . COfficeShift::SHIFT_BUFFER_HOURS . ' ) )::time
								ELSE ( os.office_start_time + interval \'1 hour\' * os.required_hours )::time
						END AS office_end_time,
						os.deleted_by
					FROM
						office_shifts os
						LEFT JOIN office_shift_associations osa ON ( os.id = osa.office_shift_id )
					WHERE
						os.default_shift = 1
						AND osa.deleted_by IS NULL
						' . $strWhereCondition . '
						AND os.is_support = false
					ORDER BY
						os.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllOfficeShiftsById( $arrintOfficeShifts, $objDatabase ) {
		$strSql = 'SELECT
						id,
						name,
						office_start_time,
						required_hours
					FROM
						office_shifts
					WHERE
						default_shift <> 1
						AND deleted_by IS NULL
						And id IN (' . implode( ',', $arrintOfficeShifts ) . ' )
					ORDER BY
						order_num';

		return parent::fetchOfficeShifts( $strSql, $objDatabase );
	}

	public static function fetchAllOfficeShiftsAmount( $objDatabase ) {
		$strSql = 'SELECT
						id,
						name,
						encrypted_shift_amount
					FROM
						office_shifts
					WHERE
						is_support IS false
						AND deleted_by IS NULL
						AND is_show_in_payroll IS true';

		return fetchData( $strSql, $objDatabase );
	}

}
?>