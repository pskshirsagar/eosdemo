<?php

class CEmployeePayAllocation extends CBaseEmployeePayAllocation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valManagerEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPendingIncreaseEncryptionAssociationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPendingHourlyEncryptionAssociationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubmittedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubmittedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>