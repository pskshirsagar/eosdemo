<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestTemplates
 * Do not add any new functions to this class.
 */

class CTestTemplates extends CBaseTestTemplates {

	public static function fetchTestTemplatesByIds( $arrintTestTemplateIds, $objDatabase ) {
		if( false == valArr( $arrintTestTemplateIds ) ) return NULL;
		return self::fetchTestTemplates( 'SELECT * FROM test_templates WHERE id IN ( ' . implode( ',', $arrintTestTemplateIds ) . ' ) ORDER BY updated_on ASC', $objDatabase );
	}

	public static function fetchQuickSearchTestTemplates( $arrstrFilteredExplodedSearch, $objDatabase, $boolAllowPhpCertificationTemplate = false ) {

		$strWhere = '';

		if( false == $boolAllowPhpCertificationTemplate )
			$strWhere = ' AND id NOT IN ( ' . implode( ',', CTestTemplate::$c_arrintPhpCertificationTemplates ) . ' ) ';

		$strSql = 'SELECT
						*
					FROM
						test_templates
					WHERE
						deleted_by IS NULL
						AND deleted_on IS NULL
					    ' . $strWhere . '
						AND name ILIKE \'%' . implode( '%\' AND name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						ORDER BY name LIMIT 10';

		return CTestTemplates::fetchTestTemplates( $strSql, $objDatabase );
	}

	public static function fetchAssociatedTestNameByTemplateId( $intTemplateId, $objDatabase ) {

		$strSql = 'SELECT
						t.name
					FROM
						test_templates tt
						LEFT JOIN test_question_template_associations tqta on tqta.test_template_id = tt.id
						LEFT JOIN tests t on t.id = tqta.test_id
					WHERE
						tt.id = ' . ( int ) $intTemplateId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountOrUndeletedPaginatedTestTemplates( $objDatabase, $boolIsCount = false, $intPageNo = NULL, $intPageSize = NULL, $boolAllowPhpCertificationTemplate = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;
		$strWhere = '';

		if( false == $boolAllowPhpCertificationTemplate )
			$strWhere = ' AND tt.id NOT IN ( ' . implode( ',', CTestTemplate::$c_arrintPhpCertificationTemplates ) . ' ) ';

		if( false == $boolIsCount )
			$strSql = 'SELECT tt.*, e.name_full AS created_by_name ';
		else
			$strSql = 'SELECT COUNT(tt.id) ';

		$strSql .= 'FROM
						test_templates tt
						LEFT JOIN users u ON ( tt.created_by = u.id )
						LEFT JOIN employees e ON ( e.id = u.employee_id )
					WHERE
						tt.deleted_on IS NULL ' . $strWhere . '';

		if( false == $boolIsCount ) {
			$strSql .= 'ORDER BY
							id DESC
							OFFSET ' . ( int ) $intOffset . 'LIMIT ' . $intLimit;

			return fetchData( $strSql, $objDatabase );
		} else {
			$arrintResponse = fetchData( $strSql, $objDatabase );

			if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
			return 0;
		}
	}

	public static function fetchDescendingTestTemplatesByTestLevelTypeId( $intTestLevelTypeId, $objDatabase ) {
		return self::fetchTestTemplates( sprintf( 'SELECT * FROM test_templates WHERE test_level_type_id <= %d ORDER BY name asc', ( int ) $intTestLevelTypeId ), $objDatabase );
	}

}
?>