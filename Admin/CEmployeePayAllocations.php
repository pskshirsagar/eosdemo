<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePayAllocations
 * Do not add any new functions to this class.
 */

class CEmployeePayAllocations extends CBaseEmployeePayAllocations {

	public static function fetchEmployeePayAllocationByEmployeeIdByPayReviewYear( $intEmployeeId, $strPayReviewYear, $objDatabase ) {
		if( false == is_numeric( $intEmployeeId ) || false == valStr( $strPayReviewYear ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM employee_pay_allocations WHERE employee_id = ' . ( int ) $intEmployeeId . ' AND EXTRACT( YEAR FROM pay_review_year ) = ' . $strPayReviewYear;
		return self::fetchEmployeePayAllocation( $strSql, $objDatabase );
	}

	public static function fetchEmployeePayAllocationsByEmployeeIdsByPayReviewYear( $arrintEmployeeIds, $strPayReviewYear, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) || false == valStr( $strPayReviewYear ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM employee_pay_allocations WHERE employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND EXTRACT( YEAR FROM pay_review_year ) = ' . $strPayReviewYear;
		return parent::fetchEmployeePayAllocations( $strSql, $objDatabase );
	}

	public static function fetchEmployeePayAllocationsByManagerEmployeeIdsByPayReviewYear( $arrintManagerEmployeeIds, $strPayReviewYear, $objDatabase ) {
		if( false == valArr( $arrintManagerEmployeeIds ) || false == valStr( $strPayReviewYear ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						id,
						approved_by,
						approved_on,
						rejected_by,
						rejected_on,
						submitted_by,
						submitted_on,
						manager_employee_id,
						details
					FROM 
						employee_pay_allocations
					WHERE 
						manager_employee_id IN ( ' . sqlIntImplode( $arrintManagerEmployeeIds ) . ' )
						AND EXTRACT( YEAR FROM pay_review_year ) = ' . $strPayReviewYear;
		return self::fetchEmployeePayAllocations( $strSql, $objDatabase );
	}

	public static function fetchEmployeePayAllocationsByEmployeeCompensationLogIds( $arrintEmployeeCompensationLogIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeCompensationLogIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT employee_id, employee_compensation_log_id, pay_review_year FROM employee_pay_allocations WHERE employee_compensation_log_id IN ( ' . implode( ',', $arrintEmployeeCompensationLogIds ) . ' )';
		return fetchData( $strSql, $objDatabase );
	}

}
?>