<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupRepositoryPermissions
 * Do not add any new functions to this class.
 */

class CGroupRepositoryPermissions extends CBaseGroupRepositoryPermissions {

	public static function fetchGroupRepositoryPermissionsByUserId( $intUserId, $objDatabase ) {

		$strSql = 'SELECT
						grp.repository_id,
						max ( grp.is_read ) AS is_read,
						max ( grp.is_write ) AS is_write
					FROM
						group_repository_permissions grp
						JOIN groups g ON ( g.id = grp.group_id AND g.deleted_by IS NULL )
						JOIN user_groups ug ON ( ug.group_id = grp.group_id AND ug.user_id = ' . ( int ) $intUserId . ' AND ug.deleted_by IS NULL )
					GROUP BY
						grp.repository_id';

		return self::fetchGroupRepositoryPermissions( $strSql, $objDatabase );
	}

	public static function fetchAllGroupRepositoryPermissions( $objDatabase ) {

		$strSql = 'SELECT
						grp.repository_id,
						g.name,
						g.id as group_id,
						CASE
							WHEN 1 = grp.is_read AND 1 = grp.is_write THEN \'rw\'
							WHEN 1 = grp.is_read THEN \'r\'
						END AS grants
					FROM
						group_repository_permissions grp
						JOIN groups g ON g.id = grp.group_id
					ORDER BY
						grp.repository_id,
						g.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssignedGroupsByRepositoryId( $intRepositoryId, $objDatabase ) {

		$strSql = 'SELECT
						grp.repository_id,
						g.id as group_id,
						g.name,
						grp.is_read,
						grp.is_write
					FROM
						groups g
						JOIN group_repository_permissions grp ON( g.id = grp.group_id )
					WHERE
						grp.repository_id = ' . ( int ) $intRepositoryId . '
						AND g.group_type_id = ' . CGroupType::SVN . '
					ORDER BY
						g.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSearchUnAssignedGroupsByRepositoryId( $intRepositoryId, $arrstrExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						g.id as group_id,
						g.name
					FROM
						groups g
					WHERE
						g.id NOT IN (
									SELECT
										grp.group_id
									FROM
										group_repository_permissions grp
									WHERE
										grp.repository_id = ' . ( int ) $intRepositoryId . '
										AND grp.group_id = g.id
									)
						AND g.group_type_id = ' . CGroupType::SVN . '
						AND g.name ILIKE \'%' . implode( '%\' AND g.name ILIKE \'%', $arrstrExplodedSearch ) . '%\'
					ORDER BY
						g.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGroupRepositoryPermissionsByRepositoryIdByGroupIds( $intRepositoryId, $arrintGroupIds, $objDatabase ) {

		if( false == valArr( $arrintGroupIds ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						group_repository_permissions
					WHERE
						repository_id = ' . ( int ) $intRepositoryId . '
						AND group_id IN ( ' . implode( ',', $arrintGroupIds ) . ')';

		return self::fetchGroupRepositoryPermissions( $strSql, $objDatabase );
	}

	public static function fetchGroupRepositoryPermissionsByRepositoryIdByUserIds( $arrintUserIds, $intRepositoryId, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) ) return false;

		$strSql = 'SELECT
						ug.user_id,
						max ( grp.is_read ) AS is_read,
						max ( grp.is_write ) AS is_write
					FROM
						group_repository_permissions grp
						JOIN user_groups ug ON ( ug.group_id = grp.group_id AND ug.deleted_by IS NULL )
					WHERE
						grp.repository_id = ' . ( int ) $intRepositoryId . '
						AND ug.user_id IN ( ' . implode( ',', $arrintUserIds ) . ')
					GROUP BY
						ug.user_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRepositoryIdsUserId( $intUserId, $objDatabase ) {
		$strSql = 'SELECT
						grp.repository_id,
						grp.is_read,
						grp.is_write
					FROM
						group_repository_permissions grp
					WHERE
						grp.group_id IN(
							SELECT
								group_id
							FROM
								user_groups
							WHERE
								user_id = ' . ( int ) $intUserId . '
								AND deleted_by IS NULL
						)ORDER BY grp.repository_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRepositoryIdsByGroupIds( $arrintGroupIds, $objDatabase ) {
		if( false == valArr( $arrintGroupIds ) ) return NULL;

		$strSql = 'SELECT
						id,
						repository_id,
						is_read,
						is_write
					FROM
						group_repository_permissions
					WHERE
						group_id IN ( ' . implode( ',', $arrintGroupIds ) . ')';

		return fetchData( $strSql, $objDatabase );

	}

}
?>