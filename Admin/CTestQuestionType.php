<?php

class CTestQuestionType extends CBaseTestQuestionType {

	const OPEN_ANSWER 		 	= 1;
	const MULTIPLE_CHOICE 		= 2;
	const TRUE_OR_FALSE 		= 3;
	const MULTISELECT_ANSWERS 	= 4;
	const AUDIO_ANSWERS 		= 5;
	const CODING_TEST			= 6;

	/**
	 * If you are making any changes in loadSmartyConstants function then
	 * please make sure the same changes would be applied to loadTemplateConstants function also.
	 */

	public static function loadSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'TEST_QUESTION_TYPE_OPEN_ANSWER', 			self::OPEN_ANSWER );
		$objSmarty->assign( 'TEST_QUESTION_TYPE_MULTIPLE_CHOICE', 		self::MULTIPLE_CHOICE );
		$objSmarty->assign( 'TEST_QUESTION_TYPE_TRUE_OR_FALSE', 		self::TRUE_OR_FALSE );
		$objSmarty->assign( 'TEST_QUESTION_TYPE_MULTISELECT_ANSWERS', 	self::MULTISELECT_ANSWERS );
		$objSmarty->assign( 'TEST_QUESTION_TYPE_AUDIO_ANSWERS', 		self::AUDIO_ANSWERS );
		$objSmarty->assign( 'TEST_QUESTION_TYPE_CODING_TEST', 			self::CODING_TEST );
	}

	public static $c_arrintTemplateConstants = array(

		'TEST_QUESTION_TYPE_OPEN_ANSWER'			=> self::OPEN_ANSWER,
		'TEST_QUESTION_TYPE_MULTIPLE_CHOICE'		=> self::MULTIPLE_CHOICE,
		'TEST_QUESTION_TYPE_TRUE_OR_FALSE'			=> self::TRUE_OR_FALSE,
		'TEST_QUESTION_TYPE_MULTISELECT_ANSWERS'	=> self::MULTISELECT_ANSWERS,
		'TEST_QUESTION_TYPE_AUDIO_ANSWERS'			=> self::AUDIO_ANSWERS
	);

}
?>