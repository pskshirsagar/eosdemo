<?php

class CAdType extends CBaseAdType {

	const LIGHTBOX						= 1;
	const TOWER							= 2;
	const BANNER						= 3;
	const EMAIL_BANNER					= 4;
	const TEXT_AD						= 5;
	const PDF_FOOTER_AD					= 6;
	const LEASE_AGENT_DASHBOARD_TOWER	= 7;
	const RWX_LOGIN						= 8;

}
?>