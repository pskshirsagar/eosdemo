<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseRequestStatusTypes
 * Do not add any new functions to this class.
 */

class CPurchaseRequestStatusTypes extends CBasePurchaseRequestStatusTypes {

	public static function fetchPurchaseRequestStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPurchaseRequestStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPurchaseRequestStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPurchaseRequestStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>