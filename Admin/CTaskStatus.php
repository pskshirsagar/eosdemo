<?php

class CTaskStatus extends CBaseTaskStatus {

	const UNUSED						= 1;
	const ACKNOWLEDGED					= 2;
	const REASSIGNED					= 3;
	const IN_PROGRESS					= 4;
	const ON_HOLD						= 5;
	const COMPLETED						= 6;
	const CANCELLED						= 7;
	const UNDER_REVIEW					= 8;
	const READY_FOR_DEV					= 9;
	const DEV_IN_PROGRESS				= 10;
	const READY_FOR_QA					= 11;
	const QA_IN_PROGRESS				= 12;
	const READY_FOR_RELEASE				= 13;
	const AWAITING_CUSTOMER_RESPONSE	= 14;
	const COMMITTED						= 15;
	const RELEASED_ON_RAPID				= 16;
	const VERIFIED_ON_RAPID				= 17;
	const BACKLOG						= 18;
	const RELEASED_ON_STAGE				= 19;
	const VERIFIED_ON_STAGE				= 20;
	const MIGRATION_IN_PROGRESS			= 21;
	const MIGRATED						= 22;
	const RELEASED_ON_STANDARD			= 23;
	const VERIFIED_ON_STANDARD			= 24;
	const AWAITING_RESPONSE				= 25;
	const VERIFICATION_REQUIRED			= 26;
	const ASSIGNED						= 27;
	const FIXED							= 29;
	const CLOSED						= 30;
	const REOPEN						= 31;
	const REJECT						= 32;
	const DEFERRED						= 33;
	const RELEASED						= 34;
	const VERIFIED						= 35;
	const REQUESTED						= 36;
	const SUBMITTED_FOR_APPROVAL		= 37;
	const APPROVED						= 38;
	const DELIVERED						= 39;
	const PENDING_RELEASE				= 40;
	const REVIEW_READY					= 41;
	const REWORK						= 42;
	const NEEDS_MAINTENANCE				= 43;
	const PASS							= 44;
	const FAIL							= 45;
	const BLOCK							= 46;
	const AWAITING_USER_RESPONSE        = 47;
	const AWAITING_APPROVAL             = 48;
	const READY_TO_PUBLISH				= 49;

	public static $c_arrstrTaskStatusGroups = [
		'new' => [
			self::UNUSED,
			self::REASSIGNED
		],

		'pending' => [
			self::IN_PROGRESS,
			self::UNDER_REVIEW,
			self::READY_FOR_DEV,
			self::DEV_IN_PROGRESS,
			self::READY_FOR_QA,
			self::QA_IN_PROGRESS,
			self::READY_FOR_RELEASE,
			self::COMMITTED,
			self::READY_TO_PUBLISH,
			self::MIGRATION_IN_PROGRESS,
			self::MIGRATED,
			self::SUBMITTED_FOR_APPROVAL
		],

		'released' => [
			self::RELEASED_ON_RAPID,
			self::RELEASED_ON_STANDARD,
			self::VERIFIED_ON_RAPID,
			self::VERIFIED_ON_STANDARD,
			self::RELEASED,
			self::VERIFIED
		],

		'stage' => [
			self::RELEASED_ON_STAGE,
			self::VERIFIED_ON_STAGE
		],

		'waiting' => [ self::AWAITING_CUSTOMER_RESPONSE, self::AWAITING_RESPONSE, self::AWAITING_USER_RESPONSE, self::AWAITING_APPROVAL ],

		'backlog' => [ self::BACKLOG ],

		'in_progress' => [ self::ACKNOWLEDGED, self::ASSIGNED, self::REOPEN, self::FIXED ],

		'hold' => [ self::ON_HOLD, self::DEFERRED ],

		'completed' => [ self::COMPLETED, self::CANCELLED, self::CLOSED, self::REJECT ],

		'delivered' => [ self::DELIVERED ],

		'all_unsolved_tickets' => [ self::UNDER_REVIEW, self::IN_PROGRESS, self::UNUSED, self::REOPEN ],

		'recently_updated_tickets' => [ self::UNUSED, self::IN_PROGRESS, self::UNDER_REVIEW, self::CLOSED, self::REOPEN ],

		'unassigned_tickets' => [ self::UNUSED, self::REOPEN ],

		'assigned_to_me' => [ self::UNUSED, self::IN_PROGRESS, self::UNDER_REVIEW, self::CLOSED, self::REOPEN ],

		'pending_tickets' => [ self::IN_PROGRESS, self::UNDER_REVIEW ],

		'recently_closed_tickets' => [ self::CLOSED ],

		'cab_support' => [ self::UNUSED, self::IN_PROGRESS, self::UNDER_REVIEW, self::CLOSED, self::REOPEN ],

		'actionable' => [
			self::UNUSED,
			self::ACKNOWLEDGED,
			self::UNDER_REVIEW,
			self::BACKLOG,
			self::AWAITING_RESPONSE
		],

		'team' => [
			self::UNUSED,
			self::ACKNOWLEDGED,
			self::UNDER_REVIEW,
			self::BACKLOG,
			self::AWAITING_RESPONSE,
			self::AWAITING_CUSTOMER_RESPONSE,
			self::ON_HOLD
		],

		'team_actionable' => [
			self::UNUSED,
			self::ACKNOWLEDGED,
			self::UNDER_REVIEW,
			self::BACKLOG,
			self::AWAITING_RESPONSE,
			self::AWAITING_CUSTOMER_RESPONSE,
			self::ON_HOLD
		],

		'self' => [
			self::UNUSED,
			self::ACKNOWLEDGED,
			self::UNDER_REVIEW,
			self::BACKLOG,
			self::AWAITING_RESPONSE,
			self::AWAITING_CUSTOMER_RESPONSE,
			self::ON_HOLD
		],

		'awt_client' => [
			self::AWAITING_CUSTOMER_RESPONSE
		],

		'support_queue' => [
			self::UNUSED,
			self::ACKNOWLEDGED,
			self::UNDER_REVIEW,
			self::BACKLOG,
			self::AWAITING_RESPONSE,
			self::AWAITING_CUSTOMER_RESPONSE,
			self::ON_HOLD
		],

		'rp_feedback_queue' => [
			self::UNUSED,
			self::ACKNOWLEDGED,
			self::UNDER_REVIEW,
			self::BACKLOG,
			self::AWAITING_RESPONSE,
			self::ON_HOLD
		],

		'Approved' => [
			self::APPROVED
		],
		'pending_release' => [
			self::PENDING_RELEASE
		]
	];

	public static $c_arrintTaskStatusesForQAApproval 	= [
		self::UNUSED,
		self::READY_FOR_DEV,
		self::DEV_IN_PROGRESS,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::READY_FOR_RELEASE,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::VERIFIED_ON_RAPID,
		self::VERIFIED_ON_STANDARD,
		self::COMMITTED,
		self::COMPLETED,
		self::RELEASED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::RELEASED,
		self::VERIFIED
	];

	public static $c_arrintTaskStatusesForPlannedTask 	= [
		self::COMPLETED,
		self::COMMITTED,
		self::CANCELLED,
		self::READY_FOR_RELEASE,
		self::RELEASED,
		self::RELEASED_ON_STANDARD,
		self::RELEASED_ON_RAPID,
		self::RELEASED_ON_STAGE,
		self::VERIFIED,
		self::VERIFIED_ON_RAPID,
		self::VERIFIED_ON_STAGE,
		self::VERIFIED_ON_STANDARD,
		self::READY_TO_PUBLISH
	];

	public static $c_arrintTaskStatusesForSprint 	= [
		self::ON_HOLD				=> 'On Hold',
		self::AWAITING_RESPONSE		=> 'Awaiting Response',
		self::UNDER_REVIEW			=> 'Under Review',
		self::UNUSED				=> 'New',
		self::READY_FOR_DEV			=> 'Dev Ready',
		self::DEV_IN_PROGRESS		=> 'Dev In Progress',
		self::READY_FOR_QA			=> 'Qa Ready',
		self::QA_IN_PROGRESS		=> 'Qa In Progress',
		self::READY_FOR_RELEASE		=> 'Release Ready',
		self::COMMITTED				=> 'Committed',
		self::RELEASED_ON_STAGE		=> 'Released on Stage',
		self::VERIFIED_ON_STAGE		=> 'Verified on Stage',
		self::RELEASED_ON_RAPID		=> 'Released on Rapid',
		self::VERIFIED_ON_RAPID		=> 'Verified on Rapid',
		self::RELEASED_ON_STANDARD	=> 'Released on Standard',
		self::VERIFIED_ON_STANDARD	=> 'Verified on standard',
		self::COMPLETED				=> 'Completed',
		self::CANCELLED				=> 'Cancelled',
		self::APPROVED				=> 'Approved',
		self::READY_TO_PUBLISH		=> 'Ready To Publish'
	];

	public static $c_arrintTaskStatusesNotRequiredForMobileRelease 	= [
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD
	];

	public static $c_arrintMobileTaskStatuses 	= [
		self::RELEASED,
		self::VERIFIED
	];

	public static $c_arrintTaskStatusesForQAApprovalRequired 	= [
		self::QA_IN_PROGRESS,
		self::COMMITTED,
		self::READY_FOR_RELEASE,
		self::VERIFIED_ON_STAGE,
		self::VERIFIED_ON_RAPID,
		self::VERIFIED_ON_STANDARD,
		self::COMPLETED,
		self::RELEASED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::RELEASED,
		self::VERIFIED
	];

	public static $c_arrintCompletedTaskStatuses 		= [
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_RAPID,
		self::VERIFIED_ON_STANDARD,
		self::COMPLETED
	];



	public static $c_arrintInCompletedTaskStatuses = [
		self::IN_PROGRESS,
		self::READY_FOR_DEV,
		self::DEV_IN_PROGRESS,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::READY_FOR_RELEASE,
		self::COMMITTED
	];

	public static $c_arrintSeleniumScriptTaskStatusIds = [
		self::UNUSED,
		self::DEV_IN_PROGRESS,
		self::QA_IN_PROGRESS,
		self::READY_FOR_QA,
		self::COMMITTED,
		self::COMPLETED,
		self::CANCELLED,
		self::ON_HOLD,
		self::VERIFIED_ON_RAPID,
		self::VERIFIED_ON_STANDARD,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::READY_FOR_RELEASE,
		self::RELEASED_ON_RAPID,
		self::RELEASED_ON_STANDARD
	];

	public static $c_arrintCodeReviewedTaskStatuses	= [
		self::COMMITTED,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::RELEASED_ON_STAGE,
		self::READY_FOR_RELEASE,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD
	];

	public static $c_arrintTaskStandardReleaseStatuses	= [
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::READY_FOR_RELEASE,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD,
		self::COMPLETED,
		self::CANCELLED
	];

	public static $c_arrintTaskStatusesForNonQAApproval 	= [
		self::UNUSED,
		self::READY_FOR_DEV,
		self::DEV_IN_PROGRESS,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::RELEASED_ON_STAGE,
		self::RELEASED_ON_RAPID
	];

	public static $c_arrintTaskStatusesForOpenBugsCount = [
		self::COMPLETED,
		self::CANCELLED,
		self::COMMITTED,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD,
		self::RELEASED,
		self::VERIFIED,
		self::APPROVED
	];

	public static $c_arrintTaskStatusesForSprintSummaryReportTasks = [
		self::COMPLETED,
		self::CANCELLED,
		self::REASSIGNED,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD,
		self::RELEASED,
		self::VERIFIED
	];

	public static $c_arrintTaskStatusesForReleaseNotes = [
		self::UNUSED,
		self::ACKNOWLEDGED,
		self::REASSIGNED,
		self::IN_PROGRESS,
		self::ON_HOLD,
		self::COMPLETED,
		self::UNDER_REVIEW,
		self::READY_FOR_DEV,
		self::DEV_IN_PROGRESS,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::READY_FOR_RELEASE,
		self::AWAITING_CUSTOMER_RESPONSE,
		self::COMMITTED,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::BACKLOG,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::MIGRATION_IN_PROGRESS,
		self::MIGRATED,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD,
		self::AWAITING_RESPONSE,
		self::VERIFICATION_REQUIRED
	];

	public static $c_arrintTaskStatusesForOpenBugCount = [
		self::UNUSED,
		self::ON_HOLD,
		self::UNDER_REVIEW,
		self::READY_FOR_DEV,
		self::DEV_IN_PROGRESS,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::READY_FOR_RELEASE,
		self::BACKLOG,
		self::AWAITING_RESPONSE,
		self::VERIFICATION_REQUIRED
	];

	public static $c_arrintAcknowledgedTaskIds	= [
		self::ACKNOWLEDGED,
		self::UNDER_REVIEW,
		self::AWAITING_RESPONSE
	];

	public static $c_arrintExcludeTaskStatusIds	= [
		self::COMPLETED,
		self::CANCELLED,
		self::AWAITING_CUSTOMER_RESPONSE
	];

	public static $c_arrintBlockedTaskStatusesForNonDefectType	= [
		self::ASSIGNED,
		self::FIXED,
		self::CLOSED,
		self::REOPEN,
		self::REJECT,
		self::DEFERRED,
	];

	public static $c_arrintSmsImmediateTaskStatus	= [
		self::COMPLETED,
		self::CANCELLED,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD,
		self::RELEASED,
		self::VERIFIED
	];

	public static $c_arrintSuspicionTaskStatus = [
		self::UNUSED,
		self::ASSIGNED,
		self::IN_PROGRESS,
		self::COMPLETED
	];

	public static $c_arrintInboxTabTaskStatuses  = [
		self::UNUSED,
		self::IN_PROGRESS,
		self::UNDER_REVIEW
	];

	public static $c_arrintTaskStatusesForCancelDefect = [
		self::CANCELLED,
		self::COMPLETED
	];

	public static $c_arrintTaskStatusesForDefectReport = [
		self::UNUSED,
		self::ASSIGNED,
		self::REOPEN,
		self::DEFERRED,
		self::READY_FOR_DEV,
		self::DEV_IN_PROGRESS,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::READY_FOR_RELEASE,
		self::COMMITTED,
		self::RELEASED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_STAGE,
		self::VERIFIED_ON_RAPID,
		self::RELEASED,
		self::VERIFIED
	];

	public static $c_arrintTaskStatusesForPastUpdateTab = [
		self::COMPLETED,
		self::RELEASED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_RAPID,
		self::VERIFIED_ON_STANDARD,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_STAGE,
	];

	public static $c_arrintTaskStatusesForComingSoonReleaseNotes = [
		self::READY_FOR_DEV,
		self::DEV_IN_PROGRESS,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::READY_FOR_RELEASE,
		self::COMMITTED,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE
	];

	public static $c_arrintTaskStatusesForUnderConsiderationReleaseNotes = [
		self::UNUSED,
		self::ACKNOWLEDGED,
		self::UNDER_REVIEW,
		self::AWAITING_CUSTOMER_RESPONSE,
		self::AWAITING_RESPONSE,
		self::READY_FOR_DEV,
		self::DEV_IN_PROGRESS,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::READY_FOR_RELEASE,
		self::COMMITTED,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::IN_PROGRESS,
		self::MIGRATION_IN_PROGRESS,
		self::MIGRATED,
		self::REASSIGNED
	];

	public static $c_arrintTaskStatusesForPastUpdatesReleaseNotes = [
		self::RELEASED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_RAPID,
		self::VERIFIED_ON_STANDARD,
		self::COMPLETED,
		self::AWAITING_CUSTOMER_RESPONSE,
		self::AWAITING_RESPONSE,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE
	];

	public static $c_arrintTaskStatusesForBugAllowanceReport = [
		self::COMMITTED,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD,
		self::COMPLETED,
		self::CANCELLED
	];

	public static $c_arrintDeveloperRestrictedDefectTaskStatuses = [
		self::REJECT,
		self::CANCELLED,
		self::CLOSED
	];

	public static $c_arrintProductBacklogUnrankedTaskStatuses = [
		self::UNUSED,
		self::UNDER_REVIEW,
		self::BACKLOG,
		self::ON_HOLD,
		self::AWAITING_RESPONSE,
		self::READY_FOR_DEV,
		self::DEV_IN_PROGRESS,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::READY_FOR_RELEASE,
		self::IN_PROGRESS,
		self::REQUESTED
	];

	public static $c_arrintRemoveRankedTaskStatuses = [
		self::COMMITTED,
		self::RELEASED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::CANCELLED,
		self::COMPLETED,
		self::VERIFIED_ON_STAGE,
		self::VERIFIED_ON_RAPID,
		self::VERIFIED_ON_STANDARD,
		self::APPROVED,
		self::SUBMITTED_FOR_APPROVAL
	];

	public static $c_arrintPendingReleaseStatuses = [
		self::COMMITTED,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD
	];

	public static $c_arrintReleaseDateRequiredStatuses = [
		self::COMMITTED,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD,
		self::VERIFIED,
		self::RELEASED,
		self::COMPLETED,
	];

	public static $c_arrintTestCaseStatuses	= [
		self::REVIEW_READY,
		self::REWORK,
		self::NEEDS_MAINTENANCE,
		self::PASS,
		self::FAIL,
		self::BLOCK
	];

	public static $c_arrintTaskValidationStatuses = [
		self::COMPLETED,
		self::CANCELLED,
		self::COMMITTED,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD,
		self::RELEASED,
		self::VERIFIED,
		self::APPROVED,
		self::SUBMITTED_FOR_APPROVAL
	];

	public static $c_arrintTaskStatusesForStoryPointCalculation = [
		self::COMMITTED,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD,
		self::VERIFIED,
		self::RELEASED,
		self::COMPLETED,
		self::CLOSED,
		self::APPROVED
	];

	public static $c_arrintSQMApprovedTaskStatuses 		= [
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_RAPID,
		self::VERIFIED_ON_STANDARD,
		self::COMPLETED,
		self::VERIFIED
	];

	public static $c_arrintItHelpdeskTaskStatusesIds	= [
		self::UNUSED,
		self::ON_HOLD,
		self::BACKLOG,
		self::IN_PROGRESS,
		self::AWAITING_USER_RESPONSE,
		self::AWAITING_APPROVAL,
		self::ACKNOWLEDGED,
		self::COMPLETED,
		self::CANCELLED
	];

	public static $c_arrmixTicketAndTaskStatuses = [
		'TASK_STATUS_NEW'							=> self::UNUSED,
		'TASK_STATUS_ACKNOWLEDGED'					=> self::ACKNOWLEDGED,
		'TASK_STATUS_REASSIGNED'					=> self::REASSIGNED,
		'TASK_STATUS_IN_PROGRESS'					=> self::IN_PROGRESS,
		'TASK_STATUS_ON_HOLD'						=> self::ON_HOLD,
		'TASK_STATUS_COMPLETED'						=> self::COMPLETED,
		'TASK_STATUS_CANCELLED'						=> self::CANCELLED,
		'TASK_STATUS_UNDER_REVIEW'					=> self::UNDER_REVIEW,
		'TASK_STATUS_READY_FOR_DEV'					=> self::READY_FOR_DEV,
		'TASK_STATUS_DEV_IN_PROGRESS'				=> self::DEV_IN_PROGRESS,
		'TASK_STATUS_READY_FOR_QA'					=> self::READY_FOR_QA,
		'TASK_STATUS_QA_IN_PROGRESS'				=> self::QA_IN_PROGRESS,
		'TASK_STATUS_READY_FOR_RELEASE'				=> self::READY_FOR_RELEASE,
		'TASK_STATUS_AWAITING_CUSTOMER_RESPONSE'	=> self::AWAITING_CUSTOMER_RESPONSE,
		'TASK_STATUS_AWAITING_RESPONSE'				=> self::AWAITING_RESPONSE,
		'TASK_STATUS_COMMITTED'						=> self::COMMITTED,
		'TASK_STATUS_RELEASED_ON_RAPID'				=> self::RELEASED_ON_RAPID,
		'TASK_STATUS_RELEASED_ON_STANDARD'			=> self::RELEASED_ON_STANDARD,
		'TASK_STATUS_VERIFIED_ON_RAPID'				=> self::VERIFIED_ON_RAPID,
		'TASK_STATUS_VERIFIED_ON_STANDARD'			=> self::VERIFIED_ON_STANDARD,
		'TASK_STATUS_BACKLOG'						=> self::BACKLOG,
		'TASK_STATUS_RELEASED_ON_STAGE'				=> self::RELEASED_ON_STAGE,
		'TASK_STATUS_VERIFIED_ON_STAGE'				=> self::VERIFIED_ON_STAGE,
		'TASK_STATUS_MIGRATION_IN_PROGRESS'			=> self::MIGRATION_IN_PROGRESS,
		'TASK_STATUS_MIGRATED'						=> self::MIGRATED,
		'TASK_STATUS_SUBMITTED_FOR_APPROVAL'		=> self::SUBMITTED_FOR_APPROVAL
	];

	public static $c_arrintTaskStatusesForStageAndProductionCommit = [
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD
	];

	public static $c_arrintTaskStatusesToAddTestCaseInBuild = [
		CTaskStatus::NEEDS_MAINTENANCE,
		CTaskStatus::APPROVED
	];

	public static $c_arrintTaskStatusesSqmTestCases = [
		CTaskStatus::VERIFIED,
		CTaskStatus::VERIFICATION_REQUIRED
	];

	public static $c_arrmixTestCaseStatusesIdsWithName	= [
		self::IN_PROGRESS	=> 'In progress',
		self::REVIEW_READY	=> 'Review Ready',
		self::REWORK		=> 'Rework',
		self::APPROVED		=> 'Approved'
	];

	public static $c_arrintTaskStatusesTicketFilterExcluded = [
		self::AWAITING_APPROVAL,
		self::REQUESTED,
		self::SUBMITTED_FOR_APPROVAL,
		self::APPROVED,
		self::DELIVERED,
		self::READY_TO_PUBLISH
	];

	public static $c_arrintTaskStatusesForPlannerStoryPoints = [
		self::COMMITTED,
		self::RELEASED_ON_STAGE,
		self::VERIFIED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_RAPID,
		self::RELEASED_ON_STANDARD,
		self::VERIFIED_ON_STANDARD,
		self::COMPLETED,
		self::APPROVED,
		self::CLOSED
	];

	public static $c_arrintTaskStatusesForDefectsAndBugs = [
		self::UNUSED,
		self::REOPEN,
		self::ASSIGNED,
		self::FIXED,
		self::REJECT,
		self::CLOSED,
		self::CANCELLED,
		self::DEFERRED,
		self::UNDER_REVIEW,
		self::ON_HOLD,
		self::BACKLOG,
		self::AWAITING_RESPONSE,
		self::READY_FOR_DEV,
		self::DEV_IN_PROGRESS,
		self::READY_FOR_QA,
		self::QA_IN_PROGRESS,
		self::READY_FOR_RELEASE,
		self::COMMITTED,
		self::RELEASED_ON_STAGE,
		self::RELEASED_ON_RAPID,
		self::VERIFIED_ON_STAGE,
		self::VERIFIED_ON_RAPID,
		self::COMPLETED
	];

	protected $m_arrobjTasks;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjTasks = [];

		return;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addTask( $objTask ) {
		$this->m_arrobjTasks[$objTask->getId()] = $objTask;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getTasks() {
		return $this->m_arrobjTasks;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setTasks( $arrobjTasks ) {
		$this->m_arrobjTasks = $arrobjTasks;
	}

	public static function populateTemplateConstants( $arrmixTemplateParameters ) {
		$arrmixTemplateParameters['TASK_STATUS_NEW']						= self::UNUSED;
		$arrmixTemplateParameters['TASK_STATUS_ACKNOWLEDGED']				= self::ACKNOWLEDGED;
		$arrmixTemplateParameters['TASK_STATUS_REASSIGNED']					= self::REASSIGNED;
		$arrmixTemplateParameters['TASK_STATUS_IN_PROGRESS']				= self::IN_PROGRESS;
		$arrmixTemplateParameters['TASK_STATUS_ON_HOLD']					= self::ON_HOLD;
		$arrmixTemplateParameters['TASK_STATUS_COMPLETED']					= self::COMPLETED;
		$arrmixTemplateParameters['TASK_STATUS_CANCELLED']					= self::CANCELLED;
		$arrmixTemplateParameters['TASK_STATUS_UNDER_REVIEW']				= self::UNDER_REVIEW;
		$arrmixTemplateParameters['TASK_STATUS_READY_FOR_DEV']				= self::READY_FOR_DEV;
		$arrmixTemplateParameters['TASK_STATUS_DEV_IN_PROGRESS']			= self::DEV_IN_PROGRESS;
		$arrmixTemplateParameters['TASK_STATUS_READY_FOR_QA']				= self::READY_FOR_QA;
		$arrmixTemplateParameters['TASK_STATUS_QA_IN_PROGRESS']				= self::QA_IN_PROGRESS;
		$arrmixTemplateParameters['TASK_STATUS_READY_FOR_RELEASE']			= self::READY_FOR_RELEASE;
		$arrmixTemplateParameters['TASK_STATUS_AWAITING_CUSTOMER_RESPONSE']	= self::AWAITING_CUSTOMER_RESPONSE;
		$arrmixTemplateParameters['TASK_STATUS_AWAITING_RESPONSE']			= self::AWAITING_RESPONSE;
		$arrmixTemplateParameters['TASK_STATUS_COMMITTED']					= self::COMMITTED;
		$arrmixTemplateParameters['TASK_STATUS_RELEASED_ON_RAPID']			= self::RELEASED_ON_RAPID;
		$arrmixTemplateParameters['TASK_STATUS_RELEASED_ON_STANDARD']		= self::RELEASED_ON_STANDARD;
		$arrmixTemplateParameters['TASK_STATUS_VERIFIED_ON_RAPID']			= self::VERIFIED_ON_RAPID;
		$arrmixTemplateParameters['TASK_STATUS_VERIFIED_ON_STANDARD']		= self::VERIFIED_ON_STANDARD;
		$arrmixTemplateParameters['TASK_STATUS_BACKLOG']					= self::BACKLOG;
		$arrmixTemplateParameters['TASK_STATUS_RELEASED_ON_STAGE']			= self::RELEASED_ON_STAGE;
		$arrmixTemplateParameters['TASK_STATUS_VERIFIED_ON_STAGE']			= self::VERIFIED_ON_STAGE;
		$arrmixTemplateParameters['TASK_STATUS_MIGRATION_IN_PROGRESS']		= self::MIGRATION_IN_PROGRESS;
		$arrmixTemplateParameters['TASK_STATUS_MIGRATED']					= self::MIGRATED;
		$arrmixTemplateParameters['TASK_STATUS_RELEASED']					= self::RELEASED;
		$arrmixTemplateParameters['TASK_STATUS_VERIFIED']					= self::VERIFIED;
		$arrmixTemplateParameters['TASK_STATUS_PENDING_RELEASE']			= self::PENDING_RELEASE;
		$arrmixTemplateParameters['TASK_STATUS_APPROVED']					= self::APPROVED;
		$arrmixTemplateParameters['TASK_STATUS_SUBMITTED_FOR_APPROVAL']		= self::SUBMITTED_FOR_APPROVAL;
		$arrmixTemplateParameters['TASK_STATUS_READY_TO_PUBLISH']			= self::READY_TO_PUBLISH;
		return $arrmixTemplateParameters;
	}

	public static function determineTaskStatusNameByTaskStatusId( $intTaskStatusId ) {
		$strTaskStatusName = NULL;

		switch( $intTaskStatusId ) {
			case self::UNUSED:
				$strTaskStatusName = 'New';
				break;

			case self::ACKNOWLEDGED:
				$strTaskStatusName = 'Acknowledged';
				break;

			case self::REASSIGNED:
				$strTaskStatusName = 'Reassigned';
				break;

			case self::IN_PROGRESS:
				$strTaskStatusName = 'In Progress';
				break;

			case self::ON_HOLD:
				$strTaskStatusName = 'On Hold';
				break;

			case self::COMPLETED:
				$strTaskStatusName = 'Completed';
				break;

			case self::CANCELLED:
				$strTaskStatusName = 'Cancelled';
				break;

			case self::UNDER_REVIEW:
				$strTaskStatusName = 'Under Review';
				break;

			case self::READY_FOR_DEV:
				$strTaskStatusName = 'Dev Ready';
				break;

			case self::DEV_IN_PROGRESS:
				$strTaskStatusName = 'Dev in Progress';
				break;

			case self::READY_FOR_QA:
				$strTaskStatusName = 'QA Ready';
				break;

			case self::QA_IN_PROGRESS:
				$strTaskStatusName = 'QA in Progress';
				break;

			case self::READY_FOR_RELEASE:
				$strTaskStatusName = 'Release Ready';
				break;

			case self::COMMITTED:
				$strTaskStatusName = 'Committed';
				break;

			case self::RELEASED_ON_RAPID:
				$strTaskStatusName = 'Released on Rapid';
				break;

			case self::RELEASED_ON_STANDARD:
				$strTaskStatusName = 'Released on Standard';
				break;

			case self::VERIFIED_ON_RAPID:
				$strTaskStatusName = 'Verified on Rapid';
				break;

			case self::VERIFIED_ON_STANDARD:
				$strTaskStatusName = 'Verified on Standard';
				break;

			case self::BACKLOG:
				$strTaskStatusName = 'Backlog';
				break;

			case self::AWAITING_CUSTOMER_RESPONSE:
				$strTaskStatusName = 'Awaiting Customer Response';
				break;

			case self::AWAITING_RESPONSE:
				$strTaskStatusName = 'Awaiting Response';
				break;

			default:
				// default case
				break;
		}

		return $strTaskStatusName;
	}

	public static function prepareClientFriendlyTaskStatuses( $arrobjTaskStatuses ) {

		$arrintInProgressTaskIds	= [
			self::IN_PROGRESS,
			self::READY_FOR_DEV,
			self::DEV_IN_PROGRESS,
			self::READY_FOR_QA,
			self::QA_IN_PROGRESS,
			self::READY_FOR_RELEASE,
			self::COMMITTED,
			self::RELEASED_ON_RAPID,
			self::RELEASED_ON_STANDARD,
			self::VERIFIED_ON_RAPID,
			self::VERIFIED_ON_STANDARD,
			self::REASSIGNED,
			self::ON_HOLD
		];

		$arrintInQueueTaskIds	= [
			self::BACKLOG
		];

		$arrintAcknowledgedTaskIds	= [
			self::ACKNOWLEDGED,
			self::UNDER_REVIEW,
			self::AWAITING_RESPONSE
		];

		$arrintInResponseNeededTaskIds	= [ self::AWAITING_CUSTOMER_RESPONSE ];

		foreach( $arrobjTaskStatuses as $objTaskStatus ) {
			if( true == in_array( $objTaskStatus->getId(), $arrintInProgressTaskIds ) ) {
				$objTaskStatus->setName( 'In Progress' );

			} elseif( true == in_array( $objTaskStatus->getId(), $arrintInQueueTaskIds ) ) {
				$objTaskStatus->setName( 'In Queue' );

			} elseif( true == in_array( $objTaskStatus->getId(), $arrintAcknowledgedTaskIds ) ) {
				$objTaskStatus->setName( 'Acknowledged' );

			} elseif( true == in_array( $objTaskStatus->getId(), $arrintInResponseNeededTaskIds ) ) {
				$objTaskStatus->setName( 'Response Needed' );
			}
		}

		return $arrobjTaskStatuses;
	}

	public static function getClientFriendlyTaskStatus( $objTaskStatus ) {
		if( false == valObj( $objTaskStatus, 'CTaskStatus' ) ) return NULL;

		$arrintInProgressTaskIds	= [
			self::IN_PROGRESS,
			self::READY_FOR_DEV,
			self::DEV_IN_PROGRESS,
			self::READY_FOR_QA,
			self::QA_IN_PROGRESS,
			self::READY_FOR_RELEASE,
			self::COMMITTED,
			self::RELEASED_ON_STAGE,
			self::VERIFIED_ON_STAGE,
			self::RELEASED_ON_RAPID,
			self::RELEASED_ON_STANDARD,
			self::VERIFIED_ON_RAPID,
			self::VERIFIED_ON_STANDARD,
			self::REASSIGNED,
			self::ON_HOLD,
			self::PENDING_RELEASE
		];

		$arrintInQueueTaskIds	= [
			self::BACKLOG
		];

		$arrintAcknowledgedTaskIds	= [
			self::ACKNOWLEDGED,
			self::UNDER_REVIEW,
			self::AWAITING_RESPONSE
		];

		$arrintInResponseNeededTaskIds	= [ self::AWAITING_CUSTOMER_RESPONSE ];

		$arrintVerificationRequiredIds = [ self::COMPLETED ];

		if( true == in_array( $objTaskStatus->getId(), $arrintInProgressTaskIds ) ) {
			$objTaskStatus->setName( __( 'In Progress' ) );

		} elseif( true == in_array( $objTaskStatus->getId(), $arrintInQueueTaskIds ) ) {
			$objTaskStatus->setName( __( 'In Queue' ) );

		} elseif( true == in_array( $objTaskStatus->getId(), $arrintInResponseNeededTaskIds ) ) {
			$objTaskStatus->setName( __( 'Response Needed' ) );

		} elseif( true == in_array( $objTaskStatus->getId(), $arrintVerificationRequiredIds ) ) {
			$objTaskStatus->setName( __( 'Verification Required' ) );

		} elseif( true == in_array( $objTaskStatus->getId(), $arrintAcknowledgedTaskIds ) ) {
			$objTaskStatus->setName( __( 'Acknowledged' ) );

		}

		return $objTaskStatus;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>