<?php

class CCommissionType extends CBaseCommissionType {

	const AUTOMATED_SALES_CLOSER			= 1;
	const SALES_CLOSER						= 2;
	const SALES_MANAGERS					= 4;
	const SEO								= 5;
	const RESIDENT_INSURE_FOR_RVPS			= 6;
	const RESIDENT_INSURE_FOR_CUSTOMERS		= 7;
	const SUCCESS_MANAGEMENT_COMMISSIONS	= 8;
	const MISCALLANEOUS						= 9;
	const PRODUCT_COMMISSIONS               = 10;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>