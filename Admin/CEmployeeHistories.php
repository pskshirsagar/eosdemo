<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeHistories
 * Do not add any new functions to this class.
 */

class CEmployeeHistories extends CBaseEmployeeHistories {

	public static function fetchEmployeeHistoriesByFilterByCountryCode( $objEmployeesHistoriesFilter, $strCountryCode, $objDatabase, $strSearch = NULL ) {

		$strAction						= ( false == is_null( $objEmployeesHistoriesFilter ) ) ? $objEmployeesHistoriesFilter->getAction() : NULL;
		$strWhereClause					= ( true == $objEmployeesHistoriesFilter->getCurrent() ) ? ' WHERE hd1.row_number = 1 ORDER BY hd1.id' : NULL;
		$intTotalTimeSpentFromMonths	= ( ( true == is_numeric( $objEmployeesHistoriesFilter->getTimeSpentFromYears() ) ) ? 12 * $objEmployeesHistoriesFilter->getTimeSpentFromYears() : 0 ) + ( ( true == is_numeric( $objEmployeesHistoriesFilter->getTimeSpentFromMonths() ) ) ? $objEmployeesHistoriesFilter->getTimeSpentFromMonths() : 0 );
		$intTotalTimeSpentToMonths		= ( ( true == is_numeric( $objEmployeesHistoriesFilter->getTimeSpentToYears() ) ) ? 12 * $objEmployeesHistoriesFilter->getTimeSpentToYears() : 0 ) + ( ( true == is_numeric( $objEmployeesHistoriesFilter->getTimeSpentToMonths() ) ) ? $objEmployeesHistoriesFilter->getTimeSpentToMonths() : 0 );

		switch( $strAction ) {
			case NULL:
			case 'Designation':
				$strInnerSelect	= ' d.name AS designation_name,';
				$strJoin		= ' LEFT JOIN designations d ON ( d.id = eh.designation_id )';
				$strWhere		= ' AND eh.designation_id IS NOT NULL';
				$strSelect		= 'hd1.designation_name,';
				break;

			case 'Department':
				$strInnerSelect	= ' dept.name AS department_name,';
				$strJoin		= ' LEFT JOIN departments dept ON ( dept.id = eh.department_id )';
				$strWhere		= ' AND eh.department_id IS NOT NULL';
				$strSelect		= 'hd1.department_name,';
				break;

			case 'Team':
				$strInnerSelect	= ' t.name AS team_name,';
				$strJoin		= ' LEFT JOIN teams t ON ( t.id = eh.team_id )';
				$strWhere		= ' AND eh.team_id IS NOT NULL';
				$strSelect		= 'hd1.team_name,';
				break;

			default:
				$strInnerSelect	= ' d.name AS designation_name,';
				$strJoin		= ' LEFT JOIN designations d ON ( d.id = eh.designation_id )';
				$strWhere		= ' AND eh.designation_id IS NOT NULL';
				$strSelect		= 'hd1.designation_name,';
				break;
		}

		if( true == valStr( $objEmployeesHistoriesFilter->getFromDate() ) && true == valStr( $objEmployeesHistoriesFilter->getToDate() ) ) {
			$strWhere .= ' AND ( eh.datetime::date BETWEEN \'' . $objEmployeesHistoriesFilter->getFromDate() . '\' AND \'' . $objEmployeesHistoriesFilter->getToDate() . '\' )';
		}

		if( 0 < $intTotalTimeSpentFromMonths || 0 < $intTotalTimeSpentToMonths ) {
			$strWhere .= ' AND ( eh.datetime::date BETWEEN now()::date - interval \'' . $intTotalTimeSpentToMonths . ' month\' AND now()::date - interval \'' . $intTotalTimeSpentFromMonths . ' month\' ) ';
		}

		if( true == valStr( $objEmployeesHistoriesFilter->getHrRepresentativeIds() ) ) {
			$strJoin .= ' LEFT JOIN team_employees te ON ( te.employee_id = e.id AND is_primary_team = 1 ) LEFT JOIN teams t1 ON ( t1.id = te.team_id ) ';
			$strWhere .= ' AND t1.hr_representative_employee_id IN ( ' . $objEmployeesHistoriesFilter->getHrRepresentativeIds() . ' ) ';
		}

		if( true == valStr( $strSearch ) ) {
			$strWhere .= ' AND e.preferred_name ILIKE \'%' . $strSearch . '%\'';
		}

		$strSql = ' WITH history_data AS (
						SELECT
							e.id,
							e.employee_number,
							e.preferred_name,
							' . $strInnerSelect . '
							eh.datetime::DATE,
							ROW_NUMBER() OVER ( partition BY eh.employee_id ORDER BY eh.datetime DESC ) AS row_number
						FROM
							employee_histories eh
							LEFT JOIN employees e ON ( e.id = eh.employee_id )
							LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
							' . $strJoin . '
						WHERE
							ea.country_code = \'' . $strCountryCode . '\'
							' . $strWhere . '
					)

					SELECT
						CASE
							WHEN hd2.row_number = hd1.row_number - 1 AND hd1.id = hd2.id THEN NULL
							ELSE hd1.id
						END as id,
						CASE
							WHEN hd2.row_number = hd1.row_number - 1 AND hd1.employee_number = hd2.employee_number THEN NULL
							ELSE hd1.employee_number
						END as employee_number,
						CASE
							WHEN hd2.row_number = hd1.row_number - 1 AND hd1.id = hd2.id THEN NULL
							ELSE hd1.preferred_name
						END as employee_name,
						' . $strSelect . '
						hd1.datetime as assigned_on,
						CASE
						WHEN hd2.datetime IS NULL THEN age ( now ( )::DATE, hd1.datetime )
							ELSE age ( hd2.datetime, hd1.datetime )
						END AS duration
					FROM
						history_data hd1
						LEFT JOIN history_data hd2 ON ( hd2.row_number = hd1.row_number - 1 AND hd1.id = hd2.id )' . $strWhereClause;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeHistoriesByEmployeeIdByDate( $intEmployeeId, $strDate, $objAdminDatabase ) {

		if( false == is_numeric( $intEmployeeId ) || false == valStr( $strDate ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							employee_id,
							designation_id,
							CASE WHEN designation_id <> LAG(designation_id) OVER()
								 THEN LAG(designation_id) OVER() ELSE designation_id END AS previous_designation,
							datetime
						FROM employee_histories
						WHERE employee_id = ' . ( int ) $intEmployeeId . ' and
								designation_id IS NOT NULL
					) sub_query WHERE sub_query.datetime >= \'' . $strDate . '\' ORDER BY sub_query.datetime ASC LIMIT 1';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeHistoriesByEmployeeIds( $arrintEmployeeIds, $objAdminDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						sub_query.*
					FROM (
							SELECT
								eh.employee_id,
								eh.designation_id,
								d.employee_salary_type_id,
								row_number() OVER(partition by eh.employee_id ORDER BY eh.datetime DESC ) as rank
							FROM employee_histories eh
							LEFT JOIN designations d ON ( d.id = eh.designation_id )
							WHERE eh.employee_id  IN (' . implode( ' ,', $arrintEmployeeIds ) . ') AND
								  eh.designation_id IS NOT NULL
								   AND EXTRACT( YEAR from eh.datetime ) = EXTRACT( YEAR from CURRENT_DATE )
						) as sub_query
					WHERE
						sub_query.rank = 2';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchTeamEmployeeHistoriesByEmployeeId( $intEmployeeId, $objAdminDatabase, $intPageNo = NULL, $intPageSize = NULL ) {

		if( false == valId( $intEmployeeId ) ) return NULL;

		if( true == valId( $intPageNo ) && true == valId( $intPageSize ) ) {
			$intOffset		= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit		= ( int ) $intPageSize;
			$strLimitClause = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$strSql = 'SELECT
						eh.created_on as date_started,
						d.name,
						e.preferred_name,
						e.name_full,
						t.name as team_name
					FROM
						employee_histories eh
						LEFT JOIN designations d ON ( d.id = eh.designation_id )
						LEFT JOIN employees e ON ( e.id = eh.manager_employee_id )
						LEFT JOIN teams t ON ( t.id = eh.team_id )
					WHERE
						eh.employee_id = ' . ( int ) $intEmployeeId . '
						AND ( e.preferred_name IS NOT NULL OR t.name IS NOT NULL OR d.name IS NOT NULL )
					ORDER BY eh.created_on DESC'
					. $strLimitClause;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeHistoryDataByEmployeeIdByStatus( $intEmployeeId, $objAdminDatabase, $strStatus = NULL ) {
		if( false == valId( $intEmployeeId ) ) {
			return NULL;
		}
		$strSql = 'SELECT
					*
					FROM employee_histories
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND CAST ( history_data ->> \'annual_bonus_type_id\' AS integer ) IS NOT NULL
						AND history_data ->> \'bonus_requirement\' IS NOT NULL
						AND ( history_data ->> \'status\' ) = \'' . ( $strStatus ) . '\'
						ORDER BY id DESC LIMIT 1';

		return self::fetchEmployeeHistory( $strSql, $objAdminDatabase );
	}

}
?>