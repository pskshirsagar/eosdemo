<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CNewTeams
 * Do not add any new functions to this class.
 */

class CNewTeams extends CBaseNewTeams {

	public static function fetchTeamsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $strOrderBy = 'name' ) {
		if( true == valStr( $strOrderBy ) ) {
			$strOrderBy = $strOrderBy;
		}
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE deleted_by IS NULL AND manager_employee_id = ' . ( int ) $intManagerEmployeeId . ' ORDER BY ' . $strOrderBy ), $objDatabase );
	}

	public static function fetchTeamsByManagerEmployeeIds( $arrintManagerEmployeeIds, $objDatabase ) {
		return self::fetchNewTeams( 'SELECT * FROM new_teams WHERE manager_employee_id IN ( ' . implode( ',', $arrintManagerEmployeeIds ) . ' ) AND deleted_by IS NULL ORDER BY name', $objDatabase );
	}

	public static function fetchConflictingTeamCountByName( $strTeamName, $intTeamId, $intManagerEmployeeId, $objDatabase ) {

		if( false == is_null( $strTeamName ) && false == is_null( $intManagerEmployeeId ) ) {

			$strWhereSql = ' WHERE
					name = \'' . trim( addslashes( $strTeamName ) ) . '\'
					AND manager_employee_id = \'' . ( int ) $intManagerEmployeeId . '\'
					AND deleted_by is NULL ';

			if( false == is_null( $intTeamId ) ) {
				$strWhereSql .= ' AND id != ' . ( int ) $intTeamId;
			}

			return self::fetchNewTeamCount( $strWhereSql, $objDatabase );
		} else {
			return NULL;
		}
	}

}
?>