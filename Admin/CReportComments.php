<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportComments
 * Do not add any new functions to this class.
 */

class CReportComments extends CBaseReportComments {

	public static function fetchReportCommentDetailsByCompanyReportId( $intCompanyReportId, $intLastReportCommentId, $objAdminDatabase, $boolLoadPreviousComments = false ) {
		if( false == is_numeric( $intCompanyReportId ) ) {
			return 0;
		}

		$strWhereCondition = '';
		if( true == is_numeric( $intLastReportCommentId ) && false == $boolLoadPreviousComments ) {
			$strWhereCondition .= ' WHERE
										rc.id > ' . ( int ) $intLastReportCommentId . '';
		} elseif( true == $boolLoadPreviousComments ) {
			$strWhereCondition = ' WHERE
										rc.id < ' . ( int ) $intLastReportCommentId . '
									ORDER BY
										rc.id DESC
									LIMIT ' . CReportComment::PER_PAGE_LIMIT_OF_REPORT_COMMENTS;
		} else {
			$strWhereCondition = ' ORDER BY
										rc.id DESC
									LIMIT ' . CReportComment::PER_PAGE_LIMIT_OF_REPORT_COMMENTS;
		}

		$strSql = 'SELECT
						rc.id AS report_comment_id,
						rc.report_comment,
						e.preferred_name,
						rc.created_on,
						e.id AS employee_id
					FROM
						report_comments AS rc
						JOIN users AS u ON ( rc.created_by = u.id AND rc.company_report_id = ' . $intCompanyReportId . ' )
						JOIN employees AS e ON ( u.employee_id = e.id )
					' . $strWhereCondition;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchReportCommentsCountByCompanyReportId( $intCompanyReportId, $objAdminDatabase ) {
		if( false == is_numeric( $intCompanyReportId ) ) {
			return 0;
		}
		$strSql = 'SELECT
						count( rc.company_report_id ) as company_report_id
					FROM
						report_comments AS rc
					WHERE
						rc.company_report_id =  ' . ( int ) $intCompanyReportId;

		return parent::fetchColumn( $strSql, 'company_report_id', $objAdminDatabase );

	}

}
?>