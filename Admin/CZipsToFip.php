<?php

class CZipsToFip extends CBaseZipsToFip {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valZipCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFipsCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMsa() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>