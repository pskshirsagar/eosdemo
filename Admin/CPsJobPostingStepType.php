<?php

class CPsJobPostingStepType extends CBasePsJobPostingStepType {

	const SCREENING		= 1;
	const TEST			= 2;
	const INTERVIEW 	= 3;
	const OTHER			= 4;
	const OFFER			= 6;
	const REFERENCES	= 7;
	const HISTORY		= 8;

	const STEP_APPLIED	= 'APPLIED';

	public static $c_arrmixDefaultPsJobPostingSteps = array(
		1 => array( 'ps_job_posting_step_type_id' => self::SCREENING, 'name' => 'Applied', 'order_num' => '1' ),
		2 => array( 'ps_job_posting_step_type_id' => self::OFFER, 'name' => 'Offer Extended', 'order_num' => '2' ),
		3 => array( 'ps_job_posting_step_type_id' => self::OFFER, 'name' => 'Offer Accepted', 'order_num' => '3' ),
		4 => array( 'ps_job_posting_step_type_id' => self::REFERENCES, 'name' => 'References' )
	);

	public static $c_arrmixDefaultPsJobPostingStepsByName = array(
		self::SCREENING  	=> 'Screening',
		self::TEST			=> 'Test',
		self::INTERVIEW		=> 'Interview',
		self::OFFER			=> 'Offer',
		self::REFERENCES	=> 'References',
		self::HISTORY		=> 'History',
		self::OTHER			=> 'Other'
	);

	public static $c_arrmixPsJobPostingBasicStepIds = [
		self::SCREENING,
		self::REFERENCES
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>