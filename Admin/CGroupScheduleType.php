<?php

class CGroupScheduleType extends CBaseGroupScheduleType {

	const DAILY_GROUP_SCHEDLUE_TYPE     = 1;
	const WEEKLY_GROUP_SCHEDLUE_TYPE    = 2;
	const MONTHLY_GROUP_SCHEDLUE_TYPE   = 3;
	const QUARTERLY_GROUP_SCHEDLUE_TYPE = 4;
	const YEARLY_GROUP_SCHEDLUE_TYPE    = 5;

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>