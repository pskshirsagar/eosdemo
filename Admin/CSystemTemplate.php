<?php

class CSystemTemplate extends CBaseSystemTemplate {

	use Psi\Libraries\Container\TContainerized;
	use Psi\Libraries\EosFoundation\TEosStoredObject;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTemplateTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTemplateName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( CClient::ID_DEFAULT, $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

	// @deprecated This function is deprecated.
	// @TODO:: This will be refactored under task [ 2810461, 2810463 ]
	public function getTempFullPath( $objObjectStorageGateway, $objDatabase, $strReferenceTag = NULL ) {

		$arrmixGatewayRequest = $this->fetchStoredObject( $objDatabase ?? $this->m_objDatabase, $strReferenceTag )->createGatewayRequest( [ 'outputFile'    => 'temp' ] );
		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );
		return $objObjectStorageGatewayResponse['outputFile'];
	}

	// @FIXME::Implement if required
	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		// place Holder
	}

	// @FIXME::Implement if required
	protected function calcStorageContainer( $strVendor = NULL ) {
		// place Holder
	}

}
?>