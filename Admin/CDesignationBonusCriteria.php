<?php

class CDesignationBonusCriteria extends CBaseDesignationBonusCriteria {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDesignationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEncryptedAnnualBonusPotential() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBonusCriteria() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolValid &= $this->valEncryptedAnnualBonusPotential();
				$boolValid &= $this->valFrequencyId();
				$boolValid &= $this->valBonusCriteria();
				$boolValid &= $this->valStartDate();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolValid = false;
				break;
		}

		return $boolValid;
	}

}
?>