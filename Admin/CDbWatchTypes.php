<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDbWatchTypes
 * Do not add any new functions to this class.
 */

class CDbWatchTypes extends CBaseDbWatchTypes {

	public static function fetchDbWatchTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDbWatchType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDbWatchType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDbWatchType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllDbWatchTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM db_watch_types';
		return self::fetchDbWatchTypes( $strSql, $objDatabase );
	}
}
?>