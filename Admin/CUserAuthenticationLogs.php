<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserAuthenticationLogs
 * Do not add any new functions to this class.
 */

class CUserAuthenticationLogs extends CBaseUserAuthenticationLogs {

	public static function fetchLatestPasswordRotationDaysByUserId( $intUserId, $objAdminDatabase ) {

		$strSql = 'SELECT
						u.id,
						( DATE \'now()\' -
										(
										SELECT
											b.created_on
										FROM
											user_authentication_logs b
										WHERE
											b.user_id = ' . ( int ) $intUserId . '
											AND previous_password IS NOT NULL
										ORDER BY
											b.id DESC
										LIMIT
											1
						) ::date ) AS rotation_days,
						u.last_login
					FROM
						users u
					WHERE
						u.id = ' . ( int ) $intUserId . '
					LIMIT
						1';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchPreviousPasswordsByUserIdByRotationNumber( $intUserId, $intRotationNumber, $objAdminDatabase ) {
		$strSql = 'SELECT
						previous_password
					FROM
						user_authentication_logs
					WHERE
						user_id = ' . ( int ) $intUserId . '
						AND previous_password IS NOT NULL
					ORDER BY
						created_on DESC';

		if( $intRotationNumber > 0 ) {
			$strSql .= ' LIMIT ' . ( int ) $intRotationNumber;
		}

		return self::fetchUserAuthenticationLogs( $strSql, $objAdminDatabase );
	}

	public static function fetchUserAuthenticationLogByIdByUserId( $intId, $intUserId, $objAdminDatabase ) {
		return self::fetchUserAuthenticationLog( sprintf( 'SELECT * FROM user_authentication_logs WHERE id = %d AND user_id = %d', ( int ) $intId, ( int ) $intUserId ), $objAdminDatabase );
	}

	public static function fetchLastUserAuthenticationLogByUserId( $intUserId, $objAdminDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						user_authentication_logs
					WHERE
						user_id = ' . ( int ) $intUserId . '
					ORDER BY
						id DESC
					OFFSET 1 LIMIT 1';

		return self::fetchUserAuthenticationLog( $strSql, $objAdminDatabase );
	}

	public static function fetchPaginatedUserAuthenticationLogsByUserId( $intPageNo, $intPageSize, $intUserId, $objAdminDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
						*
					FROM
						user_authentication_logs
					WHERE
						user_id = ' . ( int ) $intUserId . '
					ORDER BY
						id DESC OFFSET ' . ( int ) $intOffset . '
					LIMIT
						' . $intLimit;

		return self::fetchUserAuthenticationLogs( $strSql, $objAdminDatabase );
	}

	public static function fetchUserAuthenticationLogsCountByUserId( $intUserId, $objAdminDatabase ) {

		$arrmixUserAuthenticationLogs = parent::fetchUserAuthenticationLogsByUserId( $intUserId, $objAdminDatabase );
		return \Psi\Libraries\UtilFunctions\count( $arrmixUserAuthenticationLogs );

	}

	public static function updateUserSessionClickCount( $intUserAuthentiactionLogId, $objAdminDatabase ) {

		if( true == is_null( $intUserAuthentiactionLogId ) || false == is_numeric( $intUserAuthentiactionLogId ) ) {
			return false;
		}

		$strSql = ' UPDATE
						user_authentication_logs
					SET
						click_count = click_count + 1,
						last_click = NOW()
					WHERE
						id = ' . $intUserAuthentiactionLogId;

		return fetchData( $strSql, $objAdminDatabase );

	}

}
?>