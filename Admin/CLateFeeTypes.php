<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CLateFeeTypes
 * Do not add any new functions to this class.
 */

class CLateFeeTypes extends CBaseLateFeeTypes {

	public static function fetchLateFeeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CLateFeeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchLateFeeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CLateFeeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllLateFeeTypes( $objDatabase ) {
		return CLateFeeTypes::fetchLateFeeTypes( sprintf( 'SELECT * FROM %s ORDER BY order_num ', 'late_fee_types' ), $objDatabase );
	}
}
?>