<?php

class CContentTranslationStatus extends CBaseContentTranslationStatus {

	const STATUS_READY_TO_TRANSLATE		= 1;
	const STATUS_SENT_FOR_TRANSLATION	= 2;
	const STATUS_READY_TO_REVIEW		= 3;
	const STATUS_TRANSLATED				= 4;

	public static $c_arrstrContentTranslationStatuses = [ CContentTranslationStatus::STATUS_SENT_FOR_TRANSLATION, CContentTranslationStatus::STATUS_READY_TO_REVIEW ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function loadContentTranslationStatuses( $arrmixTemplateParameters ) {
		$arrmixTemplateParameters['STATUS_READY_TO_TRANSLATE']		= self::STATUS_READY_TO_TRANSLATE;
		$arrmixTemplateParameters['STATUS_SENT_FOR_TRANSLATION']	= self::STATUS_SENT_FOR_TRANSLATION;
		$arrmixTemplateParameters['STATUS_READY_TO_REVIEW']			= self::STATUS_READY_TO_REVIEW;
		$arrmixTemplateParameters['STATUS_TRANSLATED']				= self::STATUS_TRANSLATED;
		return $arrmixTemplateParameters;
	}

}
?>