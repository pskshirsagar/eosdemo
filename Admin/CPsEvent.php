<?php

class CPsEvent extends CBasePsEvent {

	protected $m_intPsEventStartTime;
	protected $m_intPsEventEndTime;

	/**
	 * Get Functions
	 */

	public function getPsEventStartTime() {
		return $this->m_intPsEventStartTime;
	}

	public function getPsEventEndTime() {
		return $this->m_intPsEventEndTime;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		// ( 0 == $arrmixValues['is_published'] ) ? $this->setIsPublished( 0 ) : $this->setIsPublished( 1 );
		if( true == isset( $arrmixValues['event_start_time'] ) ) 	$this->setPsEventStartTime( $arrmixValues['event_start_time'] );
		if( true == isset( $arrmixValues['event_end_time'] ) )	$this->setPsEventEndTime( $arrmixValues['event_end_time'] );

	}

	// function setIsPublished( $intIsPublished ) {
	//   $this->m_intIsPublished = $intIsPublished;
	// }

	public function setPsEventStartTime( $intPsEventStartTime ) {
		$this->m_intPsEventStartTime = $intPsEventStartTime;
	}

	public function setPsEventEndTime( $intPsEventEndTime ) {

		$this->m_intPsEventEndTime = $intPsEventEndTime;
	}

	/**
	 *
	 */

	public function valPsEventTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsEventTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_event_type_id', 'Event type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {

		$boolIsValid = true;
		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valStartDatetime() {
		$boolIsValid = true;

		if( CPsEventType::ANNOUNCEMENT == $this->getPsEventTypeId() ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->getStartDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_datetime', 'Start date is required. ' ) );
		} elseif( false == CValidation::validateDate( $this->getStartDatetime() ) ) {
		$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_datetime', 'Valid start date is required. ' ) );
		} elseif( true == ( strtotime( date( 'm/d/Y G:i' ) ) > strtotime( $this->getStartDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_datetime', 'Selected start date/time already passed. ' ) );
		}
		return $boolIsValid;
	}

	public function valEndDatetime() {

		$boolIsValid = true;

		if( CPsEventType::ANNOUNCEMENT == $this->getPsEventTypeId() || true == is_null( $this->getEndDatetime() ) ) {

			return $boolIsValid;
		}

		if( false == CValidation::validateDate( $this->getEndDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', 'Valid end date is required. ' ) );
		} elseif( strtotime( $this->getStartDatetime() ) >= strtotime( $this->getEndDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', 'End date/time should be greater than start date/time. ' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsFromPublish = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				if( false == $boolIsFromPublish ) {
					$boolIsValid &= $this->valPsEventTypeId();

					$boolIsValid &= $this->valTitle( $objDatabase );

					$boolIsValid &= $this->valStartDatetime();

					$boolIsValid &= $this->valEndDatetime();

				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

 	public function setStartAndEndDateTime() {

		if( CPsEventType::ANNOUNCEMENT != $this->getPsEventTypeId() ) {

			if( false == is_null( $this->getStartDatetime() ) && 0 < strlen( $this->getStartDatetime() ) ) {
				$intStartDateTime 			= date( 'H:i', strtotime( $this->getPsEventStartTime() ) );
				$intPsEventStartDateTime	= strtotime( $this->getStartDatetime() . $intStartDateTime );
				$intStartDateTime			= date( 'm/d/Y H:i', $intPsEventStartDateTime );

				$this->setStartDatetime( $intStartDateTime );
			}

			if( false == is_null( $this->getEndDatetime() ) && 0 < strlen( $this->getEndDatetime() ) ) {
				$intEndDateTime = date( 'H:i', strtotime( $this->getPsEventEndTime() ) );
				$intPsEventEndDateTime	= strtotime( $this->getEndDatetime() . $intEndDateTime );
				$intEndDateTime			= date( 'm/d/Y H:i', $intPsEventEndDateTime );

				$this->setEndDatetime( $intEndDateTime );
		 	}
		}
	}

}

?>