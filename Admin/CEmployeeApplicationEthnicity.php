<?php

class CEmployeeApplicationEthnicity extends CBaseEmployeeApplicationEthnicity {

	const EMPLOYEE_ETHINCITY_ASIAN	= 4;
	const HISPANIC_OR_LATINO 		= 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>