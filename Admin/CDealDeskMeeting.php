<?php

class CDealDeskMeeting extends CBaseDealDeskMeeting {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAgenda() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledOn() {
		$boolIsValid = true;

		if( false == \valStr( $this->m_strScheduledOn ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_on', 'Scheduled meeting date and time is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valScheduledOn();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valScheduledOn();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>