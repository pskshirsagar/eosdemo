<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskDataRequests
 * Do not add any new functions to this class.
 */

class CTaskDataRequests extends CBaseTaskDataRequests {

	public static function fetchTaskDataRequestCountDeatils( $objDatabase ) {

		$strSql = 'SELECT
						u.id as user_id,
						count( t.id ) AS task_data_request_count,
						e.preferred_name,
						e.id AS employee_id
					FROM
						users as u 
						JOIN employees as e ON ( u.employee_id = e.id AND e.employee_status_type_id =' . ( int ) CEmployeeStatusType::CURRENT . ' AND e.designation_id IN ( ' . implode( ',', CDesignation::$c_arrintDataAnaylstDesignations ) . ' ) )
						LEFT JOIN tasks as t ON ( u.id = t.user_id AND t.task_type_id = ' . ( int ) CTaskType::TASK_DATA_REQUEST . ' AND t.deleted_by IS NULL )
						LEFT JOIN task_data_requests AS tdr ON ( tdr.task_id = t.id ) 
					GROUP BY
						t.user_id,
						e.preferred_name,
						e.id,
						u.id
					ORDER BY
						task_data_request_count';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportDataRequestByCompanyReportId( $intCompanyReportId, $objDatabase ) {

		if( false == is_numeric( $intCompanyReportId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						t.id as task_id,
						t.description,
						t.user_id,
						t.task_status_id,
						e.id as employee_id,
						e.preferred_name,
						t.task_priority_id,
						tdr.requested_completion_date,
						tdr.additional_info,
						array_to_string ( array_agg ( ta.url ), \',\' ) AS tasknote_attachments
					FROM
						task_data_requests tdr
						JOIN tasks t ON ( t.id = tdr.task_id)
						LEFT JOIN task_attachments ta ON ( ta.task_id = t.id )
						JOIN users u ON ( u.id = t.user_id)
						JOIN employees e ON ( e.id = u.employee_id)
					WHERE tdr.company_report_id = ' . ( int ) $intCompanyReportId . '
					GROUP BY 
						t.id,
						e.preferred_name,
						e.id,
						t.task_priority_id,
						tdr.requested_completion_date,
						tdr.additional_info';

		$arrmixReportDetail = fetchData( $strSql, $objDatabase );
		return $arrmixReportDetail[0];

	}

	public static function fetchTaskDataRequestByCompanyReportId( $intCaReportId, $objDatabase ) {

		return self::fetchTaskDataRequest( sprintf( 'SELECT * FROM task_data_requests WHERE company_report_id = %d', ( int ) $intCaReportId ), $objDatabase );
	}

	public static function fetchTaskDataRequestsByEmployeeId( $intEmployeeId, $objDatabase, $intLimit = NULL, $intOffset = NULL, $strSortBy, $intSortOrder ) {

		if( false == valId( $intEmployeeId ) ) {
			return false;
		}
		$strLimit = '';
		if( true == valId( $intLimit ) ) {
			$strLimit = ' LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;
		}

		if( true == valStr( $strSortBy ) ) {
			$strSortDirection = ( 1 == $intSortOrder ) ? ' DESC' : ' ASC';
			switch( $strSortBy ) {
				case 'name':
					$strOrderClause = ' ORDER BY cr.name' . $strSortDirection;
					break;

				case 'task_id':
					$strOrderClause = ' ORDER BY task_id' . $strSortDirection;
					break;

				case 'task_priority_id':
					$strOrderClause = ' ORDER BY task_priority_id' . $strSortDirection;
					break;

				case 'preferred_name':
					$strOrderClause = ' ORDER BY preferred_name' . $strSortDirection;
					break;

				case 'status':
					$strOrderClause = ' ORDER BY status' . $strSortDirection;
					break;

				case 'updated_on':
					$strOrderClause = ' ORDER BY updated_on' . $strSortDirection;
					break;

				case 'created_on':
				default:
					$strOrderClause = ' ORDER BY tdr.created_on desc';
			}
		}

		$strSql = '	SELECT
						DISTINCT( cr.id ) AS report_id,
						cr.name,
						cr.url,
						t.task_priority_id,
						tp.name AS priority,
						e.preferred_name,
						t.id AS task_id,
						ts.name AS status,
						tdr.updated_on,
						tdr.created_on
					FROM
						task_data_requests AS tdr
						JOIN company_reports cr ON ( tdr.company_report_id = cr.id )
						JOIN tasks t ON ( t.id = tdr.task_id )
						JOIN users u ON ( u.id = t.user_id)
						JOIN employees e ON ( e.id = u.employee_id)
						JOIN task_priorities tp ON ( tp.id = t.task_priority_id)
						JOIN task_statuses ts ON ( ts.id = t.task_status_id)
						JOIN report_permissions rp ON ( rp.company_report_id = cr.id)
					WHERE cr.deleted_by IS NULL AND t.task_status_id NOT IN ( ' . CTaskStatus::APPROVED . ' , ' . CTaskStatus:: CANCELLED . ' ) AND rp.is_primary_owner= true AND rp.employee_id =' . ( int ) $intEmployeeId . $strOrderClause . $strLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDataRequestHistoryByEmployeeId( $intEmployeeId, $strDataRequestHistorySearch, $intLimit = 0, $intOffsetValue, $boolDataAnalyst = false, $objDatabase ) {

		$strSubSql			 = '';
		$strJoinClause		 = '';
		$strSelectClause	 = '';

		if( false == is_numeric( $intEmployeeId ) ) {
			return false;
		}

		if( true == valStr( $strDataRequestHistorySearch ) ) {
			$strSubSql = ' AND ( cr.name ILIKE \'%' . trim( $strDataRequestHistorySearch ) . '%\' OR e.preferred_name ILIKE \'%' . trim( $strDataRequestHistorySearch ) . '%\' OR to_char( t.id, \'99999999\' ) LIKE \'%' . trim( $strDataRequestHistorySearch ) . '%\' ';

			if( true == $boolDataAnalyst ) {
				$strSubSql .= ' OR e1.preferred_name ILIKE \'%' . trim( $strDataRequestHistorySearch ) . '%\' )';
			} else {
				$strSubSql .= ')';
			}
		}

		if( false == $boolDataAnalyst ) {
			$strSubSql .= ' AND rp.employee_id = ' . $intEmployeeId;
		}

		$strSql = '	SELECT
						DISTINCT( cr.id ) AS report_id,
						cr.name,
						t.task_priority_id,
						tp.name AS priority,
						e.preferred_name as completed_by,
						t.id AS task_id,
						ts.name AS status,
						tdr.updated_on,
						tdr.created_on,
						t.deleted_by,
						( SELECT created_on FROM task_assignments ta
							WHERE task_id = tdr.task_id AND task_status_id IN( ' . CTaskStatus::APPROVED . ' , ' . CTaskStatus:: CANCELLED . ' ) Order By created_on limit 1 ) AS completed_on,
						e1.preferred_name as primary_owner
					FROM
						task_data_requests AS tdr
						JOIN company_reports cr ON ( tdr.company_report_id = cr.id )
						JOIN tasks t ON ( t.id = tdr.task_id )
						JOIN users u ON ( u.id = t.updated_by)
						JOIN employees e ON ( e.id = u.employee_id)
						LEFT JOIN users u1 ON ( u1.id = tdr.created_by)
						LEFT JOIN employees e1 ON ( e1.id = u1.employee_id)
						JOIN task_priorities tp ON ( tp.id = t.task_priority_id)
						JOIN task_statuses ts ON ( ts.id = t.task_status_id)
						JOIN report_permissions rp ON ( rp.company_report_id = cr.id)
					WHERE t.task_status_id IN ( ' . CTaskStatus::APPROVED . ' , ' . CTaskStatus:: CANCELLED . ' ) ' . $strSubSql;

		if( true == is_numeric( $intOffsetValue ) && true == is_numeric( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffsetValue;
		}

		return fetchData( $strSql, $objDatabase );

	}

}
?>