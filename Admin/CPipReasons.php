<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPipReasons
 * Do not add any new functions to this class.
 */

class CPipReasons extends CBasePipReasons {

	public static function fetchActivePipReasonsByCountryCode( $strCountryCode, $objDatabase, $intDepartmentId = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strWhereCondition = ' AND department_id IS NULL';

		if( CDepartment::CALL_CENTER == $intDepartmentId ) {
			$strWhereCondition = ' AND department_id = ' . ( int ) $intDepartmentId;
		}

		$strSql = 'SELECT
						*
					FROM
						pip_reasons
					WHERE
						is_published = TRUE
						AND country_code = \'' . $strCountryCode . '\'
						' . $strWhereCondition . '
					ORDER BY
						name';

		return CPipReasons::fetchPipReasons( $strSql, $objDatabase );
	}

}
?>