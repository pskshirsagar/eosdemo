<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CRevenueTypes
 * Do not add any new functions to this class.
 */

class CRevenueTypes extends CBaseRevenueTypes {

	public static function fetchRevenueTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CRevenueType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchRevenueType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CRevenueType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedRevenueTypes( $objDatabase ) {
		return self::fetchRevenueTypes( 'SELECT * FROM revenue_types WHERE is_published=1', $objDatabase );
	}
}
?>