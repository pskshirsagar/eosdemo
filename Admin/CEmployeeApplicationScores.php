<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationScores
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationScores extends CBaseEmployeeApplicationScores {

	public static function fetchEmployeeApplicationLatestUpdatedScoreByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase, $boolCheckSubmissionDate = false ) {
		if ( false == is_numeric( $intEmployeeApplicationId ) ) return NULL;

		$strWhere = '';

		if( true == $boolCheckSubmissionDate ) $strWhere = ' submitted_on > now () - INTERVAL \'6 months\'';

		$strSql = 'SELECT
						*
					FROM
						employee_application_scores
					WHERE
						employee_application_id = ' . ( int ) $intEmployeeApplicationId . '
						AND employee_application_score_type_id IN ( ' . CEmployeeApplicationScoreType::MRAB . ',' . CemployeeApplicationscoreType::CCAT . ' )
					ORDER BY id DESC LIMIT 1';

		return self::fetchEmployeeApplicationScore( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeeApplicationScoresByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		if ( false == is_numeric( $intEmployeeApplicationId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						employee_application_scores
					WHERE
						employee_application_id = ' . ( int ) $intEmployeeApplicationId . '
						AND employee_application_score_type_id IN ( ' . CEmployeeApplicationScoreType::MRAB . ',' . CemployeeApplicationscoreType::CCAT . ' )
					ORDER BY
						id DESC';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeApplicationScoresByEmployeeApplicationIds( $arrintEmployeeApplicationIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeApplicationIds ) ) return NULL;

		$strSql = 'SELECT * FROM employee_application_scores WHERE employee_application_id IN ( ' . implode( ',', $arrintEmployeeApplicationIds ) . ')';

		return self::fetchEmployeeApplicationScores( $strSql, $objDatabase );
	}

}
?>