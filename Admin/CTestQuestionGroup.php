<?php
class CTestQuestionGroup extends CBaseTestQuestionGroup {

	const SECURITY			= 20;
	const PHP				= 1;
	const PHP_JEDI			= 76;
	const PHP_NINJA			= 77;
	const PHP_SAMURAI		= 78;
	const XMT				= 80;
	const ASSOCIATE_PGSQL	= 81;

	public static $c_arrstrPhpTestGroups = [
		self::PHP_JEDI		=> self::PHP_JEDI,
		self::PHP_NINJA		=> self::PHP_NINJA,
		self::PHP_SAMURAI	=> self::PHP_SAMURAI
	];

	public static $c_arrstrQaTestGroups = [
		self::XMT	=> self::XMT
	];

	public static $c_arrstrDbaTestGroups = [
		self::ASSOCIATE_PGSQL	=> self::ASSOCIATE_PGSQL
	];

}
?>