<?php

class CTransportEnrollment extends CBaseTransportEnrollment {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransportRouteId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intTransportRouteId ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transport_route_id', ' Route is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valTransportPickupPointId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intTransportPickupPointId ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transport_pickup_point_id', ' Pickup Point is required.' ) );
		}
		return $boolIsValid;
	}

	public function valTransportRequestStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProposedTransportRouteId() {
		$boolIsValid = true;
		if( false == valId( $this->m_intProposedTransportRouteId ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'proposed_transport_route_id', 'New Route is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valProposedTransportPickupPointId() {
		$boolIsValid = true;
		if( false == valId( $this->m_intProposedTransportPickupPointId ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'proposed_transport_pickup_point_id', 'New Pickup Point is required.' ) );
		}
		return $boolIsValid;
	}

	public function valIsPolicyAgreedByEmployee() {
		$boolIsValid = true;
		if( false == $this->m_boolIsPolicyAgreedByEmployee ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_policy_agreed_by_employee', ' Please agree to the terms and conditions. ' ) );
		}
		return $boolIsValid;
	}

	public function valRemark() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;

		if( false == isset( $this->m_strStartDate ) || false == CValidation::validateDate( $this->m_strStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Effective date is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valEndDate( $intDaysDifference ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strEndDate ) || false == CValidation::validateDate( $this->m_strEndDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Last date is required. ' ) );
		} elseif( strtotime( $this->m_strEndDate ) < strtotime( $this->m_strStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End Date should be greater than Start Date. ' ) );
		} elseif( $intDaysDifference > date_diff( date_create( $this->m_strStartDate ), date_create( $this->m_strEndDate ) )->format( '%a' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End Date should be greater than by 30 days from start date. ' ) );
		}
		return $boolIsValid;
	}

	public function valProposedStartDate() {
		$boolIsValid = true;
		if( false == isset( $this->m_strProposedStartDate ) || false == CValidation::validateDate( $this->m_strProposedStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'proposed_start_date', 'Effective date is required. ' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $intDaysDifference = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valTransportRouteId();
				$boolIsValid &= $this->valTransportPickupPointId();
				$boolIsValid &= $this->valIsPolicyAgreedByEmployee();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valProposedStartDate();
				$boolIsValid &= $this->valProposedTransportRouteId();
				$boolIsValid &= $this->valProposedTransportPickupPointId();
				break;

			case 'validate_discontinuation':
				$boolIsValid &= $this->valEndDate( $intDaysDifference );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>