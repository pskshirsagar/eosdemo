<?php

class CClientTerminationForecast extends CBaseClientTerminationForecast {

	public function valCid() {
		$boolIsValid = true;
		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTerminationReasonId() {
		$boolIsValid = true;
		if( true == is_null( $this->getTerminationReasonId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_reason_id', 'Termination reason is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompetitorId() {
		$boolIsValid = true;
		if( true == is_null( $this->getCompetitorId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'competitor_id', 'Competitor is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTerminatedPropertyCountPerMonth() {
		$boolIsValid = true;
		if( true == is_null( $this->getTerminatedPropertyCountPerMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'terminated_property_count_per_month', 'Terminated property count per month is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTerminationNote() {
		$boolIsValid = true;
		if( true == is_null( $this->getTerminationNote() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_note', 'Termination description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTerminationStartDate() {
		$boolIsValid = true;
		if( true == is_null( $this->getTerminationStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_start_date', 'termination start date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTerminationEndDate() {
		$boolIsValid = true;
		if( true == is_null( $this->getTerminationEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_end_date', 'termination end date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTerminationStartDateAndEndDate() {

		$boolIsValid = true;
		if( strtotime( $this->getTerminationEndDate() ) < strtotime( $this->getTerminationStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_end_date', 'End date should be less than start date.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			case VALIDATE_CLIENT_TERMINATE_FORECASTS_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valTerminationReasonId();
				$boolIsValid &= $this->valCompetitorId();
				$boolIsValid &= $this->valTerminatedPropertyCountPerMonth();
				$boolIsValid &= $this->valTerminationStartDate();
				$boolIsValid &= $this->valTerminationEndDate();

				if( true == $boolIsValid || ( false == is_null( $this->valTerminationStartDate() ) && false == is_null( $this->valTerminationEndDate() ) ) ) {
				$boolIsValid &= $this->valTerminationStartDateAndEndDate();
				}
				$boolIsValid &= $this->valTerminationNote();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>