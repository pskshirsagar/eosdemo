<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskQaApprovals
 * Do not add any new functions to this class.
 */

class CTaskQaApprovals extends CBaseTaskQaApprovals {

	public static function fetchTaskQaApprovalsByDateRangeByQaApprovedTasks( $strFromDate, $strToDate, $boolIsShowAllDeployedTasks, $objAdminDatabase, $intPageNo = NULL, $intPageSize = NULL ) {

		$strPageCondition = '';

		$strToDate = date( 'm/d/Y', strtotime( '+1 day', strtotime( $strToDate ) ) );
		$strCondition = 'AND tqa.deployed_on BETWEEN DATE( \'' . $strFromDate . '\' ) AND DATE( \'' . $strToDate . '\' ) ';

		if( true == $boolIsShowAllDeployedTasks ) {
			$strDateSelection = ' (CASE  WHEN tqa.qa_approved_on IS NULL THEN  to_char( (EXTRACT( EPOCH FROM AGE( CURRENT_TIMESTAMP, tqa.deployed_on )::interval ) || \'second\' )::interval, \'HH24:MI:SS\')
                                    ELSE  to_char( (EXTRACT( EPOCH FROM AGE( tqa.qa_approved_on, tqa.deployed_on )::interval ) || \'second\' )::interval, \'HH24:MI:SS\') end) AS time_to_qa';
		} else {
			$strCondition .= ' AND tqa.qa_approved_on IS NULL';
			$strDateSelection = 'to_char( (EXTRACT( EPOCH FROM AGE( CURRENT_TIMESTAMP, tqa.deployed_on )::interval ) || \'second\' )::interval, \'HH24:MI:SS\') AS time_to_qa';
		}

		$strOrderByCondition = str_replace( '\'HH24:MI:SS\')', '\'HH24:MI:SS\')::char', $strDateSelection );
		$strOrderByCondition = str_replace( 'to_char', '', $strOrderByCondition );
		$strOrderByCondition = str_replace( 'AS time_to_qa', '', $strOrderByCondition );

		if( true != is_null( $intPageNo ) && true != is_null( $intPageSize ) ) {
			$intOffset        = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$strPageCondition = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;
		}

		$strSql = 'SELECT 
						tqa.task_id,
						tqa.non_qa_reason,
						tqa.deployed_on,
						tqa.deployed_by,
						tqa.qa_approved_on,
						tqa.qa_approved_by, '
						. $strDateSelection . '
					FROM
						task_qa_approvals tqa
						JOIN tasks t ON ( t.id = tqa.task_id )
					WHERE
						t.task_type_id IN ( ' . CTaskType::BUG . ', ' . CTaskType::FEATURE . ', ' . CTaskType::DESIGN . ' )
						' . $strCondition . '
					ORDER BY
						' . $strOrderByCondition . ' DESC ' . $strPageCondition;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchQaApprovedDeployedTaskByTaskId( $intTaskId, $objAdminDatabase ) {

		$strSql = 'SELECT deployed_on, deployed_by FROM task_qa_approvals WHERE task_id = ' . ( int ) $intTaskId . ' AND qa_approved_by IS NOT NULL';

		return fetchData( $strSql, $objAdminDatabase );
	}

}
?>