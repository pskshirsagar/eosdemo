<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActionResults
 * Do not add any new functions to this class.
 */

class CActionResults extends CBaseActionResults {

	public static function fetchActionResults( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CActionResult', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchActionResult( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CActionResult', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllActionResults( $objDatabase ) {
		$strSql = 'SELECT * FROM action_results ORDER BY order_num';
		return self::fetchCachedObjects( $strSql, 'CActionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchActionResultsByIds( $arrintActionResultIds, $objDatabase ) {
		if( false == valArr( $arrintActionResultIds ) ) return NULL;

			$strSql = ' SELECT
						*
					FROM
						action_results
					WHERE
						id IN ( ' . implode( ',', $arrintActionResultIds ) . ' ) ORDER BY name ASC';

		return self::fetchActionResults( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeesActionResults( $objDatabase ) {
		$strSql = 'SELECT
						ar.*,atr.action_type_id
					FROM
						action_results ar
						JOIN action_type_results atr ON ( atr.action_result_id = ar.id )
					 WHERE
						atr.action_type_id = ' . CActionType::EMPLOYEE . 'AND ar.id NOT IN ( ' . CActionResult::LIKE . ', ' . CActionResult::ATTENDANCE_REMARK . ' )';

		return self::fetchActionResults( $strSql, $objDatabase );

	}

	public static function fetchAdoptionActivityStatus( $objDatabase ) {
		$strSql = '	SELECT
						id,
						name
					FROM
						action_results
					WHERE
						is_published = 0
					ORDER BY
						name DESC';

		return self::fetchObjects( $strSql, 'CActionResult', $objDatabase );

	}

	public static function fetchActionResultsByActionTypeIds( $arrintActionTypeIds, $objDatabase ) {

		if( false == valArr( $arrintActionTypeIds ) ) return NULL;
		$strSql = 'SELECT DISTINCT ON( ar.id )
						ar.id,
						ar.name
					FROM
						action_results ar
						JOIN action_type_results atr on atr.action_result_id = ar.id
					WHERE
						ar.is_published = 1
						AND
						atr.action_type_id IN ( ' . implode( ',', $arrintActionTypeIds ) . ' )';

		return self::fetchActionResults( $strSql, $objDatabase );
	}

}
?>