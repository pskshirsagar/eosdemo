<?php

class CProductConfigurationType extends CBaseProductConfigurationType {

	const PPC_BUDGET				= 1;
	const PERCENTAGE_GOOGLE			= 2;
	const PERCENTAGE_BING 			= 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>