<?php

class CGroupType extends CBaseGroupType {

	const PERMISSIONS 		= 1;
	const VACATION_REQUESTS = 2;
	const MONITORING 		= 3;
	const TRAINING			= 4;
	const SVN				= 5;
	const ECS				= 6;
	const HELPDESK			= 7;
	const REPORT			= 8;
	const LMS_LEARNING_CENTER = 9;

}
?>