<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTvSlides
 * Do not add any new functions to this class.
 */

class CTvSlides extends CBaseTvSlides {

	public static function fetchPublishedTvSlide( $intOffSet = 0, $objDatabase, $intTemplateTypeId = NULL ) {

		$strLimit = ' LIMIT 1 ';

		if( false == empty( $intOffSet ) ) {
			$strLimit = ' LIMIT 1 OFFSET ' . ( int ) $intOffSet;
		}

		$strWhereCondition = ( false == empty( $intTemplateTypeId ) ) ? ' template_type_id = ' . ( int ) $intTemplateTypeId . ' AND ' : ' template_type_id NOT IN ( ' . implode( ' ,', CTvSlide::$c_arrintTemplateIds ) . ' ) AND ';

		$strSql = 'SELECT
						ts.id,
						ts.title,
						ts.template_type_id,
						ts.custom_text,
						ts.custom_image_name,
						ts.order_num,
						ts.is_custom,
						ts.start_datetime,
						ts.end_datetime,
						ts.target_goal
					FROM
						tv_slides ts
					WHERE
						' . $strWhereCondition . '
						is_published = 1
						AND deleted_on IS NULL
					ORDER BY
						order_num ASC ' . ( string ) $strLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllTvSlides( $boolShowOnlyQuarterOfEmployees, $boolShowImplementationSlides, $objDatabase ) {
		$strWhereCondition	= ( true == $boolShowOnlyQuarterOfEmployees ) ? ' AND template_type_id =' . CTvSlide::TEMPLATE_QUARTER : ' AND template_type_id <> ' . CTvSlide::TEMPLATE_QUARTER;
		$strWhereCondition	.= ( true == $boolShowImplementationSlides ) ? ' AND template_type_id =' . CTvSlide::TEMPLATE_IMPLEMENTATION_IMAGE : ' AND template_type_id <> ' . CTvSlide::TEMPLATE_IMPLEMENTATION_IMAGE;

		$strSql = ' SELECT
						 *
					FROM
						tv_slides
					WHERE
						deleted_on IS NULL
						' . $strWhereCondition . '
					ORDER BY
						order_num ASC';

		return parent::fetchTvSlides( $strSql, $objDatabase );
	}

	public static function fetchTvSlidesByIds( $arrintTvSlidesIds, $objDatabase ) {

		if( false == valArr( $arrintTvSlidesIds ) ) {
			return 0;
		}

		$strSql = 'SELECT
						 *
					FROM
						tv_slides
					WHERE
						id IN ( ' . implode( ',', $arrintTvSlidesIds ) . ' )
						AND deleted_on IS NULL';

		return self::fetchTvSlides( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedTvSlidesCount( $objDatabase, $intTemplateTypeId = NULL ) {

		$strWhereCondition = ( false == empty( $intTemplateTypeId ) ) ? ' template_type_id = ' . ( int ) $intTemplateTypeId . ' AND ' : ' template_type_id NOT IN ( ' . implode( ' ,', CTvSlide::$c_arrintTemplateIds ) . ' ) AND ';

		$strSql = 'SELECT
						 COUNT(*)
					FROM
						tv_slides ts
					WHERE
						' . $strWhereCondition . '
						is_published = 1
						AND deleted_on IS NULL
						AND deleted_by IS NULL';

		$arrintTvSlidesCount = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintTvSlidesCount[0]['count'] ) ) {
			return $arrintTvSlidesCount[0]['count'];
		}
		return 0;
	}

	public static function fetchAllTvSlidesByTemplateTypeId( $intTemplateTypeId, $objDatabase, $boolIsFetchData = false ) {
		$strWhereCondition = ( false == empty( $intTemplateTypeId ) ) ? ' AND ts.template_type_id = ' . ( int ) $intTemplateTypeId : ' AND template_type_id NOT IN ( ' . implode( ' ,', CTvSlide::$c_arrintTemplateIds ) . ' ) ';

		$strSql = ' SELECT
						ts.id,
						ts.title,
						ts.template_type_id,
						ts.custom_image_name,
						ts.order_num 
					FROM
						tv_slides AS ts
					WHERE
						ts.deleted_on IS NULL
						' . $strWhereCondition . '
					ORDER BY
						order_num ASC';

		return ( true == $boolIsFetchData ) ? fetchData( $strSql, $objDatabase ) : parent::fetchTvSlides( $strSql, $objDatabase );
	}

}
?>