<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingSessionNotes
 * Do not add any new functions to this class.
 */

class CTrainingSessionNotes extends CBaseTrainingSessionNotes {

	public static function fetchTrainingSessionNotesBySessionId( $intTrainingSessionId, $objDatabase ) {
		$strSql = 'SELECT
						tsn.*,
						e.preferred_name AS name_full
					FROM
						training_session_notes AS tsn
						JOIN users AS u ON tsn.created_by = u.id
						JOIN employees AS e ON u.employee_id = e.id
					WHERE
						tsn.training_session_id = ' . ( int ) $intTrainingSessionId;

			return self::fetchTrainingSessionNotes( $strSql, $objDatabase );
	}

}
?>