<?php

class CEntrataReportResponse extends CBaseEntrataReportResponse {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultReportVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportInstanceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportHistoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataReportResponseTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMajor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportInstanceName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRespondedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>