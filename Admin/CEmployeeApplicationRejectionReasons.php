<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationRejectionReasons
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationRejectionReasons extends CBaseEmployeeApplicationRejectionReasons {

	public static function fetchEmployeeApplicationRejectionReasonsByCountryCode( $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						employee_application_rejection_reasons
					WHERE
						is_published = true
						AND country_code = \'' . $strCountryCode . '\'
					ORDER BY
						name,
						order_num';

		return parent::fetchEmployeeApplicationRejectionReasons( $strSql, $objDatabase );
	}

}
?>