<?php

class CEmployeeCard extends CBaseEmployeeCard {

	public function valCardNumber( $objDatabase, $intEmployeeApplicationTypeId = NULL ) {
		$boolValid = true;

		if( 0 == \Psi\CStringService::singleton()->strlen( trim( $this->getCardNumber() ) && 0 < $this->getCardNumber() ) && $intEmployeeApplicationTypeId != CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_CV_REPROCESSED ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'card_number', 'Access card number is required.' ) );
			return $boolValid;
		}

		if( true == isset( $objDatabase ) ) {

			$objConflictingEmployeeCard = \Psi\Eos\Admin\CEmployeeCards::createService()->fetchConflictingEmployeeCard( $this, $objDatabase );

			if( true == valObj( $objConflictingEmployeeCard, 'CEmployeeCard' ) ) {
				if( $objConflictingEmployeeCard->getCardNumber() == $this->getCardNumber() ) {
					$boolValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'card_number', 'Card number already in use.' ) );
					$this->setCardNumber( 0 );
					return $boolValid;
				}
			}
		}

		return $boolValid;
	}

	public function validate( $strAction, $objDatabase, $intEmployeeApplicationTypeId = NULL ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolValid &= $this->valCardNumber( $objDatabase, $intEmployeeApplicationTypeId );
				break;

			case VALIDATE_DELETE:
	 			break;

	 		default:
	 		   	// default case
	 			$boolValid = true;
	 		   	break;
		}

		return $boolValid;
	}

}
?>