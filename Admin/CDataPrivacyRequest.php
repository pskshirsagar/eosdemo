<?php

class CDataPrivacyRequest extends CBaseDataPrivacyRequest {

	protected $m_strClientName;
	protected $m_strPropertyName;

	protected $m_intNotesCount;
	protected $m_intTotalCount;
	protected $m_intDetailsCount;
	protected $m_intCompletedByCount;
	protected $m_intPropertyCount;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerDataRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyEmployeeName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDataPrivacyRequestType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReminderEmailSentOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function getCompletedByCount() {
		return $this->m_intCompletedByCount;
	}

	public function setCompletedByCount( $intCompletedByCount ) {
		$this->m_intCompletedByCount = $intCompletedByCount;
	}

	public function getTotalCount() {
		return $this->m_intTotalCount;
	}

	public function setTotalCount( $intTotalCount ) {
		$this->m_intTotalCount = $intTotalCount;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function getNotesCount() {
		return $this->m_intNotesCount;
	}

	public function setNotesCount( $intNotesCount ) {
		$this->m_intNotesCount = $intNotesCount;
	}

	public function getDetailsCount() {
		return $this->m_intDetailsCount;
	}

	public function setDetailsCount( $intDetailsCount ) {
		$this->m_intDetailsCount = $intDetailsCount;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['client_name'] ) )					$this->setClientName( $arrmixValues['client_name'] );
		if( true == isset( $arrmixValues['total_count'] ) )					$this->setTotalCount( $arrmixValues['total_count'] );
		if( true == isset( $arrmixValues['completed_by_count'] ) )			$this->setCompletedByCount( $arrmixValues['completed_by_count'] );
		if( true == isset( $arrmixValues['property_name'] ) )				$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['property_count'] ) )				$this->setPropertyCount( $arrmixValues['property_count'] );
		if( true == isset( $arrmixValues['notes_count'] ) )					$this->setNotesCount( $arrmixValues['notes_count'] );
		if( true == isset( $arrmixValues['details_count'] ) )				$this->setDetailsCount( $arrmixValues['details_count'] );
		return;
	}


}
?>