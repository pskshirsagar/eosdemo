<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserRoles
 * Do not add any new functions to this class.
 */

class CUserRoles extends CBaseUserRoles {

 	public static function fetchUserRolesByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserRoles( sprintf( 'SELECT * FROM user_roles WHERE deleted_on IS NULL AND user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchUserRolesByRoleId( $intRoleId, $objDatabase ) {
		return self::fetchUserRoles( sprintf( ' SELECT * FROM user_roles WHERE deleted_on IS NULL AND role_id = %d', ( int ) $intRoleId ), $objDatabase );
	}

	public static function fetchUserRolesByUserAccess( $intUserId, $intIsSuperUser, $intIsAdministrator, $objDatabase ) {

		$strSql = '';

		if( 0 == $intIsSuperUser ) {

			$strSql .= 'SELECT
							DISTINCT role_id
						FROM user_roles
						WHERE
							deleted_on IS NULL
							AND user_id = ' . ( int ) $intUserId;
		}

		if( 1 == $intIsSuperUser || 1 == $intIsAdministrator ) {

			if( false == empty( $strSql ) ) {
				$strSql .= ' UNION ';
			}

			$strSql .= 'SELECT
								id as role_id
							FROM roles ';

			if( 0 == $intIsSuperUser ) {
				$strSql .= ' WHERE
								allow_super_user_only = 0';
			}
		}

		return self::fetchUserRoles( $strSql, $objDatabase );
	}

	public static function fetchUserRoleByUserIdByRoleId( $intUserId, $intRoleId, $objDatabase ) {

		$strSql = 'SELECT
						*
 					FROM
						user_roles
					WHERE
						deleted_on IS NULL AND
						deleted_by IS NULL AND
						user_id = ' . ( int ) $intUserId . ' AND
						role_id = ' . ( int ) $intRoleId;

		return self::fetchUserRole( $strSql, $objDatabase );
	}

	public static function fetchUserRolesByRoleIdByUserIds( $intRoleId, $arrintUserIds, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) ) return NULL;

		$strSql = 'SELECT
						*
 					FROM
						user_roles
					WHERE
						deleted_on IS NULL AND
						deleted_by IS NULL AND
						user_id IN ( ' . implode( ',', $arrintUserIds ) . ' ) AND
						role_id = ' . ( int ) $intRoleId;

		return self::fetchUserRoles( $strSql, $objDatabase );

	}

	public static function fetchDeletedAssignedUsersByRoleId( $intRoleId, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( u.username ) u.username,
						ur.id,
						ur.role_id,
						u.id AS user_id,
						ur.deleted_by,
						ur.deleted_on
					FROM
						user_roles ur
						JOIN users u ON ( u.id = ur.user_id )
					WHERE
						ur.role_id = ' . ( int ) $intRoleId . '
						AND ur.deleted_by IS NOT NULL
						AND u.is_disabled = 0
					ORDER BY
						u.username';

		return parent::fetchUserRoles( $strSql, $objDatabase );
	}

	public static function fetchUserRolesRightsByUserId( $intUserId, $objDatabase ) {

		if( false == is_numeric( $intUserId ) ) return NULL;

		$strSql = 'SELECT
						ur.*,
						r.name,
						r.description,
						r.is_published
 					FROM
						user_roles ur
						LEFT JOIN roles r ON ( ur.role_id = r.id )
					WHERE
						ur.deleted_on IS NULL AND
						ur.deleted_by IS NULL AND
						ur.user_id = ' . ( int ) $intUserId . '  AND
						ur.is_managerial = true ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUserRoleDataByUserId( $intUserId, $objDatabase ) {
		if( false == is_numeric( $intUserId ) ) return NULL;
		$strSql = 'SELECT
						ur.*
 					FROM
						user_roles ur
					WHERE
						ur.deleted_on IS NULL 
						AND ur.deleted_by IS NULL 
						AND ur.user_id = ' . ( int ) $intUserId . '  
						AND ur.is_managerial = true ';
		return self::fetchUserRoles( $strSql, $objDatabase );
	}

	public static function fetchUserRoleByRoleIdByUserId( $intRoleId, $intUserId, $objDatabase ) {

		$strSql = 'SELECT
						id
 					FROM
						user_roles
					WHERE
						deleted_on IS NULL AND
						deleted_by IS NULL AND
						user_id = ' . ( int ) $intUserId . ' AND
						role_id = ' . ( int ) $intRoleId;
		$arrintData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintData ) ) ? current( $arrintData )['id'] : [];
	}

}
?>