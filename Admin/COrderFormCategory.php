<?php

class COrderFormCategory extends CBaseOrderFormCategory {

	const AGREEMENT_SERVICES		= 1;
	const CREATIVE_SERVICES			= 2;
	const MERCHANT_SERVICES			= 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>