<?php

class CTaskQaApproval extends CBaseTaskQaApproval {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeploymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRevisionNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNonQaReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeployedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeployedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQaApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQaApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>