<?php

class CSemStatusType extends CBaseSemStatusType {

	const ENABLED				= 1;
	const PAUSED				= 2;
	const DELETED				= 3;
	const ACTIVE				= 4; // This one is same as of enabled. We use it for keywords status.
	const DISABLED				= 5; // This status is required to delete Ads.

	public static $c_arrmixSemStatusTypeIdToStr = array(
		self::ENABLED => 'ENABLED',
		self::PAUSED => 'PAUSED',
		self::DELETED => 'DELETED',
		self::ACTIVE => 'ACTIVE',
		self::DISABLED => 'DISABLED'
	);
}
?>