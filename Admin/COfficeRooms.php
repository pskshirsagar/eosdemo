<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COfficeRooms
 * Do not add any new functions to this class.
 */

class COfficeRooms extends CBaseOfficeRooms {

	public static function fetchAllOfficeRoomsByCountryCode( $strCountryCode, $objDatabase ) {

		if( false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ofr.*
					FROM
						office_rooms ofr
						JOIN offices o ON (o.id = ofr.office_id)
					WHERE
						ofr.deleted_by IS NULL AND
						o.country_code = \'' . $strCountryCode . '\' ORDER BY ofr.name';

		return self::fetchOfficeRooms( $strSql, $objDatabase );
	}

	public static function fetchOfficeRoomByIdWithOffice( $intOfficeRoomId, $objDatabase ) {

		if( true == is_null( $intOfficeRoomId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ofr.*
					FROM
					    office_rooms ofr
					WHERE
						ofr.deleted_by IS NULL AND
						ofr.id = ' . ( int ) $intOfficeRoomId . '
					ORDER BY ofr.name';

		return self::fetchOfficeRoom( $strSql, $objDatabase );
	}

}

?>