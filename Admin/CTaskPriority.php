<?php

class CTaskPriority extends CBaseTaskPriority {

	const LOW 			= 1;
	const NORMAL 		= 2;
	const HIGH 			= 3;
	const URGENT 		= 4;
	const IMMEDIATE 	= 5;

	protected $m_arrobjTasks;

	/**
	 * Add Functions
	 */

	public function addTask( $objTask ) {
		$this->m_arrobjTasks[$objTask->getId()] = $objTask;
	}

	/**
	 * Get Functions
	 */

	public function getTasks() {
		return $this->m_arrobjTasks;
	}

	/**
	 * Set Functions
	 */

	public function setTasks( $arrobjTasks ) {
		$this->m_arrobjTasks = $arrobjTasks;
	}

	/**
	 * If you are making any changes in populateSmartyConstants function then
	 * please make sure the same changes would be applied to populateTemplateConstants function also.
	 */

	public static $c_arrintTemplateConstants = array(
		'TASK_PRIORITY_LOW'			=> self::LOW,
		'TASK_PRIORITY_NORMAL'		=> self::NORMAL,
		'TASK_PRIORITY_HIGH'		=> self::HIGH,
		'TASK_PRIORITY_URGENT'		=> self::URGENT,
		'TASK_PRIORITY_IMMEDIATE'	=> self::IMMEDIATE,
	);

	public static $c_arrstrTaskPriorityName = array(
		self::LOW		=> 'Low',
		self::NORMAL	=> 'Normal',
		self::HIGH		=> 'High',
		self::URGENT	=> 'Urgent',
		self::IMMEDIATE	=> 'Immediate'
	);

	public static $c_arrintHigherTaskPriorities = array(
		'TASK_PRIORITY_HIGH'		=> self::HIGH,
		'TASK_PRIORITY_URGENT'		=> self::URGENT,
		'TASK_PRIORITY_IMMEDIATE'	=> self::IMMEDIATE,
	);

	public static $c_arrmixSlaGoal = [
		'Low'       => '2 Days',
		'Normal'    => '1 Day',
		'High'      => '6 hours',
		'Urgent'    => '3 hour',
		'Immediate' => '1 Hour'
	];
}
?>