<?php

class CEmployeeAttendanceRemark extends CBaseEmployeeAttendanceRemark {

	public function valNote() {
		$boolIsValid = true;
			if( true == is_null( $this->getNote() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Attendance remark is required.' ) );
			}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNote();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>