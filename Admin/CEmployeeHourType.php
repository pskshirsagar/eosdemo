<?php

class CEmployeeHourType extends CBaseEmployeeHourType {

	const FLOOR			= 1;
	const BREAK_TIME	= 2;
	const GAMING_ROOM	= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * If you are making any changes in loadSmartyConstants function then
	 * please make sure the same changes would be applied to loadTemplateConstants function also.
	 */

	public static function loadSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'EMPLOYEE_HOUR_TYPE_FLOOR', 		self::FLOOR );
		$objSmarty->assign( 'EMPLOYEE_HOUR_TYPE_BREAK_TIME', 	self::BREAK_TIME );
		$objSmarty->assign( 'EMPLOYEE_HOUR_TYPE_GAMING_ROOM', 	self::GAMING_ROOM );
	}

	public static $c_arrintEmployeeHourTypes = array(
		'EMPLOYEE_HOUR_TYPE_FLOOR'			=> self::FLOOR,
		'EMPLOYEE_HOUR_TYPE_BREAK_TIME'		=> self::BREAK_TIME,
		'EMPLOYEE_HOUR_TYPE_GAMING_ROOM'	=> self::GAMING_ROOM
	);
}
?>