<?php

class CReimbursementRequest extends CBaseReimbursementRequest {

	protected $m_strEmployeePayStatusType;
	protected $m_strEndDate;
	protected $m_strDueDate;
	protected $m_strActionApprovedOn;
	protected $m_strRequestedEmployeeName;

	protected $m_boolIsRequestedEmployeeTerminated;
	protected $m_boolIsRequestEditable;

	/**
	 * Get Functions
	 *
	 */

	public function getEmployeePayStatusType() {
		return $this->m_strEmployeePayStatusType;
	}

	public function getPayrollPeriodEndDate() {
		return $this->m_strEndDate;
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function getTotalAmount() {
		if( false == valStr( $this->getTotalAmountEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getTotalAmountEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getActionApprovedOn() {
		return $this->m_strActionApprovedOn;
	}

	public function getRequestedEmployeeName() {
		return $this->m_strRequestedEmployeeName;
	}

	public function getIsRequestedEmployeeTerminated() {
		return $this->m_boolIsRequestedEmployeeTerminated;
	}

	public function getIsRequestEditable() {
		return $this->m_boolIsRequestEditable;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setEmployeePayStatusType( $strEmployeePayStatusType ) {
		$this->m_strEmployeePayStatusType = $strEmployeePayStatusType;
	}

	public function setPayrollPeriodEndDate( $strEndDate ) {
		$this->m_strEndDate = $strEndDate;
	}

	public function setTotalAmount( $fltTotalAmount ) {
		if( true == valStr( $fltTotalAmount ) ) {
			$this->setTotalAmountEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $fltTotalAmount, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		}
	}

	public function setDueDate( $strDueDate ) {
		$this->m_strDueDate = $strDueDate;
	}

	public function setActionApprovedOn( $strActionApprovedOn ) {
		return $this->m_strActionApprovedOn = $strActionApprovedOn;
	}

	public function setRequestedEmployeeName( $strPreferredName ) {
		$this->m_strRequestedEmployeeName = CStrings::strTrimDef( $strPreferredName, 100, NULL, true );
	}

	public function setIsRequestedEmployeeTerminated( $boolIsRequestedEmployeeTerminated ) {
		$this->m_boolIsRequestedEmployeeTerminated = $boolIsRequestedEmployeeTerminated;
	}

	public function setIsRequestEditable( $boolIsRequestEditable ) {
		$this->m_boolIsRequestEditable = $boolIsRequestEditable;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['employee_pay_status_type'] ) )			$this->setEmployeePayStatusType( $arrmixValues['employee_pay_status_type'] );
		if( true == isset( $arrmixValues['payroll_period_end_date'] ) )				$this->setPayrollPeriodEndDate( $arrmixValues['payroll_period_end_date'] );
		if( true == isset( $arrmixValues['due_date'] ) )							$this->setDueDate( $arrmixValues['due_date'] );
		if( true == isset( $arrmixValues['action_approved_on'] ) )					$this->setActionApprovedOn( $arrmixValues['action_approved_on'] );
		if( true == isset( $arrmixValues['requested_employee_name'] ) )				$this->setRequestedEmployeeName( $arrmixValues['requested_employee_name'] );
		if( true == isset( $arrmixValues['is_requested_employee_terminated'] ) )	$this->setIsRequestedEmployeeTerminated( $arrmixValues['is_requested_employee_terminated'] );
		if( true == isset( $arrmixValues['is_request_editable'] ) )					$this->setIsRequestEditable( $arrmixValues['is_request_editable'] );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valTitle() {

		$boolIsValid = true;

		if( false == valStr( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Request title is required. ' ) );
			return $boolIsValid;
		}

		if( false == preg_match( '/^[a-zA-Z0-9 ,\:\.\-\_\&\$\(\)\/]+$/', $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Invalid title.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTitle();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>