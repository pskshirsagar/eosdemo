<?php

class CEmployeeAssessment extends CBaseEmployeeAssessment {

	protected $m_strEmployeeAssessmentTypeName;
	protected $m_strEmployeeAssessmentTypeDescription;
	protected $m_strEmployeeDueDate;
	protected $m_strManagerDueDate;
	protected $m_strCountryCode;
	protected $m_strEmployeeAssessmentLockedOn;
	protected $m_strNameFull;
	protected $m_strEmailAddress;
	protected $m_strDepartmentName;
	protected $m_strDesignationName;

	protected $m_intUserId;
	protected $m_intResponsesCount;
	protected $m_intEmployeesCount;
	protected $m_intDelayedDays;
	protected $m_intDateStarted;
	protected $m_intManagerEmployeeId;
	protected $m_intFinalizedOn;
	protected $m_intEmployeeReviewCompletedDate;
	protected $m_intHrRepresentativeEmployeeId;

	protected $m_fltEmployeeAverageRating;
	protected $m_fltManagerAverageRating;

	protected $m_boolIsAdmin;
	protected $m_boolIsRevieweeResponded;
	protected $m_boolPeerReview;

	const MAXIMUM_MANAGER_COUNT 	= 3;
	const MAXIMUM_PROJECT_MANAGER 	= 1;
	const ONE_DAY_IN_SECONDS 		= 86400;

	/**
	 * Create Functions
	 */

	public function createEmployeeAssessmentEmployee() {

		$objEmployeeAssessmentEmployee = new CEmployeeAssessmentEmployee();

		$objEmployeeAssessmentEmployee->setEmployeeAssessmentId( $this->getId() );

		return $objEmployeeAssessmentEmployee;
	}

	public function createEmployeeAssessmentRespons() {

		$objEmployeeAssessmentRespons = new CEmployeeAssessmentResponse();

		$objEmployeeAssessmentRespons->setEmployeeAssessmentId( $this->getId() );
		return $objEmployeeAssessmentRespons;
	}

	/**
	 * GET Functions
	 */

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

	public function getEmployeeAssessmentTypeName() {
		return $this->m_strEmployeeAssessmentTypeName;
	}

	public function getEmployeeAssessmentTypeDescription() {
		return $this->m_strEmployeeAssessmentTypeDescription;
	}

	public function getEncryptedRecommendedNewSalary() {
		return ( false == is_null( $this->m_strRecommendedNewSalary ) ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strRecommendedNewSalary, CONFIG_SODIUM_KEY_SALARY_NOTES, [ 'legacy_secret_key' => CConfig::get( 'salary_notes_encryption_key' ) ] ) : NULL;
	}

	public function getEncryptedSalaryNotes() {
		return ( false == is_null( $this->m_strSalaryNotes ) ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strSalaryNotes, CONFIG_SODIUM_KEY_SALARY_NOTES, [ 'legacy_secret_key' => CConfig::get( 'salary_notes_encryption_key' ) ] ) : NULL;
	}

	public function getResponsesCount() {
		return $this->m_intResponsesCount;
	}

	public function getEmployeesCount() {
		return $this->m_intEmployeesCount;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getDelayedDays() {
		return $this->m_intDelayedDays;
	}

	public function getEmployeeAssessmentLockedOn() {
		return $this->m_strEmployeeAssessmentLockedOn;
	}

	public function getEmployeeAssessmentTypeLevelId() {
			return $this->m_intEmployeeAssessmentTypeLevelId;
	}

	public function getIsAdmin() {
		return $this->m_boolIsAdmin;
	}

	public function getDateStarted() {
		return $this->m_intDateStarted;
	}

	public function getManagerEmployeeId() {
		return $this->m_intManagerEmployeeId;
	}

	public function getFinalizedOn() {
		return $this->m_intFinalizedOn;
	}

	public function getEmployeeReviewCompletedDate() {
		return $this->m_intEmployeeReviewCompletedDate;
	}

	public function getIsRevieweeResponded() {
		return $this->m_boolIsRevieweeResponded;
	}

	public function getEmployeeAverageRating() {
		return $this->m_fltEmployeeAverageRating;
	}

	public function getManagerAverageRating() {
		return $this->m_fltManagerAverageRating;
	}

	public function getEmployeeDueDate() {
		return $this->m_strEmployeeDueDate;
	}

	public function getManagerDueDate() {
		return $this->m_strManagerDueDate;
	}

	public function getPeerReview() {
		return $this->m_boolPeerReview;
	}

	public function getHrRepresentativeEmployeeId() {
		return $this->m_intHrRepresentativeEmployeeId;
	}

	/**
	 * SET Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['employee_assessment_type_name'] ) ) 			$this->setEmployeeAssessmentTypeName( $arrmixValues['employee_assessment_type_name'] );
		if( true == isset( $arrmixValues['employee_assessment_type_description'] ) ) 	$this->setEmployeeAssessmentTypeDescription( $arrmixValues['employee_assessment_type_description'] );
		if( true == isset( $arrmixValues['employee_due_date'] ) ) 						$this->setEmployeeDueDate( $arrmixValues['employee_due_date'] );
		if( true == isset( $arrmixValues['manager_due_date'] ) ) 						$this->setManagerDueDate( $arrmixValues['manager_due_date'] );
		if( true == isset( $arrmixValues['employee_assessment_locked_on'] ) ) 			$this->setEmployeeAssessmentLockedOn( $arrmixValues['employee_assessment_locked_on'] );
		if( true == isset( $arrmixValues['employee_assessment_type_level_id'] ) ) 		$this->setEmployeeAssessmentTypeLevelId( $arrmixValues['employee_assessment_type_level_id'] );

		if( true == isset( $arrmixValues['responses_count'] ) ) 						$this->setResponsesCount( $arrmixValues['responses_count'] );
		if( true == isset( $arrmixValues['employees_count'] ) ) 						$this->setEmployeesCount( $arrmixValues['employees_count'] );
		if( true == isset( $arrmixValues['country_code'] ) ) 							$this->setCountryCode( $arrmixValues['country_code'] );
		if( true == isset( $arrmixValues['delayed_days'] ) ) 							$this->setDelayedDays( $arrmixValues['delayed_days'] );
		if( true == isset( $arrmixValues['is_admin'] ) ) 								$this->setIsAdmin( $arrmixValues['is_admin'] );
		if( true == isset( $arrmixValues['date_started'] ) ) 							$this->setDateStarted( $arrmixValues['date_started'] );
		if( true == isset( $arrmixValues['manager_employee_id'] ) ) 					$this->setManagerEmployeeId( $arrmixValues['manager_employee_id'] );
		if( true == isset( $arrmixValues['finalized_on'] ) ) 							$this->setFinalizedOn( $arrmixValues['finalized_on'] );
		if( true == isset( $arrmixValues['is_admin'] ) ) 								$this->setIsAdmin( $arrmixValues['is_admin'] );
		if( true == isset( $arrmixValues['name_full'] ) ) 								$this->setNameFull( $arrmixValues['name_full'] );
		if( true == isset( $arrmixValues['user_id'] ) ) 								$this->setUserId( $arrmixValues['user_id'] );
		if( true == isset( $arrmixValues['email_address'] ) ) 							$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['department_name'] ) ) 						$this->setDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['designation_name'] ) ) 						$this->setDesignationName( $arrmixValues['designation_name'] );
		if( true == isset( $arrmixValues['employee_review_completed_date'] ) ) 			$this->setEmployeeReviewCompletedDate( $arrmixValues['employee_review_completed_date'] );
		if( true == isset( $arrmixValues['is_reviewee_responded'] ) ) 					$this->setIsRevieweeResponded( $arrmixValues['is_reviewee_responded'] );
		if( true == isset( $arrmixValues['employee_average_rating'] ) ) 				$this->setEmployeeAverageRating( $arrmixValues['employee_average_rating'] );
		if( true == isset( $arrmixValues['manager_average_rating'] ) ) 					$this->setManagerAverageRating( $arrmixValues['manager_average_rating'] );
		if( true == isset( $arrmixValues['is_peer_review'] ) ) 			                $this->setPeerReview( $arrmixValues['is_peer_review'] );
		if( true == isset( $arrmixValues['hr_representative_employee_id'] ) ) 			$this->setHrRepresentativeEmployeeId( $arrmixValues['hr_representative_employee_id'] );
		return;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setDesignationName( $strDesignationName ) {
		$this->m_strDesignationName = $strDesignationName;
	}

	public function setResponsesCount( $intResponsesCount ) {
		$this->m_intResponsesCount = $intResponsesCount;
	}

	public function setEmployeesCount( $intEmployeesCount ) {
		$this->m_intEmployeesCount = $intEmployeesCount;
	}

	public function setEmployeeAssessmentTypeName( $strEmployeeAssessmentTypeName ) {
		$this->m_strEmployeeAssessmentTypeName = $strEmployeeAssessmentTypeName;
	}

	public function setEmployeeAssessmentTypeDescription( $strEmployeeAssessmentTypeDescription ) {
		$this->m_strEmployeeAssessmentTypeDescription = $strEmployeeAssessmentTypeDescription;
	}

	public function setEncryptedSalaryNotes( $strSalaryNotes ) {
		$this->m_strSalaryNotes = ( true == valStr( $strSalaryNotes ) ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $strSalaryNotes, 2000, NULL, true ), CONFIG_SODIUM_KEY_SALARY_NOTES ) : NULL;
	}

	public function setEncryptedRecommendedNewSalary( $strRecommendedNewSalary ) {
		$this->m_strRecommendedNewSalary = ( true == valStr( $strRecommendedNewSalary ) ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strToFloatDef( $strRecommendedNewSalary, NULL, false, 4 ), CONFIG_SODIUM_KEY_SALARY_NOTES ) : NULL;
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

 	public function setDelayedDays( $intDelayedDays ) {
		$this->m_intDelayedDays = $intDelayedDays;
 	}

	public function setEmployeeAssessmentLockedOn( $strEmployeeAssessmentLockedOn ) {
		$this->m_strEmployeeAssessmentLockedOn = $strEmployeeAssessmentLockedOn;
	}

	public function setEmployeeAssessmentTypeLevelId( $intEmployeeAssessmentTypeLevelId ) {
		$this->m_intEmployeeAssessmentTypeLevelId = $intEmployeeAssessmentTypeLevelId;
	}

	public function setIsAdmin( $boolIsAdmin ) {
		$this->m_boolIsAdmin = $boolIsAdmin;
	}

	public function setDateStarted( $intDateStarted ) {
		$this->m_intDateStarted = $intDateStarted;
	}

	public function setManagerEmployeeId( $intManagerEmployeeId ) {
		$this->m_intManagerEmployeeId = $intManagerEmployeeId;
	}

	public function setFinalizedOn( $intFinalizedOn ) {
		$this->m_intFinalizedOn = $intFinalizedOn;
	}

	public function setEmployeeReviewCompletedDate( $intEmployeeReviewCompletedDate ) {
		$this->m_intEmployeeReviewCompletedDate = $intEmployeeReviewCompletedDate;
	}

	public function setIsRevieweeResponded( $boolIsRevieweeResponded ) {
		$this->m_boolIsRevieweeResponded = $boolIsRevieweeResponded;
	}

	public function setEmployeeAverageRating( $fltEmployeeAverageRating ) {
		return $this->m_fltEmployeeAverageRating = $fltEmployeeAverageRating;
	}

	public function setManagerAverageRating( $fltManagerAverageRating ) {
		return $this->m_fltManagerAverageRating = $fltManagerAverageRating;
	}

	public function setEmployeeDueDate( $strEmployeeDueDate ) {
		$this->m_strEmployeeDueDate = CStrings::strTrimDef( $strEmployeeDueDate, -1, NULL, true );
	}

	public function setManagerDueDate( $strManagerDueDate ) {
		$this->m_strManagerDueDate = CStrings::strTrimDef( $strManagerDueDate, -1, NULL, true );
	}

	public function setPeerReview( $boolPeerReview ) {
		$this->m_boolPeerReview = $boolPeerReview;
	}

	public function setHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId ) {
		$this->m_intHrRepresentativeEmployeeId = $intHrRepresentativeEmployeeId;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchEmployeeAssessmentQuestions( $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeAssessmentQuestions::createService()->fetchEmployeeAssessmentQuestionsByEmployeeAssessmentId( $this->getId(), $objDatabase );
	}

	public function fetchEmployeeAssessmentResponses( $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeAssessmentResponses::createService()->fetchEmployeeAssessmentResponsesByEmployeeAssessmentEmployeeId( $this->getId(), $objDatabase );
	}

	public function fetchCurrentEmployeeAssessmentEmployeeByEmployeeId( $intEmployeeId, $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeAssessmentEmployees::createService()->fetchCurrentEmployeeAssessmentEmployeeByEmployeeAssessmentIdByEmployeeId( $this->getId(), $intEmployeeId, $objDatabase );
	}

	public function fetchCurrentEmployeeAssessmentEmployeesByEmployeeIds( $arrintManagerIds, $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeAssessmentEmployees::createService()->fetchCurrentEmployeeAssessmentEmployeesByEmployeeAssessmentIdByEmployeeIds( $this->getId(), $arrintManagerIds, $objDatabase );
	}

	public function fetchEmployeeAssessmentEmployees( $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeAssessmentEmployees::createService()->fetchEmployeeAssessmentEmployeesByEmployeeAssessmentId( $this->getId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valEmployeeAssessmentTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmployeeAssessmentTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_assessment_type_id', 'Please select an employee review type.' ) );
		}

		return $boolIsValid;
	}

	public function valPendingEmployeeAssessments( $objDatabase ) {

		$boolIsValid = true;
		$arrobjEmployeeAssessments	= \Psi\Eos\Admin\CEmployeeAssessments::createService()->fetchPendingEmployeeAssessmentsByEmployeeId( $this->getEmployeeId(), $objDatabase );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjEmployeeAssessments ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_assessment_type_id', 'Previous review is pending.' ) );
		}

			return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid	&= $this->valEmployeeAssessmentTypeId();
				$boolIsValid	&= $this->valPendingEmployeeAssessments( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid	&= $this->valEmployeeAssessmentTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>