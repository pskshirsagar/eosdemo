<?php

class CStorageObject extends CBaseStorageObject {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStorageTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getStorageTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'storage_type_id', 'Storage Type is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTeamId() {
		$boolIsValid = true;
		if( true == is_null( $this->getTeamId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'team_id', 'Team Name is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valParentStorageObjectsId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScriptId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFolderName( $boolIsInsert, $objDatabase, $boolIsOneTimeCleanup ) {
		$boolIsValid      = true;

		if( true == is_null( $this->getFolderName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'folder_name', 'Folder Name is required. ' ) );
			return $boolIsValid;
		}

		$objStorageObjects = CStorageObjects::fetchStorageObjectByFolderNameByParentFolderId( $this->getFolderName(), $this->getParentStorageObjectId(), $this->getStorageTypeId(), $objDatabase );
		if( 'false' == $boolIsOneTimeCleanup ) {
			if( false == empty( $objStorageObjects ) && false == empty( $boolIsInsert ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'folder_name', 'Folder name is already in use, please use another name.' ) );
				return $boolIsValid;
			}
		} else {
			if( false == empty( $objStorageObjects ) && false == empty( $boolIsInsert ) ) {
				foreach( $objStorageObjects as $objStorageObject ) {
					if( true == $objStorageObject->getIsOneTimeCleanup() && true == is_null( $objStorageObject->getCleanEndDatetime() ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'folder_name', 'This folders one time cleanup is already running.' ) );

						return $boolIsValid;
					}
				}
			}
		}

		$arrmixStorageTypes        = CStorageType::$c_arrintAllCStorageType;
		if( false == is_null( $this->getParentStorageObjectId() ) ) {
			$objStorageObject = CStorageObjects::fetchStorageObjectById( $this->getParentStorageObjectId(), $objDatabase );

			if( true == valObj( $objStorageObject, 'CStorageObject' ) )
				$strParentFolderPath = $objStorageObject->getFolderPath();

			$strParentFolderPath = str_replace( '/srv/www/vhosts/' . $arrmixStorageTypes[$this->getStorageTypeId()]['name'] . '/', '', $strParentFolderPath );
			$strParentFolderName = $strParentFolderPath;
		}

		// Storage Folder Path
		if( CStoragetype::MOUNTS == $this->getStorageTypeId() ) {
			$strFolderPath  = PATH_MOUNTS . $strParentFolderName . $this->getFolderName();
		} elseif( CStorageType::NON_BACKUP_MOUNTS == $this->getStorageTypeId() ) {
			$strFolderPath  = PATH_NON_BACKUP_MOUNTS . $strParentFolderName . $this->getFolderName();
		} elseif( CStorageType::VOIP_MOUNTS == $this->getStorageTypeId() ) {
			$strFolderPath  = PATH_VOIP_MOUNTS . $strParentFolderName . $this->getFolderName();
		} else {
			$strFolderPath  = PATH_MOUNTS_TEST_CLIENTS . $strParentFolderName . $this->getFolderName();
		}

		if( '*' == $this->getFolderName() || false !== \Psi\CStringService::singleton()->strpos( $strFolderPath, '*' ) ) {
			return $boolIsValid;
		} elseif( false == is_dir( $strFolderPath ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'folder_name', 'No such folder exist in storage.' ) );
			return $boolIsValid;
		}

	return $boolIsValid;
	}

	public function valFolderPath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequireCleanup() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCleanupFrequency() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCleanupCommand() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCleanStartDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCleanEndDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valObjectDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $boolIsInsert, $boolIsOneTimeCleanup ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valStorageTypeId();
				$boolIsValid &= $this->valFolderName( $boolIsInsert, $objDatabase, $boolIsOneTimeCleanup );
				$boolIsValid &= $this->valTeamId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>