<?php

class CImportRequest extends CBaseImportRequest {

	const NUCE_HAS_RESIDENTS    = 2;

	public function setPasswordEncrypted( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			$this->set( 'm_strPassword', \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( trim( $strPassword ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function getPasswordDecrypted() {
		if( false == valStr( $this->m_strPassword ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strPassword, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
	}

	/**
	 * Create Functions
	 */

	public function createImportRequestEntity() {
		$objImportRequestEntity = new CImportRequestEntity();
		$objImportRequestEntity->setImportRequestId( $this->getId() );
		$objImportRequestEntity->setPropertyId( $this->getPropertyId() );
		$objImportRequestEntity->setImportStatusTypeId( CImportStatusType::INITIALIZED );

		return $objImportRequestEntity;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDestinationCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTemplatePropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToken() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompetitorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSettingTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasResidents() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSimpleCsvUpload() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInitiatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInitiatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getDetails( $boolReturnArray = false ) {
		if( false == $boolReturnArray ) {
			return parent::getDetails();
		}

		if( true == valObj( $this->m_jsonDetails, 'stdClass' ) ) {
			$this->m_strDetails = json_encode( $this->m_jsonDetails );
		}

		return json_decode( $this->m_strDetails, true );
	}

}
?>