<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportSqlSnippets
 * Do not add any new functions to this class.
 */

class CReportSqlSnippets extends CBaseReportSqlSnippets {

	public static function fetchSqlCompanyReports( $strSqlSnippetName = NULL, $objDatabase ) {

		$strWhere = ' deleted_by IS NULL';
		if( true == valStr( $strSqlSnippetName ) ) {
			$strWhere .= ' AND name ILIKE E\'%' . $strSqlSnippetName . '%\'';
		}

		$strSql = '	SELECT
						id,
						name,
						query
					FROM
						report_sql_snippets
					WHERE 
						' . $strWhere . '
					ORDER BY
						name ASC';

		return self::fetchReportSqlSnippets( $strSql, $objDatabase );
	}

}
?>