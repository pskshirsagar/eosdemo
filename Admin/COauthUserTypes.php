<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COauthUserTypes
 * Do not add any new functions to this class.
 */

class COauthUserTypes extends CBaseOauthUserTypes {

	public static function fetchOauthUserTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'COauthUserType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchOauthUserType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'COauthUserType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>