<?php

class CInductionSession extends CBaseInductionSession {

	public static $c_arrstrSessionWeekDays = [
		1 => 'Mon',
		2 => 'Tue',
		3 => 'Wed',
		4 => 'Thu',
		5 => 'Fri'
	];

	public function valInductionSessionTime( $arrmixInductionSessions ) {
		$boolIsValid = true;

		$intMinimumSessiontime	= 30;
		if( false == is_null( $this->getStartTime() ) && false == is_null( $this->getEndTime() ) ) {
			$arrintStartTime	= explode( ':', $this->getStartTime() );
			$intStartTimeMin	= ( $arrintStartTime[0] * 60 ) + $arrintStartTime[1];

			$arrintEndTime		= explode( ':', $this->getEndTime() );
			$intEndTimeMin	= ( $arrintEndTime[0] * 60 ) + $arrintEndTime[1];

			if( ( $intEndTimeMin - $intStartTimeMin ) < $intMinimumSessiontime ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_time', 'Minimum Session time should be greater than 30 min.' ) );
				$boolIsValid = false;
			}
		}
		if( true == valArr( $arrmixInductionSessions ) ) {
			$arrmixInductionSessions = rekeyArray( 'id', $arrmixInductionSessions );
			foreach( $arrmixInductionSessions as $arrmixInductionSession ) {
				if( $this->getId() != $arrmixInductionSession['id'] && $this->getWeekDay() == $arrmixInductionSession['week_day'] && $this->getStartTime() >= $arrmixInductionSession['start_time'] && $this->getStartTime() < $arrmixInductionSession['end_time'] ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_time',  '' . $arrmixInductionSession['name'] . ' is scheduled at this time of period.' ) );
					$boolIsValid = false;
				} elseif( $arrmixInductionSession['id'] == $this->getDependantSessionId() && $this->getWeekDay() <= $arrmixInductionSession['week_day'] && $this->getStartTime() <= $arrmixInductionSession['start_time'] ) {
					$arrmixDependantInductionSession = $arrmixInductionSessions[$this->getDependantSessionId()];
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_time', 'Session schedule time should be greater than dependant induction session. ( Dependant ' . $arrmixDependantInductionSession['name'] . ' ) . ' ) );
					$boolIsValid = false;
				}
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixInductionSessions = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valInductionSessionTime( $arrmixInductionSessions );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>