<?php

class CResourceRequisitionStatus extends CBaseResourceRequisitionStatus {

	const RR_STATUS_TYPE_PENDING = 1;
	const RR_STATUS_TYPE_APPROVED = 2;
	const RR_STATUS_TYPE_REJECTED = 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>