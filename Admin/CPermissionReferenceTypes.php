<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPermissionReferenceTypes
 * Do not add any new functions to this class.
 */

class CPermissionReferenceTypes extends CBasePermissionReferenceTypes {

	public static function fetchAllPermissionReferenceTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						permission_reference_types';

		return self::fetchPermissionReferenceTypes( $strSql, $objDatabase );
	}
}
?>