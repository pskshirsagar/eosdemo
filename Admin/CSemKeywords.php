<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemKeywords
 * Do not add any new functions to this class.
 */

class CSemKeywords extends CBaseSemKeywords {

  	public static function fetchSemKeywordBySemAdGroupIdByKeywords( $intSemAdGroupId, $strKeywords, $objDatabase ) {
		return parent::fetchSemKeyword( sprintf( 'SELECT * FROM sem_keywords WHERE sem_ad_group_id = %d AND lower( keywords ) = \'%s\' ', ( int ) $intSemAdGroupId, ( string ) \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strKeywords ) ) ) ), $objDatabase );
  	}

	public static function fetchSemKeywordBySemAdGroupIdById( $intSemAdGroupId, $intId, $objDatabase ) {
		return self::fetchSemKeyword( sprintf( 'SELECT * FROM sem_keywords WHERE sem_ad_group_id = %d AND id = %d', ( int ) $intSemAdGroupId,  ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemKeywordsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemKeywords( sprintf( 'SELECT * FROM sem_keywords WHERE sem_ad_group_id = %d ORDER BY lower( keywords )', ( int ) $intSemAdGroupId ), $objDatabase );
	}

	public static function fetchPublishedSemKeywordsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		$strSql = 'SELECT * FROM sem_keywords WHERE sem_ad_group_id = ' . ( int ) $intSemAdGroupId . ' AND deleted_by IS NULL ORDER BY lower( keywords )';
		return self::fetchSemKeywords( $strSql, $objDatabase );
	}

	public static function fetchSemKeywordsBySemAdGroupIdByIsSystem( $intSemAdGroupId, $boolIsSystem, $objDatabase ) {
		return self::fetchSemKeywords( sprintf( 'SELECT * FROM sem_keywords WHERE sem_ad_group_id = %d AND is_system = %d ORDER BY lower( keywords )', ( int ) $intSemAdGroupId, ( int ) $boolIsSystem ), $objDatabase );
	}

	public static function fetchDeletedSemKeywordsWithAverageCPCByKeywordsByCid( $intCid, $strKeywords, $objDatabase ) {

		$strSql = 'SELECT sk.*,
						( SELECT COALESCE( AVG( avg_cost_per_click ), 0 ) FROM sem_keyword_sources sks WHERE sk.id = sks.sem_keyword_id ) AS avg_cost_per_click
					FROM
						sem_keywords sk, sem_ad_groups sag
					WHERE sk.deleted_by IS NOT NULL AND lower( sk.keywords ) IN( ' . \Psi\CStringService::singleton()->strtolower( $strKeywords ) . ' ) AND sk.sem_ad_group_id = sag.id AND ( sag.cid IS NULL OR sag.cid = ' . ( int ) $intCid . ' ) ';

		return self::fetchSemKeywords( $strSql, $objDatabase );
	}

	public static function fetchSemKeywordsWithAverageCPCBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {

		$strSql = 'SELECT
						*,
						( SELECT COALESCE( AVG( avg_cost_per_click ), 0 ) FROM sem_keyword_sources sks WHERE sk.id = sks.sem_keyword_id ) AS avg_cost_per_click
					FROM
						sem_keywords sk
					WHERE
						sem_ad_group_id = ' . ( int ) $intSemAdGroupId . '
						AND sk.deleted_by IS NULL
					ORDER BY lower( keywords )';

		return self::fetchSemKeywords( $strSql, $objDatabase );
	}

	public static function fetchSemKeywordsBySemAdGroupIdByPropertyIdByCid( $intSemAdGroupId, $intPropertyId, $intCid, $objAdminDatbase ) {

		$strSql = 'SELECT
						sk.id,
						sk.keywords,
						( SELECT COALESCE( AVG( avg_cost_per_click ), 0 ) FROM sem_keyword_sources sks WHERE sk.id = sks.sem_keyword_id ) AS avg_cost_per_click,
						ska.id as sem_keyword_association_id
					FROM
						sem_keywords sk
						LEFT OUTER JOIN sem_keyword_associations ska ON (
							ska.sem_keyword_id = sk.id
							AND ska.property_id = ' . ( int ) $intPropertyId . '
							AND ska.cid = ' . ( int ) $intCid . '
						)
					WHERE
						sk.sem_ad_group_id = ' . ( int ) $intSemAdGroupId . '
						AND sk.deleted_by IS NULL
					ORDER BY sk.keywords';

		return self::fetchSemKeywords( $strSql, $objAdminDatbase );
	}

	public static function fetchSemKeywordsByIsSystem( $intIsSystem, $objDatabase ) {
		return self::fetchSemKeywords( sprintf( 'SELECT * FROM sem_keywords WHERE is_system = %d', ( int ) $intIsSystem ), $objDatabase );
	}

}
?>