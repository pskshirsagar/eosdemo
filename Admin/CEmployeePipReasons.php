<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePipReasons
 * Do not add any new functions to this class.
 */

class CEmployeePipReasons extends CBaseEmployeePipReasons {

	public static function fetchEmployeePipReasonsByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						e.*,
						p.name as reason_name
					FROM
						employee_pip_reasons AS e
						LEFT JOIN pip_reasons AS p ON ( p.id = e.pip_reason_id )
						JOIN employee_histories AS eh ON ( e.employee_history_id = eh.id )
					WHERE
						e.employee_id = ' . ( int ) $intEmployeeId . '
						AND eh.pip_details ->> \'pip_end_date\' is NULL
					ORDER BY
						p.name';

		return self::fetchEmployeePipReasons( $strSql, $objDatabase );
	}

	public  static function fetchEmployeePipReasonsByEmployeePipFilter( $objEmployeeFilter, $objDatabase, $boolIsCount = false ) {

		$intOffset 		= ( 0 < $objEmployeeFilter->getPageNo() ) ? $objEmployeeFilter->getPageSize() * ( $objEmployeeFilter->getPageNo() - 1 ) : 0;
		$intLimit  		= ( int ) $objEmployeeFilter->getPageSize();
		$strOrderClause = '';
		$strOrder 		= '';

		if( false == is_null( $objEmployeeFilter ) && true == valStr( $objEmployeeFilter->getOrderByField() ) ) {
			$strOrder 		= ( false == $objEmployeeFilter->getOrderByType() ) ? ' DESC ' : $objEmployeeFilter->getOrderByType();
			$strOrderClause = ( false == $objEmployeeFilter->getOrderByField() ) ? '  ' : ' ORDER BY ' . $objEmployeeFilter->getOrderByField();
			$strOrderClause = $strOrderClause . ' ' . $strOrder;
		}

		$strWhere = CEmployeePipReasons::getSearchCriteria( $objEmployeeFilter, NULL );

		$strSql = '	SELECT
						e.id AS employee_id,
						e.preferred_name AS employee_name,
						e1.preferred_name AS hr_name,
						e2.preferred_name AS manager_name,
						e3.preferred_name AS tpm_name,
						e4.preferred_name AS add_name,
						COUNT( DISTINCT epr.employee_history_id ) as pip_count
					FROM
						employee_pip_reasons epr
						JOIN pip_reasons pr ON (epr.pip_reason_id = pr.id)
						JOIN employees e ON (epr.employee_id = e.id)
						JOIN team_employees te ON (te.employee_id = e.id AND te.is_primary_team = 1 )
						JOIN teams t ON (te.team_id = t.id)
						LEFT JOIN employees e1 ON (e1.id = t.hr_representative_employee_id)
						LEFT JOIN employees e2 ON (e2.id = e.reporting_manager_id)
						LEFT JOIN employees e3 ON (e3.id = t.tpm_employee_id)
						LEFT JOIN employees e4 ON (e4.id = t.add_employee_id)
						' . $strWhere . '
					GROUP BY
						e.id,
						e.preferred_name,
						e1.preferred_name,
						e2.preferred_name,
						e3.preferred_name,
						e4.preferred_name
						' . $strOrderClause;

		if( false == $boolIsCount ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public  static function fetchEmployeePipReasonsWithDetails( $objDatabase, $intEmployeeId = NULL, $objEmployeeFilter=NULL ) {

		$strWhere = CEmployeePipReasons::getSearchCriteria( $objEmployeeFilter, $intEmployeeId );

		$strSql = ' SELECT
						string_agg(pr.name, \', \') AS pip_reason,
						eh.pip_details ->> \'pip_start_date\' as start_date,
						eh.pip_details ->> \'pip_end_date\' AS end_date,
						CASE
						WHEN eh.pip_details ->> \'pip_end_date\' is NULL THEN \'IN PIP\'
						ELSE \'NOT IN PIP\'
						END AS status,
						e.preferred_name AS employee_name,
						e1.preferred_name AS hr_name,
						e2.preferred_name AS manager_name,
						e3.preferred_name AS tpm_name,
						e4.preferred_name AS add_name,
						string_agg(pr.name, \', \') as pip_reason_names
					FROM
						employee_pip_reasons epr
						JOIN employee_histories eh ON (epr.employee_history_id=eh.id)
						JOIN pip_reasons pr ON (epr.pip_reason_id = pr.id)
						JOIN employees e ON (epr.employee_id = e.id)
						JOIN team_employees te ON (te.employee_id = e.id AND te.is_primary_team = 1 )
						JOIN teams t ON (te.team_id = t.id)
						LEFT JOIN employees e1 ON (e1.id = t.hr_representative_employee_id)
						LEFT JOIN employees e2 ON (e2.id = e.reporting_manager_id)
						LEFT JOIN employees e3 ON (e3.id = t.tpm_employee_id)
						LEFT JOIN employees e4 ON (e4.id = t.tpm_employee_id)
						' . $strWhere . '
					GROUP BY
						epr.employee_history_id,
						eh.pip_details ->> \'pip_start_date\',
						eh.pip_details ->> \'pip_end_date\',
						e.preferred_name,
						e1.preferred_name,
						e2.preferred_name,
						e3.preferred_name,
						e4.preferred_name
					ORDER BY
						e.preferred_name,
						eh.pip_details ->> \'pip_end_date\' DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public  static function getSearchCriteria( $objEmployeeFilter, $intEmployeeId ) {

		$strWhereCondition = ' WHERE e.preferred_name IS NOT NULL  ';

		if( true == valObj( $objEmployeeFilter, 'CEmployeePIPFilter' ) ) {
			$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getHrRepresentativeEmployeeId() ) ) ? ' AND t.hr_representative_employee_id = ' . $objEmployeeFilter->getHrRepresentativeEmployeeId() : '';
			$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getAddEmployeeId() ) ) ? ' AND t.add_employee_id = ' . $objEmployeeFilter->getAddEmployeeId() : '';
			$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getTpmEmployeeId() ) ) ? ' AND t.tpm_employee_id = ' . $objEmployeeFilter->getTpmEmployeeId() : '';
			$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getStartDate() ) && true == valStr( $objEmployeeFilter->getEndDate() ) ) ? ' AND epr.created_on::date BETWEEN \' ' . $objEmployeeFilter->getStartDate() . ' \' AND \' ' . $objEmployeeFilter->getEndDate() . '\'' : '';
			$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getCountryCode() ) ) ? ' AND pr.country_code = \'' . $objEmployeeFilter->getCountryCode() . '\'' : '';
		}

		$strWhereCondition .= ( true == is_numeric( $intEmployeeId ) ) ? ' AND	e.id=' . ( int ) $intEmployeeId . '' : '';

		return $strWhereCondition;
	}

}
?>