<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemLocationTypes
 * Do not add any new functions to this class.
 */

class CSemLocationTypes extends CBaseSemLocationTypes {

	public static function fetchSemLocationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSemLocationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSemLocationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSemLocationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllSemLocationTypes( $objAdminDatabase ) {
		return self::fetchSemLocationTypes( 'SELECT * FROM sem_location_types', $objAdminDatabase );
	}
}
?>