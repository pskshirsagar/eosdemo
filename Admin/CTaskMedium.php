<?php

class CTaskMedium extends CBaseTaskMedium {

	const PHONE				= 1;
	const EMAIL				= 2;
	const LIVE_CHAT			= 3;
	const RESIDENT_WORKS	= 4;
	const TEXT				= 6;
	const VIDEO_CALL        = 7;

	const ENTRATA_HELPDESK_SUPPORT_NUMBER	= '877-478-9695';

	public static $c_arrintPhoneTaskMediumIds = [ self::PHONE, self::RESIDENT_WORKS ];

	public static $c_arrintTaskMediumIds = [
		'TASK_MEDIUM_PHONE'          => self::PHONE,
		'TASK_MEDIUM_EMAIL'          => self::EMAIL,
		'TASK_MEDIUM_LIVE_CHAT'      => self::LIVE_CHAT,
		'TASK_MEDIUM_RESIDENT_WORKS' => self::RESIDENT_WORKS,
		'TASK_MEDIUM_VIDEO_CALL'     => self::VIDEO_CALL
	];

	public static $c_arrintTicketTaskMediumIds = [
		self::EMAIL,
		self::PHONE,
		self::LIVE_CHAT,
		self::VIDEO_CALL
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function update( $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;
		$strSql = 'UPDATE
						public.task_mediums
					SET ';

		if( false == $this->getName() ) {
			$strSql .= ' name = ' . $this->sqlName() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlName() ) != $this->getOriginalValueByFieldName( 'name' ) ) {
			$arrstrOriginalValueChanges['name'] = $this->sqlName();
			$strSql .= ' name = ' . $this->sqlName() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getDescription() ) {
			$strSql .= ' description = ' . $this->sqlDescription() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlDescription() ) != $this->getOriginalValueByFieldName( 'description' ) ) {
			$arrstrOriginalValueChanges['description'] = $this->sqlDescription();
			$strSql .= ' description = ' . $this->sqlDescription() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getResolutionTime() ) {
			$strSql .= ' resolution_time = ' . $this->sqlResolutionTime() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlResolutionTime() ) != $this->getOriginalValueByFieldName( 'resolution_time' ) ) {
			$arrstrOriginalValueChanges['resolution_time'] = $this->sqlResolutionTime();
			$strSql .= ' resolution_time = ' . $this->sqlResolutionTime() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getOrderNum() ) {
			$strSql .= ' order_num = ' . $this->sqlOrderNum() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlOrderNum() ) != $this->getOriginalValueByFieldName( 'order_num' ) ) {
			$arrstrOriginalValueChanges['order_num'] = $this->sqlOrderNum();
			$strSql .= ' order_num = ' . $this->sqlOrderNum() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getIsPublished() ) {
			$strSql .= ' is_published = ' . $this->sqlIsPublished();
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName( 'is_published' ) ) {
			$arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished();
			$strSql .= ' is_published = ' . $this->sqlIsPublished();
			$boolUpdate = true;
		}

		$strSql .= '
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

}
?>