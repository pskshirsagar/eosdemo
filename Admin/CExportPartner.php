<?php

class CExportPartner extends CBaseExportPartner {

	const NETSUITE = 1;

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNumber() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase ) {

		if( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) {

			$this->setLastUpdatedBy( $intCurrentUserId );
			$this->setLastUpdatedOn( date( 'Y-m-d h:i:sa' ) );
			$strSql = 'UPDATE
					' . static::TABLE_NAME . '
				SET  
					details = ' . $this->sqlDetails() . '
				WHERE
					id = ' . ( int ) $this->sqlId();

			if( false == $this->executeSql( $strSql, $this, $objDatabase ) ) {
				return false;
			}
		}

		return true;
	}

}
?>