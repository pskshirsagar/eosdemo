<?php

class CExportObjectReference extends CBaseExportObjectReference {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportPartnerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportObjectReferenceType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportObjectReferenceTypeKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['response_details'] ) && false == is_null( $arrstrValues['response_details'] ) ) {
			$this->setResponseDetails( $arrstrValues['response_details'] );
		}

		if( true == isset( $arrstrValues['is_taxable'] ) && false == is_null( $arrstrValues['is_taxable'] ) ) {
			$this->setIsTaxable( $arrstrValues['is_taxable'] );
		}
	}

}
?>