<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingSessionGroups
 * Do not add any new functions to this class.
 */

class CTrainingSessionGroups extends CBaseTrainingSessionGroups {

	public static function fetchTrainingSessionGroups( $strSql, $objDatabase ) {
		return self::fetchObjects( $strSql, 'CTrainingSessionGroup', $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchTrainingSessionGroup( $strSql, $objDatabase ) {
		return self::fetchObject( $strSql, 'CTrainingSessionGroup', $objDatabase );
	}

	public static function fetchTrainingSessionGroupsByTrainingSessionIdByGroupIds( $intTrainingSessionId, $arrintGroupIds, $objDatabase ) {
		if( false == valArr( $arrintGroupIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						training_session_groups
					WHERE
						training_session_id = ' . ( int ) $intTrainingSessionId . '
						AND group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' ) ';

		return self::fetchTrainingSessionGroups( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionGroupsByTrainingSessionIds( $arrintTrainingSessionIds, $objDatabase ) {
		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						training_session_groups
					WHERE
						training_session_id IN ( ' . implode( ',', $arrintTrainingSessionIds ) . ' ) ';

		return self::fetchTrainingSessionGroups( $strSql, $objDatabase );
	}

}
?>