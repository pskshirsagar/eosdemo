<?php

class CTaskStakeholder extends CBaseTaskStakeholder {

	public function valStakeholder( $objDatabase ) {
		$boolIsValid = true;

		$objStakeholder = CTaskStakeholders::validateDuplicateRecords( $this->getEmployeeId(), $this->getTaskTypeId(), $this->getTaskPriorityId(), $this->getPsProductId(), $this->getPsProductOptionId(), $this->getGroupId(), $this->sqlEmail(), $objDatabase );

		if( true == valObj( $objStakeholder, 'CTaskStakeholder' ) && $objStakeholder->getId() != $this->getId() ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valStakeholder( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>