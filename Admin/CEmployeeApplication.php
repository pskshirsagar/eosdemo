<?php

define( 'PHP_MAX_POST', ini_get( 'post_max_size' ) );

use Psi\Eos\Admin\CPsWebsiteJobPostings;
use Psi\Libraries\ExternalFileUpload\CFileUpload;
use Psi\Eos\Admin\CEmployeeApplications;
use Psi\Eos\Admin\CEmployeeApplicationSteps;

use Psi\Libraries\UtilDates\CDates;

class CEmployeeApplication extends CBaseEmployeeApplication {

	protected $m_objEmployee;

	protected $m_strReviewCycle;
	protected $m_strEducationName;
	protected $m_strOfficeAddress;
	protected $m_strRecruiterName;
	protected $m_strHrEmailAddress;
	protected $m_strVerificationKey;
	protected $m_strCurrentLocation;
	protected $m_strCreatedByNameLast;
	protected $m_strPsDocumentFileName;
	protected $m_strCareerEmailAddress;
	protected $m_strCreatedByNameFirst;
	protected $m_strCreatedByPreferredName;
	protected $m_strPsWebsiteJobPostingName;
	protected $m_strPsWebsiteStatusTypeName;
	protected $m_strRecruiterEmailSignature;
	protected $m_strPsFamiliarityDescription;
	protected $m_strEmploymentApplicationName;
	protected $m_strLegalAgreementDescription;
	protected $m_strEmployeeApplicationGroupName;
	protected $m_strEmploymentApplicationSourceName;
	protected $m_strEmployeeApplicationStatusTypeName;
	protected $m_strCurrentAnnualCompensation;
	protected $m_strExpectedAnnualCompensation;

	protected $m_fltTotalExperience;

	protected $m_intScore;
	protected $m_intRawScore;
	protected $m_intMrabCutOff;
	protected $m_intDepartmentId;
	protected $m_intHasLegalStatus;
	protected $m_intRawScoreCutoff;
	protected $m_intExperienceYears;
	protected $m_intExperienceMonths;
	protected $m_intEmployeeStatusTypeId;
	protected $m_intExpectedEmploymentDays;
	protected $m_intEmployeeApplicationScoreTypeId;
	protected $m_intPsJobPostingStatusId;

	protected $m_boolIsMrabLoggedIn;
	protected $m_boolIsTestEmailSent;
	protected $m_boolPublishedPsJobPosting;
	protected $m_strEaUpdatedOn;
	protected $m_strEaCreatedOn;
	protected $m_boolIsRejectedEmployeeApplication;
	protected $m_boolIsLatestNewEmployeeApplication;
	protected $m_boolIsNewEmployeeApplication;

	const BASE_HEXADECIMAL				= 16;
	const XENTO_LOGO					= 'https://www.xento.com/images/xento-logo.png';

	const XENTO_COMPANY_NAME			= 'Xento Systems';
	const XENTO_OFFICE_PHONE_NUMBER		= '020-30481988';
	const XENTO_COMPANY_FULL_ADDRESS	= 'Xento Systems Pvt. Ltd.Fourth Floor, Tower 9, SEZ Magarpatta City, Pune.';
	const ENTRATA_OFFICE_PHONE_NUMBER	= '801-377-2471';
	const KEY_FILE_INDIA				= 'careers_careers-calendar_349015086028.json';
	const KEY_FILE_US					= 'hrresumes_applicant_tracking_system_121841019607.json';

	const BACK_OFFICE_JOB_POSTING		= 270;

	const TOOLTIP_NEW_EMPLOYEE_APPLICATION			= 'EA created before 3 months OR updated within 1 year && PR not associated/On Hold/Position Not Open.';
	const TOOLTIP_LATEST_NEW_EMPLOYEE_APPLICATION	= 'EA created within 3 months OR On Hold/Position Not Open.';
	const TOOLTIP_REJECTED_EMPLOYEE_APPLICATION		= 'EA created within 3 months && Rejected.';

	public function __construct() {
		parent::__construct();

		$this->m_boolIsMrabLoggedIn = false;

		return;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createEmployeeApplicationDetail() {
		$objEmployeeApplicationDetail = new CEmployeeApplicationDetail();
		$objEmployeeApplicationDetail->setEmployeeApplicationId( $this->getId() );

		return $objEmployeeApplicationDetail;
	}

	public function createEmployeeApplicationNote() {
		$objEmployeeApplicationNote = new CEmployeeApplicationNote();
		$objEmployeeApplicationNote->setEmployeeApplicationId( $this->getId() );
		$objEmployeeApplicationNote->setNoteDatetime( date( 'm/d/Y H:i:s' ) );

		return $objEmployeeApplicationNote;
	}

	public function createEmployeeApplicationDemographic() {
		$objEmployeeApplicationDemographic = new CEmployeeApplicationDemographic();
		$objEmployeeApplicationDemographic->setEmployeeApplicationId( $this->getId() );

		return $objEmployeeApplicationDemographic;
	}

	public function createEmployeeApplicationContact() {
		$objEmployeeApplicationContact = new CEmployeeApplicationContact();
		$objEmployeeApplicationContact->setEmployeeApplicationId( $this->getId() );
		$objEmployeeApplicationContact->setContactDatetime( date( 'm/d/Y H:i:s' ) );

		return $objEmployeeApplicationContact;
	}

	public function createEmployee() {
		$this->m_objEmployee = new CEmployee();
		$this->m_objEmployee->setEmployeeStatusTypeId( CEmployeeStatusType::CURRENT );
		$this->m_objEmployee->setNameFirst( $this->m_strNameFirst );
		$this->m_objEmployee->setNameLast( $this->m_strNameLast );
		$this->m_objEmployee->setPermanentEmailAddress( $this->m_strEmailAddress );
		$this->m_objEmployee->setNameFull( $this->m_strNameFirst . ' ' . $this->m_strNameLast );

		return $this->m_objEmployee;
	}

	public function createEmployeeApplicationScore() {
		$objEmployeeApplicationScore = new CEmployeeApplicationScore();
		$objEmployeeApplicationScore->setSubmittedOn( date( 'Y-m-d H:i:s' ) );
		$objEmployeeApplicationScore->setEmployeeApplicationId( $this->getId() );

		return $objEmployeeApplicationScore;
	}

	public function createPsDocument( $intPsDocumentTypeId, $intFileExtensionId ) {
		$objPsDocument = new CPsDocument();
		$objPsDocument->setEmployeeApplicationId( $this->getId() );
		$objPsDocument->setPsDocumentTypeId( $intPsDocumentTypeId );
		$objPsDocument->setFileExtensionId( $intFileExtensionId );
		$objPsDocument->setIsPublished( 1 );

		return $objPsDocument;
	}

	public function createEmployeeApplicationStep( $intPsJobPostingStepStatusId, $intEmployeeId = NULL ) {
		$objEmployeeApplicationStep = new CEmployeeApplicationStep();
		$objEmployeeApplicationStep->setEmployeeApplicationId( $this->getId() );
		$objEmployeeApplicationStep->setPsJobPostingStepStatusId( $intPsJobPostingStepStatusId );
		if( true == is_numeric( $intEmployeeId ) ) {
			$objEmployeeApplicationStep->setStepUpdatedByEmployeeId( $intEmployeeId );
		}
		return $objEmployeeApplicationStep;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['expected_employment_days'] ) ) {
			$this->setExpectedEmploymentDays( trim( $arrmixValues['expected_employment_days'] ) );
		}
		if( true == isset( $arrmixValues['employee_application_status_type_name'] ) ) {
			$this->setEmployeeApplicationStatusTypeName( trim( $arrmixValues['employee_application_status_type_name'] ) );
		}
		if( true == isset( $arrmixValues['ps_website_status_type_name'] ) ) {
			$this->setPsWebsiteStatusTypeName( trim( $arrmixValues['ps_website_status_type_name'] ) );
		}
		if( true == isset( $arrmixValues['employment_application_name'] ) ) {
			$this->setEmploymentApplicationName( trim( $arrmixValues['employment_application_name'] ) );
		}
		if( true == isset( $arrmixValues['employee_application_source_name'] ) ) {
			$this->setEmploymentApplicationSourceName( trim( $arrmixValues['employee_application_source_name'] ) );
		}
		if( true == isset( $arrmixValues['ps_website_job_posting_name'] ) ) {
			$this->setPsWebsiteJobPostingName( trim( $arrmixValues['ps_website_job_posting_name'] ) );
		}
		if( true == isset( $arrmixValues['employee_application_group_name'] ) ) {
			$this->setEmployeeApplicationGroupName( trim( $arrmixValues['employee_application_group_name'] ) );
		}
		if( true == isset( $arrmixValues['ps_document_file_name'] ) ) {
			$this->setPsDocumentFileName( trim( $arrmixValues['ps_document_file_name'] ) );
		}
		if( true == isset( $arrmixValues['has_legal_status'] ) ) {
			$this->setHasLegalStatus( trim( $arrmixValues['has_legal_status'] ) );
		}
		if( true == isset( $arrmixValues['current_annual_compensation_encrypted'] ) ) {
			$this->setCurrentAnnualCompensation( trim( $arrmixValues['current_annual_compensation_encrypted'] ) );
		}
		if( true == isset( $arrmixValues['expected_annual_compensation_encrypted'] ) ) {
			$this->setExpectedAnnualCompensation( trim( $arrmixValues['expected_annual_compensation_encrypted'] ) );
		}
		if( true == isset( $arrmixValues['legal_agreement_description'] ) ) {
			$this->setLegalAgreementDescription( trim( $arrmixValues['legal_agreement_description'] ) );
		}
		if( true == isset( $arrmixValues['ps_familiarity_description'] ) ) {
			$this->setPsFamiliarityDescription( trim( $arrmixValues['ps_familiarity_description'] ) );
		}
		if( true == isset( $arrmixValues['total_experience'] ) ) {
			$this->setTotalExperience( trim( $arrmixValues['total_experience'] ) );
		}
		if( true == isset( $arrmixValues['created_by_name_first'] ) ) {
			$this->setCreatedByNameFirst( trim( $arrmixValues['created_by_name_first'] ) );
		}
		if( true == isset( $arrmixValues['created_by_name_last'] ) ) {
			$this->setCreatedByNameLast( trim( $arrmixValues['created_by_name_last'] ) );
		}
		if( true == isset( $arrmixValues['created_by_preferred_name'] ) ) {
			$this->setCreatedByPreferredName( trim( $arrmixValues['created_by_preferred_name'] ) );
		}
		if( true == isset( $arrmixValues['experience_year'] ) ) {
			$this->setExperienceYear( trim( $arrmixValues['experience_year'] ) );
		}
		if( true == isset( $arrmixValues['experience_month'] ) ) {
			$this->setExperienceMonth( trim( $arrmixValues['experience_month'] ) );
		}
		if( true == isset( $arrmixValues['current_location'] ) ) {
			$this->setCurrentLocation( trim( $arrmixValues['current_location'] ) );
		}
		if( true == isset( $arrmixValues['work_experience'] ) ) {
			$this->setWorkExperience( trim( $arrmixValues['work_experience'] ) );
		}
		if( true == isset( $arrmixValues['education_description'] ) ) {
			$this->setEducationDescription( trim( $arrmixValues['education_description'] ) );
		}
		if( true == isset( $arrmixValues['education_name'] ) ) {
			$this->setEducationName( trim( $arrmixValues['education_name'] ) );
		}
		if( true == isset( $arrmixValues['score'] ) ) {
			$this->setscore( trim( $arrmixValues['score'] ) );
		}
		if( true == isset( $arrmixValues['raw_score'] ) ) {
			$this->setRawScore( trim( $arrmixValues['raw_score'] ) );
		}
		if( true == isset( $arrmixValues['recruiter_name'] ) ) {
			$this->setRecruiterName( trim( $arrmixValues['recruiter_name'] ) );
		}
		if( true == isset( $arrmixValues['recruiter_email_signature'] ) ) {
			$this->setRecruiterEmailSignature( trim( $arrmixValues['recruiter_email_signature'] ) );
		}
		if( true == isset( $arrmixValues['review_cycle'] ) ) {
			$this->setReviewCycle( trim( $arrmixValues['review_cycle'] ) );
		}
		if( true == isset( $arrmixValues['employee_status_type_id'] ) ) {
			$this->setEmployeeStatusTypeId( trim( $arrmixValues['employee_status_type_id'] ) );
		}
		if( true == isset( $arrmixValues['department_id'] ) ) {
			$this->setDepartmentId( trim( $arrmixValues['department_id'] ) );
		}

		if( true == isset( $arrmixValues['mrab_cutoff'] ) ) {
			$this->setMrabCutoff( trim( $arrmixValues['mrab_cutoff'] ) );
		}

		if( true == isset( $arrmixValues['raw_score_cutoff'] ) ) {
			$this->setRawScoreCutoff( trim( $arrmixValues['raw_score_cutoff'] ) );
		}

		if( true == isset( $arrmixValues['employee_application_score_type_id'] ) ) {
			$this->setEmployeeApplicationScoreTypeId( trim( $arrmixValues['employee_application_score_type_id'] ) );
		}

		if( true == isset( $arrmixValues['is_published'] ) ) {
			$this->setPublishedPsJobPosting( trim( $arrmixValues['is_published'] ) );
		}

		if( true == isset( $arrmixValues['ps_job_posting_status_id'] ) ) {
			$this->setPsJobPostingStatusId( trim( $arrmixValues['ps_job_posting_status_id'] ) );
		}

		if( true == isset( $arrmixValues['order_num'] ) ) {
			$this->setOrderNum( $arrmixValues['order_num'] );
		}

		if( true == isset( $arrmixValues['ps_job_posting_step_type_id'] ) ) {
			$this->setPsJobPostingStepTypeId( $arrmixValues['ps_job_posting_step_type_id'] );
		}

		if( true == isset( $arrmixValues['ps_job_posting_step_id'] ) ) {
			$this->setPsJobPostingStepId( $arrmixValues['ps_job_posting_step_id'] );
		}

		if( true == isset( $arrmixValues['ea_created_on'] ) ) {
			$this->setEaCreatedOn( $arrmixValues['ea_created_on'] );
		}

		if( true == isset( $arrmixValues['ea_updated_on'] ) ) {
			$this->setEaUpdatedOn( $arrmixValues['ea_updated_on'] );
		}

		if( true == isset( $arrmixValues['is_rejected_employee_application'] ) ) {
			$this->setIsRejectedEmployeeApplication( $arrmixValues['is_rejected_employee_application'] );
		}

		if( true == isset( $arrmixValues['is_latest_new_employee_application'] ) ) {
			$this->setIslatestNewEmployeeApplication( $arrmixValues['is_latest_new_employee_application'] );
		}

		if( true == isset( $arrmixValues['is_new_employee_application'] ) ) {
			$this->setIsNewEmployeeApplication( $arrmixValues['is_new_employee_application'] );
		}

		return;
	}

	public function setEmployeeApplicationGroupName( $strEmployeeApplicationGroupName ) {
		$this->m_strEmployeeApplicationGroupName = $strEmployeeApplicationGroupName;
	}

	public function setPsWebsiteJobPostingName( $strPsWebsiteJobPostingName ) {
		$this->m_strPsWebsiteJobPostingName = $strPsWebsiteJobPostingName;
	}

	public function setExpectedEmploymentDays( $intExpectedEmploymentDays ) {
		$this->m_intExpectedEmploymentDays = $intExpectedEmploymentDays;
	}

	public function setEmployeeApplicationStatusTypeName( $strEmployeeApplicationStatusTypeName ) {
		$this->m_strEmployeeApplicationStatusTypeName = $strEmployeeApplicationStatusTypeName;
	}

	public function setPsWebsiteStatusTypeName( $strPsWebsiteStatusTypeName ) {
		$this->m_strPsWebsiteStatusTypeName = $strPsWebsiteStatusTypeName;
	}

	public function setEmploymentApplicationName( $strEmploymentApplicationName ) {
		$this->m_strEmploymentApplicationName = $strEmploymentApplicationName;
	}

	public function setEmploymentApplicationSourceName( $strEmploymentApplicationSourceName ) {
		$this->m_strEmploymentApplicationSourceName = $strEmploymentApplicationSourceName;
	}

	public function setVerificationKey( $strVerificationKey ) {
		$this->m_strVerificationKey = $strVerificationKey;
	}

	public function setIsTestEmailSent( $intIsTestEmailSent ) {
		$this->m_boolIsTestEmailSent = $intIsTestEmailSent;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = CStrings::strTrimDef( mb_convert_case( $strNameFirst, MB_CASE_TITLE, 'UTF-8' ), 50, NULL, true );
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->m_strNameMiddle = CStrings::strTrimDef( mb_convert_case( $strNameMiddle, MB_CASE_TITLE, 'UTF-8' ), 50, NULL, true );
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = CStrings::strTrimDef( mb_convert_case( $strNameLast, MB_CASE_TITLE, 'UTF-8' ), 50, NULL, true );
	}

	public function setCity( $strCity ) {
		$this->m_strCity = CStrings::strTrimDef( mb_convert_case( $strCity, MB_CASE_TITLE, 'UTF-8' ), 50, NULL, true );
	}

	public function setState( $strState ) {
		$this->m_strState = CStrings::strTrimDef( mb_convert_case( $strState, MB_CASE_TITLE, 'UTF-8' ), 50, NULL, true );
	}

	public function setPsDocumentFileName( $strPsDocumentFileName ) {
		$this->m_strPsDocumentFileName = $strPsDocumentFileName;
	}

	public function setHasLegalStatus( $intHasLegalStatus ) {
		$this->m_intHasLegalStatus = CStrings::strToIntDef( $intHasLegalStatus, NULL, false );
	}

	public function setCurrentAnnualCompensation( $strCurrentAnnualCompensation ) {
		$this->m_strCurrentAnnualCompensation = $strCurrentAnnualCompensation;
	}

	public function setExpectedAnnualCompensation( $strExpectedAnnualCompensation ) {
		$this->m_strExpectedAnnualCompensation = $strExpectedAnnualCompensation;
	}

	public function setTotalExperience( $fltTotalExperience ) {
		$this->m_fltTotalExperience = CStrings::strToFloatDef( $fltTotalExperience, NULL, false, 2 );
	}

	public function setLegalAgreementDescription( $strLegalAgreementDescription ) {
		$this->m_strLegalAgreementDescription = CStrings::strTrimDef( $strLegalAgreementDescription, -1, NULL, true );
	}

	public function setPsFamiliarityDescription( $strPsFamiliarityDescription ) {
		$this->m_strPsFamiliarityDescription = CStrings::strTrimDef( $strPsFamiliarityDescription, -1, NULL, true );
	}

	public function setCreatedByNameFirst( $strCreatedByNameFirst ) {
		$this->m_strCreatedByNameFirst = $strCreatedByNameFirst;
	}

	public function setCreatedByNameLast( $strCreatedByNameLast ) {
		$this->m_strCreatedByNameLast = $strCreatedByNameLast;
	}

	public function setCreatedByPreferredName( $strCreatedByPreferredName ) {
		$this->m_strCreatedByPreferredName = $strCreatedByPreferredName;
	}

	public function setIsMrabLoggedIn( $boolIsMrabLoggedIn ) {
		$this->m_boolIsMrabLoggedIn = $boolIsMrabLoggedIn;
	}

	public function setTaxNumber( $strTaxNumber ) {
		if( true == valStr( $strTaxNumber ) ) {
			$this->setTaxNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( ( $strTaxNumber ), CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		}
		return false;
	}

	public function setExperienceYear( $intExperienceYears ) {
		$this->m_intExperienceYears = $intExperienceYears;
	}

	public function setExperienceMonth( $intExperienceMonths ) {
		$this->m_intExperienceMonths = $intExperienceMonths;
	}

	public function setCurrentLocation( $strCurrentLocation ) {
		$this->m_strCurrentLocation = $strCurrentLocation;
	}

	public function setWorkExperience( $strWorkExperience ) {
		$this->m_strWorkExperience = CStrings::strTrimDef( $strWorkExperience, -1, NULL, true );
	}

	public function setEducationDescription( $strEducationDescription ) {
		$this->m_strEducationDescription = CStrings::strTrimDef( $strEducationDescription, -1, NULL, true );
	}

	public function setEducationName( $strEducationName ) {
		$this->m_strEducationName = CStrings::strTrimDef( $strEducationName );
	}

	public function setScore( $intScore ) {
		$this->m_intScore = $intScore;
	}

	public function setRawScore( $intRawScore ) {
		$this->m_intRawScore = $intRawScore;
	}

	public function setRecruiterName( $strRecruiterName ) {
		$this->m_strRecruiterName = CStrings::strTrimDef( $strRecruiterName );
	}

	public function setRecruiterEmailSignature( $strRecruiterEmailSignature ) {
		$this->m_strRecruiterEmailSignature = CStrings::strTrimDef( $strRecruiterEmailSignature );
	}

	public function setReviewCycle( $strReviewCycle ) {
		$this->m_strReviewCycle = CStrings::strTrimDef( $strReviewCycle );
	}

	public function setEmployeeStatusTypeId( $intEmployeeStatusTypeId ) {
		$this->m_intEmployeeStatusTypeId = $intEmployeeStatusTypeId;
	}

	public function setDepartmentId( $intDeparmentId ) {
		$this->m_intDepartmentId = $intDeparmentId;
	}

	public function setEmployeeApplicationScoreTypeId( $intEmployeeApplicationScoreTypeId ) {
		$this->m_intEmployeeApplicationScoreTypeId = $intEmployeeApplicationScoreTypeId;
	}

	public function setMrabCutoff( $intMrabCutoff ) {
		$this->m_intMrabCutOff = $intMrabCutoff;
	}

	public function setRawScoreCutoff( $intRawScoreCutoff ) {
		$this->m_intRawScoreCutOff = $intRawScoreCutoff;
	}

	public function setPublishedPsJobPosting( $boolPublishedPsJobPosting ) {
		$this->m_boolPublishedPsJobPosting = $boolPublishedPsJobPosting;
	}

	public function setPsJobPostingStatusId( $intPsJobPostingStatusId ) {
		$this->set( 'm_intPsJobPostingStatusId', CStrings::strToIntDef( $intPsJobPostingStatusId, NULL, false ) );
	}

	public function setOrderNum( $intOrderNum ) {
		$this->m_intOrderNum = $intOrderNum;
	}

	public function setPsJobPostingStepTypeId( $intPsJobPostingStepTypeId ) {
		$this->m_intPsJobPostingStepTypeId = $intPsJobPostingStepTypeId;
	}

	public function setPsJobPostingStepId( $intPsJobPostingStepId ) {
		$this->m_intPsJobPostingStepId = $intPsJobPostingStepId;
	}

	public function setEaCreatedOn( $strEaCreatedOn ) {
		$this->m_strEaCreatedOn = $strEaCreatedOn;
	}

	public function setEaUpdatedOn( $strEaUpdatedOn ) {
		$this->m_strEaUpdatedOn = $strEaUpdatedOn;
	}

	public function setIsRejectedEmployeeApplication( $boolIsRejectedEmployeeApplication ) {
		$this->set( 'm_boolIsRejectedEmployeeApplication', CStrings::strToBool( $boolIsRejectedEmployeeApplication ) );
	}

	public function setIslatestNewEmployeeApplication( $boolIsLatestNewEmployeeApplication ) {
		$this->set( 'm_boolIsLatestNewEmployeeApplication', CStrings::strToBool( $boolIsLatestNewEmployeeApplication ) );
	}

	public function setIsNewEmployeeApplication( $boolIsNewEmployeeApplication ) {
		$this->set( 'm_boolIsNewEmployeeApplication', CStrings::strToBool( $boolIsNewEmployeeApplication ) );
	}

	/**
	 * Get Functions
	 *
	 */

	public function getEducationDescription() {
		return $this->m_strEducationDescription;
	}

	public function getWorkExperience() {
		return $this->m_strWorkExperience;
	}

	public function getVerificationKey() {
		return $this->m_strVerificationKey;
	}

	public function getNameFull() {
		return $this->getNameFirst() . ' ' . $this->getNameLast();
	}

	public function getIsTestEmailSent() {
		return $this->m_boolIsTestEmailSent;
	}

	public function getExpectedEmploymentDays() {
		return $this->m_intExpectedEmploymentDays;
	}

	public function getEmployeeApplicationGroupName() {
		return $this->m_strEmployeeApplicationGroupName;
	}

	public function getPsWebsiteJobPostingName() {
		return $this->m_strPsWebsiteJobPostingName;
	}

	public function getEmployeeApplicationStatusTypeName() {
		return $this->m_strEmployeeApplicationStatusTypeName;
	}

	public function getPsWebsiteStatusTypeName() {
		return $this->m_strPsWebsiteStatusTypeName;
	}

	public function getEmploymentApplicationName() {
		return $this->m_strEmploymentApplicationName;
	}

	public function getEmploymentApplicationSourceName() {
		return $this->m_strEmploymentApplicationSourceName;
	}

	public function getPsDocumentFileName() {
		return $this->m_strPsDocumentFileName;
	}

	public function getHasLegalStatus() {
		return $this->m_intHasLegalStatus;
	}

	public function getCurrentAnnualCompensation() {
		if( false == valStr( $this->m_strCurrentAnnualCompensation ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strCurrentAnnualCompensation, CONFIG_SODIUM_KEY_SALARY_COMPENSATION, [ 'legacy_secret_key' => CONFIG_KEY_SALARY_COMPENSATION ] );
	}

	public function getExpectedAnnualCompensation() {
		if( false == valStr( $this->m_strExpectedAnnualCompensation ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strExpectedAnnualCompensation, CONFIG_SODIUM_KEY_SALARY_COMPENSATION, [ 'legacy_secret_key' => CONFIG_KEY_SALARY_COMPENSATION ] );
	}

	public function getTotalExperience() {
		return $this->m_fltTotalExperience;
	}

	public function getLegalAgreementDescription() {
		return $this->m_strLegalAgreementDescription;
	}

	public function getPsFamiliarityDescription() {
		return $this->m_strPsFamiliarityDescription;
	}

	public function getCreatedByNameFirst() {
		return $this->m_strCreatedByNameFirst;
	}

	public function getCreatedByNameLast() {
		return $this->m_strCreatedByNameLast;
	}

	public function getCreatedByPreferredName() {
		return $this->m_strCreatedByPreferredName;
	}

	public function getIsMrabLoggedIn() {
		return $this->m_boolIsMrabLoggedIn;
	}

	public function getTaxNumber() {
		if( false == is_null( $this->m_strTaxNumberEncrypted ) ) {
			return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );

		}
		return NULL;
	}

	public function getExperienceYear() {
		return $this->m_intExperienceYears;
	}

	public function getExperienceMonth() {
		return $this->m_intExperienceMonths;
	}

	public function getCurrentLocation() {
		return $this->m_strCurrentLocation;
	}

	public function getEducationName() {
		return $this->m_strEducationName;
	}

	public function getScore() {
		return $this->m_intScore;
	}

	public function getRawScore() {
		return $this->m_intRawScore;
	}

	public function getRecruiterName() {
		return $this->m_strRecruiterName;
	}

	public function getRecruiterEmailSignature() {
		return $this->m_strRecruiterEmailSignature;
	}

	public function getReviewCycle() {
		return $this->m_strReviewCycle;
	}

	public function getEmployeeStatusTypeId() {
		return $this->m_intEmployeeStatusTypeId;
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function getEmployeeApplicationScoreTypeId() {
		return $this->m_intEmployeeApplicationScoreTypeId;
	}

	public function getMrabCutoff() {
		return $this->m_intMrabCutOff;
	}

	public function getRawScoreCutoff() {
		return $this->m_intRawScoreCutOff;
	}

	public function getPublishedPsJobPosting() {
		return $this->m_boolPublishedPsJobPosting;
	}

	public function getPsJobPostingStatusId() {
		return $this->m_intPsJobPostingStatusId;
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function getPsJobPostingStepTypeId() {
		return $this->m_intPsJobPostingStepTypeId;
	}

	public function getPsJobPostingStepId() {
		return $this->m_intPsJobPostingStepId;
	}

	public function getEaCreatedOn() {
		return $this->m_strEaCreatedOn;
	}

	public function getEaUpdatedOn() {
		return $this->m_strEaUpdatedOn;
	}

	public function getIsRejectedEmployeeApplication() {
		return $this->m_boolIsRejectedEmployeeApplication;
	}

	public function getIsLatestNewEmployeeApplication() {
		return $this->m_boolIsLatestNewEmployeeApplication;
	}

	public function getIsNewEmployeeApplication() {
		return $this->m_boolIsNewEmployeeApplication;
	}

	/**
	 * Other Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolResumeRequired = true ) {
		$boolValid = true;

		if( false == $boolResumeRequired ) {
			$boolValid = parent::insert( $intCurrentUserId, $objDatabase );
		} else {

			$objPsDocument = new CPsDocument();
			$objPsDocument->setPsDocumentTypeId( CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_APPLICATION_RESUME );

			$intEmployeeApplicationId = $this->getId();
			if( false === valId( $intEmployeeApplicationId ) ) {
				$intEmployeeApplicationId = $this->fetchNextId( $objDatabase );
				$this->setId( $intEmployeeApplicationId );
			}

			$objPsDocument->setCid( CClient::ID_DEFAULT );
			$objPsDocument->setEmployeeApplicationId( $intEmployeeApplicationId );

			if( true == empty( $_FILES['employee_resume']['name'] ) ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_resume', 'Resume is required' ) );
			}

			if( true == $boolValid ) {
				$intPsDocumentId = $objPsDocument->fetchNextId( $objDatabase );
				$objPsDocument->setId( $intPsDocumentId );

				$this->setPsDocumentId( $intPsDocumentId );
				$boolValid = parent::insert( $intCurrentUserId, $objDatabase );
			}

			if( true == $boolValid ) {

				$arrstrPathInfo = pathinfo( $_FILES['employee_resume']['name'] );

				$objFileExtension = \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrPathInfo['extension'], $objDatabase );

				if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
					return false;
				}

				$objPsDocument->setFileExtensionId( $objFileExtension->getId() );
				$objPsDocument->setFileName( $_FILES['employee_resume']['name'] );
				$objPsDocument->setTitle( $arrstrPathInfo['filename'] );

				if( false == is_null( $this->getEmployeeId() ) ) {
					$objPsDocument->setEmployeeId( $this->getEmployeeId() );
				}

				$boolValid = $objPsDocument->insert( SYSTEM_USER_ID, $objDatabase );
			}

			if( true == $boolValid ) {
				$boolValid = $this->uploadFile( $objPsDocument, $objDatabase );
			}

		}

		return $boolValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolResumeRequired = true ) {
		$boolValid	= true;
		$boolIsInsert	= false;

		if( false == $boolResumeRequired ) {
			$boolValid = parent::update( $intCurrentUserId, $objDatabase );
		} else {

			if( true == $boolValid && false == empty( $_FILES['employee_resume']['name'] ) ) {

				$intPsDocumentId = $this->getPsDocumentId();

				if( true == empty( $intPsDocumentId ) ) {
					$objPsDocument = new CPsDocument();

					$intPsDocumentId = $objPsDocument->fetchNextId( $objDatabase );
					$objPsDocument->setId( $intPsDocumentId );

					$this->setPsDocumentId( $intPsDocumentId );
					$boolIsInsert = true;
				} else {
					$objPsDocument = \Psi\Eos\Admin\CPsDocuments::createService()->fetchPsDocumentById( $intPsDocumentId, $objDatabase );
				}

				if( true == valObj( $objPsDocument, 'CPsDocument' ) ) {
					$objPsDocument->setPsDocumentTypeId( CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_APPLICATION_RESUME );
				}

				$arrstrPathInfo = pathinfo( $_FILES['employee_resume']['name'] );

				$objFileExtension = \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrPathInfo['extension'], $objDatabase );

				if( true == valObj( $objPsDocument, 'CPsDocument' ) && true == valObj( $objFileExtension, 'CFileExtension' ) ) {
					$objPsDocument->setFileExtensionId( $objFileExtension->getId() );
					$objPsDocument->setCid( CClient::ID_DEFAULT );
					$objPsDocument->setEmployeeApplicationId( $this->getId() );
					$objPsDocument->setTitle( $arrstrPathInfo['filename'] );

					if( true == $boolIsInsert ) {
						$objPsDocument->setFileName( $arrstrPathInfo['basename'] );
						$boolValid = $objPsDocument->insert( SYSTEM_USER_ID, $objDatabase );
					} else {
						$boolValid = $objPsDocument->update( SYSTEM_USER_ID, $objDatabase );
					}
				}

				$boolValid = $this->uploadFile( $objPsDocument, $objDatabase );
			}
			$boolValid = parent::update( $intCurrentUserId, $objDatabase );
		}

		return $boolValid;
	}

	public function uploadFile( $objPsDocument, $objDatabase ) {

		$boolValid			= true;

		if( false === valStr( $_FILES['employee_resume']['tmp_name'] ) || false === file_exists( $_FILES['employee_resume']['tmp_name'] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_resume']['name'] . ' Unable to upload resume. ', NULL ) );
			return false;
		}

		$objStorageGateway			= CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED );

		// Delete object in case of update file.
		$objStoredObject				= $objPsDocument->fetchStoredObject( $objDatabase );
		$arrmixGatewayRequest			= $objStoredObject->createGatewayRequest( [ 'mountSystem' => 'NON_BACKUP_MOUNTS' ] );
		$objDeleteObjectResponse 		= $objStorageGateway->deleteObject( $arrmixGatewayRequest );
		if( true == $objDeleteObjectResponse->hasErrors() || ( true === valId( $objStoredObject->getId() ) && false == $objPsDocument->deleteStoredObject( CUSER::ID_SYSTEM, $objDatabase ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_resume']['name'] . ' Unable to delete existing resume file. ', NULL ) );
			return false;
		}

		$arrstrPathInfo = pathinfo( $_FILES['employee_resume']['name'] );
		$strFileName = 'resume_' . $this->getId() . '_' . strtotime( 'now' ) . '.' . $arrstrPathInfo['extension'];
		$objPsDocument->setFileName( $strFileName );

		$arrmixGatewayRequest				= $objPsDocument->createPutGatewayRequest( [ 'data' => file_get_contents( $_FILES['employee_resume']['tmp_name'] ) ] );
		$objObjectStorageGatewayResponse	= $objStorageGateway->putObject( $arrmixGatewayRequest );

		if( true === $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to upload resume.' ) ) );
			$boolValid = false;
		} elseif( false === $objPsDocument->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse )->update( CUSER::ID_SYSTEM, $objDatabase ) ) {
			$boolIsValid = false;
		}

		return $boolValid;
	}

	public function sendEmployeeApplicationEmail( $objEmployee, $objEmailDatabase, $intWebsiteJobPostingId, $boolIsEdit = false ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$intPsWebsiteJobPostingId = base64_encode( $intWebsiteJobPostingId );
		$intEmployeeApplicationId = base64_encode( $this->getId() );

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'Asia/Calcutta' );
		}
		$this->loadCompanyData();

		$strUrlSuffix				= ( true === CONFIG_IS_SECURE_URL ) ? 'https' : 'http';
		$strEmployeeApplicationUrl	= $strUrlSuffix . '://' . CONFIG_COMPANY_BASE_DOMAIN . '/careers/xento/JP/' . $intPsWebsiteJobPostingId . '/EA/' . $intEmployeeApplicationId;
		$strHtmlContent				= 'Hello ' . \Psi\CStringService::singleton()->ucwords( $this->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucwords( $this->getNameLast() ) . ', <br/><br/>';

		if( false == $boolIsEdit ) {
			$strHtmlContent .= 'Moving ahead with your application,please click on below link for further process. <br/> <br/> <a target="_blank" href= ' . $strEmployeeApplicationUrl . '> Click here. </a> <br/><br/> Thank You, <br/><br/>';
		} else {
			$strHtmlContent .= 'It gives us immense pleasure to re-connect with you.
								Your profile is already a part of our robust database system.
								However, if there are any further updates we request you to modify your experience, skill-sets & certifications.<br/><br><br/>
								Please click on below link to edit your application. <br/> <br/> <a target="_blank" href= ' . $strEmployeeApplicationUrl . '> Click here</a>. <br/><br/>';
		}

		if( true == valStr( $objEmployee->getEmailSignature() ) ) {
			$strHtmlContent .= $objEmployee->getEmailSignature();
		} elseif( CEmployee::ID_SYSTEM == $objEmployee->getId() ) {
			$strHtmlContent .= 'Thank You,<br/>';
			$strHtmlContent .= 'HR Team<br/>' . $this->m_strOfficeAddress;
		} else {
			$strHtmlContent .= $objEmployee->getNameFull() . '<br/>' . $objEmployee->getEmailAddress() . '</br>' . $this->m_strOfficeAddress;
		}

		$strSubject = 'Employment Application Form ';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'content', $strHtmlContent );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'America/Denver' );
		}

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress = NULL, $intIsSelfDestruct = 0, $strFromEmailAddress = $this->m_strCareerEmailAddress );

		if( true == valStr( $this->getEmailAddress() ) ) {
			$objSystemEmail->setToEmailAddress( $this->getEmailAddress() );
		}

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {

			if( false == $objSystemEmail->validate( VALIDATE_INSERT ) ) {
				return false;
			}

			$boolIsSentEmail = true;
			if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
				$boolIsSentEmail = false;
			}
		}
		return $boolIsSentEmail;
	}

	public function generateEmployeeJobPostingUrls( $objAdminDatabase ) {

		$arrstrTestUrls = array();

		if( false != $this->getPsWebsiteJobPostingId() ) {

			$arrobjTestGroups = CTestGroups::fetchTestGroupsByPsWebsiteJobPostingIdsWithOutCodingTest( [ $this->getPsWebsiteJobPostingId() ], $objAdminDatabase, $boolDelete = true );

			if( true == valArr( $arrobjTestGroups ) ) {
				$objTestSubmission = new CTestSubmission();
				$objTestSubmission->setEmployeeApplicationId( $this->getId() );

				$strPsiDomain = CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN;

				foreach( $arrobjTestGroups as $objTestGroup ) {
					$objTestSubmissionClone		= clone $objTestSubmission;
					$objTestSubmissionClone->setTestId( $objTestGroup->getTestId() );
					$arrobjTestSubmissions[]	= $objTestSubmissionClone;
					$arrstrTestNames[$objTestGroup->getTestId()] = $objTestGroup->getTestName();
				}
				if( true == valArr( $arrobjTestSubmissions ) && false == CTestSubmissions::bulkInsert( $arrobjTestSubmissions, SYSTEM_USER_ID, $objAdminDatabase ) ) {
					return false;
				}

				foreach( $arrobjTestSubmissions as $objTestSubmission ) {
					$strTestUrl			= $strPsiDomain . '/?tid=' . base64_encode( $objTestSubmission->getTestId() ) . '&eid=' . base64_encode( $objTestSubmission->getEmployeeApplicationId() ) . '&ts_id=' . base64_encode( $objTestSubmission->getId() ) . '&et=' . strtotime( '+2 days' );
					$arrstrTestUrls[]	= '<a href="' . $strTestUrl . '" target="_blank">' . $arrstrTestNames[$objTestSubmission->getTestId()] . '</a>';
				}
			}
		}

		return $arrstrTestUrls;
	}

	public function generateTechnicalTestUrl( $objAdminDatabase ) {

		$arrstrTestUrls = array();

		$arrintTestQuestionDetails = CTestQuestions::fetchTestQuestionIdAndTestIdByPsWebsiteJobPostingIdByTestQuestionTypeId( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );

		if( false == valArr( $arrintTestQuestionDetails ) ) {
			return false;
		}

		$intTestId		= $arrintTestQuestionDetails[0]['test_id'];
		$intQuestionId	= $arrintTestQuestionDetails[0]['test_question_id'];

		$objTestSubmission = new CTestSubmission();
		if( true == valObj( $objTestSubmission, 'CTestSubmission' ) ) {
			$objTestSubmission->setEmployeeApplicationId( $this->getId() );
			$objTestSubmission->setTestId( $intTestId );
			if( false == $objTestSubmission->insert( SYSTEM_USER_ID, $objAdminDatabase ) ) {
				return false;
			}

			$intTestSubmissionId	= $objTestSubmission->getId();
			$strBaseUrl				= CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN;
			$strTestUrl				= $strBaseUrl . '/testquestion/' . CEncryption::encrypt( $intQuestionId, '@#1~&G*' ) . '/testsubmission/' . CEncryption::encrypt( $intTestSubmissionId, '@#1~&G*' );
			$arrstrTestUrls[]		= '<a target="_blank" href=' . $strTestUrl . '> Click Here </a>';

			return $arrstrTestUrls;
		}
	}

	public function sendTechnicalTestInvitationEmail( $objEmployee, $objAdminDatabase, $objEmailDatabase, $boolIsDryRun = false, $strTestType = NULL ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		$objPsWebsiteJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );
		$this->setDepartmentId( $objPsWebsiteJobPosting->getDepartmentId() );
		$boolCodingTest = false;
		switch( $strTestType ) {
			case NULL:
				$arrstrTestUrls = $this->generateEmployeeJobPostingUrls( $objAdminDatabase );
				break;

			case 'technical coding test':
				$boolCodingTest = true;
				$arrstrTestUrls = $this->generateTechnicalTestUrl( $objAdminDatabase );
				break;

			default:
				$arrstrTestUrls = $this->generateEmployeeJobPostingUrls( $objAdminDatabase );
				break;
		}

		if( false == valArr( $arrstrTestUrls ) ) {
			return NULL;
		}

		$strSignature		= '';
		$arrobjSystemEmails = array();
		$objSmarty			= new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$this->loadCompanyData();

		if( true == valStr( $objEmployee->getEmailSignature() ) ) {
			$strSignature = $objEmployee->getEmailSignature();
		} elseif( CEmployee::ID_SYSTEM == $objEmployee->getId() ) {
			$strSignature = 'HR team<br/>' . $this->m_strOfficeAddress;
		} else {
			$strSignature = $objEmployee->getNameFull() . '</br>' . $objEmployee->getEmailAddress() . '</br>' . $this->m_strOfficeAddress;
		}

		$strHtmlContent = 'Dear ' . \Psi\CStringService::singleton()->ucfirst( $this->getNameFirst() ) . ',<br/><br/>';
		$strHtmlContent .= 'We thank you for taking interest in our organization and as a further step towards processing your candidature, we would like to request you to appear for the Technical ' . ( ( true == $boolCodingTest ) ? 'Coding' : '' ) . ' Test.';
		$strHtmlContent .= '<br/><br/>Kindly click on below mentioned link and proceed ahead.<br/><br/>' . implode( '<br><br>', $arrstrTestUrls );
		$strHtmlContent .= '<br/><br/>Please read the instructions carefully before you start taking the test and use your registered email id with us while adding basic personal information in the test initiation form.<br/>';
		$strHtmlContent .= '(Your registered email Id - <a href=\'' . $this->getEmailAddress() . '\'>' . $this->getEmailAddress() . '</a>)<br/><br/>';

		if( true == $boolCodingTest ) {
			$strHtmlContent .= 'Note:<br/>';
			$strHtmlContent .= '- The test should strictly be appeared within 24 hours of generation post which the link will be expired.<br/>';
			$strHtmlContent .= '- Once the test starts, it needs to be completed in a stipulated time.<br/>';
			$strHtmlContent .= '- Once the time gets up, code will automatically get submitted in the system and after that, you are required to close the window.<br/>';
			$strHtmlContent .= '- You may submit the test before the complete time gets up too as per your confidence in the code.<br/><br/>';
		}

		$strHtmlContent .= 'Please feel free to reach us in case you have a query before starting the test, you can reach us on the number mentioned in the signature line.<br/><br/>Thanks & Regards,<br/> ' . $strSignature;

		$objSmarty->assign( 'content', $strHtmlContent );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'America/Denver' );
		}

		$objSystemEmailLibrary = new CSystemEmailLibrary();

		$strSubject = ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'Technical ' . ( ( true == $boolCodingTest ) ? 'Coding' : '' ) . ' Test - ' . \Psi\CStringService::singleton()->ucfirst( $this->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucfirst( $this->getNameLast() ) : 'Technical ' . ( ( true == $boolCodingTest ) ? 'Coding' : '' ) . ' Test ';

		$objSystemEmail        = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress = $this->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress = $this->m_strCareerEmailAddress, $strReplyToEmailAddress = $this->m_strHrEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$objSystemEmailClone = clone $objSystemEmail;
		if( true == $objPsWebsiteJobPosting->getIsCampusDrive() ) {
			$objSystemEmailClone->setToEmailAddress( $this->m_strCareerEmailAddress );
		} else {
			$objSystemEmailClone->setToEmailAddress( $this->m_strHrEmailAddress );
		}
		$arrobjSystemEmails = array( $objSystemEmail, $objSystemEmailClone );

		switch( NULL ) {
			default:

				$boolValid     = true;

				$objEmailDatabase->begin();

				foreach( $arrobjSystemEmails as $objSystemEmail ) {
					if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {

						if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
								$boolValid = false;
								$objEmailDatabase->rollback();
								break;
						}
					}

					if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
						$this->setRecruitmentDriveDetails( $objAdminDatabase, SYSTEM_USER_ID, $this->getPsJobPostingStepStatusId() );
					}

					$this->updatePsJopPostingStatus( CPsJobPostingStatus::IN_PROCESS, 'Tech', CPsJobPostingStepType::TEST, $objAdminDatabase );

					if( false == $this->update( SYSTEM_USER_ID, $objAdminDatabase, $boolResumeRequired = false ) ) {
						return false;
					}
				}

				$objEmailDatabase->commit();

				if( $this->getCountryCode() == CCountry::CODE_INDIA ) {
					if( false == $boolIsDryRun && false == $this->insertEmployeeApplicationNote( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( 'Technical ' . ( ( true == $boolCodingTest ) ? 'Coding' : '' ) . ' test has been generated for the applicant.', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ), SYSTEM_USER_ID, $objAdminDatabase, ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( '<strong>Technical ' . ( ( true == $boolCodingTest ) ? 'Coding' : '' ) . ' test generated by ' . $objEmployee->getNameFull() . '</strong>', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ) ) ) {
						break;
					}
				} else {
					$strDescription = '<strong>Technical ' . ( ( true == $boolCodingTest ) ? 'Coding' : '' ) . ' test generated by ' . $objEmployee->getNameFull() . '</strong>';
					$strNote = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strDescription . '</br>' . 'Technical ' . ( ( true == $boolCodingTest ) ? 'Coding' : '' ) . ' test has been generated for the applicant.', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE );
					$objEmployeeApplicationNote = new CEmployeeApplicationNote();
					if( false == $objEmployeeApplicationNote->insertEmployeeApplicationNote( SYSTEM_USER_ID, $this->getId(), CNoteType::EMPLOYEE_APPLICATION_HISTORY, $strNote, $objAdminDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Unable to insert employee application note.' ) );
						$this->m_objAdminDatabase->rollback();
						break;
					}
				}

		}

		return $boolValid;
	}

	public function sendTestResultEmails( $intJobPostingStatusId, $objAdminDatabase, $objEmailDatabase, $boolFromHireselectEmail = false ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$arrstrEmployeeApplicationDetails = $this->prepareEmployeeApplicationDetails();

		$objSmarty	= new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		( CPsJobPostingStatus::SELECTED == $intJobPostingStatusId ) ? $objSmarty->assign( 'result', true ) : $objSmarty->assign( 'result', false );

		$objSmarty->assign( 'employee_application_details', $arrstrEmployeeApplicationDetails );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'XENTO_CAREERS_EMAIL_ADDRESS', CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		$strHtmlContent				= $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/campus_drive_feedback_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$objPsWebsiteJobPosting		= CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );
		$strTestName				= ( true == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) && CEmployeeApplicationScoreType::MRAB == $objPsWebsiteJobPosting->getEmployeeApplicationScoreTypeId() ) ? 'MRAB' : 'CCAT';
		$strCareerEmailAddress		= ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'CAREERS | XENTO SYSTEMS<' . CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS . '>' : 'CAREERS | ' . CONFIG_COMPANY_NAME . '<' . CSystemEmail::CAREERS_EMAIL_ADDRESS . '>';
		$strHrEmailAddress			= ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS : CSystemEmail::PROPERTYSOLUTIONS_HR_EMAIL_ADDRESS;

		$objSystemEmailLibrary		= new CSystemEmailLibrary();
		$objSystemEmail				= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strTestName . ' Test Feedback - ' . \Psi\CStringService::singleton()->ucfirst( $this->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucfirst( $this->getNameLast() ), $strHtmlContent, $this->getEmailAddress() . ',' . $strCareerEmailAddress, 0, $strCareerEmailAddress, $strHrEmailAddress );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType			= CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sending email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		switch( NULL ) {
			default:
				if( false == $boolFromHireselectEmail && ( CPsJobPostingStatus::SELECTED == $intJobPostingStatusId || CPsJobPostingStatus::REJECTED == $intJobPostingStatusId ) && false == $this->update( SYSTEM_USER_ID, $objAdminDatabase, $boolResumeRequired = false ) ) {
					return false;
				}
		}

		if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
			return false;
		}

		if( true == in_array( $intJobPostingStatusId, [ CPsJobPostingStatus::REJECTED, CPsJobPostingStatus::SELECTED ] ) && CCountry::CODE_INDIA == $this->getCountryCode() ) {
			$strDescription = ( CPsJobPostingStatus::REJECTED == $intJobPostingStatusId ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( '<strong>' . $strTestName . ' test rejected </strong>by <strong> System. </strong>', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ) : ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( '<strong>' . $strTestName . ' test selected </strong>by <strong> System. </strong>', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE );
			$strNote		= ( CPsJobPostingStatus::REJECTED == $intJobPostingStatusId ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strTestName . ' Test failure mail sent and application status of candidate has been changed to Rejected.', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ) : ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strTestName . ' Test success mail sent and application status of candidate has been changed to ' . $strTestName . ' Selected.', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE );
		}

		if( false == $boolFromHireselectEmail && true == in_array( $intJobPostingStatusId, [ CPsJobPostingStatus::REJECTED, CPsJobPostingStatus::SELECTED ] ) && false == $this->insertEmployeeApplicationNote( $strNote, SYSTEM_USER_ID, $objAdminDatabase, $strDescription ) ) {
			return false;
		}

		if( CPsJobPostingStatus::SELECTED == $intJobPostingStatusId && true == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) ) {
			$objEmployee = CEmployees::fetchEmployeeByUserId( SYSTEM_USER_ID, $objAdminDatabase );
			if( true == $objPsWebsiteJobPosting->getIsAutoSendCodingTest() && false == $this->sendTechnicalTestInvitationEmail( $objEmployee, $objAdminDatabase, $objEmailDatabase, false, 'technical coding test' ) ) {
				return false;
			}
			if( true == $objPsWebsiteJobPosting->getIsAutoSendTechnicalTest() && false == $this->sendTechnicalTestInvitationEmail( $objEmployee, $objAdminDatabase, $objEmailDatabase, false ) ) {
				return false;
			}
		}

		return true;
	}

	public function prepareEmployeeApplicationDetails() {
		$arrstrEmployeeApplicationDetails = array(
			'first_name'					=> $this->getNameFirst(),
			'last_name'						=> $this->getNameLast(),
			'ps_website_job_posting_name'	=> $this->getPsWebsiteJobPostingName()
		);

		return $arrstrEmployeeApplicationDetails;

	}

	public function generateEmployeeApplicationTestEvent( $objEmployee, $objAdminDatabase, $objEmailDatabase, $intUserId ) {

		$objPsWebsiteJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );
		if( false == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Failed to load pswebsite job posting object. ' ) );
			trigger_error( 'Insert job posting database fail', E_USER_ERROR );
			return false;
		}

		$this->setDepartmentId( $objPsWebsiteJobPosting->getDepartmentId() );
		if( CCountry::CODE_INDIA == $objPsWebsiteJobPosting->getCountryCode() ) {

			if( CPsWebsiteJobPosting::IT_RECRUITER == $objPsWebsiteJobPosting->getId() ) {
				$strTestUrl = 'https://www.ondemandassessment.com/link/index/JB-564E80A13';
			} else if( self::BACK_OFFICE_JOB_POSTING == $objPsWebsiteJobPosting->getId() ) {
				$strTestUrl = NULL;
			} else if( false == is_null( $objPsWebsiteJobPosting->getAssessmentUrl() ) && preg_match( '/^((https?:\/\/)[a-zA-Z0-9]+\.[a-zA-Z0-9]+)[-a-z0-9+&@#\/%?=_:.]*/', $objPsWebsiteJobPosting->getAssessmentUrl() ) ) {
				$strTestUrl = $objPsWebsiteJobPosting->getAssessmentUrl();
			} else if( true == in_array( $objPsWebsiteJobPosting->getId(), [ CPsWebsiteJobPosting::PHP_NINJA, CPsWebsiteJobPosting::PHP_SAMURAI, CPsWebsiteJobPosting::PHP_JED ] ) ) {
				$strTestUrl = 'https://www.ondemandassessment.com/link/index/JB-1S569M15L';
			} else {
				$strTestUrl = 'https://www.ondemandassessment.com/link/index/JB-EUPOH4I2Q';
			}
			if( false == $this->sendEmployeeApplicationTestEvent( $objEmployee, $strTestUrl, $intUserId, $objAdminDatabase, $objEmailDatabase, $objPsWebsiteJobPosting ) ) {
				trigger_error( 'Insert test event fail', E_USER_ERROR );
				return false;
			}
			return true;
		}

		if( CCountry::CODE_USA == $objPsWebsiteJobPosting->getCountryCode() ) {
			if( false == is_null( $objPsWebsiteJobPosting->getAssessmentUrl() ) && preg_match( '/^((https?:\/\/)[a-zA-Z0-9]+\.[a-zA-Z0-9]+)[-a-z0-9+&@#\/%?=_:.]*/', $objPsWebsiteJobPosting->getAssessmentUrl() ) ) {
				$strTestUrl = $objPsWebsiteJobPosting->getAssessmentUrl();
			} else {
				return false;
			}

			if( false == $this->sendEmployeeApplicationTestEvent( $objEmployee, $strTestUrl, $intUserId, $objAdminDatabase, $objEmailDatabase, $objPsWebsiteJobPosting ) ) {
				trigger_error( 'Insert test event fail', E_USER_ERROR );
				return false;
			}
			return true;
		}
	}

	public function sendEmployeeApplicationTestEvent( $objEmployee, $arrstrScheduledTestResults, $intUserId, $objAdminDatabase, $objEmailDatabase, $objPsWebsiteJobPosting = NULL, $boolResendTest = false ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			trigger_error( 'Insert email database fail', E_USER_ERROR );
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$strSignature						= '';
		$strEmployeeApplicationTestEventKey = ( true == valArr( $arrstrScheduledTestResults ) ) ? $arrstrScheduledTestResults[$this->getEmailAddress()] : $arrstrScheduledTestResults;

		$this->loadCompanyData();

		if( false == is_null( $this->getEmailAddress() || 0 < strlen( $this->getEmailAddress() ) ) ) {

			$strSubject = ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'Test Details - Xento Systems for ' . \Psi\CStringService::singleton()->ucfirst( $this->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucfirst( $this->getNameLast() ) : 'Test Details - ' . CONFIG_COMPANY_NAME . ' !!';
			if( true == valStr( $objEmployee->getEmailSignature() ) ) {
				$strSignature = $objEmployee->getEmailSignature();
			} elseif( CEmployee::ID_SYSTEM == $objEmployee->getId() ) {
				$strSignature = 'HR Team<br/>' . $this->m_strOfficeAddress;
			} else {
				$strSignature = $objEmployee->getNameFull() . '<br>' . $objEmployee->getEmailAddress() . '<br>' . $this->m_strOfficeAddress;
			}

			$arrobjSystemEmails = array();
			$objSmarty			= new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

			$objSmarty->assign( 'employee_application', $this );
			$objSmarty->assign( 'email_signature', $strSignature );
			$objSmarty->assign( 'COUNTRY_CODE_USA', CCountry::CODE_USA );
			$objSmarty->assign( 'CONFIG_COMPANY_NAME', CONFIG_COMPANY_NAME );
			$objSmarty->assign( 'HTTP_COMPANY_BASE_URL', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN );
			$objSmarty->assign( 'employee_application_test_event_id', $strEmployeeApplicationTestEventKey );
			$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
			$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
			$objSmarty->assign( 'country_code', $this->getCountryCode() );
			$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );
			$objSmarty->assign( 'JOB_POSTING_ID', $objPsWebsiteJobPosting->getId() );
			$objSmarty->assign( 'BACK_OFFICE_JOB_POSTING_ID', self::BACK_OFFICE_JOB_POSTING );

			$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/employee_application_test_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

			$objSystemEmailLibrary	= new CSystemEmailLibrary();
			$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress = $this->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress = $this->m_strCareerEmailAddress, $strReplyToEmailAddress = $this->m_strHrEmailAddress );

			$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

			$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

			// As we are directly sedning email so we have to set Email service provider id.
			if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
				$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
			}

			$strCareerEmailAddressForEntrata	= ( CDepartment::CALL_CENTER == $this->getDepartmentId() ) ? CSystemEmail::LEASING_CENTER_RECRUITMENT_EMAIL_ADDRESS : CSystemEmail::CAREERS_EMAIL_ADDRESS;
			$objSystemEmailClone = clone $objSystemEmail;
			$objSystemEmailClone->setToEmailAddress( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS : $strCareerEmailAddressForEntrata );
			$arrobjSystemEmails = array( $objSystemEmail, $objSystemEmailClone );
		}

		foreach( $arrobjSystemEmails as $objSystemEmail ) {
			if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
				if( false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
					$strError = $objSystemEmail->insert( $intUserId, $objEmailDatabase );
					trigger_error( 'Insert system email fail' . $strError, E_USER_ERROR );
					return false;
				}
			}
		}

		// update ps job posing step status according to action

		$strJobPostingStepName = ( true == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) && ( CEmployeeApplicationScoreType::MRAB == $objPsWebsiteJobPosting->getEmployeeApplicationScoreTypeId() ) ) ? 'MRAB' : 'CCAT';
		if( false == $this->updatePsJopPostingStatus( CPsJobPostingStatus::IN_PROCESS, $strJobPostingStepName, CPsJobPostingStepType::TEST, $objAdminDatabase ) ) {
			trigger_error( 'Insert update step fail', E_USER_ERROR );
			return false;
		}

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			$this->setRecruitmentDriveDetails( $objAdminDatabase, SYSTEM_USER_ID, $this->getPsJobPostingStepStatusId() );
		}

		if( false == $this->update( SYSTEM_USER_ID, $objAdminDatabase, $boolResumeRequired = false ) ) {
			trigger_error( 'Insert application fail', E_USER_ERROR );
			return false;
		}

		$objCreatedEmployee = CEmployees::fetchEmployeeByUserId( $intUserId, $objAdminDatabase );
		if( $this->getCountryCode() == CCountry::CODE_INDIA ) {
			if( false == $this->insertEmployeeApplicationNote( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( 'Test ( ' . $strEmployeeApplicationTestEventKey . ' ) is ' . ( ( true == $boolResendTest ) ? 'sent.' : 'created.' ), CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ), $intUserId, $objAdminDatabase, ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( '<strong>' . $strJobPostingStepName . ' test ' . ( ( true == $boolResendTest ) ? 'sent' : 'generated' ) . ' </strong>by <strong>' . $objCreatedEmployee->getNameFull() . '.</strong>', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ) ) ) {
				return false;
			}
		} else {
			$strNote = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( '<strong>' . $strJobPostingStepName . ' test ' . ( ( true == $boolResendTest ) ? 'sent' : 'generated' ) . ' </strong>by <strong>' . $objCreatedEmployee->getNameFull() . '.</strong></br>Test ( ' . $strEmployeeApplicationTestEventKey . ' ) is ' . ( ( true == $boolResendTest ) ? 'sent.' : 'created.' ), CONFIG_SODIUM_KEY_EMPLOYEE_NOTE );
			$objEmployeeApplicationNote = new CEmployeeApplicationNote();
			if( false == $objEmployeeApplicationNote->insertEmployeeApplicationNote( $intUserId, $this->getId(), CNoteType::EMPLOYEE_APPLICATION_HISTORY, $strNote, $objAdminDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Unable to insert employee application note.' ) );
				$this->m_objAdminDatabase->rollback();
				return false;
			}
		}

		return true;
	}

	public function sendEmployeeApplicationStatusEmail( $objEmployee, $objEmailDatabase, $boolIsCampusDrive = false, $boolIsFromRecruitingTab = false, $boolIsTelePhonicInterview = false, $boolSkypeInterview = false, $boolInterviewLocation = false ) {
		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$this->loadCompanyData();

		$strSubject = '';

		if( true == $boolIsFromRecruitingTab || ( true == is_null( $this->getRejectedOn() ) && false == is_null( $this->getScheduledOn() ) ) ) {
			$strSubject = ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'Interview Scheduled at Xento Systems !!' : 'Interview Scheduled at ' . CONFIG_COMPANY_NAME . ' !!';
		} elseif( false == is_null( $this->getRejectedOn() ) ) {
			$strSubject = ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'Interview Feedback - Xento Systems !!' : 'Interview Feedback - ' . CONFIG_COMPANY_NAME;
		}

		$boolIsPHPJobPosting = ( true == in_array( $this->getPsWebsiteJobPostingId(), CPsWebsiteJobPosting::$c_arrintPHPJobPostingIds ) ) ? true : false;

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		CEmployeeApplicationStatusType::loadSmartyConstants( $objSmarty );

		if( false === in_array( $boolInterviewLocation, [ CInterviewType::TELEPHONIC_INTERVIEW, CInterviewType::SKYPE_INTERVIEW_TYPE, CInterviewType::GOOGLE_MEET_INTERVIEW ] ) ) {
			$boolInterviewLocation = ( 'Interviewer\'s Cabin' === $boolInterviewLocation || false == \Psi\CStringService::singleton()->strpos( $boolInterviewLocation, 'Tower 8' ) ) ? 'Fourth Floor, Tower 9, SEZ, Magarpatta City, Pune 411 013' : 'Upper Ground Floor, Tower 8, SEZ, Magarpatta City, Pune 411 013';
		}

		if( true === in_array( $boolInterviewLocation, [ CInterviewType::SKYPE_INTERVIEW_TYPE, CInterviewType::GOOGLE_MEET_INTERVIEW ] ) ) {
			$boolInterviewLocation = CInterviewType::GOOGLE_MEET_INTERVIEW;
		}

		$objSmarty->assign( 'employee_application', $this );
		$objSmarty->assign( 'employee', $objEmployee );
		$objSmarty->assign( 'interview_location', $boolInterviewLocation );
		$objSmarty->assign( 'is_from_recruting_tab', $boolIsFromRecruitingTab );
		$objSmarty->assign( 'is_telephonic_interview', $boolIsTelePhonicInterview );
		$objSmarty->assign( 'is_skype_interview', $boolSkypeInterview );
		$objSmarty->assign( 'is_php_job_posting', $boolIsPHPJobPosting );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME', CONFIG_COMPANY_NAME );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		if( true == $boolIsCampusDrive ) {
			$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/campus_drive_employee_applications_status_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		} else {
			$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/employee_applications_status_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		}

		// Adding career email address with to email address because cc doesn't with sendgrid provider.
		$strToEmailAddress = ' ' . $this->getEmailAddress() . ', ' . $this->m_strCareerEmailAddress . ' ';

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'America/Denver' );
		}

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress, $intIsSelfDestruct = 0, $strFromEmailAddress = $this->m_strCareerEmailAddress );

		// Scheduling rejection email to be sent after 12 hours. [PHK]

		if( false == $boolIsFromRecruitingTab && false == is_null( $this->getRejectedOn() ) ) {
			$objSystemEmail->setScheduledSendDatetime( date( 'm/d/Y H:i:s', strtotime( '+6 hours' ) ) );
		}

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$boolEmailSent = true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent = false;
		}

		return $boolEmailSent;
	}

	public function sendEmployeeApplicationJoinedEmail( $objEmailDatabase ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$this->loadCompanyData();

		$strUrlSuffix	= ( true === CONFIG_IS_SECURE_URL ) ? 'https' : 'http';
		$strLinkUrl		= $strUrlSuffix . '://' . basename( $_SERVER['HTTP_HOST'] );
		$strHtmlContent = 'Hi,<br/><br/>The applicant ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' status has been changed to "Joined". Please click the below link to add ' . $this->getNameFirst() . ' as an employee.</br></br>
							<a href=\'' . $strLinkUrl . '/?module=employee_applications-new&action=view_employee_applications&employee_application[id]=' . $this->getId() . '\'>Click to view employee application</a></br></br>';
		$strSubject		= 'Employee Application status changed to joined.';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );
		$objSmarty->assign( 'content', $strHtmlContent );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'America/Denver' );
		}

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress = $this->m_strHrEmailAddress, $intIsSelfDestruct = 0, $strFromEmailAddress = $this->m_strCareerEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$boolEmailSent = true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent = false;
		}

		return $boolEmailSent;
	}

	public function sendEmployeeApplicationBlacklistedEmail( $objEmployee, $objEmailDatabase, $objAdminDatabase ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$strSignature = '';
		$this->loadCompanyData();

		$strUrlSuffix = ( true === CONFIG_IS_SECURE_URL ) ? 'https' : 'http';
		$strLinkUrl   = $strUrlSuffix . '://' . basename( $_SERVER['HTTP_HOST'] );

		$objPsWebsiteJobPostingName = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );

		if( true == valStr( $objEmployee->getEmailSignature() ) ) {
			$strSignature = $objEmployee->getEmailSignature();
		} elseif( CEmployee::ID_SYSTEM == $objEmployee->getId() ) {
			$strSignature = 'HR team<br/>' . $this->m_strOfficeAddress;
		} else {
			$strSignature = $objEmployee->getNameFull() . '</br>' . $objEmployee->getEmailAddress() . '</br>' . $this->m_strOfficeAddress;
		}

		$strHtmlContent = '<div font:normal normal bold 15px/18px arial >
			<strong>Dear ' . \Psi\CStringService::singleton()->ucfirst( $this->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucfirst( $this->getNameLast() ) . ',</strong></div>
			<div font:normal normal normal 11px/18px arial; text-align:justify;text-align:left; margin-bottom:10px;">
			<p>We thank you for your interest in applying for the ' . ( $objPsWebsiteJobPostingName->getName() ) . ' role with us. However due to the unprofessional approach extended by yourself during the recruitment process, we regret to inform you that your candidature has now been blacklisted with us and you will not be able to apply for any of the job vacancies in the future.
			We initiated the recruitment process in good faith after evaluating your profile but unfortunately it is quite apparent that the quality that we are seeking is not something you posses.</p></div>
			</dt></dl>
			<dl><dt>Wishing you all the very best for all your future endeavors.</dt></dl>
			<dl><dt>Thanks and Regards,</dt></dl>
			<dl></dt>' . $strSignature . '</dt></dl>';

		$strSubject = ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'Employment Application Feedback - Xento Systems !!' : 'Employment Application Feedback - ' . CONFIG_COMPANY_NAME . ' !!';
		$objSmarty  = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'content', $strHtmlContent );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'America/Denver' );
		}

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress = $this->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress = $this->m_strHrEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$objSystemEmailClone = clone $objSystemEmail;
		$objSystemEmailClone->setToEmailAddress( $this->m_strCareerEmailAddress );

		$arrobjSystemEmails = array( $objSystemEmail, $objSystemEmailClone );

		$boolValid	 = true;

		foreach( $arrobjSystemEmails as $objSystemEmail ) {
			if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {

				if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
					$boolValid = false;
					break;
				}
			}
		}

		return $boolValid;
	}

	public function sendPositionNotOpenedEmail( $objUser, $objEmailDatabase ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$this->loadCompanyData();

		$strUrlSuffix	= ( true === CONFIG_IS_SECURE_URL ) ? 'https' : 'http';
		$strLinkUrl		= $strUrlSuffix . '://' . basename( $_SERVER['HTTP_HOST'] );
		$strSignature	= 'HR Team<br/>' . $this->m_strOfficeAddress;
		$strHtmlContent = '<dl><dt>
		<strong>Dear ' . $objUser->getEmployee()->getNameFull() . ',</strong></dt></dl>
		<dl><dt>
		<p>Thanks for sharing your reference. We currently do not have any requirement for the profile that you have shared with us. However we will keep your referred profile in our database and get in touch with the concerned candidate, once we have the position open.
		</br>We appreciate your help for recruitment initiatives and look forward to you referring more friends in the future too.</p>
		</dt></dl>
		<dl><dt>Best Regards,</dt></dl>
		<dl><dt>' . $strSignature . '</dt></dl>';

		$strSubject = 'Thanks For Your Reference';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'content', $strHtmlContent );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $objUser->getEmployee()->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress = $this->m_strCareerEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$boolEmailSent	= true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent	= false;
		}

		return $boolEmailSent;
	}

	public function sendPositionOpenedEmail( $objUser, $objEmailDatabase ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$this->loadCompanyData();

		$strSignature	= 'HR Team<br/>' . $this->m_strOfficeAddress;
		$strHtmlContent = '<dl><dt>
		Dear ' . $objUser->getEmployee()->getNameFull() . ',</dt></dl>
		<p><dt>
		<p>Thanks for sharing <b>' . \Psi\CStringService::singleton()->ucfirst( $this->getNameFirst() ) . '</b><b> ' . \Psi\CStringService::singleton()->ucfirst( $this->getNameLast() ) . ' </b> for the position <b>' . $this->getPsWebsiteJobPostingName() . '</b>.</p>
		<p>Our Talent Acquisition Team will reach out to him/her as soon as they evaluate the profile and find a suitable place. You can check further updates and status from <b> Dashboard>>References </b>.</p>
		<p>We appreciate your help in recruitment inititatives and look forward to you referring more friends in future too.</p>
		</dt></dl>
		<dl><dt>Best Regards,</dt>
		<dt>' . $strSignature . '</dt></dl>';

		$strSubject = 'Thanks For Your Reference';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'content', $strHtmlContent );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $objUser->getEmployee()->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress = $this->m_strCareerEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$boolEmailSent	= true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent	= false;
		}

		return $boolEmailSent;
	}

	public function sendEmployeeApplicationNewStatusEmail( $objEmailDatabase ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		$this->loadCompanyData();

		$strSignature = 'HR Team<br/>' . $this->m_strOfficeAddress;

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'employee_application', $this );
		$objSmarty->assign( 'COUNTRY_CODE_USA', CCountry::CODE_USA );
		$objSmarty->assign( 'email_signature', $strSignature );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME', CONFIG_COMPANY_NAME );
		$objSmarty->assign( 'CONFIG_COMPANY_BASE_DOMAIN', CONFIG_COMPANY_BASE_DOMAIN );
		$objSmarty->assign( 'HTTP_COMPANY_BASE_URL', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );
		$objSmarty->assign( 'DEPARTMENT_CALL_CENTER', CDepartment::CALL_CENTER );

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			$strSubject = 'Thank You For Applying';
		} else {
			$strSubject = 'Thank You For Applying!';
			$this->m_strCareerEmailAddress = ( CDepartment::CALL_CENTER == $this->getDepartmentId() ) ? CSystemEmail::LEASING_CENTER_RECRUITMENT_EMAIL_ADDRESS : CSystemEmail::ENTRATA_RECRUITING_EMAIL_ADDRESS;
		}

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/employee_application_applying_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress = $this->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress = $this->m_strCareerEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$boolEmailSent	= true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent	= false;
		}

		return $boolEmailSent;

	}

	public function sendEmployeeApplicationReferralUpdateEmail( $objEmailDatabase, $objUser ) {
		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$this->loadCompanyData();

		$strSubject = $this->getPsWebsiteJobPostingName() . ' - Employee Application Updated';
		$strSignature = 'HR Team<br/>' . $this->m_strOfficeAddress;
		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		CEmployeeApplicationStatusType::loadSmartyConstants( $objSmarty );

		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME', CONFIG_COMPANY_NAME );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'applicant_name', $this->getNameFull() );
		$objSmarty->assign( 'updated_by', $objUser->getEmployee()->getPreferredName() );
		$objSmarty->assign( 'signature', $strSignature );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/references/employee_application_referral_update_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$strCareersEmailAddress 	= CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS;
		$objSystemEmailLibrary		= new CSystemEmailLibrary();
		$objSystemEmail				= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strCareersEmailAddress, $intIsSelfDestruct = 0, $strCareersEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}
		$boolEmailSent = true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent = false;
		}
		return $boolEmailSent;
	}

	public function sendEmployeeApplicationJobPostingUpdateEmail( $objEmailDatabase, $objUser ) {
		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$this->loadCompanyData();

		$objPsWebsiteJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $this->m_objDatabase );

		if( true == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) ) {
			$strJobPostingName = $objPsWebsiteJobPosting->getName();
			$boolJobPostingIsPublished = $objPsWebsiteJobPosting->getIsPublished();
		}

		$intPsWebsiteJobPostingId	= base64_encode( $this->getPsWebsiteJobPostingId() );
		$intEmployeeApplicationId	= base64_encode( $this->getId() );
		$strUrlSuffix				= ( true === CONFIG_IS_SECURE_URL ) ? 'https' : 'http';
		$strEmployeeApplicationUrl	= $strUrlSuffix . '://' . CONFIG_COMPANY_BASE_DOMAIN . '/careers/xento/JP/' . $intPsWebsiteJobPostingId . '/EA/' . $intEmployeeApplicationId;

		$strSubject      = ' Updated Your Job Posting ';
		$strSignature    = 'HR Team <br />' . $this->m_strOfficeAddress;
		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		CEmployeeApplicationStatusType::loadSmartyConstants( $objSmarty );

		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME', CONFIG_COMPANY_NAME );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'applicant_name', $this->getNameFull() );
		$objSmarty->assign( 'updated_by', $objUser->getEmployee()->getPreferredName() );
		$objSmarty->assign( 'signature', $strSignature );
		$objSmarty->assign( 'update_url', $strEmployeeApplicationUrl );
		$objSmarty->assign( 'job_posting', $strJobPostingName );
		$objSmarty->assign( 'is_published', $boolJobPostingIsPublished );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/references/employee_application_job_posting_update_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$strCareersEmailAddress 	= CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS;
		$strToEmailAddress          = $this->getEmailAddress() . ',' . $strCareersEmailAddress;

		$objSystemEmailLibrary	    = new CSystemEmailLibrary();
		$objSystemEmail			    = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress, $intIsSelfDestruct = 0, $strCareersEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}
		$boolEmailSent = true;
		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent = false;
		}
		return $boolEmailSent;
	}

	public function sendUploadResumeEmail( $objEmailDatabase, $objAdminDatabase = NULL, $intCurrentUserId = NULL ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}
       if( false == is_null( $intCurrentUserId ) ) {
	       $objEmployee	= CEmployees::fetchEmployeeById( $intCurrentUserId, $objAdminDatabase );
       } else {
	       $objEmployee	= CEmployees::fetchEmployeeById( $this->getRefereeEmployeeId(), $objAdminDatabase );
       }
		$objPsWebsiteJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$intPsWebsiteJobPostingId = base64_encode( $objPsWebsiteJobPosting->getId() );
		$intEmployeeApplicationId = base64_encode( $this->getId() );

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'Asia/Calcutta' );
		}

		$strSignature = $objEmployee->getPreferredName() . '<br/>' .
						'Xento Systems Pvt. Ltd.';

		$strUrlSuffix				= ( true === CONFIG_IS_SECURE_URL ) ? 'https' : 'http';
		$strEmployeeApplicationUrl	= $strUrlSuffix . '://' . CONFIG_COMPANY_BASE_DOMAIN . '/careers/xento/JP/' . $intPsWebsiteJobPostingId . '/EA/' . $intEmployeeApplicationId;

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'full_name', \Psi\CStringService::singleton()->ucwords( $this->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucwords( $this->getNameLast() ) );
		$objSmarty->assign( 'job_posting', \Psi\CStringService::singleton()->ucwords( $objPsWebsiteJobPosting->getName() ) );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'signature', \Psi\CStringService::singleton()->ucwords( $strSignature ) );
		$objSmarty->assign( 'upload_link', $strEmployeeApplicationUrl );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		$strSubject		= 'Xento Systems Application Update';

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/references/employee_application_resume_upload_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'America/Denver' );
		}

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress = $this->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress = $objEmployee->getEmailAddress() );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$boolEmailSent	= true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent = false;
		}

		return $boolEmailSent;

	}

	public function sendEmployeeResumeEmail( $objEmailDatabase, $objAdminDatabase ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		$objPsWebsiteJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'first_name', \Psi\CStringService::singleton()->ucwords( $this->getNameFirst() ) );
		$objSmarty->assign( 'last_name', \Psi\CStringService::singleton()->ucwords( $this->getNameLast() ) );
		$objSmarty->assign( 'job_posting', \Psi\CStringService::singleton()->ucwords( $objPsWebsiteJobPosting->getName() ) );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/employee_resume_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$strSubject		= 'Employment Application Submitted by ' . \Psi\CStringService::singleton()->ucwords( $this->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucwords( $this->getNameLast() ) . ' for ' . \Psi\CStringService::singleton()->ucwords( $objPsWebsiteJobPosting->getName() );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress = ( CDepartment::CALL_CENTER == $this->getDepartmentId() ) ? CSystemEmail::LEASING_CENTER_RECRUITMENT_EMAIL_ADDRESS : CSystemEmail::RESUMES_EMAIL_ADDRESS );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		if( true === $this->valPsDocumentId() ) {
			$objPsDocument		= $this->fetchPsDocument( $objAdminDatabase );
			$objStorageGateway	= CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED );
		}

		if( true === valObj( $objStorageGateway, 'CProxyObjectStorageGateway' ) && true === valObj( $objPsDocument, 'CPsDocument' ) ) {

			// desired file location using temp path
			$strTempFilePath		= CObjectStorageUtils::getTemporaryPath( PATH_MOUNTS_TEMP_DOCUMENTS, $this->getId() ) . $objPsDocument->getFileName();
			$arrmixPathInfo			= pathinfo( $strTempFilePath );
			$strFileTitle			= ( true === valStr( $objPsDocument->getTitle() ) ) ? $objPsDocument->getTitle() : '_resume';
			$strFileTitle			= \Psi\Libraries\ExternalFileUpload\CFileUpload::cleanFilename( str_replace( '.', '_', $strFileTitle ) );
			$strUpdatedFilePath		= $arrmixPathInfo['dirname'] . '/' . $strFileTitle . '.' . $arrmixPathInfo['extension'];

			$objPsDocument->setCid( CClient:: ID_DEFAULT );
			$objPsDocument->setEmployeeApplicationId( $this->getId() );
			$objStoredObject			= $objPsDocument->fetchStoredObject( $objAdminDatabase );
			$arrmixRequest				= $objStoredObject->createGatewayRequest( [ 'mountSystem' => 'NON_BACKUP_MOUNTS', 'outputFile' => $strUpdatedFilePath ] );
			$arrmixResponse				= $objStorageGateway->getObject( $arrmixRequest );

			if( false == $arrmixResponse->hasErrors() ) {
				$objEmailAttachment = new CEmailAttachment();
				$objEmailAttachment->setFilePath( dirname( $arrmixResponse['outputFile'] ) );
				$objEmailAttachment->setTitle( $strFileTitle );
				$objEmailAttachment->setFileName( basename( $arrmixResponse['outputFile'] ) );
				$objSystemEmail->addEmailAttachment( $objEmailAttachment );
			}
		}

		$objEmailDatabase->begin();

		if( true === valArr( $objSystemEmail->getEmailAttachments() ) && false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
			$objEmailDatabase->rollback();

			return false;
		}

		$objEmailDatabase->commit();

		return true;
	}

	public function sendReferenceAddedNotificationEmail( $objEmailDatabase, $objAdminDatabase, $arrmixReferFriend ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		$objPsWebsiteJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );
		$objEmployee 			= CEmployees::fetchActiveEmployeeByEmployeeNumber( $arrmixReferFriend['employee_number'], $objAdminDatabase );

		if( false == empty( $arrmixReferFriend['employee_number'] ) ) {
			$strFromEmailAddress = $objEmployee->getEmailAddress();
		} else {
			$strFromEmailAddress = $arrmixReferFriend['email_address'];
		}

		$intPsWebsiteJobPostingId = base64_encode( $objPsWebsiteJobPosting->getId() );
		$intEmployeeApplicationId = base64_encode( $this->getId() );

		$objSmarty 					= new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );
		$strUrlSuffix				= ( true === CONFIG_IS_SECURE_URL ) ? 'https' : 'http';
		$strEmployeeApplicationUrl	= $strUrlSuffix . '://' . CONFIG_COMPANY_BASE_DOMAIN . '/careers/xento/JP/' . $intPsWebsiteJobPostingId . '/EA/' . $intEmployeeApplicationId;

		$objSmarty->assign( 'full_name', \Psi\CStringService::singleton()->ucwords( $this->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucwords( $this->getNameLast() ) );
		$objSmarty->assign( 'referee_full_name', \Psi\CStringService::singleton()->ucwords( $arrmixReferFriend['first_name'] ) . ' ' . \Psi\CStringService::singleton()->ucwords( $arrmixReferFriend['last_name'] ) );
		$objSmarty->assign( 'job_posting', \Psi\CStringService::singleton()->ucwords( $objPsWebsiteJobPosting->getName() ) );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'upload_link', $strEmployeeApplicationUrl );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		if( CPsWebsiteJobPosting::$c_arrstrRefereeTypes[1] == $arrmixReferFriend['referee_type'] ) {
			$objSmarty->assign( 'signature', \Psi\CStringService::singleton()->ucwords( $objEmployee->getEmailSignature() ) );
			$objSmarty->assign( 'current_employee', true );
		}
		$strSubject		= 'Xento Systems Application Update';
		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/reference_added_notification_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $this->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$boolEmailSent = true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent = false;
		}

		return $boolEmailSent;
	}

	public function sendEmployeeApplicationEditInfoEmail( $objEmailDatabase, $objEmployee, $boolFromTestLink = false, $boolFromAddApplication = false, $strRefereeEmployeeName = NULL, $strJobPostingName = NULL ) {
		if( false == valObj( $objEmailDatabase, 'CDatabase' ) || false == valObj( $objEmployee, 'CEmployee' ) ) {
			return false;
		}
		$intPsWebsiteJobPostingId	= base64_encode( $this->getPsWebsiteJobPostingId() );
		$intCompletedStepCount		= base64_encode( 0 );
		$intEmployeeApplicationId	= base64_encode( $this->getId() );
		$strUrlSuffix				= ( true === CONFIG_IS_SECURE_URL ) ? 'https' : 'http';

		if( true == $boolFromTestLink ) {
			$strEmployeeApplicationUrl	= $strUrlSuffix . '://' . CONFIG_COMPANY_BASE_DOMAIN . '/careers/xento/JP/' . $intPsWebsiteJobPostingId . '/EA/' . $intEmployeeApplicationId . '/EL/' . $intCompletedStepCount;
			$strFromEmailAddress		= CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS;
		} else {
			$strEmployeeApplicationUrl	= $strUrlSuffix . '://' . CONFIG_COMPANY_BASE_DOMAIN . '/careers/xento/JP/' . $intPsWebsiteJobPostingId . '/EA/' . $intEmployeeApplicationId;
			$strFromEmailAddress		= $objEmployee->getEmailAddress();
		}
		if( true == $boolFromAddApplication ) {
			$strEmployeeApplicationUrl .= '/' . base64_encode( 'byHrTeam' );
		}
		$objSmarty 					= new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );
		$objSmarty->assign( 'name', \Psi\CStringService::singleton()->ucwords( $this->getNameFirst() ) );
		$objSmarty->assign( 'recruiter_name', \Psi\CStringService::singleton()->ucwords( $objEmployee->getNameFull() ) );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'edit_link', $strEmployeeApplicationUrl );
		$objSmarty->assign( 'signature', ( false == is_null( $objEmployee->getEmailSignature() ) ) ? $objEmployee->getEmailSignature() : $objEmployee->getNameFull() . '</br> HR - Recruitment Team' . '</br>' . $this->m_strOfficeAddress );
		$objSmarty->assign( 'from_edit_link', $boolFromTestLink );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );
		$objSmarty->assign( 'JOB_POSTING_NAME', $strJobPostingName );
		$objSmarty->assign( 'REFEREE_EMPLOYEE_NAME', $strRefereeEmployeeName );

		$strHtmlContent 		= $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/employee_application_edit_basic_info_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, 'Update Xento Systems Application', $strHtmlContent, $this->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress );
		$objSystemEmail->setCcEmailAddress( CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS );

		if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
			if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function sendUpdateInfoEmail( $objEmailDatabase, $objAdminDatabase = NULL, $intCurrentUserId = NULL ) {

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}
		if( false == is_null( $intCurrentUserId ) ) {
			$objEmployee	= CEmployees::fetchEmployeeById( $intCurrentUserId, $objAdminDatabase );
		}

		$objPsWebsiteJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$intPsWebsiteJobPostingId = base64_encode( $objPsWebsiteJobPosting->getId() );
		$intEmployeeApplicationId = base64_encode( $this->getId() );

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'Asia/Calcutta' );
		}

		$this->loadCompanyData();
		$strSignature = 'HR Team<br/>' . $this->m_strOfficeAddress;

		$strUrlSuffix				= ( true === CONFIG_IS_SECURE_URL ) ? 'https' : 'http';
		$strEmployeeApplicationUrl	= $strUrlSuffix . '://' . CONFIG_COMPANY_BASE_DOMAIN . '/careers/xento/JP/' . $intPsWebsiteJobPostingId . '/EA/' . $intEmployeeApplicationId;

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'full_name', \Psi\CStringService::singleton()->ucwords( $this->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucwords( $this->getNameLast() ) );
		$objSmarty->assign( 'employee_name', \Psi\CStringService::singleton()->ucwords( $objEmployee->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucwords( $objEmployee->getNameLast() ) );
		$objSmarty->assign( 'job_posting', \Psi\CStringService::singleton()->ucwords( $objPsWebsiteJobPosting->getName() ) );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'signature', \Psi\CStringService::singleton()->ucwords( $strSignature ) );
		$objSmarty->assign( 'upload_link', $strEmployeeApplicationUrl );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		$strSubject		= 'Xento Systems Application Update';

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/references/employee_application_update_info_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'America/Denver' );
		}

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress = $this->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress = $objEmployee->getEmailAddress() );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		// As we are directly sedning email so we have to set Email service provider id.
		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$boolEmailSent	= true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent = false;
		}

		return $boolEmailSent;

	}

	// need to add email function for add reference from carrer page

	public function loadCompanyData() {
		$strCareerEmailAddressForEntrata	= ( CDepartment::CALL_CENTER == $this->getDepartmentId() ) ? CSystemEmail::LEASING_CENTER_RECRUITMENT_EMAIL_ADDRESS : CSystemEmail::CAREERS_EMAIL_ADDRESS;
		$strEmailAddress					= ( CDepartment::CALL_CENTER == $this->getDepartmentId() ) ? CSystemEmail::LEASING_CENTER_RECRUITMENT_EMAIL_ADDRESS : CSystemEmail::RESUMES_EMAIL_ADDRESS;

		$this->m_strCareerEmailAddress 		= ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'CAREERS | XENTO SYSTEMS<' . CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS . '>' : 'CAREERS | ' . CONFIG_COMPANY_NAME . '<' . $strCareerEmailAddressForEntrata . '>';
		$this->m_strHrEmailAddress			= ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS : CSystemEmail::PROPERTYSOLUTIONS_HR_EMAIL_ADDRESS;
		$this->m_strOfficeAddress			= ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? self::XENTO_COMPANY_NAME . '<br/>Office: ' . self::XENTO_OFFICE_PHONE_NUMBER . '<br/>Email : ' . CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS . '<br/>' . self::XENTO_COMPANY_FULL_ADDRESS . '<br/>' : CONFIG_COMPANY_NAME . '<br/>Office : ' . self::ENTRATA_OFFICE_PHONE_NUMBER . '<br/>Email : ' . $strEmailAddress . '<br/> ' . CONFIG_COMPANY_NAME_FULL . '<br/>';
		return true;
	}

	public function insertEmployeeApplicationNote( $strNote, $intCurrentUserId, $objAdminDatabase, $strNoteDescription, $intEmployeeApplicationStepId = NULL, $intActionTypeId = CActionType::NOTE, $intRecruiterEmployeeId = NULL ) {

		$objAction = new CAction();
		$objAction->setActionResultId( CActionResult::EMPLOYEE_APPLICATION );
		$objAction->setPrimaryReference( $intEmployeeApplicationStepId );
		$objAction->setSecondaryReference( $this->m_intId );
		$objAction->setActionTypeId( $intActionTypeId );
		$objAction->setActionDescription( $strNoteDescription );
		$objAction->setNotes( $strNote );
		$objAction->setEmployeeId( $intRecruiterEmployeeId );

		if( false == $objAction->insert( $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		return true;
	}

	public function insertEmployeeApplicationSourceNote( $strNote, $intCurrentUserId, $objAdminDatabase, $strNoteDescription, $intOldEmployeeReferenceId = NULL, $intOldEmployeeApplicationSourceTypeId = NULL, $intActionTypeId = CActionType::NOTE ) {

		$objAction = new CAction();
		$objAction->setActionResultId( CActionResult::EMPLOYEE_APPLICATION );
		$objAction->setPrimaryReference( $intOldEmployeeReferenceId );
		$objAction->setSecondaryReference( $this->m_intId );
		$objAction->setActionTypeId( $intActionTypeId );
		$objAction->setActionDescription( $strNoteDescription );
		$objAction->setActionMinutes( $intOldEmployeeApplicationSourceTypeId );
		$objAction->setNotes( $strNote );

		if( true == valObj( $objAction, CAction::class ) && false == $objAction->insert( $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		return true;
	}

	public function insertEmployeeApplicationStepByPsJobPostingStepStatusId( $intPsJobPostingStepStatus, $intCurrentUserId, $objDatabase ) {

		$objEmployeeApplicationStep = $this->createEmployeeApplicationStep( $intPsJobPostingStepStatus );
		$this->setPsJobPostingStepStatusId( $intPsJobPostingStepStatus );

		switch( NULL ) {
			default:

				if( false == $objEmployeeApplicationStep->insert( $intCurrentUserId, $objDatabase ) ) {
					$this->m_objAdminDatabase->rollback();
					return false;
				}

				if( false == $this->update( $intCurrentUserId, $objDatabase ) ) {
					$this->m_objAdminDatabase->rollback();
					return false;
				}
		}

		return true;
	}

	public function updatePsJopPostingStatus( $intJobPostingStatusId, $strJobPostingStepName, $intJobPostingStepTypeId, $objDatabase,  $intEmployeeId = NULL ) {

		$objPsJobPostingStepStatus = CPsJobPostingStepStatuses::fetchPsJobPostingStepStatusByPsJobPostingIdByPsJobPostingStatusIdByPsJobPostingStepName( $this->getPsWebsiteJobPostingId(), $intJobPostingStatusId, $strJobPostingStepName, $objDatabase, $intJobPostingStepTypeId );

		if( true == valObj( $objPsJobPostingStepStatus, 'CPsJobPostingStepStatus' ) ) {
			$this->setPsJobPostingStepStatusId( $objPsJobPostingStepStatus->getId() );
			$arrmixPrAssociationDetails = $this->checkForPrAssociation( $objDatabase );

			$objEmployeeApplicationStep = $this->createEmployeeApplicationStep( $objPsJobPostingStepStatus->getId(), $intEmployeeId );
			$objEmployeeApplicationStep->setPurchaseRequestId( ( true == valArrKeyExists( $arrmixPrAssociationDetails, 'purchase_request_id' ) ) ? $arrmixPrAssociationDetails['purchase_request_id'] : NULL );
			if( CPsJobPostingStepType::INTERVIEW == $intJobPostingStepTypeId ) {
				$intInterviewScheduledRecordCount = CEmployeeApplicationSteps::createService()->fetchEmployeeApplicationStepByCommentByEmployeeApplicationId( $this->getId(), $strJobPostingStepName . ' Scheduled', $objDatabase );

				if( 0 < $intInterviewScheduledRecordCount ) {
					$objEmployeeApplicationStep->setComments( $strJobPostingStepName . ' Re-Scheduled' );
				} else {
					$objEmployeeApplicationStep->setComments( $strJobPostingStepName . ' Scheduled' );
				}
			}

			if( false == $objEmployeeApplicationStep->insert( SYSTEM_USER_ID, $objDatabase ) ) {
				trigger_error( 'Insert in step fail', E_USER_ERROR );
				return false;
			}
			return true;
		}
		return false;
	}

	public function sendDocumentsUploadEmail( $objEmployee, $objEmailDatabase ) {
		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		$strAttachmentFilePath	= PATH_MOUNTS_GLOBAL_HR_DOCS . 'background_verification_form.pdf';

		$this->loadCompanyData();

		if( true == valStr( $objEmployee->getEmailSignature() ) ) {
			$strSignature = $objEmployee->getEmailSignature();
		} elseif( CEmployee::ID_SYSTEM == $objEmployee->getId() ) {
			$strSignature = 'HR team<br/>' . $this->m_strOfficeAddress;
		} else {
			$strSignature = $objEmployee->getNameFull() . '</br>' . $objEmployee->getEmailAddress() . '</br>' . $this->m_strOfficeAddress;
		}

		$boolIsStudent = ( CEmploymentOccupationType::STUDENT == $this->getEmploymentOccupationTypeId() ) ? true : false;
		$strUploadUrl  = '"' . CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/careers/xento/upload_documents/' . base64_encode( $this->getId() ) . '/' . base64_encode( $this->getEmailAddress() ) . '"';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'name_full', \Psi\CStringService::singleton()->ucwords( $this->getNameFirst() ) . ' ' . \Psi\CStringService::singleton()->ucwords( $this->getNameLast() ) );
		$objSmarty->assign( 'signature', $strSignature );
		$objSmarty->assign( 'is_student', $boolIsStudent );
		$objSmarty->assign( 'upload_link', $strUploadUrl );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/employee_application_documents_upload_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject = 'Pre-Joining Documents Required.', $strHtmlContent, $this->getEmailAddress(), $intIsSelfDestruct = 0, $this->m_strCareerEmailAddress, $this->m_strCareerEmailAddress );

		$objSystemEmail->setBccEmailAddress( $this->m_strCareerEmailAddress );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );
		if( true == file_exists( $strAttachmentFilePath ) ) {
			$objEmailAttachment = new CEmailAttachment();
			$objEmailAttachment->setFilePath( dirname( $strAttachmentFilePath ) );
			$objEmailAttachment->setFileName( basename( $strAttachmentFilePath ) );
			$objSystemEmail->addEmailAttachment( $objEmailAttachment );
		}

		$objEmailDatabase->begin();
		if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
			$objEmailDatabase->rollback();

			return false;
		}

		$objEmailDatabase->commit();

		return true;
	}

	public function moveApplicationDocuments( $arrobjDocuments, $intCurrentUserId, $objAdminDatabase ) {

		if( false == valArr( $arrobjDocuments ) ) {
			return false;
		}

		$strFromPath = PATH_NON_BACKUP_MOUNTS . 'Global/ps_website/employee_resumes/' . $this->getId() . '/';
		$strToPath   = PATH_MOUNTS . 'documents/employees/' . $this->getEmployeeId() . '/';

		if( false == CFileIO::recursiveMakeDir( $strToPath ) ) {
			return false;
		}

		foreach( $arrobjDocuments as $objDocument ) {
			$objDocument->setEmployeeId( $this->getEmployeeId() );
			if( false == CFileIO::copyFile( $strFromPath . $objDocument->getFileName(), $strToPath . $objDocument->getFileName() ) ) {
				return false;
			}
		}

		if( false == CPsDocuments::bulkUpdate( $arrobjDocuments, array( 'employee_id' ), $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		return true;
	}

	public function sendApplicantInterviewScheduledEmail( $strInterviewTime, $strInteviewLocation, $arrobjInterviewerEmployees, $objEmailDatabase, $objAdminDatabase ) {

		if( true == is_null( $strInterviewTime ) || true == is_null( $strInteviewLocation ) || false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		$strJobPostingName		= NULL;
		$arrstrInterviewers		= array();
		$objPsWebsiteJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );

		if( true == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) ) {
			$strJobPostingName = $objPsWebsiteJobPosting->getName();
		}

		if( true == valArr( $arrobjInterviewerEmployees ) ) {
			foreach( $arrobjInterviewerEmployees as $arrobjInterviewerEmployee ) {
				array_push( $arrstrInterviewers, $arrobjInterviewerEmployee->getPreferredName() );
			}
		}

		$this->loadCompanyData();

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		if( true == \Psi\CStringService::singleton()->strstr( $strInteviewLocation, 'Provo' ) ) {
			$strInteviewLocation = '34 East 1700 South, St. 200, Provo, (building A, the furthest building WEST on the Microfocus campus)';

		} elseif( true == \Psi\CStringService::singleton()->strstr( $strInteviewLocation, 'HQ' ) ) {
			$strInteviewLocation = '4205 Chapel Ridge Road, Lehi (this is very close to Cabela\'s and Adobe)';
		}

		$objSmarty->assign( 'interview_time', $strInterviewTime );
		$objSmarty->assign( 'interview_location', $strInteviewLocation );
		$objSmarty->assign( 'job_posting_name', $strJobPostingName );
		$objSmarty->assign( 'interviewers', $arrstrInterviewers );
		$objSmarty->assign( 'employee_application', $this );
		$objSmarty->assign( 'COUNTRY_CODE_USA', CCountry::CODE_USA );
		$objSmarty->assign( 'email_image_path', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code', $this->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA', CCountry::CODE_INDIA );
		$objSmarty->assign( 'DEPARTMENT_CALL_CENTER', CDepartment::CALL_CENTER );
		$objSmarty->assign( 'TELEPHONIC_INTERVIEW', CInterviewType::TELEPHONIC_INTERVIEW );

		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );

		$strSubject		= 'Interview Confirmation.';
		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/employee_application_interview_schedule_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $this->getEmailAddress(), 0, $this->m_strCareerEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
			return false;
		}
	}

	public function insertOrUpdateGoogleCalendarEvent( $arrobjApplicantInterviews, $objUser, $objAdminDatabase, $boolIsForSecondInterview = false, $boolInformationEdited = false ) {

		$boolUseLoggedInUserEmailId		= false;
		$boolNotificationToInterviewer	= false;
		$boolCreateNewEvent				= false;

		if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
			date_default_timezone_set( 'Asia/Calcutta' );
			$strEmailId					= CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS;
			$strKeyFile					= self::KEY_FILE_INDIA;
			$strClientId				= CONFIG_KEY_GOOGLE_CALENDAR_INDIA_CLIENT_ID;
			$strServiceAccountName		= CONFIG_KEY_GOOGLE_CALENDAR_INDIA_SERVICE_ACCOUNT_NAME;
			$boolUseLoggedInUserEmailId = true;

			if( true == \Psi\CStringService::singleton()->stristr( $objUser->getEmployee()->getEmailAddress(), '@xento.com' ) ) {
				$strEmailId = $objUser->getEmployee()->getEmailAddress();
			}

		} else {
			date_default_timezone_set( 'America/Denver' );
			$strEmailId				= CSystemEmail::RESUMES_EMAIL_ADDRESS;
			$strKeyFile				= self::KEY_FILE_US;
			$strClientId			= CONFIG_KEY_GOOGLE_CALENDAR_US_CLIENT_ID;
			$strServiceAccountName	= CONFIG_KEY_GOOGLE_CALENDAR_US_SERVICE_ACCOUNT_NAME;
		}

		$arrobjEmployeeApplicationInterviews		= rekeyObjects( 'EmployeeId', $arrobjApplicantInterviews, false, true );
		$arrobjTempEmployeeApplicationInterviews	= rekeyObjects( 'GoogleCalendarEventId', $arrobjApplicantInterviews, false, true );

		if( true == valArr( $arrobjTempEmployeeApplicationInterviews ) ) {
			$objEmployeeApplicationInterview = array_pop( $arrobjTempEmployeeApplicationInterviews );
		} else {
			$arrobjTempEmployeeApplicationInterviews = $arrobjApplicantInterviews;
			$objEmployeeApplicationInterview = array_pop( $arrobjApplicantInterviews );
		}

		if( false == valObj( $objEmployeeApplicationInterview, 'CEmployeeApplicationInterview' ) ) {
			return false;
		}

		$objPsWebsiteJobPosting = \Psi\Eos\Admin\CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );

		if( false == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) ) {
			return false;
		}

		$strCalendarId = $strServiceAccountName;

		if( false == is_null( $objEmployeeApplicationInterview->getGoogleCalendarEventId() ) && false == empty( $objEmployeeApplicationInterview->getDetailsField( 'organiser_employee_id' ) ) ) {
			$objEmployee = \Psi\Eos\Admin\CEmployees::createService()->fetchEmployeeByEmployeeId( $objEmployeeApplicationInterview->getDetailsField( 'organiser_employee_id' ), $objAdminDatabase );

			if( false == valObj( $objEmployee, 'CEmployee' ) ) {
				return false;
			}

			$strCalendarId = $strEmailId;

			if( CEmployeeStatusType::CURRENT == $objEmployee->getEmployeeStatusTypeId() && ( ( CCountry::CODE_INDIA == $this->getCountryCode() && true == \Psi\CStringService::singleton()->stristr( $objEmployee->getEmailAddress(), '@xento.com' ) ) || ( CCountry::CODE_USA == $this->getCountryCode() && true == \Psi\CStringService::singleton()->stristr( $objEmployee->getEmailAddress(), '@entrata.com' ) ) ) ) {
				$strCalendarId = $strEmailId = $objEmployee->getEmailAddress();
			} else {
				$boolCreateNewEvent = true;
			}

			$boolUseLoggedInUserEmailId = true;
		} elseif( true == $boolUseLoggedInUserEmailId && true == is_null( $objEmployeeApplicationInterview->getGoogleCalendarEventId() ) ) {
			$strCalendarId = $strEmailId;
		} elseif( true == $boolUseLoggedInUserEmailId && false == is_null( $objEmployeeApplicationInterview->getGoogleCalendarEventId() ) && true == empty( $objEmployeeApplicationInterview->getDetailsField( 'organiser_employee_id' ) ) ) {
			$strCalendarId = $strEmailId;
			$boolCreateNewEvent = true;
		}

		$objGoogleClient = new Google_Client();
		$objGoogleClient->setClientId( $strClientId );
		$objGoogleClient->setAuthConfig( PATH_CONFIG . 'Keys/' . $strKeyFile );
		$objGoogleClient->setScopes( [ 'https://www.googleapis.com/auth/calendar' ] );
		$objGoogleClient->setSubject( $strCalendarId );

		$objGoogleCalendarService	= new Google_Service_Calendar( $objGoogleClient );
		$objGoogleEvent				= new Google_Service_Calendar_Event();

		if( false == $boolCreateNewEvent && false == is_null( $objEmployeeApplicationInterview->getGoogleCalendarEventId() ) ) {
			try {
				$objCalendarEventDetails = $objGoogleCalendarService->events->get( $strCalendarId, $objEmployeeApplicationInterview->getGoogleCalendarEventId() );
			} catch( Exception $objException ) {
				return false;
			}

			$objGoogleEvent->setSequence( $objCalendarEventDetails->getSequence() );

			$arrobjAttendees = $objCalendarEventDetails->getAttendees();
			foreach( $arrobjAttendees as $objAttendee ) {
				$arrstrAttendeeEmails[] = $objAttendee->getEmail();
			}
		}

		$strBufferTime					= ( CCountry::CODE_INDIA == $this->getCountryCode() && 'No buffer time' != $objEmployeeApplicationInterview->getDetailsField( 'buffer_time' ) ) ? $objEmployeeApplicationInterview->getDetailsField( 'buffer_time' ) : '0 minutes';
		$strScheduledOnDate				= strtotime( $objEmployeeApplicationInterview->getScheduledOn() . ' +' . $strBufferTime );

		$strScheduledOnDateStartTime	= date( 'Y-m-d H:i:s', $strScheduledOnDate );
		$strScheduledOnDateEndTime		= date( 'Y-m-d H:i:s', ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? strtotime( '+1 hour', $strScheduledOnDate ) : strtotime( '+30 minutes', $strScheduledOnDate ) );
		if( true == $boolIsForSecondInterview ) {
			$strScheduledOnDateStartTime	= getConvertedDateTime( date( 'Y-m-d H:i:s', $strScheduledOnDate ), 'Y-m-d H:i:s', 'Asia/Calcutta', $strToTimeZoneName = 'America/Denver' );
			$strScheduledOnDateEndTime		= getConvertedDateTime( date( 'Y-m-d H:i:s', ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? strtotime( '+1 hour', $strScheduledOnDate ) : strtotime( '+30 minutes', $strScheduledOnDate ) ), 'Y-m-d H:i:s', 'Asia/Calcutta', $strToTimeZoneName = 'America/Denver' );
		}

		$strDescription = 'Candidate Name : ' . $this->getNameFirst() . ' ' . $this->getNameLast() . '<br/>Candidate Id : ' . $this->getId() . '<br/>Position Applied For : ' . $objPsWebsiteJobPosting->getName() . '<br/>Experience : ' . $this->getTotalExperience() . ' years';

		$arrmixEmployeeApplicationDetails = ( array ) CEmployeeApplicationSteps::createService()->fetchLatestEmployeeApplicationStepDetailsByEmployeeApplicationId( $this->getId(), $objAdminDatabase );

		if( true == valArr( $arrmixEmployeeApplicationDetails ) ) {
			if( false == empty( $arrmixEmployeeApplicationDetails[0]['purchase_request_id'] ) ) {
				$strDescription .= '<br/>PR ID : ' . $arrmixEmployeeApplicationDetails[0]['purchase_request_id'];
			}

			if( false == empty( $arrmixEmployeeApplicationDetails[0]['designation_name'] ) ) {
				$strDescription .= '<br/>Designation : ' . $arrmixEmployeeApplicationDetails[0]['designation_name'];
			}
		}

		$strScheduleDate = date( 'D j M Y g:i a', $strScheduledOnDate );
		$objGoogleEvent->setSummary( 'Interview Scheduled by ' . $objUser->getEmployee()->getPreferredName() . ( ( false == $boolIsForSecondInterview ) ? ' [Tentative]' : '' ) . '- ' . $objPsWebsiteJobPosting->getName() . ' - ' . $this->getNameFirst() . ' ' . $this->getNameLast() );

		if( true == in_array( $objEmployeeApplicationInterview->getInterviewLocation(), [ CInterviewType::SKYPE_INTERVIEW_TYPE, CInterviewType::GOOGLE_MEET_INTERVIEW ] ) ) {
			$objGoogleEvent->setLocation( CInterviewType::GOOGLE_MEET_INTERVIEW );
		} else {
			$objGoogleEvent->setLocation( $objEmployeeApplicationInterview->getInterviewLocation() );
		}

		$objGoogleEvent->setDescription( $strDescription );
		$strStartDateTime	= new DateTime( $strScheduledOnDateStartTime );
		$strEndDateTime		= new DateTime( $strScheduledOnDateEndTime );

		$objStartGoogleEventDateTime = new Google_Service_Calendar_EventDateTime();
		$objStartGoogleEventDateTime->setTimeZone( date_default_timezone_get() );
		$objStartGoogleEventDateTime->setDateTime( $strStartDateTime->format( 'Y-m-d\TH:i:s' ) );

		$objGoogleEvent->setStart( $objStartGoogleEventDateTime );

		$objEndGoogleEventDateTime = new Google_Service_Calendar_EventDateTime();
		$objEndGoogleEventDateTime->setTimeZone( date_default_timezone_get() );
		$objEndGoogleEventDateTime->setDateTime( $strEndDateTime->format( 'Y-m-d\TH:i:s' ) );

		$objGoogleEvent->setEnd( $objEndGoogleEventDateTime );
		// set event attendee
		$arrstrEmailAddresses			= array();
		if( true === valArr( $arrobjEmployeeApplicationInterviews ) ) {
			$arrmixApplicationInterviews	= \Psi\Eos\Admin\CEmployeeApplicationInterviews::createService()->fetchEmployeeApplicationInterviewsByEmployeeApplicationIdByJobPostingStepId( $objEmployeeApplicationInterview->getEmployeeApplicationId(), $objEmployeeApplicationInterview->getPsJobPostingStepId(), $objAdminDatabase, array_keys( $arrobjEmployeeApplicationInterviews ) );
		}

		if( true == valArr( $arrmixApplicationInterviews ) ) {
			$arrstrEmailAddresses = array_keys( rekeyArray( 'email_address', $arrmixApplicationInterviews ) );
		}

		if( true == valArr( $arrstrEmailAddresses ) ) {
			$boolNotificationToInterviewer = true;
		}

		// setting event creator
		$objEventCreator = new Google_Service_Calendar_EventCreator();
		$objEventCreator->setDisplayName( 'Interview' );
		$objEventCreator->setEmail( $strEmailId );
		$objGoogleEvent->setCreator( $objEventCreator );

		// setting event organizer
		$objEventAttendee = new Google_Service_Calendar_EventAttendee();
		$objEventAttendee->setEmail( $strEmailId );
		$objEventAttendee->setOrganizer( true );
		$arrobjGoogleEventAttendee[] = $objEventAttendee;

		if( true == $boolUseLoggedInUserEmailId ) {
			$objGoogleEvent->setGuestsCanSeeOtherGuests( false );

			if( true == in_array( $objEmployeeApplicationInterview->getInterviewLocation(), [ CInterviewType::SKYPE_INTERVIEW_TYPE, CInterviewType::GOOGLE_MEET_INTERVIEW ] ) ) {
				array_push( $arrstrEmailAddresses, $this->getEmailAddress() );
			}
		}

		if( true == valArr( $arrstrAttendeeEmails ) && in_array( CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS, $arrstrAttendeeEmails ) ) {
			array_push( $arrstrEmailAddresses, CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS );
		}

		if( true == valArr( $arrstrAttendeeEmails ) && in_array( $this->getEmailAddress(), $arrstrAttendeeEmails ) ) {
			array_push( $arrstrEmailAddresses, $this->getEmailAddress() );
		}

		if( true == valArr( $arrstrEmailAddresses ) ) {
			foreach( $arrstrEmailAddresses AS $strEmailAddress ) {
				if( false == CValidation::validateEmailAddresses( $strEmailAddress ) ) {
					continue;
				}
				$objAttendee = new Google_Service_Calendar_EventAttendee();
				$objAttendee->setEmail( $strEmailAddress );
				$arrobjGoogleEventAttendee[] = $objAttendee;
			}
		}
		$objGoogleEvent->setAttendees( $arrobjGoogleEventAttendee );
		$objGoogleEvent->setVisibility( 'default' );
		$objGoogleEvent->setKind( 'calendar#calendar' );

		if( true == valArr( $arrstrAttendeeEmails ) && false == in_array( CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS, $arrstrAttendeeEmails ) && false !== ( $intKey = array_search( $strEmailId, $arrstrAttendeeEmails ) ) ) {
			unset( $arrstrAttendeeEmails[$intKey] );
		}

		$objConferenceSolutionKey = new Google_Service_Calendar_ConferenceSolutionKey();
		$objConferenceSolutionKey->setType( 'hangoutsMeet' );

		$objConferenceRequest = new Google_Service_Calendar_CreateConferenceRequest();
		$objConferenceRequest->setRequestId( substr( md5( uniqid( time(), true ) ), 0, 10 ) );
		$objConferenceRequest->setConferenceSolutionKey( $objConferenceSolutionKey );

		$objConferenceData = new Google_Service_Calendar_ConferenceData();
		$objConferenceData->setConferenceId( substr( md5( uniqid( time(), true ) ), 0, 10 ) );
		$objConferenceData->setCreateRequest( $objConferenceRequest );

		$objGoogleEvent->setConferenceData( $objConferenceData );

		$arrstrOptParams = [
			'sendNotifications' => true,
			'quotaUser' => md5( uniqid( time(), true ) ),
			'conferenceDataVersion' => 1
		];

		if( false == $boolCreateNewEvent && false == is_null( $objEmployeeApplicationInterview->getGoogleCalendarEventId() ) ) {
			try {
				$objGoogleCalendarService->events->update( $strCalendarId, $objEmployeeApplicationInterview->getGoogleCalendarEventId(), $objGoogleEvent, $arrstrOptParams );
			} catch( Exception $objException ) {
				return false;
			}

			if( ( true == $boolInformationEdited && false == in_array( CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS, $arrstrAttendeeEmails ) && CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS != $strCalendarId ) || false == $this->compareTwoArrays( $arrstrAttendeeEmails, $arrstrEmailAddresses ) ) {
				if( false == $this->insertEmployeeApplicationNote( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( 'Updated Google calendar notification is sent to interviewer(s) successfully.', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ), $objUser->getId(), $objAdminDatabase, ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( '<strong>Updated Google calendar notification sent</strong>', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ), CActionType::FOLLOW_UP ) ) {
					return false;
				}
			}

			if( true == $boolUseLoggedInUserEmailId && true == in_array( $objEmployeeApplicationInterview->getInterviewLocation(), [ CInterviewType::SKYPE_INTERVIEW_TYPE, CInterviewType::GOOGLE_MEET_INTERVIEW ] ) ) {
				if( false == $this->insertEmployeeApplicationNote( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( 'Updated Google calendar notification is sent to candidate successfully.', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ), $objUser->getId(), $objAdminDatabase, ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( '<strong>Updated Google calendar notification sent</strong>', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ), CActionType::FOLLOW_UP ) ) {
					return false;
				}
			}

		} else {
			try {
				$objCalendarEventDetails = $objGoogleCalendarService->events->insert( $strCalendarId, $objGoogleEvent, $arrstrOptParams );
			} catch( Exception $objException ) {
				return false;
			}

			switch( NULL ) {

				default:
					$boolValid = true;

					$arrobjEmployeeApplicationInterviews = ( false == valArr( $arrobjEmployeeApplicationInterviews ) ) ? $arrobjTempEmployeeApplicationInterviews : $arrobjEmployeeApplicationInterviews;
					foreach( $arrobjEmployeeApplicationInterviews as $objApplicationInterview ) {
						$objApplicationInterview->setGoogleCalendarEventId( $objCalendarEventDetails->getId() );

						if( true == $boolUseLoggedInUserEmailId ) {

							if( CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS != $strCalendarId ) {
								$objApplicationInterview->setDetailsField( 'organiser_employee_id', $objUser->getEmployee()->getId() );
								$objApplicationInterview->setDetailsField( 'organiser_email_address', $objUser->getEmployee()->getEmailAddress() );
							} else {
								$objApplicationInterview->setDetailsField( 'organiser_employee_id', CEmployee::ID_RECRUITMENT_HELP_DESK );
								$objApplicationInterview->setDetailsField( 'organiser_email_address', CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS );
							}
						}

						$boolValid &= $objApplicationInterview->validate( 'validate_applicant_interview' );

						if( false == $boolValid || false == $objApplicationInterview->update( $objUser->getId(), $objAdminDatabase ) ) {
							echo json_encode( 'Failed to update application.' );
							break 2;
						}
					}
					if( true == $boolNotificationToInterviewer ) {
						if( false == $this->insertEmployeeApplicationNote( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( 'Google calendar notification is sent to interviewer(s) successfully.', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ), $objUser->getId(), $objAdminDatabase, ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( '<strong>Google calendar notification sent</strong>', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ), CActionType::FOLLOW_UP ) ) {
							$boolValid = false;
							break;
						}
					}

					if( true == $boolUseLoggedInUserEmailId && true == in_array( $objEmployeeApplicationInterview->getInterviewLocation(), [ CInterviewType::SKYPE_INTERVIEW_TYPE, CInterviewType::GOOGLE_MEET_INTERVIEW ] ) ) {
						if( false == $this->insertEmployeeApplicationNote( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( 'Google calendar notification is sent to candidate successfully.', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ), $objUser->getId(), $objAdminDatabase, ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( '<strong>Google calendar notification sent</strong>', CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ), CActionType::FOLLOW_UP ) ) {
							$boolValid = false;
							break;
						}
					}
			}
			return $boolValid;
		}
		return true;
	}

	public function sendNotificationOfReferralCandidate( $objEmployee, $intPsJobPostingStepStatusId, $objEmailDatabase, $objAdminDatabase ) {
		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		$this->loadCompanyData();

		$boolSendOfferExtendedMail	= false;
		$boolSendOfferDeclinedMail	= false;
		$boolSendReferralJoinedMail	= false;
		$arrstrPsJobPostingStepStatus = CPsJobPostingStepStatuses::fetchPsJobPostingStepStatusNameById( $intPsJobPostingStepStatusId, $objAdminDatabase );
		if( CPsJobPostingStep::OFFER_EXTENDED == $arrstrPsJobPostingStepStatus['current_step'] ) {
			 if( CPsJobPostingStatus::IN_PROCESS == $arrstrPsJobPostingStepStatus['status_id'] ) {
				 $boolSendOfferExtendedMail = true;
				 if( true == is_numeric( $this->getDesignationId() ) ) {
					 $objDesignation = CDesignations::fetchDesignationById( $this->getDesignationId(), $objAdminDatabase );
				 }
			 } elseif( CPsJobPostingStatus::REJECTED == $arrstrPsJobPostingStepStatus['status_id'] ) {
				 $boolSendOfferDeclinedMail = true;
			 }
		} elseif( CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_JOINED == $this->getEmployeeApplicationStatusTypeId() ) {
			$boolSendReferralJoinedMail	= true;
			$objPsWebsiteJobPosting		= CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objAdminDatabase );
		} elseif( CPsJobPostingStep::OFFER_ACCEPTED == $arrstrPsJobPostingStepStatus['current_step'] && CPsJobPostingStatus::REJECTED == $arrstrPsJobPostingStepStatus['status_id'] ) {
			$boolSendOfferDeclinedMail = true;
		}

		$strSignature	= 'HR Team';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'employee_name', $objEmployee->getNameFirst() );
		$objSmarty->assign( 'referral_name', $this->getNameFull() );
		$objSmarty->assign( 'signature', $strSignature );
		$objSmarty->assign( 'send_offer_extended_mail', $boolSendOfferExtendedMail );
		$objSmarty->assign( 'send_offer_declined_mail', $boolSendOfferDeclinedMail );
		$objSmarty->assign( 'send_referral_joined_mail', $boolSendReferralJoinedMail );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );
		if( true == valObj( $objDesignation, 'Designation' ) ) {
			$objSmarty->assign( 'designation', $objDesignation->getName() );
		}
		if( true == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) ) {
			$objSmarty->assign( 'referral_bonus_amount', ( false !== stripos( $objPsWebsiteJobPosting->getName(), 'Trainee' ) ) ? 0 : $objPsWebsiteJobPosting->getReferralBonusAmount() );
		}

		$strHtmlContent			= $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/send_referral_candidates_update_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, 'Xento Systems Offer Update', $strHtmlContent, $strToEmailAddress = $objEmployee->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress = $this->m_strCareerEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );
		$objSystemEmail->setCcEmailAddress( CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS );

		if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
			return false;
		}

		return true;
	}

	public function sendSecondInterviewMail( $objFirstInterviewStep, $objSecondInterviewStep, $objUser, $objEmailDatabase, $objAdminDatabase ) {
		date_default_timezone_set( 'America/Denver' );

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			echo json_encode( array( 'result' => 'error', 'message' => 'Email service is currently unavailable, unable to send email.' ) );
			exit;
		}

		if( false == valObj( $objSecondInterviewStep, 'CPsJobPostingStep' ) ) return false;

		$arrmixApplicationInterviews	= \Psi\Eos\Admin\CEmployeeApplicationInterviews::createService()->fetchEmployeeApplicationInterviewsByEmployeeApplicationIdByJobPostingStepId( $this->getId(), $objSecondInterviewStep->getId(), $objAdminDatabase );

		if( true == valObj( $objFirstInterviewStep, 'CPsJobPostingStep' ) ) {
			$arrmixFirstInterviewDetails = \Psi\Eos\Admin\CEmployeeApplicationInterviews::createService()->fetchEmployeeApplicationInterviewsByEmployeeApplicationIdByJobPostingStepId( $this->getId(), $objFirstInterviewStep->getId(), $objAdminDatabase );

            if( strtotime( $arrmixFirstInterviewDetails[0]['scheduled_on'] ) >= strtotime( $arrmixApplicationInterviews[0]['scheduled_on'] ) ) return false;
			if( true == valArr( $arrmixFirstInterviewDetails ) ) {
				foreach( $arrmixFirstInterviewDetails as $arrmixFirstInterview ) {
					$arrstrInterviewerNames[]		= $arrmixFirstInterview['name_full'];
				}

				$arrmixFirstInterviewDetails			= array_pop( $arrmixFirstInterviewDetails );
				$arrmixFirstInterviewDetails['names']	= implode( ', ', $arrstrInterviewerNames );
			}
		}

		if( true == valArr( $arrmixApplicationInterviews ) ) {
			$strEmailSignature	= '<br/>' . $objUser->getEmployee()->getNameFull() . '<br/>' . $objUser->getEmployee()->getEmailAddress();

			if( true == valStr( $objUser->getEmployee()->getEmailSignature() ) ) {
				$strEmailSignature = $objUser->getEmployee()->getEmailSignature();
			}

			$objSystemEmailLibrary = new CSystemEmailLibrary();

			foreach( $arrmixApplicationInterviews as $arrmixInterview ) {
				$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );
				$objSmarty->assign( 'interview_data', $arrmixInterview );
				$objSmarty->assign( 'employee_application', $this );
				$objSmarty->assign( 'first_interview_details', $arrmixFirstInterviewDetails );
				$objSmarty->assign( 'cancelled_event', true );
				$objSmarty->assign( 'signature', $strEmailSignature );

				$objSmarty->assign( 'base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
				$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . ( ( CCountry::CODE_INDIA == $this->getCountryCode() ) ? 'update_employee_application/xento_logo.png' : CONFIG_EMAIL_PSI_LOGO_PNG ) );

				$strHtmlEmailContent	= $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'hr/employee_applications/employee_application_interviews/second_interview_mail.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

				$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, 'Second Round Interview Intimation', $strHtmlEmailContent, $arrmixInterview['email_address'], $intIsSelfDestruct = 0, 'HR Team <' . CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS . '>' );
				$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );
				$arrobjSystemEmails[]	= $objSystemEmail;
			}
		}

		switch( NULL ) {
			default:
				$objEmailDatabase->begin();

				if( true == valArr( $arrobjSystemEmails ) ) {
					foreach( $arrobjSystemEmails as $objSystemEmail ) {
						if( false == $objSystemEmail->validate( VALIDATE_INSERT ) ) {
							break 2;
						}

						if( false == $objSystemEmail->insert( $objUser->getId(), $objEmailDatabase ) ) {
							$objEmailDatabase->rollback();
							break 2;
						}
					}

					$objEmailDatabase->commit();
					return true;
				}
		}
		return false;
	}

	public static function verifySourceCoolingPeriod( $strSourceUpdatedOn ) {
		$boolSourceCoolingPeriod = false;
		if( false == is_null( $strSourceUpdatedOn ) ) {
			$arrintDays = CDates::createService()->getDateDifference( date( 'Y-m-d', strtotime( $strSourceUpdatedOn ) ), date( 'Y-m-d' ) );
			if( 0 == $arrintDays['years'] && 12 > $arrintDays['months'] ) {
				$boolSourceCoolingPeriod = true;
			}
		}
		return $boolSourceCoolingPeriod;
	}

	public function setRecruitmentDriveDetails( $objAdminDatabase, $intCurrentUserId, $intJobPostingStepStatusId = NULL ) {
		if( $this->getCountryCode() == CCountry::CODE_INDIA ) {
			date_default_timezone_set( 'Asia/Calcutta' );
		}
		$objRecruitmentDriveDetails = \Psi\Eos\Admin\CRecruitmentDrives::createService()->fetchRecruitmentDriveByCountryCodeByScheduledOn( $this->getCountryCode(), date( 'Y-m-d' ), $objAdminDatabase, NULL, true );

		if( false == valObj( $objRecruitmentDriveDetails, 'CRecruitmentDrive' ) ) {
			return false;
		}

		$boolDriveDayAdded = false;

		if( true == valArr( $arrmixDriveDetails = ( array ) $this->getDriveDays() ) ) {
			foreach( $arrmixDriveDetails as $intDriveId => $strDriveDate ) {
				if( strtotime( $strDriveDate ) >= strtotime( date( 'Y-m-d', strtotime( '-6 month' ) ) ) ) {
					$boolDriveDayAdded = true;
					return false;
				}
			}
		}

		if( false == $boolDriveDayAdded && true == valObj( $objRecruitmentDriveDetails, 'CRecruitmentDrive' ) ) {
			$this->setDetailsField( [ 'drive_days', $objRecruitmentDriveDetails->getId() ], date( 'Y-m-d' ) );
		}

		if( true == valId( $intJobPostingStepStatusId ) ) {
			$arrstrEmployeeStepStatusName	= CPsJobPostingStepStatuses::fetchPsJobPostingStepStatusNameById( $intJobPostingStepStatusId, $objAdminDatabase );
		}

		$strStepStatusNote = '';
		if( true == valArr( $arrstrEmployeeStepStatusName ) ) {
			$strStepStatusNote = 'at step <strong> ' . $arrstrEmployeeStepStatusName['current_step'] . ' ' . $arrstrEmployeeStepStatusName['current_status'];
		}

		$strDriveDayNote			= \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( '<strong> ' . $objRecruitmentDriveDetails->getDriveType() . ' </strong> drive day <strong>' . date( 'm/d/Y' ) . '</strong> is associated with this employee application.', CConfig::get( 'sodium_key_employee_note' ) );
		$strDriveDayDescription		= \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( 'Drive day associated ' . $strStepStatusNote, CConfig::get( 'sodium_key_employee_note' ) );

		if( false == $this->insertEmployeeApplicationNote( $strDriveDayNote, $intCurrentUserId, $objAdminDatabase, $strDriveDayDescription, 'CCAT', CActionType::NOTE ) ) {
			return false;
		}
		return true;

	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchEmployeeApplicationResponses( $objDatabase ) {
		return CEmployeeApplicationResponses::fetchEmployeeApplicationResponsesByEmployeeApplicationId( $this->getId(), $objDatabase );
	}

	public function fetchEmployeeApplicationReferences( $objDatabase ) {
		return CEmployeeApplicationReferences::fetchEmployeeApplicationReferencesByEmployeeApplicationId( $this->getId(), $objDatabase );
	}

	public function fetchEmployeeApplicationDetails( $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeApplicationDetails::createService()->fetchEmployeeApplicationDetailsByEmployeeApplicationId( $this->getId(), $objDatabase );
	}

	public function fetchEmployeeApplicationEducation( $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeApplicationEducations::createService()->fetchEmployeeApplicationEducationById( $this->getEmployeeApplicationEducationId(), $objDatabase );
	}

	public function fetchPsDocument( $objDatabase ) {
		return \Psi\Eos\Admin\CPsDocuments::createService()->fetchPsDocumentById( $this->getPsDocumentId(), $objDatabase );
	}

	public function fetchEmployeeApplicationSource( $objDatabase ) {
		return CEmployeeApplicationSources::fetchEmployeeApplicationSourceById( $this->getEmployeeApplicationSourceId(), $objDatabase );
	}

	public function fetchEmployeeApplicationContacts( $objDatabase ) {
		return CEmployeeApplicationContacts::fetchEmployeeApplicationContactsByEmployeeApplicationId( $this->getId(), $objDatabase );
	}

	public function fetchEmployeeApplicationNotes( $objDatabase ) {
		return CEmployeeApplicationNotes::fetchEmployeeApplicationNotesByEmployeeApplicationId( $this->getId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Employee application id does not appear valid. ' ) );
		}

		return $boolValid;
	}

	public function valEmploymentApplicationId() {
		$boolValid = true;

		if( false == isset( $this->m_intEmploymentApplicationId ) || ( 1 > $this->m_intEmploymentApplicationId ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_application_id', 'Employment application id does not appear valid. ' ) );
		}

		return $boolValid;
	}

	public function valPsWebsiteJobPostingId() {
		$boolValid = true;

		if( false == isset( $this->m_intPsWebsiteJobPostingId ) || ( 1 > $this->m_intPsWebsiteJobPostingId ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_website_job_posting_id', 'Job Posting is required. ' ) );
		}

		return $boolValid;
	}

	public function valEmployeeApplicationEducationId() {
		$boolValid = true;

		if( false == isset( $this->m_intEmployeeApplicationEducationId ) || ( 1 > $this->m_intEmployeeApplicationEducationId ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_application_education_id', 'Education is required. ' ) );
		}

		return $boolValid;
	}

	public function valEmployeeApplicationStatusTypeId() {
		$boolValid = true;

		if( false == isset( $this->m_intEmployeeApplicationStatusTypeId ) || false == is_numeric( $this->m_intEmployeeApplicationStatusTypeId ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_application_status_type_id', 'Employee status type id does not appear valid. ' ) );
		}

		return $boolValid;
	}

	public function valRefereeEmployeeId( $objDatabase ) {
		$boolValid = true;
		if( false == valId( $this->getRefereeEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referee_employee_id', 'Referee Employee ID is required. ' ) );
		} elseif( true == valId( $this->getRefereeEmployeeId() ) ) {
			$objActiveEmployee = CEmployees::fetchActiveEmployeeByEmployeeNumber( $this->getRefereeEmployeeId(), $objDatabase );
			if( false == valObj( $objActiveEmployee, 'CEmployee' ) ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referee_employee_id', 'Referee Employee is not current Xento employee. ' ) );
			}
		}
		return $boolValid;
	}

	public function valEmployeeApplicationSourceId() {
		$boolValid = true;

		if( false == isset( $this->m_intEmployeeApplicationSourceId ) || ( 1 > $this->m_intEmployeeApplicationSourceId ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_application_source_id', 'Source is required. ' ) );
		}

		return $boolValid;
	}

	public function valNameFirst() {
		$boolValid = true;

		if( false == isset( $this->m_strNameFirst ) || 0 == strlen( $this->m_strNameFirst ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First Name is required. ' ) );
		} else {
			if( false == preg_match( '/^\pL+$/u', $this->m_strNameFirst ) ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name should be set of alphabets. ' ) );
			}
		}

		return $boolValid;
	}

	public function valNameLast() {
		$boolValid = true;

		if( false == isset( $this->m_strNameLast ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last Name is required. ' ) );
		} else {
			if( false == preg_match( '/^\pL+$/u', $this->m_strNameLast ) ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name should be set of alphabets. ' ) );
			}
		}

		return $boolValid;
	}

	public function valNameFull() {
		$boolValid = true;

		if( false == isset( $this->m_strPreferredName ) || 0 == strlen( $this->m_strPreferredName ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Full Name is required. ' ) );
		} else {
			if( false == preg_match( '/^[a-zA-Z\s]*$/u', $this->m_strPreferredName ) ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Full name should be set of alphabets. ' ) );
			}
		}

		return $boolValid;
	}

	public function valPhoneNumber() {
		$boolValid = true;

		if( false == is_null( $this->m_strPhoneNumber ) && false == is_numeric( $this->m_strPhoneNumber ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Please enter valid alternate contact number. ', 611 ) );
		}

		return $boolValid;
	}

	public function valPsWebsitePhoneNumber() {
		$boolValid = true;

		if( true == is_null( $this->m_strPhoneNumber ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required. ' ) );
		} elseif( 1 != CValidation::checkPhoneNumberFullValidation( $this->m_strPhoneNumber, true ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number must be in xxx-xxx-xxxx format. ', 611 ) );
		}

		return $boolValid;
	}

	public function valCellNumber( $objDatabase, $strEmployeeCountryCode = NULL ) {
		$boolValid = true;

		if( true == is_null( $this->m_strCellNumber ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', 'Mobile Number is required. ' ) );
		} elseif( ( CCountry::CODE_USA == $strEmployeeCountryCode && 10 < strlen( trim( str_replace( ' ', '', $this->m_strCellNumber ) ) ) && false == preg_match( '/^([0-9]{3})-[^0-9]*([0-9]{3})-[^0-9]*([0-9]{4})$/', $this->m_strCellNumber ) ) || ( CCountry::CODE_INDIA == $this->getCountryCode() && ( 10 > strlen( trim( str_replace( ' ', '', $this->m_strCellNumber ) ) ) || 5999999999 >= trim( str_replace( ' ', '', $this->m_strCellNumber ) ) ) ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', 'Please enter valid mobile number. ', 611 ) );
		}
		if( true == $boolValid && true == isset( $objDatabase ) && CCountry::CODE_INDIA == $strEmployeeCountryCode ) {

			$strSql = ' WHERE cell_number = \'' . trim( $this->getCellNumber() ) . '\'';

			if( false == is_null( $this->m_intId ) ) {
				$strSql .= ' AND id <> ' . ( int ) $this->m_intId;
			}

			$intCount = CEmployeeApplications::createService()->fetchRowCount( $strSql, 'employee_applications', $objDatabase );

			if( 0 < $intCount ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', 'Mobile Number is already exists. ' ) );
			}
		}

		return $boolValid;
	}

	public function valRefereeDetails( $arrmixRefereeData ) {
		$boolValid = true;
		if( false == valArr( $arrmixRefereeData ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Referee_details', 'Referee details is required. ' ) );
		}
		return $boolValid;
	}

	public function valPanNumber( $strAction, $objDatabase ) {

		$boolValid = true;

		$objPsWebsiteJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objDatabase );
		if( false == valStr( $this->getTaxNumberEncrypted() ) && true == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) && CEmploymentOccupationType::PROFESSIONAL == $this->getEmploymentOccupationTypeId() ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', 'PAN Number is required. ' ) );
		}

		if( true == valStr( $this->getTaxNumberEncrypted() ) && false == preg_match( '/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}$/', $this->getTaxNumber() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', 'PAN Number is not valid.' ) );
		}

		if( true == $boolValid && true == isset( $objDatabase ) ) {

			$strSql = ' WHERE tax_number_encrypted = \'' . $this->getTaxNumberEncrypted() . '\'';

			if( false == is_null( $this->m_intId ) ) {
				$strSql .= ' AND id <> ' . ( int ) $this->m_intId;
			}

			$intCount = CEmployeeApplications::createService()->fetchRowCount( $strSql, 'employee_applications', $objDatabase );

			if( false == in_array( $strAction, array( 'validate_xento_employment_application', 'validate_refer_friend', 'validate_employee_reference' ) ) ) {
				if( 0 < $intCount ) {
					$boolValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', 'PAN Number is already exists. ' ) );
				}
			}
		}

		return $boolValid;
	}

	public function valMobileNumber() {
		$boolValid			= true;
		$strMobileNumberCheck	= '/^[0-9][0-9-+]{9,15}$/';

		if( false == is_null( $this->m_strCellNumber ) ) {
			if( false == preg_match( $strMobileNumberCheck, $this->m_strCellNumber ) ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', 'Mobile number is not valid. ' ) );
			}
		} else {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', 'Mobile number is required. ' ) );
		}

		return $boolValid;
	}

	public function valEmailAddress( $objDatabase, $strEmployeeCountryCode = NULL ) {

		$boolValid = true;

		if( false == isset( $this->m_strEmailAddress ) || 0 == strlen( $this->m_strEmailAddress ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email Address is required. ' ) );
		}

		if( 0 < strlen( trim( $this->m_strEmailAddress ) ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email Address is not valid. ' ) );
		}

		if( true == $boolValid && true == isset( $objDatabase ) && CCountry::CODE_INDIA == $strEmployeeCountryCode ) {

			$strSql = ' WHERE email_address = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $this->m_strEmailAddress ) ) ) . '\'';

			if( false == is_null( $this->m_intId ) ) {
				$strSql .= ' AND id <> ' . ( int ) $this->m_intId;
			}

			$intCount = CEmployeeApplications::createService()->fetchRowCount( $strSql, 'employee_applications', $objDatabase );
			if( 0 < $intCount ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email Address is already exists.' ) );
			}
		}

		return $boolValid;
	}

	public function valEmploymentApplicationEmailAddress() {

		$boolValid = true;

		if( false == isset( $this->m_strEmailAddress ) || 0 == strlen( $this->m_strEmailAddress ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email Address is required. ' ) );
		}

		if( 0 < strlen( trim( $this->m_strEmailAddress ) ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email Address is not valid. ' ) );
		}

		return $boolValid;
	}

	public function valPsWebsiteEmailAddress() {

		$boolValid = true;

		if( false == isset( $this->m_strEmailAddress ) || 0 == strlen( $this->m_strEmailAddress ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email Address is required. ' ) );
		}

		if( 0 < strlen( trim( $this->m_strEmailAddress ) ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email Address is not valid. ' ) );
		}

		return $boolValid;
	}

	public function valEmploymentApplication( $objDatabase, $boolIsFromInsert = false, $objEmailDatabase = NULL ) {

		$boolValid = true;

		if( true == $boolIsFromInsert && true == $boolValid && true == valObj( $objDatabase, 'CDatabase' ) ) {

			$objEmployeeApplication = CEmployeeApplications::createService()->fetchConflictingEmployeeApplication( $this, $objDatabase );

			if( true == valObj( $objEmployeeApplication, 'CEmployeeApplication' ) ) {
				$objEmployee = CEmployees::fetchEmployeeById( SYSTEM_USER_ID, $objDatabase );

				if( true == valObj( $objEmployeeApplication = CEmployeeApplications::createService()->fetchEmployeeApplicationByEmailAddress( \Psi\CStringService::singleton()->strtolower( $objEmployeeApplication->getEmailAddress() ), $objDatabase ), 'CEmployeeApplication' ) ) {

					$objPsWebsiteJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getPsWebsiteJobPostingId(), $objDatabase );
					$objEmployeeApplication->setDepartmentId( $objPsWebsiteJobPosting->getDepartmentId() );

					$objEmployeeApplication->sendEmployeeApplicationEmail( $objEmployee, $objEmailDatabase, $this->getPsWebsiteJobPostingId(), $boolIsEdit = true );
					$boolValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Your application is already with us. Please check your email to edit your application. ' ) );
				}
			}
		}

		return $boolValid;
	}

	public function valPsWebsiteEmploymentApplication( $objDatabase ) {

		$boolValid = true;

		if( true == isset( $objDatabase ) ) {

			$objEmployeeApplication = CEmployeeApplications::createService()->fetchConflictingEmployeeApplication( $this, $objDatabase );

			if( true == valObj( $objEmployeeApplication, 'CEmployeeApplication' ) && $objEmployeeApplication->getPsWebsiteJobPostingId() == $this->getPsWebsiteJobPostingId() ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Your application is already with us. ' ) );
			}
		}

		return $boolValid;

	}

	public function valBirthDate() {
		$boolValid = true;

		if( false == isset( $this->m_strBirthDate ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'Date of birth is required. ' ) );
		}

		if( true == isset( $this->m_strBirthDate ) && false == CValidation::validateDate( $this->m_strBirthDate ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'Enter valid birth date. ' ) );
		}

		return $boolValid;
	}

	public function valDigitalSignature() {
		$boolValid = true;

		$strFullName			= '';
		$arrstrDigitalSignature = ( true == valStr( $this->m_strDigitalSignature ) ) ? explode( ' ', $this->m_strDigitalSignature ) : array();

		if( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrDigitalSignature ) ) {
			$strFullName				 = $this->m_strNameFirst . ' ' . $this->m_strNameLast;
			$this->m_strDigitalSignature = $arrstrDigitalSignature[0] . ' ' . $arrstrDigitalSignature[1];
		} elseif( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrDigitalSignature ) ) {
			$strFullName				 = $this->m_strNameFirst . ' ' . $this->m_strNameMiddle . ' ' . $this->m_strNameLast;
			$this->m_strDigitalSignature = $arrstrDigitalSignature[0] . ' ' . $arrstrDigitalSignature[1] . ' ' . $arrstrDigitalSignature[2];
		}

		if( false == isset( $this->m_strDigitalSignature ) || ( \Psi\CStringService::singleton()->strtolower( $this->m_strDigitalSignature ) !== \Psi\CStringService::singleton()->strtolower( $strFullName ) ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'digital_signature', 'Valid Digital Signature is required. ' ) );
		}

		return $boolValid;
	}

	public function valTaxNumberEncrypted() {
		$boolValid = true;

		if( false == isset( $this->m_strTaxNumberEncrypted ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', 'SSN or Id is required. ' ) );
		}

		return $boolValid;
	}

	public function valStreetLine1() {
		$boolValid = true;

		if( false == isset( $this->m_strStreetLine1 ) || 0 == strlen( $this->m_strStreetLine1 ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', 'Street line 1 is required. ' ) );
		}

		return $boolValid;
	}

	public function valCity() {
		$boolValid = true;

		if( false == isset( $this->m_strCity ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City is required. ' ) );
		}

		return $boolValid;
	}

	public function valCurrentLocation() {
		$boolValid = true;

		if( false == isset( $this->m_strCurrentLocation ) || false == valStr( $this->m_strCurrentLocation ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_location', ' Sub Urban Location required. ' ) );
		}

		return $boolValid;
	}

	public function valStateCode() {
		$boolValid = true;

		if( false == isset( $this->m_strStateCode ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', 'State code is required. ' ) );
		}

		return $boolValid;
	}

	public function valPostalCode() {
		$boolValid = true;

		if( false == is_null( $this->m_strPostalCode ) ) {
			if( false == preg_match( '/^([0-9]{5})(?:[-\s]*([0-9]{4}))?$/', $this->m_strPostalCode ) ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be in xxxxx or xxxxx-xxxx format. ' ) );
			}
		} else {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code is required. ' ) );
		}

		return $boolValid;
	}

	public function valAdministrativeNotes() {
		$boolValid = true;

		if( true == is_null( $this->getAdministrativeNotes() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'administrative_notes', 'Administrative notes required. ' ) );
		}

		return $boolValid;
	}

	public function valResumeFiles() {

		$boolValid = true;

		$intMaxUpload = 20 * 1024 * 1024;

		$arrstrMimeTypes = array( '', 'application/rtf', 'application/octet-stream', 'application/msword', 'application/ms-word', 'application/pdf', 'application/x-pdf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' );

		if( true == array_key_exists( 'employee_resume', $_FILES ) && false == empty( $_FILES['employee_resume']['name'] ) ) {

			$strExtension = \Psi\CStringService::singleton()->substr( $_FILES['employee_resume']['name'], ( \Psi\CStringService::singleton()->strrpos( $_FILES['employee_resume']['name'], '.' ) + 1 ) );

			if( 0 != strlen( $strExtension ) && ( 'pdf' != \Psi\CStringService::singleton()->strtolower( $strExtension ) && 'docx' != \Psi\CStringService::singleton()->strtolower( $strExtension ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_resume']['name'] . ' is not a supported extension. Please upload .pdf or .docx file for Resume. ', NULL ) );

				return false;
			}

			if( false == in_array( $_FILES['employee_resume']['type'], $arrstrMimeTypes ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_resume']['name'] . ' is not a supported extension. Please upload .pdf or .docx file for Resume. ', NULL ) );

				return false;
			}

			if( $_FILES['employee_resume']['size'] > $intMaxUpload || UPLOAD_ERR_INI_SIZE == $_FILES['employee_resume']['error'] ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_resume']['name'] . ' File failed to upload. File size is greater than permitted size of 2MB. ', NULL ) );

				return false;
			}
		} else {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_resume', 'Resume is required. ' ) );

			return false;
		}

		return $boolValid;
	}

	public function valPhotograph( $objDatabase, $objStorageGateway ) {

		$boolValid			= true;
		$intMaxHeightWidth	= 600;
		$intMaxUpload		= 20 * 1024 * 1024;
		$arrstrMimeTypes	= [ 'jpeg', 'jpg', 'pjpeg', 'gif', 'png' ];

		$objPsDocument = \Psi\Eos\Admin\CPsDocuments::createService()->fetchPsDocumentByEmployeeApplicationIdByPsDocumentTypeId( $this->getId(), CPsDocumentType::EMPLOYEE_APPLICATION_PROFILE_PHOTO, $objDatabase );
		if( false === valObj( $objPsDocument, 'CPsDocument' ) ) {
			$objPsDocument = new CPsDocument();
			$objPsDocument->setCid( CClient::ID_DEFAULT );
			$objPsDocument->setEmployeeApplicationId( $this->getId() );
			$objPsDocument->setPsDocumentTypeId( CPsDocumentType::EMPLOYEE_APPLICATION_PROFILE_PHOTO );
			$objPsDocument->setFileExtensionId( CFileExtension::IMAGE_JPG );
			$objPsDocument->setTitle( $this->getId() );
			$objPsDocument->setFileName( $this->getId() . '.jpg' );
		}

		$objStoredObject		= $objPsDocument->fetchStoredObject( $objDatabase );
		$arrmixRequest			= $objStoredObject->createGatewayRequest( [ 'mountSystem' => 'NON_BACKUP_MOUNTS', 'outputFile' => 'temp' ] );
		$arrmixResponse			= $objStorageGateway->getObject( $arrmixRequest );

		if( false == $arrmixResponse->hasErrors() ) {
			$strFilePath			= $arrmixResponse['outputFile'];
			$arrmixImageSize = getimagesize( $strFilePath );

			if( $intMaxHeightWidth < $arrmixImageSize[0] || $intMaxHeightWidth < $arrmixImageSize[1] ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Passport size photograph required( less than 15x15 cm ).', NULL ) );
				return false;
			}

			$strFileExtention = pathinfo( $strFilePath, PATHINFO_EXTENSION );
			if( false == in_array( $strFileExtention, $arrstrMimeTypes ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, basename( $strFilePath ) . ' is not a supported extension. Please upload .jpeg, .jpg, .png, .pjpeg, .gif file for photograph. ', NULL ) );
				return false;
			}

			if( $intMaxUpload < filesize( $strFilePath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, basename( $strFilePath ) . ' File failed to upload. Please upload small image. ', NULL ) );
				return false;
			}
		} else {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_photo', 'Photograph is required. ' ) );
			return false;
		}

		return $boolValid;
	}

	public function valResumeFileType() {

		$boolValid = true;

		$intMaxUpload = 20 * 1024 * 1024;

		$arrstrMimeTypes = array( '', 'application/rtf', 'application/octet-stream', 'application/msword', 'application/ms-word', 'application/pdf', 'application/x-pdf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' );

		if( true == array_key_exists( 'employee_resume', $_FILES ) && false == empty( $_FILES['employee_resume']['name'] ) ) {

			$strExtension = \Psi\CStringService::singleton()->substr( $_FILES['employee_resume']['name'], ( \Psi\CStringService::singleton()->strrpos( $_FILES['employee_resume']['name'], '.' ) + 1 ) );

			if( 0 != strlen( $strExtension ) && ( 'pdf' != \Psi\CStringService::singleton()->strtolower( $strExtension ) && 'docx' != \Psi\CStringService::singleton()->strtolower( $strExtension ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_resume']['name'] . ' is not a supported extension. Please upload .pdf or .docx file for Resume. ', NULL ) );

				return false;
			}

			if( false == in_array( $_FILES['employee_resume']['type'], $arrstrMimeTypes ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_resume']['name'] . ' is not a supported extension. Please upload .pdf or .docx file for Resume. ', NULL ) );

				return false;
			}

			if( $_FILES['employee_resume']['size'] > $intMaxUpload || UPLOAD_ERR_INI_SIZE == $_FILES['employee_resume']['error'] ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_resume']['name'] . ' File failed to upload. File size is greater than permitted size of 2MB. ', NULL ) );

				return false;
			}
		}

		return $boolValid;
	}

	public function valPostSize() {

		$boolValid = true;

		$intMaxPostSize = ( int ) str_replace( 'M', '', PHP_MAX_POST ) * 1024 * 1024;

		if( $_SERVER['CONTENT_LENGTH'] > $intMaxPostSize ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' File failed to upload. File size is greater than permitted size of 2MB. ', NULL ) );

			return false;
		}

		return $boolValid;
	}

	public function valImportEmployee( $objDatabase ) {
		$boolValid = true;

		$strSqlCondition = ( true == is_null( $this->m_strPhoneNumber ) ? ' AND phone_number IS NULL ' : 'AND ( phone_number IS NULL OR ( phone_number IS NOT NULL AND phone_number = \'' . addslashes( $this->m_strPhoneNumber ) . '\') )' );
		$strSqlCondition .= ( false == is_null( $this->m_intId ) ? ' AND id <> ' . ( int ) $this->m_intId : '' );

		$strSql = ' WHERE
						email_address = \'' . trim( addslashes( $this->m_strEmailAddress ) ) . '\'
						AND name_first = \'' . trim( addslashes( $this->m_strNameFirst ) ) . '\'
						AND name_last = \'' . trim( addslashes( $this->m_strNameLast ) ) . '\'
						' . $strSqlCondition;

		$intCount = CEmployeeApplications::createService()->fetchRowCount( $strSql, 'employee_applications', $objDatabase );

		if( 0 < $intCount ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Improper/Duplicate data of ' . $this->m_strNameFirst . ' ' . $this->m_strNameLast . '. ' ) );
		}

		return $boolValid;
	}

	public function valAddress() {
		$boolValid = true;

		if( false == isset( $this->m_strStreetLine1 ) || 0 == strlen( $this->m_strStreetLine1 ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', 'Address is required. ' ) );
		}

		return $boolValid;
	}

	public function valGender() {
		$boolValid = true;

		if( true == is_null( $this->m_strGender ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gender', 'Gender is required. ' ) );
		}

		return $boolValid;
	}

	public function valReference() {

		$boolValid = true;

		if( true == isset( $this->m_intEmployeeApplicationSourceId ) && CEmployeeApplicationSource::EMPLOYEE_REFERRALS == $this->m_intEmployeeApplicationSourceId && false == isset( $this->m_intRefereeEmployeeId ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referee_employee_id', 'Reference is required. ' ) );
		}

		return $boolValid;
	}

	public function valEmploymentOccupationTypeId() {
		$boolValid = true;

		if( false == isset( $this->m_intEmploymentOccupationTypeId ) || ( 1 > $this->m_intEmploymentOccupationTypeId ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_occupation_type_id', 'Employee Occupation is required. ' ) );
		}

		return $boolValid;
	}

	public function valDesignationId() {
		$boolValid = true;

		if( false == is_numeric( $this->getDesignationId() ) || 0 == $this->getDesignationId() ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'designation_id', ' Designation is required. ' ) );
		}

		return $boolValid;
	}

	public function valPurchaseRequestId( $objDatabase ) {
		$boolValid = true;

		if( false == is_numeric( $this->getPurchaseRequestId() ) || 0 == $this->getPurchaseRequestId() ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_request_id', ' Purchase request is required. ' ) );
		}

		if( true == valId( $this->getPurchaseRequestId() ) ) {
			$arrobjEmployeeApplications = rekeyObjects( 'Id', CEmployeeApplications::createService()->fetchEmployeeApplicationsByPurchaseRequestId( $this->getPurchaseRequestId(), $objDatabase ) );

			if( true == valArr( $arrobjEmployeeApplications ) && false == array_key_exists( $this->getId(), $arrobjEmployeeApplications ) ) {
				$objEmployeeApplication = array_shift( $arrobjEmployeeApplications );
				$boolValid				= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_request_id', '<strong>Employee Application: ' . $objEmployeeApplication->getId() . '</strong> has been Offered against this <strong>PR ID: ' . $this->getPurchaseRequestId() . '.</strong> Please associate New PR ID.' ) );
			}
		}

		return $boolValid;
	}

	public function valReferredByName() {
		$boolValid = true;

		if( false == valStr( $this->getReferredByName() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referred_by_name', ' Referee employee name is required. ' ) );
		}

		return $boolValid;
	}

	public function verifyGoogleCaptcha( $strResponse, $boolFromReferFriend = false ) {

		$boolValid	= true;
		$strUrl			= 'https://www.google.com/recaptcha/api/siteverify' . '?secret=' . \Psi\Libraries\UtilConfig\CConfig::createService()->get( 'entrata_google_recaptcha_secret_key' ) . '&response=' . $strResponse;

		$arrmixParameters = [
			'url'				=> $strUrl,
			'execution_timeout'	=> 15,
			'ssl_verify_peer'	=> false,
			'ssl_verify_host'	=> false
		];

		$objExternalRequest = new CExternalRequest();
		$objExternalRequest->setParameters( $arrmixParameters );
		$strCurlData = $objExternalRequest->execute( $boolGetResponse = true );

		unset( $objExternalRequest );

		$arrmixResult = json_decode( $strCurlData, true );

		if( $arrmixResult['success'] == 'true' ) {
			$boolValid = true;
		} else {
			if( false == $boolFromReferFriend ) {
				$this->addErrorMsg( new CErrorMsg( NULL, 'verification_key', 'Please try submitting the form once again by verifying captcha.', E_USER_ERROR ) );
			}
			$boolValid = false;
		}

		return $boolValid;
	}

	public function valPsDocumentId() {
		$boolValid = true;
		if( true == is_null( $this->getPsDocumentId() ) ) {
			$boolValid = false;
		}
		return $boolValid;

	}

	public function valCandidateLastUpdatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $strEmployeeCountryCode = NULL, $objDatabase = NULL, $boolIsExperience = false, $boolIsFromInsert = false, $objEmailDatabase = NULL, $boolIsFromPreEmploymentHistory = false, $arrmixRefereeData = NULL, $strCaptchaResponse = NULL, $intEmployeeApplicationId = NULL, $intNewPurchaseRequestId = NULL, $boolSendEditLink = false, $objStorageGateway = NULL ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valStreetLine1();
				$boolValid &= $this->valCity();
				$boolValid &= $this->valStateCode();
				$boolValid &= $this->valEmailAddress( $objDatabase );
				$boolValid &= $this->valPhoneNumber( $objDatabase );
				$boolValid &= $this->valPostalCode();
				$boolValid &= $this->valEmploymentApplicationId();
				$boolValid &= $this->valEmployeeApplicationEducationId();
				$boolValid &= $this->valEmployeeApplicationStatusTypeId();
				$boolValid &= $this->valDigitalSignature();
				break;

			case VALIDATE_UPDATE:
				$boolValid &= $this->valId();
				$boolValid &= $this->valStreetLine1();
				$boolValid &= $this->valCity();
				$boolValid &= $this->valStateCode();
				$boolValid &= $this->valEmailAddress( $objDatabase );
				$boolValid &= $this->valPhoneNumber( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolValid &= $this->valId();
				$boolValid &= $this->valStreetLine1();
				$boolValid &= $this->valCity();
				$boolValid &= $this->valStateCode();
				$boolValid &= $this->valEmailAddress( $objDatabase );
				$boolValid &= $this->valPhoneNumber();
				break;

			case 'employee_application_basic_info':
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valPhoneNumber( $objDatabase );
				$boolValid &= $this->valEmailAddress( $objDatabase );
				$boolValid &= $this->valEmployeeApplicationStatusTypeId();
				$boolValid &= $this->valEmploymentApplicationId();
				break;

			case 'validate_post_data':
				$boolValid &= $this->valPostSize();
				break;

			case 'validate_administrative_notes':
				$boolValid &= $this->valAdministrativeNotes();
				break;

			// used on xento carrer applicant application page
			case 'validate_xento_employment_application':
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();

				if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {
					$boolValid &= $this->valGender();
					$boolValid &= $this->valPhoneNumber();
				}

				$boolValid &= $this->valBirthDate();
				$boolValid &= $this->valAddress();
				$boolValid &= $this->valCity();

				if( CCity::CITY_PUNE == \Psi\CStringService::singleton()->strtoupper( $this->m_strCity ) ) {
					$boolValid &= $this->valCurrentLocation();
				}

				$boolValid &= $this->valCellNumber( $objDatabase );
				$boolValid &= $this->valEmploymentApplicationEmailAddress();
				$boolValid &= $this->valEmploymentOccupationTypeId();
				$boolValid &= $this->valPsWebsiteJobPostingId();

				if( CEmploymentOccupationType::STUDENT != $this->m_intEmploymentOccupationTypeId ) {
					$boolValid &= $this->valPanNumber( $strAction, $objDatabase );
				}

				$boolValid &= $this->valEmployeeApplicationSourceId();

				$boolSourceCoolingPeriod = CEmployeeApplication::verifySourceCoolingPeriod( $this->m_strSourceUpdatedOn );

				if( false == $boolSourceCoolingPeriod && CEmployeeApplicationSource::EMPLOYEE_REFERRALS == $this->m_intEmployeeApplicationSourceId ) {
					$boolValid &= $this->valRefereeEmployeeId( $objDatabase );
					$boolValid &= $this->valReferredByName();
				}

				$boolValid &= $this->valEmploymentApplicationId();
				$boolValid &= $this->valEmployeeApplicationStatusTypeId();

				if( false == is_null( $strCaptchaResponse ) ) {
					$boolValid &= $this->verifyGoogleCaptcha( $strCaptchaResponse );
				}
				break;

			// used on xento carrer applicant application page
			case 'validate_applicant_resume_details':
				$boolValid &= $this->valResumeFiles();
				break;

			case 'validate_applicant_photo':
				$boolValid &= $this->valPhotograph( $objDatabase, $objStorageGateway );
				break;

			// used on xento carrer applicant application page
			case 'validate_applicant_education_details':
				$boolValid &= $this->valEmployeeApplicationEducationId();
				break;

			case 'validate_ps_website_employment_application':
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valStreetLine1();
				$boolValid &= $this->valCity();
				$boolValid &= $this->valStateCode();
				$boolValid &= $this->valPsWebsiteEmailAddress();
				$boolValid &= $this->valPsWebsitePhoneNumber();
				$boolValid &= $this->valPostalCode();
				$boolValid &= $this->valEmploymentApplicationId();
				$boolValid &= $this->valEmployeeApplicationEducationId();
				$boolValid &= $this->valEmployeeApplicationStatusTypeId();
				$boolValid &= $this->valDigitalSignature();

				if( false == is_null( $strCaptchaResponse ) ) {
					$boolValid &= $this->verifyGoogleCaptcha( $strCaptchaResponse, false );
				}
				break;

			case 'validate_xento_employment_application_details':
				$boolValid &= $this->valEmploymentApplication( $objDatabase, $boolIsFromInsert, $objEmailDatabase );
				break;

			case 'validate_ps_website_employment_application_details':
				$boolValid &= $this->valPsWebsiteEmploymentApplication( $objDatabase );
				break;

			case 'validate_employee_application':
				$boolValid &= $this->valPsWebsiteJobPostingId();
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valEmailAddress( $objDatabase, $strEmployeeCountryCode );
				$boolValid &= $this->valCellNumber( $objDatabase, $strEmployeeCountryCode );
				$boolValid &= $this->valEmployeeApplicationSourceId();
				if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {
					if( false == $boolSendEditLink ) {
						$boolValid &= $this->valPanNumber( $strAction, $objDatabase );
					}
					$boolValid &= $this->valEmploymentOccupationTypeId();
					$boolValid &= $this->valGender();
					$boolValid &= $this->valCity();
				}

				$boolValid &= $this->valReference();

				if( true == $boolIsExperience && false == $boolSendEditLink ) {
					$boolValid &= $this->valResumeFiles();
				}
				break;

			case 'validate_mandatory_fields':
				// fields not on add reference page
				$boolValid &= $this->valEmploymentOccupationTypeId();
				$boolValid &= $this->valGender();
				$boolValid &= $this->valBirthDate();
				$boolValid &= $this->valAddress();
				$boolValid &= $this->valCity();
				$boolValid &= $this->valEmployeeApplicationEducationId();
				$boolValid &= $this->valEmployeeApplicationSourceId();
				$boolValid &= $this->valPsDocumentId();

				if( CEmploymentOccupationType::PROFESSIONAL == $this->m_intEmploymentOccupationTypeId ) {
					$boolValid &= $this->valPanNumber( $strAction, $objDatabase );
				}
				break;

			case 'validate_employee_reference':
				$boolValid &= $this->valPsWebsiteJobPostingId();
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valEmailAddress( $objDatabase );
				$boolValid &= $this->valCellNumber( $objDatabase );
				if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {
					$boolValid &= $this->valEmploymentOccupationTypeId();
					$boolValid &= $this->valCity();

					if( CEmploymentOccupationType::STUDENT != $this->m_intEmploymentOccupationTypeId ) {
						$boolValid &= $this->valPanNumber( $strAction, $objDatabase );
					}
				}
				break;

			case 'validate_refer_friend':
				$boolValid &= $this->valPsWebsiteJobPostingId();
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valEmailAddress( $objDatabase );
				$boolValid &= $this->valCellNumber( $objDatabase );
				if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {
					$boolValid &= $this->valEmploymentOccupationTypeId();

					if( CEmploymentOccupationType::STUDENT != $this->m_intEmploymentOccupationTypeId ) {
						$boolValid &= $this->valPanNumber( $strAction, $objDatabase );
					}
				}
				$boolValid &= $this->valRefereeDetails( $arrmixRefereeData );
				break;

			case 'validate_import_employee':
				if( false == is_null( $this->getEmailAddress() ) ) {
					$boolValid &= $this->valNameFirst();
					$boolValid &= $this->valNameLast();
					$boolValid &= $this->valEmailAddress( $objDatabase );
					$boolValid &= $this->valPhoneNumber( $objDatabase );
					$boolValid &= $this->valImportEmployee( $objDatabase );
				}
				break;

			case 'update_employee_application':
				$boolValid &= $this->valPsWebsiteJobPostingId();
				$boolValid &= $this->valEmploymentApplicationId();
				$boolValid &= $this->valEmployeeApplicationStatusTypeId();
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valEmailAddress( $objDatabase, $strEmployeeCountryCode );
				$boolValid &= $this->valCellNumber( $objDatabase );
				$boolValid &= $this->valPanNumber( $strAction, $objDatabase );

				if( false == $boolIsFromPreEmploymentHistory ) {
					$boolValid &= $this->valEmploymentOccupationTypeId();
				}

				if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {
					$boolValid &= $this->valGender();
					$boolValid &= $this->valCity();
				}

				if( true == $boolIsExperience ) {
					$boolValid &= $this->valResumeFiles();
				}
				break;

			case 'validate_employee_resume':
				$boolValid &= $this->valResumeFiles();
				break;

			case 'validate_external_trainer_employee_application':
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valEmailAddress( $objDatabase );
				$boolValid &= $this->valMobileNumber();
				$boolValid &= $this->valResumeFiles();
				break;

			case 'update_external_trainer_employee_application':
				$boolValid &= $this->valPsWebsiteJobPostingId();
				$boolValid &= $this->valEmploymentApplicationId();
				$boolValid &= $this->valEmployeeApplicationStatusTypeId();
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valEmailAddress( $objDatabase );
				$boolValid &= $this->valCellNumber( $objDatabase );
				break;

			case 'update_applicant_profile':
				$boolValid &= $this->valPsWebsiteJobPostingId();
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valEmailAddress( $objDatabase, $strEmployeeCountryCode );
				$boolValid &= $this->valCellNumber( $objDatabase, $strEmployeeCountryCode );
				$boolValid &= $this->valResumeFileType();

				if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {
					$boolValid &= $this->valGender();
					if( false == $boolIsFromPreEmploymentHistory ) {
						$boolValid &= $this->valEmploymentOccupationTypeId();
					}

					if( CEmploymentOccupationType::STUDENT != $this->m_intEmploymentOccupationTypeId ) {
						$boolValid &= $this->valPanNumber( $strAction, $objDatabase );
					}
				}
				$boolValid &= $this->valEmployeeApplicationSourceId();
				break;

			case 'validate_offer_details':
				$boolValid &= $this->valDesignationId();
				$boolValid &= $this->valPurchaseRequestId( $objDatabase );
				break;

			case 'validate_employee_share_reference':
				$boolValid &= $this->valPsWebsiteJobPostingId();
				$boolValid &= $this->valNameFirst();
				$boolValid &= $this->valNameLast();
				$boolValid &= $this->valEmailAddress( $objDatabase );
				$boolValid &= $this->valCellNumber( $objDatabase );
				break;

			case 'validate_applicant_details':
				$boolValid &= $this->valNameFull();
				$boolValid &= $this->valMobileNumber();
				$boolValid &= $this->valEmploymentOccupationTypeId();
				if( false == is_null( $strCaptchaResponse ) ) {
					$boolValid &= $this->verifyGoogleCaptcha( $strCaptchaResponse );
				}
				break;

			default:
				// no validation avaliable for delete and update action
		}

		return $boolValid;
	}

	public function checkStepPermission( $intJobPostingId, $objDatabase, $intCurrentStepId = NULL, $intJobPostingStepOrderNum = NULL ) {
		if( CCountry::CODE_USA == $this->getCountryCode() ) {
			return [ 'success' => true ];
		}

		$objJobPosting = CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $intJobPostingId, $objDatabase );

		if( false == valObj( $objJobPosting, 'CPsWebsiteJobPosting' ) ) {
			return [ 'success' => false, 'message' => 'Failed to load job posting details.' ];
		}

		if( false == $objJobPosting->getIsFollowAllSteps() ) {
			return [ 'success' => true ];
		}

		$arrintCurrentEmployeeApplicationStep = CEmployeeApplicationSteps::createService()->fetchCurrentEmployeeApplicationStepByEmployeeApplicationId( $this->getId(), $objDatabase );

		if( false == valArr( $arrintCurrentEmployeeApplicationStep ) ) {
			return [ 'success' => false, 'message' => 'Previous step has not been completed.' ];
		}

		if( false == empty( $intCurrentStepId ) ) {
			$objCurrentPsJobPostingStep = \Psi\Eos\Admin\CPsJobPostingSteps::createService()->fetchPsJobPostingStepById( $intCurrentStepId, $objDatabase );

			if( false == valObj( $objCurrentPsJobPostingStep, 'CPsJobPostingStep' ) ) {
				return [ 'success' => false, 'message' => 'Failed to load job posting step details.' ];
			}

			$intJobPostingStepOrderNum = $objCurrentPsJobPostingStep->getOrderNum();

			// this condition is for when application status is set to applied new at time of 1st interview scheduled if test score is not set
			if( CPsJobPostingStep::FIRST_INTERVIEW == $objCurrentPsJobPostingStep->getName() && CPsJobPostingStepType::SCREENING == $arrintCurrentEmployeeApplicationStep[0]['ps_job_posting_step_type_id'] && CPsJobPostingStatus::SELECTED == $arrintCurrentEmployeeApplicationStep[0]['ps_job_posting_status_id'] ) {
				return [ 'success' => true ];
			}
		}

		if( true == empty( $intJobPostingStepOrderNum ) ) {
			return [ 'success' => false, 'message' => 'Invalid step order.' ];
		}

		if( true == is_null( $arrintCurrentEmployeeApplicationStep[0]['step_order_num'] ) || $intJobPostingStepOrderNum <= $arrintCurrentEmployeeApplicationStep[0]['step_order_num'] ) {
			return [ 'success' => true ];
		}

		$intPreviousStepOrderNum = $intJobPostingStepOrderNum - 1;

		if( CPsJobPostingStatus::SELECTED == $arrintCurrentEmployeeApplicationStep[0]['ps_job_posting_status_id'] && $intPreviousStepOrderNum == $arrintCurrentEmployeeApplicationStep[0]['step_order_num'] ) {
			return [ 'success' => true ];
		}

		return [ 'success' => false, 'message' => 'Previous step has not been completed.' ];
	}

	public function checkForPrAssociation( $objAdminDatabase, $boolDoNotNullPr = false ) {
		$arrmixEmployeeApplicationDetails	= ( array ) CEmployeeApplicationSteps::createService()->fetchEmployeeApplicationStepDetails( [ $this->getId() ], $objAdminDatabase );

		if( CCountry::CODE_USA == $this->getCountryCode() && true == valArr( $arrmixEmployeeApplicationDetails ) ) {
			return $arrmixAssociatedPrDetail = ['purchase_request_id' => $arrmixEmployeeApplicationDetails[0]['purchase_request_id']];
		}
		$arrmixAssociatedPrDetail			= [ 'after_cool_down_period' => true, 'purchase_request_id' => ( true == is_null( $arrmixEmployeeApplicationDetails[0]['ps_job_posting_step_status_id'] ) ) ? $arrmixEmployeeApplicationDetails[0]['purchase_request_id'] : NULL ];

		if( CPsJobPostingStatus::REJECTED == $arrmixEmployeeApplicationDetails[0]['ps_job_posting_status_id'] && strtotime( '-6 month', strtotime( 'now' ) ) < strtotime( date( 'm/d/Y', strtotime( $arrmixEmployeeApplicationDetails[0]['created_on'] ) ) ) ) {
			$arrmixAssociatedPrDetail['after_cool_down_period'] = false;
		} elseif( true == valId( $arrmixEmployeeApplicationDetails[0]['purchase_request_id'] ) ) {

			$intPurchaseRequestId								= ( CPsJobPostingStatus::REJECTED != $arrmixEmployeeApplicationDetails[0]['ps_job_posting_status_id'] ) ? $arrmixEmployeeApplicationDetails[0]['purchase_request_id'] : NULL;
			$objPurchaseRequest									= \Psi\Eos\Admin\CPurchaseRequests::createService()->fetchPurchaseRequestById( $intPurchaseRequestId, $objAdminDatabase );
			$arrmixAssociatedPrDetail['purchase_request_id']	= NULL;

			if( true === valId( $intPurchaseRequestId ) && true === valObj( $objPurchaseRequest, 'CPurchaseRequest' ) ) {
				$arrmixAssociatedPrDetail['purchase_request_id']	= $intPurchaseRequestId;
				$arrobjJobPostingPurchaseRequests					= rekeyObjects( 'PurchaseRequestId', \Psi\Eos\Admin\CJobPostingPurchaseRequests::createService()->fetchJobPostingPurchaseRequestsByPurchaseRequestId( $intPurchaseRequestId, $objAdminDatabase ) );

				if( true == valObj( $arrobjJobPostingPurchaseRequests[$intPurchaseRequestId], 'CJobPostingPurchaseRequest' ) ) {
					$intPrAssoicatedJobPostingId = $arrobjJobPostingPurchaseRequests[$intPurchaseRequestId]->getPsJobPostingId();
					$arrmixAssociatedPrDetail['need_to_associate_pr'] = $this->getPsWebsiteJobPostingId() != $intPrAssoicatedJobPostingId;
				}
				// Check if PR is PR_ON_HOLD
				if( false == $arrmixAssociatedPrDetail['need_to_associate_pr'] ) {

					if( CPurchaseRequestStatusType::ID_PR_ON_HOLD == $objPurchaseRequest->getPurchaseRequestStatusTypeId() ) {
						$arrmixAssociatedPrDetail['need_to_associate_pr']	= true;
						$arrmixAssociatedPrDetail['pr_status_id']				= $objPurchaseRequest->getPurchaseRequestStatusTypeId();
					}
				}
			}
			$arrobjEmployeeApplications = rekeyObjects( 'Id', CEmployeeApplications::createService()->fetchEmployeeApplicationsByPurchaseRequestId( $intPurchaseRequestId, $objAdminDatabase ) );

			if( false == $boolDoNotNullPr && true == valArr( $arrobjEmployeeApplications ) && false == array_key_exists( $this->getId(), $arrobjEmployeeApplications ) ) {
				$arrmixAssociatedPrDetail['purchase_request_id'] = NULL;
			}
		}

		return $arrmixAssociatedPrDetail;
	}

	public function getEmployeeApplicationStepStatus( $intPsJobPostingStepStatusId, $objDatabase ) {
		$arrstrEmployeeStatusName	= CPsJobPostingStepStatuses::fetchPsJobPostingStepStatusNameById( $intPsJobPostingStepStatusId, $objDatabase );

		if( false == valArr( $arrstrEmployeeStatusName ) ) {
			return [ 'success' => false, 'message' => 'Failed to load job posting step status.' ];
		}

		$strEmployeeStatusName		= '<strong>' . $arrstrEmployeeStatusName['current_step'] . '  ' . $arrstrEmployeeStatusName['current_status'] . '</strong>';

		return $strEmployeeStatusName;
	}

	public function compareTwoArrays( $arrmixFirst, $arrmixSecond ) {
		if( true == valArr( $arrmixFirst ) && true == valArr( $arrmixSecond ) && false == valArr( array_diff( $arrmixFirst, $arrmixSecond ) ) && false == valArr( array_diff( $arrmixSecond, $arrmixFirst ) ) ) {
			return true;
		}
		return false;
	}

}

?>
