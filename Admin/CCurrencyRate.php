<?php

class CCurrencyRate extends CBaseCurrencyRate {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrencyRateDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExchangeRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>