<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CBusinessUnits
 * Do not add any new functions to this class.
 */

class CBusinessUnits extends CBaseBusinessUnits {

	public static function fetchAllBuHrEmployeeIds( $objDatabase ) {
		$strSql = 'SELECT
						bu_hr_employee_id
					FROM
						business_units';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveBusinessUnitDetails( $objDatabase ) {
		$strSql = 'SELECT
						bu.*,
						e2.preferred_name as add_name,
						e.preferred_name as tpoc_name,
						e1.preferred_name as rpoc_name
					FROM
						business_units bu
						LEFT JOIN employees e2 on ( bu.director_employee_id = e2.id )
						LEFT JOIN employees e on ( bu.training_representative_employee_id = e.id )
						LEFT JOIN employees e1 on ( bu.recruiter_employee_id = e1.id )
					WHERE bu.deleted_by IS NULL
					ORDER BY bu.id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveBusinessUnitBuHrEmployeeId( $intBuHrEmployeeId, $intTeamId, $objDatabase ) {

		if( false == valId( $intBuHrEmployeeId ) || false == valId( $intTeamId ) ) {
			return false;
		}

		$strSql = 'SELECT
						bu.*
					FROM
						business_units bu
						LEFT JOIN teams t ON ( t.add_employee_id = bu.director_employee_id AND t.hr_representative_employee_id = bu.bu_hr_employee_id )
					WHERE 
						bu.bu_hr_employee_id = ' . ( int ) $intBuHrEmployeeId . '
						AND t.id = ' . ( int ) $intTeamId . '
						AND bu.deleted_by IS NULL';

		return self::fetchBusinessUnit( $strSql, $objDatabase );

	}

	public static function fetchAllBusinessUnitsWithRecruiters( $objDatabase ) {
		$strSql = 'SELECT
						bu.id,
						CONCAT( bu.name, \' (\', e.preferred_name, \')\' ) as name
					FROM
						business_units bu
						JOIN employees e ON ( e.id = bu.recruiter_employee_id )
					WHERE
						bu.deleted_on IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY
						bu.id';

		return self::fetchBusinessUnits( $strSql, $objDatabase );
	}

}
?>