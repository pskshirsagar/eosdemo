<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationEthnicities
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationEthnicities extends CBaseEmployeeApplicationEthnicities {

	public static function fetchEmployeeApplicationEthnicities( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmployeeApplicationEthnicity', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchEmployeeApplicationEthnicity( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmployeeApplicationEthnicity', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllEmployeeApplicationEthnicities( $objDatabase ) {
		$strSql	= 'SELECT * FROM employee_application_ethnicities where is_published = 1';
		return self::fetchEmployeeApplicationEthnicities( $strSql, $objDatabase );
	}
}
?>