<?php

class CVirtualForeignKey extends CBaseVirtualForeignKey {

	protected $m_arrmixCustomVariables;

	public function setDefaults() {
		parent::setDefaults();

		$this->setFrequencyId( CFrequency::DAILY );
		$this->setClusterIds( array( CCluster::RAPID ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = false, $boolDirectSet );

		$this->m_arrmixCustomVariables['frequency_name'] 	= ( isset( $arrmixValues['frequency_name'] ) ) ? trim( $arrmixValues['frequency_name'] ) : '';
		$this->m_arrmixCustomVariables['error_count'] 		= ( isset( $arrmixValues['error_count'] ) ) ? trim( $arrmixValues['error_count'] ) : '';
		$this->m_arrmixCustomVariables['total_error_count'] = ( isset( $arrmixValues['total_error_count'] ) ) ? trim( $arrmixValues['total_error_count'] ) : '';
		$this->m_arrmixCustomVariables['last_log_datetime'] = ( isset( $arrmixValues['last_log_datetime'] ) ) ? trim( $arrmixValues['last_log_datetime'] ) : '';
	}

	public function getCustomVariableByKey( $strKey ) {
		return ( true == array_key_exists( $strKey, $this->m_arrmixCustomVariables ) ) ? $this->m_arrmixCustomVariables[$strKey] : NULL;
	}

	public function valName() {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', 'Frequency is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valClusterIds() {
		$boolIsValid = true;
		if( false == valArr( $this->getClusterIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cluster_ids', 'Cluster is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valSourceDatabaseTypeId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getSourceDatabaseTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'source_database_type_id', 'Source database Type is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valSourceTableName( $objConnectDatabase ) {
		$boolIsValid = true;
		if( 0 == strlen( $this->getSourceTableName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'source_table_name', 'Source table name is required. ' ) );
		}

		if( true == $boolIsValid ) {
			$boolIsValid = $this->valTableExist( $this->getSourceDatabaseTypeId(), $objConnectDatabase, $this->getSourceTableName(), $strField = 'source_table_name' );
		}

		return $boolIsValid;
	}

	public function valSourceFields() {
		$boolIsValid = true;
		if( false == valArr( $this->getSourceFields() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'source_fields', 'Source fields is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valForeignDatabaseTypeId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getForeignDatabaseTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'foreign_database_type_id', 'Foreign database type is required. ' ) );
		}

		if( CDatabaseType::CLIENT != $this->getSourceDatabaseTypeId() && CDatabaseType::CLIENT == $this->getForeignDatabaseTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'foreign_database_type_id', 'Foreign database type should not be ' . CDatabaseType::CLIENT . ' if source database type will not ' . CDatabaseType::CLIENT . ' ' ) );
		}
		return $boolIsValid;
	}

	public function valForeignTableName( $objConnectDatabase ) {
		$boolIsValid = true;
		if( 0 == strlen( $this->getForeignTableName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'foreign_table_name', 'Foreign table name is required. ' ) );
		}

		if( true == $boolIsValid ) {
			$boolIsValid = $this->valTableExist( $this->getForeignDatabaseTypeId(), $objConnectDatabase, $this->getForeignTableName(), $strField = 'foreign_table_name' );
		}
		return $boolIsValid;
	}

	public function valForeignFields() {
		$boolIsValid = true;
		if( false == valArr( $this->getForeignFields() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'foreign_fields', 'Foreign fields is required. ' ) );
		}

		if( \Psi\Libraries\UtilFunctions\count( $this->getSourceFields() ) != \Psi\Libraries\UtilFunctions\count( $this->getForeignFields() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'foreign_fields', 'Foreign fields size should be same as source fields.' ) );
		}
		return $boolIsValid;
	}

	public function valDeveloperEmployeeId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getDeveloperEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'developer_employee_id', 'Developer employee is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valQaEmployeeId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getQaEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qa_employee_id', 'Qa employee is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valTableExist( $intDatabaseTypeId, $objConnectDatabase, $strTableName, $strField ) {

		if( false == valArr( $this->getClusterIds() ) ) return false;
		if( false == is_numeric( $intDatabaseTypeId ) ) return false;

		$intClusterId = $this->getClusterId();

		if( CDatabaseType::CONNECT == $intDatabaseTypeId ) {
			$objDatabase = $objConnectDatabase;
		} else {

			$arrobjDatabases = CDatabases::fetchClientDatabasesByDatabaseUserTypeIdByDatabaseTypeIdByClusterId( CDatabaseUserType::PS_PROPERTYMANAGER, $intDatabaseTypeId, $intClusterId, $objConnectDatabase );
			$objDatabase = $arrobjDatabases[array_rand( $arrobjDatabases )];

			if( false == is_null( $objDatabase ) && true == $objDatabase->validate( 'validate_open' ) ) {
				if( false == $objDatabase->open() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strField, 'Database Error: Invalid database credentials for database type ' . ( int ) $intDatabaseTypeId ) );
					return false;
				}
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strField, 'Database Error: Invalid database credentials for database type ' . ( int ) $intDatabaseTypeId ) );
				return false;
			}
		}

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strField, 'Unable to load database.' ) );
			return false;
		}

		if( false == self::isTableExist( $objDatabase, $strTableName ) ) {
			$arrstrDatabaseNames = CDatabaseType::getDatabaseNames();
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strField,  $strTableName . ' table is not exist for database type ' . $arrstrDatabaseNames[$intDatabaseTypeId] . '( ' . CCluster::getClusterNameByClusterId( $intClusterId ) . ' ). ' ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objConnectDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valFrequencyId();
				$boolIsValid &= $this->valClusterIds();
				$boolIsValid &= $this->valSourceDatabaseTypeId();
				$boolIsValid &= $this->valSourceTableName( $objConnectDatabase );
				$boolIsValid &= $this->valSourceFields();
				$boolIsValid &= $this->valForeignDatabaseTypeId();
				$boolIsValid &= $this->valForeignTableName( $objConnectDatabase );
				$boolIsValid &= $this->valForeignFields();
				$boolIsValid &= $this->valDeveloperEmployeeId();
				$boolIsValid &= $this->valQaEmployeeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function getClusterId() {
		if( true == in_array( CCluster::RAPID, $this->getClusterIds() ) ) {
			$intClusterId = CCluster::RAPID;
		} else {
			$intClusterId = CCluster::STANDARD;
		}

		return $intClusterId;
	}

	public static function isTableExist( $objDatabase, $strTableName ) {
		$strSql = 'SELECT count(*) from pg_class where relname = \'' . $strTableName . '\' and relkind = \'r\'';
		$arrintResults = fetchData( $strSql, $objDatabase );
		return ( 0 == $arrintResults[0]['count'] ) ? false : true;
	}

}
?>