<?php

use Psi\Eos\Admin\CPsAssets;
use Psi\Eos\Admin\CPsAssetTypes;

class CPsAssetType extends CBasePsAssetType {

	const SOFTWARE 					= 1;
	const HARDWARE 					= 2;
	const PC 		 				= 3;
	const MONITOR  					= 4;
	const KEYBOARD 					= 5;
	const MOUSE 					= 6;
	const HEADPHONE					= 7;
	const TABLET 					= 9;
	const LAPTOP					= 10;
	const DOCKING_STATION			= 16;
	const SERVER					= 22;
	const MAC_MINI					= 24;
	const INTERNET_DONGAL			= 28;
	const ALL_IN_ONE				= 33;
	const CELL_PHONE				= 39;
	const APPLE_MOUSE				= 42;
	const APPLE_KEYBOARD			= 43;
	const MAC_BOOK_PRO				= 44;
	const INTERNET_WIFI_DONGLE		= 46;
	const IMAC						= 48;
	const WIRELESS_KEYBOARD_MOUSE	= 54;
	const WEBCAM                	= 49;
	const XENTO_EQUIPMENT			= 57;
	const ENTRATA_EQUIPMENT			= 58;

	// Switch is a keyword therefore using the name as ps_asset_type_switch
	const PS_ASSET_TYPE_SWITCH		= 11;

	public static $c_arrintPsAssetTypesForWorkFromHome = [
		CPsAssetType::DOCKING_STATION => [ 'id' => CPsAssetType::DOCKING_STATION, 'name' => 'DOCKING STATION' ],
		CPsAssetType::MOUSE => [ 'id' => CPsAssetType::MOUSE, 'name' => 'MOUSE' ],
		CPsAssetType::MONITOR => [ 'id' => CPsAssetType::MONITOR, 'name' => 'MONITOR - I' ],
		CPsAssetType::KEYBOARD => [ 'id' => CPsAssetType::KEYBOARD, 'name' => 'KEYBOARD' ],
		CPsAssetType::HEADPHONE => [ 'id' => CPsAssetType::HEADPHONE, 'name' => 'HEADPHONE' ],
		CPsAssetType::INTERNET_DONGAL => [ 'id' => CPsAssetType::INTERNET_DONGAL, 'name' => 'INTERNET DONGLE' ]
	];

	public static $c_arrintPsAssetTypesForDesk		= [
		self::MONITOR,
		self::KEYBOARD,
		self::MOUSE
	];

	public static $c_arrintPsAssetTypesForAssetReport = [
		self::LAPTOP,
		self::ALL_IN_ONE,
		self::PC,
		self::MAC_BOOK_PRO,
		self::IMAC,
		self::MAC_MINI,
		self::MONITOR,
		self::KEYBOARD,
		self::MOUSE,
		self::HEADPHONE,
		self::DOCKING_STATION,
		self::TABLET,
		self::INTERNET_WIFI_DONGLE,
		self::CELL_PHONE,
		self::APPLE_MOUSE,
		self::APPLE_KEYBOARD,
		self::WIRELESS_KEYBOARD_MOUSE
	];

	public static $c_arrintPsAssetTypesForPortableAsset = [
		self::HEADPHONE,
		self::TABLET,
		self::INTERNET_WIFI_DONGLE,
		self::CELL_PHONE,
		self::WEBCAM
	];

	public static $c_arrintPsAssetTypesForOtherAsset = [
		self::LAPTOP,
		self::ALL_IN_ONE,
		self::PC,
		self::MAC_BOOK_PRO,
		self::IMAC,
		self::MAC_MINI,
		self::DOCKING_STATION,
		self::APPLE_MOUSE,
		self::APPLE_KEYBOARD,
		self::WIRELESS_KEYBOARD_MOUSE
	];

	public static $c_arrintSystemPsAssetTypes = [
		self::LAPTOP,
		self::ALL_IN_ONE,
		self::PC,
		self::MAC_BOOK_PRO,
		self::IMAC,
		self::MONITOR
	];

	protected $m_strEmployeeName;

	// get functions

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	// set functions

	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = $strEmployeeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['employee_name'] ) ) 				$this->setEmployeeName( $arrmixValues['employee_name'] );
		return;
	}

	public function valPsAssetTypeId( $objDatabase = NULL, $boolXentoEquipments = false ) {
		$boolIsValid = true;
		$strAssetType = $boolXentoEquipments ? 'Equipment' : 'Asset';

		if( true == is_null( $this->getPsAssetTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_type_id', $strAssetType . ' type is required.' ) );
			return $boolIsValid;
		}

		if( false == is_null( $this->getId() ) ) {
			$objPsAssetType 	= CPsAssetTypes::createService()->fetchPsAssetTypeById( $this->getId(), $objDatabase );
			$intPsAssetsCount	= CPsAssets::createService()->fetchPsAssetsCountByPsAssetTypeId( $this->getId(), $objDatabase );

			if( ( 0 < $intPsAssetsCount ) && ( $objPsAssetType->getPsAssetTypeId() != $this->getPsAssetTypeId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_type_id', 'Failed to update the ' . $strAssetType . ' type, it is being associated with ' . $strAssetType . 's.' ) );
				return $boolIsValid;
			}
		}

		if( true != is_null( $this->getDeletedBy() ) ) {
			$intAssetsCount = CPsAssets::createService()->fetchPsAssetsCountByPsAssetTypeId( $this->getId(), $objDatabase );

			if( 0 < $intAssetsCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Failed to update the ' . $strAssetType . ' type, it is being associated with ' . $strAssetType . 's.' ) );
				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valApproverEmployeeId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getApproverEmployeeId() ) && false == $this->getIsAutoApprove() ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approver_employee_id', 'Final approver is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase = NULL, $boolXentoEquipments = false ) {
		$boolIsValid = true;
		$strTypeName = 'Asset';
		if( true == $boolXentoEquipments ) {
			$strTypeName = 'Equipment';
		}
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', $strTypeName . ' name is required.' ) );
			return $boolIsValid;
		}

		if( false == preg_match( '/^[a-zA-Z0-9- _$&+,:;=?@#|\'<>.-^*()%!]{2,25}$/i', $this->getName() ) || true == is_numeric( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', $strTypeName . ' name is invalid.' ) );
			return $boolIsValid;
		}

		if( false == is_null( $this->getId() ) ) {
			$objPsAssetType 	= CPsAssetTypes::createService()->fetchPsAssetTypeById( $this->getId(), $objDatabase );
			$intPsAssetsCount	= CPsAssets::createService()->fetchPsAssetsCountByPsAssetTypeId( $this->getId(), $objDatabase );

			if( ( 0 < $intPsAssetsCount ) && ( \Psi\CStringService::singleton()->strtoupper( $objPsAssetType->getName() ) != \Psi\CStringService::singleton()->strtoupper( $this->getName() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Failed to update the ' . $strTypeName . ' name, it is being associated with ' . $strTypeName . 's.' ) );
				return $boolIsValid;
			}

	 		$intPsAssetTypesCount = CPsAssetTypes::createService()->fetchPsAssetTypeCountByIdByName( $this->getId(), $this->getName(), $objDatabase );

	 		if( 0 < $intPsAssetTypesCount ) {
	 			$boolIsValid = false;
	 			$this->valDeletedName( $objDatabase, $strTypeName );
	 		}

	 		return $boolIsValid;
		}

		$intPsAssetTypeCount = CPsAssetTypes::createService()->fetchPsAssetTypeCountByName( $this->getName(), $objDatabase );

		if( 0 < $intPsAssetTypeCount ) {
			$boolIsValid = false;
			$this->valDeletedName( $objDatabase, $strTypeName );
		}

		return $boolIsValid;
	}

	public function valCode( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( CPsAssetType::SOFTWARE == $this->getPsAssetTypeId() ) {
			$this->setCode( NULL );
			return $boolIsValid;
		}

		if( true == is_null( $this->getCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', 'Asset code is required.' ) );
			return $boolIsValid;
		}

		if( false == preg_match( '/^[a-zA-Z0-9]{2,25}$/i', $this->getCode() ) || true == is_numeric( $this->getCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', 'Asset code is invalid.' ) );
			return $boolIsValid;
		}

		if( false == is_null( $this->getId() ) ) {

			$objPsAssetType		  = CPsAssetTypes::createService()->fetchPsAssetTypeById( $this->getId(), $objDatabase );
			$intPsAssetTypeCount  = CPsAssets::createService()->fetchPsAssetsCountByPsAssetTypeId( $this->getId(), $objDatabase );

			if( ( 0 < $intPsAssetTypeCount ) && ( \Psi\CStringService::singleton()->strtoupper( $objPsAssetType->getCode() ) != \Psi\CStringService::singleton()->strtoupper( $this->getCode() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', 'Failed to update the asset code, it is being associated with assets.' ) );
				return $boolIsValid;
			}

			$intPsAssetTypeCount = CPsAssetTypes::createService()->fetchPsAssetTypeCountByIdByCode( $this->getId(), $this->getCode(), $objDatabase );

			if( 0 < $intPsAssetTypeCount ) {
				$boolIsValid = false;
				$this->valDeletedCode( $objDatabase );
				return $boolIsValid;
			}

			return $boolIsValid;
		}

		$intPsAssetTypeCount = CPsAssetTypes::createService()->fetchPsAssetTypeCountByCode( $this->getCode(), $objDatabase );

		if( 0 < $intPsAssetTypeCount ) {
			$boolIsValid = false;
			$this->valDeletedCode( $objDatabase );
		}

		return $boolIsValid;
	}

	public function valDeletedName( $objDatabase, $strTypeName = 'Asset' ) {
		$boolIsValid 				= false;
		$intDeletedPsAssetTypeCount = CPsAssetTypes::createService()->fetchDeletedPsAssetTypeCountByName( $this->getName(), $objDatabase );

		if( 0 < $intDeletedPsAssetTypeCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', $strTypeName . ' name is already exist but it is disabled.' ) );
			return $boolIsValid;
		}

		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', $strTypeName . ' name is already exist.' ) );
		return $boolIsValid;
	}

	public function valDeletedCode( $objDatabase ) {
		$boolIsValid 				= false;
		$intDeletedPsAssetTypeCount = CPsAssetTypes::createService()->fetchDeletedPsAssetTypeCountByCode( $this->getCode(), $objDatabase );

		if( 0 < $intDeletedPsAssetTypeCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', 'Asset code is already exist but it is disabled.' ) );
			return $boolIsValid;
		}

		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', 'Asset code is already exist.' ) );
		return $boolIsValid;
	}

	public function valDependentInformation( $objDatabase = NULL, $boolXentoEquipments = false ) {
		$boolIsValid = true;
		$strTypeName = 'Asset';
		if( true == $boolXentoEquipments ) {
			$strTypeName = 'Equipment';
		}
		$intAssetsCount = CPsAssets::createService()->fetchPsAssetsCountByPsAssetTypeId( $this->getId(), $objDatabase );

		if( 0 < ( int ) $intAssetsCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Failed to delete associated ' . $strTypeName . ' type(s).' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		if( true == is_null( $this->getOrderNum() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Order number is required.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolXentoEquipments = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPsAssetTypeId( $objDatabase );
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valApproverEmployeeId( $objDatabase );
				$boolIsValid &= $this->valOrderNum();
				$boolIsValid &= $this->valCode( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependentInformation( $objDatabase, $boolXentoEquipments );
				break;

			case 'validate_xento_equipment_insert':
				$boolIsValid &= $this->valPsAssetTypeId( $objDatabase, $boolXentoEquipments );
				$boolIsValid &= $this->valName( $objDatabase, $boolXentoEquipments );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>