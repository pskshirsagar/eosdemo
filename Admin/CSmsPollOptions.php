<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSmsPollOptions
 * Do not add any new functions to this class.
 */

class CSmsPollOptions extends CBaseSmsPollOptions {

	public static function fetchSmsPollOptionsByKeywordId( $intKeywordId, $objDatabase ) {
		$strSql = 'SELECT * FROM sms_poll_options WHERE keyword_id = ' . ( int ) $intKeywordId;
		return self::fetchSmsPollOptions( $strSql, $objDatabase );
	}

	public static function fetchSmsPollOptionsCountByKeywordId( $intKeywordId, $objDatabase ) {
		$strWhere = ' WHERE keyword_id = ' . ( int ) $intKeywordId;
		return self::fetchSmsPollOptionCount( $strWhere, $objDatabase );
	}

	public static function fetchSmsPollOptionByPollOptionByKeywordId( $strPollOption, $intKeywordId, $objDatabase ) {
		$strSql = 'SELECT * FROM sms_poll_options WHERE poll_option ILIKE \'' . ( string ) addslashes( $strPollOption ) . '\' AND keyword_id = ' . ( int ) $intKeywordId;
		return self::fetchSmsPollOption( $strSql, $objDatabase );
	}

	public static function fetchTotalUniqueVotesCountByKeywordId( $intKeywordId, $objDatabase ) {
		$strSql = 'SELECT sum( unique_vote_count ) FROM sms_poll_options WHERE keyword_id = ' . ( int ) $intKeywordId;
		$arrintTotalMembersCount = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintTotalMembersCount[0]['sum'] ) ) return $arrintTotalMembersCount[0]['sum'];
		return 0;
	}
}
?>