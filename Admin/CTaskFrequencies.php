<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskFrequencies
 * Do not add any new functions to this class.
 */

class CTaskFrequencies extends CBaseTaskFrequencies {

	public static function fetchTaskFrequencies( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTaskFrequency', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchTaskFrequency( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTaskFrequency', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllTaskFrequencies( $objDatabase ) {
		$strSql = 'SELECT * FROM task_frequencies ORDER BY order_num';
		return self::fetchTaskFrequencies( $strSql, $objDatabase );
	}
}
?>