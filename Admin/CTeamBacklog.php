<?php

class CTeamBacklog extends CBaseTeamBacklog {

	const LIMIT_FOR_BACKLOG_DESCRIPTION = 140;
	const LIMIT_FOR_BACKLOG_NAME        = 50;

	protected $m_strTeamBacklogCreatorName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getTeamBacklogCreatorName() {
		return $this->m_strTeamBacklogCreatorName;
	}

	/**
	 * Set Functions
	 */

	public function setTeamBacklogCreatorName( $intPreferredName ) {
		$this->m_strTeamBacklogCreatorName = CStrings::strTrimDef( $intPreferredName, 50, NULL, true );
	}

	public function valName( $boolValidateUpdate, $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_backlog_name', 'Please enter name. ' ) );
			return $boolIsValid;
		}

		if( $this->getName() != strip_tags( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_backlog_name', 'Valid name is required. ' ) );
			return $boolIsValid;
		}

		if( self::LIMIT_FOR_BACKLOG_NAME < \Psi\CStringService::singleton()->strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_backlog_name', 'Name must be less than or equal to ' . self::LIMIT_FOR_BACKLOG_NAME . ' character. ' ) );
			return $boolIsValid;
		}

		if( true == $boolIsValid && true == isset( $objDatabase ) ) {

			$strSql 	= ' WHERE name ILIKE E\'' . trim( $this->getName() ) . '\' and deleted_by IS NULL';

			if( true == $boolValidateUpdate ) {
				$strSql .= ' AND id <> ' . $this->getId();
			}

			$intCount 	= \Psi\Eos\Admin\CTeamBacklogs::createService()->fetchRowCount( $strSql, 'team_backlogs', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' Backlog name ' . $this->getName() . ' already exist. ' ) );
			}
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'backlog_description', 'Please enter description. ' ) );
			return $boolIsValid;
		}

		if( self::LIMIT_FOR_BACKLOG_DESCRIPTION < \Psi\CStringService::singleton()->strlen( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_backlog_name', 'Description must be less than or equal to ' . self::LIMIT_FOR_BACKLOG_DESCRIPTION . ' character. ' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valSprintSize() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getSprintSize() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'backlog_sprint_size', 'Please enter numeric estimated sprint size. ' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( false, $objAdminDatabase );
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valSprintSize();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( true, $objAdminDatabase );
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valSprintSize();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['team_backlog_creator_name'] ) ) {
			$this->setTeamBacklogCreatorName( $arrmixValues['team_backlog_creator_name'] );
		}
	}

}
?>
