<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemPropertyStatusTypes
 * Do not add any new functions to this class.
 */

class CSemPropertyStatusTypes extends CBaseSemPropertyStatusTypes {

	public static function fetchSemPropertyStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CSemPropertyStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchSemUnitSpaceStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSemPropertyStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchSemPropertyStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSemPropertyStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>