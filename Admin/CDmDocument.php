<?php

class CDmDocument extends CBaseDmDocument {

	const DM_DOCUMENT_TITLE	= 'Digital Marketing Documents';

	public function validateClientDocs( $arrstrDmDocumentNames, $arrstrDmDocumentUrls ) {
		$boolIsValid = true;

		if( !valArr( $arrstrDmDocumentNames ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please enter client document name or url.' ) );
			$boolIsValid = false;
		}

		if( \Psi\Libraries\UtilFunctions\count( array_filter( $arrstrDmDocumentNames ) ) != \Psi\Libraries\UtilFunctions\count( array_filter( array_unique( $arrstrDmDocumentNames ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please enter unique document name.' ) );
			$boolIsValid = false;
		}

		if( \Psi\Libraries\UtilFunctions\count( array_filter( $arrstrDmDocumentUrls ) ) != \Psi\Libraries\UtilFunctions\count( array_filter( array_unique( $arrstrDmDocumentUrls ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Please enter unique document url.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrstrDmDocumentNames, $arrstrDmDocumentUrls ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_client_docs':
				$boolIsValid = $this->validateClientDocs( $arrstrDmDocumentNames, $arrstrDmDocumentUrls );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
