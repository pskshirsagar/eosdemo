<?php

class CJobPostingQuestionResponse extends CBaseJobPostingQuestionResponse {

	const FORMER_EMPLOYEE_ANSWER_YES = 'Yes';

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>