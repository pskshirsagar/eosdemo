<?php

class CEmployeeApplicationCompany extends CBaseEmployeeApplicationCompany {

	protected $m_strHiredEmployeeCount;
	protected $m_strInterviewedEmployeeCount;
	protected $m_strAppliedEmployeeCount;
	protected $m_strHiredEmployeeApplicationIds;
	protected $m_strInterviewedEmployeeApplicationIds;
	protected $m_strAppliedEmployeeApplicationIds;

	public function getHiredEmployeeCount() {
		return $this->m_strHiredEmployeeCount;
	}

	public function sqlHiredEmployeeCount() {
		return ( true == isset( $this->m_strHiredEmployeeCount ) ) ? '\'' . addslashes( $this->m_strHiredEmployeeCount ) . '\'' : 'NULL';
	}

	public function setHiredEmployeeCount( $strHiredEmployeeCount ) {
		$this->set( 'm_strHiredEmployeeCount', CStrings::strTrimDef( $strHiredEmployeeCount, -1, NULL, true ) );
	}

	public function getInterviewedEmployeeCount() {
		return $this->m_strInterviewedEmployeeCount;
	}

	public function sqlInterviewedEmployeeCount() {
		return ( true == isset( $this->m_strInterviewedEmployeeCount ) ) ? '\'' . addslashes( $this->m_strInterviewedEmployeeCount ) . '\'' : 'NULL';
	}

	public function setInterviewedEmployeeCount( $strInterviewedEmployeeCount ) {
		$this->set( 'm_strInterviewedEmployeeCount', CStrings::strTrimDef( $strInterviewedEmployeeCount, -1, NULL, true ) );
	}

	public function getAppliedEmployeeCount() {
		return $this->m_strAppliedEmployeeCount;
	}

	public function sqlAppliedEmployeeCount() {
		return ( true == isset( $this->m_strAppliedEmployeeCount ) ) ? '\'' . addslashes( $this->m_strAppliedEmployeeCount ) . '\'' : 'NULL';
	}

	public function setAppliedEmployeeCount( $strAppliedEmployeeCount ) {
		$this->set( 'm_strAppliedEmployeeCount', CStrings::strTrimDef( $strAppliedEmployeeCount, -1, NULL, true ) );
	}

	public function getHiredEmployeeApplicationIds() {
		return $this->m_strHiredEmployeeApplicationIds;
	}

	public function setHiredEmployeeApplicationIds( $strHiredEmployeeApplicationIds ) {
		$this->set( 'm_strHiredEmployeeApplicationIds', CStrings::strTrimDef( $strHiredEmployeeApplicationIds, -1, NULL, true ) );
	}

	public function getInterviewedEmployeeApplicationIds() {
		return $this->m_strInterviewedEmployeeApplicationIds;
	}

	public function setInterviewedEmployeeApplicationIds( $strInterviewedEmployeeApplicationIds ) {
		$this->set( 'm_strInterviewedEmployeeApplicationIds', CStrings::strTrimDef( $strInterviewedEmployeeApplicationIds, -1, NULL, true ) );
	}

	public function getAppliedEmployeeApplicationIds() {
		return $this->m_strAppliedEmployeeApplicationIds;
	}

	public function setAppliedEmployeeApplicationIds( $strAppliedEmployeeApplicationIds ) {
		$this->set( 'm_strAppliedEmployeeApplicationIds', CStrings::strTrimDef( $strAppliedEmployeeApplicationIds, -1, NULL, true ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['total_hired_employees'] ) ) {
			$this->setHiredEmployeeCount( trim( $arrmixValues['total_hired_employees'] ) );
		}
		if( true == isset( $arrmixValues['total_interviewed_employees'] ) ) {
			$this->setInterviewedEmployeeCount( trim( $arrmixValues['total_interviewed_employees'] ) );
		}
		if( true == isset( $arrmixValues['total_applied_employees'] ) ) {
			$this->setAppliedEmployeeCount( trim( $arrmixValues['total_applied_employees'] ) );
		}
		if( true == isset( $arrmixValues['hired_employee_application_ids'] ) ) {
			$this->setHiredEmployeeApplicationIds( trim( $arrmixValues['hired_employee_application_ids'] ) );
		}
		if( true == isset( $arrmixValues['interviewed_employee_application_ids'] ) ) {
			$this->setInterviewedEmployeeApplicationIds( trim( $arrmixValues['interviewed_employee_application_ids'] ) );
		}
		if( true == isset( $arrmixValues['applied_employee_application_ids'] ) ) {
			$this->setAppliedEmployeeApplicationIds( trim( $arrmixValues['applied_employee_application_ids'] ) );
		}

	}

	public function valCompanyName( $intCompanyIndex ) {
		$boolIsValid = true;
		$strErrorMessage = ' Company';
		if( true == is_null( $this->getCompanyName() ) ) {
			$boolIsValid = false;
			if( false == is_null( $intCompanyIndex ) ) {
				$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current company' : 'Previous company';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', $strErrorMessage . ' name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCity( $intCompanyIndex ) {
		$boolIsValid = true;
		$strErrorMessage = ' Company';
		if( true == is_null( $this->getCity() ) ) {
			$boolIsValid = false;

			if( false == is_null( $intCompanyIndex ) ) {
				$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current company' : 'Previous company';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', $strErrorMessage . ' city is required.' ) );
		}
		return $boolIsValid;
	}

	public function valLocation() {
		$boolIsValid = true;
		$strErrorMessage = ' Company';
		if( true == is_null( $this->getLocation() ) ) {
			$boolIsValid = false;

			if( false == is_null( $intCompanyIndex ) ) {
				$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current company' : 'Previous company';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'location', $strErrorMessage . ' location is required.' ) );
		}
		return $boolIsValid;
	}

	public function valTotalStrength( $intCompanyIndex ) {
		$boolIsValid = true;
		$strErrorMessage = ' Company';
		if( true == is_null( $this->getTotalStrength() ) ) {
			$boolIsValid = false;

			if( false == is_null( $intCompanyIndex ) ) {
				$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current company' : 'Previous company';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_strength', $strErrorMessage . ' employee strength is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPhpStrength( $intCompanyIndex ) {
		$boolIsValid = true;
		$strErrorMessage = ' Company';
		if( false == is_null( $this->getPhpStrength() ) && false == is_null( $this->getTotalStrength() ) ) {
			$intTotalStrength	= ( \Psi\CStringService::singleton()->strpos( $this->getTotalStrength(), '-' ) ) ? \Psi\CStringService::singleton()->strstr( $this->getTotalStrength(), '-', true ) : 1000; // else above 1000
			$intPhpStrength 	= ( \Psi\CStringService::singleton()->strpos( $this->getPhpStrength(), '-' ) ) ? \Psi\CStringService::singleton()->strstr( $this->getPhpStrength(), '-', true ) : 1000; // else above 1000

			if( $intTotalStrength < $intPhpStrength ) {
				$boolIsValid = false;

				if( false == is_null( $intCompanyIndex ) ) {
					$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current company' : 'Previous company';
				}
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'php_strength', $strErrorMessage . ' PHP strength should be less than total strength.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valQaStrength( $intCompanyIndex ) {
		$boolIsValid = true;
		$strErrorMessage = ' Company';
		if( false == is_null( $this->getQaStrength() ) && false == is_null( $this->getTotalStrength() ) ) {
			$intTotalStrength	= ( \Psi\CStringService::singleton()->strpos( $this->getTotalStrength(), '-' ) ) ? \Psi\CStringService::singleton()->strstr( $this->getTotalStrength(), '-', true ) : 1000; // else above 1000
			$intQaStrength 	= ( \Psi\CStringService::singleton()->strpos( $this->getQaStrength(), '-' ) ) ? \Psi\CStringService::singleton()->strstr( $this->getQaStrength(), '-', true ) : 1000; // else above 1000

			if( $intTotalStrength < $intQaStrength ) {
				$boolIsValid = false;
				if( false == is_null( $intCompanyIndex ) ) {
					$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current company' : 'Previous company';
				}
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qa_strength', $strErrorMessage . ' QA strength should be less than total strength.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intCompanyIndex = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCompanyName( $intCompanyIndex );
				$boolIsValid &= $this->valCity( $intCompanyIndex );
				$boolIsValid &= $this->valTotalStrength( $intCompanyIndex );
				$boolIsValid &= $this->valPhpStrength( $intCompanyIndex );
				$boolIsValid &= $this->valQaStrength( $intCompanyIndex );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>