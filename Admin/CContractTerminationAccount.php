<?php

class CContractTerminationAccount extends CBaseContractTerminationAccount {

	public function setDefaults() {

		$this->setUpdatedBy( SYSTEM_USER_ID );
		$this->setUpdatedOn( date( 'Y-m-d' ) );
		$this->setCreatedBy( SYSTEM_USER_ID );
		$this->setCreatedOn( date( 'Y-m-d' ) );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>