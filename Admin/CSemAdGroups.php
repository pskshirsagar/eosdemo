<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdGroups
 * Do not add any new functions to this class.
 */

class CSemAdGroups extends CBaseSemAdGroups {

	public static function fetchPreExistingSemAdGroup( $objSemAdGroup, $objAdminDatabase ) {

		$strWhere = ' WHERE id <> ' . ( int ) $objSemAdGroup->getId();

		if( false == is_null( $objSemAdGroup->getSemLocationTypeId() ) && true == is_numeric( $objSemAdGroup->getSemLocationTypeId() ) ) {
			$strWhere .= ' AND sem_location_type_id = ' . ( int ) $objSemAdGroup->getSemLocationTypeId();
		}

		$strWhere .= ' AND sem_campaign_id = ' . ( int ) $objSemAdGroup->getSemCampaignId() .
					 ' AND lower( name ) = lower(\'' . trim( addslashes( $objSemAdGroup->getName() ) ) . '\') LIMIT 1';

		return self::fetchSemAdGroupCount( $strWhere, $objAdminDatabase );
	}

	public static function fetchAllAdGroups( $objAdminDatabase ) {
		return self::fetchSemAdGroups( 'SELECT * FROM sem_ad_groups ', $objAdminDatabase );
	}

	public static function fetchAllSemAdGroupsWithCampaignName( $objAdminDatabase ) {

		$strSql = 'SELECT
						sag.*,sc.name as campaign_name
					FROM
						sem_ad_groups sag,
						sem_campaigns sc
					WHERE
						sag.sem_campaign_id = sc.id
						AND sag.is_published = 1
						AND sag.is_system = 1
						AND sc.is_disabled = 0
						AND sag.deleted_on IS NULL';

		return self::fetchSemAdGroups( $strSql, $objAdminDatabase );
	}

	public static function fetchSemAdGroupBySemCampaignIdByNameOrId( $intSemCampaignId, $strAdGroupNameOrId, $objAdminDatabase ) {

		$strSqlCondition = '';

		if( true == is_numeric( $strAdGroupNameOrId ) && 0 < $strAdGroupNameOrId ) {
			$strSqlCondition = ' AND sag.id = ' . ( int ) $strAdGroupNameOrId;
		} else {
			$strSqlCondition = ' AND regexp_replace( sag.name,\'[^0-9a-zA-Z]+\', \'\', \'g\') ILIKE \'' . preg_replace( '/[^0-9a-zA-Z]+/', '',  trim( addslashes( $strAdGroupNameOrId ) ) ) . '\'  LIMIT 1';
		}

		$strSql = 'SELECT
						sag.*
					FROM
						sem_ad_groups sag,
						sem_campaigns sc
					WHERE
						sag.sem_campaign_id = sc.id
						AND sc.id = ' . ( int ) $intSemCampaignId . $strSqlCondition;

		return self::fetchSemAdGroup( $strSql, $objAdminDatabase );
	}

}
?>