<?php

class CPsAssetNote extends CBasePsAssetNote {

	const ASSET_NOTES_LIMIT		= 5;
	const ASSET_NOTE_CHAR_LIMIT	= 50;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsAssetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>