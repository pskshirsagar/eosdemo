<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserModulePermissions
 * Do not add any new functions to this class.
 */

class CUserModulePermissions extends CBaseUserModulePermissions {

	public static function fetchUserModulePermissionsByUserId( $intUserId, $objDatabase ) {

		// user permissioned modules
		$strSql = 'SELECT
						*
					FROM
						user_module_permissions
					WHERE
						user_id = ' . ( int ) $intUserId;

		return self::fetchUserModulePermissions( $strSql, $objDatabase );
	}

	public static function fetchPermissionedModuleIdsByUserId( $intUserId, $objDatabase ) {

		// user permissioned modules
		$strSql = 'SELECT
							module_id,
							CASE WHEN has_write_access = 1 THEN 0 ELSE 1 END AS has_write_access
						FROM
							user_module_permissions
						WHERE (
								has_read_access = 1
								OR has_write_access = 1
							)
							AND user_id = ' . ( int ) $intUserId;
		// group permissioned modules
		$strSql .= 'UNION
							SELECT
								module_id,
								CASE WHEN max( has_write_access ) = 1 THEN 0 ELSE 1 END AS has_write_access
							FROM
								group_module_permissions
							WHERE
								group_id IN (
												SELECT
													ug.group_id
												FROM
													user_groups ug
												WHERE
													ug.user_id = ' . ( int ) $intUserId . '
													AND ug.deleted_by IS NULL
								)
								AND	module_id NOT IN (
														SELECT
															ump.module_id
														FROM
															user_module_permissions ump
														WHERE
															ump.user_id = ' . ( int ) $intUserId . '
								)
							GROUP BY module_id';

		$arrmixPermissionedModules = fetchData( $strSql, $objDatabase );

		$arrstrModules = array();

		if( true == valArr( $arrmixPermissionedModules ) ) {
			foreach( $arrmixPermissionedModules as $arrmixPermissionedModule ) {
				$arrstrModules[$arrmixPermissionedModule['module_id']] = $arrmixPermissionedModule['has_write_access'];
			}
		}

		return $arrstrModules;
	}

	public static function fetchPermissionByModuleIdUserId( $intModuleId, $intUserId, $objDatabase ) {

		// user permissioned modules
		$strSql = 'SELECT
							module_id,
							has_write_access
						FROM
							user_module_permissions
						WHERE (
								has_read_access = 1
								OR has_write_access = 1
							)
							AND user_id = ' . ( int ) $intUserId . '
							 AND module_id = ' . ( int ) $intModuleId;
		// group permissioned modules
		$strSql .= 'UNION
							SELECT
								module_id,
								max( has_write_access )
							FROM
								group_module_permissions
							WHERE
								module_id = ' . ( int ) $intModuleId . '
								AND group_id IN (
												SELECT
													ug.group_id
												FROM
													user_groups ug
												WHERE
													ug.user_id = ' . ( int ) $intUserId . '
													AND ug.deleted_by IS NULL
								)
								AND	module_id NOT IN (
														SELECT
															ump.module_id
														FROM
															user_module_permissions ump
														WHERE
															ump.user_id = ' . ( int ) $intUserId . '
								)
							GROUP BY module_id';

		$arrmixPermissionedModules = fetchData( $strSql, $objDatabase );

		$arrstrModules = array();

		if( true == valArr( $arrmixPermissionedModules ) ) {
			foreach( $arrmixPermissionedModules as $arrmixPermissionedModule ) {
				$arrstrModules[$arrmixPermissionedModule['module_id']] = $arrmixPermissionedModule['has_write_access'];
			}
		}

		return $arrstrModules;
	}

}
?>