<?php

class CContractPropertyConfiguration extends CBaseContractPropertyConfiguration {

	public function valContractPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getContractPropertyId() ) || false == is_numeric( $this->getContractPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_property_id', 'contract property id is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valProductConfigurationTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getProductConfigurationTypeId() ) || false == is_numeric( $this->getProductConfigurationTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'product_configuration_id', 'product configuration id is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyUserId() ) || false == is_numeric( $this->getCompanyUserId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', 'company user id is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valNumericValue() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getNumericValue() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'numeric_value', 'value should be numeric.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valContractPropertyId();
				$boolIsValid &= $this->valProductConfigurationTypeId();
				$boolIsValid &= $this->valCompanyUserId();
				$boolIsValid &= $this->valNumericValue();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>