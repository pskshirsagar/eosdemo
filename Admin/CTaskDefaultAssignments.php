<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskDefaultAssignments
 * Do not add any new functions to this class.
 */

class CTaskDefaultAssignments extends CBaseTaskDefaultAssignments {

	public static function fetchTaskDefaultAssignmentsByTaskTypeIds( $arrintTaskTypeIds, $objDatabase, $boolFromTaskTypes = false ) {

		if( false == valArr( $arrintTaskTypeIds ) ) return [];
		$strWhere = ( true == $boolFromTaskTypes ) ? ' AND ps_product_id IS NOT NULL' : '';

		$strSql = 'SELECT *
					FROM task_default_assignments
					WHERE task_type_id IN (' . implode( ',', $arrintTaskTypeIds ) . ')
					AND deleted_by IS NULL
					AND deleted_on IS NULL ' . $strWhere;

		return self::fetchTaskDefaultAssignments( $strSql, $objDatabase );
	}

	public static function fetchTaskDefaultAssignmentsByTaskTypeId( $intTaskTypeId, $objDatabase ) {

		if( false == is_numeric( $intTaskTypeId ) ) return [];

		$strSql = 'SELECT *
					FROM task_default_assignments
					WHERE task_type_id =' . ( int ) $intTaskTypeId .
					'AND deleted_by IS NULL
					AND deleted_on IS NULL';

		return self::fetchTaskDefaultAssignments( $strSql, $objDatabase );
	}

	public static function fetchTaskDefaultAssignmentsByTaskTypeIdByProductId( $intTaskTypeId, $objDatabase, $intPsProductId = NULL, $intPsProductOptionId = NULL ) {

		if( false == is_numeric( $intTaskTypeId ) ) return [];

		$strWhere  = ( true == valId( $intPsProductOptionId ) ) ? ' AND ps_product_option_id =' . ( int ) $intPsProductOptionId : '';
		$strWhere .= ' AND ps_product_id =' . ( int ) $intPsProductId;

		$strSql = 'SELECT *
					FROM task_default_assignments
					WHERE task_type_id =' . ( int ) $intTaskTypeId . $strWhere .
		          ' AND deleted_by IS NULL
					AND deleted_on IS NULL';

		return self::fetchTaskDefaultAssignments( $strSql, $objDatabase );
	}

}
?>