<?php

class CPackageProductsRelationship extends CBasePackageProductsRelationship {

	protected $m_strLeadPackageId;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPackageProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBundlePsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valListRecurringPrice() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valListSetupPrice() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createClone( $intPackageProductId, $intUserId, $objDatabase ) {
		$objPackageProductsRelationship = clone $this;

		$objPackageProductsRelationship->setId( $objPackageProductsRelationship->fetchNextId( $objDatabase ) );
		$objPackageProductsRelationship->setPackageProductId( $intPackageProductId );
		$objPackageProductsRelationship->setCreatedBy( $intUserId );
		$objPackageProductsRelationship->setCreatedOn( 'NOW()' );
		$objPackageProductsRelationship->setUpdatedBy( $intUserId );
		$objPackageProductsRelationship->setUpdatedOn( 'NOW()' );

		return $objPackageProductsRelationship;
	}

	public function getPsLeadPackageId() {
		return $this->m_strLeadPackageId;
	}

	public function setPsLeadPackageId( $intLeadPackageId ) {
		$this->m_strLeadPackageId = $intLeadPackageId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['lead_package_id'] ) ) $this->setPsLeadPackageId( $arrmixValues['lead_package_id'] );
	}

}
?>