<?php

class CBusinessUnit extends CBaseBusinessUnit {

	const ID_TRIDENT_PREETAM_DP = 7;
	protected $m_strNameFull;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['name_full'] ) ) $this->setNameFull( $arrmixValues['name_full'] );
		return;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function valName() {
		$boolValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Business Unit Name is required. ' ) );
		}
		return $boolValid;
	}

	public function valDescription() {
		$boolValid = true;
		if( true == is_null( $this->getDescription() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Business Unit Description is required. ' ) );
		}
		return $boolValid;
	}

	public function valBuHrEmployeeId() {
		$boolValid = true;
		if( false == valId( $this->getBuHrEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bu_hr_employee_id', 'Business Unit HR is required. ' ) );
		}
		return $boolValid;
	}

	public function valTrainingRepresentativeEmployeeId() {
		$boolValid = true;
		if( false == valId( $this->getTrainingRepresentativeEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'training_representative_employee_id', 'Business Unit Training point of contact is required. ' ) );
		}
		return $boolValid;
	}

	public function valRecruiterEmployeeId() {
		$boolValid = true;
		if( false == valId( $this->getRecruiterEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recruiter_employee_id', 'Recruiter point of contact is required. ' ) );
		}
		return $boolValid;
	}

	public function valDirectorEmployeeId() {
		$boolValid = true;
		if( false == valId( $this->getDirectorEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'add_employee_id', 'Associate Director of Development is required. ' ) );
		}
		return $boolValid;
	}

	public function validate( $strAction ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolValid &= $this->valDirectorEmployeeId();
				$boolValid &= $this->valName();
				$boolValid &= $this->valDescription();
				$boolValid &= $this->valBuHrEmployeeId();
				$boolValid &= $this->valTrainingRepresentativeEmployeeId();
				$boolValid &= $this->valRecruiterEmployeeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolValid = false;
				break;
		}

		return $boolValid;
	}

}
?>