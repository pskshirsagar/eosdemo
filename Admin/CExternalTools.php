<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CExternalTools
 * Do not add any new functions to this class.
 */

class CExternalTools extends CBaseExternalTools {

	public static function fetchAllPaginatedExternalTools( $intPageNo, $intPageSize, $objDatabase,$strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strOrderBy = '';

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY name';
		}

		$strSql = 'SELECT
						*
					FROM
						external_tools
					WHERE
						deleted_by IS NULL
						AND deleted_on IS NULL'
						. $strOrderBy . '
						OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchExternalTools( $strSql, $objDatabase );
	}

	public static function fetchAllExternalTools( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						external_tools
					Where
						is_published = true
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					 ORDER BY name';

		return self::fetchExternalTools( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchExternalTool( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						external_tools
					Where
						name ILIKE E\'%' . implode( '%\' AND name ILIKE E\'%', $arrstrFilteredExplodedSearch ) . '%\'
						AND deleted_by IS NULL AND deleted_on IS NULL
					ORDER BY name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchExternalToolsByIds( $arrintExternalToolIds, $objDatabase ) {

		if( false == valArr( $arrintExternalToolIds ) ) return NULL;

		$strSql = 'SELECT * FROM external_tools WHERE id IN ( ' . implode( ',', $arrintExternalToolIds ) . ' )';

		return self::fetchExternalTools( $strSql, $objDatabase );

	}

}
?>