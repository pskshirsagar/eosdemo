<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDbWatchGroups
 * Do not add any new functions to this class.
 */

class CDbWatchGroups extends CBaseDbWatchGroups {

	public static function fetchDbWatchGroupsByDbWatchId( $intDbWatchId, $objDatabase ) {
		$strSql = 'SELECT * FROM db_watch_groups WHERE db_watch_id = ' . ( int ) $intDbWatchId;

		return self::fetchDbWatchGroups( $strSql, $objDatabase );
	}

	public static function fetchDbWatchGroupsByDbWatchIds( $arrintDbWatchId, $objDatabase ) {
		$strSql = 'SELECT * FROM db_watch_groups WHERE db_watch_id IN ( ' . implode( ',', $arrintDbWatchId ) . ')';

		return self::fetchDbWatchGroups( $strSql, $objDatabase );
	}
}
?>