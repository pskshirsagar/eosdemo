<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserPermissions
 * Do not add any new functions to this class.
 */

class CUserPermissions extends CBaseUserPermissions {

	public static function fetchCustomPermissionedModulesByUserIdByPsModuleId( $intUserId, $intModuleId, $objDatabase ) {

		$strSql = 'SELECT
						up.ps_module_id
					FROM
						user_permissions up
						JOIN ps_modules pm ON ( pm.id = up.ps_module_id )
						JOIN users u ON ( u.id = up.user_id )
					WHERE
						up.user_id = ' . ( int ) $intUserId . '
						AND u.is_administrator <> 1
						AND pm.id = ' . ( int ) $intModuleId . '
						AND up.is_inherited = 0
						AND up.is_allowed = 1
					UNION
					SELECT
						gp.ps_module_id
					FROM
						user_groups ug
						JOIN group_permissions gp ON ( gp.group_id = ug.group_id )
						JOIN ps_modules pm ON ( pm.id = gp.ps_module_id )
						JOIN users u ON ( u.id = ug.user_id )
						LEFT JOIN user_permissions up ON ( up.user_id = ug.user_id AND up.ps_module_id = gp.ps_module_id )
					WHERE
						ug.user_id = ' . ( int ) $intUserId . '
						AND ug.deleted_by IS NULL
						AND u.is_administrator <> 1
						AND ( up.id IS NULL OR up.is_inherited = 1 )
						AND pm.id = ' . ( int ) $intModuleId . '
						AND gp.is_allowed = 1
					GROUP BY
						gp.ps_module_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUserPermissionsByUserIdByPsModuleId( $intUserId, $intPsModuleId, $objDatabase ) {

		if( false == valId( $intUserId ) || false == valId( $intPsModuleId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						user_permissions
					WHERE
						user_id = ' . ( int ) $intUserId . '
					AND
						ps_module_id = ' . ( int ) $intPsModuleId;

		return self::fetchUserPermission( $strSql, $objDatabase );
	}

	public static function fetchUserPermissionsByIds( $arrintUserIds, $intPsModuleId, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) || false == valId( $intPsModuleId ) ) return NULL;
		$strSql = 'SELECT
						*
					FROM
						user_permissions
					WHERE
						user_id IN ( ' . implode( ',', $arrintUserIds ) . ' )
						AND ps_module_id = ' . ( int ) $intPsModuleId;

		return self::fetchUserPermissions( $strSql, $objDatabase );
	}

	public static function fetchUserPermissionsByUserIdByPsModuleIds( $arrintUserModuleIds, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						user_permissions
					WHERE
						( user_id, ps_module_id ) IN ( ' . sqlIntMultiImplode( $arrintUserModuleIds ) . ' )';

		return self::fetchUserPermissions( $strSql, $objDatabase );
	}

	public static function fetchPsModuleIdsByUserId( $intUserId, $objDatabase ) {

		$strSql = 'SELECT
						up.ps_module_id
					FROM
						user_permissions up JOIN ps_modules pm ON( up.ps_module_id = pm.id AND up.is_allowed = 1 AND
						pm.is_published = 1 AND pm.is_public <> 1 )
					WHERE user_id = ' . ( int ) $intUserId . '
						ORDER BY up.ps_module_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUserPermissionsByPsModuleId( $intPsModuleId, $objDatabase ) {
		$strSql = 'SELECT
						up.user_id
					FROM
						user_permissions up JOIN ps_modules pm ON ( up.ps_module_id = pm.id AND up.is_allowed = 1 AND pm.is_published = 1 AND pm.is_public <> 1 )
					WHERE up.ps_module_id = ' . ( int ) $intPsModuleId . '
						ORDER BY up.ps_module_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>