<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeHourTypes
 * Do not add any new functions to this class.
 */

class CEmployeeHourTypes extends CBaseEmployeeHourTypes {

	public static function fetchEmployeeHourTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CEmployeeHourType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEmployeeHourType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CEmployeeHourType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllEmployeeHourTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						employee_hour_types
					ORDER BY
						order_num';
		return self::fetchEmployeeHourTypes( $strSql, $objDatabase );
	}
}
?>