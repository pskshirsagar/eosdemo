<?php

use Psi\Eos\Admin\CCommissionPayments;
use Psi\Eos\Admin\CCommissionRecipients;
use Psi\Eos\Admin\CCommissions;

class CCommission extends CBaseCommission {

	protected $m_intIsReady;

	/**
	 * Validate Functions
	 */

	public function valCommissionRecipientId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionRecipientId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_recipient_id', 'Commission recipient is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_type_id', 'Commission type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_datetime', 'Date is required.' ) );
		} elseif( false == CValidation::validateDate( $this->getCommissionDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_datetime', 'Valid date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionAmount() ) || 0 == $this->getCommissionAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_amount', 'Commission amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function valChargeCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getChargeCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_id', 'Charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionMemo() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionMemo() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_memo', 'Commission memo is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsManualAdjustment() {
		$boolIsValid = true;

		if( true == is_null( $this->getIsManualAdjustment() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_manual_adjustment', 'Is manual adjustment is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'post_commission':
			case 'update_commission':
				$boolIsValid  &= $this->valCommissionDatetime();
				$boolIsValid  &= $this->valCommissionAmount();
				$boolIsValid  &= $this->valChargeCodeId();
				$boolIsValid  &= $this->valCid();
				$boolIsValid  &= $this->valCommissionMemo();
			   	$boolIsValid  &= $this->valIsManualAdjustment();
				break;

			case VALIDATE_INSERT:
				$boolIsValid  &= $this->valCommissionRecipientId();
				$boolIsValid  &= $this->valCommissionTypeId();
				$boolIsValid  &= $this->valCommissionDatetime();
				$boolIsValid  &= $this->valCommissionAmount();
				$boolIsValid  &= $this->valCommissionMemo();
				$boolIsValid  &= $this->valIsManualAdjustment();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid  &= $this->valCommissionDatetime();
				$boolIsValid  &= $this->valCommissionAmount();
				$boolIsValid  &= $this->valCommissionMemo();
				$boolIsValid  &= $this->valIsManualAdjustment();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function setIsReady( $intIsReady ) {
		$this->m_intIsReady = CStrings::strToIntDef( $intIsReady, NULL, false );
	}

	public function getIsReady() {
		return $this->m_intIsReady;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = false, $boolDirectSet );

		if( true == isset( $arrmixValues['is_ready'] ) ) {
			$this->setIsReady( $arrmixValues['is_ready'] );
		}
		return;
	}

	public function sqlCommissionAmount( $boolIsPayment = false ) {

		if( true == isset( $this->m_fltCommissionAmount ) ) {
			if( true == $boolIsPayment && 0 < $this->m_fltCommissionAmount ) {
				// Transaction amount for payments are always -neg.
				return ( string ) ( $this->m_fltCommissionAmount * -1 );
			} else {
				return ( string ) $this->m_fltCommissionAmount;
			}
		} else {
			return 'NULL';
		}
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCommissionPayment( $objDatabase ) {
		return CCommissionPayments::createService()->fetchCommissionPaymentById( $this->getCommissionPaymentId(), $objDatabase );
	}

	public function fetchCommisisonFrontLoad( $objDatabase ) {
		return \Psi\Eos\Admin\CCommissionFrontLoads::createService()->fetchCommissionFrontLoadById( $this->getCommissionFrontLoadId(), $objDatabase );
	}

	public function fetchCommissionRecipient( $objDatabase ) {
		return CCommissionRecipients::createService()->fetchCommissionRecipientById( $this->getCommissionRecipientId(), $objDatabase );
	}

	public function fetchUnallocatedDebitCommissionsByLimit( $intLimit, $objDatabase ) {
		return CCommissions::createService()->fetchUnallocatedDebitCommissionsByLimitByCommissionId( $intLimit, $this->getId(), $objDatabase );
	}

	/**
	 * Create Functions
	 */

	 public function createCommissionPayment( $objDatabase = NULL ) {

		$objCommissionPayment = new CCommissionPayment();
		$objCommissionPayment->setCommissionRecipientId( $this->getCommissionRecipientId() );
		$objCommissionPayment->setCommissionBatchId( $this->getCommissionBatchId() );
		$objCommissionPayment->setPaymentDatetime( date( 'm-d-Y H:i:s' ) );
		$objCommissionPayment->setPaymentAmount( __( '{%f, 0, p:2}', [ $this->getCommissionAmount() ] ) );

		if( false == is_null( $objDatabase ) ) {
			$objCommissionRecipient = $objCommissionPayment->fetchCommissionRecipient( $objDatabase );
			if( true == valObj( $objCommissionRecipient, 'CCommissionRecipient' ) ) {
				$objCommissionPayment->setCheckPayableTo( $objCommissionRecipient->getCheckPayableTo() );
				$objCommissionPayment->setCheckBankName( $objCommissionRecipient->getCheckBankName() );
				$objCommissionPayment->setCheckAccountTypeId( $objCommissionRecipient->getCheckAccountTypeId() );
				$objCommissionPayment->setCheckRoutingNumber( $objCommissionRecipient->getCheckRoutingNumber() );
				$objCommissionPayment->setCheckAccountNumberEncrypted( $objCommissionRecipient->getCheckAccountNumberEncrypted() );
			}
		}

		return $objCommissionPayment;
	 }

	public function createOffsettingCommission() {

		$objCommission = clone $this;
		$objCommission->setId( NULL );
		$objCommission->setReversedCommissionId( $this->getId() );
		$objCommission->setCommissionBatchId( NULL );
		// $objCommission->setCommissionRateId( $this->getId() );
		$objCommission->setCommissionDatetime( date( 'm/d/Y' ) );
		$objCommission->setCommissionAmount( $this->getCommissionAmount() * ( -1 ) );
		$objCommission->setOriginatingTransactionNumber( $this->getTransactionNumber() );
		$objCommission->setTransactionNumber( NULL );
		$objCommission->setCommissionMemo( 'Reverting the commission amount of commission id[' . $this->getId() . ']' );

		return $objCommission;
	}

	/**
	 * Database Functions
	 */

	public function setNextId( $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		if( false == isset( $this->m_intId ) ) {
			$strSql = "SELECT nextval('commissions_id_seq'::text) AS id";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post commission record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrobjValues = $objDataset->fetchArray();

			$this->setId( $arrobjValues['id'] );

			$objDataset->cleanup();
		}
	}

	public function setNextTransactionNumber( $objDatabase ) {

		$objDataset = $objDatabase->createDataset();

		if( false == isset( $this->m_intTransactionNumber ) ) {
			$strSql = "SELECT nextval('commissions_transaction_number_seq'::text) AS transaction_number";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				// Unset new id on error
				$this->setId( NULL );

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post commission payment transaction record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrobjValues = $objDataset->fetchArray();
			$this->setTransactionNumber( $arrobjValues['transaction_number'] );

			$objDataset->cleanup();
		}

		return true;
	}

	public function postCommission( $intCurrentUserId, $objDatabase ) {
		$this->setNextTransactionNumber( $objDatabase );
		return parent::insert( $intCurrentUserId, $objDatabase );
	}

	public function postCommissionPayment( $intCurrentUserId, $objDatabase ) {

	 	$this->setNextTransactionNumber( $objDatabase );

		$objDataset = $objDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_null( $intId ) ) {
			$strSql = "SELECT nextval( 'commissions_id_seq'::text ) AS id";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert commission record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrobjValues = $objDataset->fetchArray();
			$this->setId( $arrobjValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .
				   'FROM commissions_insert( ' .
					$this->sqlId() . ', ' .
					$this->sqlCid() . ', ' .
					$this->sqlAccountId() . ', ' .
					$this->sqlReversedCommissionId() . ', ' .
					$this->sqlTransactionId() . ', ' .
					$this->sqlArPaymentId() . ', ' .
					$this->sqlCommissionRecipientId() . ', ' .
					$this->sqlCommissionStructureId() . ', ' .
					$this->sqlCommissionRateId() . ', ' .
					$this->sqlCommissionFrontLoadId() . ', ' .
					$this->sqlCommissionPaymentId() . ', ' .
					$this->sqlCommissionBatchId() . ', ' .
					$this->sqlChargeCodeId() . ', ' .
					$this->sqlCommissionTypeId() . ', ' .
					$this->sqlCommissionFrontLoadTypeId() . ', ' .
					$this->sqlOriginatingTransactionNumber() . ', ' .
					$this->sqlTransactionNumber() . ', ' .
					$this->sqlCommissionDatetime() . ', ' .
					$this->sqlCommissionAmount( true ) . ', ' .
					$this->sqlUnallocatedAmount() . ', ' .
					$this->sqlCommissionMemo() . ', ' .
					$this->sqlIsManualAdjustment() . ', ' .
					$this->sqlExportedBy() . ', ' .
					$this->sqlExportedOn() . ', ' .
					$this->sqlApprovedBy() . ', ' .
					$this->sqlApprovedOn() . ', ' .
					$this->sqlDeletedBy() . ', ' .
					$this->sqlDeletedOn() . ', ' .
					( int ) $intCurrentUserId . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			// Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert commission record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert commission record. The following error was reported.' ) );
			// Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrobjValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrobjValues['type'], $arrobjValues['field'], $arrobjValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function updateCommission( $intCurrentUserId, $objDatabase ) {
		return parent::update( $intCurrentUserId, $objDatabase );
	}

   	public function updateCommissionPayment( $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
					'FROM commissions_update( ' .
					 $this->sqlId() . ', ' .
					 $this->sqlCid() . ', ' .
					 $this->sqlAccountId() . ', ' .
					 $this->sqlReversedCommissionId() . ', ' .
					 $this->sqlTransactionId() . ', ' .
					 $this->sqlArPaymentId() . ', ' .
					 $this->sqlCommissionRecipientId() . ', ' .
					 $this->sqlCommissionStructureId() . ', ' .
					 $this->sqlCommissionRateId() . ',  ' .
					 $this->sqlCommissionFrontLoadId() . ', ' .
					 $this->sqlCommissionPaymentId() . ', ' .
					 $this->sqlCommissionBatchId() . ', ' .
					 $this->sqlChargeCodeId() . ', ' .
					 $this->sqlCommissionTypeId() . ', ' .
					 $this->sqlCommissionFrontLoadTypeId() . ', ' .
					 $this->sqlOriginatingTransactionNumber() . ', ' .
					 $this->sqlTransactionNumber() . ', ' .
					 $this->sqlCommissionDatetime() . ', ' .
					 $this->sqlCommissionAmount( true ) . ', ' .
					 $this->sqlUnallocatedAmount() . ', ' .
					 $this->sqlCommissionMemo() . ', ' .
					 $this->sqlIsManualAdjustment() . ', ' .
					 $this->sqlExportedBy() . ', ' .
					 $this->sqlExportedOn() . ', ' .
					 $this->sqlApprovedBy() . ', ' .
					 $this->sqlApprovedOn() . ', ' .
					 $this->sqlDeletedBy() . ', ' .
					 $this->sqlDeletedOn() . ', ' .
					 ( int ) $intCurrentUserId . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update commission record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update commission record. The following error was reported.' ) );
			while( false == $objDataset->eof() ) {
				$arrobjValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrobjValues['type'], $arrobjValues['field'], $arrobjValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function revertCommission( $intUserId, $objDatabase ) {

		if( false == is_numeric( $this->getCommissionBatchId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'commission_batch_id', 'You can not revert a commission that is unbatched.  You should directly delete.' ) );
			return false;
		}

		// Build an offsetting commission
		$objOffsettingCommission = $this->createOffsettingCommission();

		if( false == $objOffsettingCommission->postCommission( $intUserId, $objDatabase ) ) {
			return false;
		}

		if( true == is_numeric( $this->getCommissionFrontLoadId() ) ) {
			$objCommissionFrontLoad = $this->fetchCommisisonFrontLoad( $objDatabase );

			if( true == valObj( $objCommissionFrontLoad, 'CCommissionFrontLoad' ) ) {
				if( $objCommissionFrontLoad->getFrontLoadPostCount() > 0 && $objCommissionFrontLoad->getOngoingFrontLoadPostCount() == 0 ) {
					$objCommissionFrontLoad->setFrontLoadPostCount( $objCommissionFrontLoad->getFrontLoadPostCount() - 1 );
				} elseif( $objCommissionFrontLoad->getFrontLoadPostCount() > 0 && $objCommissionFrontLoad->getOngoingFrontLoadPostCount() > 0 ) {
					$objCommissionFrontLoad->setOngoingFrontLoadPostCount( $objCommissionFrontLoad->getOngoingFrontLoadPostCount() - 1 );
				} elseif( $objCommissionFrontLoad->getFrontLoadPostCount() == 0 && $objCommissionFrontLoad->getOngoingFrontLoadPostCount() > 0 ) {
					$objCommissionFrontLoad->setOngoingFrontLoadPostCount( $objCommissionFrontLoad->getOngoingFrontLoadPostCount() - 1 );
				}

				if( false == $objCommissionFrontLoad->update( $intUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objCommissionFrontLoad->getErrorMsgs() );
					return false;
				}
			}
		}

		return true;
	}

	public function delete( $intUserId, $objDatabase, $boolIsHardDelete = false ) {

		if( true == is_numeric( $this->getCommissionBatchId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'commission_batch_id', 'You can not deleted a batched commission.' ) );
			return false;
		}

		if( true == is_numeric( $this->getCommissionFrontLoadId() ) ) {
			$objCommissionFrontLoad = $this->fetchCommisisonFrontLoad( $objDatabase );

			if( true == valObj( $objCommissionFrontLoad, 'CCommissionFrontLoad' ) ) {
				if( $objCommissionFrontLoad->getFrontLoadPostCount() > 0 && $objCommissionFrontLoad->getOngoingFrontLoadPostCount() == 0 ) {
					$objCommissionFrontLoad->setFrontLoadPostCount( $objCommissionFrontLoad->getFrontLoadPostCount() - 1 );
				} elseif( $objCommissionFrontLoad->getFrontLoadPostCount() > 0 && $objCommissionFrontLoad->getOngoingFrontLoadPostCount() > 0 ) {
					$objCommissionFrontLoad->setOngoingFrontLoadPostCount( $objCommissionFrontLoad->getOngoingFrontLoadPostCount() - 1 );
				} elseif( $objCommissionFrontLoad->getFrontLoadPostCount() == 0 && $objCommissionFrontLoad->getOngoingFrontLoadPostCount() > 0 ) {
					$objCommissionFrontLoad->setOngoingFrontLoadPostCount( $objCommissionFrontLoad->getOngoingFrontLoadPostCount() - 1 );
				}

				if( false == $objCommissionFrontLoad->update( $intUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objCommissionFrontLoad->getErrorMsgs() );
					return false;
				}
			}
		}

		if( true == $boolIsHardDelete ) {
			return parent::delete( $intUserId, $objDatabase );
		} else {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );

			if( $this->update( $intUserId, $objDatabase ) ) {
				return true;
			}
		}
	}

}
?>