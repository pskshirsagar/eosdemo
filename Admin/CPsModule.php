<?php

class CPsModule extends CBasePsModule {

	const USER_ADMINISTRATION_SYSTEM 	= 1;
	const CLIENTS_SYSTEM 				= 8;
	const TASKS_SYSTEM 					= 12;
	const SALES_SYSTEM 					= 27;
	const PRODUCTS_SYSTEM 				= 55;
	const MARKETING_SYSTEM 				= 62;
	const SUPPORT_SYSTEM 				= 79;
	const MERCHANT_SYSTEM 				= 145;
	const DEVELOPMENT_SYSTEM 			= 153;
	const IT_SYSTEM 					= 176;
	const ACCOUNTING_SYSTEM 			= 190;
	const ADMIN_SYSTEM 					= 219;
	const UTILITIES_SYSTEM 				= 245;
	const HR_SYSTEM 					= 269;
	const REPORTS_SYSTEM 				= 540;
	const TRAININGS_SYSTEM 				= 738;
	const SERVICE_SYSTEM				= 1326;
	const AUTHENTICATION_SYSTEM			= 7;
	const REPORT_MODULES				= 2345;
	const EXECUTIVE_REPORTS				= 2390;

	const DASHBOARD		 				= 777;
	const CLIENTS 						= 9;
	const TASKS 						= 16;
	const SALES 						= 782;
	const PRODUCTS 						= 58;
	const MARKETING 					= 63;
	const SUPPORT 						= 783;
	const MERCHANT 						= 784;
	const DEVELOPMENT					= 154;
	const IT							= 688;
	const ACCOUNTING 					= 785;
	const ADMIN 						= 236;
	const UTILITIES 					= 256;
	const HR 							= 1132;
	const TRAININGS 					= 787;
	const AUTHENTICATION				= 794;
	const SERVICES						= 244;
	const LEASING_CENTER				= 2841;

	const PS_ALLOW_EXTERNAL_ACCESS		= 795;

	const RUN_SQL_DEPLOYMENT 			= 1152;
	const MARK_FILE_AS_RESOLVED			= 1358;
	const SCREENING_SYSTEM				= 1811;
	const SCREENING_APPLICANTS 			= 1812;
	const ROLE_ASSIGNED_USERS			= 2017;
	const HAS_ADMIN_ENTRATA_ACCESS		= 2151;

	const SERVICE_GROUP_TYPES 			= 2621;

	const BULK_COMPANY_PAYMENTS_NEW		= 2545;

	const ACCOUNT_REPORT_INDIA 			= 217;

    const SCREENING_REPORTS				= 2557;
    const SCREENING_CONFIG_REPORT		= 2918;

	const HELP_LEARNING_CENTER_NEW		= 2959;
	const HELP_LEARNING_CENTER_COURSE_NEW	= 2962;

    const INTERNET_LISTING_SERVICE		= 1224;
	const FACEBOOK_MARKETPLACE_SUBSCRIPTIONS = 3845;
	const GOOGLE_MY_BUSINESS_SUBSCRIPTIONS   = 3846;
	const YEXT_SUBSCRIPTIONS                 = 3847;

	const DEVOPS_HELPDESK_REPORT = 3139;

	const INDIAN_REIMBURSEMENT_REQUESTS_NEW	= 2528;
	const INDIAN_REIMBURSEMENT_REPORT_NEW	= 2539;
	const INDIAN_REIMBURSEMENT_EXPENSES_NEW	= 2529;

	protected $m_strParentModuleName;

	public static $c_arrintIndianReimbursementModuleIds = [
		self::INDIAN_REIMBURSEMENT_REQUESTS_NEW,
		self::INDIAN_REIMBURSEMENT_REPORT_NEW,
		self::INDIAN_REIMBURSEMENT_EXPENSES_NEW
	];

	public function getParentModuleName() {
		return $this->m_strParentModuleName;
	}

	public function setParentModuleName( $strParentModuleName ) {
		$this->m_strParentModuleName = $strParentModuleName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['parent_module_name'] ) ) {
			$this->setParentModuleName( $arrmixValues['parent_module_name'] );
		}
	}

	public function valModuleName() {
		$boolIsValid = true;

		if( true == is_null( $this->getModuleName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'module_name', 'Module name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valUrl() {
		$boolIsValid = true;

		if( true == is_null( $this->getUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'URL is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_for_ps_module':
				$boolIsValid &= $this->valModuleName();
				$boolIsValid &= $this->valUrl();
				$boolIsValid &= $this->valTitle();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>