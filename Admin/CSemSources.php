<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemSources
 * Do not add any new functions to this class.
 */

class CSemSources extends CBaseSemSources {

	public static function fetchSemSources( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSemSource', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchSemSource( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSemSource', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

}
?>