<?php

use Psi\Eos\Admin\CChartOfAccounts;

class CChartOfAccount extends CBaseChartOfAccount {

	const ACCOUNTS_RECEIVABLE			= 1;
	const DEFERRED_REVENUE				= 2;
	const OPERATING_ZIONS				= 3;
	const PSIA_OPERATING_MARKEL			= 4;
	const PSIA_OPERATING_KEMPER			= 152;
	const PSIA_OPERATING_QBE			= 238;
	const PSIA_OPERATING_IDTHEFT		= 259;
	const UNDEPOSITED_FUNDS				= 5;
	const REVENUE_ZIONS					= 52;
	const MASTER_POLICY_CASH			= 282;
	const ACH_PAYMENTS_ZIONS			= 166;

	const PREPAID_MARKEL_RI_PREMIUM		= 91;
	const PREPAID_MARKEL_RI_TAXES		= 92;
	const MARKEL_RI_PREMIUM_LIABILITY	= 93;
	const MARKEL_RI_INCOME				= 95;
	const PREPAID_ID_THEFT				= 255;
	const MASTER_POLICY					= 279;

	const ACH_REBATE_FEE				= 164;
	const CC_REBATE_FEE					= 165;

	const SALES_TAX						= 174;
	const SALES_TAX_RI					= 235;

	const RIVERSTONE_ZIONS_BANK			= 34;
	const BANK_OF_MONTREAL_REVENUE		= 337;

	/**
	* Validation Functions
	*
	*/

	public function valChartOfAccountTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getChartOfAccountTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'chart_of_account_type_id', 'Account Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAccountName( $objDatabase = NULL ) {
		$boolIsValid = true;
		if( true == is_null( $this->getAccountName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_name', 'Account Name is required.' ) );
		}

		if( true == isset ( $objDatabase ) ) {
			$intAccountCount = CChartOfAccounts::createService()->fetchChartOfAccountCountByAccountName( $this->getAccountName(), $objDatabase );

			if( 0 < $intAccountCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_name', 'An account with this name already exists.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valAccountNumber() {
		$boolIsValid = true;
		if( true == is_null( $this->getAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', 'Account Number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSecondaryNumber( $objDatabase = NULL ) {
		$boolIsValid = true;
		if( true == is_null( $this->getSecondaryNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_number', 'GL Code is required.' ) );
		}

		if( true == isset ( $objDatabase ) ) {
			$intSecondaryNumberCount = CChartOfAccounts::createService()->fetchChartOfAccountCountBySecondaryNumber( $this->getSecondaryNumber(), $objDatabase );
			if( 0 < $intSecondaryNumberCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_number', 'GL Code already exists.' ) );
			}
		}

		if( !preg_match( '/^[a-zA-Z0-9._-]*$/', $this->getSecondaryNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_number', 'Invalid GL Code.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valAccountName( $objDatabase );
				$boolIsValid &= $this->valSecondaryNumber( $objDatabase );
				$boolIsValid &= $this->valChartOfAccountTypeId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setNextOrderNum( $objDatabase ) {

		if( true == is_null( $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to connect Database' ) );
			return false;
		}

		$strSql = "SELECT nextval('chart_of_accounts_id_seq'::text) AS id";
		$arrintValues = fetchData( $strSql, $objDatabase );

		if( ( false == valArr( $arrintValues ) ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to get next value' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );
			return false;
		}

		$this->setOrderNum( $arrintValues[0]['id'] );
		$this->setId( $arrintValues[0]['id'] );
		return true;
	}

}
?>