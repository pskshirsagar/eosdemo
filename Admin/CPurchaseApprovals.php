<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseApprovals
 * Do not add any new functions to this class.
 */

class CPurchaseApprovals extends CBasePurchaseApprovals {

	public static function fetchPurchaseAppovalByPurchaseRequestIdsByEmployeeId( $arrintPurchaseRequestIds, $intEmployeeId, $objDatabase ) {
		if( false == valArr( $arrintPurchaseRequestIds ) ) return NULL;

		$strSql = 'SELECT
						sq.*
					FROM
						(
							SELECT
								*,
								rank( ) OVER ( PARTITION BY purchase_request_id, employee_id ORDER BY id DESC ) AS rank
							FROM
								purchase_approvals
							WHERE
								purchase_request_id IN ( ' . implode( ',', $arrintPurchaseRequestIds ) . ' )
								AND employee_id = ' . ( int ) $intEmployeeId . '
						) sq
					WHERE
						sq.rank = 1';

		return parent::fetchPurchaseApprovals( $strSql, $objDatabase );
	}

	public static function fetchPurchaseApprovalDetailsByPurchaseRequestId( $intPurchaseRequestId, $objDatabase ) {
		$strSql = 'SELECT
						pa.*,
						e.preferred_name as employee_name,
						CASE
							WHEN pa.approved_on IS NOT NULL THEN \'Approved\'
							ELSE
								CASE
									WHEN pa.denied_on IS NOT NULL THEN \'Denied\'
									ELSE \'-\'
							END
						END as approval_status
					FROM
						purchase_approvals pa
						JOIN employees e ON (e.id = pa.employee_id)
					WHERE
						purchase_request_id = ' . ( int ) $intPurchaseRequestId . '
					ORDER BY
						pa.order_num,
						pa.created_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPurchaseApprovalDetailsByPurchaseRequestIds( $arrintPurchaseRequestIds, $objDatabase ) {
		if( false == valArr( $arrintPurchaseRequestIds ) ) return NULL;

		$strSql = 'SELECT
						pa.*
					FROM
						purchase_approvals pa
					WHERE
						purchase_request_id IN ( ' . implode( ',', $arrintPurchaseRequestIds ) . ' )
					ORDER BY
						pa.order_num,
						pa.created_on';

		$arrstrPurchaseRequestsApprovalsTemp = fetchData( $strSql, $objDatabase );

		$arrstrPurchaseRequestsApprovals = array();
		if( true == valArr( $arrstrPurchaseRequestsApprovalsTemp ) ) {
			foreach( $arrstrPurchaseRequestsApprovalsTemp as $arrstrPurchaseRequestsApproval ) {
				$arrstrPurchaseRequestsApprovals[$arrstrPurchaseRequestsApproval['purchase_request_id']][$arrstrPurchaseRequestsApproval['employee_id']] = $arrstrPurchaseRequestsApproval;
			}
		}

		return $arrstrPurchaseRequestsApprovals;
	}

	public static function fetchSimplePurchaseApprovalsCountWithActionPerformedByPurchaseRequestIds( $arrintPurchaseRequestIds, $objDatabase, $intEmployeeId = NULL ) {
		if( false == valArr( $arrintPurchaseRequestIds ) ) return NULL;

		$strSql = 'SELECT
						pa.purchase_request_id as id, count( pa.id ) as action_count
					FROM
						purchase_approvals pa
						JOIN purchase_requests pr ON( pa.purchase_request_id = pr.id AND pr.approved_on IS NULL AND pr.denied_on IS NULL )
					WHERE
						pa.purchase_request_id IN ( ' . implode( ',', $arrintPurchaseRequestIds ) . ' )
						AND ( pa.approved_on IS NOT NULL OR pa.denied_on IS NOT NULL )
						AND CASE
							WHEN pr.purchase_request_type_id IN( ' . CPurchaseRequestType::NEW_HIRE . ',' . CPurchaseRequestType::EMPLOYEE_BONUS . ',' . CPurchaseRequestType::COMP_CHANGE . ' ) AND pr.reopened_on IS NOT NULL
							THEN pr.base_amount_dollars_encrypted IS NOT NULL AND pa.created_on >= pr.reopened_on ELSE 1=1
						END';

		if( false == is_null( $intEmployeeId ) ) $strSql .= ' AND pa.employee_id = ' . ( int ) $intEmployeeId;

		$strSql .= ' GROUP BY pa.purchase_request_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPurchaseApprovalsForEmailByPurchaseRequestId( $intPurchaseRequestId, $objDatabase ) {
		if( true == empty( $intPurchaseRequestId ) ) return NULL;

		$strSql = 'SELECT
						pa.*
					FROM
						purchase_approvals pa
						JOIN purchase_requests pr ON( pr.id = pa.purchase_request_id )
					WHERE
						pa.purchase_request_id = ' . ( int ) $intPurchaseRequestId . '
						AND CASE
							WHEN pr.purchase_request_type_id = ' . CPurchaseRequestType::NEW_HIRE . ' AND pr.reopened_on IS NOT NULL
							THEN pr.base_amount_dollars_encrypted IS NOT NULL AND pa.created_on >= pr.reopened_on ELSE 1=1
						END';

		return parent::fetchPurchaseApprovals( $strSql, $objDatabase );
	}

	public static function fetchPurchaseApprovalsByPurchaseRequestIdByEmployeeIds( $intPurchaseRequestId, $arrintApproverIds, $objDatabase ) {
		if( false == valId( $intPurchaseRequestId ) || false == valArr( array_filter( $arrintApproverIds ) ) ) return NULL;

		$strSql = 'SELECT
						pa.*
					FROM
						purchase_approvals pa
					WHERE
						pa.purchase_request_id = ' . ( int ) $intPurchaseRequestId . '
						AND pa.employee_id IN( ' . implode( ',', $arrintApproverIds ) . ' ) ';

		return parent::fetchPurchaseApprovals( $strSql, $objDatabase );
	}

	public static function fetchPurchaseApprovalDataByPurchaseRequestId( $intPurchaseRequestId, $objDatabase ) {
		if( true == is_null( $intPurchaseRequestId ) ) return NULL;

		$strSql = 'SELECT
						pa.*,
						e.preferred_name
					FROM
						purchase_approvals pa
						 LEFT JOIN employees e ON( pa.employee_id = e.id  )
					WHERE
						pa.purchase_request_id = ' . ( int ) $intPurchaseRequestId . '
						AND pa.denied_on IS NOT NULL ';

		return fetchData( $strSql, $objDatabase );
	}

}
?>