<?php

class CPsAssetModel extends CBasePsAssetModel {

	public function valPsAssetTypeId( $objDatabase, $boolXentoEquipment ) {
		$boolIsValid = true;
		$strTypeName = ( true == $boolXentoEquipment ) ? 'Equipment' : 'Asset';

		if( true == is_null( $this->getPsAssetTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_type_id', $strTypeName . ' type is required.' ) );
			return false;
		}

		$objDisableAssetType = \Psi\Eos\Admin\CPsAssetTypes::createService()->fetchPsAssetTypeById( $this->getPsAssetTypeId(), $objDatabase );

		if( false == is_null( $objDisableAssetType->getDeletedBy() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_type_id', $strTypeName . ' type is disabled.' ) );
			return false;
		}
		return $boolIsValid;
	}

	public function valPsAssetBrandId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsAssetBrandId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_brand_id', 'Brand is required.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase, $boolXentoEquipment ) {

		$boolIsValid = true;
		$strTypeName = ( true == $boolXentoEquipment ) ? 'equipment' : 'asset';

		if( true == ( is_null( $this->getName() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Model name is required.' ) );
			return false;
		}

		if( ( false == preg_match( '/^[a-zA-Z0-9- _$&+,:;=?@#|\'<>.-^*()%!]{2,50}$/', $this->getName() ) ) || ( true == is_numeric( $this->getName() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'A valid model name is required.' ) );
			return false;
		}

		if( false == is_null( $this->getPsAssetBrandId() ) ) {
			$intConflictingPsAssetModelCount = \Psi\Eos\Admin\CPsAssetModels::createService()->fetchConflictingPsAssetModelCountByName( $this->getName(), $this->getPsAssetBrandId(), $this->getId(), $objDatabase, $boolIsShowDeletedModel = true );

			if( 0 < $intConflictingPsAssetModelCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Model name with this ' . $strTypeName . ' type is already in use, but it is disabled.' ) );
				return false;
			}

			$intConflictingPsAssetModelCount = \Psi\Eos\Admin\CPsAssetModels::createService()->fetchConflictingPsAssetModelCountByName( $this->getName(), $this->getPsAssetBrandId(), $this->getId(), $objDatabase, $boolIsShowDeletedModel = false );

			if( 0 < $intConflictingPsAssetModelCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Model name exist with selected Brand type.' ) );
				return false;
			}

		}

		if( false == is_null( $this->getId() ) ) {
			$intCount = \Psi\Eos\Admin\CPsAssets::createService()->fetchPsAssetCount( 'WHERE deleted_by IS NULL AND deleted_on IS NULL AND ps_asset_model_id = ' . $this->getId(), $objDatabase );

			if( 0 != $intCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Failed to update the model, it is being associated with ' . $strTypeName ) );
				return false;
			}
		}

		return $boolIsValid;

	}

	public function valDependantInformation( $objDatabase, $boolXentoEquipment ) {

		$boolIsValid = true;
		$strTypeName = ( true == $boolXentoEquipment ) ? 'equipment' : 'asset';

		$intCount = \Psi\Eos\Admin\CPsAssets::createService()->fetchPsAssetCount( 'WHERE deleted_by IS NULL AND deleted_on IS NULL AND ps_asset_model_id = ' . $this->getId(), $objDatabase );

		if( 0 < $intCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Failed to delete the model, it is being associated with ' . $strTypeName ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase= NULL, $boolXentoEquipment = false ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPsAssetTypeId( $objDatabase, $boolXentoEquipment );
				$boolIsValid &= $this->valPsAssetBrandId( $objDatabase );
				$boolIsValid &= $this->valName( $objDatabase, $boolXentoEquipment );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase, $boolXentoEquipment );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>