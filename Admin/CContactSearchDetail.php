<?php

class CContactSearchDetail extends CBaseContactSearchDetail {

	public function valId() {
		return true;
	}

	public function valContactSearchTypeId() {
		return true;
	}

	public function valReferenceId() {
		return true;
	}

	public function valSearchString() {
		return true;
	}

	public function valName() {
		return true;
	}

	public function valLocation() {
		return true;
	}

	public function valTwitterUrl() {
		return true;
	}

	public function valLinkedinUrl() {
		return true;
	}

	public function valFacebookUrl() {
		return true;
	}

	public function valWebsiteUrl() {
		return true;
	}

	public function valBiography() {
		return true;
	}

	public function valProfilePicUrl() {
		return true;
	}

	public function valPersonTitle() {
		return true;
	}

	public function valPersonOrganization() {
		return true;
	}

	public function valCompanyFoundedOn() {
		return true;
	}

	public function valCompanyTotalEmployees() {
		return true;
	}

	public function valAdvancedDetails() {
		return true;
	}

	public function valFailedOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>