<?php

class CReleaseNoteType extends CBaseReleaseNoteType {

	const NOTE_NEW		= 1;
	const NOTE_UPDATED	= 2;
	const NOTE_FIXED	= 3;

	public static $c_arrintReleaseNoteTypes = [
		self::NOTE_NEW => 'New',
		self::NOTE_UPDATED => 'Updated',
		self::NOTE_FIXED => 'Fixed'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>