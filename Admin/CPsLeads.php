<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsLeads
 * Do not add any new functions to this class.
 */

class CPsLeads extends CBasePsLeads {

	/**
	 * This function resets the ps_leads.profile_percent value so we can see what % of the data we require on a lead is populated.
	 */

	public static function updateProfilePercents( $intPsLeadId = NULL, $objDatabase ) {

		$strCondition = ( ( true == valArr( $intPsLeadId ) ) ? ' IN ( ' . implode( ',', $intPsLeadId ) . ' )' : ( ( true == is_numeric( $intPsLeadId ) ) ? ' = ' . ( int ) $intPsLeadId : ' <> 0 ' ) );

		$strSql = 'UPDATE
						ps_leads pl
					SET
						profile_percent = ( ( unit_count_points + company_name_count + primary_person_points + ps_lead_type_points + primary_website_url_points + non_primary_person_points + competitor_points + property_type_points + entrata_competitor_points + opportunity_points )::numeric ( 15, 2 ) / 10 ) ::numeric ( 15, 2 )
					FROM
						(
							SELECT
								pl.id AS ps_lead_id,
								CASE
									WHEN ( COALESCE ( number_of_units, 0 ) > 0 AND COALESCE ( property_count, 0 ) > 0 ) THEN 1
									ELSE 0
								END unit_count_points,
								CASE
									WHEN company_name IS NOT NULL THEN 1
									ELSE 0
								END company_name_count,
								CASE
									WHEN p.ps_lead_id IS NOT NULL THEN 1
									ELSE 0
								END AS primary_person_points,
								CASE
									WHEN ps_lead_type_id IS NOT NULL THEN 1
									ELSE 0
									END ps_lead_type_points,
								CASE
									WHEN primary_website_urls IS NOT NULL THEN 1
									ELSE 0
								END AS primary_website_url_points, COALESCE ( npp.non_primary_person_points, 0 ) AS non_primary_person_points, COALESCE ( comp.competitor_points, 0 ) AS competitor_points, COALESCE ( plpt.property_type_points, 0 ) AS property_type_points, COALESCE ( entrata_plc.entrata_competitor_points, 0 ) AS entrata_competitor_points, COALESCE ( c.opportunity_points, 0 ) AS opportunity_points
							FROM
								ps_leads pl
								JOIN ps_lead_details pld ON ( pl.id = pld.ps_lead_id )
								LEFT JOIN
								(
									SELECT
										p.ps_lead_id,
										rank ( ) OVER ( PARTITION BY p.ps_lead_id
									ORDER BY
										p.created_on, p.id )
									FROM
										persons p
									WHERE
										p.is_primary = 1
										AND p.ps_lead_id ' . $strCondition . '
										AND p.id IN ( SELECT DISTINCT(person_id) FROM person_phone_numbers WHERE phone_number IS NOT NULL )
										AND p.name_first IS NOT NULL
										AND p.name_last IS NOT NULL
										AND p.email_address IS NOT NULL
										AND p.title IS NOT NULL
										AND p.person_role_id IS NOT NULL
										AND p.deleted_by IS NULL
										AND p.address_line1 IS NOT NULL
										AND p.city IS NOT NULL
										AND p.state_code IS NOT NULL
										AND p.postal_code IS NOT NULL
								) AS p ON ( p.ps_lead_id = pl.id AND p.rank = 1 )
								LEFT JOIN
								(
									SELECT
										p.ps_lead_id,
										1 AS non_primary_person_points
									FROM
										persons p
										JOIN ps_leads pl ON ( pl.id = p.ps_lead_id )
									WHERE
										p.ps_lead_id ' . $strCondition . '
										AND p.is_primary <> 1
										AND p.id IN ( SELECT DISTINCT(person_id) FROM person_phone_numbers WHERE phone_number IS NOT NULL )
										AND p.name_first IS NOT NULL
										AND p.name_last IS NOT NULL
										AND p.email_address IS NOT NULL
										AND p.person_role_id IS NOT NULL
										AND p.title IS NOT NULL
										AND p.deleted_by IS NULL
										OR ( pl.number_of_units IS NULL OR COALESCE ( pl.number_of_units, 0 ) < 500 )
									GROUP BY
										p.ps_lead_id,
										pl.number_of_units
									HAVING
										( CASE
											WHEN COALESCE ( pl.number_of_units, 0 ) < 500 THEN count ( * ) > - 1
											WHEN COALESCE ( pl.number_of_units, 0 ) < 1500 THEN count ( * ) > 0
											WHEN COALESCE ( pl.number_of_units, 0 ) < 5000 THEN count ( * ) > 2
											WHEN COALESCE ( pl.number_of_units, 0 ) < 10000 THEN count ( * ) > 4
											WHEN COALESCE ( pl.number_of_units, 0 ) < 20000 THEN count ( * ) > 6
											ELSE count ( * ) > 8
										END )
								) AS npp ON ( npp.ps_lead_id = pl.id )
								LEFT JOIN
								(
									SELECT
										plc.ps_lead_id,
										1 AS competitor_points
									FROM
										ps_lead_competitors plc
										JOIN ps_leads pl ON ( pl.id = plc.ps_lead_id )
									WHERE
										plc.ps_lead_id ' . $strCondition . '
									GROUP BY
										plc.ps_lead_id,
										pl.number_of_units
									HAVING
										( CASE
											WHEN COALESCE ( pl.number_of_units, 0 ) < 500 THEN count ( * ) > 1
											WHEN COALESCE ( pl.number_of_units, 0 ) < 1500 THEN count ( * ) > 2
											WHEN COALESCE ( pl.number_of_units, 0 ) < 5000 THEN count ( * ) > 3
											WHEN COALESCE ( pl.number_of_units, 0 ) < 10000 THEN count ( * ) > 4
											WHEN COALESCE ( pl.number_of_units, 0 ) < 20000 THEN count ( * ) > 6
											ELSE count ( * ) > 8
										END )
								) AS comp ON ( pl.id = comp.ps_lead_id )
								LEFT JOIN
								(
									SELECT
										plpt.ps_lead_id,
										1 AS property_type_points
									FROM
										ps_lead_property_types plpt
									WHERE
										property_type_id IS NOT NULL
										AND plpt.ps_lead_id ' . $strCondition . '
									GROUP BY
										plpt.ps_lead_id
									HAVING
										count ( * ) > 0
								) AS plpt ON ( plpt.ps_lead_id = pl.id )
								LEFT JOIN
								(
									SELECT
										plc.ps_lead_id,
										1 AS entrata_competitor_points
									FROM
										ps_lead_competitors plc
									WHERE
										ps_product_id = ' . CPsProduct::ENTRATA . '
										AND plc.ps_lead_id ' . $strCondition . '
										AND competitor_id IS NOT NULL
									GROUP BY
										plc.ps_lead_id
									HAVING
										count ( * ) > 0
								) AS entrata_plc ON ( entrata_plc.ps_lead_id = pl.id )
								LEFT JOIN
								(
									SELECT
										c.ps_lead_id,
										1 AS opportunity_points
									FROM
										contracts c
									WHERE
										c.contract_status_type_id NOT IN ( ' . implode( ',', CContractStatusType::$c_arrintInactiveContractStatusTypeIds ) . ' )
										AND c.ps_lead_id ' . $strCondition . '
									GROUP BY
										c.ps_lead_id
									HAVING
										count ( * ) > 0
								) AS c ON ( c.ps_lead_id = pl.id )
							WHERE
								pl.id ' . $strCondition . ' ) AS points
					WHERE
						points.ps_lead_id = pl.id
						AND pl.id ' . $strCondition . '
						AND profile_percent::NUMERIC ( 15, 2 ) <> ( ( unit_count_points + company_name_count + primary_person_points + ps_lead_type_points + primary_website_url_points + non_primary_person_points + competitor_points + property_type_points + entrata_competitor_points + opportunity_points )::numeric ( 15, 2 ) / 10 ) ::NUMERIC ( 15, 2 );';

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * Fetch Functions
	 */

	public static function fetchUnassignedPsLeadsByPsLeadOriginIdsByLimit( $arrintPsLeadOriginIds, $intLimit, $objDatabase ) {

		if( false == valArr( $arrintPsLeadOriginIds ) ) return NULL;

		$strSql = 'SELECT
						pl.*,
						p.name_first,
						p.name_last,
						p.title,
						p.id AS person_id,
						p.email_address,
						p.address_line1,
						p.address_line2,
						p.city,
						p.state_code,
						p.postal_code
					FROM
						ps_leads pl,
						persons p,
						ps_lead_details pld
					WHERE
						pl.id = p.ps_lead_id
						AND p.is_primary=1
						AND ps_lead_origin_id IN ( ' . implode( ',', $arrintPsLeadOriginIds ) . ' )
						AND pld.sales_employee_id IS NULL
						AND pl.deleted_on IS NULL
						AND pld.ps_lead_id = pl.id
					ORDER BY
						pl.request_datetime DESC
					LIMIT
						' . ( int ) $intLimit;

		return self::fetchPsLeads( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPsLeadsCount( $objPsLeadFilter, $objDatabase ) {

		$objPsLeadFilter->setIsCountRequest( true );

		$strSql = 'SELECT
						count(id)
					FROM (
						SELECT
							pl.id ' . self::buildCommonPsLeadSearchSql( $objPsLeadFilter ) . '
					GROUP BY pl.id ) AS sub ';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchPaginatedPsLeadsUsingLastContactedDate( $intPageNo, $intPageSize, $objPsLeadFilter, $objDatabase ) {

		$intOffset 			= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  			= ( 0 < $intPageSize ) ? ( int ) $intPageSize : 50;

		$strSelectParameters = '';

		if( false == is_null( $objPsLeadFilter ) && 0 == $objPsLeadFilter->getDoNotUsePsLeadView() && 0 == $objPsLeadFilter->getIsCountRequest() ) {
			$strSelectParameters = ',primary_persons.name_first,
									primary_persons.name_last,
									primary_persons.title,
									primary_persons.id AS person_id,
									primary_persons.email_address,
									primary_persons.address_line1,
									primary_persons.address_line2,
									primary_persons.city,
									pld.state_code,
									primary_persons.postal_code,
									pld.primary_website_urls ';
		} elseif( false == is_null( $objPsLeadFilter ) && 1 == $objPsLeadFilter->getDoNotUsePsLeadView() ) {
			$strSelectParameters = ',p.name_first,
									p.name_last,
									p.title,
									p.id AS person_id,
									p.email_address,
									p.address_line1,
									p.address_line2,
									pld.city,
									pld.state_code,
									pld.postal_code,
									pld.primary_website_urls ';
		}

		$strSql = ' SELECT
						DISTINCT ON ( ' . $objPsLeadFilter->getSortBy() . ', pl.id )
						pl.*, mc.id as cid
						' . $strSelectParameters;

		$strSql .= self::buildCommonPsLeadSearchSql( $objPsLeadFilter );

		$strSql .= ' ORDER BY
						' . $objPsLeadFilter->getSortBy() . ' ' . $objPsLeadFilter->getSortDirection() . ',
						pl.id
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit . ' ';

	$arrobjPsLeads = parent::fetchPsLeads( $strSql, $objDatabase );

		// Here we tack on some data that's faster to load in PHP than in the query.
		if( true == valArr( $arrobjPsLeads ) ) {

			// We need to fire a query to load the max contact date time
			$strSql = 'SELECT
							ps_lead_id,
							max(action_datetime) as max_action_datetime
						FROM
							actions
						WHERE
							ps_lead_id IN ( ' . implode( ',', array_keys( $arrobjPsLeads ) ) . ' )
							AND action_type_id IN ( ' . implode( ',', CActionType::$c_arrintLastContactDateActionTypeIds ) . ' )
						GROUP BY
							ps_lead_id';

			$arrstrLastContacts = fetchData( $strSql, $objDatabase );

			if( true == valArr( $arrstrLastContacts ) ) {
				foreach( $arrstrLastContacts as $arrstrLastContact ) {
					if( true == array_key_exists( $arrstrLastContact['ps_lead_id'], $arrobjPsLeads ) ) {
						$arrobjPsLeads[$arrstrLastContact['ps_lead_id']]->setRequestDatetime( $arrstrLastContact['max_action_datetime'] );
					}
				}
			}

			// We need to fire a query to load the max contact status type
			$strSql = 'SELECT
							ps_lead_id,
							max ( contract_status_type_id ) as max_contract_status_type_id
						FROM
							contracts
						WHERE
							ps_lead_id IN ( ' . implode( ',', array_keys( $arrobjPsLeads ) ) . ' )
						GROUP BY
							ps_lead_id';

			$arrstrMaxStatusTypes = fetchData( $strSql, $objDatabase );

			if( true == valArr( $arrstrMaxStatusTypes ) ) {
				foreach( $arrstrMaxStatusTypes as $arrstrMaxStatusType ) {
					if( true == array_key_exists( $arrstrMaxStatusType['ps_lead_id'], $arrobjPsLeads ) ) {
						$arrobjPsLeads[$arrstrMaxStatusType['ps_lead_id']]->setMaxContractStatusTypeId( $arrstrMaxStatusType['max_contract_status_type_id'] );
					}
				}
			}
		}

		return $arrobjPsLeads;
	}

	// Fetch all possible duplicate Leads count

	public static function fetchPaginatedPossibleDuplicatePsLeadsCount( $objPsLeadFilter, $objDatabase ) {

		$objPsLeadFilter->setIsCountRequest( true );

		$strCommonPsleadSql = self::buildCommonPossibleDuplicatePsLeadsSearchSql( $objPsLeadFilter, $objDatabase );

		$arrstrCommonSql			= [];
		$strCommonPsleadSelectSql	= '';
		$strCommonPsleadWhereSql	= '';

		$arrstrCommonSql[] = explode( '||', $strCommonPsleadSql );

		$strCommonPsleadSelectSql = $arrstrCommonSql[0][0];
		$strCommonPsleadWhereSql  = $arrstrCommonSql[0][1];

		$strSql = 'SELECT
						count( ps_lead_dupe_id )
					FROM
						(
							SELECT
								sub.id
							FROM
								( (
									SELECT
										pl.id AS id,
										pl.company_name,
										CASE
											WHEN mc.id IS NOT NULL THEN 0
										ELSE 1
										END AS has_client,
										mc.id as cid,
										pl_dupe.id AS dupe_ps_lead_id
									FROM
										ps_leads pl
										LEFT JOIN clients mc ON ( mc.ps_lead_id = pl.id )
										JOIN ps_lead_duplicates pld ON ( pl.id = pld.ps_lead_id1 AND pld.processed_by IS NULL )
										JOIN ps_leads pl_dupe ON ( pl_dupe.id = pld.ps_lead_id2 )
										LEFT JOIN clients mc_dupe ON ( mc_dupe.ps_lead_id = pl_dupe.id )
										JOIN persons p ON ( p.ps_lead_id = pl.id AND p.is_primary = 1)
										' . $strCommonPsleadSelectSql . '
									WHERE
										pl.company_name IS NOT NULL
										AND pl.company_name <> \'\'
										AND pl.deduped_on IS NULL
										AND pl.deleted_on IS NULL
										AND pl_dupe.deduped_on IS NULL
										AND pl_dupe.deleted_on IS NULL
										AND mc_dupe.id IS NULL
										AND pl.deleted_by IS NULL
										' . $strCommonPsleadWhereSql . '
									GROUP BY
										pl.id,
										pl_dupe.id,
										pl.company_name,
										mc.id
								) ) AS sub
							GROUP BY
								sub.id,
								sub.company_name,
								sub.cid,
								sub.has_client
						) AS ps_lead_dupe_id';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	// Temp Code.

	public static function fetchPaginatedPossibleDuplicatePsLeadsHavingMergeableLeadsCount( $objPsLeadFilter, $objDatabase ) {

		$objPsLeadFilter->setIsCountRequest( true );

		$strCommonPsleadSql = self::buildCommonPossibleDuplicatePsLeadsSearchSql( $objPsLeadFilter, $objDatabase );

		$arrstrCommonSql			= [];
		$strCommonPsleadSelectSql	= '';
		$strCommonPsleadWhereSql	= '';

		$arrstrCommonSql[] = explode( '||', $strCommonPsleadSql );

		$strCommonPsleadSelectSql = $arrstrCommonSql[0][0];
		$strCommonPsleadWhereSql  = $arrstrCommonSql[0][1];

		$strSql = 'SELECT
						count( ps_lead_dupe_id )
					FROM
						(
							SELECT
								sub.id
							FROM
								( (
									SELECT
										pl.id AS id,
										pl.company_name,
										CASE
											WHEN mc.id IS NOT NULL THEN 0
										ELSE 1
										END AS has_client,
										mc.id as cid,
										pl_dupe.id AS dupe_ps_lead_id
									FROM
										ps_leads pl
										LEFT JOIN clients mc ON ( mc.ps_lead_id = pl.id )
										JOIN ps_lead_duplicates pld ON ( pl.id = pld.ps_lead_id1 AND pld.processed_by IS NULL )
										JOIN ps_leads pl_dupe ON ( pl_dupe.id = pld.ps_lead_id2 )
										LEFT JOIN clients mc_dupe ON ( mc_dupe.ps_lead_id = pl_dupe.id )
										JOIN persons p ON ( p.ps_lead_id = pl_dupe.id AND p.is_primary = 1 )
										' . $strCommonPsleadSelectSql . '
									WHERE
										pl.company_name IS NOT NULL
										AND pl.company_name <> \'\'
										AND pl.deduped_on IS NULL
										AND pl.deleted_on IS NULL
										AND pl.ps_lead_id IS NULL
										AND pl_dupe.deduped_on IS NULL
										AND pl_dupe.deleted_on IS NULL
										AND pl.deleted_by IS NULL
										AND pl_dupe.ps_lead_id IS NULL
										AND pld.ignored_by IS NULL
										AND mc_dupe.id IS NULL
										AND pl_dupe.ps_lead_origin_id NOT IN ( ' . CPsLeadOrigin::VERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::UNVERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::MIDVERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::NMHC . ' )
										AND p.is_primary = 1' . $strCommonPsleadWhereSql . '
									GROUP BY
										pl.id,
										pl_dupe.id,
										pl.company_name,
										mc.id
								) ) AS sub
							GROUP BY
								sub.id,
								sub.company_name,
								sub.cid,
								sub.has_client
						) AS ps_lead_dupe_id';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	// Fetch all possible duplicate Leads

	public static function fetchPaginatedPossibleDuplicatePsLeadsUsingLastContactedDate( $intPageNo, $intPageSize, $objPsLeadFilter, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strCommonPsleadSql = self::buildCommonPossibleDuplicatePsLeadsSearchSql( $objPsLeadFilter, $objDatabase );

		$arrstrCommonSql			= [];
		$strCommonPsleadSelectSql	= '';
		$strCommonPsleadWhereSql	= '';

		$arrstrCommonSql[] = explode( '||', $strCommonPsleadSql );

		$strCommonPsleadSelectSql = $arrstrCommonSql[0][0];
		$strCommonPsleadWhereSql  = $arrstrCommonSql[0][1];

		$strSql = 'SELECT
					sub.id,
					sub.company_name,
					max ( sub.request_datetime ) AS request_datetime,
					max ( sub.company_name ) AS company_name,
					max ( sub.ps_lead_origin_id ) AS ps_lead_origin_id,
					max ( sub.ps_lead_source_id ) AS ps_lead_source_id,
					max ( sub.number_of_units ) AS number_of_units,
					max ( sub.property_count ) AS property_count,
					sub.cid,
					max ( sub.name_first ) AS name_first,
					max ( sub.name_last ) AS name_last,
					max ( sub.title ) AS title,
					max ( sub.id ) AS person_id,
					max ( sub.email_address ) AS email_address,
					max ( sub.address_line1 ) AS address_line1,
					max ( sub.address_line2 ) AS address_line2,
					max ( sub.city ) AS city,
					max ( sub.state_code ) AS state_code,
					max ( sub.postal_code ) AS postal_code,
					count ( sub.dupe_ps_lead_id ) AS dupe_count
				FROM
					( (
						SELECT
							pl.id AS id,
							pl.company_name,
							max ( pl.request_datetime ) AS request_datetime,
							max ( pl.ps_lead_origin_id ) AS ps_lead_origin_id,
							max ( pl.ps_lead_source_id ) AS ps_lead_source_id,

							max ( pl.number_of_units ) AS number_of_units,
							max ( pl.property_count ) AS property_count,
							CASE
							  WHEN mc.id IS NOT NULL THEN 0
							  ELSE 1
							END AS has_client,
							mc.id as cid,
							pl_dupe.id AS dupe_ps_lead_id,
							count ( pl_dupe.id ),
							max ( p.name_first ) AS name_first,
							max ( p.name_last ) AS name_last,
							max ( p.title ) AS title,
							max ( p.email_address ) AS email_address,
							max ( p.address_line1 ) AS address_line1,
							max ( p.address_line2 ) AS address_line2,
							max ( p.city ) AS city,
							max ( p.state_code ) AS state_code,
							max ( p.postal_code ) AS postal_code
						FROM
							ps_leads pl
							LEFT JOIN clients mc ON ( mc.ps_lead_id = pl.id )
							JOIN ps_lead_duplicates pld ON ( pl.id = pld.ps_lead_id1 AND pld.processed_by IS NULL )
							JOIN ps_leads pl_dupe ON ( pl_dupe.id = pld.ps_lead_id2 )
							LEFT JOIN clients mc_dupe ON ( mc_dupe.ps_lead_id = pl_dupe.id )
							JOIN persons p ON ( p.ps_lead_id = pl_dupe.id AND p.is_primary = 1 )
							LEFT JOIN person_contact_preferences pcp ON ( p.id = pcp.person_id ) ' . $strCommonPsleadSelectSql . '
						WHERE
							pl.company_name IS NOT NULL
							AND pl.company_name <> \'\'
							AND pl.deduped_on IS NULL
							AND pl.deleted_on IS NULL
							-- AND pl.ps_lead_id IS NULL
							AND pl_dupe.deduped_on IS NULL
							AND pl_dupe.deleted_on IS NULL
							AND mc_dupe.id IS NULL
							AND pl_dupe.ps_lead_id IS NULL' . $strCommonPsleadWhereSql . '
						GROUP BY
							pl.id,
							pl_dupe.id,
							pl.company_name,
							mc.id
					) ) AS sub
				GROUP BY
					sub.id,
					sub.company_name,
					sub.cid,
					sub.has_client
				ORDER BY
					sub.has_client,
					lower ( sub.company_name ),
					request_datetime DESC, id';

			$strSql .= ' OFFSET ' . ( int ) $intOffset . '
						 LIMIT ' . ( int ) $intLimit;

		$arrobjPsLeads = parent::fetchPsLeads( $strSql, $objDatabase );

		// Here we tack on some data that's faster to load in PHP than in the query.
		if( true == valArr( $arrobjPsLeads ) ) {

			// We need to fire a query to load the max contact date time
			$strSql = 'SELECT
							ps_lead_id,
							max(action_datetime) as max_action_datetime
						FROM
							actions
						WHERE
							ps_lead_id IN ( ' . implode( ',', array_keys( $arrobjPsLeads ) ) . ' )
						GROUP BY
							ps_lead_id';

			$arrstrLastContacts = fetchData( $strSql, $objDatabase );

			if( true == valArr( $arrstrLastContacts ) ) {
				foreach( $arrstrLastContacts as $arrstrLastContact ) {
					if( true == array_key_exists( $arrstrLastContact['ps_lead_id'], $arrobjPsLeads ) ) {
						$arrobjPsLeads[$arrstrLastContact['ps_lead_id']]->setRequestDatetime( $arrstrLastContact['max_action_datetime'] );
					}
				}
			}

			// We need to fire a query to load the max contact status type
			$strSql = 'SELECT
							ps_lead_id,
							max ( contract_status_type_id ) as max_contract_status_type_id
						FROM
							contracts
						WHERE
							ps_lead_id IN ( ' . implode( ',', array_keys( $arrobjPsLeads ) ) . ' )
						GROUP BY
							ps_lead_id';

			$arrstrMaxStatusTypes = fetchData( $strSql, $objDatabase );

			if( true == valArr( $arrstrMaxStatusTypes ) ) {
				foreach( $arrstrMaxStatusTypes as $arrstrMaxStatusType ) {
					if( true == array_key_exists( $arrstrMaxStatusType['ps_lead_id'], $arrobjPsLeads ) ) {
						$arrobjPsLeads[$arrstrMaxStatusType['ps_lead_id']]->setMaxContractStatusTypeId( $arrstrMaxStatusType['max_contract_status_type_id'] );
					}
				}
			}
		}

		return $arrobjPsLeads;
	}

	// Temp Code.

	public static function fetchPaginatedPossibleDuplicatePsLeadsHavingMergeableLeads( $intPageNo, $intPageSize, $objPsLeadFilter, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strCommonPsleadSql = self::buildCommonPossibleDuplicatePsLeadsSearchSql( $objPsLeadFilter, $objDatabase );

		$arrstrCommonSql			= [];
		$strCommonPsleadSelectSql	= '';
		$strCommonPsleadWhereSql	= '';

		$arrstrCommonSql[] = explode( '||', $strCommonPsleadSql );

		$strCommonPsleadSelectSql = $arrstrCommonSql[0][0];
		$strCommonPsleadWhereSql  = $arrstrCommonSql[0][1];

		$strSql = 'SELECT
					sub.id,
					sub.company_name,
					max ( sub.request_datetime ) AS request_datetime,
					max ( sub.company_name ) AS company_name,
					max ( sub.ps_lead_origin_id ) AS ps_lead_origin_id,
					max ( sub.ps_lead_source_id ) AS ps_lead_source_id,
					max ( sub.number_of_units ) AS number_of_units,
					max ( sub.property_count ) AS property_count,
					sub.cid,
					max ( sub.name_first ) AS name_first,
					max ( sub.name_last ) AS name_last,
					max ( sub.title ) AS title,
					max ( sub.id ) AS person_id,
					max ( sub.email_address ) AS email_address,
					max ( sub.address_line1 ) AS address_line1,
					max ( sub.address_line2 ) AS address_line2,
					max ( sub.city ) AS city,
					max ( sub.state_code ) AS state_code,
					max ( sub.postal_code ) AS postal_code,
					count ( sub.dupe_ps_lead_id ) AS dupe_count
				FROM
					( (
						SELECT
							pl.id AS id,
							pl.company_name,
							max ( pl.request_datetime ) AS request_datetime,
							max ( pl.ps_lead_origin_id ) AS ps_lead_origin_id,
							max ( pl.ps_lead_source_id ) AS ps_lead_source_id,

							max ( pl.number_of_units ) AS number_of_units,
							max ( pl.property_count ) AS property_count,
							CASE
							  WHEN mc.id IS NOT NULL THEN 0
							  ELSE 1
							END AS has_client,
							mc.id AS cid,
							pl_dupe.id AS dupe_ps_lead_id,
							count ( pl_dupe.id ),
							max ( p.name_first ) AS name_first,
							max ( p.name_last ) AS name_last,
							max ( p.title ) AS title,
							max ( p.id ) AS person_id,
							max ( p.email_address ) AS email_address,
							max ( p.address_line1 ) AS address_line1,
							max ( p.address_line2 ) AS address_line2,
							max ( p.city ) AS city,
							max ( p.state_code ) AS state_code,
							max ( p.postal_code ) AS postal_code
						FROM
							ps_leads pl
							LEFT JOIN clients mc ON ( mc.ps_lead_id = pl.id )
							JOIN ps_lead_duplicates pld ON ( pl.id = pld.ps_lead_id1 AND pld.processed_by IS NULL )
							JOIN ps_leads pl_dupe ON ( pl_dupe.id = pld.ps_lead_id2 )
							LEFT JOIN clients mc_dupe ON ( mc_dupe.ps_lead_id = pl_dupe.id )
							JOIN persons p ON ( p.ps_lead_id = pl_dupe.id AND p.is_primary = 1 )
							LEFT JOIN person_contact_preferences pcp ON ( p.id = pcp.person_id ) ' . $strCommonPsleadSelectSql . '
						WHERE
							pl.company_name IS NOT NULL
							AND pl.company_name <> \'\'
							AND pl.deduped_on IS NULL
							AND pl.deleted_on IS NULL
							AND pl.ps_lead_id IS NULL
							AND pld.ignored_by IS NULL
							AND pl_dupe.deduped_on IS NULL
							AND pl_dupe.deleted_on IS NULL
							AND mc_dupe.id IS NULL
							AND pl_dupe.ps_lead_origin_id NOT IN ( ' . CPsLeadOrigin::VERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::UNVERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::MIDVERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::NMHC . ' )
							AND pl_dupe.ps_lead_id IS NULL' . $strCommonPsleadWhereSql . '
						GROUP BY
							pl.id,
							pl_dupe.id,
							pl.company_name,
							mc.id
					) ) AS sub
				GROUP BY
					sub.id,
					sub.company_name,
					sub.cid,
					sub.has_client
				ORDER BY
					sub.has_client,
					lower ( sub.company_name ),
					request_datetime DESC, id';

		$strSql .= ' OFFSET ' . ( int ) $intOffset . '
						 LIMIT ' . ( int ) $intLimit;

		$arrobjPsLeads = parent::fetchPsLeads( $strSql, $objDatabase );

		// Here we tack on some data that's faster to load in PHP than in the query.
		if( true == valArr( $arrobjPsLeads ) ) {

			// We need to fire a query to load the max contact date time
			$strSql = 'SELECT
							ps_lead_id,
							max(action_datetime) as max_action_datetime
						FROM
							actions
						WHERE
							ps_lead_id IN ( ' . implode( ',', array_keys( $arrobjPsLeads ) ) . ' )
						GROUP BY
							ps_lead_id';

			$arrstrLastContacts = fetchData( $strSql, $objDatabase );

			if( true == valArr( $arrstrLastContacts ) ) {
				foreach( $arrstrLastContacts as $arrstrLastContact ) {
					if( true == array_key_exists( $arrstrLastContact['ps_lead_id'], $arrobjPsLeads ) ) {
						$arrobjPsLeads[$arrstrLastContact['ps_lead_id']]->setRequestDatetime( $arrstrLastContact['max_action_datetime'] );
					}
				}
			}

			// We need to fire a query to load the max contact status type
			$strSql = 'SELECT
							ps_lead_id,
							max ( contract_status_type_id ) as max_contract_status_type_id
						FROM
							contracts
						WHERE
							ps_lead_id IN ( ' . implode( ',', array_keys( $arrobjPsLeads ) ) . ' )
						GROUP BY
							ps_lead_id';

			$arrstrMaxStatusTypes = fetchData( $strSql, $objDatabase );

			if( true == valArr( $arrstrMaxStatusTypes ) ) {
				foreach( $arrstrMaxStatusTypes as $arrstrMaxStatusType ) {
					if( true == array_key_exists( $arrstrMaxStatusType['ps_lead_id'], $arrobjPsLeads ) ) {
						$arrobjPsLeads[$arrstrMaxStatusType['ps_lead_id']]->setMaxContractStatusTypeId( $arrstrMaxStatusType['max_contract_status_type_id'] );
					}
				}
			}
		}

		return $arrobjPsLeads;
	}

	public static function buildCommonPossibleDuplicatePsLeadsSearchSql( $objPsLeadFilter, $objDatabase ) {

		$boolJoinPersons 					= false;
		$boolJoinContracts 					= false;
		$boolJoinActions 					= false;
		$boolJoinContractProducts 			= false;
		$boolJoinPsLeadDetails 				= false;
		$boolJoinEmployees 					= false;
		$boolJoinContractWatchlists			= false;
		$boolJoinClients		= false;
		$boolPersonPrimary 					= false;
		$boolJoinContracts 					= false;

		if( 0 < strlen( trim( $objPsLeadFilter->getStateCodes() ) ) ) 						$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getTitle() ) ) ) 							$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getNameFirst() ) ) ) 						$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getNameLast() ) ) ) 						$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getEmailAddress() ) ) ) 					$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getCity() ) ) ) 							$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getPostalCode() ) ) ) 						$boolJoinPersons = true;

		if( 0 < strlen( trim( $objPsLeadFilter->getPersons() ) ) )							$boolJoinPersons = true;

		if( true == $objPsLeadFilter->getJoinPersons() ) 									$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getPhoneNumber() ) ) ) 						$boolJoinPersons = true;

		if( 0 < strlen( trim( $objPsLeadFilter->getContractStatusTypes() ) ) ) 			$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getEmployees() ) ) ) 						$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getEstimatedCloseStartDate() ) ) ) 		$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getEstimatedCloseEndDate() ) ) ) 			$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActualCloseStartDate() ) ) ) 			$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActualCloseEndDate() ) ) ) 				$boolJoinContracts = true;

		if( 1 == $objPsLeadFilter->getRequireCloseDate() ) {
			$boolJoinContracts = true;
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getResponsibleEmployees() ) ) )				$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getSupportEmployees() ) ) )					$boolJoinContracts = true;

		if( 1 == $objPsLeadFilter->getShowWatchlistGroupsOnly() ) {
			$boolJoinContracts = true;
			$boolJoinContractWatchlists = true;
			$boolJoinContracts = true;
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getCompanyStatusTypes() ) ) )				$boolJoinClients = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getMissingContractPsProducts() ) ) )		$boolJoinClients = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getContractPsProducts() ) ) )				$boolJoinClients = true;

		if( 0 < strlen( trim( $objPsLeadFilter->getMissingContractPsProducts() ) ) )		$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getContractPsProducts() ) ) )				$boolJoinContracts = true;

		if( 0 < strlen( trim( $objPsLeadFilter->getActionTypes() ) ) ) 						$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActionResults() ) ) ) 					$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getNoteText() ) ) ) 						$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getEmployees() ) ) ) 						$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActionStartDate() ) ) ) 					$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActionEndDate() ) ) ) 					$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getResultStartDate() ) ) ) 					$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getResultEndDate() ) ) ) 					$boolJoinActions = true;

		if( 0 < strlen( trim( $objPsLeadFilter->getPsProducts() ) ) ) {
			$boolJoinContractProducts = true;
			$boolJoinContracts = true;
		}
		if( 0 < strlen( trim( $objPsLeadFilter->getJoinPsLeadDetails() ) ) ) 				$boolJoinPsLeadDetails = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getJoinEmployees() ) ) ) 					$boolJoinEmployees = true;

		if( true == $objPsLeadFilter->getIsForMassEmailing() ) {
			$boolJoinPersons = true;
		}

		$strSql = '';

		if( 1 == $objPsLeadFilter->getDoNotUsePsLeadView() ) {
			$boolJoinPersons 		= true;
			$boolPersonPrimary 	= true;

		}

		$strIsBlockEmails = '';

		if( true == $boolJoinPersons && true == $objPsLeadFilter->getIsForMassEmailing() ) {
			$strIsBlockEmails .= ' AND ( p_org.email_address IS NOT NULL AND p_org.email_address LIKE \'%@%\' ) ';
		}

		if( true == $boolJoinPersons )				$strSql .= 'JOIN persons p_org ON ( p_org.ps_lead_id = pl.id AND p.is_primary=1) ' . $strIsBlockEmails;

		if( true == $boolJoinActions ) 									$strSql .= ' LEFT JOIN actions a ON ( a.ps_lead_id = pl.id ) ';
		if( true == $boolJoinPsLeadDetails ) 								$strSql .= ' LEFT JOIN ps_lead_details pld_org ON ( pld_org.ps_lead_id = pl.id ) ';
		if( true == $boolJoinEmployees && true == $boolJoinPsLeadDetails )
																			$strSql .= ' LEFT JOIN employees e_org ON
																						( ( pld_org.sales_employee_id IS NOT NULL AND pld_org.sales_employee_id = e_org.id ) )';

		if( true == $boolJoinClients ) 							$strSql .= ' LEFT JOIN clients mc_org ON ( mc_org.ps_lead_id = pl.id ) ';

		if( true == $boolJoinContracts ) 									$strSql .= ' JOIN contracts c_org ON ( c_org.ps_lead_id = pl.id  AND c_org.cid = mc.id AND mc.ps_lead_id = pl.id )
																							LEFT JOIN contract_properties cpro_org ON ( c_org.id = cpro_org.contract_id AND cpro_org.renewal_contract_property_id IS NULL AND cpro_org.is_last_contract_record = TRUE AND ( cpro_org.termination_date IS NULL OR cpro_org.termination_date > NOW() )) ';
		if( true == $boolJoinContractProducts ) 							$strSql .= ' JOIN contract_products cp_org ON ( cp_org.contract_id = c_org.id ) ';
		if( true == $boolJoinContractWatchlists ) 							$strSql .= ' JOIN contract_watchlists cw_org ON ( cw_org.contract_id = c_org.id AND cw_org.employee_id = ' . ( int ) $objPsLeadFilter->getCurrentEmployeeId() . ' ) ';

		if( 0 < strlen( trim( $objPsLeadFilter->getResponsibleEmployees() ) ) ) {
			$strSql .= ' JOIN contract_employees ce_org ON ( ce_org.contract_id = c_org.id AND ce_org.contract_employee_type_id = ' . CContractEmployeeType::RESPONSIBLE . ' AND ce_org.employee_id IN( ' . $objPsLeadFilter->getResponsibleEmployees() . ' ) ) ';
		}

		$strWhereSql = ' ';

		if( 0 < strlen( $objPsLeadFilter->getPsLeadIds() ) )  							$strWhereSql .= ' AND pl.id IN ( ' . $objPsLeadFilter->getPsLeadIds() . ' ) ';

		if( 0 < strlen( trim( $objPsLeadFilter->getPersons() ) ) ) 						$strWhereSql .= ' AND p_org.id IN ( ' . $objPsLeadFilter->getPersons() . ' ) ';

		if( 0 < strlen( trim( $objPsLeadFilter->getCompanyStatusTypes() ) ) ) 			$strWhereSql .= ' AND ( mc_org.id IS NULL OR mc_org.company_status_type_id IN ( ' . $objPsLeadFilter->getCompanyStatusTypes() . ' ) ) ';

		if( 0 < strlen( trim( $objPsLeadFilter->getPsLeadOrigins() ) ) ) 				$strWhereSql .= ' AND pl.ps_lead_origin_id IN ( ' . $objPsLeadFilter->getPsLeadOrigins() . ' ) -- AND pl_dupe.ps_lead_origin_id IN ( ' . $objPsLeadFilter->getPsLeadOrigins() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getPsLeadSources() ) ) ) 				$strWhereSql .= ' AND pl.ps_lead_source_id IN ( ' . $objPsLeadFilter->getPsLeadSources() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getSalesEmployees() ) ) ) 				$strWhereSql .= ' AND pld_org.sales_employee_id IN ( ' . $objPsLeadFilter->getSalesEmployees() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getPsLeadEvents() ) ) ) 				$strWhereSql .= ' AND pl.ps_lead_event_id IN ( ' . $objPsLeadFilter->getPsLeadEvents() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getSupportEmployees() ) ) ) 			$strSql .= ' AND pld.support_employee_id IN ( ' . $objPsLeadFilter->getSupportEmployees() . ' ) ';

		if( 0 < strlen( trim( $objPsLeadFilter->getCompanyName() ) ) ) {
			$boolPersonPrimary = false;
			$strWhereSql .= ' AND pl.company_name ILIKE \'%' . addslashes( $objPsLeadFilter->getCompanyName() ) . '%\' ';

		}

		if( true == $objPsLeadFilter->getShowNullPsEmployeeIdOnly() ) 					$strWhereSql .= ' AND pld_org.sales_employee_id IS NULL ';
		if( 0 < strlen( trim( $objPsLeadFilter->getQuickSearch() ) ) ) 					$strWhereSql .= ' AND ( pl.id = ' . ( int ) $objPsLeadFilter->getQuickSearch() . ' OR pl.company_name ILIKE \'%' . addslashes( $objPsLeadFilter->getQuickSearch() ) . '%\' )';
		if( 0 < strlen( trim( $objPsLeadFilter->getRequestDatetime() ) ) ) 				$strWhereSql .= ' AND DATE_TRUNC( \'month\', pl.request_datetime ) = \'' . $objPsLeadFilter->getRequestDatetime() . '\' ';

		if( 0 < strlen( trim( $objPsLeadFilter->getStartRequestDate() ) ) ) 			$strWhereSql .= ' AND DATE_TRUNC ( \'day\', pl.request_datetime ) >= \'' . $objPsLeadFilter->getStartRequestDate() . '\' ';
		if( 0 < strlen( trim( $objPsLeadFilter->getEndRequestDate() ) ) ) 				$strWhereSql .= ' AND DATE_TRUNC ( \'day\', pl.request_datetime ) < \'' . $objPsLeadFilter->getEndRequestDate() . '\' ';

		if( 0 < strlen( trim( $objPsLeadFilter->getEstimatedCloseStartDate() ) ) ) 		$strWhereSql .= ' AND c_org.anticipated_close_date IS NOT NULL AND c_org.anticipated_close_date >= \'' . $objPsLeadFilter->getEstimatedCloseStartDate() . '\' ';
		if( 0 < strlen( trim( $objPsLeadFilter->getEstimatedCloseEndDate() ) ) ) 		$strWhereSql .= ' AND c_org.anticipated_close_date IS NOT NULL AND c_org.anticipated_close_date < \'' . $objPsLeadFilter->getEstimatedCloseEndDate() . '\' ';
		if( 0 < strlen( trim( $objPsLeadFilter->getActualCloseStartDate() ) ) ) 		$strWhereSql .= ' AND cpro_org.close_date IS NOT NULL AND cpro_org.close_date >= \'' . $objPsLeadFilter->getActualCloseStartDate() . '\' ';
		if( 0 < strlen( trim( $objPsLeadFilter->getActualCloseEndDate() ) ) ) 			$strWhereSql .= ' AND cpro_org.close_date IS NOT NULL AND cpro_org.close_date < \'' . $objPsLeadFilter->getActualCloseEndDate() . '\' ';
		if( 1 == $objPsLeadFilter->getRequireCloseDate() ) 								$strWhereSql .= ' AND ( c_org.close_date IS NOT NULL OR c_org.anticipated_close_date IS NOT NULL ) ';

		if( true == is_numeric( $objPsLeadFilter->getPsLeadId() ) ) 					$strWhereSql .= ' AND pl.id = ' . ( int ) $objPsLeadFilter->getPsLeadId() . ' ';
		if( true == is_numeric( $objPsLeadFilter->getMinUnitCount() ) ) 				$strWhereSql .= ' AND pl.number_of_units >= ' . ( int ) $objPsLeadFilter->getMinUnitCount() . ' ';
		if( true == is_numeric( $objPsLeadFilter->getMaxUnitCount() ) ) 				$strWhereSql .= ' AND pl.number_of_units <= ' . ( int ) $objPsLeadFilter->getMaxUnitCount() . ' ';

		if( true == is_numeric( $objPsLeadFilter->getDeleteStatus() ) ) {
			switch( $objPsLeadFilter->getDeleteStatus() ) {
				case 0:
					$strWhereSql .= ' AND pl.deleted_by IS NULL ';
					break;

				case 2:
					$strWhereSql .= ' AND pl.deleted_by IS NOT NULL ';
					break;

				default:
					// default case
					break;
			}
		}

		if( true == $boolJoinPersons ) {

			if( 0 < strlen( trim( $objPsLeadFilter->getStateCodes() ) ) ) {
				$strStateCodes = '\'' . implode( '\',\'', explode( ',', $objPsLeadFilter->getStateCodes() ) ) . '\'';
				$strWhereSql .= ' AND p_org.state_code IN ( ' . $strStateCodes . ' ) ';
			}

			if( 0 < strlen( trim( $objPsLeadFilter->getTitle() ) ) ) {
				$boolPersonPrimary = false;
				$strWhereSql .= ' AND p_org.title ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getTitle() ) ) . '%\'';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getNameFirst() ) ) ) {
				$boolPersonPrimary = false;
				$strWhereSql .= ' AND p_org.name_first ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getNameFirst() ) ) . '%\'';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getNameLast() ) ) ) {
				$boolPersonPrimary = false;
				$strWhereSql .= ' AND p_org.name_last ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getNameLast() ) ) . '%\'';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getEmailAddress() ) ) ) {
				$boolPersonPrimary = false;
				$strWhereSql .= ' AND p_org.email_address ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getEmailAddress() ) ) . '%\'';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getCity() ) ) ) {
				$boolPersonPrimary = false;
				$strWhereSql .= ' AND p_org.city ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getCity() ) ) . '%\'';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getPostalCode() ) ) ) {
				$boolPersonPrimary = false;
				$strWhereSql .= ' AND p_org.postal_code ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getPostalCode() ) ) . '%\'';
			}

			if( 0 < strlen( trim( $objPsLeadFilter->getPhoneNumber() ) ) ) {
				$boolPersonPrimary = false;
				$strWhereSql .= ' AND p_org.id IN ( SELECT DISTINCT( person_id ) FROM person_phone_numbers WHERE regexp_replace( phone_number, \'[^0-9]+\', \'\', \'g\' ) LIKE \'%' . addslashes( preg_replace( '/[^0-9]/', '', $objPsLeadFilter->getPhoneNumber() ) ) . '%\')';
			}
		}

		if( true == $boolJoinActions && 0 < strlen( trim( $objPsLeadFilter->getEmployees() ) ) ) {
			$arrobjPsLeads = self::fetchPsLeadsByActionEmployees( $objPsLeadFilter->getEmployees(), $objDatabase );
		}

		if( true == $boolJoinContracts ) {

			if( true == $boolJoinPsLeadDetails ) {
				$strAppend = ' OR pld_org.sales_employee_id IN ( ' . trim( $objPsLeadFilter->getEmployees() ) . ' ) ';
			} else {
				$strAppend = '';
			}

			if( true == $boolJoinActions ) {
				if( valArr( $arrobjPsLeads ) ) {
					$strAppendAction = ' OR pl.id IN ( ' . implode( ',', array_keys( $arrobjPsLeads ) ) . ' ) ';
				} else {
					$strAppendAction = ' ';
				}
			} else {
				$strAppendAction = ' ';
			}

			if( 0 < strlen( trim( $objPsLeadFilter->getContractStatusTypes() ) ) ) 		$strWhereSql .= ' AND c_org.contract_status_type_id IN ( ' . trim( $objPsLeadFilter->getContractStatusTypes() ) . ' ) ';
			if( 0 < strlen( trim( $objPsLeadFilter->getEmployees() ) ) ) 				$strWhereSql .= ' AND ( c_org.sales_employee_id IN ( ' . trim( $objPsLeadFilter->getEmployees() ) . ' ) ' . $strAppend . $strAppendAction . ' ) ';
		}

		if( true == $boolJoinActions ) {
			if( 0 < strlen( trim( $objPsLeadFilter->getActionTypes() ) ) ) 				$strWhereSql .= ' AND a.action_type_id IN ( ' . trim( $objPsLeadFilter->getActionTypes() ) . ' )';
			if( 0 < strlen( trim( $objPsLeadFilter->getActionResults() ) ) ) 			$strWhereSql .= ' AND a.action_result_id IN ( ' . trim( $objPsLeadFilter->getActionResults() ) . ' ) ';
			if( 0 < strlen( trim( $objPsLeadFilter->getNoteText() ) ) ) 				$strWhereSql .= ' AND ( a.notes ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getNoteText() ) ) . '%\' OR pln_org.note ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getNoteText() ) ) . '%\' )';
			if( 0 < strlen( trim( $objPsLeadFilter->getActionStartDate() ) ) ) 			$strWhereSql .= ' AND a.action_datetime IS NOT NULL AND DATE_TRUNC( \'day\', a.action_datetime ) >= \'' . $objPsLeadFilter->getActionStartDate() . '\' ';
			if( 0 < strlen( trim( $objPsLeadFilter->getActionEndDate() ) ) ) 			$strWhereSql .= ' AND a.action_datetime IS NOT NULL AND DATE_TRUNC( \'day\', a.action_datetime ) < \'' . $objPsLeadFilter->getActionEndDate() . '\' ';
			if( 0 < strlen( trim( $objPsLeadFilter->getResultStartDate() ) ) ) 			$strWhereSql .= ' AND a.result_datetime IS NOT NULL AND DATE_TRUNC( \'day\', a.result_datetime ) >= \'' . $objPsLeadFilter->getResultStartDate() . '\' ';
			if( 0 < strlen( trim( $objPsLeadFilter->getResultEndDate() ) ) ) 			$strWhereSql .= ' AND a.result_datetime IS NOT NULL AND DATE_TRUNC( \'day\', a.result_datetime ) < \'' . $objPsLeadFilter->getResultEndDate() . '\' ';
		}

		if( true == $boolJoinContractProducts && 0 < strlen( trim( $objPsLeadFilter->getPsProducts() ) ) ) {
			$strWhereSql .= ' AND cp_org.ps_product_id IN ( ' . trim( $objPsLeadFilter->getPsProducts() ) . ' ) ';
		}

		if( 1 == $objPsLeadFilter->getDoNotUsePsLeadView() && true == $boolPersonPrimary ) {
			$strWhereSql .= ' AND p.is_primary = 1';
		}

		if( true == $boolJoinContracts ) {
			if( 0 < strlen( trim( $objPsLeadFilter->getMissingContractPsProducts() ) ) ) {
				$arrintMissingContractPsProductIds = explode( ',', $objPsLeadFilter->getMissingContractPsProducts() );
				if( true == valArr( $arrintMissingContractPsProductIds ) ) {
					$strWhereSql .= ' AND cpro_org.ps_product_id NOT IN ( ' . implode( ',', $arrintMissingContractPsProductIds ) . ' ) ';
				}
			}

			if( 0 < strlen( trim( $objPsLeadFilter->getContractPsProducts() ) ) ) {
				$arrintContractPsProductIds = explode( ',', $objPsLeadFilter->getContractPsProducts() );
				if( true == valArr( $arrintContractPsProductIds ) ) {
					$strWhereSql .= ' AND cpro_org.ps_product_id IN ( ' . implode( ',', $arrintContractPsProductIds ) . ' ) ';
				}
			}
		}

		return $strSql . '||' . $strWhereSql;
	}

	public static function buildCommonPsLeadSearchSql( $objPsLeadFilter ) {

		$boolJoinPersons 					= false;
		$boolJoinActions 					= false;
		$boolJoinContractProducts 			= false;
		$boolJoinContractProperties 		= false;
		$boolJoinEmployees 					= false;
		$boolJoinContractWatchlists			= false;
		$boolPersonPrimary 					= false;
		$boolJoinContracts 					= false;
		$boolJoinContractEmployees 			= false;
		$boolJoinSalesArea					= false;

		if( 0 < strlen( trim( $objPsLeadFilter->getStateCodes() ) ) ) 						$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getTitle() ) ) ) 							$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getNameFirst() ) ) ) 						$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getNameLast() ) ) ) 						$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getEmailAddress() ) ) ) 					$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getCity() ) ) ) 							$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getPostalCode() ) ) ) 						$boolJoinPersons = true;

		if( 0 < strlen( trim( $objPsLeadFilter->getPersons() ) ) )							$boolJoinPersons = true;

		if( true == $objPsLeadFilter->getJoinPersons() ) 									$boolJoinPersons = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getPhoneNumber() ) ) ) 						$boolJoinPersons = true;

		if( 0 < strlen( trim( $objPsLeadFilter->getContractStatusTypes() ) ) ) 			$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getEmployees() ) ) ) 						$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getEstimatedCloseStartDate() ) ) ) 		$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getEstimatedCloseEndDate() ) ) ) 			$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActualCloseStartDate() ) ) ) 			$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActualCloseEndDate() ) ) ) 				$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getSalesAreas() ) ) ) 						$boolJoinSalesArea = true;

		if( 0 < strlen( trim( $objPsLeadFilter->getActualCloseStartDate() ) ) ) 			$boolJoinContractProperties = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActualCloseEndDate() ) ) ) 				$boolJoinContractProperties = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getMissingContractPsProducts() ) ) ) 		$boolJoinContractProperties = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getContractPsProducts() ) ) ) 				$boolJoinContractProperties = true;

		if( 1 == $objPsLeadFilter->getRequireCloseDate() ) {
			$boolJoinContracts 			= true;
			$boolJoinContractProperties = true;
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getResponsibleEmployees() ) ) )				$boolJoinContracts = $boolJoinContractEmployees = true;

		if( 1 == $objPsLeadFilter->getShowWatchlistGroupsOnly() ) {
			$boolJoinContractWatchlists = true;
			$boolJoinContracts = true;
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getMissingContractPsProducts() ) ) )		$boolJoinContracts = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getContractPsProducts() ) ) )				$boolJoinContracts = true;

		if( 0 < strlen( trim( $objPsLeadFilter->getActionTypes() ) ) ) 						$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActionResults() ) ) ) 					$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActionStartDate() ) ) ) 					$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActionEndDate() ) ) ) 					$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getResultStartDate() ) ) ) 					$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getResultEndDate() ) ) ) 					$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getNoteText() ) ) ) 						$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getEmployees() ) ) ) 						$boolJoinActions = true;
		if( 0 < strlen( trim( $objPsLeadFilter->getActionEmployees() ) ) ) 					$boolJoinActions = true;

		if( 0 < strlen( trim( $objPsLeadFilter->getPsProducts() ) ) ) {
			$boolJoinContracts = true;
			$boolJoinContractProducts = true;
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getJoinEmployees() ) ) ) 					$boolJoinEmployees = true;

		if( true == $objPsLeadFilter->getIsForMassEmailing() ) {
			$boolJoinPersons = true;
		}

		$strSql = ' FROM ';

		if( 1 == $objPsLeadFilter->getDoNotUsePsLeadView() ) {
			if( true == $objPsLeadFilter->getIsForCloserReport() ) {
				$strSql .= ' ps_leads pl LEFT JOIN ps_lead_details pld on( pld.ps_lead_id = pl.id ) ';
			} else {
				$strSql .= ' ps_leads pl JOIN ps_lead_details pld on( pld.ps_lead_id = pl.id )';
			}
			$boolJoinPersons 	= true;
			$boolPersonPrimary 	= true;

		} elseif( 1 == $objPsLeadFilter->getIsCountRequest() ) {
			$strSql .= ' ps_leads pl JOIN ps_lead_details pld on( pld.ps_lead_id = pl.id ) ';
		} else {
			$strSql .= ' ps_leads pl JOIN ps_lead_details pld on( pld.ps_lead_id = pl.id ) JOIN persons primary_persons ON ( primary_persons.ps_lead_id = pl.id AND primary_persons.is_primary = 1 ) ';
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getCompetitors() ) ) ) {
			$strSql .= ' JOIN ps_lead_competitors plc ON ( plc.ps_lead_id = pl.id ) ';
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getPropertyTypes() ) ) ) {
			$strSql .= ' LEFT JOIN ps_lead_property_types plpt ON ( plpt.ps_lead_id = pl.id ) ';
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getPsLeadTypes() ) ) ) {
			$strSql .= ' LEFT JOIN ps_lead_types plt ON ( plt.id = pld.ps_lead_type_id ) ';
		}

		$strSql .= ' LEFT JOIN (
								SELECT
									string_agg ( pt.name::VARCHAR, \', \' ) AS ps_lead_property_types,
									pl.id
								FROM
								ps_leads pl
								JOIN ps_lead_property_types plpt ON ( pl.id = plpt.ps_lead_id )
								JOIN property_types pt ON ( plpt.property_type_id = pt.id )
								WHERE
									pl.deleted_on IS NULL
									AND pl.deduped_on IS NULL
								GROUP BY
									pl.id
					) AS subSql ON ( pl.id = subSql.id )';

		$strIsBlockEmails = '';
		$strJoinCondition = '';

		if( true == $objPsLeadFilter->getIsForMassEmailing() && true == $boolJoinPersons ) {
			$strIsBlockEmails .= ' AND ( p.email_address IS NOT NULL AND p.email_address LIKE \'%@%\' ) ';
		}

		if( true == $objPsLeadFilter->getIsForCloserReport() ) {
			$strSql .= 'LEFT JOIN persons p ON ( p.ps_lead_id = pl.id ) ';
		} elseif( true == $boolJoinPersons ) {
			$strSql .= 'JOIN persons p ON ( p.ps_lead_id = pl.id AND p.is_primary=1 ) ' . $strIsBlockEmails;
		}

		if( true == $boolJoinSalesArea && true == $boolJoinPersons ) {
			$strSql .= ' JOIN sales_area_locations sal ON p.state_code = sal.state_code JOIN sales_areas sa ON ( sa.id = sal.sales_area_id )';
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getEmployees() ) ) )  	$strJoinCondition .= ' AND a.employee_id IN ( ' . trim( $objPsLeadFilter->getEmployees() ) . ' ) ';

		if( 0 < strlen( trim( $objPsLeadFilter->getNoteText() ) ) ) {
			$strJoinCondition .= ' AND a.notes ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getNoteText() ) ) . '%\' ';
		}

		if( true == $boolJoinActions ) 			$strSql .= ' JOIN actions a ON (  pl.id = a.ps_lead_id ' . $strJoinCondition . ') ';

		if( true == $boolJoinEmployees ) 			$strSql .= ' LEFT JOIN employees e ON
																		 ( ( pld.sales_employee_id IS NOT NULL  AND pld.sales_employee_id = e.id ) )';

		$strSql .= ' LEFT JOIN clients mc ON ( mc.ps_lead_id = pl.id ) ';

		if( true == $boolJoinContracts ) 			$strSql .= ' LEFT JOIN contracts c ON ( c.ps_lead_id = pl.id AND c.contract_status_type_id <> ' . CContractStatusType::CONTRACT_TERMINATED . ' ) ';

		if( true == $boolJoinContractProperties ) {
			$strSql .= 'LEFT JOIN contract_properties cp ON ( cp.contract_id = c.id AND cp.renewal_contract_property_id IS NULL AND cp.is_last_contract_record = TRUE AND ( cp.termination_date IS NULL OR cp.termination_date > NOW() ) ) ';
		}

		if( true == $boolJoinContractProducts ) 	$strSql .= ' LEFT JOIN contract_products cpp ON ( cpp.contract_id = c.id ) ';

		if( true == $boolJoinContractWatchlists ) 	$strSql .= ' JOIN contract_watchlists cw ON ( cw.contract_id = c.id AND cw.employee_id = ' . ( int ) $objPsLeadFilter->getCurrentEmployeeId() . ' ) ';

		if( 0 < strlen( trim( $objPsLeadFilter->getResponsibleEmployees() ) ) ) {
			$strSql .= ' JOIN contract_employees ce ON ( ce.contract_id = c.id AND ce.contract_employee_type_id = ' . CContractEmployeeType::RESPONSIBLE . ' AND ce.employee_id IN( ' . $objPsLeadFilter->getResponsibleEmployees() . ' ) ) ';
		}

		$strSql .= ' WHERE 1 = 1 ';

		if( 0 < strlen( $objPsLeadFilter->getPsLeadIds() ) )  							$strSql .= ' AND pl.id IN ( ' . $objPsLeadFilter->getPsLeadIds() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getPersons() ) ) )						$strSql .= ' AND p.id IN ( ' . $objPsLeadFilter->getPersons() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getPersons() ) ) ) 						$strSql .= ' AND p.id IN ( ' . $objPsLeadFilter->getPersons() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getCompanyStatusTypes() ) ) )			$strSql .= ' AND pl.company_status_type_id IN ( ' . $objPsLeadFilter->getCompanyStatusTypes() . ' )';
		if( 0 < strlen( trim( $objPsLeadFilter->getPsLeadOrigins() ) ) ) 				$strSql .= ' AND pl.ps_lead_origin_id IN ( ' . $objPsLeadFilter->getPsLeadOrigins() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getPsLeadSources() ) ) ) 				$strSql .= ' AND pl.ps_lead_source_id IN ( ' . $objPsLeadFilter->getPsLeadSources() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getSalesEmployees() ) ) ) 				$strSql .= ' AND pld.sales_employee_id IN ( ' . $objPsLeadFilter->getSalesEmployees() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getPsLeadEvents() ) ) ) 				$strSql .= ' AND pl.ps_lead_event_id IN ( ' . $objPsLeadFilter->getPsLeadEvents() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getCompetitors() ) ) )					$strSql .= ' AND plc.competitor_id IN ( ' . $objPsLeadFilter->getCompetitors() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getPropertyTypes() ) ) ) 				$strSql .= ' AND plpt.property_type_id IN ( ' . $objPsLeadFilter->getPropertyTypes() . ' ) ';
		if( 0 < strlen( trim( $objPsLeadFilter->getPsLeadTypes() ) ) ) 					$strSql .= ' AND pld.ps_lead_type_id IN ( ' . $objPsLeadFilter->getPsLeadTypes() . ' ) ';

		if( 0 < strlen( trim( $objPsLeadFilter->getCompanyName() ) ) ) {
			$boolPersonPrimary = false;
			$strSql .= ' AND pl.company_name ILIKE \'%' . addslashes( $objPsLeadFilter->getCompanyName() ) . '%\' ';
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getNoteText() ) ) ) 					$strSql .= ' AND ( a.id IS NOT NULL )';
		if( true == $objPsLeadFilter->getShowNullPsEmployeeIdOnly() ) 					$strSql .= ' AND pld.sales_employee_id IS NULL ';
		if( 0 < strlen( trim( $objPsLeadFilter->getQuickSearch() ) ) ) 					$strSql .= ' AND ( pl.id = ' . ( int ) $objPsLeadFilter->getQuickSearch() . ' OR pl.company_name ILIKE \'%' . addslashes( $objPsLeadFilter->getQuickSearch() ) . '%\' )';
		if( 0 < strlen( trim( $objPsLeadFilter->getRequestDatetime() ) ) ) 				$strSql .= ' AND DATE_TRUNC( \'month\', pl.request_datetime ) = \'' . $objPsLeadFilter->getRequestDatetime() . '\' ';

		if( 0 < strlen( trim( $objPsLeadFilter->getStartRequestDate() ) ) ) 			$strSql .= ' AND DATE_TRUNC ( \'day\', pl.request_datetime ) >= \'' . $objPsLeadFilter->getStartRequestDate() . '\' ';
		if( 0 < strlen( trim( $objPsLeadFilter->getEndRequestDate() ) ) ) 				$strSql .= ' AND DATE_TRUNC ( \'day\', pl.request_datetime ) <= \'' . $objPsLeadFilter->getEndRequestDate() . '\' ';

		if( 0 < strlen( trim( $objPsLeadFilter->getEstimatedCloseStartDate() ) ) ) 		$strSql .= ' AND c.anticipated_close_date IS NOT NULL AND c.anticipated_close_date >= \'' . $objPsLeadFilter->getEstimatedCloseStartDate() . '\' ';
		if( 0 < strlen( trim( $objPsLeadFilter->getEstimatedCloseEndDate() ) ) ) 		$strSql .= ' AND c.anticipated_close_date IS NOT NULL AND c.anticipated_close_date <= \'' . $objPsLeadFilter->getEstimatedCloseEndDate() . '\' ';
		if( 0 < strlen( trim( $objPsLeadFilter->getActualCloseStartDate() ) ) ) 		$strSql .= ' AND cp.close_date IS NOT NULL AND cp.close_date >= \'' . $objPsLeadFilter->getActualCloseStartDate() . '\' ';
		if( 0 < strlen( trim( $objPsLeadFilter->getActualCloseEndDate() ) ) ) 			$strSql .= ' AND cp.close_date IS NOT NULL AND cp.close_date <= \'' . $objPsLeadFilter->getActualCloseEndDate() . '\' ';
		if( 1 == $objPsLeadFilter->getRequireCloseDate() ) 								$strSql .= ' AND ( cp.close_date IS NOT NULL OR c.anticipated_close_date IS NOT NULL ) ';

		if( true == is_numeric( $objPsLeadFilter->getPsLeadId() ) ) 					$strSql .= ' AND pl.id = ' . ( int ) $objPsLeadFilter->getPsLeadId() . ' ';
		if( true == is_numeric( $objPsLeadFilter->getMinUnitCount() ) ) 				$strSql .= ' AND pl.number_of_units >= ' . ( int ) $objPsLeadFilter->getMinUnitCount() . ' ';
		if( true == is_numeric( $objPsLeadFilter->getMaxUnitCount() ) ) 				$strSql .= ' AND pl.number_of_units <= ' . ( int ) $objPsLeadFilter->getMaxUnitCount() . ' ';
		if( true == $boolJoinSalesArea && true == $boolJoinPersons ) 					$strSql .= ' AND sa.id IN ( ' . $objPsLeadFilter->getSalesAreas() . ' ) ';

		if( true == is_numeric( $objPsLeadFilter->getDeleteStatus() ) ) {
			switch( $objPsLeadFilter->getDeleteStatus() ) {
				case 0:
					$strSql .= ' AND pl.deleted_by IS NULL ';
					break;

				case 2:
					$strSql .= ' AND pl.deleted_by IS NOT NULL ';
					break;

				default:
					// default case
					break;
			}
		}

		if( true == $boolJoinPersons ) {

			if( 0 < strlen( trim( $objPsLeadFilter->getStateCodes() ) ) ) {
				$strStateCodes = '\'' . implode( '\',\'', explode( ',', $objPsLeadFilter->getStateCodes() ) ) . '\'';
				$strSql .= ' AND pld.state_code IN ( ' . $strStateCodes . ' ) ';
			}

			if( 0 < strlen( trim( $objPsLeadFilter->getTitle() ) ) ) {
				$boolPersonPrimary = false;
				$strSql .= ' AND p.title ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getTitle() ) ) . '%\'';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getNameFirst() ) ) ) {
				$boolPersonPrimary = false;
				$strSql .= ' AND p.name_first ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getNameFirst() ) ) . '%\'';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getNameLast() ) ) ) {
				$boolPersonPrimary = false;
				$strSql .= ' AND p.name_last ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getNameLast() ) ) . '%\'';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getEmailAddress() ) ) ) {
				$boolPersonPrimary = false;
				$strSql .= ' AND p.email_address ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getEmailAddress() ) ) . '%\'';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getCity() ) ) ) {
				$boolPersonPrimary = false;
				$strSql .= ' AND pld.city ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getCity() ) ) . '%\'';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getPostalCode() ) ) ) {
				$boolPersonPrimary = false;
				$strSql .= ' AND pld.postal_code ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getPostalCode() ) ) . '%\'';
			}

			if( 0 < strlen( trim( $objPsLeadFilter->getPhoneNumber() ) ) ) {
				$boolPersonPrimary = false;
				$strSql .= ' AND p.id IN ( SELECT distinct(person_id) FROM person_phone_numbers WHERE regexp_replace( phone_number, \'[^0-9]+\', \'\', \'g\' ) LIKE \'%' . addslashes( preg_replace( '/[^0-9]/', '', $objPsLeadFilter->getPhoneNumber() ) ) . '%\')';
			}
		}

		if( true == $boolJoinContracts ) {
			if( 0 < strlen( trim( $objPsLeadFilter->getContractStatusTypes() ) ) ) 		$strSql .= ' AND c.contract_status_type_id IN ( ' . trim( $objPsLeadFilter->getContractStatusTypes() ) . ' ) ';
			if( 0 < strlen( trim( $objPsLeadFilter->getEmployees() ) ) ) 				$strSql .= ' AND ( c.sales_employee_id IN ( ' . trim( $objPsLeadFilter->getEmployees() ) . ' ) OR  pld.sales_employee_id IN ( ' . trim( $objPsLeadFilter->getEmployees() ) . ' ) OR a.id IS NOT NULL ) ';
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getResponsibleEmployees() ) ) ) {
			$strSql .= ' AND (ce.contract_employee_type_id = ' . CContractEmployeeType::RESPONSIBLE . ' AND ce.employee_id IN( ' . $objPsLeadFilter->getResponsibleEmployees() . ') )';
		}

		if( 0 < strlen( trim( $objPsLeadFilter->getSupportEmployees() ) ) ) 			$strSql .= ' AND pld.support_employee_id IN ( ' . $objPsLeadFilter->getSupportEmployees() . ' ) ';

		if( true == $boolJoinActions ) {
			if( 0 < strlen( trim( $objPsLeadFilter->getActionEmployees() ) ) ) 			$strSql .= ' AND a.employee_id IN ( ' . trim( $objPsLeadFilter->getActionEmployees() ) . ' )';
			if( 0 < strlen( trim( $objPsLeadFilter->getActionTypes() ) ) ) 				$strSql .= ' AND a.action_type_id IN ( ' . trim( $objPsLeadFilter->getActionTypes() ) . ' )';
			if( 0 < strlen( trim( $objPsLeadFilter->getActionResults() ) ) ) 			$strSql .= ' AND a.action_result_id IN ( ' . trim( $objPsLeadFilter->getActionResults() ) . ' ) ';
			if( 0 < strlen( trim( $objPsLeadFilter->getActionStartDate() ) ) ) 			$strSql .= ' AND a.action_datetime IS NOT NULL AND DATE_TRUNC ( \'day\', a.action_datetime ) >= \'' . $objPsLeadFilter->getActionStartDate() . '\' ';
			if( 0 < strlen( trim( $objPsLeadFilter->getActionEndDate() ) ) ) 			$strSql .= ' AND a.action_datetime IS NOT NULL AND DATE_TRUNC ( \'day\', a.action_datetime ) <= \'' . $objPsLeadFilter->getActionEndDate() . '\' ';
			if( 0 < strlen( trim( $objPsLeadFilter->getResultStartDate() ) ) ) 			$strSql .= ' AND a.action_datetime IS NOT NULL AND DATE_TRUNC( \'day\', a.action_datetime ) >= \'' . $objPsLeadFilter->getResultStartDate() . '\' ';
			if( 0 < strlen( trim( $objPsLeadFilter->getResultEndDate() ) ) ) 			$strSql .= ' AND a.action_datetime IS NOT NULL AND DATE_TRUNC( \'day\', a.action_datetime ) <=\'' . $objPsLeadFilter->getResultEndDate() . '\' ';
		}

		if( true == $boolJoinContractProducts && 0 < strlen( trim( $objPsLeadFilter->getPsProducts() ) ) ) {
			$strSql .= ' AND cpp.ps_product_id IN ( ' . trim( $objPsLeadFilter->getPsProducts() ) . ' ) ';
		}

		if( true == $boolJoinContracts ) {
			if( 0 < strlen( trim( $objPsLeadFilter->getMissingContractPsProducts() ) ) ) {
				$arrintMissingContractPsProductIds = explode( ',', $objPsLeadFilter->getMissingContractPsProducts() );
				if( true == valArr( $arrintMissingContractPsProductIds ) ) {
					$strSql .= ' AND cp.ps_product_id NOT IN ( ' . implode( ',', $arrintMissingContractPsProductIds ) . ' ) ';
				}
			}

			if( 0 < strlen( trim( $objPsLeadFilter->getContractPsProducts() ) ) ) {
				$arrintContractPsProductIds = explode( ',', $objPsLeadFilter->getContractPsProducts() );
				if( true == valArr( $arrintContractPsProductIds ) ) {
					$strSql .= ' AND cp.ps_product_id IN ( ' . implode( ',', $arrintContractPsProductIds ) . ' ) ';
				}
			}
		}

		return $strSql;
	}

 	public static function fetchPsLeadsByCids( $arrintCids, $objDatabase ) {

 		if( false == valArr( $arrintCids ) ) return NULL;

 		$arrintCids = array_unique( array_filter( $arrintCids ) );

 		if( false == valArr( $arrintCids ) ) return NULL;

	 	$strSql = 'SELECT
	 					pl.*,
	 					pld.sales_employee_id,
	 					mc.id as cid,
	 					mc.company_status_type_id
	 				FROM
	 					ps_leads pl
	 					JOIN ps_lead_details pld ON( pld.ps_lead_id  = pl.id )
	 					LEFT JOIN clients mc on ( pl.id = mc.ps_lead_id )
	 				WHERE
	 					mc.id IN (' . implode( ',', $arrintCids ) . ')
	 					AND pl.deleted_on IS NULL';

	 	return self::fetchPsLeads( $strSql, $objDatabase );
	}

	public static function fetchSimplePsLeadsByCids( $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) return NULL;

		$arrintCids = array_unique( array_filter( $arrintCids ) );

		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						pl.id,
						mc.id as cid
					FROM
						ps_leads pl
						LEFT JOIN clients mc ON ( pl.id = mc.ps_lead_id )
					WHERE
						mc.id IN (' . implode( ',', $arrintCids ) . ')
						AND pl.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPsLeadsByIds( $arrintPsLeadIds, $objDatabase ) {
		return self::fetchPsLeads( sprintf( 'SELECT pl.*, mc.id as cid FROM ps_leads pl LEFT JOIN clients mc ON ( pl.id = mc.ps_lead_id ) WHERE pl.id IN ( %s )', implode( ',', $arrintPsLeadIds ) ), $objDatabase );
	}

	public static function fetchPsLeadByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT pl.*, mc.id as cid FROM ps_leads pl LEFT JOIN clients mc ON ( pl.id = mc.ps_lead_id ) WHERE mc.id IN (' . ( int ) $intCid . ') LIMIT 1';

		return self::fetchPsLead( $strSql, $objDatabase );
	}

	public static function fetchActivePsLeadByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT pl.*, mc.id as cid FROM ps_leads pl JOIN clients mc ON ( pl.id = mc.ps_lead_id ) WHERE mc.id IN (' . ( int ) $intCid . ') AND pl.deleted_on IS NULL LIMIT 1';
		return self::fetchPsLead( $strSql, $objDatabase );
	}

	public static function fetchUnmergedPossibleDuplicatesCountByPsLead( $objPsLead, $objDatabase, $boolExcludeOrigin = false ) {

		$strSql = 'SELECT
						COUNT ( DISTINCT Pl.id )
					FROM
						ps_lead_duplicates pld
						JOIN ps_leads pl ON ( pl.id = pld.ps_lead_id2 )
						JOIN persons p ON ( p.ps_lead_id = pld.ps_lead_id2 AND p.is_primary = 1 )
					WHERE
						pl.deleted_on IS NULL
						AND pl.deduped_on IS NULL
						AND pld.processed_on IS NULL
						AND pld.ps_lead_id1 = ' . ( int ) $objPsLead->getId();

		if( true == $boolExcludeOrigin ) {
			$strSql = $strSql . ' AND pl.ps_lead_origin_id NOT IN ( ' . CPsLeadOrigin::VERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::NMHC . ' , ' . CPsLeadOrigin::UNVERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::MIDVERIFIED_INDEXED_DOMAIN . ' ) ';
		}

		$arrstrResults = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrstrResults[0]['count'] ) ) {
			return $arrstrResults[0]['count'];
		}

		return 0;
	}

	public static function fetchUnmergedPossibleDuplicatesCounts( $arrintPsLeads, $objDatabase, $boolIsMerge = false ) {

		if( false == $boolIsMerge ) {
			$strSql = ' SELECT
							sub.ps_lead_id1 AS ps_lead_id1,
							count ( sub.count1 )
						FROM
						(
							SELECT
								pld.ps_lead_id1,
								count ( pld.ps_lead_id1 ) as count1
							FROM
								ps_lead_duplicates pld
								JOIN ps_leads pl ON ( pl.id = pld.ps_lead_id2 )
								JOIN persons p ON ( p.ps_lead_id = pld.ps_lead_id2 AND p.is_primary = 1 )
							WHERE
								pl.deleted_on IS NULL
								AND pl.deduped_on IS NULL
								AND pld.processed_on IS NULL
								AND pld.ps_lead_id1 IN ( ' . implode( ',', $arrintPsLeads ) . ' ) ';
		} else {
			$strSql = ' SELECT
							sub.ps_lead_id1 AS ps_lead_id1,
							count ( sub.count1 )
						FROM
						(
							SELECT
								pld.ps_lead_id1,
								count ( pld.ps_lead_id1 ) as count1
							FROM
								ps_lead_duplicates pld
								JOIN ps_leads pl ON ( pl.id = pld.ps_lead_id2 )
								JOIN persons p ON ( p.ps_lead_id = pld.ps_lead_id2 AND p.is_primary = 1 )
								LEFT JOIN clients c on ( c.ps_lead_id = pld.ps_lead_id2 )
							WHERE
								pl.deleted_on IS NULL
								AND pl.deduped_on IS NULL
								AND pld.processed_on IS NULL
								AND pld.ignored_on IS NULL
								AND c.id IS NULL
								AND pld.ps_lead_id1 IN ( ' . implode( ',', $arrintPsLeads ) . ' ) ';
		}

		$strSql = $strSql . ' AND pl.ps_lead_origin_id NOT IN ( ' . CPsLeadOrigin::VERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::NMHC . ' , ' . CPsLeadOrigin::UNVERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::MIDVERIFIED_INDEXED_DOMAIN . ' ) ';

		$strSql .= ' GROUP BY pld.ps_lead_id1, pld.ps_lead_id2 ) sub
							  GROUP BY
								 ps_lead_id1 ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPsLeadById( $intId, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (pl.id) pl.*,
						mc.id AS cid,
						p.id AS person_id,
						p.name_first,
						p.name_last,
						p.title,
						p.email_address,
						p.address_line1,
						p.address_line2,
						p.city,
						p.state_code,
						p.postal_code
					FROM
						ps_leads pl
						LEFT JOIN clients mc ON ( pl.id = mc.ps_lead_id )
						LEFT JOIN persons p ON ( pl.id = p.ps_lead_id AND p.is_primary=1 AND p.is_disabled = 0 )
					WHERE
						pl.id =' . ( int ) $intId;

		return self::fetchPsLead( $strSql, $objDatabase );
	}

	public static function fetchCustomPsLeadsBySalesEmployeeIds( $arrstrCompanyFilter, $objDatabase, $boolTerminatedProspect = false ) {

		$intOffset 			= ( true == isset( $arrstrCompanyFilter['page_no'] ) && true == isset( $arrstrCompanyFilter['page_size'] ) ) ? $arrstrCompanyFilter['page_size'] * ( $arrstrCompanyFilter['page_no'] - 1 ) : 0;
		$intLimit 			= ( true == isset( $arrstrCompanyFilter['page_size'] ) ) ? $arrstrCompanyFilter['page_size'] : '';
		$strOrderByField 	= ' company_name';
		$strOrderByType		= ' ASC';
		$strCondition 		= '';
		$strFromClause		= '';
		$strWhereCondition	= '';

		$strWhereCompanyTypes = ' AND pl.company_status_type_id IN ( ' . implode( ',', CCompanyStatusType::$c_arrintActiveLeadCompanyStatusTypeIds ) . ' ) ';
		if( true == $boolTerminatedProspect ) {
			$strWhereCompanyTypes = ' AND (
											pl.company_status_type_id IN ( ' . implode( ',', CCompanyStatusType::$c_arrintActiveLeadCompanyStatusTypeIds ) . ' )
											OR ( pl.company_status_type_id = ' . CCompanyStatusType::TERMINATED . ' and pl.needs_follow_up = 1 )
										)';
		}

		if( false == is_null( $arrstrCompanyFilter['closer_employee_ids'] ) || false == is_null( $arrstrCompanyFilter['dialer_employee_ids'] ) ) {
			$strWhereCondition .= ' AND ( ';
		}

		if( false == is_null( $arrstrCompanyFilter['closer_employee_ids'] ) ) {
			$strWhereCondition .= ' pld.sales_employee_id IN ( ' . $arrstrCompanyFilter['closer_employee_ids'] . ' )';
		}

		if( false == is_null( $arrstrCompanyFilter['closer_employee_ids'] ) && false == is_null( $arrstrCompanyFilter['dialer_employee_ids'] ) ) {
			$strWhereCondition .= ' OR ';
		}

		if( false == is_null( $arrstrCompanyFilter['closer_employee_ids'] ) || false == is_null( $arrstrCompanyFilter['dialer_employee_ids'] ) ) {
			$strWhereCondition .= ' ) ';
		}

		if( true == valArr( $arrstrCompanyFilter ) ) {
			if( true == isset( $arrstrCompanyFilter['order_by_field'] ) ) {

				switch( $arrstrCompanyFilter['order_by_field'] ) {

					case NULL:
					case 'company_name':
						$strOrderByField = ' company_name';
						break;

					case 'opportunity_count':
						$strOrderByField = ' opportunity_count';
						break;

					case 'unit_count':
						$strOrderByField = ' unit_count';
						break;

					case 'property_count':
						$strOrderByField = ' property_count';
						break;

					case 'sales_closer':
						$strOrderByField = ' sales_employee_id';
						break;

					default:
						$strOrderByField = ' company_name';
						break;
				}
			}

			if( true == isset( $arrstrCompanyFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrCompanyFilter['order_by_type'] ) ? ' ASC NULLS FIRST' : ' DESC NULLS LAST';
			}
		}

		if( true == isset( $arrstrCompanyFilter['quick_search'] ) && true == valStr( $arrstrCompanyFilter['quick_search'] ) ) {

			$strCondition .= ' AND ';
			if( true == is_numeric( trim( $arrstrCompanyFilter['quick_search'] ) ) ) {
				$strCondition .= '( pl.id = ' . trim( addslashes( $arrstrCompanyFilter['quick_search'] ) ) . ' OR pl.company_name ILIKE \'%' . trim( addslashes( $arrstrCompanyFilter['quick_search'] ) ) . '%\') ';
			} else {
				$strCondition .= ' pl.company_name ILIKE \'%' . trim( addslashes( $arrstrCompanyFilter['quick_search'] ) ) . '%\' ';
			}
		}

		$strOpportunityCountSql = '';
		if( true == isset( $arrstrCompanyFilter['open_opportunity_count'] ) && 0 == $arrstrCompanyFilter['open_opportunity_count'] ) {
			$strCondition	.= ' AND c.id IS NULL ';
			$strFromClause	.= ' LEFT JOIN contracts c ON ( pl.id = c.ps_lead_id AND c.contract_status_type_id IN ( ' . implode( ',', CContractStatusType::$c_arrintActiveContractStatusTypeIds ) . ' ) )';
		} elseif( true == isset( $arrstrCompanyFilter['open_opportunity_count'] ) && 1 == $arrstrCompanyFilter['open_opportunity_count'] ) {
			$strOpportunityCountSql = ',(
											SELECT
												count(c.id)
											FROM
												contracts c
											WHERE
												c.ps_lead_id = pl.id
												AND c.contract_status_type_id IN ( ' . implode( ',', CContractStatusType::$c_arrintActiveContractStatusTypeIds ) . ' )
										) AS opportunity_count';
		}

		$strSql = ' SELECT
						*
					 FROM
						(
							SELECT
								pl.id,
								pl.company_name,
								mc.id as cid,
								cst.name as status,
								pl.number_of_units as unit_count,
								pl.property_count as property_count,
								pld.primary_website_urls as primary_website_urls,
								pld.sales_employee_id,
								pld.city,
								s.name as state ' . $strOpportunityCountSql . '
							FROM
								ps_leads pl
								LEFT JOIN clients mc ON ( pl.id = mc.ps_lead_id )
								JOIN company_status_types cst ON ( cst.id = pl.company_status_type_id )
								LEFT JOIN ps_lead_details pld ON ( pl.id = pld.ps_lead_id )
								LEFT JOIN persons p ON ( p.ps_lead_id = pl.id )
								LEFT JOIN states s ON ( pld.state_code = s.code )
								' . $strFromClause . '
							WHERE
								pl.deleted_on IS NULL
								AND pl.deduped_on IS NULL';

					if( true == isset( $arrstrCompanyFilter['company_status_types'] ) ) {
						$strSql .= ' AND pl.company_status_type_id IN ( ' . $arrstrCompanyFilter['company_status_types'] . ' ) ';
					} else {
						$strSql .= $strWhereCompanyTypes;
					}

					if( true == isset( $arrstrCompanyFilter['ps_lead_origins'] ) ) {
						$strSql .= ' AND pl.ps_lead_origin_id IN ( ' . $arrstrCompanyFilter['ps_lead_origins'] . ' ) ';
					} else {
						$strSql .= '';
					}

					$strSql .= $strWhereCondition;
					$strSql .= $strCondition . '
							GROUP BY
								pl.company_name,
								pl.id,
								mc.id,
								cst.name,
								pl.number_of_units,
								pl.property_count,
								pld.primary_website_urls,
								pld.sales_employee_id,
								pld.city,
								s.name
							ORDER BY pl.company_name DESC
				) ps_leads
				ORDER BY ' . $strOrderByField . $strOrderByType . '
				OFFSET ' . ( int ) $intOffset;

		if( false == is_null( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPsLeadsCountBySalesEmployeeIds( $arrstrCompanyFilter, $objDatabase, $boolTerminatedProspect = false ) {

		$strCondition 		= '';
		$strWhereCondition	= '';

		$strWhereCompanyTypes = ' AND pl.company_status_type_id IN ( ' . implode( ',', CCompanyStatusType::$c_arrintActiveLeadCompanyStatusTypeIds ) . ' ) ';
		if( true == $boolTerminatedProspect ) {
			$strWhereCompanyTypes = ' AND (
											pl.company_status_type_id IN ( ' . implode( ',', CCompanyStatusType::$c_arrintActiveLeadCompanyStatusTypeIds ) . ' )
											OR ( pl.company_status_type_id = ' . CCompanyStatusType::TERMINATED . ' and pl.needs_follow_up = 1 )
										)';
		}

		if( false == is_null( $arrstrCompanyFilter['closer_employee_ids'] ) || false == is_null( $arrstrCompanyFilter['dialer_employee_ids'] ) ) {
			$strWhereCondition .= ' AND ( ';
		}

		if( false == is_null( $arrstrCompanyFilter['closer_employee_ids'] ) ) {
			$strWhereCondition .= ' pld.sales_employee_id IN ( ' . $arrstrCompanyFilter['closer_employee_ids'] . ' )';
		}

		if( false == is_null( $arrstrCompanyFilter['closer_employee_ids'] ) && false == is_null( $arrstrCompanyFilter['dialer_employee_ids'] ) ) {
			$strWhereCondition .= ' OR ';
		}

		if( false == is_null( $arrstrCompanyFilter['closer_employee_ids'] ) || false == is_null( $arrstrCompanyFilter['dialer_employee_ids'] ) ) {
			$strWhereCondition .= ' ) ';
		}

		if( true == isset( $arrstrCompanyFilter['quick_search'] ) && true == valStr( $arrstrCompanyFilter['quick_search'] ) ) {
			$strCondition .= ' AND ';
			if( true == is_numeric( trim( $arrstrCompanyFilter['quick_search'] ) ) ) {
				$strCondition .= '( pl.id = ' . trim( addslashes( $arrstrCompanyFilter['quick_search'] ) ) . ' OR pl.company_name ILIKE \'%' . trim( addslashes( $arrstrCompanyFilter['quick_search'] ) ) . '%\') ';
			} else {
				$strCondition .= ' pl.company_name ILIKE \'%' . trim( addslashes( $arrstrCompanyFilter['quick_search'] ) ) . '%\' ';
			}
		}

		$strSql = 'SELECT
						COUNT( DISTINCT pl.id )
					FROM
						ps_leads pl
						LEFT JOIN clients mc ON ( pl.id = mc.ps_lead_id )
						LEFT JOIN ps_lead_details pld ON ( pl.id = pld.ps_lead_id )
						LEFT JOIN company_addresses ca ON ( mc.id = ca.cid )
						LEFT JOIN states s ON ( ca.state_code = s.code )
					WHERE
						pl.deleted_by IS NULL
						AND pl.deleted_on IS NULL
						AND pl.deduped_on IS NULL ';

		if( true == isset( $arrstrCompanyFilter['company_status_types'] ) ) {
			$strSql .= ' AND pl.company_status_type_id IN ( ' . $arrstrCompanyFilter['company_status_types'] . ' ) ';
		} else {
			$strSql .= $strWhereCompanyTypes;
		}

		if( true == isset( $arrstrCompanyFilter['ps_lead_origins'] ) ) {
			$strSql .= ' AND pl.ps_lead_origin_id IN ( ' . $arrstrCompanyFilter['ps_lead_origins'] . ' ) ';
		} else {
			$strSql .= '';
		}

		$strSql .= $strWhereCondition . $strCondition;

		$arrintPsLeadsCount = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintPsLeadsCount ) ) return 0;

		return $arrintPsLeadsCount[0]['count'];

	}

	public static function fetchActivePsLeadsByCid( $intCid, $objDatabase ) {

		if( true == empty( $intCid ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						pl.*,mc.id as cid
					FROM
						ps_leads
					JOIN
						clients mc
					ON ( mc.ps_lead_id = pl.id )
					WHERE
						pl.deleted_by IS NULL
						AND mc.id = ' . ( int ) $intCid;

		return self::fetchPsLeads( $strSql, $objDatabase );
	}

	public static function fetchPsLeadIdsByCids( $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						pl.id,
						mc.id as cid
					FROM
						ps_leads pl
						LEFT JOIN clients mc
						ON ( pl.id = mc.ps_lead_id )
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
						AND pl.deleted_on IS NULL';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixResponseData ) ) return NULL;

		return rekeyArray( 'cid', $arrmixResponseData );
	}

	public static function fetchPsLeadByIds( $arrintIds, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					DISTINCT ON (pl.id) pl.*,
						mc.id as cid,
						p.name_first,
						p.name_last,
						p.title,
						p.id AS person_id,
						p.email_address,
						p.address_line1,
						p.address_line2,
						p.city,
						p.state_code,
						p.postal_code,
						p.ps_lead_id
					FROM
						ps_leads pl
						LEFT JOIN clients mc ON ( pl.id = mc.ps_lead_id )
						LEFT JOIN persons p ON ( pl.id = p.ps_lead_id )
					WHERE
						pl.id IN ( ' . implode( ',', $arrintIds ) . ' ) ';

		return self::fetchPsLeads( $strSql, $objDatabase );
	}

	public static function fetchActivityCountByLeadIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

				$strSql = 'SELECT
								lead_id,
								count ( subSql.c1 )
							FROM
							(
								SELECT
									pl.id AS lead_id,
									count ( * ) AS c1
								FROM
									ps_leads pl
									JOIN actions a ON ( pl.id = a.ps_lead_id )
								WHERE
									pl.id IN ( ' . implode( ',', $arrintIds ) . ' )
									AND a.contract_id IS NOT NULL
								GROUP BY
								pl.id
							UNION ALL
								(
									SELECT
										pl.id AS lead_id,
										count ( * ) AS c2
									FROM
										ps_leads pl
										JOIN actions a ON ( pl.id = a.ps_lead_id )
									WHERE
										pl.id IN ( ' . implode( ',', $arrintIds ) . ' )
										AND a.contract_id IS NOT NULL
									GROUP BY
									pl.id
								)
							) AS subSql
							GROUP BY
								lead_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPsLeadByIds( $arrintIds, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					pl.*, mc.id as cid
				FROM
					ps_leads pl
					LEFT JOIN clients mc ON ( mc.ps_lead_id = pl.id )
				WHERE
					pl.id IN ( ' . implode( ',', $arrintIds ) . ' ) ';

		return self::fetchPsLeads( $strSql, $objDatabase );
	}

	public static function fetchAllQuickSearchPsLeads( $strSearchData, $objDatabase, $boolIsGlobalSearch = false ) {

		if( !valStr( $strSearchData ) ) {
			return NULL;
		}

		$strSearchCondition				= '';
		$strSearchData					= addslashes( trim( $strSearchData ) );

		if( 1 < strlen( $strSearchData ) ) {
			$strSearchCondition .= ' AND ( trim( to_char( pl.id, \'99999999\' ) ) ILIKE \'' . $strSearchData . '%\' OR pl.company_name ILIKE \'%' . $strSearchData . '%\' OR to_tsvector( \'english\', pl.company_name ) @@ plainto_tsquery( \'english\', \'' . $strSearchData . '\' ) )';
		}

		if( $boolIsGlobalSearch ) {
			$strSearchCondition .= ' AND pl.company_status_type_id NOT IN ( ' . CCompanyStatusType::TERMINATED . ',' . CCompanyStatusType::CANCELLED . ')';
		} else {
			$strSearchCondition .= ' AND pl.company_status_type_id <> ' . CCompanyStatusType::TERMINATED;
		}

		$strSql = 'SELECT
						pl.id,
						mc.id as company_id,
						pl.company_name
					FROM
						ps_leads AS pl
						LEFT JOIN clients AS mc ON ( pl.id = mc.ps_lead_id )
					WHERE
						pl.deleted_by IS NULL
						' . $strSearchCondition . '
					ORDER BY
						pl.request_datetime DESC,
						pl.id
					LIMIT
						15';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPsLeadDetailsAndContractDetailsByCidsByContractIds( $arrintCids, $arrintContractIds, $objAdminDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintContractIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						lead_details.*,
						cp_details.total_properties_sold,
						cp_details.total_units_sold,
						cp_details.total_implementation_amount,
						cp_details.total_monthly_recurring_amount
					FROM
						(
							SELECT
								c.id AS contract_id,
								date( min( cp.billing_start_date ) ) as billing_start_date,
							   	date( c.contract_start_date ) as contract_start_date,
								mc.id AS cid,
								mc.company_name,
								pld.sales_employee_id,
								pld.support_employee_id,
								pld.training_employee_id,
								ce.employee_id AS responsible_employee_id,
								cd.is_pilot,
								cd.implementation_weight_score,
								pl.id as ps_lead_id,
								pl.property_count,
								pl.number_of_units
							FROM
								clients mc
								JOIN ps_leads pl ON ( mc.ps_lead_id = pl.id AND pl.deleted_on IS NULL )
								JOIN ps_lead_details pld ON ( pl.id = pld.ps_lead_id )
								JOIN contracts c ON ( c.cid = mc.id )
								LEFT JOIN contract_details cd ON ( cd.contract_id = c.id )
								LEFT JOIN contract_employees ce ON ( ce.contract_id = c.id AND ce.contract_employee_type_id = ' . CContractEmployeeType::RESPONSIBLE . ' )
								JOIN contract_properties cp ON ( cp.contract_id = c.id )
							WHERE
								mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
								AND c.id IN ( ' . implode( ',', $arrintContractIds ) . ' )
							GROUP BY
						   		c.id ,
								mc.id,
								mc.company_name,
								pld.sales_employee_id,
								pld.support_employee_id,
								pld.training_employee_id,
								ce.employee_id,
								cd.is_pilot,
								cd.implementation_weight_score,
								pl.id,
								pl.property_count,
								pl.number_of_units
						) lead_details
						LEFT JOIN
						(
						SELECT
							subSql.contract_id,
							COUNT( subSql.property_id) as total_properties_sold,
							SUM( subSql.number_of_units) as total_units_sold,
							SUM( subSql.implementation_amount) as total_implementation_amount,
							SUM( subSql.monthly_recurring_amount) as total_monthly_recurring_amount
						FROM
							(
								SELECT
									c.id as contract_id,
									p.id as property_id,
									p.number_of_units,
									SUM( cp.implementation_amount ) AS implementation_amount,
									SUM( cp.monthly_recurring_amount ) AS monthly_recurring_amount
								FROM
									contracts c
									JOIN contract_properties cp ON ( c.id = cp.contract_id )
									JOIN properties p ON (cp.property_id = p.id AND cp.cid = p.cid)
								WHERE
									cp.contract_id IN ( ' . implode( ',', $arrintContractIds ) . ' )
									AND (cp.termination_date IS NULL OR cp.termination_date >= NOW())
								GROUP BY
									c.id,
									p.id,
									p.number_of_units
							) subSql
						GROUP BY
							subSql.contract_id
						) cp_details ON (cp_details.contract_id = lead_details.contract_id) ';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchPsLeadsByActionEmployees( $arrintEmployeeIds, $objDatabase ) {

	$strSql = 'SELECT
					pl.*, mc.id as cid
				FROM
					ps_leads pl
					LEFT JOIN clients mc ON ( mc.ps_lead_id = pl.id )
					LEFT JOIN actions a ON ( a.ps_lead_id = pl.id )
				WHERE
					a.employee_id IN ( ' . trim( $arrintEmployeeIds ) . ' ) ';

		return parent::fetchPsLeads( $strSql, $objDatabase );
	}

	public static function fetchPaginatedAssignmentPsLeadsByEmployeeId( $objUser, $arrstrTaskFilter, $objDatabase, $boolIsActionCount = false ) {

		if( false == valObj( $objUser, 'CUser' ) )  return NULL;

		$intOffset 				= ( true == isset( $arrstrTaskFilter['page_no'] ) && true == isset( $arrstrTaskFilter['page_size'] ) && false == $boolIsActionCount ) ? $arrstrTaskFilter['page_size'] * ( $arrstrTaskFilter['page_no'] - 1 ) : 0;
		$intLimit 				= ( true == isset( $arrstrTaskFilter['page_size'] ) ) ? $arrstrTaskFilter['page_size'] : '';
		$strOrderByField 		= 'max(ea.created_on)';
		$strOrderByType 		= ' DESC NULLS LAST';
		$strSql					= '';
		$strSelectCondition		= '';
		$strSubSelectCondition	= '';
		$strWhereCondition		= '';
		$strGroupByCondition	= '';
		$strAssociationDate		= '';

		if( true == isset( $arrstrTaskFilter['order_by_type'] ) ) {
			$strOrderByType = ( 'asc' == $arrstrTaskFilter['order_by_type'] ) ? ' ASC NULLS FIRST' : ' DESC NULLS LAST';
		}

		if( true == isset( $arrstrTaskFilter['order_by_field'] ) ) {

			switch( $arrstrTaskFilter['order_by_field'] ) {

				case NULL:
				case 'company_name':
					$strOrderByField = ' pl.company_name';
					break;

				case 'assigned_on':
					$strOrderByField = ' max(ea.created_on)';
					break;

				case 'accepted_on':
					$strOrderByField = ' max(ea.accepted_on)';
					break;

				case 'units':
					$strOrderByField = ' pl.number_of_units';
					break;

				default:
					$strOrderByField = ' max(ea.created_on)';
					break;
			}
		}

		if( true == isset( $arrstrTaskFilter['selected_subtab'] ) ) {

			switch( $arrstrTaskFilter['selected_subtab'] ) {
				case NULL:
				case '1':
					$strSelectCondition		.= ' subSql.created_by, subSql.created_on, ';
					$strSubSelectCondition	.= ' max(ea.created_by) as created_by, max(ea.created_on) as created_on ';
					$strWhereCondition		.= ' ea.employee_id = ' . $objUser->getEmployeeId() . ' AND ea.accepted_by IS NULL ';
					break;

				case '2':
					$strSelectCondition		.= ' subSql.employee_id, subSql.accepted_by, subSql.accepted_on, subSql.created_by, subSql.created_on, ';
					$strSubSelectCondition	.= '  ea.employee_id, ea.accepted_by, ea.accepted_on, ea.created_by, ea.created_on ';
					$strWhereCondition		.= '  ea.created_by = ' . $objUser->getId() . ' AND ea.accepted_by IS NULL ';
					$strGroupByCondition	.= ' , ea.id, ea.employee_id, ea.accepted_by, ea.accepted_on, ea.created_by, ea.created_on';
					break;

				case '3':
					$strSelectCondition		.= ' subSql.accepted_by, subSql.accepted_on, subSql.created_by, subSql.created_on, ';
					$strSubSelectCondition	.= '  max(ea.accepted_by) as accepted_by, max(ea.accepted_on) as accepted_on, max(ea.created_by) as created_by, max(ea.created_on) as created_on ';
					$strWhereCondition		.= ' ea.employee_id = ' . $objUser->getEmployeeId() . ' AND ea.accepted_by IS NOT NULL AND ea.accepted_by = ' . $objUser->getEmployeeId();
					break;

				default:
					$strSelectCondition		.= ' subSql.created_by, subSql.created_on, ';
					$strSubSelectCondition	.= '  max(ea.created_by) as created_by, max(ea.created_on) as created_on ';
					$strWhereCondition		.= ' ea.employee_id = ' . $objUser->getEmployeeId() . ' AND ea.accepted_by IS NULL ';
					break;
			}
		} else {
			$strSelectCondition		.= ' subSql.created_by, subSql.created_on, ';
			$strSubSelectCondition	.= ' max(ea.created_by) as created_by, max(ea.created_on) as created_on ';
			$strWhereCondition		.= ' ea.employee_id = ' . $objUser->getEmployeeId() . ' AND ea.accepted_by IS NULL ';
		}

		if( false == isset( $arrstrTaskFilter['selected_subtab'] ) || ( true == isset( $arrstrTaskFilter['selected_subtab'] ) && ( 1 == $arrstrTaskFilter['selected_subtab'] || 3 == $arrstrTaskFilter['selected_subtab'] ) ) ) {
			if( true == is_numeric( $objUser->getEmployee()->getDepartmentId() ) && ( CDepartment::SALES == $objUser->getEmployee()->getDepartmentId() ) ) {
				$strWhereCondition .= ' AND pld.sales_employee_id = ' . $objUser->getEmployeeId() . ' AND ea.department_id IN ( ' . CDepartment::SALES . ') ';
			}
		}

		$arrintPublishedActionTypeIds = array_merge( CActionType::$c_arrintPublishedActionTypeIds, CActionType::$c_arrintNotEditableActionTypeIds );

		if( false == $boolIsActionCount ) {

			$strSelectSql = '
				SELECT
					subSql.ps_lead_company_name,
					subSql.number_of_units,
					subSql.ps_lead_id,
					subSql.ps_lead_company_name , ' . $strSelectCondition . '
					(
						SELECT
							sub_p.id
						FROM persons sub_p
						WHERE
							sub_p.ps_lead_id = subSql.ps_lead_id AND
							sub_p.email_address IS NOT NULL AND
							sub_p.name_first IS NOT NULL AND
							sub_p.name_last IS NOT NULL AND
							sub_p.is_primary=1 AND
							sub_p.deleted_on IS NULL
						ORDER BY sub_p.created_on DESC OFFSET 0 LIMIT 1
					)as latest_person_id,
					(
						SELECT
							a.id
						FROM
							actions a
						WHERE
							a.ps_lead_id = subSql.ps_lead_id
							AND a.action_type_id IN ( ' . implode( ',', $arrintPublishedActionTypeIds ) . ' )
						ORDER BY
						  	a.created_on DESC OFFSET 0 LIMIT 1
					) AS action_id
				FROM
				( ';
		} else {

			$strSelectSql = '
				SELECT
					count(subSql.ps_lead_id)
				FROM
				( ';
		}

		$strSql = $strSelectSql .
				'SELECT
						pl.company_name as ps_lead_company_name,
						pl.number_of_units,
						pl.id as ps_lead_id,
						' . $strSubSelectCondition . '
					FROM
						ps_leads pl
						JOIN ps_lead_details pld ON (pl.id = pld.ps_lead_id)
						JOIN employee_associations ea ON (pld.ps_lead_id = ea.ps_lead_id)
						LEFT JOIN clients mc ON (pl.id = mc.ps_lead_id AND mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::TERMINATED . ',' . CCompanyStatusType::TEMPLATE . ' ) )
					WHERE '
						 . $strWhereCondition . ' AND
						pl.deleted_by IS NULL
					GROUP BY
					   	pl.company_name,
						pl.number_of_units,
						pl.id,
						ps_lead_company_name ' . $strGroupByCondition . '
					ORDER BY ' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset . '
				)as subSql';

		if( false == $boolIsActionCount ) {

			if( false == is_null( $intLimit ) && 0 < $intLimit ) {
				$strSql .= ' LIMIT ' . ( int ) $intLimit;
			}

			return fetchData( $strSql, $objDatabase );

		} else {

			$arrintResponse = fetchData( $strSql, $objDatabase );

			if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
			return 0;

		}
	}

	public static function fetchPaginatedPsLeadDetailsByEmployeeId( $arrstrLeadFilter, $objDatabase, $boolIsForCount = false ) {

		$strOrderedBy = '  ';

		$arrintCompanyStatusTypeids = array_keys( CCompanyStatusType::$c_arrstrBulkEmployeeAssignmentCompanyStatusTypes );
		if( true == valArr( $arrstrLeadFilter['company_status_type_ids'] ) ) {
			$arrintCompanyStatusTypeids = $arrstrLeadFilter['company_status_type_ids'];
		}

		if( false == $boolIsForCount ) {
			$intOffset = ( 0 < $arrstrLeadFilter['page_no'] ) ? $arrstrLeadFilter['page_size'] * ( $arrstrLeadFilter['page_no'] - 1 ) : 0;
			$intLimit = ( int ) $arrstrLeadFilter['page_size'];
			$strPagination	= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			$strSelect		= 'pl.id,pl.company_name,pl.number_of_units,pl.property_count,pld.state_code, ' . $arrstrLeadFilter['assignment_employee_type'] . ' AS assign_employee_id';

			if( true == isset( $arrstrLeadFilter['sort_by'] ) && 0 < strlen( trim( $arrstrLeadFilter['sort_direction'] ) ) ) {
				$strOrderedBy  = ' ORDER BY ' . $arrstrLeadFilter['sort_by'] . ' ' . $arrstrLeadFilter['sort_direction'] . '';
			} else {
				$strOrderedBy  = ' ORDER BY pl.company_name' . $arrstrLeadFilter['sort_direction'] . '';
			}
		} else {
			$strPagination = '';
			$strSelect = ' count(sub.ps_lead_id) FROM ( SELECT pl.id as ps_lead_id ';
		}

		if( false == valArr( $arrstrLeadFilter['from_employee_ids'] ) && true == valStr( $arrstrLeadFilter['assignment_employee_type'] ) ) {

			$strWhereCondition = $arrstrLeadFilter['assignment_employee_type'] . ' is NULL AND ';
		} elseif( true == valStr( $arrstrLeadFilter['assignment_employee_type'] ) ) {
			$arrintFromEmployeeIds = $arrstrLeadFilter['from_employee_ids'];
			$strWhereCondition = $arrstrLeadFilter['assignment_employee_type'] . ' IN ( ' . implode( ',', $arrintFromEmployeeIds ) . ' ) AND ';
		}

		if( true == valStr( $arrstrLeadFilter['state_code'] ) ) {
			$strWhereCondition .= 'pld.state_code = \'' . $arrstrLeadFilter['state_code'] . '\' AND ';
		}

		if( true == valId( $arrstrLeadFilter['min_number_of_units'] ) ) {
			$strWhereCondition .= 'pl.number_of_units >= ' . $arrstrLeadFilter['min_number_of_units'] . ' AND ';
		}

		if( true == valId( $arrstrLeadFilter['max_number_of_units'] ) ) {
			$strWhereCondition .= 'pl.number_of_units <= ' . $arrstrLeadFilter['max_number_of_units'] . ' AND ';
		}

		$strSql = 'SELECT
						' . $strSelect . '
				   FROM
				   		ps_leads pl
						JOIN ps_lead_details pld ON ( pl.id = pld.ps_lead_id )
						JOIN persons p ON (p.ps_lead_id = pl.id AND p.is_primary = 1)
				   WHERE
						' . $strWhereCondition . '
						pl.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeids ) . ' )
						AND pl.deleted_on IS NULL 
						AND pl.deduped_on IS NULL ' .
						$strOrderedBy . $strPagination;

		if( false == $boolIsForCount ) {
			return self::fetchPsLeads( $strSql, $objDatabase );
		} else {
			$strSql .= 'GROUP BY pl.id ) as sub';

			$arrintCount = fetchData( $strSql, $objDatabase );
			if( true == isset( $arrintCount[0]['count'] ) ) return $arrintCount[0]['count'];
			return 0;
		}
	}

	public static function fetchPaginatedPsLeadDetailsBySalesEmployeeId( $intPageNo, $intPageSize, $objPsLeadFilter, $objDatabase, $boolIsForCount = false ) {

		if( !$boolIsForCount ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit = ( int ) $intPageSize;
			$strPagination = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			$strSelect 	   = 'pl.*, pld.city, pld.state_code, mc.id AS cid ';
		} else {
			$strPagination = '';
			$strSelect = ' count( pl.id ) ';
		}

		$strSql = 'SELECT
						 ' . $strSelect . '
					FROM
						ps_leads pl
						LEFT JOIN clients mc ON mc.ps_lead_id = pl.id
						JOIN ps_lead_details pld ON pl.id = pld.ps_lead_id
						JOIN employee_associations ea ON ea.ps_lead_id = pld.ps_lead_id AND ea.department_id IN ( ' . sqlIntImplode( CDepartment::$c_arrintSalesDepartmentIds ) . ' )
							AND ea.employee_id = pld.sales_employee_id
					WHERE
						pld.sales_employee_id = ' . ( int ) $objPsLeadFilter->getSalesEmployees() . '
						AND pl.company_status_type_id IN ( ' . sqlIntImplode( CCompanyStatusType::$c_arrintActiveRelevantCompanyStatusTypeIds ) . ' )
						AND ea.association_datetime BETWEEN \'' . $objPsLeadFilter->getActionStartDate() . '\' AND \'' . $objPsLeadFilter->getActionEndDate() . '\'
						AND pl.deduped_on IS NULL
						AND pl.deleted_on IS NULL
						' . $strPagination;

		if( !$boolIsForCount ) {
			return self::fetchPsLeads( $strSql, $objDatabase );
		} else {
			$arrintCount = fetchData( $strSql, $objDatabase );
			if( isset( $arrintCount[0]['count'] ) ) return $arrintCount[0]['count'];
			return 0;
		}
	}

	public static function fetchPaginatedPsLeadsByCreatedByEmployeeIdAndCreatedOn( $intPageNo, $intPageSize, $objPsLeadFilter, $objDatabase, $boolIsForCount = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( $boolIsForCount ) {
			$strPagination = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			$strSql = 'SELECT count(*)';
		} else {
			$strPagination = '';
			$strSql = 'SELECT pl.*, pld.city, pld.state_code, mc.id AS cid ';
		}

		$strSql .= 'FROM 
						ps_leads pl
						LEFT JOIN clients mc ON mc.ps_lead_id = pl.id
						JOIN users u ON pl.created_by = u.id
						JOIN ps_lead_details pld ON pl.id = pld.ps_lead_id
					WHERE 
						pl.created_on BETWEEN \'' . $objPsLeadFilter->getActionStartDate() . '\' AND \'' . $objPsLeadFilter->getActionEndDate() . '\'
						AND u.employee_id IN ( ' . $objPsLeadFilter->getSalesEmployees() . ' )';

		$strSql .= $strPagination;

		if( $boolIsForCount ) {
			$arrintCount = fetchData( $strSql, $objDatabase );
			if( isset( $arrintCount[0]['count'] ) ) return $arrintCount[0]['count'];
			return 0;
		} else {
			return self::fetchPsLeads( $strSql, $objDatabase );
		}
	}

	public static function fetchPaginatedPsLeadsByUpdatedByEmployeeIdAndUpdatedOn( $intPageNo, $intPageSize, $objPsLeadFilter, $objDatabase, $boolIsForCount = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( $boolIsForCount ) {
			$strPagination = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			$strSql = 'SELECT count(*)';
		} else {
			$strPagination = '';
			$strSql = 'SELECT pl.*, pld.city, pld.state_code, mc.id AS cid ';
		}

		$strSql .= 'FROM 
						ps_leads pl
						LEFT JOIN clients mc ON ( mc.ps_lead_id = pl.id )
						JOIN users u ON ( pl.updated_by = u.id )
						JOIN ps_lead_details pld ON ( pl.id = pld.ps_lead_id )
					WHERE 
						pl.updated_on BETWEEN \'' . $objPsLeadFilter->getActionStartDate() . '\' AND \'' . $objPsLeadFilter->getActionEndDate() . '\'
						AND u.employee_id IN (' . $objPsLeadFilter->getSalesEmployees() . ')';

		$strSql .= $strPagination;

		if( $boolIsForCount ) {
			$arrintCount = fetchData( $strSql, $objDatabase );
			if( isset( $arrintCount[0]['count'] ) ) return $arrintCount[0]['count'];
			return 0;
		} else {
			return self::fetchPsLeads( $strSql, $objDatabase );
		}
	}

	public static function fetchPaginatedPsLeadsBySalesEmployeeIdAndClosedOn( $intPageNo, $intPageSize, $objPsLeadFilter, $objDatabase, $boolIsForCount = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( true == $boolIsForCount ) {
			$strPagination = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			$strSql = 'SELECT count(*)';
		} else {
			$strPagination = '';
			$strSql = 'SELECT pl.*, mc.id as cid ';
		}

		$strSql .= 'FROM ps_leads pl
						LEFT JOIN clients mc ON ( mc.ps_lead_id = pl.id )
						JOIN (
								SELECT
									cp.cid,
									c.sales_employee_id,
									cp.close_date,
									rank() OVER ( PARTITION BY cp.cid ORDER BY cp.close_date ASC, cp.id )
								FROM
									contract_properties cp
									JOIN contracts c ON ( cp.contract_id = c.id AND cp.cid = c.cid )
									LEFT JOIN contract_termination_requests ctr ON ( ctr.cid = cp.cid AND ctr.id = cp.contract_termination_request_id )
								WHERE
									cp.close_date IS NOT NULL
									AND c.contract_status_type_id IN ( ' . implode( ',', CContractStatusType::$c_arrintCompletedContractStatusTypeIds ) . ' )
									AND COALESCE ( ctr.contract_termination_reason_id, 0 ) <> ' . CContractTerminationReason::DUPLICATE_CONTRACT . '
							) AS cp_active ON ( cp_active.cid = mc.id AND rank = 1 )
					WHERE cp_active.close_date BETWEEN \'' . $objPsLeadFilter->getActionStartDate() . '\' AND \'' . $objPsLeadFilter->getActionEndDate() . '\'
						AND cp_active.sales_employee_id IN (' . $objPsLeadFilter->getSalesEmployees() . ')
						AND pl.deduped_on IS NULL
						AND pl.deleted_on IS NULL
						AND  pl.company_status_type_id IN ( ' . implode( ',', CCompanyStatusType::$c_arrintClosedCompanyStatusTypeIds ) . ' )';

		$strSql .= $strPagination;

		if( true == $boolIsForCount ) {
			$arrintCount = fetchData( $strSql, $objDatabase );
			if( true == isset( $arrintCount[0]['count'] ) ) return $arrintCount[0]['count'];
			return 0;
		} else {
			return self::fetchPsLeads( $strSql, $objDatabase );
		}
	}

	public static function fetchPossiblePsLeadDuplicatesByPsLead( $objPsLead, $objDatabase, $boolIsMerged = false, $boolExcludeOrigin = false ) {

		$strSql = 'SELECT
				DISTINCT ON ( pl.id )
				pl.*, mc.id as cid, ';

		if( false == $boolIsMerged ) {
			$strSql .= ' p.name_first,
				p.name_last,
				p.title,
				p.id AS person_id,
				p.email_address,
				p.address_line1,
				p.address_line2,
				p.city,
				p.state_code,
				p.postal_code,
				pld.ps_lead_duplicate_type_id,
				pld.reason,
				pld.ignored_by, ';
		}

		$strSql .= ' mc.company_status_type_id,
			 c.id AS contract_id,
			 c.contract_status_type_id AS contract_status_type_id
			 FROM ';

		if( false == $boolIsMerged ) {
			$strSql .= ' ps_lead_duplicates pld
				 JOIN ps_leads pl ON ( pl.id = pld.ps_lead_id2)
				 JOIN persons p ON ( p.ps_lead_id = pld.ps_lead_id2 AND p.is_primary=1 ) ';
		} else {
			$strSql .= ' ps_leads pl';
		}

		$strSql .= ' LEFT JOIN clients mc ON ( mc.ps_lead_id = pld.ps_lead_id2 )
			 LEFT JOIN contracts c ON ( c.ps_lead_id = pld.ps_lead_id2 )
			 WHERE ';

		if( false == $boolIsMerged ) {
			$strSql .= ' pl.deleted_on IS NULL AND pl.deduped_on IS NULL AND pld.ps_lead_id1 = ' . ( int ) $objPsLead->getId();
		} else {
			$strSql .= ' pl.deleted_on IS NOT NULL AND pl.deduped_on IS NOT NULL AND pl.ps_lead_id = ' . $objPsLead->getId();
		}

		if( true == $boolExcludeOrigin ) {
			$strSql = $strSql . ' AND pl.ps_lead_origin_id NOT IN ( ' . CPsLeadOrigin::VERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::UNVERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::MIDVERIFIED_INDEXED_DOMAIN . ' , ' . CPsLeadOrigin::NMHC . '  ) ';
		}

		return self::fetchPsLeads( $strSql, $objDatabase );
	}

	public static function fetchPossibleDuplicatesByPsLead( $objPsLead, $objDatabase, $boolIsStrict = false, $boolIsMerged = false ) {

		$strSql = 'SELECT
				DISTINCT ON ( pl.id )
				pl.*, mc.id as cid, ';

		if( false == $boolIsMerged ) {
			$strSql .= '	p.id as person_id,
				p.name_first,
				p.name_last,
				p.title,
				p.email_address,
				p.address_line1,
				p.address_line2,
				p.city,
				p.state_code,
				p.postal_code,
				p.is_primary, ';
		}

		$strSql .= '	mc.company_status_type_id,
				c.id AS contract_id,
				c.contract_status_type_id as contract_status_type_id
			FROM
				ps_leads pl ';

		if( false == $boolIsMerged ) {
			$strSql .= ' JOIN persons p ON ( p.ps_lead_id = pl.id )';
		}

		$strSql .= ' LEFT JOIN clients mc ON ( mc.ps_lead_id = pl.id )
			 LEFT JOIN contracts c ON ( c.ps_lead_id = pl.id )
			 LEFT JOIN person_phone_numbers ppn ON ( ppn.person_id = p.id AND ppn.phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' )
			WHERE ';

		if( false == $boolIsMerged ) {
			$strSql .= ' pl.deleted_on IS NULL AND pl.deduped_on IS NULL';
		} else {
			$strSql .= ' pl.deleted_on IS NOT NULL AND pl.deduped_on IS NOT NULL';
		}

		$strSql .= ' AND pl.id <> ' . ( int ) $objPsLead->getId() . ' ' . self::buildPossibleDuplicatesWhereClauseByPsLead( $objPsLead, $boolIsStrict, $boolIsMerged );

		return self::fetchPsLeads( $strSql, $objDatabase );
	}

	public static function buildPossibleDuplicatesWhereClauseByPsLead( $objPsLead, $boolIsStrict = false, $boolIsMerged = false ) {

		// This function should pull any ps leads that look like they may be conflicting.  Here are the conflicting criteria.
		// 1. Emails are the same.
		// 2. Client ID is the same.
		// 3. Lead company name is the same.
		// 4. Person First and Last Names are the same and state is the same.
		// 5. Phone numbers are the same.

		$strSql = ' AND ( ';

		if( false == $boolIsMerged ) {
			if( false == is_null( trim( $objPsLead->getEmailAddress() ) ) && 0 < strlen( trim( $objPsLead->getEmailAddress() ) ) ) {
				$strSql .= ' ( p.email_address IS NOT NULL AND p.email_address = \'' . trim( addslashes( $objPsLead->getEmailAddress() ) ) . '\' ) OR ';
			}

			// if( true == is_numeric( $objPsLead->getCid() ) ) {
			// $strSql .= ' ( pl.cid IS NOT NULL AND pl.cid = ' . ( int ) $objPsLead->getCid() . ' ) OR ';
			// }

			if( false == is_null( trim( $objPsLead->getCompanyName() ) ) && 0 < strlen( trim( $objPsLead->getCompanyName() ) ) ) {
				if( false == $boolIsStrict ) {
					$strSql .= ' ( pl.company_name IS NOT NULL AND pl.company_name ILIKE \'%' . trim( addslashes( $objPsLead->getCompanyName() ) ) . '%\' ) OR ';
				} else {
					// In strict mode, require that the company name and state code match
					$strSql .= ' ( ( pl.company_name IS NOT NULL AND lower( pl.company_name ) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objPsLead->getCompanyName() ) ) ) . '\' )
									AND
								( p.state_code IS NOT NULL AND p.state_code = \'' . trim( addslashes( $objPsLead->getStateCode() ) ) . '\' )
							  ) OR ';
				}
			}

			if( false == is_null( trim( $objPsLead->getNameFirst() ) ) && 0 < strlen( trim( $objPsLead->getNameFirst() ) )
			&& false == is_null( trim( $objPsLead->getNameLast() ) ) && 0 < strlen( trim( $objPsLead->getNameLast() ) )
			&& false == is_null( trim( $objPsLead->getStateCode() ) ) && 0 < strlen( trim( $objPsLead->getStateCode() ) ) ) {

				$strSql .= ' ( p.name_first IS NOT NULL AND lower ( p.name_first ) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objPsLead->getNameFirst() ) ) ) . '\'
							AND p.name_last IS NOT NULL AND lower( p.name_last ) = \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objPsLead->getNameLast() ) ) ) . '\'
							AND p.state_code IS NOT NULL AND p.state_code = \'' . trim( addslashes( $objPsLead->getStateCode() ) ) . '\' )	OR ';
			}

			if( false == is_null( trim( preg_replace( '/[^0-9]/', '', $objPsLead->getPhoneNumber1() ) ) ) && 9 < strlen( trim( preg_replace( '/[^0-9]/', '', $objPsLead->getPhoneNumber1() ) ) ) ) {
				$strSql .= ' ( ppn.phone_number IS NOT NULL AND regexp_replace( ppn.phone_number, \'[^0-9]*\' ,\'\', \'g\' ) = \'' . trim( preg_replace( '/[^0-9]/', '', $objPsLead->getPhoneNumber1() ) ) . '\' ) OR ';
			}
		}
		// This condition will prevent the query from failing where none of the above conditions are met.
		$strSql .= ' ( pl.ps_lead_id = ' . $objPsLead->getId() . ' ) ) ';

		return $strSql;
	}

	public static function fetchStagnentPsLeads( $arrstrPsLeads, $objAdminDatabase ) {

		if( false == valArr( $arrstrPsLeads ) ) return NULL;

		$strSql = 'SELECT
						sub.ps_lead_id,
						CASE
						   	WHEN sum(sub.contract_count) > 0
							THEN \'No\'
							ELSE \'Yes\'
						END as is_stagnant
					FROM (
							SELECT
								 a.ps_lead_id,
								 CASE
									WHEN max(a.updated_on) > current_date - INTERVAL \'120\' day AND a.action_type_id IN (' . CActionType::PROPOSAL_SENT . ',' . CActionType::CONTRACT_SENT . ',' . CActionType::CONTRACT_APPROVED . ')  THEN 1
									ELSE 0
								 End as contract_count
							FROM
								 actions a
							GROUP BY
								 a.ps_lead_id,
								 a.action_type_id
							) sub
					WHERE
						sub.ps_lead_id IN ( ' . implode( ',', $arrstrPsLeads ) . ' )
					GROUP BY
						sub.ps_lead_id';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchNewSalesAndLostRevenue( $objAdminDatabase ) {

		$strSql = ' SELECT
						to_char (sp.month, \'Mon\') as Month,
						SUM (sp.total_new_acv)+
						COALESCE (lag (SUM (sp.total_new_acv),1) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.total_new_acv),2) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.total_new_acv),3) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.total_new_acv),4) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.total_new_acv),5) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.total_new_acv),6) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.total_new_acv),7) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.total_new_acv),8) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.total_new_acv),9) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.total_new_acv),10) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.total_new_acv),11) OVER (ORDER BY sp.month),0) as cumulative_new,
						SUM (sp.lost_acv_unhappy)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),1) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),2) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),3) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),4) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),5) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),6) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),7) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),8) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),9) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),10) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (sp.lost_acv_unhappy),11) OVER (ORDER BY sp.month),0) as cumulative_lost,
						SUM(CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END )+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),1) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),2) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),3) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),4) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),5) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),6) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),7) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),8) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),9) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),10) OVER (ORDER BY sp.month),0)+
						COALESCE (lag (SUM (CASE WHEN sp.ps_product_id = ' . CPsProduct::ENTRATA . ' THEN sp.total_new_acv ELSE 0 END),11) OVER (ORDER BY sp.month),0) as cumulative_new_entrata_core
					FROM
						stats_products sp
					WHERE
						date_part( \'year\', sp.month ) = date_part( \'year\', NOW() ) group by sp.month ';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchLeadsByClientIdsBySalesEmployeeIds( $arrintCids, $intSalesEmployeeIds, $objAdminDatabase ) {

		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						pl.*,
						pld.sales_employee_id,
						mc.id as cid
					FROM
						ps_leads pl
						JOIN ps_lead_details pld ON( pld.ps_lead_id  = pl.id )
						LEFT JOIN clients mc on ( pl.id = mc.ps_lead_id )
					WHERE
					pld.sales_employee_id IN ( ' . implode( ',', $intSalesEmployeeIds ) . ')
						AND mc.id IN (' . implode( ',', $arrintCids ) . ')
						AND pl.deleted_on IS NULL';

		return self:: fetchPsLeads( $strSql, $objAdminDatabase );
	}

	public static function fetchPsLeadByCompanyName( $strCompanyName, $objDatabase ) {

		if( true == empty( $strCompanyName ) )	return NULL;

		$strSql = 'SELECT
						pl.id,
						c.id AS cid,
						pl.company_name
					FROM
						ps_leads pl
						JOIN clients c ON ( pl.id = c.ps_lead_id )
						JOIN persons p ON ( pl.id = p.ps_lead_id )
					WHERE
						pl.id = p.ps_lead_id
						AND pl.deleted_on IS NULL
						AND p.is_primary=1
						AND pl.company_name ILIKE \'' . trim( addslashes( $strCompanyName ) ) . '\'
					GROUP BY
						pl.id,
						c.id,
						pl.company_name
					ORDER BY
						pl.company_name
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>