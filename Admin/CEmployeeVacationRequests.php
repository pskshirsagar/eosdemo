<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeVacationRequests
 * Do not add any new functions to this class.
 */

class CEmployeeVacationRequests extends CBaseEmployeeVacationRequests {

	public static function fetchActiveEmployeeVacationRequestsByEmployeeId( $intEmployeeId, $objDatabase, $boolIsPaidVacationRequest = true, $boolIsDateInterval = true, $intVacationTypeId = NULL ) {

		$strSubSql = ( true == $boolIsPaidVacationRequest ) ? ' AND is_paid IS TRUE' : '';

		$strSubSql .= ( true == $boolIsDateInterval ) ? ' AND begin_datetime >= ( NOW ( ) - INTERVAL \'' . '16 Month\' ) ' : '';

		if( true == is_numeric( $intVacationTypeId ) ) {
			$strWhereCondition = 'AND vacation_type_id = ' . ( int ) $intVacationTypeId;
		} else {
			$strWhereCondition = 'AND vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';
		}

		$strSql = 'SELECT
						*
					FROM
						employee_vacation_requests
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND deleted_by IS NULL ' . $strSubSql . '
						' . $strWhereCondition . '
					ORDER BY
						id DESC';
		return self::fetchEmployeeVacationRequests( $strSql, $objDatabase );
	}

	public static function fetchEmployeeAppliedLeavesByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) return NULL;

		$strWhereCondition = '';

		$intHalfYear = 06;
		if( date( 'm' ) <= $intHalfYear && strtotime( date( 'm/d/Y' ) ) <= strtotime( date( $intHalfYear . '/' . CEmployeeVacationRequest::LEAVE_POOL_HALF_YEAR_DATE . '/Y' ) ) ) {
			$strStartDate = date( 'Y-m-d H:i:s', strtotime( '' . date( 'Y' ) - 1 . '-12-26' . ' 00:00:00' ) );
			$strEndDate   = date( 'Y-m-d H:i:s', strtotime( '' . date( 'Y' ) . '-06-25' . ' 23:59:00' ) );
		} else {
			$strStartDate	= date( 'Y-m-d H:i:s', strtotime( '' . date( 'Y' ) . '-06-26' . ' 00:00:00' ) );
			$strEndDate     = date( 'Y-m-d H:i:s', strtotime( '' . date( 'Y' ) . '-12-25' . ' 23:59:00' ) );
		}

		$strSubWhereConditionSql = ' AND end_datetime >= \'' . $strStartDate . '\' AND begin_datetime <= \'' . $strEndDate . '\'';

		$strSql = ' SELECT
						CASE
							WHEN ( approved_by IS NOT NULL ) THEN \'approved_leaves\'
							ELSE \'unapproved_leaves\'
						END as status,
						vacation_type_id,
						CASE
							WHEN ( vacation_type_id = ' . CVacationType::MARRIAGE . ' AND scheduled_vacation_hours <= ' . CEmployeeVacationRequest::VACATION_TYPE_MARRIAGE_LEAVES_WORKING_HOURS . ' ) THEN 0
							WHEN ( vacation_type_id = ' . CVacationType::MARRIAGE . ' AND scheduled_vacation_hours >= ' . CEmployeeVacationRequest::VACATION_TYPE_MARRIAGE_LEAVES_WORKING_HOURS . ' ) THEN SUM ( scheduled_vacation_hours ) - ' . CEmployeeVacationRequest::VACATION_TYPE_MARRIAGE_LEAVES_WORKING_HOURS . '
							WHEN ( vacation_type_id = ' . CVacationType::PATERNITY . ' AND scheduled_vacation_hours <= ' . CEmployeeVacationRequest::VACATION_TYPE_PATERNITY_LEAVES_WORKING_HOURS . ' ) THEN 0
							WHEN ( vacation_type_id = ' . CVacationType::PATERNITY . ' AND scheduled_vacation_hours >= ' . CEmployeeVacationRequest::VACATION_TYPE_PATERNITY_LEAVES_WORKING_HOURS . ' ) THEN SUM ( scheduled_vacation_hours ) - ' . CEmployeeVacationRequest::VACATION_TYPE_PATERNITY_LEAVES_WORKING_HOURS . '
							WHEN ( vacation_type_id = ' . CVacationType::MARRIAGE_ANNIVERSARY . ' ) THEN SUM ( scheduled_vacation_hours ) - ' . CEmployeeVacationRequest::VACATION_TYPE_MARRIAGE_ANNIVERSARY_LEAVES_WORKING_HOURS . '
							WHEN ( vacation_type_id = ' . CVacationType::BIRTHDAY . ' ) THEN SUM ( scheduled_vacation_hours ) - ' . CEmployeeVacationRequest::VACATION_TYPE_BIRTHDAY_LEAVES_WORKING_HOURS . '
							WHEN ( vacation_type_id = ' . CVacationType::OPTIONAL_HOLIDAY . ' AND scheduled_vacation_hours <= ' . CEmployeeVacationRequest::VACATION_TYPE_OPTIONAL_HOLIDAY_WORKING_HOURS . ' ) THEN 0
							WHEN ( vacation_type_id = ' . CVacationType::OPTIONAL_HOLIDAY . ' AND scheduled_vacation_hours >= ' . CEmployeeVacationRequest::VACATION_TYPE_OPTIONAL_HOLIDAY_WORKING_HOURS . ' ) THEN SUM ( scheduled_vacation_hours ) - ' . CEmployeeVacationRequest::VACATION_TYPE_OPTIONAL_HOLIDAY_WORKING_HOURS . '
							WHEN ( vacation_type_id = ' . CVacationType::COVID_HOME_ISOLATION . ' AND scheduled_vacation_hours <= ' . CEmployeeVacationRequest::VACATION_TYPE_COVID_HOME_ISOLATION_WORKING_HOURS . ' ) THEN 0
							WHEN ( vacation_type_id = ' . CVacationType::COVID_HOME_ISOLATION . ' AND scheduled_vacation_hours >= ' . CEmployeeVacationRequest::VACATION_TYPE_COVID_HOME_ISOLATION_WORKING_HOURS . ' ) THEN SUM ( scheduled_vacation_hours ) - ' . CEmployeeVacationRequest::VACATION_TYPE_COVID_HOME_ISOLATION_WORKING_HOURS . '
							WHEN ( vacation_type_id = ' . CVacationType::COVID_HOSPITALIZATION . ' AND scheduled_vacation_hours <= ' . CEmployeeVacationRequest::VACATION_TYPE_COVID_HOSPITALIZATION_WORKING_HOURS . ' ) THEN 0
							WHEN ( vacation_type_id = ' . CVacationType::COVID_HOSPITALIZATION . ' AND scheduled_vacation_hours >= ' . CEmployeeVacationRequest::VACATION_TYPE_COVID_HOSPITALIZATION_WORKING_HOURS . ' ) THEN SUM ( scheduled_vacation_hours ) - ' . CEmployeeVacationRequest::VACATION_TYPE_COVID_HOSPITALIZATION_WORKING_HOURS . '
							ELSE
								SUM ( scheduled_vacation_hours )
						END
					FROM
						employee_vacation_requests
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND vacation_type_id NOT IN ( ' . CVacationType::OFFICIAL_TOUR . ' , ' . CVacationType::WEEKEND_WORKING . ' , ' . CVacationType::MATERNITY . ', ' . CVacationType::BEREAVEMENT . ', ' . CVacationType::WORK_FROM_HOME . ' , ' . CVacationType::COMPENSATORY_OFF . ' )
						AND denied_on IS NULL
						AND deleted_by IS NULL
						' . $strWhereCondition . '
						' . $strSubWhereConditionSql . '
					GROUP BY
						vacation_type_id,
						scheduled_vacation_hours,
						approved_by';

		 return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsByDate( $objDatabase, $strStartDate, $strEndDate = NULL, $boolIsApprovedOnly = false ) {

		$strSql = '	SELECT
						evr.*
					FROM employee_vacation_requests AS evr
						 INNER JOIN employees AS e ON evr.employee_id = e.id
					WHERE
						evr.deleted_by IS NULL
						AND evr.end_datetime >= \'' . $strStartDate . '\'';

		if( false == empty( $strEndDate ) ) {
			$strSql .= ' AND evr.begin_datetime <= \'' . $strEndDate . '\'';
		}

		if( true == $boolIsApprovedOnly ) {
			$strSql .= ' AND evr.approved_by IS NOT NULL AND evr.approved_on IS NOT NULL ';
		}

		$strSql .= ' ORDER BY e.name_first, e.name_last';

		return self::fetchEmployeeVacationRequests( $strSql, $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsByMonthByYearByCountryCode( $strMonth, $strYear, $strCountryCode, $objDatabase, $strOrderBy = NULL, $boolApprovalPending = false, $intEmployeeId = NULL, $boolShowAllRequests = true ) {

		$strSingleEmployeeCondition = '';
		$strMonthCondition			= '';
		if( false == is_null( $intEmployeeId ) ) {
			$strSingleEmployeeCondition = 'e.id = ' . ( int ) $intEmployeeId . ' AND ';
		}

		if( false == is_null( $strMonth ) ) {
			$strMonthCondition = 'AND \'' . addslashes( $strMonth ) . '\' BETWEEN DATE_PART( \'month\', evr.begin_datetime ) AND DATE_PART( \'month\', evr.end_datetime )';
		}

		if( false == $boolShowAllRequests ) {
			$strMonthCondition .= ' AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';
		}

		$strSql = '	SELECT
						evr.*
					FROM
						employee_vacation_requests AS evr
						INNER JOIN employees AS e ON ( evr.employee_id = e.id )
						INNER JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						' . $strSingleEmployeeCondition . '
						evr.deleted_by IS NULL
						' . $strMonthCondition . '
						AND \'' . addslashes( $strYear ) . '\' BETWEEN DATE_PART( \'year\', evr.begin_datetime ) AND DATE_PART( \'year\', evr.end_datetime )
						AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\' AND evr.is_paid IS TRUE';

		if( false != $boolApprovalPending ) {
			$strSql .= ' AND evr.approved_by IS NULL AND evr.denied_by IS NULL';
		}

		if( false == is_null( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ' . $strOrderBy;
		} else {
			$strSql .= ' ORDER BY preferred_name';
		}

		return self::fetchEmployeeVacationRequests( $strSql, $objDatabase );
	}

	public static function fetchDetailEmployeeVacationRequestsByEmployeeIdsByDateRange( $arrintEmployeeIds, $strStartDate, $strEndDate, $strCountryCode, $objDatabase, $arrintExcludeVacationRequests = NULL, $boolIsShowOnlyApprovedRequests = true, $boolIsLeavePoolCount = true, $intVacationTypeId = NULL, $boolFetchDeniedLeaves = false, $boolCompensatoryOff = false ) {
		if( false == valArr( $arrintEmployeeIds ) ) return false;
		$strSubSql					= '';
		$strWhereCondition			= '';
		$strSubWhereCondition		= '';
		$strSubWhereConditionSql	= '';

		if( true == valArr( $arrintExcludeVacationRequests ) ) {
			$strSubSql .= ' id NOT IN (' . implode( ',', $arrintExcludeVacationRequests ) . ') AND ';
		}

		if( true == $boolIsShowOnlyApprovedRequests ) {
			$strSubSql .= ' approved_by IS NOT NULL AND ';
		}

		if( true == valStr( $strStartDate ) && true == valStr( $strEndDate ) ) {
			if( true == $boolIsLeavePoolCount ) {
				$strSubWhereCondition = ' AND employee_vacation_requests.date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'';
			} else {
				$strSubWhereConditionSql = ' AND end_datetime::DATE >= \'' . $strStartDate . '\' AND begin_datetime::DATE <= \'' . $strEndDate . '\'';
			}
		}

		if( true == is_numeric( $intVacationTypeId ) ) {
			$strWeekendWorkingCondition = 'employee_vacation_requests.vacation_type_id = ' . ( int ) $intVacationTypeId;
			if( true == $boolCompensatoryOff && $intVacationTypeId == CVacationType::WEEKEND_WORKING ) {
				$strWeekendWorkingCondition = 'employee_vacation_requests.vacation_type_id IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::COMPENSATORY_OFF . ')';
			}
		} else {

			if( true == valStr( $strStartDate ) && true == valStr( $strEndDate ) && strtotime( CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY ) >= strtotime( $strStartDate ) && strtotime( CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY ) <= strtotime( $strEndDate ) ) {
				$strWhereCondition = 'OR ( EXTRACT (\'dow\' FROM employee_vacation_requests.date) IN ( ' . CEmployeeVacationRequest::DAY_OF_WEEK_SATURDAY . ' ) AND employee_vacation_requests.date = \'' . CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY . '\' )';
			}

			$strWeekendWorkingCondition = 'employee_vacation_requests.date NOT IN (
											SELECT
												date
											FROM
												paid_holidays
											WHERE
												country_code = \'' . addslashes( $strCountryCode ) . '\'
												AND is_optional IS FALSE
												AND is_published = 1
												AND date BETWEEN employee_vacation_requests.begin_datetime::date AND employee_vacation_requests.end_datetime::date
											 )
											AND employee_vacation_requests.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';
		}

		$strSubWhereConditionSql .= ( ( false == $boolFetchDeniedLeaves ) ? ' AND denied_by IS NULL ' : '' );

		$strSql = '
					SELECT
						*,
						CASE
							WHEN ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 ) THEN 8
							WHEN ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number > 0 AND ( employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date ) ) THEN 8
							WHEN ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 AND employee_vacation_requests.serial_number > 0 ) THEN 8
							WHEN ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date ) THEN 8
							WHEN ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_trunc( \'day\', employee_vacation_requests.begin_datetime ) = DATE ) THEN 4
							WHEN ( date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND DATE = date_trunc( \'day\', employee_vacation_requests.end_datetime ) ) THEN 4 ELSE 0
						END AS vacation_hours
					FROM
						( SELECT
							DISTINCT *,
							generate_series ( 0, end_datetime::date - begin_datetime::date ) AS serial_number,
							begin_datetime::date + generate_series ( 0, end_datetime::date - begin_datetime::date ) AS date,
							SUM( scheduled_vacation_hours ) OVER ( PARTITION BY vacation_type_id ) AS vacation_requests_count,
							SUM( scheduled_vacation_hours ) OVER ( PARTITION BY employee_id ) AS total_vacation_requests_count
						FROM
							employee_vacation_requests
						WHERE
							' . $strSubSql . '
							employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
							AND deleted_by IS NULL
							' . $strSubWhereConditionSql . '
						) AS employee_vacation_requests
					WHERE
						' . $strWeekendWorkingCondition . '
						' . $strSubWhereCondition . '
						AND employee_vacation_requests.is_paid IS TRUE
					ORDER BY date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchWorkingDaysByDateRangeByCountryCode( $strBeginDate, $strEndDate, $strCountryCode, $objDatabase, $boolFetchCachedData = false ) {

		if( false == valStr( $strCountryCode ) || false == valStr( $strBeginDate ) || false == valStr( $strEndDate ) ) return NULL;

		$intDayOfWeekSaturday 	= 6;
		$intDayOfWeekSunday 	= 0;

		$strSql = '	SELECT
						DISTINCT business_days.business_day
					FROM
						( SELECT
								DISTINCT date \'' . $strBeginDate . '\' + serial_number AS business_day
							FROM
								generate_series( 0, date \'' . $strEndDate . '\' - date \'' . $strBeginDate . '\') serial_number
							WHERE
								EXTRACT ( \'dow\' FROM date \'' . $strBeginDate . '\' + serial_number ) NOT IN ( \'' . ( int ) $intDayOfWeekSaturday . '\', \'' . ( int ) $intDayOfWeekSunday . '\' )
						) AS business_days
					WHERE
						business_days.business_day NOT IN ( SELECT date FROM paid_holidays WHERE country_code = \'' . addslashes( $strCountryCode ) . '\' AND is_optional IS FALSE )
					ORDER BY
						business_days.business_day';

		if( true == $boolFetchCachedData ) {
			return fetchOrCacheData( $strSql, 86400, 'business_working_days_' . date( 'Y_m_d', strtotime( $strBeginDate ) ) . '_to_' . date( 'Y_m_d', strtotime( $strEndDate ) ), $objDatabase );
		} else {
			return fetchdata( $strSql, $objDatabase );
		}

	}

	public static function fetchPendingEmployeeVacationRequestsByCountryCodeByEmployeeIds( $arrstrCountryCode, $arrintEmployeesIds, $objDatabase, $intShowAllEmployees, $boolIsPaidVacationRequest = true ) {

		$strSql = ' SELECT
						DISTINCT(e.id), e.*, evr.*, vt.name
					FROM
						employee_vacation_requests evr
						INNER JOIN employees e ON ( e.id = evr.employee_id )
						INNER JOIN employee_addresses eaddr ON ( e.id = eaddr.employee_id )
						INNER JOIN vacation_types vt ON (vt.id = evr.vacation_type_id )
					WHERE
						eaddr.country_code IN ( \'' . implode( '\',\'', $arrstrCountryCode ) . '\' )';

					if( false == $intShowAllEmployees ) {
						$strSql .= ' AND e.id IN( ' . implode( ',', $arrintEmployeesIds ) . ' )';
					}

					if( true == $boolIsPaidVacationRequest ) $strSql .= ' AND is_paid IS TRUE';

					$strSql .= ' AND	e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
						AND evr.deleted_by IS NULL
						AND	( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND ( evr.begin_datetime ) >= ( NOW() - INTERVAL \'2 Month\' )
						AND evr.approved_by IS NULL
						AND evr.approved_on IS NULL
						AND evr.denied_by IS NULL
						AND evr.denied_on IS NULL
					ORDER BY
						evr.denied_on ASC, evr.approved_on DESC, evr.id DESC ';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchEmployeesVacationRequestsByEmployeeIds( $arrintEmployeeIds, $strHalfYearlyBeginDatetime, $strHalfYearlyEndDatetime, $objDatabase, $boolIsPaidVacationRequest = true ) {

		$strSql = ' SELECT
						DISTINCT (e.id) AS employee_id,
						SUM(evr.scheduled_vacation_hours) OVER(PARTITION BY e.id) AS used_hours
					FROM
						employees as e
						JOIN employee_vacation_requests evr ON ( e.id = evr.employee_id )
					WHERE
						e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND end_datetime >= \'' . $strHalfYearlyBeginDatetime . '\'
						AND begin_datetime <= \'' . $strHalfYearlyEndDatetime . '\'
						AND evr.approved_by IS NOT NULL
						AND evr.approved_on IS NOT NULL
						AND evr.deleted_by IS NULL
						AND evr.deleted_on IS NULL
						AND evr.denied_by IS NULL
						AND evr.denied_on IS NULL';

		if( true == $boolIsPaidVacationRequest ) $strSql .= ' AND is_paid IS TRUE';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchPaginatedActiveEmployeeVacationRequestsByEmployeeId( $intPageNo, $intPageSize, $intEmployeeId, $objDatabase, $boolIsPaidVacationRequest = true, $intVacationTypeId = NULL ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strIsPaidVacationRequestCondition = ( true == $boolIsPaidVacationRequest ) ? ' AND is_paid IS TRUE' : '';

		if( true == is_numeric( $intVacationTypeId ) ) {
			$strCondition = 'AND vacation_type_id = ' . ( int ) $intVacationTypeId;
		} else {
			$strCondition = 'AND vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';
		}

		$strSql = 'SELECT
						*
					FROM
						employee_vacation_requests
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND deleted_by IS NULL
						AND begin_datetime >= ( NOW ( ) - INTERVAL \'' . '16 Month\' )
						' . $strIsPaidVacationRequestCondition . '
						' . $strCondition . '
					ORDER BY
						id DESC
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;

		return self::fetchEmployeeVacationRequests( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeeVacationRequestsCountByEmployeeId( $intEmployeeId, $objDatabase, $boolIsPaidVacationRequest = true, $intVacationTypeId = NULL ) {

		if( true == is_numeric( $intVacationTypeId ) ) {
			$strCondition = 'vacation_type_id = ' . ( int ) $intVacationTypeId;
		} else {
			$strCondition = 'vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';
		}

		$strSql = 'SELECT
						COUNT(*)
					FROM
						employee_vacation_requests
					WHERE
						' . $strCondition . '
						AND employee_id = ' . ( int ) $intEmployeeId . '
						AND deleted_by IS NULL
						AND begin_datetime >= ( NOW ( ) - INTERVAL \'' . '16 Month\' )';

		if( true == $boolIsPaidVacationRequest ) $strSql .= ' AND is_paid IS TRUE';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchPendingEmployeeVacationRequestsByEmployeeIds( $arrintEmployeeIds, $objDatabase, $intVacationTypeId = NULL ) {

		if ( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSubSql	 = '';

		if( true == is_numeric( $intVacationTypeId ) ) {
			$strCondition = 'AND vacation_type_id = ' . ( int ) $intVacationTypeId;
			$strOrder = 'evr.denied_on ASC,evr.approved_on DESC,evr.id DESC ';
		} else {
			$strCondition = 'AND vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ', ' . CVacationType::COMPENSATORY_OFF . ' )';
			$strSubSql = ',(
							 SELECT
								data.priority
							FROM (
									SELECT
										evr1.id,
										rank() OVER(
									ORDER BY evr1.created_on) AS priority
									FROM employee_vacation_requests evr1
										 JOIN employees e ON (e.id = evr1.employee_id)
									WHERE ((
											( evr1.begin_datetime::date BETWEEN evr.begin_datetime::date and evr.end_datetime::date )
											 or
											( evr1.end_datetime::date BETWEEN evr.begin_datetime::date and evr.end_datetime::date )
												) OR (
											( evr.begin_datetime::date BETWEEN evr1.begin_datetime::date and evr1.end_datetime::date )
											or
											( evr.end_datetime::date BETWEEN evr1.begin_datetime::date and evr1.end_datetime::date )
										) )
										AND e.id IN( ' . implode( ',', $arrintEmployeeIds ) . ' )
										AND evr1.deleted_on IS NULL
										AND evr1.denied_by IS NULL
										AND	evr1.approved_by IS NULL
										ORDER BY evr.created_on,
										 evr.begin_datetime ASC
								) AS data
							WHERE data.id = evr.id
							AND evr.deleted_on IS NULL
							AND evr.denied_by IS NULL
							AND	evr.approved_by IS NULL
						) AS priority';
			$strOrder = 'evr.begin_datetime ASC,evr.created_on ASC ';
		}

		$strSql = ' SELECT
						DISTINCT(e.id),
						e.*,
						evr.*,
						vt.name,
						evr.id employee_vacation_request_id,
						evr.created_on AS requested_on
						' . $strSubSql . '
					FROM
						employee_vacation_requests evr
						JOIN employees e ON ( e.id = evr.employee_id )
						JOIN vacation_types vt ON (vt.id = evr.vacation_type_id )
					WHERE
						e.id IN( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND	e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
						AND evr.deleted_by IS NULL
						AND	( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND ( evr.begin_datetime ) >= ( NOW () - INTERVAL \'2 Month\' )
						AND evr.approved_by IS NULL
						AND evr.approved_on IS NULL
						AND evr.denied_by IS NULL
						AND evr.denied_on IS NULL
						' . $strCondition . '
					ORDER BY
						' . $strOrder;

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchEmployeesVacationTypeRequestsByEmployeeIds( $arrintEmployeeIds, $strHalfYearlyBeginDatetime, $strHalfYearlyEndDatetime, $objDatabase ) {

		$strSql = ' SELECT
						e.id,evr.vacation_type_id,
						sum(evr.scheduled_vacation_hours) AS used_hours
					FROM
						employees e
						JOIN employee_vacation_requests evr ON (e.id = evr.employee_id)
					WHERE
						e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND evr.vacation_type_id in ( ' . CVacationType::PATERNITY . ', ' . CVacationType::MATERNITY . ', ' . CVacationType::MARRIAGE . ', ' . CVacationType::MARRIAGE_ANNIVERSARY . ', ' . CVacationType::OFFICIAL_TOUR . ', ' . CVacationType::OPTIONAL_HOLIDAY . ', ' . CVacationType::COVID_HOME_ISOLATION . ', ' . CVacationType::COVID_HOSPITALIZATION . ' )
						AND evr.end_datetime >= \'' . $strHalfYearlyBeginDatetime . '\'
						AND evr.begin_datetime <= \'' . $strHalfYearlyEndDatetime . '\'
						AND evr.deleted_by IS NULL
						AND evr.deleted_on IS NULL
						AND evr.denied_by IS NULL
						AND evr.denied_on IS NULL
						AND evr.approved_by IS NOT NULL
						AND evr.approved_on IS NOT NULL
					GROUP by
						 e.id,evr.vacation_type_id';

		return fetchdata( $strSql, $objDatabase );

	}

	public static function fetchEmployeesVacationRequestsCountByMonthByYearByCountryCode( $strBeginDatetime, $strEndDatetime, $strCountryCode, $objDatabase ) {

		if( false == valStr( $strBeginDatetime ) || false == valStr( $strEndDatetime ) ) return NULL;

		$intDayOfWeekSaturday	= 6;
		$intDayOfWeekSunday 	= 0;
		$strJoinCondition		= '';
		$strWhereCondition		= '';
		$strSubCondition		= '';

		if( CCountry::CODE_USA == $strCountryCode ) {
			$strJoinCondition = 'LEFT JOIN employees e ON ( e.id = evr.employee_id )
									 LEFT JOIN departments d ON ( d.id = e.department_id ) ';

			$strWhereCondition = ' AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
									AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
									AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
									AND d.id = ' . CDepartment::CALL_CENTER;
		} elseif( strtotime( CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY ) > strtotime( $strBeginDatetime ) && strtotime( CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY ) < strtotime( $strEndDatetime ) ) {
			$strSubCondition = ' OR begin_datetime::date = \'' . CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY . '\'';
		}

		$strSql = 'SELECT
						EXTRACT ( DAY FROM business_day) AS business_day,
						count ( business_day ) AS daily_vacation_requests_count
					FROM
						(
							SELECT
								DISTINCT begin_datetime::date + serial_number AS business_day,
								employee_id
							FROM
								(
									SELECT
										DISTINCT evr.employee_id,
										evr.begin_datetime,
										generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) serial_number
									FROM
										employee_vacation_requests AS evr
										LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id ) ' . $strJoinCondition . '
									WHERE

										ea.country_code = \'' . addslashes( $strCountryCode ) . '\'
										AND evr.denied_by IS NULL
										AND evr.deleted_by IS NULL
										AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
										AND evr.is_paid IS TRUE ' . $strWhereCondition . '
										AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )
								) AS vacation_requests
							WHERE
								begin_datetime::date + serial_number NOT IN (
																				SELECT
																					DATE
																				FROM
																					paid_holidays
																				WHERE
																					country_code = \'' . addslashes( $strCountryCode ) . '\'
																					AND DATE_PART ( \'month\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'month\', DATE )
																					AND DATE_PART ( \'year\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'year\', DATE )
																					AND is_optional IS FALSE
								)
								AND EXTRACT ( \'dow\' FROM begin_datetime::date + serial_number ) NOT IN ( \'' . ( int ) $intDayOfWeekSaturday . '\', \'' . ( int ) $intDayOfWeekSunday . '\' ) ' . $strSubCondition . '
								AND begin_datetime::date + serial_number BETWEEN DATE( \'' . $strBeginDatetime . '\' ) AND DATE( \'' . $strEndDatetime . '\' )
						) AS sub_employee_vacation_requests
					GROUP BY
						business_day
					ORDER BY
						business_day';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchPaginatedLeaveEmployeesByDate( $intPageNo, $intPageSize, $strDate, $strCountryCode, $boolIsHalfDayLeaveEmployees, $boolIsFullDayLeaveEmployees, $objDatabase, $intDepartmentId = NULL, $strOrderByField = NULL, $strOrderByType = NULL, $boolIsDownload = false, $boolIsFromHRReport = false, $boolIsScheduledLeaves = false, $boolIsUnScheduledLeaves = false, $boolIsFromHRDashBoard = false, $strFilteredExplodedSearch = NULL, $boolPresentOnLeave = false, $intVacationTypeId = NULL ) {

		if( false == valStr( $strDate ) ) return NULL;

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$intDayOfWeekSaturday 	= 6;
		$intDayOfWeekSunday 	= 0;
		$strSubCondition		= '';

		if( $strCountryCode == CCountry::CODE_INDIA && strtotime( CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY ) == strtotime( $strDate ) ) {
			$strSubCondition = ' OR begin_datetime::date = \'' . CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY . '\'';
		}
		if( true == $boolPresentOnLeave ) {
			$strSelectCondition 	= ',employee_vacation_requests.total_on_floor_hours';
			$strFromSelectCondition = ',sum ( CASE
											WHEN eh.end_datetime IS NOT NULL AND eh.begin_datetime IS NOT NULL THEN eh.end_datetime - eh.begin_datetime
										ELSE \'00:00:00\'
										END ) OVER ( PARTITION BY eh.employee_id, eh.date, eh.employee_hour_type_id ) AS total_on_floor_hours';
			$strFromJoinCondition	= 'JOIN employee_hours eh ON ( eh.employee_id = evr.employee_id )';
			$strFromWhereCondition	= 'AND eh.date BETWEEN date_trunc ( \'day\',\'' . $strDate . '\' ::DATE )
									   AND date_trunc ( \'day\', \'' . $strDate . '\' ::DATE )';

		}

		$strSql = 'SELECT
						DISTINCT employee_vacation_requests.employee_id,
						employee_vacation_requests.id,
						e.preferred_name AS name_full,
						e.email_address,
						e.employee_number,
						epn.phone_number,
						d.name,
						de.name as designation,
						employee_vacation_requests.approved_on,
						employee_vacation_requests.denied_on,
						employee_vacation_requests.approved_by,
						employee_vacation_requests.denied_by,
						employee_vacation_requests.created_on::date,
						vt.name as vacation_type_name,
						est.name As employee_status,
						employee_vacation_requests.request As reason
						' . $strSelectCondition . '
					FROM
						(
							SELECT
								DISTINCT evr.*,
								generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS serial_number,
								evr.begin_datetime::date + generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS DATE,
								ea.country_code
								' . $strFromSelectCondition . '
							FROM
								employee_vacation_requests AS evr
								LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id )
								' . $strFromJoinCondition . '
							WHERE
								date_trunc ( \'day\', timestamp\'' . $strDate . '\') BETWEEN date_trunc ( \'day\', evr.begin_datetime ) ::date AND date_trunc ( \'day\', evr.end_datetime ) ::date
								AND evr.denied_by IS NULL
								AND evr.deleted_by IS NULL
								AND country_code = \'' . addslashes( $strCountryCode ) . '\'
								AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
								AND evr.is_paid IS TRUE
								' . $strFromWhereCondition . '
						) AS employee_vacation_requests
						JOIN employees AS e ON( employee_vacation_requests.employee_id = e.id )
						JOIN employee_phone_numbers AS epn ON ( employee_vacation_requests.employee_id = epn.employee_id )
						JOIN phone_number_types AS pnt ON ( epn.phone_number_type_id = pnt.id )
						LEFT JOIN departments as d ON ( e.department_id = d.id )
						LEFT JOIN designations as de ON ( e.designation_id = de.id )
						LEFT JOIN employee_status_types est ON ( e.employee_status_type_id = est.id )
						LEFT JOIN vacation_types AS vt ON ( vt.id = employee_vacation_requests.vacation_type_id )';

		if( true == is_numeric( $intVacationTypeId ) ) {
			$strSql .= '
					WHERE
						employee_vacation_requests.vacation_type_id = ' . $intVacationTypeId . ' ';
		} else {
			$strSql .= '
					WHERE
					employee_vacation_requests.date NOT IN (
													SELECT
														DATE
													FROM
														paid_holidays
													WHERE
														country_code = \'' . addslashes( $strCountryCode ) . '\'
														AND DATE_PART ( \'month\', DATE ( \'' . $strDate . '\' ) ) = DATE_PART ( \'month\', DATE )
														AND DATE_PART ( \'year\', DATE ( \'' . $strDate . '\' ) ) = DATE_PART ( \'year\', DATE )
														AND is_optional IS FALSE
													)
					AND EXTRACT ( \'dow\' FROM employee_vacation_requests.date ) NOT IN ( 6, 0 ) ' . $strSubCondition . '
					AND pnt.id= \'' . CPhoneNumberType::PRIMARY . '\'
					AND employee_vacation_requests.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' ) ';

			if( $strCountryCode == CCountry::CODE_INDIA ) {
				if( true == $boolIsHalfDayLeaveEmployees ) {
					$strSql .= 'AND ( ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_trunc( \'day\', employee_vacation_requests.begin_datetime ) = employee_vacation_requests.DATE ) OR ( date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.DATE = date_trunc( \'day\', employee_vacation_requests.end_datetime ) ) )';
				} elseif( true == $boolIsFullDayLeaveEmployees ) {

					$strSql .= 'AND (( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 )
								OR ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number > 0 AND ( employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date ) )
								OR ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 AND employee_vacation_requests.serial_number > 0 )
								OR( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date )) ';
				}
			}
		}

		$strSql .= 'AND employee_vacation_requests.date BETWEEN \'' . $strDate . '\' AND \'' . $strDate . '\'';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= 'AND d.id= ' . ( int ) $intDepartmentId . ' ';
		}

		if( true == $boolIsScheduledLeaves ) {
			$strSql .= 'AND employee_vacation_requests.vacation_type_id = ' . CVacationType::SCHEDULED;
		}

		if( true == $boolIsUnScheduledLeaves ) {
			$strSql .= 'AND employee_vacation_requests.vacation_type_id = ' . CVacationType::UNSCHEDULED;
		}

		if( true == $boolIsFromHRDashBoard ) {
			$strSql .= 'AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSql .= ' AND ( name_full ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR e.email_address ILIKE \'%' . $strFilteredExplodedSearch . '%\' )';
		}

		if( false == $boolIsFromHRReport ) $strSql .= ' ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType;

		if( false == $boolIsDownload ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . '
						LIMIT
							' . ( int ) $intLimit;
		}

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeeVacationRequestsCountByDateByCountryCode( $strDate, $strCountryCode, $boolIsHalfDayLeaveEmployees, $boolIsFullDayLeaveEmployees, $objDatabase, $intDepartmentId = NULL, $boolIsScheduledLeaves = false, $boolIsUnScheduledLeaves = false, $boolIsFromHRDashBoard = false, $strFilteredExplodedSearch = NULL ) {

		if( false == valStr( $strDate ) ) return NULL;

		$intDayOfWeekSaturday 	= 6;
		$intDayOfWeekSunday 	= 0;
		$strSubCondition		= '';
		$strWhereCondition		= '';

		if( $strCountryCode == CCountry::CODE_INDIA && strtotime( CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY ) == strtotime( $strDate ) ) {
			$strSubCondition = ' OR begin_datetime::date = \'' . CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY . '\'';
		}
		if( $strCountryCode == CCountry::CODE_INDIA ) {
			$strWhereCondition .= ' AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' ) ';
		}

		$strSql = 'SELECT
						COUNT ( DISTINCT employee_vacation_requests.employee_id )
					FROM
						(
							SELECT
								DISTINCT evr.*,
								generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS serial_number,
								evr.begin_datetime::date + generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS DATE,
								ea.country_code
							FROM
								employee_vacation_requests AS evr
								LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id )
							WHERE
								date_trunc ( \'day\', timestamp\'' . $strDate . '\') BETWEEN date_trunc ( \'day\', evr.begin_datetime ) ::date AND date_trunc ( \'day\', evr.end_datetime ) ::date
								AND evr.denied_by IS NULL
								AND evr.deleted_by IS NULL
								AND country_code = \'' . addslashes( $strCountryCode ) . '\'
								AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
								AND evr.is_paid IS TRUE
								' . $strWhereCondition . '
						) AS employee_vacation_requests
						JOIN employees AS e ON ( employee_vacation_requests.employee_id = e.id )
						LEFT JOIN departments AS d ON ( e.department_id = d.id )
					WHERE
						employee_vacation_requests.date NOT IN (
																SELECT
																	DATE
																FROM
																	paid_holidays
																WHERE
																	country_code = \'' . addslashes( $strCountryCode ) . '\'
																	AND DATE_PART ( \'month\', DATE ( \'' . $strDate . '\' ) ) = DATE_PART ( \'month\', DATE )
																	AND DATE_PART ( \'year\', DATE ( \'' . $strDate . '\' ) ) = DATE_PART ( \'year\', DATE )
																	AND is_optional IS FALSE
																)
						AND EXTRACT ( \'dow\' FROM employee_vacation_requests.date ) NOT IN ( 6, 0 ) ' . $strSubCondition;

		if( $strCountryCode == CCountry::CODE_INDIA ) {
			if( true == $boolIsHalfDayLeaveEmployees ) {
				$strSql .= 'AND ( ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_trunc( \'day\', employee_vacation_requests.begin_datetime ) = employee_vacation_requests.DATE ) OR ( date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.DATE = date_trunc( \'day\', employee_vacation_requests.end_datetime ) ) )';
			} elseif( true == $boolIsFullDayLeaveEmployees ) {
				$strSql .= 'AND (	( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 )
								OR ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number > 0 AND ( employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date ) )
								OR ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 AND employee_vacation_requests.serial_number > 0 )
								OR( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date )
							)';
			}
		}

			$strSql .= 'AND employee_vacation_requests.date BETWEEN \'' . $strDate . '\' AND \'' . $strDate . '\'';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= 'AND d.id= ' . ( int ) $intDepartmentId . ' ';
		}

		if( true == $boolIsScheduledLeaves ) {
			$strSql .= 'AND employee_vacation_requests.vacation_type_id = ' . CVacationType::SCHEDULED;
		}

		if( true == $boolIsUnScheduledLeaves ) {
			$strSql .= 'AND employee_vacation_requests.vacation_type_id = ' . CVacationType::UNSCHEDULED;
		}

		if( true == $boolIsFromHRDashBoard ) {
			$strSql .= 'AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )';
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSql .= ' AND ( name_full ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR e.email_address ILIKE \'%' . $strFilteredExplodedSearch . '%\' )';
		}

		$arrintEmployeesCount = fetchdata( $strSql, $objDatabase );
		return $arrintEmployeesCount[0]['count'];
	}

	public static function fetchWorkingDaysCountInWeekByDateRangeByCountryCode( $strBeginDate, $strEndDate, $strCountryCode, $objDatabase, $strExceptionalWeeklyoffDay = NULL, $arrstrExceptionalWorkingDays = NULL ) {

		if( false == valStr( $strCountryCode ) || true == is_null( $strBeginDate ) || true == is_null( $strEndDate ) ) return NULL;

		$intDayOfWeekSaturday 	= 6;
		$intDayOfWeekSunday 	= 0;

		if( false == is_null( $strExceptionalWeeklyoffDay ) && true == valArr( $arrstrExceptionalWorkingDays ) ) {
			$strExceptionalWeeklyoffDay		= date( 'm-d-y', strtotime( $strExceptionalWeeklyoffDay ) );

			$arrstrDays = array();

			while( true == valArr( $arrstrExceptionalWorkingDays ) ) {
				$arrstrDays[] = date( 'm-d-y', strtotime( array_pop( $arrstrExceptionalWorkingDays ) ) );
			}

			$strCondition 					= 'AND ( EXTRACT ( \'dow\' FROM business_day ) NOT IN ( ' . ( int ) $intDayOfWeekSaturday . ', ' . ( int ) $intDayOfWeekSunday . ' )
												OR EXTRACT ( \'dow\' FROM business_day ) IN ( ' . ( int ) $intDayOfWeekSaturday . ' ) AND business_day IN ( \'' . implode( '\',\'', $arrstrDays ) . '\' )
												AND business_day != \'' . $strExceptionalWeeklyoffDay . '\' )';
			$strWhereCondition 				= 'WHERE business_day != \'' . $strExceptionalWeeklyoffDay . '\'';
		} else {
			$strCondition					= 'AND EXTRACT ( \'dow\' FROM business_day ) NOT IN ( ' . ( int ) $intDayOfWeekSaturday . ', ' . ( int ) $intDayOfWeekSunday . ' )';
			$strWhereCondition				= '';
		}

		$strSql = '	SELECT
						business_days.business_day,
						EXTRACT ( WEEK FROM ( business_days.business_day ) + \'1 days\'::interval ) AS week_no,
						COUNT (
								CASE WHEN
										business_day NOT IN ( SELECT date FROM paid_holidays WHERE country_code = \'' . $strCountryCode . '\' AND is_optional IS FALSE )
										' . $strCondition . '
									 THEN
										 business_day
								END
						) OVER ( PARTITION BY ( EXTRACT( WEEK FROM ( business_days.business_day ) + \'1 days\'::interval ) ) ) AS day,
						COUNT (
								CASE WHEN
										business_day NOT IN ( SELECT date FROM paid_holidays WHERE country_code = \'' . $strCountryCode . '\' AND is_optional IS FALSE)
										' . $strCondition . '
									 THEN
										 business_day
								END
						) OVER ( PARTITION BY ( EXTRACT( MONTH FROM ( business_days.business_day ) ) ) ) AS monthly_working_days
					FROM (
							SELECT
								DISTINCT date \'' . $strBeginDate . '\' + serial_number AS business_day
							FROM
								generate_series( 0, date \'' . $strEndDate . '\' - date \'' . $strBeginDate . '\') serial_number
						) AS business_days
					' . $strWhereCondition . '
					ORDER BY
						business_day';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsScheduledHoursByDateRangeByCountryCode( $strBeginDateTime, $strEndDateTime, $strCountryCode, $objDatabase, $arrintEmployeeIds = NULL ) {

		if( false == valStr( $strBeginDateTime ) || false == valStr( $strEndDateTime ) ) return NULL;

		if( true == valArr( $arrintEmployeeIds ) ) {
			$strEmployeeIdsCondition = 'AND evr.employee_id IN (\'' . implode( '\', \'', $arrintEmployeeIds ) . '\' )';
		} else {
			$strEmployeeIdsCondition = '';
		}

		$strSql = 'SELECT
						DISTINCT evr.employee_id,
						SUM ( evr.scheduled_vacation_hours ) AS scheduled_hours
					FROM
						employee_vacation_requests evr
						JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\' )
						JOIN employees AS e ON ( e.id = ea.employee_id )
					WHERE
						evr.denied_by IS NULL
						AND evr.deleted_by IS NULL
						AND evr.begin_datetime BETWEEN date_trunc ( \'day\', \'' . $strBeginDateTime . '\' ::date ) AND date_trunc ( \'day\', \'' . $strEndDateTime . '\' ::date )
						AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\'
						AND evr.begin_datetime > e.date_confirmed
						' . $strEmployeeIdsCondition . '
					GROUP BY
						evr.employee_id
					ORDER BY
						evr.employee_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTeamEmployeeVacationRequestsHours( $intManagerId, $strBeginDatetime, $strEndDatetime, $objDatabase, $arrstrAttendanceFilter, $intVacationTypeId = NULL ) {

		if( true == is_null( $intManagerId ) && false == valStr( $strBeginDatetime ) || false == valStr( $strEndDatetime ) ) return NULL;

		$boolIsShowAll		= ( true == isset( $arrstrAttendanceFilter['show_all'] ) ) ? $arrstrAttendanceFilter['show_all'] : false;
		$intOffset			= ( true == isset( $arrstrAttendanceFilter['page_no'] ) && true == isset( $arrstrAttendanceFilter['page_size'] ) ) ? $arrstrAttendanceFilter['page_size'] * ( $arrstrAttendanceFilter['page_no'] - 1 ) : 0;
		$intLimit			= ( true == isset( $arrstrAttendanceFilter['page_size'] ) ) ? $arrstrAttendanceFilter['page_size'] : '';
		$strOrderByField	= ' employee_name';
		$strOrderByType 	= ' ASC';
		$strCondition		= ( true == is_numeric( $intVacationTypeId ) ) ? ' AND evr.vacation_type_id = ' . ( int ) $intVacationTypeId : ' AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';
		$strWhereClause = '';

		if( true == valArr( $arrstrAttendanceFilter['qam_employee_ids'] ) ) {
			$strWhereClause	.= 'e.id IN (' . implode( ',', $arrstrAttendanceFilter['qam_employee_ids'] ) . ') OR ';
		}

		if( true == valArr( $arrstrAttendanceFilter ) ) {
			if( true == isset( $arrstrAttendanceFilter['order_by_field'] ) ) {

				switch( $arrstrAttendanceFilter['order_by_field'] ) {
					case 'employee_name':
						$strOrderByField = ' e.preferred_name ';
						break;

					case 'team_name':
						$strOrderByField = ' team_name ';
						break;

					case 'total_leaves_alloted':
						$strOrderByField = ' total_leaves_alloted ';
						break;

					default:
						$strOrderByField = ' e.preferred_name ';
						break;
				}
			}

			if( true == isset( $arrstrAttendanceFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrAttendanceFilter['order_by_type'] ) ? ' ASC ' : ' DESC ';
			}
		}

		$strFromClause = ' employees e ';
		$strJoinClause = ' LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
							LEFT JOIN teams t ON ( te.team_id = t.id )';

		if( true == valArr( $arrstrAttendanceFilter['dev_qa_employee_ids'] ) ) {
			$strWhereClause	.= ' ( e.reporting_manager_id = ' . ( int ) $intManagerId . ' OR e.id IN ( ' . implode( ',', $arrstrAttendanceFilter['dev_qa_employee_ids'] ) . ' ) ) AND ';
		} else {
			$strWhereClause	.= ' e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND ';
		}

		if( true == $boolIsShowAll ) {
			$strFromClause = ' all_employees ';
			$strJoinClause = ' JOIN employees e ON ( all_employees.id = e.id ) LEFT JOIN teams t ON ( all_employees.team_id = t.id ) ';
			$strWhereClause = '';
		}

		$strRecursiveSql = ' WITH RECURSIVE all_employees( id, team_id ) AS (
							SELECT te.employee_id, te.team_id
							FROM team_employees te 
							LEFT JOIN employees e ON ( e.id = te.employee_id )
							WHERE e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND te.is_primary_team = 1 AND te.employee_id <> ' . ( int ) $intManagerId . '
							UNION ALL
							SELECT te.employee_id, te.team_id
							FROM team_employees te
							LEFT JOIN employees e ON ( e.id = te.employee_id ),
							 all_employees al 
							WHERE e.reporting_manager_id = al.id AND te.is_primary_team = 1 AND te.employee_id <> ' . ( int ) $intManagerId . ' ) ';

		$strSql = ' SELECT
						e.id AS employee_id,
						t.id AS team_id,
						e.preferred_name AS employee_name,
						t.name as team_name,
						ea.country_code,
						MAX( evr.actual_vacation_hours::FLOAT / 8 ) as actual_vacation_hours,
						( evs.hours_granted::FLOAT / 8 ) AS total_leaves_alloted,
						SUM( CASE WHEN evr.approved_on IS NOT NULL  AND evr.vacation_type_id NOT IN (' . CVacationType::MARRIAGE_ANNIVERSARY . ',' . CVacationType::MATERNITY . ', ' . CVacationType::OFFICIAL_TOUR . ',' . CVacationType::MARRIAGE . ',' . CVacationType::PATERNITY . ', ' . CVacationType::BEREAVEMENT . ', ' . CVacationType::OPTIONAL_HOLIDAY . ' ) THEN evr.scheduled_vacation_hours ELSE NULL END )/8::FLOAT AS total_used,
						d.name AS designation_name
					FROM
						' . $strFromClause . $strJoinClause . '
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.country_code IS NOT NULL AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employee_vacation_schedules evs ON ( evs.employee_id = e.id AND evs.year = EXTRACT ( YEAR FROM NOW ( ) ) )
						LEFT JOIN employee_vacation_requests evr ON ( evr.employee_id = evs.employee_id AND evr.deleted_on IS NULL AND evr.denied_on IS NULL AND evr.begin_datetime BETWEEN DATE ( \'' . $strBeginDatetime . '\' ) AND DATE ( \'' . $strEndDatetime . '\' ) AND evr.is_paid IS TRUE' . $strCondition . ' )
						LEFT JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						' . $strWhereClause . '
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id <> ' . ( int ) $intManagerId . '
					GROUP BY
						e.id,
						t.id,
						e.preferred_name,
						evs.year,
						evs.hours_granted,
						d.name,
						ea.country_code,
						t.name
					ORDER BY ' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( true == isset( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		if( true == $boolIsShowAll ) $strSql = $strRecursiveSql . $strSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsByDateRangeByCountryCode( $strBeginDate, $strEndDate, $strCountryCode, $objDatabase, $intEmployeeId = NULL, $boolDeniedBy = true, $arrintEmployeeIds = NULL, $boolIsPaidLeave = false, $strExceptionalWeeklyoffDay = NULL, $arrstrExceptionalWorkingDays = NULL ) {

		if( false == valStr( $strBeginDate ) || false == valStr( $strEndDate ) ) return NULL;

		if( true == valArr( $arrintEmployeeIds ) ) {
			$strEmployeeIdsCondition = 'AND evr.employee_id IN (\'' . implode( '\', \'', $arrintEmployeeIds ) . '\' )';
		} else {
			$strEmployeeIdsCondition = '';
		}

		$strIsPaidLeaveCondition = ( true == $boolIsPaidLeave ) ? 'AND employee_vacation_requests.is_paid IS TRUE' : '';

		$strFullDayLeave		= 'full_day_leave';
		$strFirstHalfLeave		= 'first_half_leave';
		$strSecondHalfLeave		= 'second_half_leave';

		$strEmployeeIdCondition	= ( true == is_numeric( $intEmployeeId ) ) ? 'AND evr.employee_id = ' . $intEmployeeId . '' : '';
		$strDeniedByCondition	= ( true == $boolDeniedBy ) ? 'AND evr.denied_by IS NULL' : '';

		if( false == is_null( $strExceptionalWeeklyoffDay ) && true == valStr( $strExceptionalWeeklyoffDay ) ) {
			$strExceptionalWeeklyoffDay = date( 'd-M-y', strtotime( $strExceptionalWeeklyoffDay ) );
			$strWhereCondition = ' AND employee_vacation_requests.date != \'' . $strExceptionalWeeklyoffDay . '\'';
		} else {
			$strWhereCondition = '';
		}

		if( true == valArr( $arrstrExceptionalWorkingDays ) ) {
			$arrstrDays = array();

			while( true == valArr( $arrstrExceptionalWorkingDays ) ) {
				$arrstrDays[] = date( 'd-M-y', strtotime( array_pop( $arrstrExceptionalWorkingDays ) ) );
			}

			$strCondition = ' OR EXTRACT (\'dow\' FROM
								employee_vacation_requests.date) IN ( ' . CEmployeeVacationRequest::DAY_OF_WEEK_SATURDAY . ' ) AND employee_vacation_requests.date IN ( \'' . implode( '\',\'', $arrstrDays ) . '\' )';
		} else {
			$strCondition = '';
		}

		$strSql = 'SELECT
						employee_vacation_requests.employee_id,
						employee_vacation_requests.id AS vacation_request_id,
						employee_vacation_requests.date,
						employee_vacation_requests.is_paid,
						employee_vacation_requests.denied_by,
						employee_vacation_requests.approved_by,
						employee_vacation_requests.vacation_type_id,
						EXTRACT ( DAY FROM begin_datetime::date + serial_number ) AS day,
						EXTRACT ( WEEK FROM begin_datetime::date + serial_number + \'1 days\'::interval ) AS week_no,
						CASE
							WHEN ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 ) THEN \'' . $strFullDayLeave . '\'
							WHEN ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number > 0 AND ( employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date ) ) THEN \'' . $strFullDayLeave . '\'
							WHEN ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 AND employee_vacation_requests.serial_number > 0 ) THEN \'' . $strFullDayLeave . '\'
							WHEN ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date ) THEN \'' . $strFullDayLeave . '\'
							WHEN ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_trunc( \'day\', employee_vacation_requests.begin_datetime ) = DATE ) THEN \'' . $strSecondHalfLeave . '\'
							WHEN ( date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND DATE = date_trunc( \'day\', employee_vacation_requests.end_datetime ) ) THEN \'' . $strFirstHalfLeave . '\'
						END AS status,
						vt.name AS vacation_type,
						employee_vacation_requests.scheduled_vacation_hours
					FROM
						(
							SELECT
								DISTINCT evr.id,
								evr.employee_id,
								evr.vacation_type_id,
								evr.begin_datetime,
								evr.end_datetime,
								evr.scheduled_vacation_hours,
								evr.is_paid,
								evr.denied_by,
								evr.approved_by,
								generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS serial_number,
								evr.begin_datetime::date + generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS date
							FROM
								employee_vacation_requests evr
								LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id )
							WHERE
								evr.deleted_by IS NULL
								' . $strDeniedByCondition . '
								' . $strEmployeeIdCondition . '
								AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
								AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\'
								' . $strEmployeeIdsCondition . '
						) AS employee_vacation_requests
						JOIN vacation_types AS vt ON ( employee_vacation_requests.vacation_type_id = vt.id )
						JOIN employees AS e ON( employee_vacation_requests.employee_id = e.id )
					WHERE
						employee_vacation_requests.date BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\'
						' . $strWhereCondition . '
						AND( employee_vacation_requests.date <= e.date_terminated OR e.date_terminated IS NULL )
						' . $strIsPaidLeaveCondition . '
						' . $strCondition . '
					ORDER BY
						employee_vacation_requests.employee_id,
						employee_vacation_requests.date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeVacationRequestsHoursCount( $intManagerId, $strBeginDatetime, $strEndDatetime, $objDatabase, $arrstrAttendanceFilter ) {

		if( true == is_null( $intManagerId ) && false == valStr( $strBeginDatetime ) || false == valStr( $strEndDatetime ) ) return NULL;

		$boolIsShowAll = ( true == isset( $arrstrAttendanceFilter['show_all'] ) ) ? $arrstrAttendanceFilter['show_all'] : false;

		$strFromClause = ' 	employees e ';
		$strJoinClause = ' ';

		if( true == valArr( $arrstrAttendanceFilter['dev_qa_employee_ids'] ) ) {
			$strWhereClause	= ' ( e.reporting_manager_id = ' . ( int ) $intManagerId . ' OR e.id IN ( ' . implode( ',', $arrstrAttendanceFilter['dev_qa_employee_ids'] ) . ' ) ) AND ';
		} else {
			$strWhereClause	= ' e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND ';
		}

		if( true == $boolIsShowAll ) {
			$strFromClause = ' all_employees ';
			$strJoinClause = ' JOIN employees e ON ( all_employees.id = e.id ) ';
			$strWhereClause = '';
		}

		$strRecursiveSql = ' WITH RECURSIVE all_employees( id ) AS (
							SELECT e.id
							FROM employees e 
							WHERE e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND e.id <> ' . ( int ) $intManagerId . '
							UNION ALL
							SELECT e.id
							FROM employees e, all_employees al 
							WHERE e.reporting_manager_id = al.id AND e.id <> ' . ( int ) $intManagerId . ' ) ';

		$strSql = ' SELECT
						count( sub_query.* )
					FROM(
						SELECT
								e.id AS employee_id,
								e.name_full AS employee_name,
								ea.country_code,
								MAX (evr.actual_vacation_hours/8::FLOAT ) as actual_vacation_hours,
								( evs.hours_granted / 8 )::FLOAT AS total_leaves_alloted,
								SUM( CASE WHEN evr.approved_on IS NOT NULL THEN evr.scheduled_vacation_hours ELSE NULL END )/8::FLOAT AS total_used,
								d.name AS designation_name
						FROM ' . $strFromClause . $strJoinClause . '
								LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.country_code IS NOT NULL AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
								LEFT JOIN employee_vacation_schedules evs ON ( evs.employee_id = e.id AND evs.year = EXTRACT ( YEAR FROM NOW ( ) ) )
								LEFT JOIN employee_vacation_requests evr ON ( evr.employee_id = evs.employee_id AND evr.deleted_on IS NULL AND evr.denied_on IS NULL AND evr.begin_datetime BETWEEN DATE ( \'' . $strBeginDatetime . '\' ) AND DATE ( \'' . $strEndDatetime . '\' ) )
								LEFT JOIN designations d ON ( e.designation_id = d.id )
						WHERE
								' . $strWhereClause . '
								e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
								AND e.id <> ' . ( int ) $intManagerId . '
						GROUP BY
								e.id,
								e.name_full,
								evs.year,
								evs.hours_granted,
								d.name,
								ea.country_code
								) AS sub_query';

		if( true == $boolIsShowAll ) $strSql = $strRecursiveSql . $strSql;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;

	}

	public static function fetchTeamApprovedEmployeeUsedLeaves( $intManagerId,$arrintEmployeeIds, $strBeginDatetime, $strEndDatetime, $objDatabase, $strType = NULL, $intVacationTypeId = NULL ) {

		if( true == is_null( $intManagerId ) && false == valStr( $strBeginDatetime ) || false == valStr( $strEndDatetime ) ) return NULL;
		$strWhereCondition = '';
		$strSubSql		 = '';

		if( true == is_numeric( $intVacationTypeId ) ) {
			$strWhereCondition .= 'AND evr.vacation_type_id = ' . ( int ) $intVacationTypeId;
		} else {
			$strWhereCondition .= 'AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';
			$strSubSql	 = ', (
							SELECT data.priority
							 FROM (
									select evr1.id,
									rank() OVER(
									ORDER BY evr1.created_on ) AS priority
									from employee_vacation_requests evr1
									JOIN employees e ON (e.id = evr1.employee_id)
									where ( (
											( evr1.begin_datetime::date BETWEEN evr.begin_datetime::date and evr.end_datetime::date )
											 or
											( evr1.end_datetime::date BETWEEN evr.begin_datetime::date and evr.end_datetime::date )
												) OR (
											( evr.begin_datetime::date BETWEEN evr1.begin_datetime::date and evr1.end_datetime::date )
											or
											( evr.end_datetime::date BETWEEN evr1.begin_datetime::date and evr1.end_datetime::date )
							) ) AND e.id IN( ' . implode( ',', $arrintEmployeeIds ) . ' )
								AND evr1.deleted_on IS NULL
								AND evr1.denied_by IS NULL
								AND	evr1.approved_by IS NULL
							ORDER BY evr.created_on
								) AS data
								WHERE data.id = evr.id
								AND evr.deleted_on IS NULL
								AND evr.denied_by IS NULL
								AND	evr.approved_by IS NULL
						) AS priority';
		}

		if( 'scheduled_leave' == $strType ) {

			$strWhereCondition .= ' AND evr.begin_datetime > NOW() ';

		} else {

			$strWhereCondition .= ' AND evr.begin_datetime BETWEEN \'' . $strBeginDatetime . '\' AND \'' . $strEndDatetime . '\' ';
		}

		$strSql = ' SELECT
						e.id AS employee_id,
						evr.id AS employee_vacation_request_id,
						evr.vacation_type_id,
						evr.created_on,
						evr.approved_by,
						evr.approved_on,
						evr.denied_by,
						evr.denied_on,
						evr.begin_datetime,
						evr.end_datetime,
						evr.scheduled_vacation_hours AS scheduled_hours,
						evr.request AS comment,
						e.preferred_name AS employee_name,
						vt.name AS request_type,
						ea.country_code,
						( CAST(evr.scheduled_vacation_hours AS FLOAT)/8 ) AS scheduled_days
						' . $strSubSql . '
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id )
						JOIN employees e ON ( te.employee_id = e.id AND e.date_terminated IS NULL AND te.is_primary_team = 1)
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.country_code IS NOT NULL AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employee_vacation_requests evr ON (evr.employee_id = e.id)
						LEFT JOIN vacation_types vt ON( evr.vacation_type_id = vt.id)
					WHERE
						e.date_terminated IS NULL
						AND e.id = ' . ( int ) $intManagerId . '
						AND e.employee_status_type_id = 1
						AND evr.deleted_on IS NULL
						AND evr.is_paid IS TRUE
						' . $strWhereCondition . '
					ORDER BY evr.id DESC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPaginatedTeamEmployeesVacationRequestsByDateRangeByManagerEmployeeId( $strStartDate, $strEndDate, $intManagerEmployeeId, $arrintEmployeeIds, $arrstrVacationRequestsFilter, $objDatabase, $intVacationTypeId = NULL ) {

		if( true == is_null( $intManagerEmployeeId ) || false == valArr( $arrintEmployeeIds ) ) return NULL;

		$boolIsShowAll			= ( true == isset( $arrstrVacationRequestsFilter['show_all'] ) ) ? $arrstrVacationRequestsFilter['show_all'] : false;
		$intOffset				= ( true == isset( $arrstrVacationRequestsFilter['page_no'] ) && true == isset( $arrstrVacationRequestsFilter['page_size'] ) ) ? $arrstrVacationRequestsFilter['page_size'] * ( $arrstrVacationRequestsFilter['page_no'] - 1 ) : 0;
		$intLimit				= ( true == isset( $arrstrVacationRequestsFilter['page_size'] ) ) ? $arrstrVacationRequestsFilter['page_size'] : '';
		$strOrderByField 		= ' e.id ';
		$strOrderByType			= ' ASC';

		if( true == valArr( $arrstrVacationRequestsFilter ) ) {
			if( true == isset( $arrstrVacationRequestsFilter['order_by_field'] ) ) {

				switch( $arrstrVacationRequestsFilter['order_by_field'] ) {

					case 'name_full':
						$strOrderByField = ' e.preferred_name ';
						break;

					case 'request_type':
						$strOrderByField = ' request_type ';
						break;

					case 'comment':
						$strOrderByField = ' comment ';
						break;

					case 'created_on':
						$strOrderByField = ' evr.created_on ';
						break;

					default:
						$strOrderByField = ' e.preferred_name ';
						break;
				}
			}

			if( true == isset( $arrstrVacationRequestsFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrVacationRequestsFilter['order_by_type'] ) ? ' ASC ' : ' DESC ';
			}
		}

		$strFromClause 	= ' employees e ';
		$strJoinClause	= ' ';

		if( true == valArr( $arrstrVacationRequestsFilter['dev_qa_employee_ids'] ) ) {
			$strWhereClause	= ' AND ( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN (' . implode( ',', $arrstrVacationRequestsFilter['dev_qa_employee_ids'] ) . ') ) ';
			$strWhere		= ' ( e.reporting_manager_id  = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN (' . implode( ',', $arrstrVacationRequestsFilter['dev_qa_employee_ids'] ) . ') ) AND ';
		} else {
			$strWhereClause	= ' AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;
			$strWhere		= ' e.reporting_manager_id  = ' . ( int ) $intManagerEmployeeId . ' AND ';
		}

		if( true == $boolIsShowAll ) {
			$strFromClause 	= ' all_employees ';
			$strJoinClause	= ' JOIN employees e ON ( all_employees.id = e.id )';
			$strWhereClause	= '';
		}

		if( true == valArr( $arrstrVacationRequestsFilter['qam_employee_ids'] ) ) {
			$strWhereClause	.= ' OR e.id IN (' . implode( ',', $arrstrVacationRequestsFilter['qam_employee_ids'] ) . ') ';
		}

		$strSubSql = '';

		if( true == is_numeric( $intVacationTypeId ) ) {
			$strWhereClause .= 'AND evr.vacation_type_id = ' . ( int ) $intVacationTypeId;
		} else {
			$strWhereClause .= 'AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' ) ';
			$strSubSql = ',(
							SELECT data.priority
							FROM (
									select evr1.id,
									rank() OVER(
									ORDER BY evr1.created_on ) AS priority
									from employee_vacation_requests evr1
										JOIN employees e ON (e.id = evr1.employee_id)
									where ( (
											( evr1.begin_datetime::date BETWEEN evr.begin_datetime::date and evr.end_datetime::date )
											 or
											( evr1.end_datetime::date BETWEEN evr.begin_datetime::date and evr.end_datetime::date )
												) OR (
											( evr.begin_datetime::date BETWEEN evr1.begin_datetime::date and evr1.end_datetime::date )
											or
											( evr.end_datetime::date BETWEEN evr1.begin_datetime::date and evr1.end_datetime::date )
							) ) AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
								AND evr1.deleted_on IS NULL
								AND evr1.denied_by IS NULL
								AND	evr1.approved_by IS NULL
							ORDER BY evr.created_on
								) AS data
								WHERE data.id = evr.id
								AND evr.denied_by IS NULL
								AND	evr.approved_by IS NULL
								AND evr.deleted_on IS NULL
						) AS priority';
		}

		// for recursively finding all team employees.
		$strRecursiveSql = ' WITH RECURSIVE all_employees( id ) AS (
								SELECT e.id
								FROM employees e 
								WHERE ' . $strWhere . ' e.id <> ' . ( int ) $intManagerEmployeeId . '
								UNION ALL
								SELECT e.id
								FROM employees e,
								all_employees al 
								WHERE e.reporting_manager_id  = al.id  AND e.id <> ' . ( int ) $intManagerEmployeeId . ' ) ';

		$strSql = ' SELECT
						DISTINCT( evr.id ) AS employee_vacation_request_id,
						e.id AS employee_id,
						evr.created_on,
						evr.approved_by,
						evr.approved_on,
						evr.denied_by,
						evr.denied_on,
						evr.begin_datetime,
						evr.end_datetime,
						evr.scheduled_vacation_hours AS scheduled_hours,
						evr.actual_vacation_hours AS actual_hours,
						evr.request AS comment,
						vt.name AS request_type,
						ea.country_code AS country_code,
						e.preferred_name AS name_full,
						(CAST(evr.scheduled_vacation_hours AS FLOAT)/8 ) AS scheduled_days,
						u.employee_id AS leave_response_by_employee_id
						' . $strSubSql . '
					FROM
						' . $strFromClause . $strJoinClause . '
						JOIN employee_addresses ea ON(ea.employee_id=e.id AND ea.country_code IS NOT NULL)
						LEFT JOIN employee_vacation_requests evr ON (evr.employee_id = e.id )
						LEFT JOIN vacation_types vt ON( evr.vacation_type_id = vt.id )
						JOIN users u ON ( CASE
												WHEN evr.approved_by IS NULL AND evr.denied_by IS NULL THEN evr.employee_id = u.employee_id
												WHEN evr.approved_by IS NULL AND evr.denied_by IS NOT NULL THEN evr.denied_by = u.id
												WHEN evr.approved_by IS NOT NULL AND evr.denied_by IS NULL THEN evr.approved_by = u.id
											END )
					WHERE
						e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND e.date_terminated IS NULL
						' . $strWhereClause . '
						AND e.id <> ' . ( int ) $intManagerEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND evr.begin_datetime BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . ' 23:59:59\'
						AND evr.deleted_by IS NULL
						AND evr.deleted_on IS NULL
						AND evr.is_paid IS TRUE
					ORDER BY ' . $strOrderByField . $strOrderByType . ' OFFSET ' . ( int ) $intOffset;

			if( true == isset( $intLimit ) && 0 < $intLimit ) {
				$strSql .= ' LIMIT	' . ( int ) $intLimit;
			}

			if( true == $boolIsShowAll ) $strSql = $strRecursiveSql . $strSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountTeamEmployeesVacationRequestsByDateRangeByManagerEmployeeId( $strStartDate, $strEndDate, $intManagerEmployeeId, $boolIsShowAll = false, $objDatabase, $intVacationTypeId = NULL ) {

		$strFromClause 	= ' employees e ';
		$strJoinClause	= ' ';
		$strWhereClause	= ' AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;

		if( true == $boolIsShowAll ) {
			$strFromClause 	= ' all_employees ';
			$strJoinClause	= ' JOIN employees e ON ( all_employees.id = e.id )';
			$strWhereClause	= '';
		}

		if( true == is_numeric( $intVacationTypeId ) ) {
			$strCondition = 'AND evr.vacation_type_id = ' . ( int ) $intVacationTypeId;
		} else {
			$strCondition = 'AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' ) ';
		}
		// for recursively finding all team employees.
		$strRecursiveSql = ' WITH RECURSIVE all_employees( id ) AS (
								SELECT e.id 
								FROM employees e 
								WHERE e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' AND e.id  <> ' . ( int ) $intManagerEmployeeId . '
								UNION ALL
								SELECT e.id 
								FROM employees e,all_employees al 
								WHERE e.reporting_manager_id = al.id AND  e.id  <> ' . ( int ) $intManagerEmployeeId . ' ) ';

		$strSql = ' SELECT
						count( e.id )
					FROM
						' . $strFromClause . $strJoinClause . '
						LEFT JOIN employee_vacation_requests evr ON (evr.employee_id = e.id)
						LEFT JOIN vacation_types vt ON( evr.vacation_type_id = vt.id)
					WHERE
						e.date_terminated IS NULL
						' . $strWhereClause . '
						AND e.id <> ' . ( int ) $intManagerEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND evr.begin_datetime BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
						AND evr.deleted_by IS NULL
						AND evr.deleted_on IS NULL
						' . $strCondition . '
						AND evr.is_paid IS TRUE';

		if( true == $boolIsShowAll ) $strSql = $strRecursiveSql . $strSql;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchTeamEmployeeFutureRequests( $intManagerId, $strBeginDatetime, $objDatabase, $intVacationTypeId = NULL, $arrintDevQaEmployeeIds = NULL ) {

		if( true == is_null( $intManagerId ) && false == valStr( $strBeginDatetime ) ) return NULL;

		$strWhereCondition	= ( true == is_numeric( $intVacationTypeId ) ) ? ' AND evr.vacation_type_id = ' . ( int ) $intVacationTypeId : ' AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';

		if( true == valArr( $arrintDevQaEmployeeIds ) ) {
			$strWhere = ' ( e.reporting_manager_id = ' . ( int ) $intManagerId . ' OR e.id IN ( ' . implode( ',', $arrintDevQaEmployeeIds ) . ' ) ) ';
		} else {
			$strWhere = ' e.reporting_manager_id = ' . ( int ) $intManagerId;
		}

		$strSql = '	SELECT
						e.id AS employee_id,
						SUM(CASE WHEN evr.begin_datetime >= NOW() THEN evr.scheduled_vacation_hours ELSE NULL END) / 8::FLOAT AS scheduled_feture
					FROM employees e
						LEFT JOIN employee_addresses ea ON (ea.employee_id = e.id AND ea.country_code IS NOT NULL AND ea.address_type_id = ' . CAddressType::PRIMARY . ')
						LEFT JOIN employee_vacation_requests evr ON (evr.employee_id = ea.employee_id AND evr.deleted_on IS NULL AND evr.denied_on IS NULL AND evr.begin_datetime>= NOW())
						LEFT JOIN designations d ON (e.designation_id = d.id)
					WHERE ' . $strWhere . ' AND
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id <> ' . ( int ) $intManagerId . '
						' . $strWhereCondition . '
					GROUP BY
						e.id';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchIndianEmployeesVacationDetailsByEmployeeId( $intManagerId, $objDatabase ) {

		if( true == is_null( $intManagerId ) ) return NULL;

		$strSql = '	SELECT
						evr.id AS employee_vacation_request_id,
						evr.approved_by,
						evr.approved_on,
						evr.denied_by,
						evr.denied_on,
						evr.begin_datetime,
						evr.end_datetime,
						evr.scheduled_vacation_hours AS scheduled_hours,
						EXTRACT(MONTH FROM evr.begin_datetime) || \'-\' || EXTRACT(YEAR FROM evr.begin_datetime) AS MonthYear,
						(CASE
							WHEN ( date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc( \'month\' , NOW()) AND date_trunc(\'month\', NOW()) + INTERVAL \'6 Months\' AND evr.denied_on IS NULL) THEN evr.scheduled_vacation_hours
								ELSE 0
						END) / 8::FLOAT AS scheduled_future,
						(CASE
							WHEN (date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) - INTERVAL \'6 Months\' AND date_trunc( \'month\' , NOW()) AND evr.denied_on IS NULL) THEN evr.scheduled_vacation_hours
								ELSE 0
						END) / 8::FLOAT AS leaves_taken,
						(CASE
							WHEN (date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) - INTERVAL \'6 Months\' AND date_trunc(\'month\', NOW()) + INTERVAL \'6 Months\' AND evr.denied_on IS NOT NULL) THEN evr.scheduled_vacation_hours
								ELSE 0
						END) / 8::FLOAT AS denied_leave,
						(CAST (evr.scheduled_vacation_hours AS FLOAT) / 8) AS scheduled_days
					FROM
						teams t
						JOIN team_employees te ON (t.id = te.team_id)
						JOIN employees e ON (te.employee_id = e.id AND e.date_terminated IS NULL AND te.is_primary_team = 1)
						LEFT JOIN employee_addresses ea ON (ea.employee_id = e.id AND ea.country_code IS NOT NULL AND ea.address_type_id = ' . CAddressType::PRIMARY . ')
						LEFT JOIN employee_vacation_requests evr ON (evr.employee_id = e.id)
						LEFT JOIN vacation_types vt ON (evr.vacation_type_id = vt.id)
					WHERE
						e.date_terminated IS NULL
						AND e.id = ' . ( int ) $intManagerId . '
						AND date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) - INTERVAL \'6 Months\' AND date_trunc(\'month\', NOW()) + INTERVAL \'6 Months\'
						AND e.employee_status_type_id = 1
						AND evr.deleted_on IS NULL
					ORDER BY
						evr.begin_datetime';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUSEmployeesPastVacationDetailsByEmployeeId( $intManagerId, $objDatabase ) {

		if( true == is_null( $intManagerId ) ) return NULL;

		$strSql = '	SELECT
						evr.id AS employee_vacation_request_id,
						evr.begin_datetime,
						evr.end_datetime,
						evr.approved_by,
						evr.approved_on,
						evr.denied_by,
						evr.denied_on,
						ea.country_code,
						(CASE
							WHEN (date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) - INTERVAL \'6 Months\' AND date_trunc(\'month\', NOW()) AND evr.denied_on IS NULL ) THEN evr.scheduled_vacation_hours
							 ELSE 0
						END) AS schduled_leaves_taken,
						(CASE
							WHEN (date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) - INTERVAL \'6 Months\' AND date_trunc(\'month\', NOW()) AND evr.denied_on IS NULL ) THEN evr.actual_vacation_hours
							 ELSE 0
						END) AS actual_leaves_taken,
						(CASE
							WHEN (date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) - INTERVAL \'6 Months\' AND date_trunc(\'month\', NOW()) AND evr.denied_on IS NOT NULL) THEN evr.scheduled_vacation_hours
							ELSE 0
						END) AS denied_leave,
						(CASE
							WHEN (date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) - INTERVAL \'6 Months\' AND date_trunc(\'month\', NOW()) AND evr.denied_on IS NOT NULL) THEN evr.actual_vacation_hours
							ELSE 0
						END) AS actual_denied_leave,
						EXTRACT(MONTH FROM evr.begin_datetime) || \'-\' || EXTRACT(YEAR FROM evr.begin_datetime) AS MonthYear,
						evr.scheduled_vacation_hours AS scheduled_days
					FROM
						employees e
						LEFT JOIN team_employees te ON (e.id = te.employee_id AND te.is_primary_team = 1 AND e.date_terminated IS NULL )
						LEFT JOIN employee_addresses ea ON (ea.employee_id = e.id AND ea.country_code IS NOT NULL AND ea.address_type_id = ' . CAddressType::PRIMARY . ')
						LEFT JOIN employee_vacation_requests evr ON (evr.employee_id = e.id)
						LEFT JOIN vacation_types vt ON (evr.vacation_type_id = vt.id)
					WHERE
						e.date_terminated IS NULL
						AND e.id = ' . ( int ) $intManagerId . '
						AND date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) - INTERVAL \'6 Months\' AND date_trunc(\'month\', NOW())
						AND e.employee_status_type_id = 1
						AND evr.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUSEmployeesFutureVacationDetailsByEmployeeId( $intManagerId, $objDatabase ) {

		if( true == is_null( $intManagerId ) ) return NULL;

		$strSql = '	SELECT
						evr.id AS employee_vacation_request_id,
						evr.begin_datetime,
						evr.end_datetime,
						evr.approved_by,
						evr.approved_on,
						evr.denied_by,
						evr.denied_on,
						ea.country_code,
						(CASE
							WHEN (date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) -INTERVAL \'1 months\' AND date_trunc(\'month\', NOW()) + INTERVAL \'6 Months\' AND evr.denied_on IS NULL ) THEN evr.scheduled_vacation_hours
								ELSE 0
						END) AS schduled_leaves_taken,
						(CASE
							WHEN (date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) -INTERVAL \'1 months\' AND date_trunc(\'month\', NOW()) + INTERVAL \'6 Months\' AND evr.denied_on IS NULL ) THEN evr.actual_vacation_hours
								ELSE 0
						END) AS actual_leaves_taken,
						EXTRACT(MONTH FROM evr.begin_datetime) || \'-\' || EXTRACT(YEAR FROM evr.begin_datetime) AS MonthYear,
						(CASE
							WHEN (date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) -INTERVAL \'1 months\' AND date_trunc(\'month\', NOW()) + INTERVAL \'6 Months\' AND evr.denied_on IS NOT NULL) THEN evr.scheduled_vacation_hours
								ELSE 0
							END) AS denied_leave,
						(CASE
							WHEN (date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) -INTERVAL \'1 months\' AND date_trunc(\'month\', NOW()) + INTERVAL \'6 Months\' AND evr.denied_on IS NOT NULL) THEN evr.actual_vacation_hours
								ELSE 0
						END) AS actual_denied_leave,
						evr.scheduled_vacation_hours AS scheduled_days
					FROM
						employees e
						LEFT JOIN team_employees te ON (e.id = te.employee_id AND te.is_primary_team = 1 AND e.date_terminated IS NULL )
						LEFT JOIN employee_addresses ea ON (ea.employee_id = e.id AND ea.country_code IS NOT NULL AND ea.address_type_id = ' . CAddressType::PRIMARY . ')
						LEFT JOIN employee_vacation_requests evr ON (evr.employee_id = e.id)
						LEFT JOIN vacation_types vt ON (evr.vacation_type_id = vt.id)
					WHERE
						e.date_terminated IS NULL
						AND e.id = ' . ( int ) $intManagerId . '
						AND date_trunc(\'month\', evr.begin_datetime) BETWEEN date_trunc(\'month\', NOW()) -INTERVAL \'1 months\' AND date_trunc(\'month\', NOW()) + INTERVAL \'6 Months\'
						AND e.employee_status_type_id = 1
						AND evr.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsByEmployeeIdsByDate( $arrintEmployeeIds, $strDate, $boolIsApprovedOnly = false, $objDatabase ) {

		if( false == valStr( $strDate ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						employee_vacation_requests evr
					WHERE
						employee_id in ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND \'' . $strDate . '\' BETWEEN DATE_TRUNC( \'day\', evr.begin_datetime ) AND DATE_TRUNC( \'day\', evr.end_datetime )
						AND denied_on IS NULL
						AND deleted_on IS NULL';

		if( false != $boolIsApprovedOnly ) $strSql .= ' AND approved_by IS NOT NULL AND denied_by IS NULL';
		$strSql .= ' ORDER BY employee_id DESC ';

		return self::fetchEmployeeVacationRequests( $strSql, $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsCountByVacationTypeIdByEmployeeId( $intVacationTypeId, $intEmployeeId, $objDatabase ) {

		if( true == is_null( $intVacationTypeId ) && true == is_null( $intEmployeeId ) ) return NULL;

		$strSql = 'SELECT
						count(id)
					FROM
						employee_vacation_requests
					WHERE
						vacation_type_id = ' . ( int ) $intVacationTypeId . ' AND employee_id = ' . ( int ) $intEmployeeId . ' AND approved_by IS NOT NULL AND deleted_by IS NULL';
		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchEmployeeVacationRequestsForMarriageAnniversaryLeavePerYear( $intVacationTypeId, $intEmployeeId, $strBeginDate, $strEndDate, $objDatabase ) {
		if( true == is_null( $intVacationTypeId ) && true == is_null( $intEmployeeId ) ) return NULL;

		$strSql = 'SELECT
					 	 *
					FROM
						employee_vacation_requests
					WHERE
						vacation_type_id = ' . ( int ) $intVacationTypeId . '
						AND employee_id = ' . ( int ) $intEmployeeId . '
						AND ( extract ( YEAR FROM begin_datetime::DATE ) = ' . date( 'Y', strtotime( $strBeginDate ) ) . ' OR
							extract ( YEAR FROM end_datetime::DATE ) = ' . date( 'Y', strtotime( $strEndDate ) ) . ' ) AND deleted_by IS NULL ';

		return self::fetchEmployeeVacationRequests( $strSql, $objDatabase );
	}

	public static function fetchEmployeesVacationRequestsCountByLeaveTypeByLeaveStatusByDateByDepartmentIdByTeamId( $intLeaveType, $intLeaveStatus, $strDate, $intDepartmentId, $intTeamId, $arrstrFilteredExplodedSearch, $objDatabase, $strCountryCode = NULL, $arrmixFilter =  NULL ) {

		$intDayOfWeekSaturday 	= 6;
		$intDayOfWeekSunday 	= 0;
		$strSubsql				= '';
		$strJoinCondition		= '';
		$strSearchCondition		= '';
		$strSubCondition		= '';

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( CCountry::CODE_INDIA == $strCountryCode ) {
			if( strtotime( CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY ) == strtotime( $strDate ) ) {
				$strSubCondition = ' OR begin_datetime::date = \'' . CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY . '\'';
			}
			if( 1 == $intLeaveType ) {
				// 1 - full day leaves
				$strSubsql .= ' AND (( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 )
								OR ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number > 0 AND ( employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date ) )
								OR ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 AND employee_vacation_requests.serial_number > 0 )
								OR( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date )
							)';
			} elseif( 2 == $intLeaveType ) {
				// 2 - half day leaves
				$strSubsql .= ' AND ( ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_trunc( \'day\', employee_vacation_requests.begin_datetime ) = employee_vacation_requests.DATE ) OR ( date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.DATE = date_trunc( \'day\', employee_vacation_requests.end_datetime ) ) )';
			}
		}

		if( 1 == $intLeaveStatus ) {
			// 1 - in process leaves
			$strSubsql .= ' AND employee_vacation_requests.approved_by IS NULL
							AND employee_vacation_requests.denied_by IS NULL';
		} elseif( 2 == $intLeaveStatus ) {
			// 2 - approved leaves
			$strSubsql .= ' AND employee_vacation_requests.approved_by IS NOT NULL';
		} elseif( 3 == $intLeaveStatus ) {
			// 3 - denied leaves
			$strSubsql .= ' AND employee_vacation_requests.denied_by IS NOT NULL';
		}

		if( true == is_numeric( $intTeamId ) || true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strJoinCondition = ' LEFT JOIN team_employees as te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
								  LEFT JOIN teams as t ON ( t.id = te.team_id )';
		}

		if( false == is_null( $arrstrFilteredExplodedSearch ) ) {
			$strSearchCondition = ' AND ( name_full ILIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR CAST ( employee_vacation_requests.begin_datetime::date as text ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR CAST ( employee_vacation_requests.end_datetime::date as text ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR vt.name ILIKE \'%' . $arrstrFilteredExplodedSearch . '%\' )';
		}

		$strSql = 'SELECT
						COUNT ( DISTINCT employee_vacation_requests.id )
					FROM
						(
							SELECT
								DISTINCT evr.*,
								generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS serial_number,
								evr.begin_datetime::date + generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS DATE,
								ea.country_code
							FROM
								employee_vacation_requests AS evr
								LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id )
							WHERE
								date_trunc ( \'day\', timestamp\'' . $strDate . '\') BETWEEN date_trunc ( \'day\', evr.begin_datetime ) ::date AND date_trunc ( \'day\', evr.end_datetime ) ::date
								AND evr.deleted_by IS NULL
								AND evr.denied_by IS NULL
								AND country_code = \'' . $strCountryCode . '\'
								AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						) AS employee_vacation_requests
						JOIN employees AS e ON( employee_vacation_requests.employee_id = e.id AND employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\' )
						JOIN employee_phone_numbers AS epn ON ( employee_vacation_requests.employee_id = epn.employee_id )
						JOIN phone_number_types AS pnt ON ( epn.phone_number_type_id = pnt.id )
						LEFT JOIN departments as d ON ( e.department_id = d.id )
						LEFT JOIN vacation_types vt ON ( vt.id = employee_vacation_requests.vacation_type_id )' . $strJoinCondition;

		$strSql .= 'WHERE
						employee_vacation_requests.date NOT IN (
														SELECT
															DATE
														FROM
															paid_holidays
														WHERE
															country_code = \'' . $strCountryCode . '\'
															AND DATE_PART ( \'month\', DATE ( \'' . $strDate . '\' ) ) = DATE_PART ( \'month\', DATE )
															AND DATE_PART ( \'year\', DATE ( \'' . $strDate . '\' ) ) = DATE_PART ( \'year\', DATE )
															AND is_optional IS FALSE
														)
						AND employee_vacation_requests.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )
						' . $strSubCondition . '
						AND pnt.id= \'' . CPhoneNumberType::PRIMARY . '\'' . $strSubsql . $strSearchCondition;

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= ' AND d.id = ' . ( int ) $intDepartmentId;
		}

		if( true == is_numeric( $intTeamId ) ) {
			$strSql .= ' AND t.id = ' . ( int ) $intTeamId;
		}

		if( false == is_null( $arrmixFilter['add_employee_id'] ) || false == is_null( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strSql .= ' AND t.deleted_by IS NULL' . ( ( false == is_null( $arrmixFilter['add_employee_id'] ) ) ? ' AND t.add_employee_id = ' . ( int ) $arrmixFilter['add_employee_id'] : ' AND t.hr_representative_employee_id = ' . ( int ) $arrmixFilter['hr_representative_employee_id'] );
		}

		$strSql .= ' AND employee_vacation_requests.date BETWEEN \'' . $strDate . '\' AND \'' . $strDate . '\'';

		$arrintEmployeesCount = fetchdata( $strSql, $objDatabase );
		return $arrintEmployeesCount[0]['count'];
	}

	public static function fetchEmployeesVacationRequestsCountByLeaveTypeByLeaveStatus( $intLeaveType = NULL, $intLeaveStatus, $arrstrFilteredExplodedSearch = NULL, $objDatabase, $strCountryCode = NULL, $intDepartmentId = NULL, $arrmixFilter = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		$strSubsql = '';
		$strSearchCondition = '';
		$strWhereCondition	= '';

		if( 1 == $intLeaveType ) {

			$strSubsql .= 'AND ( date_part ( \'hour\', evr.begin_datetime ) < 13
						AND date_part ( \'hour\', evr.end_datetime ) > 13 )';
		} elseif( 2 == $intLeaveType ) {

			$strSubsql .= 'AND ( date_part ( \'hour\', evr.begin_datetime ) >= 13
						OR date_part ( \'hour\', evr.end_datetime ) <= 14 )';
		}

		$strSubsql .= ( true == in_array( $intLeaveType, array( CVacationType::WORK_FROM_HOME, CVacationType::WEEKEND_WORKING ) ) ) ? ' AND evr.vacation_type_id = ' . ( int ) $intLeaveType : ' AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';

		if( 1 == $intLeaveStatus ) {
			$strSubsql .= ' AND evr.approved_by IS NULL
							AND evr.denied_by IS NULL';
		} elseif( 2 == $intLeaveStatus ) {
			$strSubsql .= ' AND evr.approved_by IS NOT NULL';
		} elseif( 3 == $intLeaveStatus ) {
			$strSubsql .= ' AND evr.denied_by IS NOT NULL';
		}

		if( false == is_null( $arrstrFilteredExplodedSearch ) ) {
			$strSearchCondition = ' AND ( e.name_full ILIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR CAST ( evr.begin_datetime::date as text ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR CAST ( evr.end_datetime::date as text ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR vt.name ILIKE \'%' . $arrstrFilteredExplodedSearch . '%\' )';
		}

		if( true == is_numeric( $intDepartmentId ) ) {
			$strWhereCondition	= 'LEFT JOIN departments as d ON ( e.department_id = d.id )';
			$strSubsql			.= ' AND d.id = ' . ( int ) $intDepartmentId;
		}

		if( true == valStr( $arrmixFilter['begin_date'] ) && true == valStr( $arrmixFilter['end_date'] ) ) {
			$strSubsql 	.= ' AND evr.begin_datetime::DATE BETWEEN \'' . $arrmixFilter['begin_date'] . '\' AND \'' . $arrmixFilter['end_date'] . '\'';
		}

		if( true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strWhereCondition = '	LEFT JOIN team_employees as te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
									LEFT JOIN teams as t ON ( t.id = te.team_id AND t.deleted_by IS NULL )';
			$strSubsql 		.= ' AND t.hr_representative_employee_id = ' . $arrmixFilter['hr_representative_employee_id'];
		}

		$strSql = 'SELECT
					count( DISTINCT ( evr.id ) )
				FROM
					employee_vacation_requests evr
					JOIN employee_addresses ea on ( evr.employee_id = ea.employee_id )
					JOIN employees e ON ( e.id = evr.employee_id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
					LEFT JOIN vacation_types vt ON ( vt.id = evr.vacation_type_id ) ' . $strWhereCondition . '
				WHERE
					e.date_terminated IS NULL
					AND evr.deleted_by IS NULL
					AND DATE( DATE_TRUNC (\'YEAR\', evr.begin_datetime::date ) ) = DATE( DATE_TRUNC (\'YEAR\', NOW()) )
					AND ea.country_code = \'' . $strCountryCode . '\'
					AND evr.is_paid IS TRUE ' . $strSubsql . $strSearchCondition;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		return 0;

	}

	public static function fetchPaginatedEmployeesVacationRequestsByLeaveTypeByLeaveStatus( $intLeaveType, $intLeaveStatus, $arrstrFilteredExplodedSearch = NULL, $intPageNo, $intPageSize, $strOrderByFieldName, $strOrderField, $objDatabase, $strCountryCode = NULL, $intDepartmentId = NULL, $arrmixFilter = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSubsql 			= '';
		$strSearchCondition = '';
		$strSelectSubsql    = '';
		$strJoinCondition   = '';

		if( 1 == $intLeaveType ) {
			// 1 - full day leaves
			$strSubsql = 'AND ( date_part ( \'hour\', evr.begin_datetime ) < 13
						AND date_part ( \'hour\', evr.end_datetime ) > 13 )';
		} elseif( 2 == $intLeaveType ) {
			// 2 - half day leaves
			$strSubsql = 'AND ( date_part ( \'hour\', evr.begin_datetime ) >= 13
						OR date_part ( \'hour\', evr.end_datetime ) <= 14 )';
		}

		$strSubsql .= ( true == in_array( $intLeaveType, array( CVacationType::WORK_FROM_HOME, CVacationType::WEEKEND_WORKING ) ) ) ? ' AND evr.vacation_type_id = ' . ( int ) $intLeaveType : ' AND evr.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )';

		if( 1 == $intLeaveStatus ) {
			// 1 - in process leaves
			$strSubsql .= ' AND evr.approved_by IS NULL
							AND evr.denied_by IS NULL';
		} elseif( 2 == $intLeaveStatus ) {
			// 2 - approved leaves
			$strSubsql .= ' AND evr.approved_by IS NOT NULL';
		} elseif( 3 == $intLeaveStatus ) {
			// 3 - denied leaves
			$strSubsql .= ' AND evr.denied_by IS NOT NULL';
		}

		if( false == is_null( $arrstrFilteredExplodedSearch ) ) {
			$strSearchCondition = ' AND ( e.preferred_name ILIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR CAST ( evr.begin_datetime::date as text ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR CAST ( evr.end_datetime::date as text ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR vt.name ILIKE \'%' . $arrstrFilteredExplodedSearch . '%\' )';
		}

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSubsql 	.= ' AND de.id = ' . ( int ) $intDepartmentId;
		}

		if( true == valStr( $arrmixFilter['begin_date'] ) && true == valStr( $arrmixFilter['end_date'] ) ) {
			$strSubsql 	.= ' AND evr.begin_datetime::DATE BETWEEN \'' . $arrmixFilter['begin_date'] . '\' AND \'' . $arrmixFilter['end_date'] . '\'';
		}

		if( true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strJoinCondition = ' LEFT JOIN teams as t ON ( t.id = te.team_id AND t.deleted_by IS NULL )';
			$strSubsql 		.= ' AND t.hr_representative_employee_id = ' . $arrmixFilter['hr_representative_employee_id'];
		}
		$strSelectSubsql = ( true == in_array( $intLeaveType, array( CVacationType::WORK_FROM_HOME, CVacationType::WEEKEND_WORKING ) ) ) ?
			' CASE
					WHEN e.department_id = ' . CDepartment::QA . '  THEN (
															SELECT
																e.preferred_name
															FROM
																employees e
															WHERE
																e.id = t1.qam_employee_id and evr.approved_by IS NULL
					)
					 WHEN (e.department_id = ' . CDepartment::CREATIVE . ' OR e.department_id = ' . CDepartment::DEVELOPMENT . ')  THEN ( SELECT
																e.preferred_name
															FROM
																employees e
															WHERE
																e.id = t1.tpm_employee_id  and evr.approved_by IS NULL)
					ELSE (SELECT
																e.preferred_name
															FROM
																employees e
																LEFT JOIN employees e1 ON ( e1.id = evr.employee_id )
															WHERE
																e.id = e1.reporting_manager_id and evr.approved_by IS NULL)
					
					END AS pending_from_qam_tpm' :
			'CASE
					WHEN evr.approved_by IS NULL THEN (
															SELECT
																e.preferred_name
															FROM
																employees e
																LEFT JOIN employees e1 ON ( e1.id = evr.employee_id )
															WHERE
																e.id = e1.reporting_manager_id
					)
					ELSE NULL
					END AS pending_from';

		$strJoinCondition .= ( true == in_array( $intLeaveType, array( CVacationType::WORK_FROM_HOME, CVacationType::WEEKEND_WORKING ) ) ) ? ' LEFT JOIN teams as t1 ON ( t1.id = te.team_id AND t1.deleted_by IS NULL )': '';

		$strSql = 'SELECT
					DISTINCT ( evr.id ) AS request_id,
					evr.employee_id,
					e.preferred_name,
					d.name AS designation,
					de.name AS department,
					evr.request,
					vt.name as type,
					evr.paid_off_date,
					evr.begin_datetime::date,
					evr.end_datetime::date,
					evr.created_on::date,
					evr.approved_on::date,
					evr.denied_on::date,
					evr.denied_by,
					' . $strSelectSubsql . '
					
				FROM
					employee_vacation_requests evr
				 	JOIN employee_addresses ea on ( evr.employee_id = ea.employee_id )
					JOIN employees e ON ( e.id = evr.employee_id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
					LEFT JOIN designations d ON ( d.id = e.designation_id )
					LEFT JOIN team_employees te ON ( te.employee_id = evr.employee_id AND te.is_primary_team = 1 )
					LEFT JOIN vacation_types vt ON ( vt.id = evr.vacation_type_id )
					LEFT JOIN departments as de ON ( e.department_id = de.id )
					' . $strJoinCondition . '
				WHERE
					e.date_terminated IS NULL
					AND evr.deleted_by IS NULL
					AND DATE( DATE_TRUNC (\'YEAR\', evr.begin_datetime::date ) ) = DATE( DATE_TRUNC (\'YEAR\', NOW()) )
					AND ea.country_code = \'' . $strCountryCode . '\'
					AND evr.is_paid IS TRUE ' . $strSubsql . $strSearchCondition;

		$strSql .= ' ORDER BY ' . $strOrderByFieldName . ' ' . $strOrderField . ' OFFSET ' . ( int ) $intOffset . '
						LIMIT
							' . ( int ) $intLimit;

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeesVacationRequestsByLeaveTypeByLeaveStatusByDateByDepartmentIdByTeamId( $intLeaveType, $intLeaveStatus, $strDate, $intDepartmentId, $intTeamId, $arrstrFilteredExplodedSearch = NULL, $intPageNo, $intPageSize, $strOrderByFieldName, $strOrderField, $objDatabase, $strCountryCode = NULL, $arrmixFilter = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$intDayOfWeekSaturday 	= 6;
		$intDayOfWeekSunday 	= 0;
		$strSearchCondition		= '';
		$strSubsql				= '';
		$strJoinCondition		= '';
		$strSubCondition		= '';

		if( CCountry::CODE_INDIA == $strCountryCode ) {
			if( strtotime( CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY ) == strtotime( $strDate ) ) {
				$strSubCondition = ' OR begin_datetime::date = \'' . CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY . '\'';
			}
			if( 1 == $intLeaveType ) {
				// 1 - full day leaves
				$strSubsql .= ' AND (( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 )
								OR ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number > 0 AND ( employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date ) )
								OR ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) > 13 AND employee_vacation_requests.serial_number > 0 )
								OR( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) < 13 AND date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.serial_number < employee_vacation_requests.end_datetime::date - employee_vacation_requests.begin_datetime::date )) ';
			} elseif( 2 == $intLeaveType ) {
				// 2 - half day leaves
				$strSubsql .= ' AND ( ( date_part ( \'hour\', employee_vacation_requests.begin_datetime ) >= 13 AND date_trunc( \'day\', employee_vacation_requests.begin_datetime ) = employee_vacation_requests.DATE ) OR ( date_part ( \'hour\', employee_vacation_requests.end_datetime ) <= 14 AND employee_vacation_requests.DATE = date_trunc( \'day\', employee_vacation_requests.end_datetime ) ) )';
			}
		}

		if( 1 == $intLeaveStatus ) {
			// 1 - in process leaves
			$strSubsql .= ' AND employee_vacation_requests.approved_by IS NULL
							AND employee_vacation_requests.denied_by IS NULL';
		} elseif( 2 == $intLeaveStatus ) {
			// 2 - approved leaves
			$strSubsql .= ' AND employee_vacation_requests.approved_by IS NOT NULL';
		} elseif( 3 == $intLeaveStatus ) {
			// 3 - denied leaves
			$strSubsql .= ' AND employee_vacation_requests.denied_by IS NOT NULL';
		}

		if( true == is_numeric( $intTeamId ) || true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strJoinCondition = ' LEFT JOIN team_employees as te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
								  LEFT JOIN teams as t ON ( t.id = te.team_id )';
		}

		if( false == is_null( $arrstrFilteredExplodedSearch ) ) {
			$strSearchCondition = ' AND ( e.preferred_name ILIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR CAST ( employee_vacation_requests.begin_datetime::date as text ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR CAST ( employee_vacation_requests.end_datetime::date as text ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
									OR vt.name ILIKE \'%' . $arrstrFilteredExplodedSearch . '%\' )';
		}

		$strSql = 'SELECT
						DISTINCT employee_vacation_requests.employee_id,
						employee_vacation_requests.id as request_id,
						e.preferred_name,
						e.email_address,
						e.employee_number,
						epn.phone_number,
						employee_vacation_requests.paid_off_date,
						employee_vacation_requests.begin_datetime::date,
						employee_vacation_requests.end_datetime::date,
						employee_vacation_requests.created_on::date,
						employee_vacation_requests.request,
						vt.name as type,
						de.name as designation,
						d.name as department,
						employee_vacation_requests.approved_by,
						employee_vacation_requests.denied_by,
						employee_vacation_requests.approved_on::date,
						employee_vacation_requests.denied_on::date
					FROM
						(
							SELECT
								DISTINCT evr.*,
								generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS serial_number,
								evr.begin_datetime::date + generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS DATE,
								ea.country_code
							FROM
								employee_vacation_requests AS evr
								LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id )
							WHERE
								date_trunc ( \'day\', timestamp\'' . $strDate . '\') BETWEEN date_trunc ( \'day\', evr.begin_datetime ) ::date AND date_trunc ( \'day\', evr.end_datetime ) ::date
								AND evr.denied_by IS NULL
								AND evr.deleted_by IS NULL
								AND country_code = \'' . $strCountryCode . '\'
								AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						) AS employee_vacation_requests
						JOIN employees AS e ON( employee_vacation_requests.employee_id = e.id AND employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\' )
						JOIN employee_phone_numbers AS epn ON ( employee_vacation_requests.employee_id = epn.employee_id )
						JOIN phone_number_types AS pnt ON ( epn.phone_number_type_id = pnt.id )
						LEFT JOIN departments as d ON ( e.department_id = d.id )
						LEFT JOIN designations as de ON ( e.designation_id = de.id )
						LEFT JOIN vacation_types vt ON ( vt.id = employee_vacation_requests.vacation_type_id )' . $strJoinCondition;
		$strSql .= 'WHERE
						employee_vacation_requests.date NOT IN (
														SELECT
															DATE
														FROM
															paid_holidays
														WHERE
															country_code = \'' . $strCountryCode . '\'
															AND DATE_PART ( \'month\', DATE ( \'' . $strDate . '\' ) ) = DATE_PART ( \'month\', DATE )
															AND DATE_PART ( \'year\', DATE ( \'' . $strDate . '\' ) ) = DATE_PART ( \'year\', DATE )
															AND is_optional IS FALSE
														)
						AND employee_vacation_requests.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )
						' . $strSubCondition . '
						AND pnt.id= \'' . CPhoneNumberType::PRIMARY . '\'' . $strSubsql . $strSearchCondition;

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= ' AND d.id = ' . ( int ) $intDepartmentId;
		}

		if( true == is_numeric( $intTeamId ) ) {
			$strSql .= ' AND t.id = ' . ( int ) $intTeamId;
		}

		if( false == is_null( $arrmixFilter['add_employee_id'] ) || false == is_null( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strSql .= ' AND t.deleted_by IS NULL' . ( ( false == is_null( $arrmixFilter['add_employee_id'] ) ) ? ' AND t.add_employee_id = ' . ( int ) $arrmixFilter['add_employee_id'] : ' AND t.hr_representative_employee_id = ' . ( int ) $arrmixFilter['hr_representative_employee_id'] );
		}

		$strSql .= ' AND employee_vacation_requests.date BETWEEN \'' . $strDate . '\' AND \'' . $strDate . '\'
					ORDER BY ' . $strOrderByFieldName . ' ' . $strOrderField . ' OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchPresentEmployeesCountByDateByDepartmentIdByTeamId( $strBeginDatetime, $strEndDatetime, $intDepartmentId, $intTeamId, $objDatabase, $strCountryCode = NULL, $arrmixFilter = NULL ) {
		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( false == valStr( $strBeginDatetime ) && false == valStr( $strEndDatetime ) ) return NULL;

		$strWhereCondition = '';

		$strSql = ' SELECT
						COUNT ( DISTINCT ( eh.employee_id ) ) AS total_present_employee,
						EXTRACT ( DAY FROM eh.date ) AS DAY
					FROM
						employee_hours eh
						LEFT JOIN employee_addresses ea ON ( eh.employee_id = ea.employee_id )
						LEFT JOIN employees e ON ( e.id = eh.employee_id ) ';

		if( false == is_null( $intDepartmentId ) ) {
			$strSql .= 'LEFT JOIN departments d ON ( d.id = e.department_id ) ';

			$strWhereCondition = ' AND d.id = ' . ( int ) $intDepartmentId;
		}

		if( false == is_numeric( $intTeamId ) ) {
			$strWhereCondition = ' AND te.team_id = ' . ( int ) $intTeamId;
		}

		if( false == is_numeric( $intTeamId ) || true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strSql .= ' LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT JOIN teams t ON ( t.id = te.team_id )';
		}

		if( false == is_null( $arrmixFilter['add_employee_id'] ) || false == is_null( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strSql .= ' AND t.deleted_by IS NULL' . ( ( false == is_null( $arrmixFilter['add_employee_id'] ) ) ? ' AND t.add_employee_id = ' . ( int ) $arrmixFilter['add_employee_id'] : ' AND t.hr_representative_employee_id = ' . ( int ) $arrmixFilter['hr_representative_employee_id'] );
		}

		$strSql .= 'WHERE
						e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND eh.date BETWEEN date_trunc( \'day\', \'' . $strBeginDatetime . '\'::date ) AND date_trunc( \'day\', \'' . $strEndDatetime . '\'::date )
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						AND ea.country_code = \'' . $strCountryCode . '\'' . $strWhereCondition;

		$strSql .= ' GROUP BY
						eh.date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPresentEmployeesOnLeaveDayCountByDateByDepartmentIdByTeamId( $strBeginDatetime, $strEndDatetime, $intDepartmentId, $intTeamId, $objDatabase, $strCountryCode = NULL, $arrmixFilter = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}
		$strJoinSql			= '';
		$strWhereCondition	= '';

		if( false == valStr( $strBeginDatetime ) && false == valStr( $strEndDatetime ) ) return NULL;

		if( false == is_null( $intDepartmentId ) ) {
			$strJoinSql 		= 'LEFT JOIN departments d ON ( d.id = e.department_id ) ';
			$strWhereCondition	= ' AND d.id = ' . ( int ) $intDepartmentId;
		}

		if( false == is_null( $intTeamId ) ) {
			$strWhereCondition = ' AND te.team_id = ' . ( int ) $intTeamId;
		}

		if( false == is_null( $intTeamId ) || true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strJoinSql = ' LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
							 LEFT JOIN teams t ON ( t.id = te.team_id )';
		}

		$strSql = ' SELECT
						sub.date,
						EXTRACT ( \'day\'
							FROM
						DATE ) AS DAY,
						sub.employee_id
					FROM
						(
						  SELECT
							  eh.date,
							  SUM ( CASE
									  WHEN eh.end_datetime IS NOT NULL AND eh.begin_datetime IS NOT NULL THEN eh.end_datetime - eh.begin_datetime
									  ELSE \'00:00:00\'
									END ) OVER ( PARTITION BY eh.employee_id, eh.date, eh.employee_hour_type_id ) AS total_on_floor_hours,
							  eh.employee_id
						  FROM
							  employee_hours eh
							  LEFT JOIN employee_addresses ea ON ( eh.employee_id = ea.employee_id )
							  LEFT JOIN employees e ON ( e.id = eh.employee_id )
							  JOIN employee_vacation_requests evr ON ( eh.employee_id = evr.employee_id )
							 ' . $strJoinSql . '
						  WHERE
							 e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
							 AND ( e.date_started IS NOT NULL
							 OR e.date_started <= NOW ( ) )
							 AND ( e.date_terminated IS NULL
							 OR e.date_terminated > NOW ( ) )
							 AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
							 AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
							 AND eh.date BETWEEN date_trunc( \'day\', \'' . $strBeginDatetime . '\'::date ) AND date_trunc( \'day\', \'' . $strEndDatetime . '\'::date )
							 AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
							 AND eh.date BETWEEN evr.begin_datetime::DATE
							 AND evr.end_datetime::DATE
							 AND evr.deleted_by IS NULL
							 AND evr.denied_by IS NULL
							 AND ea.country_code = \'' . $strCountryCode . '\'' . $strWhereCondition . '
						 GROUP BY
							eh.id
						) sub
					GROUP BY
						sub.date,
						sub.employee_id,
						sub.total_on_floor_hours
					HAVING
						sub.total_on_floor_hours >= INTERVAL \'4 hours\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAbsentEmployeesCountByDateByDepartmentIdByTeamId( $strBeginDatetime, $strEndDatetime, $intDepartmentId, $intTeamId, $objDatabase, $strCountryCode = NULL, $arrmixFilter = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( false == valStr( $strBeginDatetime ) && false == valStr( $strEndDatetime ) ) return NULL;

		$strWhereCondition 		= '';

		$strSql = ' SELECT
						EXTRACT( \'day\' from DATE ) as date,
						COUNT ( DISTINCT employees.employee_id ) AS absent_count
					FROM
						(
							(
								SELECT
									DISTINCT \'' . $strBeginDatetime . '\'::date + serial_number AS DATE,
									employee_id
								FROM
									(
										SELECT
											generate_series ( 0, \'' . $strEndDatetime . '\'::date - \'' . $strBeginDatetime . '\'::date::date ) AS serial_number,
											e.id AS employee_id,
											e.date_started
										FROM
											employees e
											LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id )';

		if( false == is_null( $intDepartmentId ) ) {
			$strSql .= ' LEFT JOIN departments d ON ( d.id = e.department_id )';

			$strWhereCondition = ' AND d.id = ' . ( int ) $intDepartmentId;
		}

		if( false == is_null( $intTeamId ) ) {
			$strSql .= 'LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )';

			$strWhereCondition = ' AND te.team_id = ' . ( int ) $intTeamId;
		}

		if( true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strSql .= '
						LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT JOIN teams t ON ( t.id = te.team_id )';
			$strWhereCondition = ' AND t.deleted_by IS NULL' . ( ( true == is_numeric( $arrmixFilter['add_employee_id'] ) ) ? ' AND t.add_employee_id =' . ( int ) $arrmixFilter['add_employee_id'] : ' AND t.hr_representative_employee_id =' . ( int ) $arrmixFilter['hr_representative_employee_id'] );
		}

		$strSql .= ' WHERE
						employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW ( ) )
						AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
						AND ea.country_code = \'' . $strCountryCode . '\'
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\' ' . $strWhereCondition;

		$strSql .= ' ) AS sub_employees
						WHERE
							\'' . $strBeginDatetime . '\'::date + serial_number NOT IN (
																						SELECT
																							DATE
																						FROM
																							paid_holidays
																						WHERE
																							country_code = \'' . $strCountryCode . '\'
																							AND DATE_PART ( \'month\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'month\', DATE )
																							AND DATE_PART ( \'year\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'year\', DATE )
																							AND is_optional IS FALSE
																						)
							AND \'' . $strBeginDatetime . '\'::date + serial_number>= sub_employees.date_started
					)
					EXCEPT
						(
							SELECT
								DISTINCT DATE,
								employee_id
							FROM
								employee_hours
							WHERE
								DATE_PART ( \'month\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'month\', DATE )
								AND DATE_PART ( \'year\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'year\', DATE )
								AND ( employee_hour_type_id = 1 OR employee_hour_type_id IS NULL )
						)
					EXCEPT
					(
						SELECT
							begin_datetime::date + serial_number AS DATE,
							employee_id
						FROM
							(
								SELECT
									DISTINCT employee_id,
									evr.begin_datetime,
									generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) serial_number
								FROM
									employee_vacation_requests evr
								WHERE
									deleted_by IS NULL
									AND denied_by IS NULL
							) AS sub
						WHERE
							DATE_PART ( \'month\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'month\', begin_datetime::date + serial_number )
							AND DATE_PART ( \'year\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'year\', begin_datetime::date + serial_number )
					)
				) AS employees
			GROUP BY
				DATE
			ORDER BY
				DATE';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesVacationRequestsCountByDateByDepartmentIdByTeamId( $strBeginDatetime, $strEndDatetime, $intDepartmentId, $intTeamId, $objDatabase, $strCountryCode = NULL, $arrmixFilter = NULL ) {

		if( false == valStr( $strBeginDatetime ) || false == valStr( $strEndDatetime ) ) return NULL;

		$strWhereCondition		= '';
		$strSubCondition		= '';
		$intDayOfWeekSaturday	= 6;
		$intDayOfWeekSunday 	= 0;

		$strSql = 'SELECT
						EXTRACT ( DAY FROM business_day) AS business_day,
						count ( business_day ) AS daily_vacation_requests_count
					FROM
						(
							SELECT
								DISTINCT begin_datetime::date + serial_number AS business_day,
								employee_id
							FROM
								(
									SELECT
										DISTINCT evr.employee_id,
										evr.begin_datetime,
										evr.vacation_type_id,
										generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) serial_number
									FROM
										employee_vacation_requests AS evr
										LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id )
										LEFT JOIN employees e ON ( e.id = ea.employee_id )';

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( CCountry::CODE_INDIA == $strCountryCode && strtotime( CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY ) > strtotime( $strBeginDatetime ) && strtotime( CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY ) < strtotime( $strEndDatetime ) ) {
			$strSubCondition = ' OR begin_datetime::date = \'' . CEmployeeVacationRequest::EXCEPTIONAL_WORKING_DAY . '\'';
		}

		if( false == is_null( $intDepartmentId ) ) {
			$strSql .= ' LEFT JOIN departments d ON ( d.id = e.department_id )';

			$strWhereCondition = ' AND d.id = ' . ( int ) $intDepartmentId;
		}

		if( false == is_null( $intTeamId ) ) {
			$strSql .= ' LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )';

			$strWhereCondition .= ' AND te.team_id = ' . ( int ) $intTeamId;
		}

		if( false == is_null( $arrmixFilter['add_employee_id'] ) || false == is_null( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strSql .= '
						LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT JOIN teams t ON ( t.id = te.team_id )';
			$strWhereCondition .= ( ( false == is_null( $arrmixFilter['add_employee_id'] ) ) ? ' AND t.add_employee_id =' . ( int ) $arrmixFilter['add_employee_id'] : '' ) . ( ( false == is_null( $arrmixFilter['hr_representative_employee_id'] ) ) ? ' AND t.hr_representative_employee_id =' . ( int ) $arrmixFilter['hr_representative_employee_id'] : '' ) . ' AND t.deleted_by IS NULL';

		}

		$strSql .= 'WHERE
						ea.country_code = \'' . $strCountryCode . '\'
						AND evr.denied_by IS NULL
						AND evr.deleted_by IS NULL
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() ) ' . $strWhereCondition;

		$strSql .= ') AS vacation_requests
					WHERE
						begin_datetime::date + serial_number NOT IN (
																		SELECT
																			DATE
																		FROM
																			paid_holidays
																		WHERE
																			country_code = \'' . $strCountryCode . '\'
																			AND DATE_PART ( \'month\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'month\', DATE )
																			AND DATE_PART ( \'year\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'year\', DATE )
																			AND is_optional IS FALSE
						)
						AND vacation_requests.vacation_type_id NOT IN ( ' . CVacationType::WEEKEND_WORKING . ', ' . CVacationType::WORK_FROM_HOME . ' )
						' . $strSubCondition . '
						AND begin_datetime::date + serial_number BETWEEN DATE( \'' . $strBeginDatetime . '\' ) AND DATE( \'' . $strEndDatetime . '\' )
					) AS sub_employee_vacation_requests
				GROUP BY
					business_day
				ORDER BY
					business_day';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsByIds( $arrintEmployeeVacationRequestsIds, $objDatabase ) {

		$strSql = 'SELECT * FROM employee_vacation_requests WHERE id IN ( ' . ( implode( ',', $arrintEmployeeVacationRequestsIds ) ) . ')';

		return parent::fetchEmployeeVacationRequests( $strSql, $objDatabase );
	}

	public static function fetchAllAbsentEmployeesCountByDateByDepartmentIdByTeamId( $strDate, $intDepartmentId, $intTeamId, $objDatabase, $strCountryCode = NULL, $strFilteredExplodedSearch = NULL, $arrmixFilter = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( false == valStr( $strDate ) ) return NULL;

		$strJoinCondition		= '';

		if( true == is_numeric( $intTeamId ) || true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strJoinCondition = ' LEFT JOIN team_employees as te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
								  LEFT JOIN teams as t ON ( t.id = te.team_id )';
		}

		$strSql = '	SELECT
						count( e.id )
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						LEFT JOIN departments as d ON ( e.department_id = d.id ) ' . $strJoinCondition;
		$strSql .= ' WHERE
						e.id NOT IN (
										SELECT
											DISTINCT ( employee_id )
										FROM
											employee_hours
										WHERE
											date = \' ' . $strDate . ' \'
						)
						AND e.id NOT IN (
											SELECT
												DISTINCT employee_id
											FROM
												(
													SELECT
														DISTINCT evr.*,
														generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) serial_number
													FROM
														employee_vacation_requests AS evr
														LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id )
													WHERE
														date_trunc ( \'day\', timestamp\'' . $strDate . '\') BETWEEN date_trunc ( \'day\', evr.begin_datetime ) ::date AND date_trunc ( \'day\', evr.end_datetime ) ::date
														AND evr.denied_by IS NULL
														AND evr.deleted_by IS NULL
														AND country_code = \'' . $strCountryCode . '\'
														AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
												) AS sub
											WHERE
												begin_datetime::date + serial_number BETWEEN \' ' . $strDate . ' \' AND \' ' . $strDate . ' \'
						)
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW ( ) )
						AND DATE( \'' . $strDate . '\' ) >= e.date_started
						AND ea.country_code = \'' . $strCountryCode . '\'
						AND \'' . $strDate . '\' NOT IN (
															SELECT
																DATE
															FROM
																paid_holidays
															WHERE
																country_code = \'' . $strCountryCode . '\'
																AND DATE_PART ( \'month\', DATE( \' ' . $strDate . ' \' ) ) = DATE_PART ( \'month\', DATE )
																AND DATE_PART ( \'year\', DATE( \' ' . $strDate . ' \' ) ) = DATE_PART ( \'year\', DATE )
																AND is_optional IS FALSE
														)';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= 'AND d.id= ' . ( int ) $intDepartmentId . ' ';
		}

		if( true == is_numeric( $intTeamId ) ) {
			$strSql .= ' AND t.id = ' . ( int ) $intTeamId;
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSql .= ' AND ( name_full ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR e.email_address ILIKE \'%' . $strFilteredExplodedSearch . '%\' )';
		}

		if( false == is_null( $arrmixFilter['add_employee_id'] ) || false == is_null( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strSql .= ' AND t.deleted_by IS NULL' . ( ( false == is_null( $arrmixFilter['add_employee_id'] ) ) ? ' AND t.add_employee_id = ' . ( int ) $arrmixFilter['add_employee_id'] : ' AND t.hr_representative_employee_id = ' . ( int ) $arrmixFilter['hr_representative_employee_id'] );
		}

		$arrintEmployeesCount = fetchData( $strSql, $objDatabase );

		return $arrintEmployeesCount[0]['count'];
	}

	public static function fetchPaginatedAbsentEmployeesCountByDateByDepartmentIdByTeamId( $strDate, $intDepartmentId, $intTeamId, $intPageNo, $intPageSize, $strOrderByField, $strOrderByType, $objDatabase, $strCountryCode = NULL, $strFilteredExplodedSearch = NULL, $arrmixFilter = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( false == valStr( $strDate ) ) return NULL;

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strJoinCondition		= '';
		if( true == is_numeric( $intTeamId ) || true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strJoinCondition = ' LEFT JOIN team_employees as te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
								  LEFT JOIN teams as t ON ( t.id = te.team_id )';
		}
		$strSql = '	SELECT
						e.id as employee_id,
						e.employee_number,
						e.preferred_name,
						e.email_address,
						epn.phone_number,
						d.name as department,
						d.name
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						LEFT JOIN employee_phone_numbers AS epn ON ( e.id = epn.employee_id )
						LEFT JOIN phone_number_types AS pnt ON ( epn.phone_number_type_id = pnt.id )
						LEFT JOIN departments as d ON ( e.department_id = d.id )
						LEFT JOIN designations as de ON ( e.designation_id = de.id ) ' . $strJoinCondition;
		$strSql .= ' WHERE
						e.id NOT IN (
										SELECT
											DISTINCT ( employee_id )
										FROM
											employee_hours
										WHERE
											date = \' ' . $strDate . ' \'
						)
						AND e.id NOT IN (
											SELECT
												DISTINCT employee_id
											FROM
												(
													SELECT
														DISTINCT evr.*,
														generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) serial_number
													FROM
														employee_vacation_requests AS evr
														LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id )
													WHERE
														date_trunc( \'day\', timestamp\'' . $strDate . '\') BETWEEN date_trunc ( \'day\', evr.begin_datetime ) ::date AND date_trunc ( \'day\', evr.end_datetime ) ::date
														AND evr.denied_by IS NULL
														AND evr.deleted_by IS NULL
														AND country_code = \'' . $strCountryCode . '\'
														AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
												) AS sub
											WHERE
												begin_datetime::date + serial_number BETWEEN \' ' . $strDate . ' \' AND \' ' . $strDate . ' \'
						)
						AND pnt.id= \'' . CPhoneNumberType::PRIMARY . '\'
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						AND e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
						AND ( e.date_terminated IS NULL
						OR e.date_terminated > NOW ( ) )
						AND DATE( \'' . $strDate . '\' ) >= e.date_started
						AND ea.country_code = \'' . $strCountryCode . '\'
						AND \'' . $strDate . '\' NOT IN (
															SELECT
																DATE
															FROM
																paid_holidays
															WHERE
																country_code = \'' . $strCountryCode . '\'
																AND DATE_PART ( \'month\', DATE( \' ' . $strDate . ' \' ) ) = DATE_PART ( \'month\', DATE )
																AND DATE_PART ( \'year\', DATE( \' ' . $strDate . ' \' ) ) = DATE_PART ( \'year\', DATE )
																AND is_optional IS FALSE
														 )';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= 'AND d.id= ' . ( int ) $intDepartmentId . ' ';
		}

		if( true == is_numeric( $intTeamId ) ) {
			$strSql .= ' AND t.id = ' . ( int ) $intTeamId;
		}

		if( true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strSql .= ' AND t.deleted_by IS NULL' . ( ( true == is_numeric( $arrmixFilter['add_employee_id'] ) ) ? ' AND t.add_employee_id = ' . ( int ) $arrmixFilter['add_employee_id'] : ' AND t.hr_representative_employee_id = ' . ( int ) $arrmixFilter['hr_representative_employee_id'] );
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSql .= ' AND ( preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR e.email_address ILIKE \'%' . $strFilteredExplodedSearch . '%\' )';
		}

		$strSql .= 'ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType;

		$strSql .= ' OFFSET ' . ( int ) $intOffset . '
						LIMIT
							' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPresentEmployeesCountByDateByDepartmentIdByTeamId( $strDate, $intDepartmentId, $intTeamId, $objDatabase, $strCountryCode = NULL, $strFilteredExplodedSearch = NULL, $arrmixFilter = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( false == valStr( $strDate ) ) return NULL;

		$strJoinCondition = '';
		if( true == is_numeric( $intTeamId ) || true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strJoinCondition = 'LEFT JOIN team_employees as te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
								  LEFT JOIN teams as t ON ( t.id = te.team_id )';
		}

		$strSql = 'SELECT
						COUNT( DISTINCT eh.employee_id )
					FROM
						employee_hours eh
						LEFT JOIN employees e ON ( eh.employee_id = e.id )
						LEFT JOIN employee_addresses ea ON ( eh.employee_id = ea.employee_id )
						LEFT JOIN departments as d ON ( e.department_id = d.id )' . $strJoinCondition;
		$strSql .= 'WHERE
						eh.date = \' ' . $strDate . ' \'
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						AND ea.country_code = \'' . $strCountryCode . '\'
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )';

		if( true == is_numeric( $intTeamId ) ) {
			$strSql .= ' AND t.id= ' . ( int ) $intTeamId;
		}

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= ' AND d.id = ' . ( int ) $intDepartmentId;
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSql .= ' AND ( name_full ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR e.email_address ILIKE \'%' . $strFilteredExplodedSearch . '%\' )';
		}

		if( true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strSql .= ' AND t.deleted_by IS NULL' . ( ( true == is_numeric( $arrmixFilter['add_employee_id'] ) ) ? ' AND t.add_employee_id = ' . ( int ) $arrmixFilter['add_employee_id'] : ' AND t.hr_representative_employee_id = ' . ( int ) $arrmixFilter['hr_representative_employee_id'] );
		}

		$arrintPresentEmployeesCount = fetchData( $strSql, $objDatabase );

		return $arrintPresentEmployeesCount[0]['count'];
	}

	public static function fetchTotalEmployeesCountByTeamIds( $intPageNo, $intPageSize, $arrintTeamsIds, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;
		$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		$strSql = 'SELECT
						t.id AS team_id,
						t.name,
						count ( e.id ) AS total_count
					FROM
						teams t
						LEFT JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						LEFT JOIN employees e ON ( te.employee_id = e.id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
					WHERE
						t.id IN ( ' . implode( ',', $arrintTeamsIds ) . ' )
					GROUP BY
						t.id, t.name ' . $strSubSql;

		return rekeyArray( 'team_id', fetchData( $strSql, $objDatabase ) );

	}

	public static function fetchPresentEmployeesCountByDateByHrRepresentativeIdByTeamIds( $strCurrentDate, $arrintTeamsIds, $objDatabase, $strCountryCode = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( false == valArr( $arrintTeamsIds ) ) return NULL;

		$strSql = 'SELECT
						te.team_id,
						COUNT ( DISTINCT ( eh.employee_id ) ) AS total_present_employee
					FROM
						employee_hours eh
						LEFT JOIN employee_addresses ea ON ( eh.employee_id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ')
						LEFT JOIN employees e ON ( e.id = eh.employee_id AND e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ')
						LEFT JOIN departments d ON ( d.id = e.department_id )
						LEFT JOIN team_employees te ON ( te.employee_id = eh.employee_id AND te.is_primary_team = 1 )
					WHERE
						eh.date = date( \'' . $strCurrentDate . '\' )
						AND ea.country_code = \'' . $strCountryCode . '\'' .
						' AND te.team_id IN( ' . implode( ',', $arrintTeamsIds ) . ' )
					GROUP BY
						eh.date,
	 					te.team_id';

		return rekeyArray( 'team_id', fetchData( $strSql, $objDatabase ) );

	}

	public static function fetchTotalEmployeesLeaveCountByTeamIds( $strCurrentDate, $arrintTeamsIds, $objDatabase, $strCountryCode = NULL ) {

		if ( false == valArr( $arrintTeamsIds ) ) return NULL;

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		$strSql = 'SELECT
						te.team_id,
						COUNT ( DISTINCT employee_vacation_requests.id ) AS leave_employees
					FROM
						(
							SELECT
								DISTINCT evr.*,
								generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS serial_number,
								evr.begin_datetime::date + generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) AS DATE
							FROM
								employee_vacation_requests AS evr
								LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\' )
							WHERE
								date_trunc ( \'day\', \'' . $strCurrentDate . '\'::date ) BETWEEN date_trunc ( \'day\', evr.begin_datetime )
								AND date_trunc ( \'day\', evr.end_datetime ) ::date
								AND evr.deleted_by IS NULL
								AND evr.denied_by IS NULL
								AND ea.country_code = \'' . $strCountryCode . '\'
								AND evr.employee_id NOT IN ( Select employee_id from employee_hours where date = date( \'' . $strCurrentDate . '\' ) )
						) AS employee_vacation_requests
						JOIN employees AS e ON ( employee_vacation_requests.employee_id = e.id AND employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN team_employees AS te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						LEFT JOIN departments AS d ON ( e.department_id = d.id )
					WHERE
						employee_vacation_requests.date NOT IN (
																	SELECT
																		DATE
																	FROM
																		paid_holidays
																	WHERE
																		country_code = \'' . $strCountryCode . '\'
																		AND DATE_PART ( \'month\', DATE ( \'' . $strCurrentDate . '\' ) ) = DATE_PART ( \'month\', DATE )
																		AND DATE_PART ( \'year\', DATE ( \'' . $strCurrentDate . '\' ) ) = DATE_PART ( \'year\', DATE )
																		AND is_optional IS FALSE
																)
						AND EXTRACT ( \'dow\' FROM employee_vacation_requests.date ) NOT IN ( 6, 0 )
						AND employee_vacation_requests.approved_by IS NOT NULL
						AND employee_vacation_requests.date = \'' . $strCurrentDate . '\'
						AND te.team_id IN ( ' . implode( ',', $arrintTeamsIds ) . ' )
					GROUP BY
						te.team_id ';

		return rekeyArray( 'team_id', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchTeamEmployeesVacationRequestsByDateRangeByPDMEmployeeId( $strStartDate, $strEndDate, $intSdmEmployeeId, $objDatabase, $arrstrVacationRequestsFilter = NULL ) {

		$intOffset				= ( true == isset( $arrstrVacationRequestsFilter['page_no'] ) && true == isset( $arrstrVacationRequestsFilter['page_size'] ) ) ? $arrstrVacationRequestsFilter['page_size'] * ( $arrstrVacationRequestsFilter['page_no'] - 1 ) : 0;
		$intLimit				= ( true == isset( $arrstrVacationRequestsFilter['page_size'] ) ) ? $arrstrVacationRequestsFilter['page_size'] : '';
		$strOrderByField		= ' e.id ';
		$strOrderByType			= ' ASC';

		if( true == valArr( $arrstrVacationRequestsFilter ) ) {
			if( true == isset( $arrstrVacationRequestsFilter['order_by_field'] ) ) {

				switch( $arrstrVacationRequestsFilter['order_by_field'] ) {

					case 'request_type':
						$strOrderByField = ' request_type ';
						break;

					case 'comment':
						$strOrderByField = ' comment ';
						break;

					case 'created_on':
						$strOrderByField = ' evr.created_on ';
						break;

					default:
						$strOrderByField = ' e.preferred_name ';
						break;
				}
			}

			if( true == isset( $arrstrVacationRequestsFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrVacationRequestsFilter['order_by_type'] ) ? ' ASC ' : ' DESC ';
			}
		}

		$strSql = 'SELECT
			 			DISTINCT ( evr.id ) AS employee_vacation_request_id,
						e.id AS employee_id,
						e.name_first AS name_first,
						e.name_last AS name_last,
						e.preferred_name AS preferred_name,
						e.preferred_name AS name_full,
						evr.created_on,
						evr.approved_by,
						evr.approved_on,
						evr.denied_by,
						evr.denied_on,
						evr.begin_datetime,
						evr.end_datetime,
						evr.scheduled_vacation_hours AS scheduled_hours,
						evr.actual_vacation_hours AS actual_hours,
						evr.request AS COMMENT,
						vt.name AS request_type,
						ea.country_code AS country_code,
						( CAST ( evr.scheduled_vacation_hours AS FLOAT ) / 8 ) AS scheduled_days,
						EXTRACT ( WEEK FROM DATE ( evr.begin_datetime ) + \'1 days\' ::interval ) AS WEEK
					FROM
						teams t
						LEFT JOIN employees e ON ( t.manager_employee_id = e.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.country_code IS NOT NULL )
						LEFT JOIN employee_vacation_requests evr ON ( evr.employee_id = e.id )
						LEFT JOIN vacation_types vt ON ( evr.vacation_type_id = vt.id )
					WHERE
						e.date_terminated IS NULL
						AND e.id <> ' . ( int ) $intSdmEmployeeId . '
						AND t.sdm_employee_id = ' . ( int ) $intSdmEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND evr.begin_datetime BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . ' 23:59:59\'
						AND evr.deleted_by IS NULL
						AND evr.deleted_on IS NULL ORDER BY ' . $strOrderByField . $strOrderByType . ' OFFSET ' . ( int ) $intOffset;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeVacationRequestDetailById( $intVacationId, $arrintEmployeeIds, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT evr.*,
						e.preferred_name AS employee_name,
						vt.name AS vacation_name,
						ea.country_code,
						(
							SELECT
								data.priority
							FROM (
									SELECT
										evr1.id,
										rank() OVER(
									ORDER BY evr1.created_on) AS priority
									FROM employee_vacation_requests evr1
										JOIN employees e ON (e.id = evr1.employee_id)
									WHERE ((
											( evr1.begin_datetime::date BETWEEN evr.begin_datetime::date and evr.end_datetime::date )
											 or
											( evr1.end_datetime::date BETWEEN evr.begin_datetime::date and evr.end_datetime::date )
												) OR (
											( evr.begin_datetime::date BETWEEN evr1.begin_datetime::date and evr1.end_datetime::date )
											or
											( evr.end_datetime::date BETWEEN evr1.begin_datetime::date and evr1.end_datetime::date )
										) )
										AND e.id IN( ' . implode( ',', $arrintEmployeeIds ) . ' )
										AND evr1.deleted_on IS NULL
										AND evr1.denied_by IS NULL
										AND	evr1.approved_by IS NULL
										ORDER BY evr.created_on
								) AS data
							WHERE data.id = evr.id
							AND evr.deleted_on IS NULL
							AND evr.denied_by IS NULL
							AND	evr.approved_by IS NULL
						) AS priority
					FROM
						employee_vacation_requests evr
						JOIN employees e ON ( evr.employee_id = e.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id )
						JOIN vacation_types vt ON ( evr.vacation_type_id = vt.id )
					WHERE
						evr.id = ' . ( int ) $intVacationId;

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return $arrmixResult[0];
	}

	public static function fetchTeamEmployeesVacationRequestsCountByEmployeeId( $intEmployeeId, $strStartDate, $strEndDate, $objDatabase ) {

		$strSubSql = '';

		if( true == valStr( $strStartDate ) ) {
			$strSubSql = 'AND \'' . $strStartDate . '\' BETWEEN evr.begin_datetime::date AND evr.end_datetime::date';
		}
		if( true == valStr( $strEndDate ) ) {
			$strSubSql = ' AND \'' . $strEndDate . '\' BETWEEN evr.begin_datetime::date AND evr.end_datetime::date';
		}

		$strSql = 'SELECT
						count(evr.id)
					FROM
						employee_vacation_requests evr
					JOIN
						team_employees te on ( te.employee_id = evr.employee_id AND te.is_primary_team = 1)
					WHERE
						te.team_id IN (
										SELECT
											team_id
										FROM
											team_employees
										WHERE
											employee_id =' . ( int ) $intEmployeeId . '
						)
						AND evr.deleted_on IS NULL
						AND evr.denied_on IS NULL
						AND evr.approved_on IS NULL
						AND evr.vacation_type_id <> ' . CVacationType::WEEKEND_WORKING . '
						' . $strSubSql;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		return $arrintResponse[0]['count'];
	}

	public static function fetchEmployeeLeavesCountByEmployeeIdAndVacationTypeId( $intEmployeeId, $intVacationTypeId, $strFromDate, $strToDate, $objDatabase ) {
		$strWhereAdditionalCondition = ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND evr.begin_datetime > \'' . $strFromDate . '\' AND evr.begin_datetime < \'' . $strToDate . '\'': '';

		if( CVacationType::UNSCHEDULED == $intVacationTypeId ) {
			$strJoinCondition = 'AND evr.vacation_type_id IN ( ' . CVacationType::UNSCHEDULED . ', ' . CVacationType::FAMILY_EMERGENCY . ', ' . CVacationType::ILLNESS . ' )';
		} elseif( CVacationType::WEEKEND_WORKING == $intVacationTypeId ) {
			$strJoinCondition = 'AND evr.vacation_type_id = ' . CVacationType::WEEKEND_WORKING;
		} elseif( CVacationType::SCHEDULED == $intVacationTypeId ) {
			$strJoinCondition = 'AND evr.vacation_type_id IN ( ' . CVacationType::SCHEDULED . ' )';
		} elseif( CVacationType::SPECIAL_LEAVES == $intVacationTypeId ) {
			$strJoinCondition = 'AND evr.vacation_type_id IN ( ' . implode( ',', CVacationType::$c_arrintSpecialLeaves ) . ' )';
		}

		$strSql = 'SELECT
						 CAST ( sum ( evr.scheduled_vacation_hours ) AS float ) / CAST ( 8 AS float ) as count
					FROM
						employees e
						JOIN employee_vacation_requests evr ON ( e.id = evr.employee_id ' . $strJoinCondition . ' )
					WHERE
						e.id = ' . ( int ) $intEmployeeId . ' AND evr.approved_by IS NOT NULL
						AND evr.deleted_by IS NULL ' . $strWhereAdditionalCondition;

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return $arrmixResult[0]['count'];
	}

	public static function fetchEmployeeVacationRequestsByEmployeeIdByVacationTypeId( $intEmployeeId, $intVacationTypeId, $objDatabase, $strBeginDate = NULL, $strEndDate = NULL, $boolSumScheduledHours = false, $boolShowDeniedRequests = false, $boolGenerateDateRange = false ) {

		$strWhereClause		= ( true == valStr( $strBeginDate ) && true == valStr( $strEndDate ) ) ? ' AND ( begin_datetime::DATE BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' OR end_datetime::DATE BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' )' : ' AND begin_datetime >= ( NOW ( ) - INTERVAL \'' . '16 Month\' )';
		$strSelectClause	= ' * ';
		$strGroupBy			= ' ORDER BY id DESC';

		if( true == $boolSumScheduledHours ) {
			$strSelectClause	= ' sum( scheduled_vacation_hours )/8 AS scheduled_vacation_hours';
			$strGroupBy			= ' GROUP BY employee_id';
		}

		if( false == $boolShowDeniedRequests ) {
			$strWhereClause .= ' AND denied_by IS NULL';
		}

		if( true == $boolGenerateDateRange ) {
			$strSelectClause .= ', begin_datetime::date + generate_series ( 0, end_datetime::date - begin_datetime::date ) AS date';
		}

		$strSql = 'SELECT
						' . $strSelectClause . '
					FROM
						employee_vacation_requests
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND deleted_by IS NULL
						' . $strWhereClause . '
						AND vacation_type_id = ' . ( int ) $intVacationTypeId . $strGroupBy;
		if( true == $boolSumScheduledHours ) {
			return	fetchData( $strSql, $objDatabase )[0]['scheduled_vacation_hours'];
		} elseif( true == $boolGenerateDateRange ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchEmployeeVacationRequests( $strSql, $objDatabase );
		}
	}

	public static function fetchEmployeeWorkFormHomeRequestsDetailsByFilter( $arrmixFilter, $objDatabase ) {

		if( true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strJoinClause		= '	LEFT JOIN team_employees as te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
									LEFT JOIN teams as t ON ( t.id = te.team_id AND t.deleted_by IS NULL )';
			$strWhereClause 	= ' AND t.hr_representative_employee_id = ' . $arrmixFilter['hr_representative_employee_id'];
		}

		if( true == valStr( $arrmixFilter['begin_date'] ) && true == valStr( $arrmixFilter['end_date'] ) ) {
			$strWhereClause 	= ' AND evr.begin_datetime::DATE BETWEEN \'' . $arrmixFilter['begin_date'] . '\' AND \'' . $arrmixFilter['end_date'] . '\'';
		}

		$strSql = 'SELECT
						DISTINCT sub_query.employee_id,
						sub_query.date,
						sub_query.preferred_name,
						sub_query.status,
						SUM ( CASE
								WHEN eh.end_datetime IS NOT NULL AND eh.begin_datetime IS NOT NULL THEN eh.end_datetime - eh.begin_datetime
								ELSE \'00:00:00\'
							  END ) OVER ( PARTITION BY eh.employee_id, eh.date ) AS total_hours
					FROM
						(
						  SELECT
								evr.employee_id,
								e.preferred_name,
								CASE WHEN evr.approved_by IS NOT NULL THEN \'Approved\' ELSE ( CASE WHEN evr.denied_by IS NULL THEN \'In Process\' ELSE \'Denied\' END ) END as status,
								generate_series ( evr.begin_datetime::DATE, evr.end_datetime::DATE, \'1 day\'::INTERVAL )::DATE AS DATE
						  FROM
								employee_vacation_requests evr
								LEFT JOIN employees e ON ( e.id = evr.employee_id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
								' . $strJoinClause . '
						  WHERE
								evr.deleted_by IS NULL
								AND evr.vacation_type_id = ' . CVacationType::WORK_FROM_HOME . '
								' . $strWhereClause . '
						) sub_query
						LEFT JOIN employee_hours eh ON ( eh.employee_id = sub_query.employee_id AND sub_query.date = eh.date )
					ORDER BY
						sub_query.date,
						sub_query.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeLeaveDetailsByEmployeeIdByDates( $intEmployeeId, $arrstrDates, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) || false == valArr( $arrstrDates ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						temp.leave_date
					FROM
						employee_vacation_requests evr
						CROSS JOIN
						(
							SELECT *
							FROM UNNEST(ARRAY [ \'' . implode( '\'::DATE,\'', $arrstrDates ) . '\'::DATE ' . ' ]) leave_date
						) as temp
					WHERE evr.employee_id = ' . ( int ) $intEmployeeId . '
						AND evr.approved_by IS NOT NULL AND evr.approved_on IS NOT NULL
						AND evr.deleted_by IS NULL AND evr.deleted_on IS NULL
						AND temp.leave_date BETWEEN begin_datetime::date
						AND end_datetime::date
						AND evr.denied_on IS NULL and evr.denied_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeLeavesByEmployeeIdAndVacationTypeId( $intEmployeeId, $intVacationTypeId, $strFromDate, $strToDate, $objDatabase ) {
		$strWhereAdditionalCondition = ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND evr.begin_datetime::date > \'' . $strFromDate . '\' AND evr.begin_datetime::date < \'' . $strToDate . '\'': '';

		if( CVacationType::UNSCHEDULED == $intVacationTypeId ) {
			$strJoinCondition = 'AND evr.vacation_type_id IN ( ' . CVacationType::UNSCHEDULED . ', ' . CVacationType::FAMILY_EMERGENCY . ', ' . CVacationType::ILLNESS . ' )';
		} elseif( CVacationType::WEEKEND_WORKING == $intVacationTypeId ) {
			$strJoinCondition = 'AND evr.vacation_type_id = ' . CVacationType::WEEKEND_WORKING;
		} elseif( CVacationType::SCHEDULED == $intVacationTypeId ) {
			$strJoinCondition = 'AND evr.vacation_type_id IN ( ' . CVacationType::SCHEDULED . ' )';
		} elseif( CVacationType::SPECIAL_LEAVES == $intVacationTypeId ) {
			$strJoinCondition = 'AND evr.vacation_type_id IN ( ' . implode( ',', CVacationType::$c_arrintSpecialLeaves ) . ' )';
		}

		$strSql = 'SELECT
							 CAST ( evr.scheduled_vacation_hours AS float ) / CAST ( 8 AS float ) as count,
							 vt.name as vacation_name,
							 evr.begin_datetime as from_date,
							 evr.end_datetime as to_date
						FROM
							employee_vacation_requests evr
							JOIN vacation_types vt ON ( vt.id = evr.vacation_type_id ' . $strJoinCondition . ' )
						WHERE
							evr.employee_id = ' . ( int ) $intEmployeeId . ' AND evr.approved_by IS NOT NULL
							AND evr.deleted_by IS NULL ' . $strWhereAdditionalCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsByEmployeeIds( $arrintLeaveEmployeeId,$strDate, $objDatabase ) {
		if( true == valArr( $arrintLeaveEmployeeId ) ) {
			$strEmployeeIds = implode( ',', $arrintLeaveEmployeeId );
		} else {
			return NULL;
		}
		$strSql = ' select
						employee_id
					from
						employee_vacation_requests
					where
						employee_id in ( ' . $strEmployeeIds . ' ) and
						\'' . $strDate . '\'BETWEEN begin_datetime::date and end_datetime::date';
		fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsByDatesByCountryCode( $arrstrDates, $strCountryCode, $objDatabase, $boolFetchDeleted = false ) {

		if( false == valArr( $arrstrDates ) ) {
			return NULL;
		}

		$strWhereClause		= ( false == $boolFetchDeleted ) ? ' AND deleted_by IS NULL ' : '';

		$strSql = 'SELECT
						DISTINCT ON ( sq.id ) 
						sq.*
					FROM
						(
							SELECT
								*,
								generate_series ( 0, end_datetime::DATE - begin_datetime::DATE ) AS serial_number,
								begin_datetime::DATE + generate_series ( 0, end_datetime::DATE - begin_datetime::DATE ) AS DATE
							 FROM
							    employee_vacation_requests
							 WHERE
								denied_by IS NULL
								' . $strWhereClause . '
						) sq
						JOIN employee_addresses AS ea ON ( sq.employee_id = ea.employee_id )
						JOIN employees AS e ON ( sq.employee_id = e.id AND ea.address_type_id = 1 AND ea.country_code = \'' . $strCountryCode . '\' )
					WHERE
						date IN ( \'' . implode( '\',\'', $arrstrDates ) . '\' )
						AND ( DATE <= e.date_terminated
						OR e.date_terminated IS NULL )';

		return self::fetchEmployeeVacationRequests( $strSql, $objDatabase );
	}

}
?>