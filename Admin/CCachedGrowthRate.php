<?php

class CCachedGrowthRate extends CBaseCachedGrowthRate {

	public function valYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectedSalesRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectedTransactionRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectedTransferInRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectedPropertyRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuranceContingentCommissionPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPercentCore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPercentStudent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPercentNonCore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>