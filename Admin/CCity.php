<?php

class CCity extends CBaseCity {

	const PUNE_ID	= 633;
	const AHMADABAD	= 302;
	const BANGALORE	= 422;
	const CALCUTTA	= 144;
	const CHENNAI	= 784;
	const DELHI		= 276;
	const HYDERABAD	= 29;
	const INDORE	= 537;
	const MUMBAI	= 620;
	const NASHIK	= 625;
	const NOIDA		= 946;
	const SURAT		= 349;

	const CITY_PUNE	= 'PUNE';
	const CITY_CA_BERKELEY	= 'BERKELEY';

	public static $c_arrintTopCities = [
		self::PUNE_ID => 'Pune',
		self::AHMADABAD => 'Ahmadabad',
		self::BANGALORE => 'Bangalore',
		self::CALCUTTA => 'Calcutta',
		self::CHENNAI => 'Chennai',
		self::DELHI => 'Delhi',
		self::HYDERABAD => 'Hyderabad',
		self::INDORE => 'Indore',
		self::MUMBAI => 'Mumbai',
		self::NASHIK => 'Nashik',
		self::NOIDA => 'Nodia',
		self::SURAT => 'Surat'
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>