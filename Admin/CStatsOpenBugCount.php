<?php

class CStatsOpenBugCount extends CBaseStatsOpenBugCount {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQamEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOpenBugCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>