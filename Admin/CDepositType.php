<?php

class CDepositType extends CBaseDepositType {

	const MANUAL 		= 1;
	const VIMC 			= 2;
	const DI 			= 3;
	const AE 			= 4;
	const ACH 			= 5;
	const EL 			= 6;
}
?>