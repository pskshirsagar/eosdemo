<?php

class CVestingSchedule extends CBaseVestingSchedule {

	public function valName() {
		$boolIsValid = true;

		$strName = strip_tags( $this->m_strName );
		if( true == empty( $strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Valid name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		$strDescription = strip_tags( $this->m_strDescription );
		if( true == empty( $strDescription ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Valid description is required.' ) );
		}
		return $boolIsValid;
	}

	public function valIsPublished( $intVestingScheduleId, $objAdminDatabase ) {
		$boolIsValid = true;

		$arrobjEmployeeOptions  = CEmployeeOptions::fetchEmployeeOptionsByVestingScheduleId( $intVestingScheduleId, $objAdminDatabase );

		if( false == $this->getIsPublished() && true == valArr( $arrobjEmployeeOptions ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', 'You cannot unpublish this vesting schedule, it is already associated with employee options.' ) );
		}

		return $boolIsValid;
	}

	public function validateDelete( $intVestingScheduleId, $objAdminDatabase ) {
		$boolIsValid = true;

		$arrobjEmployeeOptions  = CEmployeeOptions::fetchEmployeeOptionsByVestingScheduleId( $intVestingScheduleId, $objAdminDatabase );

		if( true == valArr( $arrobjEmployeeOptions ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete', 'You cannot delete this vesting schedule, it is already associated with employee options.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $intVestingScheduleId = NULL, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDescription();
				if( false == empty( $intVestingScheduleId ) ) $boolIsValid &= $this->valIsPublished( $intVestingScheduleId, $objAdminDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->validateDelete( $intVestingScheduleId, $objAdminDatabase );
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>