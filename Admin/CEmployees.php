<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployees
 * Do not add any new functions to this class.
 */
class CEmployees extends CBaseEmployees {

	const APRIL_MONTH   = 'April';
	const OCTOBER_MONTH = 'October';

	const APRIL_MONTH_NUMBER   = '04';
	const OCTOBER_MONTH_NUMBER = '10';
	const PEER_REVIEW_LIMIT    = 3;

	public static function fetchCurrentEmployees( $objDatabase ) {
		return self::fetchEmployees( 'SELECT * FROM employees WHERE employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND date_terminated IS NULL ORDER BY preferred_name ', $objDatabase );
	}

	public static function fetchCurrentAndSystemEmployees( $objDatabase ) {
		return self::fetchEmployees( 'SELECT * FROM employees WHERE employee_status_type_id IN (1,3) AND date_terminated IS NULL ORDER BY name_full', $objDatabase );
	}

	public static function fetchEmployeesByEmployeeStatusTypeIdOrderByLastName( $intStatusTypeId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM %s WHERE employee_status_type_id = %d ORDER BY name_last', 'employees', ( int ) $intStatusTypeId ), $objDatabase );
	}

	public static function fetchEmployeesByDepartmentIdOrderByNameFull( $intDepartmentId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM %s WHERE department_id = %d AND employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' ORDER BY name_full', 'employees', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchEmployeesByDepartmentIdsOrderByNameFull( $arrintDepartmentIds, $objDatabase, $boolAllEmployees = false ) {
		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strWhereCondition = 'AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT;
		if( true == $boolAllEmployees ) {
			$strWhereCondition = 'AND e.employee_status_type_id IN ( ' . ( int ) CEmployeeStatusType::CURRENT . ',' . ( int ) CEmployeeStatusType::PREVIOUS . ' )';
		}

		$strSql = 'SELECT
						e.*,
						u.id as user_id
					FROM employees e
						LEFT JOIN users u ON( u.employee_id = e.id )
					WHERE e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) ' . $strWhereCondition . '
					ORDER BY name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchPresentEmployees( $arrstrDepartmentFilter, $arrstrYearFilter, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {
		$strOrderBy = '';

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= 'ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY d.name ASC';
		}

		if( true == isset( $arrstrDepartmentFilter['department'] ) && '' != $arrstrDepartmentFilter['department'] ) {
			$strWhereDepartmentId = ' AND d.id IN ( ' . implode( ',', $arrstrDepartmentFilter['department'] ) . ' ) ';
		} else {
			$strWhereDepartmentId = ' AND e.department_id IN ( SELECT id FROM departments ) ';
		}

		if( true == isset ( $arrstrYearFilter ) && '' != $arrstrYearFilter ) {
			$strYear = $arrstrYearFilter;
		} else {
			$strYear = ' DATE_PART(\'year\', NOW()) ';
		}

		$strSql = 'SELECT
						e.id,
						e.date_started,
						e.date_terminated,
						d.id AS department_id,
						d.name
					FROM employees e
						RIGHT JOIN departments d ON (d.id = e.department_id)
					WHERE ( e.employee_status_type_id NOT IN (' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ' ) )
					AND ( ' . $strYear . ' ) >= DATE_PART ( \'year\', DATE ( e.date_started ) )
					AND ( e.date_terminated IS NULL OR ( ' . $strYear . ' ) <= DATE_PART ( \'year\', DATE ( e.date_terminated ) ) )
					' . $strWhereDepartmentId . '
					' . $strOrderBy;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesByDepartmentIdsOrderByNameFull( $arrintDepartmentIds, $objDatabase, $boolIsIndianEmployee = false, $strOrderBy = 'name_full', $boolAllEmployees = false ) {
		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}
		$strWhereCondition = 'AND e.date_terminated IS NULL';
		if( true == $boolAllEmployees ) {
			$strWhereCondition = '';
		}
		$strJoinCondition = ( true == $boolIsIndianEmployee ) ? 'JOIN employee_addresses ea ON( e.id = ea.employee_id AND ea.country_code = \'' . CCountry::CODE_INDIA . '\' )' : '';

		$strSql = ' SELECT
						DISTINCT( e.* )
					FROM
						employees e
						' . $strJoinCondition . '
					WHERE
						e.department_id IN ( ' . implode( ', ', $arrintDepartmentIds ) . ' )
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . $strWhereCondition . '
					ORDER BY
						e.' . $strOrderBy;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesByDepartmentIdsAndAllProjectManagerOrderByNameFull( $arrintDepartmentIds, $objDatabase, $strOrderBy = 'name_full', $boolAllEmployees = false ) {
		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}
		$strWhereCondition = ' AND e.date_terminated IS NULL';
		if( true == $boolAllEmployees ) {
			$strWhereCondition = '';
		}

		$strSql = ' SELECT
						DISTINCT(e.*)
					FROM
						employees e
					WHERE
						( e.department_id IN ( ' . implode( ', ', $arrintDepartmentIds ) . ' ) OR is_project_manager = 1 )
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . $strWhereCondition . ' 
					ORDER BY
						e.' . $strOrderBy;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByDesignationIdsOrderByNameFull( $arrintDesignationIds, $objDatabase, $boolIsShowDisabledData = NULL ) {
		if( false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}
		$strSubSql	= ( false == $boolIsShowDisabledData ) ? ' AND employee_status_type_id = ' . CEmployeeStatusType::CURRENT : '';
		$strSql		= 'SELECT
							e.*,
							ea.country_code
						FROM 
							employees e
							LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = 1 )
						WHERE 
							e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' ) ';

		return self::fetchEmployees( $strSql . $strSubSql . ' ORDER BY name_full', $objDatabase );
	}

	public static function fetchActiveEmployeesByDesignationIdsOrderByNameFull( $arrintDesignationIds, $objDatabase, $strOrderBy = 'name_full', $boolAllEmployees = false ) {
		if( false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}
		$strWhereCondition = ' AND date_terminated IS NULL';
		if( true == $boolAllEmployees ) {
			$strWhereCondition = '';
		}
		return self::fetchEmployees( 'SELECT * FROM employees WHERE designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' ) ' . $strWhereCondition . ' AND employee_status_type_id !=  ' . CEmployeeStatusType::NO_SHOW . '  ORDER BY ' . $strOrderBy, $objDatabase );
	}

	public static function fetchActiveEmployeesByDesignationIdsOrByIds( $arrintDesignationIds, $arrintEmployeeIds, $objDatabase, $strOrderBy = 'name_full', $boolAllEmployees = false ) {

		$boolDesignationIds = valArr( $arrintDesignationIds );
		$boolEmployeeIds    = valArr( $arrintEmployeeIds );

		if( false == $boolDesignationIds && false == $boolEmployeeIds ) {
			return NULL;
		}

		$strWhere = '';

		if( true == $boolEmployeeIds && true == $boolDesignationIds ) {
			$strWhere .= '( designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ') OR id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) )';
		} elseif( true == $boolDesignationIds ) {
			$strWhere .= '( designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' ) )';
		} elseif( true == $boolEmployeeIds ) {
			$strWhere .= '( id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) )';
		}

		if( false == $boolAllEmployees ) {
			$strWhere .= ' AND date_terminated IS NULL';
		}
		return self::fetchEmployees( 'SELECT * FROM employees WHERE ' . $strWhere . ' AND employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '  ORDER BY ' . $strOrderBy, $objDatabase );
	}

	public static function fetchEmployeeByIds( $arrintEmployeeIds, $objDatabase, $strOrderBy = NULL ) {
		$strOrder = '';

		if( false == valArr( array_filter( $arrintEmployeeIds ) ) ) {
			return NULL;
		}

		if( valStr( $strOrderBy ) ) {
			$strOrder = ' ORDER BY ' . $strOrderBy;
		}

		return self::fetchEmployees( 'SELECT * FROM employees WHERE id IN ( ' . implode( ',', array_filter( $arrintEmployeeIds ) ) . ' ) AND employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . $strOrder . ' ', $objDatabase );
	}

	public static function fetchEmployeesByDesignationIdOrderByNameFirst( $intDesignationId, $objDatabase ) {
		if( true == is_null( $intDesignationId ) ) {
			return NULL;
		}

		return self::fetchEmployees( sprintf( 'SELECT e.id, e.preferred_name AS name_full, e.department_id, e.email_address, e.physical_phone_extension_id FROM employees e join users u on e.id = u.employee_id WHERE e.designation_id = %d and u.is_disabled = 0 AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' order BY e.name_first', ( int ) $intDesignationId ), $objDatabase );
	}

	public static function fetchCurrentEmployeesByDepartmentIds( $arrintDepartmentIds, $objDatabase ) {
		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		return self::fetchEmployees( 'SELECT * FROM employees WHERE employee_status_type_id = 1 AND date_terminated IS NULL AND department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) ORDER BY name_full', $objDatabase );
	}

	public static function fetchCurrentEmployeesByManagerId( $intManagerId, $objDatabase, $boolIsIncludeManager = false ) {

		$strWhere = '';

		if( true == $boolIsIncludeManager ) {
			$strWhere = ' or e.id = ' . ( int ) $intManagerId;
		}

		$strSql = 'SELECT
						e.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
						( e.reporting_manager_id = ' . ( int ) $intManagerId . $strWhere . ' )
						AND e.employee_status_type_id =' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByManagerIds( $arrintManagerIds, $objDatabase, $boolIsIncludeManager = false ) {

		if( false == valArr( $arrintManagerIds ) ) {
			return NULL;
		}

		$strWhereCondition = '';

		if( true == $boolIsIncludeManager ) {
			$strWhereCondition = 'OR e.id IN ( ' . implode( ',', $arrintManagerIds ) . ' ) ';
		}

		$strSql = 'SELECT
						e.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
						( e.reporting_manager_id IN ( ' . implode( ',', $arrintManagerIds ) . ' ) ' . $strWhereCondition . ' )
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesByDepartmentId( $intDepartmentId, $objDatabase ) {
		$strSql = ' SELECT
						DISTINCT ( e.* ),
						ea.country_code
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e.name_last IS NOT NULL
						AND e.department_id = ' . ( int ) $intDepartmentId . '
						AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesByDepartmentIdByCountryCode( $intDepartmentId, $strCountryCode, $objDatabase ) {
		$strSql = ' SELECT
						e.*
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id )
					WHERE
						e.name_last IS NOT NULL
						AND e.department_id = ' . ( int ) $intDepartmentId . '
						AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' )
					ORDER BY
						e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesByDepartmentIds( $arrintDepartmentIds, $objDatabase, $strOrderBy = ' preferred_name ', $boolIsIncludeEmployeeStatusTypeSystem = false ) {
		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == $boolIsIncludeEmployeeStatusTypeSystem ) ? ' AND e.employee_status_type_id IN ( ' . ( int ) CEmployeeStatusType::CURRENT . ' , ' . ( int ) CEmployeeStatusType::SYSTEM . ' ) ' : ' AND e.employee_status_type_id = ( ' . ( int ) CEmployeeStatusType::CURRENT . ' ) ';

		$strSql = 'SELECT
						e.*
					 FROM
						employees e
						JOIN users as u ON ( e.id = u.employee_id )
					WHERE
						e.name_last IS NOT NULL
						AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
						' . $strWhereCondition . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND u.is_disabled = 0
					ORDER BY
					' . $strOrderBy;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesWithLastNameOrderByDepartmentNameByDepartmentIds( $arrintDepartmentIds, $objDatabase ) {
		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						employees
					WHERE
						name_last IS NOT NULL
						AND employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
						AND ( date_terminated IS NULL OR date_terminated > NOW())
					ORDER BY
						department_id,
						name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesWithLastNameWithUserIdByDepartmentIdsOrderByDepartmentByName( $arrintDepartmentIds, $objDatabase ) {
		$strSql = 'SELECT
						e.*,
						u.id as user_id
					FROM
						employees e JOIN users u ON (e.id = u.employee_id)
					WHERE
						name_last IS NOT NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR NOW() < date_terminated )
						AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
					ORDER BY
						department_id DESC,
						name_full ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesWithWorkStationIpAddressAndDnsHandle( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						employees as e
						JOIN users as u ON ( e.id = u.employee_id )
					WHERE
						e.work_station_ip_address IS NOT NULL
						AND e.dns_handle IS NOT NULL
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW ( ) )
						AND u.is_disabled = 0
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllActiveEmployees( $objDatabase ) {
		$strSql = ' SELECT
						e.*,
						CASE WHEN e.preferred_name IS NULL THEN e.name_full ELSE e.preferred_name END AS name_full,
						u.id as user_id
					FROM
						employees as e
						JOIN users as u ON ( e.id = u.employee_id )
					WHERE
						( e.date_terminated IS NULL	OR NOW() < e.date_terminated )
						AND u.is_disabled = 0
						AND e.employee_status_type_id != ' . ( int ) CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeByUserId( $intUserId, $objDatabase ) {
		if( true == is_null( $intUserId ) || false == is_numeric( $intUserId ) ) {
			return NULL;
		}

		return self::fetchEmployee( 'SELECT emp.*, usr.username FROM employees emp, users usr WHERE emp.id=usr.employee_id AND usr.id=' . ( int ) $intUserId . ' AND emp.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' ', $objDatabase );
	}

	public static function fetchEmployeeByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployee( 'SELECT emp.* FROM employees emp WHERE  emp.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND emp.id = ' . ( int ) $intEmployeeId, $objDatabase );
	}

	public static function fetchEmployeeWithPhoneNumberByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						e.*,
						epn.phone_number,
						epn2.phone_number as emergency_contact_number
					FROM
						employees e
						LEFT JOIN employee_phone_numbers AS epn ON ( e.id = epn.employee_id AND epn.phone_number_type_id = ' . ( int ) CPhoneNumberType::PRIMARY . ' )
						LEFT JOIN employee_phone_numbers AS epn2 ON ( e.id = epn2.employee_id AND epn2.phone_number_type_id = ' . ( int ) CPhoneNumberType::MOBILE . ' )
					WHERE e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
							AND e.id = ' . ( int ) $intEmployeeId;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchEmployeesWithPhoneNumber( $objDatabase ) {
		$strSql = 'SELECT
						e.*,
						epn.phone_number,
						epn2.phone_number as emergency_contact_number
					FROM
						employees e
						LEFT JOIN employee_phone_numbers AS epn ON ( e.id = epn.employee_id AND epn.phone_number_type_id = ' . ( int ) CPhoneNumberType::PRIMARY . ' )
						LEFT JOIN employee_phone_numbers AS epn2 ON ( e.id = epn2.employee_id AND epn2.phone_number_type_id = ' . ( int ) CPhoneNumberType::MOBILE . ' )
					WHERE e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
					ORDER BY e.employee_number';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeWithCountryCodeByUserId( $intUserId, $objDatabase ) {
		return self::fetchEmployee( 'SELECT emp.*, ea.country_code FROM employees emp, employee_addresses ea, users usr WHERE emp.id=usr.employee_id AND ea.employee_id = emp.id AND emp.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND usr.id=' . ( int ) $intUserId, $objDatabase );
	}

	public static function fetchEmployeesByUserIds( $arrintUserIds, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) ) {
			return NULL;
		}

		// Using array_filter to remove the blank values.
		$arrintUserIds = array_filter( $arrintUserIds );

		if( false == valArr( $arrintUserIds ) ) {
			return NULL;
		}

		return self::fetchEmployees( 'SELECT e.*, u.id as user_id FROM employees e, users u WHERE e.id = u.employee_id AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND u.id IN (' . implode( ',', $arrintUserIds ) . ')', $objDatabase );
	}

	public static function fetchActiveEmployeesByUserIds( $arrintUserIds, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) ) {
			return NULL;
		}

		return self::fetchEmployees( 'SELECT e.*, u.id as user_id FROM employees e, users u WHERE e.id = u.employee_id AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND u.id IN (' . implode( ',', $arrintUserIds ) . ')', $objDatabase );
	}

	public static function fetchEmployeesDetailsByUserIds( $arrintUserIds, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*,
						u.id as user_id,
						ea.country_code,
						d.name AS department_name
					FROM
						employees e
						JOIN users u ON (e.id = u.employee_id)
						LEFT JOIN employee_addresses ea ON ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . '
						LEFT JOIN departments d ON ( e.department_id = d.id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						 AND u.id IN (' . implode( ',', $arrintUserIds ) . ')';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeByIdByDeparmentIdsByMarketingBlurb( $intId, $arrintDepartmentIds, $objAdminDatabase ) {
		$strSql = 'SELECT
						emp.*
					FROM
						employees emp, users usr
					WHERE
						emp.id = usr.employee_id
						AND department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
						AND emp.id = ' . ( int ) $intId . '
						AND emp.marketing_blurb IS NOT NULL
						AND emp.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND emp.marketing_blurb_updated_on >= ( NOW() - INTERVAL \'1 Month\' )';

		return self::fetchEmployee( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeByIdByManagerIdByIsAdministrator( $intId, $intManagerId, $boolIsAdministrator, $objAdminDatabase ) {
		$strSql = 'SELECT
						emp.*, emp.reporting_manager_id AS manager_employee_id, ea.country_code
					FROM
						employees emp
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = emp.id )
					WHERE
						emp.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND emp.id = ' . ( int ) $intId;

		if( false == $boolIsAdministrator ) {
			$strSql .= ' AND emp.reporting_manager_id = ' . ( int ) $intManagerId;
		}

		return self::fetchEmployee( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeesByIds( $arrintEmployeeIds, $objDatabase, $boolIsBonusPotentialEmployeesOnly = false, $boolCurrentEmployees = false ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strWhereClause = ( true == $boolIsBonusPotentialEmployeesOnly ) ? ' AND e.bonus_potential_encrypted IS NOT NULL' : '';
		$strWhereClause .= ( true == $boolCurrentEmployees ) ? ' AND e.employee_status_type_id=' . CEmployeeStatusType::CURRENT . '' : '';

		$strSql = 'SELECT
						e.*,
						ea.country_code,
						e.office_desk_id,
						d.name AS department_name,
						ds.name AS designation_name,
						eap.employee_application_status_type_id
					FROM
						employees e
						LEFT JOIN departments d ON ( e.department_id = d.id )
						LEFT JOIN designations ds ON ( ds.id = e.designation_id )
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employee_applications eap ON ( eap.employee_id = e.id )
					WHERE
						e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						' . $strWhereClause . '
					ORDER BY lower(e.name_first)';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesHavingDesignationByIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
						e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND e.designation_id IS NOT NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY lower( e.name_first )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllEmployees( $objDatabase, $strOrderBy = 'name_last', $boolIsNeedFetchData = false ) {

		if( true == $boolIsNeedFetchData ) {
			$strSql = 'SELECT
						e.id,
						e.department_id,
						e.designation_id,
						e.email_address,
						e.physical_phone_extension_id,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.name_full,';
		} else {
			$strSql = 'SELECT
						e.*,';
		}
		$strSql .= 'ea.country_code,
						e.reporting_manager_id AS manager_employee_id,
						te.team_id,
						od.desk_number,
						CASE
							WHEN NOW() > e.date_terminated
							THEN 1
							ELSE 0
						END AS is_terminated
						FROM
							employees e
							LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.country_code= \'' . CCountry::CODE_INDIA . '\' )
							LEFT JOIN team_employees te ON ( e.id = te.employee_id AND is_primary_team = 1 )
							LEFT JOIN office_desks od ON ( e.office_desk_id = od.id )
						WHERE
							e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						ORDER BY e.' . $strOrderBy . '';

		if( true == $boolIsNeedFetchData ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchEmployees( $strSql, $objDatabase );
		}
	}

	public static function fetchEmployeesBirthDayAndAnniversaryInComingWeek( $objDatabase, $intShowDisabledData = 0 ) {
		$strCondition = ( 0 == $intShowDisabledData ) ? ' AND employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT : '';
		$strSql       = 'SELECT
						id,
						preferred_name,
						to_char ( birth_date, \'MM/DD\' ) as birth_date,
						NULL AS anniversary_date,
						( ( to_char ( now ( ), \'yyyy\' ) || to_char ( birth_date, \'-mm-dd\' ) )::DATE - CURRENT_DATE ) AS date_diff
					FROM
						employees
					WHERE
						( ( to_char ( now ( ), \'yyyy\' ) || to_char ( birth_date, \'-mm-dd\' ) )::DATE - CURRENT_DATE ) BETWEEN 0 AND 6
						AND CASE WHEN EXTRACT( YEAR FROM CURRENT_DATE )::int % 4 != 0 THEN to_char ( birth_date, \'-mm-dd\' ) != \'-02-29\' ELSE TRUE END
						' . $strCondition . '
					UNION
					SELECT
						id,
						preferred_name,
						NULL AS birth_date,
						to_char ( anniversary_date, \'MM/DD\' ) as anniversary_date,
						( ( to_char ( now ( ), \'yyyy\' ) || to_char ( anniversary_date, \'-mm-dd\' ) )::DATE - CURRENT_DATE ) AS date_diff
					FROM
						employees
					WHERE
						( ( to_char ( now ( ), \'yyyy\' ) || to_char ( anniversary_date, \'-mm-dd\' ) )::DATE - CURRENT_DATE ) BETWEEN 0 AND 6
						AND CASE WHEN EXTRACT( YEAR FROM CURRENT_DATE )::int % 4 != 0 THEN to_char ( anniversary_date, \'-mm-dd\' ) != \'-02-29\' ELSE TRUE END
						' . $strCondition . '
					ORDER BY
						birth_date,
						anniversary_date,
						preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployees( $objDatabase, $boolIsAdminEmployee = false, $strCountryCode = '', $boolAllEmployees = false ) {

		$strWhereCondition = 'WHERE e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . ' AND e.date_terminated IS NULL ';
		if( true == $boolAllEmployees ) {
			$strWhereCondition = 'WHERE e.employee_status_type_id IN ( ' . ( int ) CEmployeeStatusType::CURRENT . ', ' . ( int ) CEmployeeStatusType::PREVIOUS . ' )';
		}
		$strSql = 'SELECT
						e.*,
						e.department_id,
						te.team_id,
						e.reporting_manager_id AS manager_employee_id,
						d.name As designation_name,
						dep.name AS department_name,
						ea.country_code,
						epn.phone_number
					FROM
						employees e
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						LEFT JOIN designations d ON d.id = e.designation_id
						LEFT JOIN departments dep ON dep.id = e.department_id
						JOIN employee_addresses ea ON ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . '
						LEFT JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )' .
		                $strWhereCondition;

		if( '' != $strCountryCode ) {
			$strSql .= ' AND ea.country_code = \'' . $strCountryCode . '\'';
		}

		if( false == $boolIsAdminEmployee ) {
			$strSql .= ' AND e.department_id <> ' . ( int ) CDepartment::ADMINISTRATIVE . '';
		}

		$strSql .= ' ORDER BY
						 e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesData( $objDatabase, $boolIsAdminEmployee = false, $strCountryCode = '' ) {
		$strSql = 'SELECT
						e.id,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.department_id,
						e.reporting_manager_id AS manager_employee_id,
						d.name As designation_name,
						ea.country_code
					FROM
						employees e
						LEFT JOIN designations d ON d.id = e.designation_id
						JOIN employee_addresses ea ON ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . '
					WHERE
						e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL ';

		if( '' != $strCountryCode ) {
			$strSql .= ' AND ea.country_code = \'' . $strCountryCode . '\'';
		}

		if( true == $boolIsAdminEmployee ) {
			$strSql .= ' AND e.department_id <> ' . ( int ) CDepartment::ADMINISTRATIVE . '';
		}

		$strSql .= ' ORDER BY
						 e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchFloatingEmployeeById( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						e.*
					FROM
						employees e
						LEFT OUTER JOIN users u ON (e.id = u.employee_id)
					WHERE
						u.id IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id= ' . ( int ) $intEmployeeId;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchTotalEmployeeHoursInAPayrollPeriodByDateRangeByEmployeeId( $intEmployeeId, $strBeginDate, $strEndDate, $objDatabase, $boolIsIncludeEndDate = false ) {
		$strCondition = ' AND date < \'' . addslashes( $strEndDate ) . '\'';
		if( true == $boolIsIncludeEndDate ) {
			$strCondition = ' AND date <= \'' . addslashes( $strEndDate ) . '\'';
		}

		$strSql = 'SELECT
						to_char( sum( end_datetime - begin_datetime ), \'DD:HH24:MI:SS \' ) as total_hours_in_a_payroll_period
					FROM
						employee_hours
					WHERE
						date >= \'' . addslashes( $strBeginDate ) . '\' ' . $strCondition . '
						AND employee_id = ' . ( int ) $intEmployeeId . '
						AND	payroll_period_id IS NULL
					GROUP BY
						employee_id';

		$arrstrResponse                = fetchData( $strSql, $objDatabase );
		$strTotalHoursInAPayrollPeriod = ( true == isset ( $arrstrResponse[0]['total_hours_in_a_payroll_period'] ) ) ? $arrstrResponse[0]['total_hours_in_a_payroll_period'] : NULL;

		$arrmixDataSplit = explode( ':', $strTotalHoursInAPayrollPeriod );
		$strDays         = ( true == isset ( $arrmixDataSplit[0] ) ) ? $arrmixDataSplit[0] : NULL;
		$intHours        = ( true == isset ( $arrmixDataSplit[1] ) ) ? $arrmixDataSplit[1] : NULL;
		$strMinutes      = ( true == isset ( $arrmixDataSplit[2] ) ) ? $arrmixDataSplit[2] : NULL;

		if( '0' == $strMinutes || NULL == $strMinutes ) {
			$strMinutes = '00';
		}

		$intHoursInAPayrollPeriod = ( ( ( int ) $strDays * 24 ) + ( int ) $intHours );

		$strTotalHoursInAPayrollPeriod = ( false == is_null( $intHoursInAPayrollPeriod ) && 10 > $intHoursInAPayrollPeriod ? '0' : '' ) . $intHoursInAPayrollPeriod . ' hours ' . $strMinutes . ' minutes ';

		return $strTotalHoursInAPayrollPeriod;
	}

	public static function getSearchCriteria( $objEmployeeFilter ) {
		$arrstrWhereParameters = [];

		// Create SQL parameters.
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getNameFirst() ) ) {
			$arrstrWhereParameters[] = 'e.name_first ILIKE \'%' . trim( addslashes( $objEmployeeFilter->getNameFirst() ) ) . '%\'';
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getNameLast() ) ) {
			$arrstrWhereParameters[] = 'e.name_last ILIKE \'%' . trim( addslashes( $objEmployeeFilter->getNameLast() ) ) . '%\'';
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getEmailAddress() ) ) {
			$arrstrWhereParameters[] = 'e.email_address ILIKE \'%' . trim( addslashes( $objEmployeeFilter->getEmailAddress() ) ) . '%\'';
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getBirthDate() ) ) {
			$arrstrWhereParameters[] = 'e.birth_date = \'' . date( 'Y-m-d', strtotime( $objEmployeeFilter->getBirthDate() ) ) . ' 00:00:00\'';
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getJoiningDate() ) ) {
			$arrstrWhereParameters[] = 'e.date_started = \'' . date( 'Y-m-d', strtotime( $objEmployeeFilter->getJoiningDate() ) ) . ' 23:59:59\'';
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getDepartmentId() ) ) {
			$arrstrWhereParameters[] = 'e.department_id = ' . ( int ) $objEmployeeFilter->getDepartmentId();
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getDesignationId() ) ) {
			$arrstrWhereParameters[] = 'e.designation_id = ' . ( int ) $objEmployeeFilter->getDesignationId();
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getTeamManagerId() ) ) {
			$arrstrWhereParameters[] = 'e.reporting_manager_id = ' . ( int ) $objEmployeeFilter->getTeamManagerId();
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getStatusTypeId() ) ) {
			$arrstrWhereParameters[] = 'e.employee_status_type_id = ' . ( int ) $objEmployeeFilter->getStatusTypeId();
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getEmployeeId() ) ) {
			$arrstrWhereParameters[] = 'e.id = ' . ( int ) $objEmployeeFilter->getEmployeeId();
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getPhoneNumber() ) ) {
			$arrstrWhereParameters[] = 'epn.phone_number = \'' . addslashes( $objEmployeeFilter->getPhoneNumber() ) . '\'';
		}
		if( 0 < \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getCountryCode() ) && 0 >= \Psi\CStringService::singleton()->strlen( $objEmployeeFilter->getEmployeeId() ) ) {
			$arrstrWhereParameters[] = 'ea.country_code = \'' . addslashes( $objEmployeeFilter->getCountryCode() ) . '\'';
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			return $arrstrWhereParameters;
		} else {
			return NULL;
		}
	}

	public static function getFromParameters( $objEmployeeFilter ) {
		$arrstrFromParameters = [];

		if( false == is_null( $objEmployeeFilter->getPhoneNumber() ) ) {
			$arrstrFromParameters[] .= ' employee_phone_numbers as epn';
		}
		if( false == is_null( $objEmployeeFilter->getDepartmentId() ) || 'department_id' == $objEmployeeFilter->getOrderByField() ) {
			$arrstrFromParameters[] .= ' departments as d';
		}
		if( false == is_null( $objEmployeeFilter->getStatusTypeId() ) || 'employee_status_type_id' == $objEmployeeFilter->getOrderByField() ) {
			$arrstrFromParameters[] .= ' employee_status_types est';
		}
		if( false == is_null( $objEmployeeFilter->getTeamManagerId() ) ) {
			$arrstrFromParameters[] .= ' team_employees as te ';
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrFromParameters ) ) {
			return $arrstrFromParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchActiveOrAllEmployees( $boolIsShowDisableData, $objDatabase ) {
		$strSql = 'SELECT *,
						CASE
							WHEN NOW() > date_terminated
							THEN 1
							ELSE 0
						END AS is_terminated
						FROM employees 
						WHERE 
						employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW;

		if( 1 != $boolIsShowDisableData ) {
			$strSql .= ' WHERE date_started IS NULL OR date_terminated IS NULL OR NOW() < date_terminated ';
		}

		$strSql .= ' ORDER BY name_last';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchProjectManagers( $boolIsDoeList = false, $objDatabase, $strOrderBy = 'name_full', $boolCaReleaseNote = false, $boolAllEmployees = false ) {
		$strCondition        = '';
		$strOrderByCondition = 'name_full';

		if( true == $boolIsDoeList ) {

			$strCondition        = 'OR designation_id IN ( ' . implode( ',', CDesignation::$c_arrintDirectorsOfEngineeringDesignations ) . ' )';
			$strOrderByCondition = $strOrderBy;

		}

		if( false == $boolCaReleaseNote ) {

			if( true == $boolAllEmployees ) {
				$strCondition .= ' AND employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ', ' . CEmployeeStatusType::PREVIOUS . ' ) AND date_terminated IS NULL';
			} else {
				$strCondition .= ' AND employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND date_terminated IS NULL';
			}
		}
		$strSql = 'SELECT * FROM employees WHERE is_project_manager = 1 ' . $strCondition . ' ORDER BY ' . $strOrderByCondition;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamLeadsByDepartmentIds( $arrintDepartmentIds, $objDatabase, $intProductId = NULL, $intProductOptionId = NULL ) {
		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strJoin = $strProductUnion = $strUnion = $strOrderByUnion = '';
		$strUnionWhere = 'IS NULL';
		$strOrderBy = ' ORDER BY e.name_full,e.preferred_name';
		if( true == valId( $intProductId ) ) {
			$strProductJoin = 'JOIN ps_products pp ON ( e2.id = pp.sqm_employee_id OR e2.id = pp.qam_employee_id OR e2.id = pp.qa_employee_id )
 								WHERE
									pp.id = ' . $intProductId . '';

			if( true == valId( $intProductOptionId ) ) {
				$strUnionWhere = '= ' . $intProductOptionId;
				$strProductJoin = 'JOIN ps_product_options ppo ON ( e2.id = ppo.sqm_employee_id OR e2.id = ppo.qam_employee_id OR e2.id = ppo.qa_employee_id )
								WHERE
									ppo.id = ' . $intProductOptionId . '';
			}
			$strProductUnion = ' UNION
							( SELECT
								DISTINCT ( e2.* )
							FROM
								employees e2 ' . $strProductJoin . ' AND e2.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ')';
			$strJoin = ' JOIN teams t ON ( te.team_id = t.id AND ( ( t.ps_product_id = ' . $intProductId . ' AND t.ps_product_option_id ' . $strUnionWhere . ' ) AND t.deleted_on IS NULL ) )';
			$strUnion = ' UNION
						( SELECT
							DISTINCT( e1.* )
						FROM
							employees e1
							LEFT JOIN ps_product_employees ppe ON ( e1.id = ppe.employee_id AND ppe.ps_product_employee_type_id = ' . CPsProductEmployeeType::QA_TEAM_LEAD . ')
						WHERE
							ppe.ps_product_id = ' . $intProductId . ' AND ppe.ps_product_option_id ' . $strUnionWhere . ' AND e1.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ')';

			$strOrderBy = '';
			$strOrderByUnion = ' ORDER BY preferred_name';

			$strJoinOn = 'OR e3.id IN ( t.qam_employee_id, t.qa_employee_id )';
			$strRecursiveSelect = 'WITH RECURSIVE all_managers(employee_id, manager_employee_id) AS (
									SELECT e.id as employee_id, e.reporting_manager_id as manager_employee_id FROM (';
			$strRecursiveSelectEnd = ') e 
						UNION ALL
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM
							all_managers am
							JOIN employees e ON ( e.id = am.manager_employee_id )
						WHERE
							e.id <> e.reporting_manager_id
					)
					SELECT
						DISTINCT( e.* )
					FROM
						all_managers
						JOIN employees e ON (all_managers.employee_id = e.id)
					WHERE
						e.date_terminated IS NULL AND
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.department_id = ' . CDepartment::QA;
		}

		$strSql = $strRecursiveSelect . '
					( SELECT
						DISTINCT( e.* )
					FROM
					employees e,
					team_employees te
					' . $strJoin . '
					JOIN employees e3 ON ( te.employee_id = e3.id ' . $strJoinOn . ' )
					WHERE
						e.id = e3.reporting_manager_id
						AND te.is_primary_team = 1
						AND e.is_project_manager = 0
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL
						AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
					' . $strOrderBy . ')' . $strUnion . $strProductUnion . $strRecursiveSelectEnd . $strOrderByUnion;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByRoleId( $intRoleId, $objDatabase, $boolIsActiveEmployee = false, $strCountryCode = '', $boolReturnArray = false ) {

		if( false == valId( $intRoleId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( e.id ) e.*,
						u.id as user_id,
						ea.country_code
					FROM
						employees e,
						users u,
						user_roles ur,
						employee_addresses ea
					WHERE
						e.id = u.employee_id
						AND u.id = ur.user_id
						AND e.id = ea.employee_id
						AND ur.deleted_on IS NULL
						AND ur.deleted_by IS NULL
						AND ea.address_type_id = ' . CAddressType::PRIMARY . '
						AND ur.role_id = ' . ( int ) $intRoleId;

		if( true == $boolIsActiveEmployee ) {
			$strSql .= ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		if( false == empty( $strCountryCode ) ) {
			$strSql .= ' AND ea.country_code = \'' . $strCountryCode . '\'';
		}

		return ( true == $boolReturnArray ) ? fetchData( $strSql, $objDatabase ) : self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByRoleIds( $arrintRoleIds, $objDatabase, $boolIsActiveEmployee = false, $boolIsDailyNuggetsEmail = false ) {
		if( false == valArr( $arrintRoleIds ) ) {
			return NULL;
		}

		if( true == $boolIsDailyNuggetsEmail ) {
			$strSql = 'SELECT DISTINCT e.*,';
		} else {
			$strSql     = 'SELECT e.*,';
			$strOrderBy = 'ORDER BY e.name_full';
		}

		$strSql .= '
						u.id as user_id
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
						JOIN user_roles ur ON ( u.id = ur.user_id )
					WHERE
						ur.deleted_on IS NULL
						AND	ur.deleted_by IS NULL
						AND ur.role_id IN (' . implode( ',', $arrintRoleIds ) . ')';

		if( true == $boolIsActiveEmployee ) {
			$strSql .= ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		$strSql .= $strOrderBy;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentUsEmployees( $objDatabase, $strOrderBy = 'name_full' ) {
		$strSql = 'SELECT
						e.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\' )
					WHERE
						e.employee_status_type_id = 1
						AND e.date_terminated IS NULL
						AND ea.country_code IN( \'US\' )
					ORDER BY
						' . $strOrderBy;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentHrEmployeesByCountryCode( $strCountryCode, $objDatabase ) {
		$strSql = 'SELECT
						e.*
					FROM
						employees e, employee_addresses ea
					WHERE
						e.id = ea.employee_id
						AND department_id = ' . CDepartment::HR . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND date_terminated IS NULL
						AND ea.country_code IN( \'' . addslashes( $strCountryCode ) . '\' )
						ORDER BY name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllUsEmployees( $objDatabase, $strOrderBy = 'name_full', $boolAllEmployees = false ) {
		$strWhereCondition = ' AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . ' AND e.date_terminated IS NULL';
		if( true == $boolAllEmployees ) {
			$strWhereCondition = ' AND employee_status_type_id IN ( ' . ( int ) CEmployeeStatusType::CURRENT . ',' . ( int ) CEmployeeStatusType::PREVIOUS . ' )';
		}
		$strSql = '	SELECT
						e.*
					FROM
						employees e, employee_addresses ea
					WHERE
						e.id = ea.employee_id
						AND ea.country_code IN( \'US\' )' .
		            $strWhereCondition . ' 
		            ORDER BY ' . $strOrderBy;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeeRegularHoursCounts( $intPayrollPeriodId, $objDatabase ) {
		$strSql = ' SELECT
						e.id,
						to_char( sum( (extract(epoch FROM ((eh.end_datetime)::TIMESTAMP - ( eh.begin_datetime )::TIMESTAMP))/3600) ), \'999.99\' ) AS regular_hours
					FROM
						employees e
						LEFT JOIN employee_hours eh ON( e.id = eh.employee_id )
						LEFT JOIN payroll_periods pp ON (eh.payroll_period_id = pp.id)
					WHERE
						pp.id = ' . ( int ) $intPayrollPeriodId . '
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					GROUP BY
						e.id';

		$arrfltRegularHoursCounts = fetchData( $strSql, $objDatabase );

		$arrfltFinalRegularHoursCounts = [];

		if( true == valArr( $arrfltRegularHoursCounts ) ) {
			foreach( $arrfltRegularHoursCounts as $arrfltRegularHoursCount ) {
				$arrfltFinalRegularHoursCounts[$arrfltRegularHoursCount['id']] = $arrfltRegularHoursCount;
			}
		}

		return $arrfltFinalRegularHoursCounts;
	}

	public static function fetchAllEmployeeTimeOffHoursCounts( $intPayrollPeriodId, $objDatabase ) {
		$strSql = ' SELECT
						evr.employee_id AS id,
						sum( DISTINCT evr.scheduled_vacation_hours) AS time_off_hours
					FROM
						employee_vacation_requests evr, payroll_periods pp
					WHERE
						pp.id = ' . ( int ) $intPayrollPeriodId . '
						AND (
								(evr.begin_datetime BETWEEN pp.begin_date AND pp.end_date AND evr.end_datetime BETWEEN pp.begin_date AND pp.end_date)
								OR
								(evr.end_datetime BETWEEN pp.begin_date AND pp.end_date)
							)
						AND evr.approved_by IS NOT NULL
						AND evr.approved_on IS NOT NULL
					GROUP BY
						evr.employee_id';

		$arrfltTimeOffHoursCounts = fetchData( $strSql, $objDatabase );

		$arrfltFinalTimeOffHoursCounts = [];

		if( true == valArr( $arrfltTimeOffHoursCounts ) ) {
			foreach( $arrfltTimeOffHoursCounts as $arrfltTimeOffHoursCount ) {
				$arrfltFinalTimeOffHoursCounts[$arrfltTimeOffHoursCount['id']] = $arrfltTimeOffHoursCount;
			}
		}

		return $arrfltFinalTimeOffHoursCounts;
	}

	public static function fetchSupportEmployeeByCid( $intCid, $objAdminDatabase ) {
		$strSql = 'SELECT
						ce.*
					FROM
						ps_lead_details pld,
						employees ce
					WHERE
						pld.support_employee_id = ce.id
						AND ce.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND pld.cid = ' . ( int ) $intCid;

		return self::fetchEmployee( $strSql, $objAdminDatabase );
	}

	public static function fetchSupportEmployees( $objAdminDatabase ) {
		$strSql = 'SELECT
						ce.*
					FROM
						ps_lead_details pld,
						clients mc,
						employees ce
					WHERE
						pld.support_employee_id = ce.id
						AND mc.id = pld.cid
						AND mc.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ',' . CCompanyStatusType::TERMINATED . ' )
						AND ce.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						lower( ce.name_last ),
						lower( ce.name_first )';

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchSupportEmployeesByEmployeeStatusTypeId( $arrintEmployeeStatusTypeIds, $objAdminDatabase ) {
		if( false == valArr( $arrintEmployeeStatusTypeIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						e.*
					FROM
						ps_lead_details pld
						JOIN clients AS c ON pld.cid = c.id AND c.company_status_type_id = 4
						JOIN employees AS e ON pld.support_employee_id = e.id
						JOIN designations AS d ON d.id = e.designation_id
					WHERE
						c.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ',' . CCompanyStatusType::TERMINATED . ' )
						AND e.employee_status_type_id IN (' . implode( ',', $arrintEmployeeStatusTypeIds ) . ')
						AND e.designation_id <> 243
					ORDER BY
						e.name_full
					';

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchSalesEmployeesByEmployeeStatusTypeId( $arrintEmployeeStatusTypeIds, $objAdminDatabase ) {
		if( false == valArr( $arrintEmployeeStatusTypeIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						e.*
					FROM employees e
						JOIN ps_lead_details AS pld ON e.id = pld.sales_employee_id
						JOIN clients AS c ON pld.cid = c.id
						JOIN designations AS d ON d.id = e.designation_id
					WHERE
						c.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ' )
						AND e.employee_status_type_id IN (' . implode( ',', $arrintEmployeeStatusTypeIds ) . ')
					ORDER BY
						e.name_full
					;';

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchManagerEmployees( $intEmployeeId, $objDatabase ) {
		$arrobjTeamEmployees = [];

		$strSql = 'SELECT
						e.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
						e.id = ' . ( int ) $intEmployeeId . '
						AND	employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )';

		$objEmployee = self::fetchEmployee( $strSql, $objDatabase );

		if( true == valObj( $objEmployee, 'CEmployee' ) ) {
			$arrobjTeamEmployees[]	= $objEmployee;
			$arrobjTeamEmployees	= self::fetchRecursiveManagerEmployeesByEmployeeId( $objEmployee->getManagerEmployeeId(), $arrobjTeamEmployees, $objDatabase );
		}

		return $arrobjTeamEmployees;
	}

	public static function fetchHrRepresentativeEmployeeByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						e.*
					FROM
						team_employees te
						JOIN teams t ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN employees e ON ( e.id = t.hr_representative_employee_id )
					WHERE
						te.employee_id = ' . ( int ) $intEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchAssociatedSalesEmployeesByDepartmentIds( $arrintDepartmentIds, $objDatabase ) {
		$strFieldName = '';

		switch( reset( $arrintDepartmentIds ) ) {

			case CDepartment::SUCCESS_MANAGEMENT:
				$strFieldName = 'pld.support_employee_id';
				break;

			default:
				$strFieldName = 'pld.sales_employee_id';

		}

		$strSql = 'SELECT
						*
					FROM
						(
							SELECT
								e.id,
								e.name_first,
								e.name_last,
								e.preferred_name,
								e.department_id,
								count ( * ) AS lead_count
							FROM
								employees e
								LEFT JOIN ps_lead_details pld ON ( ' . $strFieldName . ' = e.id )
								LEFT JOIN ps_leads pl ON ( pl.id = pld.ps_lead_id AND pl.deduped_on IS NULL AND pl.deleted_by IS NULL AND pl.company_status_type_id IN ( ' . implode( ',', CCompanyStatusType::$c_arrintActiveRelevantCompanyStatusTypeIds ) . ' ) )
							WHERE
								( ( e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
								OR pl.id IS NOT NULL )
							GROUP BY
								e.id,
								e.name_first,
								e.name_last,
								e.preferred_name,
								e.department_id
							ORDER BY
								lower ( e.name_first ),
								lower ( e.name_last )
						) AS sub
					WHERE
						department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
						OR lead_count > 0;';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchRecursiveManagerEmployeesByEmployeeId( $intEmployeeId, $arrobjTeamEmployees, $objDatabase ) {
		$strSql = 'SELECT
						e.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
						e.id = ' . ( int ) $intEmployeeId . '
						AND	e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )';

		$objEmployee = self::fetchEmployee( $strSql, $objDatabase );

		if( true == valObj( $objEmployee, 'CEmployee' ) && $objEmployee->getId() != $objEmployee->getManagerEmployeeId() ) {
			array_unshift( $arrobjTeamEmployees, $objEmployee );

			return $arrobjTeamEmployees = self::fetchRecursiveManagerEmployeesByEmployeeId( $objEmployee->getManagerEmployeeId(), $arrobjTeamEmployees, $objDatabase );

		} else {
			if( true == valObj( $objEmployee, 'CEmployee' ) ) {
				array_unshift( $arrobjTeamEmployees, $objEmployee );
			}

			return $arrobjTeamEmployees;
		}
	}

	public static function fetchTeamEmployeesByManagerId( $intManagerId, $objDatabase ) {
		if( false == is_numeric( $intManagerId ) ) return false;

		$strSql = ' SELECT
						e.*,
						ea.country_code
					FROM
						employees e,
						team_employees te,
						employee_addresses ea
					WHERE
						e.id = te.employee_id
						AND e.id = ea.employee_id
						AND ea.address_type_id = ' . CAddressType::PRIMARY . '
						AND te.is_primary_team = 1
						AND e.reporting_manager_id = ' . ( int ) $intManagerId . '
						AND e.reporting_manager_id != e.id
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )';

		$arrobjEmployees = self::fetchEmployees( $strSql, $objDatabase );

		$objStdObjectContainer                    = new stdClass();
		$objStdObjectContainer->m_arrobjEmployees = $arrobjEmployees;

		if( true == valArr( $arrobjEmployees ) ) {
			self::fetchRecursiveTeamEmployeesByManagerIds( array_keys( $arrobjEmployees ), $objStdObjectContainer, $objDatabase );
		} else {
			return NULL;
		}

		$arrobjEmployees        = $objStdObjectContainer->m_arrobjEmployees;
		$arrobjRekeyedEmployees = [];

		foreach( $arrobjEmployees as $objEmployee ) {
			$arrobjRekeyedEmployees[$objEmployee->getId()] = $objEmployee;
		}

		return $arrobjRekeyedEmployees;
	}

	public static function fetchTeamEmployeesByRoleIdByManagerId( $intRoleId, $intManagerId, $objDatabase ) {

		if( true == is_null( $intRoleId ) || false == is_numeric( $intManagerId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id
					FROM
						employees e
						JOIN users u ON ( u.employee_id = e.id )
						JOIN user_roles ur ON ( ur.user_id = u.id AND ur.role_id = ' . ( int ) $intRoleId . ' )
					WHERE
						e.reporting_manager_id = ' . ( int ) $intManagerId . '
						AND ( e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' OR e.employee_status_type_id = ' . CEmployeeStatusType::PREVIOUS . ' )
						AND ur.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecursiveTeamEmployeesByManagerIds( $arrintTeamEmployeeIds, $objStdObjectContainer, $objDatabase ) {
		if( false == valArr( $arrintTeamEmployeeIds ) ) {
			return $objStdObjectContainer;
		}

		$strSql = 'SELECT
						e.*,
						ea.country_code
					FROM
						employees e,
						employee_addresses ea
					WHERE
						e.id = ea.employee_id
						AND ea.address_type_id = ' . CAddressType::PRIMARY . '
						AND e.reporting_manager_id IN ( ' . implode( ',', $arrintTeamEmployeeIds ) . ' )
						AND	e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )';

		$arrobjTeamEmployees = self::fetchEmployees( $strSql, $objDatabase );

		if( true == valArr( $arrobjTeamEmployees ) ) {
			$objStdObjectContainer->m_arrobjEmployees = array_merge( $objStdObjectContainer->m_arrobjEmployees, $arrobjTeamEmployees );
			self::fetchRecursiveTeamEmployeesByManagerIds( array_keys( $arrobjTeamEmployees ), $objStdObjectContainer, $objDatabase );
		} else {
			return $objStdObjectContainer->m_arrobjEmployees;
		}
	}

	public static function fetchCurrentEmployeesWithUserIdByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchEmployees( 'SELECT e.*,u.id as user_id FROM employees e JOIN users u ON (e.id = u.employee_id) WHERE e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND ( date_started IS NULL OR e.date_terminated IS NULL OR NOW() < date_terminated ) AND e.department_id = ' . ( int ) $intDepartmentId . ' ORDER BY e.name_first ', $objDatabase );
	}

	public static function fetchCurrentEmployeesWithUserIdByDepartmentIds( $arrintDepartmentIds, $objDatabase ) {
		return self::fetchEmployees( 'SELECT e.*,u.id as user_id FROM employees e JOIN users u ON (e.id = u.employee_id) WHERE e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND ( date_started IS NULL OR e.date_terminated IS NULL OR NOW() < date_terminated ) AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) ORDER BY e.name_first ', $objDatabase );
	}

	public static function fetchCurrentIndianEmployees( $objDatabase ) {
		$strSql = 'SELECT
						e.*
					FROM
						employees e,
						employee_addresses ea
					WHERE
						ea.address_type_id = ' . CAddressType::PRIMARY . '
						AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id = ea.employee_id
						AND date_terminated IS NULL
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesByCountryCode( $strCountryCode, $objDatabase, $strOrderBy = NULL, $strSortOrder = 'ASC', $boolAllEmployees = false ) {
		if( false == valStr( $strCountryCode ) ) {
			return NULL;
		}
		$strWhereCondition = ' AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT;
		if( true == $boolAllEmployees ) {
			$strWhereCondition = ' AND employee_status_type_id IN ( ' . ( int ) CEmployeeStatusType::CURRENT . ',' . ( int ) CEmployeeStatusType::PREVIOUS . ' )';
		}

		$strSql = ' SELECT
						e.*,
						od.desk_number,
						od.id as desk_id
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN office_desks od ON ( e.office_desk_id = od.id )
					WHERE
						ea.country_code IN( \'' . addslashes( $strCountryCode ) . '\' ) ' .
		                $strWhereCondition;

		if( false == is_null( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ' . addslashes( $strOrderBy ) . ' ' . $strSortOrder;
		} else {
			$strSql .= ' ORDER BY
								e.preferred_name ';
		}

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeesByCountryCode( $strCountryCode, $objDatabase, $intShowTerminated = 2, $strStartDate = NULL, $strEndDate = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strWhereClause = '';

		if( 1 == $intShowTerminated ) {
			$strWhereClause = ' AND e.employee_status_type_id = ' . CEmployeeStatusType::PREVIOUS;
		} else {
			if( 0 == $intShowTerminated ) {
				$strWhereClause = ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
			}
		}

		if( false == empty( $strStartDate ) && false == empty( $strEndDate ) ) {
			$strWhereClause .= ' AND ( e.date_terminated IS NULL OR e.date_terminated BETWEEN \'' . $strStartDate . ' 00:00:00 \' AND \'' . $strEndDate . ' 23:59:59 \' )';
		}

		$strSql = 'SELECT
						e.*,
						ea.country_code
					FROM
						employees e,
						employee_addresses ea
					WHERE
						e.id = ea.employee_id
						AND ea.country_code IN( \'' . addslashes( $strCountryCode ) . '\' )
						' . $strWhereClause . '
					ORDER BY name_first, name_last';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentReviewers( $intEmployeeId, $objDatabase, $intMaxLimit = NULL, $strCountryCode = NULL, $arrintEmployeesPeerReviewsCount = NULL, $objSession = NULL ) {
		if( CCountry::CODE_USA == $strCountryCode ) {
			$strCondition360  = '';
		} else {
			$strCondition360  = ' AND ( ( 1 <= date_part( \'year\', ( age( e.date_started ) ) ) AND e.date_confirmed IS NOT NULL )
									OR e.is_360_review = 1
									OR( 1 > date_part( \'year\', ( age( e.date_started ) ) ) AND e.is_360_review = 1 )
								)';
		}

		$strSql = 'SELECT
						e.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
						e.id = ' . ( int ) $intEmployeeId . '
						AND designation_id IS NOT NULL
						AND	e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )
						AND	department_id IS NOT NULL';

		$objEmployee = self::fetchEmployee( $strSql, $objDatabase );

		if( true == valObj( $objEmployee, 'CEmployee' ) && false == is_null( $objEmployee->getManagerEmployeeId() ) ) {

			// Here we load ?
			if( CCountry::CODE_USA != $strCountryCode ) {
				$arrobjEmployees = self::fetchRecursiveManagerEmployees( $objEmployee, $objEmployee->getManagerEmployeeId(), $objDatabase );
			}

			$arrobjEmployees[] = $objEmployee;

			$strSql = 'SELECT
							e.*
						FROM
							employees e
							LEFT JOIN departments d ON ( d.id = e.department_id )
						WHERE
							e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
							AND ( date_terminated IS NULL OR date_terminated > NOW() )
							AND e.department_id = ' . ( int ) $objEmployee->getDepartmentId() . '
							AND e.designation_id IS NOT NULL
							' . $strCondition360 . '
							AND e.reporting_manager_id = ' . ( int ) $objEmployee->getManagerEmployeeId() . '
							AND e.id != ' . ( int ) $objEmployee->getId() . '
					ORDER BY random()';

			// This value looks to see if there's anyone on this team that can do a peer review.
			$arrobjColleaguesEmployees = self::fetchEmployees( $strSql, $objDatabase );

			$strSql = 'SELECT
							e.*
						FROM
							employees e
							JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
							LEFT JOIN departments d ON ( d.id = e.department_id )
						WHERE
							e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
							AND e.designation_id IS NOT NULL
							AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
							AND e.department_id = ' . ( int ) $objEmployee->getDepartmentId() . '
							' . $strCondition360 . '
							AND e.reporting_manager_id = ' . ( int ) $objEmployee->getId() . '
					ORDER BY random()';

			// Here we are loading the people who report to the individual being reviewed (assuming the person being reviewed is a manager)
			$arrobjTeamEmployees = self::fetchEmployees( $strSql, $objDatabase );

			$intTeamLimit       = 1;
			$intColleaguesLimit = 2;

			if( CCountry::CODE_USA == $strCountryCode ) {
				return self::calculateAndLoadEligibleReviewsForUsEmployees( $objEmployee->getManagerEmployeeId(), $arrobjTeamEmployees, $arrobjColleaguesEmployees, $intTeamLimit, $intColleaguesLimit, $arrintEmployeesPeerReviewsCount, $objSession );
			} else {
				return self::calculateAndLoadEligibleReviews( $arrobjTeamEmployees, $arrobjColleaguesEmployees, $arrobjEmployees, $intMaxLimit );
			}

		} else {
			return NULL;
		}
	}

	public static function fetchEmployeesPeerReviewsCount( $objDatabase ) {
		$strSql = ' SELECT
						DISTINCT eae.employee_id,
						count ( eae.employee_id ) AS peer_review_count
					FROM
						employees e
						JOIN employee_assessment_employees eae ON (eae.employee_id = e.id AND eae.finalized_on IS NULL)
						JOIN employee_assessments ea ON ( ea.id = eae.employee_assessment_id AND ea.employee_id NOT IN (
																														SELECT
																															DISTINCT e1.id
																														FROM
																															employees e1
																														WHERE
																															( e1.reporting_manager_id = e.id )
																															OR e1.id = e.id
						) )
					GROUP BY
						eae.employee_id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecursiveManagerEmployees( $objCurrentEmployee, $intEmployeeId, $objDatabase, $intManagerCount = 0, $intProjectManagerCount = 0, $arrobjEmployees = NULL, $boolFlag = 0, $boolIsShowPrevious = false ) {
		if( false == isset( $arrobjEmployees ) || false == valArr( $arrobjEmployees ) ) {
			$arrobjEmployees = [];
		}

		if( false == $boolIsShowPrevious ) {
			$strWhereClause = 'AND	e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND	e.department_id IS NOT NULL';
		}

		$strSql = 'SELECT
						e.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
						e.id = ' . ( int ) $intEmployeeId . '
						AND e.designation_id IS NOT NULL ' . $strWhereClause;

		$objEmployee = CEmployees::fetchEmployee( $strSql, $objDatabase );

		if( true == valObj( $objEmployee, 'CEmployee' ) ) {
			if( 1 == $objEmployee->getIsProjectManager() ) {
				$intProjectManagerCount++;
				$intManagerCount++;
			} else {
				$intManagerCount++;
			}

			$arrobjEmployees[] = $objEmployee;

			if( true == $boolFlag ) {
				return $arrobjEmployees = self::fetchRecursiveManagerEmployees( $objCurrentEmployee, $objEmployee->getManagerEmployeeId(), $objDatabase, $intManagerCount, $intProjectManagerCount, $arrobjEmployees, 1 );
			} else {
				if( true == is_numeric( $objEmployee->getManagerEmployeeId() ) && 0 < $objEmployee->getManagerEmployeeId() && $objEmployee->getId() != $objEmployee->getManagerEmployeeId() && ( ( 0 == $objCurrentEmployee->getIsProjectManager() && 1 > $intProjectManagerCount ) || ( 2 >= $intManagerCount && 2 >= $intProjectManagerCount && 1 == $objCurrentEmployee->getIsProjectManager() ) ) ) {
					return $arrobjEmployees = self::fetchRecursiveManagerEmployees( $objCurrentEmployee, $objEmployee->getManagerEmployeeId(), $objDatabase, $intManagerCount, $intProjectManagerCount, $arrobjEmployees );
				} else {
					return $arrobjEmployees;
				}
			}
		} else {
			return $arrobjEmployees;
		}
	}

	public static function fetchEmployeesAndUsersByDepartmentIdByCountryCode( $intDepartmentId, $strCountryCode, $objDatabase ) {

		if( true == is_null( $intDepartmentId ) ) {
			return NULL;
		}
		if( false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						emp.*,
						u.id as user_id,
						o.country_code
					FROM
						employees emp
						INNER JOIN users u ON ( emp.id = u.employee_id )
						LEFT JOIN offices o ON (o.id = emp.office_id )
					WHERE
						emp.department_id = ' . ( int ) $intDepartmentId . '
						AND o.country_code = \'' . $strCountryCode . '\'
						AND emp.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						ORDER BY emp.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByTeamId( $intTeamId, $objDatabase, $boolIsPrimaryTeam = false ) {

		$strPrimaryTeamCondition = ( true == $boolIsPrimaryTeam ) ? ' AND te.is_primary_team = 1 ' : '';

		$strSql = '	SELECT
						DISTINCT( e.* )
					FROM
						employees as e
						JOIN team_employees as te ON ( e.id = te.employee_id )
						LEFT JOIN designations d ON d.id = e.designation_id
						LEFT JOIN departments dept ON dept.id = e.department_id
					WHERE
						te.team_id = ' . ( int ) $intTeamId . $strPrimaryTeamCondition . '
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						e.name_first,e.name_last';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByTeamIds( $arrintTeamIds, $objDatabase, $boolIsShowTerminated = false, $strCountryCode = '' ) {
		if( false == valArr( $arrintTeamIds ) ) {
			return NULL;
		}

		$strWhereClause = ( true == $boolIsShowTerminated ) ? ' AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' ' : ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		$strJoinClause  = ( false == empty( $strCountryCode ) ) ? ' JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id =' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . $strCountryCode . '\' ) ' : '';

		$strSql = '	SELECT
						DISTINCT( e.* ),
						dept.name AS department_name
					FROM
						employees as e
						JOIN team_employees as te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						' . $strJoinClause . '
						LEFT JOIN departments dept ON ( dept.id = e.department_id )
					WHERE
						te.team_id IN ( ' . implode( ',', $arrintTeamIds ) . ' )
						' . $strWhereClause . '
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesCountByDateRangeByCountry( $strStartDate, $strEndDate, $strCountryCode, $objDatabase ) {
		if( false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						COUNT ( e.id ) AS monthly_joined_employees_count
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . $strCountryCode . '\' )
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_started IS NOT NULL
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND e.date_started BETWEEN \'' . addslashes( $strStartDate ) . '\' AND \'' . addslashes( $strEndDate ) . '\'';

		return $arrmixCurrentEmployeesCountData = fetchData( $strSql, $objDatabase );
	}

	public static function fetchTotalCurrentEmployeesCountByCountry( $strCountryCode, $objDatabase, $intDepartmentId = NULL, $boolCurrentEmployee = false ) {
		if( false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strWhereClause = 'e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )';

		if( true == $boolCurrentEmployee ) {
			$strWhereClause = 'e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ' )';
		}

		$strSql = ' SELECT
						COUNT( e.id ) AS total_current_employees_count
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\' )
					WHERE
						' . $strWhereClause . '
						AND ( e.date_started IS NOT NULL OR e.date_started <= NOW() )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= 'AND e.department_id = ' . ( int ) $intDepartmentId;
		}

		return $arrmixTotalCurrentEmployeesCountData = fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByCidsByDepartmentIds( $arrintCids, $arrintDepartmentIds, $objDatabase ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pld.cid,
						e.id as employee_id,
						e.name_first as name_first,
						e.name_last as name_last,
						e.email_address as email_address
					FROM
						employee_associations ea
						JOIN employees e ON ( ea.employee_id = e.id )
						JOIN ps_lead_details pld ON ( ea.ps_lead_id = pld.ps_lead_id )
						JOIN clients mc ON ( pld.cid = mc.id )
					WHERE
						e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
						AND ea.id IN (
							SELECT
								MAX(ea.id) as id
							FROM
								employee_associations ea
								JOIN employees e ON ( ea.employee_id = e.id )
								JOIN ps_lead_details pld ON ( ea.ps_lead_id = pld.ps_lead_id )
								JOIN clients mc ON ( pld.cid = mc.id )
							WHERE
								pld.cid IN ( ' . implode( ',', $arrintCids ) . ' )
								AND	ea.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
								AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
							GROUP BY pld.cid,ea.department_id ) ';

		return self::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchEmployeesEmailAddressesByRoleId( $intRoleId, $objDatabase ) {
		$strSql = 'SELECT
						distinct on (e.id) e.id,
						e.email_address
					FROM
						employees e
						LEFT JOIN users u ON ( u.employee_id = e.id )
						LEFT JOIN user_roles ur ON ( ur.user_id = u.id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND ur.role_id =' . ( int ) $intRoleId;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentIndianEmployeesOrderByEmployeeNumberOrderByField( $strFiledName, $objDatabase ) {
		$strFiledName = ( isset( $strFiledName ) ? $strFiledName : 'employee_number' );

		$strSql = 'SELECT
						e.*
					FROM
						employees e,
						employee_addresses ea
					WHERE
						ea.country_code = \'' . CCountry::CODE_INDIA . '\'
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id = ea.employee_id
						AND date_terminated IS NULL
					ORDER BY ' . addslashes( $strFiledName );

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeReferencesByStartDateByEndDateByCountryCode( $intPageNo, $intPageSize, $strStartDate, $strEndDate, $strCountryCode, $objDatabase, $boolHidePastReferee ) {

		$strSubSql = '';

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$strSubSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;
		}
		$strWhereSql = ( $boolHidePastReferee == true ) ? 'AND er.employee_status_type_id = ' . CEmployeeStatusType::CURRENT :'';

		$strSql = 'SELECT DISTINCT
						ea.employee_id,
						e.name_full AS employee_name_full,
						e.preferred_name AS employee_preferred_name,
						ea.referee_employee_id,
						er.name_full AS referee_name_full,
						er.preferred_name AS referee_preferred_name,
						e.date_started,
						d.name AS designation
					FROM
						employees e
						JOIN designations d ON( d.id = e.designation_id )
						JOIN employee_applications ea ON( e.id = ea.employee_id )
						JOIN employees er ON( er.id = ea.referee_employee_id )
						JOIN employee_addresses ead ON( ead.employee_id = er.id )
					WHERE
						ea.referee_employee_id IS NOT NULL
						AND ea.employee_id IS NOT NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT
				  		. $strWhereSql . '
						AND ead.country_code = \'' . $strCountryCode . '\'
						AND DATE_TRUNC( \'day\', ( e.date_started AT TIME ZONE \'IST\' ) ) BETWEEN \'' . addslashes( $strStartDate ) . '\' AND \'' . addslashes( $strEndDate ) . '\'
					ORDER BY
						er.name_full ASC' . $strSubSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeeById( $intEmployeeId, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM employees WHERE date_terminated IS NULL AND employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND id = ' . ( int ) $intEmployeeId;

		return self::fetchEmployee( $strSql, $objAdminDatabase );
	}

	public static function fetchActiveEmployeesByIds( $arrintEmployeeIds, $objAdminDatabase, $strSortBy = NULL ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM employees WHERE ( date_terminated IS NULL OR date_terminated >now() ) AND employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ';

		if( NULL != $strSortBy ) {
			$strSql .= ' ORDER BY ' . $strSortBy;
		}

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchSupportAndImplementationEmployeesByCids( $arrintCids, $objAdminDatabase, $boolIsSentToImplementationManager = false ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strConditionForImplementationManager = '';
		if( true == $boolIsSentToImplementationManager ) {
			$strConditionForImplementationManager = ' OR pld.implementation_employee_id = ce.id';
		}

		$strSql = 'SELECT
						mc.id,
						ce.id as employee_id,
						ce.email_address,
						ce.name_first || \' \' || ce.name_last as employee_name
					FROM
						employees ce
						JOIN ps_lead_details pld ON ( pld.support_employee_id = ce.id ' . $strConditionForImplementationManager . ' )
						JOIN clients mc ON ( mc.id = pld.cid )
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ce.date_terminated IS NULL
						AND ce.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND mc.company_status_type_id != ' . CCompanyStatusType::TERMINATED;

		$arrstrSupportAndImplementationEmployeesEmailAddresses = fetchData( $strSql, $objAdminDatabase );

		return $arrstrSupportAndImplementationEmployeesEmailAddresses;
	}

	public static function fetchAllCurrentEmployeesEmailIdAndFullName( $objDatabase, $strCountryCode = NULL ) {

		$strWhereClause = ( true == valStr( $strCountryCode ) ) ? 'AND ea.country_code IN( \'' . addslashes( $strCountryCode ) . '\' )' : '';

		$strSql = 'SELECT
						DISTINCT( e.email_address ),
						e.preferred_name AS name_full,
						e.id
					FROM
						employees e, employee_addresses ea
					WHERE
						e.id = ea.employee_id
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )
						' . $strWhereClause . '
					ORDER BY
						name_full';

		$arrmixEmployees = fetchData( $strSql, $objDatabase );

		return $arrmixEmployees;
	}

	public static function fetchCustomerManagerDetails( $objDatabase ) {
		$strSql = '	SELECT
						e.id,
						e.preferred_name,
						SUM(spl.total_new_active_acv) AS net_acv,
						COUNT ( DISTINCT ( mc.id ) ) AS client_count
					FROM
						employees e
						JOIN ps_lead_details AS pld ON( e.id = pld.support_employee_id AND e.name_last IS NOT NULL AND e.department_id IN (' . CDepartment::SUCCESS_MANAGEMENT . ') AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ' ) AND e.date_terminated IS NULL )
						JOIN stats_ps_leads AS spl ON (pld.ps_lead_id = spl.ps_lead_id)
						JOIN clients AS mc ON( pld.cid = mc.id AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
					GROUP BY
						e.id,
						e.preferred_name
					ORDER BY
						e.preferred_name ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchClientManagersDetailsByCidsByContractIds( $arrintCids, $arrintContractIds, $objDatabase ) {
		if( !valIntArr( $arrintCids ) || !valIntArr( $arrintContractIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						emp_details.contract_id,
						emp_details.cid,
						emp_details.implementation_employee_id,
						emp_details.sales_employee_id,
						emp_details.support_employee_id,
						emp_details.training_employee_id,
						e.preferred_name AS name_full,
						e.email_address,
						emp_details.director_of_seo_designation
					FROM
						employees e,
						(
							SELECT
								c.cid AS cid,
								CASE
									WHEN ce.employee_id IS NOT NULL THEN ce.employee_id
									ELSE pld.implementation_employee_id
								END AS implementation_employee_id,
								pld.sales_employee_id,
								pld.support_employee_id,
								pld.training_employee_id,
								c.id AS contract_id,
								CASE
									WHEN cp.ps_product_id = ' . CPsProduct::SEO_SERVICES . ' THEN ' . CDesignation::DIRECTOR_OF_SEO_SEM . '
								END AS director_of_seo_designation
							FROM
								contracts c
								JOIN ps_lead_details pld ON ( pld.cid = c.cid )
								LEFT JOIN contract_employees ce ON ( c.id = ce.contract_id AND ce.contract_employee_type_id = ' . CContractEmployeeType::IMPLEMENTATION . ' )
								LEFT JOIN contract_products cp ON ( c.id = cp.contract_id AND cp.ps_product_id IN ( ' . CPsProduct::ENTRATA . ',' . CPsProduct::RESIDENT_VERIFY . ',' . CPsProduct::SEO_SERVICES . ' ) )
							WHERE
								c.cid IN (' . implode( ',', $arrintCids ) . ')
								AND c.id IN (' . implode( ',', $arrintContractIds ) . ')
							GROUP BY
								c.id,
								c.cid,
								ce.employee_id,
								pld.implementation_employee_id,
								pld.sales_employee_id,
								pld.support_employee_id,
								pld.training_employee_id,
								cp.ps_product_id
						) AS emp_details
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id = emp_details.implementation_employee_id
						OR e.id = emp_details.sales_employee_id
						OR e.id = emp_details.support_employee_id
						OR e.id = emp_details.training_employee_id
						OR e.designation_id = emp_details.director_of_seo_designation';

		$arrstrTempCompanyManagersDetails = fetchData( $strSql, $objDatabase );

		if( !valArr( $arrstrTempCompanyManagersDetails ) ) {
			return NULL;
		}

		foreach( $arrstrTempCompanyManagersDetails as $arrstrCompanyManagerDetails ) {

			if( !is_null( $arrstrCompanyManagerDetails['sales_employee_id'] ) && $arrstrCompanyManagerDetails['id'] == $arrstrCompanyManagerDetails['sales_employee_id'] ) {
				$arrstrCompanyManagersDetails[$arrstrCompanyManagerDetails['contract_id']]['sales_employee_details'] = [ 'employee_id' => $arrstrCompanyManagerDetails['id'], 'employee_name' => $arrstrCompanyManagerDetails['name_full'], 'email_address' => $arrstrCompanyManagerDetails['email_address'] ];
			}

			if( !is_null( $arrstrCompanyManagerDetails['support_employee_id'] ) && $arrstrCompanyManagerDetails['id'] == $arrstrCompanyManagerDetails['support_employee_id'] ) {
				$arrstrCompanyManagersDetails[$arrstrCompanyManagerDetails['contract_id']]['support_employee_details'] = [ 'employee_id' => $arrstrCompanyManagerDetails['id'], 'employee_name' => $arrstrCompanyManagerDetails['name_full'], 'email_address' => $arrstrCompanyManagerDetails['email_address'] ];
			}

			if( !is_null( $arrstrCompanyManagerDetails['director_of_seo_designation'] ) && $arrstrCompanyManagerDetails['id'] == $arrstrCompanyManagerDetails['director_of_seo_designation'] ) {
				$arrstrCompanyManagersDetails[$arrstrCompanyManagerDetails['contract_id']]['director_of_seo_designation'] = [ 'employee_id' => $arrstrCompanyManagerDetails['id'], 'employee_name' => $arrstrCompanyManagerDetails['name_full'], 'email_address' => $arrstrCompanyManagerDetails['email_address'] ];
			}

			if( !is_null( $arrstrCompanyManagerDetails['training_employee_id'] ) && $arrstrCompanyManagerDetails['id'] == $arrstrCompanyManagerDetails['training_employee_id'] ) {
				$arrstrCompanyManagersDetails[$arrstrCompanyManagerDetails['contract_id']]['training_employee_id'] = [ 'employee_id' => $arrstrCompanyManagerDetails['id'], 'employee_name' => $arrstrCompanyManagerDetails['name_full'], 'email_address' => $arrstrCompanyManagerDetails['email_address'] ];
			}
		}

		return $arrstrCompanyManagersDetails;
	}

	public static function fetchClientManagersDetailsByCid( $intCid, $objDatabase ) {
		if( true == is_null( $intCid ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						mc.id AS cid,
						pld.implementation_employee_id,
						pld.sales_employee_id,
						pld.support_employee_id,
						e.preferred_name AS name_full,
						e.email_address
					From
						employees e
						JOIN ps_lead_details pld ON ( e.id = pld.implementation_employee_id OR e.id = pld.sales_employee_id OR e.id = pld.support_employee_id )
						JOIN clients mc ON ( pld.cid = mc.id )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND mc.company_status_type_id <> ' . CCompanyStatusType::TERMINATED . '
						AND mc.id = ' . ( int ) $intCid;

		$arrstrTempCompanyManagersDetails = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrstrTempCompanyManagersDetails ) ) {
			return NULL;
		}

		foreach( $arrstrTempCompanyManagersDetails as $arrstrCompanyManagerDetails ) {
			if( false == is_null( $arrstrCompanyManagerDetails['implementation_employee_id'] ) && $arrstrCompanyManagerDetails['id'] == $arrstrCompanyManagerDetails['implementation_employee_id'] ) {
				$arrstrCompanyManagersDetails['implementation_employee_details'] = [ 'employee_id' => $arrstrCompanyManagerDetails['id'], 'employee_name' => $arrstrCompanyManagerDetails['name_full'], 'email_address' => $arrstrCompanyManagerDetails['email_address'] ];
			}

			if( false == is_null( $arrstrCompanyManagerDetails['sales_employee_id'] ) && $arrstrCompanyManagerDetails['id'] == $arrstrCompanyManagerDetails['sales_employee_id'] ) {
				$arrstrCompanyManagersDetails['sales_employee_details'] = [ 'employee_id' => $arrstrCompanyManagerDetails['id'], 'employee_name' => $arrstrCompanyManagerDetails['name_full'], 'email_address' => $arrstrCompanyManagerDetails['email_address'] ];
			}

			if( false == is_null( $arrstrCompanyManagerDetails['support_employee_id'] ) && $arrstrCompanyManagerDetails['id'] == $arrstrCompanyManagerDetails['support_employee_id'] ) {
				$arrstrCompanyManagersDetails['support_employee_details'] = [ 'employee_id' => $arrstrCompanyManagerDetails['id'], 'employee_name' => $arrstrCompanyManagerDetails['name_full'], 'email_address' => $arrstrCompanyManagerDetails['email_address'] ];
			}
		}

		return $arrstrCompanyManagersDetails;
	}

	public static function fetchManagerEmployeesByIds( $arrintEmployeeIds, $objDatabase, $boolPrimaryTeam = false, $intPdmEmployeeId = NULL ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strJoin = '';
		if( false == is_null( $intPdmEmployeeId ) ) {
			$strJoin = 'JOIN teams t1 ON( t.team_id = t1.id and t1.sdm_employee_id = ' . ( int ) $intPdmEmployeeId . ' )';
		}

		$strPrimaryTeamCondition = ( true == $boolPrimaryTeam ) ? ' AND t.is_primary_team = 1' : '';

		$strSql = ' SELECT
						DISTINCT(e.id)
					FROM
						employees AS e
						JOIN team_employees AS t ON ( e.id = t.manager_employee_id ) ' . $strJoin . '
						JOIN employees AS e1 ON ( t.employee_id = e1.id AND e1.date_terminated IS NULL )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )' . $strPrimaryTeamCondition;

		return parent::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchManagerEmployeesByTaskStatusIds( $arrintTaskStatusIds, $objDatabase, $intDisableData = 0 ) {

		if( false == valArr( $arrintTaskStatusIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						t.project_manager_id as id,
						e.name_full
					FROM
						tasks t
						JOIN employees e ON ( t.project_manager_id = e.id )
					WHERE
						t.task_status_id IN ( ' . implode( ',', $arrintTaskStatusIds ) . ')
						' . ( 0 == $intDisableData ? 'AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT : '' ) . '
					GROUP BY
						t.project_manager_id,
						e.name_full
					ORDER BY
						e.name_full;';

		return parent::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchHrManagerEmployeesByIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT(e.id)
					FROM
						employees AS e
						JOIN teams AS t ON ( e.id = t.hr_representative_employee_id )
						JOIN team_employees AS te ON ( te.team_id = t.id )
						JOIN employees AS e1 ON ( te.employee_id = e1.id AND e1.date_terminated IS NULL )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND te.is_primary_team = 1
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return parent::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByDesignationIdsOrderByNameFirst( $arrintDesignationIds, $objDatabase ) {
		if( false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						e.id,
						e.designation_id,
						u.id as user_id,
						e.preferred_name as name_full,
						e.department_id,
						e.email_address,
						e.physical_phone_extension_id
					FROM
						employees e JOIN users u ON ( e.id = u.employee_id )
					WHERE
						e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' )
						AND u.id NOT IN ( ' . CUSER::ID_JORDAN_BRERETON . ' )
						AND u.is_disabled = 0
						AND e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesReportingManagerByIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						e.id AS employee_id,
						e1.id AS reporting_manager_id,
						e.preferred_name AS name_full,
						e1.preferred_name AS reporting_manager
					FROM
						employees as e
						JOIN employees as e1 ON ( e1.id = e.reporting_manager_id AND e1.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' )
					WHERE
						e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesReportingManagersByCountryCode( $objDatabase, $strCountryCode = NULL, $boolCurrentEmployee = true, $boolPrimaryTeam = false ) {
		$strJoinCondition        = NULL;
		$strWhereCondition       = NULL;
		$strPrimaryTeamCondition = NULL;

		if( false == is_null( $strCountryCode ) ) {
			$strJoinCondition = 'JOIN employee_addresses ea ON ( te.employee_id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . $strCountryCode . '\' )';
		}

		if( false == $boolCurrentEmployee ) {
			$strWhereCondition = ' AND ( e.date_terminated IS NULL OR e.date_terminated >= NOW() - INTERVAL \'1 year\' )';
		} else {
			$strWhereCondition = ' AND e.date_terminated IS NULL
								AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		if( false != $boolPrimaryTeam ) {
			$strPrimaryTeamCondition = ' AND te.is_primary_team = 1';
		}

		$strSql = 'SELECT
						e.id AS employee_id,
						u.id AS user_id,
						e.reporting_manager_id AS manager_employee_id,
						manager_users.id AS manager_user_id,
						e.department_id,
						e.designation_id,
						e.preferred_name AS name_full,
						te.is_primary_team,
						t.id AS team_id
					FROM teams t
						LEFT JOIN team_employees te on ( t.id = te.team_id ' . $strPrimaryTeamCondition . ' )
						LEFT JOIN employees e ON ( e.id = te.employee_id )
						LEFT JOIN users u ON ( u.employee_id = e.id )
						LEFT JOIN users manager_users ON ( manager_users.employee_id = e.reporting_manager_id )
						' . $strJoinCondition . '
					WHERE
						 e.id IS NOT NULL
						' . $strWhereCondition . '
					ORDER BY
						 e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByManagerEmployeeIdsByGroupIds( $arrintManagerEmployeeIds, $arrintGroupIds, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT (e.id),
						e.preferred_name as name_full
					 FROM
						employees as e
						JOIN team_employees as te ON (e.id = te.employee_id)
						JOIN teams AS t ON (te.team_id = t.id)
						JOIN users u ON ( u.employee_id = e.id )
						JOIN user_groups ug ON ( ug.user_id = u.id AND ug.deleted_by IS NULL )
					WHERE
						te.team_id IN (
										SELECT
											id
										FROM
											teams
										WHERE manager_employee_id IN ( ' . implode( ',', $arrintManagerEmployeeIds ) . ' )
										)
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ug.group_id IN ( ' . implode( ',', $arrintGroupIds ) . ')
						AND e.department_id IN ( ' . CDepartment::DEVELOPMENT . ', ' . CDepartment::QA . ' )

						AND e.date_terminated IS NULL
					ORDER BY
							e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchTpmEmployeesBySdmEmployeeIdsByGroupIds( $arrintSdmEmployeeIds, $arrintGroupIds, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT (e.id),
						e.preferred_name as name_full
					 FROM
						employees as e
						JOIN users u ON ( u.employee_id = e.id )
						JOIN user_groups ug ON ( ug.user_id = u.id AND ug.deleted_by IS NULL )
					WHERE
						e.id IN (
									SELECT
										tpm_employee_id
									FROM
										teams
									WHERE sdm_employee_id IN ( ' . implode( ',', $arrintSdmEmployeeIds ) . ' )
										AND tpm_employee_id IS NOT NULL
										AND deleted_by IS NULL
									GROUP BY
										tpm_employee_id
								)
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ug.group_id IN ( ' . implode( ',', $arrintGroupIds ) . ')
						AND e.department_id IN ( ' . CDepartment::DEVELOPMENT . ', ' . CDepartment::QA . ' )

						AND e.date_terminated IS NULL
					ORDER BY
							e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedEmployees( $intPageNo, $intPageSize, $objDatabase, $boolIsDisabled, $objEmployeeFilter, $strOrderByField = NULL, $strOrderByType = NULL, $intUserId = NULL ) {
		if( false == valObj( $objEmployeeFilter, 'CPsEmployeeFilter' ) ) {
			return NULL;
		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$intAddressType           = ( true == valStr( $objEmployeeFilter->getStateCodeIds() ) ) ? CAddressType::PERMANENT : CAddressType::PRIMARY;
		$arrstrJoinsAndConditions = self::applyEmployeeSearchCriteria( $objEmployeeFilter, $boolIsDisabled );
		$strWhereCondition        = '';
		$strJoinClause            = '';
		$strWhereUserCondition    = '';

		if( true == valArr( $arrstrJoinsAndConditions ) ) {
			if( true == valStr( $arrstrJoinsAndConditions['join'] ) ) {
				$strJoinClause = $arrstrJoinsAndConditions['join'];
			}

			if( true == valStr( $arrstrJoinsAndConditions['where'] ) ) {
				$strWhereCondition = $arrstrJoinsAndConditions['where'];
			}
		}
		if( true == is_numeric( $intUserId ) ) {
			$strWhereUserCondition = 'AND created_by = ' . ( int ) $intUserId;
		}

		$strSubSelectField = ( true == valStr( $objEmployeeFilter->getTeamIds() ) ) ? ', CASE WHEN t1.id IN ( ' . $objEmployeeFilter->getTeamIds() . ' ) and e.id = t1.manager_employee_id THEN e.preferred_name || \' (Manager)\' WHEN t1.id IN ( ' . $objEmployeeFilter->getTeamIds() . ' ) and e.id = t1.qa_employee_id THEN e.preferred_name || \' (QA Mentor)\' ELSE e.preferred_name END as preferred_name ' : ', e.preferred_name ';

		$strSql = 'SELECT
						DISTINCT e.*,
						ea.country_code,
						t.name AS team_name,
						(
							SELECT
								count( id )
							FROM
								actions
							WHERE
								e.id = employee_id
								AND action_result_id = ' . CActionResult::LIKE . '
						) AS like_count,
						(
							SELECT
								count( id )
							FROM
								actions
							WHERE
								e.id = employee_id
								AND action_result_id = ' . CActionResult::LIKE . '
								' . $strWhereUserCondition . '
							) AS is_like_employee,

						e1.preferred_name AS reporting_manager,
						CASE
							WHEN NOW ( ) > e.date_terminated THEN 1
							ELSE 0
						END AS is_terminated
						' . $strSubSelectField . '
					FROM
						employees e
						LEFT OUTER JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT OUTER JOIN employees e1 ON ( e.reporting_manager_id = e1.id )
						LEFT OUTER JOIN teams t ON ( t.id = te.team_id )
						LEFT OUTER JOIN employee_dependants ed ON ( ed. employee_id = e.id )
						LEFT OUTER JOIN employee_phone_numbers AS epn ON (e.id = epn.employee_id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . ( int ) $intAddressType . ' ) ' . $strJoinClause . '
					WHERE
						e.id NOT IN ( ' . CEmployee::ID_SYSTEM . ' , ' . CEmployee::ID_ADMIN . ' )
						 ' . $strWhereCondition . '
					ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType . ' NULLS LAST OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchSearchedEmployees( $arrstrFilteredExplodedSearch, $objDatabase, $boolIsDisabled, $boolNoShow = false ) {
		$strSearchDisabled = '';

		if( false == $boolIsDisabled ) {
			$strSearchDisabled = 'AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ',' . CEmployeeStatusType::RESTRICTED . ' ) ';
		}

		$strSearchCriteria 	= '';

		if( true == valArr( $arrstrFilteredExplodedSearch, 1 ) && true == isset( $arrstrFilteredExplodedSearch[0] ) && true == is_numeric( $arrstrFilteredExplodedSearch[0] ) ) {
			$strSearchCriteria = '	OR e.id = ' . ( int ) $arrstrFilteredExplodedSearch[0] . '
									OR e.employee_number = ' . ( int ) $arrstrFilteredExplodedSearch[0];
		}
		$strSearchNoShow = ( true == $boolNoShow ) ? ' OR e.employee_status_type_id = ' . CEmployeeStatusType::NO_SHOW : '';

		$strSql = 'SELECT
						DISTINCT e.*,
						ea.country_code,
						t.name AS team_name,
						CASE
							WHEN NOW ( ) > e.date_terminated THEN 1
							ELSE 0
						END AS is_terminated
					FROM
						employees e
						LEFT OUTER JOIN teams t ON ( t.manager_employee_id = e.id )
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						( e.name_first ILIKE \'%' . implode( '%\' AND e.name_first ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_last ILIKE \'%' . implode( '%\' AND e.name_last ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						' . $strSearchCriteria . '
						OR e.email_address ILIKE \'%' . implode( '%\' AND e.email_address ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR t.name ILIKE \'%' . implode( '%\' AND t.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
						 ' . $strSearchDisabled . ' ' . $strSearchNoShow . '
					ORDER BY
						e.name_first';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedEmployeesCount( $objDatabase, $boolIsDisabled, $objEmployeeFilter ) {
		if( false == valObj( $objEmployeeFilter, 'CPsEmployeeFilter' ) ) {
			return NULL;
		}

		$strWhereCondition = '';
		$strJoinClause     = '';

		$intAddressType           = ( true == valStr( $objEmployeeFilter->getStateCodeIds() ) ) ? CAddressType::PERMANENT : CAddressType::PRIMARY;
		$arrstrJoinsAndConditions = self::applyEmployeeSearchCriteria( $objEmployeeFilter, $boolIsDisabled );

		if( true == valArr( $arrstrJoinsAndConditions ) ) {
			if( true == valStr( $arrstrJoinsAndConditions['join'] ) ) {
				$strJoinClause = $arrstrJoinsAndConditions['join'];
			}
			if( true == valStr( $arrstrJoinsAndConditions['where'] ) ) {
				$strWhereCondition = $arrstrJoinsAndConditions['where'];
			}
		}

		$strSql         = 'SELECT
						count( DISTINCT e.id )
					FROM
						employees e
						LEFT OUTER JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT OUTER JOIN teams t ON ( t.id = te.team_id )
						LEFT OUTER JOIN employee_phone_numbers AS epn ON ( e.id = epn.employee_id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
						LEFT OUTER JOIN employee_dependants ed ON ( ed.employee_id = e.id )
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . ( int ) $intAddressType . ' ) ' . $strJoinClause . '
					WHERE
						e.id NOT IN ( ' . CEmployee::ID_SYSTEM . ' , ' . CEmployee::ID_ADMIN . ' )
						' . $strWhereCondition . '
						 ';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}
	}

	public static function fetchEmployeesAsProjectManager( $objDatabase ) {
		return self::fetchEmployees( 'SELECT * FROM employees WHERE is_project_manager = 1 AND date_terminated IS NULL  AND employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' ORDER BY name_full', $objDatabase );
	}

	public static function fetchEmployeesDetails( $objDatabase, $arrstrFilteredExplodedSearch = NULL, $boolIsQuickSearch = false, $boolShowDisabledData = false ) {
		$strSearchCriteria = '';
		$strCurrentDate    = date( 'n/j/Y' );

		if( true == $boolIsQuickSearch ) {
			if( false == valArr( $arrstrFilteredExplodedSearch ) ) {
				return NULL;
			}
			$strSearchCriteria = ' AND ';

			if( true == valArr( $arrstrFilteredExplodedSearch, 1 ) && true == isset( $arrstrFilteredExplodedSearch[0] ) && true == is_numeric( $arrstrFilteredExplodedSearch[0] ) ) {
				$strSearchCriteria .= ' ( e.id = ' . ( int ) $arrstrFilteredExplodedSearch[0] . '
										OR e.employee_number = ' . ( int ) $arrstrFilteredExplodedSearch[0] . '
										OR u.id = ' . ( int ) $arrstrFilteredExplodedSearch[0] . '
										OR epn.phone_number LIKE \'' . implode( '%\' AND epn.phone_number LIKE \'', $arrstrFilteredExplodedSearch ) . '%\' ) ';
			} else {
				$strSearchCriteria .= ' ( e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
										OR e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
										OR e.email_address ILIKE \'%' . implode( '%\' AND e.email_address ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' ) ';
			}

			$strSearchCriteria .= ' ORDER BY e.id DESC OFFSET 0 LIMIT 10';
		}

		$strShowDisabledDataCondition = ( false == $boolShowDisabledData ) ? ' AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ' ) AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() ) ' : ' ';

		$strSql = 'SELECT
						DISTINCT( e.id ),
						u.id AS user_id,
						e.preferred_name AS name_full,
						e.employee_status_type_id,
						e.office_id,
						e.office_desk_id,
						e.department_id,
						e.date_started,
						e.birth_date,
						e.email_address,
						e.physical_phone_extension_id,
						e.employee_number,
						epn.phone_number,
						ea.country_code,
						cs.state_code as state_code,
						ea.street_line1,
						ea.street_line2,
						ea.street_line3,
						ea.city,
						ea.state_code,
						ea.postal_code,
						d.desk_number,
						de.name AS designation_name,
						o.name as office_name,
						eh.date as attendance,
						ep.value AS hide_phone_number_setting,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
						JOIN users AS u ON ( e.id = u.employee_id )
						JOIN employee_phone_numbers epn ON ( e.id = epn.employee_id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN country_states cs ON ( e.id = ea.employee_id AND ea.country_state_id = cs.id )
						LEFT JOIN office_desks d ON ( e.office_desk_id = d.id )
						LEFT JOIN designations de ON ( e.designation_id = de.id )
						LEFT JOIN offices o ON ( o.id = e.office_id )
						LEFT JOIN employee_hours eh ON ( e.id = eh.employee_id AND eh.date = date_trunc( \'day\', \'' . $strCurrentDate . '\'::date ) )
						LEFT JOIN employee_preferences ep ON( e.id = ep.employee_id AND ep.key = \'' . CEmployeePreference::HIDE_PHONE_NUMBER . '\')
					WHERE
						e.id NOT IN ( ' . CEmployee::ID_SYSTEM . ', ' . CEmployee::ID_ADMIN . ' ) ' . $strShowDisabledDataCondition . $strSearchCriteria;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDetailsById( $intId, $objDatabase, $intUserId = NULL ) {
		if( true == is_null( $intId ) || false == is_numeric( $intId ) ) {
			return NULL;
		}

		$strWhereUserCondition = '';
		$strCurrentDate        = date( 'n/j/Y' );

		if( true == is_numeric( $intUserId ) ) {
			$strWhereUserCondition = 'AND created_by = ' . ( int ) $intUserId;
		}

		$strSql = 'SELECT
						pp.name as product_name,
						e2.name_full AS team_manager,
						e3.name_full AS employee_bu_hr,
						e3.id AS employee_bu_hr_id,
						e.*,
						de.name AS designation_name,
						epn.phone_number,
						ea.country_code,
						ea.city,
						d.name AS department_name,
						es.name AS emplyee_status_name,
						e1.preferred_name AS reporting_manager,
						e1.email_address AS manager_email,
						ep.value AS hide_phone_number_setting,
						t.name AS team_name,
						t.id AS team_id,
						u.username as username,
						u.is_super_user as is_super_user,
						u.id AS user_id,
						(
							SELECT
								count ( id )
							FROM
								actions
							WHERE
								e.id = employee_id
								AND action_result_id = ' . CActionResult::LIKE . '
						) AS like_count,
						(
							SELECT
								count ( id )
							FROM
								actions
							WHERE
								e.id = employee_id
								' . $strWhereUserCondition . '
								AND action_result_id = ' . CActionResult::LIKE . '
						) AS is_like_employee,
						e1.id AS reporting_manager_id,
						o.name as office_name,
						od.desk_number,
						eh.date as attendance
					FROM
						employees e
						JOIN employee_phone_numbers epn ON ( e.id = epn.employee_id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN departments d ON ( e.department_id = d.id )
						JOIN employee_status_types es ON ( e.employee_status_type_id = es.id )
						LEFT JOIN offices o ON ( o.id = e.office_id )
						LEFT JOIN employee_preferences ep ON( e.id = ep.employee_id AND ep.key = \'' . CEmployeePreference::HIDE_PHONE_NUMBER . '\')
						LEFT JOIN office_desks od ON ( e.office_desk_id = od.id )
						LEFT JOIN employee_hours eh ON ( e.id = eh.employee_id AND eh.date = date_trunc( \'day\', \'' . $strCurrentDate . '\'::date ) )
						LEFT JOIN designations de ON ( e.designation_id = de.id )
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						LEFT JOIN teams t ON ( te.team_id = t.id )
						LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
						LEFT JOIN users u ON ( u.employee_id = e.id )
						LEFT JOIN ps_products pp ON ( t.ps_product_id = pp.id )
						LEFT JOIN teams t2 ON ( pp.id = t2.ps_product_id )
						LEFT JOIN employees e2 ON ( e2.id = t2.manager_employee_id )
						LEFT JOIN employees e3 ON ( e3.id = t.hr_representative_employee_id )
					WHERE
						e.id = ' . ( int ) $intId . '
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id NOT IN ( ' . CEmployee::ID_SYSTEM . ', ' . CEmployee::ID_ADMIN . ' )
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDetailsByIdForEmployeeDirectory( $intId, $objDatabase, $intUserId = NULL ) {
		if( true == is_null( $intId ) || false == is_numeric( $intId ) ) {
			return NULL;
		}

		$strWhereUserCondition = '';
		$strCurrentDate        = date( 'n/j/Y' );

		if( true == is_numeric( $intUserId ) ) {
			$strWhereUserCondition = 'AND created_by = ' . ( int ) $intUserId;
		}

		$strSql = 'SELECT
						pp.name as product_name,
						e2.name_full AS team_manager,
						e3.name_full AS employee_bu_hr,
						e3.id AS employee_bu_hr_id,
						e.*,
						de.name AS designation_name,
						epn.phone_number,
						ea.country_code,
						ea.city,
						d.name AS department_name,
						es.name AS emplyee_status_name,
						e1.preferred_name AS reporting_manager,
						ep.value AS hide_phone_number_setting,
						t.name AS team_name,
						CASE
							WHEN
								ea.country_code= \'' . CCountry::CODE_INDIA . '\'
								AND e.id <> ' . CEmployee::ID_PREETAM_YADAV . '
								AND t3.name IS NOT NULL THEN t3.name
							ELSE t.name
						END AS team_name,
						u.username as username,
						u.id AS user_id,
						(
							SELECT
								count ( id )
							FROM
								actions
							WHERE
								e.id = employee_id
								AND action_result_id = ' . CActionResult::LIKE . '
						) AS like_count,
						(
							SELECT
								count ( id )
							FROM
								actions
							WHERE
								e.id = employee_id
								' . $strWhereUserCondition . '
								AND action_result_id = ' . CActionResult::LIKE . '
						) AS is_like_employee,
						e1.id AS reporting_manager_id,
						o.name as office_name,
						od.desk_number,
						eh.date as attendance
					FROM
						employees e
						JOIN employee_phone_numbers epn ON ( e.id = epn.employee_id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN departments d ON ( e.department_id = d.id )
						JOIN employee_status_types es ON ( e.employee_status_type_id = es.id )
						LEFT JOIN offices o ON ( o.id = e.office_id )
						LEFT JOIN employee_preferences ep ON( e.id = ep.employee_id AND ep.key = \'' . CEmployeePreference::HIDE_PHONE_NUMBER . '\')
						LEFT JOIN office_desks od ON ( e.office_desk_id = od.id )
						LEFT JOIN employee_hours eh ON ( e.id = eh.employee_id AND eh.date = date_trunc( \'day\', \'' . $strCurrentDate . '\'::date ) )
						LEFT JOIN designations de ON ( e.designation_id = de.id )
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						LEFT JOIN teams t ON ( te.team_id = t.id )
						LEFT JOIN teams t3 ON( e.id = t3.manager_employee_id AND t3.deleted_by IS NULL )
						LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
						LEFT JOIN users u ON ( u.employee_id = e.id )
						LEFT JOIN ps_products pp ON ( t.ps_product_id = pp.id )
						LEFT JOIN teams t2 ON ( pp.id = t2.ps_product_id )
						LEFT JOIN employees e2 ON ( e2.id = t2.manager_employee_id )
						LEFT JOIN employees e3 ON ( e3.id = t.hr_representative_employee_id )
					WHERE
						e.id = ' . ( int ) $intId . '
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id NOT IN ( ' . CEmployee::ID_SYSTEM . ', ' . CEmployee::ID_ADMIN . ' )
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeesDetails( $objEmployeeFilter, $objDatabase, $boolShowDisabledData = false, $boolIsDownload = false, $intUserId = NULL ) {
		if( false == valObj( $objEmployeeFilter, 'CPsEmployeeFilter' ) ) {
			return NULL;
		}

		$intOffset         = ( 0 < $objEmployeeFilter->getPageNo() ) ? $objEmployeeFilter->getPageSize() * ( $objEmployeeFilter->getPageNo() - 1 ) : 0;
		$strPagination     = ( false == $boolIsDownload ) ? ' OFFSET ' . ( int ) $intOffset . ' LIMIT	' . ( int ) $objEmployeeFilter->getPageSize() : '';
		$strSubSelectField = '';

		switch( $objEmployeeFilter->getOrderByField() ) {

			case 'country_and_name':
				$strOrderByField = ' ea.country_code, e.preferred_name';
				break;

			case 'name_full':
				$strOrderByField = ' e.preferred_name';
				break;

			case 'department_name':
				$strOrderByField = ' department_name';
				break;

			case 'reporting_manager':
				$strOrderByField = ' reporting_manager ';
				break;

			case 'country_code':
				$strOrderByField = ' ea.country_code';
				break;

			case 'email_address':
				$strOrderByField = ' e.email_address';
				break;

			case 'birth_date':
				$strOrderByField   = ' ( to_char( e.birth_date, \'mm-dd\' ) )';
				$strSubSelectField = ', ( to_char( e.birth_date, \'mm-dd\' ) )';
				break;

			default:
				$strOrderByField = ' ea.country_code, e.preferred_name';
				break;
		}

		switch( \Psi\CStringService::singleton()->strtoupper( $objEmployeeFilter->getOrderByType() ) ) {
			case 'DESC':
			case 'ASC':
				$strOrderByType = trim( \Psi\CStringService::singleton()->strtoupper( $objEmployeeFilter->getOrderByType() . ' NULLS LAST ' ) );
				break;

			default:
				$strOrderByType = 'ASC NULLS LAST';
				break;
		}

		$arrstrJoinsAndConditions = self::applyEmployeeSearchCriteria( $objEmployeeFilter, $boolShowDisabledData, $boolIsDownload );

		$strWhereCondition     = '';
		$strJoinClause         = '';
		$strWhereUserCondition = '';

		if( true == valArr( $arrstrJoinsAndConditions ) ) {
			if( true == valStr( $arrstrJoinsAndConditions['join'] ) ) {
				$strJoinClause = $arrstrJoinsAndConditions['join'];
			}

			if( true == valStr( $arrstrJoinsAndConditions['where'] ) ) {
				$strWhereCondition = $arrstrJoinsAndConditions['where'];
			}
		}

		if( true == valStr( $objEmployeeFilter->getEmployeeIds() ) ) {
			$strWhereCondition = ' AND e.id IN ( ' . $objEmployeeFilter->getEmployeeIds() . ') ';
		}

		if( true == is_numeric( $intUserId ) ) {
			$strWhereUserCondition = 'AND created_by = ' . ( int ) $intUserId;
		}

		$strSubSelectField .= ( true == valStr( $objEmployeeFilter->getTeamIds() ) ) ? ', CASE WHEN t1.id IN ( ' . $objEmployeeFilter->getTeamIds() . ' ) and e.id = t1.manager_employee_id THEN e.preferred_name || \' (Manager)\' WHEN t1.id IN ( ' . $objEmployeeFilter->getTeamIds() . ' ) and e.id = t1.qa_employee_id THEN e.preferred_name || \' (QA Mentor)\' ELSE e.preferred_name END as preferred_name ' : ', e.preferred_name ';

		$strSql = 'SELECT
						DISTINCT( e.* ),
						epn.phone_number AS phone_number,
						epn1.phone_number AS emergency_contact_number,
						ea.country_code,
						d.name AS department_name,
						es.name AS emplyee_status_name,
						e.preferred_name as employee_name,
						e.pf_number_encrypted,
						e.birth_date,
						e.bonus_potential_encrypted,
						CASE WHEN trim( e1.preferred_name ) IS NULL THEN concat( e1.name_first, \' \', e1.name_last ) ELSE e1.preferred_name END AS reporting_manager,
						u1.id AS user_id,
						ea.street_line1,
						ea.street_line2,
						ea.street_line3,
						ea.city,
						ea.state_code,
						cs.state_code as state_code,
						ea.postal_code,
						eap.street_line1 as pstreet_line1,
						eap.street_line2 as pstreet_line2,
						eap.street_line3 as pstreet_line3,
						eap.city as pcity,
						ea.state_code as pstate_code,
						cs.state_code as permanent_state_code,
						csp.state_code AS current_state_code,
						eap.postal_code as ppostal_code,
						(
							SELECT
								count ( id )
							FROM
								actions
							WHERE
								e.id = employee_id
								AND action_result_id = ' . CActionResult::LIKE . '
						) AS like_count,
						(
							SELECT
								count ( id )
							FROM
								actions
							WHERE
								e.id = employee_id
								AND action_result_id = ' . CActionResult::LIKE . '
								' . $strWhereUserCondition . '
						) AS is_like_employee,
						de.name AS designation,
						o.name as office_name,
						od.desk_number,
						e.reporting_manager_id AS manager_employee_id,
						ep.value AS hide_phone_number_setting,
						t.name as team_name,
						CASE WHEN trim( emp.preferred_name ) IS NULL THEN concat( emp.name_first, \' \', emp.name_last ) ELSE emp.preferred_name END AS pdm_name,
						CASE WHEN trim( emp_qam.preferred_name ) IS NULL THEN concat( emp_qam.name_first, \' \', emp_qam.name_last ) ELSE emp_qam.preferred_name END AS quality_manager,
						CASE WHEN trim( emp_add.preferred_name ) IS NULL THEN concat( emp_add.name_first, \' \', emp_add.name_last ) ELSE emp_add.preferred_name END AS director,
						ec.card_number as card_number,
						( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) * 12 ) + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) + ( ead.experience_year * 12 ) + ead.experience_month AS total_experience,
						( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) * 12 ) + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) + COALESCE( employee_history.previous_experience, 0 ) AS xento_experience,
						CASE WHEN trim( e2.preferred_name ) IS NULL THEN concat( e2.name_first, \' \', e2.name_last ) ELSE e2.preferred_name END AS hr_representative ' . $strSubSelectField . ',
						ea2.id as employee_assesment_id,
						(
							SELECT
								avg( sqa.numeric_weight )
							FROM surveys s
								JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id and sqa.survey_template_question_id = 65 )
							WHERE s.survey_type_id = 4
								AND s.responder_reference_id = e.id
								AND s.subject_reference_id = e.id
								AND s.response_datetime::date >=
									( SELECT
										CASE
											WHEN ep1.value = \'' . self::APRIL_MONTH . '\' AND (to_char( NOW(), \'mm\' ) ) >= \'' . self::APRIL_MONTH_NUMBER . '\' THEN (to_char( NOW(), \'yyyy\') || to_char( NOW(), \'-04-01\' ))::date
											WHEN ep1.value = \'' . self::APRIL_MONTH . '\' AND (to_char( NOW(), \'mm\' ) ) < \'' . self::APRIL_MONTH_NUMBER . '\' THEN (to_char( NOW() - INTERVAL \'12 months\', \'yyyy\') || to_char( NOW(), \'-04-01\' ) )::date
											WHEN ep1.value = \'' . self::OCTOBER_MONTH . '\' AND (to_char( NOW(), \'mm\' )) >= \'' . self::OCTOBER_MONTH_NUMBER . '\' THEN (to_char( NOW(), \'yyyy\') || to_char( NOW(), \'-10-01\' ))::date
											WHEN ep1.value = \'' . self::OCTOBER_MONTH . '\' AND (to_char( NOW(), \'mm\' )) < \'' . self::OCTOBER_MONTH_NUMBER . '\' THEN (to_char( NOW() - INTERVAL \'12 months\', \'yyyy\' ) || to_char( NOW(), \'-10-01\' ))::date
										END
									FROM employee_preferences ep1
									WHERE
										ep1.employee_id = e.id
										AND ep1.key = \'' . CEmployeePreference:: EMPLOYEE_REVIEW_CYCLE . '\'
									) AND s.response_datetime::date <= NOW()::date
						) as employee_cpa_avarage_rating,
						(
							SELECT
								avg( sqa.numeric_weight )
							FROM surveys s
								JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id and sqa.survey_template_question_id = 89 )
							WHERE s.survey_type_id = 14
								AND s.survey_template_id = 25
								AND s.subject_reference_id = e.id
								AND s.response_datetime::date >=
									( SELECT
										CASE
											WHEN ep1.value = \'' . self::APRIL_MONTH . '\' AND (to_char( NOW(), \'mm\' ) ) >= \'' . self::APRIL_MONTH_NUMBER . '\' THEN (to_char( NOW(), \'yyyy\' ) || to_char(NOW(), \'-04-01\'))::date
											WHEN ep1.value = \'' . self::APRIL_MONTH . '\' AND (to_char( NOW(), \'mm\' ) ) < \'' . self::APRIL_MONTH_NUMBER . '\' THEN (to_char( NOW() - INTERVAL \'12 months\', \'yyyy\' ) || to_char( NOW(), \'-04-01\' ))::date
											WHEN ep1.value = \'' . self::OCTOBER_MONTH . '\' AND (to_char( NOW(), \'mm\' ) ) >= \'' . self::OCTOBER_MONTH_NUMBER . '\' THEN (to_char( NOW(), \'yyyy\' ) || to_char(NOW(), \'-10-01\'))::date
											WHEN ep1.value = \'' . self::OCTOBER_MONTH . '\' AND (to_char( NOW(), \'mm\' ) ) < \'' . self::OCTOBER_MONTH_NUMBER . '\' THEN (to_char( NOW() - INTERVAL \'12 months\', \'yyyy\' ) || to_char( NOW(), \'-10-01\' ))::date
										END
										FROM employee_preferences ep1
										WHERE
											ep1.employee_id = e.id
											AND ep1.key = \'' . CEmployeePreference:: EMPLOYEE_REVIEW_CYCLE . '\'
									) AND s.response_datetime::date <= NOW()::date
								) as manager_cpa_avarage_rating,
								tc.name	as technology
					FROM
						employees e
						LEFT OUTER JOIN employee_phone_numbers epn ON ( e.id = epn.employee_id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
						LEFT OUTER JOIN employee_phone_numbers epn1 ON ( e.id = epn1.employee_id AND epn1.phone_number_type_id = ' . CPhoneNumberType::MOBILE . ')
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employee_addresses eap ON ( e.id = eap.employee_id AND eap.address_type_id = ' . CAddressType::PERMANENT . ' )
						LEFT JOIN technologies tc ON ( tc.id = e.technology_id )
						LEFT JOIN country_states cs ON ( e.id = eap.employee_id AND eap.country_state_id = cs.id )
						LEFT JOIN country_states csp ON ( e.id = ea.employee_id AND ea.country_state_id = csp.id )
						LEFT JOIN employee_applications eapp ON ( eapp.employee_id = e.id )
						LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = eapp.id AND eapp.country_code = ea.country_code)
						JOIN departments d ON ( e.department_id = d.id )
						JOIN employee_status_types es ON ( e.employee_status_type_id = es.id )
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						LEFT JOIN users u1 ON ( u1.employee_id = e.id )
						LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id ) ' . $strJoinClause . '
						LEFT JOIN designations de ON( e.designation_id = de.id )
						LEFT JOIN offices o ON( e.office_id = o.id )
						LEFT JOIN office_desks od ON( e.office_desk_id = od.id )
						LEFT JOIN employee_preferences ep ON( e.id = ep.employee_id AND ep.key = \'' . CEmployeePreference::HIDE_PHONE_NUMBER . '\')
						LEFT JOIN employee_cards ec ON( ec.employee_id = e.id AND ec.is_active = 1 )
						LEFT JOIN teams AS t ON ( te.team_id = t.id )
						LEFT JOIN employees emp_qam ON ( t.qam_employee_id = emp_qam.id )
						LEFT JOIN employees emp_add ON ( t.add_employee_id = emp_add.id )
						LEFT JOIN employees emp ON ( t.sdm_employee_id = emp.id )
						LEFT JOIN employees e2 ON ( t.hr_representative_employee_id = e2.id )
						LEFT JOIN employee_dependants ed ON ( ed. employee_id = e.id )
						LEFT JOIN (
									SELECT
										subQuery.employee_id,
										sum ( subQuery.previous_experience ) as previous_experience
									FROM (
											SELECT
												eeh.employee_id,
												( extract ( YEAR FROM age ( eeh.date_terminated, eeh.date_started ) ) * 12 ) + extract ( month FROM age ( eeh.date_terminated, eeh.date_started ) ) AS previous_experience
											FROM
												employee_employment_histories eeh
										 ) AS subQuery
									GROUP BY
										subQuery.employee_id
								 ) AS employee_history ON ( e.id = employee_history.employee_id )
						LEFT JOIN employee_assessments ea2 on ( e.id = ea2.employee_id AND ea2.employee_assessment_type_id = ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ' )
					WHERE
						e.id NOT IN ( ' . CEmployee::ID_SYSTEM . ', ' . CEmployee::ID_ADMIN . ' ) ' . $strWhereCondition . '
					ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . $strPagination;

		$arrstrTempEmployeesDetails = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrstrTempEmployeesDetails ) ) {
			return NULL;
		}

		foreach( $arrstrTempEmployeesDetails as $arrstrEmployeeDetails ) {
			$arrstrEmployeesDetails[$arrstrEmployeeDetails['id']] = $arrstrEmployeeDetails;
		}

		return $arrstrEmployeesDetails;
	}

	public static function fetchPaginatedEmployeesDetailsCount( $objEmployeeFilter, $objDatabase, $boolShowDisabledData = false ) {
		if( false == valObj( $objEmployeeFilter, 'CPsEmployeeFilter' ) ) {
			return NULL;
		}

		$arrstrJoinsAndConditions = self::applyEmployeeSearchCriteria( $objEmployeeFilter, $boolShowDisabledData );

		$strWhereCondition = '';
		$strJoinClause     = '';

		if( true == valArr( $arrstrJoinsAndConditions ) ) {
			if( true == valStr( $arrstrJoinsAndConditions['join'] ) ) {
				$strJoinClause = $arrstrJoinsAndConditions['join'];
			}

			if( true == valStr( $arrstrJoinsAndConditions['where'] ) ) {
				$strWhereCondition = $arrstrJoinsAndConditions['where'];
			}
		}

		if( true == valStr( $objEmployeeFilter->getEmployeeIds() ) ) {
			$strWhereCondition = ' AND e.id IN ( ' . $objEmployeeFilter->getEmployeeIds() . ') ';
		}

		$strSql = 'SELECT
						count( DISTINCT(e.id) )
					FROM
						employees e
						JOIN employee_phone_numbers epn ON ( e.id = epn.employee_id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN departments d ON ( e.department_id = d.id )
						JOIN employee_status_types es ON ( e.employee_status_type_id = es.id )
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						LEFT JOIN employee_dependants ed ON ( ed. employee_id = e.id )
						LEFT JOIN teams t ON ( t.id = te.team_id ) ' . $strJoinClause . '
					WHERE
						e.id NOT IN ( ' . CEmployee::ID_SYSTEM . ', ' . CEmployee::ID_ADMIN . ' ) ' . $strWhereCondition;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function applyEmployeeSearchCriteria( $objEmployeeFilter, $boolShowDisabledData = false, $boolIsDownload = false ) {
		$arrstrJoinsAndConditions = [];
		if( false == valObj( $objEmployeeFilter, 'CPsEmployeeFilter' ) ) {
			return $arrstrJoinsAndConditions;
		}

		$strJoin           = '';
		$strWhereCondition = '';

		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getMessageOperatorIds() ) ) ? ' AND epn.message_operator_id IN ( ' . $objEmployeeFilter->getMessageOperatorIds() . ' )' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getGenders() ) ) ? ' AND e.gender IN ( ' . $objEmployeeFilter->getGenders() . ' )' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getBloodGroups() ) ) ? ' AND e.blood_group IN ( ' . $objEmployeeFilter->getBloodGroups() . ' )' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getBirthDate() ) ) ? ' AND to_char( e.birth_date, \'MM/DD\' ) LIKE \'' . $objEmployeeFilter->getBirthDate() . '%\'' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getKidsBirthDate() ) ) ? ' AND to_char( ed.birth_date, \'MM/DD\' ) LIKE \'' . $objEmployeeFilter->getKidsBirthDate() . '%\'' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getAnniversaryDate() ) ) ? ' AND to_char( e.anniversary_date, \'MM/DD\' ) LIKE \'' . $objEmployeeFilter->getAnniversaryDate() . '%\'' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getEmployeeSalaryTypeIds() ) ) ? ' AND e.employee_salary_type_id IN ( ' . $objEmployeeFilter->getEmployeeSalaryTypeIds() . ' )' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getDepartmentIds() ) ) ? ' AND e.department_id IN ( ' . $objEmployeeFilter->getDepartmentIds() . ' )' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getEmployeeIds() ) ) ? ' AND e.id IN ( ' . $objEmployeeFilter->getEmployeeIds() . ' )' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getTechnologyIds() ) ) ? ' AND e.technology_id IN ( ' . $objEmployeeFilter->getTechnologyIds() . ' )' : '';

		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getTeamIds() ) ) ? ' AND ( te.team_id IN ( ' . $objEmployeeFilter->getTeamIds() . ' ) OR ( t1.id IN ( ' . $objEmployeeFilter->getTeamIds() . ' ) ) )' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getDesignationIds() ) ) ? ' AND e.designation_id IN ( ' . $objEmployeeFilter->getDesignationIds() . ' )' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getIsProjectManager() ) ) ? ' AND e.is_project_manager = ' . $objEmployeeFilter->getIsProjectManager() : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getIsNoShow() ) ) ? ' AND e.employee_status_type_id = ' . CEmployeeStatusType::NO_SHOW : '';

		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getManagerEmployeeIds() ) ) ? ' AND e.reporting_manager_id IN ( ' . $objEmployeeFilter->getManagerEmployeeIds() . ' )' : '';
		$strWhereCondition .= ( true == valStr( $objEmployeeFilter->getHrRepresentativeEmployeeIds() ) ) ? 'AND t.hr_representative_employee_id IN ( ' . $objEmployeeFilter->getHrRepresentativeEmployeeIds() . ' )' : '';

		if( true == valStr( $objEmployeeFilter->getTeamIds() ) ) {
			$strJoin .= ' LEFT JOIN teams t1 ON ( t1.manager_employee_id = e.id OR t1.qa_employee_id = e.id )';
		}

		if( true == valStr( $objEmployeeFilter->getOfficeIds() ) ) {
			$strWhereCondition .= ' AND e.office_id IN ( ' . $objEmployeeFilter->getOfficeIds() . ' )';
		} elseif( true == valStr( $objEmployeeFilter->getCountryCode() ) ) {
			$arrstrCountryCode = explode( ',', $objEmployeeFilter->getCountryCode() );
			$strCountryCode    = implode( "','", $arrstrCountryCode );
			$strCountryCode    = "'" . $strCountryCode . "'";

			$strWhereCondition .= ' AND ea.country_code IN ( ' . $strCountryCode . ' )';
		}

		if( true == valStr( $objEmployeeFilter->getStateCodeIds() ) ) {
			$arrstrStateCode = explode( ',', $objEmployeeFilter->getStateCodeIds() );
			$strStateCode    = implode( "','", $arrstrStateCode );
			$strStateCode    = "'" . $strStateCode . "'";

			$strWhereCondition .= ' AND ea.country_state_id IN ( ' . $strStateCode . ' )';
		}

		if( true == valStr( $objEmployeeFilter->getFromDateStarted() ) && true == valStr( $objEmployeeFilter->getToDateStarted() ) && strtotime( $objEmployeeFilter->getToDateStarted() ) >= strtotime( $objEmployeeFilter->getFromDateStarted() ) ) {
			$strWhereCondition .= ' AND e.date_started BETWEEN \'' . $objEmployeeFilter->getFromDateStarted() . '\' AND \'' . $objEmployeeFilter->getToDateStarted() . '\'';
		}

		if( true == valStr( $objEmployeeFilter->getFromDateConfirmed() ) && true == valStr( $objEmployeeFilter->getToDateConfirmed() ) && strtotime( $objEmployeeFilter->getToDateConfirmed() ) >= strtotime( $objEmployeeFilter->getFromDateConfirmed() ) ) {
			$strWhereCondition .= ' AND e.date_confirmed BETWEEN \'' . $objEmployeeFilter->getFromDateConfirmed() . '\' AND \'' . $objEmployeeFilter->getToDateConfirmed() . '\'';
		}

		if( true == valStr( $objEmployeeFilter->getFromDateTerminated() ) && true == valStr( $objEmployeeFilter->getToDateTerminated() ) && strtotime( $objEmployeeFilter->getToDateTerminated() ) >= strtotime( $objEmployeeFilter->getFromDateTerminated() ) ) {
			$strWhereCondition .= ' AND e.date_terminated BETWEEN \'' . $objEmployeeFilter->getFromDateTerminated() . '\' AND \'' . $objEmployeeFilter->getToDateTerminated() . '\'';
		}

		if( true == valStr( $objEmployeeFilter->getEmployeeStatusTypeIds() ) ) {
			$strWhereCondition .= ' AND e.employee_status_type_id IN ( ' . $objEmployeeFilter->getEmployeeStatusTypeIds() . ' )';
		} elseif( false == $boolShowDisabledData && false == valStr( $objEmployeeFilter->getIsNoShow() ) ) {
			$strEmployeeStatusType =  CEmployeeStatusType::PREVIOUS . ', ' . CEmployeeStatusType::NO_SHOW . ', ' . CEmployeeStatusType::RESTRICTED;
			$strEmployeeStatusType .=  ( true == $boolIsDownload ) ? ', ' . CEmployeeStatusType::SYSTEM : '';
			$strWhereCondition .= ' AND e.employee_status_type_id NOT IN ( ' . $strEmployeeStatusType . ' ) AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() ) ';
		}

		if( true == valStr( $objEmployeeFilter->getGroupIds() ) || true == valStr( $objEmployeeFilter->getRoleIds() ) ) {
			$strJoin .= ' JOIN users u ON ( e.id = u.employee_id )';
			if( true == valStr( $objEmployeeFilter->getGroupIds() ) ) {
				$strWhereCondition .= ' AND ug.group_id IN ( ' . $objEmployeeFilter->getGroupIds() . ' )';
				$strJoin           .= ' LEFT JOIN user_groups ug ON ( ug.user_id = u.id AND ug.deleted_by IS NULL )';
			}

			if( true == valStr( $objEmployeeFilter->getRoleIds() ) ) {
				$strWhereCondition .= ' AND ur.role_id IN ( ' . $objEmployeeFilter->getRoleIds() . ' )';
				$strJoin           .= ' LEFT JOIN user_roles ur ON ( ur.user_id = u.id )';
			}
		}

		if( ( true == is_numeric( $objEmployeeFilter->getExperienceWithUsFromYears() ) && true == is_numeric( $objEmployeeFilter->getExperienceWithUsFromMonths() ) ) && ( true == is_numeric( $objEmployeeFilter->getExperienceWithUsToYears() ) && true == is_numeric( $objEmployeeFilter->getExperienceWithUsToMonths() ) ) ) {

			$strTotalNumberOfMonthsWithUsFromDate = $objEmployeeFilter->getExperienceWithUsFromYears() * 12 + $objEmployeeFilter->getExperienceWithUsFromMonths();
			$intTotalNumberOfMonthsWithUsToDate   = $objEmployeeFilter->getExperienceWithUsToYears() * 12 + $objEmployeeFilter->getExperienceWithUsToMonths();

			$strWhereCondition .= '	AND ( extract ( YEAR FROM age ( e.date_started::TIMESTAMP ) ) * 12 + extract ( month FROM age ( e.date_started::TIMESTAMP ) ) ) BETWEEN \'' . $strTotalNumberOfMonthsWithUsFromDate . '\' AND \'' . $intTotalNumberOfMonthsWithUsToDate . '\'';

		} else {
			if( ( true == is_numeric( $objEmployeeFilter->getExperienceWithUsFromYears() ) || true == is_numeric( $objEmployeeFilter->getExperienceWithUsFromMonths() ) ) && ( false == is_numeric( $objEmployeeFilter->getExperienceWithUsToYears() ) && false == is_numeric( $objEmployeeFilter->getExperienceWithUsToMonths() ) ) ) {

				$strTotalNumberOfMonthsWithUsFromDate = $objEmployeeFilter->getExperienceWithUsFromYears() * 12 + $objEmployeeFilter->getExperienceWithUsFromMonths();
				$strWhereCondition                    .= 'AND ( extract ( YEAR FROM age ( e.date_started::TIMESTAMP ) ) * 12 + extract ( month FROM age ( e.date_started::TIMESTAMP ) ) ) >=\'' . $strTotalNumberOfMonthsWithUsFromDate . '\'';

			} else {
				if( ( false == is_numeric( $objEmployeeFilter->getExperienceWithUsFromYears() ) || false == is_numeric( $objEmployeeFilter->getExperienceWithUsFromMonths() ) ) && ( true == is_numeric( $objEmployeeFilter->getExperienceWithUsToYears() ) && true == is_numeric( $objEmployeeFilter->getExperienceWithUsToMonths() ) ) ) {

					$intTotalNumberOfMonthsWithUsToDate = $objEmployeeFilter->getExperienceWithUsToYears() * 12 + $objEmployeeFilter->getExperienceWithUsToMonths();
					$strWhereCondition                  .= ' AND ( extract ( YEAR FROM age ( e.date_started::TIMESTAMP ) ) * 12 + extract ( month FROM age ( e.date_started::TIMESTAMP ) ) ) <= \'' . $intTotalNumberOfMonthsWithUsToDate . '\'';

				}
			}
		}

		if( ( true == is_numeric( $objEmployeeFilter->getTotalExperienceFromYears() ) || true == is_numeric( $objEmployeeFilter->getTotalExperienceFromMonths() ) ) || ( true == is_numeric( $objEmployeeFilter->getTotalExperienceToYears() ) || true == is_numeric( $objEmployeeFilter->getTotalExperienceToMonths() ) ) ) {

			$strJoin .= 'LEFT OUTER JOIN (
									SELECT
										e.id,
										e.name_full,
										e.date_started,
										( COALESCE ( ( ead.experience_year * 12 + ead.experience_month ), 0 ) + ( extract ( YEAR
									FROM
										age ( e.date_started::TIMESTAMP ) ) * 12 + extract ( month
									FROM
										age ( e.date_started::TIMESTAMP ) ) ) ) AS total_experience
									FROM
										employee_application_details AS ead
										JOIN employee_applications AS ea ON ( ead.employee_application_id = ea.id )
										JOIN employees AS e ON ( ea.employee_id = e.id )
									) AS employee_experience ON ( employee_experience.id = e.id )';
		}

		if( ( true == is_numeric( $objEmployeeFilter->getTotalExperienceFromYears() ) && true == is_numeric( $objEmployeeFilter->getTotalExperienceFromMonths() ) ) && ( true == is_numeric( $objEmployeeFilter->getTotalExperienceToYears() ) && true == is_numeric( $objEmployeeFilter->getTotalExperienceToMonths() ) ) ) {

			$strTotalNumberOfMonthsFromDate = $objEmployeeFilter->getTotalExperienceFromYears() * 12 + $objEmployeeFilter->getTotalExperienceFromMonths();
			$strTotalNumberOfMonthsToDate   = $objEmployeeFilter->getTotalExperienceToYears() * 12 + $objEmployeeFilter->getTotalExperienceToMonths();

			$strWhereCondition .= ' AND employee_experience.total_experience BETWEEN \'' . $strTotalNumberOfMonthsFromDate . '\' AND \'' . $strTotalNumberOfMonthsToDate . '\'';

		} else {
			if( ( true == is_numeric( $objEmployeeFilter->getTotalExperienceFromYears() ) || true == is_numeric( $objEmployeeFilter->getTotalExperienceFromMonths() ) ) && ( false == is_numeric( $objEmployeeFilter->getTotalExperienceToYears() ) && false == is_numeric( $objEmployeeFilter->getTotalExperienceToMonths() ) ) ) {

				$strTotalNumberOfMonthsFromDate = $objEmployeeFilter->getTotalExperienceFromYears() * 12 + $objEmployeeFilter->getTotalExperienceFromMonths();
				$strWhereCondition              .= 'AND employee_experience.total_experience >= \'' . $strTotalNumberOfMonthsFromDate . '\'';

			} else {
				if( ( false == is_numeric( $objEmployeeFilter->getTotalExperienceFromYears() ) || false == is_numeric( $objEmployeeFilter->getTotalExperienceFromMonths() ) ) && ( true == is_numeric( $objEmployeeFilter->getTotalExperienceToYears() ) && true == is_numeric( $objEmployeeFilter->getTotalExperienceToMonths() ) ) ) {

					$strTotalNumberOfMonthsToDate = $objEmployeeFilter->getTotalExperienceToYears() * 12 + $objEmployeeFilter->getTotalExperienceToMonths();
					$strWhereCondition            .= 'AND employee_experience.total_experience <= \'' . $strTotalNumberOfMonthsToDate . '\'';
				}
			}
		}

		$arrstrJoinsAndConditions['join']  = $strJoin;
		$arrstrJoinsAndConditions['where'] = $strWhereCondition;

		return $arrstrJoinsAndConditions;
	}

	public static function fetchEmployeesByEmployeesExperienceByDepartmentIdByCountryCode( $intEmployeeId, $intDepartmentId, $strCountryCode, $objAdminDatabase, $boolIsCalculateEmployeeCount = false, $boolIsCalculateTeamEmployeeCount = false, $intManagerEmployeeId = NULL ) {
		$strSql = ' SELECT
						id, name_full, years_of_experience ';

		if( true == $boolIsCalculateEmployeeCount ) {
			$strSql = '	SELECT COUNT(id) AS employee_count, employee_joining_month';
		}

		$strSql .= ' FROM
						(
							SELECT
								DISTINCT ( e.* ), ';
		if( true == $boolIsCalculateEmployeeCount ) {
			$strSql .= ' EXTRACT ( MONTH FROM e.date_started ) employee_joining_month, ';
		}

		$strSql .= 'CASE
									WHEN ( NOW() - INTERVAL \'5 YEARS\' ) >= date_started THEN \'5+\'
									WHEN ( NOW() - INTERVAL \'2 YEARS\' ) >= date_started THEN \'2-5\'
									ELSE \'0-2\'
								END AS years_of_experience
							FROM
								employees AS e
								JOIN employee_addresses AS ea ON ( ea.employee_id = e.id ) ';

		$strWhereSql = '';
		if( true == $boolIsCalculateTeamEmployeeCount && true == is_numeric( $intManagerEmployeeId ) ) {
			$strWhereSql = ' AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' AND e.employee_status_type_id = 1 ';
		}

		$strSql .= ' WHERE
								e.department_id = ' . ( int ) $intDepartmentId . '
								AND ea.country_code = \'' . $strCountryCode . '\'
								' . $strWhereSql . '
						)AS sub_employees_years_of_experience
					 WHERE
						years_of_experience = (
												SELECT
													CASE
														WHEN ( NOW() - INTERVAL \'5 YEARS\' ) >= date_started THEN \'5+\'
														WHEN ( NOW() - INTERVAL \'2 YEARS\' ) >= date_started THEN \'2-5\'
														ELSE \'0-2\'
													END AS years_of_experience
												FROM
													employees
												WHERE
													id = ' . ( int ) $intEmployeeId . '
											)';
		if( true == $boolIsCalculateEmployeeCount ) {
			$strSql .= ' GROUP BY
									employee_joining_month
								 ORDER BY
									employee_joining_month';
		}

		if( true == $boolIsCalculateEmployeeCount ) {
			return fetchData( $strSql, $objAdminDatabase );
		} else {
			return self::fetchEmployees( $strSql, $objAdminDatabase );
		}
	}

	public static function fetchEmployeesByGroupIdByFilter( $intGroupId, $objTrainingGroupTraineesFilter, $objDatabase ) {
		$strSql = 'SELECT e.*,
							u.id as user_id,
							dept.name as department_name,
							d.name as designation_name
						FROM user_groups ug
							JOIN users as u ON ug.user_id = u.id
							JOIN employees as e ON u.employee_id = e.id
							LEFT JOIN departments as dept ON e.department_id = dept.id
							LEFT JOIN designations as d ON e.designation_id = d.id
						WHERE group_id = ' . ( int ) $intGroupId . '
							AND ug.deleted_by IS NULL
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						ORDER BY e.name_full ASC OFFSET ';

		$strSql .= ( 0 < $objTrainingGroupTraineesFilter->getPageNo() ) ? $objTrainingGroupTraineesFilter->getPageSize() * ( $objTrainingGroupTraineesFilter->getPageNo() - 1 ) : 0;
		$strSql .= ' LIMIT ' . ( int ) $objTrainingGroupTraineesFilter->getPageSize();

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchEmployeesByGroupId( $intGroupId, $arrstrFilteredExplodedSearch, $objDatabase ) {
		$strSql = 'SELECT u.id as user_id,
							e.preferred_name AS name_full,
							e.email_address as email_address,
							dept.name as department_name,
							d.name as designation_name
						FROM user_groups ug
							JOIN users as u ON ug.user_id = u.id
							JOIN employees as e ON u.employee_id = e.id
							LEFT JOIN departments as dept ON e.department_id = dept.id
							LEFT JOIN designations as d ON e.designation_id = d.id
						WHERE group_id = ' . ( int ) $intGroupId . '
							AND ug.deleted_by IS NULL
							AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
							AND e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						LIMIT 10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchEmployeesBySessionId( $intSessionId, $arrstrFilteredExplodedSearch, $objDatabase ) {
		if( false == valId( $intSessionId ) || false == valArr( $arrstrFilteredExplodedSearch ) ) return NULL;

		$strSql = 'SELECT u.id as user_id,
							e.name_full as name_full,
							e.email_address as email_address,
							dept.name as department_name,
							d.name as designation_name,
							ets.is_available
					FROM employee_training_sessions as ets
						JOIN users as u ON ets.user_id = u.id
						JOIN employees as e ON e.id = u.employee_id
						LEFT JOIN designations as d ON d.id = e.designation_id
						LEFT JOIN departments as dept ON dept.id = e.department_id
					WHERE ets.training_session_id = ' . ( int ) $intSessionId . '
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					LIMIT 10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeWithDesignationAndDepartmentByUserId( $intUserId, $objDatabase ) {
		if( false == valId( $intUserId ) ) return NULL;

		$strSql = 'SELECT
							e.id,
							u.id as user_id,
							e.preferred_name AS name_full,
							e.email_address,
							e.birth_date,
							e.date_started,
							d.name AS designation_name,
							dept.name AS department_name
						FROM
							users AS u
							LEFT JOIN employees AS e ON e.id = u.employee_id
							LEFT JOIN designations AS d ON e.designation_id = d.id
							LEFT JOIN departments AS dept ON dept.id = e.department_id
						WHERE
							e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
							AND u.id = ' . ( int ) $intUserId;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchEmployeesWithDesignationByUserIds( $arrintUserIds, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) ) {
			return NULL;
		}
		$strSql = ' SELECT
								e.*,
								u.id as user_id,
								d.name AS designation_name
							FROM
								employees e
								JOIN users AS u ON u.employee_id = e.id
								LEFT JOIN designations AS d ON d.id = e.designation_id
							WHERE
								u.id IN ( ' . implode( ', ', $arrintUserIds ) . ' )
								AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
							ORDER BY
								e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeWithCountryCodeById( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						e.*,ea.country_code,
						dep.name as designation_name
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.country_code IS NOT NULL)
						LEFT JOIN designations dep ON( dep.id = e.designation_id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id =' . ( int ) $intEmployeeId;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchEmployeesWithAllocatedDesksByCountryCode( $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
		          e.*
					FROM
						employees AS e
		          JOIN office_desks od ON( e.office_desk_id = od.id )
		          JOIN offices o ON( e.office_id = o.id )
					WHERE
						e.name_full IS NOT NULL
						AND e.office_id IS NOT NULL
						AND e.office_desk_id IS NOT NULL
						AND e.employee_status_type_id =\'' . ( int ) CEmployeeStatusType::CURRENT . '\'
						AND o.country_code =\'' . $strCountryCode . '\'
						AND od.is_active = \'' . COfficeDesk::ACTIVE_DESK . '\'
						ORDER BY e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesWithUnallocatedDesksByCountryCode( $strCountryCode, $objDatabase, $boolNewAllocation = false, $arrintEmployeeIds = NULL ) {

		$strJoinClause = '';

		if( true == $boolNewAllocation ) {
			$strSelectClause = '
						e.*,
						od.id';
			$strJoinClause   = '
						LEFT JOIN office_desks od ON( e.desk_allocation_office_desk_id = od.id AND od.is_reserved = ' . COfficeDesk::RESERVED_DESK . ' )';
			$strWhereClause  = '
						AND e.desk_allocation_office_desk_id IS NOT NULL';
		} else {
			$strSelectClause = '
						e.*';
			$strWhereClause  = '
						AND e.office_desk_id IS NULL
						AND e.desk_allocation_office_desk_id IS NULL';
		}

		if( true == valArr( $arrintEmployeeIds ) ) {
			$strWhereClause .= ' AND e.id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )';
		}

		$strSql = 'SELECT '
				  . $strSelectClause . '
					FROM
						employees AS e
						LEFT JOIN offices o ON( e.office_id = o.id )'
				  . $strJoinClause . '
					WHERE
						e.name_full IS NOT NULL
						AND e.office_id IS NOT NULL '
				  . $strWhereClause . '
						AND e.employee_status_type_id =' . ( int ) CEmployeeStatusType::CURRENT . '
						AND o.country_code =\'' . $strCountryCode . '\'
						AND e.department_id <> ' . CDepartment::ADMINISTRATIVE . '
					ORDER BY e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeByOfficeDeskId( $intOfficeDeskId, $objDatabase ) {
		return self::fetchEmployee( sprintf( 'SELECT * FROM employees WHERE employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND office_desk_id = %d LIMIT 1', ( int ) $intOfficeDeskId ), $objDatabase );
	}

	public static function fetchEmployeesWithDetailsByUserIds( $arrintUserIds, $objDatabase ) {
		$strSql = 'SELECT
						e.*,
						epn.phone_number,
						des.name AS designation_name,
						dep.name AS department_name
					FROM
						employees AS e
						LEFT JOIN users AS u ON e.id = u.employee_id
						LEFT JOIN employee_phone_numbers AS epn ON e.id = epn.employee_id
						LEFT JOIN designations AS des ON e.designation_id = des.id
						LEFT JOIN departments AS dep ON e.department_id = dep.id
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND u.id IN ( ' . implode( ', ', $arrintUserIds ) . ' )
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeWithDetailsByEmployeeId( $intEmployeeId, $objDatabase, $intIsLikeEmployeeUserId = NULL, $boolFetchArray = false, $boolLatestEmployeeApplication = false ) {
		$strWhereCondition = '';

		if( false == is_null( $intIsLikeEmployeeUserId ) ) {
			$strWhereCondition = 'AND created_by = ' . ( int ) $intIsLikeEmployeeUserId;
		}

		$strOrderByClause = ' ORDER BY e.name_full ';
		if( true == $boolLatestEmployeeApplication ) {
			$strOrderByClause = ' ORDER BY ea1.id';
		}

		$strSql = 'SELECT
						e.*,
						u.id as user_id,
						epn.phone_number,
						epn1.phone_number as emergency_contact_number,
						des.name AS designation_name,
						dep.name AS department_name,
						ea.country_code,
						ea1.employee_application_status_type_id,
						ead.experience_year,
						ead.experience_month,
						ead.bonus_amount_encrypted,
						ead.bonus_description_encrypted,
						ead.relocation_allowance_encrypted,
						e.reporting_manager_id AS manager_employee_id,
						e.business_unit_id,
						te.team_id,
						t.tpm_employee_id,
						t.add_employee_id,
						ea1.id as employee_application_id,
						e1.preferred_name AS manager_employee_name,
						(
							SELECT
								count ( id )
							FROM
								actions
							WHERE
								e.id = employee_id
								AND action_result_id = ' . CActionResult::LIKE . '
								AND e.id = ' . ( int ) $intEmployeeId . '
						) AS like_count,
						(
							SELECT
								count ( id )
							FROM
								actions
							WHERE
								e.id = employee_id
								AND action_type_id = ' . CActionType::EMPLOYEE . '
								AND action_result_id = ' . CActionResult::LIKE . $strWhereCondition . '
								AND e.id = ' . ( int ) $intEmployeeId . '
						) AS is_like_employee
					FROM
						employees AS e
						LEFT JOIN users AS u ON e.id = u.employee_id
						LEFT JOIN employee_phone_numbers AS epn ON ( e.id = epn.employee_id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
						LEFT JOIN employee_phone_numbers AS epn1 ON ( e.id = epn1.employee_id AND epn1.phone_number_type_id = ' . CPhoneNumberType::MOBILE . ' )
						LEFT JOIN employee_addresses AS ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN designations AS des ON e.designation_id = des.id
						LEFT JOIN departments AS dep ON e.department_id = dep.id
						LEFT JOIN employee_applications ea1 ON ( ea1.employee_id = e.id )
						LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = ea1.id )
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND is_primary_team = 1 )
						LEFT JOIN teams t ON( t.id = te.team_id )
						LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
					WHERE
						e.id = ' . ( int ) $intEmployeeId . '
					' . $strOrderByClause;
		if( true == $boolFetchArray ) {
			$arrstrResult = fetchData( $strSql, $objDatabase );

			return ( true == isset( $arrstrResult[0] ) ) ? $arrstrResult[0] : NULL;
		}

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchAllManagers( $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT
						e.preferred_name AS name_full,
						e.id
					FROM
						employees e,
						employee_assessments ea
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ea.manager_employee_id = e.id
						AND ea.deleted_by IS NULL
					ORDER BY
						e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchReviewStatusEmployeesByDate( $intYear, $strCountryCode, $strMonth, $strReviewType, $intAddEmployeeId = NULL, $objDatabase ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( true == is_numeric( $intAddEmployeeId ) ) {
			$strWhereClause = ' AND e.id IN ( WITH recursive all_employees(employee_id, manager_employee_id, depth) AS (
																			 SELECT
																				 e.id AS employee_id,
																				 e.reporting_manager_id AS manager_employee_id,
																				 1
																			 FROM
																				 employees e
																			 WHERE
																				 ( e.reporting_manager_id = ' . ( int ) $intAddEmployeeId . '
																				 OR e.id = ' . ( int ) $intAddEmployeeId . ' )
																			 UNION
																			 SELECT
																				 e.id AS employee_id,
																				 e.reporting_manager_id AS manager_employee_id,
																				 al.depth + 1
																			 FROM
																				 employees e,
																				 all_employees al
																			 WHERE
																				 e.reporting_manager_id = al.employee_id
																				 AND al.employee_id <> ' . ( int ) $intAddEmployeeId . '
															)
														SELECT
															e.id
														FROM
															all_employees al
															JOIN employees e ON (al.employee_id = e.id)
														WHERE
															e.date_terminated IS NULL
															AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' ) ';
		} else {
			$strWhereClause = ' ';
		}
		$strWhereCondition = '';

		if( CCountry::CODE_INDIA == $strCountryCode ) {
			$strWhereCondition = ( 'April' == $strMonth ) ? ' AND EXTRACT ( MONTH FROM eas.assessment_datetime ) BETWEEN 1 AND 6 ' : ' AND EXTRACT ( MONTH FROM eas.assessment_datetime ) BETWEEN 7 AND 12 ';
		}

		if( $strReviewType == 'self_pending' || $strReviewType == 'self_complete' || $strReviewType == 'self_total' ) {
			$strWhereConditionNew = ' AND e.id = eas.employee_id AND eae.employee_id = eas.employee_id';
			if( $strReviewType == 'self_complete' ) {
				$strWhereConditionNew .= ' AND eae.finalized_on IS NOT NULL';
			} else {
				if( $strReviewType == 'self_pending' ) {
					$strWhereConditionNew .= ' AND eae.finalized_on IS NULL';
				}
			}
		} else {
			if( $strReviewType == 'manager_pending' || $strReviewType == 'manager_complete' || $strReviewType == 'manager_total' ) {
				$strWhereConditionNew = ' AND ee.id = eas.manager_employee_id and eae.employee_id = eas.manager_employee_id';
				if( $strReviewType == 'manager_complete' ) {
					$strWhereConditionNew .= ' AND eae.finalized_on IS NOT NULL';
				} else {
					if( $strReviewType == 'manager_pending' ) {
						$strWhereConditionNew .= ' AND eae.finalized_on IS NULL';
					}
				}
			}
		}

		$strSql = ' SELECT
						e.id,e.employee_number,e.preferred_name
					FROM
						employee_assessments eas
						JOIN employees e ON ( e.id = eas.employee_id )
						JOIN employee_addresses eadd ON ( eadd.employee_id = e.id AND eadd.address_type_id = 1 )
						JOIN employee_assessment_types eat ON ( eat.id = eas.employee_assessment_type_id )
						LEFT JOIN employees ee ON ( eas.manager_employee_id = ee.id )
						LEFT JOIN employee_assessment_employees eae ON ( eas.id = eae.employee_assessment_id AND eae.deleted_by IS NULL )
					WHERE
						eadd.country_code = \'' . $strCountryCode . '\'' . $strWhereClause . '
						AND eas.deleted_by IS NULL
						AND eat.is_peer_review = 0
						AND eas.employee_assessment_type_id NOT IN ( ' . implode( ', ', CEmployeeAssessmentType::$c_arrintExcludeEmployeeAssessmentTypesForReviewStatus ) . ' )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL
						' . $strWhereConditionNew . '
						AND EXTRACT ( YEAR FROM eas.assessment_datetime ) = ' . ( int ) $intYear . '
						' . $strWhereCondition . '
						ORDER BY
						e.preferred_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeesAsManagersByCountryCode( $strCountryCode, $objDatabase ) {
		$strCondition 		= '';
		if( false == is_null( $strCountryCode ) ) {
			$strCondition = 'AND ea.country_code = \'' . $strCountryCode . '\'';
		}
		$strSql = 'SELECT
						e.id,
						e.preferred_name AS name_full,
						ea.country_code
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ')
					WHERE
						e.id IN (
								SELECT
									DISTINCT ( e.id ) AS manager_employee_id
								FROM
									team_employees te
									JOIN teams t ON( t.id = te.team_id AND t.deleted_on IS NULL AND t.id NOT IN( ' . CTeam::NEW_JOINEE_INDIA . ') )
									JOIN employees e1 ON ( te.employee_id = e1.id )
									JOIN employees e ON ( e.id = e1.reporting_manager_id )
									JOIN employee_addresses ea ON ( e.id=ea.employee_id ' . $strCondition . ' AND ea.address_type_id = ' . CAddressType::PRIMARY . ')
								WHERE
									te.is_primary_team = 1
									AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						)
					order by name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesByDepartmentIdByCountryCode( $intDepartmentId, $strCountryCode, $objDatabase ) {
		$strSql = 'SELECT
						e.*,
						od.desk_number
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN office_desks od ON ( e.office_desk_id = od.id )
					WHERE
						e.employee_status_type_id = 1
						AND ( date_terminated IS NULL OR date_terminated > NOW() )
						AND e.department_id = ' . ( int ) $intDepartmentId . '
						AND ea.country_code IN( \'' . addslashes( $strCountryCode ) . '\' )
								ORDER BY name_first, name_last';

		return self::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchEmployeeByTrainingTrainerAssociationIds( $arrintTrainingTrainerAssociationIds, $objDatabase ) {
		if( false == valArr( $arrintTrainingTrainerAssociationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*,
						u.id as user_id
					FROM employees e
					JOIN users u ON u.employee_id = e.id
					JOIN actions a ON a.secondary_reference = u.id
					JOIN training_trainer_associations tta ON tta.action_id = a.id
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND tta.id IN( ' . implode( ',', $arrintTrainingTrainerAssociationIds ) . ')';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByEmployeeFilter( $arrmixEmployeeFilter, $arrstrFilteredExplodedSearch, $objDatabase, $arrintNewTraineeIds = NULL ) {
		$strSearch = '';
		if( true == valArr( $arrstrFilteredExplodedSearch ) ) {

			$strSearch = ' AND ( e.date_started IS NULL OR e.date_terminated IS NULL OR NOW() < e.date_terminated )
										AND e.department_id <> ' . CDepartment::ADMINISTRATIVE . '
										AND ( e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
								)';
		}

		$strWhere = '';

		if( true == valArr( $arrintNewTraineeIds ) ) {

			$strWhere = 'AND e.id NOT IN ( ' . implode( ',', $arrintNewTraineeIds ) . ' )';
		}

		$strSql = 'SELECT e.*,
						d.name AS designation_name,
						EXTRACT(year FROM age(NOW(),e.date_started)) as experiance_year,
						EXTRACT(month FROM age(NOW(),e.date_started)) as experiance_month
					FROM
						employees AS e
						JOIN employee_addresses ea ON( e.id = ea.employee_id )
						LEFT JOIN team_employees te ON( te.employee_id = e.id and te.is_primary_team = 1 )
						LEFT JOIN teams t ON( te.team_id = t.id )
						LEFT JOIN designations de ON( e.designation_id = de.id )
						LEFT JOIN departments d ON( e.department_id = d.id )
						LEFT JOIN business_units bu ON( t.hr_representative_employee_id = bu.bu_hr_employee_id AND t.add_employee_id = bu.director_employee_id )
					WHERE e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . $strSearch . $strWhere;

		if( true == valArr( $arrmixEmployeeFilter['department_ids'] ) ) {
			$strSql .= 'AND e.department_id IN ( ' . implode( ',', $arrmixEmployeeFilter['department_ids'] ) . ' )';
		}

		if( true == valArr( $arrmixEmployeeFilter['designation_ids'] ) ) {
			$strSql .= 'AND e.designation_id IN ( ' . implode( ',', $arrmixEmployeeFilter['designation_ids'] ) . ' )';
		}

		if( true == valArr( $arrmixEmployeeFilter['add_employee_ids'] ) ) {
			$strSql .= 'AND t.add_employee_id IN ( ' . implode( ',', $arrmixEmployeeFilter['add_employee_ids'] ) . ' )';
		}

		if( true == valArr( $arrmixEmployeeFilter['tpm_employee_ids'] ) ) {
			$strSql .= 'AND t.tpm_employee_id IN ( ' . implode( ',', $arrmixEmployeeFilter['tpm_employee_ids'] ) . ' )';
		}

		if( true == valArr( $arrmixEmployeeFilter['tpoc_employee_ids'] ) ) {
			$strSql .= 'AND bu.training_representative_employee_id IN ( ' . implode( ',', $arrmixEmployeeFilter['tpoc_employee_ids'] ) . ' )';
		}

		if( true == valArr( $arrmixEmployeeFilter['qam_employee_ids'] ) ) {
			$strSql .= 'AND t.qam_employee_id IN ( ' . implode( ',', $arrmixEmployeeFilter['qam_employee_ids'] ) . ' )';
		}

		if( true == valArr( $arrmixEmployeeFilter['team_ids'] ) ) {
			$strSql .= 'AND te.team_id IN ( ' . implode( ',', $arrmixEmployeeFilter['team_ids'] ) . ' )';
		}

		if( false == empty( $arrmixEmployeeFilter['country_code'] ) ) {
			$strSql .= ' AND ea.country_code =\'' . $arrmixEmployeeFilter['country_code'] . '\'';
		}

		if( false == empty( $arrmixEmployeeFilter['experience_year_from'] ) || false == empty( $arrmixEmployeeFilter['experience_year_to'] ) || false == empty( $arrmixEmployeeFilter['experience_month_from'] ) || false == empty( $arrmixEmployeeFilter['experience_month_to'] ) ) {
			$intFromDays = ( $arrmixEmployeeFilter['experience_year_from'] * CEmployee::YEAR_DAYS ) + ( $arrmixEmployeeFilter['experience_month_from'] * CEmployee::MONTH_DAYS );
			$intToDays   = ( $arrmixEmployeeFilter['experience_year_to'] * CEmployee::YEAR_DAYS ) + ( $arrmixEmployeeFilter['experience_month_to'] * CEmployee::MONTH_DAYS );
			$strSql      .= ' AND ' . ( int ) $intFromDays . '<= date_part ( \'days\', now ( ) - e.date_started ) AND ' . ( int ) $intToDays . ' >= date_part ( \'days\', now ( ) - e.date_started )';
		}

		$strSql .= 'ORDER BY e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDetailsByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = sprintf( 'SELECT
						e.id id, est.name status, e.preferred_name, d.name Designation, e.name_full, epn.phone_number,e.date_started date_of_joining,ea.id application_id, ead.experience_year, ead.experience_month
					FROM
						employees e
						JOIN employee_status_types est ON (e.employee_status_type_id = est.id)
						JOIN designations d ON (e.designation_id = d.id)
						LEFT JOIN employee_phone_numbers epn ON (e.id = epn.employee_id)
						LEFT JOIN employee_applications as ea ON ( ea.employee_id = e.id)
						LEFT JOIN employee_application_details ead ON (ea.id = ead.employee_application_id)
					WHERE
						e.id = %d
						AND
						epn.phone_number_type_id = 1
					ORDER BY
						e.id', ( int ) $intEmployeeId );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEmailAddressByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployee( sprintf( 'SELECT email_address FROM employees WHERE employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchAllManagerEmployees( $objDatabase, $strCountryCode = NULL ) {
		$strSql = 'SELECT
						DISTINCT( e.* )
					FROM
						employees e
						JOIN employees e1 ON ( e.id = e1.reporting_manager_id )
						JOIN team_employees te ON ( e1.id = te.employee_id AND te.is_primary_team=1 )
						JOIN teams t ON t.id = te.team_id
						JOIN employee_addresses ea ON e.id = ea.employee_id AND address_type_id = ' . CAddressType::PRIMARY . '
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND t.deleted_by IS NULL
						' . ( false == is_null( $strCountryCode ) ? 'AND ea.country_code = \'' . $strCountryCode . '\'' : '' ) . '
					ORDER BY e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchDepartmentIdByCompanyUserId( $intUserId, $objDatabase ) {
		$strSql = 'SELECT
						e.department_id AS department_id
					FROM
						users AS u
						JOIN employees AS e ON u.employee_id = e.id
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND u.id = ' . ( int ) $intUserId;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchEmployeesHavingBirthdayTodayByUserIds( $arrintUserIds, $objDatabase ) {
		$strSql = 'SELECT e.name_full,
							e.birth_date,
							u.id as user_id
						FROM employees as e
							LEFT JOIN users as u ON u.employee_id = e.id
						WHERE u.id IN (' . implode( ', ', $arrintUserIds ) . ')
							AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
							AND ' . date( 'm' ) . ' = date_part( \'month\', e.birth_date )
							AND ' . date( 'd' ) . ' = date_part( \'day\', e.birth_date )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeesByName( $strSearchEmployee, $strEmployeeType, $objDatabase ) {
		$strWhereCondition = '';

		if( 'sales_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.department_id IN ( ' . CDepartment::SALES . ', ' . CDepartment::SALES_ADMINS . ' ) ';
		} elseif( 'support_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.department_id =' . CDepartment::SUCCESS_MANAGEMENT;
		} elseif( 'responsible_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.department_id <> ' . ( int ) CDepartment::ADMINISTRATIVE;
		} elseif( 'client_executive_and_sales_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.department_id IN ( ' . CDepartment::SALES . ',' . CDepartment::SALES_DIALER . ',' . CDepartment::SUCCESS_MANAGEMENT . ' ) ';
		} elseif( 'implementation_employees' == $strEmployeeType ) {
			$strWhereCondition = ' ( e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintImplementationDepartmentIds ) . ' ) ) ';
		} elseif( 'billing_employees' == $strEmployeeType ) {
			$strWhereCondition = ' ( e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.department_id =' . CDepartment::ACCOUNTING . ' ) ';
		} elseif( 'trainer_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND ea.country_code IN( \'US\') ';
		} elseif( 'reviewed_by_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT;
		} elseif( 'executive_sponsors' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND ea.country_code IN( \'US\') ';
		} elseif( 'users' == $strEmployeeType || 'employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id IN( ' . CEmployeeStatusType::CURRENT . ', ' . CEmployeeStatusType::SYSTEM . ')';
		} elseif( 'project_managers' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.is_project_manager = 1 ';
		} elseif( 'developers' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND ug.group_id = ' . CGroup::DEVELOPMENT;
		} elseif( 'qas' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND ug.group_id = ' . CGroup::QA;
		} elseif( 'client_resolutions_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.department_id = ' . CDepartment::TECHNICAL_SUPPORT;
		} elseif( 'director_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.designation_id IN ( ' . implode( ',', CDesignation::$c_arrintDirectorDesignationIds ) . ' ) ';
		} elseif( 'seo_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.department_id = ' . CDepartment::SEO_SERVICES;
		} elseif( 'sales_engineer_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.department_id IN ( ' . CDepartment::SALES_ENGINEER . ' ) ';
		}

		if( true == in_array( $strEmployeeType, [ 'users', 'developers', 'qas', 'employees' ] ) ) {
			$strWhereCondition .= ' AND u.id != ' . CUser::ID_DAVID_BATEMAN;
		}

		$strFromClause = '';

		if( 'executive_sponsors' == $strEmployeeType || 'trainer_employees' == $strEmployeeType ) {
			$strFromClause = 'LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\' )';
		}

		if( 'developers' == $strEmployeeType || 'qas' == $strEmployeeType ) {
			$strFromClause = 'LEFT JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )';
		}

		$strSql = '	SELECT
						e.id,
						u.id as user_id,
						e.preferred_name
					FROM
						employees e
						JOIN users u ON e.id = u.employee_id ' . $strFromClause . '
					WHERE
						e.preferred_name ILIKE \'%' . trim( addslashes( $strSearchEmployee ) ) . '%\'
						AND ' . $strWhereCondition . ' AND e.date_terminated IS NULL
					ORDER BY
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeesByPreferredName( $strSearchEmployee, $strEmployeeType, $objDatabase ) {
		$strWhereCondition = '';

		if( 'users' == $strEmployeeType || 'employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id IN( ' . CEmployeeStatusType::CURRENT . ', ' . CEmployeeStatusType::SYSTEM . ')';
		} elseif( 'project_managers' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.is_project_manager = 1 ';
		} elseif( 'developers' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND ug.group_id = ' . CGroup::DEVELOPMENT;
		} elseif( 'qas' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND ug.group_id = ' . CGroup::QA;
		} elseif( 'client_resolutions_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.department_id = ' . CDepartment::TECHNICAL_SUPPORT;
		} elseif( 'director_employees' == $strEmployeeType ) {
			$strWhereCondition = 'e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . ' AND e.designation_id IN ( ' . implode( ',', CDesignation::$c_arrintDirectorDesignationIds ) . ', ' . CDesignation::VP_OF_ENGINEERING . ', ' . CDesignation::CHIEF_TECHNICAL_OFFICER . ', ' . CDesignation::SVP_OF_ENGINEERING . ' ) ';
		}

		if( true == in_array( $strEmployeeType, [ 'users', 'developers', 'qas', 'employees' ] ) ) {
			$strWhereCondition .= ' AND u.id != ' . CUser::ID_DAVID_BATEMAN;
		}

		$strFromClause = '';

		if( 'executive_sponsors' == $strEmployeeType || 'trainer_employees' == $strEmployeeType ) {
			$strFromClause = 'LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\' )';
		}

		if( 'developers' == $strEmployeeType || 'qas' == $strEmployeeType ) {
			$strFromClause = 'LEFT JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )';
		}

		$strSql = '	SELECT
						e.id,
						u.id as user_id,
						e.preferred_name,
						e.department_id
					FROM
						employees e
						JOIN users u ON e.id = u.employee_id ' . $strFromClause . '
					WHERE
						e.preferred_name ILIKE \'%' . trim( addslashes( $strSearchEmployee ) ) . '%\'
						AND ' . $strWhereCondition . ' AND e.date_terminated IS NULL
					ORDER BY
						e.employee_status_type_id ASC, e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByEmailAddresses( $arrstrEmployeeEmailAddress, $objDatabase ) {

		if( false == valArr( $arrstrEmployeeEmailAddress ) ) {
			return false;
		}

		$strSql = 'SELECT
						e.*,
						u.id as user_id
					 FROM
						employees as e
						JOIN users u ON( e.id = u.employee_id )
					 WHERE
					    e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.email_address IS NOT NULL AND e.email_address IN ( ' . "'" . implode( "', '", $arrstrEmployeeEmailAddress ) . "'" . ' )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByStakeholdersEmailAddresses( $arrstrEmployeeEmailAddress, $objDatabase ) {

		if( false == valArr( $arrstrEmployeeEmailAddress ) ) {
			return false;
		}

		$strSql = 'SELECT
						e.id,
						e.email_address
					 FROM
						employees as e
						JOIN users u ON( e.id = u.employee_id )
					 WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.email_address IS NOT NULL AND e.email_address IN ( ' . "'" . implode( "', '", $arrstrEmployeeEmailAddress ) . "'" . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeByEmailAddress( $strEmailAddress, $objDatabase ) {
		$strSql = 'SELECT
						e.id,
						e.name_full,
						e.email_address,
						u.id as user_id
					 FROM
						employees as e
						JOIN users u ON( e.id = u.employee_id )
					 WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.email_address=\'' . $strEmailAddress . '\'';

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchEmployeesReportingManagersByUserIds( $arrintUserIds, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						e.id AS employee_id,
						u.id AS user_id,
							t.manager_employee_id AS manager_employee_id,
						manager_users.id AS manager_user_id,
							e.department_id,
						e.designation_id,
						e.name_full,
						te.is_primary_team,
						t.id AS team_id
					FROM teams t
						LEFT JOIN team_employees te on ( t.id = te.team_id )
						LEFT JOIN employees e ON ( e.id = te.employee_id )
						LEFT JOIN users u ON ( u.employee_id = e.id )
						LEFT JOIN users manager_users ON ( manager_users.employee_id = t.manager_employee_id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND u.id IN ( ' . implode( ',', $arrintUserIds ) . ' )
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainerEmployeesByActionResultIds( $arrintActionResultIds, $objDatabase ) {
		if( false == valArr( $arrintActionResultIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						u.id as user_id,
						e.*
					FROM
						users u
						JOIN employees e ON e.id = u.employee_id
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_started IS NULL
						OR e.date_terminated IS NULL
						OR NOW() < e.date_terminated )
						AND e.id NOT IN ( 1, 2 )
						AND u.id NOT IN( SELECT
												u.id AS user_id
											FROM
												users AS u
												LEFT JOIN actions AS a ON a.secondary_reference = u.id
												LEFT JOIN training_trainer_associations AS tta ON tta.action_id = a.id
											WHERE
												a.action_result_id IN ( ' . implode( ',', $arrintActionResultIds ) . ' )
												AND tta.is_published = 1 )
					ORDER BY
						e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeBuddyByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						e.*
					FROM
						employees e
						JOIN employee_application_interviews eai ON ( e.id = eai.employee_id )
						JOIN employee_applications ea ON ( ea.id = eai.employee_application_id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND eai.interview_type_id <> ' . CInterviewType::GD . '
						AND ea.employee_id =' . ( int ) $intEmployeeId . '
					ORDER BY
						eai.id
					LIMIT
						1';

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesWithUserId( $objDatabase, $boolFetchData = false ) {
		$strSql = 'SELECT
						e.id,
						e.preferred_name AS name_full,
						u.id as user_id
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
					WHERE
						employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND name_full IS NOT NULL
						AND date_terminated IS NULL
					ORDER BY
						e.preferred_name ';

		if( true == $boolFetchData ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchOriginatingEmployeeNameFullByResourceRequisitionId( $intId, $objDatabase ) {
		$strSql = sprintf( 'SELECT
								e.name_full
							FROM
								employees e,
								resource_requisitions rr
							WHERE
								e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
								AND rr.id = %d
								AND	rr.originating_employee_id = e.id', ( int ) $intId );

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchEmployeesReportingManagerByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.email_address
					FROM
						employees e
					WHERE
						e.date_terminated IS NULL
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
					UNION
					SELECT
						e.email_address
					FROM
						employees e1
						JOIN employees e ON ( e1.reporting_manager_id = e.id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.date_terminated IS NULL
						AND e1.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchPaginatedAbsentEmployeesByDate( $intPageNo, $intPageSize, $strDate, $strCountryCode, $objDatabase, $intDepartmentId = NULL, $strOrderByField = NULL, $strOrderByType = NULL, $boolIsDownload = false, $strFilteredExplodedSearch = NULL ) {
		date_default_timezone_set( 'Asia/Calcutta' );

		if( false == valStr( $strDate ) || strtotime( $strDate ) > strtotime( date( 'Y-m-d' ) ) ) {
			return NULL;
		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$intDayOfWeekSaturday = 6;
		$intDayOfWeekSunday   = 0;

		$strSql = '	SELECT
						e.id as employee_id,
						e.employee_number,
						e.preferred_name AS name_full,
						e.email_address,
						epn.phone_number,
						de.name As designation,
						d.name,
						ear.note,
						ear.expected_in_time,
						est.name As employee_status,
						eevr.request As reason
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						LEFT JOIN employee_phone_numbers AS epn ON ( e.id = epn.employee_id )
						LEFT JOIN phone_number_types AS pnt ON ( epn.phone_number_type_id = pnt.id )
						LEFT JOIN departments as d ON ( e.department_id = d.id )
						LEFT JOIN designations as de ON ( e.designation_id = de.id )
						LEFT JOIN employee_status_types est ON ( e.employee_status_type_id = est.id )
						LEFT JOIN employee_attendance_remarks AS ear ON ( e.id = ear.employee_id AND ear.requested_datetime = \' ' . $strDate . ' \' )
						LEFT JOIN employee_vacation_requests AS eevr ON ( e.id = eevr.employee_id )
					WHERE
						e.id NOT IN (
										SELECT
											DISTINCT ( employee_id )
										FROM
											employee_hours
										WHERE
											date = \' ' . $strDate . ' \'
						)
						AND e.id NOT IN (
										SELECT
											DISTINCT employee_id
											FROM
											(
												SELECT
													DISTINCT evr.*,
													generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) serial_number
												FROM
													employee_vacation_requests AS evr
													LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id )
												WHERE
													date_trunc( \'day\', timestamp\'' . $strDate . '\') BETWEEN date_trunc ( \'day\', evr.begin_datetime ) ::date AND date_trunc ( \'day\', evr.end_datetime ) ::date
													AND evr.denied_by IS NULL
													AND evr.deleted_by IS NULL
													AND country_code = \'' . addslashes( $strCountryCode ) . '\'
													AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
											) AS sub
										WHERE
												EXTRACT ( \'dow\' FROM begin_datetime::date + serial_number ) NOT IN ( \'' . ( int ) $intDayOfWeekSaturday . '\', \'' . ( int ) $intDayOfWeekSunday . '\' ) AND begin_datetime::date + serial_number BETWEEN \' ' . $strDate . ' \' AND \' ' . $strDate . ' \'
						)
						AND pnt.id= \'' . CPhoneNumberType::PRIMARY . '\'
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_terminated IS NULL
						OR e.date_terminated > NOW ( ) )
						AND DATE( \'' . $strDate . '\' ) >= e.date_started
						AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\'
						AND EXTRACT ( \'dow\' FROM DATE( \'' . $strDate . '\' ) ) NOT IN ( \'' . ( int ) $intDayOfWeekSaturday . '\', \'' . ( int ) $intDayOfWeekSunday . '\' )
						AND \'' . $strDate . '\' NOT IN (
															 SELECT
																DATE
															 FROM
																paid_holidays
															 WHERE
																country_code = \'' . addslashes( $strCountryCode ) . '\'
																AND is_optional IS FALSE
																AND DATE_PART ( \'month\', DATE( \' ' . $strDate . ' \' ) ) = DATE_PART ( \'month\', DATE )
																AND DATE_PART ( \'year\', DATE( \' ' . $strDate . ' \' ) ) = DATE_PART ( \'year\', DATE )
															)';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= 'AND d.id= ' . ( int ) $intDepartmentId . ' ';
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSql .= ' AND ( name_full ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR e.email_address ILIKE \'%' . $strFilteredExplodedSearch . '%\' )';
		}

		$strSql .= 'ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType;

		if( false == $boolIsDownload ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . '
						LIMIT
							' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllAbsentEmployeesCountByDateByCountryCode( $strDate, $strCountryCode, $objDatabase, $intDepartmentId = NULL, $strFilteredExplodedSearch = NULL ) {
		if( false == valStr( $strDate ) ) {
			return NULL;
		}

		$intDayOfWeekSaturday = 6;
		$intDayOfWeekSunday   = 0;

		$strSql = '	SELECT
						count( e.id )
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						LEFT JOIN departments as d ON ( e.department_id = d.id )
					WHERE
						e.id NOT IN (
									SELECT
										DISTINCT ( employee_id )
									FROM
										employee_hours
									WHERE
										date = \' ' . $strDate . ' \'
						)
						AND e.id NOT IN (
										SELECT
											DISTINCT employee_id
										FROM
											(
												SELECT
													DISTINCT evr.*,
													generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) serial_number
												FROM
													employee_vacation_requests AS evr
													LEFT JOIN employee_addresses AS ea ON ( evr.employee_id = ea.employee_id )
												WHERE
													date_trunc ( \'day\', timestamp\'' . $strDate . '\') BETWEEN date_trunc ( \'day\', evr.begin_datetime ) ::date AND date_trunc ( \'day\', evr.end_datetime ) ::date
													AND evr.denied_by IS NULL
													AND evr.deleted_by IS NULL
													AND country_code = \'' . addslashes( $strCountryCode ) . '\'
													AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
											) AS sub
										WHERE
												EXTRACT ( \'dow\' FROM begin_datetime::date + serial_number ) NOT IN ( \'' . ( int ) $intDayOfWeekSaturday . '\', \'' . ( int ) $intDayOfWeekSunday . '\' ) AND begin_datetime::date + serial_number BETWEEN \' ' . $strDate . ' \' AND \' ' . $strDate . ' \'
						)
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ' )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW ( ) )
						AND DATE( \'' . $strDate . '\' ) >= e.date_started
						AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\'
						AND EXTRACT ( \'dow\' FROM DATE( \'' . $strDate . '\' ) ) NOT IN ( \'' . ( int ) $intDayOfWeekSaturday . '\', \'' . ( int ) $intDayOfWeekSunday . '\' )
						AND \'' . $strDate . '\' NOT IN (
															SELECT
																DATE
															FROM
																paid_holidays
															WHERE
																country_code = \'' . addslashes( $strCountryCode ) . '\'
																AND is_optional IS FALSE
																AND DATE_PART ( \'month\', DATE( \' ' . $strDate . ' \' ) ) = DATE_PART ( \'month\', DATE )
																AND DATE_PART ( \'year\', DATE( \' ' . $strDate . ' \' ) ) = DATE_PART ( \'year\', DATE )
															)';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= 'AND d.id= ' . ( int ) $intDepartmentId . ' ';
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSql .= ' AND ( name_full ILIKE \'%' . $strFilteredExplodedSearch . '%\' OR e.email_address ILIKE \'%' . $strFilteredExplodedSearch . '%\' )';
		}

		$arrintEmployeesCount = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintEmployeesCount[0]['count'] ) ) {
			return $arrintEmployeesCount[0]['count'];
		}
	}

	public static function fetchAbsentEmployeesCountByMonthByYearByCountryCode( $strBeginDatetime, $strEndDatetime, $strCountryCode, $objDatabase ) {
		if( false == valStr( $strBeginDatetime ) && false == valStr( $strEndDatetime ) ) {
			return NULL;
		}

		$intDayOfWeekSaturday = 6;
		$intDayOfWeekSunday   = 0;
		$strJoinCondition     = '';
		$strWhereCondition    = '';

		if( CCountry::CODE_USA == $strCountryCode ) {
			$strJoinCondition = ' LEFT JOIN departments d ON ( d.id = e.department_id ) ';

			$strWhereCondition = ' AND d.id = ' . CDepartment::CALL_CENTER;
		}

		$strSql = ' SELECT
						EXTRACT( \'day\' from DATE ) as date,
						COUNT ( DISTINCT employees.employee_id ) AS absent_count
					FROM
						(
							(
								SELECT
									DISTINCT \'' . $strBeginDatetime . '\'::date + serial_number AS DATE,
									employee_id
								FROM
									(
										SELECT
											generate_series ( 0, \'' . $strEndDatetime . '\'::date - \'' . $strBeginDatetime . '\'::date::date ) AS serial_number,
											e.id AS employee_id,
											e.date_started
										FROM
											employees e
											LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id ) ' . $strJoinCondition . '
										WHERE
											employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
											AND ( e.date_terminated IS NULL OR e.date_terminated > NOW ( ) )
											AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\'
											AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'' . $strWhereCondition . '
									) AS sub_employees
								WHERE
									\'' . $strBeginDatetime . '\'::date + serial_number NOT IN (
																								SELECT
																									DATE
																								FROM
																									paid_holidays
																								WHERE
																									country_code = \'' . addslashes( $strCountryCode ) . '\'
																									AND is_optional IS FALSE
																									AND DATE_PART ( \'month\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'month\', DATE )
																									AND DATE_PART ( \'year\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'year\', DATE )
																								)
									AND extract ( \'dow\' FROM \'' . $strBeginDatetime . '\'::date + serial_number ) NOT IN ( \'' . ( int ) $intDayOfWeekSaturday . '\', \'' . ( int ) $intDayOfWeekSunday . '\' )
									AND \'' . $strBeginDatetime . '\'::date + serial_number>= sub_employees.date_started
							)
							EXCEPT
							(
								SELECT
									DISTINCT DATE,
									employee_id
								FROM
									employee_hours
								WHERE
									DATE_PART ( \'month\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'month\', DATE )
									AND DATE_PART ( \'year\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'year\', DATE )
									AND ( employee_hour_type_id = 1 OR employee_hour_type_id IS NULL )
							)
							EXCEPT
							(
								SELECT
									begin_datetime::date + serial_number AS DATE,
									employee_id
								FROM
									(
										SELECT
											DISTINCT employee_id,
											evr.begin_datetime,
											generate_series ( 0, evr.end_datetime::date - evr.begin_datetime::date ) serial_number
										FROM
											employee_vacation_requests evr
										WHERE
											deleted_by IS NULL
											AND denied_by IS NULL
									) AS sub
								WHERE
									DATE_PART ( \'month\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'month\', begin_datetime::date + serial_number )
									AND DATE_PART ( \'year\', DATE( \'' . $strBeginDatetime . '\' ) ) = DATE_PART ( \'year\', begin_datetime::date + serial_number )
							)
						) AS employees
					GROUP BY
						DATE
					ORDER BY
						DATE';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesCountByMonthByYearByCountryCode( $strBeginDateTime, $strEndDatetime, $strCountryCode, $objDatabase, $arrmixFilteredExplodedSearch = NULL ) {
		if( false == valStr( $strBeginDateTime ) || false == valStr( $strEndDatetime ) ) {
			return NULL;
		}

		if( true == valArr( $arrmixFilteredExplodedSearch ) ) {

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) {
				$strSeacrhCondition = 'AND to_char( e.employee_number, \'99999\' ) LIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' ';
			} elseif( 1 < \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) ) {
				$strSeacrhCondition = 'AND ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' AND e.name_last ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' AND e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' ) OR ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' AND e.name_last ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' AND e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' OR e.email_address ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' ) OR d.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' ) AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' )';

			} else {
				$strSeacrhCondition = 'AND ( e.name_first ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.name_last ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.preferred_name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.email_address ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR d.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR de.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\') AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' ) ';
			}
		} else {
			$strSeacrhCondition = '';
		}

		$strSql = 'SELECT
						COUNT( e.id )
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN departments AS d ON (e.department_id = d.id)
						LEFT JOIN designations AS de ON ( e.designation_id = de.id )
					WHERE
						( e.date_terminated IS NULL OR ( DATE_PART ( \'month\', DATE ( \'' . $strEndDatetime . '\' ) ) <= DATE_PART ( \'month\', DATE ( e.date_terminated ) ) AND DATE_PART ( \'year\', DATE ( \'' . $strEndDatetime . '\' ) ) <= DATE_PART ( \'year\', DATE ( e.date_terminated ) ) ) OR ( e.date_terminated BETWEEN DATE ( \'' . $strBeginDateTime . '\' ) AND DATE ( \'' . $strEndDatetime . '\' ) ) )
						AND DATE( \'' . $strEndDatetime . '\' ) >= e.date_started
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\'' . $strSeacrhCondition . '';

		$arrintEmployeesCount = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintEmployeesCount[0]['count'] ) ) {
			return $arrintEmployeesCount[0]['count'];
		}
	}

	public static function fetchAllEmployeesWithUserId( $objDatabase ) {
		$strSql = 'SELECT
						e.preferred_name AS name_full,
						u.id as user_id
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
					WHERE  e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCustomAllEmployeesWithUserId( $objDatabase ) {
		$strSql = 'SELECT
						e.preferred_name AS name_full,
							e.id as id
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
					WHERE 
					e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					AND e.department_id IN( ' . CDepartment::DEVELOPMENT . ',' . CDepartment::QA . ' )
					ORDER BY
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEmailAddressBySvnUserName( $strSvnUserName, $objDatabase ) {
		$strSql         = 'SELECT e.email_address FROM employees e JOIN users u ON ( e.id = u.employee_id) WHERE  e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND u.svn_username = \'' . $strSvnUserName . '\'';
		$arrintEmailIds = fetchData( $strSql, $objDatabase );

		return ( String ) $arrintEmailIds[0]['email_address'];
	}

	public static function fetchEmployeeBySvnUserName( $strSvnUserName, $objDatabase ) {
		$strSql         = 'SELECT e.name_full FROM employees e JOIN users u ON ( e.id = u.employee_id) WHERE  e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND u.svn_username = \'' . $strSvnUserName . '\'';
		$arrintFullNameIds = fetchData( $strSql, $objDatabase );

		return ( String ) $arrintFullNameIds[0]['name_full'];
	}

	public static function fetchPaginatedEmployeesByMonthByYearByCountryCode( $intPageNo, $intPageSize, $strStartDate, $strEndDate, $strCountryCode, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL, $boolIsDownload, $arrmixFilteredExplodedSearch ) {
		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}

		if( true == valArr( $arrmixFilteredExplodedSearch ) ) {
			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) {
				$strSeacrhCondition = 'AND to_char( e.employee_number, \'99999\' ) LIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' ';
			} elseif( 1 < \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) ) {
				$strSeacrhCondition = 'AND ( ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' AND e.name_last ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' ) OR e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\')	OR ( ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' AND e.name_last ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\') OR e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\') OR e.email_address ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' ) OR d.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\'								 		 OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' )AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' )';
			} else {
				$strSeacrhCondition = 'AND ( e.name_first ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.name_last ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.preferred_name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.email_address ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR d.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR de.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\') AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' ) ';
			}
		} else {
			$strSeacrhCondition = '';
		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT
					e.id as employee_id,
					e.employee_number,
					e.preferred_name AS name_full,
					e.date_started,
					e.date_confirmed,
					e.date_terminated,
					d.name as department_name,
					de.name as designation
				FROM
					employees e
					LEFT JOIN employee_addresses AS ea ON (e.id = ea.employee_id)
					LEFT JOIN departments AS d ON (e.department_id = d.id)
					LEFT JOIN designations AS de ON ( e.designation_id = de.id )
				WHERE
					( e.date_terminated IS NULL OR ( DATE_PART ( \'month\', DATE ( \'' . $strEndDate . '\' ) ) <= DATE_PART ( \'month\', DATE ( e.date_terminated ) ) AND DATE_PART ( \'year\', DATE ( \'' . $strEndDate . '\' ) ) <= DATE_PART ( \'year\', DATE ( e.date_terminated ) ) ) OR ( e.date_terminated BETWEEN DATE ( \'' . $strStartDate . '\' ) AND DATE ( \'' . $strEndDate . '\' ) ) )
					AND DATE( \'' . $strEndDate . '\' ) >= e.date_started
					AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
					AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' )
					 ' . $strSeacrhCondition . '
				ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType . '';

		if( false == $boolIsDownload ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . 'LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleDistinctPhoneExtensionsByPhoneExtensionTypeId( $intPhoneExtensionTypeId, $objDatabase, $intEmployeeId = NULL ) {
		if( false == is_numeric( $intPhoneExtensionTypeId ) ) {
			return NULL;
		}

		$strSql                         = '';
		$arrintRekeyedPhoneExtensionIds = [];

		if( CPhoneExtensionType::PHYSICAL_PHONE_EXTENSION_TYPE == $intPhoneExtensionTypeId ) {
			$strSql .= 'SELECT DISTINCT physical_phone_extension_id AS phone_extension_id FROM employees WHERE physical_phone_extension_id IS NOT NULL';
			if( true == is_numeric( $intEmployeeId ) ) {
				$strSql .= ' AND id <> ' . ( int ) $intEmployeeId;
			}
		} elseif( CPhoneExtensionType::AGENT_PHONE_EXTENSION_TYPE == $intPhoneExtensionTypeId ) {
			$strSql .= 'SELECT DISTINCT agent_phone_extension_id AS phone_extension_id FROM employees WHERE agent_phone_extension_id IS NOT NULL';
			if( true == is_numeric( $intEmployeeId ) ) {
				$strSql .= ' AND id <> ' . ( int ) $intEmployeeId;
			}
		}

		$arrintPhoneExtensionIds = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintPhoneExtensionIds ) ) {
			foreach( $arrintPhoneExtensionIds as $arrintPhoneExtension ) {
				$arrintRekeyedPhoneExtensionIds[$arrintPhoneExtension['phone_extension_id']] = $arrintPhoneExtension['phone_extension_id'];
			}
		}

		return $arrintRekeyedPhoneExtensionIds;
	}

	public static function fetchPDMEmployeeWithProducts( $objDatabase ) {
		$strSql = 'SELECT
						pp.id AS product_id,
						e.id AS id,
						u.id AS user_id,
						e.name_full AS name_full,
						pp.name AS product_name
					FROM
						employees e
						JOIN users u ON u.employee_id = e.id
						LEFT JOIN ps_products pp ON pp.sdm_employee_id = e.id
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.designation_id = ' . CDesignation::PRODUCT_DEVELOPMENT_MANAGER;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSearchEmployeesByCountryCode( $arrmixFilteredExplodedSearch, $boolIsShowDisabledData, $strCountryCode, $objDatabase, $arrmixFilter = NULL ) {
		$strEmployeeStatusTypeCondition = ( false == $boolIsShowDisabledData ) ? 'e.employee_status_type_id NOT IN (' . CEmployeeStatusType::PREVIOUS . ' , ' . CEmployeeStatusType::NO_SHOW . ')' : ' e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ',' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ')';
		$strWhereCondition              = '';

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) {
			$strSeacrhCondition = 'to_char( e.employee_number, \'99999\' ) LIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' ';
		} elseif( 1 < \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) ) {
			$strSeacrhCondition = ' ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' AND e.name_last ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' AND e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' ) OR ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' AND e.name_last ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' AND e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR e.email_address ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' ) ';
		} else {
			$strSeacrhCondition = ' e.name_first ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.name_last ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.preferred_name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.email_address ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' ';
		}

		if( true == is_numeric( $arrmixFilter['team_id'] ) || true == is_numeric( $arrmixFilter['add_employee_id'] ) || true == is_numeric( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strJoinCondition = '	LEFT JOIN team_employees as te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
									LEFT JOIN teams as t ON ( t.id = te.team_id )';
		}

		if( true == is_numeric( $arrmixFilter['team_id'] ) ) {
			$strWhereCondition .= ' AND t.id = ' . ( int ) $arrmixFilter['team_id'];
		}

		if( false == is_null( $arrmixFilter['add_employee_id'] ) || false == is_null( $arrmixFilter['hr_representative_employee_id'] ) ) {
			$strWhereCondition .= ' AND t.deleted_by IS NULL' . ( ( false == is_null( $arrmixFilter['add_employee_id'] ) ) ? ' AND t.add_employee_id = ' . ( int ) $arrmixFilter['add_employee_id'] : ' AND t.hr_representative_employee_id = ' . ( int ) $arrmixFilter['hr_representative_employee_id'] );
		}

		$strSql = 'SELECT
						e.id,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.email_address,
						e.employee_number
					FROM
						employees AS e
						JOIN employee_addresses AS ea ON ( e.id = ea.employee_id AND ea.country_code = \'' . $strCountryCode . '\' AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						' . $strJoinCondition . '
					WHERE
						( ' . $strSeacrhCondition . ' )
						AND ' . $strEmployeeStatusTypeCondition . $strWhereCondition . '
					ORDER BY
						e.name_first
					LIMIT
						10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedEmployeesCountByDepartmentId( $intDepartmentId, $objDatabase ) {
		if( false == valId( $intDepartmentId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						count ( DISTINCT e.name_full )
					FROM
						tasks t
						JOIN users u ON ( u.id = t.user_id )
						JOIN employees e ON ( u.employee_id = e.id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.department_id = ' . ( int ) $intDepartmentId;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchSalesEmployeesDepartmentIdTeamIdByEmployeeId( $intEmployeeId, $objAdminDatabase ) {
		$strSql = 'SELECT
						 e.reporting_manager_id AS manager_employee_id,
						 e.department_id
					FROM employees e
					WHERE 
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id = ' . ( int ) $intEmployeeId . ' LIMIT 1 ';

		return self::fetchEmployee( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeesByGroupId( $intGroupId, $objDatabase, $strCountryCode = NULL, $strOrderBy = 'name_full', $boolAllEmployees = false ) {
		$strWhereCondition = '';
		if( true == is_null( $intGroupId ) ) {
			return NULL;
		}

		$strWhereCondition .= ( false == is_null( $strCountryCode ) ) ? ' AND ea.country_code = \'' . $strCountryCode . '\'' : '';
		if( true == $boolAllEmployees ) {
			$strWhereCondition .= ' AND employee_status_type_id IN ( ' . ( int ) CEmployeeStatusType::CURRENT . ',' . ( int ) CEmployeeStatusType::PREVIOUS . ' )';
		} else {
			$strWhereCondition .= ' AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT;
		}
		$strSql = 'SELECT
						DISTINCT ( e.* ),
						u.id as user_id
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						JOIN users u ON ( e.id = u.employee_id )
						JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
					WHERE
						ug.group_id = ' . ( int ) $intGroupId . $strWhereCondition . '
					ORDER BY
						e.' . $strOrderBy;

		return CEmployees::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchEmployeesByGroupIdOrBySuperAdminEmployeeId( $intGroupId, $objDatabase, $arrintSuperAdminEmployeeIds = [], $strCountryCode = NULL, $boolAllEmployees = false ) {
		if( true == is_null( $intGroupId ) ) {
			return NULL;
		}
		if( CCountry::CODE_INDIA == $strCountryCode ) {
			$arrintSuperAdminEmployeeIds[] = CEmployee::ID_PREETAM_YADAV;
		}

		$strWhereCondition = ( false == is_null( $strCountryCode ) ) ? ' AND ea.country_code = \'' . $strCountryCode . '\'' : '';
		$strWhereConditionEmployeeStatusType = ' AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT;
		if( true == $boolAllEmployees ) {
			$strWhereConditionEmployeeStatusType = ' AND employee_status_type_id IN ( ' . ( int ) CEmployeeStatusType::CURRENT . ',' . ( int ) CEmployeeStatusType::PREVIOUS . ' )';
		}

		$strUnionAllClause = '';
		if( true == valArr( $arrintSuperAdminEmployeeIds ) ) {
			$strUnionAllClause = 'UNION ALL
									(SELECT
										DISTINCT ( e.* ),
										u.id as user_id
									FROM
										employees e
										JOIN users u ON ( e.id = u.employee_id )
									WHERE
										e.id IN ( ' . implode( ',', $arrintSuperAdminEmployeeIds ) . ' )
										' . $strWhereConditionEmployeeStatusType . '
									)';
		}

		$strSql = '(SELECT
						DISTINCT ( e.* ),
						u.id as user_id
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						JOIN users u ON ( e.id = u.employee_id )
						JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
					WHERE
						ug.group_id = ' . ( int ) $intGroupId . $strWhereCondition . $strWhereConditionEmployeeStatusType . '
					)' . $strUnionAllClause;

		return CEmployees::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchEmployeeByGmailUsername( $strGmailUserName, $objDatabase ) {
		$strSql = ' SELECT
						e.*,
						u.id AS user_id
					FROM
						employees e
						JOIN users u ON ( u.employee_id = e.id )
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.email_address LIKE \'' . trim( addslashes( $strGmailUserName ) ) . '@%\'' . '
					ORDER BY
						e.employee_number
					LIMIT
						1';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchAllEmployeesByExcludingEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		$strWhereCondition = ( true == valArr( $arrintEmployeeIds ) ) ? 'AND id NOT IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )' : '';

		$strSql = ' SELECT
						id,
						CASE
							WHEN preferred_name IS NULL
							THEN name_full
							ELSE preferred_name
						END AS name_full
					FROM
						employees
					WHERE
							employee_status_type_id =' . CEmployeeStatusType::CURRENT . '
							AND date_terminated IS NULL ' . $strWhereCondition . '
					ORDER BY
						CASE
							WHEN preferred_name IS NULL
							THEN name_full
							ELSE preferred_name
						END ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSupportEmployeesByCids( $arrintCids, $objDatabase ) {
		$strSql = 'SELECT
						e.name_full,
						e.email_address,
						pld.cid,
						epn.phone_number,
						pld.ps_lead_id
					FROM
						employees e,
						ps_lead_details pld,
						employee_phone_numbers epn
					WHERE
						e.id = pld.support_employee_id
						AND e.id = epn.employee_id
						AND epn.phone_number_type_id = 1
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND pld.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW ( ) )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchTeamEmployeesByManagerEmployeeId( $arrstrFilteredExplodedSearch, $intManagerEmployeeId, $objDatabase, $arrintRecursiveDevQaEmployeeIds = NULL ) {

		if( true == valArr( $arrintRecursiveDevQaEmployeeIds ) ) {
			$strWhere = ' ( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN ( ' . implode( ',', $arrintRecursiveDevQaEmployeeIds ) . ' ) ) AND ';
		} else {
			$strWhere = ' e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' AND ';
		}

		$strSql = ' WITH RECURSIVE all_employees( employee_id, team_id, manager_employee_id, depth ) AS (
									SELECT
										e.id AS employee_id,
										te.team_id,
										e.reporting_manager_id AS manager_employee_id,
										1
									FROM
										team_employees te
										JOIN employees e ON ( te.employee_id = e.id )
									WHERE
										' . $strWhere . ' te.is_primary_team = 1
										AND e.id <> ' . ( int ) $intManagerEmployeeId . '
									UNION ALL
									SELECT
										e.id AS employee_id,
										te.team_id,
										e.reporting_manager_id AS manager_employee_id,
										al.depth + 1
									FROM
										all_employees al
										JOIN employees e ON ( e.reporting_manager_id = al.employee_id )
										JOIN team_employees te ON ( te.employee_id = e.id )
									WHERE
										te.is_primary_team = 1
										AND e.id <>' . ( int ) $intManagerEmployeeId . '
								)SELECT
						e.id,
						e.name_first,
						e.name_last,
						e.preferred_name,
						t.name AS team_name,
						e.email_address,
						e.reporting_manager_id AS manager_employee_id
					FROM
						 all_employees al
						JOIN employees e ON ( al.employee_id = e.id )
						LEFT JOIN teams t ON ( al.team_id = t.id )
						LEFT JOIN team_employees te ON ( al.employee_id = te.employee_id AND te.is_primary_team = 1 )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = 1
						AND e.id <> ' . ( int ) $intManagerEmployeeId . '
						AND ( e.name_first ILIKE \'%' . implode( '%\' AND e.name_first ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_last ILIKE \'%' . implode( '%\' AND e.name_last ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR t.name ILIKE \'%' . implode( '%\' AND t.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
					ORDER BY
						e.name_first';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCurrentEmployeesDetails( $objDatabase, $strCountryCode = NULL ) {
		$strWhere = '';
		if( false == is_null( $strCountryCode ) ) {
			$strWhere .= ' ea.country_code = \'' . addslashes( $strCountryCode ) . '\' AND';
		}

		$strCountrySortOrder = ( 'US' == $strCountryCode ) ? 'DESC' : 'ASC';

		$strSql = 'SELECT
						DISTINCT( em.email_address ),
						em.id AS employee_id,
						em.name_full,
						em.preferred_name,
						u.id,
						ea.country_code,
						em.employee_status_type_id,
						em.designation_id
					FROM
						users u
						LEFT JOIN employees em ON( u.employee_id = em.id )
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						' . $strWhere . '
						em.id NOT IN ( 1, 2 )
						AND u.id != 100
						AND em.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ')
						AND em.date_terminated IS NULL
					ORDER BY em.employee_status_type_id ASC, ea.country_code ' . $strCountrySortOrder . ', em.name_full';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPaginatedEmployeesForPendingReview( $intPageNo, $intPageSize, $strOrderByField, $strOrderByType, $objAdminDatabase, $intDepartmentId = NULL, $intHrRepresentativeEmployeeId = NULL ) {
		$intOffset                 = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit                  = ( int ) $intPageSize;
		$strInnerSqlWhereCondition = '';

		$intPendingStatus   = 1;
		$intInProcessStatus = 2;

		if( true == is_numeric( $intHrRepresentativeEmployeeId ) ) {
			$strInnerSqlWhereCondition .= ' AND t.hr_representative_employee_id = \'' . $intHrRepresentativeEmployeeId . '\'';
		}

		if( true == is_numeric( $intDepartmentId ) ) {
			$strInnerSqlWhereCondition .= ' AND d.id = \'' . $intDepartmentId . '\'';
		}

		$strSql = 'SELECT
						reviews.id,
						reviews.name_full,
						reviews.date_started,
						reviews.confirmation_date,
						reviews.total_experience,
						reviews.xento_experience,
						reviews.reporting_manager,
						reviews.hr_representative_name,
						reviews.department_name,
						CASE
							WHEN reviews.id IN (
												SELECT
													employee_id
												FROM
													employee_assessments
												WHERE
													completed_on IS NULL
							) THEN ' . ( int ) $intInProcessStatus . '
							ELSE ' . ( int ) $intPendingStatus . '
						END AS status
					FROM
						(
						SELECT
							e.id,
							e.date_started,
							e.name_full,
							e1.name_full AS reporting_manager,
							e2.name_full AS hr_representative_name,
							d.name AS department_name,
							( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) * 12 ) + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) + ( ead.experience_year * 12 ) + ead.experience_month AS total_experience,
							( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) * 12 ) + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) AS xento_experience,
							CASE
								WHEN ( ( ead.experience_year * 12 ) + ead.experience_month ) >= 12 THEN ( e.date_started + INTERVAL \'120 days\' )
								ELSE ( e.date_started + INTERVAL \'180 days\' )
							END AS confirmation_date
						FROM
							employees e
							LEFT JOIN employee_applications ea ON ( ea.employee_id = e.id )
							LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = ea.id )
							LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
							LEFT JOIN teams t ON ( te.team_id = t.id )
							LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
							LEFT JOIN employees e2 ON ( e2.id = t.hr_representative_employee_id )
							LEFT JOIN departments d ON ( d.id = e.department_id )
						WHERE
							e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
							AND ( e.office_id = \'' . COffice::TOWER_8 . '\'
							OR e.office_id = \'' . COffice::TOWER_9 . '\' )
							AND e.date_confirmed IS NULL ' . $strInnerSqlWhereCondition . '
						) AS reviews
					WHERE
						reviews.confirmation_date < CURRENT_DATE
						AND EXTRACT ( month FROM reviews.confirmation_date ) <> EXTRACT ( month FROM CURRENT_DATE )';

		$strSql .= ' ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . ' ' . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchPaginatedEmployeesCountForPendingReview( $objAdminDatabase, $intDepartmentId = NULL, $intHrRepresentativeEmployeeId = NULL ) {
		$strInnerSqlWhereCondition	= '';
		$strInnerSqlJoinCondition	= '';

		if( false == is_null( $intHrRepresentativeEmployeeId ) && true == is_numeric( $intHrRepresentativeEmployeeId ) ) {
			$strInnerSqlWhereCondition	.= ' AND t.hr_representative_employee_id = \'' . $intHrRepresentativeEmployeeId . '\'';
			$strInnerSqlJoinCondition	.= 'LEFT JOIN teams t ON ( te.team_id = t.id )';
		}

		if( false == is_null( $intDepartmentId ) && true == is_numeric( $intDepartmentId ) ) {
			$strInnerSqlWhereCondition .= ' AND d.id = \'' . $intDepartmentId . '\'';
		}

		$strSql = '	SELECT
						count ( reviews.id )
					FROM
						(
						SELECT
							e.id,
							CASE
								WHEN ( ( ead.experience_year * 12 ) + ead.experience_month ) >= 12 THEN ( e.date_started + INTERVAL \'120 days\' )
								ELSE ( e.date_started + INTERVAL \'180 days\' )
							END AS confirmation_date
						FROM
							employees e
							LEFT JOIN employee_applications ea ON ( ea.employee_id = e.id )
							LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = ea.id )
							LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )'
				  . $strInnerSqlJoinCondition . '
							LEFT JOIN departments d ON ( d.id = e.department_id )
						WHERE
							e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
							AND ( e.office_id = \'' . COffice::TOWER_8 . '\'
							OR e.office_id = \'' . COffice::TOWER_9 . '\' )
							AND e.date_confirmed IS NULL ' . $strInnerSqlWhereCondition . '
						) AS reviews
					WHERE
						reviews.confirmation_date < CURRENT_DATE
						AND EXTRACT ( month FROM reviews.confirmation_date ) <> EXTRACT ( month FROM CURRENT_DATE )';

		$arrintResponse = fetchData( $strSql, $objAdminDatabase );

		return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;

	}

	public static function fetchAllActiveEmployeesData( $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT e.id,
						e.preferred_name AS name,
						des.name AS designation,
						dep.name AS department,
						epn.phone_number AS phone,
						e.email_address AS email,
						COUNT( DISTINCT a.id ) AS likes,
						ea.country_code
					FROM
						employees e
						LEFT JOIN designations des ON ( des.id = e.designation_id )
						LEFT JOIN departments dep ON ( dep.id = e.department_id )
						LEFT JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id AND epn.phone_number_type_id =' . CPhoneNumberType::PRIMARY . ' )
						LEFT JOIN employee_certifications ec ON ( e.id = ec.employee_id)
						LEFT JOIN certifications c ON ( ec.certification_id = c.id )
						LEFT JOIN actions a ON ( e.id = a.employee_id AND a.action_result_id =' . CActionResult::LIKE . ' )
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id )
					WHERE e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ') 
					GROUP BY
						e.id,
						e.preferred_name,
						des.name,
						dep.name,
						epn.phone_number,
						e.email_address,
						ea.country_code
					ORDER BY
						e.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDetailByEmployeeId( $intEmployeeId, $objDatabase, $boolIsBonusPotentialEmployeesOnly = NULL ) {

		$strWhereClause = ( true == $boolIsBonusPotentialEmployeesOnly ) ? ' AND e.bonus_potential_encrypted IS NOT NULL' : '';

		$strSql = 'SELECT
						e.*,
						ea.country_code
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE 
						e.id = ' . ( int ) $intEmployeeId . $strWhereClause;

		return CEmployees::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchReportingManagerByEmployeeId( $intEmployeeId, $objDatabase, $boolIsIndianReportingManager = false, $boolIsPreviousEmployee = false ) {

		$strSql = 'SELECT
						e1.id,
						u.id as user_id,
						e1.name_first,
						e1.name_full,
						e1.email_address,
						e1.department_id,
						e1.designation_id,
						o.country_code,
						e1.preferred_name
					FROM
						employees e
						LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
						JOIN users u ON ( e1.id = u.employee_id )
						LEFT JOIN offices o ON o.id = e1.office_id
					WHERE
						e.id = ' . ( int ) $intEmployeeId;
		if( false == $boolIsPreviousEmployee ) {
			$strSql .= 'AND e.date_terminated IS NULL
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::CURRENT . ' )';
		}

		if( true == $boolIsIndianReportingManager ) {
			$strSql .= ' AND o.country_code = \'' . CCountry::CODE_INDIA . '\'';
		}
		$strSql .= ' LIMIT 1 ';

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByDepartmentIds( $arrintDepartmentIds, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT id
					FROM
						employees
					WHERE
						employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesAlternateSalesClosers( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						employees e
					WHERE ( e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintSalesDepartmentIds ) . ' ) AND e.employee_status_type_id = ' . CEmployeeStatusType::PREVIOUS . ' )
						OR ( e.department_id IN ( ' . CDepartment::SALES_DIALER . ', ' . CDepartment::SALES_ENGINEER . ', ' . CDepartment::SALES_ADMINS . ' ) AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ', ' . CEmployeeStatusType::PREVIOUS . ' ) )
						ORDER BY
							e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByTeamEmployeeIds( $arrintTeamEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintTeamEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*, e1.id AS employee_id
					FROM
						employees e1
						JOIN employees e ON ( e1.reporting_manager_id = e.id )
					WHERE
						e1.id IN ( ' . implode( ',', $arrintTeamEmployeeIds ) . ' )
						AND e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamsAttendanceByTeamIdsByHrRepresentativeEmployeeId( $strCurrentDate, $arrintTeamsIds = NULL, $intHrRepresentativeEmployeeId, $objDatabase, $intPageNo = NULL, $intPageSize = NULL ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;
		$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		if( false == valArr( $arrintTeamsIds ) ) {
			$strTeamsSql = 'SELECT * FROM teams WHERE hr_representative_employee_id =' . ( int ) $intHrRepresentativeEmployeeId;
		} else {
			$strTeamsSql = 'SELECT * FROM teams WHERE id IN( ' . implode( ',', $arrintTeamsIds ) . ' )';

		}

		$strSql = 'SELECT
						total.name,
						total.total_count,
						total.team_id,
						count ( present.employee_id ) AS present_count,
						CASE
							WHEN leave.leave_employees IS NULL THEN 0
							ELSE leave.leave_employees
						END AS leave_employees
					FROM
						(
						SELECT
							team.name,
							count ( te.team_id ) AS total_count,
							te.team_id
						FROM
							(
							' . $strTeamsSql . '
								) AS team
							LEFT JOIN team_employees te ON ( team.id = te.team_id )
							LEFT JOIN employees e ON ( te.employee_id = e.id )
						WHERE
							( e.date_terminated >= \'' . $strCurrentDate . '\'
							OR e.date_terminated IS NULL )
							AND te.is_primary_team = 1
							AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ')
						GROUP BY
							team.name,
							te.team_id
						) AS total
						LEFT JOIN
						(
						SELECT
							es.employee_id,
							te.team_id,
							min ( es.swipe_datetime ) AS entry_time
						FROM
							public.employee_swipes es,
							team_employees te,
							teams t
						WHERE
							DATE_TRUNC ( \'day\', swipe_datetime ) = \'' . $strCurrentDate . '\'
							AND es.employee_id = te.employee_id
							AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
							AND t.id = te.team_id
							AND te.is_primary_team = 1
						GROUP BY
							te.team_id,
							es.employee_id
						) AS present ON ( total.team_id = present.team_id )
						LEFT JOIN
						(
						SELECT
							on_leave.on_leave_employees - vacation_present.vacation_present_employees AS leave_employees,
							on_leave.team_id
						FROM
							(
								SELECT
									 count ( DISTINCT( evr.employee_id ) ) AS on_leave_employees,
									te.team_id
								FROM
									teams t
									LEFT JOIN team_employees te ON t.id = te.team_id,
									employee_vacation_requests AS evr
									INNER JOIN employees AS e ON evr.employee_id = e.id
								WHERE
									evr.deleted_by IS NULL
									AND (timezone(\'GMT\', NOW()) + INTERVAL \'5 hours 30 minutes\') BETWEEN evr.begin_datetime
									AND evr.end_datetime
									AND e.id = te.employee_id
									AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
									AND te.is_primary_team = 1
									AND e.date_terminated is null
								GROUP BY
									te.team_id
								ORDER BY
									te.team_id
							) AS on_leave
							LEFT JOIN
							(
								SELECT
									t.id,
									CASE
										WHEN vp.vacation_present_employees IS NULL THEN 0
										ELSE vp.vacation_present_employees
									END AS vacation_present_employees
								FROM
									teams t
									LEFT JOIN
									(
									SELECT
										count ( DISTINCT es.employee_id ) AS vacation_present_employees,
										t.id
									FROM
										teams t
										LEFT JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
										LEFT JOIN employee_swipes es ON ( te.employee_id = es.employee_id )
										LEFT JOIN employee_vacation_requests evr ON ( es.employee_id = evr.employee_id )
									WHERE
										date_trunc ( \'day\', es.swipe_datetime ) = \'' . $strCurrentDate . '\'
										AND es.employee_id IN (
																SELECT
																	te.employee_id
																FROM
																	teams t
																	LEFT JOIN team_employees te ON ( t.id = te.team_id )
																	LEFT JOIN employees e ON ( te.employee_id = e.id )
																WHERE
																	t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
																	AND te.is_primary_team = 1
																	AND e.date_terminated IS NULL
																	AND t.id IN (
																					SELECT
																						t.id
																					FROM
																						teams t
																					WHERE
																						t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
																	)
										)
										AND \'' . $strCurrentDate . '\' BETWEEN date_trunc ( \'day\', evr.begin_datetime )
										AND date_trunc ( \'day\', evr.end_datetime )
										AND evr.deleted_by IS NULL
									GROUP BY
										t.id
									) AS vp ON ( vp.id = t.id )
								WHERE
									t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
							) AS vacation_present ON ( on_leave.team_id = vacation_present.id )
						) AS leave ON ( total.team_id = leave.team_id )
					GROUP BY
						total.name,
						total.total_count,
						total.team_id,
						leave.leave_employees
					ORDER BY
						total.name' . $strSubSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDetailsByTeamId( $strCurrentDate, $intTeamId, $intHrRepresentativeEmployeeId, $objDatabase ) {
		if( false == is_numeric( $intTeamId ) || false == is_numeric( $intHrRepresentativeEmployeeId ) ) return NULL;

		$strSql = 'SELECT
					distinct( e.id ),
					e.preferred_name,
					e.email_address AS email_address,
					epn.phone_number AS phone_number,
					d.name AS department,
					in_time.manager_name,
					in_time.employee_entry_time,
					in_time.team_name,
					e1.preferred_name AS manager_name
				FROM
					employee_phone_numbers epn,
					employee_swipes es RIGHT JOIN
					employees e on (es.employee_id = e.id AND DATE_TRUNC ( \'day\', es.swipe_datetime ) = \'' . $strCurrentDate . '\' )
					RIGHT JOIN
					( SELECT
						te.employee_id,
						e.preferred_name as manager_name,
						to_char( employee_entry_date_time.min, \'HH12:MI:SS\' ) AS employee_entry_time,
						employee_entry_date_time.team_name
					FROM
						employees e,
						team_employees te,
						(
							SELECT
								min ( es.swipe_datetime ),
								te.employee_id,
								t.name as team_name
							FROM
								teams t
								LEFT JOIN team_employees te ON ( t.id = te.team_id )
								LEFT JOIN employees e ON ( te.employee_id = e.id )
								LEFT JOIN employee_swipes es ON ( e.id = es.employee_id AND date_trunc ( \'day\', es.swipe_datetime ) = \'' . $strCurrentDate . '\' )
							WHERE
								t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
								AND te.is_primary_team = 1
								AND ( e.date_terminated >= \'' . $strCurrentDate . '\'
								OR e.date_terminated IS NULL )
								AND t.id = ' . ( int ) $intTeamId . '

							GROUP BY
								te.employee_id,
									t.name
						) AS employee_entry_date_time,
						employees e1
					WHERE
						employee_entry_date_time.employee_id = te.employee_id
						AND te.employee_id = e1.id
						AND e.id = e1.reporting_manager_id
						AND te.team_id = ' . ( int ) $intTeamId . '
						AND te.is_primary_team = 1
					GROUP BY
						te.employee_id,
						employee_entry_date_time.min,
						e.preferred_name,
						employee_entry_date_time.team_name
					)AS in_time ON ( e.id = in_time.employee_id )
					LEFT JOIN team_employees te ON( te.employee_id = e.id )
					LEFT JOIN employees e1 ON( e1.id = e.reporting_manager_id )
					LEFT JOIN departments d ON( e.department_id = d.id )
				WHERE
					e.id IN (
							SELECT
								te.employee_id
							FROM
								employees e,
								teams t
								LEFT JOIN team_employees te ON ( t.id = te.team_id )
							WHERE
								t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
								AND e.id = te.employee_id
								AND te.is_primary_team = 1
								AND ( e.date_terminated >= \'' . $strCurrentDate . '\'
								OR e.date_terminated IS NULL )
								AND t.id = ' . ( int ) $intTeamId . '
					)
					AND e.id = epn.employee_id
					AND epn.phone_number_type_id = 1
					AND te.is_primary_team = 1
					AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ')
					--AND es.employee_id = e.id

				GROUP BY
					e.id,
					e.preferred_name,
					e.email_address,
					epn.phone_number,
					in_time.manager_name,
					in_time.employee_entry_time,
					in_time.team_name,
					e1.preferred_name,
					d.name
				ORDER BY
					e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesByDesignationIds( $arrintDesignationIds, $objDatabase ) {
		if( false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						employees
					WHERE
						date_terminated IS NULL 
						AND employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' ) ORDER BY preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByDepartmentIdsByCid( $arrstrTaskReportFilter, $objDatabase ) {
		$strWhereCondition  = '';
		$strClientCondition = '';
		if( true == isset( $arrstrTaskReportFilter['selected_properties'] ) && true == valArr( $arrstrTaskReportFilter['selected_properties'] ) ) {
			$strWhereCondition = ' AND t.property_id IN( ' . implode( ',', $arrstrTaskReportFilter['selected_properties'] ) . ' ) ';
		}
		if( true == isset( $arrstrTaskReportFilter['cids'] ) && true == valArr( $arrstrTaskReportFilter['cids'] ) ) {
			$strClientCondition = ' AND tc.cid IN( ' . implode( ',', $arrstrTaskReportFilter['cids'] ) . ' ) ';
		}
		$strTaskNoteTimeCondition = ' tn.created_on BETWEEN \'' . $arrstrTaskReportFilter['from_date'] . ' 00:00:00\' AND \'' . $arrstrTaskReportFilter['to_date'] . ' 23:59:59\'';
		$strTaskTimeCondition     = ' t.created_on BETWEEN \'' . $arrstrTaskReportFilter['from_date'] . ' 00:00:00\' AND \'' . $arrstrTaskReportFilter['to_date'] . ' 23:59:59\'';

		$strSql = ' SELECT
						e.name_full,
						u.id AS user_id
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
					WHERE
						u.id IN (
									SELECT
										DISTINCT ( CASE
														WHEN tn.task_id = tc.task_id AND tn.start_time IS NOT NULL AND tn.task_note_type_id IN ( ' . CTaskNoteType::SUPPORT_CALL_REQUEST . ',' . CTaskNoteType::SUPPORT_EMAIL_REQUEST . ',' . CTaskNoteType::SUPPORT_LIVE_CHAT_REQUEST . ') ' . $strClientCondition . ' AND ' . $strTaskNoteTimeCondition . ' THEN tn.created_by
														WHEN tn.task_id = tc.task_id AND tn.start_time IS NOT NULL AND tc.task_note_id = tn.id AND ' . $strTaskNoteTimeCondition . ' THEN tn.created_by
														WHEN t.duration_minutes IS NOT NULL AND tc.task_note_id IS NULL AND ' . $strTaskTimeCondition . ' THEN t.created_by
												END )
										FROM
											tasks t
											LEFT JOIN task_notes tn ON ( t.id = tn.task_id AND ' . $strTaskNoteTimeCondition . ' AND tn.start_time IS NOT NULL )
											JOIN task_companies tc ON ( t.id = tc.task_id ' . $strClientCondition . ' AND tc.created_on BETWEEN \'' . $arrstrTaskReportFilter['from_date'] . ' 00:00:00\' AND \'' . $arrstrTaskReportFilter['to_date'] . ' 23:59:59\')
										WHERE
											CASE
												WHEN t.duration_minutes IS NOT NULL THEN ' . $strTaskTimeCondition . '
												ELSE ' . $strTaskNoteTimeCondition . '
											END
											 ' . $strWhereCondition . $strClientCondition . '
								)
						AND e.employee_status_type_id =' . ( int ) CEmployeeStatusType::CURRENT . '
						AND e.department_id IN ( ' . implode( ',', $arrstrTaskReportFilter['selected_department_ids'] ) . ' )
					ORDER BY
						e.name_full ASC';

		return fetchData( $strSql, $objDatabase );
	}

	// Fetch all active employees with team id and manager employee id.

	public static function fetchAllActiveEmployeesContactDetails( $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT e.id,
						e.name_full,
						epn.phone_number,
						e.email_address,
						t.team_id,
						t.manager_employee_id
					FROM
						employees e
						LEFT JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id AND epn.phone_number_type_id =' . CPhoneNumberType::PRIMARY . ' )
						LEFT JOIN team_employees t ON (t.employee_id = e.id)
					WHERE
						employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )
						AND e.name_full IS NOT NULL
						AND e.email_address IS NOT NULL
						AND epn.phone_number IS NOT NULL
						AND e.email_address LIKE \'%xento.com\'
					ORDER BY
						e.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedUpcomingOrConfirmatoryReviewEmployeesByCountryCode( $strCountryCode, $intHREmployeeId = NULL, $intPageNo, $intPageSize, $objAdminDatabase, $strFromDate = NULL, $strToDate = NULL, $boolIsUpcomingReview = false, $strOrderByField, $strOrderByType, $intDepartmentId = NULL, $arrstrFilteredExplodedSearch = NULL ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSubSql                 = '';
		$strDateSubSql             = '';
		$strInnerSqlWhereCondition = '';
		$strSearchConditionSql     = '';

		$intPendingStatus   = 1;
		$intInProcessStatus = 2;
		$intCompletedStatus = 3;

		if( false == is_null( $intHREmployeeId ) && true == is_numeric( $intHREmployeeId ) ) {
			$strInnerSqlWhereCondition .= ' AND t.hr_representative_employee_id = \'' . $intHREmployeeId . '\'';
		}

		if( false == is_null( $intDepartmentId ) && true == is_numeric( $intDepartmentId ) ) {
			$strInnerSqlWhereCondition .= ' AND d.id = \'' . $intDepartmentId . '\'';
		}

		if( true == valStr( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		if( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) {
			if( CCountry::CODE_INDIA == $strCountryCode ) {
				$strDateSubSql = ' WHERE reviews.confirmation_date BETWEEN \'' . $strFromDate . '\' AND \'' . $strToDate . '\' ';
			}

		} else {
			if( false == $boolIsUpcomingReview ) {
				$strStartDate = date( 'm-01-Y', strtotime( date( 'd-m-Y' ) ) );
				$strEndDate   = date( 'm-t-Y', strtotime( date( 'd-m-Y' ) ) );
			} else {
				$strStartDate = date( 'm-d-Y', mktime( 0, 0, 0, date( 'm' ) + 1, 1, date( 'Y' ) ) );
				$strEndDate   = date( 'm-d-Y', mktime( 0, 0, 0, date( 'm' ) + 2, 0, date( 'Y' ) ) );
			}

			if( CCountry::CODE_INDIA == $strCountryCode ) {
				$strDateSubSql = ' WHERE reviews.confirmation_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\' ';
			}
		}

		if( CCountry::CODE_INDIA == $strCountryCode ) {
			$strCaseSql   = ' CASE
								WHEN ( ( ead.experience_year * 12 ) + ead.experience_month ) >= 12
								THEN ( e.date_started + INTERVAL \'120 days\' )
								ELSE ( e.date_started + INTERVAL \'180 days\' )
							END AS confirmation_date,';
			$strStatusSql = '';
		} else {
			$strCaseSql   = '';
			$strStatusSql = ' or ( ( eas.assessment_datetime not between \'' . date( 'Y-04-01' ) . '\' AND \'' . date( 'Y-04-30' ) . '\' )
								and ( eas.assessment_datetime not between \'' . date( 'Y-10-01' ) . '\' AND \'' . date( 'Y-10-t' ) . '\') )';
		}

		if( false == is_null( $arrstrFilteredExplodedSearch ) ) {
			$strSearchConditionSql = ' AND ( ( reviews.preferred_name ILIKE \'%' . implode( '%\' OR reviews.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\') OR ( reviews.reporting_manager ILIKE \'%' . implode( '%\' OR reviews.reporting_manager ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' ) )';
		}

		$strSql = 'SELECT
						DISTINCT reviews.id,
						reviews.preferred_name,
						reviews.date_started,
						reviews.reporting_manager,
						reviews.total_experience,
						reviews.xento_experience,
						reviews.upcoming_status,
						reviews.hr_representative_name,
						reviews.department_id,
						reviews.department_name,
						reviews.hr_representative_employee_id,
						reviews.last_assessed_on,
						max ( CASE
								WHEN reviews.id NOT IN ( SELECT employee_id FROM employee_assessments WHERE deleted_by IS NULL ) ' . $strStatusSql . '
								THEN ' . ( int ) $intPendingStatus . '
								ELSE CASE
										WHEN eas.id = eae.employee_assessment_id AND eae.finalized_on IS NULL
										THEN ' . ( int ) $intPendingStatus . '
										ELSE CASE
												WHEN eas.id = eae.employee_assessment_id AND eae.finalized_on IS NOT NULL AND eas.completed_on IS NULL
												THEN ' . ( int ) $intInProcessStatus . '
												ELSE ' . ( int ) $intCompletedStatus . '
											END
									END
								END ) AS status
					FROM (
							SELECT
								e.id,
								e.preferred_name,
								e.date_started,
								e.last_assessed_on::date as last_assessed_on,
								e1.preferred_name AS reporting_manager,
								e2.preferred_name AS hr_representative_name,
								d.id AS department_id,
								d.name AS department_name,
								t.hr_representative_employee_id,' . $strCaseSql . '

								CASE
									WHEN e.id IN ( SELECT employee_id FROM employee_assessments WHERE deleted_by IS NULL )
									THEN 1
									ELSE 0
								END AS upcoming_status,
								( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) * 12 ) + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) + ( ead.experience_year * 12 ) + ead.experience_month AS total_experience,
								( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) * 12 ) + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) AS xento_experience
							FROM
								employees e
								JOIN employee_addresses eadd ON ( eadd.employee_id = e.id )
								LEFT JOIN employee_applications ea ON ( ea.employee_id = e.id )
								LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = ea.id )
								LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
								LEFT JOIN teams t ON ( te.team_id = t.id )
								LEFT JOIN employees e1 ON ( e.reporting_manager_id = e1.id )
								LEFT JOIN employees e2 ON ( e2.id = t.hr_representative_employee_id )
								LEFT JOIN departments d ON ( d.id = e.department_id )
							WHERE
								e.date_terminated IS NULL
								AND e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
								AND eadd.country_code = \'' . $strCountryCode . '\'' . $strInnerSqlWhereCondition . '
							ORDER BY
								e.date_started DESC
						) AS reviews
						LEFT JOIN employee_assessments eas ON ( reviews.id = eas.employee_id AND eas.deleted_by IS NULL )
						LEFT JOIN employee_assessment_employees eae ON ( eas.id = eae.employee_assessment_id AND eae.deleted_by IS NULL )

						' . $strDateSubSql . $strSearchConditionSql . '
					GROUP BY
						reviews.id,
						reviews.preferred_name,
						reviews.date_started,
						reviews.reporting_manager,
						reviews.total_experience,
						reviews.xento_experience,
						reviews.upcoming_status,
						reviews.hr_representative_name,
						reviews.department_id,
						reviews.department_name,
						reviews.hr_representative_employee_id,
						reviews.last_assessed_on
					ORDER BY
						' . $strOrderByField . ' ' . $strOrderByType . $strSubSql;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchUpcomingOrConfirmatoryReviewEmployeesCountByCountryCode( $strCountryCode, $intHREmployeeId = NULL, $objAdminDatabase, $strFromDate = NULL, $strToDate = NULL, $boolIsUpcomingReview = false, $intDepartmentId = NULL ) {
		$strDateSubSql             = '';
		$strInnerSqlWhereCondition = '';

		if( false == is_null( $intHREmployeeId ) && true == is_numeric( $intHREmployeeId ) ) {
			$strInnerSqlWhereCondition .= ' AND t.hr_representative_employee_id = \'' . $intHREmployeeId . '\'';
		}

		if( false == is_null( $intDepartmentId ) && true == is_numeric( $intDepartmentId ) ) {
			$strInnerSqlWhereCondition .= ' AND d.id = \'' . $intDepartmentId . '\'';
		}

		if( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) {
			if( CCountry::CODE_INDIA == $strCountryCode ) {
				$strDateSubSql = ' WHERE reviews.confirmation_date BETWEEN \'' . $strFromDate . '\' AND \'' . $strToDate . '\' ';
			}
		} else {
			if( false == $boolIsUpcomingReview ) {
				$strStartDate = date( 'm-01-Y', strtotime( date( 'd-m-Y' ) ) );
				$strEndDate   = date( 'm-t-Y', strtotime( date( 'd-m-Y' ) ) );
			} else {
				$strStartDate = date( 'm-d-Y', mktime( 0, 0, 0, date( 'm' ) + 1, 1, date( 'Y' ) ) );
				$strEndDate   = date( 'm-d-Y', mktime( 0, 0, 0, date( 'm' ) + 2, 0, date( 'Y' ) ) );
			}

			if( CCountry::CODE_INDIA == $strCountryCode ) {
				$strDateSubSql = ' WHERE reviews.confirmation_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\' ';
			}
		}

		if( CCountry::CODE_INDIA == $strCountryCode ) {
			$strCaseSql = ' ,CASE
								WHEN ( ( ead.experience_year * 12 ) + ead.experience_month ) >= 12
								THEN ( e.date_started + INTERVAL \'120 days\' )
								ELSE ( e.date_started + INTERVAL \'180 days\' )
							END AS confirmation_date ';
			$strJoinSql = 'JOIN employee_applications ea ON ( ea.employee_id = e.id )
							JOIN employee_application_details ead ON ( ead.employee_application_id = ea.id )';
		} else {
			$strCaseSql = '';
			$strJoinSql = '';
		}

		$strSql = '	SELECT
						count( confirm_reviews.id )
					FROM (
							SELECT
								reviews.id
							FROM (
									SELECT
										e.id,
										e.date_started' . $strCaseSql . '
									FROM
										employees e
										JOIN employee_addresses eadd ON ( eadd.employee_id = e.id AND eadd.address_type_id = ' . CAddressType::PRIMARY . ' ) ' . $strJoinSql . '
										LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
										LEFT JOIN teams t1 ON ( t1.manager_employee_id = e.id AND t1.deleted_by IS NULL )
										LEFT JOIN teams t ON ( te.team_id = t.id AND t.deleted_by IS NULL )
										LEFT JOIN departments d ON ( d.id = e.department_id )
									WHERE
										e.date_terminated IS NULL
										AND e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
										AND eadd.country_code = \'' . $strCountryCode . '\' ' . $strInnerSqlWhereCondition . '
									ORDER BY
										e.date_started DESC
								) AS reviews
							 ' . $strDateSubSql . '
						) AS confirm_reviews';

		$arrintCurrentEmployeesForReviewCount = fetchData( $strSql, $objAdminDatabase );

		if( true == isset( $arrintCurrentEmployeesForReviewCount[0]['count'] ) ) {
			return $arrintCurrentEmployeesForReviewCount[0]['count'];
		}
	}

	public static function fetchPaginatedAnnualReviewEmployees( $objReviewsFilter = NULL, $intHrEmployeeId, $intPageNo, $intPageSize, $boolIsDownload = NULL, $strOrderByField, $strOrderByType, $strFilteredExplodedSearch = NULL, $boolIsCount = false, $arrstrReviewsDateFilter = NULL, $intAddEmployeeId, $objAdminDatabase, $arrintTeamEmployeeIds = NULL ) {
		$strSubSql                = '';
		$strWhereCondition        = '';
		$strJoinCondition         = '';
		$strAdditionalWhereClause = '';

		if( false == $boolIsDownload ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit  = ( int ) $intPageSize;

			if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
				$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			}
		}

		if( true == valArr( $arrintTeamEmployeeIds ) && false == is_null( $strFilteredExplodedSearch ) ) {
			$strWhereCondition = ' WHERE e.id IN (' . implode( ',', $arrintTeamEmployeeIds ) . ') AND e.department_id = ' . CDepartment::QA .
								'AND ( ( e.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\')
								OR ( ee.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\' ) )';
		} elseif( true == valArr( $arrintTeamEmployeeIds ) ) {
			$strWhereCondition = ' WHERE e.id IN (' . implode( ',', $arrintTeamEmployeeIds ) . ') AND e.department_id = ' . CDepartment::QA;
		}

		if( 'all_hr_representative' != $intHrEmployeeId && true == is_numeric( $intAddEmployeeId ) ) {
			if( false == is_null( $strFilteredExplodedSearch ) ) {
				$strWhereCondition = ' WHERE t.hr_representative_employee_id = \'' . ( int ) $intHrEmployeeId . '\'
										AND t.add_employee_id = \'' . ( int ) $intAddEmployeeId . '\'
										AND ( ( e.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\')
										OR ( ee.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\' ) )';
			} else {
				$strWhereCondition = ' WHERE t.hr_representative_employee_id = \'' . ( int ) $intHrEmployeeId . '\' AND t.add_employee_id = \'' . ( int ) $intAddEmployeeId . '\'';
			}

		} elseif( 'all_hr_representative' != $intHrEmployeeId && true == is_numeric( $intHrEmployeeId ) && false == is_null( $strFilteredExplodedSearch ) ) {
			$strWhereCondition = ' WHERE t.hr_representative_employee_id = \'' . ( int ) $intHrEmployeeId . '\'
										AND ( ( e.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\')
										OR ( ee.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\' ) )';
		} elseif( 'all_hr_representative' != $intHrEmployeeId && true == is_numeric( $intHrEmployeeId ) && true == is_null( $strFilteredExplodedSearch ) ) {
			$strWhereCondition = ' WHERE t.hr_representative_employee_id = \'' . ( int ) $intHrEmployeeId . '\'';
		} elseif( 'all_hr_representative' == $intHrEmployeeId && false == is_null( $strFilteredExplodedSearch ) ) {
			$strWhereCondition = ' WHERE ( ( e.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\')
										OR ( ee.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\' ) )';
		}

		if( true == valStr( $strWhereCondition ) ) {
			$strWhereCondition .= ' AND eadd.country_code = \'IN\' AND ep.key = \'EMPLOYEE_REVIEW_CYCLE\'';
		} else {
			$strWhereCondition = ' WHERE eadd.country_code = \'IN\' AND ep.key = \'EMPLOYEE_REVIEW_CYCLE\'';
		}

		if( true == is_null( $strFilteredExplodedSearch ) && true == valObj( $objReviewsFilter, 'CReviewsFilter' ) ) {

			$strWhereCondition .= ( true == valStr( $objReviewsFilter->getManagerEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' e.reporting_manager_id = ' . $objReviewsFilter->getManagerEmployeeId() : '';

			if( 'all_hr_representative' != $objReviewsFilter->getHrRepresentativeEmployeeId() ) {
				$strWhereCondition .= ( true == valStr( $objReviewsFilter->getHrRepresentativeEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.hr_representative_employee_id = ' . $objReviewsFilter->getHrRepresentativeEmployeeId() : '';
			}

			if( 'all_director' != $objReviewsFilter->getDirectorEmployeeId() ) {
				$strWhereCondition .= ( true == valStr( $objReviewsFilter->getDirectorEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.add_employee_id = ' . $objReviewsFilter->getDirectorEmployeeId() : '';
			}

			if( 'all_qam' != $objReviewsFilter->getQamEmployeeId() ) {
				$strWhereCondition .= ( true == valStr( $objReviewsFilter->getQamEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.qam_employee_id = ' . $objReviewsFilter->getQamEmployeeId() : '';
			}

			$strWhereCondition .= ( true == valStr( $objReviewsFilter->getTeamId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' te.team_id = ' . $objReviewsFilter->getTeamId() : '';
			$strWhereCondition .= ( true == valStr( $objReviewsFilter->getReviewCycle() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' ep.value = \'' . $objReviewsFilter->getReviewCycle() . '\'' : '';
			$strWhereCondition .= ( true == valId( $objReviewsFilter->getDepartmentId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' dep.id = ' . $objReviewsFilter->getDepartmentId() : '';

			if( true == valStr( $objReviewsFilter->getIsRevieweeResponse() ) || true == valStr( $objReviewsFilter->getIsManagerResponse() ) ) {
				$strJoinCondition .= ' LEFT JOIN employee_assessments eas ON( ( eas.manager_employee_id = e.reporting_manager_id AND eas.employee_id = e.id ) AND eas.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' ) AND eas.deleted_on IS NULL )
									 LEFT JOIN employee_assessment_employees eae ON( ( eae.employee_id = e.reporting_manager_id ) AND eas.id = eae.employee_assessment_id )
									 LEFT JOIN employee_assessments eas1 ON( ( eas1.employee_id = e.id ) AND eas1.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' ) AND eas1.deleted_on IS NULL )
									 LEFT JOIN employee_assessment_employees eae1 ON( ( eae1.employee_id = e.id ) AND eas1.id = eae1.employee_assessment_id )';

				$strWhereCondition .= ( 1 == $objReviewsFilter->getIsRevieweeResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae1.finalized_on IS NOT NULL ' : '';
				$strWhereCondition .= ( 2 == $objReviewsFilter->getIsRevieweeResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae1.finalized_on IS NULL ' : '';
				$strWhereCondition .= ( 1 == $objReviewsFilter->getIsManagerResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae.finalized_on IS NOT NULL ' : '';
				$strWhereCondition .= ( 2 == $objReviewsFilter->getIsManagerResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae.finalized_on IS NULL ' : '';
			}
		}

		if( true == $boolIsCount ) {

			$strSelectClause          = 'count(DISTINCT ep.employee_id)';
			$strAdditionalGroupClause = '';
			$strAllConditions         = '';
			$strOrderBy               = '';
			$intYear                  = NULL;

			if( true == valObj( $objReviewsFilter, 'CReviewsFilter' ) ) {

				if( true == valStr( $objReviewsFilter->getYear() ) ) {
					$intYear = $objReviewsFilter->getYear();
				}

				if( true == is_numeric( $intYear ) ) {
					$strJoinCondition .= ' LEFT JOIN employee_assessments eass ON ( e.id = eass.employee_id AND eass.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' ) AND eass.deleted_on IS NULL )';
					if( $intYear < date( 'Y' ) ) {
						$strAdditionalWhereClause = ' AND EXTRACT ( MONTH FROM ( eass.assessment_datetime ) ) = 10 AND EXTRACT ( MONTH FROM ( eass.assessment_datetime ) ) = ' . $intYear;
					} else {
						$strAdditionalWhereClause = ' AND ( ep1.value = ' . $intYear . '::text OR ep1.value IS NULL )';
					}
				}
			}
		} else {

			if( true == valObj( $objReviewsFilter, 'CReviewsFilter' ) ) {

				if( true == valStr( $objReviewsFilter->getYear() ) ) {
					$intYear = $objReviewsFilter->getYear();
				}

				if( true == is_numeric( $intYear ) ) {
					if( $intYear < date( 'Y' ) ) {
						$strAdditionalWhereClause = ' AND EXTRACT ( MONTH FROM ( eass.assessment_datetime ) ) = 10 AND EXTRACT ( MONTH FROM ( eass.assessment_datetime ) ) = ' . $intYear;
					} else {
						$strAdditionalWhereClause = ' AND ( ep1.value = ' . $intYear . '::text OR ep1.value IS NULL )';
					}

					$strAdditionalGroupClause = ', eass.completed_on, eass.created_on ';
					$strJoinCondition         .= ' LEFT JOIN employee_assessments eass ON ( e.id = eass.employee_id AND eass.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' ) AND eass.deleted_on IS NULL )';
				}
			}

			$intReviewYear = ( date( 'm' ) > 10 ) ? date( 'Y' ) + 1 : date( 'Y' );
			$strSelectClause = 'DISTINCT
								e.id as emp_id,
								e.name_full,
								e.preferred_name,
								e.date_started,
								e.last_assessed_on::date AS last_assessed_on,
								e.employee_number,
								e.email_address,
								e.date_confirmed,
								d.name AS designation_name,
								dep.name AS department_name,
								t.name AS team_name,
								emp.preferred_name AS pdm_name,
								ee.preferred_name AS reporting_manager,
								eee.preferred_name AS hr_representative,
								emp_qam.preferred_name AS quality_manager,
								emp_add.preferred_name AS director,
								ep.value AS review_cycle,
								CASE WHEN ep1.id IS NULL THEN \'' . $intReviewYear . '\'::text ELSE ep1.value END AS next_review_year,
								( EXTRACT ( YEAR FROM AGE ( CURRENT_DATE, e.date_started ) ) * 12 ) + EXTRACT ( MONTH FROM AGE ( CURRENT_DATE, e.date_started ) ) AS xento_experience,
								CASE
									WHEN ( ead.experience_year IS NULL OR ead.experience_year = 0 ) AND ( ead.experience_month IS NULL OR ead.experience_month = 0 ) THEN
										( EXTRACT ( YEAR FROM AGE ( CURRENT_DATE, e.date_started ) ) * 12 ) + EXTRACT ( MONTH FROM AGE ( CURRENT_DATE, e.date_started ) )
									ELSE
										( EXTRACT ( YEAR FROM AGE ( CURRENT_DATE, e.date_started ) ) * 12 ) + EXTRACT ( MONTH FROM AGE ( CURRENT_DATE, e.date_started ) ) + ( ead.experience_year * 12 ) + ead.experience_month
								END AS total_experience ';

			$strAllConditions = ' GROUP BY e.date_started, e.id, d.name, dep.name, t.name, emp.preferred_name, ee.preferred_name, eadd.country_code, eee.preferred_name, ep.value, ead.experience_year, ead.experience_month, e.date_started,emp_qam.preferred_name,emp_add.preferred_name, ep1.id ' . $strAdditionalGroupClause;
			$strOrderBy       = ' ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . $strSubSql;

		}

		if( true == valArr( $arrstrReviewsDateFilter ) ) {
			$strSelectClause   .= ', CASE
									WHEN ( SUM ( story_points_released ) != 0 ) THEN SUM ( story_points_released )
									ELSE 0
								END AS story_points_released,
								CASE
									WHEN ( SUM ( tasks_released ) != 0 ) THEN SUM ( tasks_released )
									ELSE 0
								END AS tasks_released,
								CASE
									WHEN ( SUM ( bugs_resolved ) != 0 ) THEN SUM ( bugs_resolved )
									ELSE 0
								END AS bugs_resolved,
								CASE
									WHEN ( SUM ( features_released ) != 0 ) THEN SUM ( features_released )
									ELSE 0
								END AS features_released,
								CASE
									WHEN ( SUM ( qa_rejections ) != 0 ) THEN SUM ( qa_rejections )
									ELSE 0
								END AS qa_rejections';
			$strWhereCondition .= 'AND ( sde.week BETWEEN \'' . $arrstrReviewsDateFilter['from_date'] . '\' AND \'' . $arrstrReviewsDateFilter['to_date'] . '\' OR story_points_released IS NULL )';
			$strJoinCondition  .= 'LEFT JOIN stats_developer_employees sde ON ( sde.employee_id = e.id )';
		}

		$strSql = ' SELECT
						' . $strSelectClause . '
					FROM
						employee_preferences ep
						JOIN employees e ON ( ep.employee_id = e.id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ')
						LEFT JOIN employee_applications ea ON ( ea.employee_id = e.id )
						LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = ea.id )
						LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT JOIN employees ee ON ( e.reporting_manager_id = ee.id )
						LEFT JOIN teams t ON ( te.team_id = t.id )
						LEFT JOIN employees emp ON ( t.sdm_employee_id = emp.id )
						LEFT JOIN employees emp_qam ON ( t.qam_employee_id = emp_qam.id )
						LEFT JOIN employees emp_add ON ( t.add_employee_id = emp_add.id )
						LEFT JOIN designations d ON ( e.designation_id = d.id )
						LEFT JOIN departments dep ON ( e.department_id = dep.id )
						LEFT JOIN employee_addresses eadd ON ( eadd.employee_id = e.id AND eadd.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employee_preferences ep1 ON ( e.id = ep1.employee_id AND ep1.key = \'EMPLOYEE_REVIEW_CYCLE_YEAR\' )
						LEFT JOIN employees eee ON ( t.hr_representative_employee_id = eee.id )' . $strJoinCondition . $strWhereCondition . $strAdditionalWhereClause . $strAllConditions . $strOrderBy;

		if( true == $boolIsCount ) {
			$arrintEmployeesForAnnualReviewCount = fetchData( $strSql, $objAdminDatabase );
			if( true == isset( $arrintEmployeesForAnnualReviewCount[0]['count'] ) ) {
				return $arrintEmployeesForAnnualReviewCount[0]['count'];
			}
		} else {
			return fetchData( $strSql, $objAdminDatabase );
		}
	}

	public static function fetchAnnualReviewReviewers( $objReviewsFilter = NULL, $intHrEmployeeId, $objAdminDatabase, $intReviewYear, $strCountryCode ) {

		$strAssessmentMonthSql        = '';
		$strDate                      = '';
		$strHrRepresentativeCondition = '';

		$arrintOfficeIds                    = [];
		$arrintEmployeeAssessmentTypeLevels = [];

		if( CCountry::CODE_INDIA == $strCountryCode ) {

			$strWhereClause  = 'AND eas.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' )';
			$arrintOfficeIds = [ COffice::TOWER_8, COffice::TOWER_9 ];

		} else {

			$arrintEmployeeAssessmentTypeLevels = [
				CEmployeeAssessmentTypeLevel::US_SIX_MONTHS_PERFORMANCE_REVIEW,
				CEmployeeAssessmentTypeLevel::US_THREE_MONTHS_PERFORMANCE_REVIEW
			];
			$strWhereClause                     = ' AND
						east.employee_assessment_type_level_id IN ( ' . implode( ',', $arrintEmployeeAssessmentTypeLevels ) . ' ) ';
			$arrintOfficeIds                    = [ COffice::US_HQ_FLOOR_ONE, COffice::US_HQ_FLOOR_TWO, COffice::US_HQ_FLOOR_THREE, COffice::US_HQ_FLOOR_FOUR, COffice::US_PROVO ];
		}

		if( true == valStr( $objReviewsFilter->getReviewCycle() ) ) {
			if( 'April' == $objReviewsFilter->getReviewCycle() ) {
				$strDate               = ' 04-01-' . $intReviewYear;
				$strAssessmentMonthSql = ' AND EXTRACT ( MONTH FROM ( eas.assessment_datetime ) ) IN ( 01, 02, 03, 04, 05, 06 )';
			} else {
				$strDate               = ' 10-01-' . $intReviewYear;
				$strAssessmentMonthSql = ' AND EXTRACT ( MONTH FROM ( eas.assessment_datetime ) ) IN ( 07, 08, 09, 10, 11,12 )';
			}
		}

		if( 'all_hr_representative' != $intHrEmployeeId ) {
			$strHrRepresentativeCondition = ' AND t.hr_representative_employee_id = \'' . $intHrEmployeeId . '\' ';
		}

		$strSql = 'SELECT
						final_reviews.id AS emp_id,
						final_reviews.assessment_id,
						e.preferred_name AS reviewers,
						eae.finalized_on,
						east.is_verification_required,
						CASE
							WHEN eas.completed_on IS NOT NULL AND EXTRACT( YEAR FROM( eas.assessment_datetime ) ) = ' . ( int ) $intReviewYear . '
							THEN 4
							ELSE CASE
									WHEN eas.assessment_end_date IS NULL AND EXTRACT( YEAR FROM ( eas.assessment_datetime ) ) = ' . ( int ) $intReviewYear . ' AND
										( SELECT id FROM employee_assessment_employees eae WHERE final_reviews.id = eae.employee_id AND final_reviews.assessment_id = eae.employee_assessment_id AND eae.deleted_by IS NULL ) IN
										( SELECT id FROM employee_assessment_employees eae WHERE final_reviews.assessment_id = eae.employee_assessment_id AND finalized_on IS NOT NULL AND eae.deleted_by IS NULL ) AND
										( SELECT count ( id ) FROM employee_assessment_employees WHERE employee_assessment_id = final_reviews.assessment_id AND finalized_on IS NOT NULL AND deleted_by IS NULL ) >= 2
									THEN 2
									ELSE CASE
											WHEN EXTRACT ( YEAR FROM ( eas.assessment_end_date ) ) >= ' . ( int ) $intReviewYear . ' AND EXTRACT ( YEAR FROM ( eas.assessment_datetime ) ) = ' . ( int ) $intReviewYear . '
											THEN 3
											ELSE 1
										 END
								END
						END AS status
					FROM (
							SELECT
								reviews.id,
								max(eas.id) AS assessment_id
							FROM(
								SELECT
									e.id
								FROM
									employees e
									LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
									LEFT JOIN teams t ON ( te.team_id = t.id )
								WHERE
									e.date_terminated IS NULL and e.office_id in (' . implode( ',', $arrintOfficeIds ) . ')' . $strHrRepresentativeCondition . '
								) AS reviews
								LEFT JOIN employee_assessments eas ON ( reviews.id = eas.employee_id AND eas.deleted_by IS NULL )
							WHERE
								 EXTRACT ( YEAR FROM( eas.assessment_datetime ) ) = ' . ( int ) $intReviewYear . $strAssessmentMonthSql . '
							GROUP BY reviews.id
						) AS final_reviews
						LEFT JOIN employee_assessments eas ON ( final_reviews.id = eas.employee_id AND final_reviews.assessment_id = eas.id AND eas.deleted_by IS NULL AND eas.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' ) )
						LEFT JOIN employee_assessment_employees eae ON ( final_reviews.assessment_id = eae.employee_assessment_id AND eae.deleted_by IS NULL )
						LEFT JOIN employee_assessment_types east ON ( eas.employee_assessment_type_id = east.id )
						LEFT JOIN employees e on( eae.employee_id = e.id )
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.date_terminated IS NULL ' . $strWhereClause;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchPaginatedBiAnnualReviewEmployees( $objReviewsFilter = NULL, $intPageNo, $intPageSize, $objAdminDatabase, $intReviewYear, $boolIsDownload = NULL, $strOrderByField, $strOrderByType, $strFilteredExplodedSearch = NULL, $arrintDepartmentIds = NULL ) {

		$strSubSql                   = '';
		$strSearchConditionSql       = '';
		$strDepartmentWhereCondition = '';
		$strJoinCondition            = '';

		if( date( 'm' ) < 5 || date( 'm' ) > 10 ) {
			$intReviewCycle = 04;
		}

		if( date( 'm' ) > 4 && date( 'm' ) < 11 ) {
			$intReviewCycle = 10;
		}

		if( false == $boolIsDownload ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit  = ( int ) $intPageSize;

			if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
				$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			}
		}

		if( true == valArr( $arrintDepartmentIds ) ) {
			$strDepartmentWhereCondition = ' AND e.department_id IN (' . implode( ',', $arrintDepartmentIds ) . ')';
		}

		if( NULL == $intReviewYear ) {
			$intReviewYear = date( 'Y' );
		}

		if( false == is_null( $strFilteredExplodedSearch ) ) {
			$strSearchConditionSql = ' WHERE ( ( reviews.name_full ILIKE \'%' . $strFilteredExplodedSearch . '%\') OR ( reviews.reporting_manager ILIKE \'%' . $strFilteredExplodedSearch . '%\' ) )';
		}

		$strWhereCondition = 'AND e.date_terminated IS NULL';

		if( true == is_null( $strFilteredExplodedSearch ) && true == valObj( $objReviewsFilter, 'CReviewsFilter' ) ) {

			$strWhereCondition .= ( true == valStr( $objReviewsFilter->getManagerEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' e.reporting_manager_id = ' . $objReviewsFilter->getManagerEmployeeId() : '';

			if( 'all_hr_representative' != $objReviewsFilter->getHrRepresentativeEmployeeId() ) {
				$strWhereCondition .= ( true == valStr( $objReviewsFilter->getHrRepresentativeEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.hr_representative_employee_id = ' . $objReviewsFilter->getHrRepresentativeEmployeeId() : '';
			}

			$strWhereCondition .= ( true == valStr( $objReviewsFilter->getTeamId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' te.team_id = ' . $objReviewsFilter->getTeamId() : '';

			if( true == valStr( $objReviewsFilter->getIsRevieweeResponse() ) || true == valStr( $objReviewsFilter->getIsManagerResponse() ) ) {
				$strJoinCondition .= ' LEFT JOIN employee_assessments eas ON( ( eas.manager_employee_id = e.reporting_manager_id AND eas.employee_id = e.id ) AND eas.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' ) AND eas.completed_on IS NULL )
									 LEFT JOIN employee_assessment_employees eae ON( ( eae.employee_id = e.reporting_manager_id ) AND eas.id = eae.employee_assessment_id )
									 LEFT JOIN employee_assessments eas1 ON( ( eas1.employee_id = e.id ) AND eas1.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' ) AND eas1.completed_on IS NULL )
									 LEFT JOIN employee_assessment_employees eae1 ON( ( eae1.employee_id = e.id ) AND eas1.id = eae1.employee_assessment_id )';

				$strWhereCondition .= ( 1 == $objReviewsFilter->getIsRevieweeResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae1.finalized_on IS NOT NULL ' : '';
				$strWhereCondition .= ( 2 == $objReviewsFilter->getIsRevieweeResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae1.finalized_on IS NULL ' : '';
				$strWhereCondition .= ( 1 == $objReviewsFilter->getIsManagerResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae.finalized_on IS NOT NULL ' : '';
				$strWhereCondition .= ( 2 == $objReviewsFilter->getIsManagerResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae.finalized_on IS NULL ' : '';
			}
		}

		$strSql = 'SELECT
						DISTINCT reviews.id as emp_id,
						reviews.name_full,
						reviews.preferred_name,
						reviews.date_started,
						reviews.reporting_manager,
						reviews.total_experience,
						reviews.xento_experience,
						reviews.email_address,
						reviews.date_confirmed,
						reviews.designation_name,
						reviews.department_name,
						reviews.team_name,
						reviews.pdm_name,
						reviews.country_code,
						reviews.hr_representative_name as hr_representative,
						reviews.hr_representative_employee_id,
						reviews.last_assessed_on as last_assessed_on
					FROM (
							SELECT
								e.id,
								e.name_full,
								e.preferred_name,
								e.date_started,
								e.email_address,
								e.date_confirmed,
								d.name AS designation_name,
								dep.name AS department_name,
								eadd.country_code,
								t.name AS team_name,
								emp.name_full AS pdm_name,
								e.last_assessed_on::date as last_assessed_on,
								e1.name_full AS reporting_manager,
								e2.name_full AS hr_representative_name,
								t.hr_representative_employee_id,
								( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) * 12 ) + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) + ( ead.experience_year * 12 ) + ead.experience_month AS total_experience,
								( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) * 12 ) + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) AS xento_experience
							FROM
								employees e
								JOIN employee_addresses eadd ON ( eadd.employee_id = e.id AND eadd.address_type_id = ' . CAddressType::PRIMARY . ' )
								LEFT JOIN employee_applications ea ON ( ea.employee_id = e.id )
								LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = ea.id )
								LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
								LEFT JOIN teams t ON ( te.team_id = t.id )
								LEFT JOIN employees emp ON ( t.sdm_employee_id = emp.id )
								LEFT JOIN designations d ON ( e.designation_id = d.id )
								LEFT JOIN departments dep ON ( e.department_id = dep.id )
								LEFT JOIN employees e1 ON ( e.reporting_manager_id = e1.id )
								LEFT JOIN employees e2 ON ( e2.id = t.hr_representative_employee_id )' . $strJoinCondition . '
							WHERE
								e.date_started < \'' . date( $intReviewYear . '-' . $intReviewCycle . '-01' ) . '\'
								AND e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
								AND eadd.country_code = \'' . CCountry::CODE_USA . '\'' . $strWhereCondition . '
								' . $strDepartmentWhereCondition . '
							ORDER BY
								e.date_started DESC
						) AS reviews
						' . $strSearchConditionSql . '
					GROUP BY
						reviews.id,
						reviews.name_full,
						reviews.preferred_name,
						reviews.date_started,
						reviews.reporting_manager,
						reviews.total_experience,
						reviews.xento_experience,
						reviews.email_address,
						reviews.date_confirmed,
						reviews.designation_name,
						reviews.department_name,
						reviews.team_name,
						reviews.pdm_name,
						reviews.country_code,
						reviews.hr_representative_name,
						reviews.hr_representative_employee_id,
						reviews.last_assessed_on
					ORDER BY
						' . $strOrderByField . ' ' . $strOrderByType . $strSubSql;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchBiAnnualReviewEmployeesCount( $objReviewsFilter = NULL, $objAdminDatabase, $intReviewYear, $arrintDepartmentIds ) {

		$strDepartmentWhereCondition = '';
		$strJoinCondition            = '';

		if( date( 'm' ) < 5 || date( 'm' ) > 10 ) {
			$intReviewCycle = 04;
		}
		if( date( 'm' ) > 4 && date( 'm' ) < 11 ) {
			$intReviewCycle = 10;
		}

		if( true == valArr( $arrintDepartmentIds ) ) {
			$strDepartmentWhereCondition .= ' AND e.department_id IN (' . implode( ',', $arrintDepartmentIds ) . ')';
		}

		$strWhereCondition = 'AND e.date_terminated IS NULL';

		if( true == valObj( $objReviewsFilter, 'CReviewsFilter' ) ) {

			$strWhereCondition .= ( true == valStr( $objReviewsFilter->getManagerEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' e.reporting_manager_id = ' . $objReviewsFilter->getManagerEmployeeId() : '';

			if( 'all_hr_representative' != $objReviewsFilter->getHrRepresentativeEmployeeId() ) {
				$strWhereCondition .= ( true == valStr( $objReviewsFilter->getHrRepresentativeEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.hr_representative_employee_id = ' . $objReviewsFilter->getHrRepresentativeEmployeeId() : '';
			}

			$strWhereCondition .= ( true == valStr( $objReviewsFilter->getTeamId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' te.team_id = ' . $objReviewsFilter->getTeamId() : '';

			if( true == valStr( $objReviewsFilter->getIsRevieweeResponse() ) || true == valStr( $objReviewsFilter->getIsManagerResponse() ) ) {
				$strJoinCondition .= ' LEFT JOIN employee_assessments eas ON( ( eas.manager_employee_id = e.reporting_manager_id AND eas.employee_id = e.id ) AND eas.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' ) AND eas.completed_on IS NULL )
									LEFT JOIN employee_assessment_employees eae ON( ( eae.employee_id = e.reporting_manager_id ) AND eas.id = eae.employee_assessment_id )
									LEFT JOIN employee_assessments eas1 ON( ( eas1.employee_id = e.id ) AND eas1.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' ) AND eas1.completed_on IS NULL )
									LEFT JOIN employee_assessment_employees eae1 ON( ( eae1.employee_id = e.id ) AND eas1.id = eae1.employee_assessment_id )';

				$strWhereCondition .= ( 1 == $objReviewsFilter->getIsRevieweeResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae1.finalized_on IS NOT NULL ' : '';
				$strWhereCondition .= ( 2 == $objReviewsFilter->getIsRevieweeResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae1.finalized_on IS NULL ' : '';
				$strWhereCondition .= ( 1 == $objReviewsFilter->getIsManagerResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae.finalized_on IS NOT NULL ' : '';
				$strWhereCondition .= ( 2 == $objReviewsFilter->getIsManagerResponse() ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eae.finalized_on IS NULL ' : '';
			}
		}

		$strSql = 'SELECT
						count( DISTINCT e.id )
					FROM
						employees e
						JOIN employee_addresses eadd ON ( eadd.employee_id = e.id AND eadd.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT JOIN teams t1 ON ( t1.manager_employee_id = e.id )
						LEFT JOIN teams t ON ( te.team_id = t.id ) ' . $strJoinCondition . '
					WHERE
						e.date_started < \'' . date( $intReviewYear . '-' . $intReviewCycle . '-01' ) . '\'
						AND e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
						AND eadd.country_code = \'' . CCountry::CODE_USA . '\' ' . $strWhereCondition . ' ' . $strDepartmentWhereCondition;

		$arrintCurrentEmployeesForReviewCount = fetchData( $strSql, $objAdminDatabase );

		if( true == isset( $arrintCurrentEmployeesForReviewCount[0]['count'] ) ) {
			return $arrintCurrentEmployeesForReviewCount[0]['count'];
		}
	}

	public static function fetchRequestingEmployeesByHrRepresentativeEmployeeId( $strHrRepresentativeEmployeeIds, $boolIsHrEmployee, $objDatabase, $strCountryCode = NULL ) {

		$strCountryCodeCondition = ( false == empty( $strCountryCode ) ) ? ' AND ea.country_code = \'' . $strCountryCode . '\'' : '';

		$strSql = 'SELECT
						subq.*
					FROM
						(
							SELECT
								DISTINCT e.*
							FROM
								teams t
								JOIN team_employees te ON ( te.team_id = t.id AND te.is_primary_team = 1 )
								JOIN employees e1 ON ( te.employee_id = e1.id )
								JOIN employees e ON ( e.id = e1.reporting_manager_id OR e.department_id = ' . CDepartment::HR . ' )
								LEFT JOIN employee_addresses ea ON( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ')
							WHERE
								e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
								AND e.date_terminated IS NULL' . $strCountryCodeCondition;

		if( true == $boolIsHrEmployee && false == is_null( $strHrRepresentativeEmployeeIds ) ) {
			$strSql .= ' AND t.hr_representative_employee_id in ( ' . $strHrRepresentativeEmployeeIds . ' ) ';
		}

		$strSql .= ' 	UNION
							SELECT
								DISTINCT e.*
							FROM
								employees e
								JOIN users u ON ( u.employee_id = e.id )
								JOIN user_roles ur ON ( u.id = ur.user_id AND ur.role_id IN ( ' . CRole::ALLOW_PURCHASE_REQUEST_ACCESS . ', ' . CRole::APPROVES_PURCHASE_REQUESTS . ' ) AND ur.deleted_by IS NULL )
								LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ')
							WHERE
								e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
								AND e.date_terminated IS NULL ' . $strCountryCodeCondition . '
						) AS subq
					ORDER BY
						subq.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesHiringCountByDepartmentsByDateByCountryCode( $arrintDepatmentIds, $strStartDate, $strEndDate, $strCountryCode, $objDatabase ) {
		$strDepartmentIdsCondition = '';

		if( true == valArr( $arrintDepatmentIds ) ) {
			$strDepartmentIdsCondition = 'AND e.department_id IN (' . implode( ',', $arrintDepatmentIds ) . ' )';
		}

		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						department_id,
						department_name,
						DATE ( DATE_TRUNC ( \'month\', employee_data.date_started ) ),
						count ( * )
					FROM
						 (
							SELECT
								d.id AS department_id,
								d.name AS department_name,
								e.date_started AS date_started
							FROM
								employees e
								JOIN departments d ON ( e.department_id = d.id )
								LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id )
							WHERE
								ea.address_type_id = ' . CAddressType::PRIMARY;

		if( true == valStr( $strCountryCode ) && CCountry::CODE_USA == $strCountryCode ) {
			$strSql .= ' AND ea.country_code = \'' . CCountry::CODE_USA . '\'';
		} else {
			$strSql .= ' AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'';
		}

		$strSql .= ' AND e.date_started IS NOT NULL ' . $strDepartmentIdsCondition . '
					 AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						 ) AS employee_data
					 WHERE
						 ( DATE_TRUNC ( \'month\', employee_data.date_started ) >= \'' . $strStartDate . '\'
						 AND DATE_TRUNC ( \'month\', employee_data.date_started ) <= \'' . $strEndDate . '\' )

					 GROUP BY
						 DATE_TRUNC ( \'month\', employee_data.date_started ),
						 employee_data.department_id,
						 employee_data.department_name
					ORDER BY
						department_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchReportingManagersByCountryCode( $strCountryCode, $objDatabase ) {
		$strSql = 'SELECT
					id,
					preferred_name
				FROM
					employees
				WHERE
					id IN (
							SELECT
								DISTINCT ( e.id )
							FROM
								employees e1
								JOIN employees e ON ( e.id = e1.reporting_manager_id )
								JOIN employee_addresses ea ON ( ea.employee_id = e.id )
							WHERE
								e.employee_status_type_id = 1
								AND ea.country_code = \'' . $strCountryCode . '\')
							ORDER BY
									 employees.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeesByUserIds( $arrintUserIds, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						u.id as user_id,
						e.id,
						e.name_first,
						e.name_last,
						e.name_full,
						e.preferred_name,
						e.email_address
					FROM
						employees e
						LEFT JOIN users u on u.employee_id = e.id
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND u.id IN ( ' . implode( ',', $arrintUserIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMonthlyStoryPointsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		$strSql = 'SELECT
						date_trunc( \'month\', tr.release_datetime )::date as month,
						SUM( CASE WHEN td.qa_employee_id = e.id AND t.task_type_id = 121 THEN td.qa_story_points END ) as automation,
						SUM( CASE WHEN td.qa_employee_id = e.id AND t.task_type_id = 122 THEN td.qa_story_points END ) as testcases,
						SUM( CASE WHEN td.developer_employee_id = e.id AND td.audit_story_points IS NULL THEN td.developer_story_points WHEN td.qa_employee_id = e.id THEN td.qa_story_points ELSE td.audit_story_points END ) as total_story_points,
						COUNT( DISTINCT CASE WHEN td.developer_employee_id = e.id THEN td.developer_employee_id WHEN td.qa_employee_id = e.id THEN td.qa_employee_id END ) as total_team_members,
						array_to_string( array_agg( DISTINCT t.id ), \',\' ) as task_ids,
						count( DISTINCT t.id ) as task_count
					FROM
						tasks t
						JOIN task_details td ON td.task_id = t.id
						JOIN task_releases tr ON tr.id = t.task_release_id
						JOIN employees e ON e.id = td.developer_employee_id OR e.id = td.qa_employee_id
					WHERE
						t.task_status_id IN ( ' . implode( ', ', [ CTaskStatus::RELEASED_ON_RAPID, CTaskStatus::RELEASED_ON_STANDARD, CTaskStatus::VERIFIED_ON_RAPID, CTaskStatus::VERIFIED_ON_STANDARD, CTaskStatus::COMPLETED ] ) . ' )
						AND e.id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )
						AND tr.release_datetime > NOW() - INTERVAL \'14 months\'
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					GROUP BY date_trunc( \'month\', tr.release_datetime )
					ORDER BY
							date_trunc( \'month\', tr.release_datetime )';

		$arrmixMonthlyStoryPoints = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixMonthlyStoryPoints ) ) {
			$arrmixStoryPointsByMonths = [];
			foreach( $arrmixMonthlyStoryPoints as $arrmixMonthlyStoryPoint ) {
				$arrmixStoryPointsByMonths[$arrmixMonthlyStoryPoint['month']] = $arrmixMonthlyStoryPoint;
			}

			return $arrmixStoryPointsByMonths;
		} else {
			return [];
		}
	}

	public static function fetchEmployeesByOfficeIds( $arrintOfficeIds, $objDatabase ) {

		$strSql = 'SELECT
						id,
						preferred_name
					FROM
						employees
					WHERE
						employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND office_id IN ( ' . implode( ',', $arrintOfficeIds ) . ' )
					ORDER BY
						id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesByDepartmentIdsByIds( $arrintDepartmentIds, $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintDepartmentIds ) || false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						employees
					WHERE
						id IN ( ' . implode( ', ', $arrintIds ) . ' )
						AND department_id IN ( ' . implode( ', ', $arrintDepartmentIds ) . ' )
						AND employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )
					ORDER BY
						name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCountEmployeesHavingProposedDeskByListingTypeByOfficeIds( $strListingType, $arrintOfficeIds, $objDatabase, $intUserId = NULL ) {
		$strSql = 'select count( e.id )
						FROM employees e
						LEFT JOIN office_desks od';

		if( 'ADMIN' == $strListingType ) {
			$strSql .= ' ON e.desk_allocation_office_desk_id = od.id
				WHERE 	od.is_reserved = 1 AND od.desk_shifting_request_status = 0 ';

		} else {
			if( 'IT' == $strListingType ) {
				$strSql .= ' ON e.desk_allocation_office_desk_id = od.id
					where od.desk_shifting_request_status = 1 ';
			}
		}

		if( NULL != $intUserId ) {
			$strSql .= ' AND od.desk_shifting_request_by = ' . ( int ) $intUserId;
		}

		if( true == valArr( $arrintOfficeIds ) ) {
			$strSql .= ' AND od.office_id IN ( ' . implode( ',', $arrintOfficeIds ) . ' )';
		}

		$arrintEmployees = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintEmployees[0]['count'] ) ) {
			return $arrintEmployees[0]['count'];
		}
	}

	public static function fetchPaginatedEmployeesHavingProposedDeskByListingTypeByOfficeIds( $intPageNo, $intPageSize, $strListingType, $arrintOfficeIds, $objDatabase, $strDirectionFlag = 'ASC', $intUserId = NULL ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'select
						e.id as employee_id,
						e.preferred_name,
						e.office_desk_id,
						e.desk_allocation_office_desk_id,
						od.is_reserved,
						od.desk_shifting_request_by,
						e1.preferred_name as requested_by,
						DATE_PART ( \'day\', NOW ( )::timestamp - od.desk_shifting_request_on::timestamp ) AS pending_from
					FROM
						employees e
						LEFT JOIN office_desks od
						JOIN users u ON (od.desk_shifting_request_by = u.id)
						JOIN employees e1 ON (u.employee_id = e1.id)';

		if( 'ADMIN' == $strListingType ) {
			$strSql .= ' ON e.desk_allocation_office_desk_id = od.id
					WHERE 	od.is_reserved = 1 AND od.desk_shifting_request_status = 0 ';

		} else {
			if( 'IT' == $strListingType ) {
				$strSql .= ' ON e.desk_allocation_office_desk_id = od.id
					WHERE od.desk_shifting_request_status = 1 ';
			}
		}

		if( NULL != $intUserId ) {
			$strSql .= ' AND od.desk_shifting_request_by = ' . ( int ) $intUserId;
		}

		if( true == valArr( $arrintOfficeIds ) ) {
			$strSql .= ' AND od.office_id IN ( ' . implode( ',', $arrintOfficeIds ) . ' )';
		}

		$strSql .= ' ORDER BY od.desk_shifting_request_on ' . $strDirectionFlag . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		$arrmixEmployees = fetchData( $strSql, $objDatabase );

		return $arrmixEmployees;
	}

	public static function fetchSearchedEmployeesHavingPendingRequestsByListingType( $strFilteredExplodedSearch, $strListingType, $objDatabase ) {
		$strSql = 'select
						e.id as employee_id,
						e.preferred_name,
						e.office_desk_id,
						e.desk_allocation_office_desk_id,
						od.is_reserved, od.desk_shifting_request_by,
						DATE_PART ( \'day\', NOW ( )::timestamp - od.desk_shifting_request_on::timestamp ) AS pending_from
					FROM
						employees e
						LEFT JOIN office_desks od';

		if( 'ADMIN' == $strListingType ) {
			$strSql .= ' ON e.desk_allocation_office_desk_id = od.id
					LEFT JOIN office_desks od_current ON e.office_desk_id = od_current.id
					WHERE od.is_reserved = 1
					AND
					( to_char( e.id, \'999999999\' ) LIKE \'%' . $strFilteredExplodedSearch . '%\'
						OR e.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\'
						OR od.desk_number ILIKE \'%' . $strFilteredExplodedSearch . '%\'
						OR od_current.desk_number ILIKE \'%' . $strFilteredExplodedSearch . '%\'
					)
					LIMIT 10';

		} else {
			if( 'IT' == $strListingType ) {
				$strSql .= ' ON e.office_desk_id = od.id
					WHERE od.desk_shifting_request_status = 1
					AND
					( to_char( e.id, \'999999999\' ) LIKE \'%' . $strFilteredExplodedSearch . '%\'
						OR e.preferred_name ILIKE \'%' . $strFilteredExplodedSearch . '%\'
						OR od.desk_number ILIKE \'%' . $strFilteredExplodedSearch . '%\'
					)
					LIMIT 10';

			}
		}
		$arrmixEmployees = fetchData( $strSql, $objDatabase );

		return $arrmixEmployees;
	}

	public static function fetchCountEmployeesWithUnallocatedDesksByCountryCode( $strCountryCode, $objDatabase ) {
		$strSql = 'SELECT count(e.id)
						FROM
							employees AS e
							LEFT JOIN offices AS o ON ( e.office_id = o.id )
						WHERE
							e.office_desk_id IS NULL
							AND	e.desk_allocation_office_desk_id IS NULL
							AND e.name_full IS NOT NULL
							AND e.office_id IS NOT NULL
							AND e.employee_status_type_id =' . ( int ) CEmployeeStatusType::CURRENT . '
							AND o.country_code =\'' . $strCountryCode . '\'
							AND e.department_id <> 7';

		$arrintEmployees = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintEmployees[0]['count'] ) ) {
			return $arrintEmployees[0]['count'];
		}
	}

	public static function fetchPaginatedEmployeesWithUnallocatedDesksByCountryCode( $strCountryCode, $intPageNo, $intPageSize, $objDatabase ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT e.*
						FROM
							employees AS e
							LEFT JOIN offices AS o ON ( e.office_id = o.id )
						WHERE
							e.office_desk_id IS NULL
							AND	e.desk_allocation_office_desk_id IS NULL
							AND e.name_full IS NOT NULL
							AND e.office_id IS NOT NULL
							AND e.employee_status_type_id =' . ( int ) CEmployeeStatusType::CURRENT . '
							AND o.country_code =\'' . $strCountryCode . '\'
							AND e.department_id <> 7
						ORDER BY e.name_full' . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeByProposedOfficeDeskId( $intProposedOfficeDeskId, $objDatabase ) {
		$strSql = 'SELECT e.*
						FROM employees e
						WHERE 
							e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
							AND e.desk_allocation_office_desk_id = ' . ( int ) $intProposedOfficeDeskId . ' LIMIT 1';

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByOfficeDeskNumber( $arrstrOfficeDeskNumbers, $objDatabase ) {
		if( false == valArr( $arrstrOfficeDeskNumbers ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*
					FROM
						employees e
						JOIN office_desks od ON ( od.id = e.office_desk_id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND od.desk_number IN ( ' . implode( ',', $arrstrOfficeDeskNumbers ) . ' )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByProposedOfficeDeskNumber( $arrstrOfficeDeskNumbers, $objDatabase ) {
		if( false == valArr( $arrstrOfficeDeskNumbers ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*
					FROM
						employees e
						JOIN office_desks od ON ( od.id = e.desk_allocation_office_desk_id )
					WHERE
						od.desk_number IN ( ' . implode( ',', $arrstrOfficeDeskNumbers ) . ' )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesFullNameByProposedOfficeDeskId( $objDatabase ) {
		$strSql = 'SELECT emp.name_full, e.office_desk_id
						FROM employees e,
						employees emp
						WHERE 
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.office_desk_id = emp.desk_allocation_office_desk_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPreviousEmployeesByBirthDate( $objDatabase ) {
		$strSql = 'SELECT
						e.id,
						e.name_first,
						e.permanent_email_address,
						CASE
							WHEN ( date_part( \'month\', e.birth_date ) = date_part( \'month\', NOW() ) AND date_part( \'day\', e.birth_date ) = date_part( \'day\', NOW() ) )
							THEN 1
						END AS birth_today
						
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						( date_part( \'month\', e.birth_date ) = date_part( \'month\', NOW() ) AND date_part( \'day\', e.birth_date ) = date_part( \'day\', NOW() ) )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::PREVIOUS . '
						AND e.date_terminated IS NOT NULL
						AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
					ORDER BY
						e.name_first';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamsByManagerId( $intManagerId, $intHrRepresentativeEmployeeId, $objDatabase ) {
		if( false == is_numeric( $intManagerId ) || false == is_numeric( $intHrRepresentativeEmployeeId ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT t.id,
						t.name,
						te.employee_id
					FROM
						teams t,
						team_employees te
					WHERE
						t.id = te.team_id
						AND t.manager_employee_id IN (
														SELECT
															e.id
														FROM
															employees e
															JOIN employees e1 ON ( e1.reporting_manager_id = e.id )
															LEFT JOIN team_employees te ON ( te.employee_id = e1.id )
															left join teams t on (t.id = te.team_id)
														WHERE
															te.is_primary_team = 1
															AND e.id = ' . ( int ) $intManagerId . '
															AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
															AND e.employee_status_type_id = 1
															AND ( e.date_terminated IS NULL
															OR e.date_terminated > NOW ( ) )
						)
						AND te.is_primary_team = 1
						AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
					ORDER BY
						t.name ASC';

		$arrmixEmployees							= fetchData( $strSql, $objDatabase );
		$arrmixEmployees							= rekeyArray( 'employee_id', $arrmixEmployees );
		$objStdObjectContainer						= new stdClass();
		$objStdObjectContainer->m_arrobjEmployees	= $arrmixEmployees;

		if( true == valArr( $arrmixEmployees ) ) {
			self::fetchRecursiveTeamsByManagerIds( array_keys( $arrmixEmployees ), $intHrRepresentativeEmployeeId, $objStdObjectContainer, $objDatabase );
		} else {
			return NULL;
		}

		$arrmixEmployees = $objStdObjectContainer->m_arrobjEmployees;

		return $arrmixEmployees;
	}

	public static function fetchRecursiveTeamsByManagerIds( $arrintTeamEmployeeIds, $intHrRepresentativeEmployeeId, $objStdObjectContainer, $objDatabase ) {
		if( false == valArr( $arrintTeamEmployeeIds ) ) {
			return $objStdObjectContainer;
		}

		$strSql = 'SELECT
						DISTINCT t.id,
						t.name,
						te.employee_id
					FROM
						teams t,
						team_employees te
					WHERE
						t.id = te.team_id
						AND t.manager_employee_id IN (
														SELECT
															e.id
														FROM
															employees e
															JOIN employees e1 ON ( e1.reporting_manager_id = e.id )
															LEFT JOIN team_employees te ON ( te.employee_id = e1.id )
															left join teams t on (t.id = te.team_id)
														WHERE
															te.is_primary_team = 1
															AND e.id IN ( ' . implode( ',', $arrintTeamEmployeeIds ) . ' )
															AND e.employee_status_type_id = 1
															AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
															AND ( e.date_terminated IS NULL
															OR e.date_terminated > NOW ( ) )
						)
						AND te.is_primary_team = 1
						AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
					ORDER BY
						 t.name ASC';

		$arrmixEmployees = fetchData( $strSql, $objDatabase );
		$arrmixEmployees = rekeyArray( 'employee_id', $arrmixEmployees );
		if( true == valArr( $arrmixEmployees ) ) {
			$objStdObjectContainer->m_arrobjEmployees = array_merge( $objStdObjectContainer->m_arrobjEmployees, $arrmixEmployees );
			self::fetchRecursiveTeamsByManagerIds( array_keys( $arrmixEmployees ), $intHrRepresentativeEmployeeId, $objStdObjectContainer, $objDatabase );
		} else {
			return $objStdObjectContainer->m_arrobjEmployees;
		}
	}

	public static function fetchAllTPMByHrRepresentativeId( $intHrRepresentativeEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT e.id,
						e.preferred_name
					FROM
						employees e
						JOIN employees e1 ON ( e.id = e1.reporting_manager_id )
						JOIN team_employees te ON ( te.employee_id = e1.id AND te.is_primary_team = 1 )
						JOIN teams t ON ( te.team_id = t.id AND t.deleted_on IS NULL )
						JOIN users u ON ( e.id = u.employee_id )
						JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ug.group_id = ' . CGroup::PHP_TPM . '
						AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
						AND e.office_id IN ( ' . COffice::TOWER_8 . ',' . COffice::TOWER_9 . ' )
					ORDER BY
						e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentAndPreviousEmployees( $objDatabase ) {
		$strSql = 'SELECT
						e.id,
						e.preferred_name,
						e.department_id,
						e.employee_status_type_id,
						e.base_salary_encrypted
					FROM
						employees e
						LEFT JOIN users u ON ( e.id = u.employee_id )
					WHERE
						( e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ' , ' . CEmployeeStatusType::PREVIOUS . ' )
						OR u.id IN ( ' . CUser::ID_HELP_DESK . ' ) )
					ORDER BY
						e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentPreviousAndSystemEmployees( $objDatabase ) {
		$strSql = 'SELECT
						e.id,
						e.preferred_name,
						e.department_id,
						e.employee_status_type_id,
						e.base_salary_encrypted
					FROM
						employees e
						LEFT JOIN users u ON ( e.id = u.employee_id )
					WHERE
						e.employee_status_type_id IN ( ' . implode( ',', CEmployeeStatusType::$c_arrintEmployeeStatusTypeIds ) . ' )
					ORDER BY
						e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchTerminatedEmployeesCountByDateRangeByDepartmentIdByCountryCode( $strStartDate, $strEndDate, $strCountryCode, $intDepartmentId, $objDatabase ) {
		if( false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT DATE_TRUNC ( \'month\', e.date_terminated )::date AS date_terminated,
						COUNT ( e.id ) OVER ( PARTITION BY date_trunc ( \'month\', e.date_terminated ) ) AS monthly_terminated_employees_count
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\' )
					WHERE
						DATE_TRUNC ( \'month\', e.date_terminated ) BETWEEN \'' . addslashes( $strStartDate ) . '\' AND \'' . addslashes( $strEndDate ) . '\'
						AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ')';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= ' AND e.department_id = ' . ( int ) $intDepartmentId;
		}

		$strSql .= ' ORDER BY
						DATE_TRUNC( \'month\', e.date_terminated )::date ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesCountByDateRangeByDepartmentIdByCountryCode( $strStartDate, $strEndDate, $strCountryCode, $intDepartmentId, $objDatabase ) {
		if( false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT DATE_TRUNC( \'month\', e.date_started ) ::date AS date_started,
						COUNT ( e.id ) OVER ( PARTITION BY date_trunc( \'month\', e.date_started ) ) AS monthly_joined_employees_count
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . addslashes( $strCountryCode ) . '\' )
					WHERE
						DATE_TRUNC ( \'month\', e.date_started ) BETWEEN \'' . addslashes( $strStartDate ) . '\'
						AND \'' . addslashes( $strEndDate ) . '\'
						AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ')';

		if( true == is_numeric( $intDepartmentId ) ) {
			$strSql .= ' AND e.department_id = ' . ( int ) $intDepartmentId;
		}

		$strSql .= ' ORDER BY
						DATE_TRUNC( \'month\', e.date_started )::date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeesSeperationDetailsByDateByCountryCodeByFilter( $intPageNo, $intPageSize, $strOrderByFieldName, $strOrderType, $strStartDate, $strEndDate, $strCountryCode, $strFilter, $objDatabase, $boolIsDownload = false, $intHrManager = NULL ) {
		if( false == valStr( $strFilter ) ) {
			return 0;
		}

		$strHrManagerCondition = ( false == is_null( $intHrManager ) ) ? ' AND t.hr_representative_employee_id = ' . ( int ) $intHrManager : '';

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = $intPageSize;

		if( true == $boolIsDownload ) {
			$strSubSql = '';
		} else {
			if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
				$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			}
		}

		if( 'preferred_name' == $strOrderByFieldName ) {
			$strOrderByFieldName = 'e.preferred_name';
		} else {
			if( 'designation' == $strOrderByFieldName ) {
				$strOrderByFieldName = 'd.name';
			} else {
				if( 'expected_termination_date' == $strOrderByFieldName ) {
					$strOrderByFieldName = 'to_char( e.expected_termination_date, \'MM/dd/yyyy\' )';
				}
			}
		}

		$strWhereSubSql = '';
		if( 'expected_termination_date' == $strFilter ) {
			$strWhereSubSql = ' AND ( e.expected_termination_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\' OR e.date_terminated BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\')';
		} else {
			$strWhereSubSql = ' AND e.resigned_on BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'';
		}

		$strSql = 'SELECT
						e.id,
						e.designation_id,
						d.name AS designation,
						ea.employee_application_status_type_id,
						e.preferred_name,
						e.date_started,
						e.resigned_on,
						e.employee_number,
						CASE
							WHEN ea.employee_application_status_type_id = ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_EXIT_PROCESS . ' THEN \'Standard\'
							WHEN ea.employee_application_status_type_id = ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_ABSCOND . ' THEN \'Immediate\'
							WHEN ea.employee_application_status_type_id = ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_ON_SITE . ' THEN \'Onsite\'
							ELSE \'Not Available\'
						END as exit_status,
						CASE WHEN e.employee_status_type_id IN ( ' . CEmployeeStatusType::PREVIOUS . ' ) THEN e.date_terminated ELSE e.expected_termination_date END AS expected_termination_date,
						d.id AS designation_number,
						ea1.country_code,
						t.hr_representative_employee_id,
						Extract( year from age( DATE ( CASE WHEN e.employee_status_type_id IN ( ' . CEmployeeStatusType::PREVIOUS . ' ) THEN e.date_terminated ELSE e.expected_termination_date END ) , DATE ( e.date_started ) ) ) AS with_us_year,
						Extract(month from age( DATE ( CASE WHEN e.employee_status_type_id IN ( ' . CEmployeeStatusType::PREVIOUS . ' ) THEN e.date_terminated ELSE e.expected_termination_date END ) , DATE ( e.date_started ) ) ) AS with_us_month,
						CASE
							WHEN ea1.country_code = \'' . CCountry::CODE_INDIA . '\' THEN coalesce ( d.notice_period, 2 )
						ELSE coalesce ( d.notice_period, 14 )
						END AS notice_period
					FROM
						employees e
						JOIN designations d ON ( e.designation_id = d.id )
						JOIN employee_applications ea ON ( ea.employee_id = e.id )
						JOIN employee_addresses ea1 ON ( ea1.employee_id = e.id AND ea1.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN team_employees te on ( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN teams t ON ( te.team_id = t.id )
						JOIN employee_application_status_types east ON ( east.id = ea.employee_application_status_type_id )
					 WHERE
						( ea.employee_application_status_type_id = ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_ABSCOND . ' OR ea.employee_application_status_type_id = ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_EXIT_PROCESS . ' )
						AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ',' . CEmployeeStatusType::RESTRICTED . ' )
						' . $strHrManagerCondition . '
						AND ea1.country_code = \'' . $strCountryCode . '\'
						' . $strWhereSubSql . '
					ORDER BY
						' . $strOrderByFieldName . ' ' . $strOrderType . '' . $strSubSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeesSeperationCountByDateByCountryCodeByFilter( $strStartDate, $strEndDate, $strCountryCode, $strFilter, $objDatabase, $intHrManager = NULL ) {
		if( false == valStr( $strFilter ) ) {
			return 0;
		}

		$strHrManagerCondition = ( false == is_null( $intHrManager ) ) ? ' AND t.hr_representative_employee_id = ' . ( int ) $intHrManager : '';

		$strWhereSubSql = '';
		if( 'expected_termination_date' == $strFilter ) {
			$strWhereSubSql = ' AND ( e.expected_termination_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\' OR e.date_terminated BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\')';
		} else {
			$strWhereSubSql = ' AND e.resigned_on BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'';
		}

		$strSql = ' SELECT
						count(e.id)
					FROM
						employees e
						JOIN designations d ON ( e.designation_id = d.id )
						JOIN employee_applications ea ON ( ea.employee_id = e.id )
						JOIN employee_addresses ea1 ON ( ea1.employee_id = e.id AND ea1.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN team_employees te on ( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN teams t ON ( te.team_id = t.id )
					 WHERE
						( ea.employee_application_status_type_id = ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_ABSCOND . ' OR ea.employee_application_status_type_id = ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_EXIT_PROCESS . ' OR e.employee_status_type_id = ' . CEmployeeStatusType::PREVIOUS . ' )
						AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ',' . CEmployeeStatusType::RESTRICTED . ' )
						' . $strHrManagerCondition . '
						AND ea1.country_code = \'' . $strCountryCode . '\'
						' . $strWhereSubSql . '';

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchAllDepartmentsEmployeesCountByContryCode( $strCountryCode, $objDatabase ) {
		$strSql = '	SELECT
						count(e.id),
						d.name
					FROM
						employees e JOIN departments d ON ( e.department_id = d.id )
						JOIN employee_addresses ea ON( ea.employee_id = e.id )
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND ea.address_type_id = ' . CAddressType::PRIMARY;

		if( true == valStr( $strCountryCode ) && CCountry::CODE_USA == $strCountryCode ) {
			$strSql .= 'AND ea.country_code = \'' . CCountry::CODE_USA . '\'';
		} else {
			$strSql .= 'AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'';
		}

		$strSql .= 'GROUP BY d.name ORDER BY d.name';

		return $arrstrEmployeesDepartments = fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesCountByDesignationsByContryCode( $strCountryCode, $arrintDepartmentsIds = NULL, $objDatabase ) {
		$strSql = 'SELECT
						count(e.id),
						d.name
					FROM
						employees e,
						designations d,
						employee_addresses ea
					WHERE
						e.designation_id = d.id
						AND e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
						AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\'
						AND e.id = ea.employee_id ';

		if( true == valArr( $arrintDepartmentsIds ) ) {
			$strSql .= 'AND d.department_id IN ( ' . implode( ',', $arrintDepartmentsIds ) . ' ) ';
		}

		if( true == valStr( $strCountryCode ) && CCountry::CODE_USA == $strCountryCode ) {
			$strSql .= 'AND ea.country_code = \'' . CCountry::CODE_USA . '\'';
		} else {
			$strSql .= 'AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'';
		}

		$strSql .= 'GROUP BY d.name
					ORDER BY d.name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesCountByDateByContryCode( $strStartDate, $strEndDate, $strCountryCode, $objDatabase ) {
		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						date( DATE_TRUNC( \'month\', e.date_started ) ),
						count( e.id )
					FROM
						employees e JOIN employee_addresses ea ON( ea.employee_id = e.id )
					WHERE
						e.date_started IS NOT NULL
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND ( DATE_TRUNC( \'day\', e.date_started ) >\'' . $strStartDate . '\'
						AND DATE_TRUNC( \'day\', e.date_started ) <\'' . $strEndDate . '\' )
						AND ea.address_type_id = ' . CAddressType::PRIMARY;

		if( true == valStr( $strCountryCode ) && CCountry::CODE_USA == $strCountryCode ) {
			$strSql .= ' AND ea.country_code = \'' . CCountry::CODE_USA . '\'';
		} else {
			$strSql .= ' AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'';
		}

		$strSql .= ' GROUP BY date( DATE_TRUNC( \'month\', e.date_started ) )
						ORDER BY date( DATE_TRUNC( \'month\', e.date_started ) )';

		return $arrmixCurrentEmployeesData = fetchData( $strSql, $objDatabase );
	}

	public static function fetchPastEmployeesCountByDateByContryCode( $strStartDate, $strEndDate, $strCountryCode, $objDatabase ) {
		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}

		$strPastEmployeesSql = 'SELECT
									date( DATE_TRUNC( \'month\', e.date_terminated ) ),
									count( e.id )
								FROM
									employees e JOIN employee_addresses ea ON( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
								WHERE
									e.date_terminated IS NOT NULL
									AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
									AND ea.address_type_id = ' . CAddressType::PRIMARY;
		if( true == valStr( $strCountryCode ) && CCountry::CODE_USA == $strCountryCode ) {
			$strPastEmployeesSql .= ' AND ea.country_code = \'' . CCountry::CODE_USA . '\'';
		} else {
			$strPastEmployeesSql .= ' AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'';
		}

		$strPastEmployeesSql .= ' AND ( DATE_TRUNC( \'month\', e.date_terminated ) >=\'' . $strStartDate . '\'
								 AND DATE_TRUNC( \'month\', e.date_terminated ) <=\'' . $strEndDate . '\' )
								 GROUP BY date( DATE_TRUNC( \'month\', e.date_terminated ) )
								 ORDER BY date( DATE_TRUNC( \'month\', e.date_terminated ) )';

		return $arrmixPastEmployees = fetchData( $strPastEmployeesSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByTeamId( $intTeamId, $intManagerId, $objDatabase ) {
		$strSql = 'SELECT
						data.*,
						e.id AS manager_employee_id,
						e.name_full as manager_employee_name
					FROM
						(
						SELECT
							DISTINCT ( e.* ),
							e.id AS employee_id,
							te.is_primary_team,
							d.name AS designation_name,
							dept.name AS department_name
						FROM
							employees AS e
							JOIN team_employees AS te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
							LEFT JOIN designations d ON d.id = e.designation_id
							LEFT JOIN departments dept ON dept.id = e.department_id
						WHERE
							te.team_id = ' . ( int ) $intTeamId . '
							AND e.reporting_manager_id = ' . ( int ) $intManagerId . '
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND e.date_terminated IS NULL
						ORDER BY
							e.name_first
						) AS data
						LEFT JOIN employees e1 ON ( e1.id = data.employee_id )
						LEFT JOIN employees e ON ( e1.reporting_manager_id = e.id )
					ORDER BY
						data.department_name';

		return self::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchAllEmployeesPolicyAcknowledgement( $objSecurityPolicyAcknowledgementFilter, $objDatabase, $boolIsDownload = false ) {
		if( 0 < $objSecurityPolicyAcknowledgementFilter->getPageNo() ) {
			$intOffset = ( 0 < $objSecurityPolicyAcknowledgementFilter->getPageNo() ) ? $objSecurityPolicyAcknowledgementFilter->getPageSize() * ( $objSecurityPolicyAcknowledgementFilter->getPageNo() - 1 ) : 0;
			$intLimit  = ( int ) $objSecurityPolicyAcknowledgementFilter->getPageSize();
		}

		$strWhereCondition = '';

		if( true == valStr( $objSecurityPolicyAcknowledgementFilter->getCountryCode() ) ) {
			$strWhereCondition .= ' AND ea.country_code= \'' . $objSecurityPolicyAcknowledgementFilter->getCountryCode() . '\' ';
		}

		if( true == is_numeric( $objSecurityPolicyAcknowledgementFilter->getDepartment() ) ) {
			$strWhereCondition .= ' AND d.id = ' . $objSecurityPolicyAcknowledgementFilter->getDepartment();
		}

		if( true == $objSecurityPolicyAcknowledgementFilter->getIsPolicyStatus() ) {

			if( true == $objSecurityPolicyAcknowledgementFilter->getIsTerminated() ) {
				$strWhereCondition .= ' AND e.date_terminated >= \'3/6/2014\'';
			} else {
				$strWhereCondition .= ' AND e.date_terminated IS NULL';
				$strWhereCondition .= ' AND esp.year >= \'' . date( 'Y' ) . '\' AND esp.year <= \'' . date( 'Y' ) . '\'';
			}

			$strWhereCondition .= ' AND esp.security_policy_signed_on IS NOT NULL';

		} else {

			if( true == $objSecurityPolicyAcknowledgementFilter->getIsTerminated() ) {
				$strWhereCondition .= ' AND e.date_terminated >= \'3/6/2014\'';
			} else {
				$strWhereCondition .= ' AND e.date_terminated IS NULL';
			}

			$strWhereCondition .= ' AND esp.security_policy_signed_on IS NULL';
		}

		if( true == valStr( $objSecurityPolicyAcknowledgementFilter->getFromDate() ) && ( true == valStr( $objSecurityPolicyAcknowledgementFilter->getToDate() ) ) ) {

			$strWhereCondition .= '	AND e.date_started BETWEEN \'' . $objSecurityPolicyAcknowledgementFilter->getFromDate() . '\' AND \'' . $objSecurityPolicyAcknowledgementFilter->getToDate() . '\'';
		} else {
			if( true == valStr( $objSecurityPolicyAcknowledgementFilter->getFromDate() ) && ( false == valStr( $objSecurityPolicyAcknowledgementFilter->getToDate() ) ) ) {

				$strWhereCondition .= ' AND e.date_started >= \'' . $objSecurityPolicyAcknowledgementFilter->getFromDate() . '\'';
			} else {
				if( false == valStr( $objSecurityPolicyAcknowledgementFilter->getFromDate() ) && ( true == valStr( $objSecurityPolicyAcknowledgementFilter->getToDate() ) ) ) {

					$strWhereCondition .= ' AND e.date_started <= \'' . $objSecurityPolicyAcknowledgementFilter->getToDate() . '\'';
				}
			}
		}

		if( false == is_null( $objSecurityPolicyAcknowledgementFilter->getOrderByField() ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $objSecurityPolicyAcknowledgementFilter->getOrderByField() );

			if( false == is_null( $objSecurityPolicyAcknowledgementFilter->getOrderByType() ) ) {
				$strOrderBy .= ' ' . addslashes( $objSecurityPolicyAcknowledgementFilter->getOrderByType() );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {

			$strOrderBy = ' ORDER BY id ASC';
		}

		$strSql = 'SELECT	DISTINCT e.id,
							e.name_full,
							ea.country_code,
							esp.security_policy_signed_on as signed_on,
							esp.security_policy_downloaded_on as downloaded_on,
							e.email_address,
							t.name as team,
							d.name as department_name,
							e.date_started as date_started,
							e.date_terminated as date_terminated,
							CASE
								WHEN NOW() >= DATE ( \'' . '07-01-' . date( 'Y' ) . '\'' . ') AND esp.security_policy_signed_on IS NULL THEN ( now ( ) ::date - DATE ( \'' . '07-01-' . date( 'Y' ) . '\'' . ')::date )
								WHEN NOW() < DATE ( \'' . '07-01-' . date( 'Y' ) . '\'' . ') AND esp.security_policy_signed_on IS NULL THEN ( now ( ) ::date - DATE ( \'' . '01-01-' . date( 'Y' ) . '\'' . ')::date )
							END AS difference
					FROM 	employees e
							LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
							LEFT JOIN teams t ON ( t.id = te.team_id )
							LEFT JOIN departments d ON ( e.department_id = d.id )
							LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id )
							LEFT JOIN employee_security_policies esp ON ( esp.employee_id = e.id )
					WHERE 	ea.country_code IS NOT NULL
							AND e.date_started IS NOT NULL
							AND te.team_id <> ' . CTeam::ADMIN_2 . '
							AND e.id NOT IN ( 972, 1852, 1161 )
							AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ')' . $strWhereCondition . $strOrderBy;

		if( 0 < $objSecurityPolicyAcknowledgementFilter->getPageNo() && false == $boolIsDownload ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$arrstrEmployeeData = fetchData( $strSql, $objDatabase );

		return $arrstrEmployeeData;
	}

	public static function fetchAllEmployeesPolicyAcknowledgementCount( $objSecurityPolicyAcknowledgementFilter, $objDatabase ) {
		$strWhereCondition = '';
		if( true == valStr( $objSecurityPolicyAcknowledgementFilter->getCountryCode() ) ) {
			$strWhereCondition .= ' AND ea.country_code= \'' . $objSecurityPolicyAcknowledgementFilter->getCountryCode() . '\'';
		}

		if( true == is_numeric( $objSecurityPolicyAcknowledgementFilter->getDepartment() ) ) {
			$strWhereCondition .= ' AND d.id = ' . $objSecurityPolicyAcknowledgementFilter->getDepartment();
		}

		if( true == $objSecurityPolicyAcknowledgementFilter->getIsPolicyStatus() ) {

			if( true == $objSecurityPolicyAcknowledgementFilter->getIsTerminated() ) {
				$strWhereCondition .= ' AND e.date_terminated >= \'3/6/2014\'';
			} else {
				$strWhereCondition .= ' AND e.date_terminated IS NULL';
				$strWhereCondition .= ' AND esp.year >= \'' . date( 'Y' ) . '\' AND esp.year <= \'' . date( 'Y' ) . '\'';
			}

			$strWhereCondition .= ' AND esp.security_policy_signed_on IS NOT NULL';

		} else {
			if( true == $objSecurityPolicyAcknowledgementFilter->getIsTerminated() ) {
				$strWhereCondition .= ' AND e.date_terminated >= \'3/6/2014\'';
			} else {
				$strWhereCondition .= ' AND e.date_terminated IS NULL';
			}

			$strWhereCondition .= ' AND esp.security_policy_signed_on IS NULL';
		}

		if( true == valStr( $objSecurityPolicyAcknowledgementFilter->getFromDate() ) && ( true == valStr( $objSecurityPolicyAcknowledgementFilter->getToDate() ) ) ) {

			$strWhereCondition .= '	AND e.date_started BETWEEN \'' . $objSecurityPolicyAcknowledgementFilter->getFromDate() . '\' AND \'' . $objSecurityPolicyAcknowledgementFilter->getToDate() . '\'';
		} else {
			if( true == valStr( $objSecurityPolicyAcknowledgementFilter->getFromDate() ) && ( false == valStr( $objSecurityPolicyAcknowledgementFilter->getToDate() ) ) ) {

				$strWhereCondition .= ' AND e.date_started BETWEEN \'' . $objSecurityPolicyAcknowledgementFilter->getFromDate() . '\' AND NOW()::date';
			} else {
				if( false == valStr( $objSecurityPolicyAcknowledgementFilter->getFromDate() ) && ( true == valStr( $objSecurityPolicyAcknowledgementFilter->getToDate() ) ) ) {

					$strWhereCondition .= ' AND e.date_started BETWEEN \'02-06-2014\'::date AND \'' . $objSecurityPolicyAcknowledgementFilter->getToDate() . '\'';
				}
			}
		}

		$strSql = 'SELECT
						count ( * )
					FROM
						employees e
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						LEFT JOIN teams t ON ( t.id = te.team_id )
						LEFT JOIN employee_security_policies esp ON ( esp.employee_id = e.id )
						LEFT JOIN departments d ON ( e.department_id = d.id )
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						d.is_published = 1 AND e.id NOT IN ( ' . CEmployee::ID_RAJENDRA_GAIKWAD . ' ) AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ')' . $strWhereCondition;

		$arrstrEmployeesCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrstrEmployeesCount[0]['count'] ) ) ? ( int ) $arrstrEmployeesCount[0]['count'] : 0;
	}

	public static function fetchTotalEmployeesCountsByMonthAndYearWiseByCountryCode( $strCountryCode, $strStartDate, $strEndDate, $objAdminDatabase, $arrintDepartmentIds = [] ) {
		$strJoinCondition = '';

		if( true == valArr( $arrintDepartmentIds ) ) {
			$strJoinCondition = ' AND e.department_id IN (' . implode( ',', $arrintDepartmentIds ) . ')';
		}

		$strSql = 'SELECT
							begining_of_month,
						count ( * ) AS employees_count
					FROM
						(
						SELECT
							gs::date AS begining_of_month,
							( gs + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) ::date AS end_of_month
						FROM
							generate_series ( \'' . $strStartDate . '\'::date,\'' . $strEndDate . '\'::date, \'1 month\' ) gs
						) AS ts
						JOIN employees e ON ( e.date_started <= ts.end_of_month AND ( e.date_terminated > ts.end_of_month OR e.date_terminated IS NULL ) AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ',' . CEmployeeStatusType::RESTRICTED . ' )' . $strJoinCondition . ' )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.country_code = \'' . $strCountryCode . '\' AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					GROUP BY
						begining_of_month
					ORDER BY begining_of_month';

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchEmployeesByDateStarted( $strDateStarted, $objDatabase ) {
		if( false == valStr( $strDateStarted ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*
					FROM
						employees e
					JOIN
						employee_addresses ea ON ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . '
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.date_started::date = \'' . $strDateStarted . '\' AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchUnAssignedGradersByTestIdByManagerEmployeeId( $intTestId, $objDatabase ) {
		$strSql = 'SELECT
						e.id, e.name_first, e.name_last
					FROM
						employees e
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id NOT IN (
										SELECT
											DISTINCT e.id
										FROM
											employees e
										JOIN test_graders tg ON e.id = tg.employee_id
										WHERE
											tg.test_id=' . ( int ) $intTestId . ' )
					ORDER BY e.name_first';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentAllEmployees( $objDatabase, $arrintEmployeeIds = NULL, $strUserCountryCode = '' ) {
		$strWhereCondition = ( true == valArr( $arrintEmployeeIds ) ) ? ( '' != $strUserCountryCode ) ? ( CCountry::CODE_USA == $strUserCountryCode ) ? ' AND e.payroll_remote_primary_key IN ( \'' . implode( "','", $arrintEmployeeIds ) . '\' )' : ' AND e.employee_number IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )' : ' AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )' : '';

		$strSql = 'SELECT
						e.*,
						ea.country_code
					FROM
						employees e, employee_addresses ea
					WHERE
						ea.address_type_id = ' . CAddressType::PRIMARY . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id = ea.employee_id
						AND date_terminated IS NULL
						' . $strWhereCondition . '
					ORDER BY
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesCountByDateRangeByCountryCodeByDepartmentId( $strStartDate, $strEndDate, $strCountryCode, $objAdminDatabase, $intDepartmentId = NULL ) {
		if( false == valStr( $strCountryCode ) || false == valStr( $strStartDate ) ) {
			return NULL;
		}

		$strJoinCondition = NULL;

		if( true == is_numeric( $intDepartmentId ) ) {
			$strJoinCondition = ' AND e.department_id = \'' . $intDepartmentId . '\'';
		}

		$strSql = 'SELECT
						begining_of_month,
						count ( * ) AS employees_count
					FROM
						(
						SELECT
							gs::date AS begining_of_month,
							( gs + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) ::date AS end_of_month
							FROM
							generate_series ( \'' . $strStartDate . '\' ::date, \'' . $strEndDate . '\' ::date, \'1 month\' ) gs
						) AS ts
						JOIN employees e ON ( e.date_started <= ts.end_of_month AND ( e.date_terminated > ts.end_of_month OR e.date_terminated IS NULL ) AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ')' . $strJoinCondition . ' )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.country_code = \'' . $strCountryCode . '\' AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					GROUP BY
						begining_of_month
					ORDER BY
						begining_of_month';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeesByReviewType( $objDatabase, $intEmployeeId = NULL, $arrstrBulkReviewPagination = [], $intDepartmentId = NULL, $strCountryCode = NULL, $strSearchEmployees = NULL, $arrintEmployeeDesignationIds = NULL, $arrintBulkReviewsFilter = NULL ) {
		$strPagination    = '';
		$strWhereClause   = '';
		$strJoinCondition = '';

		if( true == valArr( $arrstrBulkReviewPagination ) ) {
			$intOffset     = ( true == isset( $arrstrBulkReviewPagination['page_no'] ) && true == isset( $arrstrBulkReviewPagination['page_size'] ) ) ? $arrstrBulkReviewPagination['page_size'] * ( $arrstrBulkReviewPagination['page_no'] - 1 ) : 0;
			$intLimit      = ( int ) $arrstrBulkReviewPagination['page_size'];
			$strPagination = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit . ' ';
		}

		if( true == is_numeric( $intDepartmentId ) ) {
			$strWhereClause .= ' AND e.department_id = ' . ( int ) $intDepartmentId . ' ';
		} else {
			$strWhereClause .= ' AND e.department_id NOT IN ( ' . CDepartment::CALL_CENTER . ',' . CDepartment::SALES . ',' . CDepartment::SALES_DIALER . ',' . CDepartment::SALES_ENGINEER . ',' . CDepartment::SALES_ADMINS . ',' . CDepartment::SALES_FARMERS . ' ) ';
		}

		if( false == is_null( $strCountryCode ) ) {
			$strWhereClause .= ( false !== strpos($strCountryCode, ',') ) ? ' AND ea1.country_code IN (\'' . join("','",explode( ",", $strCountryCode ) ) . '\')' : ( ( true == valStr( $strCountryCode ) ) ? ' AND ea1.country_code = \'' . addslashes( $strCountryCode ) . '\' ' : '' );
		}

		if( true == valStr( $strSearchEmployees ) ) {
			$strWhereClause .= ' AND e.name_full ILIKE \'%' . $strSearchEmployees . '%\' ';
		}

		if( true == is_numeric( $intEmployeeId ) ) {
			$strWhereClause = ' AND e.id = ' . ( int ) $intEmployeeId . ' ';
		}

		if( true == valArr( $arrintEmployeeDesignationIds ) ) {
			$strWhereClause .= ' AND e.designation_id IN ( ' . implode( ', ', $arrintEmployeeDesignationIds ) . ' )';
		}

		if( true == valArr( $arrintBulkReviewsFilter['hr_representatives'] ) && ( $arrintBulkReviewsFilter['hr_representatives'][0] != 'undefined' && true == valStr( trim( $arrintBulkReviewsFilter['hr_representatives'][0] ) ) ) ) {
			$strWhereClause .= ' AND t.hr_representative_employee_id IN ( ' . implode( ', ', $arrintBulkReviewsFilter['hr_representatives'] ) . ' )';
		}

		$strWhereClause .= ( true == valStr( $arrintBulkReviewsFilter['review_cycle'] ) && true == valStr( $arrintBulkReviewsFilter['year'] ) && 'undefined' != $arrintBulkReviewsFilter['review_cycle'] ) ? ( ( true == valStr( $strWhereClause ) ) ? ' AND ' : '' ) . ' ep.value = \'' . $arrintBulkReviewsFilter['review_cycle'] . '\'' : '';

		if( true == valStr( $arrintBulkReviewsFilter['review_cycle'] ) && true == valStr( $arrintBulkReviewsFilter['year'] ) && 'undefined' != $arrintBulkReviewsFilter['review_cycle'] ) {

			$strYear = ( true == valStr( $arrintBulkReviewsFilter['year'] ) ) ? $arrintBulkReviewsFilter['year'] : NULL;

			$strWhereClause   .= 'AND ep.key = \'EMPLOYEE_REVIEW_CYCLE\'';
			$strJoinCondition = 'JOIN employee_preferences ep ON ( e.id = ep.employee_id )
								LEFT JOIN employee_preferences ep1 ON ( e.id = ep1.employee_id AND ep1.key = \'EMPLOYEE_REVIEW_CYCLE_YEAR\' )';

			$strYearCondition = ( $strYear > date( 'Y' ) && date( 'm' ) <= 11 ) ? '' : ' OR ep1.value IS NULL';
			$strWhereClause .= ' AND ( ep1.value = ' . $strYear . '::text ' . $strYearCondition . ' )';
		}

		$strSql = 'SELECT
						DISTINCT e.*,
						e1.preferred_name AS manager_employee_name,
						e1.id AS manager_employee_id,
						e2.preferred_name as hr_manager_name
					FROM employees e
						JOIN employee_addresses ea1 ON ( ea1.employee_id = e.id AND ea1.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN teams t ON ( t.id = te.team_id )
						LEFT JOIN employees e2 ON ( e2.id = t.hr_representative_employee_id )
						JOIN employees e1 ON ( e1.id = e.reporting_manager_id ) ' . $strJoinCondition . '
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND
						e.date_terminated IS NULL AND
						e.id NOT IN (
									SELECT
										DISTINCT ea.employee_id
									FROM
										employee_assessments ea
									WHERE ea.employee_assessment_type_id NOT IN ( ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ',' . CEmployeeAssessmentType::MANAGER_REQUEST_EMPLOYEE_TERMINATE . ' )
										AND ea.completed_on IS NULL
										AND ea.deleted_on IS NULL
									)
						 ' . $strWhereClause . '
					ORDER BY
						 e.name_full
					' . $strPagination . '';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeeDetailsByDesignationIdOrByDepartmentIdOrByOfficeId( $strWhere, $objDatabase ) {
		$strSql = 'SELECT
						e.id,
						e.name_full,
						e.email_address
					FROM
						employees e, employee_addresses ea
					WHERE ' . $strWhere . '
						AND ea.address_type_id = ' . CAddressType::PRIMARY . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id = ea.employee_id
						AND date_terminated IS NULL ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchEmployeeDetailsByGroupIds( $arrintGroupIds, $objDatabase ) {
		$strSql = 'SELECT
						e.id,
						e.name_full,
						e.email_address,
						e.preferred_name,
						u.id as user_id
					FROM
						user_groups ug
						JOIN users as u ON ug.user_id = u.id
						JOIN employees as e ON u.employee_id = e.id
						LEFT JOIN departments as dept ON e.department_id = dept.id
						LEFT JOIN designations as d ON e.designation_id = d.id
					WHERE
						ug.deleted_by IS NULL
						AND ug.group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					GROUP BY
						e.id,u.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssociatedEmployeesByTeamId( $arrintTeamIds, $objDatabase, $boolIsFromTest = false ) {

		$strWhere = '';
		if( true == $boolIsFromTest ) {
			$strWhere = ' AND te.is_primary_team = 1 ';
		}
		$strSql = '	SELECT
						e.id,
						e.name_full,
						e.email_address
					FROM
						employees as e
						JOIN team_employees as te ON ( e.id = te.employee_id )
						LEFT JOIN designations d ON d.id = e.designation_id
						LEFT JOIN departments dept ON dept.id = e.department_id
					WHERE
						te.team_id IN ( ' . implode( ',', $arrintTeamIds ) . ' ) '
				  . $strWhere . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesWithManagerDetailsByIds( $arrintEmployeeIds, $objDatabase, $boolReturnArray = false ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$boolReturnArrayOfObject = ( 1 == \Psi\Libraries\UtilFunctions\count( $arrintEmployeeIds ) && false == $boolReturnArray ) ? false : true;

		$strSql = 'SELECT
						e.*,
						d.name as designation_name,
						d1.name as department_name,
						e1.id AS manager_employee_id,
						e1.email_address as manager_email_address,
						e1.preferred_name AS manager_employee_name
					FROM
						employees e
						JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
						JOIN designations d ON ( d.id = e.designation_id )
						JOIN departments d1 ON ( d1.id = e.department_id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		if( false == $boolReturnArrayOfObject ) {
			return self::fetchEmployee( $strSql, $objDatabase );
		} else {
			return self::fetchEmployees( $strSql, $objDatabase );
		}
	}

	public static function fetchAllCurrentEmployees( $strCountryCode = NULL, $intUserId = NULL, $objAdminDatabase ) {

		$strCountrySortOrder = ( 'US' == $strCountryCode ) ? 'DESC' : 'ASC';

		$strUserIdCondition = ( false == is_null( $intUserId ) ) ? ' OR us.id = ' . ( int ) $intUserId : '';

		$strSql = 'SELECT
						DISTINCT
						us.id AS user_id,
						em.id AS employee_id,
						em.is_project_manager,
						em.preferred_name,
						CASE
							WHEN te.add_employee_id IS NOT NULL AND em.id = te.add_employee_id THEN 2
							WHEN te.qam_employee_id IS NOT NULL AND em.id = te.qam_employee_id THEN 3
						ELSE NULL
						END AS add_qam,
						lower( ea.country_code )
					FROM
						employees em
						JOIN users us ON (us.employee_id = em.id)
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN teams te ON ( te.add_employee_id = em.id OR te.qam_employee_id = em.id )
					WHERE
						( us.is_disabled <> 1 ' . $strUserIdCondition . ' )
						AND us.employee_id = em.id
						AND em.id NOT IN ( 1, 2 )
						AND em.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ', ' . CEmployeeStatusType::SYSTEM . ' )
					ORDER BY
						lower( ea.country_code ) ' . $strCountrySortOrder . ',
						em.preferred_name ';

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchEmployeesWithUserIdByDepartmentIdsOrderByNameFull( $arrintDepartmentIds, $objDatabase ) {
		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						 e.*,u.id as user_id
					FROM
						employees e
						LEFT JOIN users u ON ( e.id = u.employee_id )
					WHERE
						e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) AND e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						ORDER BY e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesCountWithDepartmentsByCountryCode( $strCountryCode, $strDate, $objAdminDatabase ) {
		$strSql = 'SELECT
						COUNT ( e.id ),
						d.name
					FROM
						employees e
						JOIN departments d ON ( e.department_id = d.id )
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . $strCountryCode . '\' )
					WHERE
						( e.date_terminated IS NULL
						OR e.date_terminated > ( \'' . $strDate . '\'::date + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) ::date )
						AND ( \'' . $strDate . '\'::date + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) ::date >= e.date_started
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					GROUP BY
						d.id,
						d.name
					ORDER BY
						d.name';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchPaginatedActiveEmployeesByDesignationIdByCountryCode( $intDesignationId, $intPageNo, $intPageSize, $objDatabase ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT
						DISTINCT e.id,
						 t.name AS team_name,
						 e.birth_date,
						 e.email_address,
						 e.preferred_name AS employee_name
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id )
						LEFT OUTER JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT OUTER JOIN employees e1 ON ( e.reporting_manager_id = e1.id )
						LEFT OUTER JOIN teams t ON ( t.id = te.team_id )
					WHERE
						e.name_last IS NOT NULL
						AND e.designation_id = ' . ( int ) $intDesignationId . '
						AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
					ORDER BY employee_name OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		$arrmixEmployeeDetails = fetchData( $strSql, $objDatabase );

		$arrstrEmployeeNames = [];

		if( true == valArr( $arrmixEmployeeDetails ) ) {
			foreach( $arrmixEmployeeDetails as $intKey => $arrmixEmployeeDetail ) {
				$arrstrEmployeeNames[$arrmixEmployeeDetail['id']]['employee_name'] = $arrmixEmployeeDetail['employee_name'];
				$arrstrEmployeeNames[$arrmixEmployeeDetail['id']]['team_name']     = $arrmixEmployeeDetail['team_name'];
				$arrstrEmployeeNames[$arrmixEmployeeDetail['id']]['birth_date']    = $arrmixEmployeeDetail['birth_date'];
				$arrstrEmployeeNames[$arrmixEmployeeDetail['id']]['email_address'] = $arrmixEmployeeDetail['email_address'];
			}
		}

		return $arrstrEmployeeNames;
	}

	public static function fetchPaginatedEmployeesExperienceByDeparmentIds( $intPageNo, $intPageSize, $strOrderByField = NULL, $strOrderByType = NULL, $boolIsDownload = false, $arrstrEmployeesExperienceReport, $arrintDepartmentIds, $objAdminDatabase ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strWhere      = '';
		$strExperience = '';

		if( false == empty( $arrstrEmployeesExperienceReport['department'] ) ) {
			if( true == valArr( $arrstrEmployeesExperienceReport['department'] ) ) {
				$strWhere = ' e.department_id IN ( ' . implode( ',', $arrstrEmployeesExperienceReport['department'] ) . ' )';
			} else {
				$strWhere = ' e.department_id IN ( ' . $arrstrEmployeesExperienceReport['department'] . ' )';
			}
		} else {
			$strWhere = ' e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )';
		}

		if( false == empty( $arrstrEmployeesExperienceReport['name'] ) ) {
			$strWhere .= ' AND ( e.name_full ILIKE \'%' . trim( $arrstrEmployeesExperienceReport['name'] ) . '%\' OR e_tl.name_full ILIKE \'%' . trim( $arrstrEmployeesExperienceReport['name'] ) . '%\' )';
		}

		if( true == isset( $arrstrEmployeesExperienceReport['xento_experience'] ) && '' != $arrstrEmployeesExperienceReport['xento_experience'] ) {
			if( false == valArr( $arrstrEmployeesExperienceReport['xento_experience'] ) ) {
				$arrstrEmployeesExperienceReport['xento_experience'] = explode( ',', $arrstrEmployeesExperienceReport['xento_experience'] );
			}

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrEmployeesExperienceReport['xento_experience'] ) && true == in_array( 5, $arrstrEmployeesExperienceReport['xento_experience'] ) ) {
				// More than five year xento_experience only
				$strExperience = ' AND extract ( YEAR FROM age ( e.date_started ) ) >=' . $arrstrEmployeesExperienceReport['xento_experience'][0];
			} else {
				if( 1 <= \Psi\Libraries\UtilFunctions\count( $arrstrEmployeesExperienceReport['xento_experience'] ) && true == in_array( 5, $arrstrEmployeesExperienceReport['xento_experience'] ) ) {
					// Five & other
					$intKey   = array_search( 5, $arrstrEmployeesExperienceReport['xento_experience'] );
					$intValue = $arrstrEmployeesExperienceReport['xento_experience'][$intKey];
					unset( $arrstrEmployeesExperienceReport['xento_experience'][$intKey] );
					$strExperience = ' AND ( extract ( YEAR FROM age( e.date_started ) ) IN ( ' . implode( ',', $arrstrEmployeesExperienceReport['xento_experience'] ) . ' ) OR extract ( YEAR FROM age ( e.date_started ) ) >= ' . ( int ) $intValue . ' )';

				} else {
					if( 1 <= \Psi\Libraries\UtilFunctions\count( $arrstrEmployeesExperienceReport['xento_experience'] ) && false == in_array( 5, $arrstrEmployeesExperienceReport['xento_experience'] ) ) {
						// All other not five
						$strExperience = ' AND extract ( YEAR FROM age( e.date_started ) ) IN ( ' . implode( ',', $arrstrEmployeesExperienceReport['xento_experience'] ) . ' )';
					}
				}
			}
		}

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY e.date_started ASC';
		}

		$strSql = 'SELECT
						e.name_full AS name,
						e.id as employee_id,
						d.name AS department,
						dg.name AS designation,
						CASE WHEN ( ( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) )* 12 + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) ) > 12 )
						THEN CONCAT( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) , \'.\', extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) , \' years\')
						ELSE CONCAT( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) , \'.\', extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) , \' year\')
						END AS xento_experience ,
						e.email_address AS email,
						e_tl.name_full AS team_lead,
						e_tl.id AS team_lead_employee_id,
						e_tl.email_address AS team_lead_email
					FROM
						employees e
						JOIN employees e_tl ON ( e.reporting_manager_id = e_tl.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN users u ON u.employee_id = e.id
						JOIN departments d ON ( e.department_id = d.id )
						JOIN designations dg ON ( e.designation_id = dg.id )
					WHERE
						' . $strWhere . '
						AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
					  	AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.date_confirmed IS NOT NULL ' . $strExperience;

		$strSql .= $strOrderBy;

		if( false == $boolIsDownload ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;
		}

		$arrmixEmployeesExperience = fetchData( $strSql, $objAdminDatabase );

		return $arrmixEmployeesExperience;
	}

	public static function fetchPaginatedEmployeesExperienceCountByDeparmentIds( $arrstrEmployeesExperienceReport, $arrintDepartmentIds, $objAdminDatabase ) {
		$strWhere      = '';
		$strExperience = '';

		if( false == empty( $arrstrEmployeesExperienceReport['department'] ) ) {

			if( true == valArr( $arrstrEmployeesExperienceReport['department'] ) ) {
				$strWhere = ' e.department_id IN ( ' . implode( ',', $arrstrEmployeesExperienceReport['department'] ) . ' )';
			} else {
				$strWhere = ' e.department_id IN ( ' . $arrstrEmployeesExperienceReport['department'] . ' )';
			}
		} else {
			$strWhere = ' e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )';
		}

		if( false == empty( $arrstrEmployeesExperienceReport['name'] ) ) {
			$strWhere .= ' AND ( e.name_full ILIKE \'%' . trim( $arrstrEmployeesExperienceReport['name'] ) . '%\' OR e_tl.name_full ILIKE \'%' . trim( $arrstrEmployeesExperienceReport['name'] ) . '%\' )';
		}

		if( true == isset( $arrstrEmployeesExperienceReport['xento_experience'] ) && '' != $arrstrEmployeesExperienceReport['xento_experience'] ) {

			if( false == valArr( $arrstrEmployeesExperienceReport['xento_experience'] ) ) {
				$arrstrEmployeesExperienceReport['xento_experience'] = explode( ',', $arrstrEmployeesExperienceReport['xento_experience'] );
			}

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrEmployeesExperienceReport['xento_experience'] ) && true == in_array( 5, $arrstrEmployeesExperienceReport['xento_experience'] ) ) {
				// More than five year xento_experience only
				$strExperience = ' AND extract ( YEAR FROM age ( e.date_started ) ) >=' . $arrstrEmployeesExperienceReport['xento_experience'][0];
			} else {
				if( 1 <= \Psi\Libraries\UtilFunctions\count( $arrstrEmployeesExperienceReport['xento_experience'] ) && true == in_array( 5, $arrstrEmployeesExperienceReport['xento_experience'] ) ) {
					// Five & other
					$intKey   = array_search( 5, $arrstrEmployeesExperienceReport['xento_experience'] );
					$intValue = $arrstrEmployeesExperienceReport['xento_experience'][$intKey];
					unset( $arrstrEmployeesExperienceReport['xento_experience'][$intKey] );
					$strExperience = ' AND ( extract ( YEAR FROM age ( e.date_started ) ) IN ( ' . implode( ',', $arrstrEmployeesExperienceReport['xento_experience'] ) . ' ) OR extract ( YEAR FROM age ( e.date_started ) ) >= ' . ( int ) $intValue . ' )';

				} else {
					if( 1 <= \Psi\Libraries\UtilFunctions\count( $arrstrEmployeesExperienceReport['xento_experience'] ) && false == in_array( 5, $arrstrEmployeesExperienceReport['xento_experience'] ) ) {
						// All other not five
						$strExperience = ' AND extract ( YEAR FROM age ( e.date_started ) ) IN ( ' . implode( ',', $arrstrEmployeesExperienceReport['xento_experience'] ) . ' )';
					}
				}
			}
		}

		$strSql = 'SELECT
						count ( e.id ) AS count
					FROM
						employees e
						JOIN employees e_tl ON ( e.reporting_manager_id = e_tl.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						' . $strWhere . '
						AND ea.country_code = \'IN\'
						AND ( e.date_terminated IS NULL
						OR e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						AND e.date_confirmed IS NOT NULL' . $strExperience;

		$arrintEmployeeCount = fetchData( $strSql, $objAdminDatabase );

		if( true == valArr( $arrintEmployeeCount ) ) {
			return $arrintEmployeeCount[0]['count'];
		} else {
			return NULL;
		}
	}

	public static function fetchSalesEmployeeByStateCode( $strStateCode, $objAdminDatabase ) {
		$strSql = ' SELECT
						e.id
					FROM
						employees e,
						employee_sales_areas esa,
						sales_area_locations sal,
						sales_areas sa,
						departments d
					WHERE
						esa.employee_id = e.id
						AND ( esa.sales_area_id = sa.id
						AND sal.sales_area_id = sa.id )
						AND employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )
						AND d.id IN ( ' . implode( ',', CDepartment::$c_arrintRandomEmployeeDepartmentIds ) . ' )
						AND e.department_id = d.id
						AND sal.state_code = \'' . $strStateCode . '\'
					ORDER BY
						random ( )
					LIMIT
						1';

		$arrmixSalesEmployees = fetchData( $strSql, $objAdminDatabase );
		if( true == valArr( $arrmixSalesEmployees ) ) {
			return $arrmixSalesEmployees[0]['id'];
		} else {
			return NULL;
		}
	}

	public static function fetchRandomSalesEmployeeId( $objAdminDatabase ) {
		$strSql = 'SELECT
						id
					FROM
						employees e
					WHERE
						e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintRandomEmployeeDepartmentIds ) . ' )
						AND employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL
						OR date_terminated > NOW ( ) )
					ORDER BY
						random ( )
					LIMIT
						1';

		$arrintSalesEmployeeIds = fetchData( $strSql, $objAdminDatabase );
		if( true == valArr( $arrintSalesEmployeeIds ) ) {
			return $arrintSalesEmployeeIds[0]['id'];
		} else {
			return NULL;
		}
	}

	public static function fetchActiveEmployeeDetailsByTeamIdByCountryCode( $intTeamId, $strCountryCode, $objDatabase ) {
		if( true == is_null( $intTeamId ) || false == is_numeric( $intTeamId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						e.preferred_name,
						e.email_address,
						epn.phone_number,
						e.department_id AS department_id,
						to_char( min( eh.begin_datetime ), \'HH12:MI:SS\' ) AS employee_entry_time
					FROM
						team_employees te
						JOIN employees e ON ( te.employee_id = e.id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						LEFT JOIN employee_addresses ea ON ( e.reporting_manager_id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
						LEFT JOIN employee_hours eh ON ( e.id = eh.employee_id AND date_trunc ( \'day\', eh.begin_datetime ) = date_trunc ( \'day\', NOW() ) )
					WHERE
						te.is_primary_team = 1
						AND ea.country_code = \'' . $strCountryCode . '\'
						AND te.team_id = ' . ( int ) $intTeamId . '
					GROUP BY
						e.id,
						e.preferred_name,
						e.email_address,
						epn.phone_number,
						e.department_id
					ORDER BY
						e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCurrentAndPreviousEmployees( $objDatabase ) {
		$strSql = 'SELECT
						e.*,
						ea.country_code
						FROM
						employees e
						JOIN employee_addresses ea ON ea.employee_id = e.id
					WHERE
						e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ' , ' . CEmployeeStatusType::PREVIOUS . ' )
						AND ea.country_code	IS NOT NULL
						AND ea.address_type_id = ' . CAddressType::PRIMARY . '
					ORDER BY
						e.name_full ASC ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesAnalysisReportDataByUserIds( $arrintUserIds, $objDatabase ) {
		$strSql = 'SELECT
						u.id,
						e.id as employee_id,
						e.email_address as email_id,
						e.name_full,
						d.name as deparment,
						dg.name as designation
					FROM
						employees e
						LEFT JOIN departments d ON ( e.department_id = d.id )
						LEFT JOIN designations dg ON ( e.designation_id = dg.id )
						LEFT JOIN users u ON ( u.employee_id = e.id )
					WHERE
						u.id IN ( ' . implode( ',', $arrintUserIds ) . ' )
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY u.id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHrRepresentativeByUserIds( $arrintUserIds, $objDatabase ) {
		$strSql = 'WITH employee AS (
					SELECT
						u.employee_id,
						u.id as user_id
					FROM
						users u
					WHERE
						u.id IN ( ' . implode( ',', $arrintUserIds ) . ' )
					)

					SELECT
						e.name_full,
						e1.user_id
					FROM
						team_employees te
						LEFT JOIN teams t ON (t.id = te.team_id)
						JOIN employees e ON (e.id = t.hr_representative_employee_id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN employee e1 ON (te.employee_id = e1.employee_id )
					WHERE
						te.is_primary_team = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchManagerEmployeeDetailsByTeamId( $intTeamId, $objDatabase ) {
		if( true == is_null( $intTeamId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*,
						e.preferred_name AS manager_employee_name,
						t.name AS team_name
					FROM
						employees e
						LEFT JOIN teams t ON ( t.manager_employee_id = e.id )
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND t.id = ' . ( int ) $intTeamId;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeesData( $boolShowSeverityCount = false, $objDatabase ) {
		$strTaskTypeCondition = '';

		if( true == $boolShowSeverityCount ) {
			$strWhereCondition = ' WHERE ( t.task_status_id IS NOT NULL AND t.task_status_id != ' . CTaskStatus::CANCELLED . ' )
										AND ( td.severity_count IS NOT NULL AND td.severity_count > 0 )
										AND t.updated_on >= DATE ( NOW ( ) - INTERVAL \'3 Months\' )
										AND t.task_type_id = ' . CTaskType::BUG . ' ';

			$strSql = ' SELECT sum ( td.severity_count ) AS severity_count ';
		} else {
			$strSql = ' SELECT
							sum ( td.developer_story_points ) AS dev_story_points';

			$strTaskTypeCondition = ' AND ( t.completed_on >= DATE ( NOW ( ) - INTERVAL \'3 Months\' ) ) AND t.task_type_id IN ( ' . CTaskType::BUG . ', ' . CTaskType::FEATURE . ' ) ';

			$strWhereCondition = ' WHERE ( t.task_status_id IS NOT NULL AND t.task_status_id = ' . CTaskStatus::COMPLETED . ' )
										AND ( td.developer_story_points > 0 )';
		}

		$strSql .= ' FROM
						tasks t
						LEFT JOIN task_details td ON ( t.id IS NOT NULL AND t.id = td.task_id ' . $strTaskTypeCondition . '
													AND ( td.developer_employee_id IS NOT NULL ) )
						LEFT JOIN employees e_dev ON ( e_dev.id = td.developer_employee_id AND e_dev.date_terminated IS NULL AND e_dev.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND e_dev.department_id IN ( ' . CDepartment::DEVELOPMENT . ', ' . CDepartment::CREATIVE . ' ) )
						' . $strWhereCondition . ' ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssignedEmployeesByTestId( $intTestId, $objDatabase, $boolIsFromEmployeesTab = false ) {
		if( false == is_numeric( $intTestId ) ) {
			return NULL;
		}
		$strWhereCondition = '';
		$strSubSelect      = '';
		$strSubJoin        = '';

		if( true == $boolIsFromEmployeesTab ) {
			$strWhereCondition = 'NOT';
		}

		if( true == in_array( $intTestId, CTest::$c_arrintPhpCertificationTests ) || CTest::ID_XENTO_MANUAL_TEST == $intTestId ) {
			$strSubJoin = ' LEFT JOIN (
									SELECT
										ts.test_id,
										ts.employee_id,
										ts.score
									FROM
										test_submissions ts
									WHERE
										ts.test_id = ' . ( int ) $intTestId . ' ) as score_data ON ( score_data.employee_id = e.id )';

			$strSubSelect = ' , CASE
									WHEN
										score_data.score LIKE \'%/%\'
									THEN
										( ( CAST ( split_part( score_data.score, \'/\', 1 ) as FLOAT ) ) * 100 ) / ( CAST ( split_part ( score_data.score, \'/\', 2 ) as FLOAT ) )
								END as test_score ';
		}

		$strSql = ' SELECT
						DISTINCT e.id,
						e.name_first,
						e.name_last,
						e.email_address,
						e.department_id ' . $strSubSelect . '
					FROM
						employees e
						LEFT JOIN employee_tests et ON ( et.employee_id = e.id ) ' . $strSubJoin . '
					WHERE
						et.employee_id ' . $strWhereCondition . ' IN (
													SELECT
														e.id
													FROM
														employees e
														JOIN users u ON ( u.employee_id = e.id )
														LEFT JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
														LEFT JOIN team_employees te ON ( te.employee_id = e.id )
														LEFT JOIN employee_training_sessions ets ON ( ets.user_id = u.id and ets.group_id = ug.group_id )
														LEFT JOIN test_groups tg1 ON ( tg1.department_id = e.department_id OR e.designation_id = tg1.designation_id OR e.office_id = tg1.office_id OR tg1.team_id = te.team_id OR tg1.group_id = ug.group_id OR tg1.training_session_id = ets.training_session_id )
													WHERE
														tg1.test_id = ' . ( int ) $intTestId . '
												)
						AND et.test_id = ' . ( int ) $intTestId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY
						e.name_first';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchUnAssignedEmployeesByTestIdByEmployeeIds( $intTestId, $objDatabase, $arrintEmployeeIds = NULL ) {

		$strSubSql = '';

		if( true == valArr( $arrintEmployeeIds ) ) {
			$strSubSql = ' AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';
		}

		$strSql = ' SELECT
						e.id,
						e.name_first,
						e.name_last,
						e.email_address,
						e.name_full
					FROM
						employees e
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.id NOT IN (
									SELECT
										DISTINCT employee_id
									FROM
										employee_tests
									WHERE
											test_id = ' . ( int ) $intTestId . '
								 ) ' . $strSubSql . '
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchTestAssignedEmployeeIdsByTestIds( $arrintTestIds, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT t.*,
						e.id
					FROM
						employees e
						LEFT JOIN users u ON ( u.employee_id = e.id )
						LEFT JOIN employee_tests et ON ( et.employee_id = e.id )
						LEFT JOIN team_employees te ON ( te.employee_id = e.id )
						LEFT JOIN user_groups ug ON ( ug.user_id = u.id AND ug.deleted_by IS NULL )
						LEFT JOIN test_groups tg ON ( tg.designation_id = e.designation_id OR tg.department_id = e.department_id OR tg.office_id = e.office_id OR te.team_id = tg.team_id OR ug.group_id = tg.group_id)
						LEFT JOIN tests t ON ( t.id = tg.test_id OR t.id = et.test_id )
					WHERE
						( t.id IN ( ' . implode( ',', $arrintTestIds ) . ' ) )
						AND employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL
						OR date_terminated > NOW ( ) )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchSimpleEmployeesDetailsByFieldByIds( $strFieldName, $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}
		if( true == is_null( $strFieldName ) ) {
			return NULL;
		}

		$strFieldToBeFetched = ' e.' . $strFieldName;

		if( 'employee_name' == $strFieldName ) {
			$strFieldToBeFetched = ' e.preferred_name as employee_name ';
		}

		$strSql = '	SELECT e.id, ' . $strFieldToBeFetched . '
					FROM
						employees e
					WHERE 
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesDetailsByDesignationIdsOrderByNameFull( $arrintDesignationIds, $objDatabase ) {
		if( false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*,
						ea.country_code
					FROM
						employees e
						JOIN employee_addresses ea ON ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . '
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND  designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' )
					ORDER BY name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByDepartmentIdOrderByNameFirst( $intDepartmentId, $objDatabase ) {
		$strSql = ' SELECT
						e.*
					FROM
						employees e
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e.department_id = ' . ( int ) $intDepartmentId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ea.country_code = \'' . CCountry::CODE_USA . '\'
						AND date_terminated IS NULL
					ORDER BY name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesContactDetailsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT e.id,
						e.name_full,
						epn.phone_number,
						e.email_address,
						t.team_id,
						t.manager_employee_id
					FROM
						employees e
						LEFT JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id AND epn.phone_number_type_id =' . CPhoneNumberType::PRIMARY . ' )
						LEFT JOIN team_employees t ON (t.employee_id = e.id)
					WHERE
						employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )
						AND e.name_full IS NOT NULL
						AND e.email_address IS NOT NULL
						AND epn.phone_number IS NOT NULL
						AND e.email_address LIKE \'%xento.com\'
						AND t.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
					ORDER BY
						e.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesContactDetailsByTeamIds( $arrintTeamIds, $objDatabase ) {
		if( false == valArr( $arrintTeamIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT e.id,
						e.name_full,
						epn.phone_number,
						e.email_address,
						t.team_id,
						t.manager_employee_id
					FROM
						employees e
						LEFT JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id AND epn.phone_number_type_id =' . CPhoneNumberType::PRIMARY . ' )
						LEFT JOIN team_employees t ON (t.employee_id = e.id)
					WHERE
						employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( date_terminated IS NULL OR date_terminated > NOW() )
						AND e.name_full IS NOT NULL
						AND e.email_address IS NOT NULL
						AND epn.phone_number IS NOT NULL
						AND e.email_address LIKE \'%xento.com\'
						AND t.team_id IN ( ' . implode( ',', $arrintTeamIds ) . ' )
					ORDER BY
						e.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestAuthorityActiveEmployeeByIdByCountryCode( $strCountryCode, $objAdminDatabase ) {
		if( false == is_null( $strCountryCode ) ) {
			if( $strCountryCode == CCountry::CODE_INDIA ) {
				$strAppend = ' AND d.country_code = \'IN\' ';
			} else {
				$strAppend = ' AND d.country_code <> \'IN\' ';
			}
		} else {
			$strAppend = '';
		}

		$strSql = ' (
					SELECT
						DISTINCT ( e.* )
					FROM
						employees e
					JOIN users u ON e.id = u.employee_id
					JOIN user_roles ur ON ur.user_id = u.id
					JOIN roles r ON r.id = ur.role_id
					JOIN designations d ON e.designation_id = d.id
					JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.country_code = d.country_code AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						ur.deleted_by IS NULL
						AND	r.id = ' . CRole::APPROVES_PURCHASE_REQUESTS . '
						' . $strAppend . '
						AND e.date_terminated IS NULL
						ORDER BY e.name_first asc
					 )
					UNION ALL
					(
					SELECT
						 e.*
					FROM
						employees e
						JOIN users u ON e.id = u.employee_id
						JOIN designations d ON ( e.designation_id = d.id)
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.country_code = d.country_code AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						u.is_super_user = 1
						' . $strAppend . '
						AND e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						ORDER BY e.name_first asc
					 ) ';

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchSuperUsersByCountryCode( $strCountryCode = NULL, $objAdminDatabase ) {
		$strWhereClause = ( false == empty( $strCountryCode ) ) ? ' AND d.country_code = \'' . $strCountryCode . '\'' : '';

		$strSql = 'SELECT
						 e.*,
						 u.id As user_id
					FROM
						employees e
						JOIN users u ON e.id = u.employee_id
						JOIN designations d ON ( e.designation_id = d.id)
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.country_code = d.country_code AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						u.is_super_user = 1
						AND e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						' . $strWhereClause;

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeesByMemberEmployeeIds( $arrintMemberEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintMemberEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						em.manager_employee_id
					FROM
						employees e
						JOIN employee_managers em ON ( e.id = em.member_employee_id)
					WHERE
						em.is_reporting_manager = 1
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND em.member_employee_id IN ( ' . implode( ',', $arrintMemberEmployeeIds ) . ' )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByDepartmentId( $intDepartmentId, $objDatabase, $strOrderBy = NULL ) {

		$strSql = 'SELECT
						e.*,
						ea.country_code,
						e.reporting_manager_id AS manager_employee_id,
						des.name AS designation_name,
						dep.name AS department_name,
						epn.phone_number,
						CASE
							WHEN NOW() > e.date_terminated
							THEN 1
							ELSE 0
						END AS is_terminated
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id )
						LEFT JOIN designations AS des ON ( des.id = e.designation_id )
						LEFT JOIN departments AS dep ON ( dep.id = e.department_id )
						LEFT JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id AND epn.phone_number_type_id = 1 )
					WHERE
						e.department_id = ' . ( int ) $intDepartmentId . '
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY ' . ( ( true == is_null( $strOrderBy ) ) ? 'lower( e.name_last ), lower( e.name_first )' : 'e.' . $strOrderBy ) . ';';

		return self::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchEmployeesByDesignationIdByDepartmentIds( $arrintDeveloperQaDepartmentIds, $arrintDesignationIds = [], $objDatabase, $boolShowDisabledData = NULL, $strOrderBy = NULL ) {
		$strSubSql = ' ';
		if( true == valArr( $arrintDesignationIds ) ) {
			$strSubSql = ' OR e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ')';
			$strSubSql .= ( false == $boolShowDisabledData ) ? ' AND employee_status_type_id = ' . CEmployeeStatusType::CURRENT : ' ';
		}

		$strSql = 'SELECT
						e.*,
						ea.country_code,
						e.reporting_manager_id AS manager_employee_id,
						des.name AS designation_name,
						dep.name AS department_name,
						epn.phone_number,
						CASE
							WHEN NOW() > e.date_terminated
							THEN 1
							ELSE 0
						END AS is_terminated
						
						
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id )
						LEFT JOIN designations AS des ON ( des.id = e.designation_id )
						LEFT JOIN departments AS dep ON ( dep.id = e.department_id )
						LEFT JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id AND epn.phone_number_type_id = 1 )
					WHERE
						e.department_id IN ( ' . implode( ',', $arrintDeveloperQaDepartmentIds ) . ' ) ' . $strSubSql . '
					ORDER BY
						' . ( ( true == is_null( $strOrderBy ) ) ? 'lower( e.name_last ), lower( e.name_first )' : 'e.' . $strOrderBy ) . ', name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fecthEmployeesBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {

		$strSql = 'SELECT
						e.*,
						d.name as department_name
					FROM
						surveys s
						JOIN employees e ON e.id = s.responder_reference_id
						LEFT JOIN departments d ON e.department_id = d.id
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND s.survey_template_id = ' . ( int ) $intSurveyTemplateId;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesEmailAddressByPsProductIdOrByPsProductOptionId( $strReportName, $intPsProductId, $intPsProductOptionId, $objDatabase ) {

		$strWhereCondition  = '';
		$strJoinCondition   = '';
		$strBundleCondition = '';

		if( 0 != $intPsProductId ) {
			$strJoinCondition = 'JOIN ps_products pp ON( pp.id = ' . ( int ) $intPsProductId . ' ) ';
		}
		if( 0 != $intPsProductOptionId ) {
			$strJoinCondition = 'JOIN ps_product_options pp ON( pp.id = ' . ( int ) $intPsProductOptionId . ' ) ';
		}

		$strUnionClause = '';

		switch( $strReportName ) {

			case 'product_management_report':
				$strWhereCondition = ' WHERE
										e.date_terminated IS NULL
										AND ( e.id = pp.product_manager_employee_id
										OR e.id = pp.product_owner_employee_id
										OR e.id = pp.business_analyst_employee_id
										OR e.id = pp.ux_employee_id
										OR e.id = pp.executive_employee_id )
										' . $strBundleCondition;
				break;

			case 'product_development_report':
				$arrintPsProductEmployeeTypeIds = [ CPsProductEmployeeType::ASSOCAITE_DIRECTOR_DEVELOPMENT ];

				$strUnionClauseCondition = '';
				if( 0 == $intPsProductOptionId ) {
					$strUnionClauseCondition = ' AND ppe.ps_product_option_id IS NULL';
				}

				if( 0 != $intPsProductId ) {
					$strUnionClauseJoinCondition = ' JOIN ps_product_employees ppe ON ( ppe.ps_product_id = pp.id )';
				}
				if( 0 != $intPsProductOptionId ) {
					$strUnionClauseJoinCondition = ' JOIN ps_product_employees ppe ON ( ppe.ps_product_option_id = pp.id ) ';
				}

				$strWhereCondition = ' WHERE
										e.date_terminated IS NULL
										AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
										AND ( e.id = pp.sdm_employee_id
										OR e.id = pp.architect_employee_id
										OR e.id = pp.tpm_employee_id
										OR e.id = pp.tl_employee_id
										OR e.id = pp.doe_employee_id )
										' . $strBundleCondition;

				$strUnionClause = ' UNION
										( SELECT
											e.email_address,
											e.name_full,
											pp.name AS product_name
										FROM
											employees e '
								  . $strJoinCondition . $strUnionClauseJoinCondition . '
										WHERE e.date_terminated IS NULL
											AND e.id = ppe.employee_id ' .
								  $strUnionClauseCondition . '
											AND ppe.ps_product_employee_type_id IN ( ' . implode( ',', $arrintPsProductEmployeeTypeIds ) . ' )
											' . $strBundleCondition . ' )';
				break;

			case 'product_services_report':
				$strWhereCondition = ' WHERE
										e.date_terminated IS NULL
										AND ( e.id = pp.implementation_employee_id
										OR e.id = pp.sales_employee_id
										OR e.id = pp.support_employee_id
										OR e.id = pp.tech_writer_employee_id )
										' . $strBundleCondition;
				break;

			case 'product_qa_report':
				$arrintPsProductEmployeeTypeIds = [ CPsProductEmployeeType::QA_TEAM_LEAD ];

				$strUnionClauseCondition = '';
				if( 0 == $intPsProductOptionId ) {
					$strUnionClauseCondition = ' AND ppe.ps_product_option_id IS NULL';
				}

				if( 0 != $intPsProductId ) {
					$strUnionClauseJoinCondition = ' JOIN ps_product_employees ppe ON ( ppe.ps_product_id = pp.id )';
				}
				if( 0 != $intPsProductOptionId ) {
					$strUnionClauseJoinCondition = ' JOIN ps_product_employees ppe ON ( ppe.ps_product_option_id = pp.id ) ';
				}

				$strWhereCondition = ' WHERE
									e.date_terminated IS NULL
									AND ( e.id = pp.qam_employee_id
									OR e.id = pp.qa_employee_id
									OR e.id = pp.sqm_employee_id
									OR e.id = pp.sme_employee_id)
									' . $strBundleCondition;

				$strUnionClause = ' UNION
									( SELECT
											e.email_address,
											e.name_full,
											pp.name AS product_name
										FROM
											employees e '
								  . $strJoinCondition . $strUnionClauseJoinCondition . '
										WHERE e.date_terminated IS NULL
											AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
											AND e.id = ppe.employee_id ' .
								  $strUnionClauseCondition . '
											AND ppe.ps_product_employee_type_id IN ( ' . implode( ',', $arrintPsProductEmployeeTypeIds ) . ' )
											' . $strBundleCondition . ' )';
				break;

			default:
				$strWhereCondition = '';
				break;
		}

		$strSql = 'SELECT
						e.email_address,
						e.name_full,
						pp.name AS product_name
					FROM
						employees e '
				  . $strJoinCondition . $strWhereCondition . $strUnionClause;

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesBySearchName( $strSearchTearm = NULL, $objDatabase ) {

		$strWhereCondition = '';

		if( false == is_null( $strSearchTearm ) ) {
			$strWhereCondition = ' AND ( e.preferred_name ILIKE E\'%' . addslashes( trim( $strSearchTearm ) ) . '%\'
									OR e.email_address ILIKE E\'%' . addslashes( trim( $strSearchTearm ) ) . '%\' )';
		}

		$strSql = 'SELECT
						e.email_address,
						e.preferred_name
					FROM
						employees e
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						' . $strWhereCondition . '
					ORDER BY
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesByDesignationIdsOrByGroupIds( $arrintDesignationIds = NULL, $arrintGroupIds = NULL, $objDatabase ) {

		$strWhereCondition = '';
		if( false == valArr( $arrintDesignationIds ) && false == valArr( $arrintGroupIds ) ) {
			return NULL;
		} else {
			if( true == valArr( $arrintDesignationIds ) && true == valArr( $arrintGroupIds ) ) {
				$strWhereCondition = ' AND ( e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' )
										OR ug.group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' ) ) ';

			} else {
				if( true == valArr( $arrintDesignationIds ) ) {
					$strWhereCondition = ' AND e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' ) ';
				} else {
					if( true == valArr( $arrintGroupIds ) ) {
						$strWhereCondition = ' AND ug.group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' ) ';
					}
				}
			}
		}

		$strSql = ' SELECT
						e.id,
						e.name_first,
						e.name_full,
						e.preferred_name
						FROM
						employees e
						LEFT JOIN users u ON ( e.id = u.employee_id )
						LEFT JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
					WHERE
						e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL '
				  . $strWhereCondition . '
					ORDER BY e.name_first ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByCountryCodeForSpecificRoleIds( $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
							e.*,
							ea.country_code
						FROM
							employees e
							JOIN users u ON ( u.employee_id = e.id )
							JOIN user_roles ur ON ( u.id = ur.user_id AND ur.deleted_on IS NULL )
							JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						WHERE
							ur.role_id = ' . ( int ) CRole::IS_HR_DIRECTOR . '
							AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' )
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						GROUP BY
							ur.user_id,
							e.id,
							ea.country_code';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEmailAddressesByPsFilterId( $intPsEmployeeFilterId, $objDatabase, $intUserId = NULL, $boolReturnSql = false ) {

		$objPsFilter = CPsFilters::fetchPsFilterByIdByFilterTypeId( $intPsEmployeeFilterId, CPsFilterType::PS_EMPLOYEES, $objDatabase );

		if( false == valObj( $objPsFilter, 'CPsFilter' ) ) {
			return NULL;
		}

		$objPsEmployeeFilter = $objPsFilter->downCast( CPsFilterType::PS_EMPLOYEES );

		$objPsEmployeeFilter->setValues( unserialize( $objPsEmployeeFilter->getSerializedFilter() ) );

		$arrstrJoinsAndConditions = self::applyEmployeeSearchCriteria( $objPsEmployeeFilter );

		$strWhereCondition     = '';
		$strJoinClause         = '';
		$strWhereUserCondition = '';

		if( true == valArr( $arrstrJoinsAndConditions ) ) {
			if( true == valStr( $arrstrJoinsAndConditions['join'] ) ) {
				$strJoinClause = $arrstrJoinsAndConditions['join'];
			}

			if( true == valStr( $arrstrJoinsAndConditions['where'] ) ) {
				$strWhereCondition = $arrstrJoinsAndConditions['where'];
			}
		}
		if( true == is_numeric( $intUserId ) ) {
			$strWhereUserCondition = 'AND created_by = ' . ( int ) $intUserId;
		}

		$strJoinClause .= ( true == valStr( $objPsEmployeeFilter->getHrRepresentativeEmployeeIds() ) || true == valStr( $objPsEmployeeFilter->getTeamIds() ) || true == valStr( $objPsEmployeeFilter->getManagerEmployeeIds() ) ) ? ' LEFT JOIN team_employees te ON( te.employee_id = e.id AND te.is_primary_team = 1 ) LEFT JOIN teams t ON( t.id = te.team_id )' : '';

		$strSql = 'SELECT
						DISTINCT e.email_address
					FROM
						employees e
						' . $strJoinClause . '
					WHERE
						1 = 1
						' . $strWhereCondition . '
						' . $strWhereUserCondition . '
					ORDER BY
						e.email_address';

		if( false == $boolReturnSql ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return $strSql;
		}

	}

	public static function fetchActiveEmployeesByDesignationIdsByCountryCode( $arrintDesignationIds, $strCountryCode, $objDatabase ) {
		if( false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
						JOIN employee_addresses ea on( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						ea.country_code = \'' . $strCountryCode . '\'
						AND ( e.date_terminated IS NULL OR e.date_terminated >= NOW() ) AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' )
					ORDER BY
						e.name_first';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesPerformanceDetails( $arrstrEmployeeCompensationFilter, $objDatabase ) {
		$strWhereCondition = '';
		if( true == valArr( $arrstrEmployeeCompensationFilter['department_ids'] ) ) {
			$strWhereCondition .= ' AND d.id IN(' . implode( ', ', $arrstrEmployeeCompensationFilter['department_ids'] ) . ')';
		}

		$strStartDate = ( false == empty( $arrstrEmployeeCompensationFilter['start_date'] ) ) ? '\'' . $arrstrEmployeeCompensationFilter['start_date'] . '\'' : 'DATE( NOW() - INTERVAL \'6 months\' )';
		$strEndDate   = ( false == empty( $arrstrEmployeeCompensationFilter['end_date'] ) ) ? '\'' . $arrstrEmployeeCompensationFilter['end_date'] . '\'' : 'DATE( NOW() )';

		$strSql = ' SELECT
						e.id AS employee_id,
						e.employee_number As employee_number,
						e.name_full AS employee_name,
						e.email_address,
						d.name AS department_name,
						dg.name AS designation_name,
						e.date_started AS date_of_joining,
						( CURRENT_DATE - e.date_started ) / 30 AS xento_experience,
						MAX(es.assessment_datetime) AS last_reviewed_on,
						( ( COALESCE(ead.experience_year,0) * 12 ) + COALESCE(ead.experience_month,0) + ( ( CURRENT_DATE - e.date_started ) / 30 ) ) AS total_experience,
						e.reporting_manager_id AS manager_employee_id,
						t.hr_representative_employee_id AS hr_rep_employee_id,
						(SUM( evr.scheduled_vacation_hours )/8::float) AS unscheduled_leaves,
						(
						SELECT
								avg ( sub_query.sum )
							FROM
								(
									SELECT
									DISTINCT(eh.date),
										SUM ( eh.end_datetime - eh.begin_datetime ) OVER ( PARTITION BY eh.date, eh.employee_id ) AS sum
									FROM
										employee_hours eh
									WHERE
										eh.employee_id = e.id
										AND eh.employee_hour_type_id = ' . CEmployeeHourType::FLOOR . '
										AND eh.date BETWEEN ' . $strStartDate . ' AND ' . $strEndDate . '
									GROUP BY
										eh.employee_id,
										eh.end_datetime,
										eh.begin_datetime,
										eh.date
								) sub_query
						) AS on_floor_hours
					FROM
						employees e
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.country_code = \'' . CCountry::CODE_INDIA . '\' AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN departments d ON ( d.id = e.department_id )
						LEFT JOIN designations dg ON ( dg.id = e.designation_id )
						LEFT JOIN employee_applications eap ON( eap.employee_id = e.id )
						LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = eap.id )
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						LEFT JOIN teams t ON ( t.id = te.team_id )
						LEFT JOIN employee_vacation_requests evr ON ( evr.employee_id = e.id AND evr.vacation_type_id = ' . CVacationType::UNSCHEDULED . ' AND evr.begin_datetime BETWEEN ' . $strStartDate . ' AND ' . $strEndDate . ')
						LEFT JOIN employee_assessments es ON( es.employee_id = e.id AND es.assessment_datetime BETWEEN ' . $strStartDate . ' AND ' . $strEndDate . ')
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . $strWhereCondition . '
					GROUP BY
						e.id,
						d.name,
						dg.name,
						ead.experience_year,
						ead.experience_month,
						e.reporting_manager_id,
						t.hr_representative_employee_id
					ORDER BY
						d.name,e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByDepartmentIdsByCountryCode( $arrintDepartmentIds, $strCountryCode, $objDatabase, $intShowTerminated = 0 ) {

		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strWhereCondition = '';

		if( 1 == $intShowTerminated ) {
			$strWhereCondition = ' AND e.employee_status_type_id = ' . CEmployeeStatusType::PREVIOUS;
		} else {
			if( 0 == $intShowTerminated ) {
				$strWhereCondition = ' AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . ' AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )';
			}
		}

		$strSql = ' SELECT
						distinct( e.* ),
						des.name AS designation_name,
						dep.name AS department_name,
						epn.phone_number
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id )
						LEFT JOIN designations AS des ON ( des.id = e.designation_id )
						LEFT JOIN departments AS dep ON ( dep.id = e.department_id )
						LEFT JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
					WHERE
						e.name_last IS NOT NULL
						AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
						AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\' ) ' . $strWhereCondition . '
					ORDER BY
						e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchProjectManagerNames( $boolIsDoeList = false, $objDatabase, $boolFromBulkAssignTask = false ) {

		$strCondition = ' )';
		if( true == $boolIsDoeList ) {
			$strCondition = 'OR designation_id IN ( ' . implode( ',', CDesignation::$c_arrintDirectorsOfEngineeringDesignations ) . ' ) )';
		}

		if( true == $boolFromBulkAssignTask ) {
			$strEmployeeStatusWhereCondition = ',' . CEmployeeStatusType::PREVIOUS;
		}

		$strSql = ' SELECT
						e.id,
						e.preferred_name,
						u.id as user_id
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
					WHERE
						( e.is_project_manager = 1 '
				  . $strCondition . '
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ' ' . $strEmployeeStatusWhereCondition . ' )
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 *
	 * @param integer   $arrintManagerId
	 * @param CDatabase $objDatabase
	 * @return integer
	 */

	public static function fetchIndianEmployeesCountByManagerId( $arrintManagerId, $objDatabase ) {

		$strSql = 'SELECT
						count(u.username) as indian_employee_count
					FROM
						employees e
						JOIN employee_addresses ea ON ea.employee_id = e.id
						JOIN users u ON u.employee_id = ea.employee_id
					WHERE
						e.reporting_manager_id = ' . $arrintManagerId . '
						AND ea.address_type_id = ' . CAddressType::PRIMARY . '
						AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'';

		$arrmixIndianEmployeesCountData = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrmixIndianEmployeesCountData[0]['indian_employee_count'] ) ) ? ( int ) $arrmixIndianEmployeesCountData[0]['indian_employee_count'] : 0;
	}

	public static function fetchEmployeeWithEncryptionKeysByCountryCode( $strCountryCode, $objAdminDatabase ) {
		if( true == empty( $strCountryCode ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						e.*,
						ea.country_code
					FROM
						employees e
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id =' . CAddressType::PRIMARY . ' )
					WHERE
						e.public_key IS NOT NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ea.country_code IN ( \'' . $strCountryCode . '\' ) ';

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeesByTaskId( $intTaskId, $objDatabase ) {

		$strSql = '	SELECT
						u.id as user_id,
							e.id as employee_id,
						e.name_full as name_full,
						e.preferred_name as preferred_name
					FROM
						employees e
						JOIN users u ON ( u.employee_id = e.id )
						JOIN tasks t ON ( t.user_id = u.id OR t.created_by = u.id OR t.project_manager_id = e.id OR t.client_resolution_employee_id = e.id )
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND t.id =' . ( int ) $intTaskId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeeCountByDepartmentIdsByEmployeeId( $arrintDepartmentIds, $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						COUNT(e.id) as employee_count
					 FROM
						employees e
						JOIN users as u ON ( e.id = u.employee_id )
					WHERE
						e.name_last IS NOT NULL
						AND e.department_id In ( ' . implode( ',', $arrintDepartmentIds ) . ' )
						AND e.id = ' . ( int ) $intEmployeeId . '
						AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND u.is_disabled = 0';

		$arrmixEmployeeCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrmixEmployeeCount[0]['employee_count'] ) ) ? ( int ) $arrmixEmployeeCount[0]['employee_count'] : 0;
	}

	public static function fetchCurrentEmployeeCountByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						COUNT(e.id) as employee_count
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\' )
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
					WHERE
						e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL
						AND ea.country_code = \'' . CCountry::CODE_USA . '\'
						AND e.id = ' . ( int ) $intEmployeeId;

		$arrmixEmployeeCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrmixEmployeeCount[0]['employee_count'] ) ) ? ( int ) $arrmixEmployeeCount[0]['employee_count'] : 0;
	}

	public static function fetchEmployeesDetailsByRoleId( $intRoleId, $intPageNo, $intPageSize, $objDatabase, $boolIsCount = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		if( true == $boolIsCount ) {
			$strFields = 'COUNT( DISTINCT( e.id ) )';
		} else {
			$strFields = 'DISTINCT ON (e.id) e.id,
									e.name_full,
									e.email_address,
									ea.country_code,
									u.id as user_id,
									t.name as team_name,
									d.name as department_name,
									emp.name_full as manager_name,
									est.name as status';
		}

		$strSql = 'SELECT ' . $strFields . '
					FROM
						employees e
						JOIN users u ON( e.id = u.employee_id )
						JOIN user_roles ur ON( u.id = ur.user_id AND ur.deleted_on IS NULL AND ur.deleted_by IS NULL )
						JOIN employee_addresses ea ON( e.id = ea.employee_id )
						JOIN team_employees te ON( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT JOIN employees emp ON( e.reporting_manager_id = emp.id )
						JOIN teams t ON( te.team_id = t.id )
						JOIN departments d ON( d.id = e.department_id )
						LEFT JOIN employee_status_types est ON( est.id = e.employee_status_type_id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND ur.role_id =' . ( int ) $intRoleId;

		if( false == $boolIsCount && 0 != ( int ) $intPageSize ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;
		}

		if( true == $boolIsCount ) {
			$arrintCount = fetchData( $strSql, $objDatabase );

			return $arrintCount[0]['count'];
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchAllActiveEmployeesDetailsWithUserId( $objDatabase ) {
		$strSql = ' SELECT
						e.name_full,
						u.id as user_id
					FROM
						employees as e
						JOIN users as u ON ( e.id = u.employee_id )
					WHERE
						( e.date_terminated IS NULL	OR NOW() < e.date_terminated )
						AND u.is_disabled = 0
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeeNames( $objDatabase ) {
		return self::fetchEmployees( 'SELECT e.id, e.preferred_name FROM employees e WHERE employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND date_terminated IS NULL ORDER BY name_full', $objDatabase );
	}

	public static function fetchEmployeeNameByEmployeeVacationRequestIds( $arrintEmployeeVacationRequestIds, $objDatabase ) {

		$strSql = 'SELECT
						evr.id,
						e.preferred_name AS name_full
					FROM
						users u
						JOIN employee_vacation_requests evr ON ( CASE
																	WHEN evr.approved_by IS NULL THEN evr.denied_by = u.id
																	ELSE evr.approved_by = u.id
																END AND evr.id IN ( ' . implode( ',', $arrintEmployeeVacationRequestIds ) . ' ) )
						JOIN employees e ON ( u.employee_id = e.id )';

		$arrmixResults    = fetchData( $strSql, $objDatabase );
		$arrmixResultData = [];

		foreach( $arrmixResults as $arrmixResult ) {
			$arrmixResultData[$arrmixResult['id']] = $arrmixResult['name_full'];
		}

		return $arrmixResultData;
	}

	public static function fetchEmployeeNameByOfficeShiftEmployeesIds( $arrintOfficeShiftEmployeesIds, $objDatabase ) {

		if( false == valArr( $arrintOfficeShiftEmployeesIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ose.id,
						e.preferred_name AS name_full
					FROM
						users u
						JOIN office_shift_employees ose ON ( CASE
																	WHEN ose.approved_by IS NULL THEN ose.denied_by = u.id
																	ELSE ose.approved_by = u.id
																END AND ose.id IN ( ' . implode( ',', $arrintOfficeShiftEmployeesIds ) . ' ) )
						JOIN employees e ON ( u.employee_id = e.id )';

		$arrmixResults    = fetchData( $strSql, $objDatabase );
		$arrmixResultData = [];

		foreach( $arrmixResults as $arrmixResult ) {
			$arrmixResultData[$arrmixResult['id']] = $arrmixResult['name_full'];
		}

		return $arrmixResultData;
	}

	public static function fetchEmployeeByfullNameOrPreferredName( $strFullName, $objDatabase ) {
		$strSql = ' SELECT
						e.*,
						u.id AS user_id
					FROM
						employees e
						JOIN users u ON ( u.employee_id = e.id )
					WHERE
					    e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.name_full LIKE \'' . trim( addslashes( $strFullName ) ) . '\'' . '
						OR e.preferred_name LIKE \'' . trim( addslashes( $strFullName ) ) . '\'' . '
					ORDER BY
						e.employee_number
					LIMIT
						1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveAndCurrentEmployeesByDepartmentIds( $arrintDepartmentIds, $objDatabase, $strOrderBy = ' name_full', $boolIsFromDeliquency = false ) {
		$strWhere = '';

		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		if( true == $boolIsFromDeliquency ) {
			$strWhere = 'AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ' , ' . CEmployeeStatusType::NO_SHOW . ' ) AND e.date_terminated IS NULL ';
		}

		$strSql = 'SELECT
								e.*
							FROM
								employees e
								JOIN users as u ON ( e.id = u.employee_id )
							WHERE
								e.name_last IS NOT NULL
								AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
								' . $strWhere . '
							ORDER BY
								' . $strOrderBy;

		return self::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchPublicKeyByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
					e.id,
					e.preferred_name,
					e.public_key
				FROM
					employees e
				WHERE
					e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					AND e.id IN( \'' . addslashes( $intEmployeeId ) . '\' )	';

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchImplementationAndCsmManagerDetailByClientId( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						pld.implementation_employee_id,
						pld.support_employee_id,
						e.email_address
					FROM
						employees e
						JOIN ps_lead_details pld ON ( e.id = pld.implementation_employee_id OR e.id = pld.support_employee_id )
						JOIN clients c ON ( pld.cid = c.id )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND c.company_status_type_id <> ' . CCompanyStatusType::TERMINATED . '
						AND c.id = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesWithHrManager( $intEmployeeIds, $objAdminDatabase ) {

		if( false == valArr( $intEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						e.*,
						te.employee_id AS employee_id,
						e1.name_full AS hr_manager_name
					FROM
						employees e
						JOIN team_employees te on ( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN teams t ON ( te.team_id = t.id )
						LEFT JOIN employees e1 ON ( t.hr_representative_employee_id = e1.id AND t.hr_representative_employee_id IS NOT NULL AND e1.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ')
					WHERE
						e.id IN ( ' . implode( ',', $intEmployeeIds ) . ' )';

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchAllEmployeeNames( $objDatabase, $boolIsActive = true ) {
		$strSql = 'SELECT id, preferred_name, employee_status_type_id FROM employees';

		if( true == $boolIsActive ) {
			$strSql .= ' WHERE employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '	AND date_terminated IS NULL';
		}

		$strSql .= ' ORDER BY preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEmailAddressAndUserIdBySvnUserName( $strSvnUserName, $objDatabase ) {
		$strSql = 'SELECT u.id, e.email_address FROM employees e JOIN users u ON ( e.id = u.employee_id) WHERE  e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND u.svn_username = \'' . $strSvnUserName . '\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesDataByRoleId( $intRoleId, $objDatabase, $boolIsActiveEmployee = false ) {
		$strSql = 'SELECT
						u.id ,
						e.preferred_name
					FROM
						employees e,
						users u,
						user_roles ur
					WHERE
						e.id = u.employee_id
						AND u.id = ur.user_id
						AND ur.deleted_on IS NULL
						AND ur.deleted_by IS NULL
						AND ur.role_id = ' . ( int ) $intRoleId;

		if( true == $boolIsActiveEmployee ) {
			$strSql .= ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesDataByDepartmentId( $intDepartmentId, $objDatabase, $boolFromBulkAssignTask = false ) {
		$strEmployeeStatusWhereCondition = '';

		if( true == $boolFromBulkAssignTask ) {
			$strEmployeeStatusWhereCondition .= ',' . CEmployeeStatusType::PREVIOUS;
		}

		$strSql = ' SELECT
						e.id,
						e.preferred_name,
						u.id as user_id
					FROM
						employees e
						JOIN users u ON( u.employee_id = e.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e.department_id = ' . ( int ) $intDepartmentId . '
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ' ' . $strEmployeeStatusWhereCondition . ' )
					ORDER BY name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesDataByDepartmentIds( $arrintDepartmentIds, $objDatabase, $strCountryCode = NULL, $boolBulkEmployee = false ) {

		if( false == valArr( array_filter( $arrintDepartmentIds ) ) ) {
			return false;
		}

		$strEmployeeStatusWhereCondition = '';

		$strWhereCondition = ( false == is_null( $strCountryCode ) ) ? ' AND ea.country_code = \'' . $strCountryCode . '\'' : '';
		if( true == $boolBulkEmployee ) {
			$strEmployeeStatusWhereCondition .= ',' . CEmployeeStatusType::PREVIOUS;
		}

		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						e.id,
						e.preferred_name,
						u.id as user_id,
						e.department_id,
						ea.country_code
					FROM
						employees e
						JOIN users u ON( u.employee_id = e.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ' ' . $strEmployeeStatusWhereCondition . ' ) ' . $strWhereCondition . '
					ORDER BY
						e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeNamesByIds( $arrintEmployeeIds, $objDatabase ) {

		$strSql = ' SELECT
						id, name_full
					FROM
						employees
					WHERE
						employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllUserEmployees( $objDatabase ) {

		$strSql = 'SELECT
						e.id AS id,
						e.preferred_name,
						us.id AS user_id
					FROM
						employees e
						INNER JOIN users us ON( us.employee_id = e.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' 
					ORDER BY
						lower( ea.country_code ) ASC, e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByIdsByEmployeeStatusTypeId( $arrintEmployeeIds, $intEmployeeStatusTypeId, $objDatabase, $strOrderByClause = NULL ) {
		if( false == valArr( $arrintEmployeeIds ) || false == valId( $intEmployeeStatusTypeId ) ) {
			return NULL;
		}

		$strOrderByClause = ( false == empty( $strOrderByClause ) ) ? ' ORDER BY ' . $strOrderByClause : '';

		return self::fetchEmployees( 'SELECT * FROM employees WHERE id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND employee_status_type_id = ' . ( int ) $intEmployeeStatusTypeId . $strOrderByClause, $objDatabase );
	}

	public static function fetchEmployeesByEmployeeStatusTypeIdByDepartmentIds( $intEmployeeStatusTypeId, $arrintDepartmentIds, $objDatabase ) {
		if( false == valId( $intEmployeeStatusTypeId ) || false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		return self::fetchEmployees( 'SELECT * FROM employees WHERE department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) AND employee_status_type_id = ' . ( int ) $intEmployeeStatusTypeId, $objDatabase );
	}

	public static function fetchAllEmployeesByDepartmentIds( $arrintDepartmentIds, $objDatabase ) {
		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		return self::fetchEmployees( 'SELECT DISTINCT( e.id ), e.*,ea.country_code FROM employees e JOIN employee_addresses ea ON( e.id = ea.employee_id ) WHERE e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND e.department_id IN( ' . implode( ',', $arrintDepartmentIds ) . ' ) ', $objDatabase );
	}

	public static function fetchPsNotificationEmployeeIdsByGroupId( $intGroupId, $boolIsPublic, $objDatabase ) {

		$strWhereCondition = '';

		if( false == is_null( $intGroupId ) && false == $boolIsPublic ) {
			$strWhereCondition = 'AND ug.group_id = ' . ( int ) $intGroupId;
		}

		$strSql = 'SELECT
						DISTINCT(e.id)
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						JOIN users u ON ( e.id = u.employee_id )
						JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . $strWhereCondition . '
					ORDER BY
						e.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesEmailAddressesById( $strEmployeeIds, $objDatabase, $boolIsCustomerAssignmentDashboard = false, $boolIsNonTerminatedEmployees = false ) {

		if( false == valStr( $strEmployeeIds ) ) {
			return NULL;
		}

		$strCondition = '';
		if( true == $boolIsNonTerminatedEmployees ) {
			$strCondition = 'AND date_terminated is NULL';
		}

		$strSql = 'SELECT
						id,
						email_address,
						name_full
					FROM employees
					WHERE employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
							AND id IN ( ' . $strEmployeeIds . ' ) ' . $strCondition;

		$arrstrEmailAddresses = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrEmailAddresses ) && false == $boolIsCustomerAssignmentDashboard ) {
			$arrstrEmployeesEmailAddresses = rekeyArray( 'email_address', $arrstrEmailAddresses );

			return $arrstrEmployeesEmailAddresses;
		} else {
			return $arrstrEmailAddresses;
		}
	}

	public static function fetchEmployeesReportingManagerById( $intEmployeeId, $objDatabase ) {

		if( false == valId( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						e.id AS employee_id,
						e1.id AS reporting_manager_id,
						e.preferred_name AS name_full,
						e1.preferred_name AS reporting_manager,
						e1.email_address,
						e.email_address AS employee_email_address,
						ea.country_code as employee_country_code,
						d.name AS department_name
					FROM
						employees as e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN employees as e1 ON ( e1.id = e.reporting_manager_id AND e1.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' )
						JOIN departments d ON ( d.id = e.department_id )
					WHERE
						e.id = ' . ( int ) $intEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDetailsByCountryCodeByStatusType( $strCountryCode, $objDatabase ) {

		if( CCountry::CODE_USA == $strCountryCode ) {
			$strWhereCondition = 'ea.country_code IN ( \'' . CCountry::CODE_USA . '\',\'' . CCountry::CODE_CANADA . '\' )';
		} else {
			$strWhereCondition = 'ea.country_code = \'' . CCountry::CODE_INDIA . '\' ';
		}

		$strSql = 'SELECT
						e.id,
						e.email_address,
						u.id AS user_id
					FROM
						employees e
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN users u ON ( u.employee_id = e.id )
					WHERE
						' . $strWhereCondition . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDetailsByUserId( $intUserId, $objDatabase ) {
		if( false == valId( $intUserId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*,
						ea.country_code
					FROM
						employees AS e
						JOIN users AS u ON ( u.employee_id = e.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id and ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND u.id = ' . ( int ) $intUserId;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchEmployeesDetailsBySecureRepositoryId( $intRoleId, $intPageNo, $intPageSize, $objDatabase, $boolIsCount = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		if( true == $boolIsCount ) {
			$strFields = 'COUNT( DISTINCT( e.id ) )';
		} else {
			$strFields = 'DISTINCT ON (e.id) e.id,
									e.name_full,
									e.email_address,
									ea.country_code,
									u.id as user_id,
									t.name as team_name,
									d.name as department_name,
									emp.name_full as manager_name,
									est.name as status,
									ur.is_read,
									ur.is_write';
		}

		$strSql = 'SELECT ' . $strFields . '
					FROM
						employees e
						JOIN users u ON( e.id = u.employee_id )
						JOIN user_repository_permissions ur ON( u.id = ur.user_id )
						JOIN employee_addresses ea ON( e.id = ea.employee_id )
						JOIN team_employees te ON( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT JOIN employees emp ON( e.reporting_manager_id = emp.id )
						JOIN teams t ON( te.team_id = t.id )
						JOIN departments d ON( d.id = e.department_id )
						LEFT JOIN employee_status_types est ON( est.id = e.employee_status_type_id AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' )
					WHERE
						( e.date_terminated IS NULL OR e.date_terminated > NOW() ) AND
						ur.repository_id =' . ( int ) $intRoleId;

		if( false == $boolIsCount && 0 != ( int ) $intPageSize ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;
		}

		if( true == $boolIsCount ) {
			$arrintCount = fetchData( $strSql, $objDatabase );

			return $arrintCount[0]['count'];
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchEmployeesDetailsByExecuteSqlAccess( $intEmployeeStatusTypeId, $intPageNo, $intPageSize, $objDatabase, $boolIsCount = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		if( true == $boolIsCount ) {
			$strFields = 'COUNT( DISTINCT( e.id ) )';
		} else {
			$strFields = 'DISTINCT ON (e.id) e.id,
									e.name_full,
									e.email_address,
									ea.country_code,
									u.id as user_id,
									t.name as team_name,
									d.name as department_name,
									emp.name_full as manager_name';
		}

		$strSql = 'SELECT ' . $strFields . '
					FROM
						employees e
						JOIN users u ON( e.id = u.employee_id )
						JOIN user_permissions up ON( u.id = up.user_id )
						JOIN employee_addresses ea ON( e.id = ea.employee_id )
						JOIN team_employees te ON( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT JOIN employees emp ON( e.reporting_manager_id = emp.id )
						JOIN teams t ON( te.team_id = t.id )
						JOIN departments d ON( d.id = e.department_id )
					WHERE
						( e.date_terminated IS NULL OR e.date_terminated > NOW() ) 
						AND e.employee_status_type_id =' . ( int ) $intEmployeeStatusTypeId;

		if( false == $boolIsCount && 0 != ( int ) $intPageSize ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;
		}

		if( true == $boolIsCount ) {
			$arrintCount = fetchData( $strSql, $objDatabase );

			return $arrintCount[0]['count'];
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchEmployeesDataByDesignationIds( $arrintDesignationIds, $objDatabase, $boolAllEmployees = false ) {
		if( false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}
		$strWhereCondition = ' AND e.date_terminated IS NULL';
		if( true == $boolAllEmployees ) {
			$strWhereCondition = '';
		}
		$strSql = ' SELECT
						e.id,
						e.date_terminated,
						u.id as user_id,
						e.preferred_name as name_full,
						e.department_id,
						e.designation_id,
						e.email_address,
						e.physical_phone_extension_id
					FROM
						employees e JOIN users u ON ( e.id = u.employee_id )
					WHERE
						e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' )
						AND u.is_disabled = 0
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . $strWhereCondition . '
					ORDER BY
						e.preferred_name ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedNoReviewCycleEmployees( $intPageNo, $intPageSize, $boolIsCount = false, $intAddEmployeeId, $objDatabase, $arrintEmployeeIds = NULL ) {

		$strSubSql = '';

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		if( true == is_numeric( $intAddEmployeeId ) ) {
			$strWhereClause = ' AND t.add_employee_id = ' . ( int ) $intAddEmployeeId . ' ';
		} else {
			$strWhereClause = ' ';
		}

		if( true == valArr( $arrintEmployeeIds ) ) {
			$strWhereClause .= ' AND e.id IN(' . implode( ',', $arrintEmployeeIds ) . ')';
		}

		if( true == $boolIsCount ) {
			$strSelectClause = 'count(*)';
		} else {
			$strSelectClause = 'e.id as emp_id,
								e.name_full,
								e.date_started,
								e.last_assessed_on::date AS last_assessed_on,
								e.employee_number,
								e.email_address,
								e.date_confirmed,
								d.name AS designation_name,
								dep.name AS department_name,
								t.name AS team_name,
								emp.name_full AS pdm_name,
								ee.name_full AS reporting_manager,
								eee.name_full AS hr_representative,
								( EXTRACT ( YEAR FROM AGE ( CURRENT_DATE, e.date_started ) ) * 12 ) + EXTRACT ( MONTH FROM AGE ( CURRENT_DATE, e.date_started ) ) AS xento_experience,
								CASE
									WHEN ( ead.experience_year IS NULL OR ead.experience_year = 0 ) AND ( ead.experience_month IS NULL OR ead.experience_month = 0 ) THEN
										( EXTRACT ( YEAR FROM AGE ( CURRENT_DATE, e.date_started ) ) * 12 ) + EXTRACT ( MONTH FROM AGE ( CURRENT_DATE, e.date_started ) )
									ELSE
										( EXTRACT ( YEAR FROM AGE ( CURRENT_DATE, e.date_started ) ) * 12 ) + EXTRACT ( MONTH FROM AGE ( CURRENT_DATE, e.date_started ) ) + ( ead.experience_year * 12 ) + ead.experience_month
								END AS total_experience';

			$strOrderBy = ' ORDER BY name_full ASC' . $strSubSql;

		}

		$strSql = ' SELECT
						' . $strSelectClause . '
					FROM
						employees e
						JOIN employee_addresses eadd ON ( eadd.employee_id = e.id AND eadd.address_type_id = ' . CAddressType::PRIMARY . ' AND eadd.country_code = \'IN\' )
						LEFT JOIN employee_preferences ep ON ( ep.employee_id = e.id AND ep.key = \'EMPLOYEE_REVIEW_CYCLE\' )
						LEFT JOIN employee_applications ea ON ( ea.employee_id = e.id )
						LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = ea.id )
						LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
						LEFT JOIN employees ee ON ( e.reporting_manager_id = ee.id )
						LEFT JOIN teams t ON ( te.team_id = t.id )
						LEFT JOIN employees emp ON ( t.sdm_employee_id = emp.id )
						LEFT JOIN designations d ON ( e.designation_id = d.id )
						LEFT JOIN departments dep ON ( e.department_id = dep.id )
						LEFT JOIN employees eee ON ( t.hr_representative_employee_id = eee.id )
					WHERE
						ep.id IS NULL AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . $strWhereClause . $strOrderBy;

		if( true == $boolIsCount ) {
			$arrintEmployeesNoReviewCycleCount = fetchData( $strSql, $objDatabase );
			if( true == isset( $arrintEmployeesNoReviewCycleCount[0]['count'] ) ) {
				return $arrintEmployeesNoReviewCycleCount[0]['count'];
			}
		} else {
			return fetchData( $strSql, $objDatabase );
		}

	}

	public static function fetchAllBUHrRepresentatives( $objDatabase, $strCountryCode = NULL ) {

		$strSubSql = '';

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( CCountry::CODE_USA == $strCountryCode ) {
			$strSubSql .= ' AND t.department_id IS NOT NULL';
		}

		$strSql = 'SELECT
						DISTINCT e.id,
						e.preferred_name,
						e.email_address
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN employees e ON ( t.hr_representative_employee_id = e.id )
						JOIN designations d on (e.designation_id = d.id)
						JOIN employee_addresses ea ON ( ea.employee_id = e.id )
					WHERE
						t.hr_representative_employee_id IS NOT NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.date_terminated is null '
				  . $strSubSql .
				  ' AND ea.country_code = \'' . $strCountryCode . '\'
					ORDER BY
						e.preferred_name ASC ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllTPMsOfIndiaOffice( $objDatabase, $arrintDepartmentIds = NULL ) {
		$strCondition = NULL;
		if( true == valArr( $arrintDepartmentIds ) ) {
			$strCondition = 'AND e.department_id NOT IN( ' . implode( ',', $arrintDepartmentIds ) . ' )';
		}
		$strSql = 'SELECT
						DISTINCT e.id,
						e.name_full,
						e.preferred_name,
						e.email_address,
						e.department_id
					FROM
						employees e
						JOIN employees e1 ON ( e.id = e1.reporting_manager_id )
						JOIN users u ON (e.id = u.employee_id)
						JOIN user_groups ug ON (u.id = ug.user_id AND ug.deleted_by IS NULL )
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ug.group_id = ' . CGroup::PHP_TPM . '
						AND e.office_id IN ( ' . COffice::TOWER_8 . ',' . COffice::TOWER_9 . ' )
						' . $strCondition . '
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchTpmDetailsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' WITH RECURSIVE all_managers( employee_id, manager_employee_id, eid ) AS (
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id,
							e.id AS eid
						FROM
							employees e
						WHERE
							e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						UNION ALL
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id,
							am.eid
						FROM
							employees e,
							all_managers am
						WHERE
							e.id = am.manager_employee_id
							AND am.manager_employee_id != am.eid )
					SELECT
						am.eid as employee_id,
						e.id as tpm_employee_id
					FROM
						all_managers am
						JOIN employees e ON ( am.employee_id = e.id )
						JOIN employees e1 ON ( am.manager_employee_id = e1.id AND e.designation_id IN ( ' . CDesignation::TECHNICAL_PROJECT_MANAGER . ' , ' . CDesignation::ASSOCIATE_TECHNICAL_PRODUCT_MANAGER . ' ) )
					WHERE
						e.employee_status_type_id = ' . CEmployeestatustype::CURRENT . '
						AND e.designation_id IN (	' . CDesignation::TECHNICAL_PROJECT_MANAGER . ' , ' . CDesignation::ASSOCIATE_TECHNICAL_PRODUCT_MANAGER . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSalesEmployeesByEmployeeStatusTypeIdsByManagerId( $arrintEmployeeStatusTypeIds, $intManagerEmployeeId, $boolIsIncludeManager = false, $objAdminDatabase ) {

		if( false == valArr( $arrintEmployeeStatusTypeIds ) || false == is_numeric( $intManagerEmployeeId ) ) {
			return false;
		}

		$strWhere = '';

		if( true == $boolIsIncludeManager ) {
			$strWhere = ' OR e.id = ' . ( int ) $intManagerEmployeeId;
		}

		$strSql = '	SELECT
						custom_employees.id,
						custom_employees.preferred_name::TEXT || \' ( \' || COUNT ( pl.id )::TEXT || \' ) \' AS preferred_name
					FROM
						(
							SELECT
								e.id,
								e.preferred_name
							FROM
								employees e
							WHERE
								e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
								AND e.employee_status_type_id IN ( ' . implode( ',', $arrintEmployeeStatusTypeIds ) . ' ) ' . $strWhere . '
						) AS custom_employees
						JOIN ps_lead_details AS pld ON custom_employees.id = pld.sales_employee_id
						JOIN ps_leads AS pl ON pl.id = pld.ps_lead_id
					WHERE
						pl.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND pld.renewal_date IS NOT NULL
					GROUP BY
						custom_employees.id,
						custom_employees.preferred_name
					ORDER BY
						custom_employees.preferred_name';

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchCustomSalesEmployeesByEmployeeStatusTypeIdByDesignationIds( $arrintEmployeeStatusTypeIds, $arrintDesignationIds, $objAdminDatabase ) {

		if( false == valArr( $arrintEmployeeStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT id,
						preferred_name
					FROM
						(
							(
								SELECT
									e.id,
									e.preferred_name::TEXT || \' ( \' || COUNT ( pld.ps_lead_id )::TEXT || \' )\' AS preferred_name
								FROM
									employees e
									JOIN ps_lead_details AS pld ON e.id = pld.sales_employee_id
									JOIN ps_leads AS pl ON pl.id = pld.ps_lead_id
								WHERE
									pld.renewal_date IS NOT NULL
									AND pl.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
									AND e.employee_status_type_id IN ( ' . implode( ',', $arrintEmployeeStatusTypeIds ) . ' )
								GROUP BY
									e.id,
									e.preferred_name
							)
							UNION
							(
								SELECT
									e.id,
									e.preferred_name::TEXT || \' ( 0 )\' AS preferred_name
								FROM
									employees e
								WHERE
									e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
									AND e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' )
								GROUP BY
									e.id,
									e.preferred_name
							)
						) as renewals_employees
					ORDER BY
						preferred_name';

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeDetailDataByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						ved.user_id,
						ved.designation_id,
						e.id,
						ved.designation_name,
						ved.department_id,
						e.email_address,
						emp.preferred_name AS manager_name,
						ved.team_name,
						e.last_assessed_on,
						e.date_started,
						ep.value AS review_cycle,
						ead.experience_year AS employee_experience_year,
						ead.experience_month AS employee_experience_month
					FROM
						employees e
						LEFT JOIN view_employee_details ved ON ( ved.employee_id = e.id )
						LEFT JOIN employees emp ON ( e.reporting_manager_id = emp.id )
						LEFT JOIN employee_applications ea ON ( ea.employee_id = e.id )
						LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = ea.id )
						LEFT JOIN employee_status_types est ON ( est.id = e.employee_status_type_id )
						LEFT JOIN employee_preferences ep ON ( ep.employee_id = e.id AND ep.key = \'EMPLOYEE_REVIEW_CYCLE\' )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id = ' . ( int ) $intEmployeeId;

		$arrintData = fetchData( $strSql, $objDatabase );

		return $arrintData[0];

	}

	public static function fetchNewHireTurnOverEmployeesByStartDateAndEndDate( $objDatabase, $strStartDate, $strEndDate, $strCountryCode, $intPageNo = NULL, $intPageSize = NULL, $intRecruiterId = NULL, $boolIsCount = false ) {

		if( true == is_null( $strStartDate ) && true == is_null( $strEndDate ) ) {
			return NULL;
		}

		$strSubSql = '';
		$strWhere  = '';
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( false == $boolIsCount ) {
			$strSelectClause = 'e.preferred_name ,
						to_char( e.date_started , \'dd Mon, YYYY\' ) as date_started,
						to_char( e.date_terminated , \'dd Mon, YYYY\' ) as date_terminated,
						COALESCE( ee.preferred_name , \'-\') AS recruiter_name,
						( e.date_terminated - e.date_started ) AS total_duration';

			if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
				$strSubSql = ' ORDER BY
						e.preferred_name,
						ee.preferred_name
						OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			}

		} else {
			$strSelectClause = 'COUNT(*) ';
		}
		if( true == is_numeric( $intRecruiterId ) ) {
			$strWhere = 'AND ee.id = ' . ( int ) $intRecruiterId;
		}

		$strSql = 'SELECT
						' . $strSelectClause . '
					FROM
						employees e
						JOIN employee_applications ea ON ( e.id = ea.employee_id AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\') )
						LEFT JOIN resource_requisitions rr ON ( rr.purchase_request_id = ea.purchase_request_id )
						LEFT JOIN employees ee ON ( rr.recruiter_employee_id = ee.id )
					WHERE
						e.date_started >= \'' . addslashes( $strStartDate ) . '\'
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.date_terminated <= \'' . addslashes( $strEndDate ) . '\' ' . $strWhere . '
					' . $strSubSql;

		if( false == $boolIsCount ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			$arrintCount = fetchData( $strSql, $objDatabase );
			if( true == isset( $arrintCount[0]['count'] ) ) {
				return $arrintCount[0]['count'];
			}

			return 0;
		}
	}

	public static function fetchHireSourceRatioByStartDateByEndDate( $strStartDate, $strEndDate, $strCountryCode, $objDatabase, $intPageNo = NULL, $intPageSize = NULL, $intRecruiterId = NULL ) {

		if( true == is_null( $strStartDate ) && true == is_null( $strEndDate ) ) {
			return NULL;
		}
		$strSubSql = '';
		$strWhere  = '';
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		if( false == is_null( $intRecruiterId ) && 0 != $intRecruiterId ) {
			$strWhere = 'AND rr.recruiter_employee_id = ' . ( int ) $intRecruiterId;
		}

		$strSql = 'SELECT
						eas.name,
						eas.id,
						count ( ea.employee_application_source_id ) AS source_count
					FROM
						employees e
						JOIN employee_applications ea ON ( e.id = ea.employee_id AND ea.country_code IN ( \'' . addslashes( $strCountryCode ) . '\') )
						LEFT JOIN resource_requisitions rr ON ( rr.purchase_request_id = ea.purchase_request_id )
						LEFT JOIN employees ee ON ( rr.recruiter_employee_id = ee.id )
						LEFT JOIN employee_application_sources eas ON ( ea.employee_application_source_id = eas.id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND e.date_started >= \'' . addslashes( $strStartDate ) . '\' AND e.date_started <= \'' . addslashes( $strEndDate ) . '\' AND eas.name IS NOT NULL ' . $strWhere . '
					GROUP BY
						eas.id,
						eas.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllActiveUsEmployeesAndPersonsContactDetails( $intPsLeadId, $objDatabase, $strSearchTearm = NULL ) {

		if( false == is_numeric( $intPsLeadId ) ) {
			return NULL;
		}

		$strWhereCondition = '';
		if( false == is_null( $strSearchTearm ) ) {
			$strWhereCondition = ' AND ( p.name_first ILIKE E\'%' . addslashes( trim( $strSearchTearm ) ) . '%\' OR p.name_last ILIKE E\'%' . addslashes( trim( $strSearchTearm ) ) . '%\' )';
		}

		$strSql = '( SELECT
						p.id,
						p.name_first,
						p.name_last,
						p.email_address
					FROM
						persons p
						WHERE
							p.is_disabled = 0
							AND p.ps_lead_id = ' . ( int ) $intPsLeadId . '
							AND p.deleted_on IS NULL
							AND p.deleted_on IS NULL
						' . $strWhereCondition . '
					ORDER BY p.name_first
					) UNION (
					SELECT
						e.id,
						e.name_first,
						e.name_last,
						e.email_address
					FROM
						employees e
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ea.country_code = \'' . CCountry::CODE_USA . '\'
						AND ( date_terminated IS NULL OR date_terminated > NOW() )
						AND e.name_full IS NOT NULL
						AND e.email_address IS NOT NULL
					ORDER BY
						e.name_full )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDataByEmailAddress( $strEmailAddress, $objDatabase ) {
		$strSql = 'SELECT
						e.id,
						e.name_full,
						e.email_address,
						u.id as user_id
					FROM
						employees as e
						JOIN users u ON( e.id = u.employee_id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.email_address=\'' . $strEmailAddress . '\'';

		$arrintData = fetchData( $strSql, $objDatabase );

		return $arrintData[0];
	}

	public static function fetchEmployeesData( $objDatabase ) {

		$strSql = 'SELECT
						em.id as employee_id,
						us.id as user_id,
						em.email_address as email_address,
						us.svn_username as svn_username,
						em.name_full
					FROM
						employees em
						LEFT JOIN users us ON ( us.employee_id = em.id )
					WHERE 
						em.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesByEmployeeStatusTypeIdByPhoneNumberTypeIdByDependentTypeIdByPreference( $objDatabase ) {
		$strSql = 'SELECT
						e.employee_number,
						e.name_first,
						e.name_last,
						e.name_middle,
						e.preferred_name,
						e.gender,
						e.blood_group,
						e.email_address,
						e.permanent_email_address,
						epn.phone_number as phone_number,
						epn1.phone_number as mobile,
						e.birth_date,
						e.anniversary_date,
						age( e.birth_date ) AS period,
						ed.name_first || \' \' || ed.name_last as spouse_name,
						ed.gender as spouse_gender,
						ed.birth_date as spouse_birth_date,
						age( ed.birth_date ) As spouse_age,
						ed1.name_first || \' \' || ed.name_last as child_name,
						ed1.gender as child_gender,
						ed1.birth_date as child_birth_date,
						age( ed1.birth_date ) child_age,
					CASE
						WHEN epf1.key = \'MAGARPATTA_ACCESS_CARD_NUMBER\' then epf1.value
					END as magarpatta_access_card_number,
					CASE
						WHEN epf2.key = \'USING_COMPANY_TRANSPORT\' then epf2.value
					END as is_using_company_transport
					FROM employees e
						LEFT OUTER JOIN employee_phone_numbers epn ON ( e.id = epn.employee_id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
						LEFT OUTER JOIN employee_phone_numbers epn1 ON ( e.id = epn1.employee_id AND epn.phone_number_type_id = ' . CPhoneNumberType::MOBILE . ')
						LEFT OUTER JOIN employee_dependants ed ON ( e.id = ed.employee_id AND ed.dependant_type_id = ' . CDependantType::SPOUSE . ')
						LEFT OUTER JOIN employee_dependants ed1 ON ( e.id = ed1.employee_id AND ed1.dependant_type_id = ' . CDependantType::CHILD . ')
						LEFT OUTER JOIN employee_preferences epf1 ON (e.id = epf1.employee_id AND epf1.key = \'MAGARPATTA_ACCESS_CARD_NUMBER\')
						LEFT OUTER JOIN employee_preferences epf2 ON (e.id = epf2.employee_id AND epf2.key = \'USING_COMPANY_TRANSPORT\')
					WHERE e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
					ORDER BY e.date_started';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByNameFull( $strSalesEngineer, $objDatabase ) {

		$strSql         = 'SELECT id FROM employees WHERE name_full like \'%' . $strSalesEngineer . '%\' AND employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW;
		$arrintEmployee = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintEmployee[0] ) ) {
			return $arrintEmployee[0];
		}

		return 0;
	}

	public static function fetchAddEmployeeByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*
					FROM
						team_employees te
						LEFT JOIN teams t ON ( t.id = te.team_id )
						JOIN employees e ON ( e.id = t.add_employee_id )
					WHERE
						te.employee_id = ' . ( int ) $intEmployeeId . '
						AND te.is_primary_team = 1 AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchQamEmployeeByEmployeeId( $intEmployeeId, $objDatabase ) {
		if( false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*
					FROM
						team_employees te
						LEFT JOIN teams t ON ( t.id = te.team_id )
						JOIN employees e ON ( e.id = t.qam_employee_id )
					WHERE
						te.employee_id = ' . ( int ) $intEmployeeId . '
						AND te.is_primary_team = 1 AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchTpmEmployeeByEmployeeId( $intEmployeeId, $objDatabase ) {
		if( false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*
					FROM
						team_employees te
						LEFT JOIN teams t ON ( t.id = te.team_id )
						JOIN employees e ON ( e.id = t.tpm_employee_id )
					WHERE
						te.employee_id = ' . ( int ) $intEmployeeId . '
						AND te.is_primary_team = 1 AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchAddEmployees( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT e.*
					FROM
						team_employees te
						LEFT JOIN teams t ON ( t.id = te.team_id )
						LEFT JOIN employees e ON ( e.id = t.add_employee_id )
						JOIN employee_addresses ea ON ( t.add_employee_id = ea.employee_id AND ea.address_type_id = 1 AND ea.country_code = \'IN\' )
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchPaginatedECSEmployeesDetailsByGroupTypeId( $intGroupTypeId, $intPageNo, $intPageSize, $objAdminDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {
		if( false == isset( $intGroupTypeId ) ) {
			return NULL;
		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT
							e.id,
							e.preferred_name as preferred_name,
							ug.is_send_esms as subscription,
							ug.user_id,
							ug.group_id,
							g.name as group_name,
							g.location as group_location
						FROM
							employees AS e,
							users AS u,
							user_groups AS ug,
							groups AS g
						WHERE
							ug.group_id = g.id
							AND ug.deleted_by IS NULL
							AND ug.user_id = u.id
							AND g.deleted_by IS NULL
							AND g.group_type_id = ' . ( int ) $intGroupTypeId . '
							AND e.id = u.employee_id
							AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '

					ORDER BY
						' . ' ' . $strOrderByField . ' ' . $strOrderByType . ' ';
		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchSearchedECSEmployees( $arrstrFilteredExplodedSearch, $objAdminDatabase ) {

		$strSql = 'SELECT
						e.id,
						e.preferred_name,
						ug.is_send_esms,
						ug.user_id,
						ug.group_id,
						g.name,
						g.location
					FROM
						employees AS e,
						users AS u,
						user_groups AS ug,
						groups AS g
					WHERE
						ug.group_id = g.id
						AND ug.user_id = u.id
						AND ug.deleted_by IS NULL
						AND g.deleted_by IS NULL
						AND g.group_type_id = ' . CGroupType::ECS . '
						AND e.id = u.employee_id
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND ( e.name_first ILIKE \'%' . implode( '%\' AND e.name_first ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_last ILIKE \'%' . implode( '%\' AND e.name_last ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.email_address ILIKE \'%' . implode( '%\' AND e.email_address ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\') ';

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchPaginatedEmployeesWithPayrollDetailsByPayrollId( $intPayrollId, $arrmixFilter, $objDatabase, $objPagination = NULL, $boolIsCount = false ) {

		if( false == valStr( $arrmixFilter['begin_date'] ) || false == valStr( $arrmixFilter['end_date'] ) ) {
			return NULL;
		}

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strOrderByType = ( 0 == $objPagination->getOrderByType() ) ? ' ASC ' : ' DESC ';
			$intOffset      = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit       = ( int ) $objPagination->getPageSize();
		}

		$strJoinSql  = '';
		$strWhereSql = '';

		if( true == isset( $arrmixFilter['hr_representative_id'] ) ) {
			$strJoinSql = ' LEFT OUTER JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
							LEFT OUTER JOIN teams t ON ( t.id = te.team_id )';

			if( true == valId( $arrmixFilter['hr_representative_id'] ) ) {

				$strWhereSql .= ' AND ( t.hr_representative_employee_id = ' . ( int ) $arrmixFilter['hr_representative_id'] . ' ) ';

			} elseif( 'other' == $arrmixFilter['hr_representative_id'] ) {

				// condition for other HR => Employees who don't have any HR assigned or HR is not in India
				$strWhereSql .= ' AND (
										t.hr_representative_employee_id IS NULL
										OR
										t.hr_representative_employee_id NOT IN (
											SELECT
												DISTINCT ON ( e.id ) e.id
											FROM
												employees e
												JOIN teams t ON ( e.id = t.hr_representative_employee_id )
												JOIN employee_addresses AS ea ON ( e.id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\')
											WHERE
												ea.country_code = \'' . CCountry::CODE_INDIA . '\'
												AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
										)
									)';
			}

		}

		if( true == valArr( $arrmixFilter['search_data'] ) ) {
			$strWhereSql .= ' AND ( e.name_full ILIKE \'%' . implode( '%\' AND name_full ILIKE \'%', $arrmixFilter['search_data'] ) . '%\' )';
		}

		if( 1 == $arrmixFilter['show_exception'] ) {
			$strWhereSql .= ' AND CASE WHEN ep.supporting_data->>\'exceptions\' <> \'\' THEN TRUE ELSE FALSE END';
		}

		if( 1 == $arrmixFilter['show_unreviewed'] ) {
			$strWhereSql .= ' AND ep.reviewed_by IS NULL';
		}

		if( true == $boolIsCount ) {
			$strSql = 'SELECT count( e.id ) ';
		} else {
			$strSql = 'SELECT
						DISTINCT e.id as employee_id,
						e.employee_number,
						e.preferred_name AS name_full,
						e.date_started,
						e.date_confirmed,
						e.date_terminated,
						e.email_address,
						d.name as department_name,
						de.name as designation,
						ep.payroll_id,
						ep.working_days,
						ep.paid_days,
						ep.unpaid_days,
						ep.extra_working_days,
						ep.present_days,
						ep.absent_days,
						ep.reviewed_by,
						ep.supporting_data->>\'exceptions\' as exceptions,
						ep.supporting_data->>\'special_leave_count\' as special_leaves,
						( ep.supporting_data->> \'leaves\' )::json ->> \'closing_leave_pool\' AS closing_leave_pool';
		}

		$strSql .= '
				FROM
					employees e
					LEFT JOIN employee_addresses AS ea ON (e.id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\')
					LEFT JOIN departments AS d ON (e.department_id = d.id)
					LEFT JOIN designations AS de ON ( e.designation_id = de.id )
					LEFT JOIN employee_payrolls AS ep ON ( e.id = ep.employee_id AND ep.payroll_id = ' . ( int ) $intPayrollId . ' ) ' .
				   $strJoinSql . '
				WHERE
					ea.country_code = \'' . CCountry::CODE_INDIA . '\'
					AND e.employee_status_type_id != ' . CEmployeeStatusType::SYSTEM . '
					AND ( e.date_terminated IS NULL OR ( DATE_PART ( \'month\', DATE ( \'' . $arrmixFilter['end_date'] . '\' ) ) <= DATE_PART ( \'month\', DATE ( e.date_terminated ) ) AND DATE_PART ( \'year\', DATE ( \'' . $arrmixFilter['end_date'] . '\' ) ) <= DATE_PART ( \'year\', DATE ( e.date_terminated ) ) ) OR ( e.date_terminated BETWEEN DATE ( \'' . $arrmixFilter['begin_date'] . '\' ) AND DATE ( \'' . $arrmixFilter['end_date'] . '\' ) ) )
					AND DATE( \'' . $arrmixFilter['end_date'] . '\' ) >= e.date_started' . $strWhereSql;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' ORDER BY ' . $objPagination->getOrderByField() . ' ' . $strOrderByType;
			if( false == $boolIsCount ) {
				$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			}
		}
		$arrmixResult = fetchData( $strSql, $objDatabase );

		return ( true == $boolIsCount ) ? $arrmixResult[0]['count'] : $arrmixResult;
	}

	public static function fetchEmployeeReferencesCount( $strStartDate, $strEndDate, $strCountryCode, $objDatabase, $boolHidePastReferee ) {

		$strWhereSql = ( $boolHidePastReferee == true ) ? 'AND er.employee_status_type_id = ' . CEmployeeStatusType::CURRENT :'';
		$strSql = 'SELECT DISTINCT
						ea.employee_id,
						e.name_full AS employee_name_full,
						e.preferred_name AS employee_preferred_name,
						ea.referee_employee_id,
						er.name_full AS referee_name_full,
						er.preferred_name AS referee_preferred_name,
						e.date_started,
						d.name AS designation
					FROM
						employees e
						JOIN designations d ON( d.id = e.designation_id )
						JOIN employee_applications ea ON( e.id = ea.employee_id )
						JOIN employees er ON( er.id = ea.referee_employee_id )
						JOIN employee_addresses ead ON( ead.employee_id = er.id )
					WHERE
						ea.referee_employee_id IS NOT NULL
						AND ea.employee_id IS NOT NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT
						. $strWhereSql . '
						AND ead.country_code = \'' . $strCountryCode . '\'
						AND DATE_TRUNC( \'day\', ( e.date_started AT TIME ZONE \'IST\' ) ) BETWEEN \'' . addslashes( $strStartDate ) . '\' AND \'' . addslashes( $strEndDate ) . '\'
					ORDER BY
						er.name_full ASC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchEmployeesInfoByIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						e.name_full,
						e.employee_status_type_id,
						e.department_id,
						e.preferred_name,
						ea.country_code,
						d.name AS department_name,
						ds.name AS designation_name
					FROM
						employees e
						LEFT JOIN departments d ON ( e.department_id = d.id )
						LEFT JOIN designations ds ON ( ds.id = e.designation_id )
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
					ORDER BY lower(e.name_first)';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						e.id,
						e.name_first,
						e.preferred_name,
						e.email_address
					FROM
						employees e
					WHERE
						e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . '
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesInfoByUserIds( $arrintUserIds, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) ) {
			return NULL;
		}

		// Using array_filter to remove the blank values.
		$arrintUserIds = array_filter( $arrintUserIds );

		if( false == valArr( $arrintUserIds ) ) {
			return NULL;
		}

		return self::fetchEmployees( 'SELECT e.id,e.preferred_name, e.name_full, u.id as user_id FROM employees e JOIN users u ON( e.id = u.employee_id ) WHERE u.id IN (' . implode( ',', $arrintUserIds ) . ') AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' ', $objDatabase );
	}

	public static function fetchEmployeesPendingSurvey( $objDatabase ) {

		$strSql = 'SELECT
						sub.*,
						ea.country_code
					FROM
						(
							SELECT
								DISTINCT ( e.id ) AS employee_id,
								e.reporting_manager_id AS manager_id,
								e.name_full AS name_full
							FROM
								surveys s
									LEFT JOIN surveys s1 ON ( s1.trigger_reference_id = s.id )
									LEFT JOIN employees e ON ( e.id = s.responder_reference_id )
									LEFT JOIN scheduled_surveys ss ON ( ss.responder_reference_id = e.id )
							WHERE
								s1.response_datetime IS NULL
								AND s1.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_MANAGER_SURVEY_IN . '
								AND s1.survey_type_id = ' . CSurveyType::MANAGER_SURVEY . '
								AND s.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_IN . '
								AND s.survey_type_id = ' . CSurveyType::PERSONAL_REVIEW . '
								AND e.date_terminated IS NULL
								AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
								AND s1.survey_datetime::DATE >= \'' . date( 'Y-m-01' ) . '\'::DATE
								AND ss.frequency_id = 4
						) sub
							LEFT JOIN employee_addresses ea ON ( ea.employee_id = sub.manager_id AND ea.address_type_id = 1 )
					WHERE
						ea.country_code != \'' . CCountry::CODE_USA . '\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchManagersTeamSurveysPending( $objAdminDatabase, $objPagination = NULL, $boolIsCount = false, $strStartDate, $strEndDate, $intEmployeeId, $strOrderBy, $strOrderType, $boolIsDownload = false ) {

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit  = ( int ) $objPagination->getPageSize();
		}

		if( 'intimated_date' == $strOrderBy ) {
			$strOrderBy = 'max( intimated_date )';
		}

		$strSql = 'WITH RECURSIVE empl_hierarchy AS (
						SELECT
							e.id AS employee_id,
							e.date_terminated,
							t.id AS team_id,
							e.preferred_name,
							e.employee_status_type_id,
							d.name AS designation_name
						FROM
							teams t
							JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
							JOIN employees e1 ON ( e1.reporting_manager_id = te.employee_id )
							JOIN employees e ON ( e.id = te.employee_id AND e.employee_status_type_id = 1 )
							JOIN designations d ON ( e.designation_id = d.id )
							JOIN departments de ON( e.department_id = de.id )
						WHERE
							t.add_employee_id = ' . ( int ) $intEmployeeId . '
							AND d.id NOT IN( ' . CDesignation::QA_MANAGER . ' )
						UNION
							SELECT
								e2.id,
								e2.date_terminated,
								t2.id,
								e2.preferred_name,
								e2.employee_status_type_id,
								d.name AS designation_name
							FROM
								empl_hierarchy r
								JOIN teams t2 ON ( t2.manager_employee_id = r.employee_id )
								JOIN team_employees te ON ( te.team_id = t2.id AND te.is_primary_team = 1 )
								JOIN employees e1 ON ( e1.reporting_manager_id = te.employee_id )
								JOIN employees e2 ON ( e2.id = te.employee_id AND e2.employee_status_type_id = 1 )
								JOIN designations d ON ( e2.designation_id = d.id )
								JOIN departments de1 ON( e2.department_id = de1.id )
							WHERE
								(	e2.id != e2.reporting_manager_id )
									AND e2.id IS NOT NULL
									AND te.is_primary_team = 1
									AND e2.date_terminated IS NULL
									AND t2.add_employee_id = ' . ( int ) $intEmployeeId . '
									AND d.id NOT IN( ' . CDesignation::QA_MANAGER . ' )
								)
							SELECT
								e.employee_id,
								e.preferred_name,
								e.designation_name,
								to_char( max( sep.intimated_date ), \'MM/DD/YY\' ) AS last_intimated_date,
							sum( CASE
									WHEN sep.points = \'1\' THEN 1 * -1
									ELSE 0
							END ) AS first_intimation_point,
							sum( CASE
									WHEN sep.points = \'2\' THEN sep.points * -1
									ELSE 0
							END ) AS second_intimation_point,
							sum ( CASE
									WHEN sep.points = \'5\' THEN sep.points * -1
									ELSE 0
							END ) AS third_intimation_point,
							sum ( CASE
								WHEN sep.points = \'10\' THEN sep.points * -1
								ELSE 0
							END ) AS fourth_intimation_point,
							sum ( CASE
								WHEN sep.points IS NULL THEN 0
								ELSE sep.points * -1
							END ) AS total_intimations_point_count
						FROM
							empl_hierarchy e
							LEFT JOIN stats_employee_performances sep ON ( e.employee_id = sep.employee_id AND sep.intimated_date BETWEEN \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . ' 00:00:01\' AND \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . ' 23:59:59\' )
						GROUP BY
							e.employee_id,
							e.preferred_name,
							e.designation_name
						ORDER BY ' .
				  $strOrderBy . ' ' . $strOrderType;

		if( false == $boolIsCount && false == $boolIsDownload ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$arrmixResult = fetchData( $strSql, $objAdminDatabase );

		return ( true == $boolIsCount ) ? \Psi\Libraries\UtilFunctions\count( $arrmixResult ) : $arrmixResult;

	}

	public static function fetchEmployeesDetailsByFilter( $arrmixFilter, $objDatabase, $arrintEmployeeIds = NULL ) {

		if( false == valStr( $arrmixFilter['begin_date'] ) || false == valStr( $arrmixFilter['end_date'] ) ) {
			return NULL;
		}

		$strWhereSql = ( true == valArr( $arrintEmployeeIds ) ) ? ' AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ')' : '';

		$strSql = ' SELECT
						e.id,
						e.designation_id
					FROM
						employees e
						JOIN employee_addresses AS ea ON (e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ' )
						AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
						AND ( e.date_terminated IS NULL OR ( DATE_PART ( \'month\', DATE ( \'' . $arrmixFilter['end_date'] . '\' ) ) <= DATE_PART ( \'month\', DATE ( e.date_terminated ) ) AND DATE_PART ( \'year\', DATE ( \'' . $arrmixFilter['end_date'] . '\' ) ) <= DATE_PART ( \'year\', DATE ( e.date_terminated ) ) ) OR ( e.date_terminated BETWEEN DATE ( \'' . $arrmixFilter['begin_date'] . '\' ) AND DATE ( \'' . $arrmixFilter['end_date'] . '\' ) ) )
						AND DATE( \'' . $arrmixFilter['end_date'] . '\' ) >= e.date_started' .
				  $strWhereSql;

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchHrRepresentativeEmployeesByCountryCode( $strCountryCode, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT e.id,
						e.preferred_name
					FROM
						employees e
						JOIN teams t ON ( e.id = t.hr_representative_employee_id )
						JOIN employee_addresses AS ea ON (e.id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\')
					WHERE
						ea.country_code = \'' . $strCountryCode . '\'
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeCountGrowthAndRevenueGrowth( $objDatabase ) {
		$strSql = '	SELECT
						sub.month::date as month,
						sub.head_count,
						sub.xento,
						sub.usa,
						SUM( rl.total_amount ) AS revenue,
						CASE WHEN sub.usa = 0 THEN 0 ELSE ( SUM ( rl.total_amount ) / sub.usa )::INTEGER END AS revenue_per_usa_employee
					FROM
						(
							SELECT 	DATE_TRUNC( \'month\', d.date ) AS month,
									COUNT( CASE WHEN e.office_id 	 IN ( ' . COffice::TOWER_8 . ',' . Coffice::TOWER_9 . ' ) THEN e.id ELSE NULL END ) AS xento,
									COUNT( CASE WHEN e.office_id NOT IN ( ' . COffice::TOWER_8 . ',' . Coffice::TOWER_9 . ' ) THEN e.id ELSE NULL END ) AS usa,
									COUNT( e.id ) AS head_count
							FROM 	dates d
									JOIN employees e ON ( DATE_TRUNC( \'month\', d.date ) >= DATE_TRUNC( \'month\',e.date_started )
									AND ( e.date_terminated IS NULL OR DATE_TRUNC( \'month\',d.date ) <= DATE_TRUNC( \'month\', e.date_terminated ) )
									AND date_part(\'day\', d.date)::INTEGER = 1)
							WHERE e.department_id NOT IN ( ' . CDepartment::CALL_CENTER . ',' . CDepartment::SALES . ' )
									--AND e.office_id NOT IN ( ' . COffice::TOWER_8 . ',' . Coffice::TOWER_9 . ' )
									AND DATE_TRUNC( \'month\', d.date ) <= DATE_TRUNC( \'month\', NOW() )
							GROUP BY DATE_TRUNC( \'month\', d.date )
							ORDER BY DATE_TRUNC( \'month\', d.date )
						) AS sub
						JOIN revenue_logs rl ON ( DATE_TRUNC ( \'month\', rl.month ) = sub.month )
					WHERE
						rl.month BETWEEN DATE_TRUNC ( \'month\', NOW() ) - INTERVAL \'5 YEARS\' AND DATE_TRUNC ( \'month\', NOW() )
					GROUP BY
						sub.month,
						sub.head_count,
						sub.xento,
						sub.usa
					ORDER BY
						sub.month';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDetailDataByEmployeeIds( $intEmployeeIds, $objDatabase ) {

		if( false == valArr( $intEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						e.preferred_name,
						e.employee_number,
						e.name_first,
						e.name_last,
						e.name_full,
						od.desk_number,
						o.name as office_name,
						t.name AS team_name,
						ea.country_code
					FROM
						employees e
						JOIN employee_addresses as ea ON ( ea.employee_id = e.id AND ea.address_type_id =' . CAddressType::PRIMARY . ')
						JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
						JOIN office_desks od ON ( od.id = e.office_desk_id )
						JOIN offices o ON ( o.id = e.office_id )
						JOIN teams t ON ( te.team_id = t.id )
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND e.id IN ( ' . implode( ',', $intEmployeeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRvpCsmNamesByCids( $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						mc.id AS cid,
						pld.implementation_employee_id,
						pld.sales_employee_id,
						pld.support_employee_id,
						e.preferred_name AS name_full,
						e.email_address
					From
						employees e
						JOIN ps_lead_details pld ON ( e.id = pld.implementation_employee_id OR e.id = pld.sales_employee_id OR e.id = pld.support_employee_id )
						JOIN clients mc ON ( pld.cid = mc.id )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND mc.company_status_type_id <> ' . CCompanyStatusType::TERMINATED . '
						AND mc.id IN ( ' . implode( ',', $arrintCids ) . ' )';

		$arrstrTempCompanyManagersDetails = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrstrTempCompanyManagersDetails ) ) {
			return NULL;
		}

		foreach( $arrstrTempCompanyManagersDetails as $arrstrCompanyManagerDetails ) {

			$arrstrCompanyManagersDetails[$arrstrCompanyManagerDetails['cid']]['id'] = $arrstrCompanyManagerDetails['cid'];

			if( false == is_null( $arrstrCompanyManagerDetails['sales_employee_id'] ) && $arrstrCompanyManagerDetails['id'] == $arrstrCompanyManagerDetails['sales_employee_id'] ) {
				$arrstrCompanyManagersDetails[$arrstrCompanyManagerDetails['cid']]['rvp'] = $arrstrCompanyManagerDetails['name_full'];
			}

			if( false == is_null( $arrstrCompanyManagerDetails['support_employee_id'] ) && $arrstrCompanyManagerDetails['id'] == $arrstrCompanyManagerDetails['support_employee_id'] ) {
				$arrstrCompanyManagersDetails[$arrstrCompanyManagerDetails['cid']]['csm'] = $arrstrCompanyManagerDetails['name_full'];
			}
		}

		return $arrstrCompanyManagersDetails;
	}

	public static function fetchEmployeeByIdByReviewCycleByAddressType( $intEmployeeId, $strReviewCycle, $strAddressType, $objDatabase ) {

		if( true == valStr( $strReviewCycle ) ) {
			$strJoinSql   = ' LEFT JOIN employee_preferences ep ON ( e.id = ep.employee_id AND ep.key = \'' . $strReviewCycle . '\' )';
			$strSubSelect = ' ep.value as review_cycle, ';
		}

		$strSql = 'SELECT
						e.name_full,
						e.preferred_name,
						e.id as employee_id,
						e.email_address,'
				  . $strSubSelect . '
						ea.country_code
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . $strAddressType . ' )'
				  . $strJoinSql . '
					WHERE
						e.id = ' . ( int ) $intEmployeeId . '
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesWithPayrollDetailsByPayrollId( $intPayrollId, $arrmixFilter, $objDatabase ) {

		if( false == valStr( $arrmixFilter['begin_date'] ) || false == valStr( $arrmixFilter['end_date'] ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT e.id as employee_id,
						e.name_first as name_first,
						e.employee_number,
						e.preferred_name AS name_full,
						e.email_address,
						d.name as department_name,
						de.name as designation,
						ep.payroll_id,
						ep.working_days,
						ep.paid_days,
						ep.unpaid_days,
						ep.extra_working_days,
						ep.present_days,
						ep.absent_days,
						te.team_id,
						ep.supporting_data->>\'exceptions\' as exceptions,
						( ep.supporting_data ->> \'leaves\' )::json ->> \'closing_leave_pool\' AS closing_leave_pool,
						ep.supporting_data->>\'special_leave_count\' as special_leave_count,
						( ep.supporting_data ->> \'shifts_count\' )::json ->>\'general_shift\' as general_shift,
						( ep.supporting_data ->> \'shifts_count\' )::json ->>\'night_shift\' as night_shift,
						( ep.supporting_data ->> \'shifts_count\' )::json ->>\'evening_shift\' as evening_shift,
						( ep.supporting_data ->> \'shifts_count\' )::json ->>\'dawn_shift\' as dawn_shift,
						( ep.supporting_data ->> \'shifts_count\' )::json ->>\'dusk_shift\' as dusk_shift,
						( ep.supporting_data ->> \'shifts_count\' )::json ->>\'morning_shift\' as morning_shift
					FROM
						employees e
						LEFT JOIN employee_addresses AS ea ON (e.id = ea.employee_id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\')
						LEFT JOIN departments AS d ON (e.department_id = d.id)
						LEFT JOIN designations AS de ON ( e.designation_id = de.id )
						JOIN employee_payrolls AS ep ON ( e.id = ep.employee_id AND ep.payroll_id = ' . ( int ) $intPayrollId . ' )
						LEFT JOIN team_employees te ON ( ep.employee_id = te.employee_id AND te.is_primary_team = 1 )
					WHERE
						e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ')
						AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
						AND ( e.date_terminated IS NULL OR ( DATE_PART ( \'month\', DATE ( \'' . $arrmixFilter['end_date'] . '\' ) ) <= DATE_PART ( \'month\', DATE ( e.date_terminated ) ) AND DATE_PART ( \'year\', DATE ( \'' . $arrmixFilter['end_date'] . '\' ) ) <= DATE_PART ( \'year\', DATE ( e.date_terminated ) ) ) OR ( e.date_terminated BETWEEN DATE ( \'' . $arrmixFilter['begin_date'] . '\' ) AND DATE ( \'' . $arrmixFilter['end_date'] . '\' ) ) )
						AND DATE( \'' . $arrmixFilter['end_date'] . '\' ) >= e.date_started';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return $arrmixResult;
	}

	public static function fetchEmployeeSVNCommitErrorCountByDates( $objAdminDatabase, $objPagination = NULL, $boolIsCount = false, $strStartDate, $strEndDate, $strOrderBy, $strOrderType, $intEmployeeId, $boolIsDownload = false ) {

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit  = ( int ) $objPagination->getPageSize();
		}

		if( 'percentage' == $strOrderBy ) {
			$strOrderBy .= ' ' . $strOrderType . ', svn_commits';
		}

		$strFetchEmployeesbyAdds = 'SELECT
										e.id AS employee_id,
										e.preferred_name,
										d.name
									FROM
										employees e
										LEFT JOIN team_employees te ON ( te.employee_id = e.id )
										LEFT JOIN teams t ON ( t.id = te.team_id )
										JOIN designations d ON ( e.designation_id = d.id )
										JOIN departments de ON ( e.department_id = de.id )
									WHERE
										te.is_primary_team = 1
										AND e.date_terminated IS NULL
										AND e.employee_status_type_id = 1
										AND de.id IN ( ' . CDepartment::DEVELOPMENT . ' )
										AND t.id NOT IN ( ' . CTEAM::SITE_TABLET_ANDROID . ', ' . CTEAM::SITE_TABLET_IOS . ', ' . CTEAM::SITE_TABLET_DEV_I . ' )
										AND t.add_employee_id = ' . ( int ) $intEmployeeId . '
									GROUP BY
										e.id,
										d.name';

		$strFetchUserCommitErrorsByDate = '
								SELECT
									sub1.employee_id,
									sum ( sub1.code_sniffer ) AS code_sniffer,
									sum ( sub1.Security ) AS Security,
									sum ( sub1.Company_Pattern_Management ) AS Company_Pattern_Management,
									sum ( sub1.total_errors_count ) AS total_errors_count
								FROM
										(
											SELECT
												uce.employee_id,
												( CASE
														WHEN uce.user_commit_check_type_id = \'1\' THEN SUM ( uce.error_count ) * - 1
													ELSE 0
												END ) AS Code_Sniffer,
												( CASE
														WHEN uce.user_commit_check_type_id = \'2\' THEN SUM ( uce.error_count ) * - 1
													ELSE 0
												END ) AS Security,
												( CASE
														WHEN uce.user_commit_check_type_id = \'3\' THEN SUM ( uce.error_count ) * - 1
													ELSE 0
												END ) AS Company_Pattern_Management,
												( CASE
														WHEN SUM ( uce.error_count ) IS NULL THEN 0
													ELSE SUM ( uce.error_count ) * - 1
												END ) AS total_errors_count
											FROM
												user_commit_errors uce
											WHERE
												to_char ( uce.created_on, \'YYYY-MM-DD\' )::DATE BETWEEN \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . '\' AND \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\'
											GROUP BY
												uce.user_commit_check_type_id,
												uce.employee_id
									) sub1
								GROUP BY
									sub1.employee_id';

		$strJoinOfEmployeeByAddsAndUserCommitErrors = 'SELECT
															sub.employee_id,
															sub.preferred_name,
															sub.name,
															( CASE
																	WHEN SUM ( sde.svn_commits ) IS NOT NULL THEN SUM ( sde.svn_commits )
																ELSE 0
															END ) AS svn_commits,
															( CASE
																	WHEN SUM ( sde.features_released ) IS NOT NULL THEN SUM ( sde.features_released )
																ELSE 0
															END ) AS features_released,
															( CASE
																	WHEN SUM ( sde.bugs_resolved ) IS NOT NULL THEN SUM ( sde.bugs_resolved )
																ELSE 0
															END ) AS bugs_resolved,
															uce1.code_sniffer,
															uce1.security,
															uce1.company_pattern_management,
															CASE WHEN uce1.total_errors_count IS NULL THEN 0 ELSE uce1.total_errors_count END
														FROM ( ' . $strFetchEmployeesbyAdds . ' ) sub
															LEFT JOIN ( ' . $strFetchUserCommitErrorsByDate . ' ) uce1 ON ( uce1.employee_id = sub.employee_id )
															LEFT JOIN stats_developer_employees sde ON ( sub.employee_id = sde.employee_id AND to_char ( sde.week, \'YYYY-MM-DD\' )::DATE BETWEEN \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . '\' AND \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\' )
														GROUP BY
															sub.employee_id,
															sub.preferred_name,
															sub.name,
															uce1.code_sniffer,
															uce1.security,
															uce1.company_pattern_management,
															uce1.total_errors_count';

		$strSql = '
					SELECT
						sub2.employee_id AS employee_id,
						sub2.preferred_name,
						sub2.name AS designation,
						sub2.svn_commits,
						sub2.features_released,
						sub2.bugs_resolved,
						( CASE
							WHEN sub2.code_sniffer IS NULL THEN 0
							ELSE sub2.code_sniffer
						END ) AS code_sniffer,
						( CASE
							WHEN sub2.security IS NULL THEN 0
							ELSE sub2.security
						END ) AS security,
						( CASE
							WHEN sub2.company_pattern_management IS NULL THEN 0
							ELSE sub2.company_pattern_management
						END ) AS company_pattern_management,
						( CASE
							WHEN sub2.total_errors_count IS NULL THEN 0
							ELSE sub2.total_errors_count
						END ) AS total_errors_count,
						( CASE
							WHEN sub2.svn_commits > 0 THEN round( ( ( ( sub2.svn_commits + ( sub2.total_errors_count ) ) / sub2.svn_commits::FLOAT ) * 100 )::NUMERIC, 2 )
							ELSE 0.00
						END ) AS percentage
					FROM
					( ' . $strJoinOfEmployeeByAddsAndUserCommitErrors . ') sub2
					ORDER BY
					' . $strOrderBy . ' ' . $strOrderType;

		if( false == $boolIsCount && false == $boolIsDownload ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$arrmixResult = fetchData( $strSql, $objAdminDatabase );

		return ( true == $boolIsCount ) ? \Psi\Libraries\UtilFunctions\count( $arrmixResult ) : $arrmixResult;

	}

	public static function fetchEmployeeDetailsByIdsWithTeamNameAndDesignationName( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						e.*,
						d.name AS designation_name,
						t.name AS team_name,
						t.preferred_name AS team_preferred_name,
						ea1.employee_application_status_type_id
					FROM
						employees e
						JOIN designations d on ( d.id = e.designation_id )
						JOIN team_employees te on ( te.employee_id = e.id )
						JOIN teams t on ( t.id = te.team_id )
						LEFT JOIN employee_applications ea1 ON ( ea1.employee_id = e.id )
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesByEmployeeStatusTypeIdByCountryCode( $objDatabase ) {

		$strSql = ' SELECT
						e.id,
						e.employee_number,
						CASE
							WHEN e.preferred_name IS NULL THEN
								e.name_full
							ELSE
								e.preferred_name
						END as name_full,
						e.email_address,
						e.work_station_ip_address,
						e.dns_handle,
						u.svn_username,
						u.username,
						d.name AS department,
						ea.country_code
					FROM employees e
						LEFT OUTER JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						LEFT OUTER JOIN departments d ON ( d.id = e.department_id )
						LEFT OUTER JOIN users u ON ( e.id = u.employee_id )
					WHERE e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
					ORDER BY
						e.name_first,d.name, e.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByGroupIdByCountryCode( $intGroupId, $objDatabase, $strCountryCode = NULL ) {
		if( true == is_null( $intGroupId ) ) {
			return NULL;
		}

		$strWhereCondition = ( false == is_null( $strCountryCode ) ) ? ' AND ea.country_code = \'' . $strCountryCode . '\'' : '';

		$strSql = 'SELECT
						DISTINCT ( e.* ),
						des.name AS designation_name,
						dep.name AS department_name,
						epn.phone_number
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						JOIN users u ON ( e.id = u.employee_id )
						JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
						LEFT JOIN designations AS des ON ( des.id = e.designation_id )
						LEFT JOIN departments AS dep ON ( dep.id = e.department_id )
						LEFT JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' )
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND ug.group_id = ' . ( int ) $intGroupId . $strWhereCondition;

		return CEmployees::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchEmployeeNamesByPhoneNumber( $arrmixPhoneNumber, $objDatabase ) {

		$strSql = 'SELECT
						e.preferred_name,
						epn.phone_number
					FROM
						employees as e
					JOIN
						employee_phone_numbers AS epn ON (e.id = epn.employee_id AND
						phone_number_type_id = 1 )
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND epn.phone_number IN ( \'' . implode( '\',\'', $arrmixPhoneNumber ) . '\' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTpmEmployees( $objDatabase, $arrintAddEmployeeIds = NULL ) {
		$strCondition = NULL;
		if( true == valArr( $arrintAddEmployeeIds ) ) {
			$strCondition = 'AND e.id NOT IN ( ' . implode( ',', $arrintAddEmployeeIds ) . ' )';
		}

		$strSql = '	SELECT
						DISTINCT e.id,
						e.preferred_name
					FROM
						employees e
						JOIN teams t ON ( e.id = t.tpm_employee_id )
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . $strCondition . '
					ORDER BY
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchFruitionBoxSummaryReport( $intAddEmployeeId, $strFromDate, $strToDate, $strOrderBy, $strOrderType, $objPagination, $objDatabase, $boolDownload = false ) {

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit  = ( int ) $objPagination->getPageSize();
		}

		$strDateCondition = '\'' . date( 'Y-m-d', strtotime( $strFromDate ) ) . '\'::DATE AND \'' . date( 'Y-m-d', strtotime( $strToDate ) ) . '\'::DATE';

		$strSql = 'WITH team_add_employees AS ( SELECT
													e.id AS employee_id,
													e.reporting_manager_id AS manager_employee_id,
													t.name,
													d.name AS designation,
													u.id AS user_id,
													e.name_full as employee_name
												FROM
													teams t
													JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
													JOIN employees e ON ( te.employee_id = e.id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
													JOIN designations d ON ( e.designation_id = d.id )
													JOIN users u ON ( e.id = u.employee_id )
												WHERE
													add_employee_id = ' . ( int ) $intAddEmployeeId . '
													AND d.id <> ' . CDesignation::QA_MANAGER . '
					), goals_count AS ( SELECT
											count ( st.id ) AS goals_cnt,
											employee_id
										FROM
											survey_tasks st
											JOIN tasks t ON ( t.id = st.task_id )
											JOIN users u ON ( t.user_id = u.id )
											JOIN employees e ON ( u.employee_id = e.id )
										WHERE
											t.task_status_id = ' . CTaskStatus::COMPLETED . '
											AND t.task_type_id = ' . CTaskType::TO_DO . '
											AND t.completed_on <= t.due_date
											AND t.completed_on::DATE BETWEEN ' . $strDateCondition . '
											AND e.id IN ( SELECT employee_id FROM team_add_employees )
										GROUP BY
											employee_id
					), user_err_count AS( SELECT
											sum ( error_count * -1 ) AS total_err_cnt,
											employee_id AS emp_id
										FROM
											user_commit_errors uce
										WHERE
											uce.employee_id IN ( SELECT employee_id FROM team_add_employees )
											AND uce.created_on::DATE BETWEEN ' . $strDateCondition . '
										GROUP BY employee_id
					), cpa_count AS ( SELECT
											sum ( points * -1 ) AS cpa_points,
											employee_id AS employee_id
										FROM
											stats_employee_performances sep
										WHERE sep.employee_id IN ( SELECT employee_id FROM team_add_employees )
											AND sep.intimated_date::DATE BETWEEN ' . $strDateCondition . '
										GROUP BY
											employee_id
					), svn_commits AS ( SELECT
											sum ( svn_commits ) AS total_svn_commits,
											sum ( features_released ) AS total_features_released,
											sum ( bugs_resolved ) AS total_bugs_resolved,
											employee_id AS employee_id
										FROM
											stats_developer_employees sde
										WHERE sde.employee_id IN ( SELECT employee_id FROM team_add_employees )
											AND to_char ( sde.week, \'YYYY-MM-DD\' )::DATE BETWEEN ' . $strDateCondition . '
										GROUP BY
											employee_id
					)	SELECT
							DISTINCT tae.user_id,
							tae.employee_id,
							tae.employee_name,
							tae.designation,
							( CASE WHEN gc.goals_cnt > 0 THEN gc.goals_cnt ELSE 0 END ) AS goals_cnt,
							( CASE WHEN uec.total_err_cnt > 0 THEN uec.total_err_cnt ELSE 0 END ) AS total_err_cnt,
							( CASE WHEN cc.cpa_points > 0 THEN cc.cpa_points ELSE 0 END ) AS cpa_points,
							( CASE WHEN sc.total_svn_commits > 0 THEN sc.total_svn_commits ELSE 0 END ) AS total_svn_commits,
							( CASE WHEN sc.total_features_released > 0 THEN sc.total_features_released ELSE 0 END ) AS total_features_released,
							( CASE WHEN sc.total_bugs_resolved > 0 THEN sc.total_bugs_resolved ELSE 0 END ) AS total_bugs_resolved
						FROM
							team_add_employees tae
							JOIN goals_count gc ON ( gc.employee_id = tae.employee_id )
							LEFT JOIN user_err_count uec ON ( uec.emp_id = tae.employee_id )
							LEFT JOIN cpa_count cc ON ( cc.employee_id = tae.employee_id )
							LEFT JOIN svn_commits sc ON ( sc.employee_id = tae.employee_id )
							ORDER BY ' . $strOrderBy . ' ' . $strOrderType;

		if( false == $boolDownload ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$arrmixData = fetchData( $strSql, $objDatabase );

		return $arrmixData;
	}

	public static function fetchSummaryReportCount( $intAddEmployeeId, $strStartDate, $strEndDate, $objDatabase ) {

		$strSql = 'SELECT
						count( distinct u.employee_id )
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN employees e ON ( te.employee_id = e.id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN designations d ON ( e.designation_id = d.id )
						JOIN users u ON ( e.id = u.employee_id )
						JOIN tasks ta ON ( ta.user_id = u.id AND ta.task_status_id = ' . CTaskStatus::COMPLETED . ' AND ta.completed_on IS NOT NULL AND ta.completed_on <= ta.due_date AND ta.task_type_id = ' . CTaskType::TO_DO . ' )
						JOIN survey_tasks st ON ( st.task_id = ta.id )
					WHERE
						add_employee_id = ' . ( int ) $intAddEmployeeId . '
						AND d.id <> ' . CDesignation::QA_MANAGER . ' AND ta.completed_on BETWEEN \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . '\'::DATE AND \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\'::DATE';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return $arrmixData[0]['count'];
	}

	public static function fetchAllEmployeesByEmployeeStatusTypeIds( $arrintEmployeeStatusTypeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						e.*
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id )
					WHERE
						e.employee_status_type_id IN ( ' . implode( ',', $arrintEmployeeStatusTypeIds ) . ' )
					ORDER BY
						ea.country_code DESC, e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchReportingManagerTPMAndADDByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT e.id,
						e.preferred_name,
						ea.country_code
					FROM
						employees e
					JOIN team_employees te ON ( te.employee_id = ' . ( int ) $intEmployeeId . ')
					JOIN employees e1 ON ( te.employee_id = e1.id )
					JOIN teams t ON ( t.id = te.team_id )
					JOIN employee_addresses ea ON ( ea.employee_id = e.id )
					WHERE
						e.id IN( e1.reporting_manager_id, t.add_employee_id, t.tpm_employee_id )
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND te.is_primary_team = 1';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeePreferredNameByUserId( $intUserId, $objDatabase ) {
		if( true == is_null( $intUserId ) || false == is_numeric( $intUserId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.preferred_name
					FROM
						employees e, users u
					WHERE
						e.id = u.employee_id 
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND u.id =' . ( int ) $intUserId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeePreferredNameById( $intEmployeeId, $objAdminDatabase ) {
		if( true == is_null( $intEmployeeId ) || false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						preferred_name
					FROM
						employees
					WHERE
						date_terminated IS NULL
						AND employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						 AND id = ' . ( int ) $intEmployeeId;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchPsProductEmployeesDataWithEmployeeId( $intPsProductId, $objAdminDatabase ) {

		$strSql = 'SELECT
						e.preferred_name as employee_name
					FROM
						employees e
						LEFT JOIN ps_product_employees ppe ON( ppe.employee_id = e.id )
					WHERE
						ppe.ps_product_id = ' . ( int ) $intPsProductId . ' 
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND ppe.ps_product_option_id IS NULL
						AND ppe.ps_product_employee_type_id != ' . CPsProductEmployeeType::REPORTING_ANALYST;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchAssociatedBillingEmployees( $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT (pld.billing_employee_id),
						e.id,
						e.preferred_name
					FROM clients c
						JOIN ps_lead_details pld ON( c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' AND c.id = pld.cid )
						JOIN employees e ON e.id = pld.billing_employee_id
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByManagerIdByTeamIds( $intManagerEmployeeId, $arrintTeamIds, $objDatabase, $arrintManagerTeamsIds = NULL, $intDepartmentId, $arrintStatusTypeIds ) {
		if( false == valArr( $arrintTeamIds ) || false == is_numeric( $intManagerEmployeeId ) ) {
			return false;
		}
			$strSubSql		= ( true == valId( $intDepartmentId ) ) ? ' AND e.department_id = ' . $intDepartmentId : '';
			$strStatusSql	= ( true == valArr( $arrintStatusTypeIds ) ) ? ' e.employee_status_type_id IN (' . implode( ',', $arrintStatusTypeIds ) . ') AND ' : '';
			$strSelect		= ( true == valArr( $arrintManagerTeamsIds ) ) ? 'CASE WHEN ( te.team_id IN ( ' . implode( ',', $arrintManagerTeamsIds ) . ' ) OR te.employee_id = ' . ( int ) $intManagerEmployeeId . ' ) THEN 1 ELSE 0 END' : '0';
			$strSql = 'SELECT
						DISTINCT e.id,
						e.preferred_name,
						e.employee_number,
						te.team_id,
						 ' . $strSelect . ' as edit_access,
						 e.employee_status_type_id
					FROM
						employees e
						JOIN employee_addresses ea on ( e.id = ea.employee_id AND ea.address_type_id = 1 )
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN office_shift_associations osa ON ( te.team_id = osa.team_id )
					WHERE
						' . $strStatusSql . '
						ea.country_code = \'' . CCountry::CODE_INDIA . '\'
						AND ( te.team_id IN (' . implode( ',', $arrintTeamIds ) . ')
						OR te.employee_id = ' . ( int ) $intManagerEmployeeId . ' )
						AND osa.deleted_by IS NULL
						' . $strSubSql . '
					ORDER BY
						e.preferred_name';
			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllADDs( $objDatabase ) {

		$strSql = 'SELECT
						e.id,
						e.preferred_name,
						e.name_full,
						t.name
					FROM
						teams t
						INNER JOIN employees e ON ( t.add_employee_id = e.id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' ';

		return self::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchAllQaManagers( $objDatabase ) {

		$strSql = 'SELECT
						e.id,
						e.preferred_name,
						e.name_full,
						e.email_address,
						p.name
					FROM
						ps_products p
						JOIN employees e ON ( p.qam_employee_id = e.id )
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeWithDesignationAndDepartmentByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = '
					SELECT
						e.id as employee_id, e.name_first, e.name_last, e.email_address, dp.name as department_name, ds.name as designation_name, epn.phone_number
					FROM
						employees e
						LEFT JOIN departments dp on e.department_id = dp.id
						LEFT JOIN designations ds on e.designation_id = ds.id
						LEFT JOIN employee_phone_numbers epn on epn.employee_id = e.id AND epn.phone_number_type_id = 1
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id = ' . ( int ) $intEmployeeId;

		$arrmixEmployee = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrmixEmployee[0] ) ) ? $arrmixEmployee[0] : NULL;
	}

	public static function fetchAllSvnEmployees( $objAdminDatabase ) {

		$strSql = ' SELECT
						DISTINCT ON (e.id) e.id,
						e.name_full as employee_name,
						e.employee_status_type_id as employee_status_type_id,
						u.id as user_id,
						emp.id as manager_id,
						emp.name_full as manager_name,
						emp.employee_status_type_id as manager_status_type_id
					FROM
						employees e
						JOIN users u ON (e.id = u.employee_id)
						LEFT JOIN employees emp ON ( e.reporting_manager_id = emp.id )
						JOIN departments d ON (d.id = e.department_id)
					WHERE
						u.svn_username IS NOT NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						OR u.svn_password IS NOT NULL
						OR d.id IN ( ' . CDepartment::DEVELOPMENT . ', ' . CDepartment::QA . ' )';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchAssociatedSystemErrorEmployeesById( $intEmployeeId, $objAdminDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						t.sdm_employee_id,
						u.id as user_id,
						te.employee_id,
						e.department_id
					FROM
						employees e
						JOIN users u ON ( u.employee_id = e.id )
						JOIN team_employees te ON ( te.employee_id = u.employee_id )
						JOIN teams t ON ( te.team_id = t.id)
					WHERE
						te.is_primary_team = 1
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND u.employee_id = ' . $intEmployeeId;

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchEmployeeDetailsByIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						u.id as user_id,
						e.id,
						u.username,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.email_address,
						e.name_full
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEmailAddressByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id as employee_id,
						e.email_address
					FROM
						employees e
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' 
						 AND e.id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )
					GROUP BY e.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublicKeysByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		$strSql = 'SELECT
					e.id,
					e.preferred_name,
					e.public_key
				FROM
					employees e
				WHERE
					 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					AND e.id IN( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeIdsAndUserIdsByDesignationId( $intDesignationId, $objDatabase ) {
		$strSql = 'SELECT u.id AS user_id,e.id AS employee_id
					FROM employees e
					JOIN users u on (u.employee_id = e.id)
					WHERE e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND e.designation_id = ' . ( int ) $intDesignationId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByPayrollIds( $arrstrPayrollIds, $objDatabase ) {
		if( false == valArr( $arrstrPayrollIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*
					FROM
						employees e
					WHERE
						e.payroll_remote_primary_key IN ( \'' . implode( "','", $arrstrPayrollIds ) . '\' )
						AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByRoleIdByIsSuperUser( $intRoleId, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT ON ( e.name_full )
						e.*
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
						LEFT JOIN user_roles ur ON ( u.id = ur.user_id AND ur.deleted_on IS NULL )
					WHERE
						( u.is_super_user = 1 OR ur.role_id = ' . ( int ) $intRoleId . ' )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesByDesignationIds( $arrintDesignationIds, $objDatabase ) {

		if( false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
						e.employee_status_type_id =' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ')
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAddEmployeesData( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT e.*,
						u.id as user_id
					FROM
						team_employees te
						LEFT JOIN teams t ON ( t.id = te.team_id )
						LEFT JOIN employees e ON ( e.id = t.add_employee_id )
						JOIN employee_addresses ea ON ( t.add_employee_id = ea.employee_id AND ea.address_type_id = 1 AND ea.country_code = \'IN\' )
						left join users u ON (e.id = u.employee_id)
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY
						e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDoeEmployeesData( $boolIsDoeList = false, $objDatabase ) {

		$strCondition = ' )';
		if( true == $boolIsDoeList ) {
			$strCondition = ' designation_id IN ( ' . implode( ',', CDesignation::$c_arrintDirectorsOfEngineeringDesignations ) . ' ) )';
		}

		$strSql = ' SELECT
						e.id,
						e.preferred_name,
						e.email_address,
						u.id as user_id
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
					WHERE
						( ' . $strCondition . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetcSupportEmployeesByCidsByCompanyStatusTypeIds( $arrintCids, $arrintCompanyStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						c.id,
						c.company_name,
						e.name_full as employee_name
					FROM
						clients c
						LEFT JOIN ps_lead_details pld on ( c.id = pld.cid )
						LEFT JOIN employees e ON ( pld.support_employee_id = e.id )
					WHERE
						company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND cid IN ( ' . implode( ',', $arrintCids ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEmailAddressByUserId( $intUserId, $objDatabase, $boolAllDetails = false ) {

		if( false == is_numeric( $intUserId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.name_first,
						e.name_last,
						e.email_address
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id)
					WHERE
						u.id = ' . ( int ) $intUserId . ' ';

		$arrmixEmployee = fetchData( $strSql, $objDatabase );

		if( true == $boolAllDetails ) {
			return $arrmixEmployee;
		}

		return ( true == isset( $arrmixEmployee[0]['email_address'] ) ) ? $arrmixEmployee[0]['email_address'] : NULL;
	}

	public static function fetchEmployeeByIdWithNameAndEmailSignature( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						name_full,
						CASE WHEN email_signature IS NOT NULL THEN email_signature
						ELSE name_full || \'</br> \' || email_address
						END as email_signature
					FROM employees
					WHERE employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND id = ' . ( int ) $intEmployeeId;

		return self::fetchEmployee( $strSql, $objDatabase );
	}

	public static function fetchEmployeeIdsByEmailIds( $arrstrEmployeeEmailIds, $objAdminDatabase ) {
		if( false == valArr( $arrstrEmployeeEmailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						quote_literal ( e.email_address ) AS email_address
					FROM
						employees e
					WHERE
						 e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.email_address IN ( ' . implode( ',', $arrstrEmployeeEmailIds ) . ' )
					ORDER BY
						e.id ASC';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeDataByEmployeeId( $intEmployeeId, $objAdminDatabase ) {
		if( true == is_null( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*,
						d.name as designation
					FROM
						employees e
						LEFT JOIN designations d ON ( d.id = e.designation_id )
					WHERE
						e.id = ' . ( int ) $intEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		$arrmixData = fetchData( $strSql, $objAdminDatabase );

		return $arrmixData[0];
	}

	public static function fetchTeamEmployeesByHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId, $objDatabase, $strStartDate = NULL, $strEndDate = NULL ) {

		if( true == is_null( $intHrRepresentativeEmployeeId ) || false == is_numeric( $intHrRepresentativeEmployeeId ) ) {
			return NULL;
		}
		$strWhereCondition = ( false == empty( $strStartDate ) && false == empty( $strEndDate ) ) ? '( e.date_terminated IS NULL OR e.date_terminated BETWEEN \'' . $strStartDate . ' 00:00:00 \' AND \'' . $strEndDate . ' 23:59:59 \' )' : 'e.date_terminated IS NULL AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		$strSql = ' SELECT
						DISTINCT e.*
					FROM
						employees AS e
						JOIN team_employees AS te on( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN teams AS t ON ( te.team_id = t.id )
					WHERE
						' . $strWhereCondition . '
						AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						name_first,
						name_last ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchLateStayFemaleEmployees( $objDatabase ) {

		$strSql = 'SELECT
							sub_query.employee_number AS employee_number,
							sub_query.name_full AS name_full,
							sub_query.name AS name,
							sub_query.manager_name AS manager_name,
							sub_query.desk_number AS desk_number
						FROM
							(
							SELECT
								e.employee_number,
								e.name_full,
								t.name,
								eh.id AS employee_hours_id,
								eh.end_datetime,
								e1.name_full AS manager_name,
								od1.desk_number,
								row_number ( ) over ( partition BY e.id
							ORDER BY
								eh.created_on DESC ) AS row_number
							FROM
								employees e
								LEFT JOIN team_employees te ON ( te.employee_id = e.id )
								LEFT JOIN teams t ON ( t.id = te.team_id AND t.team_type_id = 1 )
								LEFT JOIN office_desks od1 ON ( e.office_desk_id = od1.id )
								LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id )
								LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
								LEFT JOIN employee_hours eh ON ( eh.employee_id = e.id )
							WHERE
								e.gender = \'F\'
								AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
								AND te.is_primary_team = 1
								AND ea.country_code = \'IN\'
								AND ea.address_type_id = 1
								AND eh.date = \'' . date( 'm/d/Y', time() ) . '\'
							) AS sub_query
						WHERE
							row_number = 1
							AND end_datetime IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDetails( $objDatabase, $strCountryCode = NULL ) {
		$strWhere = '';
		if( CCountry::CODE_USA == $strCountryCode ) {
			$strWhere = ' AND e.designation_id = ' . CDesignation::HR_DIRECTOR . ' AND e.employee_status_type_id = 1 ';
		} else if( CCountry::CODE_INDIA == $strCountryCode || true == is_null( $strCountryCode ) ) {
			$strWhere = ' AND e.designation_id = ' . CDesignation::HEAD_TALENT_ACQUISITION;
		}

		$strSql = 'SELECT
						*
					FROM
						employees e
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . $strWhere . '
					ORDER BY
						e.created_on ASC';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return $arrmixData[0];
	}

	public static function fetchEmployeeEmailAddressesByDesignationIds( $arrintDesignationIds, $objDatabase ) {

		if( false == valArr( $arrintDesignationIds ) ) {
			return;
		}

		$strSql = 'SELECT
						e.email_address
					FROM
						employees e
						JOIN designations d ON ( d.id = e.designation_id )
					WHERE
						d.id IN (' . implode( ', ', $arrintDesignationIds ) . ' )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchVoxEmployeeDataChanges( $strLastUpdatedOn, $objDatabase, $arrintSwagStoreAccessEmployeeIds, $arrintSwagStoreUnAccessEmployeeIds ) {
		if( false == valStr( $strLastUpdatedOn ) ) {
			return NULL;
		}
		$strAdditionalCondition = '';
		if( true == valArr( $arrintSwagStoreAccessEmployeeIds ) ) {
			$strAdditionalCondition .= ' AND id NOT IN( ' . implode( ',', $arrintSwagStoreAccessEmployeeIds ) . ' )';
		}
		if( true == valArr( $arrintSwagStoreUnAccessEmployeeIds ) ) {
			$strAdditionalCondition .= ' OR ( id IN( ' . implode( ',', $arrintSwagStoreUnAccessEmployeeIds ) . ' ) )';
		}
		$strSql = ' SELECT
						e.*,
						d.name as department_name,
						ds.name as designation_name,
						e1.preferred_name as manager_employee_name,
						e1.id AS manager_employee_id
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id and ea.address_type_id = ' . CAddressType::PRIMARY . ' and ea.country_code = \'US\'  )
						JOIN departments d ON d.id = e.department_id
						JOIN designations ds ON ds.id = e.designation_id
						LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
					WHERE ( e.updated_on >= \'' . $strLastUpdatedOn . '\' OR e.date_terminated >= \'' . $strLastUpdatedOn . '\' )
						AND e.payroll_remote_primary_key IS NOT NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND e.id NOT IN (
										SELECT
												id
										FROM
											employees
											WHERE office_id IN(' . implode( ',', COffice::$c_arrintSwagStoreExcludeOfficeIds ) . ')
											' . $strAdditionalCondition . '
										)';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchVoxEmployeeDataFullRefresh( $strTerminatedAfterDate, $objDatabase, $arrintSwagStoreAccessEmployeeIds, $arrintSwagStoreUnAccessEmployeeIds ) {
		if( false == valStr( $strTerminatedAfterDate ) ) {
			return NULL;
		}
		$strAdditionalCondition = '';

		if( true == valArr( $arrintSwagStoreAccessEmployeeIds ) ) {
			$strAdditionalCondition .= ' AND id NOT IN( ' . implode( ',', $arrintSwagStoreAccessEmployeeIds ) . ' )';
		}
		if( true == valArr( $arrintSwagStoreUnAccessEmployeeIds ) ) {
			$strAdditionalCondition .= ' OR ( id IN( ' . implode( ',', $arrintSwagStoreUnAccessEmployeeIds ) . ' ) )';
		}
		$strSql = ' SELECT
						e.*,
						 d.name as department_name,
						ds.name as designation_name,
						e1.preferred_name as manager_employee_name,
						e1.id AS manager_employee_id
						FROM employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id and ea.address_type_id = ' . CAddressType::PRIMARY . ' and ea.country_code = \'US\'  )
						JOIN departments d ON d.id = e.department_id
						JOIN designations ds ON ds.id = e.designation_id
						LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
					WHERE ( e.date_terminated IS NULL OR e.date_terminated >= \'' . $strTerminatedAfterDate . '\')
							AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
							AND e.payroll_remote_primary_key IS NOT NULL
							AND e.id NOT IN (
											SELECT
												id
											FROM
												employees
											WHERE office_id IN(' . implode( ',', COffice::$c_arrintSwagStoreExcludeOfficeIds ) . ')
											' . $strAdditionalCondition . '
									)';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchVoxEmployeeDataFullTermination( $strTermDate, $objDatabase ) {

		$strSql = ' SELECT
						e.*,
						 d.name as department_name,
						ds.name as designation_name,
						e1.preferred_name as manager_employee_name,
						e1.id AS manager_employee_id
						FROM employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id and ea.address_type_id = ' . CAddressType::PRIMARY . ' and ea.country_code = \'US\'  )
						JOIN departments d ON d.id = e.department_id
						JOIN designations ds ON ds.id = e.designation_id
						LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
					WHERE  e.date_terminated IS NOT NULL AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND e.date_terminated >= \'' . $strTermDate . '\'';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchSdmEmployees( $objDatabase ) {

		$strSql = 'SELECT
				DISTINCT
					e.id,
					e.name_full
				FROM
					tasks t
					LEFT JOIN ps_products pp ON t.ps_product_id = pp.id
					LEFT JOIN ps_product_options ppo ON t.ps_product_option_id = ppo.id
					JOIN employees e ON e.id = COALESCE( t.project_manager_id, ppo.sdm_employee_id, pp.sdm_employee_id )
				WHERE
					e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
				ORDER BY
					e.name_full;';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeIdsByDesignationIdsByStateCodes( $arrintDesignationIds, $arrstrStateCodes = NULL, $objDatabase ) {

		$strWhereDesignationCond = '';
		$strWhereStateCond       = '';
		$strAndCond              = '';
		if( false == valArr( $arrintDesignationIds ) && false == valArr( $arrstrStateCodes ) ) {
			return NULL;
		}

		if( true == valArr( $arrintDesignationIds ) ) {
			$strWhereDesignationCond .= 'e.designation_id IN ( \'' . implode( '\', \'', $arrintDesignationIds ) . '\' ) AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW;
			$strAndCond              = 'OR';
		}

		if( true == valArr( $arrstrStateCodes ) ) {
			$strWhereStateCond = $strAndCond . ' ea.state_code IN ( \'' . implode( '\', \'', $arrstrStateCodes ) . '\' )';
		}

		$strSql = 'SELECT
						e.id
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						' . $strWhereDesignationCond . $strWhereStateCond;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDirectorEmployeeIds( $objAdminDatabase ) {
		$strSql = 'SELECT
						e.id AS doe_id
					FROM
						ps_products pp
					JOIN unnest ( ARRAY [ 1, 2 ] ) pp1 ON TRUE
					LEFT JOIN ps_product_options ppo ON ( pp.id = ppo.ps_product_id AND pp1 = 1 )
					LEFT JOIN ps_product_employees ppe ON ppe.ps_product_id = pp.id AND ppe.ps_product_option_id IS NULL AND pp1 = 2 AND ppe.ps_product_employee_type_id = 2
					LEFT JOIN employees e ON ( ( ppo.id IS NULL AND pp.doe_employee_id = e.id ) OR ( ppo.id IS NOT NULL AND ppo.doe_employee_id = e.id ) )
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND pp.deleted_on IS NULL
						AND ( pp1 = 2
						OR ppo.id IS NOT NULL )
					GROUP BY
						e.id';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchSqmArchitectsSdmsEmployeeIds( $objAdminDatabase ) {

		$strSql = 'SELECT
						e.id AS sqm_employee_id,
						e1.id AS architect_employee_id,
						e2.id AS sdm_employee_id
					FROM
						ps_products pp
						LEFT JOIN ps_product_options ppo ON ( pp.id = ppo.ps_product_id )
						LEFT JOIN employees e ON ( e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND ( pp.sqm_employee_id = e.id  OR ppo.sqm_employee_id = e.id ) )
						LEFT JOIN employees e1 ON ( e1.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND ( pp.architect_employee_id = e1.id OR ppo.architect_employee_id = e1.id ) )
						LEFT JOIN employees e2 ON ( e2.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND ( pp.sdm_employee_id = e2.id  OR ppo.sdm_employee_id = e2.id ) )
					WHERE
						pp.deleted_on IS NULL
					GROUP BY
						pp.name, e.id,e1.id,e2.id';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchCustomEmployeesByDesignationIdsOrderByNameFull( $arrintDesignationIds, $objDatabase, $boolIncludePastEmployees = false, $arrintAdditionalUsers = NULL ) {

		if( false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strWhere = 'e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		if( true == CStrings::strToBool( $boolIncludePastEmployees ) ) {
			$strWhere = 'e.employee_status_type_id IN ( ' . implode( ', ', [ CEmployeeStatusType::CURRENT, CEmployeeStatusType::PREVIOUS ] ) . ' )';
		}

		$strWhere .= ' AND ( e.designation_id IN ( ' . sqlIntImplode( $arrintDesignationIds ) . ' ) ';

		if( true == valArr( $arrintAdditionalUsers ) ) {
			$strWhere .= ' OR u.id IN ( ' . sqlIntImplode( $arrintAdditionalUsers ) . ' ) ';
		}

		$strWhere .= ')';

		$strSql = 'SELECT
						e.*,
						ea.country_code,
						e.office_desk_id,
						d.name AS department_name,
						ds.name AS designation_name,
						eap.employee_application_status_type_id
					FROM
						employees e
						JOIN users u ON ( u.employee_id = e.id )
						LEFT JOIN departments d ON ( e.department_id = d.id )
						LEFT JOIN designations ds ON ( ds.id = e.designation_id )
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employee_applications eap ON ( eap.employee_id = e.id )
					WHERE
						' . $strWhere . '
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveAndCertainPeriodInactiveEmployeesByCountryCode( $objDatabase, $strCountryCode = '' ) {
		$strSql = 'SELECT
						e.id,
						e.name_full
					FROM
						employees e
						JOIN employee_addresses ea ON ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . '
					WHERE
						( ( e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . ' AND e.date_terminated IS NULL )
						OR ( e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::PREVIOUS . ' AND e.date_terminated > CURRENT_DATE - INTERVAL \'2 months\' ) )';

		if( '' != $strCountryCode ) {
			$strSql .= ' AND ea.country_code = \'' . $strCountryCode . '\'';
		}

		$strSql .= ' ORDER BY
						e.preferred_name ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeeDetailsByDepartmentIdsByIds( $arrintDepartmentIds, $arrintIds, $objDatabase ) {

		if( false == valArr( $arrintDepartmentIds ) || false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						u.id,
						e.name_full
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
					WHERE
						e.id IN ( ' . implode( ', ', $arrintIds ) . ' )
						OR e.department_id IN ( ' . implode( ', ', $arrintDepartmentIds ) . ' )
						AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR date_terminated > NOW() )
					ORDER BY
						name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDesignationIdByUserId( $intUserId, $objDatabase ) {
		$strSql = 'SELECT
						emp.designation_id
					FROM
						employees emp
						JOIN users u ON ( emp.id = u.employee_id )
					WHERE
						u.id = ' . ( int ) $intUserId . '
						AND emp.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ';';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSupportEmployeesWithLeadCount( $objAdminDatabase ) {

		$strSql = 'SELECT
						e.id,
						e.preferred_name::TEXT || \' ( \' || COUNT ( pld.ps_lead_id )::TEXT || \' )\' AS preferred_name
					FROM
						employees e
						JOIN ps_lead_details AS pld ON pld.support_employee_id = e.id
						JOIN ps_leads AS pl ON pl.id = pld.ps_lead_id
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND pl.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND pld.renewal_date IS NOT NULL
					GROUP BY
						e.id,
						e.preferred_name
					ORDER BY
						preferred_name';

		return self::fetchEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchCsmEmployeesByCids( $arrintCids, $objAdminDatabase, $boolReturnEmailAddress = false ) {

		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						e.id,
						e.preferred_name as csm_employee_name,
						e.email_address
					FROM
						employees e
						JOIN ps_lead_details pld ON pld.support_employee_id = e.id
						JOIN clients c ON c.id = pld.cid
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND c.id IN( ' . implode( ',', $arrintCids ) . ' )';

		$arrmixEmployee = fetchData( $strSql, $objAdminDatabase );

		if( true == $boolReturnEmailAddress && true == valArr( $arrmixEmployee ) ) {
			return $arrmixEmployee[0]['email_address'];
		} else {
			return $arrmixEmployee;
		}
	}

	public static function fetchEmployeeTerminationDetailsByDatesRange( $strStartDate, $strEndDate, $objDatabase, $boolDownloadData = false ) {

		if( false == ( valStr( $strStartDate ) || $strEndDate ) ) {
			return NULL;
		}

		if( true == $boolDownloadData ) {
			$strSubSql = 'stq.question, sqa.id';
		} else {
			$strSubSql = 's.id AS survey_id';
		}

		$strSql = 'SELECT
						DISTINCT e.employee_number AS employee_id,
						e.preferred_name AS employee_name,
						ear.answer as reason_for_leaving,
						e.expected_termination_date AS last_working_day,
						' . $strSubSql . '
					FROM
						employees e
						LEFT JOIN employee_assessments ea ON (e.id = ea.employee_id)
						LEFT JOIN surveys s ON ( s.subject_reference_id = e.id )
						LEFT JOIN survey_question_answers sqa ON ( sqa.survey_id = s.id )
						LEFT JOIN survey_template_questions stq ON ( sqa.survey_template_question_id = stq.id )
						LEFT JOIN employee_assessment_employees eae ON ( eae.employee_id = e.id )
						LEFT JOIN employee_assessment_responses ear ON ( ear.employee_assessment_id = ea.id )
						JOIN users AS u ON ( u.employee_id = e.id )
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND e.expected_termination_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
						AND sqa.survey_template_id = ' . CSurveyTemplate::EXIT_INTERVIEW . '
						AND ( ear.employee_assessment_question_id = ' . CEmployeeAssessmentQuestion::EXIT_PROCESS_EMPLOYEE_QUESTION_ONE_ID . ' AND ear.created_by <> u.id )
					ORDER BY
						e.expected_termination_date';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchActiveEmployeeByEmployeeNumber( $intEmployeeNumber, $objAdminDatabase ) {
		$strSql = 'SELECT *
                    FROM employees 
                    WHERE 
                    employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
                    AND date_terminated IS NULL 
                    AND employee_number = ' . ( int ) $intEmployeeNumber;

		return self::fetchEmployee( $strSql, $objAdminDatabase );
	}

	public static function fetchActiveEmployeeByEmployeeNumberByEmailAddress( $intEmployeeNumber, $strEmailAddress, $objAdminDatabase ) {
		if( false == is_numeric( $intEmployeeNumber ) || false == valStr( $strEmailAddress ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						employees e
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.email_address = \'' . $strEmailAddress . '\'
						AND e.employee_number = ' . ( int ) $intEmployeeNumber;

		return self::fetchEmployee( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeAndCompanyEventsByDepartmentIds( $arrintDepartmentIds, $objAdminDatabase ) {

		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strSql = '	WITH employee AS (
										SELECT
											e.name_first AS event_title,
											to_char( NOW(), \'YYYY-\' ) || to_char( e.birth_date, \'mm-dd\' ) AS event_date,
											1 AS event_type
										FROM
											employees AS e
										WHERE
											( e.date_terminated IS NULL OR e.date_terminated > NOW() )
											AND e.employee_status_type_id = 1
											AND e.department_id IN ( ' . CDepartment::PROJECT_MANAGEMENT . ' )
									UNION
										SELECT
											e.name_first AS event_title,
											to_char( NOW(), \'YYYY-\' ) || to_char( e.anniversary_date, \'mm-dd\' ) AS event_date,
											2 AS event_type
										FROM
											employees AS e
										WHERE
											( e.date_terminated IS NULL OR e.date_terminated > NOW() )
											AND e.employee_status_type_id = 1
											AND e.department_id IN ( ' . CDepartment::PROJECT_MANAGEMENT . ' )
									UNION
										SELECT
											title AS event_title,
											CASE
												WHEN
													start_datetime < CURRENT_DATE AND end_datetime IS NOT NULL AND end_datetime >= CURRENT_DATE
												THEN
													to_char ( NOW(), \'YYYY-mm-dd\' )
												WHEN
													start_datetime < CURRENT_DATE AND end_datetime IS NOT NULL AND end_datetime < CURRENT_DATE
												THEN
													to_char ( end_datetime, \'YYYY-mm-dd\' )
												ELSE
													to_char ( start_datetime, \'YYYY-mm-dd\' )
											END AS event_date,
											3 AS event_type
										FROM
											ps_events AS pe
										WHERE
											pe.deleted_on IS NULL
											AND pe.ps_event_type_id <> ' . CPsEventType::ANNOUNCEMENT . '
											AND show_in_client_admin = TRUE
											AND ( ( ( ( CURRENT_DATE - to_char( pe.start_datetime, \'YYYY/MM/DD\' ) ::date ) <= 60 ) )
											OR ( ( pe.end_datetime >= CURRENT_DATE ) AND ( pe.start_datetime <= CURRENT_DATE ) ) )
										ORDER BY
											event_date DESC
									)
					SELECT
						subsql.event_title,
						subsql.event_date,
						subsql.event_type
					FROM(
							(SELECT
								event_title,
								to_char( event_date::date, \'Dy\' ) || \' \' || to_char( event_date::date, \'FMMM-DD\' ) AS event_date,
								event_type,
								event_date AS date
							FROM
								employee
							WHERE
								event_date::date < NOW()::date
							ORDER BY
								date DESC
							LIMIT 3)
						UNION
							(SELECT
								event_title,
								to_char( event_date::date, \'Dy\' ) || \' \' || to_char( event_date::date, \'FMMM-DD\' ) AS event_date,
								event_type,
								event_date AS date
							FROM
								employee
							WHERE
								event_date::date >= NOW()::date
							ORDER BY
								date
							LIMIT 10)
						) AS subsql
					ORDER BY subsql.date';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchActiveEmployeesDataForFaceGame( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT emp.id,
						emp.preferred_name AS emp_name,
						teams.name AS team_name,
						CASE
							WHEN dep.name IN ( \'Accounts\', \'Administration\', \'Data Processing\', \'HR\', \'IT\', \'Training & Development\' ) THEN dep.name
							ELSE teams.add_employee_id::TEXT
						END AS player_category,
						emp.email_address AS email,
						emp.gender,
						dep.name AS department,
						des.name AS designation,
						teams.manager_employee_id AS manager_id
					FROM
						employees emp
						LEFT JOIN designations des ON ( des.id = emp.designation_id )
						LEFT JOIN departments dep ON ( dep.id = emp.department_id )
						LEFT JOIN team_employees TE ON TE.employee_id = emp.id AND TE.is_primary_team = 1
						LEFT JOIN teams ON teams.id = TE.team_id
						LEFT JOIN employee_addresses ON emp.id = employee_addresses.employee_id
					WHERE
						emp.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ')
						AND employee_addresses.country_code <> \'' . CCountry::CODE_USA . '\'
						AND emp.email_address IS NOT NULL
				ORDER BY
						emp.id ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSearchedStakeholders( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSearchCriteria = '';
		if( true == valArr( $arrstrFilteredExplodedSearch, 1 ) && true == isset( $arrstrFilteredExplodedSearch[0] ) && true == is_numeric( $arrstrFilteredExplodedSearch[0] ) ) {
			$strSearchCriteria = '	OR e.id = ' . ( int ) $arrstrFilteredExplodedSearch[0] . '
									OR e.employee_number = ' . ( int ) $arrstrFilteredExplodedSearch[0];
		}

		$strSql = 'SELECT
						DISTINCT e.id,
						e.preferred_name,
						d.name
					FROM
						employees e
						JOIN departments d ON ( d.id = e.department_id )
					WHERE
						e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . '
						AND ( e.name_first ILIKE \'%' . implode( '%\' AND e.name_first ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_last ILIKE \'%' . implode( '%\' AND e.name_last ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						' . $strSearchCriteria . '
						OR e.email_address ILIKE \'%' . implode( '%\' AND e.email_address ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
					ORDER BY
						e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function calculateAndLoadEligibleReviewsForUsEmployees( $intEmployeeManagerId, $arrobjTeamEmployees, $arrobjColleaguesEmployees, $intTeamLimit, $intColleaguesLimit, $arrintEmployeesPeerReviewsCount = NULL, $objSession ) {
		$arrobjEmployeeStakeholders = [];

		if( true == valArr( $arrobjColleaguesEmployees ) ) {
			foreach( $arrobjColleaguesEmployees as $objColleaguesEmployee ) {
				if( 0 == $intColleaguesLimit ) {
					break;
				}
				if( $intEmployeeManagerId != $objColleaguesEmployee->getId() ) {

					$boolAllowStakeHolder = true;

					if( true == valArr( $arrintEmployeesPeerReviewsCount ) && true == array_key_exists( $objColleaguesEmployee->getId(), $arrintEmployeesPeerReviewsCount ) && self::PEER_REVIEW_LIMIT <= $arrintEmployeesPeerReviewsCount[$objColleaguesEmployee->getId()]['peer_review_count'] ) {
						$boolAllowStakeHolder = false;
					}

					if( true == $boolAllowStakeHolder ) {
						$arrobjEmployeeStakeholders[$objColleaguesEmployee->getId()] = $objColleaguesEmployee;
					}

					if( true == valArr( $arrintEmployeesPeerReviewsCount ) ) {
						if( true == array_key_exists( $objColleaguesEmployee->getId(), $arrintEmployeesPeerReviewsCount ) ) {
							$arrintEmployeesPeerReviewsCount[$objColleaguesEmployee->getId()]['peer_review_count'] = ++$arrintEmployeesPeerReviewsCount[$objColleaguesEmployee->getId()]['peer_review_count'];
						} else {
							$arrintEmployeesPeerReviewsCount[$objColleaguesEmployee->getId()]['employee_id']       = $objColleaguesEmployee->getId();
							$arrintEmployeesPeerReviewsCount[$objColleaguesEmployee->getId()]['peer_review_count'] = 1;
						}

						$objSession->setValue( [ 'client_admin', 'peer_reviews_count' ], $arrintEmployeesPeerReviewsCount );

					} else {
						$arrintEmployeesPeerReviewsCount[$objColleaguesEmployee->getId()]['employee_id']       = $objColleaguesEmployee->getId();
						$arrintEmployeesPeerReviewsCount[$objColleaguesEmployee->getId()]['peer_review_count'] = 1;
					}

					$intColleaguesLimit--;
				}
			}
		}

		if( true == valArr( $arrobjTeamEmployees ) ) {
			foreach( $arrobjTeamEmployees as $objTeamEmployee ) {
				if( 0 == $intTeamLimit ) {
					break;
				}
				if( $intEmployeeManagerId != $objTeamEmployee->getId() && false == array_key_exists( $objTeamEmployee->getId(), array_keys( $arrobjEmployeeStakeholders ) ) ) {

					$boolAllowStakeHolder = true;

					if( true == valArr( $arrintEmployeesPeerReviewsCount ) && true == array_key_exists( $objTeamEmployee->getId(), $arrintEmployeesPeerReviewsCount ) && self::PEER_REVIEW_LIMIT < $arrintEmployeesPeerReviewsCount[$objTeamEmployee->getId()]['peer_review_count'] ) {
						$boolAllowStakeHolder = false;
					}

					if( true == $boolAllowStakeHolder ) {
						$arrobjEmployeeStakeholders[$objTeamEmployee->getId()] = $objTeamEmployee;
					}

					if( true == valArr( $arrintEmployeesPeerReviewsCount ) ) {
						if( true == array_key_exists( $objTeamEmployee->getId(), $arrintEmployeesPeerReviewsCount ) ) {
							$arrintEmployeesPeerReviewsCount[$objTeamEmployee->getId()]['peer_review_count'] = ++$arrintEmployeesPeerReviewsCount[$objTeamEmployee->getId()]['peer_review_count'];
						} else {
							$arrintEmployeesPeerReviewsCount[$objTeamEmployee->getId()]['employee_id']       = $objTeamEmployee->getId();
							$arrintEmployeesPeerReviewsCount[$objTeamEmployee->getId()]['peer_review_count'] = 1;
						}
						$objSession->setValue( [ 'client_admin', 'peer_reviews_count' ], $arrintEmployeesPeerReviewsCount );
					} else {
						$arrintEmployeesPeerReviewsCount[$objColleaguesEmployee->getId()]['employee_id']       = $objTeamEmployee->getId();
						$arrintEmployeesPeerReviewsCount[$objColleaguesEmployee->getId()]['peer_review_count'] = 1;
					}

					$intTeamLimit--;
				}
			}
		}

		return $arrobjEmployeeStakeholders;
	}

	public static function calculateAndLoadEligibleReviews( $arrobjTeamEmployees, $arrobjColleaguesEmployees, $arrobjEmployees, $intMaxLimit ) {

		$intTeamEmployeesCount       = \Psi\Libraries\UtilFunctions\count( $arrobjTeamEmployees );
		$intColleaguesEmployeesCount = \Psi\Libraries\UtilFunctions\count( $arrobjColleaguesEmployees );

		if( true == isset( $intMaxLimit ) && 0 < ( $intMaxLimit - \Psi\Libraries\UtilFunctions\count( $arrobjEmployees ) ) ) {

			$intMaxLimit = $intMaxLimit - \Psi\Libraries\UtilFunctions\count( $arrobjEmployees );

			if( 0 == $intTeamEmployeesCount && 0 < $intColleaguesEmployeesCount ) {

				foreach( $arrobjColleaguesEmployees as $objColleaguesEmployee ) {
					if( 0 == $intMaxLimit ) {
						break;
					}
					$arrobjEmployees[] = $objColleaguesEmployee;
					$intMaxLimit--;
				}
			} elseif( 0 == $intColleaguesEmployeesCount && 0 < $intTeamEmployeesCount ) {

				foreach( $arrobjTeamEmployees as $objTeamEmployee ) {
					if( 0 == $intMaxLimit ) {
						break;
					}
					$arrobjEmployees[] = $objTeamEmployee;
					$intMaxLimit--;
				}
			} elseif( 0 < $intColleaguesEmployeesCount && 0 < $intTeamEmployeesCount ) {

				$intLimit = ( $intMaxLimit % 2 == 0 ) ? ( $intMaxLimit / 2 ) : floor( $intMaxLimit / 2 ) + 1;

				if( $intLimit > $intTeamEmployeesCount ) {
					$intLimit = $intLimit + ( $intLimit - $intTeamEmployeesCount );
				}

				foreach( $arrobjColleaguesEmployees as $objColleaguesEmployee ) {
					if( 0 == $intLimit ) {
						break;
					}
					$arrobjEmployees[] = $objColleaguesEmployee;
					$intLimit--;
					$intMaxLimit--;
				}

				if( 0 >= $intMaxLimit ) {
					return $arrobjEmployees;
				}

				foreach( $arrobjTeamEmployees as $objTeamEmployee ) {
					if( 0 == $intMaxLimit ) {
						break;
					}
					$arrobjEmployees[] = $objTeamEmployee;
					$intMaxLimit--;
				}
			}
		} else {
			if( true == valArr( $arrobjTeamEmployees ) ) {
				foreach( $arrobjTeamEmployees as $objTeamEmployee ) {
					$arrobjEmployees[] = $objTeamEmployee;
				}
			}
			if( true == valArr( $arrobjColleaguesEmployees ) ) {
				foreach( $arrobjColleaguesEmployees as $objColleaguesEmployee ) {
					$arrobjEmployees[] = $objColleaguesEmployee;
				}
			}
		}

		return $arrobjEmployees;
	}

	public static function fetchAssignedImplementationUsersAndMangers( $objDatabase ) {
		$strSql = ' SELECT
						DISTINCT ON ( e.id ) e.id AS employee_id,
						e.preferred_name AS assigned_user,
						mgrs.id AS manager_id,
						mgrs.preferred_name AS project_manager
					FROM
						employees e
						JOIN employees mgrs ON e.reporting_manager_id = mgrs.id
						JOIN contract_employees ce ON ce.employee_id = e.id AND ce.contract_employee_type_id = ' . CContractEmployeeType::IMPLEMENTATION . '
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
					ORDER BY
						e.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesWithInductionSessions( $objDatabase, $strCountryCode = NULL, $arrintEmployeeIds = NULL, $strSearchEmployee = NULL, $strOrderByField = NULL, $strOrderByValue = NULL ) {

		$strWhere = NULL;

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( true == valArr( $arrintEmployeeIds ) ) {
			$strWhere .= ' AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ';
		}

		if( true == valStr( $strSearchEmployee ) ) {
			$strWhere .= ' AND e.preferred_name ILIKE \'%' . $strSearchEmployee . '%\'';
		}

		if( true == valStr( $strOrderByField ) ) {
			$strOrder = ' ORDER BY ' . $strOrderByField;
			if( true == valStr( $strOrderByValue ) ) {
				$strOrder .= ' ' . $strOrderByValue;
			}
		}

		$strSql = ' SELECT
						DISTINCT e.id AS employee_id,
						u.id AS user_id,
						e.employee_number,
						e.preferred_name,
						iss.id AS induction_session_id,
						iss.name AS induction_session_name
					FROM
						employees e
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = 1 )
						JOIN users u ON ( u.employee_id = e.id )
						JOIN induction_session_associations isa ON ( e.department_id = isa.department_id )
						JOIN induction_sessions iss ON ( iss.id = isa.induction_session_id )
					WHERE
						ea.country_code = \'' . $strCountryCode . '\'
						AND e.date_started::DATE > \'2017-01-03\'::DATE
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND (
								( e.department_id = isa.department_id AND e.technology_id = isa.technology_id )
								OR ( e.department_id = isa.department_id AND e.technology_id IS NULL )
							)' . $strWhere . $strOrder;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedInductionSessionEmployeeIds( $objDatabase, $strCountryCode = NULL, $strSearchEmployee = NULL, $boolCount = false, $intPageNo = NULL, $intPageSize = NULL, $strOrderByField = NULL, $strOrderByValue = NULL ) {
		$strWhere  = NULL;
		$strOrder  = NULL;
		$strOffset = NULL;

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( true == valStr( $strSearchEmployee ) ) {
			$strWhere = ' AND e.preferred_name ILIKE \'%' . $strSearchEmployee . '%\'';
		}

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit  = ( int ) $intPageSize;

			$strOffset = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		if( false == $boolCount && true == valStr( $strOrderByField ) ) {
			$strOrder = ' ORDER BY ' . $strOrderByField;
			if( true == valStr( $strOrderByValue ) ) {
				$strOrder .= ' ' . $strOrderByValue;
			}
		}

		$strSelect = ( true == $boolCount ) ? 'count ( DISTINCT e.id )' : 'DISTINCT e.id, e.employee_number';

		$strSql = ' SELECT
						' . $strSelect . '
					FROM
						employees e
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = 1 )
						JOIN users u ON ( u.employee_id = e.id )
						JOIN induction_session_associations isa ON ( e.department_id = isa.department_id )
						JOIN induction_sessions iss ON ( iss.id = isa.induction_session_id )
					WHERE
						ea.country_code = \'' . $strCountryCode . '\'
						AND e.date_started::DATE > \'2017-01-03\'::DATE
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND (
								( e.department_id = isa.department_id AND e.technology_id = isa.technology_id )
								OR ( e.department_id = isa.department_id AND e.technology_id IS NULL )
							)' . $strWhere . $strOrder . $strOffset;

		if( true == $boolCount ) {
			$arrintCount = fetchData( $strSql, $objDatabase );
			$intCount    = ( true == valArr( $arrintCount ) ) ? $arrintCount[0]['count'] : NULL;

			return $intCount;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDateStartedByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		$strSql = 'SELECT
						e.id,
						e.date_started
					FROM
						employees e
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND e.id IN (' . implode( ',', $arrintEmployeeIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesIdsByEmployeeDependants( $objDatabase ) {

		$strSql = 'SELECT DISTINCT
						ARRAY_TO_STRING( ARRAY_AGG( DISTINCT CASE WHEN e.anniversary_date IS NOT NULL OR e.anniversary_date IS NULL THEN e.id END), \',\' ) AS total_employee_ids,
						ARRAY_TO_STRING( ARRAY_AGG( DISTINCT CASE WHEN e.anniversary_date IS NOT NULL THEN e.id END), \',\' ) AS married_employee_ids,
						ARRAY_TO_STRING( ARRAY_AGG( DISTINCT CASE WHEN e.anniversary_date IS NULL THEN e.id END), \',\' ) AS single_employee_ids,
						ARRAY_TO_STRING( ARRAY_AGG( DISTINCT CASE WHEN ( e.anniversary_date IS NOT NULL OR e.anniversary_date IS NULL ) AND ( ed.dependant_type_id = ' . CDependantType::SPOUSE . ' OR ed.dependant_type_id = ' . CDependantType::CHILD . ' ) THEN e.id END), \',\' ) AS married_dependant_ids,
						ARRAY_TO_STRING( ARRAY_AGG( DISTINCT CASE WHEN ed.dependant_type_id = ' . CDependantType::SPOUSE . ' THEN e.id END), \',\' ) AS spounse_ids,
						ARRAY_TO_STRING( ARRAY_AGG( DISTINCT CASE WHEN ed.dependant_type_id = ' . CDependantType::CHILD . ' THEN e.id END), \',\' ) AS kids_ids,
						COUNT( DISTINCT CASE WHEN ed.dependant_type_id = ' . CDependantType::SPOUSE . ' THEN ed.id END) as spouse_count,
						COUNT( DISTINCT CASE WHEN ed.dependant_type_id = ' . CDependantType::CHILD . ' THEN ed.id END) as kids_count
					FROM
						employees e
						LEFT JOIN employee_dependants ed ON e.id = ed.employee_id
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
							( e.date_terminated IS NULL OR e.date_terminated > NOW() ) AND e.id NOT IN ( ' . CEmployee::ID_SYSTEM . ', ' . CEmployee::ID_ADMIN . ' ) AND ea.country_code IN ( \'' . CCountry::CODE_INDIA . '\' ) and e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ')';

		$arrmixEmployeeData = fetchData( $strSql, $objDatabase );

		return $arrmixEmployeeData[0];
	}

	public static function fetchActiveEmployeesWithUserIdByDepartmentIdsByDesignationIdsOrderByNameFull( $arrintDepartmentIds, $arrintDesignationIds, $objDatabase ) {

		if( false == valArr( $arrintDepartmentIds ) || false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.*,
						u.id as user_id
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR NOW() < date_terminated )
						AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
						AND e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' )
					ORDER BY
						name_full ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchKeyContactEmployeesByProductId( $objDatabase, $intPsProductId, $intPsProductOptionId = NULL ) {

		if( true == is_numeric( $intPsProductOptionId ) ) {
			$intProductId       = $intPsProductOptionId;
			$strJoinCondition   = ' JOIN ps_product_options ppo ON (pp.id = ppo.ps_product_id) ';
			$strSelectCondition = ' CASE
										WHEN ppo.business_analyst_employee_id IS NULL THEN pp.business_analyst_employee_id
										ELSE ppo.business_analyst_employee_id
									END AS business_analyst_employee_id,
									CASE
										WHEN ppo.product_manager_employee_id IS NULL THEN pp.product_manager_employee_id
										ELSE ppo.product_manager_employee_id
									END AS product_manager_employee_id,
									CASE
										WHEN ppo.product_owner_employee_id IS NULL THEN pp.product_owner_employee_id
										ELSE ppo.product_owner_employee_id
									END AS product_owner_employee_id,
									 CASE
										WHEN ppo.sdm_employee_id IS NULL THEN pp.sdm_employee_id
										ELSE ppo.sdm_employee_id
									END AS sdm_employee_id';
			$strWhereCondition  = ' ppo.id ';
		} else {
			$strSelectCondition = ' pp.business_analyst_employee_id, pp.product_manager_employee_id, pp.product_owner_employee_id,pp.sdm_employee_id ';
			$intProductId       = $intPsProductId;
			$strJoinCondition   = ' ';
			$strWhereCondition  = ' pp.id ';
		}

		$strSql = ' SELECT DISTINCT e.id,
							e.preferred_name
					FROM (	SELECT
								' . $strSelectCondition . '
							FROM ps_products pp
								' . $strJoinCondition . '
							WHERE ' . $strWhereCondition . ' = ' . ( int ) $intProductId . '
						) sub
					JOIN employees e ON (e.id IN (sub.business_analyst_employee_id, sub.product_manager_employee_id, sub.product_owner_employee_id, sub.sdm_employee_id ) AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchNoSalaryEmployees( $arrstrEmployeeCompensationFilter, $objDatabase, $boolCount = false ) {
		$strOrderByField = ( false == empty( $arrstrEmployeeCompensationFilter['order_by_field'] ) ) ? $arrstrEmployeeCompensationFilter['order_by_field'] : '';
		$strOrderByType  = ( false == empty( $arrstrEmployeeCompensationFilter['order_by_type'] ) ) ? $arrstrEmployeeCompensationFilter['order_by_type'] : '';
		$strComma        = ( false == empty( $strOrderByField ) ) ? ',' : '';
		$intOffset       = ( false == $boolCount && false == empty( $arrstrEmployeeCompensationFilter['page_no'] ) && false == empty( $arrstrEmployeeCompensationFilter['page_size'] ) ) ? $arrstrEmployeeCompensationFilter['page_size'] * ( $arrstrEmployeeCompensationFilter['page_no'] - 1 ) : 0;
		$intLimit        = ( false == $boolCount && true == isset( $arrstrEmployeeCompensationFilter['page_size'] ) ) ? $arrstrEmployeeCompensationFilter['page_size'] : '';
		$strSelectClause = ( false == $boolCount ) ? ' e.id, e.preferred_name, e.name_full, e.employee_number, e.date_started, d.name AS designation_name, de.name AS department_name, st.name AS employee_salary_type, e1.id AS manager_employee_id, e1.preferred_name AS reporting_manager ' : ' count( e.id ) ';

		$strWhereClause   = ' e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND ec.employee_id IS NULL ';
		$strWhereClause   .= ( false == empty( $arrstrEmployeeCompensationFilter['department_ids'] ) && true == valArr( array_filter( $arrstrEmployeeCompensationFilter['department_ids'] ) ) ) ? ' AND e.department_id IN ( ' . implode( ',', $arrstrEmployeeCompensationFilter['department_ids'] ) . ') ' : '';
		$strOrderByClause = ( false == $boolCount ) ? ' ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . $strComma . 'name_full' : '';

		if( true == valStr( $arrstrEmployeeCompensationFilter['single_date'] ) ) {
			$strWhereClause .= ' AND e.date_started::date BETWEEN \'' . $arrstrEmployeeCompensationFilter['single_date'] . ' 00:00:00 \' AND \'' . $arrstrEmployeeCompensationFilter['single_date'] . ' 23:59:59\'';
		}

		$strSql = ' SELECT '
				  . $strSelectClause . '
					FROM
						employees e
						JOIN employee_addresses ea ON( e.id = ea.employee_id AND ea.address_type_id = ' . ( int ) CAddressType::PRIMARY . ' AND ea.country_code = \'IN\' )
						JOIN designations d ON( e.designation_id = d.id )
						JOIN departments de ON( e.department_id = de.id )
						JOIN employee_salary_types st ON( e.employee_salary_type_id = st.id )
						JOIN employees as e1 ON ( e1.id = e.reporting_manager_id )
						LEFT JOIN employee_compensations ec ON( e.id = ec.employee_id )
					WHERE
						' . $strWhereClause . $strOrderByClause . '
					OFFSET ' . ( int ) $intOffset;

		if( true == $boolCount ) {
			$arrintEmployeesCount = fetchData( $strSql, $objDatabase );

			return ( true == valArr( $arrintEmployeesCount ) ) ? $arrintEmployeesCount[0]['count'] : 0;
		} else {
			if( 0 < $intLimit ) {
				$strSql .= ' LIMIT ' . ( int ) $intLimit;
			}

			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchActiveEmployeesByFilter( $arrmixFilter, $objDatabase ) {

		if( false == valArr( $arrmixFilter ) ) {
			return NULL;
		}

		$strSelectClause = '';
		$strJoinClause   = '';
		$strWhereClause  = '';

		if( 0 == $arrmixFilter['show_terminated'] ) {
			$strWhereClause .= ' AND e.date_terminated IS NULL';
		}

		if( 1 == $arrmixFilter['show_terminated'] ) {
			$strWhereClause .= ' AND e.date_terminated IS NOT NULL';
		}

		if( true == valArr( $arrmixFilter['department_ids'] ) ) {
			$strWhereClause .= ' AND e.department_id IN ( ' . implode( ',', $arrmixFilter['department_ids'] ) . ' )';
		}

		if( true == valArr( $arrmixFilter['employee_ids'] ) ) {
			$strWhereClause .= ' AND e.id IN ( ' . implode( ',', $arrmixFilter['employee_ids'] ) . ' )';
		}

		if( true == valArr( $arrmixFilter['ps_product'] ) || true == valArr( $arrmixFilter['ps_product_option'] ) ) {
			$strSelectClause .= ', t.ps_product_id, t.ps_product_option_id';
			$strJoinClause   .= ' JOIN team_employees te ON( e.id = te . employee_id AND te.is_primary_team = 1 )
								JOIN teams t ON( te.team_id = t.id AND t.deleted_by IS NULL )';

			if( true == valArr( $arrmixFilter['ps_product'] ) && false == valArr( $arrmixFilter['ps_product_option'] ) ) {
				$strWhereClause .= ' AND t.ps_product_id IN ( ' . implode( ',', $arrmixFilter['ps_product'] ) . ' )';
			} elseif( true == valArr( $arrmixFilter['ps_product'] ) && true == valArr( $arrmixFilter['ps_product_option'] ) ) {
				$strWhereClause .= ' AND ( t.ps_product_id IN ( ' . implode( ',', $arrmixFilter['ps_product'] ) . ' ) OR t.ps_product_option_id IN ( ' . implode( ',', $arrmixFilter['ps_product_option'] ) . ' ) ) ';
			} elseif( false == valArr( $arrmixFilter['ps_products'] ) && true == valArr( $arrmixFilter['ps_product_option'] ) ) {
				$strWhereClause .= ' AND t.ps_product_option_id IN ( ' . implode( ',', $arrmixFilter['ps_product_option'] ) . ' ) ';
			}
		}

		$strSql = 'SELECT
						e.id,
						e.department_id,
						e.preferred_name,
						e.date_terminated
						' . $strSelectClause . '
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = 1 )
						' . $strJoinClause . '
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND ea.country_code = \'' . $arrmixFilter['country_code'] . '\''
				  . $strWhereClause;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRvpCsmEmailAdressByCid( $arrintCid, $objDatabase ) {

		if( false == valId( $arrintCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						mc.id AS cid,
						e.email_address
					From
						employees e
						JOIN ps_lead_details pld ON ( e.id = pld.sales_employee_id OR e.id = pld.support_employee_id )
						JOIN clients mc ON ( pld.cid = mc.id )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						 AND mc.company_status_type_id <> ' . CCompanyStatusType::TERMINATED . '
						 AND e.id <> ' . CEmployee::ID_ASSOCIATE_ACCOUNT_MANAGEMENT . '
						 AND mc.id = ' . ( int ) $arrintCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllImplementedEmployeesByDepartmentIds( $arrintDepartmentIds, $objDatabase ) {

		if( false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						subSql.id,
						subSql.preferred_name
					FROM
						(
						SELECT
							e.id,
							e.preferred_name
						FROM
							employees AS e
						WHERE
							e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) 
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
							AND ( e.date_terminated IS NULL
							OR e.date_terminated > NOW ( ) )
						UNION
						SELECT
							DISTINCT ( e.id ),
							e.preferred_name
						FROM
							contract_properties AS cp
							JOIN contracts AS co ON ( cp.contract_id = co.id )
							JOIN clients AS c ON ( co.cid = c.id AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
							LEFT JOIN contract_employees AS ce ON ( co.cid = ce.cid AND cp.contract_id = ce.contract_id AND ce.contract_employee_type_id = ' . CContractEmployeeType::IMPLEMENTATION . ' )
							LEFT JOIN users AS u ON ( u.id = cp.implemented_by )
							JOIN employees AS e ON ( e.id = COALESCE ( u.employee_id, ce.employee_id ) AND e.employee_status_type_id = ' . CEmployeeStatusType::PREVIOUS . ' AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) )
						WHERE
							cp.commission_bucket_id <> ' . CCommissionBucket::RENEWAL . '
						) AS subSql
					ORDER BY
						subSql.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesInformationByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT( e.id ),
						( ( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) * 12 ) + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) + ( ead.experience_year * 12 ) + ead.experience_month ) AS total_experience,
						( ( extract ( YEAR FROM age ( CURRENT_DATE, e.date_started ) ) * 12 ) + extract ( month FROM age ( CURRENT_DATE, e.date_started ) ) ) AS xento_experience,
						ep.value AS review_cycle
					FROM
						employees e
						LEFT JOIN employee_preferences ep ON( e.id = ep.employee_id AND ep.key = \'EMPLOYEE_REVIEW_CYCLE\')
						LEFT JOIN employee_applications eapp ON ( eapp.employee_id = e.id )
						LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = eapp.id )
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllActiveEmployeesDetails( $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT( e.email_address ),
						e.id AS employee_id,
						e.name_full,
						e.preferred_name,
						u.id,
						e.designation_id
					FROM
						users AS u
						LEFT JOIN employees AS e ON( u.employee_id = e.id )
					WHERE
						e.employee_status_type_id =' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL
					ORDER BY
						e.preferred_name ASC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchReportDefaultOwnerEmployeesByRoleId( $intRoleId, $objDatabase ) {

		if( false == valId( $intRoleId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT e.id,
						ur.id AS user_role_id
					FROM
						employees e
						JOIN users u ON ( e. id = u.employee_id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN user_roles ur ON ( u.id = ur.user_id )
					WHERE
						ur.deleted_on IS NULL
						AND ur.deleted_by IS NULL
						AND ur.role_id = ' . ( int ) $intRoleId . '
						ORDER BY ur.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllTpocEmployeeData( $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT e.*
					FROM
						business_units bu
						JOIN employees e ON ( e.id = bu.training_representative_employee_id )
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' ';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchManagerResponseSurveyReport( $arrmixResponseSurveyFilter, $strOrderByType = NULL, $strOrderByField = NULL, $objAdminDatabase ) {

		$strWhereCondition = '';

		if( true == valStr( $arrmixResponseSurveyFilter['from_date'] ) && true == valStr( $arrmixResponseSurveyFilter['to_date'] ) ) {
			$strWhere = ' AND s.survey_datetime::DATE BETWEEN \'' . $arrmixResponseSurveyFilter['from_date'] . '\' AND \'' . $arrmixResponseSurveyFilter['to_date'] . '\'';
			switch( $arrmixResponseSurveyFilter['cpa_frequency'] ) {
				case CFrequency::QUARTERLY:
					$strWhereCondition .= $strWhere . ' AND ss.frequency_id = ' . CFrequency::QUARTERLY;
					break;

				case CFrequency::MONTHLY:
					$strWhereCondition .= $strWhere . ' AND ss.frequency_id = ' . CFrequency::MONTHLY;
					break;

				case CFrequency::WEEKLY:
					$strWhereCondition .= $strWhere . ' AND ss.frequency_id = ' . CFrequency::WEEKLY;
					break;

				default:
					$strWhereCondition .= '';
			}
		}

		if( true == array_key_exists( 'hr_representative_employee_id', $arrmixResponseSurveyFilter ) && true == is_numeric( $arrmixResponseSurveyFilter['hr_representative_employee_id'] ) ) {
			$strWhereCondition .= ' AND t.hr_representative_employee_id = ' . $arrmixResponseSurveyFilter['hr_representative_employee_id'];
		}
		if( false == is_null( $strOrderByField ) && false == is_null( $strOrderByType ) ) {
			$strOrderClause = ' ORDER BY  ' . $strOrderByField . ' ' . $strOrderByType . ', e.preferred_name, s.id';
		} else {
			$strOrderClause = ' ORDER BY e.preferred_name, s.id';
		}
		$strSql = 'SELECT e1.id,
						e1.preferred_name AS manager_name,
						e.preferred_name AS employee_name,
						CASE WHEN s.response_datetime IS NULL AND s.declined_datetime IS NULL THEN date (NOW()) - s.survey_datetime::DATE
							WHEN s.declined_datetime IS NOT NULL THEN s.declined_datetime::DATE - s.survey_datetime::DATE
						ELSE
						 s.response_datetime::DATE - s.survey_datetime::DATE
						END AS pending_from,
						e1.email_address,
						d.name AS department_name,
						f.name as frequency,
						s.id AS survey_id,
						s.response_datetime::Date AS response_date,
						s.survey_datetime::DATE AS survey_created_on,
						s.declined_datetime::DATE AS declined_date,
						e2.id AS hr_representative_employee_id,
						e2.preferred_name as hr_representative_name
					FROM
						employees e1
						JOIN employees e3 ON ( e3.reporting_manager_id = e1.id )
						JOIN team_employees  te ON ( e3.id = te.employee_id )
						JOIN teams t ON( t.id = te.team_id )
						JOIN employees e on ( e.id = te.employee_id and te.is_primary_team = 1) OR ( e.id = t.manager_employee_id AND e.id NOT IN ( SELECT DISTINCT ( employee_id )FROM team_employees ) )
						JOIN surveys s ON ( s.subject_reference_id = e.id )
						JOIN surveys s1 ON ( s.trigger_reference_id = s1.id )
						JOIN scheduled_surveys ss ON ( s1.scheduled_survey_id = ss.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e1.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . $arrmixResponseSurveyFilter['country_code'] . '\' )
						LEFT JOIN frequencies f ON ( f.id = ss.frequency_id )
						LEFT JOIN departments d ON ( d.id = e1.department_id )
						LEFT JOIN employees e2 ON ( e2.id = t.hr_representative_employee_id )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND s.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_MANAGER_SURVEY_IN . '
						AND s.survey_type_id = ' . CSurveyType::MANAGER_SURVEY . '
						AND s.survey_datetime::DATE BETWEEN \'' . $arrmixResponseSurveyFilter['from_date'] . '\' AND \'' . $arrmixResponseSurveyFilter['to_date'] . '\'
						' . $strWhereCondition . '
						AND s.response_datetime IS NULL '
				  . $strOrderClause;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeesWithCpaRatingByByDate( $arrmixSurveyFilter, $intOffset, $intPageSize, $objAdminDatabase, $boolCountOnly = false ) {
		$strSubQuery = '';
		$strWhere    = '';

		if( true == valStr( $arrmixSurveyFilter['date'] ) ) {
			$strWhere .= 'AND to_char( s1.survey_datetime::DATE, \'mm-yyyy\' ) = to_char( \'01 ' . $arrmixSurveyFilter['date'] . '\'::DATE, \'mm-yyyy\')';
		} else {
			$strWhere .= 'AND to_char( s1.survey_datetime::DATE, \'mm-yyyy\' ) = to_char( current_date::DATE , \'mm-yyyy\' )';
		}
		if( true == valId( $arrmixSurveyFilter['hr_representative_employee_id'] ) ) {
			$strSubQuery = ' AND  t.hr_representative_employee_id = ' . ( int ) $arrmixSurveyFilter['hr_representative_employee_id'];
		}
		if( true == valId( $arrmixSurveyFilter['cpa_rating_weight'] ) ) {
			$strWhere .= 'AND sqa.numeric_weight = ' . $arrmixSurveyFilter['cpa_rating_weight'];
		}
		if( true == valId( $arrmixSurveyFilter['cpa_frequency'] ) ) {
			$strWhere .= 'AND ss.frequency_id = ' . $arrmixSurveyFilter['cpa_frequency'];
		}

		if( true == valStr( $arrmixSurveyFilter['order_by_field'] ) && true == valStr( $arrmixSurveyFilter['order_by_type'] ) ) {
			$strOrderClause = ' ORDER BY ' . $arrmixSurveyFilter['order_by_field'] . ' ' . $arrmixSurveyFilter['order_by_type'];
		} else {
			$strOrderClause = ' ORDER BY sqa.numeric_weight DESC, e.preferred_name';
		}
		$strSql = 'SELECT
						s.id,
						e.preferred_name,
						sqa.numeric_weight,
						s.response_datetime,
						e2.preferred_name as hr_representative_name,
						d.name AS department_name,
						f.name as frequency
					FROM
						employees e
						JOIN surveys s ON ( s.subject_reference_id = e.id )
						JOIN survey_question_answers sqa ON ( sqa.survey_id = s.id )
						JOIN surveys s1 ON ( s.trigger_reference_id = s1.id )
						JOIN scheduled_surveys ss ON ( s1.scheduled_survey_id = ss.id )
						JOIN team_employees te ON ( e.id = te.employee_id and te.is_primary_team = 1 )
						JOIN teams t on (te.team_id = t.id ' . $strSubQuery . ' )
						LEFT JOIN employees e2 ON (e2.id = t.hr_representative_employee_id)
						LEFT JOIN frequencies f ON (f.id = ss.frequency_id)
						LEFT JOIN departments d ON (d.id = e.department_id)
					WHERE
						s.survey_type_id = ' . CSurveyType::MANAGER_SURVEY . '
						AND s.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_MANAGER_SURVEY_IN . '
						AND sqa.numeric_weight IS NOT NULL
						' . $strWhere . '
					GROUP BY
						sqa.numeric_weight,
						e.preferred_name,
						s.id,
						s.response_datetime,
						e2.preferred_name,
						d.name,
						f.name';

		if( false == $boolCountOnly && false == is_null( $intOffset ) && false == is_null( $intPageSize ) ) {
			$strSql .= $strOrderClause . '
						 OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;
		}

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchQuickSearchEmployeesByPerferredName( $strPreferredName, $objDatabase ) {
		$strSql = 'SELECT * FROM ' . CEmployee::TABLE_NAME . ' WHERE employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND preferred_name ILIKE \'%' . $strPreferredName . '%\' ORDER BY preferred_name ASC LIMIT 10';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByTeamIdByManagerIdByDepartmentId( $intTeamId, $intManagerEmployeeId, $intDepartmentId, $objDatabase ) {

		if( false == valId( $intTeamId ) || false == valId( $intManagerEmployeeId ) ) {
			return NULL;
		}
		if( true == valId( $intDepartmentId ) ) {
			$strWhere = ' AND department_id = ' . ( int ) $intDepartmentId;
		}

		$strSql = 'SELECT
						employee_id,
						preferred_name AS employee_name
					FROM
						team_employees te
						JOIN employees e ON ( te.employee_id = e.id AND e.employee_status_type_id = 1 )
					WHERE
						( team_id = ' . ( int ) $intTeamId . ' OR employee_id = ' . ( int ) $intManagerEmployeeId . ' )
						AND is_primary_team = 1' . $strWhere;

		return fetchData( $strSql, $objDatabase );
	}

	public  static function fetchDevelopmentMentors( $arrintExcludeEmployeeIds, $arrintDepartmentIds, $objDatabase ) {

		if( false == valArr( $arrintExcludeEmployeeIds ) || false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						 DISTINCT( e.id ),
							e.preferred_name,
							e.name_full
					FROM
						employees e1
						JOIN employees e ON( e1.reporting_manager_id = e.id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN employee_addresses ea ON( ea.employee_id = e.id AND ea.country_code = \'' . CCountry::CODE_INDIA . '\' AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e.id NOT IN( ' . implode( ',', $arrintExcludeEmployeeIds ) . ' )
						AND e.department_id IN( ' . implode( ',', $arrintDepartmentIds ) . ' )
					ORDER BY
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchAddVsQamIdAndNameFull( $objDatabase ) {

		$strSql = 'SELECT
						add_employee_id,
						e1.name_first add_name,
						e1.name_full add_name_full,
						qam_employee_id,
						e2.name_first qam_name,
						e2.name_full qam_name_full
					FROM
						(
						SELECT
							ps.qam_employee_id qam_employee_id,
							pse.employee_id add_employee_id,
							COUNT(ps.qam_employee_id) count, 
							ROW_NUMBER() OVER( Partition BY ps.qam_employee_id ORDER BY COUNT( ps.qam_employee_id ) DESC ) 
						FROM
							ps_products ps, 
							ps_product_options po, 
							ps_product_employees pse
						WHERE
							ps.id = po.ps_product_id
						AND pse.ps_product_id = ps.id
						AND pse.ps_product_option_id = po.id
						AND ps_product_employee_type_id = ' . CPsProductEmployeeType::ASSOCAITE_DIRECTOR_DEVELOPMENT . ' 
						GROUP BY ps.qam_employee_id, pse.employee_id
						) add_qam_ids,
						employees e1,
						employees e2
					WHERE row_number = 1
					AND add_employee_id = e1.id 
					AND qam_employee_id = e2.id ORDER BY e1.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTransportPocEmployeeSearch( $objDatabase, $arrmixRequestFilteredSearch = NULL, $strOrderByField = NULL, $strOrderBy = NULL, $intPageNo, $intPageSize ) {

		$strSearchCondition = '';

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( true == valArr( $arrmixRequestFilteredSearch ) ) {
			$strSearchCondition = ' AND e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrmixRequestFilteredSearch ) . '%\' ';
		}

		$strSql = 'SELECT
						e.preferred_name as full_name,
						epn.phone_number as phone,
						ep.id as prf_id,
						ep.value as status
					FROM
						employees e
						INNER JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id and epn.phone_number_type_id = 1 )
						INNER JOIN employee_preferences ep ON ( ep.employee_id = e.id )
					WHERE e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' 
						AND ep.key = \'' . CEmployeePreference::POC_KEY . '\'' . $strSearchCondition . '
					ORDER BY ' . $strOrderByField . ' ' . $strOrderBy;

		if( true == isset( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}
		$strSql .= ' OFFSET ' . ( int ) $intOffset;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentPaginatedPocEmployeeCount( $objDatabase, $arrmixRequestFilteredSearch = NULL ) {

		$strSearchCondition = '';

		if( true == valArr( $arrmixRequestFilteredSearch ) ) {
			$strSearchCondition = ' AND e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrmixRequestFilteredSearch ) . '%\' ';
		}

		$strSql = 'SELECT
						count( e.id )
					FROM
						employees e
						INNER JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id and epn.phone_number_type_id = 1 )
						INNER JOIN employee_preferences ep ON ( ep.employee_id = e.id )
					WHERE e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . ' AND  ep.key = \'' . CEmployeePreference::POC_KEY . '\'' . $strSearchCondition;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintCount ) && 0 < $arrintCount[0]['count'] ) ? $arrintCount[0]['count'] : NULL;
	}

	public static function fetchAllEmployeesForPoc( $arrintDepartmentIds, $objDatabase, $strCountryCode = NULL ) {

		if( false == valArr( array_filter( $arrintDepartmentIds ) ) ) {
			return false;
		}

		$strWhereCondition = ( false == is_null( $strCountryCode ) ) ? ' AND ea.country_code = \'' . $strCountryCode . '\'' : '';

		$strSql = ' SELECT
						e.id,
						e.preferred_name,
						u.id as user_id,
						e.department_id,
						ea.country_code
					FROM
						employees e
						JOIN users u ON( u.employee_id = e.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employee_preferences ep ON ( ep.employee_id = e.id and ep.key = \'' . CEmployeePreference::POC_KEY . '\' )
					WHERE
						ep.id IS NULL
						AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
						AND e.employee_status_type_id IN ( ' . CEmployeeStatusType::CURRENT . ' ) ' . $strWhereCondition . '
					ORDER BY name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTransportEnrollmentPOC( $objDatabase ) {
		$strSql = 'SELECT
						e.name_full as full_name,
						epn.phone_number as phone
					FROM
						employees e
						INNER JOIN employee_phone_numbers epn ON ( epn.employee_id = e.id and epn.phone_number_type_id = 1 )
						INNER JOIN employee_preferences ep ON ( ep.employee_id = e.id )
					WHERE ep.key = \'' . CEmployeePreference::POC_KEY . '\' and ep.value = \'1\' ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchOfficeDeskIdByEmployeeId( $intEmployeeId, $objDatabase ) {
		if( false == valId( $intEmployeeId ) ) return false;
		$strSql = ' SELECT 
						e.office_desk_id
						FROM
							employees e 
						WHERE
						e.id = ' . ( int ) $intEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountEmployeesHavingProposedDeskAndAssetsByListingTypeByOfficeIds( $strListingType, $arrintOfficeIds, $objDatabase, $intUserId = NULL ) {
		$strSql = 'select count( e.id )
						FROM employees e
						LEFT JOIN office_desks od';

		if( 'ADMIN' == $strListingType ) {
			$strSql .= ' ON e.desk_allocation_office_desk_id = od.id
				WHERE 	od.is_reserved = 1 ';

		} else {
			if( 'IT' == $strListingType ) {
				$strSql .= ' ON e.desk_allocation_office_desk_id = od.id
					where od.desk_shifting_request_status = 2 ';
			}
		}

		if( NULL != $intUserId ) {
			$strSql .= ' AND od.desk_shifting_request_by = ' . ( int ) $intUserId;
		}

		if( true == valArr( $arrintOfficeIds ) ) {
			$strSql .= ' AND od.office_id IN ( ' . implode( ',', $arrintOfficeIds ) . ' )';
		}

		$arrintEmployees = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintEmployees[0]['count'] ) ) {
			return $arrintEmployees[0]['count'];
		}

	}

	public static function fetchPaginatedEmployeesHavingProposedDeskAndAssetsByListingTypeByOfficeIds( $intPageNo, $intPageSize, $strListingType, $arrintOfficeIds, $objDatabase, $strDirectionFlag = 'ASC', $intUserId = NULL ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'select
						e.id as employee_id,
						e.preferred_name,
						e.office_desk_id,
						e.desk_allocation_office_desk_id,
						od.is_reserved,
						od.desk_shifting_request_by,
						DATE_PART ( \'day\', NOW ( )::timestamp - od.desk_shifting_request_on::timestamp ) AS pending_from
					FROM
						employees e
						LEFT JOIN office_desks od';

		if( 'IT' == $strListingType ) {
			$strSql .= ' ON e.desk_allocation_office_desk_id = od.id
					WHERE 	od.is_reserved = 1 AND od.desk_shifting_request_status = ' . COfficeDesk:: DESK_SHIFTING_REQUEST_IT_PENDING_FOR_ASSETS . ' ';

		}

		if( NULL != $intUserId ) {
			$strSql .= ' AND od.desk_shifting_request_by = ' . ( int ) $intUserId;
		}

		if( true == valArr( $arrintOfficeIds ) ) {
			$strSql .= ' AND od.office_id IN ( ' . implode( ',', $arrintOfficeIds ) . ' )';
		}

		$strSql .= ' ORDER BY od.desk_shifting_request_on ' . $strDirectionFlag . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		$arrmixEmployees = fetchData( $strSql, $objDatabase );

		return $arrmixEmployees;
	}

	public static function fetchSdmEmployeeIdsByPsProductOptionIds( $arrintPsProductOptionIds, $objAdminDatabase ) {

		if( false == valArr( $arrintPsProductOptionIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						DISTINCT e.id
					FROM
						employees e
						JOIN ps_product_options ppo ON ( e.id = ppo.sdm_employee_id )
					WHERE
						e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW . '
						AND ppo.id IN(  ' . implode( ',', $arrintPsProductOptionIds ) . ' )';

		return rekeyArray( 'id', fetchData( $strSql, $objAdminDatabase ) );
	}

	public static function fetchNewJoinerDetailsByDatesRange( $strStartDate, $strEndDate, $objDatabase ) {

		if( false == ( valStr( $strStartDate ) || $strEndDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON (e.id)
						e.id,
						e.employee_number,
						e.preferred_name,
						d.name AS designation,
						e.date_started AS date_of_joining,
						e.birth_date,
						e.email_address,
						e.blood_group,
						e.gender,
						( select phone_number from employee_phone_numbers where( e.id = employee_id  and phone_number_type_id = 4 ) ) as emergency_contact,
						( select phone_number from employee_phone_numbers where( e.id = employee_id  and phone_number_type_id = 1 ) ) as personal_contact
					FROM 
						employees e
						JOIN designations AS d ON ( d.id = e.designation_id )
						JOIN employee_addresses AS ed ON ( e.id = ed.employee_id AND ed.country_code = \'IN\' )
					WHERE
						e.date_started BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY 
						e.id DESC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchNewJoinerDependentsDetailsByDatesRange( $strStartDate, $strEndDate, $objDatabase ) {

		if( false == ( valStr( $strStartDate ) || $strEndDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON (e.id)
						e.employee_number,
						e.anniversary_date,
						e.preferred_name,
						d.name AS designation,
						ed.name_first || \'  \' || ed.name_last AS spouse_name,
						ed.birth_date AS spouse_birth_date,
						ed1.name_first || \'  \' || ed1.name_last AS child1_name,
						ed1.birth_date AS child1_birth_date,
						ed2.name_first || \'  \' || ed2.name_last AS child2_name,
						ed2.birth_date AS child2_birth_date,
						e.birth_date
					FROM
						employees e
						JOIN designations AS d ON ( d.id = e.designation_id )
						JOIN employee_addresses AS ea ON ( e.id = ea.employee_id AND ea.country_code = \'IN\' )
						LEFT JOIN employee_dependants AS ed ON ( e.id = ed.employee_id AND ed.dependant_type_id = 1 )
						LEFT JOIN employee_dependants AS ed1 ON ( e.id = ed1.employee_id AND ed1.id = 
						(
						SELECT
						id
						FROM
						employee_dependants
						WHERE
						employee_id = e.id
						AND dependant_type_id = 5 OFFSET 0
						LIMIT
						1
						) )
						LEFT JOIN employee_dependants AS ed2 ON ( e.id = ed2.employee_id AND ed2.id = 
						(
						SELECT
						id
						FROM
						employee_dependants
						WHERE
						employee_id = e.id
						AND dependant_type_id = 5 OFFSET 1
						LIMIT
						1
						) )
					WHERE
						e.date_started BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY 
						e.id DESC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchBacklogSharedEmployeesByTeamIds( $arrintTeamIds, $objDatabase ) {
		if( false == valArr( $arrintTeamIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						DISTINCT( e.* )
					FROM
						employees as e
						JOIN team_employees as te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id =' . CAddressType::PRIMARY . ' )
					WHERE
						te.team_id NOT IN ( ' . implode( ',', $arrintTeamIds ) . ' )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDetailsForNoduesById( $intEmployeeId, $objDatabase, $boolIsFromScript = false ) {
		if( false == valId( $intEmployeeId ) && false == $boolIsFromScript ) {
			return NULL;
		}
		$strWhereClause = ( true == valId( $intEmployeeId ) && false == $boolIsFromScript )?'e.id = ' . ( int ) $intEmployeeId . ' AND ': ' e.expected_termination_date = now()::date + INTERVAL \'1 days\' AND ';
		$strSql = ' SELECT
						DISTINCT ON (e.id)
						e.id,
						e.employee_number,
						e.gender,
						e.preferred_name,
						e.email_address,
						e.expected_termination_date,
						e1.preferred_name as bu_hr,
						d.name as department_name,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.country_code = \'' . CCountry::CODE_INDIA . '\')
						JOIN team_employees te ON ( te.employee_id = e.id )
						JOIN departments d ON ( d.id = e.department_id)
						LEFT JOIN teams t ON ( te.team_id = t.id )
						LEFT JOIN employees e1 ON ( t.hr_representative_employee_id = e1.id AND e1.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
					WHERE
						' . $strWhereClause . '
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND
						te.is_primary_team	= 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveDMCEmployeesByDesignationIds( $arrintDesignationIds, $objDatabase ) {
		if( false == valIntArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strSql	= 'SELECT
						id, preferred_name
					FROM
						employees
					WHERE
						employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND date_terminated IS NULL
						AND designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' ) 
					ORDER BY
						preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeeIdsByRoleIds( $arrintRoleIds, $objDatabase ) {
		if( false == valArr( $arrintRoleIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT e.id
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
						JOIN user_roles ur ON ( u.id = ur.user_id )
					WHERE
						ur.deleted_on IS NULL
						AND	ur.deleted_by IS NULL
						AND ur.role_id IN (' . implode( ',', $arrintRoleIds ) . ')
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY 
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeDetailsById( $intEmployeeId, $objAdminDatabase ) {
		if( false == valId( $intEmployeeId ) ) return NULL;

		$strSql = 'SELECT
						e.preferred_name as name,
						d.employee_salary_type_id,
						e.id as employee_id,
						e.birth_date,
						CASE WHEN e.bonus_potential_encrypted IS NULL THEN dbc.encrypted_annual_bonus_potential ELSE e.bonus_potential_encrypted END as bonus_potential_encrypted,
						CASE WHEN e.bonus_potential_encrypted IS NULL THEN dbc.bonus_criteria ELSE e.bonus_requirement END as bonus_requirement,
						d.name as designation,
						d1.name as department_name,
						d1.id as department_id,
						e1.preferred_name AS reporting_manager,
						e1.id AS manager_employee_id,
						e.employee_status_type_id,
						e.date_terminated,
						e.designation_id,
						t.name AS team_name,
						abt.id as annual_bonus_type_id,
						abt.name AS bonus_frequency,
						est.name AS salary_type,
						ROW_NUMBER() OVER ( ORDER BY e.preferred_name ) as row_num
					FROM
						employees e
						JOIN team_employees te ON ( e.id = te.employee_id AND is_primary_team = 1 )
						JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
						JOIN designations d ON ( d.id = e.designation_id )
						JOIN departments d1 ON ( d1.id = d.department_id )
						LEFT JOIN designation_bonus_criterias dbc ON ( dbc.designation_id = d.id AND dbc.approved_on IS NOT NULL AND CURRENT_DATE >= dbc.start_date::DATE AND ( dbc.end_date IS NULL OR ( dbc.end_date::DATE >= CURRENT_DATE AND dbc.start_date::DATE <= dbc.end_date::DATE ) ) )
						JOIN teams t ON (t.id = te.team_id )
						LEFT JOIN annual_bonus_types abt ON( e.annual_bonus_type_id = abt.id OR dbc.annual_bonus_type_id = abt.id )
						LEFT JOIN employee_salary_types est ON( e.employee_salary_type_id = est.id )
					WHERE
						e.id = ' . ( int ) $intEmployeeId . '
					ORDER BY 
						row_num DESC LIMIT 1';

		$arrmixEmployeeDetails = fetchData( $strSql, $objAdminDatabase );
		return $arrmixEmployeeDetails[0];
	}

	public static function fetchPreviousTeamEmployeesByDesignationIdByManagerId( $intManagerEmployeeId, $intDesignationId, $objAdminDatabase ) {
		if( false == valId( $intManagerEmployeeId ) || false == valId( $intDesignationId ) ) return NULL;

		$strSql = 'SELECT
						e.id,
						e.date_terminated,
						e.date_started,
						e.resigned_on,
						e.name_full,
						e.expected_termination_date
					FROM
						employees e
						LEFT JOIN employee_assessments eass ON ( eass.employee_id = e.id AND eass.employee_assessment_type_id = ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ' ) 
					WHERE
						e.designation_id = ' . ( int ) $intDesignationId . '
						AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
						AND eass.deleted_on IS NULL
						AND e.resigned_on IS NOT NULL
						ORDER BY e.resigned_on ASC';
		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeesUserIdByfullNameOrPreferredName( $strFullName, $objDatabase ) {
		$strSql = 'SELECT
						u.id AS user_id
					FROM
						employees e
						JOIN users u ON ( u.employee_id = e.id )
					WHERE
					    e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND ( e.name_full ILIKE \'%' . trim( addslashes( $strFullName ) ) . '%\'' . '
						OR e.preferred_name ILIKE \'%' . trim( addslashes( $strFullName ) ) . '%\'' . '
						 OR e.name_first ILIKE \'%' . trim( addslashes( $strFullName ) ) . '%\'' . ' )
					ORDER BY
						u.id ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomTerminatedEmployeesByDateRange( $strStartDate, $strEndDate, $objAdminDatabase ) {

		$strSql = 'SELECT
						id,
						name_full
					FROM
						employees
					WHERE
						date_terminated BETWEEN \'' . $strEndDate . '\' AND \'' . $strStartDate . '\'
					ORDER BY id	ASC';
		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchCustomEmployeeEmailAddressByIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						e.id,
						e.name_first,
					    array_to_string(array_agg(e.email_address), \',\') as email_addresses   
				   FROM 
					 employees e 
				   WHERE 
				     e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
				     AND e.id IN (' . sqlIntImplode( array_filter( $arrintEmployeeIds ) ) . ')
				   GROUP BY e.id';

			return fetchData( $strSql, $objDatabase );

	}

	public static function fetchEmployeesDataByOrderFormTypeRoles( $objDatabase ) {
		if( false == valArr( CRole::$c_arrintOrderFormTypeIdsByUserRoles ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ur.role_id,
						e.id AS employee_id,
						e.preferred_name
					FROM
						employees AS e
						INNER JOIN users u ON ( u.employee_id = e.id AND u.is_disabled = 0 )
						INNER JOIN user_roles ur ON ( u.id = ur.user_id AND ur.deleted_on IS NULL AND ur.deleted_by IS NULL )
					WHERE 
						ur.role_id IN ( ' . implode( ',', array_keys( CRole::$c_arrintOrderFormTypeIdsByUserRoles ) ) . ' )
					AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . '
					ORDER BY e.preferred_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomEmployeesByGroupId( $intGroupId, $objDatabase ) {

		if( false == valId( $intGroupId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						e.name_full
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id )
						JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
					WHERE
						e.employee_status_type_id <> ' . CEmployeeStatusType::NO_SHOW . '
						AND e.date_terminated IS NULL
						AND u.is_disabled = 0
						AND ug.group_id =' . ( int ) $intGroupId . '
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeByGroupIdByUserId( $intGroupId, $intUserId, $objDatabase ) {

		if( false == valId( $intGroupId ) || false == valId( $intUserId ) ) {
			return 0;
		}

		$strSql = 'SELECT
						count(id) as total_records
					FROM
						user_groups
					WHERE
						group_id = ' . ( int ) $intGroupId . '
						AND user_id = ' . ( int ) $intUserId . '
						AND deleted_by IS NULL';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) ) {
			return $arrmixData[0]['total_records'];
		} else {
			return 0;
		}
	}

	public static function fetchTeamEmployeesDetailByManagerEmployeeIds( $arrintManagerEmployeeIds, $objDatabase ) {

		if( false == valIntArr( $arrintManagerEmployeeIds ) ) {
			return NULL;
		}

		$strSql	= 'WITH RECURSIVE all_employees( employee_id ) AS
					(
							SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id
							FROM
								employees AS e
							WHERE
								e.reporting_manager_id IN( ' . implode( ',', $arrintManagerEmployeeIds ) . ' )
						UNION ALL
							SELECT
								e.id AS employee_id,
								al.manager_employee_id
							FROM
								employees AS e,
								all_employees AS al
							WHERE
								e.reporting_manager_id = al.employee_id
					)
					SELECT
						ae.employee_id,
						ae.manager_employee_id,
						e.preferred_name
					FROM
						all_employees AS ae
						JOIN employees AS e ON ( ae.employee_id = e.id )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesWithDetailsByIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						e.*,
						d.name AS designation_name,
						t.id AS team_id,
						t.name AS team_name,
						t.logo_file_path AS team_logo_file_path, 
						t.preferred_name AS team_preferred_name
					FROM
						employees e
						JOIN designations d on ( d.id = e.designation_id )
						JOIN team_employees te on ( te.employee_id = e.id )
						JOIN teams t on ( t.id = te.team_id )
					WHERE
						te.is_primary_team = 1
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCustomEmployeesByDesignationIdsByRoleIds( $arrintDesignationIds, $arrintRoleIds,$objDatabase, $boolIncludePastEmployees = false ) {

		if( false == valArr( $arrintDesignationIds ) || false == valArr( $arrintRoleIds ) ) {
			return NULL;
		}

		$strUnionSql = '';

		$strWhere = ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		if( true == CStrings::strToBool( $boolIncludePastEmployees ) ) {
			$strUnionSql = ' UNION ALL
								SELECT
									e.*
								FROM
									employees e
								JOIN users u ON ( u.employee_id = e.id )
								JOIN user_roles ur ON ( u.id = ur.user_id )
								JOIN designations ds ON ( ds.id = e.designation_id )
								WHERE
									ur.role_id IN ( ' . sqlIntImplode( $arrintRoleIds ) . ' )';
		}

		$strSql = 'SELECT
						e.*
					FROM
						employees e
						JOIN users u ON ( u.employee_id = e.id )
						JOIN user_roles ur ON ( u.id = ur.user_id )
						JOIN designations ds ON ( ds.id = e.designation_id )
					WHERE
						( e.designation_id IN ( ' . sqlIntImplode( $arrintDesignationIds ) . ' ) AND ur.role_id IN ( ' . sqlIntImplode( $arrintRoleIds ) . ' ) )
						' . $strWhere . '
					' . $strUnionSql . '
					ORDER BY
						name_full';
		return self::fetchEmployees( $strSql, $objDatabase );

	}

	public static function fetchEmployeeIdByDeskId( $intDeskId, $objDatabase ) {
		if( false == valId( $intDeskId ) ) {
			return NULL;
		}
		$strSql = 'SELECT 
						id 
					FROM 
					employees 
					WHERE
					employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					AND office_desk_id = ' . $intDeskId;
		$arrintData = fetchData( $strSql, $objDatabase );
		return $arrintData[0];

	}

	public static function fetchEmployeesJoinedBeforeMonths( $strMonth, $objDatabase, $boolFetchConfirmed = false ) {
		$strDateConfirmedSql = ( true == $boolFetchConfirmed ) ? 'e.date_confirmed IS NOT NULL' : 'e.date_confirmed IS NULL';
		$strSql = '
				SELECT
					e.preferred_name,
					e.date_started,
					e.date_confirmed,
					e.id AS employee_id,
					e.email_address,
					e1.id AS reporting_manager_id,
					e1.preferred_name AS reporting_manager,
					e1.email_address AS reporting_manager_email_address
				FROM
					employees e
					JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . '  AND ea.country_code = \'' . CCountry::CODE_INDIA . '\' )
					JOIN employees as e1 ON ( e1.id = e.reporting_manager_id AND e1.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::SYSTEM . ', ' . CEmployeeStatusType::NO_SHOW . ' ) )
				WHERE
					' . $strDateConfirmedSql . '
					AND e.date_started  = ( CURRENT_DATE - INTERVAL \'' . addslashes( $strMonth ) . '\' )
					AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					AND e.expected_termination_date IS NULL
					AND e.date_terminated IS NULL
				ORDER BY
					e.preferred_name';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllReportingMangersByCountryCode( $strCountryCode, $objDatabase ) {

		if( false == valStr( $strCountryCode ) ) return NULL;

		$strSql	= 'SELECT DISTINCT (e.*)
					FROM ps_website_job_postings pwjp
						JOIN job_posting_purchase_requests jppr ON (jppr.ps_job_posting_id =
							pwjp.id)
						JOIN resource_requisitions rr ON (rr.purchase_request_id =
							jppr.purchase_request_id)
						JOIN employees e ON (e.id = rr.manager_employee_id)
					WHERE pwjp.deleted_by IS NULL AND
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND 
						pwjp.deleted_on IS NULL AND
						pwjp.country_code = \'' . addslashes( $strCountryCode ) . '\'
					ORDER BY e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchNewSaleEmailStakeholders( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT e.id,
						e.preferred_name,
						e.employee_status_type_id
					FROM
						employees e
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						LEFT JOIN contract_product_stakeholders cps ON ( cps.employee_id = e.id )
					WHERE
						e.id = ' . CEmployee::ID_BILLING_BILLING . '
						OR cps.employee_id IS NOT NULL
						OR ( ea.country_code = \'' . CCountry::CODE_USA . '\' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
					ORDER BY 
						e.preferred_name';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeesOnNoticePeriodByManagerId( $intManagerId, $objDatabase ) {
		if( false == is_numeric( $intManagerId ) ) {
			return false;
		}

		$strSql = 'SELECT
						count(e.id) as noticed_employee_count
					FROM
						employees e
					WHERE
						( e.reporting_manager_id = ' . ( int ) $intManagerId . ' )
						AND e.employee_status_type_id =' . ( int ) CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS  NULL  AND e.resigned_on IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDeskAvailabilityByEmpIdsByDeskIds( $intSourceEmpID, $intDestEmpID, $intSourceDeskId, $intDestDeskId, $objDatabase ) {
		if( false == valId( $intSourceEmpID ) || false == valId( $intDestEmpID ) || false == valId( $intSourceDeskId ) || false == valId( $intDestDeskId ) ) {
			return NULL;
		}
		$strSql = 'SELECT count(id) as count
		FROM employees
		WHERE ( office_desk_id = ' . $intSourceDeskId . ' AND id = ' . $intSourceEmpID . ' ) OR
		      ( office_desk_id = ' . $intDestDeskId . ' AND id = ' . $intDestEmpID . ' ) ';
		$arrintData = fetchData( $strSql, $objDatabase );
		return $arrintData[0];

	}

	public static function fetchCurrentEmployeesByManagerIdHavingUserId( $intManagerId, $objDatabase, $boolIsIncludeManager = false ) {

		$strWhere = '';

		if( true == $boolIsIncludeManager ) {
			$strWhere = ' or e.id = ' . ( int ) $intManagerId;
		}

		$strSql = 'SELECT
						e.id,
						e.preferred_name,
						e.resigned_on,
						e.designation_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
						JOIN users u ON( u.employee_id = e.id )
					WHERE
						( e.reporting_manager_id = ' . ( int ) $intManagerId . $strWhere . ' )
						AND e.employee_status_type_id =' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						AND u.employee_id IS NOT NULL
					ORDER BY
						e.name_full';

		return self::fetchEmployees( $strSql, $objDatabase );
	}

}
?>
