<?php

class CDmTemplateBundleAssociation extends CBaseDmTemplateBundleAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDmTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDmBundleIds() {
		$boolIsValid = true;

		if( false == valArr( $this->m_arrintDmBundleIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Please select at least 1 bundle' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDmBundleIds();

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>