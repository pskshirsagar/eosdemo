<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingQuestions
 * Do not add any new functions to this class.
 */

class CJobPostingQuestions extends CBaseJobPostingQuestions {

	public static function fetchPaginatedJobPostingQuestionsByJobPostingIdByCountryCode( $intJobPostingId, $intPageNo, $intPageSize, $strCountryCode, $objDatabase ) {

		if( false == is_numeric( $intJobPostingId ) ) return NULL;

		$intOffset			 = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit			 = ( int ) $intPageSize;
		$strWhereCondition	 = '';

		if( CCountry::CODE_USA == $strCountryCode ) {
			$strWhereCondition = ' OR jpq.job_posting_id = ' . CPsWebsiteJobPosting::DEFAULT_QUESTIONS_USA;
		}

		$strSql = ' SELECT
						jpq.*,
						sqt.name as type_name
					FROM
						job_posting_questions jpq
						JOIN survey_question_types sqt ON ( jpq.question_type_id = sqt.id )
					WHERE
						( jpq.job_posting_id =' . ( int ) $intJobPostingId . $strWhereCondition . ')
						AND jpq.deleted_on IS NULL
					ORDER BY
						id DESC OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . $intLimit;

		return self::fetchJobPostingQuestions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedJobPostingQuestionCountByJobPostingIdByCountryCode( $intJobPostingId, $strCountryCode, $objDatabase ) {

		if( false == is_numeric( $intJobPostingId ) ) return NULL;

		$strWhereCondition	 = '';

		if( CCountry::CODE_USA == $strCountryCode ) {
			$strWhereCondition = ' OR jpq.job_posting_id = ' . CPsWebsiteJobPosting::DEFAULT_QUESTIONS_USA;
		}

		$strSql = ' SELECT
						count(*)
					FROM
						job_posting_questions jpq
						JOIN survey_question_types sqt ON ( jpq.question_type_id = sqt.id )
					WHERE
						( jpq.job_posting_id =' . ( int ) $intJobPostingId . $strWhereCondition . ')
						AND jpq.deleted_on IS NULL';

		$arrintJobPostingQuestionsCount = fetchData( $strSql, $objDatabase );
		return ( true == isset( $arrintJobPostingQuestionsCount[0]['count'] ) ) ? $arrintJobPostingQuestionsCount[0]['count'] : 0;
	}

	public static function fetchJobPostingQuestionsByIds( $arrintJobPostingQuestionIds, $objDatabase ) {

		if( false == valArr( $arrintJobPostingQuestionIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						job_posting_questions
					WHERE
						id IN (' . implode( ',', $arrintJobPostingQuestionIds ) . ')';

		return self::fetchJobPostingQuestions( $strSql, $objDatabase );
	}

	public static function fetchJobPostingQuestionsWithDetailsByJobPostingIdByEmployeeApplicationId( $intJobPostingId, $intEmployeeApplicationId, $objDatabase ) {

		if( false == is_numeric( $intJobPostingId ) || false == is_numeric( $intEmployeeApplicationId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						jpq.question,
						jpqr.answer,
						row_number() OVER( partition by jpq.question ORDER BY COALESCE( jpqr.id, 0 ) DESC ) as rank
					FROM
						job_posting_questions jpq
						LEFT JOIN job_posting_question_responses jpqr ON ( jpqr.question_id = jpq.id AND jpqr.employee_application_id =' . ( int ) $intEmployeeApplicationId . ' )
					WHERE
						( ( jpq.job_posting_id = ' . ( int ) $intJobPostingId . ' ) OR jpqr.employee_application_id = ' . ( int ) $intEmployeeApplicationId . ' )
						AND jpq.is_published = 1
						AND jpq.deleted_on IS NULL
					ORDER BY
						jpq.id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>