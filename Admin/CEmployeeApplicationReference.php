<?php

class CEmployeeApplicationReference extends CBaseEmployeeApplicationReference {

	public function valCompanyName( $intCount ) {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', 'Reference ' . ( int ) ( $intCount + 1 ) . ': Company Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst( $intCount, $boolIsHrTab = false, $boolIsReferenceRequired = false ) {
		$boolIsValid = true;
		$strMessage = ( false == $boolIsHrTab ) ? 'Reference ' . ( int ) ( $intCount + 1 ) . ': ' : '';
		if( true == is_null( $this->getNameFirst() ) && true == $boolIsReferenceRequired ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', $strMessage . ' First name is required.' ) );
		} elseif( false == is_null( $this->getNameFirst() ) && false == \Psi\CStringService::singleton()->preg_match( '/^\pL+$/u', $this->m_strNameFirst ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', $strMessage . ' First name should be set of alphabets.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast( $intCount, $boolIsHrTab = false, $boolIsReferenceRequired = false ) {
		$boolIsValid = true;
		$strMessage = ( false == $boolIsHrTab ) ? 'Reference ' . ( int ) ( $intCount + 1 ) . ': ' : '';
		if( true == is_null( $this->getNameLast() ) && true == $boolIsReferenceRequired ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', $strMessage . ' Last name is required.' ) );
		} elseif( false == is_null( $this->getNameLast() ) && false == \Psi\CStringService::singleton()->preg_match( '/^\pL+$/u', $this->m_strNameLast ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', $strMessage . ' Last name should be set of alphabets.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber( $intCount, $boolIsHrTab = false, $boolIsReferenceRequired = false ) {
		$boolIsValid = true;
		$strMessage = ( false == $boolIsHrTab ) ? 'Reference ' . ( int ) ( $intCount + 1 ) . ': ' : '';
		if( true == is_null( $this->getPhoneNumber() ) && true == $boolIsReferenceRequired ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', $strMessage . ' Phone number is required.' ) );
		} elseif( false == is_null( $this->getPhoneNumber() ) && 1 != CValidation::checkPhoneNumberFullValidation( $this->m_strPhoneNumber, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', $strMessage . ' Phone number must be in xxx-xxx-xxxx format.', 611 ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress( $intCount, $boolIsHrTab = false, $boolIsReferenceRequired = false ) {

		$boolIsValid = true;
		$strMessage = ( false == $boolIsHrTab ) ? 'Reference ' . ( int ) ( $intCount + 1 ) . ': ' : '';
		if( true == empty( $this->m_strEmailAddress ) && true == $boolIsReferenceRequired ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', $strMessage . ' Email address is required.' ) );
		} elseif( false == empty( $this->m_strEmailAddress ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', $strMessage . ' Valid email address is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeApplicationReferencePhoneNumber( $intCount ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Reference ' . ( int ) ( $intCount + 1 ) . ': Phone number is required.' ) );
		} elseif( false == is_null( $this->getPhoneNumber() ) && 1 != CValidation::checkPhoneNumberFullValidation( $this->m_strPhoneNumber, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Reference ' . ( int ) ( $intCount + 1 ) . ': Phone number must be in xxx-xxx-xxxx format.', 611 ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intCount, $boolIsHrTab = false, $boolIsReferenceRequired = false ) {
		$boolIsValid = true;
		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:

			case 'validate_employee_application_reference':
				$boolIsValid &= $this->valNameFirst( $intCount, $boolIsHrTab, $boolIsReferenceRequired );
				$boolIsValid &= $this->valNameLast( $intCount, $boolIsHrTab, $boolIsReferenceRequired );
				$boolIsValid &= $this->valPhoneNumber( $intCount, $boolIsHrTab, $boolIsReferenceRequired );
				$boolIsValid &= $this->valEmailAddress( $intCount, $boolIsHrTab, $boolIsReferenceRequired );
				break;

			case 'validate_xento_employment_application':
				$boolIsValid &= $this->valNameFirst( $intCount, $boolIsHrTab, $boolIsReferenceRequired );
				$boolIsValid &= $this->valNameLast( $intCount, $boolIsHrTab, $boolIsReferenceRequired );
				$boolIsValid &= $this->valPhoneNumber( $intCount, $boolIsHrTab, $boolIsReferenceRequired );
				$boolIsValid &= $this->valEmailAddress( $intCount, $boolIsHrTab, $boolIsReferenceRequired );
				break;

			case 'validate_xento_employment_application_reference2':
				$boolIsValid &= $this->valPhoneNumber( $intCount );
				$boolIsValid &= $this->valEmailAddress( $intCount );
				break;

			default:
				// no validation avaliable for delete and update action
				break;
		}

		return $boolIsValid;
	}

}
?>