<?php

class CEmployeeTickerTypes extends CBaseEmployeeTickerTypes {

	public static function fetchEmployeeTickerTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CEmployeeTickerType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEmployeeTickerType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CEmployeeTickerType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>