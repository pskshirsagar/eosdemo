<?php

class CTestAnswerOption extends CBaseTestAnswerOption {

	/**
	 * Validation Functions
	 *
	 */

	public function valAnswerOption() {
		$boolIsValid = true;

		if( true == is_null( $this->getAnswerOption() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer_option', 'Answer option is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAnswerOption();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	* other Functions
	*
	*/

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setAnswerOption( addslashes( $this->getAnswerOption() ) );

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setAnswerOption( addslashes( $this->getAnswerOption() ) );

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}
}
?>