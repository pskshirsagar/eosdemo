<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStoryPointCommissions
 * Do not add any new functions to this class.
 */

class CStoryPointCommissions extends CBaseStoryPointCommissions {

	public static function fetchTeamEmployeesCountByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = ' SELECT
						COUNT(te.employee_id),
						spc.commission_month,
						EXTRACT(MONTH FROM spc.commission_month) AS employee_count_for_month
					FROM
						story_point_commissions spc
						JOIN team_employees te on ( spc.team_id = te.team_id )
					WHERE
						te.employee_id = ' . ( int ) $intEmployeeId . '
						AND te.is_primary_team = 1
					GROUP BY
						spc.commission_month';

		return fetchData( $strSql, $objDatabase );
	}

}
?>