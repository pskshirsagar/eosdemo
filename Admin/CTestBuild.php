<?php

class CTestBuild extends CBaseTestBuild {

	const BUILD_FIRST_TAB	= 1;
	const BUILD_SECOND_TAB	= 2;
	const BUILD_THIRD_TAB	= 3;
	const BUILD_FOURTH_TAB	= 4;
	const ACTIVE_BUILD		= 1;
	const INACTIVE_BUILD	= 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Test Build Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTestCaseProductFolderId() {
		$boolIsValid = true;

		if( false == valId( $this->getTestCaseProductFolderId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_case_product_folder_id', 'Test Case Product Folder is required.' ) );
		}

		return $boolIsValid;
	}

	public function valReleaseDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getReleaseDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'release_date', 'Release Date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsStandardRelease() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQaLeadEmployeeId() {
		$boolIsValid = true;

		if( false == valId( $this->getQaLeadEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qa_lead_employee_id', 'QA Team Lead is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valQaLeadEmployeeId();
				$boolIsValid &= $this->valTestCaseProductFolderId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>