<?php

class CRecruitmentDrive extends CBaseRecruitmentDrive {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDriveType() {
		$boolIsValid = true;

		if( false == in_array( $this->getDriveType(), [ 'Scheduled', 'Walk In', 'Mixed' ] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'drive_type', 'Drive type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valScheduledOn() {
		$boolIsValid = true;

		if( false == valStr( $this->getScheduledOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_on', 'Scheduled on is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getCountryCode() ) || false == valStr( $this->getCountryCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', 'Country code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valScheduledOn();
				$boolIsValid &= $this->valCountryCode();
				$boolIsValid &= $this->valDriveType();

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>