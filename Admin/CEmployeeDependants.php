<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeDependants
 * Do not add any new functions to this class.
 */

class CEmployeeDependants extends CBaseEmployeeDependants {

	public static function fetchEmployeeDependantsByIds( $arrintEmployeeDependantsIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeDependantsIds ) ) return NULL;

		$strSql = 'SELECT * FROM employee_dependants WHERE id IN ( ' . implode( ',', $arrintEmployeeDependantsIds ) . ' )';

		return self::fetchEmployeeDependants( $strSql, $objDatabase );
	}

	public static function fetchEmployeeDependantsByEmployeeIds( $arrEmployeeIds, $objDatabase ) {

		$strSql = 'SELECT
						id,
						employee_id,
						dependant_type_id,
						name_first,
						name_last,
						gender,
						contact_number,
						birth_date
					FROM
						employee_dependants
					WHERE
						dependant_type_id = ' . CDependantType::SPOUSE . '
						OR dependant_type_id = ' . CDependantType::CHILD . '
						AND employee_id IN ( ' . implode( $arrEmployeeIds, ', ' ) . ' )
					ORDER BY
						employee_id';

		return self::fetchEmployeeDependants( $strSql, $objDatabase );
	}

	public function fetchRowCountByEmployeeIds( $arrEmployeeIds, $objDatabase ) {
		$strWhere = 'WHERE
						(dependant_type_id = ' . CDependantType::SPOUSE . '
						OR dependant_type_id = ' . CDependantType::CHILD . ')
						 AND employee_id IN ( ' . implode( $arrEmployeeIds, ', ' ) . ' )';

		return self::fetchRowCount( $strWhere, 'employee_dependants', $objDatabase );
	}


}
?>