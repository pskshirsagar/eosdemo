<?php

class COfficeRoom extends CBaseOfficeRoom {

	protected $m_strOfficeName;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['office_name'] ) )			$this->setOfficeName( $arrmixValues['office_name'] );
	}

	public function setOfficeName( $strOfficeName ) {
		$this->m_strOfficeName = CStrings::strTrimDef( $strOfficeName, 50, NULL, true );
	}

	public function getOfficeName() {
		return $this->m_strOfficeName;
	}

}
?>