<?php

class CTransferProperty extends CBaseTransferProperty {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreviousCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreviousPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillingStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreviousContractTerminationRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ? 'nextval( \'public.transfer_properties_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
								public.transfer_properties
							SELECT ' .
								$strId . ', ' .
								$this->sqlCid() . ', ' .
								$this->sqlPropertyId() . ', ' .
								$this->sqlPreviousCid() . ', ' .
								$this->sqlPreviousPropertyId() . ', ' .
								$this->sqlTaskId() . ', ' .
								$this->sqlBillingStartDate() . ', ' .
								$this->sqlPreviousContractTerminationRequestId() . ', ' .
								( int ) $intCurrentUserId . ', ' .
								$this->sqlUpdatedOn() . ', ' .
								( int ) $intCurrentUserId . ', ' .
								$this->sqlCreatedOn() . '
							WHERE NOT EXISTS (
								SELECT 
									id 
								FROM transfer_properties tp 
								WHERE 
									tp.cid = ' . $this->sqlCid() . '
									AND tp.property_id = ' . $this->sqlPropertyId() . '
									AND tp.previous_cid = ' . $this->sqlPreviousCid() . '	
									AND tp.previous_property_id = ' . $this->sqlPreviousPropertyId() . '	
							) RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>