<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsSupportEmployees
 * Do not add any new functions to this class.
 */

class CStatsSupportEmployees extends CBaseStatsSupportEmployees {

	public static function fetchSupportEmployeeStackRank( $objStdFilter, $objDatabase ) {

		$strSql = '
			SELECT *
			FROM (
				SELECT
					totals.employee_id,
					totals.date_started,
					totals.preferred_name,
					totals.num_employees,
					totals.is_manager,
					totals.ticket_contact_count,
					totals.avg_minutes_to_contact,
					totals.first_day_completions,
					calculate_grade( CASE
										WHEN totals.first_day_completions > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.first_day_completions ) ) * 10 )
										ELSE 1
									END::INTEGER ) AS first_day_completions_grade,
					totals.nps_count,
					totals.average_nps_score::numeric( 15, 1 ),
					calculate_grade( CASE
										WHEN totals.average_nps_score > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.average_nps_score ) ) * 10 )
										ELSE 1
									END::INTEGER ) AS average_nps_score_grade,
					totals.qa_count,
					totals.qa_expertise_rating,
					totals.qa_professionalism_rating,
					totals.non_escalated_new_calls::integer,
					totals.non_escalated_new_chats::integer,
					totals.non_escalated_new_texts::integer,
					totals.non_escalated_new_emails::integer,
					totals.non_escalated_new_total::integer,
					totals.non_escalated_open_calls::integer,
					totals.non_escalated_open_chats::integer,
					totals.non_escalated_open_texts::integer,
					totals.non_escalated_open_emails::integer,
					totals.non_escalated_open_total::integer,
					calculate_grade( CASE
										WHEN totals.non_escalated_open_total > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.non_escalated_open_total ) ) * 10 )
										ELSE 1
									END::INTEGER ) AS non_escalated_open_total_grade,
					totals.non_escalated_closed_calls::integer,
					totals.non_escalated_closed_chats::integer,
					totals.non_escalated_closed_texts::integer,
					totals.non_escalated_closed_emails::integer,
					totals.non_escalated_closed_total::integer,
					calculate_grade( CASE
										WHEN totals.non_escalated_closed_total > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.non_escalated_closed_total ) ) * 10 )
										ELSE 1
									END::INTEGER ) AS non_escalated_closed_total_grade,
					( totals.non_escalated_avg_open_mins / 1440.0 ) ::numeric( 15, 2 ) AS non_escalated_avg_open_days,
					calculate_grade( CASE
										WHEN totals.non_escalated_avg_open_mins > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.non_escalated_avg_open_mins DESC ) ) * 10 )
										ELSE 10
									END::INTEGER ) AS non_escalated_avg_open_days_grade,
					totals.escalated_new::integer,
					totals.escalated_open::integer,
					totals.escalated_closed::integer,
					( totals.escalated_avg_open_mins / 1440.0 ) ::numeric( 15, 2 ) AS escalated_avg_open_days,
					calculate_grade( CASE
										WHEN totals.escalated_avg_open_mins > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.escalated_avg_open_mins DESC ) ) * 10 )
										ELSE 10
									END::INTEGER ) AS escalated_avg_open_days_grade,
					totals.reopen_count::integer,
					calculate_grade( CASE
										WHEN totals.reopen_count > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.reopen_count DESC) ) * 10 )
										ELSE 10
									END::INTEGER ) AS reopen_count_grade,
					totals.phone_total_active_mins,
					totals.answered_calls,
					calculate_grade( CASE
										WHEN totals.answered_calls > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.answered_calls ) ) * 10 )
										ELSE 1
									END::INTEGER ) AS answered_calls_grade,
					totals.phone_avg_available_mins,
					calculate_grade( CASE
										WHEN totals.phone_avg_available_mins > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.phone_avg_available_mins ) ) * 10 )
										ELSE 1
									END::INTEGER ) AS phone_avg_available_mins_grade,
					( totals.avg_minutes_to_close / 1440.0 ) ::NUMERIC( 15, 2 ) AS avg_days_to_close,
					calculate_grade( CASE
										WHEN totals.avg_minutes_to_close > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.avg_minutes_to_close DESC ) ) * 10 )
										ELSE 10
									END::INTEGER ) AS avg_days_to_close_grade
				FROM (
					SELECT
						managers.manager_emp_id AS employee_id,
						e.preferred_name,
						e.date_started,
						COUNT( DISTINCT managers.employee_id ) AS num_employees,
						CASE WHEN 1 < COUNT( DISTINCT managers.employee_id ) THEN 1 ELSE 0 END AS is_manager,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.ticket_contact_count ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS ticket_contact_count,
						CASE
							WHEN SUM( sse.ticket_contact_count ) > 0 THEN ( SUM( sse.avg_minutes_to_contact * sse.ticket_contact_count ) / SUM( ticket_contact_count ) )
							ELSE 0
						END AS avg_minutes_to_contact,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.first_day_completions ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS first_day_completions,
						SUM( sse.nps_count ) AS nps_count,
						CASE
							WHEN SUM( sse.nps_count ) > 0 THEN ( SUM( sse.average_nps_score * sse.nps_count ) / SUM( nps_count ) )
							ELSE 0
						END AS average_nps_score,
						SUM( sse.qa_count ) AS qa_count,
						CASE
							WHEN SUM( sse.qa_count ) > 0 THEN ( SUM( sse.qa_expertise_rating * sse.qa_count ) / SUM( nps_count ) )
							ELSE 0
						END AS qa_expertise_rating,
						CASE
							WHEN SUM( sse.qa_count ) > 0 THEN ( SUM( sse.qa_professionalism_rating * sse.qa_count ) / SUM( nps_count ) )
							ELSE 0
						END AS qa_professionalism_rating,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.non_escalated_new_calls ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS non_escalated_new_calls,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.non_escalated_new_chats ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS non_escalated_new_chats,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.non_escalated_new_texts ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS non_escalated_new_texts,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.non_escalated_new_emails ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS non_escalated_new_emails,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.non_escalated_new_total ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS non_escalated_new_total,
						AVG( sse.non_escalated_open_calls ) AS non_escalated_open_calls,
						AVG( sse.non_escalated_open_chats ) AS non_escalated_open_chats,
						AVG( sse.non_escalated_open_texts ) AS non_escalated_open_texts,
						AVG( sse.non_escalated_open_emails ) AS non_escalated_open_emails,
						AVG( sse.non_escalated_open_total ) AS non_escalated_open_total,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.non_escalated_closed_calls ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS non_escalated_closed_calls,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.non_escalated_closed_chats ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS non_escalated_closed_chats,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.non_escalated_closed_texts ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS non_escalated_closed_texts,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.non_escalated_closed_emails ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS non_escalated_closed_emails,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.non_escalated_closed_total ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS non_escalated_closed_total,
						CASE
							WHEN SUM( sse.non_escalated_open_total ) > 0 THEN ( SUM( sse.non_escalated_avg_open_mins * sse.non_escalated_open_total ) / SUM( non_escalated_open_total ) )
							ELSE 0
						END AS non_escalated_avg_open_mins,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.escalated_new ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS escalated_new,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.escalated_open ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS escalated_open,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.escalated_closed ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS escalated_closed,
						CASE
							WHEN SUM( sse.escalated_open ) > 0 THEN ( SUM( sse.escalated_avg_open_mins * sse.escalated_open ) / SUM( escalated_open ) )
							ELSE 0
						END AS escalated_avg_open_mins,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sse.reopen_count ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS reopen_count,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sa.phone_total_active_mins ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS phone_total_active_mins,
						CASE
							WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sa.answered_calls ) / COUNT( DISTINCT managers.employee_id )
							ELSE 0
						END AS answered_calls,
						CASE
							WHEN SUM( CASE WHEN sa.phone_total_active_mins > 0 THEN 1 ELSE 0 END ) > 0 THEN SUM( sa.phone_total_active_mins ) / SUM( CASE WHEN sa.phone_total_active_mins > 0 THEN 1 ELSE 0 END )
							ELSE 0
						END AS phone_avg_available_mins,
						CASE
							WHEN SUM( sse.escalated_closed + sse.non_escalated_closed_total ) > 0 THEN SUM( sse.avg_minutes_to_close * ( sse.escalated_closed + sse.non_escalated_closed_total ) ) / SUM( sse.escalated_closed + sse.non_escalated_closed_total )
							ELSE 0
						END AS avg_minutes_to_close
					FROM (
						SELECT
						( get_all_parent_managers_from_employee( e.id ) ) . manager_emp_id,
						e.id as employee_id
						FROM employees e
						WHERE e.department_id = ' . CDepartment::TECHNICAL_SUPPORT . '
						UNION ALL
						SELECT
						e.id AS manager_emp_id,
						e.id as employee_id
						FROM employees e
						WHERE e.department_id = ' . CDepartment::TECHNICAL_SUPPORT . '
					) AS managers
						JOIN stats_support_employees sse ON ( managers.employee_id = sse.employee_id )
						LEFT JOIN stats_agents sa ON ( sse.employee_id = sa.employee_id AND sse.day = sa.day )
						JOIN employees e ON ( managers.manager_emp_id = e.id )
						JOIN departments d ON ( e.department_id = d.id )
					WHERE
						sse.day >( NOW( ) - INTERVAL \'1 month\' )
						AND e.department_id = ' . CDepartment::TECHNICAL_SUPPORT . '
						AND e.id <> ' . CEmployee::ID_SYSTEM_MONITOR . '
					GROUP BY
						manager_emp_id,
						e.preferred_name,
						e.date_started,
						d.name
				) AS totals
				ORDER BY ' . ( ( false == valStr( $objStdFilter->sort_by ) ) ? '1' : $objStdFilter->sort_by . ' ' . $objStdFilter->sort_direction ) . '
			) AS full_result_set
			WHERE ( 1 = 1 )
			' . ( ( false == is_null( $objStdFilter->employees ) ) ? ' AND employee_id IN ( ' . $objStdFilter->employees . ' ) ' : '' ) . '
			' . ( ( false == is_null( $objStdFilter->is_manager ) ) ? ' AND is_manager = ' . $objStdFilter->is_manager : '' ) . '
			;
		';

		return fetchData( $strSql, $objDatabase );
	}

	public static function rebuildSupportEmployeeStats( $intSupportEmployeeId, $objDatabase ) {

		if( true == is_numeric( $intSupportEmployeeId ) ) {
			$strEmployeeCondition = ' = ' . $intSupportEmployeeId;
		} else {
			$strEmployeeCondition = ' <> 0 ';
		}

		$objDatabase->begin();
		if( false === fetchData( self::buildSupportStatsInsertSql( $strEmployeeCondition ), $objDatabase ) ) {
			$objDatabase->rollback();
			return false;
		}
		$objDatabase->commit();

		$strSql = self::buildSupportNpsScoreData( $strEmployeeCondition );
		$strSql .= self::buildSupportTicketCountData( $strEmployeeCondition );
		$strSql .= self::buildQualityAssuranceData( $strEmployeeCondition );
		$strSql .= self::buildEscalatedTicketData( $strEmployeeCondition );
		$strSql .= self::buildReopenCounts( $strEmployeeCondition );
		$strSql .= self::buildAverageMinutesToContact( $strEmployeeCondition );
		$strSql .= self::buildFirstDayCompletionCounts( $strEmployeeCondition );

		return fetchData( $strSql, $objDatabase );
	}

	public static function buildSupportStatsInsertSql( $strEmployeeCondition ) {

		$strSql = '
				DELETE FROM stats_support_employees WHERE employee_id ' . $strEmployeeCondition . ';

				INSERT INTO
					public.stats_support_employees (
							id,
							employee_id,
							department_id,
							day,
							created_by,
							created_on )
					SELECT
						nextval ( \'stats_support_employees_id_seq\' ) AS id,
						e.id AS employee_id,
						e.department_id,
						d.date,
						1 AS created_by,
						NOW ( ) AS created_on
					FROM
						( SELECT
								e.id,
								e.department_id,
								e.date_started,
								e.date_terminated
							FROM
								employees e
							WHERE
								e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintSupportDepartmentIds ) . ' )
								AND e.employee_status_type_id NOT IN (' . CEmployeeStatusType::SYSTEM . ',' . CEmployeeStatusType::NO_SHOW . ' )
							GROUP BY
								e.id,
								e.department_id,
								e.date_started,
								e.date_terminated ) e
						JOIN dates d ON ( DATE_TRUNC ( \'month\', d.date ) <= DATE_TRUNC ( \'month\', NOW ( ) ) )
						LEFT JOIN stats_support_employees sse ON ( d.date = sse.day AND e.id = sse.employee_id )
					WHERE
						DATE_TRUNC ( \'day\', e.date_started )::DATE <= d.date
						AND e.id ' . $strEmployeeCondition . '
						AND ( e.date_terminated IS NULL OR DATE_TRUNC ( \'month\', e.date_terminated )::DATE >= d.date )
						AND sse.id IS NULL;';

		return $strSql;
	}

	public static function buildSupportNpsScoreData( $strEmployeeCondition ) {

		$strSql = 'UPDATE
						stats_support_employees sse
					SET
						nps_count = data.nps_count,
						average_nps_score = data.average_nps_score
					FROM
						(
						SELECT
							e.id AS employee_id,
							DATE_TRUNC ( \'day\', s.response_datetime ) ::DATE AS DAY,
							count ( s.id ) AS nps_count,
							CASE
								WHEN count ( s.id ) > 0 THEN ( SUM ( sto.numeric_weight ) ::INTEGER / count ( s.id ) ::INTEGER )
								ELSE 0
							END AS average_nps_score
						FROM
							survey_question_answers sqa
							JOIN surveys s ON ( sqa.survey_id = s.id )
							JOIN survey_template_options sto ON ( sto.id = sqa.survey_template_option_id )
							JOIN tasks t ON ( s.trigger_reference_id = t.id )
							JOIN users u ON ( u.id = t.user_id )
							JOIN employees e ON ( e.id = u.employee_id )
						WHERE
							sqa.survey_template_question_id = ' . CSurveyTemplateQuestion::QUESTION_SUPPORT_NPS_SCORE . '
						GROUP BY
							e.id,
							DATE_TRUNC ( \'day\', s.response_datetime )
						) AS data
					WHERE
						sse.employee_id = data.employee_id
						AND sse.employee_id ' . $strEmployeeCondition . '
						AND sse.day = data.day
						AND ( sse.nps_count <> COALESCE( data.nps_count, 0 )
								OR
							sse.average_nps_score <> COALESCE( data.average_nps_score, 0 ) );';

		return $strSql;
	}

	public static function buildSupportTicketCountData( $strEmployeeCondition ) {

		$strSql = ' UPDATE
						stats_support_employees sse
					SET
						non_escalated_new_calls = data.non_escalated_new_calls,
						non_escalated_new_chats = data.non_escalated_new_chats,
						non_escalated_new_texts = data.non_escalated_new_texts,
						non_escalated_new_emails = data.non_escalated_new_emails,
						non_escalated_new_total = data.non_escalated_new_total
					FROM
						( 
							SELECT 
								t.task_datetime::DATE AS DAY,
								u.employee_id, 
								COUNT( CASE WHEN t.task_medium_id IS NULL OR t.task_medium_id IN ( ' . implode( ',', CTaskMedium::$c_arrintPhoneTaskMediumIds ) . ' ) THEN t.id ELSE NULL END ) AS non_escalated_new_calls,
								COUNT( CASE WHEN t.task_medium_id = ' . CTaskMedium::LIVE_CHAT . ' THEN t.id ELSE NULL END ) AS non_escalated_new_chats,
								COUNT( CASE WHEN t.task_medium_id = ' . CTaskMedium::TEXT . ' THEN t.id ELSE NULL END ) AS non_escalated_new_texts,
								COUNT( CASE WHEN t.task_medium_id = ' . CTaskMedium::EMAIL . ' THEN t.id ELSE NULL END ) AS non_escalated_new_emails,
								COUNT( t.id ) AS non_escalated_new_total
							FROM
								tasks t
								JOIN users u ON ( t.user_id = u.id )
							WHERE
								t.task_type_id = ' . CTaskType::SUPPORT . ' 
								AND NOT EXISTS ( SELECT 1 FROM task_associations ta WHERE ta.task_id = t.id ) 
							GROUP BY 
								t.task_datetime::DATE,
								u.employee_id
						 ) AS data
					WHERE
						data.employee_id = sse.employee_id
						AND data.day = sse.day
						AND sse.employee_id ' . $strEmployeeCondition . '
						AND ( sse.non_escalated_new_calls <> COALESCE( data.non_escalated_new_calls, 0 )
							OR sse.non_escalated_new_chats <> COALESCE( data.non_escalated_new_chats, 0 )
							OR sse.non_escalated_new_texts <> COALESCE( data.non_escalated_new_texts, 0 )
							OR sse.non_escalated_new_emails <> COALESCE( data.non_escalated_new_emails, 0 )
							OR sse.non_escalated_new_total <> COALESCE( data.non_escalated_new_total, 0 ) );';

		$strSql .= ' UPDATE
						stats_support_employees sse
					SET
						non_escalated_closed_calls = data.non_escalated_closed_calls,
						non_escalated_closed_chats = data.non_escalated_closed_chats,
						non_escalated_closed_texts = data.non_escalated_closed_texts,
						non_escalated_closed_emails = data.non_escalated_closed_emails,
						non_escalated_closed_total = data.non_escalated_closed_total
					FROM
						( 
							SELECT 
								t.completed_on::DATE AS day,
								u.employee_id,
								COUNT( CASE WHEN t.task_medium_id IS NULL OR t.task_medium_id IN ( ' . implode( ',', CTaskMedium::$c_arrintPhoneTaskMediumIds ) . ' ) THEN t.id ELSE NULL END) AS non_escalated_closed_calls,
								COUNT( CASE WHEN t.task_medium_id = ' . CTaskMedium::LIVE_CHAT . ' THEN t.id ELSE NULL END ) AS non_escalated_closed_chats,
								COUNT( CASE WHEN t.task_medium_id = ' . CTaskMedium::TEXT . ' THEN t.id ELSE NULL END ) AS non_escalated_closed_texts,
								COUNT( CASE WHEN t.task_medium_id = ' . CTaskMedium::EMAIL . ' THEN t.id ELSE NULL END ) AS non_escalated_closed_emails,
								COUNT( t.id ) AS non_escalated_closed_total
							FROM 
								tasks t
								JOIN users u ON ( t.user_id = u.id )
							WHERE 
								t.task_type_id = ' . CTaskType::SUPPORT . ' 
								AND	t.completed_on IS NOT NULL 
								AND NOT EXISTS ( SELECT 1 FROM task_associations ta WHERE ta.task_id = t.id ) 
							GROUP BY 
								t.completed_on::DATE,
								u.employee_id
						 ) AS data
					WHERE
						data.employee_id = sse.employee_id
						AND sse.employee_id ' . $strEmployeeCondition . '
						AND data.day = sse.day
						AND ( sse.non_escalated_closed_calls <> COALESCE( data.non_escalated_closed_calls, 0 )
							OR sse.non_escalated_closed_chats <> COALESCE( data.non_escalated_closed_chats, 0 )
							OR sse.non_escalated_closed_texts <> COALESCE( data.non_escalated_closed_texts, 0 )
							OR sse.non_escalated_closed_emails <> COALESCE( data.non_escalated_closed_emails, 0 )
							OR sse.non_escalated_closed_total <> COALESCE( data.non_escalated_closed_total, 0 ) );';

		$strSql .= ' UPDATE
						stats_support_employees sse
					SET
						non_escalated_open_calls = data.non_escalated_open_calls,
						non_escalated_open_chats = data.non_escalated_open_chats,
						non_escalated_open_texts = data.non_escalated_open_texts,
						non_escalated_open_emails = data.non_escalated_open_emails,
						non_escalated_open_total = data.non_escalated_open_total
					FROM
						( 
							SELECT 
								d.date AS DAY,
								t.user_id,
								COUNT( CASE WHEN t.task_medium_id IS NULL OR t.task_medium_id IN ( ' . implode( ',', CTaskMedium::$c_arrintPhoneTaskMediumIds ) . ' ) THEN t.id ELSE NULL END) AS non_escalated_open_calls,
								COUNT( CASE WHEN task_medium_id = ' . CTaskMedium::LIVE_CHAT . ' THEN t.id ELSE NULL END ) AS non_escalated_open_chats,
								COUNT( CASE WHEN task_medium_id = ' . CTaskMedium::TEXT . ' THEN t.id ELSE NULL END ) AS non_escalated_open_texts,
								COUNT( CASE WHEN task_medium_id = ' . CTaskMedium::EMAIL . ' THEN t.id ELSE NULL END ) AS non_escalated_open_emails,
								COUNT( t.id ) AS non_escalated_open_total
							FROM 
								tasks t
								JOIN dates d ON ( d.date >= t.task_datetime::date AND d.date <= COALESCE( t.completed_on, NOW() ) )
							WHERE 
								t.task_type_id = ' . CTaskType::SUPPORT . ' AND
								t.completed_on IS NOT NULL
								AND NOT EXISTS ( SELECT 1 FROM task_associations ta WHERE ta.task_id = t.id ) 
							GROUP BY 
								d.date,
								t.user_id
						 ) AS data
						 JOIN users u ON (data.user_id = u.id)
					WHERE
						u.employee_id = sse.employee_id
						AND sse.employee_id ' . $strEmployeeCondition . '
						AND data.day = sse.day
						AND ( sse.non_escalated_open_calls <> COALESCE( data.non_escalated_open_calls, 0 )
							OR sse.non_escalated_open_chats <> COALESCE( data.non_escalated_open_chats, 0 )
							OR sse.non_escalated_open_texts <> COALESCE( data.non_escalated_open_texts, 0 )
							OR sse.non_escalated_open_emails <> COALESCE( data.non_escalated_open_emails, 0 )
							OR sse.non_escalated_open_total <> COALESCE( data.non_escalated_open_total, 0 ) );';

		$strSql .= ' UPDATE
							stats_support_employees sse
						SET
							non_escalated_avg_open_mins = data.non_escalated_avg_open_mins
						FROM
							(
								SELECT 
									t.user_id,
									d.date AS DAY,
									COUNT( t.id ) AS ticket_count,
									CASE WHEN COUNT( t.id ) > 0 THEN (SUM( EXTRACT( EPOCH FROM ( COALESCE( t.completed_on, d.date ) - t.task_datetime ) ) ) / 60 / COUNT( t.id ) )::INTEGER ELSE 0 END AS non_escalated_avg_open_mins
								FROM tasks t
									JOIN dates d ON ( d.date >= t.task_datetime::date AND d.date <= COALESCE( t.completed_on, NOW() ) )
								WHERE 
									t.task_type_id = ' . CTaskType::SUPPORT . ' AND
									t.completed_on IS NOT NULL 
									AND NOT EXISTS ( SELECT 1 FROM task_associations ta WHERE ta.task_id = t.id )
								GROUP BY 
									d.date,
									t.user_id
							) AS data
							JOIN users u ON ( data.user_id = u.id )
						WHERE
							u.employee_id = sse.employee_id
							AND sse.employee_id ' . $strEmployeeCondition . '
							AND data.day = sse.day
							AND sse.non_escalated_avg_open_mins <> COALESCE ( data.non_escalated_avg_open_mins, 0 );';

		return $strSql;
	}

	// We can't code this one until the QA system / script is finished.

	public static function buildQualityAssuranceData() {
		return '';
	}

	public static function buildEscalatedTicketData( $strEmployeeCondition ) {

		$strSql = ' UPDATE
						stats_support_employees sse
					SET
						escalated_new = data.escalated_new
					FROM
						( 
							SELECT 
								t.task_datetime::DATE AS DAY,
								t.user_id,
								COUNT( t.id ) as escalated_new    
							FROM 
								tasks t
								JOIN task_associations ta ON ( ta.task_id = t.id )
							WHERE 
								t.task_type_id = ' . CTaskType::SUPPORT . '
							GROUP BY 
								t.task_datetime::DATE,
								t.user_id
						) AS data
						JOIN users u ON ( data.user_id = u.id )
					WHERE
						u.employee_id = sse.employee_id
						AND sse.employee_id ' . $strEmployeeCondition . '
						AND data.day = sse.day
						AND sse.escalated_new <> COALESCE( data.escalated_new, 0 );';

		$strSql .= ' UPDATE
						stats_support_employees sse
					SET
						escalated_closed = data.escalated_closed
					FROM
						( 
							SELECT 
								t.user_id,
								COUNT( t.id ) AS escalated_closed,
								t.completed_on::DATE AS DAY
							FROM 
								tasks t
								JOIN task_associations ta ON ( ta.task_id = t.id )
							WHERE 
								t.task_type_id = ' . CTaskType::SUPPORT . ' AND
								t.completed_on IS NOT NULL
							GROUP BY 
								t.completed_on::DATE,
								t.user_id
						) AS data
						JOIN users u ON ( data.user_id = u.id )
					WHERE
						u.employee_id = sse.employee_id
						AND sse.employee_id ' . $strEmployeeCondition . '
						AND data.day = sse.day
						AND sse.escalated_closed <> COALESCE( data.escalated_closed, 0 ); ';

		$strSql .= 'UPDATE
						stats_support_employees sse
					SET
						escalated_open = data.escalated_open
					FROM
						( 
							SELECT 
								t.user_id,
								COUNT( t.id ) AS escalated_open,
								d.date AS DAY
							FROM tasks t
							   JOIN dates d ON ( d.date >= t.task_datetime::DATE AND d.date <= COALESCE( t.completed_on, NOW() ) )
							   JOIN task_associations ta ON ( ta.task_id = t.id )
							WHERE 
								t.task_type_id = ' . CTaskType::SUPPORT . '
							GROUP BY 
								d.date,
								t.user_id
						) AS data
						JOIN users u ON ( data.user_id = u.id )
					WHERE
						u.employee_id = sse.employee_id
						AND sse.employee_id ' . $strEmployeeCondition . '
						AND data.day = sse.day
						AND sse.escalated_open <> COALESCE( data.escalated_open, 0 );';

		$strSql .= 'UPDATE
						stats_support_employees sse
					SET
						escalated_avg_open_mins = data.escalated_avg_open_mins
					FROM
						(
							SELECT 
								t.user_id,
								COUNT( t.id ) AS ticket_count,
								d.date AS DAY,
								CASE WHEN COUNT( t.id ) > 0 THEN (SUM( EXTRACT( EPOCH FROM ( COALESCE( t.completed_on, D.date ) - t.task_datetime ) ) ) / 60 / COUNT( t.id ) )::INTEGER ELSE 0 END AS escalated_avg_open_mins
							FROM 
								tasks t
								JOIN dates d ON ( d.date >= t.task_datetime::DATE AND d.date <= COALESCE( t.completed_on, NOW() ) )
								JOIN task_associations ta ON ( ta.task_id = t.id )
							WHERE 
								t.task_type_id = ' . CTaskType::SUPPORT . '
							GROUP BY 
								d.date,
								t.user_id
						) AS data
						JOIN users u ON ( data.user_id = u.id )
					WHERE
						u.employee_id = sse.employee_id
						AND sse.employee_id ' . $strEmployeeCondition . '
						AND data.day = sse.day
						AND sse.escalated_avg_open_mins <> COALESCE ( data.escalated_avg_open_mins, 0 );';

		$strSql .= 'UPDATE
						stats_support_employees sse
					SET
						avg_minutes_to_close = data.avg_minutes_to_close
					FROM
						(
							SELECT
								u.employee_id,
								count ( t.id ) AS ticket_count,
								DATE_TRUNC ( \'day\', t.completed_on ) ::DATE AS day,
								CASE
									WHEN count ( t.id ) > 0
									THEN ( SUM ( EXTRACT( EPOCH FROM ( t.completed_on - t.task_datetime ) ) ) / 60 / count ( t.id ) )::INTEGER
									ELSE 0
								END AS avg_minutes_to_close
							FROM
								tasks t
								JOIN users u ON ( t.user_id = u.id )
							WHERE
								t.task_type_id = ' . CTaskType::SUPPORT . '
								AND t.completed_on IS NOT NULL
							GROUP BY
								DATE_TRUNC ( \'day\', t.completed_on ),
								u.employee_id
						) AS data
					WHERE
						data.employee_id = sse.employee_id
						AND sse.employee_id ' . $strEmployeeCondition . '
						AND data.day = sse.day
						AND sse.avg_minutes_to_close <> COALESCE ( data.avg_minutes_to_close, 0 );';

		return $strSql;
	}

	public static function buildReopenCounts( $strEmployeeCondition ) {

		$strSql = 'UPDATE
							stats_support_employees sse
						SET
							reopen_count = data.reopen_count
						FROM
							( 
								SELECT 
									t.user_id,
									ta_incomplete.created_on::DATE AS DAY,
									COUNT( t.id ) AS reopen_count
								FROM
									tasks t 
									JOIN task_assignments ta_complete ON ( ta_complete.task_id = t.id ) 
									JOIN task_assignments ta_incomplete ON ( ta_complete.task_id = ta_incomplete.task_id AND ta_complete.created_on < ta_incomplete.created_on )
								WHERE 
									t.task_type_id = ' . CTaskType::SUPPORT . '
									AND ta_complete.task_status_id = ' . CTaskStatus::COMPLETED . '
									AND ta_incomplete.task_status_id <> ' . CTaskStatus::COMPLETED . '
								GROUP BY 
									t.id,
									t.user_id,
									ta_incomplete.created_on::DATE
							) AS data
							JOIN users u ON ( data.user_id = u.id )
						WHERE
							u.employee_id = sse.employee_id
							AND sse.employee_id ' . $strEmployeeCondition . '
							AND data.day = sse.day
							AND sse.reopen_count <> COALESCE ( data.reopen_count, 0 );';

		return $strSql;
	}

	// We'll have to wait on this one.  Until we have a good event log on a task, we won't be able to generate this data effectively.

	public static function buildAverageMinutesToContact( $strEmployeeCondition ) {

	}

	public static function buildFirstDayCompletionCounts( $strEmployeeCondition ) {

		$strSql = ' UPDATE
						stats_support_employees sse
					SET
						first_day_completions = data.first_day_completions
					FROM
						(
						SELECT
							u.employee_id,
							count ( t.id ) AS first_day_completions,
							DATE_TRUNC ( \'day\', t.completed_on ) ::DATE AS DAY
						FROM
							tasks t
							JOIN users u ON ( t.user_id = u.id )
						WHERE
							t.task_type_id = ' . CTaskType::SUPPORT . '
							AND t.completed_on IS NOT NULL
							AND ( t.completed_on - t.task_datetime ) < INTERVAL \'1 day\'
						GROUP BY
							DATE_TRUNC ( \'day\', t.completed_on ),
							u.employee_id
						) AS data
					WHERE
						data.employee_id = sse.employee_id
						AND sse.employee_id ' . $strEmployeeCondition . '
						AND data.day = sse.day
						AND sse.first_day_completions <> COALESCE ( data.first_day_completions, 0 );';

		return $strSql;
	}

}
?>