<?php

class CPsJobPostingStepStatus extends CBasePsJobPostingStepStatus {

	public static $c_arrmixDefaultPsJobPostingStepStatusCssClassess = array(
		2 => array( 'class' => 'greenbulb' ),
		3 => array( 'class' => 'yellowbulb' ),
		4 => array( 'class' => 'check-box' ),
		5 => array( 'class' => 'redbulb' )
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>