<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CVestingSchedules
 * Do not add any new functions to this class.
 */

class CVestingSchedules extends CBaseVestingSchedules {

	public static function fetchPaginatedVestingSchedules( $arrstrVestingScheduleFilter, $objAdminDatabase, $boolIsForCount = false ) {

		if( true == isset( $arrstrEmployeeOptionsFilter['order_by_field'] ) ) {

			switch( $arrstrEmployeeOptionsFilter['order_by_field'] ) {

				case 'name':
					$strOrderByField = ' vs.name';
					break;

				case 'description':
					$strOrderByField = ' vs.description ';
					break;

				case 'is_published':
					$strOrderByField = ' vs.is_published ';
					break;

				default:
					$strOrderByField = ' vs.order_num ';
					break;
			}
		}

		$strOrderByField  	= ( false == empty( $strOrderByField ) ) ? $strOrderByField : 'vs.order_num';
		$strOrderByType  	= ( false == empty( $arrstrVestingScheduleFilter['order_by_type'] ) ) ? $arrstrVestingScheduleFilter['order_by_type'] : 'ASC';
		$intOffset			= ( false == $boolIsForCount && false == empty( $arrstrVestingScheduleFilter['page_no'] ) && false == empty( $arrstrVestingScheduleFilter['page_size'] ) ) ? $arrstrVestingScheduleFilter['page_size'] * ( $arrstrVestingScheduleFilter['page_no'] - 1 ) : 0;
		$intLimit			= ( false == $boolIsForCount && true == isset( $arrstrVestingScheduleFilter['page_size'] ) ) ? $arrstrVestingScheduleFilter['page_size'] : '';

		$strSelectClause	= ( false == $boolIsForCount ) ?  'vs.*' : 'vs.id';

		$strSql = ' SELECT
						' . $strSelectClause . '
					FROM
						 vesting_schedules vs
					ORDER BY
						 ' . $strOrderByField . ' ' . $strOrderByType . ' NULLS LAST
					OFFSET ' . ( int ) $intOffset;

		if( true == $boolIsForCount ) {
			$arrintVestingSchedulesCount = fetchData( $strSql, $objAdminDatabase );

			return ( true == valArr( $arrintVestingSchedulesCount ) ) ? \Psi\Libraries\UtilFunctions\count( $arrintVestingSchedulesCount ) : 0;
		}

		if( 0 < $intLimit ) $strSql .= ' LIMIT ' . ( int ) $intLimit;

		return self::fetchVestingSchedules( $strSql, $objAdminDatabase );
	}

	public static function fetchAllPublishedVestingSchedules( $objAdminDatabase ) {

		$strSql = ' SELECT
						 *
					FROM
						vesting_schedules
					WHERE
						is_published=true
					ORDER BY
						order_num';

		return self::fetchVestingSchedules( $strSql, $objAdminDatabase );
	}

	public static function fetchVestingSchedulesByIds( $arrintVestingScheduleIds, $objAdminDatabase ) {

		if( false == valArr( $arrintVestingScheduleIds ) ) return NULL;

		$strSql = ' SELECT
						 *
					FROM
						vesting_schedules
					WHERE
						id IN( ' . implode( ', ', $arrintVestingScheduleIds ) . ' )';

		return self::fetchVestingSchedules( $strSql, $objAdminDatabase );
	}

}
?>