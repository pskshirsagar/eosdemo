<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupScheduleTypes
 * Do not add any new functions to this class.
 */

class CGroupScheduleTypes extends CBaseGroupScheduleTypes {

	public static function fetchGroupScheduleTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGroupScheduleType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchGroupScheduleType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGroupScheduleType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllGroupScheduleTypes( $objDatabase ) {
		return self::fetchCachedObjects( 'SELECT * FROM group_schedule_types', 'CGroupScheduleType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>