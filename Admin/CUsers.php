<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUsers
 * Do not add any new functions to this class.
 */

class CUsers extends CBaseUsers {

	public static function fetchUsersByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchUsers( 'SELECT DISTINCT ON (u.id) u.* FROM users u, user_groups ug WHERE u.id = ug.user_id AND ug.deleted_by IS NULL AND ug.group_id::integer = ' . ( int ) $intGroupId . '::integer', $objDatabase );
	}

	public static function fetchUsersByGroupIdByRoleId( $intGroupId, $intRoleId, $objDatabase ) {
		return self::fetchUsers( 'SELECT DISTINCT ON (u.id) u.* FROM users u, user_groups ug, user_roles ur WHERE ( u.id = ug.user_id AND ug.deleted_by IS NULL ) AND u.id = ur.user_id AND ug.group_id::integer = ' . ( int ) $intGroupId . '::integer AND ur.role_id::integer = ' . ( int ) $intRoleId . '::integer AND is_disabled != 1', $objDatabase );
	}

	public static function fetchUsersByRoleId( $intRoleId, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON (u.id) u.id, u.username,u.password_encrypted, u.is_administrator, ur.role_id, epn.phone_number, emp.id as employee_id, emp.email_address, emp.name_full, emp.name_first, emp.name_last
					FROM
						users u JOIN user_roles ur ON ( u.id = ur.user_id AND ur.role_id = ' . ( int ) $intRoleId . ' AND deleted_on IS NULL )
						LEFT JOIN employee_phone_numbers epn ON ( u.employee_id = epn.employee_id )
						LEFT JOIN employees emp ON ( u.employee_id = emp.id )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchSuperUsersAndUsersByRoleId( $intRoleId, $objDatabase ) {
		$strSql = '( SELECT
						DISTINCT ON (u.id) u.id, u.username,u.password_encrypted, epn.phone_number,emp.email_address,emp.preferred_name
					FROM
						users u
						LEFT JOIN employees emp ON ( u.employee_id = emp.id )
						LEFT JOIN employee_phone_numbers epn ON ( u.employee_id = epn.employee_id )
						WHERE u.is_super_user=1
					)
					UNION
					( SELECT
						DISTINCT ON (u.id) u.id, u.username,u.password_encrypted, epn.phone_number,emp.email_address,emp.preferred_name
					 FROM
						users u JOIN user_roles ur ON ( u.id = ur.user_id AND ur.role_id = ' . ( int ) $intRoleId . ' AND deleted_on IS NULL )
						LEFT JOIN employee_phone_numbers epn ON ( u.employee_id = epn.employee_id )
						LEFT JOIN employees emp ON ( u.employee_id = emp.id )
					)';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUserWithEmployeeInfoById( $intId, $objDatabase ) {
		$strSql = 'SELECT
						u.id,
						u.username,
						u.password_encrypted,
						u.is_administrator,
						u.active_directory_guid,
						epn.phone_number,
						emp.email_address
					FROM
						users u LEFT JOIN employee_phone_numbers epn ON ( u.employee_id = epn.employee_id )
						LEFT JOIN employees emp ON ( u.employee_id = emp.id )
					Where u.id = ' . ( int ) $intId;

		return self::fetchUser( $strSql, $objDatabase );
	}

	public static function fetchConflictingSvnUserCountBySvnUsernameById( $strSvnUsername, $intId, $objDatabase ) {
		$strWhereSql = ' WHERE svn_username = \'' . trim( addslashes( $strSvnUsername ) ) . '\' AND id::integer!=' . ( int ) $intId . '::integer';

		return self::fetchUserCount( $strWhereSql, $objDatabase );
	}

	public static function fetchUserByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchUser( 'SELECT * FROM users WHERE employee_id = ' . ( int ) $intEmployeeId . ' LIMIT 1 ', $objDatabase );
	}

	public static function fetchUsersByIds( $arrintUserIds, $objDatabase ) {
		if( false == valArr( $arrintUserIds ) ) return NULL;
		return self::fetchUsers( 'SELECT * FROM users WHERE id IN ( ' . implode( ',', $arrintUserIds ) . ' ) ', $objDatabase );
	}

	public static function fetchResponsibleCollectionsUser( $objDatabase ) {

		$strSql = 'SELECT
						u.*
					FROM
						users u,
						employees e,
						system_settings ss
					WHERE
						u.employee_id = e.id
						AND ss.key = \'EMPLOYEE_RESPONSIBLE_FOR_COLLECTIONS\'
						AND ss.value = e.id::text';

		return self::fetchUser( $strSql, $objDatabase );
	}

	public static function fetchAllManagers( $objDatabase ) {

		$strSql = 'SELECT
						u1.*
					FROM
						users u1,
						employees e1
					WHERE
						u1.employee_id = e1.id
						AND u1.id IN ( 	SELECT
											DISTINCT ON (u.id) u.id
										FROM
											users u,
											employees e
										WHERE
											u.employee_id = e.reporting_manager_id 
									 )
					ORDER BY
						lower( e1.name_first ),lower( u1.username )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUserByUsername( $strUsername, $objDatabase ) {
		return self::fetchUser( 'SELECT * FROM users WHERE username = \'' . addslashes( trim( $strUsername ) ) . '\'', $objDatabase );
	}

	public static function fetchUsersWithNameByIds( $arrintUserIds, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) || false == valArr( array_filter( $arrintUserIds ) ) ) return NULL;

		$strSql = 'SELECT
						u.id,
						u.employee_id,
						u.username,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.email_address,
						e.name_full
					FROM
						users u
						JOIN employees e ON ( e.id = u.employee_id )
					WHERE
						u.id IN ( ' . implode( ',', array_filter( $arrintUserIds ) ) . ' )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUsersByGroupIds( $arrintGroupIds, $objDatabase ) {
		return self::fetchUsers( 'SELECT DISTINCT ON (u.id) u.* FROM users u, user_groups ug WHERE ( u.id = ug.user_id AND ug.deleted_by IS NULL ) AND ug.group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' ) ', $objDatabase );
	}

	public static function fetchUsersByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		return self::fetchUsers( 'SELECT * FROM users WHERE employee_id IN( ' . implode( ',', $arrintEmployeeIds ) . ' )', $objDatabase );
	}

	public static function fetchUserDetailsById( $intId, $objDatabase, $boolFetchDept = false ) {

		if( true == $boolFetchDept ) {
			 $strSql = 'SELECT dep.id as department_id';
		} else {
			 $strSql = 'SELECT u.*,
						e.name_first,
						e.name_last,
						e.name_full,
						e.preferred_name,
						dep.name as department_name,
						des.name as designation_name,
						dep.id as department_id';
		}

		$strSql .= ' FROM
						users u
						JOIN employees e ON ( e.id = u.employee_id )
						LEFT JOIN departments dep ON ( e.department_id = dep.id )
						LEFT JOIN designations des ON ( e.designation_id = des.id )
					WHERE
						u.id =' . ( int ) $intId;

		return self::fetchUser( $strSql, $objDatabase );
	}

	public static function fetchUserByIdByGroupId( $intId, $intGroupId, $objDatabase ) {
		return self::fetchUser( 'SELECT u.id FROM users u, user_groups ug WHERE u.id= ' . ( int ) $intId . ' AND u.id = ug.user_id AND ug.deleted_by IS NULL AND ug.group_id = ' . ( int ) $intGroupId, $objDatabase );
	}

	public static function fetchUsersWithNameByEmployeeIds( $arrintEmployeeIds, $objDatabase, $boolActiveUsersOnly = false ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						us.*,
						em.name_first,
						em.name_last,
						em.preferred_name,
						em.reporting_manager_id AS manager_id
					FROM
						users us
						JOIN employees em ON (us.employee_id = em.id)
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						' . ( true == $boolActiveUsersOnly ? 'us.is_disabled <> 1 AND ' : '' ) . '
						em.id NOT IN ( 1, 2 )
						AND em.id IN( ' . implode( ',', $arrintEmployeeIds ) . ' )
					ORDER BY
						lower( em.name_first ),lower( em.preferred_name ),lower( us.username )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchAllProjectManagerUsers( $objDatabase ) {

		$strSql = 'SELECT
						us.*,
						em.name_first,
						em.name_last
					FROM
						users us
						JOIN employees em ON ( us.employee_id = em.id )
					WHERE
						em.is_project_manager = 1 AND
						em.employee_status_type_id = 1 AND
						em.date_terminated IS NULL AND
						em.id NOT IN ( 1, 2 )
					ORDER BY
						lower( em.name_first ),
						lower( us.username )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchAllActiveUsersByFilteringEmployeeIds( $arrintEmployeeIds, $objDatabase, $strCountryCode = NULL ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strCountrySortOrder = ( 'US' == $strCountryCode ) ? 'DESC' : 'ASC';

		$strSql = 'SELECT
						us.*,
						em.name_first,
						em.name_last,
						em.preferred_name,
						mngr.id as manager_id
					FROM
						users us
						JOIN employees em ON ( us.employee_id = em.id )
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT OUTER JOIN users mngr ON ( mngr.employee_id = em.reporting_manager_id )
					WHERE
						us.is_disabled <> 1
						AND em.date_terminated IS NULL
						AND em.id NOT IN ( 1, 2 )
						And em.id NOT IN (' . implode( ',', $arrintEmployeeIds ) . ' )
					ORDER BY
						lower( ea.country_code ) ' . $strCountrySortOrder . ',
						lower( em.name_first ),
						lower( em.preferred_name ),
						lower( us.username )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUserBySvnUsername( $strSvnUsername, $objDatabase ) {
		return self::fetchUser( sprintf( 'SELECT * FROM users WHERE svn_username = \'%s\'', $strSvnUsername ), $objDatabase );
	}

	public static function fetchAllPaginatedUsers( $intPageNo, $intPageSize, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL, $boolActiveUsersOnly = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
						us.*,
						em.name_first,
						em.name_last,
						em.preferred_name AS preferred_name,
						em.email_address AS email_address,
						em.work_station_ip_address AS work_station_ip_address,
						em.dns_handle AS dns_handle,
						em.office_id,
						mngr.id as manager_id,
						es.name AS status_types,
						epn.phone_number,
						ep.value AS hide_phone_number
					FROM
						users us
						JOIN employees em ON (us.employee_id = em.id)
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT OUTER JOIN users mngr ON (mngr.employee_id = em.reporting_manager_id )
						LEFT OUTER JOIN employee_status_types as es ON ( em.employee_status_type_id = es.id )
						LEFT OUTER JOIN employee_phone_numbers as epn ON ( us.employee_id = epn.employee_id AND ( epn.phone_number = NULL OR epn.phone_number_type_id = 1 ) )
						LEFT JOIN employee_preferences ep ON( em.id = ep.employee_id AND ep.key = \'' . CEmployeePreference::HIDE_PHONE_NUMBER . '\')
					WHERE
						' . ( true == $boolActiveUsersOnly?'us.is_disabled <> 1 AND':'' ) . '
						em.id NOT IN ( 1, 2 ) ';

		if( 'name_first' == $strOrderByField ) {
			$strSql .= ' ORDER BY CASE WHEN em.preferred_name IS NULL THEN em.name_first ELSE em.preferred_name END ' . ' ' . $strOrderByType;
		} else {
			$strSql .= ' ORDER BY ' . addslashes( $strOrderByField ) . ' ' . $strOrderByType;
		}

			$strSql .= ' OFFSET ' . ( int ) $intOffset . '
					 	LIMIT ' . ( int ) $intLimit;

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedUsersCount( $objDatabase, $boolActiveUsersOnly = false ) {

		$strSql = 'SELECT
						count( * )
					FROM
						users us
						JOIN employees em ON (us.employee_id = em.id)
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT OUTER JOIN users mngr ON (mngr.employee_id = em.reporting_manager_id )
						LEFT OUTER JOIN employee_status_types as es ON ( em.employee_status_type_id = es.id )
						LEFT OUTER JOIN employee_phone_numbers as epn ON ( us.employee_id = epn.employee_id AND ( epn.phone_number = NULL OR epn.phone_number_type_id = 1 ) )
					WHERE
						' . ( true == $boolActiveUsersOnly?'us.is_disabled <> 1 AND':'' ) . '
						em.id NOT IN ( 1, 2 )';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchEnabledPaginatedUsers( $intPageNo, $intPageSize, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL, $intUserId = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strUserIdCondition = ( false == is_null( $intUserId ) ) ? ' OR us.id = ' . ( int ) $intUserId : '';

		$strSql = 'SELECT
						us.*,
						em.name_first,
						em.name_last,
						em.preferred_name AS preferred_name,
						em.email_address AS email_address,
						em.work_station_ip_address AS work_station_ip_address,
						em.dns_handle AS dns_handle,
						em.office_id,
						es.name AS status_types,
						epn.phone_number,
						ep.value AS hide_phone_number,
						em.reporting_manager_id As manager_id
					FROM
						users us
						LEFT OUTER JOIN employees as em ON (us.employee_id = em.id)
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT OUTER JOIN employee_status_types as es ON ( em.employee_status_type_id = es.id )
						LEFT JOIN employee_phone_numbers as epn ON ( us.employee_id = epn.employee_id AND ( epn.phone_number = NULL OR epn.phone_number_type_id = 1 ) )
						LEFT JOIN employee_preferences ep ON( em.id = ep.employee_id AND ep.key = \'' . CEmployeePreference::HIDE_PHONE_NUMBER . '\')
					WHERE
						( is_disabled <> 1 ' . $strUserIdCondition . ')
						AND us.employee_id = em.id ';

		if( 'name_first' == $strOrderByField ) {
			$strSql .= ' ORDER BY CASE WHEN em.preferred_name IS NULL THEN em.name_first ELSE em.preferred_name END ' . ' ' . $strOrderByType;
		} else {
			$strSql .= ' ORDER BY ' . addslashes( $strOrderByField ) . ' ' . $strOrderByType;
		}

			$strSql .= ' OFFSET ' . ( int ) $intOffset . '
					 LIMIT ' . ( int ) $intLimit;

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchEnabledPaginatedUsersCount( $objDatabase, $intUserId = NULL ) {

		$strUserIdCondition = ( false == is_null( $intUserId ) ) ? ' OR us.id = ' . ( int ) $intUserId : '';

		$strSql = 'SELECT
						count( * )
					FROM
						users us
						LEFT OUTER JOIN employees em ON (us.employee_id = em.id)
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT OUTER JOIN employee_status_types as es ON ( em.employee_status_type_id = es.id )
						LEFT OUTER JOIN employee_phone_numbers as epn ON ( us.employee_id = epn.employee_id AND ( epn.phone_number = NULL OR epn.phone_number_type_id = 1 ) )
					WHERE
						( is_disabled <> 1 ' . $strUserIdCondition . ')
						AND us.employee_id = em.id
						AND em.id NOT IN ( 1, 2 )';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchSearchedUsers( $arrstrFilteredExplodedSearch, $objDatabase, $intIsDisabled ) {

		$strIsDisabledCondition = ( 0 == $intIsDisabled ) ? 'us.is_disabled =0 ' : ' (us.is_disabled =0 OR us.is_disabled = ' . ( int ) $intIsDisabled . ')';

		$strSql = 'SELECT
						us.id,
						us.username,
						us.is_disabled,
						em.name_first,
						em.name_last,
						em.name_full,
						em.preferred_name,
						em.dns_handle,
						em.email_address,
						em.work_station_ip_address,
						d.desk_number

					FROM
						users us
						JOIN employees em ON (us.employee_id = em.id)
						LEFT JOIN office_desks d ON ( em.office_desk_id = d.id )
					WHERE
						( em.name_first ILIKE \'%' . implode( '%\' AND em.name_first ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR us.username ILIKE \'%' . implode( '%\' AND us.username ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR em.name_full ILIKE \'%' . implode( '%\' AND em.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR em.preferred_name ILIKE \'%' . implode( '%\' AND em.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR em.email_address ILIKE \'%' . implode( '%\' AND em.email_address ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR em.dns_handle ILIKE \'%' . implode( '%\' AND em.dns_handle ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR em.work_station_ip_address ILIKE \'%' . implode( '%\' AND em.work_station_ip_address ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\')
						AND ' . $strIsDisabledCondition . '
					ORDER BY
						em.preferred_name LIMIT 10';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUsersByGroupOrderByName( $intGroupId, $arrintDesignationIds, $objDatabase ) {

		if( true == is_null( $intGroupId ) ) return NULL;

		$strWhere = '';

		if( true == valArr( $arrintDesignationIds ) ) {
			$strWhere .= 'AND em.designation_id NOT IN( ' . implode( ',', $arrintDesignationIds ) . ')';
		}

		$strSql = 'SELECT
						u.*,
						em.name_first,
						em.name_last,
						em.preferred_name,
						em.designation_id,
						em.email_address
					FROM
						users u
						JOIN employees em ON ( u.employee_id = em.id ),
						user_groups ug
					WHERE
						em.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND u.id = ug.user_id
						AND ug.group_id::integer = ' . ( int ) $intGroupId . '
						AND ug.deleted_by IS NULL
						AND u.is_disabled = 0 ' . $strWhere . '

					ORDER BY
						lower ( em.preferred_name )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUsersByGroupIdByDesignationIds( $intGroupId, $arrintDesignationIds, $objDatabase ) {

		if( true == is_null( $intGroupId ) && false == valArr( $arrintDesignationIds ) ) return NULL;

		$strSql = 'SELECT
						u.*,
						em.name_first,
						em.name_last,
						em.preferred_name,
						em.designation_id,
						em.email_address
					FROM
						users u
						JOIN employees em ON ( u.employee_id = em.id ),
						user_groups ug
					WHERE
						em.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND u.id = ug.user_id
						AND ug.group_id::integer = ' . ( int ) $intGroupId . '
						AND ug.deleted_by IS NULL
						AND u.is_disabled = 0 
						AND em.designation_id IN( ' . implode( ',', $arrintDesignationIds ) . ')
						
					ORDER BY
						lower ( em.preferred_name )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchPermissionedModuleIds( $objUser, $objDatabase ) {

		if( 1 == $objUser->getIsAdministrator() ) {
			$strSql = 'SELECT id AS ps_module_id FROM ps_modules WHERE is_published = 1';
		} else {
			// user permissioned modules
			$strSql = '	SELECT
							ps_module_id
						FROM
							user_permissions
						WHERE
							is_allowed = 1
							AND user_id = ' . ( int ) $objUser->getId() . '
					';

			// group permissioned modules
			$strSql .= 'UNION

						SELECT
							gp.ps_module_id
						FROM
							group_permissions gp
						WHERE
							gp.group_id IN (
											 SELECT
												 ug.group_id
											 FROM
												 user_groups ug
											 WHERE
												 ug.user_id = ' . ( int ) $objUser->getId() . '
												 AND ug.deleted_by IS NULL
							)
							AND gp.ps_module_id NOT IN (
														 SELECT
															 up.ps_module_id
														 FROM
															 user_permissions up
														 WHERE
															 up.is_inherited = 0
															 AND up.user_id = ' . ( int ) $objUser->getId() . '
							)
							AND gp.is_allowed = 1
					';

			// always load public modules
			$strSql .= 'UNION

					SELECT id FROM ps_modules WHERE is_public = 1';
		}

		$arrintResultData = fetchData( $strSql, $objDatabase );

		$arrintModuleIds = array();

		if( true == valArr( $arrintResultData ) ) {
			foreach( $arrintResultData as $arrmixModuleIds ) {
				$arrintModuleIds[$arrmixModuleIds['ps_module_id']] = $arrmixModuleIds['ps_module_id'];
			}
		}

		return $arrintModuleIds;
	}

	public static function fetchUnAssignedUsers( $intGroupId, $objDatabase ) {
		$strSql = 'SELECT
						em.name_first,
						em.name_last,
						em.preferred_name,
						od.desk_number,
						em.designation_id,
						d.name as designation_name,
						u.*
					FROM
						users u
						JOIN employees em ON ( u.employee_id = em.id )
						LEFT JOIN office_desks od ON ( em.office_desk_id = od.id )
						LEFT JOIN designations d ON d.id = em.designation_id
					WHERE
						u.is_disabled = 0
						AND u.id NOT IN (
												SELECT
													DISTINCT u.id
												FROM
													users u
													JOIN user_groups ug ON ( u.id = ug.user_id )
												WHERE
													ug.group_id = ' . ( int ) $intGroupId . ' AND ug.deleted_by IS NULL AND u.is_disabled = 0
						)
					ORDER BY em.preferred_name';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchAllUsersBySprint( $objDatabase ) {
		$strSql = 'SELECT
						u.*,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.reporting_manager_id AS manager_id
					FROM
						users u,
						employees e
						LEFT JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
					WHERE
						u.employee_id = e.id
					ORDER BY
						u.id';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchAllActiveUsersByGroupIds( $arrintGroupIds, $objDatabase ) {

		$strSql = ' SELECT
						us.id,
						us.employee_id,
						us.active_directory_guid,
						em.name_first,
						em.name_last,
						em.name_full,
						ug.group_id,
						em.preferred_name
					FROM
						users us
						JOIN employees em ON ( us.employee_id = em.id AND em.email_address IS NOT NULL )
						JOIN user_groups ug ON ( us.id = ug.user_id)
					WHERE
						us.is_disabled <> 1
						AND em.date_terminated IS NULL
						AND us.id = ug.user_id
						AND ug.deleted_by IS NULL
						AND ( ug.group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' ) OR us.is_administrator = 1 )
					ORDER BY
						CASE
							WHEN em.preferred_name IS NULL
							THEN lower( em.name_full )
							ELSE lower( em.preferred_name )
						END ';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUserInformationById( $intUserId, $objDatabase ) {

		$strSql = 'SELECT e.id as employee_id,
							e.email_address as email_address,
							e.name_first,
							e.name_last
					FROM users as u
						LEFT JOIN employees as e ON u.employee_id = e.id
					WHERE u.id = ' . ( int ) $intUserId;
		return self::fetchUser( $strSql, $objDatabase );
	}

	public static function fetchEmployeeNamesBySvnUsername( $arrstrSvnUsernames, $objDatabase ) {

		if( false == valArr( $arrstrSvnUsernames ) ) return NULL;

		$strSql = 'SELECT
						u.svn_username,
						e.name_full
					FROM
						users u JOIN employees e ON ( e.id = u.employee_id )
					WHERE
						u.svn_username IN (' . implode( ',', $arrstrSvnUsernames ) . ')';
		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchManagerUserIdByEmployeeUserId( $intEmployeeUserId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeUserId ) ) return NULL;

		$strSql = 'SELECT
						mng_usr.id,
						e.email_address,
						e.name_full,
						mng_usr.username
					FROM
						users u
						JOIN employees e1 ON ( e1.id = u.employee_id )
						join employees e on ( e1.reporting_manager_id = e.id )
						join users mng_usr on ( mng_usr.employee_id = e.id )
					WHERE
						u.id = ' . ( int ) $intEmployeeUserId;

		return self::fetchUser( $strSql, $objDatabase );
	}

	public static function fetchUserIdsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		$strSql = 'SELECT u.id,u.employee_id from users u WHERE u.employee_id IN(' . implode( ',', $arrintEmployeeIds ) . ')';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUsersByDepartmentId( $intDepartmentId, $objDatabase ) {

		$strSql = 'SELECT
						u.id
					FROM
						users u
						INNER JOIN employees e ON u.employee_id = e.id
					WHERE
						e.department_id = ' . ( int ) $intDepartmentId;

		return self::fetchUsers( $strSql, $objDatabase );

	}

	public static function fetchQAUsersForPsProducts( $objDatabase ) {
		$strSql = 'SELECT
						pp.id as ps_product_id,
						pp.qa_employee_id,
						u.id as qa_user_id
					FROM
						ps_products pp
						LEFT JOIN users u ON ( u.employee_id = pp.qa_employee_id )
					ORDER BY
						pp.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeeUsers( $objDatabase ) {

		$strSql = 'SELECT
						us.*,
						em.name_first,
						em.name_last,
						em.preferred_name,
						em.email_address
					FROM
						users us
						JOIN employees em ON (us.employee_id = em.id)
						JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = \'' . CAddressType::PRIMARY . '\')
					WHERE
						us.is_disabled <> 1
						AND em.date_terminated IS NULL
						AND em.id NOT IN ( 1, 2 )
					ORDER BY
						lower( em.name_first )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUserByTeamEmployeeId( $intTeamEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						u.*
					FROM
						users u
						JOIN employees e ON ( e.id = u.employee_id ) 
					WHERE
						e.id =' . ( int ) $intTeamEmployeeId . '
						AND e.reporting_manager_id IS NOT NULL
					LIMIT 1';

		return self::fetchUser( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByUserIds( $arrintUserIds, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) ) return;

		$strSql = 'SELECT
						u.id,
						e.name_full,
						e.name_first,
						e.id AS employee_id,
						e.email_address
					FROM
						users u
						LEFT JOIN employees e ON ( u.employee_id = e.id )
					WHERE
						u.id IN ( ' . implode( ',', $arrintUserIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllTrainingUsers( $objDatabase, $strOrderBy = 'name_full', $boolAllEmployees = false ) {
		$strWhereCondition = 'AND em.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT;
		if( true == $boolAllEmployees ) {
			$strWhereCondition = 'AND em.employee_status_type_id IN ( ' . ( int ) CEmployeeStatusType::CURRENT . ',' . ( int ) CEmployeeStatusType::PREVIOUS . ' )';
		}

		$strSql = 'SELECT
						us.*,
						em.id,
						em.name_first,
						em.name_last,
						em.employee_status_type_id,
						em.preferred_name
					FROM
						users us
						JOIN employees em ON ( us.employee_id = em.id )
						JOIN user_groups ug on ( ug.user_id = us.id )
						JOIN groups g on ( g.id = ug.group_id )
					WHERE
						g.id = ' . CGroup::TECHNICAL_WRITERS . '
						AND ug.deleted_by IS NULL ' .
		                $strWhereCondition . '
					ORDER BY ' . $strOrderBy;

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchAllUsDesignerUsers( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						users u
						JOIN employees e ON ( e.id = u.employee_id )
					WHERE
						e.office_id NOT IN ( ' . COffice::TOWER_8 . ',' . COffice::TOWER_9 . ')
						AND e.department_id IN ( ' . CDepartment::CREATIVE . ',' . CDepartment::UX . ')
						AND e.date_terminated IS NULL';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUsersBySvnUsernames( $arrstrUsernames, $objDatabase ) {

		if( false == valArr( $arrstrUsernames ) ) return;

		$strSql = 'SELECT
						u.svn_username,
						u.employee_id,
						e.name_full,
						u.id
					FROM
						users u
						JOIN employees e ON ( e.id = u.employee_id )
					WHERE
						u.svn_username IN ( \'' . implode( '\', \'', array_unique( $arrstrUsernames ) ) . '\' )
					ORDER BY
						u.username';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleUsersByEmployeeIds( $arrintEmployeeIds, $objDatabase, $arrstrColumnNames = NULL ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strColumnNames = ( true == valArr( $arrstrColumnNames ) ? implode( ',', $arrstrColumnNames ) : '*' );

		$strSql			= 'SELECT '
								. $strColumnNames . '
							FROM
								users
							WHERE
								employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllUsersByGroupId( $intGroupId, $objDatabase ) {

		$strSql = ' SELECT
						us.id,
						us.employee_id,
						us.active_directory_guid,
						em.name_first,
						em.name_last,
						em.name_full,
						ug.group_id,
						em.preferred_name
					FROM
						users us
						JOIN employees em ON ( us.employee_id = em.id AND em.email_address IS NOT NULL )
						JOIN user_groups ug ON ( us.id = ug.user_id)
					WHERE
						us.is_disabled <> 1
						AND em.date_terminated IS NULL
						AND us.id = ug.user_id
						AND ug.deleted_by IS NULL
						AND ( ug.group_id = ' . ( int ) $intGroupId . ' )
					ORDER BY
						CASE
							WHEN em.preferred_name IS NULL
							THEN lower( em.name_full )
							ELSE lower( em.preferred_name )
						END ';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUserIdByUserIdByRoleIdWithIsSuperUser( $intUserId, $intRoleId, $objDatabase ) {

		$strSql = 'SELECT
						u.id
					FROM
						users u
						LEFT JOIN user_roles ur ON ( u.id = ur.user_id AND ur.role_id = ' . ( int ) $intRoleId . ' )
					WHERE
						u.id = ' . ( int ) $intUserId . '
						AND ( u.is_super_user = 1 OR ( ur.id IS NOT NULL AND ur.deleted_on IS NULL ) )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllUsersDetails( $objDatabase, $boolActiveUsersOnly = false, $strCountryCode = NULL, $intUserId = NULL, $boolNewTaskSystem = false, $boolNeedSpecificondition = true ) {

		$strWhereCondition		= '';
		$strActiveUserCondition	= '';

		$strCountrySortOrder	= ( 'US' == $strCountryCode ) ? 'DESC' : 'ASC';

		$strActiveUserCondition = '';

		if( true == $boolActiveUsersOnly ) {
			$strActiveUserCondition = ' AND ( u.is_disabled <> 1 ';
			$strActiveUserCondition	.= ( true == is_numeric( $intUserId ) ) ? ' OR u.id = ' . ( int ) $intUserId : '';
			$strActiveUserCondition	.= ')';
		}

		if( true == $boolNewTaskSystem ) {
			$strWhereCondition = 'AND u.id != ' . CUser::ID_DAVID_BATEMAN;
		}

		if( true == $boolNeedSpecificondition ) {
			$strTerminatedEmployeeStatusCondition = 'AND em.date_terminated IS NULL AND em.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ', ' . CEmployeeStatusType::NO_SHOW . ' )';
		}

		$strSql = 'SELECT
						u.id,
						em.id as employee_id,
						em.is_project_manager,
						em.department_id,
						em.name_first,
						em.name_last,
						em.name_full,
						em.preferred_name,
						ea.country_code
					FROM
						users u
						JOIN employees em ON ( u.employee_id = em.id ' . $strTerminatedEmployeeStatusCondition . ' )
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						em.id NOT IN ( 1, 2 ) ' . $strWhereCondition . $strActiveUserCondition . '
					ORDER BY
						em.employee_status_type_id ASC, lower( ea.country_code ) ' . $strCountrySortOrder . ',
						lower(em.preferred_name),
						lower( u.username )';
		return fetchData( $strSql, $objDatabase );
	}

	// Please be careful before using this sql. This sql leads to high memory usage due to fetchUsers().

	public static function fetchAllUsers( $objDatabase, $strCountryCode = NULL, $boolActiveUsersOnly = false ) {

		$strCountrySortOrder = ( 'US' == $strCountryCode ) ? 'DESC' : 'ASC';

		$strSql = 'SELECT
						us.*,
						em.name_first,
						em.name_last,
						em.preferred_name,
						em.email_address,
						mngr.id as manager_id
					FROM
						users us
						JOIN employees em ON (us.employee_id = em.id)
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT OUTER JOIN users mngr ON (mngr.employee_id = em.reporting_manager_id )
					WHERE
						' . ( true == $boolActiveUsersOnly?'us.is_disabled <> 1 AND':'' ) . '
						em.id NOT IN ( 1, 2 )
					ORDER BY
						lower( ea.country_code ) ' . $strCountrySortOrder . ',
						CASE
							WHEN em.preferred_name IS NULL
							THEN lower(em.name_first)
							ELSE lower(em.preferred_name)
						END,
						lower( us.username )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	// Please be careful before using this sql. This sql leads to high memory usage due to fetchUsers().

	public static function fetchAllActiveUsers( $objDatabase, $strCountryCode = NULL, $boolIsAdminUser = false ) {

		$strCountrySortOrder = ( 'US' == $strCountryCode ) ? 'DESC' : 'ASC';

		$strSql = 'SELECT
						us.*,
						em.name_first,
						em.name_last,
						em.preferred_name,
						em.department_id,
						mngr.id as manager_id
					FROM
						users us
						JOIN employees em ON ( us.employee_id = em.id )
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT OUTER JOIN users mngr ON ( mngr.employee_id = em.reporting_manager_id )
					WHERE
						us.is_disabled <> 1
						AND em.date_terminated IS NULL';

		if( true == $boolIsAdminUser ) {
			$strSql .= ' AND em.id NOT IN ( 1, 2 )';
		}

		$strSql .= ' ORDER BY lower( ea.country_code ) ' . $strCountrySortOrder . ', COALESCE( em.preferred_name, em.name_first || \' \' || em.name_last ), lower( us.username )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	// Please be careful before using this sql. This sql leads to high memory usage due to fetchUsers().

	public static function fetchEnabledUsers( $objDatabase, $strCountryCode = NULL, $intUserId = NULL ) {

		$strCountrySortOrder = ( 'US' == $strCountryCode ) ? 'DESC' : 'ASC';

		$strUserIdCondition = ( false == is_null( $intUserId ) ) ? ' OR us.id = ' . ( int ) $intUserId : '';

		$strSql = 'SELECT
						us.*,
						em.name_first,
						em.name_last,
						em.preferred_name,
						em.name_full
					FROM
						users us
						LEFT OUTER JOIN employees em ON (us.employee_id = em.id)
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						( is_disabled <> 1 ' . $strUserIdCondition . ')
						AND us.employee_id = em.id
						AND em.id NOT IN ( 1, 2 )
					ORDER BY
						lower( ea.country_code ) ' . $strCountrySortOrder . ',
						em.name_first,
						em.preferred_name,
						lower(us.username)';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchAllEnabledUsers( $objDatabase, $strCountryCode = NULL, $intUserId = NULL ) {

		$strCountrySortOrder = ( 'US' == $strCountryCode ) ? 'DESC' : 'ASC';

		$strUserIdCondition = ( false == is_null( $intUserId ) ) ? ' OR us.id = ' . ( int ) $intUserId : '';

		$strSql = 'SELECT
						us.id,
						em.preferred_name
					FROM
						users us
						LEFT OUTER JOIN employees em ON (us.employee_id = em.id)
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						( is_disabled <> 1 ' . $strUserIdCondition . ')
						AND us.employee_id = em.id
						AND em.id NOT IN ( 1, 2 )
					ORDER BY
						lower( ea.country_code ) ' . $strCountrySortOrder . ',
						em.name_first,
						em.preferred_name,
						lower(us.username)';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchAllCurrentUsersDetails( $objDatabase, $boolActiveUsersOnly = false, $intUserId = NULL, $strCountryCode = NULL, $boolNeedsDesignationDetails = false ) {

		$strActiveUserCondition	= '';
		$strJoinDesignations	= '';
		$strDesignationSelect	= '';

		if( true == $boolActiveUsersOnly ) {
			$strActiveUserCondition = ' AND ( us.is_disabled <> 1 ';
			$strActiveUserCondition	.= ( true == is_numeric( $intUserId ) ) ? ' OR us.id = ' . ( int ) $intUserId : '';
			$strActiveUserCondition	.= ')';
		}

		if( true == valStr( $strCountryCode ) ) {
			$strActiveUserCondition = 'AND ea.country_code IN( \'' . addslashes( $strCountryCode ) . '\' )';
		}

		if( true == $boolNeedsDesignationDetails ) {
			$strDesignationSelect = ', d.name AS designation_name';
			$strJoinDesignations = ' LEFT JOIN designations d ON ( em.designation_id = d.id ) ';
		}

		$strSql = 'SELECT
						us.id,
						em.id as employee_id,
						em.is_project_manager,
						em.department_id,
						em.name_first,
						em.name_last,
						em.name_full,
						em.preferred_name,
						ea.country_code ' . $strDesignationSelect . '
					FROM
						users us
						JOIN employees em ON (us.employee_id = em.id AND em.date_terminated IS NULL AND em.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						LEFT OUTER JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' ) ' . $strJoinDesignations . '
					WHERE
						em.id NOT IN ( 1, 2 ) ' . $strActiveUserCondition . '
					ORDER BY
						CASE
							WHEN em.preferred_name IS NULL
							THEN lower(em.name_full)
							ELSE lower(em.preferred_name)
						END,
						lower( us.username )';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUsersData( $objDatabase ) {

		$strSql = 'SELECT
						emp.value as value,
						us.id as user_id,
						em.email_address as email_address,
						us.svn_username as svn_username,
						em.name_full
					FROM
						users us
						JOIN employees em ON (us.employee_id = em.id )
						JOIN employee_preferences emp ON ( us.employee_id = emp.employee_id AND emp.key = \'ZENDESK_USER_ID\' )
					WHERE
						em.id NOT IN ( 1, 2 )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUserBySvnUserNameAndDesignationId( $strSvnUserName, $arrintDesignationIds, $objDatabase ) {

		$strSql = 'SELECT
						u.id,
						u.svn_username,
						e.name_full
					FROM
						users u
						JOIN employees e ON e.id = u.employee_id
					WHERE
						u.svn_username =\'' . $strSvnUserName . '\' AND e.designation_id in (' . implode( ',', $arrintDesignationIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUserByUsernameOrEmailAddress( $strUsername, $strEmailAddress = NULL, $objDatabase, $boolForgetPassword = false ) {

		$strWhereCondition = $strJoin = $strSelect = '';

		if( false == is_null( $strEmailAddress ) ) {
			$strWhereCondition = 'OR e.email_address ILIKE \'' . addslashes( $strEmailAddress ) . '\'';
		}

		if( true == $boolForgetPassword ) {
			$strWhereCondition .= ' AND ea.address_type_id = 1';
			$strJoin = ' LEFT JOIN employee_addresses ea ON ea.employee_id = e.id';
			$strSelect = ', ea.country_code';
		}

		$strSql = 'SELECT
						u.id,
						u.username,
						u.active_directory_guid,
						e.id AS employee_id,
						e.name_first,
						e.email_address,
						e.permanent_email_address,
						e.name_full' . $strSelect . '
					FROM
						users u
						JOIN employees e ON e.id = u.employee_id
						' . $strJoin . '
					WHERE
						u.username ILIKE \'' . addslashes( $strUsername ) . '\' ' . $strWhereCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchQaManagerUserByPsProductIdOrPsProductOptionId( $intPsProductId, $intPsProductOptionId, $objDatabase ) {

		$strFromCondition	= 'JOIN ps_products pp ON( u.employee_id = pp.qam_employee_id )';
		$strWhereCondition	= 'pp.id = ' . ( int ) $intPsProductId;

		if( false == is_null( $intPsProductOptionId ) ) {
			$strFromCondition	= 'JOIN ps_product_options ppo ON( u.employee_id = ppo.qam_employee_id )';
			$strWhereCondition	= 'ppo.id = ' . ( int ) $intPsProductOptionId;
		}

		$strSql = 'SELECT
						u.id AS user_id,u.employee_id
 					FROM
						users u
						' . $strFromCondition . '
					WHERE
						' . $strWhereCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllUsersForNpsTasks( $objDatabase ) {

		$strSql = 'SELECT
						u.id,
						em.id as employee_id,
						em.name_first,
						em.name_last,
						em.name_full,
						em.preferred_name
					FROM
						users u
						JOIN employees em ON ( u.employee_id = em.id AND em.date_terminated IS NULL AND em.employee_status_type_id NOT IN( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::SYSTEM . ') )
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						( u.is_disabled <> 1 )
						AND u.id != ' . CUser::ID_DAVID_BATEMAN . '
						AND em.department_id NOT IN ( ' . CDepartment::RESIDENT_SREVICES . ',' . CDepartment::UTILITY_BILLING . ',' . CDepartment::CALL_CENTER . ' )
						AND ea.country_code=\'US\'
					ORDER BY
						em.employee_status_type_id ASC, lower( ea.country_code ) DESC,
						CASE
							WHEN em.preferred_name IS NULL
							THEN lower(em.name_full)
							ELSE lower(em.preferred_name)
						END,
						lower( u.username )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssignedUsersByRoleId( $intRoleId, $objDatabase, $boolIsShowDisabled = false ) {

		$strWhere = ( false == $boolIsShowDisabled ) ? ' AND employee_status_type_id = ' . CEmployeeStatusType::CURRENT : ' ';

		$strSql = 'SELECT
						DISTINCT ON ( u.username, e.name_full ) u.username,
						ur.id AS user_role_id,
						ur.role_id,
						u.id AS user_id,
						e.id,
						e.name_full,
						e.preferred_name,
						d.name,
						o.desk_number
					FROM
						user_roles ur
						JOIN users u ON ( u.id = ur.user_id )
						JOIN employees e ON ( u.employee_id = e.id )
						LEFT JOIN office_desks o ON ( e.office_desk_id = o.id )
						LEFT JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						e.employee_status_type_id != ' . CEmployeeStatusType::NO_SHOW . '
						AND ur.role_id = ' . ( int ) $intRoleId . '
						AND ur.deleted_by IS NULL
						AND u.is_disabled = 0
						' . $strWhere . ' 
					ORDER BY
						e.name_full,
						u.username';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurentUsersByRoleId( $intRoleId, $objDatabase ) {

		$arrintEmployeeStatusTypeIds = array( CEmployeeStatusType::CURRENT, CEmployeeStatusType::SYSTEM );

		$strSql = 'SELECT

						DISTINCT ON (u.id) u.id, u.username,u.password_encrypted, u.is_administrator, ur.role_id, epn.phone_number, emp.id as employee_id, emp.email_address, emp.name_full
					FROM
						users u JOIN user_roles ur ON ( u.id = ur.user_id AND ur.role_id = ' . ( int ) $intRoleId . ' AND deleted_on IS NULL )
						LEFT JOIN employee_phone_numbers epn ON ( u.employee_id = epn.employee_id )
						LEFT JOIN employees emp ON ( u.employee_id = emp.id )
					Where emp.employee_status_type_id IN( ' . implode( ',', $arrintEmployeeStatusTypeIds ) . ' )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByDepartmentId( $intDepartmentId, $objDatabase, $boolPreviousEmployee = true ) {

		if ( false == valId( $intDepartmentId ) ) return NULL;

		$strCondition = '';
		if( false == $boolPreviousEmployee ) {
			$strCondition = ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		$strSql = 'SELECT
						u.id,
						u.employee_id,
						e.name_full,
						u.id as user_id,
						e.preferred_name
					FROM
						users u
						INNER JOIN employees e ON ( u.employee_id = e.id )
					WHERE
						e.department_id = ' . ( int ) $intDepartmentId . $strCondition . '
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomEmployeesByEmployeeIdsByDesignationIds( $arrintEmployeeIds, $arrintDesignationIds, $objDatabase ) {

		if ( false == valArr( $arrintDesignationIds ) ) return NULL;

		$strSubSql = '';

		if( true == valArr( $arrintEmployeeIds ) ) {
			$strSubSql = ' AND u.employee_id in ( ' . sqlIntImplode( $arrintEmployeeIds ) . ' )';
		}

		$strSql = 'SELECT
						u.*,
						e.name_full
					FROM
						users u
						INNER JOIN employees e ON ( u.employee_id = e.id )
					WHERE
						e.designation_id IN ( ' . sqlIntImplode( $arrintDesignationIds ) . ' )
		                AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT
						. $strSubSql . '
					ORDER BY
						e.name_full';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchCurrentUsersData( $objDatabase ) {

		$strSql = 'SELECT
						em.id as employee_id,
						us.id as user_id,
						em.email_address as email_address,
						us.svn_username as svn_username,
						em.name_full
					FROM
						users us
						LEFT JOIN employees em ON (us.employee_id = em.id )
					WHERE
						us.svn_username IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllRelatedUsers( $objDatabase, $strCountryCode = NULL, $strWhereCondition ) {

		$strCountrySortOrder	= ( 'US' == $strCountryCode ) ? 'DESC' : 'ASC';

		$strSql = 'SELECT
					us.id,
					em.designation_id,
					em.department_id,
					em.preferred_name,
					em.name_full
				FROM
					users us
					JOIN employees em ON ( us.employee_id = em.id )
					LEFT JOIN employee_addresses ea ON ( ea.employee_id = em.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
				WHERE
					( us.is_disabled <> 1 )
					' . $strWhereCondition . '
					AND em.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					AND em.date_terminated IS NULL
					AND us.employee_id = em.id
					AND em.id NOT IN ( 1, 2 )
				ORDER BY
					LOWER ( ea.country_code ) ' . $strCountrySortOrder . ',
					em.name_first,
					em.preferred_name,
					LOWER ( us.username )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchUsersPhoneNumberByRoleId( $intRoleId, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT u.employee_id,
						epn.phone_number
					FROM
						user_roles ur
						JOIN users u ON ( u.id = ur.user_id AND ur.role_id = ' . ( int ) $intRoleId . ' )
						JOIN employee_phone_numbers epn ON ( epn.employee_id = u.employee_id )
					WHERE
						epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY;

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUsersWithPhoneNumberByGroupName( $strGroupName, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT u.employee_id,
						epn.phone_number
					FROM
						users u
						JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
						JOIN employees e ON ( e.id = u.employee_id AND ug.is_send_esms = true )
						JOIN groups g ON ( LOWER( g.name ) = LOWER( \'' . $strGroupName . '\' ) AND g.id = ug.group_id AND g.group_type_id = ' . CGroupType::ECS . ' AND g.deleted_by IS NULL )
						JOIN employee_phone_numbers epn ON ( epn.employee_id = u.employee_id )
					WHERE
						epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY;

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUserIdsByDepartmentIdByTeamId( $intDepartmentId, $intTeamId, $objDatabase ) {

		$strSql = 'SELECT
						u.id
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id )
						JOIN employees e ON ( e.id = te.employee_id )
						JOIN users u ON ( u.employee_id = e.id )
					WHERE
						e.department_id = ' . ( int ) $intDepartmentId . '
						AND t.id = ' . ( int ) $intTeamId . '
					ORDER BY
						u.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUserIdByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT id FROM users WHERE employee_id = ' . ( int ) $intEmployeeId;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( false == is_null( $arrintResponse[0]['id'] ) ) return $arrintResponse[0]['id'];
		return 0;
	}

	public static function fetchUsersByOfficeId( $intOfficeId, $objDatabase ) {

		$strWhere .= ( false == is_null( $intOfficeId ) ) ? ' AND e.office_id =' . ( int ) $intOfficeId : ' ';

		$strSql = 'SELECT
						u.*,
						e.office_id,
						e.preferred_name
					FROM
						users u
					JOIN
						employees e ON ( u.employee_id = e.id )
					WHERE
						( is_disabled <> 1 )' . $strWhere . '
					AND
						e.id NOT IN ( 1, 2 )
					ORDER BY
						e.name_first,
						e.preferred_name';

		return parent::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchAdministratorUsers( $objDatabase, $boolFetchData = true ) {

		$strSql = 'SELECT
						u.id,emp.preferred_name
					FROM
						users u
						LEFT JOIN employees emp ON ( u.employee_id = emp.id )
					WHERE
						is_administrator = 1';

		if( false == $boolFetchData ) {
			return self::fetchUsers( $strSql, $objDatabase );
		}
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNonPermissionedUsersByPsModuleId( $objDatabase, $intModuleId, $arrstrFilteredExplodedSearch ) {

		$strSql = 'SELECT
						u.id,
						u.username,
						e.name_full,
						e.id as employee_id
					FROM
						employees e
						LEFT JOIN users u ON e.id = u.employee_id
					WHERE
						u.is_disabled = 0
						AND u.is_administrator = 0
						AND e.employee_status_type_id = 1
						AND ( e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							OR u.username ILIKE \'%' . implode( '%\' AND u.username ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						)
						AND NOT EXISTS (
										SELECT
											1
										FROM
											(
											SELECT
												id
											FROM
												users
											WHERE
												id = u.id
												AND is_administrator = 0
												AND is_disabled = 0
											) AS u_temp
											JOIN
											(
											SELECT
												ps_module_id AS id
											FROM
												ps_modules
											WHERE
												id = ' . ( int ) $intModuleId . '
											) pm ON TRUE
											LEFT JOIN user_groups ug ON ( u_temp.id = ug.user_id AND ug.deleted_by IS NULL )
											LEFT JOIN group_permissions gp ON ( gp.group_id = ug.group_id AND pm.id = gp.ps_module_id )
											LEFT JOIN user_permissions up ON ( up.user_id = u_temp.id AND pm.id = up.ps_module_id )
										GROUP BY
											pm.id,
											u_temp.id
										HAVING
											max ( up.is_allowed ) = 0 OR max ( gp.is_allowed ) = 0
						)
						AND NOT EXISTS (
										SELECT
											1
										FROM
											user_permissions up
										WHERE
											user_id = u.id
											AND ps_module_id = ' . ( int ) $intModuleId . '
											AND is_allowed = 1
						)
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPermissionedUsersByPsModuleId( $intModuleId, $objDatabase ) {

		$strSql = 'SELECT
						u.id,
						u.username,
						e.name_full,
						e.id as employee_id
					FROM
						users u
						JOIN employees e ON ( u.employee_id = e.id )
						JOIN user_permissions up ON ( u.id = up.user_id )
					WHERE
						u.is_disabled = 0
						AND u.is_administrator = 0
						AND up.ps_module_id = ' . ( int ) $intModuleId . '
						AND up.is_allowed = 1
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUsersByDesignationIds( $arrintDesignationIds, $objDatabase, $boolFromReport = false ) {

		if( false == valArr( $arrintDesignationIds ) ) return NULL;
		$strWhereCondition = '';
		if( true == $boolFromReport ) {
			$strWhereCondition = ' AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ', ' . CEmployeeStatusType::NO_SHOW . ' )
									AND e.date_terminated IS NULL';
		}

		$strSql = 'SELECT
						u.id,
						u.active_directory_guid,
						e.designation_id,
						e.department_id,
						e.preferred_name,
						e.email_address,
						u.employee_id
					FROM
						users u
						JOIN employees e ON ( u.employee_id = e.id )
					WHERE
						u.is_disabled = 0
						AND e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' ) ' . $strWhereCondition . '
					ORDER BY
						u.username';
		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchAllUsersWithSvnDetail( $objDatabase ) {

		$strSql = 'SELECT
						u.svn_username,
						e.email_address
					FROM
						employees e
						JOIN users u ON ( u.employee_id = e.id )
					WHERE
						u.svn_username IS NOT NULL
						AND e.email_address IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUsersDetailsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( array_filter( $arrintEmployeeIds ) ) ) return NULL;

		$strSql = 'SELECT
						u.id,
						u.employee_id,
						u.username,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.email_address,
						e.name_full
					FROM
						users u
						JOIN employees e ON ( e.id = u.employee_id )
					WHERE
						u.employee_id IN ( ' . implode( ',', array_filter( $arrintEmployeeIds ) ) . ' )';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUsersByDepartmentIdByEmployeeStatusTypeIdByOfficeIds( $intDepartmentId, $intEmployeeStatusTypeId, $arrintOfficeIds, $objDatabase ) {

		if( false == valArr( $arrintOfficeIds ) ) return NULL;

		$strSql = 'SELECT
						u.id,
						u.employee_id,
						e.name_full
					FROM
						users u
						JOIN employees e ON u.employee_id = e.id
					WHERE
						e.department_id = ' . ( int ) $intDepartmentId . '
						AND e.employee_status_type_id = ' . ( int ) $intEmployeeStatusTypeId . '
						AND e.office_id IN (' . sqlIntImplode( $arrintOfficeIds ) . ')
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveAdminsId( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						users
					WHERE
						is_administrator = 1
						AND is_disabled = 0';

		return parent::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchUsersDetailByDepartmentIdByEmployeeStatusTypeIdByOfficeIdsByDesignationIds( $intDepartmentId, $intEmployeeStatusTypeId, $arrintOfficeIds, $objDatabase, $arrintDesignationIds ) {

		if( false == valArr( $arrintOfficeIds ) ) return NULL;

		$strWhere = ' AND e.department_id = ' . ( int ) $intDepartmentId . ' AND e.office_id IN (' . sqlIntImplode( $arrintOfficeIds ) . ')';

		if( true == valArr( $arrintDesignationIds ) ) {
			$strWhere = ' AND e.designation_id IN (' . sqlIntImplode( $arrintDesignationIds ) . ')' . ' AND e.office_id NOT IN (' . sqlIntImplode( $arrintOfficeIds ) . ')';
		}

		$strSql = 'SELECT
						u.id,
						u.employee_id,
						e.name_full
					FROM
						users u
						JOIN employees e ON u.employee_id = e.id
					WHERE
						e.employee_status_type_id = ' . ( int ) $intEmployeeStatusTypeId . '
						' . $strWhere . '
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHelpDeskUsersByGroupTypeId( $intGroupTypeId = NULL, $objAdminDatabase, $boolUserObject = false, $boolQuickHelpdeskUsers = false, $boolParentGroup = false, $boolExculdeSuspicionGroup = false ) {
		if( false == is_numeric( $intGroupTypeId ) ) return NULL;

		if( false == $boolQuickHelpdeskUsers ) {
			$strSubSelectUnion = ' , g.id AS group_id ';
			$strSelectEdit = ' ( SELECT * FROM';
			$strClosingBracket = ') AS sql1 ORDER BY sql1.group_id,sql1.employee_id )';
			$strWhereSql .= ' JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
						JOIN groups g ON ( g.id = ug.group_id AND g.group_type_id = ' . ( int ) $intGroupTypeId . ' )';
		} else {
			$strWhereSql .= 'WHERE u.username ILIKE \'%helpdesk%\' AND u.id NOT IN ( ' . CUser::ID_DBA_HELPDESK . ', ' . CUser::ID_DBA . ', ' . CUser::ID_IT_HELP_DESK . ', ' . CUser::ID_ACCOUNT_HELP_DESK . ', ' . CUser::ID_HR_HELP_DESK . ', ' . CUser::ID_RECRUITMENT_HELP_DESK . ' ) AND u.is_disabled <> 1 ORDER BY e.preferred_name ) ';
		}

		$strWhereIsParent = '';
		if( true == $boolParentGroup ) {
			$strWhereIsParent .= ' AND g.parent_group_id IS NULL ';
		}

		$strWhereExcludeGroup = '';
		if( true == $boolExculdeSuspicionGroup ) {
			$strWhereExcludeGroup = 'g.id NOT IN(' . CGroup::SUSPICIOUS_ACTIVITY_SUPPORT . ' ) AND ';
		}

		$strSql = $strSelectEdit . '
					( SELECT
						u.id,
						e.preferred_name,
						1 AS is_user,
						e.id as employee_id'
		 				. $strSubSelectUnion . '
					FROM
						users u
						JOIN employees e ON ( e.id = u.employee_id AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ', ' . CEmployeeStatusType::NO_SHOW . ' ) )' . $strWhereSql;

		$strSql .= 'UNION ALL
						( SELECT
							g.id,
							g.name AS preferred_name,
							0 AS is_user,
							0 As employee_id'
							. $strSubSelectUnion . '
						FROM
							groups g
						WHERE ' . $strWhereExcludeGroup . '
							 g.group_type_id = ' . ( int ) $intGroupTypeId . $strWhereIsParent . ' ORDER BY g.name ) ' . $strClosingBracket;
		if( true == $boolUserObject ) {
			return self::fetchUsers( $strSql, $objAdminDatabase );
		} else {
			return fetchData( $strSql, $objAdminDatabase );
		}

	}

	public static function fetchUsersByCountryCodeByDepartmentId( $strCountryCode, $intDepartmentId, $objDatabase ) {

		$strSql = 'SELECT
						u.id,
						e.name_full
					FROM
						users u
						INNER JOIN employees e ON u.employee_id = e.id
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = e.id )
					WHERE
						u.is_disabled != 1
						AND ea.country_code = \'' . $strCountryCode . '\'
						AND e.department_id = ' . ( int ) $intDepartmentId;

		return self::fetchUsers( $strSql, $objDatabase );

	}

	public static function fetchUserIdByQAFileOwnerEmployeeId( $intQAFileOwnerEmployeeId, $objDatabase ) {

		if( true == empty( $intQAFileOwnerEmployeeId ) ) return NULL;

		$strSql = 'SELECT
						id As qa_user_id
					FROM
					    users
					WHERE
						employee_id = ' . ( int ) $intQAFileOwnerEmployeeId;

		$arrintUserIds = fetchData( $strSql, $objDatabase );

		return $arrintUserIds[0]['qa_user_id'];
	}

	public static function fetchUserIdByIdByRoleId( $intUserId, $intRoleId, $objDatabase ) {

		$strSql = 'SELECT
						u.id
					FROM
						users u
						JOIN user_roles ur ON ( u.id = ur.user_id )
					WHERE
						u.id = ' . ( int ) $intUserId . '
						AND ur.role_id = ' . ( int ) $intRoleId . '
						AND ur.deleted_on IS NULL
						AND ur.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentUserIdsByDepartmentIdByOfficeIdsByPsProductId( $intDepartmentId, $arrintOfficeIds, $intPsProductId, $objDatabase, $intUserId = NULL ) {

		if( false == valArr( $arrintOfficeIds ) ) {
			return NULL;
		}

		$strWhere = ( true == valId( $intUserId ) ) ? ' AND u.id = ' . ( int ) $intUserId : '';

		$strSql = 'SELECT
			        distinct(u.id),
					u.employee_id,
					e.name_full
				FROM
				    users u
				    JOIN employees e ON u.employee_id = e.id
				    JOIN team_employees te ON (te.employee_id = e.id)
				    JOIN teams ts ON (ts.id = te.team_id)
				WHERE
				    e.department_id = ' . ( int ) $intDepartmentId . '
				    AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
				    AND e.office_id IN ( ' . sqlIntImplode( $arrintOfficeIds ) . ' )
				    AND te.is_primary_team = 1
				    AND ts.ps_product_id = ' . ( int ) $intPsProductId . $strWhere;

		return rekeyArray( 'id', ( array ) fetchData( $strSql, $objDatabase ) );

	}

	public static function fetchCustomUsersByGroupIdByDepartmentId( $intGroupId, $intDepartmentId, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ( u.id ),
						e.name_full,
						e.id as employee_id
					FROM
						users u
						JOIN employees e ON ( u.employee_id = e.id )
						Join user_groups as ug ON (u.id = ug.user_id)
					WHERE
						u.is_disabled = 0
						AND ( ug.group_id = ' . ( int ) $intGroupId . '
						OR e.department_id = ' . ( int ) $intDepartmentId . ' )
						ORDER BY e.name_full';

		return self::fetchUsers( $strSql, $objDatabase );
	}

	public static function fetchCurrentUserIdsByDepartmentIdsByOfficeIdsByPsProductId( $arrintDepartmentIds, $arrintOfficeIds, $intPsProductId, $objDatabase ) {

		if( false == valArr( $arrintOfficeIds ) || false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
			        distinct(u.id)
				FROM
				    users u
				    JOIN employees e ON u.employee_id = e.id
				    JOIN team_employees te ON (te.employee_id = e.id)
				    JOIN teams ts ON (ts.id = te.team_id)
				WHERE
				    e.department_id IN (' . sqlIntImplode( $arrintDepartmentIds ) . ' )
				    AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
				    AND e.office_id IN ( ' . sqlIntImplode( $arrintOfficeIds ) . ' )
				    AND te.is_primary_team = 1
				    AND ts.ps_product_id = ' . ( int ) $intPsProductId;

		return rekeyArray( 'id', ( array ) fetchData( $strSql, $objDatabase ) );

	}

	/**
	 * @param $intDays
	 * @param $objDatabase
	 * @return array
	 */
	public static function fetchTerminatedUsersWithinDays( $intDays, $objDatabase ) {

		$strSql = ' SELECT
					    u.id,
					    u.username,
					    u.is_disabled,
					    u.active_directory_guid,
					    e.email_address
					FROM
					    users u
					    JOIN employees e ON ( u.employee_id = e.id )
					WHERE
					    e.date_terminated IS NOT NULL
					    AND e.date_terminated >= CURRENT_DATE - INTERVAL \'' . ( int ) $intDays . ' days\' 
					    AND u.active_directory_guid IS NOT NULL;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveUserByIdByPassword( $intId, $strPassword, $objDatabase ) {

	    $strSql = 'SELECT
	                    *
                    FROM
                        users
                    WHERE
                        id = ' . ( int ) $intId . '
                        AND password_encrypted = \'' . ( string ) $strPassword . '\'
                        AND is_disabled <> 1';

	    return self::fetchUser( $strSql, $objDatabase );
    }

	public static function fetchUserByEmailAddress( $arrmixEmails, $objDatabase ) {

		$strSql = 'SELECT
						u.id
					FROM
						users u
						JOIN employees e ON ( e.id = u.employee_id )
					WHERE
						e.email_address IN (\'' . implode( '\',\'', $arrmixEmails ) . '\')';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchAllRVFullPermissionAccessUsersByRoleId( $intRoleId, $objDatabase ) {
		if( false == valId( $intRoleId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						us.employee_id,
						em.name_full,
						em.preferred_name,
						ur.created_on,
						ur.created_by,
						ur.deleted_on,
						ur.deleted_by
					FROM
						user_roles ur
						JOIN users us ON ( us.id = ur.user_id )
						JOIN employees em ON ( us.employee_id = em.id )
					WHERE
						ur.role_id = ' . ( int ) $intRoleId . '
						AND us.is_disabled = 0
						AND em.employee_status_type_id <>' . CEmployeeStatusType::NO_SHOW . '
						AND em.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY
						em.name_full, 
						ur.created_on';

		return fetchData( $strSql, $objDatabase );
	}

}
?>