<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPlannerParticipants
 * Do not add any new functions to this class.
 */

class CPlannerParticipants extends CBasePlannerParticipants {

    public static function fetchPlannerParticipantByPlannerIdByEmployeeId( $intPlannerId, $intEmployeeId, $objDatabase ) {
    	$strSql = 'SELECT
    					*
    			   FROM
    					planner_participants
    			   WHERE
    					planner_id =' . ( int ) $intPlannerId . '
    			   AND
    			   		employee_id =' . ( int ) $intEmployeeId;

    	return self::fetchObject( $strSql, 'CPlannerParticipant', $objDatabase );
    }

    public static function fetchPlannerParticipantByPlannerIdByEmployeeIds( $intPlannerId, $arrintPlannerParticipantsId, $objDatabase ) {

    	if( false == valArr( $arrintPlannerParticipantsId ) ) return 0;

    	$strSql = 'SELECT
    					*
    			   FROM
    					planner_participants
    			   WHERE
    					planner_id =' . ( int ) $intPlannerId . '
    			   AND
    			   		employee_id IN ( ' . implode( ',', $arrintPlannerParticipantsId ) . ' )';

    	return self::fetchObjects( $strSql, 'CPlannerParticipant', $objDatabase );
    }

	public static function fetchActivePlannerParticipantsByPlannerId( $intPlannerId, $objDatabase ) {
    	$strSql = 'SELECT
    					*
    			   FROM
    					planner_participants
    			   WHERE
    					planner_id =' . ( int ) $intPlannerId . '
    			   AND
    			   		leave_meeting = false';

    	return self::fetchObjects( $strSql, 'CPlannerParticipant', $objDatabase );
    }

    public static function fetchActivePlannerParticipantsSumOfStoryPointsByPlannerId( $intPlannerId, $objDatabase ) {
    	$strSql = 'SELECT
    					sum(dev_points) as dev_sum,
    					sum(qa_points) as qa_sum
    			   FROM
    					planner_participants
    			   WHERE
    					planner_id =' . ( int ) $intPlannerId . '
    			   AND
    			   		leave_meeting = false';

    	return fetchData( $strSql, $objDatabase );
    }

	public static function fetchPlannerParticipantsByPlannerIds( $arrintPlannerIds, $objAdminDatabase ) {
		if( false == valArr( $arrintPlannerIds ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						planner_participants
					WHERE
						planner_id IN ( ' . implode( ',', $arrintPlannerIds ) . ')';

		return self::fetchPlannerParticipants( $strSql, $objAdminDatabase );
	}

}
?>