<?php

use Psi\Eos\Admin\CExportBatches;
use Psi\Eos\Admin\CChartOfAccounts;
use Psi\Eos\Admin\CTransactions;

class CExportBatch extends CBaseExportBatch {

	protected $m_fltChargesTotal;
	protected $m_fltPaymentsTotal;
	protected $m_strFileText;

	public function __construct() {
		parent::__construct();

		$this->m_fltChargesTotal 	= 0;
		$this->m_fltPaymentsTotal 	= 0;
		$this->m_strFileText 		= '';

	}

	/**
	 * Get Functions
	 */

	public function getChargesTotal() {
		return $this->m_fltChargesTotal;
	}

	public function getPaymentsTotal() {
		return $this->m_fltPaymentsTotal;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {
		// Last day of previous month
		$this->m_strExportBatchDate = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm' ), 0, date( 'Y' ) ) );
	}

	public function setChargesTotal( $fltChargesTotal ) {
		$this->m_fltChargesTotal = $fltChargesTotal;
	}

	public function setPaymentsTotal( $fltPaymentsTotal ) {
		$this->m_fltPaymentsTotal = $fltPaymentsTotal;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchDeposits( $objDatabase ) {

		return \Psi\Eos\Admin\CDeposits::createService()->fetchDepositsByExportBatchId( $this->m_intId, $objDatabase );
	}

	public function fetchInternalTransfers( $objDatabase ) {

		return CInternalTransfers::fetchInternalTransfersByExportBatchIdGroupedByAccount( $this->m_intId, $objDatabase );
	}

	public function fetchInternalTransfersGroupedByCurrencyCode( $objDatabase ) {
		return CInternalTransfers::fetchInternalTransfersByExportBatchIdGroupedByAccountByCurrencyCode( $this->m_intId, $objDatabase );
	}

	public function fetchAggregatedCharges( $objDatabase ) {

		$strSql = 'SELECT
						charge_code_id,
						sum( transaction_amount ) as sum
					FROM
						transactions t
					JOIN clients c on c.id = t.cid and c.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ' , ' . CCompanyStatusType::TERMINATED . ' )
					WHERE
						export_batch_id = ' . $this->getId() . '
						AND company_payment_id IS NULL
						AND ( deferred_export_batch_id IS NULL OR deferred_export_batch_id = export_batch_id )
					GROUP BY
						charge_code_id';

		$arrfltAggregatedCharges = fetchData( $strSql, $objDatabase );
		$arrfltRekeyedAggregatedCharges = array();

		if( true == valArr( $arrfltAggregatedCharges ) ) {
			foreach( $arrfltAggregatedCharges as $arrfltAggregatedCharge ) {
				$arrfltRekeyedAggregatedCharges[$arrfltAggregatedCharge['charge_code_id']] = $arrfltAggregatedCharge['sum'];
			}
		}

		return $arrfltRekeyedAggregatedCharges;
	}

	public function fetchAggregatedChargesWithGroupByCurrencyCode( $objDatabase ) {

		$strSql = 'SELECT
						charge_code_id,
						t.currency_code,
						sum( transaction_amount ) as sum
					FROM
						transactions t
					JOIN clients c on c.id = t.cid and c.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ' , ' . CCompanyStatusType::TERMINATED . ' )
					WHERE
						export_batch_id = ' . $this->getId() . '
						AND company_payment_id IS NULL
						AND ( deferred_export_batch_id IS NULL OR deferred_export_batch_id = export_batch_id )
					GROUP BY
						charge_code_id,
						t.currency_code';

		$arrfltAggregatedCharges = fetchData( $strSql, $objDatabase );
		$arrfltRekeyedAggregatedCharges = array();

		if( true == valArr( $arrfltAggregatedCharges ) ) {
			foreach( $arrfltAggregatedCharges as $arrfltAggregatedCharge ) {
				$arrfltRekeyedAggregatedCharges[$arrfltAggregatedCharge['currency_code']][$arrfltAggregatedCharge['charge_code_id']] = $arrfltAggregatedCharge['sum'];
			}
		}

		return $arrfltRekeyedAggregatedCharges;
	}

	public function fetchDeferredCharges( $objDatabase ) {
		$strSql = 'SELECT
					sum( transaction_amount ) AS sum
				FROM
					transactions t
				JOIN clients c on c.id = t.cid and c.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ' , ' . CCompanyStatusType::TERMINATED . ' )
				WHERE
					deferred_export_batch_id = ' . $this->getId() . '
					AND company_payment_id IS NULL
					AND ( export_batch_id IS NULL OR deferred_export_batch_id <> export_batch_id )';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchClearedDeferredCharges( $objDatabase ) {
		$strSql = 'SELECT
					charge_code_id,
					sum( transaction_amount ) as sum
				FROM
					transactions t
				JOIN clients c on c.id = t.cid and c.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ' , ' . CCompanyStatusType::TERMINATED . ' )
				WHERE
					export_batch_id = ' . $this->getId() . '
					AND company_payment_id IS NULL
					AND deferred_export_batch_id IS NOT NULL
					AND deferred_export_batch_id <> export_batch_id
				GROUP BY
					charge_code_id';

		$arrfltAggregatedCharges = fetchData( $strSql, $objDatabase );
		$arrfltRekeyedClearedDeferredCharges = array();

		if( true == valArr( $arrfltAggregatedCharges ) ) {
			foreach( $arrfltAggregatedCharges as $arrfltAggregatedCharge ) {
				$arrfltRekeyedClearedDeferredCharges[$arrfltAggregatedCharge['charge_code_id']] = $arrfltAggregatedCharge['sum'];
			}
		}

		return $arrfltRekeyedClearedDeferredCharges;
	}

	public function fetchTransactions( $objDatabase ) {

		return CTransactions::createService()->fetchTransactionsByExportBatchId( $this->m_intId, $objDatabase );
	}

	public function fetchChargesTotal( $objDatabase ) {

		$strSql = 'SELECT sum(transaction_amount)
					FROM
						transactions
					WHERE
						export_batch_id = ' . $this->m_intId . '
						AND company_payment_id IS NULL';

		$arrstrChargesAmount = fetchData( $strSql, $objDatabase );

		$this->m_fltChargesTotal = $arrstrChargesAmount[0]['sum'];

		return $arrstrChargesAmount[0]['sum'];
	}

	public function fetchPaymentsTotal( $objDatabase ) {

		$strSql = ' SELECT sum(deposit_amount)
					FROM (
						SELECT DISTINCT ON (dep.id) dep.*
					FROM
						company_payments cp
					JOIN transactions t ON (cp.id = t.company_payment_id)
					JOIN deposits dep ON (cp.deposit_id = dep.id)
					WHERE
						dep.export_batch_id = ' . $this->m_intId . '
						AND t.company_payment_id IS NOT NULL) AS sub_query1';

		$arrstrPaymentsAmount = fetchData( $strSql, $objDatabase );

		$this->m_fltPaymentsTotal = $arrstrPaymentsAmount[0]['sum'];

		return ( float ) $arrstrPaymentsAmount[0]['sum'];
	}

	public function fetchClients( $objDatabase ) {
		return \Psi\Eos\Admin\CClients::createService()->fetchClientsByExportBatchId( $this->m_intId, $objDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function valExportBatchTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getExportBatchTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'export_batch_type_id', 'Export batch type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valExportBatchDate( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getExportBatchDate() ) || false == CValidation::validateDate( $this->m_strExportBatchDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'export_batch_date', 'A valid batch date is required.' ) );
			return $boolIsValid;
		}

		$objLastExportBatch = CExportBatches::createService()->fetchLastExportBatch( $objDatabase );

		if( false == isset( $objLastExportBatch ) ) {
			return true;
		}

		if( $this->m_strExportBatchDate == $objLastExportBatch ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'export_batch_date', 'Export batch already exists for this date.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPaymentsTotal() {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltPaymentsTotal ) ) {
			$this->m_fltPaymentsTotal = 0;
		}

		if( CExportBatchType::INTERNAL_TRANSFERS == $this->m_intExportBatchTypeId ) {
			if( true == is_null( $this->m_fltPaymentsTotal ) || 0 == $this->m_fltPaymentsTotal ) {
				$boolIsValid = false;
				$strMsgText = 'Internal Transfers must exist before a monthly batch export can be made.';
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strMsgText ) );
			}
		}

		return $boolIsValid;
	}

	public function valChargesTotal() {
		$boolIsValid = true;

		if( CExportBatchType::INTERNAL_TRANSFERS == $this->m_intExportBatchTypeId ) {
			return true;
		}

		if( true == is_null( $this->m_fltChargesTotal ) || 0 == $this->m_fltChargesTotal ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Charges must exist before a monthly batch export can be made.' ) );
		}

		return $boolIsValid;
	}

	public function valUndepositedPayments( $objDatabase ) {
		$boolIsValid = true;

		$intUndepositedPaymentsCount = \Psi\Eos\Admin\CCompanyPayments::createService()->fetchUndepositedPaymentsCountByPaymentDate( $objDatabase );

		if( 0 < $intUndepositedPaymentsCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reconciled_on', 'All payments posted before ' . $this->m_strExportBatchDate . ' must be associated to a deposit before monthly data can be exported.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case 'create':
				$boolIsValid &= $this->valExportBatchDate( $objDatabase );
				$boolIsValid &= $this->valChargesTotal();
				$boolIsValid &= $this->valPaymentsTotal();
				break;

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valExportBatchDate( $objDatabase );
				$boolIsValid &= $this->valExportBatchTypeId();
				$boolIsValid &= $this->valChargesTotal();
				$boolIsValid &= $this->valPaymentsTotal();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function forceDownload( $objDatabase ) {

		$arrobjChargeCodes     = \Psi\Eos\Admin\CChargeCodes::createService()->fetchAllPublishedAndNotPublishedChargeCodes( $objDatabase );
		$arrobjChartOfAccounts = CChartOfAccounts::createService()->fetchAllChartOfAccounts( $objDatabase );
		$arrobjPaymentTypes    = CPaymentTypes::fetchAllPaymentTypes( $objDatabase );
		$arrobjDepositTypes    = \Psi\Eos\Admin\CDepositTypes::createService()->fetchAllDepositTypes( $objDatabase );
		$arrobjDeposits        = $this->fetchDeposits( $objDatabase );

		$arrstrAggregatedCharges      = $this->fetchAggregatedCharges( $objDatabase );
		$arrstrDeferredCharges        = $this->fetchDeferredCharges( $objDatabase );
		$arrstrClearedDeferredCharges = $this->fetchClearedDeferredCharges( $objDatabase );
		$arrmixSalesTaxCharges        = \Psi\Eos\Admin\CSalesTaxTransactions::createService()->fetchSalesTaxTransactionTotalsByExportBatchId( $this->getId(), $objDatabase );

		if( CExportBatchType::MASTER_POLICY == $this->getExportBatchTypeId() ) {
			$arrstrMPCommissionCreditCharges = \Psi\Eos\Admin\CTransactions::createService()->fetchMPCommissionCreditChargesByExportBatchId( $this->getId(), $objDatabase );
			$arrstrMPARCharges               = \Psi\Eos\Admin\CTransactions::createService()->fetchMPARChargesByExportBatchId( $this->getId(), $objDatabase );
		}

		$this->addCompanyHeaders();
		$this->addTransactionHeaders();

		$arrobjPositiveDeposits = array();
		$arrobjNegativeDeposits = array();

		if( true == valArr( $arrobjDeposits ) ) {
			foreach( $arrobjDeposits as $objDeposit ) {
				if( $objDeposit->getDepositAmount() > 0 ) {
					$arrobjPositiveDeposits[$objDeposit->getId()] = $objDeposit;
				} elseif( $objDeposit->getDepositAmount() < 0 ) {
					$arrobjNegativeDeposits[$objDeposit->getId()] = $objDeposit;
				}
			}
		}

		$intChargeCount = $this->addAggregatedChargeTransactions( $arrstrAggregatedCharges, $arrobjChartOfAccounts, $arrobjChargeCodes );

		if( CExportBatchType::MASTER_POLICY == $this->getExportBatchTypeId() ) {
			$intChargeCount = $this->addAggregatedChargeTransactions( $arrstrMPARCharges, $arrobjChartOfAccounts, $arrobjChargeCodes, $intChargeCount );

			if( true == valArr( $arrstrMPCommissionCreditCharges ) ) {
				$arrintChargeCodes = ( array_keys( $arrstrMPCommissionCreditCharges ) );
				if( true == valArr( $arrintChargeCodes ) ) {
					$intChargeCodeId = $arrintChargeCodes[0];
					if( true == valObj( $arrobjChargeCodes[$intChargeCodeId], 'CChargeCode' ) ) {
						$arrobjChargeCodes[$intChargeCodeId]->setName( 'Reversal ' . $arrobjChargeCodes[$intChargeCodeId]->getName() );
					}
				}

				$intChargeCount = $this->addAggregatedChargeTransactions( $arrstrMPCommissionCreditCharges, $arrobjChartOfAccounts, $arrobjChargeCodes, $intChargeCount );
			}
		}

		$intChargeCount = $this->addDeferredTransactions( $arrstrDeferredCharges, $arrobjChartOfAccounts, $intChargeCount );
		$intChargeCount = $this->addClearedDeferredChargeTransactions( $arrstrClearedDeferredCharges, $arrobjChartOfAccounts, $arrobjChargeCodes, $intChargeCount );

		if( false == in_array( $this->getExportBatchTypeId(), CExportBatchType::$c_arrintBatchTypeInsurance ) ) {
			$this->addSalesTaxCharges( $intChargeCount, $arrmixSalesTaxCharges, $arrobjChartOfAccounts );
		}
		$this->addPositiveDepositTransactions( $arrobjPositiveDeposits, $arrobjChartOfAccounts, $arrobjChargeCodes, $arrobjDepositTypes );
		$this->addNegativeDepositTransactions( $arrobjNegativeDeposits, $arrobjChartOfAccounts, $arrobjChargeCodes, $arrobjDepositTypes );
		$this->addDifferredRevenue( $arrobjChartOfAccounts );

		// If this is the Resident Insure Export Batch, enter manual adjusting entries.

		switch( $this->getExportBatchTypeId() ) {
			case CExportBatchType::PROPERTY_SOLUTIONS:
				break;

			case CExportBatchType::RESIDENT_INSURE_MARKEL:
			case CExportBatchType::RESIDENT_INSURE_KEMPER:
			case CExportBatchType::RESIDENT_INSURE_QBE:
			case CExportBatchType::RESIDENT_INSURE_IDTHEFT:
				// Removing this per Accounting, these should not be included in the export.
				// $this->addResidentInsureAdjustments( $arrobjDeposits, $arrstrAggregatedCharges, $arrobjChartOfAccounts, $objDatabase );
				break;

			case CExportBatchType::MASTER_POLICY:
				break;

			default:
				trigger_error( 'Unexpected export batch type.', E_USER_ERROR );
				break;
		}

		// Force download of file to iif format (iif is Quickbooks file import / export format)
		$strFilename = date( 'Y_m_d', strtotime( $this->getExportBatchDate() ) ) . '_export_batch_no_' . $this->getId() . '.iif';

		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: private', false );
		header( 'Content-type: application/vnd.ms-excel' );
		header( 'Content-Disposition: attachment; filename="' . $strFilename . '"' );
		header( 'Content-length: ' . \Psi\CStringService::singleton()->strlen( $this->m_strFileText ) );
		header( 'Content-Transfer-Encoding: binary' );

		print $this->m_strFileText;
		exit;
	}

	public function forceDownloadInternalTransfers( $objDatabase ) {

		$arrobjChartOfAccounts		= rekeyObjects( 'ProcessingBankAccountId', ( array ) CChartOfAccounts::createService()->fetchChartOfAccountsByChartOfAccountTypes( array( CChartOfAccountType::ACCOUNT_TYPE_BANK, CChartOfAccountType::ACCOUNT_TYPE_CURRENT_ASSET ), $objDatabase, $boolRequiredProcessingBankId = true ) );
		$arrmixInternalTransfers 	= ( array ) $this->fetchInternalTransfers( $objDatabase );

		$this->addCompanyHeaders();
		$this->addTransactionHeaders();

		$this->addInternalTransfers( $arrmixInternalTransfers, $arrobjChartOfAccounts );

		// Force download of file to iif format (iif is Quickbooks file import / export format)
		$strFilename = date( 'Y_m_d', strtotime( $this->getExportBatchDate() ) ) . '_export_batch_no_' . $this->getId() . '.iif';

		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: private', false );
		header( 'Content-type: application/vnd.ms-excel' );
		header( 'Content-Disposition: attachment; filename="' . $strFilename . '"' );
		header( 'Content-length: ' . \Psi\CStringService::singleton()->strlen( $this->m_strFileText ) );
		header( 'Content-Transfer-Encoding: binary' );

		print $this->m_strFileText;
		exit;
	}

	public function addTransactionHeaders() {

		$this->m_strFileText .= "!TRNS\tTRNSID\tTRNSTYPE\tDATE\tNAME\tACCNT\tCLASS\tAMOUNT\tDOCNUM\tMEMO\n";
		$this->m_strFileText .= "!SPL\tSPLID\tTRNSTYPE\tDATE\tNAME\tACCNT\tCLASS\tAMOUNT\tDOCNUM\tMEMO\n";
		$this->m_strFileText .= "!ENDTRNS\n";
	}

	public function addCompanyHeaders() {

		switch( $this->getExportBatchTypeId() ) {
			case CExportBatchType::PROPERTY_SOLUTIONS:
			case CExportBatchType::INTERNAL_TRANSFERS:
				$this->m_strFileText .= "!CUST\tNAME\tBADDR1\tBADDR2\tBADDR3\tBADDR4\tFIRSTNAME\tMIDINIT\tLASTNAME\tPHONE1\tPHONE2\tCUSTFLD1\n";
				$this->m_strFileText .= "CUST\tCLIENTADMIN\tProperty Solutions\t1656 S East Bay Blvd Ste 200\tPROVO UT 84606\t\t\t\t\t801-375-5522\t801-705-1835\t" . CSystemEmail::INFO_EMAIL_ADDRESS . "\n";
				break;

			case CExportBatchType::RESIDENT_INSURE_MARKEL:
			case CExportBatchType::RESIDENT_INSURE_KEMPER:
			case CExportBatchType::RESIDENT_INSURE_QBE:
			case CExportBatchType::RESIDENT_INSURE_IDTHEFT:
			case CExportBatchType::MASTER_POLICY:
				$this->m_strFileText .= "!CUST\tNAME\tBADDR1\tBADDR2\tBADDR3\tBADDR4\tFIRSTNAME\tMIDINIT\tLASTNAME\tPHONE1\tPHONE2\tCUSTFLD1\n";
				$this->m_strFileText .= "CUST\tCLIENTADMIN\tPSIA\t1656 S East Bay Blvd Ste 200\tPROVO UT 84606\t\t\t\t\t801-375-5522\t801-705-1835\tinfo@residentinsure.com\n";
				break;

			default:
				trigger_error( 'Unexpected export batch type.', E_USER_ERROR );
				break;
		}
	}

	public function addAggregatedChargeTransactions( $arrstrAggregatedCharges, $arrobjChartOfAccounts, $arrobjChargeCodes ) {

		$intChargeCount = 0;

		if( true == valArr( $arrstrAggregatedCharges ) ) {
			foreach( $arrstrAggregatedCharges as $intChargeCodeId => $fltChargeTotal ) {

				$intChargeCount++;

				$objCurrentChargeCode 				= $arrobjChargeCodes[$intChargeCodeId];
				$objCurrentDebitChartOfAccount		= $arrobjChartOfAccounts[$objCurrentChargeCode->getDebitChartOfAccountId()];

				if( CExportBatchType::RESIDENT_INSURE_KEMPER == $this->getExportBatchTypeId() && CChargeCode::RI_RETURN_ADJUSTMENT == $intChargeCodeId ) {
					$objCurrentCreditChartOfAccount		= $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_KEMPER];
				} elseif( CExportBatchType::RESIDENT_INSURE_QBE == $this->getExportBatchTypeId() && CChargeCode::RI_RETURN_ADJUSTMENT == $intChargeCodeId ) {
					$objCurrentCreditChartOfAccount		= $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_QBE];
				} elseif( CExportBatchType::RESIDENT_INSURE_IDTHEFT == $this->getExportBatchTypeId() && CChargeCode::RI_RETURN_ADJUSTMENT == $intChargeCodeId ) {
					$objCurrentCreditChartOfAccount		= $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_IDTHEFT];
					$objCurrentDebitChartOfAccount      = $arrobjChartOfAccounts[CChartOfAccount::PREPAID_ID_THEFT];
				} else {
					$objCurrentCreditChartOfAccount		= $arrobjChartOfAccounts[$objCurrentChargeCode->getCreditChartOfAccountId()];
				}

				if( ( CChargeCode::SALES_TAX == $intChargeCodeId ) || ( CChargeCode::SALES_TAX_ADJUSTMENT == $intChargeCodeId ) ) {

					if( true == in_array( $this->getExportBatchTypeId(), CExportBatchType::$c_arrintBatchTypeInsurance ) ) {
						$objCurrentCreditChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::SALES_TAX_RI];
					} else {
						// Don't add sales tax, it will be added in function addSalesTaxCharges.
						continue;
					}
				}

				if( false == valObj( $objCurrentDebitChartOfAccount, 'CChartOfAccount' ) || false == valObj( $objCurrentCreditChartOfAccount, 'CChartOfAccount' ) ) {
					trigger_error( 'Aggregated charge failed to load. Process aborted.', E_USER_ERROR );
					exit;
				}

				$this->m_strFileText .= "TRNS\t" . $this->getId() . CStrings::strNachaDef( $intChargeCount, 4, true, true, '0' ) . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $this->getExportBatchDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentDebitChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass( $objCurrentCreditChartOfAccount ) . "\t" . ( float ) $fltChargeTotal . "\t\tAggregated Charge - " . \Psi\CStringService::singleton()->substr( $objCurrentChargeCode->getName(), 0, 20 ) . "\n";
				$this->m_strFileText .= "SPL\t" . $this->getId() . CStrings::strNachaDef( $intChargeCount, 4, true, true, '0' ) . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $this->getExportBatchDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentCreditChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass( $objCurrentCreditChartOfAccount ) . "\t" . ( float ) ( -1 * $fltChargeTotal ) . "\t\tAggregated Charge - " . \Psi\CStringService::singleton()->substr( $objCurrentChargeCode->getName(), 0, 20 ) . "\n";
				$this->m_strFileText .= "ENDTRNS\n";
			}
		}

		return $intChargeCount;
	}

	public function addDeferredTransactions( $arrstrAggregatedCharges, $arrobjChartOfAccounts, $intChargeCount ) {

		if( CExportBatchType::PROPERTY_SOLUTIONS != $this->getExportBatchTypeId() ) {
			return $intChargeCount;
		}

		if( true == valArr( $arrstrAggregatedCharges ) && true == array_key_exists( 'sum', $arrstrAggregatedCharges[0] ) ) {
			$fltChargeTotal = round( $arrstrAggregatedCharges[0]['sum'], 2 );
			$intChargeCount++;

			$objCurrentDebitChartOfAccount		= $arrobjChartOfAccounts[CChartOfAccount::ACCOUNTS_RECEIVABLE];
			$objCurrentCreditChartOfAccount 	= $arrobjChartOfAccounts[CChartOfAccount::DEFERRED_REVENUE];

			if( false == valObj( $objCurrentDebitChartOfAccount, 'CChartOfAccount' ) || false == valObj( $objCurrentCreditChartOfAccount, 'CChartOfAccount' ) ) {
				trigger_error( 'Deferred charge failed to load. Process aborted.', E_USER_ERROR );
				exit;
			}

			$this->m_strFileText .= "TRNS\t" . $this->getId() . CStrings::strNachaDef( $intChargeCount, 4, true, true, '0' ) . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $this->getExportBatchDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentDebitChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass( $objCurrentCreditChartOfAccount ) . "\t" . ( float ) $fltChargeTotal . "\t\tDeferred Revenue\n";
			$this->m_strFileText .= "SPL\t" . $this->getId() . CStrings::strNachaDef( $intChargeCount, 4, true, true, '0' ) . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $this->getExportBatchDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentCreditChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass( $objCurrentCreditChartOfAccount ) . "\t" . ( float ) ( -1 * $fltChargeTotal ) . "\t\tDeferred Revenue\n";
			$this->m_strFileText .= "ENDTRNS\n";

		}

		return $intChargeCount;
	}

	public function addClearedDeferredChargeTransactions( $arrstrAggregatedCharges, $arrobjChartOfAccounts, $arrobjChargeCodes, $intChargeCount ) {

		if( CExportBatchType::PROPERTY_SOLUTIONS != $this->getExportBatchTypeId() ) {
			return $intChargeCount;
		}

		if( true == valArr( $arrstrAggregatedCharges ) ) {
			foreach( $arrstrAggregatedCharges as $intChargeCodeId => $fltChargeTotal ) {

				$intChargeCount++;

				$objCurrentChargeCode 				= $arrobjChargeCodes[$intChargeCodeId];
				$objCurrentDebitChartOfAccount		= $arrobjChartOfAccounts[CChartOfAccount::DEFERRED_REVENUE];

				$objCurrentCreditChartOfAccount		= $arrobjChartOfAccounts[$objCurrentChargeCode->getCreditChartOfAccountId()];

				if( false == valObj( $objCurrentDebitChartOfAccount, 'CChartOfAccount' ) || false == valObj( $objCurrentCreditChartOfAccount, 'CChartOfAccount' ) ) {
					trigger_error( 'Cleared deferred charge failed to load. Process aborted.', E_USER_ERROR );
					exit;
				}

				$this->m_strFileText .= "TRNS\t" . $this->getId() . CStrings::strNachaDef( $intChargeCount, 4, true, true, '0' ) . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $this->getExportBatchDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentDebitChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass( $objCurrentCreditChartOfAccount ) . "\t" . ( float ) $fltChargeTotal . "\t\tAggregated Charge - " . \Psi\CStringService::singleton()->substr( $objCurrentChargeCode->getName(), 0, 20 ) . "\n";
				$this->m_strFileText .= "SPL\t" . $this->getId() . CStrings::strNachaDef( $intChargeCount, 4, true, true, '0' ) . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $this->getExportBatchDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentCreditChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass( $objCurrentCreditChartOfAccount ) . "\t" . ( float ) ( -1 * $fltChargeTotal ) . "\t\tAggregated Charge - " . \Psi\CStringService::singleton()->substr( $objCurrentChargeCode->getName(), 0, 20 ) . "\n";
				$this->m_strFileText .= "ENDTRNS\n";
			}
		}

		return $intChargeCount;
	}

	public function addSalesTaxCharges( $intChargeCount, $arrmixSalesTaxCharges, $arrobjChartOfAccounts ) {

		if( true == valArr( $arrmixSalesTaxCharges ) ) {
			foreach( $arrmixSalesTaxCharges as $arrmixSalesTaxCharge ) {

				$intChargeCount++;

				$objCurrentDebitChartOfAccount		= $arrobjChartOfAccounts[$arrmixSalesTaxCharge['debit_chart_of_account_id']];
				$objCurrentCreditChartOfAccount		= $arrobjChartOfAccounts[$arrmixSalesTaxCharge['credit_chart_of_account_id']];
				$fltChargeTotal						= $arrmixSalesTaxCharge['total_tax'];

				if( false == valObj( $objCurrentDebitChartOfAccount, 'CChartOfAccount' ) || false == valObj( $objCurrentCreditChartOfAccount, 'CChartOfAccount' ) ) {
					trigger_error( 'Aggregated charge failed to load. Process aborted.', E_USER_ERROR );
					exit;
				}

				$this->m_strFileText .= "TRNS\t" . $this->getId() . CStrings::strNachaDef( $intChargeCount, 4, true, true, '0' ) . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $this->getExportBatchDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentDebitChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass( $objCurrentCreditChartOfAccount ) . "\t" . ( float ) $fltChargeTotal . "\t\tAggregated Charge - " . \Psi\CStringService::singleton()->substr( $objCurrentCreditChartOfAccount->getDescription(), 0, 20 ) . "\n";
				$this->m_strFileText .= "SPL\t" . $this->getId() . CStrings::strNachaDef( $intChargeCount, 4, true, true, '0' ) . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $this->getExportBatchDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentCreditChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass( $objCurrentCreditChartOfAccount ) . "\t" . ( float ) ( -1 * $fltChargeTotal ) . "\t\tAggregated Charge - " . \Psi\CStringService::singleton()->substr( $objCurrentCreditChartOfAccount->getDescription(), 0, 20 ) . "\n";
				$this->m_strFileText .= "ENDTRNS\n";
			}
		}
	}

	public function addPositiveDepositTransactions( $arrobjPositiveDeposits, $arrobjChartOfAccounts, $arrobjChargeCodes, $arrobjDepositTypes ) {

		$intChargeCount = 0;

		if( true == valArr( $arrobjPositiveDeposits ) ) {
			foreach( $arrobjPositiveDeposits as $objDeposit ) {

				$intChargeCount++;

				$objCurrentChargeCode 				= $arrobjChargeCodes[CChargeCode::PAYMENT_RECEIVED];

				// For Renters Insurance we need to change the debit account to Bank, instead of using undeposited funds
				$objCurrentDebitChartOfAccount		= $arrobjChartOfAccounts[$objCurrentChargeCode->getDebitChartOfAccountId()];
				$objCurrentCreditChartOfAccount		= $arrobjChartOfAccounts[$objCurrentChargeCode->getCreditChartOfAccountId()];
				$objPsiaOperatingBankAccountMarkel	= $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_MARKEL];
				$objPsiaOperatingBankAccountKemper	= $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_KEMPER];
				$objPsiaOperatingBankAccountQbe		= $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_QBE];
				$objPsiaOperatingBankAccountIdTheft	= $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_IDTHEFT];
				$objPsiaOperatingBankAccountMasterPolicy	= $arrobjChartOfAccounts[CChartOfAccount::MASTER_POLICY_CASH];
				$objDepositType	 					= $arrobjDepositTypes[$objDeposit->getDepositTypeId()];
				$objDepositChartOfAccount			= $arrobjChartOfAccounts[$objDeposit->getChartOfAccountId()];

				if( false == valObj( $objCurrentDebitChartOfAccount, 'CChartOfAccount' )
						|| false == valObj( $objCurrentCreditChartOfAccount, 'CChartOfAccount' )
						|| false == valObj( $objDepositType, 'CDepositType' ) ) {

					trigger_error( 'Positive payment failed to load. Process aborted.', E_USER_ERROR );
					exit;
				}

				switch( $this->getExportBatchTypeId() ) {
					case CExportBatchType::PROPERTY_SOLUTIONS:
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tPAYMENT\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentDebitChartOfAccount->getSecondaryNumber() . "\t\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tPAYMENT\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentCreditChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
						break;

					case CExportBatchType::RESIDENT_INSURE_MARKEL:
						$objPrepaidMarkelRiPremiumChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::PREPAID_MARKEL_RI_PREMIUM];
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPsiaOperatingBankAccountMarkel->getSecondaryNumber() . "\tMarkel\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPrepaidMarkelRiPremiumChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
						break;

					case CExportBatchType::RESIDENT_INSURE_KEMPER:
						$objPrepaidMarkelRiPremiumChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::PREPAID_MARKEL_RI_PREMIUM];
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPsiaOperatingBankAccountKemper->getSecondaryNumber() . "\tKemper\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPrepaidMarkelRiPremiumChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
						break;

					case CExportBatchType::RESIDENT_INSURE_QBE:
						$objPrepaidMarkelRiPremiumChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::PREPAID_MARKEL_RI_PREMIUM];
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPsiaOperatingBankAccountQbe->getSecondaryNumber() . "\tQBE\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPrepaidMarkelRiPremiumChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
						break;

					case CExportBatchType::RESIDENT_INSURE_IDTHEFT:
					    $objIdTheftChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::PREPAID_ID_THEFT];
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPsiaOperatingBankAccountIdTheft->getSecondaryNumber() . "\tIdTheft\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objIdTheftChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
					    break;

					case CExportBatchType::MASTER_POLICY:
						$objMasterPolicyChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::MASTER_POLICY];
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPsiaOperatingBankAccountMasterPolicy->getSecondaryNumber() . "\tMasterPolicy\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objMasterPolicyChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
						break;

					default:
						trigger_error( 'Unexpected export batch type.', E_USER_ERROR );
						break;
				}
			}
		}
	}

	public function addInternalTransfers( $arrmixInternalTransfers, $arrobjChartOfAccounts ) {

		$intCount = 0;

		foreach( $arrmixInternalTransfers as $arrmixInternalTransfer ) {

			$intCount++;
			$objDebitChartOfAccount = $arrobjChartOfAccounts[$arrmixInternalTransfer['debit_processing_bank_account_id']];
			$objCreditChartOfAccount = $arrobjChartOfAccounts[$arrmixInternalTransfer['credit_processing_bank_account_id']];

			if( false == valObj( $objDebitChartOfAccount, 'CChartOfAccount' ) || false == valObj( $objCreditChartOfAccount, 'CChartOfAccount' ) ) {
				trigger_error( 'Internal Transfer failed to load. Process aborted.', E_USER_ERROR );
				exit;
			}

			$this->m_strFileText .= "TRNS\t" . '1' . CStrings::strNachaDef( $intCount, 4, true, true, '0' ) . "\tGENERAL JOURNAL\t" . date( 'm/d/Y', strtotime( $arrmixInternalTransfer['transfer_date'] ) ) . "\tCLIENTADMIN\t" . $objDebitChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * ( $arrmixInternalTransfer['daily_total'] / 100 ) ) . "\t" . $objDebitChartOfAccount->getAccountName() . "\n";
			$this->m_strFileText .= "SPL\t" . '2' . CStrings::strNachaDef( $intCount, 4, true, true, '0' ) . "\tGENERAL JOURNAL\t" . date( 'm/d/Y', strtotime( $arrmixInternalTransfer['transfer_date'] ) ) . "\tCLIENTADMIN\t" . $objCreditChartOfAccount->getSecondaryNumber() . "\t\t" . ( $arrmixInternalTransfer['daily_total'] / 100 ) . "\t" . $objCreditChartOfAccount->getAccountName() . "\n";
			$this->m_strFileText .= "ENDTRNS\n";
		}
	}

	public function addNegativeDepositTransactions( $arrobjNegativeDeposits, $arrobjChartOfAccounts, $arrobjChargeCodes, $arrobjDepositTypes ) {

		$intChargeCount = 0;

		if( true == valArr( $arrobjNegativeDeposits ) ) {
			foreach( $arrobjNegativeDeposits as $objDeposit ) {

				$intChargeCount++;

				$objCurrentChargeCode 				= $arrobjChargeCodes[CChargeCode::PAYMENT_RECEIVED];
				$objCurrentDebitChartOfAccount		= $arrobjChartOfAccounts[$objCurrentChargeCode->getDebitChartOfAccountId()];
				$objCurrentCreditChartOfAccount		= $arrobjChartOfAccounts[$objCurrentChargeCode->getCreditChartOfAccountId()];
				$objPsiaOperatingBankAccountMarkel	= $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_MARKEL];
				$objPsiaOperatingBankAccountKemper	= $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_KEMPER];
				$objPsiaOperatingBankAccountQbe		= $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_QBE];
				$objPsiaOperatingBankAccountIdTheft = $arrobjChartOfAccounts[CChartOfAccount::PSIA_OPERATING_IDTHEFT];
				$objPsiaOperatingBankAccountMasterPolicy	= $arrobjChartOfAccounts[CChartOfAccount::MASTER_POLICY_CASH];
				$objDepositType	 					= $arrobjDepositTypes[$objDeposit->getDepositTypeId()];
				$objDepositChartOfAccount			= $arrobjChartOfAccounts[$objDeposit->getChartOfAccountId()];

				if( false == valObj( $objCurrentDebitChartOfAccount, 'CChartOfAccount' )
						|| false == valObj( $objCurrentCreditChartOfAccount, 'CChartOfAccount' )
						|| false == valObj( $objDepositType, 'CDepositType' ) ) {

					trigger_error( 'Negative payment failed to load. Process aborted.', E_USER_ERROR );
					exit;
				}

				switch( $this->getExportBatchTypeId() ) {
					case CExportBatchType::PROPERTY_SOLUTIONS:
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentDebitChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass() . "\t" . $objDeposit->getDepositAmount() . "\tDeposit " . $objDeposit->getId() . ' - ' . $objDepositType->getName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objCurrentCreditChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass() . "\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\tDeposit " . $objDeposit->getId() . ' - ' . $objDepositType->getName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
						break;

					case CExportBatchType::RESIDENT_INSURE_MARKEL:
						$objPrepaidMarkelRiPremiumChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::PREPAID_MARKEL_RI_PREMIUM];
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPsiaOperatingBankAccountMarkel->getSecondaryNumber() . "\tMarkel\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPrepaidMarkelRiPremiumChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
						break;

					case CExportBatchType::RESIDENT_INSURE_KEMPER:
						$objPrepaidMarkelRiPremiumChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::PREPAID_MARKEL_RI_PREMIUM];
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPsiaOperatingBankAccountKemper->getSecondaryNumber() . "\tKemper\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPrepaidMarkelRiPremiumChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
						break;

					case CExportBatchType::RESIDENT_INSURE_QBE:
						$objPrepaidMarkelRiPremiumChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::PREPAID_MARKEL_RI_PREMIUM];
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPsiaOperatingBankAccountQbe->getSecondaryNumber() . "\tQBE\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPrepaidMarkelRiPremiumChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
						break;

					case CExportBatchType::RESIDENT_INSURE_IDTHEFT:
					    $objIdTheftChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::PREPAID_ID_THEFT];
					    $this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPsiaOperatingBankAccountIdTheft->getSecondaryNumber() . "\tIdTheft\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
					    $this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objIdTheftChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
					    $this->m_strFileText .= "ENDTRNS\n";
					    break;

					case CExportBatchType::MASTER_POLICY:
						$objMasterPolicyChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::MASTER_POLICY];
						$this->m_strFileText .= "TRNS\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objPsiaOperatingBankAccountMasterPolicy->getSecondaryNumber() . "\tMasterPolicy\t" . $objDeposit->getDepositAmount() . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "SPL\t" . $objDeposit->getId() . "\tCASH REFUND\t" . \Psi\CStringService::singleton()->substr( $objDeposit->getDepositDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objMasterPolicyChartOfAccount->getSecondaryNumber() . "\t\t" . ( -1 * $objDeposit->getDepositAmount() ) . "\t" . $objDeposit->getId() . '-' . $objDepositChartOfAccount->getAccountName() . "\t" . $objDeposit->getDepositMemo() . "\n";
						$this->m_strFileText .= "ENDTRNS\n";
						break;

					default:
						trigger_error( 'Unexpected export batch type.', E_USER_ERROR );
						break;
				}
			}
		}
	}

	// Resident Insure is a separate company with it's own set of books.
	// When we integrate with Resident Insure QuickBooks, we have to do a few adjusting entries
	// to properly account for the liabilities associated with prepayment of insurance premiums. This is
	// a bit tricky but I'll try to leave good notes on how / why this is being done.

	public function addResidentInsureAdjustments( $arrstrAggregatedCharges, $arrobjChartOfAccounts ) {

		$objMarkelRiPremiumChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::MARKEL_RI_PREMIUM_LIABILITY];
		$objMarkelRiIncomeChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::MARKEL_RI_INCOME];

		if( false == valObj( $objMarkelRiPremiumChartOfAccount, 'CChartOfAccount' )
				|| false == valObj( $objMarkelRiIncomeChartOfAccount, 'CChartOfAccount' ) ) {

			trigger_error( 'Chart of accounts failed to load.', E_USER_ERROR );
			exit;
		}

		// Calculate the total amount due to markel by multiplying the current premium charges by .25
		$fltTotalAmountDueToMarkel = ( true == isset ( $arrstrAggregatedCharges[CChargeCode::MARKEL_RI_PREMIUMS] ) ) ? ( float ) round( ( $arrstrAggregatedCharges[CChargeCode::MARKEL_RI_PREMIUMS] * .25 ), 2 ) : 0;

		// Create a journal entry to reflect what we currently owe to markel
		if( 0 != $fltTotalAmountDueToMarkel ) {
			$this->m_strFileText .= "TRNS\t" . $this->getId() . "002\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $this->getExportBatchDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objMarkelRiIncomeChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass() . "\t" . ( float ) $fltTotalAmountDueToMarkel . "\t\tMove money owed to markel to markel payables\n";
			$this->m_strFileText .= "SPL\t" . $this->getId() . "002\tGENERAL JOURNAL\t" . \Psi\CStringService::singleton()->substr( $this->getExportBatchDate(), 0, 10 ) . "\tCLIENTADMIN\t" . $objMarkelRiPremiumChartOfAccount->getSecondaryNumber() . "\t" . $this->loadClass() . "\t" . ( float ) ( -1 * $fltTotalAmountDueToMarkel ) . "\t\tMove money owed to markel to markel payables\n";
			$this->m_strFileText .= "ENDTRNS\n";
		}
	}

	public function addDifferredRevenue( $arrobjChartOfAccounts ) {

		switch( $this->getExportBatchTypeId() ) {
			case CExportBatchType::PROPERTY_SOLUTIONS:
				$objDeferredRevenueChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::DEFERRED_REVENUE];
				$objAccountsReceivableChartOfAccount = $arrobjChartOfAccounts[CChartOfAccount::ACCOUNTS_RECEIVABLE];

				// Get this list of all charges that are future dated and invoiced, we need to defer that revenue.

				// We need the list of all aggregate charges that are now coming out of deferred revenue.

				// Note: we will only want to deferr revenue for charge codes that have a debit chart of accounts of Accounts Receiveable.
				break;

			case CExportBatchType::RESIDENT_INSURE_MARKEL:
			case CExportBatchType::RESIDENT_INSURE_KEMPER:
			case CExportBatchType::RESIDENT_INSURE_QBE:
			case CExportBatchType::RESIDENT_INSURE_IDTHEFT:
			case CExportBatchType::MASTER_POLICY:
				break;

			default:
				trigger_error( 'Unexpected export batch type.', E_USER_ERROR );
				break;
		}
	}

	public function fetchExportBatchTotalChargesAndPaymentDetails( $objDatabase ) {
		return CTransactions::createService()->fetchTransactionExportBatchDetailsByExportBatchId( $this->getId(), $objDatabase );
	}

	public function loadClass( $objCurrentCreditChartOfAccount = NULL ) {

		$strClass = '';

		switch( $this->getExportBatchTypeId() ) {

			case CExportBatchType::RESIDENT_INSURE_MARKEL:
				$strClass = 'Markel';
				break;

			case CExportBatchType::RESIDENT_INSURE_KEMPER:
				$strClass = 'Kemper';
				break;

			case CExportBatchType::RESIDENT_INSURE_QBE:
				$strClass = 'QBE';
				break;

			default:
				$strClass = ( true == valObj( $objCurrentCreditChartOfAccount, 'CChartOfAccount' ) ) ? trim( $objCurrentCreditChartOfAccount->getClass() ) : '';
				break;
		}

		return $strClass;
	}

	public function fetchStateSalesTaxes( $objAdminDatabase ) {
		return CExportBatches::createService()->fetchStateSalesTaxesByExportBatchId( $this->getId(), $objAdminDatabase );
	}

	public function fetchStateSalesTaxAdjustments( $objAdminDatabase ) {
		return CExportBatches::createService()->fetchStateSalesTaxAdjustmentsByExportBatchId( $this->getId(), $objAdminDatabase );
	}

	public function updateExportBatchIdIntoDeposits( $arrobjDeposits, $objAdminDatabase ) {

		if( false == valArr( $arrobjDeposits ) ) {
			return true;
		}// It is possible to have no deposits for a month so return true.

		$strSql = 'UPDATE deposits
						SET
							export_batch_id = ' . $this->getId() . '
						WHERE
							id IN ( ' . implode( ',', array_keys( $arrobjDeposits ) ) . ' )';

		return $objAdminDatabase->execute( $strSql );
	}

	public function updateExportBatchIdIntoTransactions( $objAdminDatabase ) {
		$strSql = 'UPDATE transactions
						SET
							export_batch_id = ' . $this->getId() . '
						FROM
							company_payments,
							deposits
						WHERE
							transactions.company_payment_id = company_payments.id
							AND deposits.id = company_payments.deposit_id
							AND deposits.export_batch_id = ' . $this->getId() . '
							AND transactions.charge_code_id = ' . CChargeCode::PAYMENT_RECEIVED . '
							AND transactions.export_batch_id IS NULL
							AND company_payments.deposit_id IS NOT NULL
							AND transactions.currency_code = \'' . $this->getCurrencyCode() . '\';';

		return $objAdminDatabase->execute( $strSql );

	}

	public function updateExportBatchIdIntoSalesTaxTransactions( $objAdminDatabase ) {
		$strSql = 'UPDATE sales_tax_transactions
						SET
							export_batch_id = ' . $this->getId() . '
						FROM
							transactions
						WHERE
							transactions.id = sales_tax_transactions.transaction_id
							AND transactions.export_batch_id = ' . $this->getId();

		return $objAdminDatabase->execute( $strSql );
	}

	public function updateExportBatchIdIntoOtherNonPaymentTransactions( $objAdminDatabase ) {
		switch( $this->getExportBatchTypeId() ) {
			case CExportBatchType::PROPERTY_SOLUTIONS:
				$arrintAccountTypeIds = array(
					CAccountType::PRIMARY,
					CAccountType::INTERMEDIARY,
					CAccountType::CLEARING,
					CAccountType::VACANCY,
					CAccountType::STANDARD,
					CAccountType::UTILITY_BILLING,
					CAccountType::MERCHANT_SWEEP,
					CAccountType::SMALL_PROPERTIES,
					CAccountType::MERCHANT,
					CAccountType::COLLECTIONS,
					CAccountType::VENDOR_ACCESS
				);
				break;

			case CExportBatchType::RESIDENT_INSURE_MARKEL:
				$arrintAccountTypeIds = array( CAccountType::INSURANCE_MARKEL );
				break;

			case CExportBatchType::RESIDENT_INSURE_KEMPER:
				$arrintAccountTypeIds = array( CAccountType::INSURANCE_KEMPER );
				break;

			case CExportBatchType::RESIDENT_INSURE_QBE:
				$arrintAccountTypeIds = array( CAccountType::INSURANCE_QBE );
				break;

			case CExportBatchType::RESIDENT_INSURE_IDTHEFT:
			    $arrintAccountTypeIds = array( CAccountType::INSURANCE_IDTHEFT );
			    break;

			case CExportBatchType::MASTER_POLICY:
				$arrintAccountTypeIds = array( CAccountType::MASTER_POLICY );
				break;

			default:
				trigger_error( 'Unexpected export batch type.', E_USER_ERROR );
				break;
		}

		// Update the charges
		$strSql = 'UPDATE transactions t
								SET
									export_batch_id = ' . $this->getId() . '
								FROM
									clients c,
									accounts a
								WHERE
									t.cid = c.id
									AND t.account_id = a.id
									AND t.charge_code_id != ' . CChargeCode::PAYMENT_RECEIVED . '
									AND c.company_status_type_id IN (' . ( int ) CCompanyStatusType::CLIENT . ',' . ( int ) CCompanyStatusType::TERMINATED . ')
									AND a.account_type_id IN ( ' . implode( ',', $arrintAccountTypeIds ) . ' )
									AND ( DATE_TRUNC( \'DAY\', t.transaction_datetime ) <= \'' . $this->getExportBatchDate() . '\'
										OR ( t.transaction_id IS NOT NULL and t.post_month <= \'' . $this->getExportBatchDate() . '\' ) )
									AND t.export_batch_id IS NULL
									AND t.company_payment_id IS NULL
									AND t.currency_code = \'' . $this->getCurrencyCode() . '\';

							UPDATE export_batches eb
							SET cached_charges_total = sub.batch_total
							FROM ( SELECT eb.id AS export_batch_id, sum ( t.transaction_amount ) as batch_total
									FROM
										export_batches eb,
										transactions t
									WHERE
										eb.id = t.export_batch_id
										AND t.company_payment_id IS NULL
										AND eb.id = ' . $this->getId() . '
									GROUP BY eb.id ) as sub
							WHERE eb.id = sub.export_batch_id AND eb.id = ' . $this->getId() . ';

							UPDATE export_batches eb
							SET cached_payments_total = sub.batch_total
							FROM ( SELECT eb.id AS export_batch_id, sum ( t.transaction_amount ) as batch_total
									FROM
										export_batches eb,
										transactions t
									WHERE
										eb.id = t.export_batch_id
										AND t.company_payment_id IS NOT NULL
										AND eb.id = ' . $this->getId() . '
									GROUP BY eb.id ) as sub
							WHERE eb.id = sub.export_batch_id AND eb.id = ' . $this->getId() . ';

							UPDATE export_batches eb
							SET cached_ar_balance = sub.cached_ar_balance
							FROM ( SELECT
										id as export_batch_id,
										( SELECT (sum( cached_charges_total ) + sum( cached_payments_total )) FROM export_batches WHERE export_batch_date <= eb.export_batch_date AND export_batch_type_id = eb.export_batch_type_id AND currency_code = \'' . $this->getCurrencyCode() . '\' ) as cached_ar_balance
									FROM
										export_batches eb ) as sub
							WHERE
								eb.id = sub.export_batch_id
								AND eb.id = ' . $this->getId() . ';';

		return $objAdminDatabase->execute( $strSql );
	}

	public function updateDeferredExportBatchIdIntoTransactions( $objAdminDatabase ) {

		if( CExportBatchType::PROPERTY_SOLUTIONS != $this->getExportBatchTypeId() ) {
			return true;
		}

		$arrintAccountTypeIdsExclude = array(
			CAccountType::INSURANCE_MARKEL,
			CAccountType::INSURANCE_KEMPER,
			CAccountType::INSURANCE_QBE,
		    CAccountType::INSURANCE_IDTHEFT,
			CAccountType::MASTER_POLICY
		);

		// Update the charges
		$strSql = 'UPDATE transactions t
						SET
							deferred_export_batch_id = ' . $this->getId() . '
						FROM
							clients c,
							accounts a,
							invoices i
						WHERE
							t.cid = c.id
							AND t.account_id = a.id
							AND t.invoice_id = i.id
							AND c.company_status_type_id IN (' . ( int ) CCompanyStatusType::CLIENT . ',' . ( int ) CCompanyStatusType::TERMINATED . ')
							AND a.account_type_id NOT IN ( ' . implode( ',', $arrintAccountTypeIdsExclude ) . ' )
							AND DATE_TRUNC( \'DAY\', i.invoice_date ) <= \'' . $this->getExportBatchDate() . '\'
							AND t.export_batch_id IS NULL
							AND t.deferred_export_batch_id IS NULL
							AND t.company_payment_id IS NULL
							AND t.currency_code = \'' . $this->getCurrencyCode() . '\';';

		return $objAdminDatabase->execute( $strSql );
	}

	public function updateExportBatchIdIntoInternalTransfers( $arrmixInternalTransfers, $objAdminDatabase ) {

		if( false == valArr( $arrmixInternalTransfers ) ) {
			return false;
		}

		$strInternalTransferIds = implode( ',', array_keys( rekeyArray( 'internal_transfer_ids', $arrmixInternalTransfers ) ) );

		$strSql = 'UPDATE internal_transfers
						SET
							export_batch_id = ' . $this->getId() . '
						WHERE
							id IN ( ' . $strInternalTransferIds . ' )';

		return $objAdminDatabase->execute( $strSql );
	}

	public function updateExportBatchIdIntoAllocations( $arrintAccountTypeIds, $strCurrencyCode, $strExportBatchDate, $objAdminDatabase ) {
		$strSubQuery = \Psi\Eos\Admin\CAllocations::createService()->fetchAllocationsByAccountTypeIds( $arrintAccountTypeIds, $strCurrencyCode, $strExportBatchDate, $objAdminDatabase, true );
		if( false == valStr( $strSubQuery ) ) {
			return false;
		}

		$strSql = '
			UPDATE allocations
			SET export_batch_id = ' . $this->getId() . '
			FROM (
				' . $strSubQuery . '
			) sub
			WHERE allocations.id = sub.id;
		';

		return $objAdminDatabase->execute( $strSql );
	}

	public function resetExportBatchId( $objAdminDatabase ) {

		if( false == valId( $this->getId() ) ) {
			return false;
		}

		// set export_batch_id NULL ON deposits and transactions
		$strSql = 'UPDATE deposits SET export_batch_id = NULL WHERE export_batch_id = ' . $this->getId() . ';';

		$strSql .= 'UPDATE transactions SET export_batch_id = NULL WHERE export_batch_id = ' . $this->getId() . ';';

		$strSql .= 'UPDATE transactions SET deferred_export_batch_id = NULL WHERE deferred_export_batch_id = ' . $this->getId() . ';';

		$strSql .= 'UPDATE sales_tax_transactions SET export_batch_id = NULL WHERE export_batch_id = ' . $this->getId() . ';';

		$strSql .= 'UPDATE invoice_batches SET export_batch_id = NULL, exported_on = NULL, exported_by = NULL WHERE export_batch_id = ' . $this->getId() . ';';

		$strSql .= 'UPDATE internal_transfers SET export_batch_id = NULL WHERE export_batch_id = ' . $this->getId() . ';';

		$strSql .= 'UPDATE allocations SET export_batch_id = NULL WHERE export_batch_id = ' . $this->getId() . ';';

		return $objAdminDatabase->execute( $strSql );

	}

}
?>