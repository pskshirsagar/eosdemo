<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportSqlDetails
 * Do not add any new functions to this class.
 */

class CReportSqlDetails extends CBaseReportSqlDetails {

	public static function fetchReportSqlDetailByCompanyReportId( $intCompanyReportId, $objDatabase ) {

		if( false == is_numeric( $intCompanyReportId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rsd.query AS report_sql,
						array_to_string(array_agg(rsd1.database_id), \',\') AS databases_ids,
						cr.name,
						cr.updated_on
					FROM
						report_sql_details rsd
						JOIN report_sql_databases rsd1 ON ( rsd.company_report_id = ' . ( int ) $intCompanyReportId . ' AND rsd.id = rsd1.report_sql_detail_id )
						JOIN report_details rd ON ( rsd.id = rd.report_sql_detail_id AND rd.is_current_version = true )
						JOIN company_reports cr ON ( cr.id = rsd.company_report_id )
					GROUP BY
						rsd.query,
						cr.name,
						cr.updated_on';

		$arrmixReportDetail = fetchData( $strSql, $objDatabase );
		return $arrmixReportDetail[0];
	}

	public static function fetchReportSqlByCompanyReportId( $intCompanyReportId, $objDatabase ) {

		if( false == is_numeric( $intCompanyReportId ) ) {
			return false;
		}

		$strSql = 'SELECT 
						rsd.id,
						rsd.query,
						array_to_string(array_agg( DISTINCT ( rsdb.database_id) ), \',\') as database_ids
					FROM
						report_sql_details rsd
					JOIN report_details rd ON ( rd.report_sql_detail_id = rsd.id AND rd.is_current_version = true AND rd.report_data_type_id = ' . CReportDataType::SQL . ' )
					JOIN report_sql_databases rsdb ON ( rsdb.report_sql_detail_id = rd.report_sql_detail_id )
					WHERE rsd.company_report_id = ' . ( int ) $intCompanyReportId . '
					GROUP BY rsd.id,rsd.query';

		$arrmixReportDetail = fetchData( $strSql, $objDatabase );
		return $arrmixReportDetail[0];

	}

}
?>