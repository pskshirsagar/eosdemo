<?php

class CPsLeadStage extends CBasePsLeadStage {

	const PROSPECTING_HOLD		= 1;
	const PROSPECTING			= 2;
	const CONTACT				= 3;
	const QUALIFIED_OPPORTUNITY	= 4;
	const ACTIVE_EVALUATION		= 5;
	const CLOSING				= 6;

	public static $c_arrmixPsLeadStageName = [
		self::PROSPECTING_HOLD			=> 'Prospecting Hold',
		self::PROSPECTING				=> 'Prospecting',
		self::CONTACT					=> 'Contact',
		self::QUALIFIED_OPPORTUNITY		=> 'Qualified Opportunity',
		self::ACTIVE_EVALUATION			=> 'Active Evaluation',
		self::CLOSING					=> 'Closing'
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getPsLeadStageNameById( $intPsLeadStageId ) {
		return self::$c_arrmixPsLeadStageName[$intPsLeadStageId] ?? 'Invalid Stage id.';
	}

}
?>