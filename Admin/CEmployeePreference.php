<?php

class CEmployeePreference extends CBaseEmployeePreference {

	const SYNC_TO_XMPP											= 'SYNC_TO_XMPP';
	const PSI_VOICEMAIL_ACTION									= 'PSI_VOICEMAIL_ACTION';
	const EMPLOYEE_REVIEW_CYCLE									= 'EMPLOYEE_REVIEW_CYCLE';
	const ENABLE_NEW_TASK_SYSTEM								= 'ENABLE_NEW_TASK_SYSTEM';
	const HIDE_PHONE_NUMBER										= 'HIDE_PHONE_NUMBER';
	const NO_DUES_APPROVED_BY_LIVE_IT							= 'NO_DUES_APPROVED_BY_LIVE_IT';
	const NO_DUES_APPROVED_BY_DESKTOP_IT						= 'NO_DUES_APPROVED_BY_DESKTOP_IT';
	const IS_EXTERNAL_TOOLS_UPDATED								= 'IS_EXTERNAL_TOOLS_UPDATED';
	const RESIDENT_UTILITY_BILLING_CAPACITY						= 'RESIDENT_UTILITY_BILLING_CAPACITY';
	const IS_MANAGER_APPROVAL_MODE								= 'IS_MANAGER_APPROVAL_MODE';
	const TEAM_STRUCTURE										= 'TEAM_STRUCTURE';
	const POC_KEY 												= 'EMPLOYEE_TRANSPORT_POC';
	const EMAIL_TODAYS_SPECIAL_XENTO							= 'EMAIL_TODAYS_SPECIAL_XENTO';
	const EMAIL_TODAYS_SPECIAL_ENTRATA							= 'EMAIL_TODAYS_SPECIAL_ENTRATA';
	const EMAIL_TODAYS_SPECIAL_LEASING_CENTER					= 'EMAIL_TODAYS_SPECIAL_LEASING_CENTER';
	const EMAIL_TODAYS_SPECIAL_HAPPY_BIRTHDAY					= 'EMAIL_TODAYS_SPECIAL_HAPPY_BIRTHDAY';
	const EMAIL_TODAYS_SPECIAL_WORK_ANNIVERSARY					= 'EMAIL_TODAYS_SPECIAL_WORK_ANNIVERSARY';
	const EMAIL_TODAYS_SPECIAL_WELCOME_ABOARD					= 'EMAIL_TODAYS_SPECIAL_WELCOME_ABORD';
	const EMAIL_TODAYS_SPECIAL_WEDDING_ANNIVERSARY				= 'EMAIL_TODAYS_SPECIAL_WEDDING_ANNIVERSARY';
	const IS_TRAINER											= 'IS_TRAINER';
	const CURRENT_LEAVES										= 'CURRENT_LEAVES';
	const KEY_EMP_TAX_SAVINGS_DOCS_ACCESS_RESTRICTED_USER_ID	= 'KEY_EMP_TAX_SAVINGS_DOCS_ACCESS_RESTRICTED_USER_ID';
	const WORK_FORM_HOME_KEY									= 'IS_WORK_FROM_HOME';
	const ASSET_ID_FOR_VPN_POLICY_SIGNED						= 'ASSET_ID_FOR_VPN_POLICY_SIGNED';
	const ASSET_ID_FOR_HARDWARE_ALLOCATION_FORM_SIGNED			= 'ASSET_ID_FOR_HARDWARE_ALLOCATION_FORM_SIGNED';
	const VPN_ONLY												= 'IS_VPN_ONLY';
	const IS_CPA_RATING_OPTIONAL								= 'IS_CPA_RATING_OPTIONAL';
	const ASSET_REMOTE_WFH_POLICY_SIGNED						= 'ASSET_REMOTE_WFH_POLICY_SIGNED';
	const EMPLOYEE_NCND_AGREEMENT_SIGNED						= 'EMPLOYEE_NCND_AGREEMENT_SIGNED';
	const REGENERATE_HARDWARE_POLICY							= 'REGENERATE_HARDWARE_POLICY';
	const REGENERATE_VPN_POLICY									= 'REGENERATE_VPN_POLICY';
	const REGENERATE_WFH_POLICY									= 'REGENERATE_WFH_POLICY';
    const CURRENT_ADDRESS_CHANGE_SETTING                        = 'CURRENT_ADDRESS_CHANGE_SETTING';
    const ADDRESS_CHANGE_SETTING_LIFETIME                       = 86400;
    const AWS_ACCESS											= 'AWS_ACCESS';
	const IT_WFH_POLICY_START_DATE								= '2020-03-15';
	const WFH_POLICY_ENFORCEMENT_DAYS							= 10;

	const NCND_FORCE_FILL_JOIN_DATE		= '2021-01-01';
	const NCND_MAX_UNSIGNED_DAYS		= 3;
	const TWELVE_HOURS_IN_SECONDS		= 43200;

	public Static $c_arrstrEmployeeAssetPreferences = [
		self::WORK_FORM_HOME_KEY,
		self::ASSET_ID_FOR_VPN_POLICY_SIGNED,
		self::ASSET_ID_FOR_HARDWARE_ALLOCATION_FORM_SIGNED,
		self::VPN_ONLY,
		self::ASSET_REMOTE_WFH_POLICY_SIGNED
	];

	public static $c_arrstrTodaySpecialEmployeePreferences = [
		self::EMAIL_TODAYS_SPECIAL_ENTRATA,
		self::EMAIL_TODAYS_SPECIAL_XENTO,
		self::EMAIL_TODAYS_SPECIAL_LEASING_CENTER,
		self::EMAIL_TODAYS_SPECIAL_HAPPY_BIRTHDAY,
		self::EMAIL_TODAYS_SPECIAL_WORK_ANNIVERSARY,
		self::EMAIL_TODAYS_SPECIAL_WEDDING_ANNIVERSARY
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// no default validation.
		}

		return $boolIsValid;
	}

	public static function createEmployeePreference( $intEmployeeId, $strEmployeePreferenceKey, $intKeyValue ) {
		$objEmployeePreference = new CEmployeePreference();
		$objEmployeePreference->setEmployeeId( $intEmployeeId );
		$objEmployeePreference->setKey( $strEmployeePreferenceKey );
		$objEmployeePreference->setValue( $intKeyValue );

		return $objEmployeePreference;
	}

}
?>