<?php

class CResourceRequisition extends CBaseResourceRequisition {

	const NEW_HIRE_REPLACEMENT = 3;
	const YEAR_TO_SHOW_PR_AFTER = 2015;

	/**
	 * Validate functions
	 *
	 */

	public function valDesignationId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intDesignationId ) || ( 1 > $this->m_intDesignationId ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'designation_id', 'Designation is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valNumberOfResources() {
		$boolIsValid = true;

		if( CRequirementType::REPLACEMENT != $this->m_intRequirementTypeId && ( 0 >= ( int ) $this->getNumberOfResources() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_resources', 'No. of resources is required.' ) );
		}
		return $boolIsValid;
	}

	public function valRequirementTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intRequirementTypeId ) || ( 1 > $this->m_intRequirementTypeId ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requirement_type_id', 'Requirement type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valRequirementDueDate() {
		$boolIsValid = true;

		if( false == isset( $this->m_strRequirementDueDate ) || ( 0 == strlen( $this->m_strRequirementDueDate ) ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requirementDueDate', 'Target hire date is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valManagerEmployeeId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intManagerEmployeeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'manager_employee_id', 'Reporting manager is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valReplacementEmployeeIds() {
		$boolIsValid = true;

		if( true == empty( $this->m_strReplacementEmployeeIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'replacement_employee_id', 'Employee to be replaced is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valRequiredExperience( $strCountryCode ) {
		$boolIsValid = true;
		if( CCountry::CODE_INDIA == $strCountryCode && ( false == isset( $this->m_intRequiredExperience ) || ( 1 > $this->m_intRequiredExperience ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_experience', 'Experience is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valRequiredQualification() {
		$boolIsValid = true;
		if( false == isset( $this->m_intRequiredQualification ) || ( 1 > $this->m_intRequiredQualification ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_qualification', 'Educational qualification is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valDeletionRequestedBy() {
		$boolIsValid = true;

		if( false == isset( $this->m_intDeletionRequestedBy ) || ( 1 > $this->m_intDeletionRequestedBy ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deletion_requested_by', 'Request received from is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valDeletionConfirmedBy() {
		$boolIsValid = true;

		if( false == isset( $this->m_intDeletionConfirmedBy ) || ( 1 > $this->m_intDeletionConfirmedBy ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deletion_confirmed_by', 'Request confirmed by is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valJobDescription() {
		$boolIsValid = true;

		if( false == isset( $this->m_strJobDescription ) || ( 0 == strlen( $this->m_strJobDescription ) ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Description', 'Job description is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valPsProduct( $strCountryCode ) {
		$boolValid = true;

		if( CCountry::CODE_INDIA == $strCountryCode && false == valId( $this->getProductId() ) && false == valId( $this->getProductOptionId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Product', 'Product is required.' ) );
		}

		return $boolValid;
	}

	public function valDepartmentId( $intDepartmentId ) {
		$boolValid = true;

		if( false == valId( $intDepartmentId ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Department', 'Department is required.' ) );
		}

		return $boolValid;
	}

	public function valRecruiterEmployeeId() {
		$boolIsValid = true;

		if( false == valId( $this->getRecruiterEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recruiter_employee_id', 'Recruiter point of contact is required. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIgnoreExperience = false, $strCountryCode = NULL, $objUser = NULL, $intPurchaseRequestStatusTypeId = NULL, $intDepartmentId = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valDesignationId();
				$boolIsValid &= $this->valPsProduct( $strCountryCode );
				$boolIsValid &= $this->valNumberOfResources();
				if( CCountry::CODE_INDIA != $strCountryCode ) {
					$boolIsValid &= $this->valJobDescription();
				}
				$boolIsValid &= $this->valManagerEmployeeId();
				if( false == $boolIgnoreExperience ) {
					$boolIsValid &= $this->valRequiredExperience( $strCountryCode );
				}
				$boolIsValid &= $this->valRequiredQualification();
				$boolIsValid &= $this->valRequirementDueDate();
				$boolIsValid &= $this->valRequirementTypeId();

				if( CCountry::CODE_INDIA == $strCountryCode && ( CDepartment::HR == $objUser->getEmployee()->getDepartmentId() || true == in_array( CRole::APPROVES_PURCHASE_REQUESTS, $objUser->getAssociatedRoleIds() ) ) ) {
					$boolIsValid &= $this->valRecruiterEmployeeId();
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDesignationId();
				$boolIsValid &= $this->valPsProduct( $strCountryCode );
				$boolIsValid &= $this->valNumberOfResources();

				if( CCountry::CODE_INDIA != $strCountryCode ) {
					$boolIsValid &= $this->valJobDescription();
				}

				$boolIsValid &= $this->valManagerEmployeeId();
				if( false == $boolIgnoreExperience ) {
					$boolIsValid &= $this->valRequiredExperience( $strCountryCode );
				}

				$boolIsValid &= $this->valRequiredQualification();
				$boolIsValid &= $this->valRequirementDueDate();
				$boolIsValid &= $this->valRequirementTypeId();
				if( CCountry::CODE_INDIA == $strCountryCode && ( CDepartment::HR == $objUser->getEmployee()->getDepartmentId() || true == in_array( CRole::APPROVES_PURCHASE_REQUESTS, $objUser->getAssociatedRoleIds() ) ) ) {
					$boolIsValid &= $this->valRecruiterEmployeeId();
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valNumberOfResources();
				$boolIsValid &= $this->valJobDescription();
				$boolIsValid &= $this->valRequiredExperience();
				$boolIsValid &= $this->valRequiredQualification();
				$boolIsValid &= $this->valRequirementDueDate();
				$boolIsValid &= $this->valRequirementTypeId();
				$boolIsValid &= $this->valDesignationId();
				$boolIsValid &= $this->valDeletionConfirmedBy();
				$boolIsValid &= $this->valDeletionRequestedBy();
				break;

			case 'validate_insert_new_hire':
				if( CCountry::CODE_INDIA == $strCountryCode ) {
					$boolIsValid &= $this->valDepartmentId( $intDepartmentId );
				}
				$boolIsValid &= $this->valDesignationId();
				$boolIsValid &= $this->valPsProduct( $strCountryCode );
				$boolIsValid &= $this->valNumberOfResources();

				if( CCountry::CODE_INDIA != $strCountryCode ) {
					$boolIsValid &= $this->valJobDescription();
				}

				$boolIsValid &= $this->valRequiredQualification();
				$boolIsValid &= $this->valRequirementDueDate();
				$boolIsValid &= $this->valRequirementTypeId();
				$boolIsValid &= $this->valManagerEmployeeId();

				if( CRequirementType::REPLACEMENT == $this->getRequirementTypeId() ) {
					$boolIsValid &= $this->valReplacementEmployeeIds();
				}

				if( false == $boolIgnoreExperience ) {
					$boolIsValid &= $this->valRequiredExperience( $strCountryCode );
				}

				if( CCountry::CODE_INDIA == $strCountryCode && ( CDepartment::HR == $objUser->getEmployee()->getDepartmentId() || true == in_array( CRole::APPROVES_PURCHASE_REQUESTS, $objUser->getAssociatedRoleIds() ) ) ) {
					$boolIsValid &= $this->valRecruiterEmployeeId();
				}
				break;

			case 'VALIDATE_US_NEW_HIRE':
				$boolIsValid &= $this->valRequirementTypeId();
				$boolIsValid &= $this->valPsProduct( $strCountryCode );
				$boolIsValid &= $this->valDesignationId();

				if( false == valId( $intPurchaseRequestStatusTypeId ) || false == in_array( $intPurchaseRequestStatusTypeId, [ CPurchaseRequestStatusType::ID_HOLD, CPurchaseRequestStatusType::ID_PENDING_FOR_REMOVE_FROM_HR ] ) ) {
					$boolIsValid &= $this->valRequirementDueDate();
				}

				$boolIsValid &= $this->valManagerEmployeeId();
				$boolIsValid &= $this->valRequiredExperience( $strCountryCode );

				if( CRequirementType::REPLACEMENT == $this->getRequirementTypeId() ) {
					$boolIsValid &= $this->valReplacementEmployeeIds();
				}
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>