<?php

class CIlsFeedType extends CBaseIlsFeedType {

	const FULL_FEED 				 = 1;
	const FULL_FEED_AND_LIMITED_FEED = 2;
	const LIMITED_FEED 				 = 3;
	const NO_PHOTOS_FEED 			 = 4;
	const PROSPECT_FEED 			 = 5;

	public static $c_arrstrIlsFeedTypeNamesByIlsFeedTypeId = array(
		self::FULL_FEED					 => 'Full Feed',
		self::FULL_FEED_AND_LIMITED_FEED => 'Full & Limited Feed',
		self::LIMITED_FEED				 => 'Limited Feed',
		self::NO_PHOTOS_FEED			 => 'No Photos Feed',
		self::PROSPECT_FEED				 => 'Prospect Feed'
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>