<?php

class CTaskSvnFile extends CBaseTaskSvnFile {

	protected $m_intUpdatedBy;
	protected $m_intDatabaseTypeId;
	protected $m_strFilename;
	protected $m_strTitle;
	protected $m_strCommitDateTime;
	protected $m_strNameFull;
	protected $m_strPreferredName;

 	/**
 	 * Get And Set Functions
 	 *
 	 */

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function valUpdatedBy() {
		return true;
	}

	public function sqlUpdatedBy() {
		return  ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setDatabaseTypeId( $intDatabaseId ) {
		$this->m_intDatabaseTypeId = CStrings::strToIntDef( $intDatabaseId, NULL, false );
	}

	public function getDatabaseTypeId() {
		return $this->m_intDatabaseTypeId;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFilename = CStrings::strTrimDef( $strFileName, -1, NULL, true );
	}

	public function getFileName() {
		return $this->m_strFilename;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = CStrings::strTrimDef( $strTitle, -1, NULL, true );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function setCommitDateTime( $strCommitDateTime ) {
		$this->m_strCommitDateTime = $strCommitDateTime;
	}

	public function getCommitDateTime() {
		return $this->m_strCommitDateTime;
	}

	public function setId( $intId ) {
		parent::setId( $intId );
		$this->setOrderNum( $intId );
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = CStrings::strTrimDef( $strNameFull, 100, NULL, true );
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function setPreferredName( $strPreferredName ) {
		$this->m_strPreferredName = CStrings::strTrimDef( $strPreferredName, 100, NULL, true );
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valSvnFileId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSvnFileId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'svn_file_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( false == ( 'RFR' == \Psi\CStringService::singleton()->substr( $this->getFileName(), 0, 3 ) || 'RFS' == \Psi\CStringService::singleton()->substr( $this->getFileName(), 0, 3 ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'Invalid file name, it should start with RFR/RFS.' ) );
		}

		return $boolIsValid;
	}

	public function valSchemaApprovedBy() {
		$boolIsValid = true;

		if( true == is_null( $this->getSchemaApprovedBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'schema_approved_by', 'Please select schema approved by for ' . $this->getFileName() ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsSchemaApprovedByRequired = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valSvnFileId();
				$boolIsValid &= $this->valTaskId();
				if( true == $boolIsSchemaApprovedByRequired ) {
					$boolIsValid &= $this->valSchemaApprovedBy();
				}
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['database_type_id'] ) ) $this->setDatabaseTypeId( $arrmixValues['database_type_id'] );
		if( true == isset( $arrmixValues['file_name'] ) ) $this->setFileName( $arrmixValues['file_name'] );
		if( true == isset( $arrmixValues['title'] ) ) $this->setTitle( $arrmixValues['title'] );
		if( true == isset( $arrmixValues['commit_date_time'] ) ) $this->setCommitDateTime( $arrmixValues['commit_date_time'] );
		if( true == isset( $arrmixValues['name_full'] ) ) $this->setNameFull( $arrmixValues['name_full'] );
		if( true == isset( $arrmixValues['preferred_name'] ) ) $this->setPreferredName( $arrmixValues['preferred_name'] );

		return;
	}

}
?>