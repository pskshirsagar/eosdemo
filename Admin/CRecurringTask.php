<?php

class CRecurringTask extends CBaseRecurringTask {

	/**
	* Get Functions
	*/

	public function getRecurringTasks() {
		return $this->m_arrobjRecurringTasks;
	}

	/**
	* Add Functions
	*/

	public function addRecurringTask( $objRecurringTask ) {

		if( false == valObj( $objRecurringTask, 'CRecurringTask' ) ) return false;

		if( false == is_null( $objRecurringTask->getId() ) ) {
			$this->m_arrobjRecurringTasks[$objRecurringTask->getId()] = $objRecurringTask;
		} else {
			$this->m_arrobjRecurringTasks[] = $objRecurringTask;
		}

		return true;
	}

	/**
	* Validate Functions
	*/

	public function valTaskFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_frequency_id', 'Frequency is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUserId() {
		$boolIsValid = true;
		if( true == is_null( $this->getUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Employee name is required.' ) );
		}

		return $boolIsValid;

	}

	public function valTaskTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_type_id', 'Task Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTaskStatusId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskStatusId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_status_id', 'Status is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTaskPriorityId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskPriorityId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_priority_id', 'Priority is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		if( CTaskType::FEATURE == $this->getTaskTypeID() ) {

			if( true == is_null( $this->getPsProductId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product', 'Product is required.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valClusterId() {
		$boolIsValid = true;

		if( CTaskType::FEATURE == $this->getTaskTypeID() ) {

			if( true == is_null( $this->getClusterId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cluster_id', 'Release type is required.' ) );
			}
		}
		return $boolIsValid;

	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			 $boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBeginDate( $boolIsUpdate = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->getBeginDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_date', 'Begin date is required.' ) );
		}

		if( true == $boolIsValid && ( strtotime( $this->getBeginDate() ) < strtotime( date( 'm/d/Y' ) ) ) && false == $boolIsUpdate ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_date', 'Begin date must be greater than today\'s date.' ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getBeginDate() ) && false == is_null( $this->getEndDate() ) ) {
			if( ( strtotime( $this->getBeginDate() ) ) > ( strtotime( $this->getEndDate() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date cannot be earlier than begin date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valStoryPoints() {
		$boolIsValid = true;

		if( $this->getDeveloperStoryPoints() < 0 || $this->getDeveloperStoryPoints() > 50 ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'developer_story_points', 'Story points should be between 0 and 50.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTaskTypeId();
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valTaskFrequencyId();
					$boolIsValid &= $this->valBeginDate();
					$boolIsValid &= $this->valEndDate();
					$boolIsValid &= $this->valUserId();
					$boolIsValid &= $this->valClusterId();
					$boolIsValid &= $this->valPsProductId();
					$boolIsValid &= $this->valTitle();
					$boolIsValid &= $this->valDescription();
					$boolIsValid &= $this->valTaskPriorityId();
					$boolIsValid &= $this->valTaskStatusId();
					$boolIsValid &= $this->valStoryPoints();
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTaskTypeId();
				$boolIsValid &= $this->valTaskFrequencyId();
				$boolIsValid &= $this->valBeginDate( true );
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valClusterId();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valTaskPriorityId();
				$boolIsValid &= $this->valTaskStatusId();
				$boolIsValid &= $this->valStoryPoints();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Create Functions
	*/

	public function createTask( $intTaskDatetime, $intNextTaskReleaseId ) {

		$objTask = new CTask();
		$objTask->setDefaults();

		$objTask->setCid( $this->m_intCid );
		$objTask->setUserId( $this->m_intUserId );
		$objTask->setCreatedBy( $this->m_intCreatedBy );
		$objTask->setDepartmentId( $this->m_intDepartmentId );
		$objTask->setTaskTypeId( $this->m_intTaskTypeId );
		$objTask->setTaskStatusId( $this->m_intTaskStatusId );
		$objTask->setTaskPriorityId( $this->m_intTaskPriorityId );
		$objTask->setPropertyId( $this->m_intPropertyId );
		$objTask->setTitle( $this->m_strTitle );
		$objTask->setDescription( $this->m_strDescription );
		$objTask->setDevStoryPoints( $this->m_fltDeveloperStoryPoints );
		$objTask->setTaskReleaseId( $intNextTaskReleaseId );
		$objTask->setTaskDatetime( date( 'm/d/Y', $intTaskDatetime ) );
		$objTask->setUrl( $this->m_strUrl );

		if( CTaskType::FEATURE == $this->getTaskTypeId() ) {
			$objTask->setPsProductId( $this->getPsProductId() );
			$objTask->setPsProductOptionId( $this->getPsProductOptionId() );
			$objTask->setClusterId( $this->getClusterId() );
		}
		// Calculate and set due dates
		if( true == isset ( $this->m_intDaysAllotted ) ) {
			$strDueDate = date( 'm/d/Y', ( $intTaskDatetime + ( $this->m_intDaysAllotted * 24 * 3600 ) ) );

			$objTask->setDueDate( $strDueDate );
			$objTask->setOriginalDueDate( $strDueDate );
			$objTask->setPlannedCompletionDate( $strDueDate );
		}

		if( CTaskType::HELPDESK == $this->getTaskTypeId() ) {
			$objTask->setGroupId( CGroup::IT_SUPPORT );
			$objTask->setTaskReleaseId( NULL );
			if( false == in_array( $this->getTaskStatusId(), CTaskStatus::$c_arrintItHelpdeskTaskStatusesIds ) ) {
				$objTask->setTaskStatusId( CTaskStatus::UNUSED );
			}
		}

		return $objTask;
	}

	public function createTasks( $intNextTaskReleaseId ) {

		$arrobjTasks 		= array();
		$intTodayTimeStamp 	= strtotime( date( 'm/d/Y' ) );

		// Here we figure out the first task date that we need to post.
		if( false == isset ( $this->m_strLastPostedOn ) ) {
			$intNextTaskPostDate = strtotime( $this->m_strBeginDate );

		} else {

			$intLastPostedOn		= strtotime( $this->m_strLastPostedOn );
			$intNextTaskPostDate	= strtotime( $this->m_strBeginDate );

			switch( $this->m_intTaskFrequencyId ) {
				case CTaskFrequency::ONCE:
					break;

				case CTaskFrequency::DAILY:
					while( $intNextTaskPostDate <= ( $intLastPostedOn ) ) {
						$intNextTaskPostDate = strtotime( '+1 day', $intNextTaskPostDate );
					}
					break;

				case CTaskFrequency::WEEKLY:
					while( $intNextTaskPostDate <= ( $intLastPostedOn ) ) {
						$intNextTaskPostDate = strtotime( '+1 week', $intNextTaskPostDate );
					}
					break;

				case CTaskFrequency::MONTHLY:
					while( $intNextTaskPostDate <= ( $intLastPostedOn ) ) {
						$intNextTaskPostDate = strtotime( '+1 month', $intNextTaskPostDate );
					}
					break;

				case CTaskFrequency::QUARTERLY:
					while( $intNextTaskPostDate <= ( $intLastPostedOn ) ) {
						$intNextTaskPostDate = strtotime( '+3 months', $intNextTaskPostDate );
					}
					break;

				case CTaskFrequency::ANNUALLY:
					while( $intNextTaskPostDate <= ( $intLastPostedOn ) ) {
						$intNextTaskPostDate = strtotime( '+1 year', $intNextTaskPostDate );
					}
					break;

				default:
					// default case
					// break;

			}
		}

		// If the next task post date is greater than the end date, return null.
		if( true == isset( $this->m_strEndDate ) && $intNextTaskPostDate > strtotime( $this->m_strEndDate ) ) {
			return array();
		}

		// If the next task post date is greater than now, return null.
		if( $intNextTaskPostDate > $intTodayTimeStamp ) {
			return array();
		}

		// If the current date is less than the begin date, return null.
		if( $intTodayTimeStamp < strtotime( $this->m_strBeginDate ) ) {
			return array();
		}

		// Now we loop and create all of the tasks that need to be posted since the last post date.
		// This should usually only be one task unless an task was created with an old start date or if the
		// task poster cron failed to run for a period.
		while( $intNextTaskPostDate <= $intTodayTimeStamp && ( false == isset ( $this->m_strEndDate ) || ( true == isset ( $this->m_strEndDate ) && $intNextTaskPostDate < strtotime( $this->m_strEndDate ) ) ) ) {

			$arrobjTasks[] = $this->createTask( $intNextTaskPostDate, $intNextTaskReleaseId );
			$this->setLastPostedOn( date( 'm/d/Y', $intNextTaskPostDate ) );

			// Increment the next task post date
			switch( $this->m_intTaskFrequencyId ) {
				case CTaskFrequency::ONCE:
					return $arrobjTasks;
					exit;

				case CTaskFrequency::DAILY:
					$intNextTaskPostDate = strtotime( '+1 day', $intNextTaskPostDate );
					break;

				case CTaskFrequency::WEEKLY:
					$intNextTaskPostDate = strtotime( '+1 week', $intNextTaskPostDate );
					break;

				case CTaskFrequency::MONTHLY:
					$intNextTaskPostDate = strtotime( '+1 month', $intNextTaskPostDate );
					break;

				case CTaskFrequency::QUARTERLY:
					$intNextTaskPostDate = strtotime( '+3 months', $intNextTaskPostDate );
					break;

				case CTaskFrequency::ANNUALLY:
					$intNextTaskPostDate = strtotime( '+1 year', $intNextTaskPostDate );
					break;

				default:
					trigger_error( 'Task frequency is null.', E_USER_ERROR );
					exit;
			}
		}
		return $arrobjTasks;
	}

	/**
	 * Database Functions
	*/

	public function delete( $intUserId, $objDataBase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( '  NOW() ' );

		if( $this->update( $intUserId, $objDataBase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

	/**
	* Fetch Functions
	*/

	public function fetchTaskType( $objDatabase ) {
		return CTaskTypes::fetchTaskTypeById( $this->m_intTaskTypeId, $objDatabase );
	}

}
?>