<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPersonAccessLogs
 * Do not add any new functions to this class.
 */
class CPersonAccessLogs extends CBasePersonAccessLogs {

	public static function fetchPersonAccessLogs( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPersonAccessLog', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPersonAccessLog( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPersonAccessLog', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDuplicatePersonAccessLogs( $intPersonId, $intPersonRoleId, $strModuleName, $strActionName, $strDate, $objDatabase ) {

		$strSql = 'SELECT
							*
						FROM
							person_access_logs pal
						WHERE
							pal.person_id = ' . ( int ) $intPersonId . '
							AND pal.person_role_id = ' . ( int ) $intPersonRoleId . '
							AND pal.module LIKE \'' . $strModuleName . '\'
							AND pal.action LIKE \'' . $strActionName . '\'
							AND date_part ( \'year\', pal.month ) = ' . date( 'Y', strtotime( $strDate ) ) . '
							AND date_part ( \'month\', pal.month ) = ' . date( 'm', strtotime( $strDate ) ) . '
							LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimplePersonAccessLogs( $strOrderBy = NULL, $strSortDirection = NULL, $intPageSize = NULL, $intOffset = NULL, $intNumberOfDay, $objPersonLogFilter = NULL, $objDatabase ) {

		$strWhere = ' WHERE pal.hit_count >= 1';

	 	if( false == is_null( $objPersonLogFilter ) ) {

	 		$strPersonName = $objPersonLogFilter->getPersonName();

	 		if( false == is_null( $strPersonName ) ) {

	 			$intCount		= substr_count( $strPersonName, ' ' );
	 			$strSqlOperator = ' AND ';

	 			if( 0 < $intCount ) {
	 				$arrstrNames	= explode( ' ', $strPersonName );
	 				$strFirstName	= $arrstrNames[0];
	 				$strLastName	= $arrstrNames[$intCount];

	 				if( true == empty( $strLastName ) ) {
	 					$strSqlOperator = ' OR ';
	 					$strLastName	= $arrstrNames[0];
	 				}

	 			} else {
	 				$strSqlOperator = ' OR ';
	 				$strFirstName	= $strPersonName;
	 				$strLastName	= $strPersonName;
	 			}

	 			if( true == valStr( $strFirstName ) ) {
	 				$strWhere .= ' AND ( p.name_first ILIKE \'%' . $strFirstName . '%\'';

	 				if( true == valStr( $strLastName ) ) {
	 					$strWhere .= $strSqlOperator . 'p.name_last ILIKE \'%' . $strLastName . '%\'';
	 				}
	 				$strWhere .= ' ) ';
	 			}
	 		}

	 		if( false == is_null( $objPersonLogFilter->getPersonIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objPersonLogFilter->getPersonIds() ) ) {
	 			$strWhere .= ' AND pal.person_id IN ( ' . implode( ', ', $objPersonLogFilter->getPersonIds() ) . ' )';
	 		}

	 		if( false == is_null( $objPersonLogFilter->getPersonRoleIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objPersonLogFilter->getPersonRoleIds() ) ) {
	 			$strWhere .= ' AND pal.person_role_id IN ( ' . implode( ', ', $objPersonLogFilter->getPersonRoleIds() ) . ' )';
	 		}

	 		$strModule = $objPersonLogFilter->getModule();

	 		if( false == is_null( $strModule ) && false == empty( $strModule ) ) {
	 			$strWhere .= ' AND pal.module ILIKE \'%' . $strModule . '%\'';
	 		}

	 		$strAction = $objPersonLogFilter->getAction();

	 		if( false == is_null( $strAction ) && false == empty( $strAction ) ) {
	 			$strWhere .= ' AND pal.action ILIKE \'%' . $strAction . '%\'';
	 		}

	 		$strFromDate = $objPersonLogFilter->getFromMonth();

	 		if( false == is_null( $strFromDate ) && false == empty( $strFromDate ) ) {
	 			$strWhere .= ' AND pal.month >= \'' . $strFromDate . '\'';
	 		}

	 		$strToDate = $objPersonLogFilter->getToMonth();

	 		if( false == is_null( $strToDate ) && false == empty( $strToDate ) ) {
	 			$strWhere .= ' AND pal.month <= \'' . $strToDate . '\'';
	 		}
	 	}

	 	if( true == is_null( $strFromDate ) && true == is_null( $strToDate ) && false == is_null( $intNumberOfDay ) ) {
			$strWhere .= ' AND EXTRACT( month FROM ( pal.month ) ) >= EXTRACT( month FROM ( NOW() - INTERVAL \'' . $intNumberOfDay . ' days\' ) ) ';
		}

		$strSql	= 'SELECT
						pal.*,
						p.name_first || \' \' || p.name_last as person_name,
						p.cid as company,
						pr.name as role_name
					FROM
						person_access_logs pal
						LEFT JOIN person_roles pr ON ( pal.person_role_id = pr.id )
						LEFT JOIN persons p ON ( pal.person_id = p.id )
							' . $strWhere . '
											ORDER BY ' . $strOrderBy . ' ' . $strSortDirection . '
											OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objDatabase );
	}

	public static function loadSimplePersonAccessLogsCount( $intNumberOfDay, $objPersonLogFilter = NULL, $objDatabase ) {

		$strWhere = ' WHERE pal.hit_count >= 1';

		if( false == is_null( $objPersonLogFilter ) ) {

			$strPersonName = $objPersonLogFilter->getPersonName();

			if( false == is_null( $strPersonName ) ) {

				$intCount		= substr_count( $strPersonName, ' ' );
				$strSqlOperator = ' AND ';

				if( 0 < $intCount ) {
					$arrstrNames	= explode( ' ', $strPersonName );
					$strFirstName	= $arrstrNames[0];
					$strLastName	= $arrstrNames[$intCount];

					if( true == empty( $strLastName ) ) {
						$strSqlOperator = ' OR ';
						$strLastName	= $arrstrNames[0];
					}

				} else {
					$strSqlOperator = ' OR ';
					$strFirstName	= $strPersonName;
					$strLastName	= $strPersonName;
				}

				if( true == valStr( $strFirstName ) ) {
	 				$strWhere .= ' AND ( p.name_first ILIKE \'%' . $strFirstName . '%\'';

	 				if( true == valStr( $strLastName ) ) {
	 					$strWhere .= $strSqlOperator . 'p.name_last ILIKE \'%' . $strLastName . '%\'';
	 				}
	 				$strWhere .= ' ) ';
	 			}
			}

			if( false == is_null( $objPersonLogFilter->getPersonIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objPersonLogFilter->getPersonIds() ) ) {
				$strWhere .= 'AND pal.person_id IN ( ' . implode( ', ', $objPersonLogFilter->getPersonIds() ) . ' )';
			}

			if( false == is_null( $objPersonLogFilter->getPersonRoleIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objPersonLogFilter->getPersonRoleIds() ) ) {
				$strWhere .= 'AND pal.person_role_id IN ( ' . implode( ', ', $objPersonLogFilter->getPersonRoleIds() ) . ' )';
			}

			$strModule = $objPersonLogFilter->getModule();

			if( false == is_null( $strModule ) && false == empty( $strModule ) ) {
				$strWhere .= 'AND pal.module ILIKE \'%' . $strModule . '%\'';
			}

			$strAction = $objPersonLogFilter->getAction();

			if( false == is_null( $strAction ) && false == empty( $strAction ) ) {
				$strWhere .= 'AND pal.action ILIKE \'%' . $strAction . '%\'';
			}

			$strFromDate = $objPersonLogFilter->getFromMonth();

			if( false == is_null( $strFromDate ) && false == empty( $strFromDate ) ) {
				$strWhere .= ' AND pal.month >= \'' . $strFromDate . '\'';
			}

			$strToDate = $objPersonLogFilter->getToMonth();

			if( false == is_null( $strToDate ) && false == empty( $strToDate ) ) {
				$strWhere .= ' AND pal.month <= \'' . $strToDate . '\'';
			}

		}

		if( true == is_null( $strFromDate ) && true == is_null( $strToDate ) && false == is_null( $intNumberOfDay ) ) {
			$strWhere .= ' AND EXTRACT( month FROM ( pal.month ) ) >= EXTRACT( month FROM ( NOW() - INTERVAL \'' . $intNumberOfDay . ' days\' ) ) ';
		}

		$strSql = 'SELECT
						count ( pal.* )
					FROM
						person_access_logs pal
						LEFT JOIN person_roles pr ON ( pal.person_role_id = pr.id )
						LEFT JOIN persons p ON ( pal.person_id = p.id )
						' . $strWhere;

		return fetchData( $strSql, $objDatabase );
	}

}
?>