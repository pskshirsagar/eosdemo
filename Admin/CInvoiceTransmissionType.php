<?php

class CInvoiceTransmissionType extends CBaseInvoiceTransmissionType {

	const TRANSMISSIOM_TYPE_MAIL		= 1;
	const TRANSMISSIOM_TYPE_EMAIL		= 2;
	const TRANSMISSIOM_TYPE_FTP 		= 3;
	const TRANSMISSIOM_TYPE_COUPA_CXML	= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>