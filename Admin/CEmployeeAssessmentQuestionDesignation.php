<?php

class CEmployeeAssessmentQuestionDesignation extends CBaseEmployeeAssessmentQuestionDesignation {

	protected $m_strDesignationName;

	/**
	 * Get Functions
	 */

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

	/**
	 * Set Functions
	 */

	public function setDesignationName( $strDesignationName ) {
		$this->m_strDesignationName = $strDesignationName;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['designation_name'] ) ) $this->setDesignationName( $arrValues['designation_name'] );
	}

	/**
	 * Validate Functions
	 */

	public function valDesignationId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDesignationId() ) ) {
			$boolIsValid = $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'designation_ids', 'Please select at least one designation.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid	&= $this->valDesignationId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );

		return parent::update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}
}
?>