<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestionTypes
 * Do not add any new functions to this class.
 */

class CTestQuestionTypes extends CBaseTestQuestionTypes {

	public static function fetchTestQuestionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTestQuestionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTestQuestionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTestQuestionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedQuestionTypes( $objDatabase ) {
		return parent::fetchTestQuestionTypes( 'SELECT * FROM test_question_types WHERE is_published = 1 ORDER BY name', $objDatabase );
	}
}
?>