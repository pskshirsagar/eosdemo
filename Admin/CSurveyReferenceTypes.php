<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyReferenceTypes
 * Do not add any new functions to this class.
 */

class CSurveyReferenceTypes extends CBaseSurveyReferenceTypes {

	public static function fetchSurveyReferenceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSurveyReferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchSurveyReferenceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSurveyReferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>