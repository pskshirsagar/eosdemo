<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationCompanies
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationCompanies extends CBaseEmployeeApplicationCompanies {

	public static function fetchPublishedEmployeeApplicationCompanies( $objDatabase ) {
		return self::fetchEmployeeApplicationCompanies( 'SELECT * FROM employee_application_companies WHERE is_published = true ORDER BY company_name', $objDatabase );
	}

	public static function fetchEmployeeApplicationCompaniesByIds( $arrintCompanyIds, $objDatabase, $boolIsExcludeByIds = false ) {
		if( false == valArr( $arrintCompanyIds ) ) return NULL;
		$strWhereCondition = '';
		$strWhere = ( false == $boolIsExcludeByIds ) ? 'IN' : 'NOT IN';
		$strWhereCondition = ( false == $boolIsExcludeByIds ) ? '' : ' AND is_published = true ';

		$strSql = ' SELECT
						*
					FROM
						employee_application_companies
					WHERE
						id ' . $strWhere . ' ( ' . implode( ',', $arrintCompanyIds ) . ' )
						' . $strWhereCondition . '
					ORDER BY company_name';

		return self::fetchEmployeeApplicationCompanies( $strSql, $objDatabase );
	}

	public static function fetchEmployeeApplicationCompaniesByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		if( true == is_null( $intEmployeeApplicationId ) ) return NULL;

		$strSql = 'SELECT
						eac.*
					FROM
						employee_application_companies eac
						LEFT JOIN employee_application_histories eah ON ( eah.employee_application_company_id = eac.id )
					WHERE eah.employee_application_id =' . ( int ) $intEmployeeApplicationId;

		return parent::fetchObjects( $strSql, 'CEmployeeApplicationCompany', $objDatabase, false );
	}

	public static function fetchPaginatedEmployeeApplicationCompanies( $objCompaniesFilter, $objDatabase, $boolIsPublish = true ) {

		$intOffset	= ( 0 < $objCompaniesFilter->getPageNo() ) ? $objCompaniesFilter->getPageSize() * ( $objCompaniesFilter->getPageNo() - 1 ) : 0;
		$intLimit	= ( int ) $objCompaniesFilter->getPageSize();
		$strOrderBy	= 'eac.company_name';

		if( true == is_numeric( $objCompaniesFilter->getTopCompanies() ) && 0 != $objCompaniesFilter->getTopCompanies() ) {
			$intLimit		= ( $objCompaniesFilter->getPageSize() < ( $objCompaniesFilter->getTopCompanies() - $intOffset ) ) ? ( int ) $objCompaniesFilter->getPageSize() : ( $objCompaniesFilter->getTopCompanies() - $intOffset );
			$strOrderBy		= 'total_hired_employees DESC NULLS LAST';
		}

		$strSubSql		= ( true == $boolIsPublish ) ? 'eac.is_published = true' : ' eac.is_published = false ';
		$strWhere		= self::getSearchCriteria( $objCompaniesFilter );

		if( true == valStr( $strWhere ) ) $strWhere = ' AND ' . $strWhere;

		$strSql = 'SELECT
						eac.*,
						array_length( array_remove( array_agg( DISTINCT CASE
										WHEN ea.employee_id IS NOT NULL AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ' )
										THEN e.id
									END ), NULL ), 1 ) as total_hired_employees,
						array_length( array_remove( array_agg( DISTINCT CASE
										WHEN pjps.id = ' . CPsJobPostingStatus::NEW_STATUS . ' OR ( pjps.id = ' . CPsJobPostingStatus::ON_HOLD . ' AND pjpstep.name = \'' . CPsJobPostingStep::APPLIED . '\' ) AND ea.employee_id IS NULL AND ea.ps_website_job_posting_id IS NOT NULL
										THEN ea.id
									END ), NULL ), 1 ) as total_applied_employees,
						array_length( array_remove( array_agg( DISTINCT CASE
										WHEN ( pjps.id IN ( ' . CPsJobPostingStatus::IN_PROCESS . ', ' . CPsJobPostingStatus::SELECTED . ', ' . CPsJobPostingStatus::REJECTED . ' ) OR ( pjps.id = ' . CPsJobPostingStatus::ON_HOLD . ' AND pjpstep.name <> \'' . CPsJobPostingStep::APPLIED . '\' ) ) AND ea.employee_id IS NULL AND ea.ps_website_job_posting_id IS NOT NULL
										THEN ea.id
									END ), NULL ), 1 ) as total_interviewed_employees,
						array_remove( array_agg ( DISTINCT CASE
										WHEN ea.employee_id IS NOT NULL AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ' )
										THEN e.id
									END ), NULL ) AS hired_employee_application_ids,
						array_remove( array_agg ( DISTINCT CASE
										WHEN ( pjps.id IN ( ' . CPsJobPostingStatus::IN_PROCESS . ', ' . CPsJobPostingStatus::SELECTED . ', ' . CPsJobPostingStatus::REJECTED . ' ) OR ( pjps.id = ' . CPsJobPostingStatus::ON_HOLD . ' AND pjpstep.name <> \'' . CPsJobPostingStep::APPLIED . '\' ) ) AND ea.employee_id IS NULL AND ea.ps_website_job_posting_id IS NOT NULL
										THEN ea.id
									END ), NULL ) AS interviewed_employee_application_ids,
						array_remove( array_agg ( DISTINCT CASE
										WHEN pjps.id = ' . CPsJobPostingStatus::NEW_STATUS . ' OR ( pjps.id = ' . CPsJobPostingStatus::ON_HOLD . ' AND pjpstep.name = \'' . CPsJobPostingStep::APPLIED . '\' ) AND ea.employee_id IS NULL AND ea.ps_website_job_posting_id IS NOT NULL
										THEN ea.id
									END ), NULL ) AS applied_employee_application_ids
					FROM
						employee_application_companies eac
						LEFT JOIN employee_application_histories eah ON ( eac.id = eah.employee_application_company_id )
						LEFT JOIN employee_applications ea ON ( ea.id = eah.employee_application_id )
						LEFT JOIN ps_job_posting_step_statuses pjpss ON ( pjpss.id = ea.ps_job_posting_step_status_id )
						LEFT JOIN ps_job_posting_statuses pjps ON ( pjps.id = pjpss.ps_job_posting_status_id )
						LEFT JOIN ps_job_posting_steps pjpstep ON ( pjpss.ps_job_posting_step_id = pjpstep.id )
						LEFT JOIN employees e ON ( e.id = ea.employee_id )
					WHERE
						' . $strSubSql . $strWhere . '
					GROUP BY
						eac.id
					ORDER BY
						' . $strOrderBy;
		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchEmployeeApplicationCompanies( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeeApplicationCompaniesCount( $objCompaniesFilter, $objDatabase, $boolIsPublish = true ) {

		if( true == is_numeric( $objCompaniesFilter->getTopCompanies() ) ) return $objCompaniesFilter->getTopCompanies();

		$strSubSql = ( true == $boolIsPublish ) ? ' eac.is_published = true' : ' eac.is_published = false ';
		$strWhere = self::getSearchCriteria( $objCompaniesFilter );
		if( true == valStr( $strWhere ) ) $strWhere = ' AND ' . $strWhere;

		$strSql = 'SELECT
						count(*) as count
					FROM
						employee_application_companies eac
					WHERE
						' . $strSubSql . $strWhere;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchQuickSearchSession( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						employee_application_companies eac
					WHERE
						eac.company_name ILIKE \'%' . implode( '%\' AND eac.company_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR eac.city ILIKE \'%' . implode( '%\' AND eac.city  ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						LIMIT 10';

		return fetchData( $strSql, $objDatabase );
	}

	protected static function getSearchCriteria( $objCompaniesFilter ) {

		$strWhereCondition = '';

		if( false == valObj( $objCompaniesFilter, 'CEmployeeApplicationCompanyFilter' ) ) return $strWhereCondition;

		$strWhereCondition .= ( true == valStr( $objCompaniesFilter->getCompanyName() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eac.company_name ILIKE \'%' . $objCompaniesFilter->getCompanyName() . '%\'' : '';
		$strWhereCondition .= ( true == valStr( $objCompaniesFilter->getCity() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eac.city ILIKE \'%' . $objCompaniesFilter->getCity() . '%\'' : '';
		$strWhereCondition .= ( true == valStr( $objCompaniesFilter->getLocation() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eac.location ILIKE \'%' . $objCompaniesFilter->getLocation() . '%\'' : '';
		$strWhereCondition .= ( true == valStr( $objCompaniesFilter->getTotalStrength() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eac.total_strength = \'' . $objCompaniesFilter->getTotalStrength() . '\'': '';
		$strWhereCondition .= ( true == valStr( $objCompaniesFilter->getPhpStrength() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eac.php_strength = \'' . $objCompaniesFilter->getPhpStrength() . '\'' : '';
		$strWhereCondition .= ( true == valStr( $objCompaniesFilter->getQaStrength() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' eac.qa_strength = \'' . $objCompaniesFilter->getQaStrength() . '\'': '';

		return $strWhereCondition;
	}

}
?>