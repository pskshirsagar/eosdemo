<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyRuleTypes
 * Do not add any new functions to this class.
 */

class CSurveyRuleTypes extends CBaseSurveyRuleTypes {

	public static function fetchSurveyRuleTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSurveyRuleType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSurveyRuleType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSurveyRuleType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>