<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplateTypes
 * Do not add any new functions to this class.
 */

class CDocumentTemplateTypes extends CBaseDocumentTemplateTypes {

	public static function fetchDocumentTemplateTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDocumentTemplateType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDocumentTemplateType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDocumentTemplateType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>