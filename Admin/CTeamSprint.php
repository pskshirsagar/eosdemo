<?php

class CTeamSprint extends CBaseTeamSprint {

	const SPRINT_BURN_DOWN_TASK_TYPE = 'sprint_burn_down';

	public static $c_arrstrActions = array(
		'view_planner_tasks',
		'edit_planner_info',
		'search_tasks',
		'insert_or_update_sprint',
		'insert_or_update_planner_task',
		'insert_or_update_stakeholders',
		'update_planner_info',
		'view_stakeholders',
		'update_sprint_title',
		'delete_planner_task',
		'search_task',
		'search_stakeholders',
		'load_planner_tasks',
		'load_users',
		'load_planner_participant',
	);

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPlannerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateComparison() {
		$boolIsValid = true;

		if( strtotime( $this->m_strStartDatetime ) >= strtotime( $this->m_strEndDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_datetime', 'Sprint start Date should be less than Sprint end date.' ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCompleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valDateComparison();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

}
?>