<?php

class CPersonEntityAssociation extends CBasePersonEntityAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPersonId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>