<?php

class CAdStat extends CBaseAdStat {

	public function incrementViewCount( $objDatabase ) {
		$strSql = 'UPDATE ad_stats SET view_count = ( view_count + 1 ) WHERE id = ' . ( int ) $this->getId();
		fetchData( $strSql, $objDatabase );
	}

	public function incrementClickCount( $objDatabase ) {
		$strSql = 'UPDATE ad_stats SET click_count = ( click_count + 1 ) WHERE id = ' . ( int ) $this->getId();
		fetchData( $strSql, $objDatabase );
	}
}
?>