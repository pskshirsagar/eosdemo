<?php

class CEmployeeHour extends CBaseEmployeeHour {

	protected $m_boolShowWarning;
	protected $m_boolOpenEmployeeHour;
	protected $m_boolHalfDayCompensatoryOff;

	protected $m_intOffsetOfListSeconds;
	protected $m_intEndOfListSeconds;

	protected $m_fltTotalDailyHours;
	protected $m_fltTimeDifference;
	protected $m_fltWorkedHours;

	protected $m_strAmountOfTime;
	protected $m_strCurrentDateTime;
	protected $m_strCurrentTotalDailyHours;
	protected $m_strEmployeeName;
	protected $m_boolIsOptional;

	const MIN_REQUIRED_ONFLOOR_HOURS_FOR_FULL_DAY	= '06:00:00';
	const MIN_REQUIRED_ONFLOOR_HOURS_FOR_HALF_DAY	= '04:00:00';
	const EMPLOYEE_ESTIMATED_HOURS 				    = 40;

	public function __construct() {
		parent::__construct();

		$this->m_fltTotalDailyHours 		= 0;
		$this->m_fltTimeDifference  		= 0;
		$this->m_strAmountOfTime			= 0;
		$this->m_boolShowWarning			= 0;
		$this->m_strCurrentDateTime			= date( 'Y/m/d H:i:s' );
		$this->m_intOffsetOfListSeconds		= 300;
		$this->m_intEndOfListSeconds 		= 43200;
		$this->m_strCurrentTotalDailyHours 	= 0;
		$this->m_boolOpenEmployeeHour		= 0;

		return;
	}

	/**
	 * Get Functions
	 */

	public function getTotalDailyEmployeeHours() {
		return $this->m_fltTotalDailyHours;
	}

	public function getTimeDifference() {
		return $this->m_fltTimeDifference;
	}

	public function getAmountOfTime() {
		return $this->m_strAmountOfTime;
	}

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	public function getShowWarning() {
		return $this->m_boolShowWarning;
	}

	public function getCurrentDateTime( $strCountyCode = NULL ) {

		if( CCountry::CODE_INDIA == $strCountyCode ) {
			$strCurrentTimeZone = date_default_timezone_get();

			date_default_timezone_set( 'Asia/Kolkata' );

			$this->m_strCurrentDateTime	= date( 'Y/m/d H:i:s' );

			date_default_timezone_set( $strCurrentTimeZone );
		}

		return $this->m_strCurrentDateTime;
	}

	public function getCurrentTotalDailyHours() {
		return $this->m_strCurrentTotalDailyHours;
	}

	public function getOpenEmployeeHour() {
		return $this->m_boolOpenEmployeeHour;
	}

	public function getBeginDatetime() {
		$strBeginDatetime = parent::getBeginDatetime();
		// This code changes 09/26/2007 to 2007/09/28, if the date is in the previous format.
		\Psi\CStringService::singleton()->preg_match( '/^(\d\d)\/(\d\d)\/(\d\d\d\d)\s+(\d\d):(\d\d):(\d\d).*$/', $strBeginDatetime, $arrmixMatches );
		if( 0 != \Psi\Libraries\UtilFunctions\count( $arrmixMatches ) ) {
			$strBeginDatetime = $arrmixMatches[3] . '/' . $arrmixMatches[1] . '/' . $arrmixMatches[2] . ' ' . $arrmixMatches[4] . ':' . $arrmixMatches[5] . ':' . $arrmixMatches[6];
		}
		return $strBeginDatetime;
	}

	public function getEndDatetime() {
		$strEndDatetime = parent::getEndDatetime();
		// This code changes 09/26/2007 to 2007/09/28, if the date is in the previous format.
		\Psi\CStringService::singleton()->preg_match( '/^(\d\d)\/(\d\d)\/(\d\d\d\d)\s+(\d\d):(\d\d):(\d\d).*$/', $strEndDatetime, $arrmixMatches );
		if( 0 != \Psi\Libraries\UtilFunctions\count( $arrmixMatches ) ) {
			$strEndDatetime = $arrmixMatches[3] . '/' . $arrmixMatches[1] . '/' . $arrmixMatches[2] . ' ' . $arrmixMatches[4] . ':' . $arrmixMatches[5] . ':' . $arrmixMatches[6];
		}
		return $strEndDatetime;
	}

	public function getWorkedHours() {
		return $this->m_fltWorkedHours;
	}

	public function getIsHalfDayCompensatoryOff() {
		return $this->m_boolHalfDayCompensatoryOff;
	}

	public function getIsOptional() {
		return $this->m_boolIsOptional;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['total_daily_hours'] ) ) $this->setTotalDailyEmployeeHours( $arrmixValues['total_daily_hours'] );
		if( true == isset( $arrmixValues['time_difference'] ) ) $this->setTimeDifference( $arrmixValues['time_difference'] );
		if( true == isset( $arrmixValues['worked_hours'] ) ) $this->setWorkedHours( $arrmixValues['worked_hours'] );
		if( true == isset( $arrmixValues['is_half_day_compensatory_off'] ) ) $this->setIsHalfDayCompensatoryOff( $arrmixValues['is_half_day_compensatory_off'] );
		if( true == isset( $arrmixValues['employee_name'] ) ) $this->setEmployeeName( $arrmixValues['employee_name'] );
		if( true == isset( $arrmixValues['is_optional'] ) ) $this->setIsOptional( $arrmixValues['is_optional'] );
		return;
	}

	public function setTotalDailyEmployeeHours( $fltTotalDailyHours ) {
		$this->m_fltTotalDailyHours = $fltTotalDailyHours;
	}

	public function setTimeDifference( $fltTimeDifference ) {
		$this->m_fltTimeDifference = $fltTimeDifference;
	}

	public function setAmountOfTime( $strAmountOfTime ) {
		$this->m_strAmountOfTime = $strAmountOfTime;
	}

	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = $strEmployeeName;
	}

	public function setShowWarning( $boolShowWarning ) {
		$this->m_boolShowWarning = $boolShowWarning;
	}

	public function setCurrentTotalDailyHours( $objDatabase, $intSecondsPassedTillNowFromLastBeginTime, $strCountryCode = NULL, $boolIsFromDashboard = false ) {

		$strCurrentDate = date( 'm/d/Y', strtotime( $this->getCurrentDateTime( $strCountryCode ) ) );

		$intEmployeeHourTypeId = NULL;

		if( true == $boolIsFromDashboard ) {
			$intEmployeeHourTypeId = CEmployeeHourType::FLOOR;
		}

		$strTotalDailyHoursForToday			= CEmployeeHours::fetchTotalDailyEmployeeHoursByDateByEmployeeId( $strCurrentDate, $this->getEmployeeId(), $objDatabase, $intEmployeeHourTypeId );
		$arrstrExplodedDailyHoursForToday 	= explode( ':', $strTotalDailyHoursForToday );

		$intTotalHours = ( true == isset ( $arrstrExplodedDailyHoursForToday[0] ) ) ? $arrstrExplodedDailyHoursForToday[0] : NULL;
		$intTotalMinutes = ( true == isset ( $arrstrExplodedDailyHoursForToday[1] ) ) ? $arrstrExplodedDailyHoursForToday[1] : NULL;
		$intTotalSeconds = ( true == isset ( $arrstrExplodedDailyHoursForToday[2] ) ) ? $arrstrExplodedDailyHoursForToday[2] : NULL;

		$strSecondsForTotalDailyHoursForToday			= $intTotalSeconds + ( $intTotalMinutes * 60 ) + ( $intTotalHours * 3600 );

		// total hours timed in are calculated as sum of total daily hours for today and the time passed from the last begin datetime till now
		$intTotalTimeDifferenceSeconds					= $intSecondsPassedTillNowFromLastBeginTime + $strSecondsForTotalDailyHoursForToday;
		$intTotalTimeDifferenceHours					= 0;
		$intTotalTimeDifferenceMinutes					= 0;

		if( 59 < $intTotalTimeDifferenceSeconds ) {

			$intTotalTimeDifferenceMinutes				= intval( $intTotalTimeDifferenceSeconds / 60 );
			$intTotalTimeDifferenceSeconds				= $intTotalTimeDifferenceSeconds % 60;

			if( 59 < $intTotalTimeDifferenceMinutes ) {
				$intTotalTimeDifferenceHours 			= intval( $intTotalTimeDifferenceMinutes / 60 );
				$intTotalTimeDifferenceMinutes			= $intTotalTimeDifferenceMinutes % 60;
			}
		}

		$this->m_strCurrentTotalDailyHours = ( int ) $intTotalTimeDifferenceHours . ' hours ' . ( int ) $intTotalTimeDifferenceMinutes . ' mins ' . ( int ) $intTotalTimeDifferenceSeconds . ' seconds';
	}

	public function setOpenEmployeeHour( $boolOpenEmployeeHour ) {
		$this->m_boolOpenEmployeeHour = $boolOpenEmployeeHour;
	}

	public function setCurrentDateTime( $strCurrentDateTime ) {
		$this->m_strCurrentDateTime = $strCurrentDateTime;
	}

	public function setWorkedHours( $fltWorkedHours ) {
		$this->m_fltWorkedHours = $fltWorkedHours;
	}

	public function setIsHalfDayCompensatoryOff( $boolHalfDayCompensatoryOff ) {
		$this->m_boolHalfDayCompensatoryOff = $boolHalfDayCompensatoryOff;
	}

	public function setIsOptional( $boolIsOptional ) {
		$this->m_boolIsOptional = $boolIsOptional;
	}

	/**
	 * Validation Functions
	 */

	public function valDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'Start date is required.' ) );
		}

		if( false == is_null( $this->getDate() ) ) {

			$strTodayDate = strtotime( 'now' );
			$strDateEntered = strtotime( $this->getDate() );
			$strDateDiff = $strTodayDate - $strDateEntered;

			if( 0 > $strDateDiff ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', 'The start date should be today\'s date or earlier.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valAmountOfTime() {
		$boolIsValid = true;

		if( 0 == $this->getAmountOfTime() && 1 == $this->m_intIsManuallyAdded ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount_of_time', 'Amount of time is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBeginDatetime( $objDatabase ) {
		$boolIsValid = true;

		$objPayrollPeriod = CPayrollPeriods::fetchLatestPayrollPeriodByCountryCode( CCountry::CODE_USA,  $objDatabase );

		if( ( false == is_null( $objPayrollPeriod ) ) && strtotime( $this->getBeginDatetime() ) < strtotime( $objPayrollPeriod->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_datetime', 'Insertion for past payroll period is not allowed.' ) );
		}

		return $boolIsValid;
	}

	public function valNoOverlappingHours( $objDatabase ) {
		$boolIsValid = true;
		$arrobjEmployeeHours = CEmployeeHours::fetchAllUnbatchedEmployeeHoursByEmployeeId( $this->m_intEmployeeId, $objDatabase );

		if( true == valArr( $arrobjEmployeeHours ) ) {
			foreach( $arrobjEmployeeHours as $objEmployeeHour ) {
				$strBeginDatetime			= strtotime( $this->getBeginDatetime() );
				$strEndDatetime				= strtotime( $this->getEndDatetime() );
				$strUnbatchedBeginDatetime	= strtotime( $objEmployeeHour->getBeginDatetime() );
				$strUnbatchedEndDatetime	= strtotime( $objEmployeeHour->getEndDatetime() );

				if( $this->getId() != $objEmployeeHour->getId() && $strBeginDatetime >= $strUnbatchedBeginDatetime && $strBeginDatetime <= $strUnbatchedEndDatetime ) {
					if( true == $boolIsValid ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'overlapping beginDatetime or endDatetime', 'Overlapping hours. Please delete the conflicting period.' ) );
					}
					$boolIsValid = false;
				} elseif( $this->getId() != $objEmployeeHour->getId() && ( ( ( $strBeginDatetime > $strUnbatchedBeginDatetime ) && ( $strBeginDatetime < $strUnbatchedEndDatetime ) ) || ( ( $strEndDatetime > $strUnbatchedBeginDatetime ) && ( $strEndDatetime < $strUnbatchedEndDatetime ) ) || ( ( $strBeginDatetime < $strUnbatchedBeginDatetime ) && ( $strEndDatetime > $strUnbatchedEndDatetime ) && ( false == is_null( $strUnbatchedEndDatetime ) ) ) ) ) {
					if( true == $boolIsValid ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'overlapping beginDatetime or endDatetime', 'Overlapping hours. Please delete the conflicting period.' ) );
					}
					$boolIsValid = false;
				}
			}
		}
		return $boolIsValid;
	}

	public function valNoOverlappingHoursTesting( $objDatabase ) {
		$boolIsValid			= true;
		$arrobjEmployeeHours	= CEmployeeHours::fetchAllUnbatchedEmployeeHoursByEmployeeId( $this->m_intEmployeeId, $objDatabase );

		if( true == valArr( $arrobjEmployeeHours ) ) {
			$objPsNotification = new CPsNotification();
			foreach( $arrobjEmployeeHours as $objEmployeeHour ) {
				$strBeginDatetime			= strtotime( $this->getBeginDatetime() );
				$strEndDatetime				= strtotime( $this->getEndDatetime() );
				$strUnbatchedBeginDatetime	= strtotime( $objEmployeeHour->getBeginDatetime() );
				$strUnbatchedEndDatetime	= strtotime( $objEmployeeHour->getEndDatetime() );

				$strNotificationMessage = 'Clock in issue !!. <br>Employee ID:' . $this->getEmployeeId() . '<br>BeginDateTime:' . $strBeginDatetime . '<br>EndDateTime:' . $strEndDatetime . '<br>DbBeginDateTime:' . $strUnbatchedBeginDatetime . '<br>DbEndDateTime:' . $strUnbatchedEndDatetime . '<br>Id:' . $this->getId() . '<br>DbId:' . $objEmployeeHour->getId();

				if( $this->getId() != $objEmployeeHour->getId() && $strBeginDatetime >= $strUnbatchedBeginDatetime && $strBeginDatetime <= $strUnbatchedEndDatetime ) {
					$objPsNotification->sendNotifications( CPsNotificationType::NOTIFICATION_TYPE_TASK, $strNotificationMessage, 3495, $objDatabase, NULL, [ 163, 593, 3312, 3474 ] );
				} elseif( $this->getId() != $objEmployeeHour->getId() && ( ( ( $strBeginDatetime > $strUnbatchedBeginDatetime ) && ( $strBeginDatetime < $strUnbatchedEndDatetime ) ) || ( ( $strEndDatetime > $strUnbatchedBeginDatetime ) && ( $strEndDatetime < $strUnbatchedEndDatetime ) ) || ( ( $strBeginDatetime < $strUnbatchedBeginDatetime ) && ( $strEndDatetime > $strUnbatchedEndDatetime ) && ( false == is_null( $strUnbatchedEndDatetime ) ) ) ) ) {
					$objPsNotification->sendNotifications( CPsNotificationType::NOTIFICATION_TYPE_TASK, $strNotificationMessage, 3495, $objDatabase, NULL, [ 163, 593, 3312, 3474 ] );
				}
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $strEmployeeCountryCode = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				// $boolIsValid &= $this->valBeginDatetime( $objDatabase );

				// As we are facing overlapping issue for Alyssa Widdison, Kyle and not able to trace actual issue, Calling separate function to by pass the error.
				if( true == in_array( $this->getEmployeeId(), [ 4888, 5430, 5789, 6847, 7325, 6474 ] ) ) {
					$boolIsValid &= $this->valNoOverlappingHoursTesting( $objDatabase );
				} else {
					$boolIsValid &= $this->valNoOverlappingHours( $objDatabase );
				}

				if( CCountry::CODE_INDIA != $strEmployeeCountryCode ) {
					$boolIsValid &= $this->valDate();
					$boolIsValid &= $this->valAmountOfTime();
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_amount_of_time':
				$boolIsValid &= $this->valAmountOfTime();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function calculateAmountOfTime() {

		for( $intCount = $this->m_intOffsetOfListSeconds; $intCount <= $this->m_intEndOfListSeconds; $intCount = $intCount + $this->m_intOffsetOfListSeconds ) {

			$strMinutes = $intCount / 60;
			$strHours	= floor( $strMinutes / 60 );

			if( 0 == ( $strMinutes % 60 ) ) {
				$strMinutes = '';
			} else {
				$strMinutes = ( $strMinutes % 60 ) . ' mins ';
			}

			if( false == is_null( $strHours ) ) {
				$arrstrAmountOfTime[$intCount] = $strHours . ' hrs ' . $strMinutes;
			} else {
				$arrstrAmountOfTime[$intCount] = $strMinutes;
			}
		}
		return $arrstrAmountOfTime;
	}

}

?>