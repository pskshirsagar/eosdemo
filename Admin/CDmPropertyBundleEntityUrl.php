<?php

class CDmPropertyBundleEntityUrl extends CBaseDmPropertyBundleEntityUrl {

	public function valUrl() {
		$boolIsValid = false;

		if( valStr( $this->getUrl() ) && !CValidation::checkUrl( $this->getUrl(), false ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Please enter valid URL.' ) );
			return $boolIsValid;
		}

		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valUrl();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>