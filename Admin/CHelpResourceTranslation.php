<?php

class CHelpResourceTranslation extends CBaseHelpResourceTranslation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContentTranslationStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLocaleCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTranslatedDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTranslationReturnReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContentVerifiedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContentVerifiedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLayoutVerifiedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLayoutVerifiedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>