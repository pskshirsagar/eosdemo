<?php

class CInvoiceTransmissionDetail extends CBaseInvoiceTransmissionDetail {

	protected $m_intInvoiceStatusTypeId;

	public function setInvoiceStatusTypeId( $intInvoiceStatusTypeId ) {
		$this->m_intInvoiceStatusTypeId = CStrings::strToIntDef( $intInvoiceStatusTypeId, NULL, false );
	}

	public function getInvoiceStatusTypeId() {
		return $this->m_intInvoiceStatusTypeId;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['invoice_status_type_id'] ) ) {
			$this->setInvoiceStatusTypeId( $arrmixValues['invoice_status_type_id'] );
		}
	}

	public function setInvoiceTransmissionDetails( $objInvoice, $intUserId ) {

		$this->setCid( $objInvoice->getCid() );
		$this->setAccountId( $objInvoice->getAccountId() );
		$this->setInvoiceId( $objInvoice->getId() );
		$this->setInvoiceTransmissionTypeId( CInvoiceTransmissionType::TRANSMISSIOM_TYPE_COUPA_CXML );
		$this->setInvoiceSentOn( NULL );
		$this->setInvoiceSentResponse( NULL );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( 'NOW()' );
		$this->setCreatedBy( $intUserId );
		$this->setCreatedBy( 'NOW()' );

		return $this;
	}

}
?>