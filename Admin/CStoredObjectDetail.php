<?php

class CStoredObjectDetail extends CBaseStoredObjectDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTag() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVendor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContainer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContentLength() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEtag() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSharedObject() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAttachments() {
		$boolIsValid	= true;
		$intCount		= 0;

		$arrstrFileExtensions	= [ 'gif', 'png', 'jpg', 'jpeg', 'pdf', 'bmp', 'tif', 'tiff', 'zip', 'csv', 'pdf', 'xlsx', 'xls', 'txt', 'doc', 'docx' ];

		foreach( $_FILES['attachments']['name'] AS $arrmixFile ) {
			$strFileExtension = strtolower( pathinfo( $_FILES['attachments']['name'][$intCount], PATHINFO_EXTENSION ) );
			$intCount++;
			if( false == in_array( $strFileExtension, $arrstrFileExtensions ) ) {
				$boolIsValid	= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Invalid file extension, please select valid file/files.' ) );
				break;
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_attachments':
				$boolIsValid &= $this->valAttachments();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function uploadObject( $boolEncrypt = true ) {
		if( empty( $this->getId() ) || empty( $this->getKey() ) || empty( $this->getVendor() ) || empty( $this->getContainer() ) ) return false;

		$strFileContent = CFileIo::fileGetContents( $this->getTempFileName() );

		if( empty( $strFileContent ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'StorageGateway', 'Invalid file content' ) );
			return false;
		}

		$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( $this->getVendor() );
		$objObjectStorageGateway->initialize();

		if( false == is_null( $this->getKey() ) && false == is_null( $this->getTitle() ) ) {
			$objObjectStorageGateway = $objObjectStorageGateway->putObject( [
				'objectId'  => $this->getId(),
				'container' => $this->getContainer(),
				'key'       => $this->getKey(),
				'data'      => CFileIo::fileGetContents( $this->getTempFileName() ),
				'noEncrypt' => !$boolEncrypt
			] );

			if( true == $objObjectStorageGateway->hasErrors() ) {
				$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'StorageGateway', 'Failed to upload file' ) );
				return false;
			}

			if( true == $boolEncrypt ) {
				return $objObjectStorageGateway;
			}

			return true;
		}

		return false;
	}

	public function downloadObject( $boolReturnPath = false, $boolDisposition = false ) {
		if( empty( $this->getId() ) || empty( $this->getKey() ) || empty( $this->getVendor() ) || empty( $this->getContainer() ) ) return false;

		$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( $this->getVendor() );

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( [
			'objectId'		=> $this->getId(),
			'container'		=> $this->getContainer(),
			'key'			=> $this->getKey(),
			'outputFile'	=> 'temp'
		] );

		$strTempFullPath = $objObjectStorageGatewayResponse['outputFile'];

		if( false == CFileIo::fileExists( $strTempFullPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'File is not present or has been deleted...' ) );
			return false;
		}

		$arrstrFileParts    = pathinfo( $strTempFullPath );
		$strFileName        = $this->getTitle();
		$strMimeType		= '';

		if( false == empty( $arrstrFileParts['extension'] ) ) {
			$strMimeType = CFileExtension::$c_arrmixAllFileExtensions[strtolower( $arrstrFileParts['extension'] )];
		}

		if( true == empty( $strMimeType ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invalid File extension.' ) );
			return false;
		}

		if( true == $boolReturnPath ) {
			return $strTempFullPath;
		}

		$strDisposition = ( false == $boolDisposition ) ? 'attachment' : 'inline';

		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: public' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Length:' . CFileIo::getFileSize( $strTempFullPath ) );
		header( 'Content-Type:' . $strMimeType );
		header( 'Content-Disposition: ' . $strDisposition . '; filename= "' . $strFileName . '"' );
		header( 'Content-Transfer-Encoding:binary' );
		header( 'Accept-Ranges: bytes' );

		if( 'text/html' == $strMimeType )
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';

		echo CFileIo::fileGetContents( $strTempFullPath );
		return true;
	}

	public function deleteObject() {
		if( empty( $this->getId() ) || empty( $this->getKey() ) || empty( $this->getVendor() ) || empty( $this->getContainer() ) ) return false;

		$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( $this->getVendor() );
		$objObjectStorageGateway->initialize();

		$objDeleteObjectResponse = $objObjectStorageGateway->deleteObject( [
			'objectId'        => $this->getId(),
			'container'       => $this->getContainer(),
			'key'             => $this->getKey()
		] );

		if( true == valObj( $objDeleteObjectResponse, 'CRouterObjectStorageGateway ' ) && true == $objDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to remove the file.' ) );
			return false;
		}

		return true;

	}

}
?>