<?php

class CSkillset extends CBaseSkillset {

	public function valDepartments() {
		$boolIsValid = true;
		if( true == is_null( $this->getDepartments() ) || 0 == $this->getDepartments() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'departments', 'Department is required' ) );
		}
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) || '' == $this->getName() || true == is_numeric( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Skill Name is required' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDepartments();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>