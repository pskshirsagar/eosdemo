<?php

class CHonourBadgeType extends CBaseHonourBadgeType {
	const SILVER_HONOUR_BADGE    = 1;
	const GOLDEN_HONOUR_BADGE    = 2;
	const BRONZE_HONOUR_BADGE    = 11;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRatio() {
		$boolIsValid = true;

		if( false == is_null( $this->getRatio() ) && false == preg_match( '/^(\d+)(\.\d)*(:\d+)(\.\d)*$/', $this->getRatio() ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_DELETE:
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRatio();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>