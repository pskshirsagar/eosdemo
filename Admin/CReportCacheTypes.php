<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportCacheTypes
 * Do not add any new functions to this class.
 */

class CReportCacheTypes extends CBaseReportCacheTypes {

	public static function fetchReportCacheTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReportCacheType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchReportCacheType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReportCacheType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllReportCacheTypes( $objDatabase ) {
		$strSql = '	SELECT
						rct.name,
						rct.id
					FROM
						report_cache_types AS rct
					ORDER BY
						rct.id ASC';

		return fetchData( $strSql, $objDatabase );
	}

}
?>