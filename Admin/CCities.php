<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCities
 * Do not add any new functions to this class.
 */

class CCities extends CBaseCities {

	public static function fetchAllCities( $objDatabase, $strCountryCode = NULL, $boolIsPublished = 'true', $arrintExcludeCityIds = NULL ) {

		$strWhereCondition = '';

		if( true == valStr( $strCountryCode ) ) {
			$strWhereCondition = ' AND country_code = \'' . $strCountryCode . '\'';
		}

		if( true == valArr( $arrintExcludeCityIds ) ) {
			$strWhereCondition .= ' AND id NOT IN ( ' . implode( ',', $arrintExcludeCityIds ) . ' )';
		}

		$strSql = ' SELECT * FROM cities WHERE is_published = ' . $boolIsPublished . $strWhereCondition . ' ORDER BY name';
		return self::fetchCities( $strSql, $objDatabase );
	}

}
?>