<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyValueNominationScores
 * Do not add any new functions to this class.
 */

class CCompanyValueNominationScores extends CBaseCompanyValueNominationScores {

	public static function fetchCompanyValueNominationScoresByEmployeeIdByCompanyValueNominationBatchId( $intEmployeeId, $intCompanyValueNominationBatchId, $objDatabase ) {
		if( false == valId( $intEmployeeId ) || false == valId( $intCompanyValueNominationBatchId ) ) {
			return NULL;
		}

		$strSql = '
			SELECT cvns.*
			FROM 
				company_value_nomination_scores cvns
				JOIN company_value_nominations cvn ON cvn.id = cvns.company_value_nomination_id
			WHERE
				cvns.employee_id = ' . ( int ) $intEmployeeId . '
				AND cvn.company_value_nomination_batch_id = ' . ( int ) $intCompanyValueNominationBatchId . ';
		';

		return self::fetchCompanyValueNominationScores( $strSql, $objDatabase );
	}

	public static function fetchCompanyValueNominationScoresByEmployeeIdByCompanyValueNominationIds( $intEmployeeId, $arrintCompanyValueNominationIds, $objDatabase ) {
		if( false == valId( $intEmployeeId ) || false == valIntArr( $arrintCompanyValueNominationIds ) ) {
			return [];
		}

		$strSql = '
			SELECT cvns.*
			FROM
				company_value_nomination_scores cvns
			WHERE
				cvns.employee_id = ' . ( int ) $intEmployeeId . '
				AND cvns.company_value_nomination_id IN ( ' . implode( ',', $arrintCompanyValueNominationIds ) . ' );
		';

		return self::fetchCompanyValueNominationScores( $strSql, $objDatabase );
	}

}
?>