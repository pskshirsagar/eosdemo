<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEntities
 * Do not add any new functions to this class.
 */

class CEntities extends CBaseEntities {

	public static function fetchActiveEntityByUsernameByPassword( $strUsername, $strPassword, $objAdminDatabase ) {
		$strSql = 'SELECT
					*
				   FROM
				   	entities
				   WHERE username = \'' . trim( addslashes( $strUsername ) ) . '\' AND password_encrypted = \'' . trim( addslashes( $strPassword ) ) . '\'';

		return self::fetchEntity( $strSql, $objAdminDatabase );
	}

	public static function fetchActiveEntityByUsername( $strUsername, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM entities WHERE username = \'' . trim( addslashes( $strUsername ) ) . '\'';
		return self::fetchEntity( $strSql, $objAdminDatabase );
	}

	public static function fetchPreExistingEntityByUsername( $strUsername, $objAdminDatabase, $intEntityId = NULL ) {

		$strCondition = '';

		if( false == is_null( $intEntityId ) && true == is_numeric( $intEntityId ) ) {
			$strCondition = ' AND id != ' . ( int ) $intEntityId;
		}

		$strSql = 'SELECT * FROM public.entities WHERE username = \'' . trim( addslashes( $strUsername ) ) . '\'' . $strCondition . ' LIMIT 1';

		return self::fetchEntity( $strSql, $objAdminDatabase );
	}

	public static function fetchEntitiesByIds( $arrintEntityIds, $objAdminDatabase ) {
		if( false == valArr( $arrintEntityIds ) ) return NULL;

		$strSql = 'SELECT * FROM public.entities WHERE id IN ( ' . implode( ',', $arrintEntityIds ) . ' )';

		return self::fetchEntities( $strSql, $objAdminDatabase );
	}

	public static function fetchEntityByUsernameById( $strUsername, $intId, $objAdminDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						entities
					WHERE
						username = \'' . trim( addslashes( $strUsername ) ) . '\'' . '
						AND id =  ' . ( int ) $intId;

		return self::fetchEntity( $strSql, $objAdminDatabase );
	}
}
?>