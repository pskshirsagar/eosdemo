<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COutages
 * Do not add any new functions to this class.
 */

class COutages extends CBaseOutages {

	public static function fetchAllOutagesByTaskId( $intTaskId, $objDatabase ) {

		$strSql = 'SELECT o.*,e.preferred_name
				FROM outages o
				LEFT JOIN users u on u.id = o.completed_by
				LEFT JOIN employees e on e.id = u.employee_id
				WHERE o.task_id = ' . ( int ) $intTaskId . '
				AND o.deleted_by IS NULL ORDER BY o.id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchOutageByTaskId( $intTaskId, $objDatabase ) {
		$strSql = 'SELECT * FROM outages WHERE deleted_by IS NULL AND task_id = ' . ( int ) $intTaskId . ' ORDER BY id LIMIT 1';
		return self::fetchOutage( $strSql, $objDatabase );
	}

	public static function fetchOutagesByTaskIdsByProductId( $arrintTaskIds, $intPsProductId, $objDatabase ) {

		if( false == valArr( $arrintTaskIds ) ) {
			return;
		}
		$strWhere = ' AND o.deleted_by IS NULL';

		if( true == is_numeric( $intPsProductId ) ) {
			$strWhere .= ' AND outage_details->\'ps_products\' ? \'' . $intPsProductId . '\'';
		}
		$strSql = 'SELECT *
				   FROM outages	o
				   WHERE o.task_id IN ( ' . implode( ',', $arrintTaskIds ) . ')' . $strWhere;

		return self::fetchOutages( $strSql, $objDatabase );
	}

	public static function fetchOutagesProductsByTaskIds( $arrintTaskIds, $objDatabase ) {

		if( false == valArr( $arrintTaskIds ) ) {
			return;
		}
		$strSql = 'SELECT o.task_id,
                    sub.pid,
                    pp.name
					FROM outages o
				     INNER JOIN
				     (
				       SELECT task_id,
				              jsonb_array_elements_text(outage_details -> \'ps_products\') AS pid
				       FROM outages o
				       WHERE task_id IN ( ' . implode( ',', $arrintTaskIds ) . ')
				       AND o.deleted_on IS NULL
				       GROUP BY pid,
				                task_id
				     )sub ON ( sub.task_id = o.task_id AND o.deleted_on IS NULL)
				     INNER JOIN  ps_products pp ON ( CAST(sub.pid AS integer) = pp.id)
				     WHERE o.deleted_on IS NULL
				     GROUP BY o.task_id,
				     sub.pid,
				     pp.name
				     ORDER BY o.task_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>