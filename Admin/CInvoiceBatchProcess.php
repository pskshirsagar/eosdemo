<?php

class CInvoiceBatchProcess extends CBaseInvoiceBatchProcess {

	protected $m_strStatus;
	protected $m_fltProgress;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_strNotes;

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function setStatus( $strStatus ) {
		$this->m_strStatus = $strStatus;
	}

	public function getProgress() {
		return $this->m_fltProgress;
	}

	public function setProgress( $fltProgress ) {
		$this->m_fltProgress = $fltProgress;
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->m_strStartDatetime = $strStartDatetime;
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->m_strEndDatetime = $strEndDatetime;
	}

	public function setNotes( $strNotes ) {
		$this->m_strNotes = $strNotes;
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['status'] ) ) $this->setStatus( $arrmixValues['status'] );
		if( isset( $arrmixValues['progress'] ) ) $this->setProgress( $arrmixValues['progress'] );
		if( isset( $arrmixValues['start_datetime'] ) ) $this->setStartDatetime( $arrmixValues['start_datetime'] );
		if( isset( $arrmixValues['end_datetime'] ) ) $this->setEndDatetime( $arrmixValues['end_datetime'] );
		if( isset( $arrmixValues['notes'] ) ) $this->setNotes( $arrmixValues['notes'] );

	}

}
?>