<?php

class CHelpCenterSearchEvent extends CBaseHelpCenterSearchEvent {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSearchDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHttpReferer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHttpUri() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestedModule() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestedAction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSearchStringUri() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSearchStringReferer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>