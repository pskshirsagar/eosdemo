<?php

class CReportComment extends CBaseReportComment {

	const PER_PAGE_LIMIT_OF_REPORT_COMMENTS = 10;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCaReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportComment() {
		$boolIsValid = true;
		if( true == is_null( $this->getReportComment() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_comment', 'Comment is required. ' ) );
			return $boolIsValid;
		} elseif( $this->getReportComment() != strip_tags( $this->getReportComment() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_comment', 'Valid report comment is required. ' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valReportComment();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>