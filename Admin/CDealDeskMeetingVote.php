<?php

class CDealDeskMeetingVote extends CBaseDealDeskMeetingVote {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {

		$boolIsValid = true;
		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is missing. ' ) );
		}
		return $boolIsValid;
	}

	public function valContractId() {

		$boolIsValid = true;
		if( true == is_null( $this->getContractId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_id', 'Contract is missing. ' ) );
		}
		return $boolIsValid;
	}

	public function valDealDeskMeetingId() {

		$boolIsValid = true;
		if( true == is_null( $this->getDealDeskMeetingId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deal_desk_meeting_id', 'Deal Desk meeting is missing. ' ) );
		}
		return $boolIsValid;
	}

	public function valReason() {

		$boolIsValid = true;
		if( false == isset( $this->m_strReason ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_name', 'Reason is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDealDeskMeetingDetailId() {

		$boolIsValid = true;
		if( true == is_null( $this->getDealDeskMeetingDetailId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deal_desk_meeting_detail_id', 'Deal Desk meeting details is missing. ' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valContractId();
				$boolIsValid &= $this->valDealDeskMeetingId();
				$boolIsValid &= $this->valDealDeskMeetingDetailId();
				$boolIsValid &= $this->valReason();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>