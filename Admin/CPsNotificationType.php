<?php

class CPsNotificationType extends CBasePsNotificationType {

	const NOTIFICATION_TYPE_TASK			 			= 1;
	const NOTIFICATION_TYPE_SYSTEM_ERROR				= 2;
	const NOTIFICATION_TYPE_RELEASE 					= 3;
	const NOTIFICATION_TYPE_CHANGE_REPORTING_MANAGER 	= 4;
	const NOTIFICATION_TYPE_DESIGNATION_CHANGE 			= 5;
	const NOTIFICATION_TYPE_TEAM_CHANGE					= 6;
	const NOTIFICATION_TYPE_CUSTOM_NOTIFICATION 		= 7;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
		   		break;

		   	default:
				// no validation avaliable for delete action
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>