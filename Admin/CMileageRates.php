<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMileageRates
 * Do not add any new functions to this class.
 */

class CMileageRates extends CBaseMileageRates {

	public static function fetchMileageRateByDistance( $strCreatedOn, $objDatabase, $intDistance = NULL ) {

		if( true == is_null( $strCreatedOn ) ) return NULL;

		$strSqlWhere = ( true == is_numeric( $intDistance ) )? 'WHERE distance = ' . ( int ) $intDistance . '' : 'WHERE distance IS NULL ';

		return self::fetchMileageRate( 'SELECT * FROM mileage_rates ' . $strSqlWhere . ' AND mileage_start_date <= ( \'' . $strCreatedOn . '\' ) ORDER BY id DESC LIMIT 1;', $objDatabase );
	}

	public static function fetchAllMileageRates( $strCreatedOn, $objDatabase ) {

		if( true == is_null( $strCreatedOn ) ) return NULL;

		$strSql = 'SELECT * FROM mileage_rates WHERE distance IS NOT NULL AND mileage_start_date <= ( \'' . $strCreatedOn . '\' ) ORDER BY distance ASC';
		return self::fetchMileageRates( $strSql, $objDatabase );
	}

}
?>