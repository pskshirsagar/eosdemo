<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplates
 * Do not add any new functions to this class.
 */

class CDocumentTemplates extends CBaseDocumentTemplates {

	public static function fetchDocumentTemplatesByDocumentTypeIdByDocumentSubTypeId( $intDocumentTypeId, $intDocumentSubTypeId, $objDatabase ) {
		$strSql = 'SELECT * FROM document_templates WHERE is_archived <> 1 AND deleted_on IS NULL AND document_type_id = ' . ( int ) $intDocumentTypeId;

		if( true == is_null( $intDocumentSubTypeId ) ) {
			$strSql .= ' AND document_sub_type_id IS NULL';
		} else {
			$strSql .= ' AND document_sub_type_id = ' . ( int ) $intDocumentSubTypeId;
		}

		return self::fetchDocumentTemplates( $strSql, $objDatabase );
	}

}
?>