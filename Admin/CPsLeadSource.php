<?php

class CPsLeadSource extends CBasePsLeadSource {

	const PS_LEAD_TRADE_SHOW 		= 3;
	const OTHER 					= 6;
	const SALES_LEAD_GENERATOR 		= 657;

	protected $m_boolIsSelected;

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

	public function valName( $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}
		if( true == $boolIsValid ) {

			if( true == is_numeric( $this->getName() ) || ( 0 < preg_match( '@[^a-z0-9 ]+@i', $this->getName() ) ) ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name should be alphanumeric.' ) );
			}
		}

		if( true == $boolIsValid && 3 > strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name must be at least 3 characters.' ) );
		}

		if( true == $boolIsValid ) {

			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$intCount = \Psi\Eos\Admin\CPsLeadSources::createService()->fetchPsLeadSourceCount( ' WHERE lower( name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'' . $strSqlCondition, $objAdminDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Lead source already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPsLeadSource( $objDatabase ) {
		$boolIsValid = true;

		$intPsLeadCount = \Psi\Eos\Admin\CPsLeads::createService()->fetchPsLeadsCountByPsLeadSourceId( $this->getId(), $objDatabase );

		if( 0 < ( int ) $intPsLeadCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Lead Source can not be deleted because it is associated with Leads.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
						$boolIsValid &= $this->valName( $objAdminDatabase );
				break;

			case VALIDATE_DELETE:
		   		 	$boolIsValid &= $this->valPsLeadSource( $objAdminDatabase );
			   	break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>