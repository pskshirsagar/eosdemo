<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCertifications
 * Do not add any new functions to this class.
 */

class CCertifications extends CBaseCertifications {

	public static function fetchPaginatedCertifications( $intPageNo, $intPageSize, $objDatabase, $arrmixFilteredExplodedSearch = NULL, $strOrderByField = NULL, $strOrderByType = NULL, $boolIsCount = false, $objCertificateFilter = NULL ) {

		if( true == valArr( $arrmixFilteredExplodedSearch ) ) {
			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) ) {
				$strSeacrhCondition = ' AND ( ct.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR clt.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR dep.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR des.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR c.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' ) ';
			} elseif( 1 < \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) ) {
				if( true == array_key_exists( 2, $arrmixFilteredExplodedSearch ) ) {
					$strSeacrhCondition = ' AND ( ct.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' OR ct.name ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' OR clt.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' OR clt.name ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' OR dep.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' OR dep.name ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' OR des.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' OR des.name ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' OR c.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' OR c.name ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' )';
				} else {
					$strSeacrhCondition = ' AND ( ct.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' OR clt.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' OR dep.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' OR des.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' OR c.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' )';
				}
			}
		} else {
			$strSeacrhCondition = '';
		}

		if( true == valObj( $objCertificateFilter, 'CCertificationFilter' ) ) {

			$arrintCertificationIds		 = $objCertificateFilter->getCertificationIds();
			$arrintDepartmentIds	     = $objCertificateFilter->getDepartmentIds();
			$arrintDesignationIds		 = $objCertificateFilter->getDesignationIds();
			$boolPublished			     = $objCertificateFilter->getPublished();

			if( true == valArr( $arrintCertificationIds ) ) {
				$strWhereClause = ' AND c.id IN( ' . implode( ',', $arrintCertificationIds ) . ' )';
			}
			if( true == valArr( $arrintDepartmentIds ) ) {
				$strWhereClause .= ' AND dep.id IN( ' . implode( ',', $arrintDepartmentIds ) . ' )';
			}
			if( true == valArr( $arrintDesignationIds ) ) {
				$strWhereClause .= ' AND des.id IN( ' . implode( ',', $arrintDesignationIds ) . ' )';
			}
			if( true == $boolPublished ) {
				$strWhereClause .= ' AND c.is_published = 1';
			}
		} else {
			$strWhereClause = '';
		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
						c.*
					FROM
						certifications c
						LEFT JOIN certification_types ct ON ( c.certification_type_id = ct.id )
						LEFT JOIN certification_level_types clt ON ( c.certification_level_type_id = clt.id )
						LEFT JOIN departments dep ON ( c.department_id = dep.id )
						LEFT JOIN designations des ON ( c.designation_id = des.id )
					WHERE
						c.deleted_by IS NULL
						' . $strSeacrhCondition . $strWhereClause;

		if( false == $boolIsCount ) $strSql .= ' ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return parent::fetchCertifications( $strSql, $objDatabase );
	}

	public static function fetchCertificationsByIds( $arrintCertificationIds, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						certifications
					WHERE
						id IN ( ' . implode( ', ', $arrintCertificationIds ) . ' )';

		return parent::fetchCertifications( $strSql, $objDatabase );
	}

	public static function fetchCertificationIdByCertificationTypeIdByCertificationLevelTypeIdByDepartmentIdByDesignationId( $intCertificateTypeId, $intCertificateLevelTypeId, $intDepartmentId, $intDesignationId, $boolIsExternal, $objDatabase, $boolIsChildCertificateLevelTypeId = false, $boolIsCurrentCertificateLevelTypeId = false, $strCertificationName = NULL ) {

		if( false == is_numeric( $intCertificateTypeId ) ) return NULL;

		if( false == $boolIsCurrentCertificateLevelTypeId ) {
			if( true == $boolIsChildCertificateLevelTypeId ) {

				$strCertificateLevelType = ( true == is_numeric( $intCertificateLevelTypeId ) ) ? ' AND c.certification_level_type_id = ( SELECT
								clt.id
							FROM
								certification_level_types clt
								JOIN certifications c1 ON ( clt.id = c1.certification_level_type_id )
							WHERE
								clt.id > ' . $intCertificateLevelTypeId . '
							ORDER BY
								id DESC
							LIMIT
								1 ) ' : ' AND c.certification_level_type_id IS NULL';

			} else {

				$strCertificateLevelType = ( true == is_numeric( $intCertificateLevelTypeId ) ) ? ' AND c.certification_level_type_id = ( SELECT
							clt.id
						FROM
							certification_level_types clt
							JOIN certifications c1 ON ( clt.id = c1.certification_level_type_id )
						WHERE
							clt.id < ' . $intCertificateLevelTypeId . '
						ORDER BY
							id ASC
						LIMIT
							1 ) ' : ' AND c.certification_level_type_id IS NULL';
			}
		} else {
			$strCertificateLevelType = ( true == is_numeric( $intCertificateLevelTypeId ) ) ? ' AND c.certification_level_type_id= ' . $intCertificateLevelTypeId : ' AND c.certification_level_type_id IS NULL';
		}

		$strDepartment = ( true == is_numeric( $intDepartmentId ) ) ? ' AND c.department_id=' . $intDepartmentId : ' AND c.department_id IS NULL';

		$strDesignation = ( true == is_numeric( $intDesignationId ) ) ? ' AND c.designation_id=' . $intDesignationId : ' AND c.designation_id IS NULL';

		$strIsExternal = ( true == $boolIsExternal ) ? ' AND c.is_external=1' : ' AND c.is_external=0';

		$strWhere = ( true == valStr( $strCertificationName ) ) ? ' AND lower( c.name ) = lower( \'' . $strCertificationName . '\' )' : '';

		$strSql = 'SELECT
						c.id As id
					FROM
						certifications c
						LEFT JOIN departments dep ON ( c.department_id = dep.id )
						LEFT JOIN designations des ON ( c.designation_id = des.id )
						LEFT JOIN certification_level_types clt ON ( c.certification_level_type_id = clt.id )
					WHERE
						( c.certification_type_id =' . $intCertificateTypeId . $strDepartment . $strDesignation . $strCertificateLevelType . $strIsExternal . $strWhere .
					' AND c.deleted_by is NULL )
						ORDER BY
						c.id ASC
					LIMIT 1';

		$arrintResponse = fetchData( $strSql, $objDatabase );
		return ( true == isset( $arrintResponse[0]['id'] ) ) ? $arrintResponse[0]['id'] : NULL;

	}

	public static function fetchActiveCertifications( $objDatabase, $boolIsFromDashboard = false ) {

		$strSelectClause	= NULL;
		$strJoinClause		= NULL;
		$strWhereClause		= NULL;

		if( true == $boolIsFromDashboard ) {
			$strJoinClause		= ' LEFT JOIN (
										SELECT
											c1.id as certification_id,
											count ( tg.test_id ) AS test_count
										FROM
											certifications c1
											JOIN test_groups tg ON ( c1.id = tg.certification_id )
										GROUP BY
											c1.id ) as test_counts ON ( test_counts.certification_id = c.id )';
			$strSelectClause	= ' , test_counts.test_count';
			$strWhereClause		= ' AND test_counts.test_count IS NULL';
		}

		$strSql = 'SELECT
					c.id,
					c.is_external,
					ct.id AS certification_type_id,
					ct.name AS certification_type_name,
					clt.id AS certification_level_type_id,
					clt.name AS certification_level_name,
					CASE
						WHEN ( c.is_external = 1 ) THEN c.name || \' | External\'
						WHEN ( c.is_external = 0 ) THEN c.name || \' | Internal\'
					END AS certificate_name ' . $strSelectClause . '
				FROM
					certifications c
					LEFT JOIN certification_types AS ct ON ( c.certification_type_id = ct.id )
					LEFT JOIN certification_level_types AS clt ON ( c.certification_level_type_id = clt.id ) ' . $strJoinClause . '
				WHERE
					c.deleted_by IS NULL
					AND c.is_published = 1 
					AND c.is_external = 1 ' . $strWhereClause . '
				ORDER BY
					ct.order_num,
					clt.order_num';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedCertifications( $objDatabase ) {

		$strSql = ' SELECT
						c.id,
						CASE
							WHEN ( c.is_external = 1 ) THEN c.name || \' | External\'
							WHEN ( c.is_external = 0 ) THEN c.name || \' | Internal\'
						END AS name
					FROM
						certifications c
						LEFT JOIN certification_types AS ct ON ( c.certification_type_id = ct.id )
						LEFT JOIN certification_level_types AS clt ON ( c.certification_level_type_id = clt.id )
					WHERE
						c.deleted_by IS NULL
						AND c.is_published = 1
					ORDER BY
						ct.order_num,
						clt.order_num';

		return self::fetchCertifications( $strSql, $objDatabase );
	}

	public static function fetchAllCertifications( $objDatabase, $boolPurchaseRequest = false ) {
		$strSelect	= 'c.*';
		$strJoin	= NULL;

		if( true == $boolPurchaseRequest ) {
			$strSelect = '	c.id,
							c.name || \' ( \' || ct.name || \' )\' as name,
							c.description';

			$strJoin = ' JOIN certification_types ct ON ( ct.id = c.certification_type_id )';
		}

		$strSql = ' SELECT
						' . $strSelect . '
					FROM
						certifications c ' . $strJoin .
				  ' WHERE c.deleted_by IS NULL';

		return self::fetchCertifications( $strSql, $objDatabase );
	}

	public static function fetchCertificationsByUploadedName( $strLogoPath, $objDatabase ) {
		if( true == is_null( $strLogoPath ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						c.id,
						c.logo_path
					FROM
						certifications c
					WHERE
						c.logo_path = \'' . $strLogoPath . '\'
						AND c.deleted_by IS NULL
						AND c.is_published = 1 ';

		return self::fetchCertifications( $strSql, $objDatabase );
	}

}
?>