<?php

class CTaskImpactType extends CBaseTaskImpactType {
	const NON_IMPACTFUL		= 1;
	const MAJOR				= 2;
	const HIGH				= 3;
	const MEDIUM			= 4;
	const LOW				= 5;

	public static $c_arrstrTaskImpactTypes	= array(
		self::MAJOR			=> 'Major (Highlighted)',
		self::HIGH			=> 'High',
		self::MEDIUM		=> 'Medium',
		self::LOW			=> 'Low (Internal)',
		self::NON_IMPACTFUL	=> 'Non-impactful'
	);

	public static $c_arrintTaskImpactTypesForExternalRelease = array(
		self::MAJOR,
		self::HIGH,
		self::MEDIUM
	);

	public static $c_arrintTaskImpactTypesForAll = array(
		self::MAJOR,
		self::HIGH,
		self::MEDIUM,
		self::LOW
	);

	public static $c_arrintTaskImpactTypesForUAT = [ self::MAJOR, self::HIGH ];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>