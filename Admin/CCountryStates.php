<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCountryStates
 * Do not add any new functions to this class.
 */

class CCountryStates extends CBaseCountryStates {

	public static function fetchStatesByCountryCodes( $arrstrCountryCodes, $objAdminDatabase ) {
		if ( false == valArr( $arrstrCountryCodes ) ) return NULL;
		$strSql = 'SELECT
						*
					FROM
						country_states
					WHERE
						country_code IN ( \'' . implode( '\',\'', $arrstrCountryCodes ) . '\' )
					ORDER BY
						state_code';

				return parent::fetchCountryStates( $strSql, $objAdminDatabase );
	}

	public static function fetchStateCodeById( $intId, $objAdminDatabase ) {
			if( false == is_numeric( $intId ) || true == is_null( $intId ) ) {
				return NULL;
			}

			$strSql = 'SELECT
						state_code
					FROM
						country_states
					WHERE
						id = ' . ( int ) $intId;

			$arrstrCountryCode = fetchData( $strSql, $objAdminDatabase );
			if( true == valArr( $arrstrCountryCode ) ) {
				return $arrstrCountryCode[0]['state_code'];
			}
			return NULL;
	}

	public static function fetchAllStates( $objDatabase, $strOrderBy = NULL ) {
		$strOrderBy = ( true == is_null( $strOrderBy ) ) ? 'name' : $strOrderBy;
		return parent::fetchCountryStates( 'SELECT id, state_code, name, country_code FROM country_states ORDER BY ' . $strOrderBy, $objDatabase );
	}

}
?>