<?php

class CTaskFilter extends CBaseTaskFilter {
	protected $m_strStakeholders;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strIsSaveSearch;
	protected $m_strIsFromTaskFilter;
	protected $m_strIsAdministrator;
	protected $m_strTaskIds;
	protected $m_strTeamIds;
	protected $m_strTaskMediumIds;

	protected $m_intTaskStartReleaseId;
	protected $m_intTaskEndReleaseId;
	protected $m_intIsPublished;
	protected $m_intIsUpcomingFeature;
	protected $m_intTaskReleaseClient;
	protected $m_intIsInternalRelease;
	protected $m_intIsShowStakeHolder;
	protected $m_intLoggedInUserId;
	protected $m_intLoggedInEmployeeId;
	protected $m_intIsShowClientResolutionEmployees;
	protected $m_intAmIDeveloper;
	protected $m_intAmIQa;
	protected $m_intPdmTaskReleaseId;
	protected $m_intTaskId;
	protected $m_intTaskNoteTypeId;
	protected $m_intTaskMediumId;
	protected $m_intProjectManager;

	protected $m_boolShowNullMarketingDescription;
	protected $m_boolIsPurchasedProducts;
	protected $m_boolIsShowKeyClient;
	protected $m_boolIsShowLastClientUpdatedOn;
	protected $m_boolIsSupportTicket;

	protected $m_boolIsSessionSet;

	public function __construct() {

		parent::__construct();
		$this->m_boolIsShowKeyClient			= false;
		$this->m_boolIsShowLastClientUpdatedOn	= false;
		return;
	}

	/**
	 * Get Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		$this->setDepartments( ( true == isset( $arrmixValues['departments'] ) ) ? $this->prepareSetValues( $arrmixValues['departments'] ) : NULL );
		$this->setPsProducts( ( true == isset( $arrmixValues['ps_products'] ) ) ? $this->prepareSetValues( $arrmixValues['ps_products'] ) : NULL );
		$this->setPsProductOptions( ( true == isset( $arrmixValues['ps_product_options'] ) ) ? $this->prepareSetValues( $arrmixValues['ps_product_options'] ) : NULL );
		$this->setTaskReleases( ( true == isset( $arrmixValues['task_releases'] ) ) ? $this->prepareSetValues( $arrmixValues['task_releases'] ) : NULL );
		$this->setPdmTaskReleases( ( true == isset( $arrmixValues['pdm_task_releases'] ) ) ? $this->prepareSetValues( $arrmixValues['pdm_task_releases'] ) : NULL );
		$this->setUsers( ( true == isset( $arrmixValues['users'] ) ) ? $this->prepareSetValues( $arrmixValues['users'] ) : NULL );
		$this->setClients( ( true == isset( $arrmixValues['clients'] ) ) ? $this->prepareSetValues( $arrmixValues['clients'] ) : NULL );
		$this->setCreatedByUsers( ( true == isset( $arrmixValues['created_by_users'] ) ) ? $this->prepareSetValues( $arrmixValues['created_by_users'] ) : NULL );
		$this->setTaskTypes( ( true == isset( $arrmixValues['task_types'] ) ) ? $this->prepareSetValues( $arrmixValues['task_types'] ) : NULL );
		$this->setTaskStatuses( ( true == isset( $arrmixValues['task_statuses'] ) ) ? $this->prepareSetValues( $arrmixValues['task_statuses'] ) : NULL );
		$this->setTaskPriorities( ( true == isset( $arrmixValues['task_priorities'] ) ) ? $this->prepareSetValues( $arrmixValues['task_priorities'] ) : NULL );
		$this->setStakeholders( ( true == isset( $arrmixValues['stakeholders'] ) ) ? $this->prepareSetValues( $arrmixValues['stakeholders'] ) : NULL );
		$this->setProjectManagers( ( true == isset( $arrmixValues['project_managers'] ) ) ? $this->prepareSetValues( $arrmixValues['project_managers'] ) : NULL );
		$this->setClientResolutionEmployees( ( true == isset( $arrmixValues['client_resolution_employees'] ) ) ? $this->prepareSetValues( $arrmixValues['client_resolution_employees'] ) : NULL );
		$this->setProjectManager( ( true == isset( $arrmixValues['project_manager'] ) ) ? $this->prepareSetValues( $arrmixValues['project_manager'] ) : NULL );

		if( true == isset( $arrmixValues['name_first'] ) )							$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_middle'] ) )						$this->setNameMiddle( $arrmixValues['name_middle'] );
		if( true == isset( $arrmixValues['name_last'] ) )							$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['is_save_search'] ) ) 					$this->setIsSaveSearch( $arrmixValues['is_save_search'] );
		if( true == isset( $arrmixValues['is_from_task_filter'] ) )				$this->setIsFromTaskFilter( $arrmixValues['is_from_task_filter'] );
		if( true == isset( $arrmixValues['task_start_release_id'] ) ) 				$this->setTaskStartReleaseId( $arrmixValues['task_start_release_id'] );
		if( true == isset( $arrmixValues['task_end_release_id'] ) ) 				$this->setTaskEndReleaseId( $arrmixValues['task_end_release_id'] );
		if( true == isset( $arrmixValues['is_published'] ) ) 						$this->setIsPublished( $arrmixValues['is_published'] );
		if( true == isset( $arrmixValues['is_release_internal'] ) ) 				$this->setIsInternalRelease( $arrmixValues['is_release_internal'] );
		if( true == isset( $arrmixValues['is_show_key_client'] ) )					$this->setIsShowKeyClient( $arrmixValues['is_show_key_client'] );
		if( true == isset( $arrmixValues['is_show_last_client_updated_on'] ) )		$this->setIsShowLastClientUpdatedOn( $arrmixValues['is_show_last_client_updated_on'] );
		if( true == isset( $arrmixValues['is_show_stake_holder'] ) )		$this->setIsShowStakeHolder( $arrmixValues['show_stake_holder'] );
		if( true == isset( $arrmixValues['task_ids'] ) )					$this->setTaskIds( $arrmixValues['task_ids'] );
		if( true == isset( $arrmixValues['am_i_developer'] ) ) 					$this->setAmIDeveloper( $arrmixValues['am_i_developer'] );
		if( true == isset( $arrmixValues['am_i_qa'] ) ) 							$this->setAmIDeveloper( $arrmixValues['am_i_qa'] );
		if( true == isset( $arrmixValues['team_ids'] ) ) 							$this->setTeamIds( $arrmixValues['team_ids'] );
		if( true == isset( $arrmixValues['pdm_task_release_id'] ) ) 				$this->setPdmTaskReleaseId( $arrmixValues['pdm_task_release_id'] );
		if( true == isset( $arrmixValues['task_id'] ) ) 							$this->setTaskId( $arrmixValues['task_id'] );
		if( true == isset( $arrmixValues['is_support_ticket'] ) )					$this->setIsSupportTicket( $arrmixValues['is_support_ticket'] );
		if( true == isset( $arrmixValues['is_session_set'] ) )						$this->setIsSessionSet( $arrmixValues['is_session_set'] );
		if( true == isset( $arrmixValues['task_note_type_id'] ) )					$this->setTaskNoteTypeId( $arrmixValues['task_note_type_id'] );
		if( true == isset( $arrmixValues['task_medium_id'] ) )						$this->setTaskMediumId( $arrmixValues['task_medium_id'] );
		if( true == isset( $arrmixValues['task_medium_ids'] ) )					$this->setTaskMediumIds( $arrmixValues['task_medium_ids'] );
		return;
	}

	public function getStakeholders() {
		return $this->m_strStakeholders;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getIsSaveSearch() {
		return $this->m_strIsSaveSearch;
	}

 	public function getIsFromTaskFilter() {
		return $this->m_strIsFromTaskFilter;
 	}

	public function getIsAdministrator() {
		return $this->m_strIsAdministrator;
	}

	public function getTaskStartReleaseId() {
		return $this->m_intTaskStartReleaseId;
	}

	public function getTaskEndReleaseId() {
		return $this->m_intTaskEndReleaseId;
	}

	public function getShowNullMarketingDescription() {
		return $this->m_boolShowNullMarketingDescription;
	}

	public function getIsPurchasedProducts() {
		return $this->m_boolIsPurchasedProducts;
	}

	public function getTaskReleaseClient() {
		return $this->m_intTaskReleaseClient;
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function getIsUpcomingFeature() {
		return $this->m_intIsUpcomingFeature;
	}

	public function getIsInternalRelease() {
		return $this->m_intIsInternalRelease;
	}

	public function getIsShowKeyClient() {
		return $this->m_boolIsShowKeyClient;
	}

	public function getIsShowLastClientUpdatedOn() {
		return $this->m_boolIsShowLastClientUpdatedOn;
	}

	public function getIsShowStakeHolder() {
		return $this->m_intIsShowStakeHolder;
	}

	public function getLoggedInUserId() {
		return $this->m_intLoggedInUserId;
	}

	public function getLoggedInEmployeeId() {
		return $this->m_intLoggedInEmployeeId;
	}

	public function getIsShowClientResolutionEmployees() {
		return $this->m_intIsShowClientResolutionEmployees;
	}

	public function getTaskIds() {
		return $this->m_strTaskIds;
	}

	public function getAmIDeveloper() {
		return $this->m_intAmIDeveloper;
	}

	public function getAmIQa() {
		return $this->m_intAmIQa;
	}

	public function getTeamIds() {
		return $this->m_strTeamIds;
	}

	public function getPdmTaskReleaseId() {
		return $this->m_intPdmTaskReleaseId;
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function getIsSupportTicket() {
		return $this->m_boolIsSupportTicket;
	}

	public function getIsSessionSet() {
		return $this->m_boolIsSessionSet;
	}

	public function getTaskNoteTypeId() {
		return $this->m_intTaskNoteTypeId;
	}

	public function getTaskMediumId() {
		return $this->m_intTaskMediumId;
	}

	public function getTaskMediumIds() {
		return $this->m_strTaskMediumIds;
	}

	public function getProjectManager() {
		return $this->m_intProjectManager;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults( $objAdminDatabase = NULL, $boolIsLoadTaskPriorites = true, $boolIsLoadTaskTypes = true, $boolShowCompletedTasks = true ) {

		$arrobjTaskTypes = array();

		if( true == $boolIsLoadTaskPriorites ) {

			$arrobjTaskPriorities		= CTaskPriorities::fetchAllTaskPriorities( $objAdminDatabase );
			$this->m_strTaskPriorities	= ( true == valArr( $arrobjTaskPriorities ) ) ? implode( ',', array_keys( $arrobjTaskPriorities ) ) : '';
		}

		if( true == $boolIsLoadTaskTypes ) {
			$arrobjTaskTypes 				= CTaskTypes::fetchEnabledParentTaskTypes( $objAdminDatabase, $boolIsIncludeSeleniumTaskType = true );
			$this->m_strTaskTypes			= ( true == valArr( $arrobjTaskTypes ) ) ? implode( ',', array_keys( $arrobjTaskTypes ) ) : '';
		}

		if( true == $boolShowCompletedTasks ) {
			$arrintBlockedTaskStatusIds = array( CTaskStatus::CANCELLED );
		} else {
			$arrintBlockedTaskStatusIds = array( CTaskStatus::CANCELLED, CTaskStatus::COMPLETED );
		}

		$arrobjTaskStatuses			= \Psi\Eos\Admin\CTaskStatuses::createService()->fetchTaskStatusesNotInIds( $arrintBlockedTaskStatusIds, $objAdminDatabase );

		if( true == valArr( $arrobjTaskStatuses ) ) {
			$strTaskStatusesIds 			= implode( ',', array_keys( $arrobjTaskStatuses ) );
			$this->m_strTaskStatuses		= $strTaskStatusesIds;
		}

		$arrobjDepartments				= CDepartments::fetchPublishedDepartments( $objAdminDatabase );
		$this->m_strDepartments			= ( true == valArr( $arrobjDepartments ) ) ? implode( ',', array_keys( $arrobjDepartments ) ) : '';
	}

	public function setIsRestored( $boolIsRestored ) {
		$this->m_boolIsRestored = $boolIsRestored;
	}

	public function setStakeholders( $strStakeholders ) {
		$this->m_strStakeholders = $strStakeholders;
	}

	public function setTaskStatusIds( $strTaskStatuses ) {
		$this->m_strTaskStatuses = $strTaskStatuses;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

 	public function setNameMiddle( $strNameMiddle ) {
		$this->m_strNameMiddle = $strNameMiddle;
 	}

 	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
 	}

	public function setIsSaveSearch( $strIsSaveSearch ) {
		$this->m_strIsSaveSearch = $strIsSaveSearch;
	}

	public function setIsFromTaskFilter( $strIsFromTaskFilter ) {
		$this->m_strIsFromTaskFilter = $strIsFromTaskFilter;
	}

 	public function setIsAdministrator( $strIsAdministrator ) {
		$this->m_strIsAdministrator = $strIsAdministrator;
 	}

 	public function setTaskStartReleaseId( $intTaskStartReleaseId ) {
		$this->m_intTaskStartReleaseId = $intTaskStartReleaseId;
 	}

 	public function setTaskEndReleaseId( $intTaskEndReleaseId ) {
		$this->m_intTaskEndReleaseId = $intTaskEndReleaseId;
 	}

	public function setShowNullMarketingDescription( $boolShowNullMarketingDescription ) {
		$this->m_boolShowNullMarketingDescription = $boolShowNullMarketingDescription;
	}

	public function setIsPurchasedProducts( $boolIsPurchasedProducts ) {
		$this->m_boolIsPurchasedProducts = $boolIsPurchasedProducts;
	}

	public function setTaskReleaseClient( $intTaskReleaseClient ) {
		$this->m_intTaskReleaseClient = $intTaskReleaseClient;
	}

	public function setIsPublished( $intIsPublished ) {
		$this->m_intIsPublished = $intIsPublished;
	}

	public function setIsUpcomingFeature( $intIsUpcomingFeature ) {
		$this->m_intIsUpcomingFeature = $intIsUpcomingFeature;
	}

	public function setIsInternalRelease( $intIsInternalRelease ) {
		$this->m_intIsInternalRelease = $intIsInternalRelease;
	}

	public function setIsShowKeyClient( $boolIsShowKeyClient = false ) {
		$this->m_boolIsShowKeyClient = $boolIsShowKeyClient;
	}

	public function setIsShowLastClientUpdatedOn( $boolIsShowLastClientUpdatedOn = false ) {
		$this->m_boolIsShowLastClientUpdatedOn = $boolIsShowLastClientUpdatedOn;
	}

	public function setIsShowStakeHolder( $intIsShowStakeHolder = 0 ) {
		$this->m_intIsShowStakeHolder = $intIsShowStakeHolder;
	}

	public function setLoggedInUserId( $intLoggedInUserId ) {
		$this->m_intLoggedInUserId = $intLoggedInUserId;
	}

	public function setLoggedInEmployeeId( $intLoggedInEmployeeId ) {
		$this->m_intLoggedInEmployeeId = $intLoggedInEmployeeId;
	}

	public function setIsShowClientResolutionEmployees( $intIsShowClientResolutionEmployees ) {
		$this->m_intIsShowClientResolutionEmployees = $intIsShowClientResolutionEmployees;
	}

	public function setTaskIds( $strTaskIds ) {
		$this->m_strTaskIds = $strTaskIds;
	}

	public function setAmIDeveloper( $intAmIDeveloper ) {
		$this->m_intAmIDeveloper = $intAmIDeveloper;
	}

	public function setAmIQa( $intAmIQa ) {
		$this->m_intAmIQa = $intAmIQa;
	}

	public function setTeamIds( $strTeamIds ) {
		$this->m_strTeamIds = $strTeamIds;
	}

	public function setPdmTaskReleaseId( $intPdmTaskReleaseId ) {
		$this->m_intPdmTaskReleaseId = $intPdmTaskReleaseId;
	}

	public function setTaskId( $intTaskId ) {
		$this->m_intTaskId = $intTaskId;
	}

	public function setIsSupportTicket( $boolIsSupportTicket ) {
		$this->m_boolIsSupportTicket = $boolIsSupportTicket;
	}

	public function setIsSessionSet( $boolIsSessionSet ) {
		$this->m_boolIsSessionSet = $boolIsSessionSet;
	}

	public function setTaskNoteTypeId( $intTaskNoteTypeId ) {
		$this->m_intTaskNoteTypeId = $intTaskNoteTypeId;
	}

	public function setTaskMediumId( $intTaskMediumId ) {
		$this->m_intTaskMediumId = $intTaskMediumId;
	}

	public function setTaskMediumIds( $strTaskMediumIds ) {
		$this->m_strTaskMediumIds = $strTaskMediumIds;
	}

	public function setProjectManager( $intProjectManager ) {
		return $this->m_intProjectManager = $intProjectManager;
	}

	/**
	 * Val Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Id is required' ) );
		}

		return $boolIsValid;
	}

	public function valTitle( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Filter name is required.' ) );
		}

		if( true == $boolIsValid ) {

			$strSql = ' WHERE title=' . $this->sqlTitle() . ' ' . ( 0 < $this->getUserId() ? ' AND created_by = ' . $this->getUserId() : '' ) . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );

			$intCount = \Psi\Eos\Admin\CTaskFilters::createService()->fetchRowCount( $strSql, 'task_filters', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Filter name already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valStartAndEndDateSequence( $strStartFieldName, $strEndFieldName, $strStartDate, $strEndDate ) {
		$boolIsValid = true;

		if( false == is_null( $strStartDate ) ) {
			if( false == is_null( $strEndDate ) && strtotime( $strStartDate ) > strtotime( $strEndDate ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strEndFieldName . ' date cannot be earlier than ' . $strStartFieldName . ' date . ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDate( $strFieldName, $strDate ) {
		$boolIsValid = true;

		if( false == is_null( $strDate ) ) {
			if( false == CValidation::validateDate( $strDate ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strFieldName . ' date is not a valid date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTitle( $objDatabase );
				$boolIsValid &= $this->valDate( 'Start', $this->m_strStartDate );
				$boolIsValid &= $this->valDate( 'End', $this->m_strEndDate );
				$boolIsValid &= $this->valDate( 'Start Due', $this->m_strStartDueDate );
				$boolIsValid &= $this->valDate( 'End Due', $this->m_strEndDueDate );
				$boolIsValid &= $this->valStartAndEndDateSequence( 'Start', 'End', $this->m_strStartDate, $this->m_strEndDate );
				$boolIsValid &= $this->valStartAndEndDateSequence( 'Start Due', 'End Due', $this->m_strStartDueDate, $this->m_strEndDueDate );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle( $objDatabase );
				$boolIsValid &= $this->valDate( 'Start', $this->m_strStartDate );
				$boolIsValid &= $this->valDate( 'End', $this->m_strEndDate );
				$boolIsValid &= $this->valDate( 'Start Due', $this->m_strStartDueDate );
				$boolIsValid &= $this->valDate( 'End Due', $this->m_strEndDueDate );
				$boolIsValid &= $this->valStartAndEndDateSequence( 'Start', 'End', $this->m_strStartDate, $this->m_strEndDate );
				$boolIsValid &= $this->valStartAndEndDateSequence( 'Start Due', 'End Due', $this->m_strStartDueDate, $this->m_strEndDueDate );
				break;

			case 'validate_date':
				$boolIsValid &= $this->valDate( 'Start', $this->m_strStartDate );
				$boolIsValid &= $this->valDate( 'End', $this->m_strEndDate );
				$boolIsValid &= $this->valDate( 'Start Due', $this->m_strStartDueDate );
				$boolIsValid &= $this->valDate( 'End Due', $this->m_strEndDueDate );
				$boolIsValid &= $this->valStartAndEndDateSequence( 'Start', 'End', $this->m_strStartDate, $this->m_strEndDate );
				$boolIsValid &= $this->valStartAndEndDateSequence( 'Start Due', 'End Due', $this->m_strStartDueDate, $this->m_strEndDueDate );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

				// Default
			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function prepareSetValues( $arrintValues ) {
		if( true == valArr( $arrintValues ) ) {
			return implode( ',', $arrintValues );
		} else {
			return $arrintValues;
		}
	}

	public function resetTaskStatusesByBlockedTaskStatusIds( $arrintBlockedTaskStatusIds, $objDatabase ) {

		$arrobjTaskStatuses			= \Psi\Eos\Admin\CTaskStatuses::createService()->fetchTaskStatusesNotInIds( $arrintBlockedTaskStatusIds, $objDatabase );
		$strTaskStatusesIds 		= implode( ',', array_keys( $arrobjTaskStatuses ) );
		$this->setTaskStatusIds( $strTaskStatusesIds );
	}

}
?>