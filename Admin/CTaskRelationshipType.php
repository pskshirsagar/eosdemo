<?php

class CTaskRelationshipType extends CBaseTaskRelationshipType {

	const ASSIGNED_TO_ME 	= 1;
	const I_AM_DEVELOPER 	= 2;
	const I_AM_QA		 	= 3;
	const I_AM_SDM			= 4;
	const CREATED_BY_ME		= 5;
	const I_AM_STAKEHOLDER	= 6;
	const MY_TEAM			= 7;
	const I_AM_DESIGNER		= 8;

	public static $c_arrintCommonTaskRelationshipTypes	= array(
		CTaskRelationshipType::ASSIGNED_TO_ME,
		CTaskRelationshipType::CREATED_BY_ME,
		CTaskRelationshipType::I_AM_STAKEHOLDER,
		CTaskRelationshipType::MY_TEAM
	);

	public static $c_arrintHideToDoTaskRelationshipTypes	= array(
		CTaskRelationshipType::I_AM_DESIGNER,
		CTaskRelationshipType::I_AM_DEVELOPER,
		CTaskRelationshipType::I_AM_QA,
		CTaskRelationshipType::I_AM_STAKEHOLDER
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * If you are making any changes in populateSmartyConstants function then
	 * please make sure the same changes would be applied to populateTemplateConstants function also.
	 */

	public static function populateSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'TASK_RELATIONSHIP_TYPE_TYPE_I_AM_STAKEHOLDER',	self::I_AM_STAKEHOLDER );
		$objSmarty->assign( 'TASK_RELATIONSHIP_TYPE_TYPE_I_AM_DEVELOPER',	self::I_AM_DEVELOPER );
		$objSmarty->assign( 'TASK_RELATIONSHIP_TYPE_TYPE_I_AM_QA',			self::I_AM_QA );
		$objSmarty->assign( 'TASK_RELATIONSHIP_TYPE_TYPE_MY_TEAM',			self::MY_TEAM );
		$objSmarty->assign( 'TASK_RELATIONSHIP_TYPE_TYPE_I_AM_DESIGNER',	self::I_AM_DESIGNER );
	}

	public static $c_arrintTemplateConstants = array(
		'TASK_RELATIONSHIP_TYPE_TYPE_I_AM_STAKEHOLDER'	=> self::I_AM_STAKEHOLDER,
		'TASK_RELATIONSHIP_TYPE_TYPE_I_AM_DEVELOPER'	=> self::I_AM_DEVELOPER,
		'TASK_RELATIONSHIP_TYPE_TYPE_I_AM_QA'			=> self::I_AM_QA,
		'TASK_RELATIONSHIP_TYPE_TYPE_MY_TEAM'			=> self::MY_TEAM,
		'TASK_RELATIONSHIP_TYPE_TYPE_I_AM_DESIGNER'		=> self::I_AM_DESIGNER,
	);
}
?>