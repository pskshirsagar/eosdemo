<?php

class CCompanyReport extends CBaseCompanyReport {

	const LIMIT_FOR_REPORT_NAME        = 95;

	/**
	 * Create Functions
	 */

	public function createReportDetail() {

		$objCReportDetail		= new CReportDetail();
		$objCReportDetail->setCompanyReportId( $this->m_intId );

		return $objCReportDetail;
	}

	public function createReportPermission() {

		$objReportPermission		= new CReportPermission();
		$objReportPermission->setCompanyReportId( $this->m_intId );

		return $objReportPermission;
	}

	public function createReportDepartmentAssociation() {

		$objReportDepartmentAssociation		= new CReportDepartmentAssociation();
		$objReportDepartmentAssociation->setCompanyReportId( $this->m_intId );

		return $objReportDepartmentAssociation;
	}

	public function createReportTag() {

		$objReportTag			= new CTagsAssociation();
		$objReportTag->setCompanyReportId( $this->m_intId );

		return $objReportTag;
	}

	public function setCompanyReportSequence( $objAdminDatabase ) {
		if( false == is_numeric( $this->getId() ) ) return NULL;

		$strSql = 'SELECT setval( \'company_reports_id_seq\', ' . ( int ) $this->getId() . ', FALSE )';
		return fetchData( $strSql, $objAdminDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportCacheTypeId( $strTab ) {
		$boolIsValid = true;

		if( false == valId( $this->getReportCacheTypeId() ) && 'sql' == $strTab ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_cache_type_id', 'Please select data refresh frequency. ' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_name', 'Please enter report name. ' ) );
			return $boolIsValid;
		} elseif( $this->getName() != strip_tags( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_name', 'Valid report name is required. ' ) );
			return $boolIsValid;
		}

		if( self::LIMIT_FOR_REPORT_NAME < \Psi\CStringService::singleton()->strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_name', 'Report name must be less than or equal to ' . self::LIMIT_FOR_REPORT_NAME . ' character. ' ) );
			return $boolIsValid;
		}

		if( true == $boolIsValid && true == isset( $objDatabase ) ) {

			$strSql 	= ' WHERE name ILIKE E\'' . trim( addslashes( $this->getName() ) ) . '\' and deleted_by IS NULL';
			if( true == valId( $this->getId() ) ) {
				$strSql .= ' AND id <>' . $this->getId();
			}
			$intCount 	= \Psi\Eos\Admin\CCompanyReports::createService()->fetchRowCount( $strSql, 'company_reports', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' Report name ' . $this->getName() . ' already exist. ' ) );
			}
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_description', 'Please enter report description. ' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $strTab=NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valReportCacheTypeId( $strTab );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>