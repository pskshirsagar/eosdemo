<?php

class CPsLeadStageReason extends CBasePsLeadStageReason {

	protected static $c_arrstrStageReasonName;

	const LOW_UNIT_COUNT				= 1;
	const NON_SELLABLE					= 2;
	const SELF_STORAGE					= 3;
	const HOA							= 4;
	const CONDO							= 5;
	const RENT_CONTROL					= 6;
	const TOWN_HOMES					= 7;
	const SINGLE_FAMILY					= 8;
	const AFFORDABLE					= 9;
	const INTERNATIONAL					= 10;
	const MANUFACTURED_HOMES			= 11;
	const NON_REAL_ESTATE				= 12;
	const SHORT_TERM_RENTALS			= 13;
	const CONSTRUCTION_DEVELOPMENT		= 14;

	public static $c_arrmixStageReasonName = [
		self::LOW_UNIT_COUNT			=> 'Low Unit Count',
		self::NON_SELLABLE				=> 'Non-sellable',
		self::SELF_STORAGE				=> 'Self-Storage',
		self::HOA						=> 'HOA',
		self::CONDO						=> 'Condo',
		self::RENT_CONTROL				=> 'Rent Control',
		self::TOWN_HOMES				=> 'Town Homes',
		self::SINGLE_FAMILY				=> 'Single-Family',
		self::AFFORDABLE				=> 'Affordable',
		self::INTERNATIONAL				=> 'International',
		self::MANUFACTURED_HOMES		=> 'Manufactured Homes',
		self::NON_REAL_ESTATE			=> 'Non-Real Estate',
		self::SHORT_TERM_RENTALS		=> 'Short-Term Rentals',
		self::CONSTRUCTION_DEVELOPMENT	=> 'Construction / Development'
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getPsLeadStageReasonNameById( $intPsLeadStageId ) {
		return self::$c_arrmixStageReasonName[$intPsLeadStageId] ?? 'Invalid Stage Reason id.';
	}

}
?>