<?php

class CWorkbookCategory extends CBaseWorkbookCategory {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWorkbookTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWorkbookStepId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSectionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalColumns() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCategoryDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelperText() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelperLink() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsComment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEditable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>