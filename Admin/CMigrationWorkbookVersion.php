<?php

class CMigrationWorkbookVersion extends CBaseMigrationWorkbookVersion {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMigrationWorkbookId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMigrationWorkbookId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'migration_workbook_id', 'Migration Workbook is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStepName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVersionDataValues() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVersionCustomMapping() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getVersionDataValues( $boolReturnArray = false ) {
		if( false == $boolReturnArray ) {
			return parent::getVersionDataValues();
		}

		if( true == valObj( $this->m_jsonVersionDataValues, 'stdClass' ) ) {
			$this->m_strVersionDataValues = json_encode( $this->m_jsonVersionDataValues );
		}

		return json_decode( $this->m_strVersionDataValues, true );
	}

	public function getVersionCustomMapping( $boolReturnArray = false ) {
		if( false == $boolReturnArray ) {
			return parent::getVersionCustomMapping();
		}

		if( true == valObj( $this->m_jsonVersionCustomMapping, 'stdClass' ) ) {
			$this->m_strVersionCustomMapping = json_encode( $this->m_jsonVersionCustomMapping );
		}

		return json_decode( $this->m_strVersionCustomMapping, true );
	}

}
?>