<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssociations
 * Do not add any new functions to this class.
 */

class CEmployeeAssociations extends CBaseEmployeeAssociations {

	public static function fetchEmployeeAssociationsByDepartmentIdByPsLeadId( $intDepartmentId, $intPsLeadId, $objDatabase ) {

		$strSql = 'SELECT
						ea.*,
						e.name_first as name_first,
						e.name_last as name_last,
						e.preferred_name
					FROM
						employee_associations ea,
						employees e
					WHERE
						ea.employee_id = e.id
						AND ea.department_id = ' . ( int ) $intDepartmentId . '
						AND ea.ps_lead_id = ' . ( int ) $intPsLeadId . '
					ORDER BY association_datetime ASC';

		return self::fetchEmployeeAssociations( $strSql, $objDatabase );
	}

	public static function fetchEmployeeAssociationsCountByDepartmentIdByPsLeadId( $intDepartmentId, $intPsLeadId, $objDatabase ) {

		$strSql = 'SELECT
						count(ea.*)
					FROM
						employee_associations ea,
						employees e
					WHERE
						ea.employee_id = e.id
						AND ea.ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND ea.department_id = ' . ( int ) $intDepartmentId;

		$arrstrData = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrstrData ) )? $arrstrData[0]['count']:0;
	}

	public static function fetchEmployeeAssociationsByDepartmentIdsByPsLeadId( $arrintDepartmentIds, $intPsLeadId, $objDatabase ) {

		$strSql = 'SELECT
						ea.*,
						e.name_first as name_first,
						e.name_last as name_last,
						e.preferred_name
					FROM
						employee_associations ea,
						employees e
					WHERE
						ea.employee_id = e.id
						AND ea.ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND ea.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )
					ORDER BY association_datetime ASC';

		return self::fetchEmployeeAssociations( $strSql, $objDatabase );
	}

	public static function fetchEmployeeAssociationByPsLeadId( $intPsLeadId, $objUser, $objDatabase ) {

		$strWhereCondition = '';
		if( true == is_numeric( $objUser->getEmployee()->getDepartmentId() ) && ( CDepartment::SALES == $objUser->getEmployee()->getDepartmentId() ) ) {
			$strWhereCondition .= ' AND pld.sales_employee_id = ' . $objUser->getEmployeeId() . ' AND ea.department_id IN ( ' . CDepartment::SALES . ') ';
		}

		$strSql = 'SELECT
						ea.*
					FROM
						employee_associations ea
						JOIN employees e ON ( e.id = ea.employee_id )
						JOIN ps_lead_details pld ON ( pld.ps_lead_id = ea.ps_lead_id )
					WHERE
						ea.employee_id = e.id
						AND ea.accepted_by IS NULL
						AND ea.accepted_on IS NULL
						AND ea.ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND ea.employee_id = ' . ( int ) $objUser->getEmployeeId() . $strWhereCondition . '
					ORDER BY association_datetime DESC';

		return self::fetchEmployeeAssociations( $strSql, $objDatabase );

	}

}
?>