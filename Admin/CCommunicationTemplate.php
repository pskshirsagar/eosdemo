<?php

class CCommunicationTemplate extends CBaseCommunicationTemplate {

	protected $m_strGroupName;
	protected $m_strDepartmentName;
	protected $m_strEmployeeNameFull;
	protected $m_strCommunicationTemplateTypeName;

	const MRAB_REJECT										= 4;
	const INTERVIEW_REJECT									= 5;
	const DECLINE_AT_LAST_MOMENT							= 111;
	const IMMEDIATELY_DECLINE								= 112;
	const DEFAULT_US_SIGNATURE 								= 'Default US Signature';
	const INTERVIEW_SCHEDULE_POSTPONE						= 119;
	const CCAT_CLEAR_AND_INTERVIEW_SCHEDULE					= 164;
	const FIRST_ROUND_CLEAR_AND_SECOND_INTERVIEW_SCHEDULE	= 165;
	const SECOND_INTERVIEW_CLEAR_AND_HR_INTERVIEW_SCHEDULE	= 166;
	const IJP_REJECTING_MAIL								= 122;
	const IJP_SELECTION_MAIL								= 121;

	const ASSOCIATE_TECHNICAL_PRODUCT_MANAGER				= 173;
	const BLACKLISTED_CANDIDATE								= 120;
	const CANCELLING_INTERVIEW								= 127;
	const CHANGE_IN_DOJ										= 125;
	const E_INTERVIEW_SCHEDULE_PHP							= 176;
	const E_INTERVIEW_SCHEDULE_SQL							= 175;
	const EMBEDED_SYSTEM_ENGINEER_JOB_DESCRIPTION			= 128;
	const EX_EMPLOYEE_APPLICATION_FEEDBACK					= 116;
	const JOB_DESCRIPTION_PHP_NINJA_1_3_YRS					= 171;
	const JOB_DESCRIPTION_PHP_SAMURAI_3_5_YRS				= 172;
	const MRAB_SELECT										= 3;
	const OFFER_EXTENDED 									= 27;
	const OFFER_LETTER_REVOKED								= 169;
	const ON_HOLD											= 118;
	const POSITION_FILED_BY_OTHER_RESOURCE 					= 163;
	const INTERVIEW_FEEDBACK_REJECT							= 5;


	public static $c_arrintCandidateNotificationTemplateIds = [
		self::INTERVIEW_SCHEDULE_POSTPONE,
		self::CCAT_CLEAR_AND_INTERVIEW_SCHEDULE,
		self::FIRST_ROUND_CLEAR_AND_SECOND_INTERVIEW_SCHEDULE,
		self::SECOND_INTERVIEW_CLEAR_AND_HR_INTERVIEW_SCHEDULE
	];

	public static $c_arrintATSGeneralCommunicationTemplateIds = [
		self::ASSOCIATE_TECHNICAL_PRODUCT_MANAGER,
		self::BLACKLISTED_CANDIDATE,
		self::CANCELLING_INTERVIEW,
		self::CHANGE_IN_DOJ,
		self::E_INTERVIEW_SCHEDULE_PHP,
		self::E_INTERVIEW_SCHEDULE_SQL,
		self::EMBEDED_SYSTEM_ENGINEER_JOB_DESCRIPTION,
		self::EX_EMPLOYEE_APPLICATION_FEEDBACK,
		self::JOB_DESCRIPTION_PHP_NINJA_1_3_YRS,
		self::JOB_DESCRIPTION_PHP_SAMURAI_3_5_YRS,
		self::MRAB_SELECT,
		self::OFFER_EXTENDED,
		self::OFFER_LETTER_REVOKED,
		self::ON_HOLD,
		self::POSITION_FILED_BY_OTHER_RESOURCE,
	];

	Public static $c_arrintATSRejectionCommunicationTemplatesIds = [
		self::INTERVIEW_FEEDBACK_REJECT,
		self::MRAB_REJECT
	];

	public static $c_arrintIJPTemplateIds = [
		self::IJP_SELECTION_MAIL,
		self::IJP_REJECTING_MAIL
	];

	public static $c_arrintATSDeclineCommunicationTemplateIds = [
		self::DECLINE_AT_LAST_MOMENT,
		self::IMMEDIATELY_DECLINE
	];

	/**
	 * Get Functions
	 *
	 */

	public function getEmployeeNameFull() {
		return $this->m_strEmployeeNameFull;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getGroupName() {
		return $this->m_strGroupName;
	}

	public function getCommunicationTemplateTypeName() {
		return $this->m_strCommunicationTemplateTypeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['employee_name_full'] ) ) $this->setEmployeeNameFull( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['employee_name_full'] ) : $arrmixValues['employee_name_full'] );
		if( true == isset( $arrmixValues['dapartment_name'] ) ) $this->setDepartmentName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['dapartment_name'] ) : $arrmixValues['dapartment_name'] );
		if( true == isset( $arrmixValues['group_name'] ) ) $this->setGroupName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['group_name'] ) : $arrmixValues['group_name'] );
		if( true == isset( $arrmixValues['communication_template_type_name'] ) ) $this->setCommunicationTemplateTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['communication_template_type_name'] ) : $arrmixValues['communication_template_type_name'] );
	}

	public function setEmployeeNameFull( $strEmployeeNameFull ) {
		$this->m_strEmployeeNameFull = CStrings::strTrimDef( $strEmployeeNameFull, 240, NULL, true );
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = CStrings::strTrimDef( $strDepartmentName, 240, NULL, true );
	}

	public function setGroupName( $strGroupName ) {
		$this->m_strGroupName = CStrings::strTrimDef( $strGroupName, 240, NULL, true );
	}

	public function setCommunicationTemplateTypeName( $strCommunicationTemplateTypeName ) {
		$this->m_strCommunicationTemplateTypeName = CStrings::strTrimDef( $strCommunicationTemplateTypeName, 240, NULL, true );
	}

	public function valId() {
		$boolValid = true;
		return $boolValid;
	}

	public function valDepartmentId( $objDatabase = NULL ) {
		$boolValid = true;

		if( true == is_null( $this->getDepartmentId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Department is required. ' ) );
		}

		if( true == $boolValid ) {
			$strSubSql = '';
			if( false == is_null( $this->getId() ) ) {
				$strSubSql = ' AND id <> ' . $this->getId();
			}

			$strSql = ' WHERE country_code = \'' . $this->getCountryCode() . '\' AND communication_template_type_id = \'' . $this->getCommunicationTemplateTypeId() . '\' AND department_id = \'' . $this->getDepartmentId() . '\'' . $strSubSql;
			$intCount = \Psi\Eos\Admin\CCommunicationTemplates::createService()->fetchRowCount( $strSql, 'communication_templates', $objDatabase );

			if( 0 < $intCount ) {
				if( CCommunicationTemplateType::EMAIL_SIGNATURE == $this->getCommunicationTemplateTypeId() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Email signature template already exists for selected department.' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'One canned response already exists for selected department.' ) );
				}
				$boolValid = false;
			}
		}

		return $boolValid;
	}

	public function valGroupId( $objDatabase = NULL ) {
		$boolValid = true;

		if( true == is_null( $this->getGroupId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Group is required. ' ) );
		}

		if( true == $boolValid ) {
			$strSubSql = '';
			if( false == is_null( $this->getId() ) ) {
				$strSubSql = ' AND id <> ' . $this->getId();
			}

			$strSql = ' WHERE country_code = \'' . $this->getCountryCode() . '\' AND communication_template_type_id = \'' . $this->getCommunicationTemplateTypeId() . '\' AND group_id = \'' . $this->getGroupId() . '\'' . $strSubSql;
			$intCount = \Psi\Eos\Admin\CCommunicationTemplates::createService()->fetchRowCount( $strSql, 'communication_templates', $objDatabase );

			if( 0 < $intCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Email signature template already exists for selected group.' ) );
				$boolValid = false;
			}
		}
		return $boolValid;

	}

	public function valCommunicationTemplateTypeId() {
		$boolValid = true;
		return $boolValid;
	}

	public function valName( $intCommunicationTemplateTypeId = NULL, $objDatabase = NULL ) {
		$boolValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolValid = false;
			if( CCommunicationTemplateType::TRAINING_AND_DEVELOPMENT == $intCommunicationTemplateTypeId ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Email subject is required. ' ) );
			} elseif( CCommunicationTemplateType::EMAIL_SIGNATURE == $intCommunicationTemplateTypeId ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Signature name is required. ' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Canned response name is required. ' ) );
			}
		}

		if( true == $boolValid && CCommunicationTemplateType::TRAINING_AND_DEVELOPMENT != $intCommunicationTemplateTypeId ) {
			$strSubSql = '';
			if( false == is_null( $this->getId() ) ) {
				$strSubSql = ' AND id <> ' . $this->getId();
			}

			$strSql = ' WHERE name = \'' . addslashes( $this->getName() ) . '\' AND communication_template_type_id = ' . ( int ) $intCommunicationTemplateTypeId . $strSubSql;
			$intCount = \Psi\Eos\Admin\CCommunicationTemplates::createService()->fetchRowCount( $strSql, 'communication_templates', $objDatabase );

			if( 0 < $intCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Canned response name already exists for this template type.' ) );
				$boolValid = false;
			}
		}
		return $boolValid;
	}

	public function valSubject() {
		$boolValid = true;

		if( 0 >= strlen( trim( $this->getSubject(), ' ' ) ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subject', ' Subject is required.' ) );
		}
		return $boolValid;
	}

	public function valContent( $intCommunicationTemplateTypeId = NULL ) {
		$boolValid = true;

		$strContent	= str_replace( '&nbsp;', '', str_replace( '<br />', '', $this->getContent() ) );
		$strContent = strip_tags( html_entity_decode( nl2br( trim( $strContent ) ), ENT_QUOTES ) );

		if( 0 >= strlen( $strContent ) ) {
			$boolValid = false;
			if( CCommunicationTemplateType::EMAIL_SIGNATURE == $intCommunicationTemplateTypeId ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Signature is required. ' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'content', ' Description is required.' ) );
			}
		}

		return $boolValid;

	}

	public function valCountryCode() {
		$boolValid = true;
		return $boolValid;
	}

	public function valIsPublished() {
		$boolValid = true;
		return $boolValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $intCommunicationTemplateTypeId = NULL, $strType = NULL, $objUser = NULL, $boolIsDefaultSignature = false, $boolIsDefaultSignatureExist = false ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolValid &= $this->valName( $intCommunicationTemplateTypeId, $objDatabase );
				if( CCommunicationTemplateType::TRAINING_AND_DEVELOPMENT == $intCommunicationTemplateTypeId ) {
					$boolValid &= $this->valDepartmentId( $objDatabase );
				} elseif( CCommunicationTemplateType::EMAIL_SIGNATURE == $intCommunicationTemplateTypeId ) {
					if( false == is_numeric( $this->getDepartmentId() ) && false == is_numeric( $this->getGroupId() ) && false == $boolIsDefaultSignature ) {
						if( 'Department' == $strType ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'group_id', 'Department is required. ' ) );
						} elseif( 'Group' == $strType ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'group_id', 'Group is required. ' ) );
						} else {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'group_id', 'Type is required. ' ) );
						}

						$boolValid = false;
					} elseif( true == is_numeric( $this->getGroupId() ) && false == $boolIsDefaultSignature ) {
						$boolValid &= $this->valGroupId( $objDatabase );
					} elseif( false == $boolIsDefaultSignature ) {
						$boolValid &= $this->valDepartmentId( $objDatabase );
					}
					if( true == $boolIsDefaultSignature && true == $boolIsDefaultSignatureExist ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Default Signature already exist. ' ) );
					}
				} elseif( CCommunicationTemplateType::APPLICANT_TRACKING_SYSTEM == $intCommunicationTemplateTypeId ) {
					if( CCountry::CODE_INDIA == $objUser->getEmployee()->getCountryCode() ) {
						$boolValid &= $this->valSubject();
					}
				} else {
					$boolValid &= $this->valSubject();
				}

				$boolValid &= $this->valContent( $intCommunicationTemplateTypeId );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolValid = false;
		}

		return $boolValid;
	}

}
?>