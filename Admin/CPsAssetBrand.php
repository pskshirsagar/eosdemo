<?php

class CPsAssetBrand extends CBasePsAssetBrand {

	public function valPsAssetTypeId( $objDatabase, $boolXentoEquipment ) {
		$boolIsValid = true;
		$strTypeName = ( true == $boolXentoEquipment ) ? 'Equipment' : 'Asset';

		if( true == is_null( $this->getPsAssetTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_type_id', $strTypeName . ' type is required.' ) );
			return $boolIsValid;
		}

		$objDisableAssetType = \Psi\Eos\Admin\CPsAssetTypes::createService()->fetchPsAssetTypeById( $this->getPsAssetTypeId(), $objDatabase );

		if( false == is_null( $objDisableAssetType->getDeletedBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_type_id', $strTypeName . ' type is disabled.' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valName( $objDatabase, $boolXentoEquipment ) {

		$boolIsValid = true;
		$strTypeName = ( true == $boolXentoEquipment ) ? 'equipment' : 'asset';

		if( true == ( is_null( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Brand name is required.' ) );
			return $boolIsValid;
		}


		if( ( false == preg_match( '/^[a-zA-Z0-9- _$&+,:;=?@#|\'<>.-^*()%!]{2,50}$/', $this->getName() ) ) || ( true == is_numeric( $this->getName() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'A valid brand name is required.' ) );
			return false;
		}

		if( false == is_null( $this->getPsAssetTypeId() ) ) {
			$intConflictingPsAssetBrandCount = \Psi\Eos\Admin\CPsAssetBrands::createService()->fetchConflictingPsAssetBrandCountByName( $this->getName(), $this->getPsAssetTypeId(), $this->getId(), $objDatabase, $boolIsShowDeletedBrand = true );

			if( 0 < $intConflictingPsAssetBrandCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Brand name with this ' . $strTypeName . ' type is already in use, but it is disabled.' ) );
				return $boolIsValid;
			}

			$intConflictingPsAssetBrandCount = \Psi\Eos\Admin\CPsAssetBrands::createService()->fetchConflictingPsAssetBrandCountByName( $this->getName(), $this->getPsAssetTypeId(), $this->getId(), $objDatabase, $boolIsShowDeletedBrand = false );

			if( 0 < $intConflictingPsAssetBrandCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Brand name exist with selected ' . $strTypeName . ' type.' ) );
				return $boolIsValid;
			}

		}

		if( false == is_null( $this->getId() ) ) {

			$intCount = \Psi\Eos\Admin\CPsAssets::createService()->fetchPsAssetCount( 'WHERE deleted_by IS NULL AND deleted_on IS NULL AND ps_asset_brand_id = ' . $this->getId(), $objDatabase );

			if( 0 != $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Failed to update the brand, it is being associated with ' . $strTypeName . '' ) );
				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase, $boolXentoEquipment ) {

		$boolIsValid = true;
		$strTypeName = ( true == $boolXentoEquipment ) ? 'equipments' : 'assets';

		$intCount = \Psi\Eos\Admin\CPsAssets::createService()->fetchPsAssetCount( 'WHERE deleted_by IS NULL AND deleted_on IS NULL AND ps_asset_brand_id = ' . $this->getId(), $objDatabase );

		if( 0 < $intCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Failed to delete the brand, it is being associated with ' . $strTypeName ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolXentoEquipment = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPsAssetTypeId( $objDatabase, $boolXentoEquipment );
				$boolIsValid &= $this->valName( $objDatabase, $boolXentoEquipment );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase, $boolXentoEquipment );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>