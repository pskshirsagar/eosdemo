<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseChartDetails
 * Do not add any new functions to this class.
 */

class CReleaseChartDetails extends CBaseReleaseChartDetails {

	public static function  fetchReleaseChartDetailsByRelaseReportDetailIdByReleaseChartReportTemplateId( $intReleaseReportDetailId, $strReleaseChartReportTemplateIds = '', $objDatabase ) {

		$strCondition = '';
		if( true == valStr( $strReleaseChartReportTemplateIds ) ) {
			$strCondition = 'AND release_chart_report_template_id IN ( ' . $strReleaseChartReportTemplateIds . ' )';
		}
		$strSql = 'SELECT
						*
					FROM
						release_chart_details
					WHERE
						release_report_detail_id = ' . ( int ) $intReleaseReportDetailId . '
						' . $strCondition;
		$arrobjReleaseChartDetails = self::fetchReleaseChartDetails( $strSql, $objDatabase );
		$arrobjReleaseChartDetails = rekeyObjects( 'ReleaseChartReportTemplateId', $arrobjReleaseChartDetails );

		return $arrobjReleaseChartDetails;
	}

}
?>