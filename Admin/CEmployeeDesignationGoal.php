<?php

class CEmployeeDesignationGoal extends CBaseEmployeeDesignationGoal {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeGoalTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getEmployeeGoalTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to associate employee goal type id value, please try later.' ) );
		}

		return $boolIsValid;
	}

	public function valDesignationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinNumericValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxNumericValue() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getMaxNumericValue() ) || 0 > $this->getMaxNumericValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Please enter valid new goal value.' ) );
		}

		return $boolIsValid;
	}

	public function valMinPercentageValue() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getMinPercentageValue() ) || 0 > $this->getMinPercentageValue() || 100 < $this->getMinPercentageValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Please enter valid new goal value.' ) );
		}

		return $boolIsValid;
	}

	public function valMaxPercentageValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				switch( $this->getEmployeeGoalTypeId() ) {
					case CEmployeeGoalType::QA:
						$boolIsValid &= $this->valMinPercentageValue();
						break;

					case CEmployeeGoalType::OCCURRENCES:
						$boolIsValid &= $this->valMaxNumericValue();
						break;

					case CEmployeeGoalType::ADHERENCE:
						$boolIsValid &= $this->valMinPercentageValue();
						break;

					case CEmployeeGoalType::WRAP_UP:
						$boolIsValid &= $this->valMaxNumericValue();
						break;

					default:
						$boolIsValid &= $this->valEmployeeGoalTypeId();
						break;
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>