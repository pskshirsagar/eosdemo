<?php

use Psi\Eos\Admin\CClients;
use Psi\Eos\Admin\CPersons;
use Psi\Eos\Admin\CContractProducts;
use Psi\Eos\Admin\CStateAreaCodes;
use Psi\Eos\Admin\CPsProductRelationships;
use Psi\Eos\Admin\CContractStatusTypes;
use Psi\Eos\Admin\CContracts;
use Psi\Eos\Admin\CPsLeadCompetitors;
use Psi\Eos\Admin\CPsLeads;

class CPsLead extends CBasePsLead {

	protected $m_objPerson;
	protected $m_objPsLeadDetail;

	protected $m_arrobjWebsiteProductRequests;
	protected $m_arrobjContracts;
	protected $m_arrobjPersons;

	protected $m_strLastContactedOn;
	protected $m_strPsLeadSourceName;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strTitle;
	protected $m_strPhoneNumber;
	protected $m_strPhoneNumber1;
	protected $m_strPhoneNumber2;
	protected $m_strPhoneNumber3;
	protected $m_strEmailAddress;
	protected $m_strAddressLine1;
	protected $m_strAddressLine2;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strReason;
	protected $m_strShoeSize;
	protected $m_strShirtSize;

	protected $m_intMaxContractStatusTypeId;
	protected $m_intImportActionTypeId;
	protected $m_intMergePsLeadId;
	protected $m_intCompanyStatusTypeId;
	protected $m_intAttachmentsCount;
	protected $m_intPhoneNumber1TypeId;
	protected $m_intPhoneNumber2TypeId;
	protected $m_intPhoneNumber3TypeId;
	protected $m_intContractId;
	protected $m_intDupeCount;
	protected $m_intContractStatusTypeId;
	protected $m_intIgnoredBy;
	protected $m_intMergeLeadIntoId;
	protected $m_intMargeLeadIndex;
	protected $m_intMargeLeadIntoIndex;
	protected $m_intPsLeadDuplicateTypeId;
	protected $m_intCompetitorId;
	protected $m_intCid;
	protected $m_intSalesEmployeeId;
	protected $m_intAssignedEmployeeId;
	protected $m_intPersonId;
	protected $m_strPsLeadTypeId;
	protected $m_strPsLeadPropertyTypes;

	const ID_DB_XENTO_PVT_LTD				= 216770;
	const ID_RESIDENT_VERIFY_DEMO_CO_2013	= 212290;
	const ID_DEV_TEST_COMPANY				= 217345;
	const ID_PRIMARY_SALES_DEMO				= 217616;

	public static $c_arrintAWSS3TestClientPsLeadIds = array(
		'DB_XENTO_PVT_LTD'			=> self::ID_DB_XENTO_PVT_LTD,
		'Dev_Test_Company'			=> self::ID_DEV_TEST_COMPANY
	);

	public function __construct() {
		parent::__construct();

		$this->m_arrobjWebsiteProductRequests = array();
		$this->m_arrobjPersons = array();
		$this->m_arrobjContracts = array();
		$this->m_intAttachmentsCount = 0;

		return;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getAttachmentsCount() {
		return $this->m_intAttachmentsCount;
	}

	public function getFormattedMobilePhoneNumber() {
		return preg_replace( '/[^0-9xX ]/', '', $this->m_strPhoneNumber2 );
	}

	public function getFormattedBusinessPhoneNumber() {
		return preg_replace( '/[^0-9xX ]/', '', $this->m_strPhoneNumber1 );
	}

	public function getPsLeadDetail() {
		return $this->m_objPsLeadDetail;
	}

	public function getOrFetchPsLeadDetail( $objAdminDatabase ) {
		if( NULL == $this->m_objPsLeadDetail ) {
			$this->m_objPsLeadDetail = $this->fetchPsLeadDetail( $objAdminDatabase );
		}

		return $this->m_objPsLeadDetail;
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function getPersonId() {
		return $this->m_intPersonId;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createPerson() {

		$objPerson = new CPerson();

		$objPerson->setCompanyName( $this->getCompanyName() );
		$objPerson->setNameFirst( $this->getNameFirst() );
		$objPerson->setNameLast( $this->getNameLast() );
		$objPerson->setTitle( $this->getTitle() );
		$objPerson->setEmailAddress( $this->getEmailAddress() );
		$objPerson->setAddressLine1( $this->getAddressLine1() );
		$objPerson->setAddressLine2( $this->getAddressLine2() );
		$objPerson->setStateCode( $this->getStateCode() );
		$objPerson->setCity( $this->getCity() );
		$objPerson->setPostalCode( $this->getPostalCode() );
		$objPerson->setIsDisabled( 0 );
		$objPerson->setPsLeadId( $this->getId() );

		return $objPerson;
	}

	public function createPsLeadDetail() {

		$objPsLeadDetail = new CPsLeadDetail();
		$objPsLeadDetail->setPsLeadId( $this->getId() );

		return $objPsLeadDetail;
	}

	public function createAction() {

		$objAction = new CAction();
		$objAction->setPsLeadId( $this->getId() );
		$objAction->setActionDatetime( date( 'm/d/Y H:i:s' ) );
		$objAction->setActionTypeId( CActionType::CALL );

		return $objAction;
	}

	public function createContract( $objAdminDatabase = NULL ) {

		$objClient = NULL;
		$intContactedContractsCount = 0;

		if( true == valObj( $objAdminDatabase, 'CDatabase' ) && valId( $this->getId() ) ) {
			$intContactedContractsCount = CContracts::createService()->fetchContactedContractsCountByPsLeadId( $this->getId(), $objAdminDatabase );
			$objClient = CClients::createService()->fetchClientByPsLeadId( $this->getId(), $objAdminDatabase );
		}

		if( 0 < $intContactedContractsCount ) {
			$intContractStatusTypeId = CContractStatusType::CONTACTED;
		} else {
			$intContractStatusTypeId = CContractStatusType::UNUSED;
		}

		$objContract = new CContract();
		$objContract->setPsLeadId( $this->getId() );
		$objContract->setContractStatusTypeId( $intContractStatusTypeId );
		$objContract->setContractDatetime( 'NOW()' );
		$objContract->setContractTypeId( CContractType::STANDARD );
		$objContract->setCid( $this->getCid() );

		if( valObj( $objClient, 'CClient' ) && valStr( $objClient->getCurrencyCode() ) ) {
			$objContract->setCurrencyCode( $objClient->getCurrencyCode() );
		}

		return $objContract;
	}

	public function createContractWatchlist() {

		$objContractWatchlist = new CContractWatchlist();
		$objContractWatchlist->setPsLeadId( $this->getId() );

		return $objContractWatchlist;
	}

	public function createPsLeadCompetitor() {

		$objPsLeadCompetitor = new CPsLeadCompetitor();
		$objPsLeadCompetitor->setPsLeadId( $this->getId() );

		return $objPsLeadCompetitor;
	}

	public function createPsLeadCompetitorProperty() {
		$objPsLeadCompetitorProperty = new CPsLeadCompetitorProperty();
		$objPsLeadCompetitorProperty->setPsLeadId( $this->getId() );

		return $objPsLeadCompetitorProperty;
	}

	public function createPsLeadPropertyType( $intPropertyTypeId = NULL ) {

		$objPsLeadPropertyType	= new CPsLeadPropertyType();
		$objPsLeadPropertyType->setPsLeadId( $this->getId() );
		$objPsLeadPropertyType->setPropertyTypeId( $intPropertyTypeId );

		return $objPsLeadPropertyType;
	}

	public function createPsLeadProduct() {

		$objPsLeadProduct = new CPsLeadProduct();
		$objPsLeadProduct->setPsLeadId( $this->getId() );

		return $objPsLeadProduct;
	}

	public function createPsLeadProperty() {

		$objPsLeadProperty = new CPsLeadProperty();
		$objPsLeadProperty->setPsLeadId( $this->getId() );

		return $objPsLeadProperty;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getCompetitorId() {
		return $this->m_intCompetitorId;
	}

	public function getContracts() {
		return $this->m_arrobjContracts;
	}

	public function getPsWebsiteProductRequests() {
		return $this->m_arrobjWebsiteProductRequests;
	}

	public function getAgreeToTerms() {
		return $this->m_intAgreeToTerms;
	}

	public function getMaxContractStatusTypeId() {
		return $this->m_intMaxContractStatusTypeId;
	}

	public function getLastContactedOn() {
		return $this->m_strLastContactedOn;
	}

	public function getImportActionTypeId() {
		return $this->m_intImportActionTypeId;
	}

	public function getMergePsLeadId() {
		return $this->m_intMergePsLeadId;
	}

	public function getCompanyStatusTypeId() {
		return $this->m_intCompanyStatusTypeId;
	}

	public function getPersons() {
		return $this->m_arrobjPersons;
	}

	public function getCondensedRequestDatetime() {
		return date( 'm/d/y', strtotime( $this->getRequestDatetime() ) );
	}

	public function getPsLeadSourceName() {
		return $this->m_strPsLeadSourceName;
	}

	public function getPerson() {
		return $this->m_objPerson;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getPhoneNumber1TypeId() {
		return $this->m_intPhoneNumber1TypeId;
	}

	public function getPhoneNumber2TypeId() {
		return $this->m_intPhoneNumber2TypeId;
	}

	public function getPhoneNumber3TypeId() {
		return $this->m_intPhoneNumber3TypeId;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getPhoneNumber1() {
		return $this->m_strPhoneNumber1;
	}

	public function getPhoneNumber2() {
		return $this->m_strPhoneNumber2;
	}

	public function getPhoneNumber3() {
		return $this->m_strPhoneNumber3;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getAddressLine1() {
		return $this->m_strAddressLine1;
	}

	public function getAddressLine2() {
		return $this->m_strAddressLine2;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getReason() {
		return $this->m_strReason;
	}

	public function getPsLeadDuplicateTypeId() {
		return $this->m_intPsLeadDuplicateTypeId;
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function getDupeCount() {
		return $this->m_intDupeCount;
	}

	public function getContractStatusTypeId() {
		return $this->m_intContractStatusTypeId;
	}

	public function getIgnoredBy() {
		return $this->m_intIgnoredBy;
	}

	public function getMergeLeadIntoId() {
		return $this->m_intMergeLeadIntoId;
	}

	public function getMargeLeadIndex() {
		return $this->m_intMargeLeadIndex;
	}

	public function getMargeLeadIntoIndex() {
		return $this->m_intMargeLeadIntoIndex;
	}

	public function getShoeSize() {
		return $this->m_strShoeSize;
	}

	public function getSalesEmployeeId() {
		return $this->m_intSalesEmployeeId;
	}

	public function getAssignedEmployeeId() {
		return $this->m_intAssignedEmployeeId;
	}

	public function getPsLeadTypeId() {
		return $this->m_strPsLeadTypeId;
	}

	public function getPsLeadPropertyTypes() {
		return $this->m_strPsLeadPropertyTypes;
	}

	public function getPhoneNumberExtension() {
		return $this->m_strPhoneNumberExtension;
	}

	public function getShirtSize() {
		return $this->m_strShirtSize;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setCompetitorId( $intCompetitorId ) {
		$this->m_intCompetitorId = $intCompetitorId;
	}

	public function setPsLeadDetail( $objPsLeadDetail ) {
		$this->m_objPsLeadDetail = $objPsLeadDetail;
	}

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setAttachmentsCount( $intAttachmentsCount ) {
		$this->m_intAttachmentsCount = $intAttachmentsCount;
	}

	public function setContracts( $arrobjContracts ) {
		$this->m_arrobjContracts = $arrobjContracts;
	}

	public function setAgreeToTerms( $intAgreeToTerms ) {
		$this->m_intAgreeToTerms = $intAgreeToTerms;
	}

	public function setMaxContractStatusTypeId( $intMaxContractStatusTypeId ) {
		$this->m_intMaxContractStatusTypeId = $intMaxContractStatusTypeId;
	}

	public function setLastContactedOn( $strLastContactedOn ) {
		$this->m_strLastContactedOn = $strLastContactedOn;
	}

	public function setImportActionTypeId( $intImportActionTypeId ) {
		$this->m_intImportActionTypeId = $intImportActionTypeId;
	}

	public function setMergePsLeadId( $intMergePsLeadId ) {
		$this->m_intMergePsLeadId = $intMergePsLeadId;
	}

	public function setCompanyStatusTypeId( $intCompanyStatusTypeId ) {
		$this->m_intCompanyStatusTypeId = $intCompanyStatusTypeId;
	}

	public function setStateCode( $strStateCode ) {
		$arrobjStates = \Psi\Eos\Admin\CStates::createService()->getStatesArrayKeyedByStateCode();

		if( true == array_key_exists( trim( \Psi\CStringService::singleton()->strtoupper( $strStateCode ) ), $arrobjStates ) ) {
			$this->m_strStateCode = trim( trim( \Psi\CStringService::singleton()->strtoupper( $strStateCode ) ) );
		}
	}

	public function setPsLeadSourceName( $strPsLeadSourceName ) {
		$this->m_strPsLeadSourceName = $strPsLeadSourceName;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = trim( $strNameFirst );
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = trim( $strNameLast );
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setPhoneNumber1TypeId( $intPhoneNumber1TypeId ) {
		$this->m_intPhoneNumber1TypeId = $intPhoneNumber1TypeId;
	}

	public function setPhoneNumber2TypeId( $intPhoneNumber2TypeId ) {
		$this->m_intPhoneNumber2TypeId = $intPhoneNumber2TypeId;
	}

	public function setPhoneNumber3TypeId( $intPhoneNumber3TypeId ) {
		$this->m_intPhoneNumber3TypeId = $intPhoneNumber3TypeId;
	}

	public function setPersonId( $intPersonId ) {
		$this->m_intPersonId = trim( $intPersonId );
	}

	public function setPhoneNumber1( $strPhoneNumber1 ) {
		$this->m_strPhoneNumber1 = trim( $strPhoneNumber1 );
	}

	public function setPhoneNumber2( $strPhoneNumber2 ) {
		$this->m_strPhoneNumber2 = $strPhoneNumber2;
	}

	public function setPhoneNumber3( $strPhoneNumber3 ) {
		$this->m_strPhoneNumber3 = $strPhoneNumber3;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = trim( $strEmailAddress );
	}

	public function setAddressLine1( $strAddressLine1 ) {
		$this->m_strAddressLine1 = $strAddressLine1;
	}

	public function setAddressLine2( $strAddressLine2 ) {
		$this->m_strAddressLine2 = $strAddressLine2;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = $strCity;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function setReason( $strReason ) {
		$this->m_strReason = $strReason;
	}

	public function setPsLeadDuplicateTypeId( $intPsLeadDuplicateTypeId ) {
		$this->m_intPsLeadDuplicateTypeId = $intPsLeadDuplicateTypeId;
	}

	public function setContractId( $intContractId ) {
		$this->m_intContractId = $intContractId;
	}

	public function setDupeCount( $intDupeCount ) {
		$this->m_intDupeCount = $intDupeCount;
	}

	public function setContractStatusTypeId( $intContractStatusTypeId ) {
		$this->m_intContractStatusTypeId = $intContractStatusTypeId;
	}

	public function setIgnoredBy( $intIgnoredBy ) {
		$this->m_intIgnoredBy = $intIgnoredBy;
	}

	public function setMergeLeadIntoId( $intMergeLeadIntoId ) {
		$this->m_intMergeLeadIntoId = $intMergeLeadIntoId;
	}

	public function setMargeLeadIndex( $intMargeLeadIndex ) {
		$this->m_intMargeLeadIndex = $intMargeLeadIndex;
	}

	public function setMargeLeadIntoIndex( $intMargeLeadIntoIndex ) {
		$this->m_intMargeLeadIntoIndex = $intMargeLeadIntoIndex;
	}

	public function setShoeSize( $strShoeSize ) {
		$this->m_strShoeSize = $strShoeSize;
	}

	public function setDefaults() {

		$this->setRequestDatetime( date( 'm/d/Y H:i:s' ) );
		$this->setPsLeadOriginId( CPsLeadOrigin::MANUAL );
		$this->setCompanyStatusTypeId( CCompanyStatusType::PROSPECT );
	}

	public function setPerson( $objPerson ) {
		$this->m_objPerson = $objPerson;
	}

	public function setSalesEmployeeId( $intSalesEmployeeId ) {
		$this->m_intSalesEmployeeId = CStrings::strToIntDef( $intSalesEmployeeId, NULL, false );
	}

	public function setAssignedEmployeeId( $intAssignedEmployeeId ) {
		$this->m_intAssignedEmployeeId = CStrings::strToIntDef( $intAssignedEmployeeId, NULL, false );
	}

	public function setPsLeadTypeId( $strPsLeadTypeId ) {
		$this->m_strPsLeadTypeId = $strPsLeadTypeId;
	}

	public function setPsLeadPropertyTypes( $strPsLeadPropertyTypes ) {
		$this->m_strPsLeadPropertyTypes = $strPsLeadPropertyTypes;
	}

	public function setPhoneNumberExtension( $strPhoneNumberExtension ) {
		$this->m_strPhoneNumberExtension = CStrings::strTrimDef( $strPhoneNumberExtension, 30, NULL, true );
	}

	public function setShirtSize( $strShirtSize ) {
		$this->m_strShirtSize = $strShirtSize;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['merge_ps_lead_id'] ) ) $this->setMergePsLeadId( $arrmixValues['merge_ps_lead_id'] );
		if( true == isset( $arrmixValues['import_action_type_id'] ) ) $this->setImportActionTypeId( $arrmixValues['import_action_type_id'] );
		if( true == isset( $arrmixValues['email_address'] ) ) $this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['max_contract_status_types_id'] ) ) $this->setMaxContractStatusTypeId( $arrmixValues['max_contract_status_types_id'] );
		if( true == isset( $arrmixValues['company_status_type_id'] ) ) $this->setCompanyStatusTypeId( $arrmixValues['company_status_type_id'] );
		if( true == isset( $arrmixValues['last_contacted_on'] ) ) $this->setLastContactedOn( $arrmixValues['last_contacted_on'] );
		if( true == isset( $arrmixValues['attachments_count'] ) ) 		$this->setAttachmentsCount( $arrmixValues['attachments_count'] );
		if( true == isset( $arrmixValues['lead_source'] ) )	$this->setPsLeadSourceName( $arrmixValues['lead_source'] );

		if( true == isset( $arrmixValues['name_first'] ) ) $this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) $this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['title'] ) ) $this->setTitle( $arrmixValues['title'] );
		if( true == isset( $arrmixValues['phone_number1_type_id'] ) )	$this->setPhoneNumber1TypeId( $arrmixValues['phone_number1_type_id'] );
		if( true == isset( $arrmixValues['phone_number2_type_id'] ) )	$this->setPhoneNumber2TypeId( $arrmixValues['phone_number2_type_id'] );
		if( true == isset( $arrmixValues['phone_number3_type_id'] ) )	$this->setPhoneNumber3TypeId( $arrmixValues['phone_number3_type_id'] );
		if( true == isset( $arrmixValues['phone_number1'] ) )	$this->setPhoneNumber1( $arrmixValues['phone_number1'] );
		if( true == isset( $arrmixValues['phone_number2'] ) )	$this->setPhoneNumber2( $arrmixValues['phone_number2'] );
		if( true == isset( $arrmixValues['phone_number3'] ) )	$this->setPhoneNumber3( $arrmixValues['phone_number3'] );
		if( true == isset( $arrmixValues['phone_number_extension'] ) )	$this->setPhoneNumberExtension( $arrmixValues['phone_number_extension'] );
		if( true == isset( $arrmixValues['address_line1'] ) ) $this->setAddressLine1( $arrmixValues['address_line1'] );
		if( true == isset( $arrmixValues['address_line2'] ) ) $this->setAddressLine2( $arrmixValues['address_line2'] );
		if( true == isset( $arrmixValues['city'] ) ) $this->setCity( $arrmixValues['city'] );
		if( true == isset( $arrmixValues['state_code'] ) ) $this->setStateCode( $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['postal_code'] ) ) $this->setPostalCode( $arrmixValues['postal_code'] );
		if( true == isset( $arrmixValues['reason'] ) ) $this->setReason( $arrmixValues['reason'] );
		if( true == isset( $arrmixValues['ps_lead_duplicate_type_id'] ) ) $this->setPsLeadDuplicateTypeId( $arrmixValues['ps_lead_duplicate_type_id'] );
		if( true == isset( $arrmixValues['contract_id'] ) ) $this->setContractId( $arrmixValues['contract_id'] );
		if( true == isset( $arrmixValues['contract_status_type_id'] ) ) $this->setContractStatusTypeId( $arrmixValues['contract_status_type_id'] );
		if( true == isset( $arrmixValues['ignored_by'] ) ) $this->setIgnoredBy( $arrmixValues['ignored_by'] );
		if( true == isset( $arrmixValues['dupe_count'] ) ) $this->setDupeCount( $arrmixValues['dupe_count'] );
		if( true == isset( $arrmixValues['marge_lead_index'] ) ) $this->setMargeLeadIndex( $arrmixValues['marge_lead_index'] );
		if( true == isset( $arrmixValues['marge_lead_into_index'] ) ) $this->setMargeLeadIntoIndex( $arrmixValues['marge_lead_into_index'] );
		if( true == isset( $arrmixValues['competitor_id'] ) ) $this->setCompetitorId( $arrmixValues['competitor_id'] );
		if( true == isset( $arrmixValues['cid'] ) ) $this->setCid( $arrmixValues['cid'] );
		if( true == isset( $arrmixValues['shoe_size'] ) ) $this->setShoeSize( $arrmixValues['shoe_size'] );
		if( true == isset( $arrmixValues['sales_employee_id'] ) ) $this->setSalesEmployeeId( $arrmixValues['sales_employee_id'] );
		if( true == isset( $arrmixValues['assign_employee_id'] ) ) $this->setAssignedEmployeeId( $arrmixValues['assign_employee_id'] );
		if( true == isset( $arrmixValues['ps_lead_type_id'] ) ) $this->setPsLeadTypeId( $arrmixValues['ps_lead_type_id'] );
		if( true == isset( $arrmixValues['ps_lead_property_types'] ) ) $this->setPsLeadPropertyTypes( $arrmixValues['ps_lead_property_types'] );
		if( true == isset( $arrmixValues['shirt_size'] ) ) $this->setShirtSize( $arrmixValues['shirt_size'] );
		if( true == isset( $arrmixValues['person_id'] ) ) $this->setPersonId( $arrmixValues['person_id'] );

		return;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addContract( $objContract ) {
		$this->m_arrobjContracts[$objContract->getId()] = $objContract;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchPersons( $objDatabase ) {
		return CPersons::createService()->fetchPersonsByPsLeadId( $this->m_intId, $objDatabase );
	}

	public function fetchNonDeletedContracts( $objDatabase ) {
		return CContracts::createService()->fetchNonDeletedContractsByPsLeadId( $this->getId(), $objDatabase );
	}

	public function fetchPersonsKeyedByFirstNameLastName( $objDatabase ) {

		$arrobjRekeyedPersons = array();

		$arrobjPersons = CPersons::createService()->fetchPersonsByPsLeadId( $this->m_intId, $objDatabase );

		if( true == valArr( $arrobjPersons ) ) {
			foreach( $arrobjPersons as $objPerson ) {
				$arrobjRekeyedPersons[trim( \Psi\CStringService::singleton()->strtolower( $objPerson->getNameFirst() ) ) . '-' . trim( \Psi\CStringService::singleton()->strtolower( $objPerson->getNameLast() ) )] = $objPerson;
			}
		}

		return $arrobjRekeyedPersons;
	}

	public function fetchPersonById( $intPersonId, $objDatabase, $intIsDisabled = 0 ) {
		return CPersons::createService()->fetchPersonByPersonIdByPsLeadId( $intPersonId, $this->getId(), $objDatabase, $intIsDisabled );
	}

	public function fetchActions( $objDatabase, $boolIsContactDatetimeOrderByDesc = true ) {
		return CActions::fetchActionsByPsLeadId( $this->getId(), $objDatabase, $boolIsContactDatetimeOrderByDesc );
	}

	public function fetchActionsByPersonId( $intPersonId, $objDatabase, $boolContractDatetimeDesc = true ) {
		return CActions::fetchActionsByPersonIdByPsLeadId( $intPersonId, $this->getId(), $objDatabase, $boolContractDatetimeDesc );
	}

	public function fetchSingleOpportunityByOpportunityStatusTypeIds( $arrintOpportunityStatusTypeIds, $objDatabase ) {
		return CContracts::createService()->fetchSingleOpportunityByPsLeadIdByOpportunityStatusTypeIds( $this->getId(), $arrintOpportunityStatusTypeIds, $objDatabase );
	}

	public function fetchSingleContract( $objDatabase ) {
		return CContracts::createService()->fetchSingleContractByPsLeadId( $this->getId(), $objDatabase );
	}

	public function fetchContractById( $intContractId, $objDatabase, $objPsLeadFilter = NULL ) {
		return CContracts::createService()->fetchContractByIdByPsLeadId( $intContractId, $this->getId(), $objDatabase, $objPsLeadFilter );
	}

	public function fetchActionById( $intActionId, $objDatabase ) {
		return CActions::fetchActionByIdByPsLeadId( $intActionId, $this->getId(), $objDatabase );
	}

	public function fetchContractWatchlists( $objDatabase ) {
		return \Psi\Eos\Admin\CContractWatchlists::createService()->fetchContractWatchlistsByPsLeadId( $this->getId(), $objDatabase );
	}

	public function fetchContractProducts( $objDatabase ) {
		return CContractProducts::createService()->fetchContractProductsByPsLeadId( $this->getId(), $objDatabase );
	}

	public function fetchMergePsLead( $objDatabase ) {
		return CPsLeads::createService()->fetchPsLeadById( $this->getMergePsLeadId(), $objDatabase );
	}

	public function fetchPsLeadCompetitors( $objAdminDatabase ) {
		return CPsLeadCompetitors::createService()->fetchPsLeadCompetitorsByPsLeadId( $this->getId(), $objAdminDatabase );
	}

	public function fetchPsLeadCompetitorsByPsProductId( $intPsProductId, $objDatabase ) {
		return CPsLeadCompetitors::createService()->fetchPsLeadCompetitorsByPsProductIdByPsLeadId( $intPsProductId, $this->getId(), $objDatabase );
	}

	public function fetchPsLeadDetail( $objAdminDatabase ) {
		$this->m_objPsLeadDetail = \Psi\Eos\Admin\CPsLeadDetails::createService()->fetchPsLeadDetailByPsLeadId( $this->getId(), $objAdminDatabase );
		return $this->m_objPsLeadDetail;
	}

	public function fetchContracts( $objAdminDatabase ) {
		return CContracts::createService()->fetchContractsByPsLeadId( $this->getId(), $objAdminDatabase );
	}

	public function fetchAllActionsByActionTypeIdByActionResultId( $intActionTypeId, $intActionResultId, $objDatabase ) {
		return CActions::fetchAllActionsByActionTypeIdByActionResultIdByPsLeadId( $intActionTypeId, $intActionResultId, $this->getId(), $objDatabase );
	}

	public function fetchAllActionsByActionTypeIdByActionCategoryId( $intActionTypeId, $intActionCategoryId, $objDatabase ) {
		return \Psi\Eos\Admin\CActions::createService()->fetchAllActionsByActionTypeIdByActionCategoryIdByPsLeadId( $intActionTypeId, $intActionCategoryId, $this->getId(), $objDatabase );
	}

	public function fetchClient( $objAdminDatabase ) {

		$objClient = CClients::createService()->fetchClientByPsLeadId( $this->getId(), $objAdminDatabase );

		if( true == valObj( $objClient, 'CClient' ) ) {
			$this->setCid( $objClient->getId() );
			return $objClient;
		} else {
			return NULL;
		}
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valPsLeadSourceId( $objDatabase ) {

		$boolIsValid = true;

		if( false == is_null( $this->getPsLeadSourceId() ) ) {
			$objPsLeadSourceId = \Psi\Eos\Admin\CPsLeadSources::createService()->fetchPsLeadSourceById( $this->getPsLeadSourceId(), $objDatabase );

			if( true == is_null( $objPsLeadSourceId ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_website_lead_source_id', 'Ps website lead source id ' . $this->getPsLeadSourceId() . ' is invalid. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCompetitorId( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_numeric( $this->getCompetitorId() ) && 0 < $this->getCompetitorId() ) {

			$objCompetitor = \Psi\Eos\Admin\CCompetitors::createService()->fetchCompetitorById( $this->getCompetitorId(), $objDatabase );

			if( true == is_null( $objCompetitor ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_competitor_id', 'Competitor id' . $this->getCompetitorId() . ' is invalid. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPsLeadOriginId( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getPsLeadOriginId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_lead_origin_id', 'Lead origin is required. ' ) );
		} else {

			$objPsLeadOriginId = \Psi\Eos\Admin\CPsLeadOrigins::createService()->fetchPsLeadOriginById( $this->getPsLeadOriginId(), $objDatabase );

			if( true == is_null( $objPsLeadOriginId ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_lead_origin_id', 'Lead origin id ' . $this->getPsLeadOriginId() . ' is invalid. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRequiredField() {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCompanyName() ) && 0 == strlen( $this->getEmailAddress() ) && 0 == strlen( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Company name or email address or first name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContractStatusTypeId( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getContractStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lead_status_type_id', 'Request status type id missing.' ) );
		} else {

			$objContractStatusTypeId = CContractStatusTypes::createService()->fetchContractStatusTypeById( $this->getContractStatusTypeId(), $objDatabase );

			if( true == is_null( $objContractStatusTypeId ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lead_status_type_id', 'Request status type id ' . $this->getContractStatusTypeId() . ' is invalid. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valNameFirst() {

		$boolIsValid = true;

		if( 0 == strlen( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {

		$boolIsValid = true;

		if( 0 == strlen( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {

		$boolIsValid = true;

		if( 0 == strlen( $this->getNameFirst() ) && 0 == strlen( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber1() {

		$boolIsValid = true;
		if( true == is_null( $this->getPhoneNumber1() ) || 0 == strlen( $this->getPhoneNumber1() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Enter phone number.' ) );
			$boolIsValid = false;
		} elseif( 0 < $this->getPhoneNumber1() && ( 10 > strlen( $this->getPhoneNumber1() ) || 15 < strlen( $this->getPhoneNumber1() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number1', 'Phone number must be between 10 and 15 characters.', 504 ) );
			$boolIsValid = false;
		} elseif( false == ( CValidation::validateFullPhoneNumber( $this->getPhoneNumber1(), false ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number1', 'Valid phone number is required.', 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valTitle() {

		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Please enter title.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyName() {

		$boolIsValid = true;

		if( true == is_null( $this->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company', 'Company name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNumberOfUnits( $boolIsNumberOfUnitsRequired = false ) {

		$boolIsValid = true;

		if( true == $boolIsNumberOfUnitsRequired && true == is_null( $this->getNumberOfUnits() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_units', 'Enter the number of units.' ) );
		}

		return $boolIsValid;
	}

	public function valIpAddress() {

		$boolIsValid = true;

		if( true == is_null( $this->getIpAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP address is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRequest() {

		$boolIsValid = true;

		if( true == is_null( $this->getRequest() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request', 'Request is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress( $boolIsAddEditLead = false ) {

		$boolIsValid = true;

		if( false == $boolIsAddEditLead && ( true == is_null( $this->getEmailAddress() ) || 0 == strlen( $this->m_strEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is required.' ) );
			return;
		}

		if( 0 < strlen( $this->m_strEmailAddress ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Enter a valid email address.' ) );
			return;
		}

		return $boolIsValid;
	}

	public function valAgreeToTerms() {

		$boolIsValid = true;

		if( true == is_null( $this->getAgreeToTerms() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agree_to_terms', 'You must indicate that you agree to the terms.' ) );
		}

		return $boolIsValid;
	}

	public function valStateCode( $objDatabase, $strInsertType ) {

		$boolIsValid = true;

		if( 'csv' == $strInsertType && false == is_null( $this->getStateCode() ) ) {

			$strWhereSql = 'WHERE code = \'' . $this->getStateCode() . '\'';
			$intStateCodeCount = \Psi\Eos\Admin\CStates::createService()->fetchStateCount( $strWhereSql, $objDatabase );

			if( 0 < $intStateCodeCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', 'State code is invalid. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDuplicateClient( $objDatabase ) {

		$boolIsValid = true;

		$objConflictingPsLead = CPsLeads::createService()->fetchConflictingPsLeadByPsLead( $this, $objDatabase );

		if( true == isset ( $objConflictingPsLead ) && true == valObj( $objConflictingPsLead, 'CPsLead' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Selected company is already associated to another lead.' ) );
		}

		return $boolIsValid;
	}

	public function valContractDeletion( $objDatabase ) {

		if( false == isset ( $objDatabase ) ) {
			trigger_error( 'Database sent is not valid.', E_USER_ERROR );
			exit;
		}

		$arrobjContracts = $this->fetchNonDeletedContracts( $objDatabase );

		if( true == valArr( $arrobjContracts ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'contract_id', 'You cannot delete or merge a lead if it has a contract associated to it.', NULL ) );
			return false;
		}

		return true;
	}

	public function valPropertyCount() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyCount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_number_of_properties', 'Number Of Properties is required' ) );
		}

		return $boolIsValid;
	}

	public function valShoeSize() {

		$boolIsValid = true;

		if( 'invalid' == $this->getShoeSize() || true == is_null( $this->getShoeSize() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'shoe_size', 'Shoe size is required.' ) );
		}

		return $boolIsValid;
	}

	public function valShirtSize() {

		$boolIsValid = true;

		if( 'invalid' == $this->getShirtSize() || true == is_null( $this->getShirtSize() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'shirt_size', 'Shirt size is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPsLeadOriginId( $objDatabase );
				$boolIsValid &= $this->valPsLeadSourceId( $objDatabase );
				$boolIsValid &= $this->valCompetitorId( $objDatabase );
				$boolIsValid &= $this->valRequiredField();
				$boolIsValid &= $this->valDuplicateClient( $objDatabase );
				if( 0 < strlen( trim( $this->getEmailAddress() ) ) ) {
					$boolIsValid &= $this->valEmailAddress();
				}
				if( VALIDATE_UPDATE == $strAction ) {
					$boolIsValid &= $this->valCompanyName();
				}
				break;

			// Added for new Add Lead page interface.
			case 'validate_insert_new':
			case 'validate_update_new':
				$boolIsValid &= $this->valEmailAddress( $boolIsAddEditLead = true );
				$boolIsValid &= $this->valPsLeadOriginId( $objDatabase );
				$boolIsValid &= $this->valPsLeadSourceId( $objDatabase );
				$boolIsValid &= $this->valCompetitorId( $objDatabase );
				$boolIsValid &= $this->valRequiredField();
				$boolIsValid &= $this->valDuplicateClient( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valContractDeletion( $objDatabase );
				break;

			case 'ps_website_request':
				$boolIsValid &= $this->valPsLeadOriginId( $objDatabase );
				$boolIsValid &= $this->valPsLeadSourceId( $objDatabase );
				$boolIsValid &= $this->valCompetitorId( $objDatabase );
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber1();
				$boolIsValid &= $this->valDuplicateClient( $objDatabase );
				break;

			case 'ps_website_naa2015_request':
				$boolIsValid &= $this->valShoeSize();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->valEmailAddress();
				break;

			case 'ps_website_newsletter_subscription_request':
				$boolIsValid &= $this->valPsLeadOriginId( $objDatabase );
				$boolIsValid &= $this->valPsLeadSourceId( $objDatabase );
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valDuplicateClient( $objDatabase );
				break;

			case 'insert_ps_website_newsletter_subscription_request':
				$boolIsValid &= $this->valEmailAddress();
				break;

			case 'client_insert':
				$boolIsValid &= $this->valPsLeadOriginId( $objDatabase );
				$boolIsValid &= $this->valPsLeadSourceId( $objDatabase );
				$boolIsValid &= $this->valCompetitorId( $objDatabase );
				$boolIsValid &= $this->valDuplicateClient( $objDatabase );
				break;

			case 'ps_website_seo_whitepaper':
			case 'ps_website_case_study':
				$boolIsValid &= $this->valPsLeadOriginId( $objDatabase );
				$boolIsValid &= $this->valPsLeadSourceId( $objDatabase );
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber1();
				break;

			case 'ps_website_insert':
				$boolIsValid &= $this->valPsLeadOriginId( $objDatabase );
				$boolIsValid &= $this->valPsLeadSourceId( $objDatabase );
				$boolIsValid &= $this->valCompetitorId( $objDatabase );
				$boolIsValid &= $this->valDuplicateClient( $objDatabase );
				$boolIsValid &= $this->valNumberOfUnits( true );
				$boolIsValid &= $this->valPropertyCount();
				break;

			case 'create_contract':
				$boolIsValid &= $this->valCompetitorId( $objDatabase );
				break;

			case 'mass_email':
				$boolIsValid &= $this->valPsLeadOriginId( $objDatabase );
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valDuplicateClient( $objDatabase );
				break;

			case 'enterprise_login_request':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber1();
				$boolIsValid &= $this->valCompanyName();
				break;

			case 'entrata_website_sfgiants_request':
				$boolIsValid &= $this->valShirtSize();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->valEmailAddress();
				break;

			case 'entrata_website_gaffigan_request':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->valEmailAddress();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Database Functions
	 *
	 */

	public function insert( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsPrimaryPersonEntryRequired = true, $boolIsCreatePerson = true ) {

		if( true == is_null( $this->getStateCode() ) && 2 < strlen( trim( preg_replace( '/[^0-9]/', '', $this->getPhoneNumber1() ) ) ) ) {
			$strFirstThreeNumbers = \Psi\CStringService::singleton()->substr( trim( preg_replace( '/[^0-9]/', '', $this->getPhoneNumber1() ) ), 0, 3 );

			$objStateAreaCode = CStateAreaCodes::createService()->fetchStateAreaCodeByAreaCode( $strFirstThreeNumbers, $objDatabase );

			if( true == valObj( $objStateAreaCode, 'CStateAreaCode' ) ) {
				$this->setStateCode( $objStateAreaCode->getStateCode() );
			}
		}

		if( false == parent::insert( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) return false;

		if( true == $boolIsCreatePerson ) {
			$objPerson = $this->createPerson();

			if( true == $boolIsPrimaryPersonEntryRequired ) {
				$objPerson->setIsPrimary( 1 );
			}

			if( false == $objPerson->validate( 'validate_email' ) || false == $objPerson->insert( $intUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $objPerson->getErrorMsgs() );
				return false;
			}

			$intPersonId				= $objPerson->getId();
			$arrobjPersonPhoneNumbers	= $this->createPersonPhoneNumbers();

			if( valArr( $arrobjPersonPhoneNumbers ) ) {
				foreach( $arrobjPersonPhoneNumbers as $objPersonPhoneNumber ) {
					$objPersonPhoneNumber->setPersonId( $intPersonId );
					if( !$objPersonPhoneNumber->validate( VALIDATE_INSERT ) ) {
						$this->addErrorMsgs( $objPersonPhoneNumber->getErrorMsgs() );

						return false;
					}
				}
			}

			if( valArr( $arrobjPersonPhoneNumbers ) && !\Psi\Eos\Admin\CPersonPhoneNumbers::createService()->bulkInsert( $arrobjPersonPhoneNumbers, $intUserId, $objDatabase ) ) {
				$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to save phone numbers' ) ] );
				return false;
			}
		}

		return true;
	}

	public function update( $intUserId, $objDatabase, $boolIsUpdateOnlyNewsletterLead = false ) {

		if( true == is_null( $this->getStateCode() ) && 2 < strlen( trim( preg_replace( '/[^0-9]/', '', $this->getPhoneNumber1() ) ) ) ) {
			$strFirstThreeNumbers = \Psi\CStringService::singleton()->substr( trim( preg_replace( '/[^0-9]/', '', $this->getPhoneNumber1() ) ), 0, 3 );

			$objStateAreaCode = CStateAreaCodes::createService()->fetchStateAreaCodeByAreaCode( $strFirstThreeNumbers, $objDatabase );

			if( true == isset ( $objStateAreaCode ) && true == valObj( $objStateAreaCode, 'CStateAreaCode' ) ) {
				$this->setStateCode( $objStateAreaCode->getStateCode() );
			}
		}

		if( false == parent::update( $intUserId, $objDatabase ) ) return false;

		$objPersons = CPersons::createService()->fetchPersonsByPsLeadId( $this->getId(), $objDatabase );

		if( true == valArr( $objPersons ) && true == $boolIsUpdateOnlyNewsletterLead ) {
			foreach( $objPersons as $objPerson ) {
				if( true == valObj( $objPerson, 'CPerson' ) ) {
					$objPerson->setCompanyName( $this->getCompanyName() );
					$objPerson->setNameFirst( $this->getNameFirst() );
					$objPerson->setNameLast( $this->getNameLast() );
					$objPerson->setTitle( $this->getTitle() );
					$objPerson->setEmailAddress( $this->getEmailAddress() );
				}
				if( false == $objPerson->update( $intUserId, $objDatabase ) ) return false;
			}
		}

		return true;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		// $this->setCid( NULL );
		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );

		return parent::update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

	/**
	 * Other Functions
	 *
	 */

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				$this->addErrorMsg( $objErrorMsg );
			}
		}
	}

	public function addPsWebsiteProductRequest( $objPsWebsiteProductRequest ) {
		$this->m_arrobjWebsiteProductRequests[] = $objPsWebsiteProductRequest;
	}

	public function fetchPrimaryPerson( $objDatabase ) {
		return CPersons::createService()->fetchPrimaryPersonByPsLeadId( $this->getId(), $objDatabase );
	}

	public function fetchPersonByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return CPersons::createService()->fetchPersonByPsLeadIdByCompanyUserId( $this->getId(), $intCompanyUserId, $objDatabase );
	}

	public function fetchEligibleClients( $objDatabase ) {
		return CClients::createService()->fetchEligibleClientsByPsLeadId( $this->getId(), $objDatabase );
	}

	public function fetchContractsByContractStatusTypeIds( $arrintContractStatusTypeIds, $objDatabase ) {
		return CContracts::createService()->fetchContractsByContractStatusTypeIdsByPsLeadId( $arrintContractStatusTypeIds, $this->getId(), $objDatabase );
	}

	public function fetchFirstPrimaryPerson( $objDatabase ) {
		return CPersons::createService()->fetchFirstPersonByPsLeadId( $this->getId(), $objDatabase );
	}

	public function updateProfilePercent( $objDatabase ) {
		if( true == valArr( CPsLeads::createService()->updateProfilePercents( $this->getId(), $objDatabase ), 0 ) ) {
			return true;
		}

		return false;
	}

	public function fetchPsLeadProducts( $objDatabase ) {
		return \Psi\Eos\Admin\CPsLeadProducts::createService()->fetchPsLeadProductsByPsLeadId( $this->getId(), $objDatabase );
	}

	public function fetchPsProductRelationships( $objDatabase ) {
		return CPsProductRelationships::createService()->fetchPsProductRelationshipsByPsLeadId( $this->getId(), $objDatabase );
	}

	public function sendNewLeadAssignedEmail( $objPsLead, $objPsLeadDetail, $strToEmailAddress, $arrobjEmployees, $objPrimaryPerson, $arrobjStates, $objEmailDatabase ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );
		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		$objSmarty->assign( 'states',			$arrobjStates );
		$objSmarty->assign( 'employees',		$arrobjEmployees );
		$objSmarty->assign( 'base_uri',			CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'view_ps_lead_url',	'http://' . basename( $_SERVER['HTTP_HOST'] ) );
		$objSmarty->assign( 'ps_lead',			$objPsLead );
		$objSmarty->assign( 'ps_lead_detail',	$objPsLeadDetail );
		$objSmarty->assign( 'primary_person',	$objPrimaryPerson );
		$objSmarty->assign( 'logo_url',			CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );

		$strHtmlContent	= $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'ps_lead/info/new_lead_assigned_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$strSubject		= $this->getCompanyName() . ' - ' . $objPsLead->getId() . ' - ' . ' Lead Assigned';

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, $strHtmlContent, $strToEmailAddress );

		if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) return false;

		return true;
	}

	public function fetchPsLeadProperties( $objDatabase ) {
		return \Psi\Eos\Admin\CPsLeadProperties::createService()->fetchPsLeadPropertiesByPsLeadId( $this->getId(), $objDatabase );
	}

	public function createPersonPhoneNumbers() {
		$arrobjPersonPhoneNumbers	= [];
		$objPersonPhoneNumber		= new CPersonPhoneNumber();
		$objPersonPhoneNumber->setPsLeadId( $this->getId() );

		if( valStr( $this->getPhoneNumber1() ) ) {
			$objClonedPersonPhoneNumber = clone $objPersonPhoneNumber;
			$objClonedPersonPhoneNumber->setPhoneNumberTypeId( CPhoneNumberType::OFFICE );
			$objClonedPersonPhoneNumber->setPhoneNumber( $this->getPhoneNumber1() );
			$arrobjPersonPhoneNumbers[] = $objClonedPersonPhoneNumber;
		}

		if( valStr( $this->getPhoneNumber2() ) ) {
			$objClonedPersonPhoneNumber = clone $objPersonPhoneNumber;
			$objClonedPersonPhoneNumber->setPhoneNumberTypeId( CPhoneNumberType::MOBILE );
			$objClonedPersonPhoneNumber->setPhoneNumber( $this->getPhoneNumber2() );
			$arrobjPersonPhoneNumbers[] = $objClonedPersonPhoneNumber;
		}

		if( valStr( $this->getPhoneNumber3() ) ) {
			$objClonedPersonPhoneNumber = clone $objPersonPhoneNumber;
			$objClonedPersonPhoneNumber->setPhoneNumberTypeId( CPhoneNumberType::FAX );
			$objClonedPersonPhoneNumber->setPhoneNumber( $this->getPhoneNumber3() );
			$arrobjPersonPhoneNumbers[] = $objClonedPersonPhoneNumber;
		}

		return $arrobjPersonPhoneNumbers;
	}

}
?>