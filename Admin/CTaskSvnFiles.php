<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskSvnFiles
 * Do not add any new functions to this class.
 */

class CTaskSvnFiles extends CBaseTaskSvnFiles {

	public static function fetchTaskSvnFilesByTaskId( $intTaskId, $objAdminDatabase ) {

		$strSql = 'SELECT
						tsf.*,
						sf.database_type_id,
						sf.file_name,
						e.preferred_name,
						e.name_full
					FROM
						task_svn_files tsf
						LEFT JOIN employees e ON ( tsf.schema_approved_by = e.id )
						LEFT JOIN svn_files sf ON ( tsf.svn_file_id = sf.id )
					WHERE
						tsf.task_id = ' . ( int ) $intTaskId . '
					ORDER BY
						tsf.order_num ';

		return parent::fetchTaskSvnFiles( $strSql, $objAdminDatabase );
	}

	public static function fetchTaskSvnFilesByTaskIds( $arrintTaskIds, $objAdminDatabase ) {
		return parent::fetchTaskSvnFiles( 'SELECT * FROM task_svn_files WHERE task_id IN ( ' . implode( ',', $arrintTaskIds ) . ' ) ORDER BY task_id, order_num', $objAdminDatabase );
	}

	public static function fetchTaskReadyForExecutionSvnFilesByTaskReleaseId( $strPreFix, $intTaskReleaseId, $objAdminDatabase ) {

		$strSql = 'SELECT
						tsf.*,
						sf.database_type_id,
						sf.file_name
					FROM
						task_svn_files tsf
						INNER JOIN tasks t ON ( t.id = tsf.task_id AND t.task_release_id = ' . ( int ) $intTaskReleaseId . ')
						INNER JOIN svn_files sf ON ( sf.id = tsf.svn_file_id )
					WHERE
						position(\'' . $strPreFix . '.\' in sf.file_name) = 1
						AND t.task_status_id IN ( ' . CTaskStatus::RELEASED_ON_STAGE . ',' . CTaskStatus::VERIFIED_ON_STAGE . ',' . CTaskStatus::APPROVED . ')
					ORDER BY
						database_type_id, sf.task_id, sf.order_num, sf.file_name';

		return parent::fetchTaskSvnFiles( $strSql, $objAdminDatabase );
	}

	public static function fetchSprintSummaryReportSqlsByReleaseId( $intReleaseId, $intIsReleased, $objDatabase, $strOrderByField, $strOrderByType ) {

		$strWhereCondition = 't.task_release_id = ' . ( int ) $intReleaseId;

		if( false == $intIsReleased ) {
			$strWhereCondition = 't.task_release_id = ' . ( int ) $intReleaseId .
			                     ' AND sf.file_name NOT LIKE \'ROS.%\'
						AND sf.file_name NOT LIKE \'ROSM.%\'
						AND sf.file_name NOT LIKE \'ROR.%\'
						AND sf.file_name NOT LIKE \'RORM.%\'
						AND t.task_status_id IN ( ' . CTaskStatus::RELEASED_ON_STAGE . ',' . CTaskStatus::VERIFIED_ON_STAGE . ',' . CTaskStatus::APPROVED . ')';
		}

		$strSql = 'SELECT
						tsf.task_id as task_id,
						sf.id as svn_file_id,
						sf.file_name as sql_file_name,
						sf.database_type_id as database_id,
						e.id as employee_id,
						e.name_full as committed_by,
						e1.id as pdm_id,
						CASE
							WHEN t.project_manager_id IS NULL THEN e2.preferred_name
							ELSE e1.preferred_name
						END AS pdm,
						tst.id as status_id,
						tst.name AS status,
						tp.name AS priority,
						ttp.name AS type,
						t.qa_approved_by as qa_approved,
						pp.name AS product_name,
						ppo.name AS ps_product_option_name,
						e3.name_full as sql_reviewed_by,
						e3.id as sql_reviewed_by_id,
						t.code_reviewed_by
					FROM
						task_svn_files tsf
						LEFT JOIN tasks t ON ( t.id = tsf.task_id )
						LEFT JOIN svn_files sf ON ( sf.id = tsf.svn_file_id )
						LEFT JOIN users u ON ( u.id = tsf.user_id )
						LEFT JOIN employees e ON ( e.id = u.employee_id )
						LEFT JOIN task_statuses  tst ON ( tst.id = t.task_status_id )
						LEFT JOIN task_priorities tp ON ( tp.id = t.task_priority_id )
						LEFT JOIN task_types ttp ON ( ttp.id = t.task_type_id )
						LEFT JOIN ps_products pp ON ( pp.id = t.ps_product_id )
						LEFT JOIN ps_product_options ppo ON ( ppo.id = t.ps_product_option_id )
						LEFT JOIN employees e1 on ( e1.id = t.project_manager_id )
						LEFT JOIN employees e2 ON ( e2.id = pp.sdm_employee_id )
						LEFT JOIN employees e3 ON ( e3.id = sf.sql_reviewed_by )
					WHERE
						' . $strWhereCondition . '
					ORDER BY ' . $strOrderByField . ' ' . $strOrderByType;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllTaskSvnFilesByTaskId( $intTaskId, $objDatabase ) {

		$strSql = ' SELECT *
					FROM
						svn_files sf
						LEFT JOIN task_svn_files tsf ON ( sf.id = tsf.svn_file_id )
					WHERE
						tsf.task_id = ' . ( int ) $intTaskId;

		return parent::fetchTaskSvnFiles( $strSql, $objDatabase );
	}

	public static function fetchTaskSvnFilesBySvnFileIds( $arrintSvnFileIds, $objDatabase ) {

		$strSql = 'SELECT * FROM task_svn_files WHERE svn_file_id IN ( ' . implode( ',', $arrintSvnFileIds ) . ' ) ';

		return parent::fetchTaskSvnFiles( $strSql, $objDatabase );
	}

	public static function fetchTasksByTaskIds( $arrintTaskIds, $objDatabase ) {

		$strSql = 'SELECT
						   t.id AS task_id,
						   sf.database_type_id AS database_type_id,
						   sf.file_name AS file_name,
						   ts.name AS task_status_name,
						   ts.id AS task_status_id,
						   tr.name AS task_release_datetime
						FROM
						   task_svn_files tsf
						   LEFT JOIN svn_files sf ON ( sf.id = tsf.svn_file_id )
						   LEFT JOIN tasks t ON ( t.id = tsf.task_id )
						   LEFT JOIN task_statuses ts ON ( ts.id = t.task_status_id )
						   LEFT JOIN task_releases tr ON (tr.id = t.task_release_id)
						WHERE
						   tsf.task_id IN ( ' . implode( ',', $arrintTaskIds ) . ' )
						   AND
						   t.deleted_on IS NULL';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchStageSqlCommitApprovalListByReleaseId( $intReleaseId, $objDatabase ) {

		$strSql = 'SELECT
						tsf.task_id as task_id,
						sf.id as svn_file_id,
						sf.file_name as sql_file_name,
						sf.database_type_id as database_type_id,
						e.id as employee_id,
						e.name_full as committed_by,
						e2.name_full as stage_commit_approved_by
					FROM
						task_svn_files tsf
						LEFT JOIN tasks t ON ( t.id = tsf.task_id )
						LEFT JOIN task_details td ON ( tsf.task_id = td.task_id )
						LEFT JOIN svn_files sf ON ( sf.id = tsf.svn_file_id )
						LEFT JOIN users u ON ( u.id = tsf.user_id )
						LEFT JOIN employees e ON ( e.id = u.employee_id )
						LEFT JOIN employees e2 ON ( e2.id = td.stage_commit_approved_by )
					WHERE
						(t.task_release_id = ' . ( int ) $intReleaseId . ' )
						AND td.stage_commit_approved_by IS NOT NULL
						AND sf.file_name NOT LIKE \'ROP.%\'
						AND sf.file_name NOT LIKE \'ROPM.%\'';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSprintSummaryReportSqlsByReleaseIdByFileNames( $arrstrFileNames, $objDatabase ) {

		$strSql = 'SELECT
						tsf.task_id as task_id,
						sf.id as svn_file_id,
						sf.file_name as sql_file_name,
						sf.database_type_id as database_id,
						e.id as employee_id,
						e.name_full as committed_by,
						e1.id as pdm_id,
						CASE
							WHEN t.project_manager_id IS NULL THEN e2.preferred_name
							ELSE e1.preferred_name
						END AS pdm,
						tst.id as status_id,
						tst.name AS status,
						tp.name AS priority,
						ttp.name AS type,
						t.qa_approved_by as qa_approved,
						pp.name AS product_name,
						ppo.name AS ps_product_option_name,
						e3.name_full as sql_reviewed_by,
						e3.id as sql_reviewed_by_id,
						t.code_reviewed_by
					FROM
						task_svn_files tsf
						LEFT JOIN tasks t ON ( t.id = tsf.task_id )
						LEFT JOIN svn_files sf ON ( sf.id = tsf.svn_file_id )
						LEFT JOIN users u ON ( u.id = tsf.user_id )
						LEFT JOIN employees e ON ( e.id = u.employee_id )
						LEFT JOIN task_statuses  tst ON ( tst.id = t.task_status_id )
						LEFT JOIN task_priorities tp ON ( tp.id = t.task_priority_id )
						LEFT JOIN task_types ttp ON ( ttp.id = t.task_type_id )
						LEFT JOIN ps_products pp ON ( pp.id = t.ps_product_id )
						LEFT JOIN ps_product_options ppo ON ( ppo.id = t.ps_product_option_id )
						LEFT JOIN employees e1 on ( e1.id = t.project_manager_id )
						LEFT JOIN employees e2 ON ( e2.id = pp.sdm_employee_id )
						LEFT JOIN employees e3 ON ( e3.id = sf.sql_reviewed_by )
					WHERE
						 sf.file_name IN( ' . "'" . implode( "', '", $arrstrFileNames ) . "'" . ' )';

		return fetchData( $strSql, $objDatabase );
	}

}
?>