<?php

class CSalesArea extends CBaseSalesArea {

	protected $m_boolIsSelected;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsSelected = false;
		return;
	}

	/**
	 * Set Functions
	 */

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

	/**
	 * Create Functions
	 */

	public function createSalesAreaLocation() {

		$objSalesAreaLocation = new CSalesAreaLocation();

		$objSalesAreaLocation->setSalesAreaId( $this->getId() );

		return $objSalesAreaLocation;
	}

	public function createEmployeeSalesArea() {

		$objEmployeeSalesArea = new CEmployeeSalesArea();

		$objEmployeeSalesArea->setSalesAreaId( $this->getId() );

		return $objEmployeeSalesArea;
	}

	/**
	 * Validate Functions
	 */

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>