<?php

class CTransportPickupPoint extends CBaseTransportPickupPoint {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase, $boolValidateName ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Pickup Point Name is required.' ) );
		}

		if( true == $boolValidateName && true == $boolIsValid ) {
			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$strSql = " WHERE name ILIKE '" . trim( addslashes( $this->getName() ) ) . "' " . $strSqlCondition;

			$intCount = \Psi\Eos\Admin\CTransportPickupPoints::createService()->fetchTransportPickupPointCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Pickup Point already exists .' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '  Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolValidateName = true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
					$boolIsValid &= $this->valName( $objDatabase, $boolValidateName );
					$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>