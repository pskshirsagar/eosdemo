<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeTests
 * Do not add any new functions to this class.
 */

class CEmployeeTests extends CBaseEmployeeTests {

	public static function fetchTestIdsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		$strSql = 'SELECT DISTINCT
						test_id
					FROM
						employee_tests
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ';

		return array_keys( rekeyArray( 'test_id', fetchData( $strSql, $objDatabase ) ) );
	}

	public static function fetchEmployeTestsByEmployeeIdsByTestId( $arrintEmployeeIds, $intTestId, $objDatabase ) {
		return CEmployeeTests::fetchEmployeeTests( 'SELECT * FROM employee_tests WHERE test_id =' . ( int ) $intTestId . ' AND employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ', $objDatabase );
	}

	public function fetchEmployeeTestsByTestIdsByEmployeeIds( $arrintTestIds, $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintTestIds ) || false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						employee_tests
					WHERE
						test_id IN ( ' . implode( ',', $arrintTestIds ) . ' ) 
						AND employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ';
		return self::fetchEmployeeTests( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTestsByTestIdByAssignmentTypes( $intTestId, $strAssignmentType, $arrintAssignmentIds, $objDatabase ) {
		if( true == is_null( $intTestId ) || false == valStr( $strAssignmentType ) || false == valArr( $arrintAssignmentIds ) ) return false;

		$strJoin = '';
		$strWhereCondition = '';
		if( 'department' == $strAssignmentType ) {
			$strWhereCondition = ' AND e.department_id IN ( ' . implode( ',', $arrintAssignmentIds ) . ' )';
		} elseif( 'office' == $strAssignmentType ) {
			$strWhereCondition = ' AND e.office_id IN ( ' . implode( ',', $arrintAssignmentIds ) . ' )';
		} elseif( 'designation' == $strAssignmentType ) {
			$strWhereCondition = ' AND e.designation_id IN ( ' . implode( ',', $arrintAssignmentIds ) . ' )';
		} elseif( 'group' == $strAssignmentType ) {
			$strJoin = 'JOIN users u ON( u.employee_id = e.id ) JOIN user_groups ug ON ( ug.user_id = u.id AND ug.deleted_by IS NULL )';
			$strWhereCondition = ' AND ug.group_id IN ( ' . implode( ',', $arrintAssignmentIds ) . ' )';
		} elseif( 'team' == $strAssignmentType ) {
			$strJoin = 'JOIN team_employees te ON( te.employee_id = e.id AND te.is_primary_team = 1 )';
			$strWhereCondition = ' AND te.team_id IN ( ' . implode( ',', $arrintAssignmentIds ) . ' )';
		} elseif( 'training' == $strAssignmentType ) {
			$strJoin = ' JOIN users u ON( u.employee_id = e.id ) JOIN employee_training_sessions ets ON( ets.user_id = u.id )';
			$strWhereCondition = ' AND ets.training_session_id IN ( ' . implode( ',', $arrintAssignmentIds ) . ' )';
		}

		$strSql = 'SELECT
					    DISTINCT ON ( et.employee_id, et.test_id ) et.employee_id,
					    et.id
					FROM
					    employee_tests et
					    JOIN employees e ON ( et.employee_id = e.id ) ' . $strJoin . '
					WHERE
					    et.test_id = ' . ( int ) $intTestId . $strWhereCondition;

		return parent::fetchEmployeeTests( $strSql, $objDatabase );
	}

}
?>