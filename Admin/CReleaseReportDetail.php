<?php

class CReleaseReportDetail extends CBaseReleaseReportDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskReleaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDownTimeDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReleasedTaskDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPublishedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>