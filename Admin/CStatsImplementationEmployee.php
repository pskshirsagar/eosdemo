<?php

class CStatsImplementationEmployee extends CBaseStatsImplementationEmployee {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case self::VALIDATE_INSERT:
			case self::VALIDATE_UPDATE:
			case self::VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
		}
		return $boolIsValid;
	}
}
?>