<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CAnnualBonusTypes
 * Do not add any new functions to this class.
 */

class CAnnualBonusTypes extends CBaseAnnualBonusTypes {

	public static function fetchAnnualBonusTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CAnnualBonusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAnnualBonusType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CAnnualBonusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllAnnualBonusTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM annual_bonus_types order by id desc';
		return parent::fetchAnnualBonusTypes( $strSql, $objDatabase );
	}
}
?>