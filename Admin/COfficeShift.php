<?php

class COfficeShift extends CBaseOfficeShift {

	const CURRENT_START_DATE					= '05/06/2013';
	const DEFAULT_PREVIOUS_OFFICE_START_TIME	= '10:00:00';
	const DEFAULT_OFFICE_SHIFT	= 1;
	const GENERAL_SHIFT			= 1;
	const MORNING_SHIFT			= 5;
	const EVENING_SHIFT			= 6;
	const NIGHT_SHIFT			= 7;
	const DAWN_SHIFT			= 8;
	const DUSK_SHIFT			= 9;
	const REGULAR_SHIFT			= 10;
	const SUNSET_SHIFT			= 11;
	const MIDNIGHT_SHIFT		= 12;
	const DP_NOON_SHIFT		    = 13;
	const DP_NIGHT_SHIFT		= 14;

	const UPDATE_SHIFT_START_TIME = '00:00:00';
	const UPDATE_SHIFT_END_TIME   = '07:00:00';
	const END_TIME                = '23:59:59';
	const SHIFT_BUFFER_HOURS      = 0.5;

	public static $c_arrintOfficeShifts = array(
		'GENERAL_SHIFT'		=> self::GENERAL_SHIFT,
		'MORNING_SHIFT'		=> self::MORNING_SHIFT,
		'EVENING_SHIFT'		=> self::EVENING_SHIFT,
		'NIGHT_SHIFT'		=> self::NIGHT_SHIFT,
		'DAWN_SHIFT'		=> self::DAWN_SHIFT,
		'DUSK_SHIFT'		=> self::DUSK_SHIFT,
		'REGULAR_SHIFT'		=> self::REGULAR_SHIFT,
		'SUNSET_SHIFT'		=> self::SUNSET_SHIFT,
		'MIDNIGHT_SHIFT'	=> self::MIDNIGHT_SHIFT,
		'DP_NOON_SHIFT'     => self::DP_NOON_SHIFT,
		'DP_NIGHT_SHIFT'    => self::DP_NIGHT_SHIFT
	);

	public static $c_arrintOfficeShiftDetails = array(
		self::GENERAL_SHIFT => 'general_shift',
		self::MORNING_SHIFT => 'morning_shift',
		self::EVENING_SHIFT => 'evening_shift',
		self::NIGHT_SHIFT 	=> 'night_shift',
		self::DAWN_SHIFT	=> 'dawn_shift',
		self::DUSK_SHIFT	=> 'dusk_shift'
	);

	public static $c_arrintBufferTimeOfficeShifts = array(
		'GENERAL_SHIFT'		=> self::GENERAL_SHIFT,
		'MORNING_SHIFT'		=> self::MORNING_SHIFT,
		'EVENING_SHIFT'		=> self::EVENING_SHIFT,
		'NIGHT_SHIFT'		=> self::NIGHT_SHIFT
	);

	protected $m_strOfficeStartTimeLabel;

	public function getOfficeStartTimeLabel() {
		return $this->m_strOfficeStartTimeLabel;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['office_start_time_label'] ) ) $this->setOfficeStartTimeLabel( $arrmixValues['office_start_time_label'] );
		return;
	}

	public function setOfficeStartTimeLabel( $strOfficeStartTime ) {
		$this->m_strOfficeStartTimeLabel = $strOfficeStartTime;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>