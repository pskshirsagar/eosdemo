<?php

use Psi\Libraries\UtilPsHtmlMimeMail\CPsHtmlMimeMail;

class CSemBudget extends CBaseSemBudget {

	protected $m_fltBillingAmount;
	protected $m_fltMonthlyBudgetOld;

	public function __construct() {
		parent::__construct();

		$this->m_fltBillingAmount;

		return;
	}

	public function createSemTransaction() {

		$objSemTransaction = new CSemTransaction();
		$objSemTransaction->setCid( $this->getCid() );
		$objSemTransaction->setPropertyId( $this->getPropertyId() );
		$objSemTransaction->setSemAdGroupId( $this->getSemAdGroupId() );
		$objSemTransaction->setSemBudgetId( $this->getId() );
		$objSemTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );

		return $objSemTransaction;
	}

	public function createSemBudgetLog() {

		$objSemBudgetLog = new CSemBudgetLog();
		$objSemBudgetLog->setCid( $this->getCid() );
		$objSemBudgetLog->setPropertyId( $this->getPropertyId() );
		$objSemBudgetLog->setLogDatetime( date( 'm/d/Y H:i:s' ) );
		$objSemBudgetLog->setDailyRemainingBudget( $this->getDailyRemainingBudget() );

		return $objSemBudgetLog;
	}

	public function getBillingAmount() {
		return $this->m_fltBillingAmount;
	}

	public function getMonthlyBudgetOld() {
		return $this->m_fltMonthlyBudgetOld;
	}

	public function setBillingAmount( $fltBillingAmount ) {
		$this->m_fltBillingAmount = $fltBillingAmount;
	}

	public function setMonthlyBudgetOld( $fltMonthlyBudgetOld ) {
		$this->m_fltMonthlyBudgetOld = $fltMonthlyBudgetOld;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Proeprty id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMonthlyBudget() {
		$boolIsValid = true;

		if( true == is_null( $this->getMonthlyBudget() ) || 0 == $this->getMonthlyBudget() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'monthly_budget', 'Amount must be greater than zero.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAccountId();
				break;

			case 'insert_one_time_budget':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valMonthlyBudget();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function sendSemBudgetEmails( $objDatabase, $strSubject = NULL ) {

		$objProperty 			= $this->fetchProperty( $objDatabase );
		$objClient   = $objProperty->fetchClient( $objDatabase );

		if( false == valObj( $objClient, 'CClient' ) ) {
			trigger_error( 'client FAILED TO LOAD.', E_USER_ERROR );
		}

		$objClientDatabase = $objClient->loadDatabase( CDatabaseUserType::PS_PROPERTYMANAGER );

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			trigger_error( 'CLIENT DATABASE FAILED TO LOAD.', E_USER_ERROR );
		}

		$arrobjCompanyEmployees	= $objProperty->fetchCompanyEmployees( $objClientDatabase );

		if( true == valArr( $arrobjCompanyEmployees ) ) {

			// Added by Sandy to generate HTML email
			require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

			$objSmarty 		= new CPsSmarty( PATH_INTERFACES_RESIDENT_WORKS, false );

			$objSmarty->assign( 'base_uri', 				CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
			$objSmarty->assign( 'property', 				$objProperty );

			$objHtmlMimeEmail = new CPsHtmlMimeMail();
			$objHtmlMimeEmail->setFrom( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
			$objHtmlMimeEmail->setSubject( 'Vacancy.com Negative Balance Notification' );

			foreach( $arrobjCompanyEmployees as  $objCompanyEmployee ) {

				// Generate Text Email Content
				if( false == is_null( $objCompanyEmployee ) && 0 < strlen( $objCompanyEmployee->getEmailAddress() ) ) {

					$objMimeEmail = clone $objHtmlMimeEmail;

					$objSmarty->assign( 'company_employee', 			$objCompanyEmployee );

					$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/vacancy/balance_notification_email.tpl' );

					$objMimeEmail->setHtml( $strHtmlEmailOutput );

					$objMimeEmail->send( array( $objCompanyEmployee->getEmailAddress() ) );
				}
			}
		}
	}

	public function calculateBudgetParameters( $objSemSetting, $objDatabase ) {

		$this->setMonthlyBudget( $this->getMonthlyBudget() + $this->getBillingAmount() );

		$fltAccountBalance = $this->fetchBalance( $objDatabase );

		$this->setRemainingBudget( $fltAccountBalance * ( -1 ) );

		// If balance is greater than or equal to zero set daily remaning budget to zero.
		if( 0 <= $fltAccountBalance ) {
			$this->setDailyRemainingBudget( 0 );
		} else {
			if( CFrequency::MONTHLY == $objSemSetting->getFrequencyId() ) {
				$this->setDailyRemainingBudget( $this->getRemainingBudget() / ( int ) ( ( date( 't' ) - date( 'd' ) ) + 1 ) );
			} else {
				$this->setDailyRemainingBudget( $this->getRemainingBudget() );
			}
		}
	}

	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Admin\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchBalance( $objDatabase ) {

		$strSql = 'SELECT
						sum( transaction_amount )
					FROM
						sem_transactions
					WHERE 1 = 1 ';

		if( true == is_numeric( $this->getPropertyId() ) ) {
			$strSql .= ' AND property_id = ' . ( int ) $this->getPropertyId() . ' ';
		} elseif( true == is_numeric( $this->getSemAdGroupId() ) ) {
			$strSql .= ' AND sem_ad_group_id = ' . ( int ) $this->getSemAdGroupId() . ' AND property_id IS NULL ';
		}

		$arrfltBalance = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrfltBalance[0]['sum'] ) ) {
			return ( float ) $arrfltBalance[0]['sum'];

		} else {
			return 0;
		}
	}

	public function fetchAccount( $objDatabase ) {
		return \Psi\Eos\Admin\CAccounts::createService()->fetchAccountById( $this->getAccountId(), $objDatabase );
	}

	public function fetchSemSetting( $objDatabase ) {
		return CSemSettings::fetchSemSettingByPropertyId( $this->getPropertyId(), $objDatabase );
	}

}
?>