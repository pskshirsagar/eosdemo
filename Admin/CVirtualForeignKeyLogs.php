<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CVirtualForeignKeyLogs
 * Do not add any new functions to this class.
 */

class CVirtualForeignKeyLogs extends CBaseVirtualForeignKeyLogs {

	public static function fetchLatestVirtualForeignKeyLogsBYVirtualForeignKeyId( $intVirtualForeignKeyId, $objDatabase ) {

		$strSql = 'SELECT
			s.*,
			SUM ( s.error_count ) OVER ( PARTITION BY ( s.virtual_foreign_key_id ) )
		FROM
			(
			SELECT
				vfkl.*,
				rank ( ) OVER ( PARTITION BY virtual_foreign_key_id
					ORDER BY
					log_datetime DESC ) AS rank
			FROM
				virtual_foreign_key_logs vfkl
			) AS s
		WHERE
			s.virtual_foreign_key_id = ' . $intVirtualForeignKeyId . '
			AND rank = 1
		ORDER BY
			s.log_datetime';

	return self::fetchVirtualForeignKeyLogs( $strSql, $objDatabase );
	}
}
?>