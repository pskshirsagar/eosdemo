<?php

class CSalesTaxSetting extends CBaseSalesTaxSetting {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrencyCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsNonBilledSalesTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsBlockedSalesTax() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>