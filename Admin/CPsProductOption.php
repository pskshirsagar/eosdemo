<?php

use Psi\Eos\Admin\CPsProductOptions;

class CPsProductOption extends CBasePsProductOption {

	const CLIENT_SERVICES		= 6;
	const ACCOUNTING			= 7;
	const SALES_AND_MARKETING	= 8;
	const HR_AND_GENERAL		= 9;
	const SMS					= 12;
	const SECURITY				= 42;
	const DEPLOY				= 41;
	const DBA					= 44;
	const DATA_WAREHOUSE		= 37;
	const LEASE_WORKFLOWS_AR	= 4;
	const LEASE_FINANCIALS_AR	= 50;
	const RATES					= 59;
	const DATA_REQUEST			= 49;

	const REPORT_AR_AND_PAYMENTS		= 3;
	const ENTRATA_PASS_REPORT			= 10;
	const RESIDENT_PORTAL_APP			= 55;
	const RESIDENT_PORTAL_API			= 57;
	const RESIDENT_PORTAL_ALEXA_SKILL	= 68;
	const RESIDENT_PORTAL_MOBILE		= 22;
	const WHITE_LABEL_MOBILE_APP		= 53;
	const REPORT_AP_GL_AND_FACILITIES	= 46;

	const LOBBY_DISPLAY_BETA			= 2;
	const SNIPPETS_AND_WRAPPERS			= 20;
	const RATINGS_AND_REVIEWS			= 21;
	const PROSPECT_PORTAL_ARCHITECTURE	= 30;
	const PROSPECT_PORTAL_ONLINE_APPLICATIONS = 1;

	const ENTRATA_AFFORDABLE_HUD		= 27;
	const ENTRATA_AFFORDABLE_TAX_CREDIT	= 28;

	const PS_PRODUCT_RESIDENT_MAINTENANCE	= 36;

	const LEASE_EXECUTION_LITE	= 51;

	const PS_PRODUCT_ENTRATA_COMMERCIAL	= 54;
	const PS_PRODUCT_ENTRATA_MILITARY	= 60;
	const PS_PRODUCT_ENTRATA_STUDENT	= 26;

	const REPORT_CUSTOMS				= 67;
	const REPORT_FRAMEWORK				= 65;

	const ENTRATA_FACILITIES_APP		= 66;
	const LIVE_IT						= 69;
	const RESIDENT_PORTAL_APP_ANDROID	= 72;
	const RESIDENT_PORTAL_APP_IOS		= 74;
	const ENTRATA_INTERNATIONAL			= 73;

	const MIGRATIONS					= 25;

	const ENTRATA_CORE_APP_STORE			= 34;
	const ENTRATA_APPROVAL_ROUTING			= 35;
	const ENTRATA_CORE_ACCOUNTING_AP		= 5;
	const ENTRATA_CORE_ACCOUNTING_GL		= 75;
	const ENTRATA_PASS_CALENDAR				= 64;
	const ENTRATA_PASS_DASHBOARD			= 48;
	const ENTRATA_PASS_PROPERTY_MANAGEMENT	= 40;
	const ENTRATA_PASS_HELP_AND_SUPPORT		= 17;
	const ENTRATA_PRICING_WEB_RECON			= 38;
	const LEAD_MANAGER_LEASE_RENEWALS		= 43;
	const ENTRATA_PASS_USERS_AND_GROUPS		= 18;
	const PMS_INTEGRATION_RESIDENTS			= 15;
	const TRANSMISSIONS_REVENUE_MANAGEMENT	= 63;
	const PRODUCT_DEV_AND_SUPPORT			= 80;

	const GOOGLE_MY_BUSINESS	= 85;
	const YEXT					= 86;

	const PRODUCT_LEASE_FINANCIALS	= 'Lease Financials';
	const PRODUCT_RATES		        = 'Rates';

	protected $m_intBundlePsProductId;

	public static $c_arrintEntrataCoreARProductOptionIds = [
		self::ENTRATA_CORE_APP_STORE,
		self::PS_PRODUCT_ENTRATA_STUDENT,
		self::LEASE_FINANCIALS_AR,
		self::LEASE_WORKFLOWS_AR,
		self::MIGRATIONS,
		self::RATES,
		self::REPORT_AR_AND_PAYMENTS,
		self::ENTRATA_PASS_REPORT,
		self::REPORT_FRAMEWORK
	];

	public static $c_arrintEntrataCoreAPProductOptionIds = [
		self::ENTRATA_APPROVAL_ROUTING,
		self::ENTRATA_CORE_ACCOUNTING_AP,
		self::ENTRATA_CORE_ACCOUNTING_GL,
		self::REPORT_AP_GL_AND_FACILITIES,
		self::REPORT_CUSTOMS
	];

	public static $c_arrintLeasingProductOptionIds = [
		self::ACCOUNTING,
		self::CLIENT_SERVICES,
		self::DATA_REQUEST,
		self::HR_AND_GENERAL,
		self::SALES_AND_MARKETING,
		self::ENTRATA_PASS_CALENDAR,
		self::ENTRATA_PASS_DASHBOARD,
		self::ENTRATA_PASS_HELP_AND_SUPPORT,
		self::LEAD_MANAGER_LEASE_RENEWALS,
		self::PRODUCT_DEV_AND_SUPPORT
	];

	public static $c_arrintResidentsProductOptionIds = [
		self::ENTRATA_PASS_USERS_AND_GROUPS,
		self::SMS,
		self::PMS_INTEGRATION_RESIDENTS,
		self::RESIDENT_PORTAL_APP,
		self::RESIDENT_PORTAL_APP_ANDROID,
		self::RESIDENT_PORTAL_MOBILE
	];

	public static $c_arrintMarketingAndPricingProductOptionIds = [
		self::ENTRATA_PASS_PROPERTY_MANAGEMENT,
		self::ENTRATA_PRICING_WEB_RECON,
		self::LOBBY_DISPLAY_BETA,
		self::PROSPECT_PORTAL_ONLINE_APPLICATIONS,
		self::RATINGS_AND_REVIEWS,
		self::SNIPPETS_AND_WRAPPERS,
		self::DBA,
		self::SECURITY,
		self::TRANSMISSIONS_REVENUE_MANAGEMENT
	];

	public static $c_arrintReportProductIds = [
		CPsProductOption::REPORT_AP_GL_AND_FACILITIES,
		CPsProductOption::REPORT_AR_AND_PAYMENTS,
		CPsProductOption::ENTRATA_PASS_REPORT,
		CPsProductOption::REPORT_CUSTOMS
	];

	public static $c_arrstrProductOptionNames = [
		self::SMS		=> 'SMS'
	];

	public static $c_arrintMobilePsProductOptionIds = [
		self::ENTRATA_FACILITIES_APP,
		self::RESIDENT_PORTAL_ALEXA_SKILL,
		self::RESIDENT_PORTAL_API,
		self::RESIDENT_PORTAL_APP,
		self::RESIDENT_PORTAL_APP_ANDROID,
		self::RESIDENT_PORTAL_APP_IOS,
		self::RESIDENT_PORTAL_MOBILE,
		self::WHITE_LABEL_MOBILE_APP
	];

	public static $c_arrintMobileTaskProductOptionIds = [
		self::RESIDENT_PORTAL_MOBILE,
		self::WHITE_LABEL_MOBILE_APP,
		self::RESIDENT_PORTAL_APP,
		self::RESIDENT_PORTAL_APP_ANDROID,
		self::RESIDENT_PORTAL_APP_IOS
	];

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['bundle_ps_product_id'] ) ) {
			$this->setBundlePsProductId( $arrmixValues['bundle_ps_product_id'] );
		}

		return;
	}

	public function setBundlePsProductId( $intBundlePSProductId ) {
		$this->m_intBundlePsProductId = $intBundlePSProductId;
	}

	/**
	 * get Functions
	 *
	 */

	public function getBundlePsProductId() {
		return $this->m_intBundlePsProductId;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valSetUpChargeCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSetUpChargeCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'set_up_charge_code_id', 'Setup charge is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRecurringChargeCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getRecurringChargeCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recurring_charge_code_id', 'Recurring Charge Code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateName( $objAdminDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			return $boolIsValid;
		}

		$intPsProductOptionCount = CPsProductOptions::createService()->fetchConflictingPsProductOptionByName( $this->getName(), $this->getId(), $objAdminDatabase );

		if( 0 < $intPsProductOptionCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is already in use, please enter another one.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSetUpAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getSetUpAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'set_up_amount', 'Setup amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMonthlyRecurringAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getMonthlyRecurringAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'monthly_recurring_amount', 'Recurring amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDuplicateName( $objAdminDatabase );

				$boolIsValid &= $this->valSetUpChargeCodeId();
				$boolIsValid &= $this->valSetUpAmount();
				$boolIsValid &= $this->valRecurringChargeCodeId();
				$boolIsValid &= $this->valMonthlyRecurringAmount();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDuplicateName( $objAdminDatabase );

				$boolIsValid &= $this->valSetUpChargeCodeId();
				$boolIsValid &= $this->valSetUpAmount();
				$boolIsValid &= $this->valRecurringChargeCodeId();
				$boolIsValid &= $this->valMonthlyRecurringAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>