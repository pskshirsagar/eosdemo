<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskImpactTypes
 * Do not add any new functions to this class.
 */

class CTaskImpactTypes extends CBaseTaskImpactTypes {

	public static function fetchAllTaskImpactTypes( $objDatabase ) {
		$strSql = ' SELECT
						tit.*
					FROM
						task_impact_types as tit
					ORDER BY
						order_num';

		return self::fetchTaskImpactTypes( $strSql, $objDatabase );
	}

}
?>