<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskReleases
 * Do not add any new functions to this class.
 */

class CTaskReleases extends CBaseTaskReleases {

	public static function fetchPaginatedTaskReleases( $intPageNo, $intPageSize, $strOrderByFieldName = NULL, $strOrderField = NULL, $objTaskReleaseFilter = NULL, $objDatabase ) {

		$intOffset 	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit 	= ( int ) $intPageSize;

		if( true == valObj( $objTaskReleaseFilter, 'CTaskReleaseFilter' ) ) {
			$arrstrAndSearchParameters = self::fetchSearchCriteria( $objTaskReleaseFilter );
		}

		$strSubSql = 'SELECT
						t.task_release_id, COUNT( t.id ) as task_count
					FROM
						task_releases tr
						LEFT JOIN tasks t ON ( tr.id = t.task_release_id ) ' .
						( ( true == valArr( $arrstrAndSearchParameters ) ) ? '
					WHERE ' .
						implode( ' AND ', $arrstrAndSearchParameters ) : '' ) . '
					GROUP BY
						t.task_release_id,tr.id
					ORDER BY  task_release_id

					OFFSET ' .
						( int ) $intOffset . '
					LIMIT ' .
						$intLimit;

		$strSql = 'SELECT
						tr.*, trt.task_count
					FROM
						task_releases tr
						LEFT JOIN ( ' . $strSubSql . ' ) trt ON tr.id = trt.task_release_id ' .
						( ( true == valArr( $arrstrAndSearchParameters ) ) ? '
					WHERE ' .
						implode( ' AND ', $arrstrAndSearchParameters ) : '' ) . '
					ORDER BY' . ' tr.' . $strOrderByFieldName . ' ' . $strOrderField . '

					OFFSET ' .
						( int ) $intOffset . '
					LIMIT ' .
						$intLimit;

		return self::fetchTaskReleases( $strSql, $objDatabase );
	}

	public static function fetchSearchCriteria( $objTaskReleaseFilter = NULL ) {

		$arrstrAndSearchParameters = array();

		// Create SQL parameters.
		$strName = trim( $objTaskReleaseFilter->getName() );
		$strDescription = trim( $objTaskReleaseFilter->getDescription() );
		$strReleaseDatetime = trim( $objTaskReleaseFilter->getReleaseDatetime() );
		$strNotes = trim( $objTaskReleaseFilter->getNotes() );
		$strSystemDownOn = trim( $objTaskReleaseFilter->getSystemDownOn() );
		$strSystemUpOn = trim( $objTaskReleaseFilter->getSystemUpOn() );
		$strIsPublished = trim( $objTaskReleaseFilter->getIsPublished() );
		$strKeyword = trim( $objTaskReleaseFilter->getKeyword() );
		if( false == empty( $strName ) )						$arrstrAndSearchParameters[] = "tr.name LIKE '%" . addslashes( $objTaskReleaseFilter->sqlName() ) . "%'";
		if( false == empty( $strDescription ) ) 				$arrstrAndSearchParameters[] = "tr.description LIKE '%" . stripslashes( $objTaskReleaseFilter->getDescription() ) . "%'";
		if( false == empty( $strReleaseDatetime ) ) 			$arrstrAndSearchParameters[] = "to_char(tr.release_datetime,'mm/dd/yyyy') = '" . trim( $objTaskReleaseFilter->getReleaseDatetime() ) . "' ";
		if( false == empty( $strNotes ) ) 						$arrstrAndSearchParameters[] = "tr.notes LIKE '%" . stripslashes( $objTaskReleaseFilter->getNotes() ) . "%'";
		if( false == empty( $strSystemDownOn ) ) 				$arrstrAndSearchParameters[] = "to_char(`system_down_on,'mm/dd/yyyy') = '" . trim( $objTaskReleaseFilter->getSystemDownOn() ) . "' ";
		if( false == empty( $strSystemUpOn ) ) 					$arrstrAndSearchParameters[] = "to_char(system_up_on,'mm/dd/yyyy') = '" . trim( $objTaskReleaseFilter->getSystemDownOn() ) . "' ";
		if( false == empty( $strIsPublished ) ) 				$arrstrAndSearchParameters[] = 'tr.is_published = ' . ( int ) $objTaskReleaseFilter->getIsPublished();

		if( false == empty( $strKeyword ) ) {
			$arrstrAndSearchParameters[] = "
				(	tr.name ILIKE '%" . addslashes( $objTaskReleaseFilter->sqlKeyword() ) . "%'
			 	OR  tr.description ILIKE '%" . addslashes( $objTaskReleaseFilter->sqlKeyword() ) . "%'
			 	OR 	tr.notes ILIKE '%" . addslashes( $objTaskReleaseFilter->sqlKeyword() ) . "%' ) ";
		}

		if( false == is_null( $objTaskReleaseFilter->getStartReleaseDatetime() ) && false == is_null( $objTaskReleaseFilter->getEndReleaseDatetime() ) ) {
			$arrstrAndSearchParameters[] = "tr.release_datetime >= '" . date( 'Y-m-d', strtotime( $objTaskReleaseFilter->getStartReleaseDatetime() ) ) . " '";
			$arrstrAndSearchParameters[] = "tr.release_datetime <= '" . date( 'Y-m-d', strtotime( $objTaskReleaseFilter->getEndReleaseDatetime() ) ) . " '";
		}

		if( false == is_null( $objTaskReleaseFilter->getStartReleaseDatetime() ) && true == is_null( $objTaskReleaseFilter->getEndReleaseDatetime() ) ) {
			$arrstrAndSearchParameters[] = "tr.release_datetime = '" . date( 'Y-m-d', strtotime( $objTaskReleaseFilter->getStartReleaseDatetime() ) ) . " '";
		}

		if( true == is_null( $objTaskReleaseFilter->getStartReleaseDatetime() ) && false == is_null( $objTaskReleaseFilter->getEndReleaseDatetime() ) ) {
			$arrstrAndSearchParameters[] = "tr.release_datetime = '" . date( 'Y-m-d', strtotime( $objTaskReleaseFilter->getEndReleaseDatetime() ) ) . " '";
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
			return $arrstrAndSearchParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchPaginatedTaskReleasesCount( $intPageNo, $intPageSize, $objTaskReleaseFilter = NULL, $objDatabase ) {

		$intOffset 	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit 	= $intPageSize;

		if( true == valObj( $objTaskReleaseFilter, 'CTaskReleaseFilter' ) ) {
			$arrstrAndSearchParameters = self::fetchSearchCriteria( $objTaskReleaseFilter );
		}

		$strSql = ' SELECT
						count(tr.id)
					FROM
						task_releases tr' .
						( ( true == valArr( $arrstrAndSearchParameters ) ) ? ' WHERE ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchAllTaskReleases( $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL, $boolIncludeMobile = false ) {

		$strOrderByField = ( true == is_null( $strOrderByField ) )? 'release_datetime' : $strOrderByField;
		$strOrderByType  = ( true == is_null( $strOrderByType ) )? 'desc' : $strOrderByType;

		$strWhereCondition = '';
		if( false == $boolIncludeMobile ) {
			$$strWhereCondition = ' WHERE cluster_id NOT IN ( ' . CCluster::MOBILE . ' )';
		}
		$strSql = ' SELECT
						*
					FROM
						task_releases ' . $strWhereCondition . '
					ORDER BY ' . $strOrderByField . ' ' . $strOrderByType;

		return self::fetchTaskReleases( $strSql, $objDatabase );
	}

	public static function fetchTaskReleasesByIds( $arrintTaskReleasesIds, $objDatabase ) {
		if( false == valArr( $arrintTaskReleasesIds ) ) return NULL;
		return self::fetchTaskReleases( 'SELECT * FROM task_releases WHERE id IN ( ' . implode( ',', $arrintTaskReleasesIds ) . ' ) ORDER BY id DESC ', $objDatabase );
 	}

	public static function fetchUnPublishedTaskReleases( $objDatabase ) {

		$intCurrentMonth = date( 'm' );
		$intCurrentYear	 = date( 'Y' );

		$strSql = 'SELECT
						 *
					FROM
						task_releases
					WHERE
						is_published <> 1
					AND
						release_datetime BETWEEN

						TIMESTAMP \'' . ( int ) $intCurrentMonth . '/01/' . ( int ) $intCurrentYear . '\' - INTERVAL \'1 MONTH \'
					AND
						now() + INTERVAL \'6 MONTH\' ORDER BY release_datetime';

		return self::fetchTaskReleases( $strSql, $objDatabase );
	}

	public static function fetchPreviousTaskReleasesByLimit( $intLimit, $objDatabase, $boolIsCurrentRelease = false ) {

		$strWhereCondition = ( false == $boolIsCurrentRelease ) ? 'tr.is_released = 1' : 'tr.id <= ( SELECT min ( id ) FROM  task_releases WHERE is_released = 0 )';

		$strSql = 'SELECT
						tr.id,
						tr.release_datetime::DATE AS release_date,
						(
							SELECT
								MAX ( release_datetime ) + INTERVAL \'1 day\'
							FROM
								task_releases
							WHERE
								release_datetime < tr.release_datetime
						)::DATE AS start_release_date
					FROM
						task_releases tr
					WHERE
						' . $strWhereCondition . '
						AND cluster_id NOT IN (' . CCluster::MOBILE . ')
					ORDER BY
						tr.release_datetime DESC
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestTaskRelease( $objAdminDatabase ) {
		return self::fetchTaskRelease( 'SELECT * FROM task_releases WHERE release_datetime >= \'' . date( 'Y-m-d' ) . '\' AND cluster_id NOT IN ( ' . CCluster::MOBILE . ') ORDER BY release_datetime LIMIT 1', $objAdminDatabase );
	}

	public static function fetchLatestTaskReleaseByClusterId( $intClusterId, $objAdminDatabase ) {
		return self::fetchTaskRelease( 'SELECT * FROM task_releases WHERE cluster_id = ' . ( int ) $intClusterId . ' AND release_datetime >= \'' . date( 'Y-m-d' ) . '\' ORDER BY release_datetime LIMIT 1', $objAdminDatabase );
	}

	public static function fetchLatestTaskReleasesByClusterId( $intClusterId, $objAdminDatabase ) {
		return self::fetchTaskReleases( 'SELECT * FROM task_releases WHERE cluster_id = ' . ( int ) $intClusterId . ' AND release_datetime >= \'' . date( 'Y-m-d' ) . '\' ORDER BY id', $objAdminDatabase );
	}

	public static function fetchUpcomingTaskReleases( $objAdminDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						task_releases
					WHERE
						DATE ( release_datetime ) = DATE ( (
															SELECT
																release_datetime
															FROM
																task_releases
															WHERE
																DATE ( release_datetime ) >= \'' . date( ' Y-m-d ' ) . '\'
															AND is_released = 0
															AND cluster_id NOT IN ( ' . CCluster::MOBILE . ')
														ORDER BY
															release_datetime
														LIMIT 1
			 										) )
				ORDER BY
					release_datetime';

		return self::fetchTaskReleases( $strSql, $objAdminDatabase );

	}

	public static function fetchTaskReleasesByReleaseDatetime( $strStartReleaseDate, $strEndReleaseDate, $objAdminDatabase, $boolIncludeMobile = false ) {
		$strCondition = ( false == $boolIncludeMobile ) ? ' AND cluster_id NOT IN ( ' . CCluster::MOBILE . ')' : '';

		return self::fetchTaskReleases( ' SELECT * FROM task_releases WHERE release_datetime >= \'' . trim( $strStartReleaseDate ) . '\' AND release_datetime <= \'' . trim( $strEndReleaseDate ) . '\' ' . $strCondition . ' ORDER BY release_datetime DESC ', $objAdminDatabase );
	}

	public static function fetchLatestReleasedTaskRelease( $objDatabase, $boolIncludeMobile = false ) {

		$strWhereCondition = ( $boolIncludeMobile == false ) ? 'AND cluster_id !=' . CCluster::MOBILE : '';

		$strSql = 'SELECT * FROM task_releases WHERE is_released = 1' . $strWhereCondition . ' ORDER BY id DESC LIMIT 1';

		return self::fetchTaskRelease( $strSql, $objDatabase );
	}

	public static function fetchLatestReleasedTaskReleaseByClusterId( $intClusterId, $objDatabase ) {
		$strSql = 'SELECT * FROM task_releases WHERE is_released = 1 AND cluster_id = ' . ( int ) $intClusterId . ' ORDER BY release_datetime DESC LIMIT 1';

		return self::fetchTaskRelease( $strSql, $objDatabase );
	}

	public static function fetchTaskReleasesByIsReleased( $intIsReleased, $objDatabase ) {
		return self::fetchTaskReleases( sprintf( 'SELECT * FROM task_releases WHERE is_released = %d', ( int ) $intIsReleased ), $objDatabase );
	}

	public static function fetchPreviousTaskReleaseByReleaseDatetime( $strCurrentReleaseDate, $objAdminDatabase ) {

		if( true == empty( $strCurrentReleaseDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM task_releases WHERE release_datetime < \'' . $strCurrentReleaseDate . '\' AND cluster_id IN( ' . CCluster::RAPID . ', ' . CCluster::STANDARD . ' ) ORDER BY release_datetime  DESC LIMIT 1';

		return self::fetchTaskRelease( $strSql, $objAdminDatabase );
	}

	public static function fetchCurrentTaskReleases( $objDatabase ) {

		return self::fetchTaskReleases( 'SELECT
												*
											FROM
												public.task_releases tr
											WHERE
												release_datetime <=
												(
													SELECT
														release_datetime
													FROM
														public.task_releases tr
													WHERE
														release_datetime >= CURRENT_DATE
													ORDER BY
														release_datetime ASC
													LIMIT
														1
												)
												AND tr.cluster_id!=' . CCluster::MOBILE . '
											ORDER BY
												release_datetime DESC', $objDatabase );
	}

	public static function fetchCurrentRapidTaskReleases( $objDatabase ) {

		return self::fetchTaskReleases( 'SELECT
												*
											FROM
												public.task_releases tr
											WHERE
												release_datetime <=
												(
													SELECT
														release_datetime
													FROM
														public.task_releases tr
													WHERE
														release_datetime >= CURRENT_DATE
														AND cluster_id = ' . CCluster::RAPID . '
													ORDER BY
														release_datetime ASC
													LIMIT
														1
												) AND cluster_id = ' . CCluster::RAPID . '
											ORDER BY
												release_datetime DESC', $objDatabase );
	}

	public static function fetchTaskReleasesBetweenDates( $strStartDate, $strEndDate, $objDatabase, $boolIncludeMobile = false, $boolDescOrder =  false ) {

		$strWhereCondition = '';
		if( $boolIncludeMobile == false ) {
			$strWhereCondition = ' AND cluster_id NOT IN ( ' . CCluster::MOBILE . ' )';
		}

		if( $boolDescOrder == false ) {
			$strClauseCondition = ' ORDER BY id desc ';
		}

		$strSql = 'SELECT
						id,
						release_datetime::date,
						name,
						cluster_id,
						is_released
					 FROM
						public.task_releases tr
					 WHERE
						release_datetime::date <=\'' . $strEndDate . '\'::date
						AND release_datetime::date >= \'' . $strStartDate . '\'::date ' . $strWhereCondition . '
					' . $strClauseCondition;

		return self::fetchTaskReleases( $strSql, $objDatabase );
	}

	public static function fetchCustomRcaTaskReleasesBetweenDates( $strStartDate, $strEndDate, $objDatabase ) {
		return self::fetchTaskReleases( 'SELECT
										id,
										release_datetime::date,
										name,
										cluster_id
									 FROM
										public.task_releases tr
									 WHERE
										release_datetime::date <=\'' . $strEndDate . '\'::date
										AND release_datetime::date >= \'' . $strStartDate . '\'::date
										AND cluster_id = ' . CCluster::RAPID . '
									 ORDER BY
										release_datetime ASC ', $objDatabase );
	}

	public static function fetchTaskReleasesByDateById( $strReleaseDate, $intTaskReleaseId, $objDatabase ) {

		$strSql = 'SELECT
						*
					 FROM
						task_releases
					 WHERE
						release_datetime = \'' . $strReleaseDate . '\' AND id = ' . ( int ) $intTaskReleaseId;

		return self::fetchTaskReleases( $strSql, $objDatabase );
	}

}
?>