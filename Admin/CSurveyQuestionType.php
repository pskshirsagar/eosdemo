<?php

class CSurveyQuestionType extends CBaseSurveyQuestionType {

	const RATING					= 1;
	const RATING_W_OPTIONAL_TEXT	= 2;
	const SELECT_BOX				= 3;
	const INPUT						= 4;
	const TEXT_AREA					= 5;
	const RADIO						= 6;
	const YES_NO					= 7;
	const WIDGET					= 8;
	const MATRIX					= 9;
	const PASSWORD					= 10;
	const CHECKBOX					= 11;

	public static $c_arrintJobPostingQuestionTypes = array(
		self::YES_NO,
		self::INPUT,
		self::TEXT_AREA
	);

	/**
	 * If you are making any changes in getOrAssignSmartySurveyQuestionTypeToString function then
	 * please make sure the same changes would be applied to getOrAssignTemplateSurveyQuestionTypeToString function also.
	 */

	public function getOrAssignSmartySurveyQuestionTypeToString( $objSmarty = NULL ) {

		$arrmixSureyQuestionTypeToString = array(
			self::INPUT						=> 'SURVEY_QUESTION_TYPE_INPUT',
			self::RADIO						=> 'SURVEY_QUESTION_TYPE_RADIO',
			self::YES_NO					=> 'SURVEY_QUESTION_TYPE_YES_NO',
			self::WIDGET					=> 'SURVEY_QUESTION_TYPE_WIDGET',
			self::MATRIX					=> 'SURVEY_QUESTION_TYPE_MATRIX',
			self::PASSWORD					=> 'SURVEY_QUESTION_TYPE_PASSWORD',
			self::CHECKBOX					=> 'SURVEY_QUESTION_TYPE_CHECKBOX',
			self::TEXT_AREA					=> 'SURVEY_QUESTION_TYPE_TEXT_AREA',
			self::SELECT_BOX				=> 'SURVEY_QUESTION_TYPE_SELECT_BOX',
			self::RATING					=> 'SURVEY_QUESTION_TYPE_RATING',
			self::RATING_W_OPTIONAL_TEXT	=> 'SURVEY_QUESTION_TYPE_RATING_W_OPTIONAL_TEXT'

		);

		if( true == is_null( $objSmarty ) ) {
			return $arrmixSureyQuestionTypeToString;
		}

		$objSmarty->assign( 'survey_question_type_names', $arrmixSureyQuestionTypeToString );
	}

	public function getOrAssignTemplateSurveyQuestionTypeToString( $arrmixTemplateParameters = NULL ) {

		$arrmixSureyQuestionTypeToString = array(
			self::INPUT						=> 'SURVEY_QUESTION_TYPE_INPUT',
			self::RADIO						=> 'SURVEY_QUESTION_TYPE_RADIO',
			self::YES_NO					=> 'SURVEY_QUESTION_TYPE_YES_NO',
			self::WIDGET					=> 'SURVEY_QUESTION_TYPE_WIDGET',
			self::MATRIX					=> 'SURVEY_QUESTION_TYPE_MATRIX',
			self::PASSWORD					=> 'SURVEY_QUESTION_TYPE_PASSWORD',
			self::CHECKBOX					=> 'SURVEY_QUESTION_TYPE_CHECKBOX',
			self::TEXT_AREA					=> 'SURVEY_QUESTION_TYPE_TEXT_AREA',
			self::SELECT_BOX				=> 'SURVEY_QUESTION_TYPE_SELECT_BOX',
			self::RATING					=> 'SURVEY_QUESTION_TYPE_RATING',
			self::RATING_W_OPTIONAL_TEXT	=> 'SURVEY_QUESTION_TYPE_RATING_W_OPTIONAL_TEXT'
		);

		if( true == is_null( $arrmixTemplateParameters ) ) {
			return $arrmixSureyQuestionTypeToString;
		}

		$arrmixTemplateParameters['survey_question_type_names']		= $arrmixSureyQuestionTypeToString;
		return $arrmixTemplateParameters;
	}
}
?>