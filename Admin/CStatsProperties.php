<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsProperties
 * Do not add any new functions to this class.
 */

class CStatsProperties extends CBaseStatsProperties {

	public static function fetchStatsProperties( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CStatsProperty', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchStatsProperty( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CStatsProperty', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function rebuildPropertyStats( $intPropertyId, $strStartDate, $strEndDate, $objAdminDatabase, $objConnectDatabase ) {

		set_time_limit( 600 );

		if( true == is_numeric( $intPropertyId ) ) {
			$strPropertyCondition = ' = ' . $intPropertyId;
		} else {
			$strPropertyCondition = ' <> 0 ';
		}

		$strSql = '
			CREATE TEMPORARY TABLE stats_properties_temp (
				property_id INTEGER NOT NULL,
				month DATE NOT NULL,
				active_leases INTEGER DEFAULT 0 NOT NULL
			)
			ON COMMIT DROP;';

		$arrobjClientDatabases = CDatabases::fetchClientDatabasesByDatabaseUserTypeIdByDatabaseTypeId( CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseType::CLIENT, $objConnectDatabase );

		foreach( $arrobjClientDatabases AS $objClientDatabase ) {

			if( true == $objClientDatabase->validate( 'validate_open' ) && false == $objClientDatabase->open() ) {
				print 'Couldn\'t open database.';
				return;
			}

			$strEntrataSql = self::buildPropertyStatsEntrataActiveLeasesSql( $strPropertyCondition, $strStartDate, $strEndDate );

			$arrmixData = fetchData( $strEntrataSql, $objClientDatabase );

			$strSqlValues = '';

			foreach( $arrmixData as $arrmixDataRow ) {
				$arrmixDataRow['month'] = '\'' . $arrmixDataRow['month'] . '\'';

				if( 0 < strlen( $strSqlValues ) ) {
					$strSqlValues .= ',';
				}

				$strSqlValues .= '(' . implode( ',', $arrmixDataRow ) . ')';
			}

			if( 0 < strlen( $strSqlValues ) ) {
				$strSql .= '
				/* data from ' . $objClientDatabase->getDatabaseName() . ' */
				INSERT INTO stats_properties_temp
				VALUES ' . $strSqlValues . ';';
			}
		}

		$strSql .= '
			INSERT INTO stats_properties (
				property_id,
				month,
				created_by,
				created_on
			)
			SELECT
				spt.property_id,
				spt.month,
				1 AS created_by,
				NOW() AS created_on
			FROM
				stats_properties_temp spt
				LEFT JOIN stats_properties sp ON sp.property_id = spt.property_id AND sp.month = spt.month
			WHERE
				sp.id IS NULL

			UPDATE
				stats_properties sp
			SET
				active_leases = data.active_leases
			FROM
				stats_properties_temp data
			WHERE
				sp.property_id = data.property_id
				AND sp.month = data.month;';

// 		display( $strSql );
		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function buildPropertyStatsEntrataActiveLeasesSql( $strPropertyCondition, $strStartDate, $strEndDate ) {
		$strSql = '
			SELECT
				l.property_id,
				generate_series( li.lease_start_date, LEAST( li.lease_end_date, NOW() ), \'1 month\' )::DATE AS month,
				COUNT(*) AS lease_count
			FROM leases l
				JOIN lease_intervals li ON li.cid = l.cid AND li.lease_id = l.id
			WHERE
				l.cid <> 0
				AND l.property_id ' . $strPropertyCondition . '
				AND li.lease_start_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
			GROUP BY
				l.property_id,
				month;';

		return $strSql;
	}
}
?>