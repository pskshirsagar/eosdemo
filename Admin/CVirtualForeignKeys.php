<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CVirtualForeignKeys
 * Do not add any new functions to this class.
 */

class CVirtualForeignKeys extends CBaseVirtualForeignKeys {

	public static function fetchVirtualForeignKeysByFrequencyIds( $arrintFrequencyIds, $objDatabase ) {
		if( false == valArr( $arrintFrequencyIds ) )  return NULL;

		$strSql = 'SELECT * FROM virtual_foreign_keys WHERE frequency_id IN ( ' . implode( ',', $arrintFrequencyIds ) . ' )  ORDER BY id';
		return self::fetchVirtualForeignKeys( $strSql, $objDatabase );
	}

	public static function fetchPaginatedVirtualForeignKeys( $intPageNo, $intPageSize, $objDatabase, $objVirtualForeignKeysFilter ) {

		$arrstrWhereParameters = array();
		$strFreeSearchValue = trim( $objVirtualForeignKeysFilter->getFreeSearchValue() );

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;
		$strSubSql = '';

		$strSql = 'SELECT
						s.*,
						COALESCE( SUM ( s.error_count ) OVER ( PARTITION BY ( s.virtual_foreign_key_id, s.log_datetime ) ), 0 ) as total_error_count
					FROM
						(
						SELECT
							vfk.*,
							f.name as frequency_name,
							vfkl.virtual_foreign_key_id,
							vfkl.log_datetime,
							vfkl.error_count,
							rank() OVER ( PARTITION BY vfk.id
						ORDER BY
							vfkl.log_datetime DESC ) AS rank
						FROM
							virtual_foreign_keys vfk
							JOIN frequencies f ON ( f.id = vfk.frequency_id )
							LEFT JOIN virtual_foreign_key_logs vfkl ON ( vfkl.virtual_foreign_key_id = vfk.id )
						) AS s
					WHERE
						rank = 1';

		if( false == empty( $strFreeSearchValue ) ) {
			$strSql .= ' AND ( ';
			$strSql .= '	s.id = ' . $strFreeSearchValue . '::integer ';
			$strSql .= '	OR s.source_table_name ilike \'%' . $strFreeSearchValue . '%\'';
			$strSql .= '	OR s.foreign_table_name ilike \'%' . $strFreeSearchValue . '%\'';
			$strSql .= ' )';
		}

		$strOrderByField = ( true == valStr( $objVirtualForeignKeysFilter->getOrderByField() ) ) ? $objVirtualForeignKeysFilter->getOrderByField() : 's.id';
		$strOrderByType  = ( true == valStr( $objVirtualForeignKeysFilter->getOrderByType() ) ) ? $objVirtualForeignKeysFilter->getOrderByType() : '';

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$strSubSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		$strSql .= ' ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType . $strSubSql;

		return self::fetchVirtualForeignKeys( $strSql, $objDatabase );
	}

	public static function fetchPaginatedVirtualForeignKeysCount( $objDatabase ) {

		$strSql = 'SELECT
					   count(*)
					FROM
						virtual_foreign_keys vfk
						JOIN frequencies f ON ( f.id = vfk.frequency_id )
						LEFT JOIN virtual_foreign_key_logs vfkl ON ( vfkl.virtual_foreign_key_id = vfk.id )';

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;

	}

	public static function fetchVirtualForeignKeyDetailById( $intVirtualForeignKeyId, $objDatabase ) {

		$strSql = 'SELECT
						s.id,
						s.name,
						s.last_log_datetime,
						s.source_database_type_id,
						source_table_name,
						source_fields,
						foreign_database_type_id,
						foreign_table_name,
						foreign_fields,
						SUM ( s.error_count ) AS total_error_count
					FROM
						(
						SELECT
							vfk.*,
							vfkl.log_datetime AS last_log_datetime,
							vfkl.error_count,
							rank ( ) OVER ( PARTITION BY vfk.id
						ORDER BY
							vfkl.log_datetime DESC ) AS rank
						FROM
							virtual_foreign_keys vfk
							JOIN virtual_foreign_key_logs vfkl ON ( vfkl.virtual_foreign_key_id = vfk.id )
						) AS s
					WHERE
						s.id = ' . ( int ) $intVirtualForeignKeyId . '
						AND rank = 1
					GROUP BY
						s.id,
						s.name,
						s.last_log_datetime,
						s.source_database_type_id,
						source_table_name,
						source_fields,
						foreign_database_type_id,
						foreign_table_name,
						foreign_fields';

		return self::fetchVirtualForeignKey( $strSql, $objDatabase );
	}
}
?>