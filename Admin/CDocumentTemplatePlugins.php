<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplatePlugins
 * Do not add any new functions to this class.
 */

class CDocumentTemplatePlugins extends CBaseDocumentTemplatePlugins {

	public static function fetchDocumentTemplatePlugins( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDocumentTemplatePlugin', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchDocumentTemplatePlugin( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDocumentTemplatePlugin', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

}
?>