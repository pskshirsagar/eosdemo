<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdGroupSources
 * Do not add any new functions to this class.
 */

class CSemAdGroupSources extends CBaseSemAdGroupSources {

	public static function fetchSemAdGroupSourceBySemSourceIdBySemCampaignIdByRemotePrimaryKey( $intSemSourceId, $intSemCampaignId, $strRemotePrimaryKey, $objAdminDatabase ) {

		$strSql = 'SELECT
						sags.*,
						sag.name
					FROM
						sem_ad_group_sources sags,
						sem_ad_groups sag
					WHERE
						sags.sem_ad_group_id = sag.id
					AND
						sag.sem_location_type_id  IN ( ' . CSemLocationType::CITY . ',' . CSemLocationType::CUSTOM . ', ' . CSemLocationType::STATE . ' )
					AND
						sags.remote_primary_key = \'' . ( string ) addslashes( $strRemotePrimaryKey ) . '\'
					AND
						sags.sem_source_id = ' . ( int ) $intSemSourceId . '
					AND
						sag.sem_campaign_id = ' . ( int ) $intSemCampaignId;

		return self::fetchSemAdGroupSource( $strSql, $objAdminDatabase );

	}

	public static function fetchSemAdGroupSourceBySemSourceIdBySemCampaignIdByName( $intSemSourceId, $intSemCampaignId, $strName, $objAdminDatabase ) {

		$strSql = 'SELECT
						sags.*,
						sag.name
					FROM
						sem_ad_group_sources sags,
						sem_ad_groups sag
					WHERE
						sags.sem_ad_group_id = sag.id
					AND
						sag.sem_location_type_id  IN ( ' . CSemLocationType::CITY . ',' . CSemLocationType::CUSTOM . ', ' . CSemLocationType::STATE . ' )
					AND
						sag.name = \'' . ( string ) addslashes( $strName ) . '\'
					AND
						sags.sem_source_id = ' . ( int ) $intSemSourceId . '
					AND
						sag.sem_campaign_id = ' . ( int ) $intSemCampaignId . '
					AND
						sag.is_published = 1
					AND
						sag.deleted_on IS NULL
					AND
						sag.deleted_by IS NULL';

		return self::fetchSemAdGroupSource( $strSql, $objAdminDatabase );

	}

	public static function fetchSemAdGroupSourcesBySemAccountIdBySemSourceId( $intSemAccountId, $intSemSourceId, $objADminDatabase ) {
		return self::fetchSemAdGroupSources( sprintf( 'SELECT * FROM sem_ad_group_sources WHERE sem_account_id = %d AND sem_source_id = %d', ( int ) $intSemAccountId, ( int ) $intSemSourceId ), $objADminDatabase );
	}

	public static function fetchSyncReadySemAdGroupSourcesBySemAccountIdBySemSourceId( $intSemAccountId, $intSemSourceId, $objAdminDatabase ) {

		$strSql = 'SELECT
								sags.*,
								sag1.name,
								sag1.is_system,
								sag1.deleted_by,
								sag1.deleted_on,
								sag1.updated_on as sem_ad_group_updated_on,
								scs.remote_primary_key AS sem_campign_remote_primary_key,
								COALESCE( inner_table.associated_properties_count, 0 ) AS associated_properties_count
						FROM
						sem_ad_groups sag1
						LEFT OUTER JOIN
							(
									SELECT
											sag.id, count(p.id) as associated_properties_count
									FROM
											sem_ad_groups sag
									JOIN sem_ad_group_sources sags ON (sag.id = sags.sem_ad_group_id AND sags.sem_account_id = ' . ( int ) $intSemAccountId . ' AND sags.sem_source_id = ' . ( int ) $intSemSourceId . ' )
									JOIN sem_ad_group_associations saga ON ( sag.id = saga.sem_ad_group_id )
									JOIN sem_ad_group_association_sources sagas ON ( saga.id = sagas.sem_ad_group_association_id AND sagas.sem_source_id = ' . ( int ) $intSemSourceId . ' )
									JOIN properties p ON ( saga.property_id = p.id AND p.is_disabled <> 1 AND p.property_type_id = ' . CPropertyType::APARTMENT . ' )
									JOIN sem_property_details spd ON ( spd.property_id = p.id AND spd.sem_property_status_type_id = ' . CSemPropertyStatusType::ENABLED . ' AND spd.longitude IS NOT NULL AND spd.latitude IS NOT NULL )
									JOIN sem_settings ss ON ( ss.property_id = p.id )
									JOIN postal_codes pc ON ( pc.postal_code = spd.postal_code AND pc.longitude IS NOT NULL AND pc.latitude IS NOT NULL )
									LEFT OUTER JOIN sem_budgets sb ON ( p.id = sb.property_id AND DATE_TRUNC ( \'month\', budget_date ) = DATE_TRUNC ( \'month\', NOW() ))
									WHERE
										sags.sync_requested_on IS NOT NULL
									AND
										sag.sem_location_type_id  IN ( ' . CSemLocationType::CITY . ', ' . CSemLocationType::CUSTOM . ', ' . CSemLocationType::STATE . ' )
									AND
										sb.daily_remaining_budget > 0
									GROUP BY sag.id
									) AS inner_table ON ( sag1.id = inner_table.id )
						JOIN sem_ad_group_sources sags ON (sag1.id = sags.sem_ad_group_id AND sags.sem_account_id = ' . ( int ) $intSemAccountId . ' AND sags.sem_source_id = ' . ( int ) $intSemSourceId . ' )
						JOIN sem_campaign_sources scs ON ( scs.sem_campaign_id = sag1.sem_campaign_id AND scs.sem_source_id = sags.sem_source_id AND sags.sem_account_id = scs.sem_account_id AND scs.sem_account_id = ' . ( int ) $intSemAccountId . ' AND scs.sem_source_id = ' . ( int ) $intSemSourceId . ' )
						WHERE
							sags.sync_requested_on IS NOT NULL
						AND
							sag1.sem_location_type_id  IN ( ' . CSemLocationType::CITY . ', ' . CSemLocationType::CUSTOM . ', ' . CSemLocationType::STATE . ' )';

		// $strSql = 'SELECT
						  // sags.*,
						  // sag.name,
						 // scs.remote_primary_key AS sem_campign_remote_primary_key,
						 // 1 AS associated_properties_count
						// FROM
						//	 sem_ad_groups sag
						// JOIN sem_ad_group_sources sags ON (sag.id = sags.sem_ad_group_id AND sags.sem_account_id = ' . ( int ) $intSemAccountId . ' AND sags.sem_source_id = ' . ( int ) $intSemSourceId . ' )
						// JOIN sem_campaign_sources scs ON ( scs.sem_campaign_id = sag.sem_campaign_id AND scs.sem_source_id = sags.sem_source_id AND sags.sem_account_id = scs.sem_account_id AND sags.sem_account_id = ' . ( int ) $intSemAccountId . ' AND sags.sem_source_id = ' . ( int ) $intSemSourceId . ' )
						// WHERE
						//	sag.sync_requested_on IS NOT NULL
						// AND
						//	sag.longitude IS NOT NULL
						// AND
						//	sag.latitude IS NOT NULL
						// AND
						//	sag.sem_location_type_id  = ' . CSemLocationType::CITY . '
						// LIMIT 2';

		return self::fetchSemAdGroupSources( $strSql, $objAdminDatabase );
	}

}
?>