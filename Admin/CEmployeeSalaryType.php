<?php

class CEmployeeSalaryType extends CBaseEmployeeSalaryType {

	const HOURLY_BASED_EMPLOYEE			= '1';
	const SALARIED_EXEMPT_EMPLOYEE		= '2';
	const SALARIED_NON_EXEMPT_EMPLOYEE	= '3';

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				return false;
		}

		return $boolIsValid;
	}

}
?>