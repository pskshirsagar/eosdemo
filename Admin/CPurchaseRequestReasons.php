<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseRequestReasons
 * Do not add any new functions to this class.
 */

class CPurchaseRequestReasons extends CBasePurchaseRequestReasons {

	public static function fetchPurchaseRequestReasons( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CPurchaseRequestReason::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPurchaseRequestReason( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CPurchaseRequestReason::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>