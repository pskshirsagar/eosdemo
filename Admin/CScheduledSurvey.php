<?php

class CScheduledSurvey extends CBaseScheduledSurvey {

	protected $m_intPsLeadId;
	protected $m_strEmailAddress;
	protected $m_boolIsSelfEvalution;
	protected $m_intCid;
	protected $m_intDatabaseId;
	protected $m_intCompanyUserId;

	public function valStartDate() {

		$boolIsValid = true;

		if( true == isset( $this->m_strEndDate ) && false == isset( $this->m_strStartDate ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Please select start date.' ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {

		$boolIsValid = true;

		if( true == isset( $this->m_strEndDate ) && strtotime( $this->m_strStartDate ) > strtotime( $this->m_strEndDate ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date should be greater than start date.' ) );
		}
		return $boolIsValid;
	}

	public function valScheduledSurveyId( $objDatabase ) {

		$boolIsValid = true;

		if( 1 <= CSurveys::fetchSurveyCount( 'WHERE scheduled_survey_id = ' . ( int ) $this->getId(), $objDatabase ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Unable to delete scheduled survey as review is already scheduled. Delete surveys first if not submitted yet.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) && 1 > $this->m_intCid ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required' ) );
		}

		return $boolIsValid;
	}

	public function valPsLeadId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPsLeadId ) && 1 > $this->m_intPsLeadId ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Ps lead id is required' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valScheduledSurveyInsert( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valStartDate();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valScheduledSurveyId( $objDatabase );
				break;

			case 'VALIDATE_INSERT_PERSON':
				$boolIsValid &= $this->valCid( true );
				$boolIsValid &= $this->valPsLeadId( true );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valScheduledSurveyInsert( $objDatabase ) {

		$boolIsValid = true;

		if( true == $this->getSelfEvalution() ) {

			if( 1 <= CScheduledSurveys::fetchScheduledSurveysCount( 'WHERE subject_reference_id = responder_reference_id AND scheduled_survey_id = ' . ( int ) $this->getId(), $objDatabase ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Self evalution for employee already exists.' ) );

			}
		}
		return $boolIsValid;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['self_evalution'] ) )				$this->setSelfEvalution( $arrmixValues['self_evalution'] );
		if( true == isset( $arrmixValues['ps_lead_id'] ) )					$this->setPsLeadId( $arrmixValues['ps_lead_id'] );
		if( true == isset( $arrmixValues['email_address'] ) )				$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['cid'] ) )							$this->setCid( $arrmixValues['cid'] );
		if( true == isset( $arrmixValues['database_id'] ) )					$this->setDatabaseId( $arrmixValues['database_id'] );
		if( true == isset( $arrmixValues['company_user_id'] ) )				$this->setCompanyUserId( $arrmixValues['company_user_id'] );

		return;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->m_intPsLeadId = $intPsLeadId;
	}

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setSelfEvalution( $boolSelfEvalution ) {
		$this->m_boolIsSelfEvalution = $boolSelfEvalution;
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->m_intDatabaseId = $intDatabaseId;
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = $intCompanyUserId;
	}

	/**
	 * get Functions
	 */

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function getSelfEvalution() {
		return $this->m_boolIsSelfEvalution;
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	/**
	 * other Functions
	 */

	public function createSurvey() {
		$objSurvey 	= new CSurvey();

		$objSurvey->setPsLeadId( $this->getPsLeadId() );
		$objSurvey->setSurveyTypeId( $this->getSurveyTypeId() );
		$objSurvey->setSurveyDatetime( 'NOW()' );
		$objSurvey->setSurveyTemplateId( $this->getSurveyTemplateId() );
		$objSurvey->setScheduledSurveyId( $this->getId() );
		$objSurvey->setSubjectReferenceId( $this->getSubjectReferenceId() );
		$objSurvey->setResponderReferenceId( $this->getResponderReferenceId() );

		return $objSurvey;
	}

	public function declineEmployeeSurveyData( $intCurrentUserId, $intEmployeeId, $objDatabase ) {
		// If employee status type is changed from current to previous, then decline it's all scheduled surveys and open surveys

		$strSql = 'UPDATE
						surveys
					SET
						declined_datetime = NOW(),
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = NOW()
					WHERE
						( responder_reference_id = ' . ( int ) $intEmployeeId . ' OR subject_reference_id = ' . ( int ) $intEmployeeId . ' )
						AND response_datetime IS NULL
						AND declined_datetime IS NULL;

					UPDATE
						scheduled_surveys
					SET
						end_date = NOW(),
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = NOW()
					WHERE
						( responder_reference_id = ' . ( int ) $intEmployeeId . ' OR  subject_reference_id = ' . ( int ) $intEmployeeId . ' )
						AND ( end_date >= NOW() OR end_date IS NULL );';

		fetchData( $strSql, $objDatabase );

		return true;
	}

	public function declineSurveyData( $intUserId, $intSurveyTemplateId, $objDatabase ) {
		// If unpublished or delete last question on template then decline related template surveys
		$strSql = 'UPDATE
						surveys
					SET
						declined_datetime = NOW(),
						updated_by = ' . ( int ) $intUserId . ',
						updated_on = NOW()
					WHERE
						survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND response_datetime IS NULL
						AND declined_datetime IS NULL';

		fetchData( $strSql, $objDatabase );

		return true;
	}

    public function declineScheduledDependentSurveyData( $intUserId, $intScheduledSurveyId, $objDatabase ) {
        // If unpublished or delete last question on template then decline related template surveys
        $strSql = 'UPDATE
						surveys
					SET
						declined_datetime = NOW(),
						updated_by = ' . ( int ) $intUserId . ',
						updated_on = NOW()
					WHERE
						scheduled_survey_id = ' . ( int ) $intScheduledSurveyId . '
						AND response_datetime IS NULL
						AND declined_datetime IS NULL';

        fetchData( $strSql, $objDatabase );

        return true;
    }

}
?>