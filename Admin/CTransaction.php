<?php
use Psi\Eos\Admin\CContractProperties;
use Psi\Eos\Admin\CAllocations;
use Psi\Eos\Admin\CCommissions;
use Psi\Eos\Admin\CCommissionStructures;
use Psi\Eos\Admin\CCompanyPayments;


class CTransaction extends CBaseTransaction {

	protected $m_objClient;
	protected $m_intIsPaymentTransaction;
	protected $m_intIsBeginningBalanceTransaction;
	protected $m_intIsDepositTransaction;
	protected $m_intIsRefundTransaction;
	protected $m_intIsChargeTransaction;
	protected $m_intIsAdjustmentTransaction;
	protected $m_intIsCheckTransaction;
	protected $m_strChargeCodeName;
	protected $m_strPropertyName;
	protected $m_strChargeCode;
	protected $m_fltTransactionAmountApplied;
	protected $m_fltTransactionAmountDue;
	protected $m_intPaymentTypeId;
	protected $m_fltAmountApplied;
	protected $m_boolAutoAllocate;
	protected $m_strLateFeesLastPostedOn;
	protected $m_strMailInvoiceFeesLastPostedOn;
	protected $m_boolIsClientRunDonationTransaction;
	protected $m_intCompanyMerchantAccountId;
	protected $m_intClearingAccountId;
	protected $m_intProcessingBankAccountId;
	protected $m_strDeleteMemo;
	protected $m_strEftChargeDetails;
	protected $m_strBilltoNameFull;
	protected $m_strAccountName;
	protected $m_strCheckNumber;
	protected $m_fltPaymentAmount;
	protected $m_fltTotalAmountDue;
	protected $m_strBundleProductName;
	protected $m_intInvoiceNumber;

	/**
	 * Getters
	 *
	 */

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getCid() {

		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient->getId();
		} else {
			return parent::getCid();
		}
	}

	public function getTransactionAmountApplied() {
		return $this->m_fltTransactionAmountApplied;
	}

	public function getTransactionAmountDue() {
		return $this->m_fltTransactionAmountDue;
	}

	public function getIsCheckTransaction() {
		return $this->m_intIsCheckTransaction;
	}

	public function getIsAdjustmentTransaction() {
		return $this->m_intIsAdjustmentTransaction;
	}

	public function getIsPaymentTransaction() {
		return $this->m_intIsPaymentTransaction;
	}

	public function getIsBeginningBalanceTransaction() {
		return $this->m_intIsBeginningBalanceTransaction;
	}

	public function getIsDepositTransaction() {
		return $this->m_intIsDepositTransaction;
	}

	public function getIsRefundTransaction() {
		return $this->m_intIsRefundTransaction;
	}

	public function getIsChargeTransaction() {
		return $this->m_intIsChargeTransaction;
	}

	public function getChargeCodeName() {
		return $this->m_strChargeCodeName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getAmountApplied() {
		return $this->m_fltAmountApplied;
	}

	public function getTransactionDate() {
		if( true == isset( $this->m_strTransactionDatetime ) ) {
			$intTransactionDatetime = strtotime( $this->m_strTransactionDatetime );
			if( 0 <= $intTransactionDatetime ) {
				return date( 'm/d/Y', $intTransactionDatetime );
			}
		}
		return NULL;
	}

	public function getAutoAllocate() {
		return $this->m_boolAutoAllocate;
	}

	public function getLateFeesLastPostedOn() {
		return $this->m_strLateFeesLastPostedOn;
	}

	public function getMailInvoiceFeesLastPostedOn() {
		return $this->m_strMailInvoiceFeesLastPostedOn;
	}

	public function getIsClientRunDonationTransaction() {
		return $this->m_boolIsClientRunDonationTransaction;
	}

	public function getCompanyMerchantAccountId() {
		return $this->m_intCompanyMerchantAccountId;
	}

	public function getClearingAccountId() {
		return $this->m_intClearingAccountId;
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function getDeleteMemo() {
		return $this->m_strDeleteMemo;
	}

	public function getEftChargeDetails() {
		return $this->m_strEftChargeDetails;
	}

	public function getBilltoNameFull() {
		return $this->m_strBilltoNameFull;
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getChargeCode() {
		return $this->m_strChargeCode;
	}

	public function getTotalAmountDue() {
		return $this->m_fltTotalAmountDue;
	}

	public function getCheckNumber() {
		return $this->m_strCheckNumber;
	}

	public function getBundleProductName() {
		return $this->m_strBundleProductName;
	}

	public function getInvoiceNumber() {
		return $this->m_intInvoiceNumber;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_payment_transaction'] ) ) {
			$this->setIsPaymentTransaction( $arrmixValues['is_payment_transaction'] );
		}
		if( true == isset( $arrmixValues['is_beginning_balance_transaction'] ) ) {
			$this->setIsBeginningBalanceTransaction( $arrmixValues['is_beginning_balance_transaction'] );
		}
		if( true == isset( $arrmixValues['is_deposit_transaction'] ) ) {
			$this->setIsDepositTransaction( $arrmixValues['is_deposit_transaction'] );
		}
		if( true == isset( $arrmixValues['is_refund_transaction'] ) ) {
			$this->setIsRefundTransaction( $arrmixValues['is_refund_transaction'] );
		}
		if( true == isset( $arrmixValues['is_charge_transaction'] ) ) {
			$this->setIsChargeTransaction( $arrmixValues['is_charge_transaction'] );
		}
		if( true == isset( $arrmixValues['is_adjustment_transaction'] ) ) {
			$this->setIsAdjustmentTransaction( $arrmixValues['is_adjustment_transaction'] );
		}
		if( true == isset( $arrmixValues['is_check_transaction'] ) ) {
			$this->setIsCheckTransaction( $arrmixValues['is_check_transaction'] );
		}
		if( true == isset( $arrmixValues['charge_code_name'] ) ) {
			$this->setChargeCodeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['charge_code_name'] ) : $arrmixValues['charge_code_name'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_name'] ) : $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['transaction_amount_applied'] ) ) {
			$this->setTransactionAmountApplied( $arrmixValues['transaction_amount_applied'] );
		}
		if( true == isset( $arrmixValues['transaction_amount_due'] ) ) {
			$this->setTransactionAmountDue( $arrmixValues['transaction_amount_due'] );
		}
		if( true == isset( $arrmixValues['company_charge_id'] ) ) {
			$this->setCompanyChargeId( $arrmixValues['company_charge_id'] );
		}
		if( true == isset( $arrmixValues['transaction_date'] ) ) {
			$this->setTransactionDate( $arrmixValues['transaction_date'] );
		}
		if( true == isset( $arrmixValues['amount_applied'] ) ) {
			$this->setAmountApplied( $arrmixValues['amount_applied'] );
		}

		if( true == isset( $arrmixValues['payment_type_id'] ) ) {
			$this->setPaymentTypeId( $arrmixValues['payment_type_id'] );
		}

		if( true == isset( $arrmixValues['late_fees_last_posted_on'] ) ) {
			$this->setLateFeesLastPostedOn( $arrmixValues['late_fees_last_posted_on'] );
		}
		if( true == isset( $arrmixValues['mail_invoice_fees_last_posted_on'] ) ) {
			$this->setMailInvoiceFeesLastPostedOn( $arrmixValues['mail_invoice_fees_last_posted_on'] );
		}
		if( true == isset( $arrmixValues['is_client_run_donation_transaction'] ) ) {
			$this->setIsClientRunDonationTransaction( $arrmixValues['is_client_run_donation_transaction'] );
		}
		if( true == isset( $arrmixValues['company_merchant_account_id'] ) ) {
			$this->setCompanyMerchantAccountId( $arrmixValues['company_merchant_account_id'] );
		}
		if( true == isset( $arrmixValues['clearing_account_id'] ) ) {
			$this->setClearingAccountId( $arrmixValues['clearing_account_id'] );
		}
		if( true == isset( $arrmixValues['processing_bank_account_id'] ) ) {
			$this->setProcessingBankAccountId( $arrmixValues['processing_bank_account_id'] );
		}
		if( true == isset( $arrmixValues['delete_memo'] ) ) {
			$this->setDeleteMemo( $arrmixValues['delete_memo'] );
		}
		if( true == isset( $arrmixValues['eft_charge_details'] ) ) {
			$this->setEftChargeDetails( $arrmixValues['eft_charge_details'] );
		}
		if( true == isset( $arrmixValues['billto_name_full'] ) ) {
			$this->setBilltoNameFull( $arrmixValues['billto_name_full'] );
		}
		if( true == isset( $arrmixValues['payment_amount'] ) ) {
			$this->setPaymentAmount( $arrmixValues['payment_amount'] );
		}
		if( true == isset( $arrmixValues['ps_product_id'] ) ) {
			$this->setPsProductId( $arrmixValues['ps_product_id'] );
		}
		if( true == isset( $arrmixValues['account_name'] ) ) {
			$this->setAccountName( $arrmixValues['account_name'] );
		}
		if( true == isset( $arrmixValues['charge_code'] ) ) {
			$this->setChargeCode( $arrmixValues['charge_code'] );
		}
		if( true == isset( $arrmixValues['bundle_product_name'] ) ) {
			$this->setBundleProductName( $arrmixValues['bundle_product_name'] );
		}

		if( true == isset( $arrmixValues['total_amount_due'] ) ) {
			$this->setTotalAmountDue( $arrmixValues['total_amount_due'] );
		}
		if( true == isset( $arrmixValues['check_number'] ) ) {
			$this->setCheckNumber( $arrmixValues['check_number'] );
		}
		if( true == isset( $arrmixValues['invoice_number'] ) ) {
			$this->setInvoiceNumber( $arrmixValues['invoice_number'] );
		}

		return;
	}

	public function setDefaults() {

		$this->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
		$this->m_boolAutoAllocate = true;

		return;
	}

	public function setTransactionDate( $strTransactionDate ) {

		if( false == isset( $this->m_strTransactionDatetime ) ) {
			$this->m_strTransactionDatetime = date( 'm/d/Y H:i:s', strtotime( $strTransactionDate ) );
		} else {
			$strNewTransactionDate = date( 'm/d/Y', strtotime( $strTransactionDate ) );
			$strOldTransactionDate = date( 'm/d/Y', strtotime( $this->m_strTransactionDatetime ) );

			if( $strNewTransactionDate != $strOldTransactionDate ) {
				$this->m_strTransactionDatetime = $strNewTransactionDate;
			}
		}
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->m_intPaymentTypeId = $intPaymentTypeId;
	}

	public function setDeleteMemo( $strDeleteMemo ) {
		$this->m_strDeleteMemo = CStrings::strTrimDef( $strDeleteMemo, 240, NULL, true );
	}

	public function setEftChargeDetails( $strEftChargeDetails ) {
		$this->m_strEftChargeDetails = $strEftChargeDetails;
	}

	public function setBilltoNameFull( $strBilltoNameFull ) {
		$this->m_strBilltoNameFull = $strBilltoNameFull;
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->m_fltPaymentAmount = $fltPaymentAmount;
	}

	public function setTransactionAmountApplied( $fltTransactionAmountApplied ) {
		$this->m_fltTransactionAmountApplied = CStrings::strToFloatDef( $fltTransactionAmountApplied, NULL, true, 4 );
	}

	public function setTransactionAmountDue( $fltTransactionAmountDue ) {
		$this->m_fltTransactionAmountDue = CStrings::strToFloatDef( $fltTransactionAmountDue, NULL, true, 4 );
	}

	public function setIsPaymentTransaction( $intIsPaymentTransaction ) {
		$this->m_intIsPaymentTransaction = CStrings::strToIntDef( $intIsPaymentTransaction, NULL, true );
	}

	public function setAmountApplied( $fltAmountApplied ) {
		$this->m_fltAmountApplied = $fltAmountApplied;
	}

	public function setIsBeginningBalanceTransaction( $intIsBeginningBalanceTransaction ) {
		$this->m_intIsBeginningBalanceTransaction = CStrings::strToIntDef( $intIsBeginningBalanceTransaction, NULL, true );
	}

	public function setIsDepositTransaction( $intIsDepositTransaction ) {
		$this->m_intIsDepositTransaction = CStrings::strToIntDef( $intIsDepositTransaction, NULL, true );
	}

	public function setIsRefundTransaction( $intIsRefundTransaction ) {
		$this->m_intIsRefundTransaction = CStrings::strToIntDef( $intIsRefundTransaction, NULL, true );
	}

	public function setIsChargeTransaction( $intIsChargeTransaction ) {
		$this->m_intIsChargeTransaction = CStrings::strToIntDef( $intIsChargeTransaction, NULL, true );
	}

	public function setIsAdjustmentTransaction( $intIsAdjustmentTransaction ) {
		$this->m_intIsAdjustmentTransaction = CStrings::strToIntDef( $intIsAdjustmentTransaction, NULL, true );
	}

	public function setIsCheckTransaction( $intIsCheckTransaction ) {
		$this->m_intIsCheckTransaction = CStrings::strToIntDef( $intIsCheckTransaction, NULL, true );
	}

	public function setChargeCodeName( $strChargeCodeName ) {
		$this->m_strChargeCodeName = CStrings::strTrimDef( $strChargeCodeName, 240, NULL, true );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 240, NULL, true );
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->m_fltTransactionAmount = CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 4 );
	}

	public function setCid( $intCid ) {
		parent::setCid( $intCid );
		// If we are setting the client id directly we unbind the
		// client object to avoid any concurrency issues
		$this->m_objClient = NULL;
	}

	public function setClient( $objClient ) {
		if( false == valObj( $objClient, 'CClient' ) ) {
			trigger_error( 'Incompatible types: \'CClient\' and \'' . get_class( $objClient ) . '\' - CTransaction::setClient()', E_USER_ERROR );
			return false;
		}

		$this->m_objClient = $objClient;
		return true;
	}

	public function setAutoAllocate( $boolAutoAllocate ) {
		$this->m_boolAutoAllocate = $boolAutoAllocate;
	}

	public function setLateFeesLastPostedOn( $strLateFeesLastPostedOn ) {
		$this->m_strLateFeesLastPostedOn = CStrings::strTrimDef( $strLateFeesLastPostedOn, -1, NULL, true );
	}

	public function setMailInvoiceFeesLastPostedOn( $strMailInvoiceFeesLastPostedOn ) {
		$this->m_strMailInvoiceFeesLastPostedOn = CStrings::strTrimDef( $strMailInvoiceFeesLastPostedOn, -1, NULL, true );
	}

	public function setIsClientRunDonationTransaction( $boolIsClientRunDonationTransaction ) {
		$this->m_boolIsClientRunDonationTransaction = ( bool ) $boolIsClientRunDonationTransaction;
	}

	public function setCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {
		$this->m_intCompanyMerchantAccountId = ( int ) $intCompanyMerchantAccountId;
	}

	public function setClearingAccountId( $intClearingAccountId ) {
		$this->m_intClearingAccountId = ( int ) $intClearingAccountId;
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->m_intProcessingBankAccountId = ( int ) $intProcessingBankAccountId;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = ( int ) $intPsProductId;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setChargeCode( $strChargeCode ) {
		$this->m_strChargeCode = $strChargeCode;
	}

	public function setTotalAmountDue( $fltTotalAmountDue ) {
		$this->m_fltTotalAmountDue = $fltTotalAmountDue;
	}

	public function setCheckNumber( $strCheckNumber ) {
		$this->m_strCheckNumber = $strCheckNumber;
	}

	public function setBundleProductName( $strBundleProductName ) {
		$this->m_strBundleProductName = $strBundleProductName;
	}

	public function setInvoiceNumber( $intInvoiceNumber ) {
		$this->m_intInvoiceNumber = ( int ) $intInvoiceNumber;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchAssociatedTransaction( $objDatabase ) {

		if( false == is_numeric( $this->getTransactionId() ) ) {
			return NULL;
		}

		return \Psi\Eos\Admin\CTransactions::createService()->fetchTransactionById( $this->getTransactionId(), $objDatabase );
	}

	public function fetchCommissions( $objDatabase ) {

		return CCommissions::createService()->fetchCommissionsByCidByTransactionId( $this->getCid(), $this->getId(), $objDatabase, $boolGetActiveOnly = true );
	}

	public function fetchAccount( $objDatabase ) {

		return \Psi\Eos\Admin\CAccounts::createService()->fetchAccountById( $this->getAccountId(), $objDatabase );
	}

	public function fetchClient( $objDatabase ) {
		return \Psi\Eos\Admin\CClients::createService()->fetchClientById( $this->getCid(), $objDatabase );
	}

	public function fetchCompanyPayment( $objDatabase ) {
		return CCompanyPayments::createService()->fetchCompanyPaymentById( $this->m_intCompanyPaymentId, $objDatabase );
	}

	public function fetchActiveCommissions( $objDatabase ) {
		return CCommissions::createService()->fetchActiveCommissionsByTransactionId( $this->getId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valTransactionDatetime( $strDateFormat='MM/DD/YYYY' ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strTransactionDatetime ) || 0 == strlen( $this->m_strTransactionDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', 'Transaction datetime is required.' ) );
			return $boolIsValid;
		}

		$intTransactionTimestamp = strtotime( $this->m_strTransactionDatetime );
		if( false === $intTransactionTimestamp ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', 'Transaction datetime is not a valid date.' ) );
			return $boolIsValid;
		}

		if( false == CValidation::validateDate( date( 'm/d/Y', $intTransactionTimestamp ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', 'Transaction datetime is not formatted properly. (' . $strDateFormat . ')' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valAccountId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valChargeCodeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intChargeCodeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_id', 'Charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTransactionAmount() {
		$boolIsValid = true;

		if( false == isset( $this->m_fltTransactionAmount ) || false == is_numeric( $this->m_fltTransactionAmount ) || 0 == $this->m_fltTransactionAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'Transaction amount is required.' ) );
		}

		if( true == isset( $this->m_fltTransactionAmount ) && true == is_numeric( $this->m_fltTransactionAmount ) && 1000000000 < $this->m_fltTransactionAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'You cannot enter an amount greater than a billion dollars.' ) );
		}

		return $boolIsValid;
	}

	public function valMemo( $objDatabase ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strMemo ) ) {
			$objChargeCode = \Psi\Eos\Admin\CChargeCodes::createService()->fetchChargeCodeById( $this->getChargeCodeId(), $objDatabase );

			if( true == valObj( $objChargeCode, 'CChargeCode' ) ) {
				$this->setMemo( $objChargeCode->getName() );
			}
		}

		return $boolIsValid;
	}

	public function valInvoiceId() {
		$boolIsValid = true;

		if( true == isset( $this->m_intInvoiceId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_id', 'You cannot modify a transaction that has already been invoiced.' ) );
		}

		return $boolIsValid;
	}

	public function valIsCommissioned( $objDatabase ) {
		$boolIsValid = true;

		if( true == $this->getIsCommissioned() ) {
			$strSql = 'WHERE transaction_id =' . $this->getId() . ' AND commission_batch_id IS NOT NULL AND deleted_on IS NULL';
			$intCommissionsCount = CCommissions::createService()->fetchCommissionCount( $strSql, $objDatabase );

			if( 0 < $intCommissionsCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'You cannot modify a transaction that has already been commissioned.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valExportBatchId() {
		$boolIsValid = true;

		if( false == is_null( $this->getExportBatchId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'export_batch_id', 'You cannot modify a transaction that has already been exported.' ) );
		}

		return $boolIsValid;
	}

	public function valAlreadyDeleted() {
		$boolIsValid = true;

		if( false == is_null( $this->getTransactionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_id', 'You cannot delete a transaction that has already been deleted.' ) );
		}

		return $boolIsValid;
	}

	public function valTransferAmount() {
		$boolIsValid = true;

		if( false == isset( $this->m_fltTransactionAmount ) || false == is_numeric( $this->m_fltTransactionAmount ) || 0 == $this->m_fltTransactionAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'Transfer amount is required.' ) );
		}

		if( true == isset( $this->m_fltTransactionAmount ) && true == is_numeric( $this->m_fltTransactionAmount ) && 1000000000 < $this->m_fltTransactionAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'You cannot enter an amount greater than a billion dollars.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				break;

			case 'post_payment':
			case 'post_charge':
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valMemo( $objDatabase );
				break;

			case 'post_recurring_charge':
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valTransactionAmount();
				break;

			case 'update_charge':
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valMemo( $objDatabase );
				$boolIsValid &= $this->valInvoiceId();
				$boolIsValid &= $this->valExportBatchId();
				break;

			case 'update_one_time_invoice_id':
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valMemo( $objDatabase );
				$boolIsValid &= $this->valInvoiceId();
				break;

			case 'delete_charge':
				$boolIsValid &= $this->valAlreadyDeleted();
				break;

			case 'delete_payment':
				$boolIsValid &= $this->valInvoiceId();
				$boolIsValid &= $this->valExportBatchId();
				break;

			case 'bulk_post_charge':
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valTransactionDatetime();
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valTransactionAmount();
				break;

			case 'charge_transaction':
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valTransactionDatetime( 'YYYY/MM/DD' );
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valTransactionAmount();
				break;

			case 'balance_movement':
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valTransferAmount();
				break;

			case 'move_charge':
				$boolIsValid &= $this->valAccountId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function reverseWithCharge( $intUserId, $objDatabase, $objAccount = NULL, $intReversalChargeCodeId = NULL, $intCompanyPaymentId = NULL ) {

		// You cannot reverse a payment transaction with this function, unless you pass in a reversal charge code id that is not equal to "payment received"
		if( ( $this->getChargeCodeId() == CChargeCode::PAYMENT_RECEIVED && true == is_null( $intReversalChargeCodeId ) ) || CChargeCode::PAYMENT_RECEIVED == $intReversalChargeCodeId ) {
			trigger_error( 'You cannot reverse a transaction with an offsetting charge code id = Payment Received.', E_USER_ERROR );
			return false;
		}

		if( false == valObj( $objAccount, 'CAccount' ) ) {
			$objAccount = $this->fetchAccount( $objDatabase );
			if( false == valObj( $objAccount, 'CAccount' ) ) {
				trigger_error( 'Invalid account.', E_USER_ERROR );
			}
		}

		$strPropertyName = '';

		$intOffsetChargeCodeId = ( false == is_numeric( $intReversalChargeCodeId ) ) ? $this->getChargeCodeId() : $intReversalChargeCodeId;

		$objOffsetTransaction = $objAccount->createTransaction();
		$objOffsetTransaction->setDefaults();
		$objOffsetTransaction->setCompanyPaymentId( $intCompanyPaymentId );

		$objOffsetTransaction->setExportBatchId( NULL );
		$objOffsetTransaction->setInvoiceId( NULL );
		$objOffsetTransaction->setReferenceNumber( NULL );
		$objOffsetTransaction->setIsCommissioned( 0 );
		$objOffsetTransaction->setBundlePsProductId( $this->getBundlePsProductId() );
		$objOffsetTransaction->setChargeCodeId( $intOffsetChargeCodeId );
		$objOffsetTransaction->setTransactionId( $this->getId() );
		$objOffsetTransaction->setPostMonth( $this->getPostMonth() );
		$objOffsetTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
		$objOffsetTransaction->setTransactionAmount( $this->getTransactionAmount() * -1 );

		$intPropertyId = $this->getPropertyId();
		if( true == is_null( $intPropertyId ) && false == is_null( $this->getContractPropertyId() ) ) {
			$objContractProperty = CContractProperties::createService()->fetchContractPropertyById( $this->getContractPropertyId(), $objDatabase );
			if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
				$intPropertyId = $objContractProperty->getPropertyId();
			}
		}

		if( false == is_null( $intPropertyId ) ) {
			$arrmixProperty = \Psi\Eos\Admin\CProperties::createService()->fetchPropertyNamesByPropertyIdsByCid( array( $intPropertyId ), $this->getCid(), $objDatabase );
			if( true == valArr( $arrmixProperty ) ) {
				$strPropertyName = ' for property ' . $arrmixProperty[$intPropertyId];
			}
			$objOffsetTransaction->setPropertyId( $intPropertyId );
			$objOffsetTransaction->setContractPropertyId( $this->getContractPropertyId() );
		}

		$objOffsetTransaction->setMemo( 'Credit for ' . $this->getChargeCodeName() . $strPropertyName );

		$objOffsetTransaction->setInternalNotes( $this->getInternalNotes() );

		$objOffsetTransaction->setAutoAllocate( false );

		$objOffsetTransaction->setTransactionReversalTypeId( $this->getTransactionReversalTypeId() );

		// Post the adjustment charge/transaction on the company ledger
		if( false == $objOffsetTransaction->validate( VALIDATE_INSERT ) || false == $objOffsetTransaction->postCharge( $intUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Could not post an return adjustment charge on the company ledger for this payment.', NULL ) );
			return false;
		}

		return $this->loadCommonReverseTransactionData( $intUserId, $objDatabase, $objAccount, $objOffsetTransaction );
	}

	// This function lets you offset a payment transaction with another payment transaction. We can't allow someone
	// to call this function to offset a charge transaction with a payment transaction.

	public function reverseWithPayment( $intUserId, $objDatabase, $objAccount, $intCompanyPaymentId ) {

		// You cannot reverse a payment with this function, unless you pass in a reversal charge code id that is not equal to "payment received"
		if( $this->getChargeCodeId() != CChargeCode::PAYMENT_RECEIVED ) {
			trigger_error( 'You cannot reverse a charge with a payment.', E_USER_WARNING );
			return false;
		}

		if( false == valObj( $objAccount, 'CAccount' ) ) {
			$objAccount = $this->fetchAccount( $objDatabase );
		}

		if( false == $this->deleteAllocations( $objDatabase, $intUserId, true ) ) {
			trigger_error( 'Allocation failed to delete.', E_USER_WARNING );
			return false;
		}

		// We need to create an offsetting company payment record
		$objCompanyPayment = CCompanyPayments::createService()->fetchCompanyPaymentById( $intCompanyPaymentId, $objDatabase );

		if( false == valObj( $objCompanyPayment, 'CCompanyPayment' ) ) {
			trigger_error( 'Company payment object failed to load.' );
			exit;
		}

		$objOffsetCompanyPayment = clone $objCompanyPayment;
		$objOffsetCompanyPayment->setId( NULL );
		$objOffsetCompanyPayment->setInvoiceId( NULL );
		$objOffsetCompanyPayment->setDepositId( NULL );
		$objOffsetCompanyPayment->setPaymentTypeId( CPaymentType::CHECK );
		$objOffsetCompanyPayment->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
		$objOffsetCompanyPayment->setCheckDate( date( 'm/d/Y' ) );
		$objOffsetCompanyPayment->setPaymentDatetime( date( 'm/d/Y H:i:s' ) );
		$objOffsetCompanyPayment->setEftBatchedOn( NULL );
		$objOffsetCompanyPayment->setPaymentAmount( $objCompanyPayment->getPaymentAmount() * -1 );
		$objOffsetCompanyPayment->setPaymentMemo( 'Return offset for electronic payment id# ' . $objCompanyPayment->getId() );
		$objOffsetCompanyPayment->setChargeCodeId( CChargeCode::PAYMENT_RECEIVED );
		$objOffsetCompanyPayment->setUpdatedOn( NULL );
		$objOffsetCompanyPayment->setUpdatedBy( NULL );
		$objOffsetCompanyPayment->setCreatedOn( NULL );
		$objOffsetCompanyPayment->setCreatedBy( NULL );

		// Post the adjustment payment/transaction
		if( false == $objOffsetCompanyPayment->postRpcPayment( $intUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Could not post an offsetting company payment.', NULL ) );
			return false;
		}

		$objOffsetTransaction = $objAccount->createTransaction();
		$objOffsetTransaction->setDefaults();
		$objOffsetTransaction->setCompanyPaymentId( $objOffsetCompanyPayment->getId() );
		$objOffsetTransaction->setChargeCodeId( $this->getChargeCodeId() );
		$objOffsetTransaction->setTransactionId( $this->getId() );
		$objOffsetTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
		$objOffsetTransaction->setTransactionAmount( -1 * $this->getTransactionAmount() ); // sqlTransactionAmount Function makes transaction amount as negative
		$objOffsetTransaction->setMemo( 'Offset for transaction id# ' . $this->getId() );

		$objOffsetTransaction->setExportBatchId( NULL );
		$objOffsetTransaction->setInvoiceId( NULL );
		$objOffsetTransaction->setReferenceNumber( NULL );
		$objOffsetTransaction->setIsCommissioned( 0 );
		$objOffsetTransaction->setAutoAllocate( false );

		// Post the adjustment payment/transaction
		if( false == $objOffsetTransaction->validate( VALIDATE_INSERT ) || false == $objOffsetTransaction->postPayment( $intUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Could not post an return adjustment charge on the company ledger for this payment.', NULL ) );
			return false;
		}

		if( false == $this->loadCommonReverseTransactionData( $intUserId, $objDatabase, $objAccount, $objOffsetTransaction ) ) {
			return false;
		}

		return true;
	}

	public function loadCommonReverseTransactionData( $intUserId, $objDatabase, $objAccount, $objOffsetTransaction ) {

		// If this is a renters insurance premium charge, we should find the associated insurance_charges record and indicate the insurance premium charge
		// is reversed which indicates that the coverage originally planned was reversed, and the expected coverage didn't actually take place.
		// This is very important for our insurance system to function properly.

		if( true == in_array( $objAccount->getAccountTypeId(), CAccountType::getInsuranceAccountTypeIds() ) ) {
			if( CChargeCode::MARKEL_RI_PREMIUMS == $this->getChargeCodeId()
			    || CChargeCode::PSI_EARNED_PREMIUM == $this->getChargeCodeId()
			    || CChargeCode::RESIDENT_SECURE_CARRIER_LIABILITY == $this->getChargeCodeId() ) {

				$objInsuranceDatabase = CDatabases::createDatabase( CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseType::INSURANCE );
				$objInsuranceDatabase->begin();

				$arrobjInsuranceCharges = ( array ) \Psi\Eos\Insurance\CInsuranceCharges::createService()->fetchInsuranceChargesByPremiumTransactionId( $this->getId(), $objInsuranceDatabase );

				if( true == valArr( $arrobjInsuranceCharges ) ) {

					$arrintInsurancePolicyLiabilityChargeIds = array_filter( array_keys( rekeyObjects( 'insurancePolicyLiabilityChargeId', $arrobjInsuranceCharges ) ) );

					$arrobjAdjustedInsurancePolicyLiabilityCharges = [];
					if( true === valIntArr( $arrintInsurancePolicyLiabilityChargeIds ) ) {
						$arrobjAdjustedInsurancePolicyLiabilityCharges = \Psi\Eos\Insurance\CInsurancePolicyLiabilityCharges::createService()->fetchInsurancePolicyLiabilityChargesByInsurancePolicyLiabilityChargeIds( $arrintInsurancePolicyLiabilityChargeIds, $objInsuranceDatabase );
						$arrobjAdjustedInsurancePolicyLiabilityCharges = rekeyObjects( 'insurancePolicyLiabilityChargeId', $arrobjAdjustedInsurancePolicyLiabilityCharges );
					}

					foreach( $arrobjInsuranceCharges as $objInsuranceCharge ) {

						$objInsuranceCharge->setReturnedOn( date( 'm/d/Y H:i:s' ) );

						if( false == $objInsuranceCharge->update( $intUserId, $objInsuranceDatabase ) ) {
							$objInsuranceDatabase->rollback();
							trigger_error( 'Insurance charge failed to update.', E_USER_WARNING );
						} else {
							// we will add charge back entry in insurance_charges for adjustment
							$objAdjustedInsuranceCharge = new CInsuranceCharge();
							$objAdjustedInsuranceCharge->setEntityId( $objInsuranceCharge->getEntityId() );
							$objAdjustedInsuranceCharge->setInsurancePolicyId( $objInsuranceCharge->getInsurancePolicyId() );
							$objAdjustedInsuranceCharge->setChargeDatetime( date( 'm/d/Y H:i:s' ) );
							$objAdjustedInsuranceCharge->setChargeAmount( -1 * $objInsuranceCharge->getChargeAmount() );
							$objAdjustedInsuranceCharge->setPremiumTransactionId( $objOffsetTransaction->getId() );
							$objAdjustedInsuranceCharge->setEarnedPremium( -1 * $objInsuranceCharge->getEarnedPremium() );
							$objAdjustedInsuranceCharge->setLiabilityPremium( -1 * $objInsuranceCharge->getLiabilityPremium() );

							$intInsurancePolicyLiabilityChargeId = $objInsuranceCharge->getInsurancePolicyLiabilityChargeId();
							if( true === valArr( $arrobjAdjustedInsurancePolicyLiabilityCharges ) && true === valObj( $arrobjAdjustedInsurancePolicyLiabilityCharges[$intInsurancePolicyLiabilityChargeId], 'CInsurancePolicyLiabilityCharge' ) ) {
								$objAdjustedInsuranceCharge->setInsurancePolicyLiabilityChargeId( $arrobjAdjustedInsurancePolicyLiabilityCharges[$intInsurancePolicyLiabilityChargeId]->getId() );
							}

							$objAdjustedInsuranceCharge->setInsuranceChargeTypeId( CInsuranceChargeType::getChargeTypeByPaymentType( $objAccount->getPaymentTypeId(), true ) );

							if( false == $objAdjustedInsuranceCharge->insert( $intUserId, $objInsuranceDatabase ) ) {
								$objInsuranceDatabase->rollback();
								trigger_error( 'Insurance charge failed to insert for adjustment.', E_USER_WARNING );
							}
						}
					}
				} else {
					trigger_error( 'Insurance charge(s) failed to load.', E_USER_WARNING );
					return false;
				}

				$objInsuranceDatabase->commit();
			} elseif( CChargeCode::RI_PROVIDER_LIABILITY_AMOUNT == $this->getChargeCodeId() ) {

				$objInsuranceDatabase = CDatabases::createDatabase( CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseType::INSURANCE );
				$objInsuranceDatabase->begin();

				$objInsurancePolicyLiabilityCharge = \Psi\Eos\Insurance\CInsurancePolicyLiabilityCharges::createService()->fetchInsurancePolicyLiabilityChargeByPremiumTransactionId( $this->getId(), $objInsuranceDatabase );

				if( true == valObj( $objInsurancePolicyLiabilityCharge, 'CInsurancePolicyLiabilityCharge' ) ) {

					$objInsurancePolicyLiabilityCharge->setReturnedOn( date( 'm/d/Y H:i:s' ) );

					if( false == $objInsurancePolicyLiabilityCharge->update( $intUserId, $objInsuranceDatabase ) ) {
						$objInsuranceDatabase->rollback();
						trigger_error( 'Insurance policy liability charge failed to update.', E_USER_WARNING );
					} else {
						// we will add charge back entry in insurance_policy_liability_charges for adjustment
						$objAdjustedInsurancePolicyLiabilityCharge = new CInsurancePolicyLiabilityCharge();
						$objAdjustedInsurancePolicyLiabilityCharge->setInsurancePolicyId( $objInsurancePolicyLiabilityCharge->getInsurancePolicyId() );
						$objAdjustedInsurancePolicyLiabilityCharge->setLiabilityPremium( $objOffsetTransaction->getTransactionAmount() );
						$objAdjustedInsurancePolicyLiabilityCharge->setLiabilityChargeTransactionId( $objOffsetTransaction->getId() );
						$objAdjustedInsurancePolicyLiabilityCharge->setInsurancePolicyLiabilityChargeId( $objInsurancePolicyLiabilityCharge->getId() );
						$objAdjustedInsurancePolicyLiabilityCharge->setInsuranceChargeTypeId( CInsuranceChargeType::getChargeTypeByPaymentType( $objAccount->getPaymentTypeId(), true ) );

						if( false == $objAdjustedInsurancePolicyLiabilityCharge->insert( $intUserId, $objInsuranceDatabase ) ) {
							$objInsuranceDatabase->rollback();
							trigger_error( 'Insurance policy liability charge failed to insert for adjustment.', E_USER_WARNING );
						}
					}
				} else {
					trigger_error( 'Insurance policy liability charge failed to load.', E_USER_WARNING );
					return false;
				}

				$objInsuranceDatabase->commit();
			}
		}

		$this->setTransactionId( $objOffsetTransaction->getId() );
		$this->setAutoAllocate( false );

		if( CChargeCode::PAYMENT_RECEIVED == $this->getChargeCodeId() ) {

			// code to allocate payment with refund payment
			if( 0 != round( abs( $this->getTransactionAmount() ), 2 ) && false == in_array( $objAccount->getAccountTypeId(), CAccountType::$c_arrintNetSuiteNotAllowedAccountIds ) ) {
				$objAllocation = new CAllocation();
				$objAllocation->setCreditTransactionId( $this->getId() );
				$objAllocation->setChargeTransactionId( $objOffsetTransaction->getId() );
				$objAllocation->setCid( $this->getCid() );
				$objAllocation->setAccountId( $this->getAccountId() );
				$objAllocation->setAllocationAmount( abs( $this->getTransactionAmount() ) );

				if( false == $objAllocation->insert( $intUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $this->getErrorMsgs() );
					return false;
				}
			}

			if( false == $this->updatePayment( $intUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $this->getErrorMsgs() );
			}
		} else {
			if( false == $this->updateCharge( $intUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $this->getErrorMsgs() );
			}
		}

		return true;
	}

	public function deleteAllocations( $objDatabase, $intCurrentUserId = NULL, $boolIsSoftDelete = false ) {

		$boolExcludeSoftDeleted = false;

		if( true == $boolIsSoftDelete ) {
			$boolExcludeSoftDeleted = true;
		}

		$arrobjAllocations = CAllocations::createService()->fetchAllocationsByTransactionId( $this->getId(), $objDatabase, $boolExcludeSoftDeleted );

		if( true == valArr( $arrobjAllocations ) ) {
			$objDatabase->begin();

			if( false == CAllocations::createService()->deleteAllocations( $arrobjAllocations, $objDatabase, $intCurrentUserId, $boolIsSoftDelete ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete ALLOCATIONS.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDatabase->rollback();
				return false;
			}

			$objDatabase->commit();
		}

		return true;
	}

	public function sqlCid() {
		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient->sqlId();
		} else {
			return parent::sqlCid();
		}
	}

	public function sqlAutoAllocate() {
		return ( true == isset( $this->m_boolAutoAllocate ) ) ? ( string ) intval( $this->m_boolAutoAllocate ) : '1';
	}

	public function postCharge( $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		if( false == isset( $this->m_intId ) ) {
			$strSql = "SELECT nextval('transactions_id_seq'::text) AS id";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post company charge transaction record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrmixValues = $objDataset->fetchArray();
			$this->setId( $arrmixValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .

				'FROM transactions_post_charge( ' .
				$this->sqlId() . ', ' .
				$this->sqlEntityId() . ', ' .
				$this->sqlCid() . ', ' .
				$this->sqlAccountId() . ', ' .
				$this->sqlDepositId() . ', ' .
				$this->sqlCompanyChargeId() . ', ' .
				$this->sqlTransactionId() . ', ' .
				$this->sqlChargeCodeId() . ', ' .
				$this->sqlInvoiceId() . ', ' .
				$this->sqlExportBatchId() . ', ' .
				$this->sqlContractPropertyId() . ', ' .
				$this->sqlBundlePsProductId() . ', ' .
				$this->sqlReferenceNumber() . ', ' .
				$this->sqlTransactionDatetime() . ', ' .
				$this->sqlTransactionAmount() . ', ' .
				$this->sqlCostAmount() . ', ' .
				$this->sqlItemCount() . ', ' .
				$this->sqlMemo() . ', ' .
				$this->sqlInternalNotes() . ', ' .
				$this->sqlIsCommissioned() . ', ' .
				$this->sqlAutoAllocate() . ', ' .
				$this->sqlPostMonth() . ', ' .
				( int ) $intCurrentUserId . ', ' .
				$this->sqlTransactionReversalTypeId() . ', ' .
				$this->sqlPropertyId() . ', ' .
				$this->sqlCurrencyCode() . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			// Unset new id on error
			$this->setId( NULL );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post company charge transaction record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post company charge transaction record. The following error was reported.' ) );
			// Unset new id on error
			$this->setId( NULL );

			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		// Try to post commissions for this transactions.
		$this->postCommissions( $intCurrentUserId, $objDatabase, $intDebugMode = 0 );

		// Return true even if the commission posts or not.
		return true;
	}

	public function postCommissions( $intCurrentUserId, $objDatabase, $intDebugMode = 0 ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * FROM transactions_post_commissions( ' . $this->sqlId() . ', ' . ( int ) $intCurrentUserId . ', ' . ( int ) $intDebugMode . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post commission record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post commission record. The following error was reported.' ) );

			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function postRecommissions( $intUserId, $objDatabase ) {

		$boolIsValid = true;
		switch( NULL ) {
			default:

				if( false == $this->getIsChargeTransaction() ) {
					$this->addErrorMsgs( 'transaction [' . $this->getId() . '] is not a valid charge transaction to post recommission.' );
					$boolIsValid = false;
					break;
				}

				if( 1 == $this->getIsCommissioned() ) {
					$arrobjCommissions = CCommissions::createService()->fetchCommissionsByTransactionId( $this->getId(), $objDatabase );

					if( true == valArr( $arrobjCommissions ) ) {
						$arrintCommissionIdsWhichGotReversed = array_filter( ( array ) array_keys( ( array ) rekeyObjects( 'ReversedCommissionId', $arrobjCommissions ) ) );

						foreach( $arrobjCommissions as $objCommission ) {

							// We should not consider already reverted commissions, while recommissioning
							if( ( true == valArr( $arrintCommissionIdsWhichGotReversed ) && true == in_array( $objCommission->getId(), $arrintCommissionIdsWhichGotReversed ) ) || false == is_null( $objCommission->getReversedCommissionId() ) ) {
								continue;
							}

							if( false == is_numeric( $objCommission->getCommissionBatchId() ) ) {
								if( false == $objCommission->delete( $intUserId, $objDatabase, $boolIsHardDelete = true ) ) {
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Error: Failed to delete commission record [' . $objCommission->getId() . ']' ) );
									$boolIsValid = false;
									break;
								}
							} else {
								if( false == $objCommission->revertCommission( $intUserId, $objDatabase ) ) {
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Error: Failed to delete commission record [' . $objCommission->getId() . ']' ) );
									$boolIsValid = false;
									break;
								}
							}
						}
					}
				}

				if( true == $boolIsValid ) {

					if( 1 == $this->getIsCommissioned() ) {
						$this->setIsCommissioned( 0 );

						if( false == $this->update( $intUserId, $objDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Error: Failed to update transaction record [' . $this->getId() . ']' ) );
							$boolIsValid = false;
							break;
						}
					}

					if( false == $this->postCommissions( $intUserId, $objDatabase, $intDebugMode = 1 ) ) {
						$boolIsValid = false;
						break;

					}

				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Existing commissions failed to revert on transaction record [' . $this->getId() . ']' ) );
					$boolIsValid = false;
					break;
				}

		}
		return $boolIsValid;
	}

	public function updateCharge( $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .

					'FROM transactions_update_charge( ' .
					 $this->sqlId() . ', ' .
					 $this->sqlAccountId() . ', ' .
					 $this->sqlDepositId() . ', ' .
					 $this->sqlTransactionId() . ', ' .
					 $this->sqlChargeCodeId() . ', ' .
					 $this->sqlInvoiceId() . ', ' .
					 $this->sqlExportBatchId() . ', ' .
					 $this->sqlTransactionDatetime() . ', ' .
					 $this->sqlTransactionAmount() . ', ' .
					 $this->sqlCostAmount() . ', ' .
					 $this->sqlMemo() . ', ' .
					 $this->sqlInternalNotes() . ', ' .
					 ( int ) $intCurrentUserId . ', ' .
					 $this->sqlTransactionReversalTypeId() . ' , ' .
					 $this->sqlContractPropertyId() . ' , ' .
					 $this->sqlPropertyId() . ' , ' .
					 $this->sqlCurrencyCode() . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update charge transaction record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update charge transaction record. The following error was reported.' ) );
			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function deleteCharge( $intCurrentUserId, $objDatabase ) {

		// Delete or revert commissions.

		$arrobjCommissions = $this->fetchCommissions( $objDatabase );

		if( true == valArr( $arrobjCommissions ) ) {
			$arrintCommissionStructureIds = extractUniqueFieldValuesFromObjects( 'getCommissionStructureId', $arrobjCommissions );
			$arrobjCommissionStructures   = CCommissionStructures::createService()->fetchCommissionStructuresByIds( $arrintCommissionStructureIds, $objDatabase );

			foreach( $arrobjCommissions as $objCommission ) {
				if( true == in_array( $arrobjCommissionStructures[$objCommission->getCommissionStructureId()]->getCommissionStructureTypeId(), CCommissionStructureType::$c_arrintCommissionStructureTypesExcludedFromClawBack ) ) {
					continue;
				}

				if( true == is_numeric( $objCommission->getCommissionBatchId() ) ) {
					if( false == $objCommission->revertCommission( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsgs( $objCommission->getErrorMsgs() );
						return false;
					}
				} else {
					if( false == $objCommission->delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = true ) ) {
						$this->addErrorMsgs( $objCommission->getErrorMsgs() );
						return false;
					}
				}
			}
		}

		// We can hard delete this charge if the following conditions apply:
		// 1. The charge isn't exported.
		// 2. The charge isn't invoiced.

		if( true == is_null( $this->getExportBatchId() )
		    && true == is_null( $this->getInvoiceId() ) ) {

			$objDataset = $objDatabase->createDataset();

			$strSql = 'SELECT * ' .
			          'FROM transactions_delete_charge( ' .
			          $this->sqlId() . ', ' .
			          ( int ) $intCurrentUserId . ' ) AS result;';

			if( false == $objDataset->execute( $strSql ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete charge transaction record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();

				return false;
			}

			if( 0 < $objDataset->getRecordCount() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete charge transaction record. The following error was reported.' ) );
				while( !$objDataset->eof() ) {
					$arrmixValues = $objDataset->fetchArray();
					$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
					$objDataset->next();
				}

				$objDataset->cleanup();

				return false;
			}

			$objDataset->cleanup();

			// Otherwise let's offest the transaction
		} else {
			$this->reverseWithCharge( $intCurrentUserId, $objDatabase );
		}

		return true;
	}

	public function postPayment( $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		if( false == isset( $this->m_intId ) ) {
			$strSql = "SELECT nextval('transactions_id_seq'::text) AS id";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post payment transaction record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrmixValues = $objDataset->fetchArray();
			$this->setId( $arrmixValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .

					'FROM transactions_post_payment( ' .
					$this->sqlId() . ', ' .
					$this->sqlEntityId() . ', ' .
					$this->sqlCid() . ', ' .
					$this->sqlAccountId() . ', ' .
					$this->sqlDepositId() . ', ' .
					$this->sqlTransactionId() . ', ' .
					$this->sqlChargeCodeId() . ', ' .
					$this->sqlCompanyPaymentId() . ', ' .
					$this->sqlInvoiceId() . ', ' .
					$this->sqlExportBatchId() . ', ' .
					$this->sqlBundlePsProductId() . ', ' .
					$this->sqlReferenceNumber() . ', ' .
					$this->sqlTransactionDatetime() . ', ' .
					$this->sqlTransactionAmount() . ', ' .
					$this->sqlCostAmount( true ) . ', ' .
					$this->sqlItemCount() . ', ' .
					$this->sqlMemo() . ', ' .
					$this->sqlInternalNotes() . ', ' .
					$this->sqlAutoAllocate() . ', ' .
					$this->sqlPostMonth() . ', ' .
					( int ) $intCurrentUserId . ', ' .
					$this->sqlCurrencyCode() . ', ' .
		            $this->sqlPropertyId() . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			// Unset new id on error
			$this->setId( NULL );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post payment transaction record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post company payment transaction record. The following error was reported.' ) );
			// Unset new id on error
			$this->setId( NULL );

			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function updatePayment( $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .

					'FROM transactions_update_payment( ' .
					$this->sqlId() . ', ' .
					$this->sqlAccountId() . ', ' .
					$this->sqlTransactionId() . ', ' .
					$this->sqlChargeCodeId() . ', ' .
					$this->sqlInvoiceId() . ', ' .
					$this->sqlExportBatchId() . ', ' .
					$this->sqlTransactionDatetime() . ', ' .
					$this->sqlTransactionAmount() . ', ' .
					$this->sqlCostAmount() . ', ' .
					$this->sqlMemo() . ', ' .
					$this->sqlInternalNotes() . ', ' .
					( int ) $intCurrentUserId . ', ' .
					$this->sqlCurrencyCode() . ', ' .
		            $this->sqlPropertyId() . ') AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update payment transaction record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update payment transaction record. The following error was reported.' ) );
			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function postOrUpdatePayment( $intCurrentUserId, $objDatabase ) {
		if( true == isset( $this->m_intId ) ) {
			return $this->updatePayment( $intCurrentUserId, $objDatabase );
		} else {
			return $this->postPayment( $intCurrentUserId, $objDatabase );
		}
	}

	public function deletePayment( $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
		          'FROM transactions_delete_payment( ' .
		          $this->sqlId() . ', ' .
		          ( int ) $intCurrentUserId . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete payment transaction record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete payment transaction record. The following error was reported.' ) );
			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function postScheduledCharge( $intCurrentUserId, $objDatabase, $boolReturnSql = false ) {

		$objDataset = $objDatabase->createDataset();

		if( false == isset( $this->m_intId ) ) {
			$strSql = "SELECT nextval('transactions_id_seq'::text) AS id";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post scheduled charge transaction record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrmixValues = $objDataset->fetchArray();
			$this->setId( $arrmixValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .

					'FROM transactions_post_scheduled_charge( ' .
					$this->sqlId() . ', ' .
					$this->sqlEntityId() . ', ' .
					$this->sqlCid() . ', ' .
					$this->sqlAccountId() . ', ' .
					$this->sqlDepositId() . ', ' .
					$this->sqlCompanyChargeId() . ', ' .
					$this->sqlTransactionId() . ', ' .
					$this->sqlChargeCodeId() . ', ' .
					$this->sqlInvoiceId() . ', ' .
					$this->sqlExportBatchId() . ', ' .
					$this->sqlContractPropertyId() . ', ' .
					$this->sqlBundlePsProductId() . ', ' .
					$this->sqlReferenceNumber() . ', ' .
					$this->sqlTransactionDatetime() . ', ' .
					$this->sqlTransactionAmount() . ', ' .
					$this->sqlCostAmount() . ', ' .
					$this->sqlItemCount() . ', ' .
					$this->sqlMemo() . ', ' .
					$this->sqlInternalNotes() . ', ' .
					$this->sqlIsCommissioned() . ', ' .
					$this->sqlAutoAllocate() . ', ' .
					$this->sqlPostMonth() . ', ' .
					( int ) $intCurrentUserId . ',' .
					$this->sqlPropertyId() . ',' .
					$this->sqlCurrencyCode() . ') AS result;';

		if( true == $boolReturnSql ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {
			// Unset new id on error
			$this->setId( NULL );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post scheduled charge transaction record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post scheduled charge transaction record. The following error was reported.' ) );
			// Unset new id on error
			$this->setId( NULL );

			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return $this->postCommissions( $intCurrentUserId, $objDatabase, $intDebugMode = 0 );
	}

	public function postContractPropertyCharge( $intCurrentUserId, $objAdminDatabase, $strLastPostedOn = NULL, $boolIsAutoAllocate = NULL ) {

		$objDataset = $objAdminDatabase->createDataset();

		if( false == isset( $this->m_intId ) ) {
			$strSql = "SELECT nextval('transactions_id_seq'::text) AS id";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to post contract property charge transaction record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objAdminDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrmixValues = $objDataset->fetchArray();
			$this->setId( $arrmixValues['id'] );

			$objDataset->cleanup();
		}

		if( false == is_null( $strLastPostedOn ) ) {
			$strLastPostedOn = '\'' . $strLastPostedOn . '\'';
		}

		$strSql = 'SELECT * ' .

				'FROM transactions_post_contract_property( ' .
				$this->sqlId() . ', ' .
				$this->sqlEntityId() . ', ' .
				$this->sqlCid() . ', ' .
				$this->sqlAccountId() . ', ' .
				$this->sqlDepositId() . ', ' .
				$this->sqlTransactionId() . ', ' .
				$this->sqlChargeCodeId() . ', ' .
				$this->sqlInvoiceId() . ', ' .
				$this->sqlExportBatchId() . ', ' .
				$this->sqlContractPropertyId() . ', ' .
				$this->sqlBundlePsProductId() . ', ' .
				$this->sqlReferenceNumber() . ', ' .
				$this->sqlTransactionDatetime() . ', ' .
				$this->sqlTransactionAmount() . ', ' .
				$this->sqlCostAmount() . ', ' .
				$this->sqlItemCount() . ', ' .
				$this->sqlMemo() . ', ' .
				$this->sqlInternalNotes() . ', ' .
				$this->sqlIsCommissioned() . ', ' .
				( ( false == is_null( $boolIsAutoAllocate ) && false == $boolIsAutoAllocate ) ? '0' : $this->sqlAutoAllocate() ) . ', ' .
				$this->sqlPostMonth() . ', ' .
				( int ) $intCurrentUserId . ',' .
				$strLastPostedOn . ',' .
				$this->sqlPropertyId() . ',' .
				$this->sqlCurrencyCode() . '';

		$strSql .= ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return false;
		}

		$objDataset->cleanup();
		return true;
	}

	public function fetchAllocationDetails( $objAdminDatabase ) {
		return CAllocations::createService()->fetchAllocationDetailsByChargeTransactionId( $this->getId(), $objAdminDatabase );
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setTransactionDueAmount( $this->getTransactionAmount() );

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>