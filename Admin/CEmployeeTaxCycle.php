<?php

class CEmployeeTaxCycle extends CBaseEmployeeTaxCycle {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialYearName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCycle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCycleStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCycleEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>