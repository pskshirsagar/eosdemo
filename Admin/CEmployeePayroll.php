<?php

class CEmployeePayroll extends CBaseEmployeePayroll {

	protected $m_fltGrossPayDecrypted;
	protected $m_fltHourlyPayRateDecrypted;
	protected $m_m_strGrossPayEncrypted;
	protected $m_strHourlyPayRateEncrypted;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['gross_pay_encrypted'] ) ) 	 $this->setGrossPayEncrypted( $arrmixValues['gross_pay_encrypted'] );
		if( true == isset( $arrmixValues['hourly_pay_rate_encrypted'] ) )  $this->setHourlyPayRateEncrypted( $arrmixValues['hourly_pay_rate_encrypted'] );

		return;
	}

	/**
	 * Setter function
	 *
	 */

	public function setGrossPayDecrypted( $fltGrossPayDecrypted ) {
		$this->m_fltGrossPayDecrypted = $fltGrossPayDecrypted;
	}

	public function setHourlyPayRateDecrypted( $fltHourlyPayRateDecrypted ) {
		$this->m_fltHourlyPayRateDecrypted = $fltHourlyPayRateDecrypted;
	}

	public function setGrossPayEncrypted( $strGrossPayEncrypted ) {
		$this->m_strGrossPayEncrypted = CStrings::strTrimDef( $strGrossPayEncrypted, -1, NULL, true );
	}

	public function setHourlyPayRateEncrypted( $strHourlyPayRateEncrypted ) {
		$this->m_strHourlyPayRateEncrypted = CStrings::strTrimDef( $strHourlyPayRateEncrypted, -1, NULL, true );
	}

	/**
	 * Getter functions
	 *
	 */

	public function getGrossPayDecrypted() {
		return $this->m_fltGrossPayDecrypted;
	}

	public function getHourlyPayRateDecrypted() {
		return $this->m_fltHourlyPayRateDecrypted;
	}

	public function getGrossPayEncrypted() {
		return $this->m_strGrossPayEncrypted;
	}

	public function getHourlyPayRateEncrypted() {
		return $this->m_strHourlyPayRateEncrypted;
	}

	/**
	 * Validate functions
	 *
	 */

	public function valEmployeeId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_intEmployeeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valHoursWorked() {
		$boolIsValid = true;

		if( true == empty( $this->m_intHoursWorked ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hours_worked', ' Hours worked is required.' ) );
		}

		return $boolIsValid;
	}

	public function valHourlyPayRate() {
		$boolIsValid = true;

		if( true == empty( $this->m_intHourlyPayCompensationAssociationId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_pay_rate', ' Hourly pay rate is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPayrollPeriodId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intPayrollPeriodId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payroll_period_id', 'Payroll period is required.' ) );
		}
		 return $boolIsValid;
	}

	public function validate( $strAction, $boolValidateHourlyPayRate = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valPayrollPeriodId();

				if( true == $boolValidateHourlyPayRate ) {
					$boolIsValid &= $this->valHoursWorked();
					$boolIsValid &= $this->valHourlyPayRate();
				}

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>