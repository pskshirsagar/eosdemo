<?php

class CEmployeeDependant extends CBaseEmployeeDependant {

	public function valId() {
	   $boolIsValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Employee dependent id does not appear valid.' ) );
		}

		return $boolIsValid;
	}

	public function valDependantTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intDependantTypeId ) || ( 1 > $this->m_intDependantTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Relation is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst( $objDatabase ) {

		$boolIsValid = $this->validateName( 'First name', $this->m_strNameFirst, 'name_first' );

		$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );

		$intCount = CEmployeeDependants::fetchEmployeeDependantCount( ' WHERE employee_id= ' . ( int ) $this->getEmployeeId() . ' AND  lower( name_first )= \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $this->m_strNameFirst ) ) . '\'' . $strSqlCondition, $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Name already exist.' ) );
		}

		return $boolIsValid;
	}

	public function valGender() {
		$boolIsValid = true;

		if( false == isset( $this->m_strGender ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gender', 'Select gender.' ) );
		}

		return $boolIsValid;
	}

	public function validateName( $strNameType, $strNameValue, $strFieldName ) {

		$boolIsValid = true;

		$strErrorMessage = $strNameType;

		if( false == isset( $strNameValue ) ) {
			$boolIsValid = false;

			$strErrorMessage = $strErrorMessage . ' is required.';
		} else {
			if( false == preg_match( '/^\pL*(\s?)\pL+$/u', $strNameValue ) ) {
				$boolIsValid = false;
				$strErrorMessage = $strErrorMessage . ' should be set of alphabets.';
			}
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {

		$boolIsValid = $this->validateName( 'Last name', $this->m_strNameLast, 'name_last' );

 		return $boolIsValid;
	}

	public function valContactNumber() {

		$boolIsValid = true;

		$strErrorMessage = 'Contact number';

			if( true == is_null( $this->m_strContactNumber ) && $this->getDependantTypeId() != CDependantType::CHILD ) {
				$boolIsValid = false;
				$strErrorMessage = $strErrorMessage . ' is required.';
			} else {
				if( false == is_null( $this->m_strContactNumber ) && false == is_numeric( $this->m_strContactNumber ) ) {
					$boolIsValid = false;
					$strErrorMessage = $strErrorMessage . ' should be numeric.';
				} elseif( 15 < strlen( $this->m_strContactNumber ) ) {
					$boolIsValid = false;
					$strErrorMessage = $strErrorMessage . ' should not exceed 15 digits.';
				}
			}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cell_number', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valBirthDate() {
		$boolIsValid = true;

		if( false == isset( $this->m_strBirthDate ) || false == CValidation::validateDate( $this->m_strBirthDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'A valid birth date is required.' ) );
		}

		$strTodaysDate = date( 'Y-m-d' );

		$strToday = strtotime( $strTodaysDate );
		$strBirthDate = strtotime( $this->m_strBirthDate );

		if( $strBirthDate > $strToday ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'Birth date should not be future date.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
					$boolIsValid &= $this->valDependantTypeId();
					$boolIsValid &= $this->valGender();
					$boolIsValid &= $this->valNameFirst( $objDatabase );
					$boolIsValid &= $this->valNameLast();
					$boolIsValid &= $this->valContactNumber();
					$boolIsValid &= $this->valBirthDate();
				break;

			case VALIDATE_UPDATE:
					$boolIsValid &= $this->valId();
					$boolIsValid &= $this->valGender();
					$boolIsValid &= $this->valDependantTypeId();
					$boolIsValid &= $this->valNameFirst( $objDatabase );
					$boolIsValid &= $this->valContactNumber();
					$boolIsValid &= $this->valNameLast();
					$boolIsValid &= $this->valBirthDate();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>