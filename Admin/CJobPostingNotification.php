<?php

class CJobPostingNotification extends CBaseJobPostingNotification {

	protected $m_strCategoryName;

	/**
	 * Get Functions
	 *
	 */

	public function getCategoryName() {
		return $this->m_strCategoryName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setCategoryName( $strCategoryName ) {
		$this->m_strCategoryName = $strCategoryName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['category_name'] ) )	$this->setCategoryName( $arrmixValues['category_name'] );

		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>