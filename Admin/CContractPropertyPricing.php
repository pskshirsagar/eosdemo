<?php

/**
 * @method setPpcStartDate( $strDate ) Sets details field using TEosDetails::__call
 * @method getPpcStartDate()
 * @method setPpcEndDate( $getPpcEndDate )
 * @method getPpcEndDate()
 * @method setPpcPrice( $getPpcPrice )
 * @method getPpcPrice()
 * @method setPpcDescription( $getPpcDescription )
 * @method getPpcDescription()
 * @method setPpcAccountId( $getPpcAccountId )
 * @method getPpcAccountId()
 * @method setPpcPriceUpdatedBy( $getPpcPriceUpdatedBy )
 * @method getPpcPriceUpdatedBy()
 * @method setPpcPriceUpdatedOn( $getPpcPriceUpdatedOn )
 * @method getPpcPriceUpdatedOn()
 * @method getIsReviewed()
 * @method setIsReviewed( $getIsReviewed )
 */
class CContractPropertyPricing extends CBaseContractPropertyPricing {

	protected $m_intContractId;

	protected $m_strLastPostedOn;
	protected $m_strBillingStartDate;
	protected $m_strPricingUpdatedBy;

	public function setLastPostedOn( $strLastPostedOn ) {
		$this->m_strLastPostedOn = CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true );
	}

	public function setContractId( $intContractId ) {
		$this->m_intContractId = $intContractId;
	}

	public function getLastPostedOn() {
		return $this->m_strLastPostedOn;
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlLastPostedOn() {
		return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NULL';
	}

	public function setBillingStartDate( $strBillingStartDate ) {
		$this->m_strBillingStartDate = CStrings::strTrimDef( $strBillingStartDate, -1, NULL, true );
	}

	public function getBillingStartDate() {
		return $this->m_strBillingStartDate;
	}

	public function sqlBillingStartDate() {
		return ( true == isset( $this->m_strBillingStartDate ) ) ? '\'' . $this->m_strBillingStartDate . '\'' : 'NULL';
	}

	public function setPricingUpdatedBy( $strPricingUpdatedBy ) {
		$this->m_strPricingUpdatedBy = $strPricingUpdatedBy;
	}

	public function getPricingUpdatedBy() {
		return $this->m_strPricingUpdatedBy;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['billing_start_date'] ) ) {
			$this->setBillingStartDate( $arrstrValues['billing_start_date'] );
		}
		if( true == isset( $arrstrValues['last_posted_on'] ) ) {
			$this->setLastPostedOn( $arrstrValues['last_posted_on'] );
		}
		if( true == isset( $arrstrValues['contract_id'] ) ) {
			$this->setContractId( $arrstrValues['contract_id'] );
		}
		if( true == isset( $arrstrValues['pricing_updated_by'] ) ) {
			$this->setPricingUpdatedBy( $arrstrValues['pricing_updated_by'] );
		}
	}

	public function updateOldContractPropertyPricings( $intNewRecurringAmount, $strNewBillingDate, $fltMonthlyChangeAmount, $objSelectedContractDocument, $boolIsDetails = false ) {

		$this->setPrice( $intNewRecurringAmount );
		$this->setStartDate( $strNewBillingDate );
		$this->setMonthlyChangeAmount( $fltMonthlyChangeAmount );
		if( true == valObj( $objSelectedContractDocument, 'CContractDocument' ) ) {
			$this->setContractDocumentId( $objSelectedContractDocument->getId() );
		}
		if( true == $boolIsDetails ) {
			$this->setIsReviewed( 'false' );
		}
		return $this;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createPpcEngineCharges( $objOldContractPropertyPricing ) {
		/** @var $objOldContractPropertyPricing CContractPropertyPricing */

		$this->setPpcStartDate( $objOldContractPropertyPricing->getPpcStartDate() );
		$this->setPpcEndDate( $objOldContractPropertyPricing->getPpcEndDate() );
		$this->setPpcPrice( $objOldContractPropertyPricing->getPpcPrice() );
		$this->setPpcDescription( $objOldContractPropertyPricing->getPpcDescription() );
		$this->setPpcAccountId( $objOldContractPropertyPricing->getPpcAccountId() );

		if( false == is_null( $objOldContractPropertyPricing->getPpcPriceUpdatedBy() ) ) {
			$this->setPpcPriceUpdatedBy( $objOldContractPropertyPricing->getPpcPriceUpdatedBy() );
			$this->setPpcPriceUpdatedOn( $objOldContractPropertyPricing->getPpcPriceUpdatedOn() );
		}

		return $this;
	}

	public function createOldContractPropertyPricing( $objDatabase ) {
		/** @var CContractPropertyPricing[] $arrobjContractPropertyPricing */
		$arrobjContractPropertyPricing = \Psi\Eos\Admin\CContractPropertyPricings::createService()->fetchRenewalContractPropertyPricingsDetailsByContractPropertyIds( $this->getContractPropertyId(), $objDatabase );

		if( false == valArr( $arrobjContractPropertyPricing ) ) {
			return NULL;
		}

		$intContractPropertyPricingId = array_keys( $arrobjContractPropertyPricing )[0];

		if( false == valId( $intContractPropertyPricingId ) ) {
			return NULL;
		}

		$strOldPPCEndDate = $arrobjContractPropertyPricing[$intContractPropertyPricingId]->getPpcEndDate();

		$strNewStartDate = date( 'm/d/Y', strtotime( $this->getStartDate() ) );

		if( false == valStr( $strOldPPCEndDate ) || strtotime( $strNewStartDate ) > strtotime( $strOldPPCEndDate ) ) {
			return NULL;
		}

		$this->setPpcStartDate( $strNewStartDate );
		$this->setPpcEndDate( $arrobjContractPropertyPricing[$intContractPropertyPricingId]->getPpcEndDate() );
		$this->setPpcPrice( $arrobjContractPropertyPricing[$intContractPropertyPricingId]->getPpcPrice() );
		$this->setPpcDescription( $arrobjContractPropertyPricing[$intContractPropertyPricingId]->getPpcDescription() );

		$strOldEndDate = date( 'm/t/Y', strtotime( $arrobjContractPropertyPricing[$intContractPropertyPricingId]->getEndDate() ) );

		$arrobjContractPropertyPricing[$intContractPropertyPricingId]->setPpcEndDate( $strOldEndDate );

		return $arrobjContractPropertyPricing[$intContractPropertyPricingId];
	}

}
?>
