<?php

class CStatsSalesMonthlyCommit extends CBaseStatsSalesMonthlyCommit {

	protected $m_strCompanyName;
	protected $m_strTitle;

	protected $m_intContractTypeId;

	/**
	 * Get Functions
	 *
	 */

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getContractTypeId() {
		return $this->m_intContractTypeId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) )		$this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['title'] ) ) 				$this->setTitle( $arrmixValues['title'] );
		if( true == isset( $arrmixValues['contract_type_id'] ) ) 	$this->setContractTypeId( $arrmixValues['contract_type_id'] );

		return;
	}

	public function setCompanyName( $strCompanyName ) {
		return $this->m_strCompanyName = $strCompanyName;
	}

	public function setTitle( $strTitle ) {
		return $this->m_strTitle = $strTitle;
	}

	public function setContractTypeId( $intContractTypeId ) {
		return $this->m_intContractTypeId = $intContractTypeId;
	}

	/**
 	 * Validate Functions
	 *
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>