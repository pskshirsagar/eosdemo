<?php

class CTestSuite extends CBaseTestSuite {

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	/**
	 * @throws \UnexpectedValueException
	 */
	public function valPsProductId() {
		if( !valId( $this->getPsProductId() ) ) {
			throw new UnexpectedValueException( __( 'Product ID missing for {%s, 0}', [ $this->getName() ] ), ERROR_TYPE_VALIDATION );
		}
	}

	public function valPsProductOptionId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	/**
	 * @throws \UnexpectedValueException
	 */
	public function valTestSuiteTypeId() {
		if( !valId( $this->getTestSuiteTypeId() ) ) {
			throw new UnexpectedValueException( __( 'Test Suite Type ID missing for {%s, 0}', [ $this->getName() ] ), ERROR_TYPE_VALIDATION );
		}
	}

	/**
	 * @throws \UnexpectedValueException
	 */
	public function valName() {
		if( !valStr( $this->getName() ) ) {
			throw new UnexpectedValueException( __( 'Test Suite name missing.' ), ERROR_TYPE_VALIDATION );
		}
	}

	public function valDetails() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	/**
	 * @param $strAction
	 * @return bool
	 * @throws \UnexpectedValueException
	 */
	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$this->valPsProductId();
				$this->valTestSuiteTypeId();
				$this->valName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setOrderNum( $intOrderNum ) {
		$this->setDetailsField( 'order_num', $intOrderNum );
	}

	public function setAttributes( $arrmixAttributes ) {
		$this->setDetailsField( 'attributes', $arrmixAttributes );
	}

}

?>