<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAds
 * Do not add any new functions to this class.
 */

class CSemAds extends CBaseSemAds {

	public static function fetchPaginatedSemAdsBySemAdGroupId( $intSemAdGroupId, $intPageNo, $intPageSize, $objAdminDatabase ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;
		return self::fetchSemAds( 'SELECT * FROM sem_ads WHERE sem_ad_group_id = ' . ( int ) $intSemAdGroupId . ' AND deleted_by IS NULL AND deleted_on IS NULL OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit, $objAdminDatabase );
	}

	public static function fetchPaginatedSemAdsCountBySemAdGroupId( $intSemAdGroupId, $objAdminDatabase ) {
		$arrintResponse = fetchData( 'SELECT count(id) FROM sem_ads WHERE sem_ad_group_id = ' . ( int ) $intSemAdGroupId . ' AND deleted_by Is NULL AND deleted_on IS NULL', $objAdminDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		return 0;
	}

	public static function fetchSemAdBySemAdGroupIdBySemAdId( $intSemAdGroupId, $intSemAdId, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM sem_ads WHERE sem_ad_group_id = ' . ( int ) $intSemAdGroupId . ' AND id = ' . ( int ) $intSemAdId;
		return self::fetchSemAd( $strSql, $objAdminDatabase );
	}

	public static function fetchSemAdsByPropertyIdByCid( $intPropertyId, $intCid, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM sem_ads WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchSemAds( $strSql, $objAdminDatabase );
	}

	public static function fetchCompetingSemAdBySemAdGroupIdByTitleByNonCompetingSemAdId( $intSemAdGroupId, $strTitle, $intCompetingSemAdId, $objAdminDatabase ) {

		$strCondition = ( true == is_numeric( $intCompetingSemAdId ) ) ? ' AND id <> ' . ( int ) $intCompetingSemAdId : '';

		$strWhere = ' WHERE
								sem_ad_group_id = ' . ( int ) $intSemAdGroupId . '
								AND title ILIKE \'' . trim( addslashes( $strTitle ) ) . '\'' . $strCondition . ' LIMIT 1';

		return self::fetchSemAdCount( $strWhere, $objAdminDatabase );
	}
}
?>