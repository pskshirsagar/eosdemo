<?php

class CDealDeskMeetingDetail extends CBaseDealDeskMeetingDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDealDeskMeetingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReason( $boolAllowNull = false ) {
		$boolIsValid = true;

		if( true == $boolAllowNull && false == valStr( $this->m_strReason ) ) {
			return true;
		}

		if( false == valStr( $this->m_strReason ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reason', 'Reason is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valReason( true );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valReason();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>