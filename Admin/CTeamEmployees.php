<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTeamEmployees
 * Do not add any new functions to this class.
 */ /** @noinspection PhpCSValidationInspection */

class CTeamEmployees extends CBaseTeamEmployees {

	public static function fetchTeamEmployeesByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchTeamEmployees( sprintf( 'SELECT * FROM team_employees WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

	public static function fetchTeamEmployeesByEmployeeIds( $arrintEmployeeIds, $objDatabase, $boolIsPrimaryTeam = false, $boolBuTeamTab = false ) {
		if( false == valIntArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strJoinCondition	= '';
		$strSelectClause	= '';

		$strWhereCondition	= ( true == $boolIsPrimaryTeam ) ? 'AND te.is_primary_team = 1' : '';

		if( true == $boolBuTeamTab ) {
			$strSelectClause	= ' , t.hr_representative_employee_id';
			$strJoinCondition	= ' LEFT JOIN teams t ON ( te.team_id = t.id ) ';
		}

		$strSql = 'SELECT
						te.* ' . $strSelectClause . '
					FROM
						team_employees te ' . $strJoinCondition . '
					WHERE
						te.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )' . $strWhereCondition;

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByTeamIds( $arrintTeamIds, $objDatabase ) {
		if( false == valArr( $arrintTeamIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						te.*,
						CASE WHEN e.preferred_name IS NULL THEN e.name_full ELSE e.preferred_name END AS name_full,
						e.id
					FROM
						team_employees te
						JOIN employees e ON (e.id = te.employee_id)
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND te.team_id IN ( ' . implode( ',', array_filter( $arrintTeamIds ) ) . ' )';

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByTeamIdsByEmployeeStatusTypeId( $arrintTeamIds, $intEmployeeStatusTypeId, $objDatabase ) {
		if( false == valArr( $arrintTeamIds ) || false == valId( $intEmployeeStatusTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						te.*
					FROM
						team_employees te
						JOIN employees e ON (e.id = te.employee_id)
					WHERE
						e.employee_status_type_id = ' . ( int ) $intEmployeeStatusTypeId . '
						AND team_id IN ( ' . implode( ',', $arrintTeamIds ) . ' )';

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByTeamIdsByPrimaryTeam( $arrintTeamIds, $objDatabase ) {
		if( false == valArr( $arrintTeamIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						te.*
					FROM
						team_employees te
					JOIN
						employees e ON (e.id = te.employee_id  AND te.is_primary_team = 1 )
					WHERE
						te.team_id IN ( ' . implode( ',', $arrintTeamIds ) . ' )  AND
						e.employee_status_type_id = 1
					ORDER
						BY te.team_id';

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByTeamIdByEmployeeId( $intTeamId, $intEmployeeId, $objDatabase ) {
		if( false == valId( $intTeamId ) || false == valId( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						team_employees
					WHERE
						team_id = ' . ( int ) $intTeamId . '
						AND employee_id = ' . ( int ) $intEmployeeId . '
						AND is_primary_team = 1
					ORDER BY
						is_primary_team DESC,
						COALESCE( manager_employee_id, 0 ) DESC
					LIMIT 1';

		return self::fetchTeamEmployee( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeByEmployeeIdByIsPrimaryTeam( $intEmployeeId, $objDatabase ) {
		return self::fetchTeamEmployee( sprintf( ' SELECT * FROM team_employees WHERE employee_id = %d AND is_primary_team = 1 LIMIT 1', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeesByTeamId( $intTeamId, $objDatabase, $boolPrimaryTeamOnly = false ) {
		if( false == valId( $intTeamId ) ) {
			return NULL;
		}

		$strJoinCondition = ( true == $boolPrimaryTeamOnly ) ? ' AND te.is_primary_team = 1' : '';

		$strSql = '	SELECT
						DISTINCT ( e.* ),
						e.reporting_manager_id AS manager_employee_id,
						te.is_primary_team,
						d.name As designation_name,
						te1.team_id
					FROM
						employees AS e
						LEFT JOIN team_employees AS te ON ( e.id = te.employee_id ' . $strJoinCondition . ' )
						LEFT JOIN team_employees AS te1 ON ( e.id = te1.employee_id AND te1.is_primary_team = 1 )
						LEFT JOIN designations d ON d.id = e.designation_id
					WHERE
						te.team_id = ' . ( int ) $intTeamId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND date_terminated IS NULL
					ORDER BY
						e.name_first,
						e.name_last';

		return CEmployees::fetchEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentTeamEmployeesCountByTeamIds( $arrintTeamEmployeeIds, $objDatabase, $boolIsPrimaryTeam = false ) {
		if( false == valArr( $arrintTeamEmployeeIds ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == $boolIsPrimaryTeam ) ? 'AND te.is_primary_team = 1' : '';

		$strSql = ' SELECT
						team_id,count( te.id ) AS member_count
					FROM
						team_employees te
						JOIN employees e on te.employee_id = e.id
					WHERE
						te.team_id IN (' . implode( ',', array_keys( $arrintTeamEmployeeIds ) ) . ') AND
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						' . $strWhereCondition . '
					GROUP BY
						team_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesHavingManagerEmployeeIdsByEmployeeId( $intEmployeeId, $objDatabase, $boolIsPrimaryTeam = false ) {

		$strSql = ' SELECT *
					FROM
						team_employees
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
					 	AND manager_employee_id IS NOT NULL';
		if( true == $boolIsPrimaryTeam ) {
			$strSql .= ' AND is_primary_team = 1 ';
		}

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeByEmployeeId( $intEmployeeId, $objDatabase ) {
		if( false == valId( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM team_employees WHERE employee_id = ' . ( int ) $intEmployeeId . ' AND is_primary_team = 1 LIMIT 1';
		return self::fetchTeamEmployee( $strSql, $objDatabase );
	}

	public static function fetchActiveTeamEmployeesByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $boolPrimaryTeam = false, $arrintDevQaEmployeeIds = NULL, $boolFromStoryPoints = false ) {
		if( false == valId( $intManagerEmployeeId ) ) {
			return NULL;
		}

		$strCondition = ( false == $boolPrimaryTeam ) ? NULL : 'AND te.is_primary_team = 1';

		if( true == $boolFromStoryPoints && true == valArr( $arrintDevQaEmployeeIds ) ) {
			$strWhere = ' e.reporting_manager_id IN ( ' . ( int ) $intManagerEmployeeId . ',' . implode( ',', $arrintDevQaEmployeeIds ) . ' ) ';
		} elseif( true == valArr( $arrintDevQaEmployeeIds ) ) {
			$strWhere = ' ( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN ( ' . implode( ',', $arrintDevQaEmployeeIds ) . ' ) )';
		} else {
			$strWhere = ' e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;
		}

		$strSql = 'SELECT
						DISTINCT e.id AS employee_id,
						CASE WHEN e.preferred_name IS NULL THEN e.name_full ELSE e.preferred_name END AS name_full,
						e.reporting_manager_id AS manager_employee_id,
						te.team_id
					FROM
						employees as e
						LEFT JOIN team_employees as te ON ( e.id = te.employee_id )
					WHERE
						' . $strWhere . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND date_terminated IS NULL
						' . $strCondition . '
					ORDER BY
						name_full';

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveTeamEmployeesByManagerEmployeeIds( $arrintManagerEmployeeIds, $objDatabase, $boolIsPrimaryTeam = false, $arrintDepartmentIds = NULL, $arrintExcludeEmployeeIds = NULL ) {
		if( false == valArr( $arrintManagerEmployeeIds ) ) {
			return NULL;
		}

		$strCondition = ( false == $boolIsPrimaryTeam ) ? NULL : 'AND te.is_primary_team = 1';

		if( true == valArr( $arrintDepartmentIds ) ) {
			$strCondition .= ' AND e.department_id IN (' . implode( ',', $arrintDepartmentIds ) . ')';
		}

		if( true == valArr( $arrintExcludeEmployeeIds ) ) {
			$strCondition .= ' AND e.id NOT IN (' . implode( ',', $arrintExcludeEmployeeIds ) . ')';
		}

		$strSql = 'SELECT
						DISTINCT e.id AS employee_id,
						e.preferred_name AS name_full,
						e.reporting_manager_id AS manager_employee_id,
						te.team_id
					FROM
						employees as e
						JOIN team_employees as te ON ( e.id = te.employee_id )
						JOIN teams t ON( t.manager_employee_id = te.employee_id AND t.deleted_by IS NULL )
					WHERE
						e.reporting_manager_id IN (' . implode( ',', $arrintManagerEmployeeIds ) . ')
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND date_terminated IS NULL
						' . $strCondition . '
					ORDER BY
						name_full';

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByManagerEmployeeIds( $arrintManagerEmployeeIds, $intYear, $objDatabase ) {
		if( false == valArr( $arrintManagerEmployeeIds ) ) {
			return NULL;
		}

		$strCondition = ( true == is_numeric( $intYear ) ) ? 'AND evs.year = ' . ( int ) $intYear : '';

		$strSql = 'SELECT
						DISTINCT (e.id) AS employee_id,
						e.name_first AS employee_first_name,
						e.name_last AS employee_last_name,
						e.reporting_manager_id AS manager_employee_id,
						t.id AS team_id,
						t.name AS team_name,
						evs.hours_granted AS hours_granted
					 FROM
						employees as e
						JOIN team_employees as te ON (e.id = te.employee_id)
						JOIN teams AS t ON (te.team_id = t.id)
						JOIN employee_vacation_schedules evs ON (evs.employee_id = e.id)
					WHERE
						te.team_id IN (
										SELECT
											id
										FROM
											teams
										 WHERE manager_employee_id IN ( ' . implode( ',', $arrintManagerEmployeeIds ) . ' )
									  )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . $strCondition .
						'AND e.date_terminated IS NULL
						AND te.is_primary_team = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeManagerByEmployeeId( $intEmployeeId, $objDatabase ) {
		if( false == valId( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e1.id,
						e1.name_full,
						e1.email_address,
						te.team_id
					FROM
						employees e1
						LEFT JOIN employees e on ( e1.id = e.reporting_manager_id) OR ( e.id = e1.id AND e.id NOT IN ( SELECT DISTINCT(employee_id) FROM team_employees ) )
						LEFT JOIN team_employees te on ( e.id = te.employee_id )
					WHERE e.date_terminated IS NULL
						AND e.employee_status_type_id =  ' . CEmployeeStatusType::CURRENT . '
						AND e.id =' . ( int ) $intEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeManagerByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						DISTINCT employee_id,
						manager_employee_id as reporting_manager_id
					FROM
						team_employees
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ')
						AND is_primary_team = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByPrimaryTeam( $objDatabase ) {

		$strSql = ' SELECT
						te.*,
						e1.name_full,
						e1.email_address,
						e1.is_project_manager,
						d.name AS designation_name,
						u.id AS user_id
					FROM
						team_employees AS te
						LEFT JOIN employees AS e1 ON ( te.employee_id = e1.id )
						LEFT JOIN employees AS e2 ON ( e1.reporting_manager_id = e2.id )
	 					LEFT JOIN designations AS d ON ( d.id = e1.designation_id )
						JOIN users AS u ON ( u.employee_id = e1.id )
					WHERE
						te.is_primary_team = 1
						AND e1.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e2.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY
						e1.name_full';

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByReportingManagerId( $intManagerEmployeeId, $objDatabase, $arrintDevQaEmployeeIds = NULL ) {

		if( false == is_numeric( $intManagerEmployeeId ) ) {
			return NULL;
		}

		if( true == valArr( $arrintDevQaEmployeeIds ) ) {
			$strWhere = ' ( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN ( ' . implode( ',', $arrintDevQaEmployeeIds ) . ' ) )';
		} else {
			$strWhere = ' e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;
		}

		// recursive fetching employee id based on team Heirarchy based on ManagerEmployeeId

		$strSql	= 'WITH RECURSIVE empl_hierarchy AS (
						SELECT e.id AS employee_id
						FROM employees e
						WHERE ' . $strWhere . '
						UNION ALL
						SELECT e.id AS employee_id
						FROM empl_hierarchy r
						JOIN employees e ON ( e.reporting_manager_id = r.employee_id )
						WHERE e.id != e.reporting_manager_id
					)
					SELECT DISTINCT eh.employee_id
					FROM 
						empl_hierarchy eh
						JOIN employees e ON ( e.id = eh.employee_id )
					WHERE e.date_terminated IS NULL 
					AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY eh.employee_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesHeirarchyByReportingManagerId( $intEmployeeId, $objDatabase, $strCountry = '', $arrintDevQaEmployeeIds = NULL ) {

		$strJoin = '';
		if( true == valStr( $strCountry ) ) {
			$strJoin = ' JOIN employee_addresses ead ON ead.employee_id = e.id AND ead.address_type_id=' . CAddressType::PRIMARY . ' AND ead.country_code =\'' . $strCountry . '\'';
		}

		if( true == valArr( $arrintDevQaEmployeeIds ) ) {
			$strWhere = ' e.id IN ( ' . ( int ) $intEmployeeId . ',' . implode( ',', $arrintDevQaEmployeeIds ) . ' ) ';
		} else {
			$strWhere = ' e.id = ' . ( int ) $intEmployeeId;
		}

		$strSql = '	WITH RECURSIVE empl_hierarchy AS (
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM 
							employees e
						WHERE ' . $strWhere . '
						UNION ALL
						SELECT 
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM
							empl_hierarchy r
							JOIN employees e ON ( e.reporting_manager_id = r.employee_id )
						WHERE 
							( e.id != e.reporting_manager_id )
					)
					SELECT
						DISTINCT e.id,
						e.id as employee_id,
						eh.manager_employee_id,
						e.name_full,
						e.designation_id,
						e.preferred_name
					FROM
						empl_hierarchy eh
						JOIN employees e ON ( e.id = eh.employee_id  AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN users u ON ( u.employee_id = e.id )
						' . $strJoin . '
					ORDER BY
						e.name_full';

		if( false == valStr( $strCountry ) ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return parent::fetchTeamEmployees( $strSql, $objDatabase );
		}
	}

	public static function fetchManagersHirerchyByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'WITH RECURSIVE all_managers(employee_id, manager_employee_id, depth) AS
						  ( SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id,
								1
							FROM employees e
							WHERE
								e.id = ' . ( int ) $intEmployeeId . '
							UNION ALL
							SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id,
								am.depth + 1
							FROM employees e,
							all_managers am
							WHERE
								e.id <> e.reporting_manager_id
								AND e.id = am.manager_employee_id )
						SELECT e.preferred_name AS name_full,
							all_managers.*
						FROM all_managers
						JOIN employees e ON (all_managers.employee_id = e.id)
						WHERE e.date_terminated IS NULL
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						ORDER BY all_managers.depth DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedRecursiveAllTeamEmployeesByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $arrstrTeamEmployeesFilter, $intPdmEmployeeId = NULL, $boolIsPdm = false, $boolShowDisabledData = false, $boolShowMyTeamSprint = false, $boolShowHonorBadge = false, $intEmployeeDesignationId = NULL ) {

		if( false == is_numeric( $intManagerEmployeeId ) || false == valArr( $arrstrTeamEmployeesFilter ) ) {
			return NULL;
		}

		$intOffset 				= ( true == isset( $arrstrTeamEmployeesFilter['page_no'] ) && true == isset( $arrstrTeamEmployeesFilter['page_size'] ) ) ? $arrstrTeamEmployeesFilter['page_size'] * ( $arrstrTeamEmployeesFilter['page_no'] - 1 ) : 0;
		$intLimit 				= ( true == isset( $arrstrTeamEmployeesFilter['page_size'] ) ) ? ( int ) $arrstrTeamEmployeesFilter['page_size'] : '';
		$strOrderByField 		= ' name_full ';
		$strOrderByType			= ' ASC';
		$boolIsShowAll			= ( true == isset( $arrstrTeamEmployeesFilter['show_all'] ) && true == $arrstrTeamEmployeesFilter['show_all'] ) ? true : false;
		$strWhere				= $strGroupByClause = '';

		if( true == isset( $arrstrTeamEmployeesFilter['is_add'] ) && true == $arrstrTeamEmployeesFilter['is_add'] ) {
			$strCondition = ' AND t.add_employee_id = t.tpm_employee_id';
		}

		if( true == isset( $arrstrTeamEmployeesFilter['is_qam'] ) && true == $arrstrTeamEmployeesFilter['is_qam'] ) {
			$strCondition = ' AND  ( t.qam_employee_id = ' . ( int ) $intManagerEmployeeId . ' OR all_employees.manager_employee_id = ' . $intManagerEmployeeId . ' ) ';
		}

		if( true == valArr( $arrstrTeamEmployeesFilter ) ) {
			if( true == isset( $arrstrTeamEmployeesFilter['order_by_field'] ) ) {

				switch( $arrstrTeamEmployeesFilter['order_by_field'] ) {

					case 'employee_name':
						$strOrderByField = ' name_full';
						break;

					case 'team_name':
						$strOrderByField = ' team_name';
						break;

					default:
						$strOrderByField = ' name_full';
						break;
				}
			}

			if( true == isset( $arrstrTeamEmployeesFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrTeamEmployeesFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}
		}

		$strDistinctSelectClause 	= ' DISTINCT e.id AS employee_id, ';
		$strSelectClause			= ' e.reporting_manager_id AS manager_employee_id, te.team_id AS team_id';
		$strFromClause				= ' employees AS e ';
		$strJoinClause				= ' JOIN team_employees te ON ( e.id = te.employee_id ) LEFT JOIN team_employees te1 ON ( te.employee_id = te1.manager_employee_id )';

		if( true == $boolIsPdm ) {
			$strJoinClause .= 'LEFT JOIN team_employees te2 ON ( e.reporting_manager_id = te2.employee_id )';
			$strJoinClause .= 'JOIN teams t ON ( te.team_id = t.id AND t.sdm_employee_id = ' . ( int ) $intPdmEmployeeId . ')';
		} else {
			$strJoinClause .= 'JOIN teams t ON ( te.team_id = t.id )';
		}

		if( true == in_array( $intEmployeeDesignationId, CDesignation::$c_arrintTpmDesignationIds ) ) {
			$strWhereClause  = ' AND te.team_id IN (SELECT id FROM teams WHERE (manager_employee_id = ' . ( int ) $intManagerEmployeeId . ' OR tpm_employee_id = ' . ( int ) $intManagerEmployeeId . ') AND deleted_on IS NULL) AND e.department_id= ' . CDepartment::QA . ' AND 
			e.reporting_manager_id NOT IN ( SELECT employee_id FROM team_employees WHERE team_id IN (SELECT id FROM teams WHERE (manager_employee_id = ' . ( int ) $intManagerEmployeeId . ' OR tpm_employee_id = ' . ( int ) $intManagerEmployeeId . ') AND deleted_on IS NULL) AND is_primary_team = 1 ) AND te.is_primary_team = 1 ';
		} else if( true == in_array( $intEmployeeDesignationId, CDesignation::$c_arrintQamLeadDesignationIds ) && true == valArr( $arrstrTeamEmployeesFilter['dev_qa_employee_ids'] ) ) {
			$strWhereClause		= ' AND e.id IN (' . implode( ',', $arrstrTeamEmployeesFilter['dev_qa_employee_ids'] ) . ') AND te.is_primary_team = 1 ';
		} else if( true == valArr( $arrstrTeamEmployeesFilter['dev_qa_employee_ids'] ) ) {
			$strWhereClause		= ' AND ( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN (' . implode( ',', $arrstrTeamEmployeesFilter['dev_qa_employee_ids'] ) . ') ) AND te.is_primary_team = 1 ';
		} else {
			$strWhereClause		= ' AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' AND te.is_primary_team = 1 ';
		}
		$strUnionAll = ( true == $arrstrTeamEmployeesFilter['is_md'] ) ? '' : ' UNION ALL SELECT e.id AS employee_id, te.team_id, e.reporting_manager_id AS manager_employee_id, al.depth + 1 FROM team_employees te JOIN employees e ON ( te.employee_id = e.id ) JOIN all_employees al ON ( e.reporting_manager_id = al.employee_id ) WHERE te.is_primary_team = 1 AND e.id <> ' . ( int ) $intManagerEmployeeId . '';

		$strRecursiveSql 			= ' WITH RECURSIVE all_employees( employee_id, team_id, manager_employee_id, depth ) AS (
											SELECT
												e.id AS employee_id,
												te.team_id,
												e.reporting_manager_id AS manager_employee_id,
												1
											FROM
												team_employees te
												JOIN employees e ON ( te.employee_id = e.id )
											WHERE
												e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
												AND te.is_primary_team = 1
												AND e.id <> ' . ( int ) $intManagerEmployeeId . '
											' . $strUnionAll . '
										)';

		if( true == $boolIsShowAll ) {
			$strDistinctSelectClause 	= '';
			$strSelectClause			= ' all_employees.* ';
			$strFromClause				= ' all_employees ';
			$strJoinClause				= ' JOIN employees e ON ( all_employees.employee_id = e.id ) JOIN teams t ON ( all_employees.team_id = t.id ' . $strCondition . ')';
			$strWhereClause				= '';
		}

		if( false == $boolShowDisabledData ) {
			$strWhere = ' AND e.date_terminated IS NULL AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT;
		}

		if( true == $boolShowHonorBadge ) {
			$strMonthPrevious	= date( 'm', strtotime( '-1 month' ) );
			$strYear = date( 'Y' );
			if( $strMonthPrevious == '12' ) {
				$strYear = date( 'Y', strtotime( '-1 year' ) );
			}

			$strSelectClause	.= ', COUNT( DISTINCT hb.id) AS badge_count';
			$strJoinClause		.= ' LEFT JOIN honour_badges hb ON ( hb.target_employee_id = e.id AND EXTRACT(MONTH FROM hb.created_on) = ' . $strMonthPrevious . ' AND EXTRACT(YEAR FROM hb.created_on) = ' . $strYear . ' AND hb.deleted_by IS NULL ) ';
			$strGroupByClause	= ' GROUP BY e.id, d.name, d.country_code, t.name, t.hr_representative_employee_id, e.reporting_manager_id, te.team_id ';
		}

		if( true == valArr( $arrstrTeamEmployeesFilter['qam_employee_ids'] ) ) {
			$strWhereClause .= ' OR  e.id IN (' . implode( ',', $arrstrTeamEmployeesFilter['qam_employee_ids'] ) . ')';
		}

		$strMyTeamSprintJoinClause = '';
		if( true == $boolShowMyTeamSprint ) {
			$strSprintDepartments = implode( ',', CDepartment::$c_arrintSprintDepartmentIds );
			$strMyTeamSprintJoinClause = ' JOIN departments de ON ( e.department_id = de.id AND e.department_id IN ( ' . $strSprintDepartments . ' ) ) 
											JOIN team_employees t2 ON ( e.id = t2.manager_employee_id AND t2.is_primary_team = 1 )';

			$strSql = ' 
						SELECT 
							fn.*
						FROM
						(
						SELECT
						' . $strDistinctSelectClause . '
						e.preferred_name AS name_full,
						d.name AS designation_name,
						d.country_code,
						e.employee_status_type_id AS employee_status_type_id,
						t.name AS team_name,
						t.hr_representative_employee_id,
						' . $strSelectClause . '
					FROM
						' . $strFromClause . $strJoinClause . '
						JOIN designations d ON ( e.designation_id = d.id )
						' . $strMyTeamSprintJoinClause . '
					WHERE
						e.id <> ' . ( int ) $intManagerEmployeeId . $strWhere . $strWhereClause . '
					ORDER BY
						' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset . ' ) AS fn
					WHERE 
						(
								SELECT 
									COUNT( te3.id )
								FROM 
									team_employees as te3
									JOIN employees as e3 ON ( te3.employee_id = e3.id AND te3.manager_employee_id = fn.employee_id AND te3.is_primary_team = 1 AND e3.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . ' )
							) >= 1';
		} else {
			$strSql = ' SELECT
						' . $strDistinctSelectClause . '
						e.preferred_name AS name_full,
						d.name AS designation_name,
						d.country_code,
						e.employee_status_type_id AS employee_status_type_id,
						t.name AS team_name,
						t.hr_representative_employee_id,
						' . $strSelectClause . '
					FROM
						' . $strFromClause . $strJoinClause . '
						JOIN designations d ON ( e.designation_id = d.id )
						' . $strMyTeamSprintJoinClause . '
					WHERE
						e.id <> ' . ( int ) $intManagerEmployeeId . $strWhere . $strWhereClause . $strGroupByClause . '
					ORDER BY
						' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;
		}

		if( true == is_numeric( $intLimit ) ) {
			$strSql .= ' LIMIT ' . $intLimit;
		}

		if( true == $boolIsShowAll ) {
			$strSql = $strRecursiveSql . $strSql;
		}

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId, $objDatabase, $arrstrTeamEmployeesFilter ) {

		if( false == valId( $intHrRepresentativeEmployeeId ) || false == valArr( $arrstrTeamEmployeesFilter ) ) {
			return NULL;
		}

		$intOffset 						 = ( true == isset( $arrstrTeamEmployeesFilter['page_no'] ) && true == isset( $arrstrTeamEmployeesFilter['page_size'] ) ) ? $arrstrTeamEmployeesFilter['page_size'] * ( $arrstrTeamEmployeesFilter['page_no'] - 1 ) : 0;
		$intLimit 						 = ( true == isset( $arrstrTeamEmployeesFilter['page_size'] ) ) ? ( int ) $arrstrTeamEmployeesFilter['page_size'] : '';
		$strOrderByField 				 = ' name_full ';
		$strOrderByType					 = ' ASC';
		$boolIsShowAll					 = ( true == isset( $arrstrTeamEmployeesFilter['show_all'] ) && true == $arrstrTeamEmployeesFilter['show_all'] ) ? true : false;

		if( true == valArr( $arrstrTeamEmployeesFilter ) ) {
			if( true == isset( $arrstrTeamEmployeesFilter['order_by_field'] ) ) {

				switch( $arrstrTeamEmployeesFilter['order_by_field'] ) {

					case 'employee_name':
						$strOrderByField = ' name_full';
						break;

					case 'team_name':
						$strOrderByField = ' team_name';
						break;

					default:
						$strOrderByField = ' name_full';
						break;
				}
			}

			if( true == isset( $arrstrTeamEmployeesFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrTeamEmployeesFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}
		}

		$strSql = ' SELECT
						DISTINCT e.id AS employee_id,
						e.preferred_name AS name_full,
						d.name AS designation_name,
						d.country_code,
						t.name AS team_name,
						e1.reporting_manager_id AS manager_employee_id,
						te.team_id,
						t.hr_representative_employee_id
					FROM
						employees AS e
						JOIN employees e1 ON ( e.id = e1.reporting_manager_id )
						JOIN team_employees te ON ( te.employee_id = e1.id )
						JOIN teams t ON ( te.team_id = t.id )
						JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						e.date_terminated IS NULL
						AND e.id <> ' . ( int ) $intHrRepresentativeEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
						AND te.is_primary_team = 1 AND e1.employee_status_type_id = 1
					ORDER BY
						' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;
		if( true == is_numeric( $intLimit ) ) $strSql .= ' LIMIT ' . $intLimit;

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesCountByHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId, $objDatabase, $arrstrTeamEmployeesFilter ) {

		if( false == valId( $intHrRepresentativeEmployeeId ) || false == valArr( $arrstrTeamEmployeesFilter ) ) {
			return NULL;
		}

		$boolIsShowAll = ( true == isset( $arrstrTeamEmployeesFilter['show_all'] ) && true == $arrstrTeamEmployeesFilter['show_all'] ) ? true : false;

		$strSql = ' SELECT
						COUNT( DISTINCT e.id )
					FROM
						employees AS e
						JOIN employees e1 ON ( e.id = e1.reporting_manager_id )
						JOIN team_employees te ON ( te.employee_id = e1.id )
						JOIN teams t ON ( te.team_id = t.id )
						JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						e.date_terminated IS NULL
						AND e.id <> ' . ( int ) $intHrRepresentativeEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
						AND te.is_primary_team = 1 AND e1.employee_status_type_id = 1';

		$arrintTeamEmployeeCount = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintTeamEmployeeCount ) ) {
			return 0;
		}

		return $arrintTeamEmployeeCount[0]['count'];

	}

	public static function fetchTeamEmployeesCountByPdmEmployeeId( $intPdmEmployeeId, $objDatabase, $arrstrTeamEmployeesFilter ) {

		if( false == is_numeric( $intPdmEmployeeId ) || false == valArr( $arrstrTeamEmployeesFilter ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						COUNT( DISTINCT e.id )
					FROM
						employees AS e
						JOIN employees e1 ON ( e.id = e1.reporting_manager_id )
						JOIN team_employees te ON ( te.employee_id = e1.id )
						JOIN teams t ON ( te.team_id = t.id )
						JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						e.date_terminated IS NULL
						AND e.id <> ' . ( int ) $intPdmEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND t.sdm_employee_id = ' . ( int ) $intPdmEmployeeId . '
						AND te.is_primary_team = 1 AND e1.employee_status_type_id = 1';

		$arrintTeamEmployeeCount = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintTeamEmployeeCount ) ) {
			return 0;
		}

		return $arrintTeamEmployeeCount[0]['count'];

	}

	public static function fetchPaginatedRecursiveAllTeamEmployeesCountByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $arrstrTeamEmployeesFilter, $intPdmEmployeeId = NULL, $boolIsPdm = false ) {
		if( false == valId( $intManagerEmployeeId ) ) {
			return NULL;
		}

		$boolIsShowAll = ( true == isset( $arrstrTeamEmployeesFilter['show_all'] ) && true == $arrstrTeamEmployeesFilter['show_all'] ) ? true : false;

		$strSelectClause 	= ' DISTINCT e.id AS employee_id ';
		$strFromClause		= ' employees e ';
		$strJoinClause		= ' JOIN team_employees te ON ( e.id = te.employee_id )';
		$strRecursiveSql 	= ' WITH RECURSIVE all_employees( employee_id, manager_employee_id, depth ) AS (
								SELECT
									e.id AS employee_id,
									e.reporting_manager_id AS manager_employee_id,
									1
								FROM
									employees e
								WHERE
									e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
									AND e.id <> ' . ( int ) $intManagerEmployeeId . '
								UNION ALL
								SELECT
									e.id AS employee_id,
									e.reporting_manager_id AS manager_employee_id,
									al.depth + 1
								FROM
									employees e,
									all_employees al
								WHERE
									e.reporting_manager_id = al.employee_id
									AND e.id <> ' . ( int ) $intManagerEmployeeId . '
							)';

		if( true == valArr( $arrstrTeamEmployeesFilter['dev_qa_employee_ids'] ) ) {
			$strWhereClause	= ' AND ( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN (' . implode( ',', $arrstrTeamEmployeesFilter['dev_qa_employee_ids'] ) . ') ) AND te.is_primary_team = 1 ';
		} else {
			$strWhereClause	= ' AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' AND te.is_primary_team = 1 ';
		}

		if( true == $boolIsPdm ) {
			$strJoinClause .= 'LEFT JOIN team_employees te2 ON ( e.reporting_manager_id = te2.employee_id )';
			$strJoinClause .= 'JOIN teams t ON ( te.team_id = t.id AND t.sdm_employee_id = ' . ( int ) $intPdmEmployeeId . ')';
		} else {
			$strJoinClause .= 'JOIN teams t ON ( te.team_id = t.id )';
		}

		if( true == $boolIsShowAll ) {
			$strSelectClause 	= ' e.id ';
			$strFromClause 		= ' all_employees ';
			$strJoinClause		= ' JOIN employees e ON ( all_employees.employee_id = e.id ) ';
			$strWhereClause		= '';
		}

		$strSql = ' SELECT
							' . $strSelectClause . '
					FROM
							' . $strFromClause . $strJoinClause . '
							JOIN designations d ON ( e.designation_id = d.id )
					WHERE
							e.date_terminated IS NULL
							AND e.id <> ' . ( int ) $intManagerEmployeeId . '
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . $strWhereClause;

		if( true == $boolIsShowAll ) {
			$strSql = $strRecursiveSql . $strSql;
		}

		$arrintTeamEmployeeCount = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintTeamEmployeeCount ) ) {
			return 0;
		}

		return \Psi\Libraries\UtilFunctions\count( $arrintTeamEmployeeCount );
	}

	public static function fetchRecursiveEmployeeManagersByEmployeeId( $intEmployeeId, $intCurrentEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) || false == is_numeric( $intCurrentEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' WITH RECURSIVE all_managers( employee_id, manager_employee_id, team_id, depth ) AS (
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id,
							te.team_id,
							1
						 FROM
							team_employees te
							JOIN employees e ON ( te.employee_id = e.id )
						 WHERE
							e.id = ' . ( int ) $intEmployeeId . '
							AND te.is_primary_team = 1
						 UNION ALL
						 SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id,
							te.team_id,
							am.depth + 1
						 FROM
							all_managers am
							JOIN employees e ON ( e.id = am.manager_employee_id )
							JOIN team_employees te ON ( te.employee_id = e.id )
						 WHERE
							e.id <> e.reporting_manager_id
							AND am.manager_employee_id != ' . ( int ) $intCurrentEmployeeId . '
							AND te.is_primary_team = 1)
					SELECT
						e.preferred_name AS name_full,
						e.email_address,
						all_managers.*,
						t.name AS team_name
					FROM
						all_managers
						JOIN employees e ON ( all_managers.employee_id = e.id )
						JOIN teams t ON ( all_managers.team_id = t.id )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = 1
					ORDER BY
						all_managers.depth DESC';

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamManagersByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $intPdmEmployeeId = NULL, $boolIsExecutiveAtTop = false, $strCountryCode = NULL, $arrintDevQaEmployeeIds = NULL ) {
		if( false == valId( $intManagerEmployeeId ) ) {
			return NULL;
		}

		$strOrderByCondition 	= ( true == $boolIsExecutiveAtTop && true == valArr( CDesignation::$c_arrintChiefExecutiveDesignationIds ) ) ? ' CASE WHEN e.designation_id IN( ' . implode( ',', CDesignation::$c_arrintChiefExecutiveDesignationIds ) . ' ) THEN e.name_full END ASC, ' : '';
		$strWhereClause 		= ( true == $boolIsExecutiveAtTop ) ? ' AND d.country_code = \'' . $strCountryCode . '\'' : '';

		if( true == valId( $intPdmEmployeeId ) ) {
			$strJoinClause = '  JOIN teams t ON ( te1.team_id = t.id AND t.sdm_employee_id = ' . ( int ) $intPdmEmployeeId . ' )';
		}

		if( true == valArr( $arrintDevQaEmployeeIds ) ) {
			$strWhere	= ' AND ( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN (' . implode( ',', $arrintDevQaEmployeeIds ) . ') ) ';
		} else {
			$strWhere	= ' AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;
		}

		$strSql = ' SELECT
						e.id AS employee_id,
						e.preferred_name AS name_full,
						te.team_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						team_employees te
						JOIN employees e ON ( te.employee_id = e.id )
						JOIN team_employees te1 ON ( te1.manager_employee_id = te.employee_id AND te1.is_primary_team = 1 ) ' . $strJoinClause . '
						JOIN employees e1 ON ( te1.employee_id = e1.id AND e1.date_terminated IS NULL )
						JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						e.date_terminated IS NULL' . $strWhereClause . '
						AND e.id <> ' . ( int ) $intManagerEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND te.is_primary_team = 1
						' . $strWhere . '
					 GROUP BY
						e.id,
						te.team_id,
						e.reporting_manager_id,
						d.country_code
					ORDER BY ' . $strOrderByCondition . ' name_full ASC';

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamManagersByPdmEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		if( false == is_numeric( $intManagerEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT e.id AS employee_id,
						e.preferred_name AS name_full,
						te.team_id,
						e.id AS manager_employee_id
					FROM
						team_employees te
						JOIN employees e1 ON ( te.employee_id = e1.id AND te.is_primary_team = 1 )
						JOIN employees e ON ( e1.reporting_manager_id = e.id )
						JOIN teams t ON ( t.id = te.team_id )
					WHERE
						e.date_terminated IS NULL
						AND e.id <> ' . ( int ) $intManagerEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND t.sdm_employee_id =' . ( int ) $intManagerEmployeeId . '
					ORDER BY
						name_full ASC';

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeCountByEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		if( false == valId( $intManagerEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
	 					count( DISTINCT te.id )
 					FROM
						team_employees te
	 					JOIN employees e ON te.employee_id = e.id
 					WHERE
						e.id IS NOT NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL
						AND te.is_primary_team = 1
						AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;

		$arrintTeamsCountsData = fetchData( $strSql, $objDatabase );

		return $arrintTeamsCountsData[0]['count'];
	}

	public static function fetchEmployeeIdsByTeamId( $intTeamId, $objDatabase ) {

		if( true == is_null( $intTeamId ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						e.id

					FROM
						employees AS e
						LEFT JOIN team_employees AS te ON ( e.id = te.employee_id and te.is_primary_team = 1)
					WHERE
						te.team_id = ' . ( int ) $intTeamId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND date_terminated IS NULL
					ORDER BY
						e.id';

		$arrintEmployeeIds = fetchData( $strSql, $objDatabase );

		if( false != isset( $arrintEmployeeIds ) ) {
			foreach( $arrintEmployeeIds as $intKey => $arrintValue ) {
				$arrintEmployeeIds[$intKey] = $arrintValue['id'];
			}
			return $arrintEmployeeIds;
		}
	}

	public static function fetchTeamEmployeesByManagerId( $intManagerEmployeeId, $intOptionalManagerEmployeeId = NULL, $arrstrReviewFilter, $objDatabase, $intEmployeeSkillRatingsHistory = 0 ) {

		$strOrderByField 			= ( true == isset( $arrstrReviewFilter['order_by_field'] ) ) ? $arrstrReviewFilter['order_by_field'] : 'name_full';
		$strOrderByType				= ( true == isset( $arrstrReviewFilter['order_by_type'] ) ) ? $arrstrReviewFilter['order_by_type'] : 'ASC';

		switch( $strOrderByField ) {
			case 'name_full':
				$strOrderByField = ' employee_name';
				break;

			case 'team_name':
				$strOrderByField = ' team_name';
				break;

			default:
				$strOrderByField = ' employee_name';
				break;
		}
		if( 1 == ( int ) $intEmployeeSkillRatingsHistory ) {
			$strWhereClause = 'AND e.id = ' . ( int ) $intOptionalManagerEmployeeId;
		} else {
			$strWhereClause = 'AND e.id <> ' . ( int ) $intManagerEmployeeId;
		}

		$strSql = ' SELECT
					 *
					FROM
						(
						SELECT
						DISTINCT ON ( e.id )
							t.id AS team_id,
							e.id AS employee_id,
							e.preferred_name AS employee_name,
							d.name AS designation_name,
							t.name AS team_name
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id )
						JOIN employees e ON ( te.employee_id = e.id AND e.date_terminated IS NULL AND te.is_primary_team = 1)
						LEFT JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
						AND e.date_terminated IS NULL
						' . $strWhereClause . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					) AS sub_query
					ORDER BY
						' . $strOrderByField . ' ' . $strOrderByType . ' ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchSimpleManagerEmployeeIdsByEmployeeIds( $arrintManagerEmployeeIds, $objAdminDatabase ) {

		if( false == valArr( $arrintManagerEmployeeIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees as e
						JOIN team_employees as te ON ( e.id = te.employee_id )
					WHERE
						te.team_id IN ( SELECT id FROM teams WHERE manager_employee_id IN ( ' . implode( ',', $arrintManagerEmployeeIds ) . ' ) )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL
						AND te.is_primary_team = 1
					GROUP BY
						e.reporting_manager_id';

		$arrintManagerEmployeeIds = fetchData( $strSql, $objAdminDatabase );
		$arrintManagerEmployeeIds = rekeyArray( 'manager_employee_id', $arrintManagerEmployeeIds );

		return $arrintManagerEmployeeIds;
	}

	public static function fetchTeamEmployeesDetailsByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( true == is_null( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						team_id,
						manager_employee_id
					FROM
						team_employees
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						OR manager_employee_id = ' . ( int ) $intEmployeeId . '
					GROUP BY
						team_id,
						manager_employee_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCountryspecificParentManagersFromEmployeeId( $intEmployeeId, $intPurchaseRequestTypeId, $objDatabase, $intStartOrder = 1, $intDesignationId = NULL, $boolRequirementType = NULL ) {

		// 1. Select Employee Mail chain
		// 2. Remove Preetam and Dave from mail chain
		// 3. Add final approver from the designation table

		$strSql = 'SELECT * FROM get_all_parent_managers_from_employee ( ' . ( int ) $intEmployeeId . ', ' . ( int ) $intStartOrder . ' )';

		$arrmixManagers = fetchData( $strSql, $objDatabase );
		$arrmixManagers = rekeyArray( 'manager_emp_id', $arrmixManagers );

		unset( $arrmixManagers[CEmployee::ID_DAVID_BATEMEN] );
		unset( $arrmixManagers[CEmployee::ID_PREETAM_YADAV] );

		$arrmixEmployeeDetails = \Psi\Eos\Admin\CEmployeeAddresses::createService()->fetchEmployeeAddressByEmployeeId( $intEmployeeId, $objDatabase, CAddressType::PRIMARY );

		if( CPurchaseRequestType::ASSET_PURCHASE == $intPurchaseRequestTypeId && CCountry::CODE_INDIA == $arrmixEmployeeDetails->getCountryCode() ) {
			unset( $arrmixManagers[CEmployee::ID_RYAN_BYRD], $arrmixManagers[CEmployee::ID_CHASE_HARRINGTON] );
			if( false == in_array( CEmployee::ID_SANDEEP_GARUD, array_keys( $arrmixManagers ) ) && false == in_array( $intEmployeeId, [ CEmployee::ID_SANDEEP_GARUD, CEmployee::ID_PREETAM_YADAV ] ) ) {
				$intDepth                                    = \Psi\Libraries\UtilFunctions\count( $arrmixManagers ) + $intStartOrder;
				$arrmixManagers[CEmployee::ID_SANDEEP_GARUD] = [ 'manager_emp_id' => CEmployee::ID_SANDEEP_GARUD, 'depth' => $intDepth ];
			}
		}

		$intFinalApproverId = CPurchaseRequest::fetchFinalApproverByEmployeeId( $intEmployeeId, $intPurchaseRequestTypeId, $objDatabase, $boolRequirementType, $intDesignationId );

		if( true == valId( $intFinalApproverId ) ) {
			if( false == array_key_exists( $intFinalApproverId, $arrmixManagers ) ) {
				$intOrderNum						 = \Psi\Libraries\UtilFunctions\count( $arrmixManagers ) + $intStartOrder;
				$arrmixManagers[$intFinalApproverId] = [ 'manager_emp_id' => $intFinalApproverId, 'depth' => $intOrderNum ];
			}

			foreach( $arrmixManagers as $intKey => $arrmixValue ) {
				if( $arrmixValue['depth'] > $arrmixManagers[$intFinalApproverId]['depth'] ) {
					unset( $arrmixManagers[$intKey] );
				}
			}
		}

		return $arrmixManagers;
	}

	public static function fetchEmployeesByManagerEmployeeIds( $arrintManagerEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintManagerEmployeeIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						DISTINCT e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
						e.reporting_manager_id IN ( ' . implode( ',', $arrintManagerEmployeeIds ) . ' )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeIdsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $boolIsShowDisabledData = false ) {

		if( false == valId( $intManagerEmployeeId ) ) {
			return NULL;
		}

		$strWhereCondition = '';

		if( false == $boolIsShowDisabledData ) {
			$strWhereCondition = ' AND e.date_terminated IS NULL AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT;
		}

		$strSql = 'WITH RECURSIVE all_employees( employee_id ) AS
					(
						SELECT
							e.id AS employee_id
						FROM
							employees e
						WHERE
							e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
							AND e.id <>  ' . ( int ) $intManagerEmployeeId . '
						UNION ALL
						SELECT
							e.id AS employee_id
						FROM
							employees e,
							all_employees al
						WHERE
							e.reporting_manager_id = al.employee_id
							AND e.id <>  ' . ( int ) $intManagerEmployeeId . '
							AND e.id != e.reporting_manager_id )
						SELECT
							all_employees.employee_id
						FROM
							all_employees
							JOIN employees e ON ( all_employees.employee_id = e.id )
						WHERE
						  e.id <> ' . ( int ) $intManagerEmployeeId . $strWhereCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTeamEmployeeDetailsByManagerEmployeeId( $objPagination, $objInteractionTrackerFilter, $objAdminDatabase, $boolIsCountOnly = false, $boolIsShowDisabledData = false ) {
		if( false == valObj( $objInteractionTrackerFilter, 'CInteractionTrackerFilter' ) || false == valId( $objInteractionTrackerFilter->getEmployeeId() ) ) {
			return false;
		}

		$intOffset			= ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
		$intLimit			= $objPagination->getPageSize();
		$strWhereCondition	= NULL;

		if( false == $boolIsShowDisabledData ) {
			$strWhereCondition = ' AND e.date_terminated IS NULL AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT;
		}

		if( true == valStr( $objInteractionTrackerFilter->getGenericData() ) ) {
			$strWhereCondition .= ' AND ( LOWER( e.name_full ) LIKE LOWER( \'%' . addslashes( $objInteractionTrackerFilter->getGenericData() ) . '%\' ) )';
		}

		if( true == $boolIsCountOnly ) {
			$strSql = 'SELECT 
							COUNT( employee_id ) AS employees_count ';
		} else {
			$strSql = 'SELECT
							employee_id,
							employee_name,
							manager_employee_id,
							manager_name,
							coach_employee_id,
							e3.preferred_name AS coach_name,
							employee_designation_id,
							employee_title ';
		}

		$strSql .= 'FROM ( WITH RECURSIVE all_employees( employee_id, manager_employee_id ) AS
					(
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM
							employees AS e
						WHERE
							e.reporting_manager_id = ' . ( int ) $objInteractionTrackerFilter->getEmployeeId() . '
							AND e.id <>  ' . ( int ) $objInteractionTrackerFilter->getEmployeeId() . '
						UNION ALL
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM
							employees AS e,
							all_employees AS al
						WHERE
							e.reporting_manager_id = al.employee_id
							AND e.id <>  ' . ( int ) $objInteractionTrackerFilter->getEmployeeId() . ' ) 
						SELECT
							all_employees.employee_id,
							e.preferred_name AS employee_name,
							all_employees.manager_employee_id,
							e2.preferred_name AS manager_name,
							eg.employee_id AS coach_employee_id, 
							d.id AS employee_designation_id, 
							d.name AS employee_title 
						FROM
							all_employees
							JOIN employees AS e ON ( all_employees.employee_id = e.id )
							JOIN employees AS e2 ON ( all_employees.manager_employee_id = e2.id )
							LEFT JOIN employee_goals AS eg ON ( eg.call_agent_employee_id = e.id AND eg.deleted_by IS NULL AND eg.deleted_on IS NULL )
							JOIN designations AS d ON ( e.designation_id = d.id )
						WHERE
							e.id <> ' . ( int ) $objInteractionTrackerFilter->getEmployeeId() . $strWhereCondition . ' ) AS all_employees_result 
							LEFT JOIN employees e3 ON ( all_employees_result.coach_employee_id = e3.id )';

		if( false == $boolIsCountOnly ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$arrmixEmployeeDetails = ( array ) fetchData( $strSql, $objAdminDatabase );

		if( true == $boolIsCountOnly ) {
			return ( true == isset( $arrmixEmployeeDetails[0]['employees_count'] ) ) ? $arrmixEmployeeDetails[0]['employees_count'] : 0;
		} else {
			return $arrmixEmployeeDetails;
		}
	}

	public static function fetchCustomEmployeeDirectorsByEmployeeIds( $arrintEmployeeIds, $objAdminDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT DISTINCT 
						employee_id AS direct_report_employee_id,
						director_name,
						aliased_employee_id AS employee_id
					FROM ( WITH RECURSIVE filtered_employees( employee_id, manager_employee_id, aliased_employee_id, depth ) AS
					(
						SELECT 
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id,
							e.id AS aliased_employee_id,
							0 AS depth
						FROM 
							employees AS e
						WHERE 
							e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) 
						UNION ALL
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id,
							fe.aliased_employee_id,
							depth + 1
						FROM
							employees AS e,
							filtered_employees AS fe
						WHERE 
							e.id = fe.manager_employee_id 
							AND fe.manager_employee_id != fe.aliased_employee_id
					)
					SELECT
						employee_id,
						manager_employee_id,
						e.preferred_name AS director_name,
						aliased_employee_id,
						depth
					FROM
						filtered_employees
						JOIN employees AS e ON ( filtered_employees.manager_employee_id = e.id )
						JOIN designations AS d ON ( e.designation_id = d.id) AND ( e.designation_id IN ( ' . CDesignation::LEASING_CENTER_ASSOCIATE_DIRECTOR . ' ,' . CDesignation::LEASING_CENTER_OPERATIONS_SPECIALIST . ' , ' . CDesignation::DIRECTOR_OF_QUALITY_AND_TRAINING . ' , ' . CDesignation::ASSOCIATE_DIRECTOR_OF_AUXILIARY_SERVICES . ' ) ) ) AS employee_director_result ';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchTeamHierarchyEmployeeIdsByManagerEmployeeIds( $arrintManagerEmployeeIds, $objAdminDatabase, $boolIsShowDisabledData = false ) {
		if( false == valArr( $arrintManagerEmployeeIds ) ) {
			return NULL;
		}

		$strManagerEmployeeIds = implode( ',', $arrintManagerEmployeeIds );
		$strWhereCondition = '';

		if( false == $boolIsShowDisabledData ) {
			$strWhereCondition = ' AND e.date_terminated IS NULL AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT;
		}

		$strSql = 'WITH RECURSIVE all_employees( employee_id ) AS
					(
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM
							employees e
						WHERE
							e.reporting_manager_id IN ( ' . $strManagerEmployeeIds . ' )
							AND e.id NOT IN ( ' . $strManagerEmployeeIds . ' )
						UNION ALL
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM
							employees e,
							all_employees al
						WHERE
							e.reporting_manager_id = al.employee_id
							AND e.id NOT IN ( ' . $strManagerEmployeeIds . ' ) )
						SELECT
							all_employees.employee_id,
							all_employees.manager_employee_id
						FROM
							all_employees
							JOIN employees e ON ( all_employees.employee_id = e.id )
						WHERE
							e.id NOT IN ( ' . $strManagerEmployeeIds . ' ) ' . $strWhereCondition;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchTeamEmployeeIdsByHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId, $objDatabase, $strCountryCode=CCountry::CODE_INDIA ) {

		$strSql = ' SELECT
						DISTINCT e1.id AS employee_id
					FROM
						employees AS e
						JOIN team_employees te ON ( e.id = te.employee_id )
						JOIN employees e1 ON ( e1.id = e.id OR e1.id = e.reporting_manager_id )
						JOIN teams t ON ( te.team_id = t.id )
						JOIN employee_addresses ea ON( e1.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e1.date_terminated IS NULL
						AND e1.id <> ' . ( int ) $intHrRepresentativeEmployeeId . '
						AND e1.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ea.country_code=\'' . $strCountryCode . '\'
						AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeIdsByPdmEmployeeId( $intPdmEmployeeId, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT e1.id AS employee_id,
						t.id AS team_id
					FROM
						employees AS e
						JOIN team_employees te ON ( e.id = te.employee_id )
						JOIN employees e1 ON ( e1.id = e.id OR e1.id = e.reporting_manager_id )
						JOIN teams t ON ( te.team_id = t.id )
						JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						e1.date_terminated IS NULL
						AND e1.id <> ' . ( int ) $intPdmEmployeeId . '
						AND e1.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND t.sdm_employee_id = ' . ( int ) $intPdmEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllTeamEmployeesByManagerEmployeeIds( $arrintManagerEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintManagerEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						*
					FROM
						team_employees
					WHERE
						manager_employee_id IN(' . implode( ',', $arrintManagerEmployeeIds ) . ' )
						AND is_primary_team = 1';

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesByDepartmentId( $intDepartmentId, $objDatabase, $boolPrimaryTeam = false, $boolFetchData = false, $intProductId = NULL, $intProductOptionId = NULL ) {

		$strJoinCondition = 'employees e
							JOIN team_employees te ON ( te.employee_id = e.id )
							JOIN teams t ON ( te.team_id = t.id )';
		$strSelect = '';
		$strWhere = 'AND t.department_id = ' . ( int ) $intDepartmentId;
		if( true == $boolPrimaryTeam ) {

			$strJoinCondition = 'teams t
								JOIN team_employees te ON ( te.team_id = t.id )
								LEFT JOIN ps_products pp ON ( t.ps_product_id = pp.id )
								LEFT JOIN ps_products sub ON ( t.ps_product_id = sub.ps_product_id )
								LEFT JOIN ps_product_options ppo ON ( t.ps_product_id = ppo.ps_product_id )
								JOIN employees e ON ( te.employee_id = e.id OR e.id = pp.sqm_employee_id OR e.id = ppo.sqm_employee_id ) 
								JOIN users u ON (u.employee_id = e.id) 
								LEFT JOIN user_roles ur ON ( u.id = ur.user_id AND ur.deleted_on IS NULL AND ur.deleted_by IS NULL)';
			$strSelect = ', u.id';
			$strWhere = 'AND ( e.department_id = ' . ( int ) $intDepartmentId . ' OR ur.role_id IN ( ' . CRole::TEST_CASE_TESTER . ' , ' . CRole::TEST_CASE_LEADER . ' ) ) 
						AND t.ps_product_id IS NOT NULL 
						AND te.is_primary_team = 1 ';

			if( true == valId( $intProductId ) ) {
				$strWhere .= ' AND t.ps_product_id = ' . ( int ) $intProductId;

				if( true == valId( $intProductOptionId ) ) {
					$strWhere .= ' AND t.ps_product_option_id = ' . ( int ) $intProductOptionId;
				}
			}
		}

		$strSql = ' SELECT
						DISTINCT e.id AS employee_id,
						e.name_full AS name_full,
						e.preferred_name AS preferred_name,
						t.ps_product_id' . $strSelect . '
					FROM
						' . $strJoinCondition . '
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL
						' . $strWhere . '
					ORDER BY
						e.name_full ASC';

		if( true == $boolFetchData ) {
			return fetchData( $strSql, $objDatabase );
		}
		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesDataByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
							te.employee_id, e.reporting_manager_id AS manager_employee_id, t.hr_representative_employee_id, t.id AS team_id
						FROM
							team_employees te
							JOIN employees e ON ( te.employee_id = e.id )
							JOIN teams t ON ( te.team_id = t.id )
						WHERE
							te.is_primary_team = 1
							AND	te.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHrAndReportingManagerByEmployeeId( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						te.employee_id as employee_id, e.reporting_manager_id AS reporting_manager_employee_id, t.hr_representative_employee_id as hr_manager_employee_id
					FROM
						team_employees te
						JOIN employees e ON ( te.employee_id = e.id )
						JOIN teams t ON ( te.team_id = t.id )
					WHERE
						te.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND	te.is_primary_team = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveEmployeesByDepartmentIdByDesignationIds( $intDepartmentId, $arrintDesignationIds, $objDatabase ) {
		if( false == valId( $intDepartmentId ) || false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT e.id AS employee_id,
						e.preferred_name AS name_full
					FROM
						employees e
						JOIN team_employees te ON ( te.employee_id = e.id )
						JOIN teams t ON ( te.team_id = t.id )
					WHERE
						t.department_id = ' . ( int ) $intDepartmentId . '
						AND e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' )
						AND e.employee_status_type_id = 1
						AND e.date_terminated IS NULL
					ORDER BY
						e.preferred_name ASC';

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeDetailsByEmployeeIdByIsPrimaryTeam( $intEmployeeId, $objDatabase ) {

		$strSql = ' SELECT
						*,
						t.name AS team_name,
						te.id as id
					FROM
						team_employees te
						JOIN teams t ON ( te.team_id = t.id )
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND is_primary_team = 1 LIMIT 1';

		return parent::fetchTeamEmployee( $strSql, $objDatabase );
	}

	public static function fetchCurrentTeamEmployeesHeirarchyByReportingManagerId( $intEmployeeId, $objDatabase, $arrintDevQaEmployeeIds = NULL ) {

		if( false == valId( $intEmployeeId ) ) {
			return false;
		}

		if( true == valArr( $arrintDevQaEmployeeIds ) ) {
			$strWhere = ' e.id IN ( ' . ( int ) $intEmployeeId . ',' . implode( ',', $arrintDevQaEmployeeIds ) . ' ) ';
		} else {
			$strWhere = ' e.id = ' . ( int ) $intEmployeeId;
		}

		$strSql = 'WITH RECURSIVE empl_hierarchy AS (
							SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id
							FROM
								employees e
							WHERE
								' . $strWhere . '
							UNION ALL
							SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id
							FROM
								empl_hierarchy r
								JOIN employees e ON ( e.reporting_manager_id = r.employee_id )
							WHERE
								e.id != e.reporting_manager_id
						)
						SELECT
							DISTINCT employee_id,
							manager_employee_id,
							name_full
						FROM
							empl_hierarchy eh
							JOIN employees e ON ( e.id = eh.employee_id )
						WHERE
							date_terminated IS NULL
							AND employee_status_type_id = 1
						ORDER BY
							name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeIdsByAddEmployeeId( $intAddEmployeeId, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT e.id AS employee_id,
						e.preferred_name AS name_full,
						e.id AS manager_employee_id
					FROM
						employees AS e
						JOIN employees e1 ON ( e1.reporting_manager_id = e.id )
						JOIN team_employees te ON ( te.employee_id = e1.id )
						JOIN teams t ON ( te.team_id = t.id )
						JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						e.date_terminated IS NULL
						AND t.deleted_by IS NULL
						AND e.id <> ' . ( int ) $intAddEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND t.add_employee_id = ' . ( int ) $intAddEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByEmployeeIdByFilter( $intEmployeeId, $objDatabase, $arrstrTeamEmployeesFilter = NULL ) {

		if( false == is_numeric( $intEmployeeId ) || false == valArr( $arrstrTeamEmployeesFilter ) ) {
			return NULL;
		}

		$intOffset			= ( true == isset( $arrstrTeamEmployeesFilter['page_no'] ) && true == isset( $arrstrTeamEmployeesFilter['page_size'] ) ) ? $arrstrTeamEmployeesFilter['page_size'] * ( $arrstrTeamEmployeesFilter['page_no'] - 1 ) : 0;
		$intLimit			= ( true == isset( $arrstrTeamEmployeesFilter['page_size'] ) ) ? ( int ) $arrstrTeamEmployeesFilter['page_size'] : '';
		$strOrderByField	= ' name_full ';
		$strOrderByType		= ' ASC';
		$strCondition		= $strWhereCondition = $strWhrCondition = '';

		if( true == valArr( $arrstrTeamEmployeesFilter ) ) {
			if( true == isset( $arrstrTeamEmployeesFilter['order_by_field'] ) ) {

				switch( $arrstrTeamEmployeesFilter['order_by_field'] ) {

					case 'employee_name':
						$strOrderByField = ' name_full';
						break;

					case 'team_name':
						$strOrderByField = ' team_name';
						break;

					default:
						$strOrderByField = ' team_name';
						break;
				}
				switch( $arrstrTeamEmployeesFilter['logged_in_employee'] ) {

					case 'bu_hr':
						$strCondition = 'hr_representative_employee_id';
						break;

					case 'sdm':
						$strCondition = 'sdm_employee_id';
						break;

					default:
						$strCondition = 'add_employee_id';
				}
			}

			if( true == isset( $arrstrTeamEmployeesFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrTeamEmployeesFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}
		}

		if( CEmployee::ID_PREETAM_YADAV != $intEmployeeId ) {
			$strWhereCondition = ' AND t.qam_employee_id <> ' . CEmployee::ID_PREETAM_YADAV;
			$strWhrCondition = ' AND t1.tpm_employee_id <> ' . CEmployee::ID_PREETAM_YADAV;
		}

		// As per business logic we are listing teams manager so using t.manager_employee_id
		$strSql = ' SELECT
						DISTINCT ON (e.id) employee_id,
						e.preferred_name AS name_full,
						d.name AS designation_name,
						d.country_code,
						td.team_name,
						td.manager_employee_id,
						td.id AS team_id,
						td.hr_representative_employee_id,
						td.sdm_employee_id
						FROM
							(
							SELECT
								t.name AS team_name,
								t.manager_employee_id,
								t.id,
								t.hr_representative_employee_id,
								t.qam_employee_id AS employee_id,
								t.sdm_employee_id
							FROM
								teams t
							WHERE
								t.deleted_by IS NULL
								AND t.' . $strCondition . ' = ' . ( int ) $intEmployeeId . '
								AND t.qam_employee_id IS NOT NULL
								' . $strWhereCondition . '
							UNION
							SELECT
								t1.name AS team_name,
								t1.manager_employee_id,
								t1.id,
								t1.hr_representative_employee_id,
								t1.tpm_employee_id AS employee_id,
								t1.sdm_employee_id
							FROM
								teams t1
							WHERE
								t1.deleted_by IS NULL
								AND t1.' . $strCondition . ' = ' . ( int ) $intEmployeeId . '
								AND t1.tpm_employee_id IS NOT NULL
								' . $strWhrCondition . '
							) AS td
							JOIN employees e ON ( td.employee_id = e.id )
							JOIN designations d ON ( e.designation_id = d.id )
						WHERE
							e.date_terminated IS NULL
							AND e.id <> ' . ( int ) $intEmployeeId . '
							AND e.employee_status_type_id = 1
					ORDER BY
						e.id,
						' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( true == is_numeric( $intLimit ) ) {
			$strSql .= ' LIMIT ' . $intLimit;
		}
		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchCurrentTeamEmployeesHeirarchyByReportingManagerIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'WITH RECURSIVE
						empl_hierarchy AS (
							SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id,
								e.preferred_name AS name_full,
								t.hr_representative_employee_id
							FROM
								team_employees te
								JOIN employees e ON (te.employee_id = e.id)
								JOIN teams t ON (t.id = team_id)
							WHERE
								e.id IN ( ' . sqlIntImplode( $arrintEmployeeIds ) . ' )
								AND is_primary_team = 1
								AND e.date_terminated IS NULL
								AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
								AND t.deleted_by IS NULL
						UNION ALL
							SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id,
								e.preferred_name AS name_full,
								t.hr_representative_employee_id
							FROM empl_hierarchy r
								JOIN employees e ON ( e.reporting_manager_id = r.employee_id )
								JOIN team_employees te ON ( te.employee_id = e.id )
								JOIN teams t ON (t.id = team_id)
							WHERE e.id != e.reporting_manager_id 
								AND is_primary_team = 1
								AND e.date_terminated IS NULL
								AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
								AND t.deleted_by IS NULL
						)
						SELECT
							eh.employee_id,
							eh.manager_employee_id,
							eh.name_full,
							eh.hr_representative_employee_id
						FROM
							empl_hierarchy eh
						GROUP BY
							eh.employee_id,
							eh.manager_employee_id,
							eh.name_full,
							eh.hr_representative_employee_id
						ORDER BY name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllTeamEmployeesByManagerEmployeeIdByApproverRoleManagerEmployeeIds( $intManagerEmployeeId, $arrintApproverRoleManagerEmployeeIds, $objDatabase ) {

		if( true == is_null( $intManagerEmployeeId ) || false == valArr( $arrintApproverRoleManagerEmployeeIds ) ) {
			return NULL;
		}

		$strSql = ' WITH RECURSIVE
						all_employees( employee_id, manager_employee_id, depth, level ) AS (
							SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id,
								1,
								0
							FROM
								employees e
							WHERE
								 e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
								AND e.id <> ' . ( int ) $intManagerEmployeeId . '
							UNION ALL
							SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id,
								al.depth + 1,
								CASE
										WHEN e.reporting_manager_id IN ( ' . implode( ' ,', $arrintApproverRoleManagerEmployeeIds ) . ' ) OR al.level > 0 THEN
											al.level+1
										ELSE
											al.level
								END AS level
							FROM
								employees e,
								all_employees al
							WHERE
								e.reporting_manager_id = al.employee_id
								AND e.id <> ' . ( int ) $intManagerEmployeeId . '
						)
						SELECT
							e.id
						FROM
							all_employees al
							JOIN employees e ON ( al.employee_id = e.id )
						WHERE
							( e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' OR  e.employee_status_type_id = ' . CEmployeeStatusType::PREVIOUS . ' )
							AND e.id <> ' . ( int ) $intManagerEmployeeId . '
							AND al.level < 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchImmediateApprovalManagerEmployeeIdByEmployeeIdByApproverRoleManagerEmployeeIds( $arrintEmployeeIds, $arrintApproverRoleManagerEmployeeIds, $objDatabase, $boolIsIndianReimbursementRequests = false, $arrintIndianReimbursementDesignationIds = NULL ) {

		if( true == is_null( $arrintEmployeeIds ) || ( false == valArr( $arrintApproverRoleManagerEmployeeIds ) && false == valArr( $arrintIndianReimbursementDesignationIds ) ) ) {
			return NULL;
		}

		$strSelectCondition = 'e.id reporting_manager_id';
		$strJoinCondition	= ' ';
		$strSelect			= ' ';
		$strSelectUnion		= ' ';
		$strCaseCondition	= ' WHEN e1.id IN ( ' . implode( ' ,', $arrintApproverRoleManagerEmployeeIds ) . ' ) THEN am.level+1 ';

		if( true == $boolIsIndianReimbursementRequests ) {

			if( false !== ( $intKey = array_search( CDesignation::ASSOCIATE_DIRECTOR_OF_DEVELOPEMENT, $arrintIndianReimbursementDesignationIds ) ) ) {
				unset( $arrintIndianReimbursementDesignationIds[$intKey] );
			}

			$strSelectCondition = '( CASE
										WHEN e.id IN (' . CEmployee::ID_SANDEEP_GARUD . ', ' . CEmployee::ID_PREETAM_YADAV . ')
										THEN am.hr_representative_employee_id
										ELSE e.id
									END ) AS reporting_manager_id,
									( CASE
										WHEN e.id IN (' . CEmployee::ID_SANDEEP_GARUD . ', ' . CEmployee::ID_PREETAM_YADAV . ')
										THEN am.email_address
										ELSE e.email_address
									END ) AS reporting_manager_email_address,
									( CASE
										WHEN e.id IN (' . CEmployee::ID_SANDEEP_GARUD . ', ' . CEmployee::ID_PREETAM_YADAV . ')
										THEN am.name_first
										ELSE e.name_first
									END ) AS reporting_manager_name_first,
									e.id AS manager_id ';

			$strJoinCondition	= ' JOIN teams AS t ON( t.id = te.team_id ) JOIN employees AS e ON( t.hr_representative_employee_id = e.id )';
			$strSelect			= ', t.hr_representative_employee_id, e.email_address AS email_address, e.name_first';
			$strSelectUnion		= ', am.hr_representative_employee_id, am.email_address, am.name_first';
			$strCaseCondition	= ' WHEN e.designation_id IN ( ' . implode( ' ,', $arrintIndianReimbursementDesignationIds ) . ' ) THEN am.level+1
									WHEN e.id IN (' . CEmployee::ID_SANDEEP_GARUD . ', ' . CEmployee::ID_PREETAM_YADAV . ') THEN am.level+1 ';
		}

		$strSql = ' WITH RECURSIVE
						all_managers( employee_id, manager_employee_id, team_id, depth, level, requester_id ) AS (
							SELECT
								e1.id AS employee_id,
								e1.reporting_manager_id AS manager_employee_id,
								te.team_id,
								1,
								0,
								e1.id requester_id
								 ' . $strSelect . '
							FROM
								team_employees AS te
								JOIN employees e1 ON ( te.employee_id = e1.id )
								' . $strJoinCondition . '
							WHERE
								e1.id IN ( ' . implode( ' ,', $arrintEmployeeIds ) . ' )
								AND te.is_primary_team = 1
							UNION ALL
							SELECT
								e1.id AS employee_id,
								e1.reporting_manager_id AS manager_employee_id,
								te.team_id,
								am.depth + 1,
								CASE
									WHEN am.level > 0 THEN am.level+1
									' . $strCaseCondition . '
									ELSE
										am.level
								END AS level,
								am.requester_id
								' . $strSelectUnion . '
							FROM
								team_employees AS te
								JOIN employees e1 ON ( te.employee_id = e1.id AND te.is_primary_team = 1 )
								JOIN all_managers AS am ON ( e1.id = am.manager_employee_id )
								JOIN employees AS e ON( am.manager_employee_id = e.id )
							WHERE
								e1.id <> e1.reporting_manager_id
						)
						SELECT
							am.requester_id employee_id,
							e1.preferred_name,
							' . $strSelectCondition . '
						FROM
							all_managers AS am
							JOIN employees AS e ON ( am.employee_id = e.id )
							LEFT JOIN teams AS t ON ( am.team_id = t.id )
							JOIN employees AS e1 ON ( am.requester_id = e1.id )
						WHERE
							e.date_terminated IS NULL
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
							AND am.level = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveTeamEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		if( true == is_null( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id,
						e.name_full
					FROM
						employees e
						JOIN team_employees te ON ( te.employee_id = e.id )
					WHERE
						te.team_id IN (
										SELECT
											team_id
										FROM
											team_employees te
										WHERE
											te.is_primary_team = 1
											AND employee_id = ' . ( int ) $intEmployeeId . '
									)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllManagerEmployeeIds( $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT e1.reporting_manager_id AS manager_employee_id
					FROM employees e1
						 JOIN employees e ON ( e.id = e1.reporting_manager_id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByTeamIdByDesignationIds( $intTeamId, $arrintDesignationIds, $objDatabase, $boolIsPrimaryTeam = true ) {
		if( false == valId( $intTeamId ) || false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == $boolIsPrimaryTeam ) ? ' AND te.is_primary_team = 1' : '';

		$strSql = 'SELECT
						te.*
					FROM
						team_employees te
						JOIN employees e ON (e.id = te.employee_id)
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL
						AND e.designation_id  IN ( ' . implode( ' ,', $arrintDesignationIds ) . ' )
						AND te.team_id = ' . ( int ) $intTeamId . $strWhereCondition;

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllTeamEmployeesByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $boolIsPrimaryTeam = false ) {

		if( false == valId( $intManagerEmployeeId ) ) {
			return NULL;
		}

		$strCondition = ( false == $boolIsPrimaryTeam ) ? NULL : 'AND te.is_primary_team = 1';

		$strSql = 'SELECT
						DISTINCT e.id AS employee_id,
						e.preferred_name AS name_full,
						e.reporting_manager_id AS manager_employee_id,
						te.team_id
					FROM
						employees as e
						LEFT JOIN team_employees as te ON ( e.id = te.employee_id )
					WHERE
						e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
						AND ( e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' OR e.employee_status_type_id = ' . CEmployeeStatusType::PREVIOUS . ' )
						' . $strCondition . '
					ORDER BY
						name_full';

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchEmployeeIdsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {

		$strSql = '	SELECT
							employee_id
						FROM
							team_employees
						WHERE
							manager_employee_id = ' . ( int ) $intManagerEmployeeId . ' AND team_employees.is_primary_team = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchManagerByUserId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						e.reporting_manager_id AS manager_employee_id,
						u.id as user_id
					FROM
						employees e
						JOIN users u ON ( e.id = u.employee_id  and u.id = ' . ( int ) $intEmployeeId . ' )';

		$arrobjEmplpoyee = fetchdata( $strSql, $objDatabase );
		return $arrobjEmplpoyee[0]['manager_employee_id'];
	}

	public static function fetchRecommendedTeamEmployeesByEmployeeIds( $arrintEmployeeIds, $arrstrTeamEmployeesFilter, $objDatabase, $boolIsFromStoryPoint = false ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		if( true == valArr( $arrstrTeamEmployeesFilter ) && true == isset( $arrstrTeamEmployeesFilter['order_by_field'] ) ) {

			switch( $arrstrTeamEmployeesFilter['order_by_field'] ) {

				case 'employee_name':
					$strOrderByField = ' order by name_full';
					break;

				case 'team_name':
					$strOrderByField = ' order by team_name';
					break;

				default:
					$strOrderByField = ' order by name_full';
					break;
			}

			if( true == isset( $arrstrTeamEmployeesFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrTeamEmployeesFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}
		}

		$strWhere = '';
		if( true == $boolIsFromStoryPoint ) {
			$strWhere = ' AND e.department_id IN ( ' . implode( ', ', CDepartment::$c_arrintReleaseFilterVisibleDepartmentIds ) . ' ) ';
		}

		$strSql = 'SELECT
						t.id as team_id,
						t.name AS team_name,
						e.name_full,
						d.name AS designation_name,
						e.employee_status_type_id,
						e.reporting_manager_id AS manager_employee_id,
						t.hr_representative_employee_id,
						e.id As employee_id,
						d.country_code
					FROM
						team_employees te 
						JOIN teams t ON ( t.id = te.team_id )
						JOIN employees e ON ( e.id = te.employee_id )
						JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) 
						AND te.is_primary_team = 1 
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . $strWhere . $strOrderByField . ' ' . $strOrderByType;

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchManagerEmployeeIdsByManagerIds( $arrintEmployeeId, $objDatabase ) {
		if( false == valArr( $arrintEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT (e.id),
						t.manager_employee_id
					FROM
						employees e
						JOIN team_employees t ON ( e.id = t.employee_id )
						JOIN team_employees t1 ON ( e.id = t1.manager_employee_id )
					WHERE
						t.manager_employee_id IN ( ' . sqlIntImplode( $arrintEmployeeId ) . ') 
						AND t.is_primary_team = 1
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleTeamEmployeesByDepartmentId( $intDepartmentId, $objDatabase, $strEmployeeIds = NULL, $intEmployeeStatusTypeId = NULL ) {
		if( false == valId( $intDepartmentId ) ) {
			return NULL;
		}

		$strWhere = '';

		if( true == valStr( $strEmployeeIds ) ) {
			$strWhere = ' AND te.employee_id IN( ' . $strEmployeeIds . ' ) ';
		}

		if( true == valId( $intEmployeeStatusTypeId ) ) {
			$strWhere .= ' AND e.employee_status_type_id = ' . ( int ) $intEmployeeStatusTypeId;
		}

		$strSql = 'SELECT
						te.team_id,
						t.name,
						te.employee_id,
						e.name_first,
						e.name_last,
						CASE WHEN e.preferred_name IS NOT NULL THEN e.preferred_name ELSE CONCAT( e.name_first, \' \', e.name_last ) END AS preferred_name
					FROM
						teams AS t
						JOIN team_employees AS te ON ( t.id = te.team_id )
						JOIN employees AS e ON ( te.employee_id = e.id )
					WHERE
						e.department_id = ' . ( int ) $intDepartmentId . '
						AND te.is_primary_team = 1 ' .
						$strWhere . '
					ORDER BY
						t.name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeesBySdmEmployeeIds( $arrintSdmEmployeeIds, $objDatabase ) {

		if( false == valIntArr( $arrintSdmEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						te.employee_id,
						t.id,
						t.name,
						t.sdm_employee_id,
						e.reporting_manager_id AS manager_employee_id,
						e.preferred_name AS name_full,
						e.date_terminated,
						te.is_primary_team,
						u.id AS user_id,
						e.preferred_name
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id )
						JOIN employees e ON ( e.id = te.employee_id )
						JOIN users u ON ( u.employee_id = e.id )
					WHERE
						t.sdm_employee_id IN (' . implode( ',', $arrintSdmEmployeeIds ) . ')
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND te.is_primary_team = 1 ';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchOfficeShiftEmployeesTeamsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $intTeamId = NULL, $boolDeleted = false, $boolCheckDepth = true ) {
		if( false == is_numeric( $intManagerEmployeeId ) ) {
			return false;
		}
		$strWhereCondition = ( true == is_numeric( $intTeamId ) ) ? ' AND al.team_id <> ' . ( int ) $intTeamId : '';
		$strWhereCondition .= ( false == $boolDeleted ) ? ' AND osa.deleted_by IS NULL' : '';
		$strWhereCondition .= ( true == $boolCheckDepth ) ? ' AND al.depth <= 2' : '';

		$strSql = 'WITH RECURSIVE all_employees( team_id, employee_id, manager_employee_id, depth ) AS (
					SELECT
						DISTINCT te.team_id,
						e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id,
						1
					FROM
						team_employees te
						JOIN employees e ON ( te.employee_id = e.id )
					WHERE
						e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
						AND te.is_primary_team = 1
					UNION ALL
					SELECT
						DISTINCT te.team_id,
						e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id,
						al.depth + 1
					FROM
						all_employees al
						JOIN employees e ON ( e.reporting_manager_id = al.employee_id )
						JOIN team_employees te ON ( te.employee_id = e.id )
					WHERE
						te.is_primary_team = 1
						)
					SELECT
						DISTINCT ON ( al.team_id, osa.office_shift_id ) al.*,
						osa.office_shift_id
					FROM
						all_employees al
						JOIN office_shift_associations osa ON ( al.team_id = osa.team_id )
					WHERE 1 = 1' . $strWhereCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchManagersTeamEmployeesByManagerEmployeeIds( $arrintManagerEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintManagerEmployeeIds ) ) {
			return NULL;
		}

		$strSql = '
					WITH RECURSIVE
						all_employees( employee_id, approver_employee_id, manager_employee_id ) AS (
							SELECT
								e.id As employee_id,
								' . CEmployee::ID_DAVID_BATEMEN . ',
								e.reporting_manager_id AS manager_employee_id
							FROM
								employees e
								JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'US\' )
							WHERE
								e.reporting_manager_id = ' . ( int ) CEmployee::ID_DAVID_BATEMEN . '
							UNION ALL
							SELECT
								e.id As employee_id,
								CASE
									WHEN e.reporting_manager_id IN ( ' . implode( ' ,', $arrintManagerEmployeeIds ) . ' ) THEN
										e.reporting_manager_id
									ELSE
										al.approver_employee_id
								END AS approver_employee_id,
								e.reporting_manager_id AS manager_employee_id
							FROM
								employees e,
								all_employees al
							WHERE
								e.reporting_manager_id = al.employee_id
						)
						SELECT
							array_to_string( array_agg(e.id), \',\') AS employee_ids,
							al.approver_employee_id AS manager_employee_id
						FROM
							all_employees al
							JOIN employees e ON (al.employee_id = e.id)
							JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'US\' )
						WHERE
							e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
							AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
						GROUP BY
							al.approver_employee_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentTeamEmployeesByManagerIds( $arrintManagerIds, $objDatabase, $strOrderBy = 'e.name_full' ) {

		$strSql = 'SELECT
						e.name_full,
						e.preferred_name,
						e.reporting_manager_id AS employee_id,
						e.id as id
					FROM
						employees e
					WHERE
						e.reporting_manager_id IN ( ' . implode( ', ', $arrintManagerIds ) . ' )
						AND e.employee_status_type_id =' . ( int ) CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR e.date_terminated > NOW() )
					ORDER BY ' . $strOrderBy;

		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchManagerAddTpmByUserName( $strUsername, $objDatabase ) {
		if( false == valStr( $strUsername ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.email_address
					FROM
						team_employees te
						JOIN employees e1 ON ( te.employee_id = e1.id )
						JOIN teams t ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN users u ON ( u.username = \'' . $strUsername . '\' AND u.employee_id = te.employee_id )
						JOIN employees e ON ( e.id IN ( t.tpm_employee_id, t.add_employee_id, e1.reporting_manager_id ) )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllTeamEmployeesByManagerEmployeeIdByDesignationId( $intManagerEmployeeId, $arrintDesignationIds, $objDatabase ) {

		if( true == is_null( $intManagerEmployeeId ) || false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strSql = ' WITH RECURSIVE
						all_employees( employee_id, manager_employee_id, depth, level ) AS (
							SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id,
								1,
								0
							FROM
								employees AS e
							WHERE
								e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
								AND e.id <> ' . ( int ) $intManagerEmployeeId . '
							UNION ALL
							SELECT
								e1.id AS employee_id,
								e1.reporting_manager_id AS manager_employee_id,
								al.depth + 1,
								CASE
									WHEN e.designation_id IN ( ' . implode( ' ,', $arrintDesignationIds ) . ' ) OR al.level > 0 THEN
										al.level+1
									ELSE
										al.level
								END AS level
							FROM
								employees AS e1
								JOIN all_employees AS al ON ( e1.reporting_manager_id = al.employee_id )
								JOIN employees AS e ON ( e1.reporting_manager_id = e.id )
							WHERE
								e1.id <>' . ( int ) $intManagerEmployeeId . ' )
						SELECT
							e.id
						FROM
							all_employees AS al
							JOIN employees AS e ON ( al.employee_id = e.id )
						WHERE
							e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
							AND e.id <> ' . ( int ) $intManagerEmployeeId . '
							AND al.level < 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeIdsReportingToSuperUserByHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId, $arrintDesignationIds, $objDatabase ) {

		if( true == is_null( $intHrRepresentativeEmployeeId ) || false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		if( false !== ( $intKey = array_search( CDesignation::ASSOCIATE_DIRECTOR_OF_DEVELOPEMENT, $arrintDesignationIds ) ) ) {
			unset( $arrintDesignationIds[$intKey] );
		}

		$strSql = ' WITH RECURSIVE
						all_employees( employee_id, manager_employee_id, depth, level ) AS (
							SELECT
								e.id AS employee_id,
								e.reporting_manager_id AS manager_employee_id,
								1,
								0
							FROM
								team_employees AS te
								JOIN teams AS t ON ( t.id = te.team_id )
								JOIN employees AS e ON ( te.employee_id = e.id )
								JOIN employees AS e1 ON ( e.reporting_manager_id = e1.id )
							WHERE
								( e.reporting_manager_id IN (' . CEmployee::ID_SANDEEP_GARUD . ', ' . CEmployee::ID_PREETAM_YADAV . ') OR e1.designation_id = ' . CDesignation::ASSOCIATE_DIRECTOR_OF_DEVELOPEMENT . ' )
								AND te.is_primary_team = 1
								AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
							UNION ALL
							SELECT
								e1.id AS employee_id,
								e.id AS manager_employee_id,
								al.depth + 1,
								CASE
									WHEN 
										e.designation_id IN ( ' . implode( ' ,', $arrintDesignationIds ) . ' ) OR al.level > 0 THEN al.level+1
									ELSE
										al.level
								END AS level
							FROM
								team_employees AS te
								JOIN employees e1 ON ( te.employee_id = e1.id )
								JOIN employees AS e ON ( e1.reporting_manager_id = e.id )
								JOIN all_employees AS al ON ( e.id = al.employee_id )
								JOIN teams AS t ON ( t.id = te.team_id )
							WHERE
								te.is_primary_team = 1
								AND e1.id <> ' . ( int ) $intHrRepresentativeEmployeeId . '
								AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . ' )
						SELECT
							e.id
						FROM
							all_employees AS al
							JOIN employees AS e ON ( al.employee_id = e.id )
						WHERE
							( e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
							AND e.id <> ' . ( int ) $intHrRepresentativeEmployeeId . '
							AND al.level < 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeIdsByAddEmployeeIds( $arrintAddEmployeeIds, $objDatabase, $strCountryCode=CCountry::CODE_INDIA, $boolSilver ) {

		if( true == $boolSilver ) {
			$strCondition = ' AND t.add_employee_id IN ( ' . implode( ',', $arrintAddEmployeeIds ) . ')';
		} else {
			$strCondition = ' AND t.add_employee_id NOT IN ( ' . implode( ',', $arrintAddEmployeeIds ) . ')';
		}
		$strSql = ' SELECT
						DISTINCT e1.id AS employee_id,
						e1.preferred_name as name
					FROM
						employees AS e
						JOIN team_employees te ON ( e.id = te.employee_id )
						JOIN employees e1 ON ( e1.id = e.id OR e1.id = e.reporting_manager_id )
						JOIN teams t ON ( te.team_id = t.id )
						JOIN employee_addresses ea ON( e1.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						e1.date_terminated IS NULL
						AND te.is_primary_team = 1
						AND e1.id NOT IN ( ' . implode( ',', $arrintAddEmployeeIds ) . ')
						AND e1.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ea.country_code=\'' . $strCountryCode . '\'' . $strCondition . '
					ORDER BY e1.preferred_name asc';

		$arrintEmployeeIds = fetchData( $strSql, $objDatabase );
		return $arrintEmployeeIds;
	}

	public static function fetchTeamEmployeesDetailsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						te.team_id as team_id,
						e.preferred_name,
						e.id
					FROM
						team_employees AS te 
						JOIN employees AS e ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
					WHERE
						te.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
					GROUP BY
						te.team_id,
						e.id
					ORDER BY
						e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecursiveTeamDetailsByEmployeeId( $intManagerEmployeeId, $objDatabase, $boolTeam = false ) {
		if( false == is_numeric( $intManagerEmployeeId ) ) {
			return NULL;
		}

		$strSelect = 'al.*';
		$strWhereClause = 'e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id = ' . ( int ) $intManagerEmployeeId;

		if( true == $boolTeam ) {
			$strSelect = 'al.*';
			$strWhereClause = ' e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;
		}

		$strSql = 'WITH RECURSIVE all_employees( employee_id, preferred_name, team_id, manager_employee_id ) AS (
					SELECT
						DISTINCT e.id AS employee_id,
						e.preferred_name,
						te.team_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						team_employees te 
						JOIN employees e ON ( te.employee_id = e.id and e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
					WHERE
						( ' . $strWhereClause . ' )
						AND te.is_primary_team = 1
						AND e.reporting_manager_id != e.id
					UNION ALL
					SELECT
						DISTINCT e.id AS employee_id,
						e.preferred_name,
						te.team_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						team_employees te
						JOIN employees e ON ( te.employee_id = e.id and e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN all_employees al ON ( e.reporting_manager_id = al.employee_id )
					WHERE
						te.is_primary_team = 1
						AND e.reporting_manager_id != e.id
						)
					SELECT
						DISTINCT ' . $strSelect . '
					FROM
						all_employees al
					ORDER BY
						al.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecursuiveTeamsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $arrintDevQaEmployeeIds = NULL ) {

		if( false == is_numeric( $intManagerEmployeeId ) ) {
			return false;
		}

		if( true == valArr( $arrintDevQaEmployeeIds ) ) {
			$strWhere = ' ( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN ( ' . implode( ',', $arrintDevQaEmployeeIds ) . ' ) )';
		} else {
			$strWhere = ' e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;
		}

		$strSql = 'WITH RECURSIVE all_employees( team_id, employee_id, manager_employee_id) AS (
					SELECT
						DISTINCT te.team_id,
						e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						team_employees te
						JOIN teams AS t ON ( te.team_id = t.id and t.deleted_by IS NULL )
						JOIN employees e ON ( te.employee_id = e.id )
					WHERE
						' . $strWhere . '
						AND te.is_primary_team = 1
					UNION ALL
					SELECT
						DISTINCT te.team_id,
						e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						team_employees te
						JOIN employees e ON ( te.employee_id = e.id )
						JOIN all_employees al ON ( e.reporting_manager_id = al.employee_id )
						JOIN teams as t1 ON ( te.team_id = t1.id and t1.deleted_by IS NULL )
					WHERE
						te.is_primary_team = 1
						)
					SELECT
						DISTINCT  al.team_id,
						al.employee_id
					FROM
						all_employees al';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMyTeamSprintEmployeesBySdmEmployeeIdByAddEmployeeIdByManagerEmployeeId( $boolSdmEmployeeId = true, $boolAddEmployeeId =false, $intManagerEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intManagerEmployeeId ) ) {
			return false;
		}
		if( true == $boolSdmEmployeeId ) {
			$strTableFieldName = 't.sdm_employee_id';
			$strJoinClause = 'JOIN employees e1 ON ( e1.reporting_manager_id = e.id ) JOIN team_employees te ON ( te.employee_id = e1.id AND te.is_primary_team = 1 AND e.employee_status_type_id = 1 )';
		} elseif( true == $boolAddEmployeeId ) {
			$strTableFieldName = 't.add_employee_id';
			$strJoinClause = 'JOIN team_employees te ON (e.id = te.employee_id AND te.is_primary_team = 1 AND e.employee_status_type_id = 1 )
						LEFT JOIN employees e1 ON ( e1.reporting_manager_id = e.id ) LEFT JOIN team_employees te1 ON ( te1.employee_id = e1.id AND te1.is_primary_team = 1 )';
		}

		$strSql = '
					SELECT 
						DISTINCT e.id AS employee_id,
						e.preferred_name AS name_full,
						d.name AS designation_name,
						d.country_code,
						t.name AS team_name,
						t.sdm_employee_id,
						t.add_employee_id,
						e1.reporting_manager_id AS manager_employee_id,
						te.team_id,
						t.hr_representative_employee_id
					FROM 
						employees AS e ' .
						$strJoinClause . '
						JOIN teams t ON ( te.team_id = t.id )
						JOIN designations d ON ( e.designation_id = d.id )
					WHERE 
						e.date_terminated IS NULL 
						AND t.deleted_on IS NULL
						AND e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintSprintDepartmentIds ) . ' ) 
						AND e.id NOT IN ( ' . ( int ) $intManagerEmployeeId . ' ) AND 
						' . $strTableFieldName . ' IN (' . ( int ) $intManagerEmployeeId . ') 
					ORDER BY 
						name_full ASC';

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByTPMIds( $arrintTpmIds, $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintTpmIds ) || false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT e.id AS employee_id,
						e.preferred_name AS name_full,
						d.name AS designation_name,
						d.country_code,
						t.name AS team_name,
						t.sdm_employee_id,
						t.add_employee_id,
						e.reporting_manager_id AS manager_employee_id,
						te.team_id,
						t.hr_representative_employee_id
					FROM
						team_employees AS te 
						JOIN employees AS e ON ( e.id = te.employee_id and e.employee_status_type_id = ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_NEW . ' AND e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintSprintDepartmentIds ) . ') )
						JOIN designations d ON ( e.designation_id = d.id )
						JOIN teams AS t ON ( te.is_primary_team = 1 AND t.id = te.team_id AND ( t.tpm_employee_id IN ( ' . implode( ',', $arrintTpmIds ) . ' ) OR te.employee_id IN (' . implode( ',', $arrintEmployeeIds ) . ') ) )';

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchActiveTeamEmployeesAndManagerDetailsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						e.id AS employee_id,
						e.preferred_name AS name_full,
						u.id AS user_id
					FROM
						employees as e
						JOIN users AS u ON ( u.employee_id = e.id )
						JOIN team_employees as te ON ( e.id = te.employee_id )
					WHERE
						te.team_id IN ( SELECT id FROM teams WHERE manager_employee_id = ' . ( int ) $intManagerEmployeeId . ' )
						AND te.is_primary_team = 1
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL
					UNION
					SELECT
						e.id AS employee_id,
						e.preferred_name AS name_full,
						u.id AS user_id
					FROM
						employees AS e
						JOIN users AS u ON ( u.employee_id = e.id )
					WHERE
						e.id = ' . ( int ) $intManagerEmployeeId . '
					ORDER BY
						name_full';

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByManagerEmployeeIdByDesignationIds( $intManagerEmployeeId, $arrintDesignationIds, $objAdminDatabase ) {
		if( false == valId( $intManagerEmployeeId ) || false == valArr( $arrintDesignationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						e.id AS employee_id,
						concat_ws(\' \', e.name_first, e.name_last ) AS name_full,
						d.id AS designation_id,
						d.name AS designation_name
					FROM
						employees e
						JOIN designations d ON ( e.designation_id = d.id )
					WHERE
						e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
						AND e.designation_id IN ( ' . sqlIntImplode( $arrintDesignationIds ) . ' )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL';

		return self::fetchTeamEmployees( $strSql, $objAdminDatabase );
	}

	public static function fetchTeamChangeCountByEmployeeId( $intEmployeeId, $objAdminDatabase ) {
		if( false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						COUNT( team_id ) AS team_change_count
					FROM
						team_employees
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
					';

		$arrintData = fetchData( $strSql, $objAdminDatabase );
		if( true == valArr( $arrintData ) ) {
			return $arrintData[0]['team_change_count'];
		}

		return NULL;
	}

	public static function fetchTpmAndQamByPdmEmployeeId( $intEmployeeId, $objAdminDatabase ) {

		if( false == valId( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT tpm_employee_id as employee_id,
						e.preferred_name
					FROM
						teams t
						JOIN employees e ON ( e.id = t.tpm_employee_id)
					WHERE
						e.date_terminated IS NULL
						AND t.sdm_employee_id = ' . ( int ) $intEmployeeId . '
						AND t.tpm_employee_id != ' . ( int ) $intEmployeeId . '
					UNION
					SELECT
						DISTINCT qam_employee_id as employee_id,
						e.preferred_name
					FROM
						teams t
						JOIN employees e ON ( e.id = t.qam_employee_id)
					WHERE
						e.date_terminated IS NULL
						AND t.sdm_employee_id = ' . ( int ) $intEmployeeId . '
						AND t.qam_employee_id != ' . ( int ) $intEmployeeId .
						' AND t.deleted_by IS NULL';
		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchTeamHierarchyByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $boolFetchData = false, $arrintDevQaEmployeeIds = NULL ) {

		if( false == valId( $intManagerEmployeeId ) ) {
			return false;
		}

		if( true == valArr( $arrintDevQaEmployeeIds ) ) {
			$strWhere = ' AND ( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN ( ' . implode( ',', $arrintDevQaEmployeeIds ) . ' ) )';
		} else {
			$strWhere = ' AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;
		}

		$strSql = 'WITH RECURSIVE team_hierarchy ( employee_id, manager_employee_id ) AS
					(
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM
							employees e
						WHERE
							e.employee_status_type_id = 1
							' . $strWhere . '
						UNION ALL
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM
							team_hierarchy th
							JOIN employees e ON ( e.reporting_manager_id = th.employee_id AND e.employee_status_type_id = 1 )
						WHERE
							e.id != e.reporting_manager_id )
					SELECT
						 DISTINCT vte.*
					FROM
						team_hierarchy th
						JOIN view_employee_details vte ON ( th.employee_id = vte.employee_id )
						ORDER BY
						employee_id';

		if( true == $boolFetchData ) {
			return fetchData( $strSql, $objDatabase );
		}
		return self::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchTeamsByManagerEmployeeIds( $arrintManagerEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintManagerEmployeeIds ) ) {
			return false;
		}

		$strSql = 'WITH RECURSIVE team_hierarchy ( team_id, employee_id, manager_employee_id ) AS
					(
						SELECT
							team_id,
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM
							team_employees
							JOIN employees e ON ( employee_id = e.id AND e.employee_status_type_id = 1 )
						WHERE
							is_primary_team = 1
							AND e.reporting_manager_id IN ( ' . implode( ',', $arrintManagerEmployeeIds ) . ' ) 
						UNION ALL
						SELECT
							te.team_id,
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id
						FROM
							team_hierarchy th
							JOIN employees e ON ( th.employee_id = e.reporting_manager_id AND e.employee_status_type_id = 1 )
							JOIN team_employees te ON ( te.employee_id = e.id )
						WHERE
							( e.id != e.reporting_manager_id )
							AND te.is_primary_team = 1)
					SELECT
						 DISTINCT th.team_id
					FROM
						team_hierarchy AS th';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByManagerEmployeeId( $intManagerEmployeeId,$objAdminDatabase ) {
		if( false == valId( $intManagerEmployeeId ) ) return NULL;

		$strSql = 'SELECT
						e.id,
						e.preferred_name
					FROM 
						employees AS e
					WHERE 
						e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchSimpleTeamEmployeesByManagerEmployeeId( $intManagerEmployeeId,$objAdminDatabase ) {
		if( false == valId( $intManagerEmployeeId ) ) return NULL;

		$strSql = 'SELECT
						e.id,
						e.preferred_name,
						e.designation_id,
						t.name AS team_name,
						t.preferred_name AS team_preferred_name,
						t.logo_file_path AS team_logo_file_path
					FROM 
						team_employees AS te
						JOIN employees AS e ON ( te.employee_id = e.id AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' )
						JOIN teams t ON( t.id = te.team_id )
					WHERE 
						te.is_primary_team = 1
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND e.date_terminated IS NULL';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchTeamEmployeesByTeamIdByManagerEmployeeId( $intTeamId, $intManagerEmployeeId, $objDatabase ) {

		if( false == valId( $intTeamId ) || false == valId( $intManagerEmployeeId ) ) {
			return NULL;
		}

		$strSql = '	SELECT 
						te.*
					FROM 
						employees as e
						LEFT JOIN team_employees as te ON ( e.id = te.employee_id )
					WHERE 
						e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' AND
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' AND
						te.is_primary_team = 1 AND
						te.team_id = ' . ( int ) $intTeamId . '
					ORDER BY name_full';

		return parent::fetchTeamEmployees( $strSql, $objDatabase );
	}

	public static function fetchAllTeamsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {

		if( false == valId( $intManagerEmployeeId ) ) {
			return false;
		}

		$strSql = 'WITH RECURSIVE all_employees( team_id, employee_id, manager_employee_id) AS (
					SELECT
						DISTINCT te.team_id,
						e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						team_employees te
						JOIN employees e ON ( te.employee_id = e.id )
						JOIN teams AS t ON ( te.team_id = t.id and t.deleted_by IS NULL )
					WHERE
						( e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . ' OR e.id =  ' . ( int ) $intManagerEmployeeId . ' )
						AND te.is_primary_team = 1
					UNION ALL
					SELECT
						DISTINCT te.team_id,
						e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						team_employees te
						JOIN employees e ON ( te.employee_id = e.id )
						JOIN all_employees al ON ( e.reporting_manager_id = al.employee_id )
						JOIN teams as t1 ON ( te.team_id = t1.id and t1.deleted_by IS NULL )
					WHERE
						te.is_primary_team = 1
						)
					SELECT
						DISTINCT ON( al.team_id )
						al.team_id,
						al.manager_employee_id
					FROM
						all_employees al';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPrimaryTeamEmployeesByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return false;
		}

		$strSql = 'SELECT 
						e.reporting_manager_id AS manager_employee_id,
						json_agg( json_build_object( \'team_id\', te.team_id, \'employee_id\', te.employee_id, \'employee_name\', e.preferred_name, \'country_code\', ea.country_code, \'designation_name\', d.name, \'team_name\', t.name, \'date_terminated\', e.date_terminated, \'resigned_on\', e.resigned_on, \'expected_termination_date\', e.expected_termination_date ) ORDER BY ( e.preferred_name ) )::jsonb as team_members
					FROM 
						team_employees te 
						JOIN employees e ON ( e.id = te.employee_id and e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN users u ON (e.id = u.employee_id)
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN teams t ON ( t.id = te.team_id )
						LEFT JOIN designations d ON ( e.designation_id = d.id )
					WHERE 
						te.is_primary_team = 1 
						AND e.date_terminated IS NULL 
						AND e.reporting_manager_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND te.team_id <> ' . CTeam::NEW_JOINEE_US . '
					GROUP BY
						e.reporting_manager_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllManagersByManagerIdByCountryCode( $intManagerEmployeeId, $strCountryCode, $objDatabase, $boolPublicKeyGenerated = false ) {
		if( false == $intManagerEmployeeId ) {
			return NULL;
		}

		$strWhereCondition = ( true == $boolPublicKeyGenerated ) ? ' AND e.employee_status_type_id = 1 AND e.public_key IS NOT NULL ' : NULL;

		$strSql = 'WITH RECURSIVE all_managers AS (
					SELECT
						e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
						e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
					UNION ALL
					SELECT
						e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						all_managers r
						JOIN employees e ON ( e.reporting_manager_id = r.employee_id )
					WHERE
						e.id != e.reporting_manager_id )
					SELECT
						DISTINCT
						sub.team_size,
						sub.manager_employee_id as employee_id,
						e.preferred_name,
						e.department_id
					FROM
						(
							SELECT
								count ( am.employee_id ) AS team_size,
								am.manager_employee_id
							FROM
								all_managers am
							GROUP BY
								am.manager_employee_id
						) sub
						JOIN teams t ON ( t.manager_employee_id = sub.manager_employee_id )
						JOIN employees e ON ( e.id = sub.manager_employee_id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						LEFT JOIN employee_addresses ea ON ( ea.employee_id = sub.manager_employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						ea.country_code = \'' . $strCountryCode . '\'
						' . $strWhereCondition . '
						AND t.deleted_by IS NULL
					ORDER BY
						e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTopManagerHierarchyByEmployeeIds( $arrintEmployeeIds, $objDatabase, $boolTestCasesAccess = false ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return false;
		}

		if( $boolTestCasesAccess ) {
			$strSelect = ', e.*,
						u.id as user_id';
			$strWhere = ' AND ( e.department_id = ' . CDepartment::QA . ' OR ur.id IS NOT NULL ) ';
			$strJoin = 'JOIN users u ON( u.employee_id = e.id )
						LEFT JOIN user_roles ur ON( ur.user_id = u.id AND ur.deleted_on IS NULL AND ur.role_id IN ( ' . CRole::TEST_CASE_TESTER . ', ' . CRole::TEST_CASE_LEADER . ' ) )';
		}

		$strSql = 'WITH RECURSIVE all_managers(employee_id, manager_employee_id, team_id, selected_employee_id, depth) AS
					(	SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id,
							te.team_id,
							e.id as selected_employee_id,
							1
						FROM
							team_employees te
							JOIN employees e ON ( te.employee_id = e.id )
						WHERE
							e.id IN ( ' . sqlIntImplode( $arrintEmployeeIds ) . ' )
							AND te.is_primary_team = 1
						UNION ALL
						SELECT
							e.id AS employee_id,
							e.reporting_manager_id AS manager_employee_id,
							te.team_id,
							am.selected_employee_id,
							am.depth + 1
						FROM
							all_managers am
							JOIN employees e ON ( e.id = am.manager_employee_id )
							JOIN team_employees te ON ( te.employee_id = e.id )
						WHERE
							e.id <> e.reporting_manager_id
							AND te.is_primary_team = 1
					)
					SELECT
						e.preferred_name AS name_full,
						all_managers.*,
						t.name AS team_name
						' . $strSelect . '
					FROM
						all_managers
						JOIN employees e ON (all_managers.employee_id = e.id)
						JOIN teams t ON (all_managers.team_id = t.id)
						' . $strJoin . '
					WHERE
						e.date_terminated IS NULL AND
						e.employee_status_type_id = 1
						' . $strWhere . '
					ORDER BY
						all_managers.depth DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHrAndReportingManagerDetailsByEmployeeId( $intEmployeeId, $objDatabase ) {
		if( false == valId( $intEmployeeId ) ) return;

		$strSql = 'SELECT
						e1.id as employee_id,
                        e1.reporting_manager_id as reporting_manager_employee_id,
                        t.hr_representative_employee_id as hr_manager_employee_id,
                        e1.name_full as employee_name,
                        e2.name_full as reporting_manager_name,
                        e3.name_full as hr_representative_name
					FROM
						team_employees te
						JOIN teams t ON ( te.team_id = t.id )
                        JOIN employees e1 ON ( te.employee_id = e1.id )
                        JOIN employees e2 ON ( e1.reporting_manager_id = e2.id )
                        JOIN employees e3 ON ( t.hr_representative_employee_id = e3.id )
					WHERE
						e1.id = ' . ( int ) $intEmployeeId . '
						AND	te.is_primary_team = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesDetailsByTeamIds( $arrintTeamIds, $intCurrentUserId, $objDatabase, $arrintLCEmployeeIds = NULL ) {
		if( false == valArr( $arrintTeamIds ) || false == valId( $intCurrentUserId ) ) {
			return NULL;
		}

		$strWhere = '';
		if( true == valArr( $arrintLCEmployeeIds ) ) {
			$strWhere = ' AND u.id NOT IN ( ' . implode( ',', $arrintLCEmployeeIds ) . ' ) ';
		}

		$strSql = ' SELECT
					te.employee_id,
					e.preferred_name AS employee_name,
					u.id as user_id
				FROM
					team_employees te
				JOIN
					employees e ON (e.id = te.employee_id AND te.is_primary_team = 1 )
				LEFT JOIN 
					users u ON( u.employee_id = e.id )
				WHERE
					( te.team_id IN ( ' . implode( ',', $arrintTeamIds ) . ' ) OR u.id = ' . ( int ) $intCurrentUserId . ' )
					AND e.employee_status_type_id = 1 ' . $strWhere . '
				ORDER BY e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
