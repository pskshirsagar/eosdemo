<?php

class CTestQuestion extends CBaseTestQuestion {

	// Question Languages
	const PHP 			= 'PHP';
	const POSTGRESQL 	= 'POSTGRESQL';
	const JAVASCRIPT 	= 'JAVASCRIPT';
	const CPP			= 'CPP';
	const MYSQL			= 'MYSQL';

	protected $m_arrobjTestAnswerOptions;
	protected $m_intTestSubmissionCount;
	protected $m_arrobjTestSubmissionAnswers;
	protected $m_intTestId;
	protected $m_intMaximumMinutes;
	protected $m_intMaximumSeconds;
	protected $m_intMinutesFromMaximumTime;
	protected $m_intSecondsFromMaximumTime;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjTestAnswerOptions 		= array();
		$this->m_arrobjTestSubmissionAnswers	= array();

		$this->m_intIsPublic					= 0;

		return;
	}

	public static $c_arrstrLanguages = [
		self::PHP => self::PHP,
		self::CPP => self::CPP,
		self::POSTGRESQL => self::POSTGRESQL,
		self::JAVASCRIPT => self::JAVASCRIPT,
		self::MYSQL => self::MYSQL
	];

	/**
	 * Get Functions
	 *
	 */

	public function getTestSubmissionCount() {
		return $this->m_intTestSubmissionCount;
	}

	public function getTestAnswerOptions() {
		return $this->m_arrobjTestAnswerOptions;
	}

	public function getTestSubmissionAnswers() {
		return $this->m_arrobjTestSubmissionAnswers;
	}

	public function getTestId() {
		return $this->m_intTestId;
	}

	public function getMaximumMinutes() {
		return $this->m_intMaximumMinutes;
	}

	public function getMaximumSeconds() {
		return $this->m_intMaximumSeconds;
	}

	public function getMinutesFromMaximumTime() {
		return $this->m_intMinutesFromMaximumTime;
	}

	public function getSecondsFromMaximumTime() {
		return $this->m_intSecondsFromMaximumTime;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['test_id'] ) )						$this->m_intTestId = trim( $arrmixValues['test_id'] );
		if( true == isset( $arrmixValues['maximum_minutes'] ) )		$this->setMaximumMinutes( $arrmixValues['maximum_minutes'] );
		if( true == isset( $arrmixValues['maximum_seconds'] ) )		$this->setMaximumSeconds( $arrmixValues['maximum_seconds'] );
		if( isset( $arrmixValues['maximum_time'] ) )				$this->setMinutesFromMaximumTime( $this->m_intMaximumTime );
		if( isset( $arrmixValues['maximum_time'] ) )				$this->setSecondsFromMaximumTime( $this->m_intMaximumTime );
		return;
	}

	public function setTestSubmissionCount( $intTestSubmissionCount ) {
		$this->m_intTestSubmissionCount = $intTestSubmissionCount;
	}

	public function setTestId( $intTestId ) {
		$this->m_intTestId = CStrings::strToIntDef( $intTestId, NULL, false );
	}

	public function setMaximumMinutes( $intMaximumMinutes ) {
		$this->m_intMaximumMinutes = $intMaximumMinutes;
	}

	public function setMaximumSeconds( $intMaximumSeconds ) {
		$this->m_intMaximumSeconds = $intMaximumSeconds;
	}

	public function setMinutesFromMaximumTime( $intMaximumTime ) {
		$this->m_intMinutesFromMaximumTime = ( int ) ( $intMaximumTime / 60 );
	}

	public function setSecondsFromMaximumTime( $intMaximumTime ) {
		$this->m_intSecondsFromMaximumTime = ( int ) ( $intMaximumTime % 60 );
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchTestAnswerOptions( $objDatabase ) {
		return CTestAnswerOptions::fetchTestAnswerOptionsByTestQuestionIdByTestId( $this->getId(), $this->getTestId(), $objDatabase );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valTestQuestionTypeId() {
		 $boolIsValid = true;

		 if( true == is_null( $this->getTestQuestionTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_question_type_id', 'Type is required. ' ) );
		 }

		 return $boolIsValid;
	}

	public function valTestQuestionGroupId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTestQuestionGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_question_group_id', 'Group is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valTestLevelTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTestLevelTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_level_type_id', 'Level is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valQuestion( $objDatabase ) {

		$boolIsValid = true;

		if( 0 >= strlen( $this->getQuestion() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Question is required. ' ) );
		}

		// Skipping below validation as the question can be duplicate for the coding type.
		// Also its removing the HTML tags from question, which is not required for coding type question
		if( CTestQuestionType::CODING_TEST == $this->getTestQuestionTypeId() ) {
			return $boolIsValid;
		}

		if( true == $boolIsValid && true == valObj( $objDatabase, 'CDatabase' ) ) {

			if( true == is_numeric( $this->m_intId ) ) {
				$strWhere = ' WHERE question = \'' . trim( addslashes( $this->getQuestion() ) ) . '\' AND id <> ' . $this->m_intId . ' AND deleted_on IS NULL';
			} else {
				$strWhere = ' WHERE question = \'' . trim( addslashes( $this->getQuestion() ) ) . '\' AND deleted_on IS NULL';
			}

			// checking only within PHP tests for duplication questions when it belongs to PHP test groups.
			if( false == is_null( getArrayElementByKey( $this->getTestQuestionGroupId(), CTestQuestionGroup::$c_arrstrPhpTestGroups ) ) ) {
				$strWhere .= ' AND test_question_group_id IN ( ' . implode( ',', array_keys( CTestQuestionGroup::$c_arrstrPhpTestGroups ) ) . ' )';
			}

			$intTestQuestionCount = CTestQuestions::fetchTestQuestionCount( $strWhere, $objDatabase );

			if( 0 < $intTestQuestionCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Question is already in use. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valAnswer() {

		$boolIsValid = true;

		if( CTestQuestionType::MULTIPLE_CHOICE == $this->getTestQuestionTypeId() ) {
			$arrstrTestAnswerOptions = array();

			if( true == valArr( $this->m_arrobjTestAnswerOptions ) ) {
				foreach( $this->m_arrobjTestAnswerOptions as $objTestAnswerOption ) {
					$arrstrTestAnswerOptions[] = trim( $objTestAnswerOption->getAnswerOption() );
					if( true == is_null( $objTestAnswerOption->getAnswerOption() ) ) {
						$boolIsValid = false;
						break;
					}
				}
			} else {
				$boolIsValid = false;
			}
			if( false == $boolIsValid ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer_option', 'All answer options are required. ' ) );
				return $boolIsValid;
			}

			if( true == $boolIsValid && false == ( array_unique( $arrstrTestAnswerOptions ) == $arrstrTestAnswerOptions ) ) {
			foreach( $this->m_arrobjTestAnswerOptions as $objTestAnswerOption ) {
				$boolIsValid = false;
				if( 1 == $objTestAnswerOption->getIsCorrect() ) {
					$boolIsValid = true;
					break;
				}
			}

			if ( false == $boolIsValid )
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_correct', 'Select the correct answer. ' ) );
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer_option', 'All answer options should be different. ' ) );
				return $boolIsValid;
			}

			foreach( $this->m_arrobjTestAnswerOptions as $objTestAnswerOption ) {
				$boolIsValid = false;
				if( 1 == $objTestAnswerOption->getIsCorrect() ) {
					$boolIsValid = true;
					break;
				}
			}

			if ( false == $boolIsValid )
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_correct', 'Select the correct answer. ' ) );

		} elseif( true == is_null( $this->getAnswer() ) && CTestQuestionType::MULTIPLE_CHOICE != $this->getTestQuestionTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer', 'Answer is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valAnswerOptions() {

		$boolIsValid = true;
		$intAnswerCount = 0;

		if( ( CTestQuestionType::MULTIPLE_CHOICE == $this->getTestQuestionTypeId() || CTestQuestionType::MULTISELECT_ANSWERS == $this->getTestQuestionTypeId() ) && true == valArr( $this->m_arrobjTestAnswerOptions ) ) {
			foreach( $this->m_arrobjTestAnswerOptions as $objTestAnswer ) {
				if( false == valStr( $objTestAnswer->getAnswerOption() ) && $intAnswerCount < 2 ) {
					$boolIsValid = false;
					break;
				}

				$intAnswerCount++;
			}
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer_option', 'Select at least two options. ' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valMaximumPoints() {
		$boolIsValid = true;

		if( true == is_null( $this->getMaximumPoints() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maximum_points', 'Maximum points are required. ' ) );
			return $boolIsValid;
		}

		if( NULL != $this->getMaximumPoints() && false == is_numeric( $this->getMaximumPoints() ) || false !== \Psi\CStringService::singleton()->strpos( $this->getMaximumPoints(), '.' ) || 1 > $this->getMaximumPoints() || 100 < $this->getMaximumPoints() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maximum_points', 'Maximum points should be Integer only ( in between 1 to 100 ). ' ) );
		}

		return $boolIsValid;
	}

	public function valAdditionalFields() {
		$boolIsValid = true;

		$arrstrAdditionalFields = json_decode( $this->getAdditionalFields(), true );

		if( true == valArr( $arrstrAdditionalFields ) ) {

			if( false == valStr( $arrstrAdditionalFields['language'] ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'language', 'language is required.' ) );
			}

			if( false == valStr( $arrstrAdditionalFields['function_definition'] ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'function_definition', 'Function definition is required.' ) );
			}

			if( false == isset( $arrstrAdditionalFields['sample_input'] ) || false == valStr( $arrstrAdditionalFields['sample_input'] ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sample_input', 'Sample Input is required.' ) );
			}

			if( false == valStr( $arrstrAdditionalFields['expected_output'] ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expected_output', 'Expected output is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

			switch( $strAction ) {
				case VALIDATE_INSERT:
					$boolIsValid &= $this->valTestLevelTypeId();
					$boolIsValid &= $this->valTestQuestionGroupId();
					$boolIsValid &= $this->valMaximumPoints();
					if( CTestQuestionType::CODING_TEST == $this->getTestQuestionTypeId() ) {
						$boolIsValid &= $this->valAdditionalFields();
					} else {
						$boolIsValid &= $this->valAnswer();
					}
					$boolIsValid &= $this->valQuestion( $objDatabase );
					$boolIsValid &= $this->valTestQuestionTypeId();
					break;

				case VALIDATE_UPDATE:
					$boolIsValid &= $this->valQuestion( $objDatabase );
					$boolIsValid &= $this->valMaximumPoints();
					if( CTestQuestionType::CODING_TEST == $this->getTestQuestionTypeId() ) {
						$boolIsValid &= $this->valAdditionalFields();
					} else {
						$boolIsValid &= $this->valAnswer();
					}
					$boolIsValid &= $this->valTestQuestionTypeId();
					break;

				default:
					$boolIsValid = false;
					break;
			}

			return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function addTestAnswerOption( $objTestAnswerOption ) {
		if( false == is_null( $objTestAnswerOption->getId() ) ) {
			$this->m_arrobjTestAnswerOptions[$objTestAnswerOption->getId()]	= $objTestAnswerOption;
		} else {
			$this->m_arrobjTestAnswerOptions[]	= $objTestAnswerOption;
		}
	}

	public function addTestSubmissionAnswer( $objTestSubmissionAnswer ) {
		$this->m_arrobjTestSubmissionAnswers[$objTestSubmissionAnswer->getTestQuestionId()]	= $objTestSubmissionAnswer;
	}

	public function delete( $intUserId, $objAdminDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( 'NOW()' );

		if( $this->update( $intUserId, $objAdminDatabase, $boolReturnSqlOnly ) ) return true;
	}

	public function calculateMaximumTime() {
		$intMaximumTime = ( false == is_null( $this->m_intMaximumMinutes ) && is_numeric( $this->m_intMaximumMinutes ) ) ? $this->m_intMaximumMinutes * 60 : 0;
		$intMaximumTime += ( false == is_null( $this->m_intMaximumSeconds ) && is_numeric( $this->m_intMaximumSeconds ) ) ? $this->m_intMaximumSeconds : 0;

		if( 0 === $intMaximumTime ) {
			return NULL;
		}

		return $intMaximumTime;
	}

}
?>