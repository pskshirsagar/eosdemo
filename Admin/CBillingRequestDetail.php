<?php

class CBillingRequestDetail extends CBaseBillingRequestDetail {

	public function valBillingRequestId() {
		$boolIsValid = true;
		if( true == is_null( $this->getBillingRequestId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_request_id', 'Billing Request id is required' ) );
		}
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required' ) );
		}
		return $boolIsValid;
	}

	public function valProductId() {
		$boolIsValid = true;
		if( true == is_null( $this->getProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'product_id', 'Product is required' ) );
		}
		return $boolIsValid;
	}

	public function valCurrentBillingDate() {
		$boolIsValid = true;
		if( true == is_null( $this->getCurrentBillingDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_billing_date', 'Valid current billing date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRequestedBillingDate( $intBillingRequestTypeId ) {

		if( true == is_null( $this->getRequestedBillingDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requested_billing_date', 'Requested Billing Date is required.' ) );
			return false;
		}

		$intCurrentBillingDate = strtotime( $this->getCurrentBillingDate() );

		if( true == ( strtotime( $this->getRequestedBillingDate() ) <= strtotime( 'now' ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requested_billing_date', 'Requested Billing Date should be greater than current date.' ) );
			return false;
		}

		if( CBillingRequestType::SUSPENDED_BILLING == $intBillingRequestTypeId ) {
			$intCurrentBillingDate = strtotime( '+1 month', strtotime( 'now' ) );
		}

		if( CBillingRequestType::SUSPENDED_BILLING == $intBillingRequestTypeId && ( true == ( strtotime( $this->getRequestedBillingDate() ) <= strtotime( '+1 month', strtotime( $this->getCurrentBillingDate() ) ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requested_billing_date', 'Requested Billing Date should be greater than ' . date( 'M Y', strtotime( '+1 month', strtotime( $this->getCurrentBillingDate() ) ) ) ) );

			return false;
		}

		if( false == is_null( $this->getRequestedBillingDate() ) && ( false == is_null( $this->getCurrentBillingDate() ) && ( true == ( strtotime( $this->getRequestedBillingDate() ) <= $intCurrentBillingDate ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requested_billing_date', 'Requested Billing Date should be greater than ' . ( ( CBillingRequestType::DELAYED_BILLING == $intBillingRequestTypeId ) ? 'Current Billing Date.' : date( 'M Y', $intCurrentBillingDate ) ) ) );

			return false;
		}

		return true;
	}

	public function validate( $strAction, $intBillingRequestTypeId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBillingRequestId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valProductId();
				$boolIsValid &= $this->valRequestedBillingDate( $intBillingRequestTypeId );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>