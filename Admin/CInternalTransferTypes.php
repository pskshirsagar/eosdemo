<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInternalTransferTypes
 * Do not add any new functions to this class.
 */

class CInternalTransferTypes extends CBaseInternalTransferTypes {

	public static function fetchInternalTransferTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CInternalTransferType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchInternalTransferType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CInternalTransferType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllInternalTransferTypes( $objDatabase, $strOrderBy = NULL ) {
	    $strSql = 'SELECT * FROM internal_transfer_types';

	    if( false == is_null( $strOrderBy ) ) {
	        $strSql .= ' ORDER BY ' . addslashes( $strOrderBy );
	    }

	    return self::fetchInternalTransferTypes( $strSql, $objDatabase );
	}

}
?>