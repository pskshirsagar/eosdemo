<?php

class CContractStatusType extends CBaseContractStatusType {

	const UNUSED				= 1;
	// @todo: Need to remove ON_HOLD constant once we remove its usages from EntrataWebsite directory
	const ON_HOLD				= 2;
	const CONTACTED				= 3;
    const QUALIFIED				= 4;
    const DISCOVERY             = 5;
    const DEMOED                = 6;
    const PROPOSAL_SENT			= 7;
    const CONTRACT_NEGOTIATION	= 8;
    const CONTRACT_SENT		    = 9;
    const CONTRACT_APPROVED		= 10;
    const CONTRACT_TERMINATED	= 11;
    const CANCELLED				= 12;
    const LOST					= 12;

	protected $m_boolIsSelected;

	public static $c_arrintCompletedContractStatusTypeIds				= [ self::CONTRACT_APPROVED, self::CONTRACT_TERMINATED ];
	public static $c_arrintCompletedWithOnHoldContractStatusTypeIds		= [ self::CONTRACT_APPROVED, self::CONTRACT_TERMINATED, self::ON_HOLD ];
	public static $c_arrintForecastedContractStatusTypeIds				= [ self::QUALIFIED, self::DEMOED, self::DISCOVERY, self::CONTRACT_NEGOTIATION, self::PROPOSAL_SENT, self::CONTRACT_SENT ];
	public static $c_arrintCommitableContractStatusTypeIds				= [ self::QUALIFIED, self::DEMOED, self::DISCOVERY, self::CONTRACT_NEGOTIATION, self::PROPOSAL_SENT, self::CONTRACT_SENT ];
	public static $c_arrintBelowContractApprovedContractStatusTypeIds	= [ self::CONTRACT_SENT, self::DEMOED, self::DISCOVERY, self::CONTRACT_NEGOTIATION, self::PROPOSAL_SENT, self::QUALIFIED, self::CONTACTED, self::ON_HOLD, self::UNUSED ];
	public static $c_arrintUptoContractApprovedContractStatusTypeIds	= [ self::CONTRACT_SENT, self::DEMOED, self::DISCOVERY, self::CONTRACT_NEGOTIATION, self::PROPOSAL_SENT, self::QUALIFIED, self::CONTACTED, self::ON_HOLD, self::UNUSED, self::CONTRACT_APPROVED ];
	public static $c_arrintIsOpportunityContractStatusTypeIds	        = [ self::CONTRACT_SENT, self::DEMOED, self::DISCOVERY, self::CONTRACT_NEGOTIATION, self::PROPOSAL_SENT, self::QUALIFIED, self::CONTACTED, self::ON_HOLD, self::UNUSED ];
	// Thsis status is also used to fetch open opportunites.
	public static $c_arrintActiveContractStatusTypeIds 				    = [ self::UNUSED, self::CONTACTED, self::QUALIFIED, self::DEMOED, self::DISCOVERY, self::CONTRACT_NEGOTIATION, self::PROPOSAL_SENT, self::CONTRACT_SENT ];
	// Thsis status is also used to fetch inactive opportunites.
	public static $c_arrintInactiveContractStatusTypeIds 				= [ self::CONTRACT_APPROVED, self::CONTRACT_TERMINATED, self::LOST, self::ON_HOLD ];
	public static $c_arrintDeactivatedContractStatusTypeIds 			= [ self::CONTRACT_TERMINATED, self::LOST ];
	// This status is used to get inactive opportunities
	public static $c_arrintInactiveOpportunityStatusTypeIds 			= [ self::CONTRACT_TERMINATED, self::LOST, self::ON_HOLD ];
	public static $c_arrintInactiveStatusTypeIds						= [ self::ON_HOLD, self::LOST ];
	public static $c_arrintHelpdeDeskSupportContractStatusTypeIds       = [ self::DEMOED, self::PROPOSAL_SENT, self::CONTRACT_SENT, self::CONTRACT_APPROVED ];

	public static $c_arrintContractDraftsContractStatusTypeIds			= [ self::UNUSED, self::CONTACTED, self::QUALIFIED, self::DEMOED, self::DISCOVERY, self::CONTRACT_NEGOTIATION, self::PROPOSAL_SENT, self::CONTRACT_SENT ];

	public static $c_arrintEntrataContractStatusTypeIds = [
		self::UNUSED,
		self::CONTACTED,
		self::QUALIFIED,
		self::DEMOED,
		self::DISCOVERY,
		self::CONTRACT_NEGOTIATION,
		self::PROPOSAL_SENT,
		self::CONTRACT_SENT
	];

	public static $c_arrintNonReverseContractStatusTypeIds = [
		self::LOST
	];

	public static $c_arrintContractApprovedNonReverseContractStatusTypeIds = [
		self::UNUSED,
		self::ON_HOLD,
		self::CONTACTED,
		self::QUALIFIED,
		self::DISCOVERY,
		self::DEMOED,
		self::PROPOSAL_SENT,
		self::CONTRACT_NEGOTIATION,
		self::CONTRACT_SENT
	];

	public static $c_arrintSalesOpportunityRedesignContractStatusTypeIds = [
		self::ON_HOLD,
		self::CONTACTED,
		self::QUALIFIED,
		self::DEMOED,
		self::DISCOVERY,
		self::CONTRACT_NEGOTIATION,
		self::PROPOSAL_SENT,
		self::CONTRACT_SENT,
		self::CONTRACT_APPROVED,
		self::LOST
	];

	public static $c_arrintExpectedCloseDateRequiredContractStatusTypeIds = [
		self::QUALIFIED,
		self::PROPOSAL_SENT,
		self::CONTRACT_SENT,
		self::CONTRACT_TERMINATED,
		self::DISCOVERY,
		self::DEMOED,
		self::CONTRACT_NEGOTIATION
	];

	public static $c_arrintProposedOpportunityContractStatusTypeIds = [
		self::PROPOSAL_SENT,
		self::CONTRACT_SENT
	];

	public static $c_arrintSuccessRevieweContractStatusTypeIds = [
		self::UNUSED,
		self::CONTACTED
	];

	public static $c_arrintDisableDatePickerContractStatusTypeIds = [
		self::CONTRACT_APPROVED,
		self::LOST
	];

	public static $c_arrintSalesEngineerContractStatusTypeIds = [
		self::CONTRACT_APPROVED,
		self::CONTRACT_SENT
	];

	public static $c_arrintCompletedWithLostContractStatusTypeIds = [
		self::CONTRACT_APPROVED,
		self::CONTRACT_TERMINATED,
		self::LOST
	];

	public static $c_arrintPipelineContractStatusTypeIds = [
		self::QUALIFIED,
		self::PROPOSAL_SENT,
		self::CONTRACT_SENT
	];

	public function __construct() {
		parent::__construct();

		$this->m_boolIsSelected = false;
	}

	public function getConciseFormattedName() {
		return implode( '<br/>', explode( ' ', trim( $this->getName() ) ) );
	}

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>