<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeSecurityPolicies
 * Do not add any new functions to this class.
 */

class CEmployeeSecurityPolicies extends CBaseEmployeeSecurityPolicies {

	public static function fetchEmployeeSecurityPolicies( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmployeeSecurityPolicy', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEmployeeSecurityPolicy( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmployeeSecurityPolicy', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEmployeeSecurityPoliciesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return parent::fetchEmployeeSecurityPolicy( sprintf( 'SELECT * FROM employee_security_policies e WHERE e.employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>