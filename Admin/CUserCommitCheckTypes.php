<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserCommitCheckTypes
 * Do not add any new functions to this class.
 */

class CUserCommitCheckTypes extends CBaseUserCommitCheckTypes {

	public static function fetchUserCommitCheckTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CUserCommitCheckType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchUserCommitCheckType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CUserCommitCheckType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>