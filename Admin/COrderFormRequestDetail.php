<?php

class COrderFormRequestDetail extends CBaseOrderFormRequestDetail {
	use Psi\Libraries\Container\TContainerized;
	use Psi\Libraries\EosFoundation\TEosStoredObject;

	protected $m_strPropertyName;
	protected $m_strFilePath;
	protected $m_strTempFileName;
	protected $m_strUploadFileName;
	protected $m_strTitle;
	protected $m_intOrderFormTypeId;
	protected $m_intPsLeadId;
	protected $m_objObjectStorageGatewayResponse;

	const STORAGE_DOCUMENTS_PS_LEADS_PATH = 'documents/ps_leads/';

	/**
	 * get functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getOrderFormTypeId() {
		return $this->m_intOrderFormTypeId;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getObjectStorageGatewayResponse() {
		return $this->m_objObjectStorageGatewayResponse;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function getUploadFileName() {
		return $this->m_strUploadFileName;
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	/**
	 * set functions
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setClientName( $strClientName ) {
		$this->m_strCompanyName = $strClientName;
	}

	public function setOrderFormTypeId( $intOrderFormTypeId ) {
		$this->m_intOrderFormTypeId = $intOrderFormTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['company_name'] ) ) $this->setClientName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['order_form_type_id'] ) ) $this->setOrderFormTypeId( $arrmixValues['order_form_type_id'] );
	}

	public function setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse ) {
		$this->m_objObjectStorageGatewayResponse = $objObjectStorageGatewayResponse;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	public function setUploadFileName( $strUploadFileName ) {
		$this->m_strUploadFileName = $strUploadFileName;
	}

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->m_intPsLeadId = $intPsLeadId;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function valOrderFormRequestId() {
		$boolIsValid = true;

		if( false == valId( $this->getOrderFormRequestId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Order form request id is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Client Id is not valid. ' ) );
		}
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == valId( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Property Name is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valPhoneNumber( $strPhoneNumber, $strName = NULL ) {
		$boolIsValid = true;
		if( false == valStr( $strPhoneNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' ' . $strName . ' Phone Number is required. ' ) );
			return $boolIsValid;
		}

		if( 10 != \Psi\CStringService::singleton()->strlen( str_replace( [ '(', ')', '-', ' ' ], '', trim( $strPhoneNumber ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' ' . $strName . ' Phone Number must be 10 digits long. ' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valReasonForChangeOption() {
		$boolIsValid = true;
		if( false == valId( $this->getOrderFormReferenceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select reason for change. ' ) );
		}
		if( COrderFormReference::ORDER_FORM_REFERENCE_OTHER == $this->getOrderFormReferenceId() && false == valStr( $this->getReasonForChange() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please enter details for other reason for change option. ' ) );
		}
		return $boolIsValid;
	}

	public function valDistributionTypeChangeOption() {
		$boolIsValid = true;

		if( false == valId( $this->getDistributionTypeChangeOption() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select distribution type change option. ' ) );
		}
		return $boolIsValid;
	}

	public function valComments() {
		$boolIsValid = true;

		if( false == valStr( $this->getComments() ) && COrderFormReference::TO_CREATE_A_LOGO_BY_DESIGNER == $this->getOrderFormReferenceId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please add comments.' ) );
		}
		return $boolIsValid;
	}

	public function valUploadFile( $objAdminDatabase, $strCurrentPlan = NULL ) {
		if( NULL != $this->getTempFilesToReview() ) {
			$arrstrFilesToValidate = json_decode( json_encode( $this->getTempFilesToReview() ), true );
		} elseif( NULL != $this->getFilesToReview() ) {
			$arrstrFilesToValidate = json_decode( json_encode( $this->getFilesToReview() ), true );
		} elseif( NULL != $this->getFloorPlanFiles() && 'true' == $this->getIsDraft() ) {
			$arrstrFilesToValidate = json_decode( json_encode( $this->getFloorPlanFiles() ), true );
		}

		if( COrderFormType::FLOOR_PLAN_PACKAGE == $this->getOrderFormTypeId() && true == valStr( $strCurrentPlan ) ) {
			$arrstrFilesToValidateForCurrent[$strCurrentPlan] = $arrstrFilesToValidate[$strCurrentPlan];
			$arrstrFilesToValidate = $arrstrFilesToValidateForCurrent;
		}

		if( true == valArr( $arrstrFilesToValidate ) ) {
			foreach( $arrstrFilesToValidate as $strFileType => $arrstrFileNames ) {
				$arrstrFileNames = json_decode( json_encode( $arrstrFileNames ), true );
				if( false == valArr( $arrstrFileNames ) && false == is_null( $arrstrFileNames ) ) {
					$arrstrFileNames = ( array ) $arrstrFileNames;
				}
				if( true == valArr( $arrstrFileNames ) && COrderFormType::CUSTOM_LOGO != $this->getOrderFormTypeId() ) {
					// if order form is of type floor plan & add files from CA side then set file count as 3 else uploaded file count
					$this->setFileCounts( ( ( COrderFormReference::FLOOR_PLAN_CONTRACT_AND_PAYMENT == $this->getOrderFormReferenceId() && 'false' == $this->getIsDraft() ) ? COrderFormRequest::FLOOR_PLAN_ADD_FILES_COUNT : \Psi\Libraries\UtilFunctions\count( $arrstrFileNames ) ) );
				}

				$boolIsValid = $this->processUploadFiles( $arrstrFileNames, $objAdminDatabase );
				if( false == $boolIsValid ) {
					return $boolIsValid;
				}
			}
		} else {
			$boolIsValid = $this->processUploadFiles( NULL, $objAdminDatabase );
		}

		return $boolIsValid;
	}

	public function processUploadFiles( $arrstrFileNames = NULL, $objAdminDatabase ) {
		$objFileExtension = \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionsByIds( CFileExtension::$c_arrintOrderFormImageExtensionIds, $objAdminDatabase );
		if( true == in_array( $this->getOrderFormTypeId(), [ COrderFormType::CUSTOM_LOGO, COrderFormType::TEMPLATE_LOGO ] ) ) {
			$objFileExtension = \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionsByIds( CFileExtension::$c_arrintOrderFormImageExtensionForCustomAndTemplateLogo, $objAdminDatabase );
		}
		$intFileCounts = ( true == valId( $this->getFileCounts() ) ) ? $this->getFileCounts() : 1;
		$boolIsValid = true;
		$intCountPdf = 0;
		$intCountJpg = 0;
		for( $intCount = 0; $intCount < $intFileCounts; $intCount++ ) {
			if( true == valArr( $arrstrFileNames ) ) {
				$strFileName = $arrstrFileNames[$intCount];
			} elseif( false == is_null( $this->getFileCounts() ) ) {
				$strGetFileName = 'getFileName' . $intCount;
				$strFileName = $this->$strGetFileName();
			} else {
				$strFileName = $this->getFileName();
			}

			if( false == in_array( $this->getOrderFormTypeId(), [ COrderFormType::TEMPLATE_LOGO, COrderFormType::CUSTOM_LOGO, COrderFormType::FLOOR_PLAN_PACKAGE ] ) ) {
				if( ( false == valStr( $strFileName ) && COrderFormReference::TO_CREATE_A_LOGO_WITH_INSTRUCTIONS == $this->getOrderFormReferenceId() ) || ( false == valStr( $strFileName ) && COrderFormReference::TO_CREATE_A_LOGO_BY_DESIGNER != $this->getOrderFormReferenceId() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select file to upload.' ) );
					return $boolIsValid;
				}
			}

			$arrstrFileInfo = pathinfo( $strFileName );

			if( $this->getOrderFormTypeId() != COrderFormType::FLOOR_PLAN_PACKAGE && ( false == array_key_exists( $arrstrFileInfo['extension'], rekeyObjects( 'Extension', $objFileExtension ) ) && false == is_null( $strFileName ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Only ' . implode( ', ', array_keys( rekeyObjects( 'Extension', $objFileExtension ) ) ) . ' type of files supported. ' ) );
				return $boolIsValid;
			}

			if( $this->getOrderFormTypeId() == COrderFormType::FLOOR_PLAN_PACKAGE ) {
				if( $arrstrFileInfo['extension'] == 'pdf' ) {
					$intCountPdf++;
				}
				if( $arrstrFileInfo['extension'] == 'jpg' ) {
					$intCountJpg++;
				}
				$this->setCountPdf( $intCountPdf );
				$this->setCountJpg( $intCountJpg );
			}
		}
		if( ( $this->getOrderFormTypeId() == COrderFormType::FLOOR_PLAN_PACKAGE ) && ( ( $this->getCountPdf() != COrderFormRequest::FLOOR_PLAN_REQUIRED_COUNT_BY_EXT ) || ( $this->getCountJpg() != COrderFormRequest::FLOOR_PLAN_REQUIRED_COUNT_BY_EXT ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Please upload 3 PDF and 3 JPG type of files for each plan. ' ) );

			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valCustomLogoName() {
		$boolIsValid = true;

		if( false == valStr( $this->getLogoTextName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select name you wish to be created into logo. ' ) );
		}
		return $boolIsValid;
	}

	public function valLogoTypeName() {
		$boolIsValid = true;

		if( false == valStr( $this->getLogoTypeName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select logo type. ' ) );
		}
		return $boolIsValid;
	}

	public function valNumberOfFloorPlans() {
		$boolIsValid = true;

		if( '' == $this->getNumberOfFloorPlans() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please enter number of floor plans. ' ) );
		}

		return $boolIsValid;
	}

	public function valNewMaximumLimit() {
		$boolIsValid = true;

		if( ( 0 >= $this->getCheckScanMaximumLimit() ) && ( 0 >= $this->getAchMaximumLimit() ) && ( 0 >= $this->getCreditCardMaximumLimit() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Atleast one \'New Maximum Limit\' is required ' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	// Domain order form validation

	public function valDomainUrl() {
		$strDomainUrl = $this->getDomainUrl();
		$strNewUrl = '';

		if( true == $this->getIsReordered() ) {
			$strDomainUrl = $this->getNewDomainUrl();
			if( 0 == strcasecmp( $strDomainUrl, $this->getDomainUrl() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' New URL should not be same as Old URL for any record(s). ' ) );
				return false;
			}
			$strNewUrl = 'New ';
		}
		if( false == valStr( $strDomainUrl ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'domain_url', $strNewUrl . 'Domain url is required. ' ) );
			return false;
		}
		/** As we have requirement to pass URL without http and https
		 * But filter_var() ( FILTER_VALIDATE_URL ) returns false when http or https are not added to URL
		 * So, we have added it manually to requested URL for validating purpose only
		 */
		$arrstrDomainUrlParts = parse_url( $strDomainUrl );
		if( false == valStr( $arrstrDomainUrlParts['scheme'] ) ) {
			$strDomainUrl = 'http://' . $strDomainUrl;
		}

		if( false == filter_var( $strDomainUrl, FILTER_VALIDATE_URL ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'domain_url', $strNewUrl . 'Domain url is invalid. ' ) );
			return false;
		}

		return true;
	}

	public function valAuthorizationCode() {
		$boolIsValid = true;
		if( false == valStr( $this->getAuthorizationCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'authorization_code', 'Authorization code is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valNameOfFloorPlans() {

		$boolIsValid = true;

		$intNumberOfFloorPans = ( int ) $this->getNumberOfFloorPlans();
		// As we have to access array so converting JSON object to array.
		$arrstrFloorPlansNames = json_decode( json_encode( $this->getNameOfFloorPlans() ), true );

		if( true == valId( $intNumberOfFloorPans ) && \Psi\Libraries\UtilFunctions\count( $arrstrFloorPlansNames ) != $intNumberOfFloorPans ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Number of Floor Plan and Name of Plan count should be same. ' ) );
			return $boolIsValid;
		}

		for( $intCount = 1; $intCount <= $intNumberOfFloorPans; $intCount++ ) {
			if( false == valStr( $arrstrFloorPlansNames[$intCount] ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please enter Name of Plan ' . ( int ) $intCount . '. ' ) );
			}
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		if( true == valArr( $arrstrFloorPlansNames ) && \Psi\Libraries\UtilFunctions\count( $arrstrFloorPlansNames ) !== \Psi\Libraries\UtilFunctions\count( array_count_values( $arrstrFloorPlansNames ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Floor Plan names should be unique.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valSitePlanComment() {
		$boolIsValid = true;

		if( false == valStr( $this->getSitePlanFileComment() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please enter comment.' ) );
		}

		return $boolIsValid;
	}

	public function valStyle() {
		$boolIsValid = true;
		if( false == valStr( $this->getStyle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select style for floor plan. ' ) );
		}
		return $boolIsValid;
	}

	public function valAngle() {
		$boolIsValid = true;
		if( false == valStr( $this->getAngle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select angle for floor plan. ' ) );
		}
		return $boolIsValid;
	}

	public function valColorOrTexture() {
		$boolIsValid = true;
		if( false == valStr( $this->getFloorPlanKitchenFlooring() ) || false == valStr( $this->getFloorPlanStudyFlooring() ) || false == valStr( $this->getFloorPlanLivingRoomFlooring() ) || false == valStr( $this->getFloorPlanBedroomFlooring() ) || false == valStr( $this->getFloorPlanBathroomFlooring() ) || false == valStr( $this->getFloorPlanCabinets() ) || false == valStr( $this->getFloorPlanCountertops() ) || false == valStr( $this->getFloorPlanAppliances() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select all the styles. ' ) );
		}
		return $boolIsValid;
	}

	public function validateFloorPlanDetails( $intFloorPlanStepId, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $intFloorPlanStepId ) {
			case COrderFormReference::FLOOR_PLAN_INFORMANTION:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valNumberOfFloorPlans();
				$boolIsValid &= $this->valNameOfFloorPlans();
				break;

			case COrderFormReference::FLOOR_PLAN_UPLOAD_FILES:
				$boolIsValid &= $this->valUploadFile( $objAdminDatabase );
				break;

			case COrderFormReference::FLOOR_PLAN_SELECT_PLAN:
				$boolIsValid &= $this->valStyle();
				$boolIsValid &= $this->valAngle();
				break;

			case COrderFormReference::FLOOR_PLAN_SELECT_STYLES:
				$boolIsValid &= $this->valColorOrTexture();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	public function valName( $strNameValue, $strName ) {
		$boolIsValid = true;

		if( false == valStr( $strNameValue ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Please Enter ' . $strName . ' name. ' ) );
		} elseif( \Psi\CStringService::singleton()->preg_match( '/[0-9]+/', $strNameValue ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Numbers are not allowed for ' . $strName . ' name.' ) );
		}
		return $boolIsValid;
	}

	public function valEmailAddress( $strEmailAddress, $strName ) {
		$boolIsValid = true;

		if( false == isset( $strEmailAddress ) || ( false == CValidation::validateEmailAddresses( $strEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', ' ' . $strName . ' email address does not appear to be valid.' ) );
		}
		return $boolIsValid;
	}

	public function validateOrderFormRequestDetails( $intOrderFormTypeId, $objAdminDatabase, $intFloorPlanStepId ) {
		$boolIsValid = true;
		switch( $intOrderFormTypeId ) {

			case COrderFormType::CHECK_SCANNER:
				$boolIsValid &= $this->valOrderFormRequestId();
				$boolIsValid &= $this->valPropertyId();
				break;

			case COrderFormType::DOMAIN_ORDER_TRANSFER:
				$boolIsValid &= $this->valPropertyId();
				if( COrderFormReference::TRANSFER_EXISTING_DOMAIN_TO_OTHER == $this->getOrderFormReferenceId() ) {
					$boolIsValid &= $this->valAuthorizationCode();
				}
				$boolIsValid &= $this->valDomainUrl();
				break;

			case COrderFormType::FLOOR_PLAN_PACKAGE:
				$boolIsValid &= $this->validateFloorPlanDetails( $intFloorPlanStepId, $objAdminDatabase );
				break;

			case COrderFormType::TEMPLATE_LOGO:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valLogoTypeName();
				break;

			case COrderFormType::DISTRIBUTION_TYPE_CHANGE_REQUEST:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valDistributionTypeChangeOption();
				$boolIsValid &= $this->valAuthorizedSignerId();
				break;

			case COrderFormType::CHECK_SCANNING_MAX_PAYMENT_INCREASE_REQUEST:
				$boolIsValid &= $this->valOrderFormRequestId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valNewMaximumLimit();
				$boolIsValid &= $this->valReasonForChangeOption();
				$boolIsValid &= $this->valAuthorizedSignerId();
				break;

			case COrderFormType::SITE_PLAN:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valSitePlanComment();
				$boolIsValid &= $this->valUploadFile( $objAdminDatabase );
				break;

			case COrderFormType::CUSTOM_LOGO:
				$boolIsValid &= $this->valCustomLogoName();
				$boolIsValid &= $this->valComments();
				$boolIsValid &= $this->valUploadFile( $objAdminDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intOrderFormTypeId, $objAdminDatabase, $intFloorPlanStepId ) {

		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->validateOrderFormRequestDetails( $intOrderFormTypeId, $objAdminDatabase, $intFloorPlanStepId );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->validateOrderFormRequestDetails( $intOrderFormTypeId, $objAdminDatabase, $intFloorPlanStepId );
				break;

			case VALIDATE_DELETE:
				break;

			case VALIDATE_PROPERTY_PHONE_NUMBER_DETAILS:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPhoneNumber( $this->getPhoneNumber() );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * other functions
	 */

	public function setPaymentOptionDetails( $intPaymentTypeId, $strCCContactNumber, $strAchContactNumber, $arrstrMailByCheckDetails ) {
		if( true == in_array( $intPaymentTypeId, CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			$this->setPaymentDetailContactNumber( $strCCContactNumber );
		} elseif( $intPaymentTypeId == CPaymentType::ACH ) {
			$this->setPaymentDetailContactNumber( $strAchContactNumber );
		} elseif( $intPaymentTypeId == CPaymentType::CHECK ) {
			$this->setPaymentDetailContactNumber( $arrstrMailByCheckDetails['contact_number'] );
			$this->setPaymentDetailEmailId( $arrstrMailByCheckDetails['email_address'] );
		}
	}

	public function valAuthorizedSignerId() {
		$boolIsValid = true;
		if( false == valId( $this->getAuthorizedSignerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Authorized Signer is required. ' ) );
		}
		return $boolIsValid;
	}

	public function buildFileData( $strInputTypeFileName = 'order_form_request_details', $intCid, $intPsLeadId, $intFileIndex = NULL ) {

		if( empty( $_FILES ) && empty( $_POST ) && isset( $_SERVER['REQUEST_METHOD'] ) && 'post' == \Psi\CStringService::singleton()->strtolower( $_SERVER['REQUEST_METHOD'] ) ) {
			$strPostMaxSize = ini_get( 'post_max_size' );
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload. File size should not exceed more than {%s,0}B.', [ $strPostMaxSize ] ) ) );
			return false;
		}

		$this->setCid( $intCid );
		$this->setPsLeadId( $intPsLeadId );
		$this->setFilePath( $this->calcStorageKey() );

		if( !is_numeric( $intFileIndex ) ) {
			if( isset( $_FILES[$strInputTypeFileName]['name'] ) ) 	 $this->setUploadFileName( time() . '-' . $_FILES[$strInputTypeFileName]['name'] );
			if( isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) ) 	 $this->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'] );
		} else {
			if( isset( $_FILES[$strInputTypeFileName]['name'] ) ) 	 $this->setUploadFileName( time() . '-' . $_FILES[$strInputTypeFileName]['name'][$intFileIndex] );
			if( isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) ) 	 $this->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'][$intFileIndex] );
		}

		return true;
	}

	public function uploadObject( $objObjectStorageGateway, $boolEncrypt = true ) {

		if( !valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			return false;
		}

		$arrmixRequest = $this->createPutGatewayRequest( [ 'data' => CFileIo::fileGetContents( $this->getTempFileName() ), 'noEncrypt' => !$boolEncrypt ] );
		if( !is_null( $this->getFilePath() ) && !is_null( $this->getUploadFileName() ) ) {

			$objResponse = $objObjectStorageGateway->putObject( $arrmixRequest );

			if( true == $objResponse->hasErrors() ) {
				return false;
			}

			$this->setObjectStorageGatewayResponse( $objResponse );
			return true;
		}

		return false;

	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

		$strPath = '';

		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				if( COrderFormType::SITE_PLAN == $this->getOrderFormTypeId() && $this->getIsReviewFiles() ) {
					$strPath	= '/ps_leads/' . $this->getPsLeadId() . '/order_forms/' . \COrderFormType::$c_arrintOrderFormFolderNames[$this->getOrderFormTypeId()] . '/' . $this->getOrderFormRequestId() . '/' . $this->getPropertyName() . '/';
				} else if( COrderFormType::FLOOR_PLAN_PACKAGE == $this->getOrderFormTypeId() && $this->getIsReviewFiles() ) {
					$strPath	= '/ps_leads/' . $this->getPsLeadId() . '/order_forms/' . \COrderFormType::$c_arrintOrderFormFolderNames[$this->getOrderFormTypeId()] . '/' . $this->getOrderFormRequestId() . '/files_to_review/' . $this->getFloorPlanPackageName() . '/';
				} else if( COrderFormType::FLOOR_PLAN_PACKAGE == $this->getOrderFormTypeId() && $this->getIsReviewFeedbackFiles() ) {
					$strPath	= '/ps_leads/' . $this->getPsLeadId() . '/order_forms/' . \COrderFormType::$c_arrintOrderFormFolderNames[$this->getOrderFormTypeId()] . '/' . $this->getOrderFormRequestId() . '/feedback_files/';
				} else {
					$strPath	= '/ps_leads/' . $this->getPsLeadId() . '/order_forms/' . \COrderFormType::$c_arrintOrderFormFolderNames[$this->getOrderFormTypeId()] . '/' . $this->getOrderFormRequestId() . '/';
				}
				break;

			default:
				$strPath = sprintf( '%d/%s/%d/%d/%d/%d/%s/%d/', $this->getCid(), 'ps_leads', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getPsLeadId(), 'order_forms', strtotime( date( 'h:i:sa' ) ) );
		}

		return $this->getUploadFileName() ? $strPath . $this->getUploadFileName() : $strPath;
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return PATH_MOUNTS_DOCUMENTS;

			default:
				return CONFIG_OSG_BUCKET_DOCUMENTS;
		}
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

	public function downloadObject( $objObjectStorageGateway, $strFileName, $strDispositionType = NULL, $boolIsFullPath = false, $strReferenceTag = NULL ) {

		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase, $strReferenceTag )->createGatewayRequest( [ 'checkExists' => true ] );
		unset( $arrmixStorageArgs['cid'] );
		if( $arrmixStorageArgs['checkExists'] ) {
			unset( $arrmixStorageArgs['checkExists'] );
			$arrmixStorageArgs['outputFile'] = 'temp';
			$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixStorageArgs );
			if( valObj( $arrobjObjectStorageResponse, Psi\Libraries\ObjectStorage\DataTransferObjects\CObjectStorageGatewayResponse::class ) && $arrobjObjectStorageResponse->hasErrors() ) {
				return false;
			}
			// TODO: Remove this once we migrate files from Mounts to OSG
			if( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM == $arrmixStorageArgs['storageGateway'] ) {
				$strFullPath = $arrobjObjectStorageResponse['request']['completePath'];
			} else {
				$strFullPath = $arrobjObjectStorageResponse['outputFile'];
			}

		}

		if( $boolIsFullPath ) {
			return $strFullPath;
		}

		header( 'Pragma:public' );
		header( 'Expires:0' );
		header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control:public' );
		header( 'Content-Description:File Transfer' );
		header( 'Content-Type: application/pdf' );
		header( 'Content-Disposition: ' . $strDispositionType . '; filename="' . $strFileName . '"' );
		echo CFileIo::fileGetContents( $strFullPath );
		exit();
	}

	public function fetchFloorPlanStoredObjects( $objDatabase, $arrmixWhere, $intLimit = 0 ) {
		$strWhere = '';
		if( valArr( $arrmixWhere ) ) {
			foreach( $arrmixWhere as $strColumnName => $mixColumnMatchValue ) {
				if( valStr( $strWhere ) ) {
					$strWhere .= ' AND ';
				}

				if( stripos( $mixColumnMatchValue, ' NULL' ) ) {
					$strWhere .= $strColumnName . ' ' . $mixColumnMatchValue;
				} else {
					switch( $strColumnName ) {
						case 'reference_tag':
						case 'title':
							$strWhere .= $strColumnName . ' LIKE \'' . $mixColumnMatchValue . '\'';
							break;

						default:
							$strWhere .= $strColumnName . ' = ' . $mixColumnMatchValue;
							break;
					}
				}
			}

			$strLimit = ( $intLimit && 0 < $intLimit ) ? ' LIMIT ' . $intLimit : '';

			$strStoredObjectSql = ' SELECT *
						FROM stored_objects
						WHERE ' . $strWhere . '
						ORDER BY id' . $strLimit;
			return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjects( $strStoredObjectSql, $objDatabase );
		}
		return false;
	}

	public function generateReferenceTag( $intOrderFormTypeId, $strReferenceType = NULL, $intFloorPlanId = NULL, $intUploadedFileCount = NULL, $strFileKey = NULL ) {

		switch( $strReferenceType ) {
			case 'add_order_form' :
				return str_replace( ' ', '_', strtolower( COrderFormType::$c_arrintOrderFormNames[$intOrderFormTypeId] ) );

			case 'floor_plan_files_to_review' :
				return $strReferenceType . '-' . $this->getId() . '-' . $intFloorPlanId . '-' . $intUploadedFileCount;

			case 'floor_plan_feedback_files' :
				$strFileKey = valId($intFloorPlanId) ? '-floor-plan-' . $intFloorPlanId : '-' . $strFileKey;
				return $strReferenceType . '-' . $this->getId() . $strFileKey . '-' . $intUploadedFileCount . '-';

			default :
				return str_replace( ' ', '_', strtolower( COrderFormType::$c_arrintOrderFormNames[ $intOrderFormTypeId ] ) ) . '-' . $this->getId() . '-';
		}
	}

}
?>