<?php

class CEeocDetail extends CBaseEeocDetail {

	protected $m_strEEOCEmployeeSignature;

	/**
	 * Get Functions
	 *
	 */

	public function getEmployeeSignature() {
		return $this->m_strEEOCEmployeeSignature;
	}

	/**
	 * Get Functions
	 *
	 */

	public function setEmployeeSignature( $strEEOCEmployeeSignature ) {
		$this->m_strEEOCEmployeeSignature = $strEEOCEmployeeSignature;
	}

	/**
	 * Set Value Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['employee_signature'] ) ) 		$this->setEmployeeSignature( $arrmixValues['employee_signature'] );

	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsVolunteered( $objCurrentUser ) {

		$boolIsValid = true;

		if( true == $this->getIsVolunteered() && false == valStr( $this->getEmployeeSignature() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_volunteered', 'Please provide your signature. ' ) );
		}

		if( true == valStr( $this->getEmployeeSignature() ) ) {
			if( true == \Psi\CStringService::singleton()->strcasecmp( $objCurrentUser->getEmployee()->getNameFull(), $this->getEmployeeSignature() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_volunteered', 'Please provide a valid signature. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaritalStatus() {

		$boolIsValid = true;

		if( false == valStr( $this->getMaritalStatus() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'marital_status', 'Please select appropriate marital status. ' ) );
		}

		return $boolIsValid;
	}

	public function valGender() {

		$boolIsValid = true;

		if( false == valStr( $this->getGender() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gender', 'Please select appropriate gender. ' ) );
		}

		return $boolIsValid;
	}

	public function valVeteranStatus() {

		$boolIsValid = true;

		if( false == valStr( $this->getVeteranStatus() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'veteran_status', 'Please select appropriate veteran status. ' ) );
		}

		return $boolIsValid;
	}

	public function valDisabilityStatus() {

		$boolIsValid = true;

		if( false == valStr( $this->getDisabilityStatus() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disability_status', 'Please select appropriate disability status. ' ) );
		}

		return $boolIsValid;
	}

	public function valEthnicGroup() {

		$boolIsValid = true;

		if( false == valStr( $this->getEthnicGroup() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ethnic_group', 'Please select appropriate ethnic group. ' ) );
		} elseif( 'Not Hispanic or Latino' == $this->getEthnicGroup() ) {
			$boolIsValid = $this->valRacialGroup();
		}

		return $boolIsValid;
	}

	public function valRacialGroup() {

		$boolIsValid = true;

		if( false == valStr( $this->getRacialGroup() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'racial_group', 'Please select appropriate racial group. ' ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryLanguage() {
		$boolIsValid = true;

		if( false == valStr( json_decode( $this->getOtherEeocDetails(), true )['primary_language'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'other_eeoc_details', 'Primary language required.' ) );
		}

		return $boolIsValid;
	}

	public function valSecondLanguage() {
		$boolIsValid = true;

		if( false == valArr( json_decode( $this->getOtherEeocDetails(), true )['second_language'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'other_eeoc_details', 'Second language(s) required.' ) );
		}

		return $boolIsValid;
	}

	public function valThirdLanguage() {
		$boolIsValid = true;

		if( false == valArr( json_decode( $this->getOtherEeocDetails(), true )['third_language'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'other_eeoc_details', 'Third language(s) required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objCurrentUser ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valMaritalStatus();
				$boolIsValid &= $this->valGender();
				$boolIsValid &= $this->valVeteranStatus();
				$boolIsValid &= $this->valDisabilityStatus();
				$boolIsValid &= $this->valEthnicGroup();
				$boolIsValid &= $this->valIsVolunteered( $objCurrentUser );

				if( CCountry::CODE_USA == $objCurrentUser->getEmployee()->getCountryCode() ) {
					$boolIsValid &= $this->valPrimaryLanguage();
					$boolIsValid &= $this->valSecondLanguage();
					$boolIsValid &= $this->valThirdLanguage();
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMaritalStatus();
				$boolIsValid &= $this->valGender();
				$boolIsValid &= $this->valVeteranStatus();
				$boolIsValid &= $this->valDisabilityStatus();
				$boolIsValid &= $this->valEthnicGroup();

				if( CCountry::CODE_USA == $objCurrentUser->getEmployee()->getCountryCode() ) {
					$boolIsValid &= $this->valPrimaryLanguage();
					$boolIsValid &= $this->valSecondLanguage();
					$boolIsValid &= $this->valThirdLanguage();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>