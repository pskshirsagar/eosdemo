<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyPhoneNumbers
 * Do not add any new functions to this class.
 */

class CCompanyPhoneNumbers extends CBaseCompanyPhoneNumbers {

	public static function fetchCompanyPhoneNumberByCidByPhoneNumberTypeId( $intCid, $intPhoneNumberTypeId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_phone_numbers cpn
					WHERE
						cpn.cid = ' . ( int ) $intCid . '
						AND cpn.phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId;

		return self::fetchCompanyPhoneNumber( $strSql, $objDatabase );
	}

}
?>