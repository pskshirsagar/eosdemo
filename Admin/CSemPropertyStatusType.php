<?php

class CSemPropertyStatusType extends CBaseSemPropertyStatusType {

	const NOT_REQUESTED			= 1;
	const WORKS_UN_POSTED		= 2;
	const VACANCY_UN_POSTED		= 3;
	const PENDING_APPROVAL		= 4;
	const ENABLED				= 5;
	const REJECTED				= 6;
	const NOT_RETURN_LOCKED		= 7;
	const NOT_TERMINATED		= 8;
	const DEMO					= 9;
	const PAUSED				= 10;
	const DISABLED				= 11;

	public static function assignSmartyProprtyStatusTypes( $objSmarty ) {

		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_NOT_REQUESTED',			self::NOT_REQUESTED );
		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_WORKS_UN_POSTED',  		self::WORKS_UN_POSTED );
		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_VACANCY_UN_POSTED',		self::VACANCY_UN_POSTED );
		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_PENDING_APPROVAL', 		self::PENDING_APPROVAL );
		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_ENABLED', 		 		self::ENABLED );
		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_REJECTED', 				self::REJECTED );
		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_NOT_RETURN_LOCKED', 		self::NOT_RETURN_LOCKED );
		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_NOT_TERMINATED', 			self::NOT_TERMINATED );
		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_DEMO', 					self::DEMO );
		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_PAUSED', 					self::PAUSED );
		$objSmarty->assign( 'SEM_PROPERTY_STATUS_TYPE_DISABLED',				self::DISABLED );
	}
}
?>