<?php

class CReportSqlDetail extends CBaseReportSqlDetail {

	public function valId() {
		$boolValid = true;
		return $boolValid;
	}

	public function valName() {
		$boolValid = true;
		return $boolValid;
	}

	public function valDescription() {
		$boolValid = true;
		return $boolValid;
	}

	public function valQuery( $objAdminDatabase ) {
		$boolValid = true;
		return $boolValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolValid &= $this->valQuery( $objAdminDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolValid = false;
				break;
		}

		return $boolValid;
	}

}
?>