<?php

class CEmployeeCompensation extends CBaseEmployeeCompensation {

	protected $m_strCompensationEncrypted;

	protected $m_intCompensationDecrypted;

	const SHOW_BOTH_TERMINATED_AND_NONTERMINATED_EMPLOYEES = 2;

	/**
	 * Other Functions
	 */

	public static function insertEmployeeCompensation( $intEmployeeId, $strDateStarted, $objDatabase, $intUserId ) {

	$objEmployeeCompensation = new CEmployeeCompensation();

		$objEmployeeCompensation->setEmployeeId( $intEmployeeId );

		if( true == $objEmployeeCompensation->insert( $intUserId, $objDatabase ) ) {
			if( false == CEmployeeCompensationLog::insertEmployeeCompensationLog( $intEmployeeId, $strDateStarted, $objDatabase, $intUserId ) ) {
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Setter Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['compensation_encrypted'] ) ) $this->setCompensationEncrypted( $arrmixValues['compensation_encrypted'] );
	}

	public function setCompensationEncrypted( $strCompensationEncrypted ) {
		$this->m_strCompensationEncrypted = $strCompensationEncrypted;
	}

	public function setCompensationDecrypted( $intCompensationDecrypted ) {
		$this->m_intCompensationDecrypted = $intCompensationDecrypted;
	}

	/**
	 * Getter Functions
	 */

	public function getCompensationEncrypted() {
		return $this->m_strCompensationEncrypted;
	}

	public function getCompensationDecrypted() {
		return $this->m_intCompensationDecrypted;
	}

	public function valEmployeeId( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == empty( $this->m_intEmployeeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee id is required.' ) );
		} elseif( true == is_numeric( $this->m_intEmployeeId ) ) {
			$objEmployeeCompensaton = CEmployeeCompensations::fetchEmployeeCompensationsByEmployeeId( $this->m_intEmployeeId, $objAdminDatabase );

			if( true == valObj( $objEmployeeCompensaton, 'CEmployeeCompensation' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee Compensation already exists. Please update existing one.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid = $this->valEmployeeId( $objAdminDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>