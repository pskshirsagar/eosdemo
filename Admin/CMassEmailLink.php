<?php

use Psi\Eos\Admin\CMassEmailClicks;

class CMassEmailLink extends CBaseMassEmailLink {
	protected $m_arrobjMassEmailClicks;

	public function valMassEmailId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMassEmailId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mass_email_id', 'Mass email id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUrl() {
		$boolIsValid = true;

		if( true == is_null( $this->getUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Url is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valUrl();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMassEmailId();
				$boolIsValid &= $this->valUrl();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchMassEmailClicks( $objAdminDatabase ) {
		return CMassEmailClicks::createService()->fetchMassEmailClicksByMassEmailLinkId( $this->getId(), $objAdminDatabase );
	}

	public function fetchMassEmailClicksByEmployeeId( $intEmployeeId, $objAdminDatabase ) {
		return $this->m_arrobjMassEmailClicks = CMassEmailClicks::createService()->fetchMassEmailClicksByMassEmailIdByMassEmailLinkIdByEmployeeId( $this->getMassEmailId(), $intEmployeeId, $this->getId(), $objAdminDatabase );
	}

	/**
	 * Get Functions
	 */

	public function getMassEmailClicks() {
		return $this->m_arrobjMassEmailClicks;
	}

	/**
	 * Set Functions
	 */

	public function setMassEmailClicks( $arrobjMassEmailClicks ) {
		$this->m_arrobjMassEmailClicks = $arrobjMassEmailClicks;
	}

	/**
	 * Add Functions
	 */

	public function addMassEmailClick( $objMassEmailClick ) {
		$this->m_arrobjMassEmailClicks[$objMassEmailClick->getId()] = $objMassEmailClick;
	}

	/**
	 * Other Functions
	 */

	public function incrementClickCount() {
		$this->setClickCount( $this->getClickCount() + 1 );
	}

}
?>