<?php

class CExpensesStatusTypes extends CBaseExpensesStatusTypes {

	public static function fetchExpensesStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CExpensesStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchExpensesStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CExpensesStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>