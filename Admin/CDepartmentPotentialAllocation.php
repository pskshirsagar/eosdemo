<?php

class CDepartmentPotentialAllocation extends CBaseDepartmentPotentialAllocation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepartmentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllocatedPotential() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayReviewYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdditionalPotential() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>