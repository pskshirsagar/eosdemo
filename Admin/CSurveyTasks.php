<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTasks
 * Do not add any new functions to this class.
 */

class CSurveyTasks extends CBaseSurveyTasks {

	public static function fetchSurveyTasksByTaskIdBySubjectReferenceId( $intTaskId, $intSubjectReferenceId, $objDatabase, $intSurveyId = NULL ) {

		$strJoinCondition = ( true == valId( $intSurveyId ) ) ? ' AND s.id = ' . ( int ) $intSurveyId : '';

		$strSql = 'SELECT
						st.*
					FROM
						survey_tasks st
						JOIN surveys s ON ( st.survey_id = s.id ' . $strJoinCondition . ' )
					WHERE
						task_id = ' . ( int ) $intTaskId . '
						AND s.subject_reference_id = ' . ( int ) $intSubjectReferenceId . '
					ORDER BY
						st.id DESC';

		return self::fetchSurveyTasks( $strSql, $objDatabase );
	}

	public static function fetchRecentSurveyTasksBySurveyIdsByReviewedEmployeeIds( $arrintSurveyIds, $arrintResponderReferenceIds, $objDatabase, $boolIsAddCompletedOnDate = false, $strLastAccessedOnDatetime = NULL ) {

		$strWhereClause = '';
		if( false == valArr( $arrintSurveyIds ) || false == valArr( $arrintResponderReferenceIds ) ) return NULL;

		$strWhereClause .= ( true == $boolIsAddCompletedOnDate && true == valStr( $strLastAccessedOnDatetime ) ) ? 'AND s.response_datetime >  \'' . $strLastAccessedOnDatetime . '\'': '';

		$strSql = ' SELECT
						*
					FROM
						(
							SELECT
								DISTINCT ON ( st.id ) t.id AS task_id,
								st.survey_id,
								st.task_id,
								st.is_achieved,
								st.created_by,
								sqa.answer,
								s.responder_reference_id,
								t.description,
								t.task_status_id,
								t.due_date,
								e.preferred_name AS name_full
							FROM
								survey_tasks st
								LEFT JOIN surveys s ON ( s.id = st.survey_id )
								LEFT JOIN survey_question_answers sqa ON (s.id = sqa.survey_id)
								JOIN tasks t ON ( st.task_id = t.id AND t.deleted_by IS NULL )
								LEFT JOIN users u ON ( st.created_by = u.id )
								LEFT JOIN employees e ON ( u.employee_id = e.id )
							WHERE
								s.responder_reference_id IN ( ' . implode( ',', $arrintResponderReferenceIds ) . ' )
								AND s.survey_type_id  = ' . CSurveyType::PERSONAL_REVIEW . '
								AND s.id IN ( ' . implode( ',', $arrintSurveyIds ) . ') ' . $strWhereClause . '
							ORDER BY
								st.id DESC
						) as sub_query
					ORDER BY sub_query.created_by';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchProgressReportSurveyGoalsByEmployeeIdByUserId( $intEmployeeId, $intUserId, $intSurveyId, $objDatabase, $boolIsLoadIncompleteGoals = false, $boolIsDraft = false ) {

		$strWhereClause = ' AND st.is_achieved = 1 AND  st.survey_id =  ' . ( int ) $intSurveyId;
		$strOrderBy 	= ' ORDER BY
							t.completed_on DESC';
		$strSurveyClause = ' st.survey_id <=  ' . ( int ) $intSurveyId . ' ';

		if( true == $boolIsDraft ) {
			$strSurveyClause = ' st.survey_id =  ' . ( int ) $intSurveyId . ' AND st.created_by =  ' . ( int ) $intUserId;
		}

		if( true == $boolIsLoadIncompleteGoals ) {
			$strWhereClause = ' AND st.is_achieved <> 1
								AND ' . $strSurveyClause . '
								AND st.task_id NOT IN (
												SELECT
													st.task_id
												FROM
													survey_tasks st
													JOIN tasks t ON ( st.task_id = t.id AND t.deleted_by IS NULL )
													JOIN surveys s ON ( s.id = st.survey_id )
												WHERE
													t.user_id = ' . ( int ) $intUserId . '
													AND st.survey_id >= s.id
													AND s.subject_reference_id = s.responder_reference_id
													AND s.responder_reference_id = ' . ( int ) $intEmployeeId . '
													AND ' . $strSurveyClause . '
													AND st.is_achieved = 1
												)';

			$strOrderBy = ' ORDER BY
								t.due_date DESC';
		}

		if( false == $boolIsLoadIncompleteGoals && true == $boolIsDraft ) {
			$strWhereClause = ' AND st.is_achieved = 0 AND t.task_status_id = ' . CTaskStatus:: COMPLETED . ' AND  st.survey_id =  ' . ( int ) $intSurveyId;
		}

		$strSql = 'SELECT st.*,
						t.description AS task_description,
						t.task_status_id,
						t.due_date,
						t.completed_on,
						t.task_type_id,
						tr.id as task_reference_id
					FROM
						survey_tasks st
						JOIN tasks t ON ( st.task_id = t.id AND t.deleted_by IS NULL)
						JOIN surveys s ON ( s.id = st.survey_id )
						LEFT JOIN task_references tr ON ( tr.task_id = t.id AND tr.deleted_by IS NULL)
					WHERE
						t.user_id = ' . ( int ) $intUserId . '
						AND st.survey_id >= s.id
						AND s.subject_reference_id = s.responder_reference_id
						AND ( to_date( to_char( t.completed_on, \'MM/DD/YYYY\' ), \'MM/DD/YYYY\' ) <= to_date( to_char( t.due_date, \'MM/DD/YYYY\' ), \'MM/DD/YYYY\' ) OR t.completed_on IS NULL )
						AND s.responder_reference_id = ' . ( int ) $intEmployeeId .
						$strWhereClause .
					$strOrderBy;

		return self::fetchSurveyTasks( $strSql, $objDatabase );

	}

	public static function fetchSurveyTasksGoalsByTasksIds( $arrintTaskIds, $objDatabase ) {

		if( false == valArr( $arrintTaskIds ) ) return NULL;

		$strSql = 'SELECT
					st.*,
					t.description AS task_description,
					t.task_status_id,
					t.due_date
				FROM
					survey_tasks st
					JOIN tasks t ON ( st.task_id = t.id )
				WHERE
					st.task_id IN ( ' . implode( ',', $arrintTaskIds ) . ' )
					AND t.deleted_on IS NULL';

		return self::fetchSurveyTasks( $strSql, $objDatabase );
	}

	public static function fetchSurveyTasksBySurveyIds( $arrintSurveyIds, $objDatabase ) {

		if( false == valArr( $arrintSurveyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						survey_tasks
					WHERE
						survey_id IN ( ' . implode( ',', $arrintSurveyIds ) . ' )';

		return self::fetchSurveyTasks( $strSql, $objDatabase );
	}

	public static function fetchEmployeeCompletedGoalsAndAccomplishments( $boolIsGoals, $intEmployeeId, $strFromDate, $strToDate, $objDatabase ) {

		$strSelectCondition = ( true == $boolIsGoals ) ? 'DISTINCT ON ( t.id ) t.id AS task_id' : 'count( st.id )';

		$strFromClause = '';

		if( true == $boolIsGoals ) {
			$strWhereAdditionalCondition = ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND s.response_datetime > \'' . $strFromDate . '\' AND s.response_datetime < \'' . $strToDate . '\'' : '';
			$strWhereClause = ' AND st.is_achieved <> 1 AND t.completed_on IS NULL' . $strWhereAdditionalCondition;
			$strFromClause = 'LEFT JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )';
		} else {
			$strWhereAdditionalCondition = ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND s.response_datetime > \'' . $strFromDate . '\' AND s.response_datetime < \'' . $strToDate . '\'' : '';
			$strWhereClause = ' AND st.is_achieved = 1 AND s.survey_type_id = ' . CSurveyType::PERSONAL_REVIEW . ' AND s.subject_reference_id = s.responder_reference_id' . $strWhereAdditionalCondition;
		}

		$strSql = ' SELECT ' . $strSelectCondition . '
					FROM
						survey_tasks st
						JOIN tasks t ON ( st.task_id = t.id AND t.deleted_by IS NULL )
						JOIN surveys s ON ( s.id = st.survey_id ) ' . $strFromClause . '
					WHERE s.responder_reference_id = ' . ( int ) $intEmployeeId . $strWhereClause;

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == $boolIsGoals ) ? \Psi\Libraries\UtilFunctions\count( $arrstrData ) : $arrstrData[0]['count'];
	}

	public static function fetchTeamAverageCPARatings( $intEmployeeId, $strFromDate, $strToDate, $objDatabase ) {

		$strWhereAdditionalCondition = ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND s.response_datetime > \'' . $strFromDate . '\' AND s.response_datetime < \'' . $strToDate . '\'': '';
		$strSql = '	WITH team_surveys AS (
											SELECT
												sqa.numeric_weight AS team_employee_rating,
												s.id AS survey_id
											FROM
												surveys s
												INNER JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
											WHERE
												s.subject_reference_id IN ( 
													(WITH recursive all_employees( employee_id, depth) AS ((
													 SELECT e.id,
															1
													 FROM employees e
													 WHERE (e.id = ' . ( int ) $intEmployeeId . ' OR
														e.reporting_manager_id = ' . ( int ) $intEmployeeId . ') 
													ORDER BY e.id = ' . ( int ) $intEmployeeId . ' DESC
													)
													UNION
													SELECT e.id, al.depth + 1
													FROM employees e, all_employees al
													WHERE e.reporting_manager_id = al.employee_id AND
													al.employee_id <> ' . ( int ) $intEmployeeId . '
													)
													SELECT e.id
													FROM all_employees al
													JOIN employees e ON (al.employee_id = e.id)
													WHERE e.date_terminated IS NULL AND e.employee_status_type_id = 1) )
													AND s.declined_datetime IS NULL
 													AND s.survey_type_id = ' . CSurveyType::PERSONAL_REVIEW . '
													AND sqa.numeric_weight IS NOT NULL' . $strWhereAdditionalCondition . '
											ORDER BY
												s.created_by DESC)
				SELECT
					round( ( ( SUM ( ts.team_employee_rating ) ) / ( count ( ts.survey_id ) ) ), 1 ) as average_team_rating
				FROM
					team_surveys ts ';

		return current( fetchData( $strSql, $objDatabase ) );

	}

	public static function fetchEmployeeAndManagerAverageCPARatings( $arrintEmployeeIds, $intEmployeeId = NULL, $strFromDate = NULL, $strToDate = NULL, $boolShowIndividualHighlights = false, $objDatabase, $boolCpaFeedBack = false ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return [];
		}

		if( true == $boolShowIndividualHighlights ) {
			$strMainSelectClause = ' SELECT * FROM (
										SELECT
											es.subject_reference_id,
											round( ( ( SUM ( es.employee_rating ) ) / ( count ( es.survey_id ) ) ), 1 ) AS average_employee_rating,
											dense_rank() OVER( ORDER BY round( ( ( SUM ( es.employee_rating ) ) / ( count ( es.survey_id ) ) ), 1 ) DESC ) AS average_employee_rating_rank,
											round( ( ( SUM ( ms.manager_rating ) ) / ( count ( ms.trigger_reference_id ) ) ), 1 ) AS average_manager_rating,
											dense_rank() OVER( ORDER BY round( ( ( SUM ( ms.manager_rating ) ) / ( count ( ms.trigger_reference_id ) ) ), 1 ) DESC ) AS average_manager_rating_rank,
											count( DISTINCT es.survey_id ) AS total_cpa
										FROM
											employee_surveys es
											LEFT JOIN manager_surveys ms ON ( es.survey_id = ms.trigger_reference_id )
										WHERE ms.manager_rating IS NOT NULL
										GROUP BY
											es.subject_reference_id
									) main WHERE main.subject_reference_id = ' . ( int ) $intEmployeeId;
		} else {
			$strMainSelectClause = ' SELECT
										es.subject_reference_id,
										round( ( ( SUM ( es.employee_rating ) ) / ( count ( es.survey_id ) ) ), 1 ) AS average_employee_rating,
										round( ( ( SUM ( ms.manager_rating ) ) / ( count ( ms.trigger_reference_id ) ) ), 1 ) AS average_manager_rating,
										count( DISTINCT es.survey_id ) AS total_cpa
									FROM
										employee_surveys es
										LEFT JOIN manager_surveys ms ON ( es.survey_id = ms.trigger_reference_id )
									WHERE ms.manager_rating IS NOT NULL
									GROUP BY
										es.subject_reference_id';
		}

		$strWhereManagerAdditionalClause = '';
		if( true == valStr( $strFromDate ) && true == valStr( $strToDate ) && false == $boolCpaFeedBack ) {
			$strWhereManagerAdditionalClause .= ' AND s.response_datetime::date >= \'' . $strFromDate . '\' AND s.response_datetime::date <= \'' . $strToDate . '\'';
		} elseif( true == valStr( $strFromDate ) && true == $boolCpaFeedBack ) {
			$strWhereManagerAdditionalClause .= ' AND s.response_datetime::date >= \'' . $strFromDate . '\'';
		}

		$strWhereEmployeeAdditionalCondition = ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND s.response_datetime::date >= \'' . $strFromDate . '\' AND s.response_datetime::date <= \'' . $strToDate . '\'': '';

		$strSql = '
			WITH employee_surveys AS (
						SELECT
							s.subject_reference_id,
							sqa.numeric_weight AS employee_rating,
							s.id AS survey_id
						FROM
							surveys s
							JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						WHERE
							s.subject_reference_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
							AND s.declined_datetime IS NULL
 							AND s.survey_type_id = ' . CSurveyType::PERSONAL_REVIEW . '
							AND sqa.numeric_weight IS NOT NULL' . $strWhereEmployeeAdditionalCondition . '
						ORDER BY
							s.created_by DESC),
				 manager_surveys AS (
						SELECT
							DISTINCT ( s.trigger_reference_id ) AS trigger_reference_id,
							s.subject_reference_id,
							sqa.numeric_weight AS manager_rating
						FROM
							surveys s
							JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						WHERE
							s.subject_reference_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
							AND s.declined_datetime IS NULL
							AND s.survey_type_id = ' . CSurveyType::MANAGER_SURVEY . '
							AND sqa.numeric_weight IS NOT NULL' . $strWhereManagerAdditionalClause . ')	'
							. $strMainSelectClause;
		return fetchData( $strSql, $objDatabase );

	}

}
?>