<?php

class CTaskReferenceType extends CBaseTaskReferenceType {

	const CALL_ID					= 1;
	const PROPERTY_ID				= 5;
	const RECURRING_TASK_ID			= 6;
	const SYSTEM_ERROR_ID			= 7;
	const TEAM_ID					= 8;
	const TRANSACTION_ID			= 9;
	const WEBSITE_ID				= 10;
	const TASK_MEDIUM				= 11;
	const TASK_ISSUE_TYPE_ID		= 15;
	const CONTRACT_TERMINATION		= 16;
	const DIAGNOSTICS_ID			= 17;
	const ZENDESK_SUPPORT			= 18;
	const STANDARD_TASK				= 19;
	const TICKET_NOTIFICATION		= 20;
	const NEW_RELIC_ID				= 22;
	const PCI_TASK					= 23;
	const DUPLICATE_TASK			= 24;
	const DEVOPS_SUPPORT			= 25;
	const DBA_SUPPORT				= 26;
	const ADMIN_TASK				= 27;
	const VENDOR					= 28;
	const SMS_REPLY					= 29;
	const SMS_SDM					= 30;
	const SMS_ARCHITECT				= 31;
	const SMS_DOE					= 32;
	const TECHNICAL_SUBTASK			= 33;
	const QA_REQUIRED				= 34;
	const TASK_CANCELLATION_REASONS = 35;
	const DOMAIN_TRANSFER			= 36;
	const DOMAIN_TERMINATION		= 37;
	const DISCLAIMER				= 38;
	const ENVIRONMENT_TYPES			= 39;
	const REJECT_REASON				= 40;
	const HR_TASK					= 41;
	const TRAINING_TASK				= 44;
	const RECRUITMENT_TASK			= 42;
	const ACCOUNT_TASK				= 43;
	const RCA_FOLLOW_UP_EMAIL		= 45;
	const TICKET_EMAIL_UPDATES		= 46;
	const DUPLICATE_CANCELLATION_REASON = 47;
	const TASK_DEFAULT_REPORT			= 48;
	const STANDARD_TASK_STORY_POINT		= 49;
	const REVIEW_GOALS					= 50;
	const HELP_RESOURCE					= 51;
	const SMS_TPM						= 52;
	const SMS_QAM						= 53;
	const SMS_ADD						= 54;
	const SMS_ALL_DOE					= 55;
	const L1_INDUCTION					= 56;
	const L2_L3_INDUCTION				= 57;
	const SMS_SQM						= 58;

	const NEEDS_RESEARCH				= 59;
	const RESEARCH_COMPLETE				= 60;
	const NEEDS_UX						= 61;
	const UX_COMPLETE					= 62;
	const NEEDS_ARCHITECTURE			= 63;
	const ARCHITECTURE_COMPLETE			= 64;
	const RESIDENT						= 65;

	const SMS_PO						= 66;
	const SMS_SANDY						= 67;
	const CHAT							= 68;
	const TASK_VENDOR					= 69;

	const RESEARCH_NOT_NEEDED           = 70;
	const UX_NOT_NEEDED                 = 71;
	const ARCHITECTURE_NOT_NEEDED       = 72;

	const RELEASE_NOTE					= 73;
	const RELEASE_NOTE_NOT_NEEDED		= 74;

    const FEEDBACK		= 75;

	public static $c_arrmixTaskReferenceTypeIds = [
		self::NEEDS_RESEARCH		    => 'Needs Research',
		self::RESEARCH_COMPLETE		    => 'Research Complete',
		self::NEEDS_UX				    => 'Needs UX',
		self::UX_COMPLETE			    => 'UX Complete',
		self::NEEDS_ARCHITECTURE	    => 'Needs Architecture',
		self::ARCHITECTURE_COMPLETE	    => 'Architecture Complete',
		self::RESEARCH_NOT_NEEDED       => 'Research Not Needed',
		self::UX_NOT_NEEDED             => 'UX Not Needed',
		self::ARCHITECTURE_NOT_NEEDED   => 'Architecture Not Needed'
	];

	public static $c_arrintReferenceTypesForHelpdeskSystem	= [
		self::ADMIN_TASK		=> CUser::ID_ADMIN_HELP_DESK,
		self::ACCOUNT_TASK		=> CUser::ID_ACCOUNT_HELP_DESK,
		self::TRAINING_TASK		=> CUser::ID_TRAINING_HELP_DESK,
		self::RECRUITMENT_TASK	=> CUser::ID_RECRUITMENT_HELP_DESK,
		self::HR_TASK			=> CUser::ID_HR_HELP_DESK
	];

	public static $c_arrintReferenceTypesForCancel	= [
		self::QA_REQUIRED					=> self::QA_REQUIRED,
		self::TASK_CANCELLATION_REASONS		=> self::TASK_CANCELLATION_REASONS,
		self::DUPLICATE_CANCELLATION_REASON => self::DUPLICATE_CANCELLATION_REASON
	];

	public static $c_arrintReferenceTypesForSms = [
		self::SMS_SDM,
		self::SMS_SQM,
		self::SMS_ARCHITECT,
		self::SMS_DOE,
		self::SMS_TPM,
		self::SMS_QAM,
		self::SMS_ADD,
		self::SMS_SANDY,
		self::SMS_PO,
		self::SMS_ALL_DOE
	];

	public static $c_arrintFirstTierReferenceTypesForSms = [
		self::SMS_SDM,
		self::SMS_SQM,
		self::SMS_TPM,
		self::SMS_QAM
	];

	public static $c_arrintReferenceTypesForBugAllowanceReport	= [
		self::SYSTEM_ERROR_ID,
		self::DIAGNOSTICS_ID
	];

	public static $c_arrintTaskReferenceTypes = array(
		'TASK_REFERENCE_TYPE_NEEDS_RESEARCH'		    => self::NEEDS_RESEARCH,
		'TASK_REFERENCE_TYPE_RESEARCH_COMPLETE'		    => self::RESEARCH_COMPLETE,
		'TASK_REFERENCE_TYPE_NEEDS_UX'				    => self::NEEDS_UX,
		'TASK_REFERENCE_TYPE_UX_COMPLETE'			    => self::UX_COMPLETE,
		'TASK_REFERENCE_TYPE_NEEDS_ARCHITECTURE'	    => self::NEEDS_ARCHITECTURE,
		'TASK_REFERENCE_TYPE_ARCHITECTURE_COMPLETE'	    => self::ARCHITECTURE_COMPLETE,
		'TASK_REFERENCE_TYPE_RESEARCH_NOT_NEEDED'       => self::RESEARCH_NOT_NEEDED,
		'TASK_REFERENCE_TYPE_UX_NOT_NEEDED'             => self::UX_NOT_NEEDED,
		'TASK_REFERENCE_TYPE_ARCHITECTURE_NOT_NEEDED'   => self::ARCHITECTURE_NOT_NEEDED
	);

	public static $c_arrstrReferenceTypeNotes = [
		CTaskReferenceType::NEEDS_RESEARCH			=> 'require research.',
		CTaskReferenceType::RESEARCH_COMPLETE		=> 'Research completed.',
		CTaskReferenceType::NEEDS_UX				=> 'require UX.',
		CTaskReferenceType::UX_COMPLETE				=> 'Ux completed.',
		CTaskReferenceType::NEEDS_ARCHITECTURE		=> 'require Architecture.',
		CTaskReferenceType::ARCHITECTURE_COMPLETE	=> 'Architecture completed.',
		CTaskReferenceType::RESEARCH_NOT_NEEDED	    => 'Research no longer required.',
		CTaskReferenceType::UX_NOT_NEEDED	        => 'UX no longer required.',
		CTaskReferenceType::ARCHITECTURE_NOT_NEEDED	=> 'Architecture no longer required.'
	];

	public static $c_arrintReferenceTypeIds = [
		CTaskReferenceType::NEEDS_RESEARCH		    => CTaskReferenceType::RESEARCH_COMPLETE,
		CTaskReferenceType::NEEDS_UX			    => CTaskReferenceType::UX_COMPLETE,
		CTaskReferenceType::NEEDS_ARCHITECTURE	    => CTaskReferenceType::ARCHITECTURE_COMPLETE,
		CTaskReferenceType::RESEARCH_NOT_NEEDED	    => CTaskReferenceType::NEEDS_RESEARCH,
		CTaskReferenceType::UX_NOT_NEEDED	        => CTaskReferenceType::NEEDS_UX,
		CTaskReferenceType::ARCHITECTURE_NOT_NEEDED	=> CTaskReferenceType::NEEDS_ARCHITECTURE
	];

	public static $c_arrintNotNeededReferenceTypeIds = [
		CTaskReferenceType::RESEARCH_NOT_NEEDED,
		CTaskReferenceType::UX_NOT_NEEDED,
		CTaskReferenceType::ARCHITECTURE_NOT_NEEDED
	];
	public static $c_arrstrReferenceTypeGroup = [
		'Research'      => [ CTaskReferenceType::RESEARCH_NOT_NEEDED, CTaskReferenceType::NEEDS_RESEARCH, CTaskReferenceType::RESEARCH_COMPLETE ],
		'UX'            => [ CTaskReferenceType::UX_NOT_NEEDED, CTaskReferenceType::NEEDS_UX, CTaskReferenceType::UX_COMPLETE ],
		'Architecture'  => [ CTaskReferenceType::ARCHITECTURE_NOT_NEEDED, CTaskReferenceType::NEEDS_ARCHITECTURE, CTaskReferenceType::ARCHITECTURE_COMPLETE ]
	];

	public static $c_arrmixStoryReferenceTypes = [
		'Research'     => [
			CTaskReferenceType::RESEARCH_NOT_NEEDED => 'None Needed',
			CTaskReferenceType::NEEDS_RESEARCH      => 'In Progress',
			CTaskReferenceType::RESEARCH_COMPLETE   => 'Complete'
		],
		'UX'           => [
			CTaskReferenceType::UX_NOT_NEEDED => 'None Needed',
			CTaskReferenceType::NEEDS_UX      => 'In Progress',
			CTaskReferenceType::UX_COMPLETE   => 'Complete'
		],
		'Architecture' => [
			CTaskReferenceType::ARCHITECTURE_NOT_NEEDED => 'None Needed',
			CTaskReferenceType::NEEDS_ARCHITECTURE      => 'In Progress',
			CTaskReferenceType::ARCHITECTURE_COMPLETE   => 'Complete'
		]
	];

	public static $c_arrintTaskReferenceTypeIds = [
		CTaskReferenceType::NEEDS_RESEARCH			=> CTaskReferenceType::RESEARCH_COMPLETE,
		CTaskReferenceType::NEEDS_UX				=> CTaskReferenceType::UX_COMPLETE,
		CTaskReferenceType::NEEDS_ARCHITECTURE		=> CTaskReferenceType::ARCHITECTURE_COMPLETE,
		CTaskReferenceType::RESEARCH_COMPLETE		=> 0,
		CTaskReferenceType::UX_COMPLETE				=> 0,
		CTaskReferenceType::ARCHITECTURE_COMPLETE 	=> 0,
		CTaskReferenceType::RESEARCH_NOT_NEEDED		=> CTaskReferenceType::NEEDS_RESEARCH,
		CTaskReferenceType::UX_NOT_NEEDED		    => CTaskReferenceType::NEEDS_UX,
		CTaskReferenceType::ARCHITECTURE_NOT_NEEDED		=> CTaskReferenceType::NEEDS_ARCHITECTURE
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>