<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CWorkflows
 * Do not add any new functions to this class.
 */

class CWorkflows extends CBaseWorkflows {

	public static function fetchWorkflowsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						w.id,
						w.name,
						w.field_name AS data,
						w.updated_on,
						e.preferred_name AS updated_by
					FROM
						workflows w
						JOIN users AS u ON ( u.id = w.updated_by )
						JOIN employees AS e ON ( e.id = u.employee_id )
					WHERE
						w.cid = ' . ( int ) $intCid . '
					ORDER BY
						w.created_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchWorkflowsByCidById( $intCid, $intId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						workflows
					WHERE
						cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $intId;

		return self::fetchWorkflow( $strSql, $objDatabase );
	}

}
?>