<?php

class CTaskDataRequest extends CBaseTaskDataRequest {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCaReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdditionalInfo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestedCompletionDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectedCompletionDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>