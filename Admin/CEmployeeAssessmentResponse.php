<?php

class CEmployeeAssessmentResponse extends CBaseEmployeeAssessmentResponse {

	public static $c_arrintRatingTypes	= array( 1 => 'Poor', 2 => 'Fair', 3 => 'Satisfactory', 4 => 'Good', 5 => 'Excellent' );

	public static $c_arrstrIndiaRatingTypes			= array(
		1 => 'Have drowned.',
		2 => 'On the verge of drowning.',
		3 => 'Learning to swim.',
		4 => 'Swimming, but still underwater.',
		5 => 'On top of the water. but yet to swim in the right direction.',
		6 => 'On top of the water. Swimming in the right direction.',
		7 => 'Learning to walk on water.',
		8 => 'Walking on water. You are just that good.',
		9 => 'Creating water but yet to fly at will.',
		10 => 'Created water and are flying at will. You are God.',
	);

	public static $c_arrstrIndiaExitProcessRatingTypes	= array(
		1 => '1 Star: Far below expectations / needs significant improvement / is on a performance improvement plan / lives none of the values.',
		2 => '2 Star: Below expectations / misses goals and deadlines / performance is discussed / lives few if any of the values.',
		3 => '3 Star: Meets expectations -solid / keeps the plane in the air / Lives some of the values.',
		4 => '4 Star: Exceeds expectations / achieves nearly all goals / room for some/but not a lot of improvement / lives most of the values most of the time.',
		5 => '5 Star: Far exceeds all expectations / achieves all goals / is a role model of perfection / lives all values all the time.',
	);

	public static $c_arrstrUSRatingTypes	= array(
		1 => 'Far below expectations, needs significant improvement; is on a performance improvement plan, lives none of the values.',
		2 => 'Below expectations, misses goals and deadlines, performance is discussed, lives few if any of the values.',
		3 => 'Meets expectations; solid; keeps the plane in the air. Lives some of the values.',
		4 => 'Exceeds expectations, achieves nearly all goals, room for some, but not a lot of improvement, lives most of the values most of the time.',
		5 => 'Far exceeds all expectations, achieves all goals, is a role model of perfection, lives all values all the time.',
	);

	public static $c_arrstrUSEntrataRatingTypes = [
		1  => '',
		2  => '',
		3  => '',
		4  => '',
		5  => '',
		6  => '',
		7  => '',
		8  => '',
		9  => '',
		10 => '',
	];

	protected $m_intIsRating;
	protected $m_intEmployeesRating;

	protected $m_intManagersRating;
	protected $m_intTotalRating;

	protected $m_boolIsMandatory;
	protected $m_boolIsEntrataRating;

	const WORDS_COUNT = 20;

	/**
	 * GET Functions
	 */

	public function getIsRating() {
		return $this->m_intIsRating;
	}

	public function getEmployeesRating() {
		return $this->m_intEmployeesRating;
	}

	public function getManagersRating() {
		return $this->m_intManagersRating;
	}

	public function getTotalRating() {
		return $this->m_intTotalRating;
	}

	public function getDecryptedAnswer() {
		return ( false == is_null( $this->m_strAnswer ) ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strAnswer, CONFIG_SODIUM_KEY_SALARY_NOTES, [ 'legacy_secret_key' => CConfig::get( 'salary_notes_encryption_key' ) ] ) : NULL;
	}

	public function getIsMandatory() {
		return $this->m_boolIsMandatory;
	}

	public function getIsEntrataRating() {
		return $this->m_boolIsEntrataRating;
	}

	/**
	 * SET Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_rating'] ) ) $this->setIsRating( $arrmixValues['is_rating'] );
		if( true == isset( $arrmixValues['employees_rating'] ) ) $this->setEmployeesRating( $arrmixValues['employees_rating'] );
		if( true == isset( $arrmixValues['managers_rating'] ) ) $this->setManagersRating( $arrmixValues['managers_rating'] );
		if( true == isset( $arrmixValues['total_rating'] ) ) $this->setTotalRating( $arrmixValues['total_rating'] );
		if( true == isset( $arrmixValues['is_entrata_rating'] ) ) $this->setIsEntrataRating( $arrmixValues['is_entrata_rating'] );

		return;
	}

	public function setIsMandatory( $boolIsMandatory ) {
		$this->m_boolIsMandatory = $boolIsMandatory;
	}

	public function setIsRating( $intIsRating ) {
		$this->m_intIsRating = $intIsRating;
	}

	public function setEmployeesRating( $intEmployeesRating ) {
		$this->m_intEmployeesRating = $intEmployeesRating;
	}

	public function setManagersRating( $intManagersRating ) {
		$this->m_intManagersRating = $intManagersRating;
	}

	public function setTotalRating( $intTotalRating ) {
		$this->m_intTotalRating = $intTotalRating;
	}

	public function setEncryptedAnswer( $strAnswer ) {
		$this->m_strAnswer = ( true == valStr( $strAnswer ) ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $strAnswer, -1, NULL, true ), CONFIG_SODIUM_KEY_SALARY_NOTES ) : NULL;
	}

	public function setIsEntrataRating( $boolIsEntrataRating ) {
		$this->m_boolIsEntrataRating = $boolIsEntrataRating;
	}

	/**
	 * Validation Functions
	 */

	public function valAnswer( $boolIsVerificationRequired ) {
		$boolIsValid = true;

		$intWordsCount = ( true == $boolIsVerificationRequired ) ? self::WORDS_COUNT : 0;

		if( ( true == $this->getIsEntrataRating() && false == is_numeric( $this->getAnswer() ) ) || ( false == $this->getIsEntrataRating() && 1 == $this->getIsRating() && ( false == is_numeric( $this->getAnswer() ) && 'N/A' != $this->getAnswer() ) ) || ( 0 == $this->getIsRating() && $intWordsCount >= strlen( preg_replace( '/[^a-zA-Z]+/', '', $this->getAnswer() ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer', 'Answer is required.' ) );
		}

		return $boolIsValid;
	}

	public function valComment( $boolIsVerificationRequired ) {

		$boolIsValid = true;

		$intWordsCount = ( true == $boolIsVerificationRequired ) ? self::WORDS_COUNT : 0;

		if( 1 == $this->getIsRating() && true == $this->getIsMandatory() && ( $intWordsCount >= strlen( preg_replace( '/[^a-zA-Z]+/', '', $this->getComments() ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer', 'Comment is required.' ) );
		}

		return $boolIsValid;
	}

	public function valExistingReviewResponse( $objDatabase, $intEmployeeAssessmentEmployeeId ) {

		$boolIsValid = true;

 		$intEmployeeAssessmentResponsesCount = \Psi\Eos\Admin\CEmployeeAssessmentResponses::createService()->fetchEmployeeAssessmentResponsesCountByEmployeeAssessmentEmployeeId( $intEmployeeAssessmentEmployeeId, $objDatabase );

		if( 0 < $intEmployeeAssessmentResponsesCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Reviewer has already finished the review.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $intEmployeeAssessmentEmployeeId = NULL, $boolIsVerificationRequired = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case 'answer':
				$boolIsValid	&= $this->valAnswer( $boolIsVerificationRequired );
				$boolIsValid	&= $this->valComment( $boolIsVerificationRequired );
				break;

			case VALIDATE_DELETE:
				break;

			case 'existing_review':
				$boolIsValid 	&= $this->valExistingReviewResponse( $objDatabase, $intEmployeeAssessmentEmployeeId );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>