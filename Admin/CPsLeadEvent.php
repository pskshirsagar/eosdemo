<?php

class CPsLeadEvent extends CBasePsLeadEvent {

	const SALES_LEAD_GENERATOR = 15;

	protected $m_boolIsSelected;

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );

		}

		if( true == $boolIsValid ) {

			if( true == is_numeric( $this->getName() ) ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name should be alphanumeric.' ) );
			}
		}

		if( true == $boolIsValid ) {

			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$intCount = \Psi\Eos\Admin\CPsLeadEvents::createService()->fetchPsLeadEventCount( ' WHERE deleted_on IS NULL  AND lower( name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'' . $strSqlCondition, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name already exists.' ) );
			}

		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}
		if( true == $boolIsValid ) {

			if( true == is_numeric( $this->getDescription() ) ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description should be alphanumeric.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valYear() {
		$boolIsValid = true;

		if( false == is_null( $this->getYear() ) ) {

			if( false == is_numeric( $this->getYear() ) ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year', 'Year should be numeric.' ) );
				return $boolIsValid;
			}

			if( false == is_numeric( $this->getYear() ) || 4 != strlen( $this->getYear() ) ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year', 'Year should be four digit number.' ) );
			} elseif( 1900 > $this->getYear() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year', 'Year must be greater than 1900.' ) );
			}
		}
		return $boolIsValid;
	}

	public function getYear() {

		if( $this->m_strYear == NULL || false === \Psi\CStringService::singleton()->strpos( $this->m_strYear, '/' ) ) {
			return $this->m_strYear;
		}

		$arrstrDateParts = explode( '/', $this->m_strYear );

		if( \Psi\Libraries\UtilFunctions\count( $arrstrDateParts ) <> 3 ) {
			return $this->m_strYear;
		}

		if( 3 <= \Psi\Libraries\UtilFunctions\count( $arrstrDateParts ) ) {
			return $arrstrDateParts[2];
		}
	}

	public function setYear( $strYear ) {
		$strYear = trim( $strYear );

		if( is_numeric( $strYear ) ) {
			if( $strYear >= 0 && $strYear <= 999 ) $strYear += 2000;
			$strYear = ( $strYear ) ? sprintf( '%02d/%02d/%04d', 1, 1, ( int ) $strYear ) : NULL;
		}

		$this->m_strYear = CStrings::strTrimDef( $strYear, -1, NULL, true );
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valYear();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( '  NOW() ' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

}
?>