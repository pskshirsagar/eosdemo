<?php

class CPropertyProductUsage extends CBasePropertyProductUsage {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// here goes the default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>