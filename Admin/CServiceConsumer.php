<?php

class CServiceConsumer extends CBaseServiceConsumer {

	const EXTERNAL_CLIENT                = 1;
	const INTERNAL_CLIENT                = 2;
	const THIRD_PARTY_APP                = 3;
	const INTERNAL_NETWORK               = 4;
	const SITE_TABLET_IOS                = 5;
	const SITE_TABLET_ANDROID            = 6;
	const RESIDENT_PORTAL_MOBILE         = 7;
	const RESIDENT_PORTAL_MOBILE_IOS     = 8;
	const RESIDENT_PORTAL_MOBILE_ANDROID = 9;
	const DOC_SCAN                       = 10;
	const LEAD_ALERT                     = 11;
	const DOC_SCAN_WINDOWS               = 12;
	const ENTRATA_MAINTENANCE_ANDROID    = 13;
	const RESIDENTINSURE_WEB             = 14;
	const LEAD_ALERT_WINDOWS             = 15;
	const ENTRATA_MAINTENANCE_IOS        = 16;
	const ENTRATA_PRICING_WEB            = 17;

	const SYSTEM_CODE_EXTERNAL_CLIENT                = 'external';
	const SYSTEM_CODE_INTERNAL_CLIENT                = 'internal';
	const SYSTEM_CODE_THIRD_PARTY_APP                = 'tpa';
	const SYSTEM_CODE_INTERNAL_NETWORK               = 'internal-ntw';
	const SYSTEM_CODE_SITE_TABLET_IOS                = 'st-ios';
	const SYSTEM_CODE_SITE_TABLET_ANDROID            = 'st-android';
	const SYSTEM_CODE_RESIDENT_PORTAL_MOBILE         = 'rpm';
	const SYSTEM_CODE_RESIDENT_PORTAL_MOBILE_IOS     = 'rpm-ios';
	const SYSTEM_CODE_RESIDENT_PORTAL_MOBILE_ANDROID = 'rpm-android';
	const SYSTEM_CODE_DOC_SCAN                       = 'docscan';
	const SYSTEM_CODE_LEAD_ALERT                     = 'leadalert';
	const SYSTEM_CODE_DOC_SCAN_WINDOWS               = 'docscan-win';
	const SYSTEM_CODE_ENTRATA_MAINTENANCE_ANDROID    = 'em-android';
	const SYSTEM_CODE_RESIDENTINSURE_WEB             = 'rinsure-web';
	const SYSTEM_CODE_LEAD_ALERT_WINDOWS             = 'leadalert-win';
	const SYSTEM_CODE_ENTRATA_MAINTENANCE_IOS        = 'em-ios';
	const SYSTEM_CODE_ENTRATA_PRICING_WEB            = 'ep-web';
	const SYSTEM_CODE_RP_WHITE_LABELED_APP_ANDROID   = 'rpm-wl-android';
	const SYSTEM_CODE_RP_WHITE_LABELED_APP_IOS       = 'rpm-wl-ios';
	const SYSTEM_CODE_RESIDENT_PORTAL_WEB            = 'rpweb';
	const SYSTEM_CODE_RESERVATION_HUB                = 'rhweb';
	const SYSTEM_CODE_TENANT_PORTAL                  = 'tpweb';

	public static $c_arrstrSystemCodeServiceConsumers = [
		self::EXTERNAL_CLIENT                => self::SYSTEM_CODE_EXTERNAL_CLIENT,
		self::INTERNAL_CLIENT                => self::SYSTEM_CODE_INTERNAL_CLIENT,
		self::THIRD_PARTY_APP                => self::SYSTEM_CODE_THIRD_PARTY_APP,
		self::INTERNAL_NETWORK               => self::SYSTEM_CODE_INTERNAL_NETWORK,
		self::SITE_TABLET_IOS                => self::SYSTEM_CODE_SITE_TABLET_IOS,
		self::SITE_TABLET_ANDROID            => self::SYSTEM_CODE_SITE_TABLET_ANDROID,
		self::RESIDENT_PORTAL_MOBILE         => self::SYSTEM_CODE_RESIDENT_PORTAL_MOBILE,
		self::RESIDENT_PORTAL_MOBILE_IOS     => self::SYSTEM_CODE_RESIDENT_PORTAL_MOBILE_IOS,
		self::RESIDENT_PORTAL_MOBILE_ANDROID => self::SYSTEM_CODE_RESIDENT_PORTAL_MOBILE_ANDROID,
		self::DOC_SCAN                       => self::SYSTEM_CODE_DOC_SCAN,
		self::LEAD_ALERT                     => self::SYSTEM_CODE_LEAD_ALERT,
		self::DOC_SCAN_WINDOWS               => self::SYSTEM_CODE_DOC_SCAN_WINDOWS,
		self::ENTRATA_MAINTENANCE_ANDROID    => self::SYSTEM_CODE_ENTRATA_MAINTENANCE_ANDROID,
		self::RESIDENTINSURE_WEB             => self::SYSTEM_CODE_RESIDENTINSURE_WEB,
		self::LEAD_ALERT_WINDOWS             => self::SYSTEM_CODE_LEAD_ALERT_WINDOWS,
		self::ENTRATA_MAINTENANCE_IOS        => self::SYSTEM_CODE_ENTRATA_MAINTENANCE_IOS,
		self::ENTRATA_PRICING_WEB            => self::SYSTEM_CODE_ENTRATA_PRICING_WEB
	];

	public static $c_arrstrMobileAppServiceConsumers = [
		self::SYSTEM_CODE_RESIDENT_PORTAL_MOBILE_ANDROID,
		self::SYSTEM_CODE_RP_WHITE_LABELED_APP_ANDROID,
		self::SYSTEM_CODE_RESIDENT_PORTAL_MOBILE_IOS,
		self::SYSTEM_CODE_RP_WHITE_LABELED_APP_IOS
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>