<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserSettings
 * Do not add any new functions to this class.
 */

class CUserSettings extends CBaseUserSettings {

	// For Email check

	public static function fetchUserIdByStatusAndKey( $objAdminDatabase, $strTaskTypePriority ) {
		$strSql = 'select user_id from user_settings where status = 1 AND key = \'' . $strTaskTypePriority . '\'';
		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchUserAllSettingsByUserIdKey( $intUserId, $objDatabase, $key ) {

		return parent::fetchUserSettings( sprintf( 'SELECT * FROM user_settings WHERE user_id = %d AND key = \'%s\'', ( int ) $intUserId, $key ), $objDatabase );
	}

	// For view my profile page

	public static function fetchUserSettingsHavingActiveStatusByUserId( $objAdminDatabase, $userId = NULL ) {

		$strSql = 'SELECT key FROM user_settings WHERE status = 1 AND user_id = ' . ( int ) $userId;
		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchUserSettingsForBugUrgentAndImmediateByUserId( $intUserId, $arrStrKeys, $objDatabase ) {

		$strSql = ' SELECT
					*
					FROM
						user_settings
					WHERE
					user_id = ' . ( int ) $intUserId . '
					AND Key IN ( \'' . implode( '\' , \'', $arrStrKeys ) . '\' )
					AND Status = 1';
		return parent::fetchUserSettings( $strSql, $objDatabase );
	}

}
?>