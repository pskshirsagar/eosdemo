<?php

class CTraining extends CBaseTraining {

	const INDUCTION = 56;

	protected $m_strNameFull;
	protected $m_strDeprtmentName;
	protected $m_intSessionId;
	protected $m_strTrainingParentName;

	/**
	 * Get Functions
	 *
	 */

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getSessionId() {
		return $this->m_intSessionId;
	}

	public function getDepartmentName() {
		return $this->m_strDeprtmentName;
	}

	public function getTrainingParentName() {
		return $this->m_strTrainingParentName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_full'] ) )	$this->setNameFull( $arrmixValues['name_full'] );
		if( true == isset( $arrmixValues['department_name'] ) )	$this->setDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['session_id'] ) )	$this->setSessionId( $arrmixValues['session_id'] );
		if( true == isset( $arrmixValues['training_parent_name'] ) )	$this->setTrainingParentName( $arrmixValues['training_parent_name'] );
		return;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDeprtmentName = $strDepartmentName;
	}

	public function setSessionId( $intSessionId ) {
		$this->m_intSessionId = $intSessionId;
	}

	public function setTrainingParentName( $strTrainingParentName ) {
		$this->m_strTrainingParentName = $strTrainingParentName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valName( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' Training name is required.' ) );
		}

		if( true == $boolIsValid && true == isset( $objDatabase ) ) {
			$strSql = ' WHERE deleted_by IS NULL AND LOWER(name) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->m_strName ) ) . '\' AND id <> ' . ( int ) $this->m_intId;
			$intCount = CTrainings::fetchRowCount( $strSql, 'trainings', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' Training name " ' . $this->m_strName . ' " already exists.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// no validation avaliable for delete action
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Delete Functions
	 *
	 */

	public function delete( $intCurrenUserId, $objDataBase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrenUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intCurrenUserId );
		$this->setUpdatedOn( 'NOW()' );

		if( $this->update( $intCurrenUserId, $objDataBase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

}
?>