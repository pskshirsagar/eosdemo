<?php

class CPsProductCompetitor extends CBasePsProductCompetitor {

	public function valPsProductId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompetitorId() {
		$boolIsValid = true;

	   	if( true == is_null( $this->getCompetitorId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'competitor_id', 'Competior id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valProductName( $arrobjPsProducts ) {
		$boolIsValid = true;

		if( true == is_null( $this->getProductName() ) ) {
			$boolIsValid = false;
			$strMessage = '';
			if( true == valArr( $arrobjPsProducts ) ) {
			 	$strMessage = ' for ' . $arrobjPsProducts[$this->getPsProductId()]->getName();
 			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'product_name', 'Product name is required.' . $strMessage ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrobjPsProducts = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
							// $boolIsValid &= $this->valProductName( $arrobjPsProducts );
							$boolIsValid &= $this->valCompetitorId();
							$boolIsValid &= $this->valPsProductId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}
}
?>