<?php

class CEmployeeCallStat extends CBaseEmployeeCallStat {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepartmentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneFirstPinIn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneLastPinOut() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnansweredCalls() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAnsweredCalls() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVoicemailsReceived() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneIngoreCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneRejectCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneWrapUpMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneWaitingMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneRingingMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneTalkingMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneTotalActiveMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneRemovedMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNewMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneBreakMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneMiscMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneLunchMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneProjectMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneMeetingMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneTrainingMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneCoachingMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneEmailQueueMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneChatMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneTotalMins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>