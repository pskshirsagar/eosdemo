<?php

class CContractDetail extends CBaseContractDetail {

	const NEW_CONTRACT_WEIGHT_SCORE = 3;

	public function valContractId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getContractId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_id', 'Contract id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContractTermMonths() {
		$boolIsValid = true;

		if( true == is_null( $this->getContractTermMonths() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_term_months', 'Contract term months is required.' ) );
		} elseif( false == is_numeric( $this->m_intContractTermMonths ) || 0 >= $this->m_intContractTermMonths ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_term_months', 'Valid contract term months required.' ) );
		}

		return $boolIsValid;
	}

	public function valContingentAcv() {
		$boolIsValid = true;

		if( 0 > $this->getContingentAcv() ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contingent_acv', ' Contingent ACV should not be less than 0.' ) );
		}

		return $boolIsValid;
	}

	public function valLostAcv() {
		$boolIsValid = true;

		if( 0 > $this->getLostAcv() ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lost_acv', ' Lost ACV should not be less than 0.' ) );
		}

		return $boolIsValid;
	}

	public function valWonAcv() {
		$boolIsValid = true;

		if( 0 > $this->getLostAcv() ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'won_acv', ' Won ACV should not be less than 0.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valContractId();
				$boolIsValid &= $this->valContractTermMonths();
				$boolIsValid &= $this->valContingentAcv();
				$boolIsValid &= $this->valLostAcv();
				$boolIsValid &= $this->valWonAcv();
				break;

			case VALIDATE_DELETE:
				break;

			case 'contract_renew':
				$boolIsValid &= $this->valContractTermMonths();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>