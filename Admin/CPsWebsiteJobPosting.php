<?php

class CPsWebsiteJobPosting extends CBasePsWebsiteJobPosting {

	const EXTERNAL_TRAINER			= 368;
	const DEFAULT_QUESTIONS_USA		= 515;
	const OTHER						= 556;
	const TRAINEE_PHP_DEVELOPER		= 203;
	const QUALITY_ASSURANCE_ANALYST	= 150;
	const C_SHARP_DEVELOPERS		= 151;
	const HR_GENERALIST_BU			= 201;

	const PHP_JED					= 152;
	const PHP_SAMURAI				= 166;
	const PHP_DEVELOPERS			= 167;
	const PHP_JR_DEVELOPERS			= 168;
	const PHP_TRAINEE				= 203;
	const PHP_NINJA					= 352;
	const PHP_SEO					= 583;
	const IT_RECRUITER   			= 187;

	protected $m_objPosition;
	protected $m_boolHiringManager;
	protected $m_boolReportingManager;
	protected $m_strIconType;
	protected $m_strGroupName;

	public static $c_arrstrPHPDeveloperSkills = array(
		1	=> 'Database',
		2	=> 'Operating System',
		3	=> 'Data Structure',
		4	=> 'OOPs',
		5	=> 'Networking',
		6	=> 'WebSecurity',
		7	=> 'Programming Languages'
	);

	public static $c_arrstrRefereeTypes = array(
		1	=> 'Current Employee',
		2	=> 'Past Employee',
		3	=> 'Other'
	);

	public static $c_arrstrQualityAssuranceAnalystSkills = array(
		1	=> 'Quality Analysis',
		2	=> 'Automation testing knowledge',
		3	=> 'Testing strategies',
		4	=> 'Test Case',
		5	=> 'Test Plan',
		6	=> 'Bug Tracking',
		7	=> 'Database knowledge',
		8	=> 'Web services',
		9	=> 'Logic',
		10	=> 'Analysis and comprehension ablities',
		11	=> 'General Technical knowledge for the position',
		10	=> 'Attitude',
		11	=> 'Communication'
	);

	public static $c_arrstrCSharpDeveloperSkills = array(
		1	=> 'ASP.net(3.5)',
		2	=> 'ASP.net(2.0)',
		3	=> 'VB.Net',
		4	=> 'SQL Server (2000/2005)',
		5	=> 'HTML',
		6	=> 'CSS',
		7	=> 'Ado.Net',
		8	=> 'C,C++,C#',
		9	=> 'Javascript',
		10	=> 'AJAX',
		11	=> 'XML'
	);

	public static $c_arrintPHPJobPostingIds = array(
		self::PHP_JED,
		self::PHP_SAMURAI,
		self::PHP_DEVELOPERS,
		self::PHP_JR_DEVELOPERS,
		self::PHP_TRAINEE,
		self::PHP_NINJA
	);

	/**
	 * Get Functions
	 *
	 */

	public function getIsReportingManger() {
		return $this->m_boolReportingManager;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setIsReportingManger( $boolReportingManager ) {
		$this->m_boolReportingManager = CStrings::strToBool( $boolReportingManager );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_reporting_manager'] ) )	$this->setIsReportingManger( $arrmixValues['is_reporting_manager'] );
		if( true == isset( $arrmixValues['icon_type'] ) )	$this->set( 'm_strIconType', trim( stripcslashes( $arrmixValues['icon_type'] ) ) );
		if( true == isset( $arrmixValues['group_name'] ) )	$this->set( 'm_strGroupName', trim( stripcslashes( $arrmixValues['group_name'] ) ) );
		return;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createEmployeeApplication() {

		$objEmployeeApplication = new CEmployeeApplication();

		$objEmployeeApplication->setEmployeeApplicationStatusTypeId( CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_NEW );
		$objEmployeeApplication->setAppliedOn( 'NOW()' );
		$objEmployeeApplication->setSubmissionDatetime( 'NOW()' );

		return $objEmployeeApplication;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchEmploymentApplicationsByDepartmentIdByDepartmentIdNull( $objDatabase ) {
		return CEmploymentApplications::fetchEmploymentApplicationsByDepartmentIdByDepartmentIdNull( $this->getDepartmentId(), $objDatabase );
	}

	public function fetchEmploymentApplicationQuestions( $objDatabase ) {
		return CEmploymentApplicationQuestions::fetchEmploymentApplicationQuestionsByEmploymentApplicationId( $this->getEmploymentApplicationId(), $objDatabase );
	}

	/** Setting getter Setter methods */
	public function setIconType( $strIconType ) {
		$this->set( 'm_strIconType', CStrings::strTrimDef( $strIconType, 10, NULL, true ) );
	}

	public function getIconType() {
		return $this->m_strIconType;
	}

	public function setGroupName( $strGroupName ) {
		$this->set( 'm_strGroupName', CStrings::strTrimDef( $strGroupName, 50, NULL, true ) );
	}

	public function getGroupName() {
		return $this->m_strGroupName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valEmploymentApplicationId() {
		$boolValid = true;

		if( true == is_null( $this->getEmploymentApplicationId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employment_application_id', 'Employment application is required. ' ) );
		}

		return $boolValid;

	}

	public function valName( $objDatabase = NULL ) {
		$boolValid = true;
		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getName() != $this->getName() ) {
			$boolValid = false;
		}

		if( false == $this->getIsReportingManger() && true == is_null( $this->getName() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required. ' ) );
		}

		if( true === valObj( $objDatabase, 'CDatabase' ) ) {
			$arrmixPositions = \Psi\Eos\Admin\CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingsByFilter( [ 'id' => $this->getId(), 'name' => $this->getName(), 'designation_id' => $this->getDesignationId(), 'department_id' => $this->getDepartmentId() ], $objDatabase );

			if( true === valArr( $arrmixPositions ) ) {
				foreach( $arrmixPositions as $arrmixPosition ) {
					if( true === is_null( $arrmixPosition['deleted_on'] ) ) {
						$boolValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Position name already exists.' ) );
						break;
					}
				}
			}
		}

		return $boolValid;
	}

	public function valDesignationId( $objDatabase = NULL ) {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getDesignationId() != $this->getDesignationId() ) {
			$boolValid = false;
		}

		if( true == $boolValid && false == valId( $this->getId() ) && true === valObj( $objDatabase, 'CDatabase' ) && true == valId( $this->getDepartmentId() ) ) {
			$arrintDesignationIds = array_keys( rekeyObjects( 'DesignationId', ( array ) \Psi\Eos\Admin\CDesignations::createService()->fetchAllDesignationsByCountryCodeByDepartmentId( $objDatabase, $this->getCountryCode(), $this->getDepartmentId() ) ) );
			$boolValid = ( true === valArr( $arrintDesignationIds ) && true == valId( $this->getDesignationId() ) && false == in_array( $this->getDesignationId(), $arrintDesignationIds ) ) || ( true === valArr( $arrintDesignationIds ) && false == valId( $this->getDesignationId() ) ) ? false : true;
		}

		if( true == $boolValid && false == valId( $this->getDepartmentId() ) && false == valId( $this->getDesignationId() ) ) {
			$boolValid = false;
		}

		if( false == $boolValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'designation_id', 'Designation is required. ' ) );
		}

		return $boolValid;
	}

	public function valDepartmentId( $boolPositionTab = false, $objDatabase = NULL ) {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getDepartmentId() != $this->getDepartmentId() ) {
			$boolValid = false;
		}

		if( false == $this->getIsReportingManger() && true == is_null( $this->getDepartmentId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_id', 'Department is required. ' ) );
		}

		if( false == $this->getIsReportingManger() && true == valObj( $objDatabase, 'CDatabase' ) && true == $boolPositionTab ) {
			$objDesignation = CDesignations::fetchDesignationById( $this->getDesignationId(), $objDatabase );

			if( true == valObj( $objDesignation, 'CDesignation' ) && $objDesignation->getDepartmentId() != $this->getDepartmentId() ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_id', 'Department not matching with selected designation\'s department.' ) );
			}
		}

		return $boolValid;
	}

	public function valAssessmentUrl() {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getAssessmentUrl() != $this->getAssessmentUrl() ) {
			$boolValid = false;
		}

		if( false == $this->getIsReportingManger() && true == is_null( $this->getAssessmentUrl() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'assessment_url', 'Assessment URL is required. ' ) );
		} else if( false == $this->getIsReportingManger() && false == preg_match( '/^((https?:\/\/)[a-zA-Z0-9]+\.[a-zA-Z0-9]+)[-a-z0-9+&@#\/%?=_:.]*/', $this->getAssessmentUrl() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'assessment_url', 'Please enter valid Test URL.' ) );
		}

		return $boolValid;

	}

	public function valEmployeeApplicationScoreType() {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getEmployeeApplicationScoreTypeId() != $this->getEmployeeApplicationScoreTypeId() ) {
			$boolValid = false;
		}

		if( false == $this->getIsReportingManger() && true == is_null( $this->getEmployeeApplicationScoreTypeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_application_score_type_id', 'Test is required. ' ) );
		}

		return $boolValid;
	}

	public function valPublishedOn() {
		$boolValid = true;
		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getPublishedOn() != $this->getPublishedOn() ) {
			$boolValid = false;
		}

		if( false == $this->getIsReportingManger() && true == valId( $this->getIsPublished() ) && true == is_null( $this->getPublishedOn() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Post Publish length is required. ' ) );
		}

		return $boolValid;
	}

	public function valDescription() {
		$boolValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required. ' ) );
		}

		return $boolValid;
	}

	public function valDeactivateOn() {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getCity() != $this->getCity() ) {
			$boolValid = false;
		}

		if( false == $this->getIsReportingManger() && false == is_null( $this->getDeactivateOn() ) && false == CValidation::validateDate( $this->getDeactivateOn() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Deactivation date is not a valid date.' ) );
		}

		return $boolValid;
	}

	public function valRawScoreCutoff() {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getRawScoreCutoff() != $this->getRawScoreCutoff() ) {
			$boolValid = false;
		}

		if( false == $this->getIsReportingManger() && true == empty( $this->m_intRawScoreCutoff ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'raw_score_cutoff', 'Raw score is required.' ) );
		}

		return $boolValid;
	}

	public function valHiringManagerEmployeeId() {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getHiringManagerEmployeeId() != $this->getHiringManagerEmployeeId() ) {
			$boolValid = false;
		}

		return $boolValid;
	}

	public function valMrabCutOff() {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getMrabCutOff() != $this->getMrabCutOff() ) {
			$boolValid = false;
		}

		return $boolValid;
	}

	public function valCity() {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getCity() != $this->getCity() ) {
			$boolValid = false;
		}

		return $boolValid;
	}

	public function valStateCode() {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getCity() != $this->getCity() ) {
			$boolValid = false;
		}

		return $boolValid;
	}

	public function valCountryCode() {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getCity() != $this->getCity() ) {
			$boolValid = false;
		}

		return $boolValid;
	}

	public function valIsPublished() {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getCity() != $this->getCity() ) {
			$boolValid = false;
		}

		return $boolValid;
	}

	public function valExperience( $objDatabase = NULL ) {
		$boolValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && ( $this->m_objPosition->getDetails()->min_exp != $this->getDetails()->min_exp || $this->m_objPosition->getDetails()->max_exp != $this->getDetails()->max_exp ) ) {
			$boolValid = false;
		}

		if( true == $boolValid && false == valId( $this->getId() ) && true === valObj( $objDatabase, 'CDatabase' ) && true == valId( $this->getDepartmentId() ) ) {
			$arrintDesignationIds = array_keys( rekeyObjects( 'DesignationId', ( array ) \Psi\Eos\Admin\CDesignations::createService()->fetchAllDesignationsByCountryCodeByDepartmentId( $objDatabase, $this->getCountryCode(), $this->getDepartmentId() ) ) );
			if( true === valArr( $arrintDesignationIds ) ) {
				if( true == valId( $this->getDesignationId() ) ) {
					$objDesignation = \Psi\Eos\Admin\CDesignations::createService()->fetchDesignationById( $this->getDesignationId(), $objDatabase );
					if( $this->getDetailsField( 'min_exp' ) != $objDesignation->getMinExperience() || $this->getDetailsField( 'max_exp' ) != $objDesignation->getMaxExperience() ) {
						$boolValid = false;
					}
				} else {
					$boolValid = false;
				}
			}
		}

		if( true == $boolValid && false == valId( $this->getDepartmentId() ) && false == valId( $this->getDesignationId() ) ) {
			$boolValid = false;
		}

		if( false == $boolValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', 'Experience is required.' ) );
		}
		return $boolValid;
	}

	public function valReferralBonusAmount() {
		if( false == is_numeric( $this->getReferralBonusAmount() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referral_bonus_amount', 'Referral Bonus Amount is required.' ) );
			return false;
		}

		return true;
	}

	public function valEducationalAndAdditionalDetails() {
		$boolValid = true;

		if( true == $this->getIsReportingManger() && true == valId( $this->getIsPublished() ) ) {
			if( false == valStr( $this->getDetailsField( 'educational_details' ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', 'Education is required.' ) );
				$boolValid = false;
			}

			if( false == valStr( $this->getDetailsField( 'additional_details' ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', 'Additional details is required.' ) );
				$boolValid = false;
			}
		}

		return $boolValid;
	}

	public function valTechnologyId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == valObj( $this->m_objPosition, 'CPsWebsiteJobPosting' ) && true == $this->getIsReportingManger() && $this->m_objPosition->getTechnologyId() != $this->getTechnologyId() ) {
			$boolIsValid = false;
		}
		if( true == $boolIsValid && false == valId( $this->getId() ) && true === valObj( $objDatabase, 'CDatabase' ) && true == valId( $this->getDepartmentId() ) ) {
			$arrintTechnologyIds = array_keys( rekeyObjects( 'Id', ( array ) \Psi\Eos\Admin\CTechnologies::createService()->fetchPublishedTechnologiesByDepartmentId( $this->getDepartmentId(), $objDatabase ) ) );
			$boolIsValid = ( true === valArr( $arrintTechnologyIds ) && true == valId( $this->getTechnologyId() ) && false == in_array( $this->getTechnologyId(), $arrintTechnologyIds ) ) || ( true === valArr( $arrintTechnologyIds ) && false == valId( $this->getTechnologyId() ) ) ? false : true;
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'technology_id', 'Technology is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valIsApproved() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolReportingManager = false ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolValid &= $this->valDepartmentId();

				if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
					$boolValid &= $this->valDesignationId( $objDatabase );
					$boolValid &= $this->valTechnologyId( $objDatabase );
				}

				$boolValid &= $this->valName();
				$boolValid &= $this->valDescription();
				$boolValid &= $this->valEmploymentApplicationId();
				$boolValid &= $this->valDeactivateOn();

				if( CCountry::CODE_INDIA == $this->getCountryCode() ) {
					$boolValid &= $this->valRawScoreCutoff();
					$boolValid &= $this->valExperience( $objDatabase );
					$boolValid &= $this->valReferralBonusAmount();
				}

				if( CCountry::CODE_USA == $this->getCountryCode() ) {
					$boolValid &= $this->valEmployeeApplicationScoreType();
				}

				if( ( CCountry::CODE_INDIA == $this->getCountryCode() && ( true == $this->getIsPublished() || true == $this->getIsPublishInternal() || true == $this->getAutoSendMrab() ) ) || CCountry::CODE_USA == $this->getCountryCode() ) {
					$boolValid &= $this->valAssessmentUrl();
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_name':
				$boolValid &= $this->valName();
				break;

			case 'insert_update_position':
				if( false == $boolReportingManager ) {
					if( true == valId( $this->getId() ) ) {
						$this->m_objPosition = \Psi\Eos\Admin\CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $this->getId(), $objDatabase );
					}

					$boolValid &= $this->valName( $objDatabase );
					$boolValid &= $this->valDepartmentId( true, $objDatabase );
					$boolValid &= $this->valAssessmentUrl();
					$boolValid &= $this->valEmployeeApplicationScoreType();
					$boolValid &= $this->valPublishedOn();
					$boolValid &= $this->valDescription();
				}
				break;

			case 'validate_assessment_url':
				$boolValid &= $this->valAssessmentUrl();
				break;

			default:
				// default case
				$boolValid = true;
				break;
		}

		return $boolValid;
	}

	/**
	 * Insert/Update Functions
	 *
	 */

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		return parent::insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>
