<?php

class CDealDeskMeetingAttendee extends CBaseDealDeskMeetingAttendee {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDealDeskMeetingId() {
		$boolIsValid = true;

		if( false == \valId( $this->m_intDealDeskMeetingId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deal_desk_meeting_id', 'Deal Desk Meeting is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAttendeeId() {
		$boolIsValid = true;

		if( false == \valId( $this->m_intAttendeeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'attendee_id', 'Attendee is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valDealDeskMeetingId();
				$boolIsValid &= $this->valAttendeeId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>