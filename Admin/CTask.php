<?php
use Psi\Libraries\UtilValidation\CValidation;
use Psi\Libraries\Container\TContainerized;
use Psi\Eos\Admin\CPsProducts;
use Psi\Eos\Admin\CPsProductOptions;
use Psi\Eos\Admin\CPsProductEmployees;
use Psi\Eos\Admin\CTaskNotes;
use Psi\Eos\Admin\CTaskCompanies;
use Psi\Eos\Admin\CTaskReleases;
use function Psi\Libraries\UtilFunctions\generateRandomId;

class CTask extends CBaseTask {

	use TContainerized;
	const WORKING_HOUR				= 12;
	const FOLLOW_UP_START_TIME		= 6;
	const FOLLOW_UP_END_TIME		= 18;
	const ONE_DAY_HOUR				= 24;
	const MINUTE_SEGMENT			= 5;
	const HALF_MINUTE_SEGMENT		= 3;
	const DAY_OF_WEEK_SATURDAY		= 6;
	const DAY_OF_WEEK_SUNDAY		= 7;
	const RECEIVED_SMS_COUNT_THREE 	= 3;
	const DOMAIN_URI_PREFIX			= 'https://smsticket.page.link';
	const FIREBASE_SHORTLINK_URI	= 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=';
	const RW_USER_ID				= 10;
	const WEEK_DAYS_COUNT			= 7;
	const NEED_RELEASE_NOTE						= 1;
	const NEED_APPROVAL							= 2;
	const NEED_UPDATE							= 3;
	const APPROVED								= 4;

	const BUGS_FILTER_DIAGNOSTICS_BUGS			= 17;
	const BUGS_FILTER_SYSTEM_BUGS				= 7;
	const BUGS_FILTER_CLIENT_FACING_BUGS		= 1;
	const BUGS_FILTER_CLIENT_NON_FACING_BUGS	= 0;
	const DEVELOPER 							= 'Developer';
	const QA  									= 'QA';
	const CRS									= 'CRS';

	protected $m_objSubTask;
	protected $m_objTaskDetail;
	protected $m_objObjectStorageGateway;

	protected $m_arrobjTasks;
	protected $m_arrobjTaskNotes;
	protected $m_arrobjTaskWatchlists;
	protected $m_arrobjTaskAttachments;
	protected $m_arrobjTaskInlineAttachments;
	protected $m_arrobjSMSTaskNotes;

	protected $m_arrstrTaskClientsData;

	protected $m_strContact;
	protected $m_strPsProductName;
	protected $m_strPsProductOptionName;
	protected $m_strPropertyName;
	protected $m_strHtmlEmailOutput;
	protected $m_strAsssignedToEmail;
	protected $m_strLastClientUpdatedOn;
	protected $m_strTaskCompaniesPostDatetime;
	protected $m_strTaskCompanyNames;
	protected $m_strStepToDuplicate;
	protected $m_strClientHtmlTaskContent;
	protected $m_strTaskLastUpdatedOn;
	protected $m_strTaskTypeName;
	protected $m_strTaskPriorityName;
	protected $m_strTaskStatusName;
	protected $m_strTaskImpact;
	protected $m_strEmployeeFullName;
	protected $m_strEmployeeNameFirst;
	protected $m_strEmployeeNameLast;
	protected $m_strManagerName;
	protected $m_strTeamName;
	protected $m_strTaskReleaseDatetime;
	protected $m_strTaskStatus;
	protected $m_strTaskPriority;
	protected $m_strTaskType;
	protected $m_strTaskAssignedTo;
	protected $m_strTaskManager;
	protected $m_strRootCauseDescription;
	protected $m_strRootCauseName;
	protected $m_strResolutionTime;
	protected $m_strClientName;
	protected $m_strTaskReleaseName;
	protected $m_strClusterName;
	protected $m_strProjectManagerName;
	protected $m_strSecureBaseName;
	protected $m_strReleaseUpdatedStatusId;
	protected $m_strUpdatedReleaseId;
	protected $m_strPendingTaskLastUpdatedOn;
	protected $m_strJsonTaskData;
	protected $m_strEmailSummaryId;
	protected $m_strQamName;
	protected $m_strQuarterName;
	protected $m_strTaskTagName;
	protected $m_strNameFull;
	protected $m_strOfficeDeskNumber;
	protected $m_strTestCaseTaskIds;
	protected $m_strCodeReviewerName;
	protected $m_strTaskReferenceName;
	protected $m_strRwxDomain;

	protected $m_intId;
	protected $m_fltSeverityCount;
	protected $m_intTeamId;
	protected $m_intAssignedTasks;
	protected $m_intCompletedTasks;
	protected $m_intMonth;
	protected $m_intLiveSupportChatId;
	protected $m_intTasksCount;
	protected $m_intTaskVoteCount;
	protected $m_intDevelopmentTasksCount;
	protected $m_intShowUserWatchlist;
	protected $m_intSendMailSalesRep;
	protected $m_intSendMailManager;
	protected $m_intAttachmentsCount;
	protected $m_intSupportFeeAccountId;
	protected $m_intTaskAssignmentsTaskStatusId;
	protected $m_intIsKeyClient;
	protected $m_intEmployeeId;
	protected $m_intBugTaskCount;
	protected $m_intFeatureTaskCount;
	protected $m_intOtherTaskCount;
	protected $m_intTeamMembersCount;
	protected $m_intQaStoryPoints;
	protected $m_intDevStoryPoints;
	protected $m_intKeyClients;
	protected $m_intTaskCategoryId;
	protected $m_intTaskNoteId;
	protected $m_intPsLeadId;
	protected $m_intTaskQuarterId;
	protected $m_intParentTaskId;
	protected $m_intReferenceNumber;
	protected $m_intTaskReferenceTypeId;
	protected $m_intTaskIssueTypeId;
	protected $m_intSubTaskTypeUserId;
	protected $m_intClientRiskStatus;
	protected $m_intKeyClientRank;
	protected $m_intResolvedOn;
	protected $m_intDeveloperEmployeeId;
	protected $m_intCodeReviewerEmployeeId;
	protected $m_intAssociatedPropertiesCount = 0;
	protected $m_intStoryOrderNum;

	protected $m_boolIsDescriptionRequired;
	protected $m_boolIsStepToDuplicateRequired;
	protected $m_boolIsPsProductNotRequired;
	protected $m_boolIsTaskParentIdNotRequired;
	protected $m_boolIsSharedWithLimitedClients;
	protected $m_boolIsRestored;
	protected $m_boolIsAddTask;
	protected $m_boolSendEmailToClients = true;
	protected $m_boolAutomationFieldRequired;
	protected $m_boolIsSdm;
	protected $m_boolIsQaApproved;
	protected $m_boolIsSqmApproved;
	protected $m_boolIsQAFileOwner;
	protected $m_boolYearly;

	protected $m_fltSupportFeeAmountPerMinute;
	protected $m_fltTaskStoryPoints;
	protected $m_fltStoryPoints;
	protected $m_fltAverageStoryPoints;

	protected $m_arrintTaskNoteCids 	= array();
	protected $m_arrintTicketAndTaskWashlists 			= array();
	protected $m_arrintSendEmailToCids 	= array();
	protected $m_arrintNewTaskWatchlistsUserIds			= array();
	protected $m_arrintCompanyUserId					= array();
	protected $m_arrintSmsReceiverEmployeeIds			= array();

	protected $m_arrstrClientCompanyData                 = array();
	protected $m_arrstrCompanyUserEmailAddressesLocale = [];
	protected $m_arrstrTaskSequences;

	protected $m_strDefaultLocale                    = 'en_US';
	protected $m_strEmailFooterHelpLineNumber;
	const EMPLOYEE_TOKEN_LENGTH		= 30;
	const EMPLOYEE_TOKEN_EXPIRATION = 15;

	public function __construct() {
		parent::__construct();
		$this->m_boolIsRestored = false;
		$this->m_intIsKeyClient = 0;
		$this->m_objObjectStorageGateway = \CObjectStorageGatewayFactory::createObjectStorageGateway( \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED );
		return;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	/**
	 * Get Functions
	 */

	public function getAutomationFieldRequired() {
		return $this->m_boolAutomationFieldRequired;
	}

	public function getAssignedToEmail() {
		return $this->m_strAsssignedToEmail;
	}

	public function getTicketAndTaskWashlists() {
		return $this->m_arrintTicketAndTaskWashlists;
	}

	public function getTasks() {
		return $this->m_arrobjTasks;
	}

	public function getTasksCount() {
		return $this->m_intTasksCount;
	}

	public function getIsRestored() {
		return $this->m_boolIsRestored;
	}

	public function getCompletedTasks() {
		return $this->m_intCompletedTasks;
	}

	public function getAssignedTasks() {
		return $this->m_intAssignedTasks;
	}

	public function getMonth() {
		return $this->m_intMonth;
	}

	public function getYear() {
		return $this->m_intYear;
	}

	public function getIsAddTask() {
		return $this->m_boolIsAddTask;
	}

	public function getSendMailSalesRep() {
		return $this->m_intSendMailSalesRep;
	}

	public function getSendMailManager() {
		return $this->m_intSendMailManager;
	}

	public function getContact() {
		return $this->m_strContact;
	}

	public function getTaskNotes() {
		return $this->m_arrobjTaskNotes;
	}

	public function getPsProductName() {
		return $this->m_strPsProductName;
	}

	public function getPsProductOptionName() {
		return $this->m_strPsProductOptionName;
	}

	public function getLastClientUpdatedOn() {
		return $this->m_strLastClientUpdatedOn;
	}

	public function getTaskCompaniesPostDatetime() {
		return $this->m_strTaskCompaniesPostDatetime;
	}

	public function getTaskStoryPoints() {
		return $this->m_fltTaskStoryPoints;
	}

	public function getTaskCompanyNames() {
		return $this->m_strTaskCompanyNames;
	}

	public function getRootCauseDescription() {
		return $this->m_strRootCauseDescription;
	}

	public function getRootCauseName() {
		return $this->m_strRootCauseName;
	}

	public function getQaStoryPoints() {
		return $this->m_intQaStoryPoints;
	}

	public function getDevStoryPoints() {
		return $this->m_intDevStoryPoints;
	}

	public function getResolutionTime() {
		return $this->m_strResolutionTime;
	}

	public function getTaskIssueTypeId() {
		return $this->m_intTaskIssueTypeId;
	}

	public function getReferenceNumber() {
		return $this->m_intReferenceNumber;
	}

	public function getTaskDetail() {
		return $this->m_objTaskDetail;
	}

	public function getIsSdm() {
		return $this->m_boolIsSdm;
	}

	public function getIsQaApproved() {
		return $this->m_boolIsQaApproved;
	}

	public function getSubTaskTypeUserId() {
		return $this->m_intSubTaskTypeUserId;
	}

	public function getClientRiskStatus() {
		return $this->m_intClientRiskStatus;
	}

	public function getKeyClientRank() {
		return $this->m_intKeyClientRank;
	}

	public function getResolvedOn() {
		return $this->m_intResolvedOn;
	}

	public function getJsonTaskData() {
		return $this->m_strJsonTaskData;
	}

	public function getEmailSummaryId() {
		return $this->m_strEmailSummaryId;
	}

	public function getTaskReferenceTypeId() {
		return $this->m_intTaskReferenceTypeId;
	}

	public function getIsSqmApproved() {
		return $this->m_boolIsSqmApproved;
	}

	public function setIsSqmApproved( $boolIsSqmApproved ) {
		return $this->m_boolIsSqmApproved = $boolIsSqmApproved;
	}

	public function setFeedbackTitle( $strTitleName, $strFeedbackCategory ) {

		if( true == isset( $strTitleName ) && 'bug' == $strFeedbackCategory ) {
			$this->setTaskTypeId( CTaskType::BUG );
			$this->setTitle( 'A bug or a problem in the software ' . $strTitleName );

		} elseif( true == isset( $strTitleName ) && 'feature' == $strFeedbackCategory ) {
			$this->setTaskTypeId( CTaskType::FEATURE );
			$this->setTitle( 'A feature you would like to see ' . $strTitleName );
		}

	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == array_key_exists( 'title', $arrmixValues ) && 0 < \Psi\CStringService::singleton()->strlen( $arrmixValues['title'] ) ) {
			$this->setTitle( $arrmixValues['title'] );
		}

		if( true == array_key_exists( 'assigned_tasks', $arrmixValues ) ) {
			$this->setAssignedTasks( $arrmixValues['assigned_tasks'] );
		}

		if( true == array_key_exists( 'completed_tasks', $arrmixValues ) ) {
			$this->setCompletedTasks( $arrmixValues['completed_tasks'] );
		}

		if( true == array_key_exists( 'month', $arrmixValues ) ) {
			$this->setMonth( $arrmixValues['month'] );
		}

		if( true == array_key_exists( 'year', $arrmixValues ) ) {
			$this->setYear( $arrmixValues['year'] );
		}

		if( true == array_key_exists( 'support_fee_account_id', $arrmixValues ) ) {
			$this->setSupportFeeAccountId( $arrmixValues['support_fee_account_id'] );
		}

		if( true == array_key_exists( 'support_fee_amount_per_minute', $arrmixValues ) ) {
			$this->setSupportFeeAmountPerMinute( $arrmixValues['support_fee_amount_per_minute'] );
		}

		if( true == array_key_exists( 'cyear', $arrmixValues ) ) {
			$this->setYear( $arrmixValues['cyear'] );
		}

		if( true == isset( $arrmixValues['tasks_count'] ) ) 							$this->setTasksCount( $arrmixValues['tasks_count'] );
		if( true == isset( $arrmixValues['step_to_duplicate'] ) )						$this->setStepToDuplicate( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['step_to_duplicate'] ) : $arrmixValues['step_to_duplicate'] );
		if( true == isset( $arrmixValues['status_at_release_end'] ) )					$this->setTaskAssignmentsTaskStatusId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['status_at_release_end'] ) : $arrmixValues['status_at_release_end'] );
		if( true == isset( $arrmixValues['release_updated_at_status'] ) )				$this->setReleaseUpdatedStatusId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['release_updated_at_status'] ) : $arrmixValues['release_updated_at_status'] );
		if( true == isset( $arrmixValues['pending_task_updated_on'] ) ) 				$this->setPendingTaskLastUpdatedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['pending_task_updated_on'] ) : $arrmixValues['pending_task_updated_on'] );
		if( true == isset( $arrmixValues['ps_product_name'] ) ) 						$this->setPsProductName( $arrmixValues['ps_product_name'] );
		if( true == isset( $arrmixValues['ps_product_option_name'] ) )					$this->setPsProductOptionName( $arrmixValues['ps_product_option_name'] );
		if( true == isset( $arrmixValues['assinged_to_email'] ) )						$this->setAssignedToEmail( $arrmixValues['assinged_to_email'] );
		if( true == isset( $arrmixValues['ticket_and_task_watchlists'] ) )				$this->setTicketAndTaskWashlists( $arrmixValues['ticket_and_task_watchlists'] );
		if( true == isset( $arrmixValues['is_key_client'] ) ) 							$this->setIsKeyClient( $arrmixValues['is_key_client'] );
		if( true == isset( $arrmixValues['last_client_updated_on'] ) )					$this->setLastClientUpdatedOn( $arrmixValues['last_client_updated_on'] );
		if( true == isset( $arrmixValues['task_story_points'] ) ) 						$this->setTaskStoryPoints( $arrmixValues['task_story_points'] );
		if( true == isset( $arrmixValues['post_datetime'] ) )							$this->setTaskCompaniesPostDatetime( $arrmixValues['post_datetime'] );
		if( true == isset( $arrmixValues['company_names'] ) )							$this->setTaskCompanyNames( $arrmixValues['company_names'] );
		if( true == isset( $arrmixValues['task_updated_on'] ) )							$this->setTaskLastUpdatedOn( $arrmixValues['task_updated_on'] );
		if( true == isset( $arrmixValues['company_names'] ) )							$this->setTaskCompanyNames( $arrmixValues['company_names'] );
		if( true == isset( $arrmixValues['task_type_name'] ) )							$this->setTaskTypeName( $arrmixValues['task_type_name'] );
		if( true == isset( $arrmixValues['task_priority_name'] ) )						$this->setTaskPriorityName( $arrmixValues['task_priority_name'] );
		if( true == isset( $arrmixValues['task_status_name'] ) )						$this->setTaskStatusName( $arrmixValues['task_status_name'] );
		if( true == isset( $arrmixValues['task_employee_name_full'] ) )					$this->setTaskEmployeeFullName( $arrmixValues['task_employee_name_full'] );
		if( true == isset( $arrmixValues['employee_name_first'] ) )						$this->setEmployeeNameFirst( $arrmixValues['employee_name_first'] );
		if( true == isset( $arrmixValues['employee_name_last'] ) )						$this->setEmployeeNameLast( $arrmixValues['employee_name_last'] );
		if( true == isset( $arrmixValues['manager_name'] ) )							$this->setManagerName( $arrmixValues['manager_name'] );
		if( true == isset( $arrmixValues['team_name'] ) )								$this->setTeamName( $arrmixValues['team_name'] );
		if( true == isset( $arrmixValues['task_release_datetime'] ) )					$this->setTaskReleaseDatetime( $arrmixValues['task_release_datetime'] );
		if( true == isset( $arrmixValues['task_status'] ) )								$this->setTaskStatus( $arrmixValues['task_status'] );
		if( true == isset( $arrmixValues['task_impact'] ) )								$this->setTaskImpact( $arrmixValues['task_impact'] );
		if( true == isset( $arrmixValues['task_priority'] ) )							$this->setTaskPriority( $arrmixValues['task_priority'] );
		if( true == isset( $arrmixValues['task_type'] ) )								$this->setTaskType( $arrmixValues['task_type'] );
		if( true == isset( $arrmixValues['task_assigned_to'] ) )						$this->setTaskAssignedTo( $arrmixValues['task_assigned_to'] );
		if( true == isset( $arrmixValues['task_manager'] ) )							$this->setTaskManager( $arrmixValues['task_manager'] );
		if( true == isset( $arrmixValues['employee_id'] ) )								$this->setEmployeeId( $arrmixValues['employee_id'] );
		if( true == isset( $arrmixValues['team_id'] ) )									$this->setTeamId( $arrmixValues['team_id'] );
		if( true == isset( $arrmixValues['team_members_count'] ) )						$this->setTeamMembersCount( $arrmixValues['team_members_count'] );
		if( true == isset( $arrmixValues['bug_task_count'] ) )							$this->setBugTaskCount( $arrmixValues['bug_task_count'] );
		if( true == isset( $arrmixValues['feature_task_count'] ) )						$this->setFeatureTaskCount( $arrmixValues['feature_task_count'] );
		if( true == isset( $arrmixValues['other_task_count'] ) )						$this->setOtherTaskCount( $arrmixValues['other_task_count'] );
		if( true == isset( $arrmixValues['story_points'] ) )							$this->setStoryPoints( $arrmixValues['story_points'] );
		if( true == isset( $arrmixValues['severity_count'] ) )							$this->setSeverityCount( $arrmixValues['severity_count'] );
		if( true == isset( $arrmixValues['average_story_points'] ) )					$this->setAverageStoryPoints( $arrmixValues['average_story_points'] );
		if( true == isset( $arrmixValues['developer_story_points'] ) )					$this->setDevStoryPoints( $arrmixValues['developer_story_points'] );
		if( true == isset( $arrmixValues['qa_story_points'] ) )							$this->setQaStoryPoints( $arrmixValues['qa_story_points'] );
		if( true == isset( $arrmixValues['root_cause_description'] ) )					$this->setRootCauseDescription( $arrmixValues['root_cause_description'] );
		if( true == isset( $arrmixValues['root_cause_name'] ) )							$this->setRootCauseName( $arrmixValues['root_cause_name'] );
		if( true == isset( $arrmixValues['key_clients'] ) )								$this->setKeyClients( $arrmixValues['key_clients'] );
		if( true == isset( $arrmixValues['resolution_time'] ) )							$this->setResolutionTime( $arrmixValues['resolution_time'] );
		if( true == isset( $arrmixValues['is_automation_field_required'] ) )			$this->setAutomationFieldRequired( $arrmixValues['is_automation_field_required'] );
		if( true == isset( $arrmixValues['task_category_id'] ) )						$this->setTaskCategoryId( $arrmixValues['task_category_id'] );
		if( true == isset( $arrmixValues['client_name'] ) )								$this->setClientName( $arrmixValues['client_name'] );
		if( true == isset( $arrmixValues['task_note_id'] ) )							$this->setTaskNoteId( $arrmixValues['task_note_id'] );
		if( true == isset( $arrmixValues['name_full'] ) )								$this->setNameFull( $arrmixValues['name_full'] );
		if( true == isset( $arrmixValues['office_desk_number'] ) )						$this->setOfficeDeskNumber( $arrmixValues['office_desk_number'] );
		if( true == isset( $arrmixValues['task_release_name'] ) )						$this->setTaskReleaseName( $arrmixValues['task_release_name'] );
		if( true == isset( $arrmixValues['project_manager_name'] ) )					$this->setProjectManagerName( $arrmixValues['project_manager_name'] );
		if( true == isset( $arrmixValues['property_name'] ) )							$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['ps_lead_id'] ) )								$this->setPropertyName( $arrmixValues['ps_lead_id'] );
		if( true == isset( $arrmixValues['task_quarter_id'] ) )							$this->setTaskQuarterId( $arrmixValues['task_quarter_id'] );
		if( true == isset( $arrmixValues['parent_task_id'] ) )							$this->setParentTaskId( $arrmixValues['parent_task_id'] );
		if( true == isset( $arrmixValues['parent_task_type_name'] ) )					$this->setParentTaskTypeName( $arrmixValues['parent_task_type_name'] );
		if( true == isset( $arrmixValues['task_tag_name'] ) )							$this->setTaskTagName( $arrmixValues['task_tag_name'] );
		if( true == isset( $arrmixValues['reference_number'] ) )						$this->setReferenceNumber( $arrmixValues['reference_number'] );
		if( true == isset( $arrmixValues['task_reference_type_id'] ) )					$this->setTaskReferenceTypeId( $arrmixValues['task_reference_type_id'] );
		if( true == isset( $arrmixValues['task_issue_type_id'] ) )						$this->setTaskIssueTypeId( $arrmixValues['task_issue_type_id'] );
		if( true == isset( $arrmixValues['task_detail'] ) )								$this->setTaskDetail( $arrmixValues['task_detail'] );
		if( true == isset( $arrmixValues['is_sdm'] ) )									$this->setIsSdm( $arrmixValues['is_sdm'] );
		if( true == isset( $arrmixValues['is_qa_approved'] ) )							$this->setIsQaApproved( $arrmixValues['is_qa_approved'] );
		if( true == isset( $arrmixValues['is_sqm_approved'] ) )							$this->setIsSqmApproved( $arrmixValues['is_sqm_approved'] );
		if( true == isset( $arrmixValues['sub_task_type_user_id'] ) )					$this->setSubTaskTypeUserId( $arrmixValues['sub_task_type_user_id'] );
		if( true == isset( $arrmixValues['client_risk_status'] ) )						$this->setClientRiskStatus( $arrmixValues['client_risk_status'] );
		if( true == isset( $arrmixValues['key_client_rank'] ) )							$this->setKeyClientRank( $arrmixValues['key_client_rank'] );
		if( true == isset( $arrmixValues['resolved_on'] ) )								$this->setResolvedOn( $arrmixValues['resolved_on'] );
		if( true == isset( $arrmixValues['json_task_data'] ) )							$this->setJsonTaskData( $arrmixValues['json_task_data'] );
		if( true == isset( $arrmixValues['email_summary_id'] ) )						$this->setEmailSummaryId( $arrmixValues['email_summary_id'] );
		if( true == isset( $arrmixValues['qam_name'] ) ) 								$this->setQamName( $arrmixValues['qam_name'] );
		if( true == isset( $arrmixValues['quarter_name'] ) )							$this->setQuarterName( $arrmixValues['quarter_name'] );
		if( true == isset( $arrmixValues['is_qa_file_owner'] ) )						$this->setIsQAFileOwner( $arrmixValues['is_qa_file_owner'] );
		if( true == isset( $arrmixValues['test_case_task_ids'] ) ) 						$this->setTestCaseTaskIds( $arrmixValues['test_case_task_ids'] );
		if( true == isset( $arrmixValues['keywords'] ) ) 								$this->setKeywords( $arrmixValues['keywords'] );
		if( true == isset( $arrmixValues['developer_employee_id'] ) ) 					$this->setDeveloperEmployeeId( $arrmixValues['developer_employee_id'] );
		if( true == isset( $arrmixValues['code_reviewer_employee_id'] ) ) 				$this->setCodeReviewerEmployeeId( $arrmixValues['code_reviewer_employee_id'] );
		if( true == isset( $arrmixValues['code_reviewer_name'] ) ) 						$this->setCodeReviewerName( $arrmixValues['code_reviewer_name'] );
		if( true == isset( $arrmixValues['task_reference_name'] ) ) 					$this->setTaskReferenceName( $arrmixValues['task_reference_name'] );
		if( true == isset( $arrmixValues['is_yearly'] ) )                               $this->setYearly( $arrmixValues['is_yearly'] );
		if( true == isset( $arrmixValues['rwx_domain'] ) )          					$this->setRwxDomain( $arrmixValues['rwx_domain'] );
		if( true == isset( $arrmixValues['associated_properties_count'] ) )          	$this->setAssociatedPropertiesCount( $arrmixValues['associated_properties_count'] );
		if( true == isset( $arrmixValues['story_order_num'] ) )          				$this->setStoryOrderNum( $arrmixValues['story_order_num'] );

	}

	public function getTaskVoteCount() {
		return $this->m_intTaskVoteCount;
	}

	public function getVotes() {
		return $this->m_intVotes;
	}

	public function getShowUserWatchlist() {
		return $this->m_intShowUserWatchlist;
	}

	public function getLiveSupportChatId() {
		return $this->m_intLiveSupportChatId;
	}

	public function getSupportFeeAmountPerMinute() {
		return $this->m_fltSupportFeeAmountPerMinute;
	}

	public function getSupportFeeAccountId() {
		return $this->m_intSupportFeeAccountId;
	}

	public function getSafeDescription() {
		$strShortDescription = \Psi\CStringService::singleton()->preg_replace( '[^ \n\r a-zA-Z0-9.\-]', '', strip_tags( $this->getDescription() ) );
		return $strShortDescription;
	}

	public function getAttachmentsCount() {
		return $this->m_intAttachmentsCount;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getSendEmailTaskNoteCids() {
		return $this->m_arrintTaskNoteCids;
	}

	public function getSendEmailToCids() {
		return $this->m_arrintSendEmailToCids;
	}

	public function getNewTaskWatchlistsUserIds() {
		return $this->m_arrintNewTaskWatchlistsUserIds;
	}

	public function getTaskWatchlists() {
		return $this->m_arrobjTaskWatchlists;
	}

	public function getSendEmailToClients() {
		return $this->m_boolSendEmailToClients;
	}

	public function getIsKeyClient() {
		return $this->m_intIsKeyClient;
	}

	public function getTaskTypeName() {
		return $this->m_strTaskTypeName;
	}

	public function getTaskPriorityName() {
		return $this->m_strTaskPriorityName;
	}

	public function getTaskStatusName() {
		return $this->m_strTaskStatusName;
	}

	public function getTaskImpact() {
		return $this->m_strTaskImpact;
	}

	public function getTaskEmployeeFullName() {
		return $this->m_strEmployeeFullName;
	}

	public function getEmployeeNameFirst() {
		return $this->m_strEmployeeNameFirst;
	}

	public function getEmployeeNameLast() {
		return $this->m_strEmployeeNameLast;
	}

	public function getManagerName() {
		return $this->m_strManagerName;
	}

	public function getTeamName() {
		return $this->m_strTeamName;
	}

	public function getTaskReleaseDatetime() {
		return $this->m_strTaskReleaseDatetime;
	}

	public function getTaskStatus() {
		return $this->m_strTaskStatus;
	}

	public function getTaskPriority() {
		return $this->m_strTaskPriority;
	}

	public function getTaskType() {
		return $this->m_strTaskType;
	}

	public function getTaskAssignedTo() {
		return $this->m_strTaskAssignedTo;
	}

	public function getTaskManager() {
		return $this->m_strTaskManager;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getTeamMembersCount() {
		return $this->m_intTeamMembersCount;
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function getBugTaskCount() {
		return $this->m_intBugTaskCount;
	}

	public function getFeatureTaskCount() {
		return $this->m_intFeatureTaskCount;
	}

	public function getOtherTaskCount() {
		return $this->m_intOtherTaskCount;
	}

	public function getStoryPoints() {
		return $this->m_fltStoryPoints;
	}

	public function getSeverityCount() {
		return $this->m_fltSeverityCount;
	}

	public function getAverageStoryPoints() {
		return $this->m_fltAverageStoryPoints;
	}

	public function getKeyClients() {
		return $this->m_intKeyClients;
	}

	public function getTaskCategoryId() {
		return $this->m_intTaskCategoryId;
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getTaskNoteId() {
		return $this->m_intTaskNoteId;
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getOfficeDeskNumber() {
		return $this->m_strOfficeDeskNumber;
	}

	public function getTaskReleaseName() {
		return $this->m_strTaskReleaseName;
	}

	public function getClusterName() {
		return $this->m_strClusterName;
	}

	public function getProjectManagerName() {
		return $this->m_strProjectManagerName;
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function getTaskQuarterId() {
		return $this->m_intTaskQuarterId;
	}

	public function getParentTaskId() {
		return $this->m_intParentTaskId;
	}

	public function getTaskTagName() {
		return $this->m_strTaskTagName;
	}

	public function getParentTaskTypeName() {
		return $this->m_strParentTaskTypeName;
	}

	public function getQamName() {
		return $this->m_strQamName;
	}

	public function getQuarterName() {
		return $this->m_strQuarterName;
	}

	public function getIsQAFileOwner() {
		return $this->m_boolIsQAFileOwner;
	}

	public function getTestCaseTaskIds() {
		return $this->m_strTestCaseTaskIds;
	}

	public function getKeywords() {
		return $this->m_strKeywords;
	}

	public function getDeveloperEmployeeId() {
		return $this->m_intDeveloperEmployeeId;
	}

	public function getCodeReviewerEmployeeId() {
		return $this->m_intCodeReviewerEmployeeId;
	}

	public function getCodeReviewerName() {
		return $this->m_strCodeReviewerName;
	}

	public function getTaskReferenceName() {
		return $this->m_strTaskReferenceName;
	}

	public function getYearly() {
		return $this->m_boolYearly;
	}

	public function getRwxDomain() {
		return $this->m_strRwxDomain;
	}

	public function getEmailFooterHelpLineNumber() {
		return $this->m_strEmailFooterHelpLineNumber;
	}

	public function getAssociatedPropertiesCount() {
		return $this->m_intAssociatedPropertiesCount;
	}

	public function getStoryOrderNum() {
		return $this->m_intStoryOrderNum;
	}

	/**
	 * Set Functions
	 */

	public function setAutomationFieldRequired( $boolAutomationFieldRequired ) {
		$this->m_boolAutomationFieldRequired = $boolAutomationFieldRequired;
	}

	public function setDefaults() {

		$this->m_intTaskStatusId					= CTaskStatus::UNUSED;
		$this->m_intTaskPriorityId					= CTaskPriority::LOW;
		$this->m_intIsPublished						= 0;

		$this->m_boolIsDescriptionRequired			= false;
		$this->m_boolIsStepToDuplicateRequired		= false;
		$this->m_boolIsPsProductNotRequired			= false;
		$this->m_boolIsTaskParentIdNotRequired		= false;
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {

		parent::setDescription( $strDescription, $strLocaleCode );

		$arrintWithoutTitleTaskTypes = array(
			CTaskType::BUG,
			CTaskType::FEATURE,
			CTaskType::TO_DO,
			CTaskType::PROJECT
		);

		if( false == is_null( $this->getTaskTypeId() ) && true == in_array( $this->getTaskTypeId(), $arrintWithoutTitleTaskTypes ) && true == is_null( $this->m_strTitle ) && 0 == \Psi\CStringService::singleton()->strlen( trim( $this->m_strTitle ) ) ) {

			$strDescription		= str_replace( '&nbsp;', '', \Psi\CStringService::singleton()->preg_replace( '/\s\s+/', ' ', $this->getDescription() ) );
			$strDescription		= strip_tags( html_entity_decode( nl2br( trim( $strDescription ) ), ENT_QUOTES ) );
			$strDescription		= \Psi\CStringService::singleton()->wordwrap( $strDescription, 28, ' ', true );
			$this->m_strTitle	= html_entity_decode( truncate( $strDescription, 100 ), ENT_QUOTES );
			$this->m_strTitle	= trim( str_replace( 'User Story :', '', $this->m_strTitle ) );
		}
	}

	public function setAssignedToEmail( $strAssignedEmail ) {
		$this->m_strAsssignedToEmail = $strAssignedEmail;
	}

	public function setTicketAndTaskWashlists( $arrintTicketAndTaskWashlists ) {
		$this->m_arrintTicketAndTaskWashlists = $arrintTicketAndTaskWashlists;
	}

	public function setIsRestored( $boolIsRestored ) {
		$this->m_boolIsRestored = $boolIsRestored;
	}

	public function setTasksCount( $intTasksCount ) {
		$this->m_intTasksCount = $intTasksCount;
	}

	public function setTaskVoteCount( $intTaskVoteCount ) {
		$this->m_intTaskVoteCount = $intTaskVoteCount;
	}

	public function setVotes( $intVotes ) {
		$this->m_intVotes = $intVotes;
	}

	public function setCompletedTasks( $intCompletedTasks ) {
		$this->m_intCompletedTasks = $intCompletedTasks;
	}

	public function setAssignedTasks( $intAssignedTasks ) {
		$this->m_intAssignedTasks = $intAssignedTasks;
	}

	public function setMonth( $intMonth ) {
		$this->m_intMonth = $intMonth;
	}

	public function setYear( $intYear ) {
		$this->m_intYear = $intYear;
	}

	public function setShowUserWatchlist( $intShowUserWatchlist ) {
		$this->m_intShowUserWatchlist = $intShowUserWatchlist;
	}

	public function setLiveSupportChatId( $intLiveSupportChatId ) {
		$this->m_intLiveSupportChatId = $intLiveSupportChatId;
	}

	public function setIsAddTask( $boolIsAddTask ) {
		$this->m_boolIsAddTask = $boolIsAddTask;
	}

	public function setSendMailSalesRep( $intSendMailSalesRep ) {
		$this->m_intSendMailSalesRep = $intSendMailSalesRep;
	}

	public function setSendMailManager( $intSendMailManager ) {
		$this->m_intSendMailManager = $intSendMailManager;
	}

	public function setContact( $strContact ) {
		$this->m_strContact = $strContact;
	}

	public function setAttachmentsCount( $intAttachmentsCount ) {
		$this->m_intAttachmentsCount = $intAttachmentsCount;
	}

	public function setTaskNotes( $arrobjTaskNotes ) {
		$this->m_arrobjTaskNotes = $arrobjTaskNotes;
	}

	public function setSupportFeeAmountPerMinute( $fltSupportFeeAmountPerMinute ) {
		$this->m_fltSupportFeeAmountPerMinute = $fltSupportFeeAmountPerMinute;
	}

	public function setSupportFeeAccountId( $intSupportFeeAccountId ) {
		$this->m_intSupportFeeAccountId = $intSupportFeeAccountId;
	}

	public function setPsProductName( $strPsProductName ) {
		$this->m_strPsProductName = $strPsProductName;
	}

	public function setPsProductOptionName( $strPsProductOptionName ) {
		$this->m_strPsProductOptionName = $strPsProductOptionName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setSendEmailTaskNoteCids( $arrintTaskNoteCids ) {
		$this->m_arrintTaskNoteCids = $arrintTaskNoteCids;
	}

	public function setSendEmailToCids( $arrintSendEmailToCids ) {
		$this->m_arrintSendEmailToCids = $arrintSendEmailToCids;
	}

	public function setNewTaskWatchlistsUserIds( $arrintNewTaskWatchlistsUserIds ) {
		$this->m_arrintNewTaskWatchlistsUserIds = $arrintNewTaskWatchlistsUserIds;
	}

	public function setTaskWatchlists( $arrobjTaskWatchlists ) {
		$this->m_arrobjTaskWatchlists = $arrobjTaskWatchlists;
	}

	public function setSendEmailToClients( $boolSendEmailToClients ) {
		$this->m_boolSendEmailToClients = $boolSendEmailToClients;
	}

	public function setIsKeyClient( $intIsKeyClient = 0 ) {
		$this->m_intIsKeyClient = $intIsKeyClient;
	}

	public function setLastClientUpdatedOn( $strLastClientUpdatedOn ) {
		$this->m_strLastClientUpdatedOn = $strLastClientUpdatedOn;
	}

	public function setTaskStoryPoints( $fltTaskStoryPoints ) {
		$this->m_fltTaskStoryPoints = $fltTaskStoryPoints;
	}

	public function setTaskCompaniesPostDatetime( $strTaskCompaniesPostDatetime ) {
		$this->m_strTaskCompaniesPostDatetime = $strTaskCompaniesPostDatetime;
	}

	public function setTaskCompanyNames( $strTaskCompanyNames ) {
		$this->m_strTaskCompanyNames = $strTaskCompanyNames;
	}

	public function setTaskTypeName( $strTaskTypeName ) {
		$this->m_strTaskTypeName = $strTaskTypeName;
	}

	public function setTaskIssueTypeId( $intTaskIssueTypeId ) {
		$this->m_intTaskIssueTypeId = $intTaskIssueTypeId;
	}

	public function setTaskPriorityName( $strTaskPriorityName ) {
		$this->m_strTaskPriorityName = $strTaskPriorityName;
	}

	public function setTaskStatusName( $strTaskStatusName ) {
		$this->m_strTaskStatusName = $strTaskStatusName;
	}

	public function setTaskImpact( $strTaskImpact ) {
		return $this->m_strTaskImpact = $strTaskImpact;
	}

	public function setTaskEmployeeFullName( $strEmployeeFullName ) {
		$this->m_strEmployeeFullName = $strEmployeeFullName;
	}

	public function setEmployeeNameFirst( $strEmployeeNameFirst ) {
		$this->m_strEmployeeNameFirst = $strEmployeeNameFirst;
	}

	public function setEmployeeNameLast( $strEmployeeNameLast ) {
		$this->m_strEmployeeNameLast = $strEmployeeNameLast;
	}

	public function setManagerName( $strManagerName ) {
		$this->m_strManagerName = $strManagerName;
	}

	public function setTeamName( $strTeamName ) {
		$this->m_strTeamName = $strTeamName;
	}

	public function setTaskReleaseDatetime( $strTaskReleaseDatetime ) {
		return $this->m_strTaskReleaseDatetime = $strTaskReleaseDatetime;
	}

	public function setTaskStatus( $strTaskStatus ) {
		return $this->m_strTaskStatus = $strTaskStatus;
	}

	public function setTaskPriority( $strTaskPriority ) {
		return $this->m_strTaskPriority = $strTaskPriority;
	}

	public function setTaskType( $strTaskType ) {
		return $this->m_strTaskType = $strTaskType;
	}

	public function setTaskAssignedTo( $strTaskAssignedTo ) {
		return $this->m_strTaskAssignedTo = $strTaskAssignedTo;
	}

	public function setTaskManager( $strTaskManager ) {
		return $this->m_strTaskManager = $strTaskManager;
	}

	public function setEmployeeId( $intEmployeeId ) {
		return $this->m_intEmployeeId = $intEmployeeId;
	}

	public function setTeamMembersCount( $intTeamMembersCount ) {
		return $this->m_intTeamMembersCount = $intTeamMembersCount;
	}

	public function setTeamId( $intTeamId ) {
		return $this->m_intTeamId = $intTeamId;
	}

	public function setBugTaskCount( $intBugTaskCount ) {
		return $this->m_intBugTaskCount = $intBugTaskCount;
	}

	public function setFeatureTaskCount( $intFeatureTaskCount ) {
		return $this->m_intFeatureTaskCount = $intFeatureTaskCount;
	}

	public function setOtherTaskCount( $intOtherTaskCount ) {
		return $this->m_intOtherTaskCount = $intOtherTaskCount;
	}

	public function setRootCauseDescription( $strRootCauseDescription ) {
		return $this->m_strRootCauseDescription = $strRootCauseDescription;
	}

	public function setRootCauseName( $strRootCauseName ) {
		return $this->m_strRootCauseName = $strRootCauseName;
	}

	public function setStoryPoints( $fltStoryPoints ) {
		return $this->m_fltStoryPoints = $fltStoryPoints;
	}

	public function setAverageStoryPoints( $fltAverageStoryPoints ) {
		return $this->m_fltAverageStoryPoints = $fltAverageStoryPoints;
	}

	public function setSeverityCount( $fltSeverityCount ) {
		return $this->m_fltSeverityCount = $fltSeverityCount;
	}

	public function setQaStoryPoints( $intQaStoryPoints ) {
		return $this->m_intQaStoryPoints = $intQaStoryPoints;
	}

	public function setDevStoryPoints( $intDevStoryPoints ) {
		return $this->m_intDevStoryPoints = $intDevStoryPoints;
	}

	public function setKeyClients( $intKeyClients ) {
		$this->m_intKeyClients = $intKeyClients;
	}

	public function setResolutionTime( $strResolutionTime ) {
		$this->m_strResolutionTime = $strResolutionTime;
	}

	public function setTaskCategoryId( $intTaskCategoryId ) {
		$this->m_intTaskCategoryId = $intTaskCategoryId;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setTaskNoteId( $intTaskNoteId ) {
		$this->m_intTaskNoteId = $intTaskNoteId;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setOfficeDeskNumber( $strOfficeDeskNumber ) {
		$this->m_strOfficeDeskNumber = $strOfficeDeskNumber;
	}

	public function setTaskReleaseName( $strTaskReleaseName ) {
		$this->m_strTaskReleaseName = $strTaskReleaseName;
	}

	public function setClusterName( $strClusterName ) {
		$this->m_strClusterName = $strClusterName;
	}

	public function setProjectManagerName( $strProjectManagerName ) {
		$this->m_strProjectManagerName = $strProjectManagerName;
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->m_intPsLeadId = $intPsLeadId;
	}

	public function setTaskQuarterId( $intTaskQuarterId ) {
		$this->m_intTaskQuarterId = CStrings::strToIntDef( $intTaskQuarterId, NULL, false );
	}

	public function setParentTaskId( $intParentTaskId ) {
		$this->m_intParentTaskId = $intParentTaskId;
	}

	public function setTaskTagName( $strTaskTagName ) {
		$this->m_strTaskTagName = $strTaskTagName;
	}

	public function setParentTaskTypeName( $strParentTaskTypeName ) {
			$this->m_strParentTaskTypeName = $strParentTaskTypeName;
	}

	public function setReferenceNumber( $intReferenceNumber ) {
		return $this->m_intReferenceNumber = $intReferenceNumber;
	}

	public function setTaskDetail( $objTaskDetail ) {
		return $this->m_objTaskDetail = $objTaskDetail;
	}

	public function setIsSdm( $boolIsSdm ) {
		return $this->m_boolIsSdm = $boolIsSdm;
	}

	public function setIsQaApproved( $boolIsQaApproved ) {
		return $this->m_boolIsQaApproved = $boolIsQaApproved;
	}

	public function setSubTaskTypeUserId( $intSubTaskTypeUserId ) {
		$this->m_intSubTaskTypeUserId = $intSubTaskTypeUserId;
	}

	public function setClientRiskStatus( $intClientRiskStatus ) {
		$this->m_intClientRiskStatus = $intClientRiskStatus;
	}

	public function setKeyClientRank( $intKeyClientRank ) {
		$this->m_intKeyClientRank = $intKeyClientRank;
	}

	public function setResolvedOn( $intResolvedOn ) {
		$this->m_intResolvedOn = $intResolvedOn;
	}

	public function setJsonTaskData( $strJsonTaskData ) {
		$this->m_strJsonTaskData = $strJsonTaskData;
	}

	public function setEmailSummaryId( $strEmailSummaryId ) {
		$this->m_strEmailSummaryId = $strEmailSummaryId;
	}

	public function setQamName( $strQamName ) {
		$this->m_strQamName = $strQamName;
	}

	public function setQuarterName( $strQuarterName ) {
		$this->m_strQuarterName = $strQuarterName;
	}

	public function setTestCaseTaskIds( $strTestCaseTaskIds ) {
		$this->m_strTestCaseTaskIds = $strTestCaseTaskIds;
	}

	public function setTaskReferenceTypeId( $intTaskReferenceTypeId ) {
		return $this->m_intTaskReferenceTypeId = $intTaskReferenceTypeId;
	}

	public function setIsQAFileOwner( $boolIsQAFileOwner ) {
		$this->m_boolIsQAFileOwner = $boolIsQAFileOwner;
	}

	public function setKeywords( $strKeywords ) {
		$this->set( 'm_strKeywords', CStrings::strTrimDef( $strKeywords, NULL, NULL, true ) );
	}

	public function setDeveloperEmployeeId( $intDeveloperEmployeeId ) {
		return $this->m_intDeveloperEmployeeId = $intDeveloperEmployeeId;
	}

	public function setCodeReviewerEmployeeId( $intCodeReviewerEmployeeId ) {
		return $this->m_intCodeReviewerEmployeeId = $intCodeReviewerEmployeeId;
	}

	public function setCodeReviewerName( $strCodeReviewerName ) {
		return $this->m_strCodeReviewerName = $strCodeReviewerName;
	}

	public function setTaskReferenceName( $strTaskReferenceName ) {
		$this->m_strTaskReferenceName = $strTaskReferenceName;
	}

	public function setYearly( $boolYearly ) {
		return $this->m_boolYearly = $boolYearly;
	}

	public function setRwxDomain( $strRwxDomain ) {
		$this->m_strRwxDomain = $strRwxDomain;
	}

	public function setEmailFooterHelpLineNumber( $strEmailFooterHelpLineNumber ) {
		return $this->m_strEmailFooterHelpLineNumber = $strEmailFooterHelpLineNumber;
	}

	public function setAssociatedPropertiesCount( $intAssociatedPropertyCount ) {
		return $this->m_intAssociatedPropertiesCount = ( int ) $intAssociatedPropertyCount;
	}

	public function setStoryOrderNum( $intOrderNum ) {
		$this->m_intStoryOrderNum = ( int ) $intOrderNum;
	}

	/**
	 * Add Functions
	 */

 	public function addTask( $objTask ) {

		if( false == valObj( $objTask, 'CTask' ) ) return false;

		if( false == is_null( $objTask->getId() ) ) {
			$this->m_arrobjTasks[$objTask->getId()] = $objTask;

		} else {
			$this->m_arrobjTasks[] = $objTask;
		}

		return true;
	}

	public function addTaskNote( $objTaskNote ) {
		$this->m_arrobjTaskNotes[$objTaskNote->getId()] = $objTaskNote;
	}

	/**
	 * Create Functions
	 */

	public function createTask() {

		$objTask = new CTask();
		$objTask->setDefaults();

		return $objTask;
	}

	public function createSupportFeeTransaction( $objAdminDatabase ) {

		$objClient	= CClients::fetchClientById( $this->getCid(), $objAdminDatabase );

		$objTransaction	= new Transaction();
		$objTransaction->setCid( $this->getCid() );
		$objTransaction->setEntityId( $objClient->getEntityId() );
		$objTransaction->setAccountId( $this->getSupportFeeAccountId() );
		$objTransaction->setChargeCodeId( CChargeCode::SUPPORT_FEE );
		$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
		$objTransaction->setTransactionAmount( $this->getDurationMinutes() * $this->getSupportFeeAmountPerMinute() );
		$objTransaction->setMemo( 'Support Fee for Task #' . $this->getId() );

		return $objTransaction;
	}

	public function createTaskAttachment() {

		$objTaskAttachment = new CTaskAttachment();
		$objTaskAttachment->setTaskId( $this->m_intId );

		return $objTaskAttachment;
	}

	public function createTaskSvnFile() {

		$objTaskSvnFile = new CTaskSvnFile();
		$objTaskSvnFile->setTaskId( $this->m_intId );

		return $objTaskSvnFile;
	}

	public function createTaskNote() {

		$objTaskNote = new CTaskNote();
		$objTaskNote->setTaskId( $this->m_intId );

		return $objTaskNote;
	}

	public function createTaskAssignment() {

		$objTaskAssignment = new CTaskAssignment();
		$objTaskAssignment->setTaskId( $this->m_intId );
		$objTaskAssignment->setCid( $this->getCid() );
		$objTaskAssignment->setUserId( $this->getUserId() );
		$objTaskAssignment->setGroupId( $this->getGroupId() );
		$objTaskAssignment->setTaskStatusId( $this->getTaskStatusId() );
		$objTaskAssignment->setTaskPriorityId( $this->getTaskPriorityId() );
		$objTaskAssignment->setTaskTypeId( $this->getTaskTypeId() );
		$objTaskAssignment->setTaskReleaseId( $this->getTaskReleaseId() );
		$objTaskAssignment->setSdmEmployeeId( $this->getProjectManagerId() );
		$objTaskAssignment->setPsProductId( $this->getPsProductId() );
		$objTaskAssignment->setPsProductOptionId( $this->getPsProductOptionId() );

		$intDateDiff = strtotime( $this->getDueDate() ) - strtotime( date( 'n/j/Y' ) );

		if( 0 < $intDateDiff ) {
			$objTaskAssignment->setMinutesAssigned( ( $intDateDiff / 86400 ) * 24 * 60 );
		} else {
			$objTaskAssignment->setMinutesAssigned( 1 * 24 * 60 );
		}

		return $objTaskAssignment;
	}

	public function createTaskWatchList() {

		$objTaskWatchList = new CTaskWatchlist();
		$objTaskWatchList->setCid( $this->getCid() );
		$objTaskWatchList->setTaskId( $this->getId() );
		// $objTaskWatchList->setUserId( $this->getUserId() );
		$objTaskWatchList->setIsForcefullyEmail( 1 );
		return $objTaskWatchList;
	}

	public function createParentTaskAssociation() {

		$objTaskAssociation = new CTaskAssociation();
		$objTaskAssociation->setTaskId( $this->getId() );

		return $objTaskAssociation;

	}

	public function createTaskCompany() {

		$objTaskCompany = new CTaskCompany();
		$objTaskCompany->setCid( $this->getCid() );
		$objTaskCompany->setTaskId( $this->getId() );
		$objTaskCompany->setUserId( $this->getUserId() );

		return $objTaskCompany;
	}

	public function createTaskServer() {

		$objTaskCompany = new CTaskServer();
		$objTaskCompany->setTaskId( $this->getId() );
		$objTaskCompany->setUserId( $this->getUserId() );

		return $objTaskCompany;
	}

	public function createTicketAndTaskWatchlist() {

		$objTicketAndTaskWatchlist = new CTicketAndTaskWatchlist();
		$objTicketAndTaskWatchlist->setTaskId( $this->getId() );
		$objTicketAndTaskWatchlist->setCid( $this->getCid() );

		return $objTicketAndTaskWatchlist;
	}

	public function createTaskDetail() {

		$objTaskDetail = new CTaskDetail();
		$objTaskDetail->setTaskId( $this->getId() );
		return $objTaskDetail;
	}

	public function createOutboundCall( $objCallQueue, $objCallAgent ) {

		$objOutboundCall = new COutboundCall();

		$objOutboundCall->setTaskId( $this->getId() );
		$objOutboundCall->setCid( $this->getCid() );
		$objOutboundCall->setPropertyId( $this->getPropertyId() );
		$objOutboundCall->setPhoneNumberTypeId( $this->getContactPhoneNumberTypeId() );
		$objOutboundCall->setCallTypeId( CCallType::OUTBOUND_SUPPORT );
		$objOutboundCall->setOutboundCallStatusTypeId( COutboundCallStatusType::PENDING );
		$objOutboundCall->setReceipientName( $this->getContactNameFirst() . ' ' . $this->getContactNameLast() );
		$objOutboundCall->setPhoneNumber( \Psi\CStringService::singleton()->preg_replace( '/[^0-9]/s', '', $this->getContactPhoneNumber() ) );
		$objOutboundCall->setOutboundCallCampaignId( COutboundCallCampaign::SUPPORT_OUTBOUND_DIALER );
		$objOutboundCall->setStartTime( date( 'H:m:i' ) );
		$objOutboundCall->setDontCallAfter( date( 'Y-m-d', strtotime( '+1 day' ) ) );
		$objOutboundCall->setAssignedOn( 'NOW()' );
		$objOutboundCall->setInitiatedOn( 'NOW()' );

		if( true == valObj( $objCallQueue, 'CCallQueue' ) ) $objOutboundCall->setCallQueueId( $objCallQueue->getId() );
		if( true == valObj( $objCallAgent, 'CCallAgent' ) ) $objOutboundCall->setCallAgentId( $objCallAgent->getId() );

		return $objOutboundCall;
	}

	public function createChat() {

		$objChat = new CChat();
		$objChat->setPsProductId( $this->getPsProductId() );
		$objChat->setCid( $this->getCid() );
		$objChat->setIpAddress( gethostbyname( gethostname() ) );

		return $objChat;
	}

	public function createTaskReference( $intReferenceNumber = NULL, $intReferenceTypeId = NULL ) {

		$objTaskReference = new CTaskReference();
		$objTaskReference->setTaskId( $this->getId() );
		$objTaskReference->setReferenceNumber( $intReferenceNumber );
		$objTaskReference->setTaskReferenceTypeId( $intReferenceTypeId );

		return $objTaskReference;
	}

	public function createSurvey( $intTaskId, $intSurveyTypeId, $intSurveyTemplateId ) {
		if( false == valId( $intTaskId ) || false == valId( $intSurveyTypeId ) || false == valId( $intSurveyTemplateId ) ) return NULL;

		$objSurvey = new CSurvey();
		$objSurvey->setSurveyTypeId( $intSurveyTypeId );
		$objSurvey->setSurveyTemplateId( $intSurveyTemplateId );
		$objSurvey->setTriggerReferenceId( $intTaskId );
		$objSurvey->setSubjectReferenceId( $this->getEmployeeId() );
		return $objSurvey;
	}

	public static function createInductionTasks( $intEmployeeId, $intPreviousTeamId, $objDatabase ) {
		$arrobjTaskObjects		= [];
		$intTeamChangeCount		= CTeamEmployees::fetchTeamChangeCountByEmployeeId( $intEmployeeId, $objDatabase );
		$arrmixEmployeeDetails	= ( array ) rekeyArray( 'employee_id', ( array ) CTeams::fetchManagerByEmployeeIds( array( $intEmployeeId ), $objDatabase ) );

		if( false == valId( $arrmixEmployeeDetails[$intEmployeeId]['tpoc_user_id'] ) ) {
			return false;
		}

		$objTask = new CTask();
		$objTask = $objTask->createTask();

		if( 2 == $intTeamChangeCount && CTeam::NEW_JOINEE_INDIA == $intPreviousTeamId && false == CTask::checkIfTaskAlreadyCreated( $intEmployeeId, CTaskReferenceType::L1_INDUCTION, $objDatabase ) ) {
			$objTaskClone = clone $objTask;
			$objTaskClone->setTaskTypeId( CTaskType::TO_DO );
			$objTaskClone->fetchNextId( $objDatabase );
			$objTaskClone->setUserId( $arrmixEmployeeDetails[$intEmployeeId]['tpoc_user_id'] );
			$objTaskClone->setTitle( 'Complete L1 induction for ' . $arrmixEmployeeDetails[$intEmployeeId]['employee_name'] );
			$objTaskClone->setDescription( 'Complete L1 induction for ' . $arrmixEmployeeDetails[$intEmployeeId]['employee_name'] );
			$objTaskClone->setDueDate( date( 'm/d/Y', strtotime( '+15 days' ) ) );
			$arrobjTaskObjects['tasks'][]	= $objTaskClone;

			$objTaskReferenceClone	= $objTaskClone->createTaskReference( $intEmployeeId, CTaskReferenceType::L1_INDUCTION );
			$objTaskReferenceClone->setCreatedBy( CUser::ID_SYSTEM );
			$objTaskReferenceClone->setCreatedOn( 'NOW()' );
			$arrobjTaskObjects['task_references'][] = $objTaskReferenceClone;

			$objTaskClone = clone $objTask;
			$objTaskClone->setTaskTypeId( CTaskType::TO_DO );
			$objTaskClone->fetchNextId( $objDatabase );
			$objTaskClone->setUserId( ( CDepartment::QA == $arrmixEmployeeDetails[$intEmployeeId]['department_id'] && false == is_null( $arrmixEmployeeDetails[$intEmployeeId]['qa_mentor_id'] ) ) ? $arrmixEmployeeDetails[$intEmployeeId]['qa_mentor_id'] : $arrmixEmployeeDetails[$intEmployeeId]['manager_user_id'] );
			$objTaskClone->setTitle( 'Complete L2-L3 induction for ' . $arrmixEmployeeDetails[$intEmployeeId]['employee_name'] );
			$objTaskClone->setDescription( 'Complete L2-L3 induction for ' . $arrmixEmployeeDetails[$intEmployeeId]['employee_name'] );
			$arrobjTaskObjects['tasks'][] = $objTaskClone;

			$objTaskReferenceClone	= $objTaskClone->createTaskReference( $intEmployeeId, CTaskReferenceType::L2_L3_INDUCTION );
			$objTaskReferenceClone->setCreatedBy( CUser::ID_SYSTEM );
			$objTaskReferenceClone->setCreatedOn( 'NOW()' );
			$arrobjTaskObjects['task_references'][] = $objTaskReferenceClone;

			$arrintTaskWatchListUserIds = trim( implode( '', ( array ) $arrmixEmployeeDetails[$intEmployeeId]['user_ids'] ), '{}' );
			$arrintTaskWatchListUserIds = array_filter( explode( ',', $arrintTaskWatchListUserIds ) );

			if( true == valArr( $arrintTaskWatchListUserIds ) ) {
				foreach( $arrintTaskWatchListUserIds as $intTaskStakeholder ) {
					$objTaskWatchList = $objTaskClone->createTaskWatchList();
					$objTaskWatchList->setUserId( $intTaskStakeholder );
					$objTaskWatchList->setTaskId( $objTaskClone->getId() );
					$arrobjTaskObjects['task_watchlists'][] = $objTaskWatchList;
				}
			}
		}

		return $arrobjTaskObjects;
	}

	public static function checkIfTaskAlreadyCreated( $intEmployeeId, $intTaskReferenceTypeId, $objDatabase ) {

		$arrobjTaskReferences = \Psi\Eos\Admin\CTaskReferences::createService()->fetchTaskReferencesByTaskReferenceTypeIdByReferenceNumber( $intTaskReferenceTypeId, $intEmployeeId, $objDatabase );
		if( true == valArr( $arrobjTaskReferences ) ) {
			return true;
		}

		return false;
	}

	public function setSupportTicketDetails( $objAdminDatabase, $intUserId, $intTaskStatus = CTaskStatus::UNDER_REVIEW ) {

		$arrobjChildTasks = ( array ) \Psi\Eos\Admin\CTasks::createService()->fetchTasksByParentTaskIdByTaskTypeIds( $this->getId(), NULL, $objAdminDatabase, true );

		if( true == valArr( $arrobjChildTasks ) ) {
			$arrobjTaskAssignment = $arrobjFollowUpNotes = [];
			foreach( $arrobjChildTasks as $objChildTask ) {
				$objChildTask->setTaskstatusId( $intTaskStatus );
				$arrobjTaskAssignment[] = $objChildTask->createTaskAssignment();
				$arrstrFollowUpTime     = CTaskFollowUpDetails::fetchTaskFollowUpDetailsByTaskTypeIdByTaskPriorityIdByTaskStatusId( CTaskType::SUPPORT, $objChildTask->getTaskPriorityId(), $objChildTask->getTaskStatusId(), $objAdminDatabase );

				if( true == valArr( $arrstrFollowUpTime ) ) {
					$strPreviousFollowUpDateTime = $objChildTask->getFollowUpDatetime();
					$objChildTask->setTaskFollowUpDateTime( $arrstrFollowUpTime['follow_up_time'], $objAdminDatabase );
					$objFollowUpNote = $objChildTask->addTaskNoteForTaskFollowUp( $strPreviousFollowUpDateTime, $objChildTask->getFollowUpDatetime(), $intUserId );
					if( true == valObj( $objFollowUpNote, 'CTaskNote' ) ) {
						$arrobjFollowUpNotes[] = $objFollowUpNote;
					}
				}
			}
			if( true == valArr( $arrobjChildTasks ) && false == \Psi\Eos\Admin\CTasks::createService()->bulkUpdate( $arrobjChildTasks, [ 'task_status_id' ], $intUserId, $objAdminDatabase ) ) {
				return false;
			}
			if( true == valArr( $arrobjTaskAssignment ) && false == \Psi\Eos\Admin\CTaskAssignments::createService()->bulkInsert( $arrobjTaskAssignment, $intUserId, $objAdminDatabase ) ) {
				return false;
			}
			if( true == valArr( $arrobjFollowUpNotes ) && false == CTaskNotes::createService()->bulkInsert( $arrobjFollowUpNotes, $intUserId, $objAdminDatabase ) ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchTaskType( $objDatabase ) {

		return CTaskTypes::fetchTaskTypeById( $this->m_intTaskTypeId, $objDatabase );
	}

	public function fetchPsProduct( $objDatabase ) {
		return CPsProducts::createService()->fetchPsProductById( $this->m_intPsProductId, $objDatabase );
	}

	public function fetchSubTasks( $objDatabase ) {
		return \Psi\Eos\Admin\CTasks::createService()->fetchTasksByParentTaskId( $this->m_intId, $objDatabase );
	}

	public function fetchTaskNotes( $objDatabase, $boolIsShowHiddenData = false, $boolShowEmailNotes = true ) {
		return CTaskNotes::createService()->fetchAllTaskNotesByTaskId( $this->m_intId, $objDatabase, $boolIsShowHiddenData, $boolShowEmailNotes );
	}

	public function fetchTaskAttachments( $objDatabase ) {
		return \Psi\Eos\Admin\CTaskAttachments::createService()->fetchTaskAttachmentsByTaskId( $this->m_intId, $objDatabase );
	}

	public function fetchTaskReleases( $objDatabase ) {
			return CTaskReleases::createService()->fetchTaskAttachmentsByTaskId( $this->m_intId, $objDatabase );
	}

	public function fetchTaskNoteById( $intTaskNoteId, $objDatabase ) {
		return CTaskNotes::createService()->fetchTaskNoteByIdByTaskId( $intTaskNoteId, $this->m_intId, $objDatabase );
	}

	public function fetchTaskNotesWithAttachemnt( $objDatabase ) {
		return CTaskNotes::createService()->fetchTaskNotesWithAttachemnt( $this->m_intId, $objDatabase );
	}

	public function fetchAllTaskAndTaskNoteAttachments( $objDatabase ) {
		return \Psi\Eos\Admin\CTaskAttachments::createService()->fetchAllTaskAndTaskNoteAttachments( $this->m_intId, $objDatabase );
	}

	public function fetchTaskWatchLists( $objDatabase ) {
		return \Psi\Eos\Admin\CTaskWatchlists::createService()->fetchTaskWatchlistsByTaskId( $this->m_intId, $objDatabase );
	}

	public function fetchTaskCompanies( $objDatabase ) {
		return CTaskCompanies::createService()->fetchTaskCompaniesByTaskId( $this->m_intId, $objDatabase );
	}

	public function fetchTaskSvnFiles( $objDatabase ) {
		return CTaskSvnFiles::fetchTaskSvnFilesByTaskId( $this->m_intId, $objDatabase );
	}

	public function fetchTaskServers( $objDatabase ) {
		return CTaskServers::fetchTaskServersByTaskId( $this->m_intId, $objDatabase );
	}

	public function fetchTaskAssignments( $objDatabase ) {
		return \Psi\Eos\Admin\CTaskAssignments::createService()->fetchTaskAssignmentsByTaskId( $this->m_intId, $objDatabase );
	}

	public function fetchTaskCompanyByCid( $intCid, $objDatabase ) {
		return CTaskCompanies::createService()->fetchTaskCompanyByCidByTaskId( $intCid, $this->m_intId, $objDatabase );
	}

	public function fetchTaskCompanyLastUpdatedByCid( $intCid, $objDatabase ) {
		return CTaskCompanies::createService()->fetchTaskCompanyLastUpdatedByTaskIdByCid( $this->m_intId, $intCid, $objDatabase );
	}

	public function fetchTaskDetail( $objDatabase ) {
		return \Psi\Eos\Admin\CTaskDetails::createService()->fetchTaskDetailByTaskId( $this->getId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valQaApproved() {

		$boolIsValid = true;

		 if( true == is_null( $this->getQaApprovedBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_qa_approved', 'Please select QA Approved checkbox.' ) );
		 }

		return $boolIsValid;
	}

	public function valTaskTypeId( $boolIsCheckSubTasksRequired = false, $objAdminDatabase = NULL ) {

		$boolIsValid = true;

		if( true == is_null( $this->getTaskTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_type_id', 'Task Type is required.' ) );

		} elseif( true == $boolIsCheckSubTasksRequired && true == valObj( $objAdminDatabase, 'CDatabase' ) ) {
			$arrintTaskTypeIds = array( CTaskType::FEATURE, CTaskType::BUG, CTaskType::NPS );
			$arrobjSubTasks = \Psi\Eos\Admin\CTasks::createService()->fetchTasksByParentTaskIdByTaskTypeIds( $this->getId(), $arrintTaskTypeIds, $objAdminDatabase );
			if( true == valArr( $arrobjSubTasks ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_type_id', 'Task is associated with Bug/Feature type sub tasks.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valTaskStatusId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskStatusId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_status_id', 'Status is required.' ) );
		}

		return $boolIsValid;
	}

	public function valShareWithClients() {
		$boolIsValid = true;
		if( true == is_null( $this->getCid() ) && true == $this->getIsPublished() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_name', 'Client name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTaskPriorityId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskPriorityId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_priority_id', 'Priority is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUserId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Assigned To is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDueDate() {
		$boolIsValid = true;

		if( 'mm/dd/yyyy' == $this->getDueDate() || '' == $this->getDueDate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', 'Due date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContactEmailAddress() {

		$boolIsValid = true;

		if( true == is_null( $this->getContactEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email_address', __( 'Contact email address is required.' ) ) );

		} elseif( false == CValidation::validateEmailAddresses( $this->getContactEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email_address', __( 'Invalid contact email address.' ) ) );
		}

		return $boolIsValid;
	}

	public function valContactPhoneNumber() {

		$boolIsValid = true;

		if( true == is_null( $this->getContactPhoneNumber() ) && false == is_null( $this->getContactPhoneNumberTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_phone_number', __( 'Contact phone number is required.' ) ) );
		}

		if( false == is_null( $this->getContactPhoneNumber() ) && ( ( \Psi\CStringService::singleton()->strlen( $this->getContactPhoneNumber() ) < 10 ) || false == ( CValidation::validateFullPhoneNumber( $this->getContactPhoneNumber(), false ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_phone_number', __( 'Invalid contact phone number.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPhoneExtension() {

		$boolIsValid = true;

		if( false == is_null( $this->getPhoneExtension() ) && false == is_numeric( $this->getPhoneExtension() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', __( 'Invalid Extension.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTitle( $strValidationForField = NULL, $boolEscalate = false ) {

		$boolIsValid = true;
		$arrintWithoutTitleTaskTypes = array(
			CTaskType::BUG,
			CTaskType::FEATURE,
			CTaskType::TO_DO,
			CTaskType::PROJECT
		);
		if( true == $boolEscalate ) {
			$arrintWithoutTitleTaskTypes = [];
		}

		if( false == is_null( $this->getTaskTypeId() ) && false == in_array( $this->getTaskTypeId(), $arrintWithoutTitleTaskTypes ) && true == is_null( $this->getTitle() ) ) {
			 $boolIsValid = false;
			 $strValidationForField = ( true == is_null( $strValidationForField ) ) ? 'Title' : $strValidationForField;
			 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', $strValidationForField . ' ' . __( 'is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDurationMinutes() {
		// $boolIsValid = parent::valDurationMinutes();

		$boolIsValid = true;

		if( true == $this->getIsSupportTicket() && $this->getTaskStatusId() == CTaskStatus::COMPLETED && CTaskType::SUPPORT == $this->getTaskTypeId() ) {

			if( true == is_null( $this->getDurationMinutes() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'duration_minutes', 'Support duration in Min(s) is required.' ) );
			} elseif( false == is_numeric( $this->getDurationMinutes() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'duration_minutes', 'Invalid support duration in Min(s).' ) );
			}

		}

		return $boolIsValid;
	}

	public function valTaskReleaseId( $objDatabase = NULL, $boolIsRequired = false ) {

		$boolIsValid = true;

		if( CTaskStatus::READY_FOR_DEV == $this->getTaskStatusId() ) {
			return true;
		}

		if( true == $boolIsRequired ) {

			if( true == is_null( $this->getTaskReleaseId() ) && ( true == in_array( $this->getTaskTypeId(), [ CTaskType::BUG, CTaskType::DESIGN, CTaskType::SCHEMA_CHANGE, CTaskType::FEATURE, CTaskType::AUTOMATION, CTaskType::TEST_CASES, CTaskType::PROJECT ] ) ) && false == in_array( $this->getTaskStatusId(), array( CTaskStatus::UNUSED, CTaskStatus::UNDER_REVIEW, CTaskStatus::ON_HOLD, CTaskStatus::CANCELLED, CTaskStatus::AWAITING_CUSTOMER_RESPONSE, CTaskStatus::AWAITING_RESPONSE, CTaskStatus::BACKLOG ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_release_id', 'Release date is required.' ) );
			}
		}

		if( false == is_null( $objDatabase ) && false == is_null( $this->getTaskReleaseId() ) ) {
			$objTaskRelease = CTaskReleases::createService()->fetchTaskReleaseById( $this->getTaskReleaseId(), $objDatabase );

			if( false == in_array( $this->getTaskTypeId(), array( CTaskType::QUESTION_RESEARCH, CTaskType::OUTAGE, CTaskType::IT ) ) && false == in_array( $this->getTaskStatusId(), array( CTaskStatus::CANCELLED, CTaskStatus::COMPLETED, CTaskStatus::READY_FOR_RELEASE, CTaskStatus::RELEASED_ON_RAPID, CTaskStatus::RELEASED_ON_STANDARD, CTaskStatus::VERIFIED_ON_RAPID, CTaskStatus::VERIFIED_ON_STANDARD, CTaskStatus::COMMITTED ) )
				&& false == is_null( $objTaskRelease ) && 1 == $objTaskRelease->getIsReleased() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_release_id', 'The selected sprint is marked as Released.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		$strDescription	= str_replace( '&nbsp;', '', str_replace( '<br />', '', $this->getDescription() ) );
		$strDescription = strip_tags( html_entity_decode( nl2br( trim( $strDescription ) ), ENT_QUOTES ) );

		if( 0 >= \Psi\CStringService::singleton()->strlen( $strDescription ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		$boolValidDescription = self::validateCreditCardNumber( $this->getDescription() );

		if( false == $boolValidDescription ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Please remove credit card number from task description.' ) );
		}

		return $boolIsValid;
	}

	// step to duplicate is not a database field

	public function valStepToDuplicate() {
		$boolIsValid = true;

		$strStepToDuplicate	= str_replace( '&nbsp;', '', str_replace( '<br />', '', $this->getStepToDuplicate() ) );
		$strStepToDuplicate = strip_tags( html_entity_decode( nl2br( trim( $strStepToDuplicate ) ), ENT_QUOTES ) );

		if( 0 >= \Psi\CStringService::singleton()->strlen( $strStepToDuplicate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'step_to_duplicate', 'Steps to duplicate is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAssociatedTasks( $objAdminDatabase ) {

		$boolIsValid = true;

		$intCountOpenTasks = \Psi\Eos\Admin\CTasks::createService()->fetchCountOpenTasksByParentTaskId( $this->getId(), $objAdminDatabase );

		if( 0 < $intCountOpenTasks ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'You cannot close/delete support contact(s), unless and until all the associated tasks are closed.' ) );
		}

		return $boolIsValid;
	}

	public function valContactNameFirst() {

		$boolIsValid = true;
		if( CClients::$c_intNullCid !== $this->getCid() ) {
			if( true == is_null( $this->getContactNameFirst() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_name_first', __( 'First name is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valContactNameLast() {

		$boolIsValid = true;
		if( CClients::$c_intNullCid !== $this->getCid() ) {
			if( true == is_null( $this->getContactNameLast() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_name_last', __( 'Last name is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valProjectManagerId() {

		$boolIsValid = true;
		if( true == is_null( $this->getProjectManagerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'project_manager_id', 'Product development manager is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId( $objDatabase = NULL, $arrobjPsProducts = NULL ) {

		$boolIsValid = true;

		if( ( false == $this->getIsPsProductNotRequired() && true == in_array( $this->getTaskTypeId(), array( CTaskType::BUG, CTaskType::FEATURE, CTaskType::PROJECT ) ) && false == in_array( $this->getUserId(), CTaskReferenceType::$c_arrintReferenceTypesForHelpdeskSystem ) )
		|| ( CTaskType::SUPPORT == $this->getTaskTypeId() && CTaskMedium::LIVE_CHAT == $this->getTaskMediumId() ) ) {

			if( true == is_null( $this->getPsProductId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', __( 'Product is required.' ) ) );
			}

			if( true == valObj( $objDatabase, 'CDatabase' ) && false == is_null( $this->getPsProductId() ) ) {

				if( false == valArr( $arrobjPsProducts ) ) {
					$arrobjPsProducts = CPsProducts::createService()->fetchAllPsProducts( $objDatabase );
				}

				if( true == valArr( $arrobjPsProducts ) ) {
					if( false == in_array( $this->getPsProductId(), array_keys( $arrobjPsProducts ) ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', __( 'Please Enter Valid Product Id.' ) ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valTaskwithUser() {
		$boolIsValid = true;

		if( CTaskType::FEATURE != $this->getTaskTypeId() && true == in_array( $this->getUserId(), CUser::$c_arrintHelpdeskUsers ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_type_id', 'Only Feature task can be assigned to Helpdesk Task.' ) );
		}
		return $boolIsValid;
	}

	public function valPrimaryContactPhoneNumber() {

		$boolIsValid = true;

		if( false == is_null( $this->getContactPhoneNumber() ) && ( ( \Psi\CStringService::singleton()->strlen( $this->getContactPhoneNumber() ) < 10 ) || false == ( CValidation::validateFullPhoneNumber( $this->getContactPhoneNumber(), false ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_phone_number', 'Invalid contact phone number.' ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryContactPhoneExtension() {

		$boolIsValid = true;

		if( false == is_null( $this->getPhoneExtension() ) && true == is_numeric( $this->getPhoneExtension() ) && ( true == is_null( $this->getContactPhoneNumber() ) || '' == $this->getContactPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_phone_number', 'Contact phone number required.' ) );
		}

		if( false == is_null( $this->getPhoneExtension() ) && false == is_numeric( $this->getPhoneExtension() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', 'Invalid Extension.' ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryContactEmailAddress() {

		$boolIsValid = true;

		if( false == is_null( $this->getContactEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->getContactEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email_address', 'Invalid contact email address.' ) );
		}

		return $boolIsValid;
	}

	public function valTicketTitle() {

		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDefectUserId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getUserId() ) && ( CTaskType::DEFECT == $this->getTaskTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Assigned to is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDefectDescription() {

		$boolIsValid = true;

		if( false == valStr( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDefectType() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getSubTaskTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_task_type_id', 'Defect Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDefectStatus() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getTaskStatusId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_status_id', 'Defect Status is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( ' Property is required. ' ) ) );
		}
		return $boolIsValid;
	}

	public function setStepToDuplicate( $strStepToDuplicate ) {
		$this->m_strStepToDuplicate = CStrings::strTrimDef( $strStepToDuplicate, -1, NULL, true );
	}

	public function getStepToDuplicate() {
		return $this->m_strStepToDuplicate;
	}

	public function setIsDescriptionRequired( $boolRequired ) {
		$this->m_boolIsDescriptionRequired = $boolRequired;
	}

	public function getIsDescriptionRequired() {
		return $this->m_boolIsDescriptionRequired;
	}

	public function setIsStepToDuplicateRequired( $boolRequired ) {
		$this->m_boolIsStepToDuplicateRequired = $boolRequired;
	}

	public function getIsStepToDuplicateRequired() {
		return $this->m_boolIsStepToDuplicateRequired;
	}

	public function setClientHtmlTaskContent( $strClientHtmlTaskContent ) {
		$this->m_strClientHtmlTaskContent = $strClientHtmlTaskContent;
	}

	public function getClientHtmlTaskContent() {
		return $this->m_strClientHtmlTaskContent;
	}

	public function setTaskLastUpdatedOn( $strTaskLastUpdatedOn ) {
		$this->m_strTaskLastUpdatedOn = $strTaskLastUpdatedOn;
	}

	public function getTaskLastUpdatedOn() {
		return $this->m_strTaskLastUpdatedOn;
	}

	public function setIsPsProductNotRequired( $boolRequired ) {
		$this->m_boolIsPsProductNotRequired = $boolRequired;
	}

	public function getIsPsProductNotRequired() {
		return $this->m_boolIsPsProductNotRequired;
	}

	public function setIsTaskParentIdNotRequired( $boolRequired ) {
		$this->m_boolIsTaskParentIdNotRequired = $boolRequired;
	}

	public function getIsTaskParentIdNotRequired() {
		return $this->m_boolIsTaskParentIdNotRequired;
	}

	public function setIsSharedWithLimitedClients( $boolIsSharedWithLimitedClients ) {
		$this->m_boolIsSharedWithLimitedClients = $boolIsSharedWithLimitedClients;
	}

	public function getIsSharedWithLimitedClients() {
		return $this->m_boolIsSharedWithLimitedClients;
	}

	public function setTaskAssignmentsTaskStatusId( $intTaskAssignmentsTaskStatusId ) {
		$this->m_intTaskAssignmentsTaskStatusId = $intTaskAssignmentsTaskStatusId;
	}

	public function getTaskAssignmentsTaskStatusId() {
		return $this->m_intTaskAssignmentsTaskStatusId;
	}

	public function setReleaseUpdatedStatusId( $strReleaseUpdatedStatusId ) {
		$this->m_strReleaseUpdatedStatusId = $strReleaseUpdatedStatusId;
	}

	public function getReleaseUpdatedStatusId() {
		return $this->m_strReleaseUpdatedStatusId;
	}

	public function setPendingTaskLastUpdatedOn( $strPendingTaskLastUpdatedOn ) {
		$this->m_strPendingTaskLastUpdatedOn = $strPendingTaskLastUpdatedOn;
	}

	public function getPendingTaskLastUpdatedOn() {
		return $this->m_strPendingTaskLastUpdatedOn;
	}

	/**
	 * Other Functions
	 */

	public function fetchTaskStatuses( $objDatabase, $intTaskStatusType = NULL ) {

		$arrintDevelopmentTaskStatuses = array(
			CTaskType::DESIGN,
			CTaskType::BUG,
			CTaskType::FEATURE,
			CTaskType::AUTOMATION,
			CTaskType::TEST_CASES
		);

		$arrintQuestionResearchTaskStatusesIds = array(
			CTaskStatus::UNUSED,
			CTaskStatus::ACKNOWLEDGED,
			CTaskStatus::ON_HOLD,
			CTaskStatus::IN_PROGRESS,
			CTaskStatus::BACKLOG,
			CTaskStatus::AWAITING_CUSTOMER_RESPONSE,
			CTaskStatus::AWAITING_RESPONSE,
			CTaskStatus::COMPLETED,
			CTaskStatus::CANCELLED
		);

		if( true == in_array( $this->getTaskTypeId(), $arrintDevelopmentTaskStatuses ) ) {
			return \Psi\Eos\Admin\CTaskStatuses::createService()->fetchDevelopmentTaskStatuses( $objDatabase );
		} elseif( $this->getTaskTypeId() == CTaskType::QUESTION_RESEARCH ) {
			return \Psi\Eos\Admin\CTaskStatuses::createService()->fetchTaskStatusesByIds( $arrintQuestionResearchTaskStatusesIds, $objDatabase );
		} else {
			return \Psi\Eos\Admin\CTaskStatuses::createService()->fetchNonDevelopmentTaskStatuses( $objDatabase, $intTaskStatusType );
		}
	}

	public function determineIsCritical() {
		$intOneMonthSeconds = ( 24 * 31 * 60 * 60 );
		if( ( false == is_null( $this->getDueDate() ) && time() > strtotime( $this->getDueDate() ) )
				|| true == is_null( $this->getLastViewedOn() )
				|| $intOneMonthSeconds < ( time() - strtotime( $this->getLastViewedOn() ) ) ) {

			return true;
		}

		return false;
	}

	public function determineTaskTypeName() {
		return CTaskType::$c_arrintTaskTypeName[$this->getTaskTypeId()];
	}

	public function determineTaskStatusName() {
		return CTaskStatus::determineTaskStatusNameByTaskStatusId( $this->getTaskStatusId() );
	}

	public function buildAbbreviatedTitle() {
		$intMaximumAllowableSize = 240;
		return \Psi\CStringService::singleton()->substr( $this->getTitle(), 0, $intMaximumAllowableSize );
	}

	public function buildAbbreviatedDescription() {
		$intMaximumAllowableSize = 220;
		$strTrailingCharacters = '';

		if( \Psi\CStringService::singleton()->strlen( $this->getTitle() ) > $intMaximumAllowableSize ) return NULL;
		if( \Psi\CStringService::singleton()->strlen( $this->getTitle() . $this->getDescription() ) > $intMaximumAllowableSize ) $strTrailingCharacters = '...';

		return \Psi\CStringService::singleton()->substr( $this->getDescription(), 0, $intMaximumAllowableSize - \Psi\CStringService::singleton()->strlen( $this->getTitle() ) ) . $strTrailingCharacters;
	}

	public function buildAbbreviatedTaskDatetime() {
		if( true == is_null( $this->getTaskDatetime() ) ) return NULL;
		return date( 'm/d/y', strtotime( $this->getTaskDatetime() ) );
	}

	public function buildAbbreviatedDueDate() {
		if( true == is_null( $this->getDueDate() ) ) return NULL;
		return date( 'm/d/y', strtotime( $this->getDueDate() ) );
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( CTaskStatus::COMPLETED == $this->m_intTaskStatusId && ( true == is_null( $this->m_intCompletedBy ) || true == is_null( $this->m_strCompletedOn ) ) ) {
			if( true == is_null( $this->m_intCompletedBy ) ) $this->m_intCompletedBy = $intCurrentUserId;
			if( true == is_null( $this->m_strCompletedOn ) ) $this->m_strCompletedOn = date( 'm/d/Y H:i:s' );
		}

		$this->setDescription( str_replace( '\\', '/', $this->getDescription() ) );

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $intCompletedBy = NULL, $strCompletedOn = NULL, $boolReturnSqlOnly = false ) {

		if( CTaskStatus::COMPLETED == $this->m_intTaskStatusId && ( true == is_null( $this->m_intCompletedBy ) || true == is_null( $this->m_strCompletedOn ) ) ) {

			if( true == is_null( $this->m_intCompletedBy ) ) $this->m_intCompletedBy = $intCurrentUserId;
			if( true == is_null( $this->m_strCompletedOn ) ) $this->setCompletedOn( 'NOW()' );
		}

		if( CTaskStatus::COMPLETED != $this->m_intTaskStatusId && ( false == is_null( $this->m_intCompletedBy ) || false == is_null( $this->m_strCompletedOn ) ) ) {
			$this->m_intCompletedBy = NULL;
			$this->m_strCompletedOn = NULL;
		}

		if( 0 < \Psi\CStringService::singleton()->strlen( trim( $intCompletedBy ) ) ) {
			$this->m_intCompletedBy = $intCompletedBy;
		}

		if( 0 < \Psi\CStringService::singleton()->strlen( trim( $strCompletedOn ) ) ) {
			$this->m_strCompletedOn = $strCompletedOn;
		}

		if( CTaskStatus::COMPLETED == $this->m_intTaskStatusId && $this->getTaskTypeId() == CTaskType::QUESTION_RESEARCH ) {
			$objTaskRelease = CTaskReleases::createService()->fetchNearestTaskReleaseByTaskCompletionDate( $this->m_strCompletedOn, $objDatabase );
			$this->setTaskReleaseId( $objTaskRelease->getId() );
		}

		$this->setDescription( str_replace( '\\', '/', $this->getDescription() ) );

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getIsRestored() ) {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );
		} else {
			$this->setDeletedBy( NULL );
			$this->setDeletedOn( NULL );
		}

		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( date( 'm/d/Y H:i:s' ) );

		$this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		return true;
	}

	public function close( $intCurrenUserId, $objAdminDatabase ) {

		$this->setClosedBy( $intCurrenUserId );
		$this->setClosedOn( 'NOW()' );
		$this->setTaskStatusId( CTaskStatus::COMPLETED );

		return $this->update( $intCurrenUserId, $objAdminDatabase );
	}

	public function createTaskValidator() {

		$objTaskValidator = new CTaskValidator();
		$objTaskValidator->setTask( $this );

		return $objTaskValidator;
	}

	public function validate( $strAction, $boolIsRequired = false, $objAdminDatabase = NULL, $objUser = NULL, $arrobjPsProducts = NULL, $boolReferenceIdTask = false ) {

		$boolIsValid					= true;
		$boolIsCheckSubTasksRequired 	= false;

		switch( $strAction ) {

			case VALIDATE_UPDATE:
				if( false == in_array( $this->getTaskTypeId(), array( CTaskType::PROJECT, CTaskType::SUPPORT, CTaskType::FEATURE, CTaskType::BUG, CTaskType::NPS, CTaskType::TO_DO, CTaskType::MIGRATION ) ) && false == $boolReferenceIdTask ) {
					$boolIsCheckSubTasksRequired = true;
				}

				if( CTaskType::SCHEMA_CHANGE == $this->getTaskTypeId() && CTaskStatus::SUBMITTED_FOR_APPROVAL == $this->getTaskStatusId() ) {
					$arrintDOEandArchitectEmployeeIds = array_column( CEmployees::fetchEmployeesDataByDesignationIds( CDesignation::$c_arrintSchemaApprovalDesignationIds, $objAdminDatabase ), 'user_id' );
					$objAssignedUser = CUsers::fetchUserById( $this->getUserId(), $objAdminDatabase );
					$arrobjUsers = \Psi\Eos\Admin\CUsers::createService()->fetchUsersByRoleId( CRole::SCHEMA_CHANGE_ACCESS, $objAdminDatabase );

					if( false == $objAssignedUser->getIsSuperUser() && false == in_array( $this->getUserId(), $arrintDOEandArchitectEmployeeIds ) && false == valArrKeyExists( $arrobjUsers, $this->getUserId() ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'User Assigned must be Architect or DOE/who is having access to Schema Change Approval role.' ) );
						$boolIsValid &= false;
					}
				}

				if( false == $boolReferenceIdTask ) {
					$boolIsValid &= $this->valTaskwithUser();
				}
			case VALIDATE_INSERT:
				if( false == $boolReferenceIdTask ) {
					$boolIsValid &= $this->valPsProductId( $objAdminDatabase, $arrobjPsProducts );
				}

			case 'insert_post_scheduled_task_script':
			case 'insert_sub_task':
			case 'update_sub_task':
				$boolIsValid &= $this->valTitle();

				if( true == $this->getIsDescriptionRequired() ) {
					$boolIsValid &= $this->valDescription();
				}

				if( true == $this->getIsStepToDuplicateRequired() ) {
					$boolIsValid &= $this->valStepToDuplicate();
				}

				if( true == valObj( $objUser, 'CUser' ) && ( true == in_array( CGroup::QA, $objUser->getAssociatedGroupIds() ) || true == in_array( CGroup::DEVELOPMENT, $objUser->getAssociatedGroupIds() ) ) && true == in_array( $this->getTaskStatusId(), array( CTaskStatus::READY_FOR_RELEASE, CTaskStatus::VERIFIED_ON_RAPID, CTaskStatus::VERIFIED_ON_STANDARD, CTaskStatus::VERIFIED_ON_STAGE ) ) && false == in_array( $this->getTaskTypeId(), array( CTaskType::AUTOMATION, CTaskType::TEST_CASES, CTaskType::MIGRATION ) ) ) {
					$boolIsValid &= $this->valQaApproved();
				}

				$boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valTaskPriorityId();
				$boolIsValid &= $this->valTaskTypeId( $boolIsCheckSubTasksRequired, $objAdminDatabase );
				$boolIsValid &= $this->valTaskStatusId();
				// $boolIsValid &= $this->valDurationMinutes();
				$boolIsValid &= $this->valShareWithClients();
				if( false == $boolReferenceIdTask ) {
					$boolIsValid &= $this->valTaskReleaseId( $objAdminDatabase, $boolIsRequired );
				}
				break;

			case 'insert_feature_sub_task':
				$boolIsValid &= $this->valPsProductId( $objAdminDatabase, $arrobjPsProducts );
				$boolIsValid &= $this->valTaskReleaseId( $objAdminDatabase, $boolIsRequired );
				break;

			case 'insert_support_contact_task':
			case 'update_support_contact_task':
				$boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valTaskPriorityId();
				$boolIsValid &= $this->valTaskTypeId();
				$boolIsValid &= $this->valTaskStatusId();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valContactPhoneNumber();
				break;

			case 'insert_ticket_and_task':
				if( true == $boolIsRequired ) {
					$boolIsValid &= $this->valTitle( $strValidationForField = __( 'Subject ' ) );
				}

				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valContactNameFirst();
				$boolIsValid &= $this->valContactNameLast();
				$boolIsValid &= $this->valContactEmailAddress();
				$boolIsValid &= $this->valPhoneExtension();

				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valTaskTypeId();

			case 'update_ticket_and_task':
				$boolIsValid &= $this->valTaskPriorityId();
				$boolIsValid &= $this->valTaskStatusId();
				break;

			case 'update_ticket':
				$boolIsValid &= $this->valContactNameFirst();
				$boolIsValid &= $this->valContactNameLast();
				$boolIsValid &= $this->valContactEmailAddress();
				break;

			case 'close_support_contact_task':
			case 'delete_support_contact_task':
				$boolIsValid &= $this->valAssociatedTasks( $objAdminDatabase );
				break;

			case 'insert_root_cause_type':
			case 'update_root_cause_type':
				$boolIsValid &= $this->valTaskTypeId();
				$boolIsValid &= $this->valTaskStatusId();
				$boolIsValid &= $this->valTaskPriorityId();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valUserId();
				break;

			case 'insert_to_do_task':
				$boolIsValid &= $this->valDueDate();
			case 'insert_to_do_task_from_dashboard':
				$boolIsValid &= $this->valUserId();
				if( true == $this->getIsDescriptionRequired() ) {
					$boolIsValid &= $this->valDescription();
				}
				if( true == $boolIsRequired ) {
					$boolIsValid &= $this->valDueDate();
				}
				$boolIsValid &= $this->valTaskTypeId( $boolIsCheckSubTasksRequired, $objAdminDatabase );
				$boolIsValid &= $this->valTaskStatusId();
				break;

			case 'update_task_primary_contact':
				$boolIsValid &= $this->valPrimaryContactPhoneNumber();
				$boolIsValid &= $this->valPrimaryContactPhoneExtension();
				$boolIsValid &= $this->valPrimaryContactEmailAddress();
				break;

			case 'insert_va_ticket':
				$boolIsValid &= $this->valTicketTitle();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valContactNameFirst();
				$boolIsValid &= $this->valContactNameLast();
				$boolIsValid &= $this->valContactEmailAddress();
				$boolIsValid &= $this->valContactPhoneNumber();
				$boolIsValid &= $this->valPhoneExtension();
				break;

			case 'validate_defects':
				$boolIsValid &= $this->valDefectDescription();
				$boolIsValid &= $this->valDefectType();
				$boolIsValid &= $this->valDefectStatus();
				$boolIsValid &= $this->valDefectUserId();
				break;

			case VALIDATE_COMPANY_DOMAIN_INSERT:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valTaskTypeId();
				break;

			case 'resolve_ticket_and_task':
			case VALIDATE_DELETE:
			default:
		}

		return $boolIsValid;
	}

	public function sendEmailAlert( $objLoggedInUser, $objDatabase, $arrobjOldTaskCompanies = NULL, $boolIsRequestFromEntrata = false, $boolIsFromUpdateTask = false, $strTaskTypePriority = NULL, $strSecureBaseUrl = NULL, $objObjectStorageGateway = NULL ) {
		if( ( false == $boolIsRequestFromEntrata && false == valObj( $objLoggedInUser, 'CUser' ) )
		    || ( true == $boolIsRequestFromEntrata && false == valObj( $objLoggedInUser, 'CCompanyUser' ) )
		    || ( CTaskType::RP_FEEDBACK == $this->getTaskTypeId() ) ) {
			return true;
		}

		if( false == is_null( $objObjectStorageGateway ) ) {
			$this->m_objObjectStorageGateway = $objObjectStorageGateway;
		}

		$arrintClientAdminUserIds        = [];
		$arrstrClientAdminEmailAddresses = [];

		// if only new PSI task watchlists has been added, but no other change has been made in task, then only new task watchlist should get email update.
		if( true == valArr( $this->getNewTaskWatchlistsUserIds() ) ) {
			$arrintClientAdminUserIds = $this->getNewTaskWatchlistsUserIds();

			// Fetch email addresses of users to trigger emails.
			$arrobjEmployees = CEmployees::fetchActiveEmployeesByUserIds( $arrintClientAdminUserIds, $objDatabase );

			if( true == valArr( $arrobjEmployees ) ) {
				$arrobjEmployeePreference = rekeyObjects( 'EmployeeId', CEmployeePreferences::fetchEmployeePreferenceByEmployeeIdsByKey( array_keys( $arrobjEmployees ), 'EMAIL_TASK_UPDATES_TO_SELF', NULL, $objDatabase ) );

				foreach( $arrobjEmployees as $objEmployee ) {
					if( true == valStr( $objEmployee->getEmailAddress() ) ) {
						$strEmailAddress = \Psi\CStringService::singleton()->strtolower( trim( $objEmployee->getEmailAddress() ) );
						// If the looged in user does not want an email for his own task updates, do not include.
						if( $objLoggedInUser->getId() != $objEmployee->getUserId() || ( true == valArr( $arrobjEmployeePreference ) && true == array_key_exists( $objEmployee->getId(), $arrobjEmployeePreference ) && true == $arrobjEmployeePreference[$objEmployee->getId()]->getValue() ) ) {
							$arrstrClientAdminEmailAddresses[$objEmployee->getUserId()] = $strEmailAddress;
						}
					}
				}
			}
		} else {

			// Client Admin - Task Creator User
			if( false == is_null( $this->getCreatedBy() ) ) {
				$arrintClientAdminUserIds[$this->getCreatedBy()] = $this->getCreatedBy();
			}

			if( false == $boolIsRequestFromEntrata ) {
				// Client Admin - Logged In User
				$arrintClientAdminUserIds[$objLoggedInUser->getId()] = $objLoggedInUser->getId();
			}

			// Client Admin - Assign to user.
			$arrintClientAdminUserIds[$this->getUserId()] = $this->getUserId();

			// Client Admin Users - Processing watchlist Users
			$arrobjTaskWatchlists = \Psi\Eos\Admin\CTaskWatchlists::createService()->fetchAllWatchlistUsersByTaskId( $this->getId(), $objDatabase );

			if( true == valArr( $arrobjTaskWatchlists ) ) {
				foreach( $arrobjTaskWatchlists as $objTaskWatchlist ) {
				    if( true == valId( $objTaskWatchlist->getUserId() ) ) {
                        $arrintClientAdminUserIds[$objTaskWatchlist->getUserId()] = $objTaskWatchlist->getUserId();
                    }
				}
			}

			// Fetch email addresses of users to trigger emails.
			$arrobjEmployees = CEmployees::fetchActiveEmployeesByUserIds( $arrintClientAdminUserIds, $objDatabase );

			if( true == valArr( $arrobjEmployees ) ) {
				$arrobjEmployeePreference = rekeyObjects( 'employeeId', CEmployeePreferences::fetchEmployeePreferenceByEmployeeIdsByKey( array_keys( $arrobjEmployees ), 'EMAIL_TASK_UPDATES_TO_SELF', NULL, $objDatabase ) );

				foreach( $arrobjEmployees as $objEmployee ) {
					if( true == valStr( $objEmployee->getEmailAddress() ) ) {
						$strEmailAddress = \Psi\CStringService::singleton()->strtolower( trim( $objEmployee->getEmailAddress() ) );
						if( $objLoggedInUser->getId() == $objEmployee->getUserId() && ( false == valArr( $arrobjEmployeePreference ) || ( true == valArr( $arrobjEmployeePreference ) && false == array_key_exists( $objEmployee->getId(), $arrobjEmployeePreference ) ) || ( true == valArr( $arrobjEmployeePreference ) && true == array_key_exists( $objEmployee->getId(), $arrobjEmployeePreference ) && false == $arrobjEmployeePreference[$objEmployee->getId()]->getValue() ) ) ) {
							continue;
						}

						$arrstrClientAdminEmailAddresses[$objEmployee->getUserId()] = $strEmailAddress;
					}
				}
			}

			// Client Admin Users - Processing Project Development Manager and Client Resolution Specialist.
			$arrintManagerIds = [];

			if( false == is_null( $this->getProjectManagerId() ) ) {
				$arrintManagerIds[] = $this->getProjectManagerId();
			}

			if( false == is_null( $this->getClientResolutionEmployeeId() ) ) {
				$arrintManagerIds[] = $this->getClientResolutionEmployeeId();
			}

			if( true == valArr( $arrintManagerIds ) ) {
				$arrobjEmployees = CEmployees::fetchActiveEmployeesByIds( $arrintManagerIds, $objDatabase );
				if( true == valArr( $arrobjEmployees ) ) {
					foreach( $arrobjEmployees as $objEmployee ) {
						$strEmailAddress = \Psi\CStringService::singleton()->strtolower( trim( $objEmployee->getEmailAddress() ) );
						$objUser         = $objEmployee->fetchUser( $objDatabase );
						$intUserId       = $objUser->getId();
						if( true == valStr( $strEmailAddress ) ) {
							$arrstrClientAdminEmailAddresses[$intUserId] = $strEmailAddress;
						}
					}
				}
			}

		}

		// Array to decide whether mail content is required to prepare for client or not;
		$arrintPrepareEmailContentForCids = [];

		// Processing Associated clients and Processing Client Admin Users( Client Executives ) and PSadmin users email address.
		$arrintSendEmailToCids = $this->getSendEmailToCids();

		if( true == valArr( $arrintSendEmailToCids ) && ( true == is_null( $this->getIsPublished() ) || true == $this->getIsPublished() ) ) {

			// Sending email to Implementation manager if task type is support/bug and task priority is immediate/urgent/high.
			$boolIsSentToImplementationManager = false;

			if( true == in_array( $this->getTaskTypeId(), [ CTaskType::SUPPORT, CTaskType::BUG ] ) && true == in_array( $this->getTaskPriorityId(), [ CTaskPriority::IMMEDIATE, CTaskPriority::URGENT, CTaskPriority::HIGH ] ) ) {
				$boolIsSentToImplementationManager = true;
			}

			$arrstrSupportAndImplementationEmployeesEmailAddresses = CEmployees::fetchSupportAndImplementationEmployeesByCids( $arrintSendEmailToCids, $objDatabase, $boolIsSentToImplementationManager );
			$arrobjPsLeadDetails                                   = \Psi\Eos\Admin\CPsLeadDetails::createService()->fetchPsLeadDetailsByCids( $arrintSendEmailToCids, $objDatabase );
			$arrobjPsLeadDetails                                   = rekeyObjects( 'Cid', $arrobjPsLeadDetails );
			$arrobjClients                                         = CClients::fetchClientsByIds( $arrintSendEmailToCids, $objDatabase );

			if( true == valArr( $arrobjPsLeadDetails ) && true == valArr( $arrintSendEmailToCids ) && true == valArr( $arrobjClients ) && true == $this->getSendEmailToClients() ) {

				$this->loadClientDatabases( $arrintSendEmailToCids );

				foreach( $arrintSendEmailToCids as $intSendEmailToCid ) {

					$arrintPrepareEmailContentForCids[$intSendEmailToCid] = false;

					$objPsLeadDetail = $arrobjPsLeadDetails[$intSendEmailToCid];

					if( false == valObj( $objPsLeadDetail, 'CPsLeadDetail' ) ) {
						continue;
					}

					$this->m_arrstrTaskClientsData[$intSendEmailToCid]['PSadmin_email_addresses'] = [];

					$arrstrPersonsEmailAddresses = array_keys( rekeyArray( 'email_address', \Psi\Eos\Admin\CPersons::createService()->fetchActiveSelectedPersonsByPsLeadId( $objPsLeadDetail->getPsLeadId(), $objDatabase ) ) );

					( true == valStr( $objPsLeadDetail->getTaskNotificationEmails() ) ) ? array_push( $arrstrPersonsEmailAddresses, $objPsLeadDetail->getTaskNotificationEmails() ) : NULL;

					// PSadmin users for clients - Task Notification PSadmin users
					if( true == valArr( $arrstrPersonsEmailAddresses ) ) {
						$arrintPrepareEmailContentForCids[$intSendEmailToCid] = true;
						$arrstrTaskNotificationEmailIds                       = array_filter( $arrstrPersonsEmailAddresses );

						$arrstrTempTaskNotificationEmailIds = [];
						foreach( $arrstrTaskNotificationEmailIds as $strTaskTaskNotificationEmailId ) {
							$this->m_arrintCompanyUserId[CUser::ID_ENTRATA_ID]                   = str_replace( ' ', '', $strTaskTaskNotificationEmailId );
							$arrstrTempTaskNotificationEmailIds[$strTaskTaskNotificationEmailId] = str_replace( ' ', '', $strTaskTaskNotificationEmailId );
						}

						$this->m_arrstrTaskClientsData[$intSendEmailToCid]['PSadmin_email_addresses'] = $arrstrTempTaskNotificationEmailIds;
					}

					// PSadmin users for clients - Watchlist PSadmin users
					if( true == array_key_exists( $intSendEmailToCid, $arrobjClients ) && true == valArr( $this->m_arrstrClientCompanyData )
					    && true == valObj( $arrobjClients[$intSendEmailToCid], 'CClient' )
					    && true == isset( $this->m_arrstrClientCompanyData['directives'] )
					    && true == valArr( $this->m_arrstrClientCompanyData['directives'] )
					    && true == isset( $this->m_arrstrClientCompanyData['databases'] )
					    && true == valArr( $this->m_arrstrClientCompanyData['databases'] )
					    && true == array_key_exists( $intSendEmailToCid, $this->m_arrstrClientCompanyData['directives'] ) ) {

						$arrobjDirectives = $this->m_arrstrClientCompanyData['directives'];
						$arrobjDatabases  = $this->m_arrstrClientCompanyData['databases'];
						$intDatabaseId    = $arrobjDirectives[$intSendEmailToCid]->getDatabaseId();

						if( true == array_key_exists( $intDatabaseId, $arrobjDatabases ) ) {
							$objClientDatabase = $arrobjDatabases[$intDatabaseId];
							$objClientDatabase->open();
							$arrstrPSadminWatchLists = $arrobjClients[$intSendEmailToCid]->loadTicketAndTaskWatchlishUsersEmailAddress( $this->getId(), $objClientDatabase );

							if( true == valArr( $arrstrPSadminWatchLists ) ) {
								$arrstrCompanyUserEmailAddress = [];
								foreach( $arrstrPSadminWatchLists as $intUserId => $strPSadminWatchLists ) {
									$arrstrPSadminWatchList = explode( ' - ', $strPSadminWatchLists );

									if( true == valArr( $arrstrPSadminWatchList ) ) {
										if( true == isset( $arrstrPSadminWatchList[1] ) ) {
											array_push( $arrstrCompanyUserEmailAddress, $arrstrPSadminWatchList[1] );
											$this->m_arrintCompanyUserId[$intUserId] = $arrstrPSadminWatchList[1];
										}
									}
								}

								$arrintPrepareEmailContentForCids[$intSendEmailToCid]                         = true;
								$this->m_arrstrTaskClientsData[$intSendEmailToCid]['PSadmin_email_addresses'] = array_merge( $this->m_arrstrTaskClientsData[$intSendEmailToCid]['PSadmin_email_addresses'], $arrstrCompanyUserEmailAddress );
							}
							// If Mail request comes from Entrata, include logged in company user email in mail list.
							if( true == $boolIsRequestFromEntrata ) {
								// Psadmin User - Processing Logged In Company User
								$objLoggedInCompanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $objLoggedInUser->getCompanyEmployeeId(), $this->getCid(), $objClientDatabase );

								if( true == valObj( $objLoggedInCompanyEmployee, 'CCompanyEmployee' ) && true == valStr( $objLoggedInCompanyEmployee->getEmailAddress() ) ) {
									$arrstrLoggedInCompanyEmployee[$objLoggedInCompanyEmployee->getEmailAddress()] = $objLoggedInCompanyEmployee->getEmailAddress();
									$this->m_arrintCompanyUserId[$objLoggedInUser->getId()]                        = $objLoggedInCompanyEmployee->getEmailAddress();
									$this->m_arrstrTaskClientsData[$intSendEmailToCid]['PSadmin_email_addresses']  = array_merge( $this->m_arrstrTaskClientsData[$intSendEmailToCid]['PSadmin_email_addresses'], $arrstrLoggedInCompanyEmployee );
									$arrintPrepareEmailContentForCids[$intSendEmailToCid]                          = true;
								}
							}

							$objClientDatabase->close();
						}
					}
				}
			}

			// PSadmin user - Task Contact detail.
			if( true == valStr( $this->getContactEmailAddress() ) && true == $this->getSendEmailToClients() ) {

				// if task is created from PSadmin
				if( CClients::$c_intNullCid !== $this->getCid() ) {
					$intContactCid = $this->getCid();
				}
				$intPropertyId = $this->getPropertyId();
				if( false == empty( $intPropertyId ) ) {
					$objProperty = \Psi\Eos\Admin\CProperties::createService()->fetchCustomPropertyById( $intPropertyId, $objDatabase );
				}
				if( true == empty( $intContactCid ) && false == empty( $intPropertyId ) ) {

					// if task is created from Client Admin and added contact with property.

					if( true == valObj( $objProperty, 'CProperty' ) ) {
						$intContactCid = $objProperty->getCid();
					}
				}

				// sending mail to added email id
				if( true == isset( $intContactCid ) && false == is_null( $intContactCid ) && true == in_array( $intContactCid, $arrintSendEmailToCids ) ) {
					$arrstrContactEmailAddress[$this->getContactEmailAddress()]               = $this->getContactEmailAddress();
					$this->m_arrintCompanyUserId[CUser::ID_ENTRATA_ID]                        = $this->getContactEmailAddress();
					$this->m_arrstrTaskClientsData[$intContactCid]['PSadmin_email_addresses'] = array_merge( $this->m_arrstrTaskClientsData[$intContactCid]['PSadmin_email_addresses'], $arrstrContactEmailAddress );
					$arrintPrepareEmailContentForCids[$intContactCid]                         = true;
				}
			}
		}

		// Client Admin Users - Client Executives for Associated clients
		if( true == isset( $arrstrSupportAndImplementationEmployeesEmailAddresses ) && true == valArr( $arrstrSupportAndImplementationEmployeesEmailAddresses ) && ( true == is_null( $this->getIsPublished() ) || true == $this->getIsPublished() ) ) {
			foreach( $arrstrSupportAndImplementationEmployeesEmailAddresses as $strEmailAddress ) {
				$strEmailAddress                                   = \Psi\CStringService::singleton()->strtolower( trim( $strEmailAddress['email_address'] ) );
				$arrstrClientAdminEmailAddresses[$strEmailAddress] = $strEmailAddress;
			}
		}

		$strTaskType         = \Psi\CStringService::singleton()->preg_replace( '#\s|\-|\/#', '_', \Psi\CStringService::singleton()->strtoupper( CTaskType::$c_arrintTaskTypeName[$this->getTaskTypeId()] ) );
		$strTaskPriority     = \Psi\CStringService::singleton()->strtoupper( CTaskPriority::$c_arrstrTaskPriorityName[$this->getTaskPriorityId()] );
		$strTaskTypePriority = $strTaskType . '_' . $strTaskPriority;

		$arrintStakeHoldersForProductSecurity = [];

		if( CPsProductOption::SECURITY == $this->getPsProductOptionId() ) {

			$arrintDefaultStakeHolders = array_keys( CUsers::fetchUsersByGroupId( CGroup::SECURITY_TEAM_STAKEHOLDERS, $objDatabase ) );

			if( true == valArr( $arrintDefaultStakeHolders ) ) {
				foreach( $arrintDefaultStakeHolders as $intDefaultStakeHolder ) {
					$arrintStakeHoldersForProductSecurity[$intDefaultStakeHolder] = $intDefaultStakeHolder;
				}
			}
		}

		if( false == is_null( $strTaskTypePriority ) ) {
			$arrmixTaskTypePriorityUserIds = CUserSettings::fetchUserIdByStatusAndKey( $objDatabase, $strTaskTypePriority );
			if( true == valArr( $arrmixTaskTypePriorityUserIds ) ) {
				foreach( $arrmixTaskTypePriorityUserIds as $arrintTaskTypePriorityUser ) {
					if( valArr( $arrintStakeHoldersForProductSecurity ) ) {
						if( false == in_array( $arrintTaskTypePriorityUser['user_id'], $arrintStakeHoldersForProductSecurity ) ) {
							unset( $arrstrClientAdminEmailAddresses[$arrintTaskTypePriorityUser['user_id']] );
						}
					} else {
						unset( $arrstrClientAdminEmailAddresses[$arrintTaskTypePriorityUser['user_id']] );
					}
				}
			}
		}

		// Validate task and task note attached file size
		$boolIsAttachFileSizeExceeded = $this->claculateTaskAndTaskNoteAttachementFileSize( $objDatabase, $objLoggedInUser );

		$arrstrLocales              = [];
		$arrstrTempTaskClientEmails = [];
		$intCid                     = $this->getCid();
		$objClientDatabase          = NULL;
		if( true == valId( $intCid ) ) {
			if( true == valArr( $this->m_arrstrTaskClientsData[$intContactCid]['PSadmin_email_addresses'] ) ) {
				$arrstrTempTaskClientEmails = array_unique( array_map( 'strtolower', array_map( 'trim', $this->m_arrstrTaskClientsData[$intContactCid]['PSadmin_email_addresses'] ) ) );
			}
			$this->loadClientDatabases( $arrintSendEmailToCids );
			$arrobjDirectives        = $this->m_arrstrClientCompanyData['directives'];
			$arrobjDatabases         = $this->m_arrstrClientCompanyData['databases'];
			if( true == valObj( $arrobjDirectives[$intCid], 'CDirective' ) ) {
				$intDatabaseId = $arrobjDirectives[$intCid]->getDatabaseId();
				if( isset( $arrobjDatabases[$intDatabaseId] ) && true == valObj( $arrobjDatabases[$intDatabaseId], 'CDatabase' ) ) {
					$objClientDatabase = $arrobjDatabases[$intDatabaseId];
					$objClientDatabase->open();
					foreach( $arrstrTempTaskClientEmails as $arrstrTempTaskClientEmail ) {
						$objUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByEmailAddressByCid( $arrstrTempTaskClientEmail, $intContactCid, $objClientDatabase );
						if( true == valObj( $objUser, 'CCompanyUser' ) ) {
							$this->m_arrstrCompanyUserEmailAddressesLocale[$arrstrTempTaskClientEmail] = $objUser->getPreferredLocaleCode();
						} else {
							$this->m_arrstrCompanyUserEmailAddressesLocale[$arrstrTempTaskClientEmail] = NULL;
						}
					}
					$objClient = CClients::fetchClientById( $intContactCid, $objClientDatabase );
					if( true == valObj( $objProperty, 'CProperty' ) ) {
						$arrstrLocales[]             = $objProperty->getLocaleCode();
						$this->m_strDefaultLocale = $objProperty->getLocaleCode();
					}
					if( true == valObj( $objClient, 'CClient' ) ) {
						$arrstrLocales[] = $objClient->getLocaleCode();
						if( false == valStr( $this->m_strDefaultLocale ) && true == valStr( $objClient->getLocaleCode() ) ) {
							$this->m_strDefaultLocale = $objClient->getLocaleCode();
						} elseif( false == valStr( $this->m_strDefaultLocale ) && false == valStr( $objClient->getLocaleCode() ) ) {
							$this->m_strDefaultLocale = 'en_US';
						}
					}
					$arrstrLocales              = ( array ) array_unique( array_filter( array_merge( $arrstrLocales, array_values( $this->m_arrstrCompanyUserEmailAddressesLocale ), ( array ) ( $this->m_strDefaultLocale ) ) ) );

				}
			}
		}
		$this->m_strHtmlEmailOutput = $this->prepareTaskEmailHtmlContent( $objLoggedInUser, $objDatabase, $arrstrLocales, $arrintPrepareEmailContentForCids, $arrobjOldTaskCompanies, $boolIsRequestFromEntrata, $boolIsAttachFileSizeExceeded, $strSecureBaseUrl, $objClientDatabase );
		if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {
			$objClientDatabase->close();
		}
		if( false == $this->processTaskSystemEmail( $objLoggedInUser, $arrstrClientAdminEmailAddresses, $boolIsAttachFileSizeExceeded, $boolIsFromUpdateTask, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function prepareTaskEmailHtmlContent( $objLoggedInUser, $objDatabase, $arrstrLocales, $arrintPrepareEmailContentForCids = NULL, $arrobjOldTaskCompanies = NULL, $boolIsRequestFromEntrata = false, $boolIsAttachFileSizeExceeded = false, $strSecureBaseUrl = NULL, $objClientDatabase = NULL ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		// Initializing email body variables with default values
		$objPsProduct 						= NULL;
		$objProperty						= NULL;
		$objPsProductOption 				= NULL;
		$objTaskPriority 					= NULL;
		$objTaskStatus 						= NULL;
		$objTaskType 						= NULL;
		$objTaskRelease 					= NULL;
		$arrobjClients						= array();
		$arrobjPsLeads 						= array();
		$objSmarty 							= new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		// parse inline attachment to attach to an mail
		$this->setDescription( $this->parseTaskInlineAttachments( $this->getDescription() ) );

		if( false == is_null( $this->getPsProductId() ) ) {
			$objPsProduct = CPsProducts::createService()->fetchPsProductById( $this->getPsProductId(), $objDatabase );
		}

		if( false == is_null( $this->getPropertyId() ) ) {
			$objProperty = \Psi\Eos\Admin\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		if( false == is_null( $this->getPsProductOptionId() ) ) {
			$objPsProductOption = CPsProductOptions::createService()->fetchPsProductOptionById( $this->getPsProductOptionId(), $objDatabase );
		}

		if( false == is_null( $this->getTaskPriorityId() ) ) {
			$objTaskPriority = CTaskPriorities::fetchTaskPriorityById( $this->getTaskPriorityId(), $objDatabase );
		}

		if( false == is_null( $this->getTaskStatusId() ) ) {
			$objTaskStatus = \Psi\Eos\Admin\CTaskStatuses::createService()->fetchTaskStatusById( $this->getTaskStatusId(), $objDatabase );
		}

		if( false == is_null( $this->getTaskTypeId() ) ) {
			$objTaskType = CTaskTypes::fetchTaskTypeById( $this->getTaskTypeId(), $objDatabase );
		}

		if( false == is_null( $this->getId() ) ) {
			$objTaskDetail = \Psi\Eos\Admin\CTaskDetails::createService()->fetchTaskDetailByTaskId( $this->getId(), $objDatabase );
		}

		if( false == is_null( $this->getTaskReleaseId() ) ) {
			$objTaskRelease = CTaskReleases::createService()->fetchTaskReleaseById( $this->getTaskReleaseId(), $objDatabase );
		}

		$arrobjUsers				= CUsers::fetchAllUsers( $objDatabase );
		$arrobjEmployees 			= CEmployees::fetchAllEmployees( $objDatabase );
		$arrobjTaskCompanies 		= $this->fetchTaskCompanies( $objDatabase );
		$arrobjTaskNotes 			= $this->fetchTaskNotes( $objDatabase );
		if( true == valArr( $arrobjTaskNotes ) ) {
			foreach( $arrobjTaskNotes as $objTaskNote ) {
				$strTaskNote = $this->parseTaskInlineAttachments( $objTaskNote->getTaskNote() );

				$objTaskNote->setTaskNote( str_replace( '<p>', '<p style="margin:0px;">', $strTaskNote ) );

				if( true == valArr( $arrobjTaskCompanies ) ) {
					foreach( $arrobjTaskCompanies as $objTaskCompany ) {
						if( $objTaskNote->getId() == $objTaskCompany->getTaskNoteId() || true == $objTaskNote->getIsClientVisible() ) {
							$this->m_arrstrTaskClientsData[$objTaskCompany->getCid()]['client_email_task_notes'][$objTaskNote->getId()] = $objTaskNote;
						}
					}
				}
			}
		}

		$this->m_objSubTask = \Psi\Eos\Admin\CTasks::createService()->fetchSubTaskByParentTaskIdByTaskTypeId( $this->getId(), array( CTaskType::AUTOMATION, CTaskType::TEST_CASES ), $objDatabase );

		if( true == valArr( $arrobjTaskCompanies ) ) {
			$arrobjClients 	= CClients::fetchClientsByIds( array_keys( rekeyObjects( 'Cid', $arrobjTaskCompanies ) ), $objDatabase );
			$arrobjPsLeads				= rekeyObjects( 'Cid', CPsLeads::fetchPsLeadsByCids( array_keys( $arrobjClients ), $objDatabase ) );
		}

		$this->unSerializeAndSetOriginalValues();
		$objOldTask 	= unserialize( $this->getSerializedOriginalValues() );

		$this->setDescription( str_replace( '<p>', '<p style="margin:0px;">', $this->m_strDescription ) );

		if( NULL == $strSecureBaseUrl ) {
			$strHostBaseName	= basename( $_SERVER['HTTP_HOST'] );
			$strHttpHost		= ( true === CONFIG_IS_SECURE_URL ) ? 'https://' : 'http://';
			$strLinkTaskUri		= \Psi\CStringService::singleton()->preg_replace( '![a-z0-9]*\.{1}!i', $strHttpHost . 'clientadmin.', $strHostBaseName, 1 );
		} else {
			$strLinkTaskUri		= $strSecureBaseUrl;
		}

		$arrmixTask 		= \Psi\Eos\Admin\CTasks::createService()->fetchTicketStatusByTicketId( $this->getCid(), ( int ) $this->getId(), $objDatabase );
        $arrobjAcceptanceCriteria	= \Psi\Eos\Admin\CTaskAcceptanceCriterias::createService()->fetchAllTaskAcceptanceCriteriasByTaskId( $this->getId(), $objDatabase );

		$arrobjTaskStatusesForQAApproval = array(
			CTaskStatus::READY_FOR_RELEASE,
			CTaskStatus::RELEASED_ON_STAGE,
			CTaskStatus::VERIFIED_ON_STAGE,
			CTaskStatus::VERIFIED_ON_RAPID,
			CTaskStatus::VERIFIED_ON_STANDARD,
			CTaskStatus::COMMITTED,
			CTaskStatus::RELEASED_ON_RAPID,
			CTaskStatus::RELEASED_ON_STANDARD,
			CTaskStatus::COMPLETED
		);

		$arrobjTaskTypesForStoryPoints 	= array(
			CTaskType::BUG,
			CTaskType::FEATURE,
			CTaskType::DESIGN,
			CTaskType::AUTOMATION,
			CTaskType::TEST_CASES
		);

		$objSmarty->assign( 'logged_in_user',						$objLoggedInUser );
		$objSmarty->assign( 'task',									$this );
		$objSmarty->assign( 'old_task', 							$objOldTask );
		$objSmarty->assign( 'ps_product', 							$objPsProduct );
		$objSmarty->assign( 'ps_product_option', 					$objPsProductOption );
		$objSmarty->assign( 'clients', 								$arrobjClients );
		$objSmarty->assign( 'ps_leads', 							$arrobjPsLeads );
		$objSmarty->assign( 'task_priority', 						$objTaskPriority );
		$objSmarty->assign( 'task_type', 							$objTaskType );
		$objSmarty->assign( 'task_release', 						$objTaskRelease );
		$objSmarty->assign( 'task_detail', 							$objTaskDetail );
		$objSmarty->assign( 'task_status',	 						$objTaskStatus );
		$objSmarty->assign( 'task_impact',	 						$this->m_strTaskImpact );
		$objSmarty->assign( 'users', 								$arrobjUsers );
		$objSmarty->assign( 'employees', 							$arrobjEmployees );
		$objSmarty->assign( 'task_notes', 							$arrobjTaskNotes );
		$objSmarty->assign( 'acceptance_criteria', 					$arrobjAcceptanceCriteria );
		$objSmarty->assign( 'task_companies', 						rekeyObjects( 'Cid', $arrobjTaskCompanies ) );
		$objSmarty->assign( 'task_old_companies', 					rekeyObjects( 'Cid', $arrobjOldTaskCompanies ) );
		$objSmarty->assign( 'task_statuses_for_qaapproval', 		$arrobjTaskStatusesForQAApproval );
		$objSmarty->assign( 'task_types_for_story_points', 			$arrobjTaskTypesForStoryPoints );
		$objSmarty->assign( 'sub_task', 							$this->m_objSubTask );
		$objSmarty->assign( 'property', 							$objProperty );

		$objSmarty->assign( 'task_reuqest_uri', 						$strLinkTaskUri );
		$objSmarty->assign( 'USER_ID_ENTRATA_ID', 						CUser::ID_ENTRATA_ID );
		$objSmarty->assign( 'USER_ID_ADMIN', 							CUser::ID_ADMIN );
		$objSmarty->assign( 'USER_ID_SYSTEM', 							CUser::ID_SYSTEM );
		$objSmarty->assign( 'base_uri',									CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		// temparary fixes for job task url
		$objSmarty->assign( 'site_url',									CONFIG_SECURE_HOST_PREFIX . 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN . '/' );

		$objSmarty->assign( 'bool_is_request_from_entrata',				$boolIsRequestFromEntrata );
		$objSmarty->assign( 'bool_is_attach_file_size_exceeded',		$boolIsAttachFileSizeExceeded );
		$objSmarty->assign( 'task_status_cancelled',					CTaskStatus::CANCELLED );
		$objSmarty->assign( 'TASK_TYPE_AUTOMATION',						CTaskType::AUTOMATION );
		$objSmarty->assign( 'TASK_TYPE_TEST_CASES',						CTaskType::TEST_CASES );
		$objSmarty->assign( 'TASK_TYPE_FEATURE',				 		CTaskType::FEATURE );
		$objSmarty->assign( 'TASK_TYPE_NEW',				 			CTaskStatus::UNUSED );
		$objSmarty->assign( 'TASK_TYPE_QUESTION_RESEARCH',				CTaskType::QUESTION_RESEARCH );
		$objSmarty->assign( 'logo_url',									CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX', 				CConfig::get( 'entrata_logo_prefix' ) );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME', 						CONFIG_COMPANY_NAME );
		$objSmarty->assign( 'image_url',								CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		// Mail Body for Client Admin Employees.
		$strHtmlTaskContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'tasks/task_email/task_email_format.tpl', PATH_INTERFACES_ADMIN . 'user_administration/admin_email_template.tpl' );

		// Prepareing Mail Body for PSadmin employees.
		if( ( true == is_null( $this->getIsPublished() ) || true == $this->getIsPublished() ) && true == valArr( $arrintPrepareEmailContentForCids ) ) {
			$objClientFriendlyTaskStatus = CTaskStatus::getClientFriendlyTaskStatus( $objTaskStatus );

			if( true == valArr( $arrmixTask ) && true == isset( $arrmixTask[0]['resolved_on'] ) && true == isset( $arrmixTask[0]['completed_on'] ) ) {
				$objClientFriendlyTaskStatus->setName( 'Completed' );
			}

			$objSmarty->assign( 'client_friendly_task_status', $objClientFriendlyTaskStatus );
			$objSmarty->assign( 'TASK_PRIORITY_IMMEDIATE', CTaskPriority::IMMEDIATE );

			foreach( $arrintPrepareEmailContentForCids as $intCid => $boolIsMailBodyRequired ) {

				if( false == $boolIsMailBodyRequired ) continue;

				$arrobjTaskNotes = array();

				if( true == isset( $this->m_arrstrTaskClientsData[$intCid]['client_email_task_notes'] ) ) {
					$arrobjTaskNotes = $this->m_arrstrTaskClientsData[$intCid]['client_email_task_notes'];
				}

				$objSmarty->assign( 'task_notes', $arrobjTaskNotes );
				$objSmarty->assign( 'current_cid', $intCid );
				$objSmarty->assign( 'image_url',	CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
				if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {
					foreach( $arrstrLocales as $strLocale ) {
						$objLocaleContainer = CLocaleContainer::createService();
						$objClient = CClients::fetchClientById( $intCid, $objClientDatabase );
						$objLocaleContainer->initializeByClient( $objClient, $objClientDatabase );
						$objLocaleContainer->initializeDatabase( $objDatabase );
						$objLocaleContainer->setPreferredLocaleCode( $strLocale, $objClientDatabase );
						$boolIsInternational = CLocaleContainer::createService()->getIsInternational();
						$objSmarty->assign( 'is_international', $boolIsInternational );
						/**
						 *loading toll free number based on locale
						 */
						if( true == valObj( $objClientDatabase, 'CDatabase' ) && true == valId( $this->getCompanyUserId() ) && true == valId( $this->getCid() ) ) {
							$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $this->getCompanyUserId(), $this->getCid(), $objClientDatabase );
							if( true == valObj( $objCompanyUser, 'CCompanyUser' )
							    && true == valId( $this->getTaskTypeId() )
							    && CTaskType::SUPPORT == $this->getTaskTypeId() ) {
								if( true == $objCompanyUser->getIsAdministrator() && false == $objCompanyUser->getIsPSIUser() ) {
									$objCompanyPreference         = $objClient->fetchCompanyPreferencesByKeys( [ 'ENTRATA_HELPDESK_SUPPORT_NUMBER' ], $objClientDatabase );
									$strHelpDeskNumber            = ( true == valArr( $objCompanyPreference ) ) ? current( $objCompanyPreference )->getValue() : '';
									$strWhereClause               = ' WHERE cid = ' . ( int ) $this->getCid() . ' AND ps_product_id =' . \CPsProduct::ENTRATA_HELP_DESK . '';
									$arrobjPropertiesWithHelpdesk = \Psi\Eos\Entrata\CPropertyProducts::createService()->fetchPropertyProductCount( $strWhereClause, $objClientDatabase );
								} else {
									$objCompanyPreference         = $objClient->fetchCompanyPreferencesByKeys( [ 'ENTRATA_HELPDESK_SUPPORT_NUMBER' ], $objClientDatabase );
									$strHelpDeskNumber            = ( true == valArr( $objCompanyPreference ) ) ? current( $objCompanyPreference )->getValue() : '';
									$arrobjPropertiesWithHelpdesk = \Psi\Libraries\UtilFunctions\count( ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyDetailsByCidByCompanyUserIdByProductId( $this->getCid(), $this->getCompanyUserId(), $objClientDatabase ) );
								}
							}
						}
						if( true == isset( $this->m_strEmailFooterHelpLineNumber ) && true == valStr( $this->m_strEmailFooterHelpLineNumber ) ) {
							$objSmarty->assign( 'email_footer_help_line_number', $this->m_strEmailFooterHelpLineNumber );
						} else if( true == valId( $arrobjPropertiesWithHelpdesk ) && true == valStr( $strHelpDeskNumber ) ) {
							$objSmarty->assign( 'email_footer_help_line_number', $strHelpDeskNumber );
						} else if( true == valStr( $objClient->getLocaleCode() ) && true == valStr( $this->getTollFreeNumberByLocal( $objClient->getLocaleCode() ) ) ) {
							$objSmarty->assign( 'email_footer_help_line_number', $this->getTollFreeNumberByLocal( $objClient->getLocaleCode() ) );
						} else {
							$objSmarty->assign( 'email_footer_help_line_number', $this->getTollFreeNumberByLocal( CLocale::DEFAULT_LOCALE ) );
						}

						$strClientHtmlTaskContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'tasks/task_client_email_format.tpl', PATH_INTERFACES_ADMIN . 'common/layouts/ps_email_admin.tpl' );

						$this->m_arrstrTaskClientsData[$intCid]['client_email_html_content'][$strLocale]	= $strClientHtmlTaskContent;
						$objLocaleContainer->unsetPreferredLocaleCode( $objClientDatabase );
					}
				}
			}
		}

		return $strHtmlTaskContent;
	}

	public function processTaskSystemEmail( $objLoggedInUser, $arrstrClientAdminEmailAddresses, $boolIsAttachFileSizeExceeded = false, $boolIsFromUpdateTask = false, $objDatabase = NULL ) {

		$arrobjEmailAttachments 			= array();
		$arrobjClientEmailAttachment 		= array();
		$arrobjCommonClientEmailAttachment	= array();
		$arrstrTaskClientEmails				= array();

		$objEmailDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_DEVELOPER );

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		$strSubject = 'Task ID ' . $this->getId() . ' : ' . $this->getTitle();
		$intCreatedBy 		= CUser::ID_ENTRATA_ID;

		if( true == valObj( $objLoggedInUser, 'CUser' ) ) {
			$intCreatedBy = $objLoggedInUser->getId();
		}

		// Task Email for Client Admin Users
		if( false == $boolIsAttachFileSizeExceeded && true == valArr( $this->m_arrobjTaskAttachments ) ) {

			$boolIsValid            = true;
			$objEmailAttachmentTemp = new CEmailAttachment();

			foreach( $this->m_arrobjTaskAttachments as $objTaskAttachment ) {
				$objEmailAttachment = clone $objEmailAttachmentTemp;
				$objTaskAttachment->setObjectStorageGateway( $this->m_objObjectStorageGateway );
				$strAttachmentFilePath = $objTaskAttachment->getFileTmpName() ?? $objTaskAttachment->downloadTaskAttachment( $intCreatedBy, $objDatabase, true );

				if( !$strAttachmentFilePath ) {
					continue;
				}

				$objEmailAttachment->setFilePath( dirname( $strAttachmentFilePath ) );
				$objEmailAttachment->setFileName( basename( $strAttachmentFilePath ) );
				$arrobjEmailAttachments[] = $objEmailAttachment;

				if( false == is_null( $objTaskAttachment->getTaskNoteId() ) ) {
					$arrobjClientEmailAttachment[$objTaskAttachment->getTaskNoteId()] = $objEmailAttachment;
				} else {
					$arrobjCommonClientEmailAttachment[] = $objEmailAttachment;
				}
			}
		}

		$intIsSelfDestruct = ( CTaskType::TO_DO == $this->getTaskTypeId() ? 1 : 0 );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmailTemp		= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::TASK, $strSubject, $strHtmlContent = NULL, $strToEmailAddress = NULL, $intIsSelfDestruct, $strFromEmailAddress = CSystemEmail::TASK_EMAIL_ADDRESS );

		if( true == valArr( array_filter( $arrstrClientAdminEmailAddresses ) ) ) {

			$strReplayToEmailAddress = ( true == $boolIsFromUpdateTask ) ? 'In-Reply-To: ' . $this->getId() : 'Message-ID: ' . $this->getId();

			$objSystemEmailTemp->setReplyToEmailAddress( $strReplayToEmailAddress );

			$objSystemEmail = clone $objSystemEmailTemp;

			$objSystemEmail->setHtmlContent( $this->m_strHtmlEmailOutput );
			$objSystemEmail->setToEmailAddress( implode( ', ', $arrstrClientAdminEmailAddresses ) );

			if( true == valArr( $arrobjEmailAttachments ) ) {
				foreach( $arrobjEmailAttachments as $objEmailAttachment ) {
					$objSystemEmail->addEmailAttachment( $objEmailAttachment );
				}
			}

			// Attach parsed inline attachment to system email
			if( valArr( $this->m_arrobjTaskInlineAttachments ) ) {
				foreach( $this->m_arrobjTaskInlineAttachments as $arrobjTaskInlineAttachment ) {
					$objSystemEmail->addEmailAttachment( $arrobjTaskInlineAttachment );
				}
			}

			if( false == $objSystemEmail->insert( $objLoggedInUser->getId(), $objEmailDatabase ) ) {
				return false;
			}
		}

		// Task Emails for PSadmin users
		if( true == valArr( $this->getSendEmailToCids() ) && true == valArr( $this->m_arrstrTaskClientsData ) ) {

			foreach( $this->getSendEmailToCids() as $intCid ) {

				$objClient = CClients::fetchClientById( $intCid, $objDatabase );

				if( true == valObj( $objClient, 'CClient' ) && CCompanyStatusType::PROSPECT == $objClient->getCompanyStatusTypeId() || CCompanyStatusType::CLIENT == $objClient->getCompanyStatusTypeId() || CCompanyStatusType::TEST_DATABASE == $objClient->getCompanyStatusTypeId() ) {

					if( false == array_key_exists( $intCid, $this->m_arrstrTaskClientsData )
						|| false == isset( $this->m_arrstrTaskClientsData[$intCid]['PSadmin_email_addresses'] )
						|| false == valArr( $this->m_arrstrTaskClientsData[$intCid]['PSadmin_email_addresses'] ) ) {
							continue;
					}
							$arrstrTaskClientEmails 	= array_unique( array_map( 'strtolower', array_map( 'trim', $this->m_arrstrTaskClientsData[$intCid]['PSadmin_email_addresses'] ) ) );
							$this->m_arrintCompanyUserId 		= array_map( 'strtolower', $this->m_arrintCompanyUserId );
							if( true == valArr( $arrstrTaskClientEmails ) ) {
								foreach( $arrstrTaskClientEmails as $strClientEmailAddress ) {
									$strLocale = $this->m_arrstrCompanyUserEmailAddressesLocale[$strClientEmailAddress] && valStr( $this->m_arrstrCompanyUserEmailAddressesLocale[$strClientEmailAddress] )?$this->m_arrstrCompanyUserEmailAddressesLocale[$strClientEmailAddress]:$this->m_strDefaultLocale;
									if( false == valStr( $this->m_arrstrTaskClientsData[$intCid]['client_email_html_content'][$strLocale] ) || false == isset( $this->m_arrstrTaskClientsData[$intCid]['client_email_html_content'][$strLocale] ) ) {
										continue;
									}
									if( false == valStr( $strClientEmailAddress ) || ( true == valArr( $arrstrClientAdminEmailAddresses ) && true == in_array( $strClientEmailAddress, $arrstrClientAdminEmailAddresses ) ) ) continue;

									$objSystemEmail 		= clone $objSystemEmailTemp;
									$arrobjClientTaskNotes 	= array();
									$arrobjEmailAttachment 	= array();

									if( true == isset( $this->m_arrstrTaskClientsData[$intCid]['client_email_task_notes'] ) ) {
										$arrobjClientTaskNotes = $this->m_arrstrTaskClientsData[$intCid]['client_email_task_notes'];
									}

									$intTempCompanyUserId 	= array_search( $strClientEmailAddress, $this->m_arrintCompanyUserId );
									$intCompanyUserId 		= ( ( false == empty( $intTempCompanyUserId ) ) ? $intTempCompanyUserId : CUser::ID_ENTRATA_ID );

									if( true == $boolIsFromUpdateTask ) {
										$objSystemEmail->setReplyToEmailAddress( 'In-Reply-To: ' . $this->getId() . '~ PSI Support <ticket-' . $intCid . '-' . $intCompanyUserId . '-' . $this->getId() . '@tickets.entrata.com>' );
									} else {
										$objSystemEmail->setReplyToEmailAddress( 'Message-ID: ' . $this->getId() . '~ PSI Support <ticket-' . $intCid . '-' . $intCompanyUserId . '-' . $this->getId() . '@tickets.entrata.com>' );
									}

									$objSystemEmail->setToEmailAddress( $strClientEmailAddress );
									$objSystemEmail->setCid( $intCid );
									$objSystemEmail->setHtmlContent( $this->m_arrstrTaskClientsData[$intCid]['client_email_html_content'][$strLocale] );

									if( true == valArr( $arrobjClientEmailAttachment ) && true == valArr( $arrobjClientTaskNotes ) ) {
										foreach( $arrobjClientTaskNotes as $objClientTaskNote ) {
											if( true == array_key_exists( $objClientTaskNote->getId(), $arrobjClientEmailAttachment ) ) {
												$arrobjEmailAttachment[] = $arrobjClientEmailAttachment[$objClientTaskNote->getId()];
											}
										}
									}
									$arrobjTaskAttchments = array_merge( $arrobjEmailAttachment, $arrobjCommonClientEmailAttachment );

									if( true == valArr( $arrobjTaskAttchments ) ) {
										foreach( $arrobjTaskAttchments as $objTaskAttachment ) {
											if( true == valObj( $objTaskAttachment, 'CEmailAttachment' ) && true == valStr( $objTaskAttachment->getUrl() ) ) {
												$objEmailAttachment = new CEmailAttachment();
												$objTaskAttachment->setObjectStorageGateway( $this->m_objObjectStorageGateway );
												$strAttachmentFilePath = $objTaskAttachment->getFileTmpName() ?? $objTaskAttachment->downloadTaskAttachment( $intCreatedBy, $objDatabase, true );

												if( !$strAttachmentFilePath ) {
													continue;
												}

												$objEmailAttachment->setFilePath( dirname( $strAttachmentFilePath ) );
												$objEmailAttachment->setFileName( basename( $strAttachmentFilePath ) );
												$objSystemEmail->addEmailAttachment( $objEmailAttachment );
											}
										}
									}

									// Attach parsed inline attachment to system email
									if( valArr( $this->m_arrobjTaskInlineAttachments ) ) {
										foreach( $this->m_arrobjTaskInlineAttachments as $arrobjTaskInlineAttachment ) {
											$objSystemEmail->addEmailAttachment( $arrobjTaskInlineAttachment );
										}
									}

									if( false == $objSystemEmail->insert( $objLoggedInUser->getId(), $objEmailDatabase ) ) {
										return false;
									}

									if( true == $boolIsAttachFileSizeExceeded ) {
										continue;
									}
								}
							}
				}
			}
		}

		return true;
	}

	public function loadClientDatabases( $arrintCids = NULL ) {

		if( false == valArr( $arrintCids ) ) return NULL;

		$objConnectDatabase 		= CDatabases::createConnectDatabase();
		$arrobjDirectives 			= CDirectives::fetchClientDirectivesByCids( $objConnectDatabase, $arrintCids );
		$this->m_arrstrClientCompanyData 	= array();

		if( true == valArr( $arrobjDirectives ) ) {
			$this->m_arrstrClientCompanyData['databases'] 	= CDatabases::fetchDatabaseByIdsByDatabaseUserTypeIdByDatabaseTypeId( array_keys( rekeyObjects( 'DatabaseId', $arrobjDirectives ) ), CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseType::CLIENT, $objConnectDatabase );
			$this->m_arrstrClientCompanyData['directives'] 	= rekeyObjects( 'Cid', $arrobjDirectives );
		}

		return $this->m_arrstrClientCompanyData;
	}

	// function to calculate task and task note attached file size

	public function claculateTaskAndTaskNoteAttachementFileSize( $objDatabase, $objLoggedInUser = NULL ) {

		if( CTaskType::SUPPORT == $this->getTaskTypeId() || ( CTaskType::BUG == $this->getTaskTypeId() && true == in_array( $this->getTaskPriorityId(), array( CTaskPriority::IMMEDIATE, CTaskPriority::URGENT ) ) ) ) {
			$this->m_arrobjTaskAttachments = \Psi\Eos\Admin\CTaskAttachments::createService()->fetchAllTaskAndTaskNoteAttachments( $this->getId(), $objDatabase );
		}

		$intTotalAttachmentFileSize	= 0;
		$intCreatedBy = CUser::ID_ENTRATA_ID;

		if( true == valObj( $objLoggedInUser, 'CUser' ) ) {
			$intCreatedBy = $objLoggedInUser->getId();
		}

		if( true == valArr( $this->m_arrobjTaskAttachments ) ) {
			foreach( $this->m_arrobjTaskAttachments as $objTaskAttachment ) {
				$objTaskAttachment->setObjectStorageGateway( $this->m_objObjectStorageGateway );
				$strAttachmentPath = $objTaskAttachment->downloadTaskAttachment( $intCreatedBy, $objDatabase, true );

				if( false == file_exists( $strAttachmentPath ) ) {
					continue;
				}
				$intTotalAttachmentFileSize += filesize( $strAttachmentPath );
			}
			// 1MB = 1048576 Bytes
			$intTotalAttachmentFileSize = $intTotalAttachmentFileSize / 1048576;
		}

		if( CTaskAttachment::MAX_FILE_SIZE < $intTotalAttachmentFileSize ) {
			return true;
		}
		return false;
	}

	protected function parseTaskInlineAttachments( $strConect ) {
		$objEmailAttachmentTemp 		= new CEmailAttachment();

		if( $strConect ) {
			preg_match_all( '/<img[^>]+>/i', $strConect, $arrstrImageTags );

			if( !empty( $arrstrImageTags[0] ) ) {
				foreach( $arrstrImageTags[0] as $strImageTag ) {
					preg_match( '/(src)=("[^"]*")/i', $strImageTag, $arrstrSrc );

					if( !empty( $arrstrSrc[2] ) ) {
						preg_match( '/data:image\/(.*);base64,/', $arrstrSrc[2], $arrstrAttachmentsExtensionData );

						if( empty( $arrstrAttachmentsExtensionData[1] ) ) {
							continue;
						}

						$strContentEncoded = preg_replace( '/data:image\/(.*);base64,/', '', $arrstrSrc[2] );
						$strFileName = time() . '_inline_attachment_' . $this->getId() . '.' . $arrstrAttachmentsExtensionData[1];

						if( !$strContentEncoded ) {
							continue;
						}

						$strContentDecoded = base64_decode( $strContentEncoded );

						$strEmailAttachmentFilePath = PATH_NON_BACKUP_MOUNTS_TEMP_SYSTEM_EMAIL_AWS_S3 . 'system_email_attachments/';

						CFileIo::recursiveMakeDir( $strEmailAttachmentFilePath );

						CFileIo::filePutContents( $strEmailAttachmentFilePath . $strFileName, $strContentDecoded );

						$objEmailAttachment = clone $objEmailAttachmentTemp;
						$objEmailAttachment->setFilePath( $strEmailAttachmentFilePath );
						$objEmailAttachment->setFileName( $strFileName );
						$objEmailAttachment->setTitle( $strFileName );
						$objEmailAttachment->setIsInline( true );

						$this->m_arrobjTaskInlineAttachments[] = $objEmailAttachment;

						$strConect = str_replace( $arrstrSrc[2], '"' . $strFileName . '"', $strConect );
					}
				}
			}
		}

		return $strConect;
	}

	public function setLatestUpdateForClients( $objCurrentUser, $objDatabase, $arrintCids = NULL, $arrstrAttachments = NULL ) {

		$arrobjTaskCompanies = CTaskCompanies::createService()->fetchTaskCompaniesExceptSharedNoteByTaskId( $this->getId(), $objDatabase );

		if( false == valArr( $arrobjTaskCompanies ) ) return true;

		$objBeforeUpdateTask = unserialize( $this->getSerializedOriginalValues() );
		$strLatestUpdate = '';

		if( $objBeforeUpdateTask->getTitle() != $this->getTitle() ) {
			$strLatestUpdate = 'Title Updated.';
		}

		if( $objBeforeUpdateTask->getDescription() != $this->getDescription() ) {
			$strLatestUpdate = 'Description Updated.';
		}

		if( true == valArr( $arrstrAttachments ) ) {
			$strLatestUpdate = 'Attachment Added.';
		}

		if( true == $this->validateClientFriendlyStatus( $objBeforeUpdateTask->getTaskStatusId(), $this->getTaskStatusId() ) ) {
			$strLatestUpdate = 'Task Status Updated.';
		}

		if( true == valArr( $arrintCids ) && false == valStr( $strLatestUpdate ) ) {
			$strLatestUpdate = 'Note Added.';

			$arrobjTempTaskCompanies = rekeyObjects( 'Cid', $arrobjTaskCompanies );

			foreach( $arrintCids as $intCid ) {
				if( true == array_key_exists( $intCid, $arrobjTempTaskCompanies ) ) {
					$arrobjFinalTempTaskCompanies[$arrobjTempTaskCompanies[$intCid]->getId()] = $arrobjTempTaskCompanies[$intCid];
				}
			}

			$arrobjTaskCompanies = $arrobjFinalTempTaskCompanies;
		}

		if( true == valArr( $arrobjTaskCompanies ) && true == valStr( $strLatestUpdate ) ) {
			foreach( $arrobjTaskCompanies as $objTaskCompany ) {
				$objTaskCompany->setLatestUpdate( $strLatestUpdate );

				if( false == $objTaskCompany->update( $objCurrentUser->getId(), $objDatabase ) ) {
					return false;
				}
			}
		}

		return true;
	}

	public function validateClientFriendlyStatus( $intOldTaskStatusId, $intNewTaskStatusId ) {

		$arrintClientFriendlyInProgressTaskStatuses = array_flip( array(
			CTaskStatus::IN_PROGRESS,
			CTaskStatus::READY_FOR_DEV,
			CTaskStatus::DEV_IN_PROGRESS,
			CTaskStatus::READY_FOR_QA,
			CTaskStatus::QA_IN_PROGRESS,
			CTaskStatus::READY_FOR_RELEASE,
			CTaskStatus::COMMITTED,
			CTaskStatus::RELEASED_ON_RAPID,
			CTaskStatus::RELEASED_ON_STANDARD,
			CTaskStatus::VERIFIED_ON_RAPID,
			CTaskStatus::VERIFIED_ON_STANDARD,
			CTaskStatus::RELEASED_ON_STAGE,
			CTaskStatus::VERIFIED_ON_STAGE,
			CTaskStatus::ACKNOWLEDGED,
			CTaskStatus::REASSIGNED

		) );

		$arrintClientFriendlyInQueueTaskStatuses = array_flip( array(
			CTaskStatus::ON_HOLD,
			CTaskStatus::BACKLOG,
			CTaskStatus::UNDER_REVIEW

		) );

		if( $intOldTaskStatusId == $intNewTaskStatusId ) {
			return false;
		}

		if( true == array_key_exists( $intOldTaskStatusId, $arrintClientFriendlyInProgressTaskStatuses ) ) {
			if( true == array_key_exists( $intNewTaskStatusId, $arrintClientFriendlyInProgressTaskStatuses ) ) {
				return false;
			}
		}

		if( true == array_key_exists( $intOldTaskStatusId, $arrintClientFriendlyInQueueTaskStatuses ) ) {
			if( true == array_key_exists( $intOldTaskStatusId, $arrintClientFriendlyInQueueTaskStatuses ) ) {
				return false;
			}
		}
		return true;
	}

	public function setTitleFromDescription() {
		if( false == valStr( $this->getTitle() ) && true == valStr( $this->getDescription() ) ) {
			$strTitle	= str_replace( '&nbsp;', '', \Psi\CStringService::singleton()->preg_replace( '/\s\s+/', ' ', $this->getDescription() ) );
			$strTitle	= strip_tags( html_entity_decode( nl2br( trim( $strTitle ) ), ENT_QUOTES ) );
			$strTitle	= \Psi\CStringService::singleton()->wordwrap( $strTitle, 28, ' ', true );
			$strTitle	= html_entity_decode( truncate( $strTitle, 100 ), ENT_QUOTES );
			$strTitle	= trim( str_replace( 'User Story :', '', $strTitle ) );
			$strTitle	= \Psi\CStringService::singleton()->preg_replace( '/[]~`]|[{}[]/', '', $strTitle );
			$this->setTitle( $strTitle );
		}
	}

	public function setPsProductIdBasedOnPsProductOptionId( $objDatabase ) {
		$objPsProductOption = CPsProductOptions::createService()->fetchPsProductOptionById( $this->getPsProductOptionId(), $objDatabase );
		if( true == valObj( $objPsProductOption, 'CPsProductOption' ) ) {
			$this->setPsProductId( $objPsProductOption->getPsProductId() );
			$this->setProjectManagerId( $objPsProductOption->getSdmEmployeeId() );
		} else {
			$this->setPsProductId( NULL );
		}
	}

	public function createMessages( $intEmployeeId, $objPsProduct, $objPsProductOption, $objAdminDatabase, $objSmsDatabase, $intTaskReferenceTypeId, $boolEscalationMessage = false, $boolAllDoe = false, $boolChainEnd = false, $boolResolve = false, $intHierarchyLevel = NULL, $objTaskReference = NULL ) {

		$arrobjPushMessages = array();
		$arrstrBlockedPhoneNumbers = array();

		$objEmployeePhoneNumber = CEmployeePhoneNumbers::fetchEmployeePhoneNumberByEmployeeIdByPhoneNumberTypeId( $intEmployeeId, CPhoneNumberType::PRIMARY, $objAdminDatabase );

		if( true == valObj( $objEmployeePhoneNumber, 'CEmployeePhoneNumber' ) ) {
			$arrstrBlockedPhoneNumbers = \Psi\Eos\Sms\CBlocks::createService()->fetchBlockedPhoneNumbersByMobileNumberByMessageTypeIds( $objEmployeePhoneNumber->getPhoneNumber(), array( CMessageType::TASK_NOTIFICATION, CMessageType::STOP ), $objSmsDatabase );

			if( false == valArr( $arrstrBlockedPhoneNumbers ) ) {

				$arrmixEmployeeAddressData = current( CEmployeeAddresses::fetchEmployeeCountryCodeByEmployeeIdByAddressTypeId( $intEmployeeId, CAddressType::PRIMARY, $objAdminDatabase ) );
				$strEmployeeCountryCode = ( true == valArr( $arrmixEmployeeAddressData ) ) ? $arrmixEmployeeAddressData['country_code'] : NULL;

				$objMessage = new CMessage();

				$arrobjPushMessages = $objMessage->createTaskNotificationMessages( $objMessage, $this, $objPsProduct, $objPsProductOption, $intEmployeeId, $objEmployeePhoneNumber->getPhoneNumber(), $strEmployeeCountryCode, $objAdminDatabase, $boolEscalationMessage, $objSmsDatabase, $boolAllDoe, $intTaskReferenceTypeId, $boolChainEnd, $boolResolve, $intHierarchyLevel, $objTaskReference );
			}
		}
		return $arrobjPushMessages;
	}

	public function sendUrgentBugSms( $objAdminDatabase ) {
		$boolSendUrgentBugSms	= false;

		if( CTaskType::BUG == $this->getTaskTypeId() && CTaskPriority::URGENT == $this->getTaskPriorityId() ) {
			$intDateToday			= getConvertedDateTimeByTimeZoneName( date( 'Y-m-d H:i:s' ), 'Y/m/d H:i:s', 'Asia/Calcutta' );
			$arrstrPaidHolidays		= ( array ) rekeyArray( 'date', \Psi\Eos\Admin\CPaidHolidays::createService()->fetchPaidHolidaysByCountryCodeByDateRange( date( 'Y/m/d', strtotime( $intDateToday ) ), date( 'Y/m/d', strtotime( $intDateToday . ' +1 day' ) ), CCountry::CODE_INDIA, $objAdminDatabase ) );
			if( true == in_array( date( 'N', strtotime( date( 'Y/m/d', strtotime( $intDateToday ) ) ) ), array( COfficeShiftEmployee::DAY_OF_WEEK_SUNDAY, COfficeShiftEmployee::DAY_OF_WEEK_SATURDAY ) ) || false == is_null( getArrayElementByKey( date( 'm/d/Y', strtotime( $intDateToday ) ), $arrstrPaidHolidays ) ) ) {
				$boolSendUrgentBugSms	 = true;
			} elseif( date( 'G', strtotime( $intDateToday ) ) >= 17 && ( false == is_null( getArrayElementByKey( date( 'm/d/Y', strtotime( $intDateToday . ' +1 day' ) ), $arrstrPaidHolidays ) ) || true == in_array( date( 'N', strtotime( date( 'Y-m-d', strtotime( $intDateToday . ' +1 day' ) ) ) ), array( COfficeShiftEmployee::DAY_OF_WEEK_SUNDAY, COfficeShiftEmployee::DAY_OF_WEEK_SATURDAY ) ) ) ) {
				$boolSendUrgentBugSms	 = true;
			}
		}

		return $boolSendUrgentBugSms;
	}

	public function setTaskSequence( $intSequenceValue, $objAdminDatabase ) {
		if( false == is_numeric( $intSequenceValue ) ) return NULL;

		$strSql = 'SELECT setval( \'tasks_id_seq\', ' . ( int ) $intSequenceValue . ', FALSE )';
		$this->m_arrstrTaskSequences = fetchData( $strSql, $objAdminDatabase );
		return $this->m_arrstrTaskSequences;
	}

	public function setDataForDefect( $objTask, $arrmixDefect ) {

		$this->setTitle( ( false === \Psi\CStringService::singleton()->strpos( $objTask->getTitle(), 'Defect -' ) )? 'Defect - ' . $objTask->getTitle() : $objTask->getTitle() );
		$this->setTaskTypeId( CTaskType::DEFECT );
		$this->setTaskPriorityId( $objTask->getTaskPriorityId() );
		$this->setPsProductId( $objTask->getPsProductId() );
		$this->setClusterId( $objTask->getClusterId() );
		$this->setPsProductOptionId( $objTask->getPsProductOptionId() );
		$this->setProjectManagerId( $objTask->getProjectManagerId() );
		$this->setUserId( $arrmixDefect['user_id'] );
		$this->setDescription( \Psi\CStringService::singleton()->htmlspecialchars( $arrmixDefect['description'] ) );
		$this->setTaskStatusId( $arrmixDefect['task_status_id'] );
		$this->setSubTaskTypeId( $arrmixDefect['sub_task_type_id'] );

		return $this;
	}

	public function loadSMSReceiverEmployeeIds( $objPsProduct, $objPsProductOption, $objAdminDatabase ) {

		$boolActiveTimeZone = CSmsTaskLibrary::validateTimeZone();

		 $arrintSmsReceiverEmployeeIds = array();
		 if( CTaskPriority::IMMEDIATE != $this->getTaskPriorityId() && CTaskPriority::URGENT != $this->getTaskPriorityId() ) {
		 	return $arrintSmsReceiverEmployeeIds;
		 }
		if( false == valObj( $objPsProduct, 'CPsProduct' ) ) return;
		if( false == is_null( $this->getPsProductOptionId() ) && true == valObj( $objPsProductOption, 'CPsProductOption' ) ) {
			if( CTaskPriority::URGENT == $this->getTaskPriorityId() ) {
				$arrintEmployeeId = CPsProductEmployees::createService()->fetchAddByPsProductIdByPsProductOptionId( $this->getPsProductId(), $this->getPsProductOptionId(), $objAdminDatabase );

				if( true == valArr( $arrintEmployeeId ) ) {
					$arrintSmsReceiverEmployeeIds[current( $arrintEmployeeId )['add_employee_id']] = CTaskReferenceType::SMS_ADD;
				}
			}
			( false == is_null( $objPsProductOption->getTpmEmployeeId() ) ) ? $arrintSmsReceiverEmployeeIds[$objPsProductOption->getTpmEmployeeId()] = CTaskReferenceType::SMS_TPM : NULL;
			( false == is_null( $objPsProductOption->getQamEmployeeId() ) ) ? $arrintSmsReceiverEmployeeIds[$objPsProductOption->getQamEmployeeId()] = CTaskReferenceType::SMS_QAM : NULL;
			if( CTaskPriority::IMMEDIATE == $this->getTaskPriorityId() ) {
				( false == is_null( $objPsProductOption->getSdmEmployeeId() ) && true == $boolActiveTimeZone ) ? $arrintSmsReceiverEmployeeIds[$objPsProductOption->getSdmEmployeeId()] = CTaskReferenceType::SMS_SDM : NULL;
				( false == is_null( $objPsProductOption->getSqmEmployeeId() && true == $boolActiveTimeZone ) && true == $boolActiveTimeZone ) ? $arrintSmsReceiverEmployeeIds[$objPsProductOption->getSqmEmployeeId()] = CTaskReferenceType::SMS_SQM : NULL;
			}
		} else {
			if( CTaskPriority::URGENT == $this->getTaskPriorityId() ) {
				$arrintEmployeeId = CPsProductEmployees::createService()->fetchAddByPsProductIdByPsProductOptionId( $this->getPsProductId(), NULL, $objAdminDatabase );
				if( true == valArr( $arrintEmployeeId ) ) {
					$arrintSmsReceiverEmployeeIds[current( $arrintEmployeeId )['add_employee_id']] = CTaskReferenceType::SMS_ADD;
				}
			}
			( false == is_null( $objPsProduct->getTpmEmployeeId() ) ) ? $arrintSmsReceiverEmployeeIds[$objPsProduct->getTpmEmployeeId()] = CTaskReferenceType::SMS_TPM : NULL;
			( false == is_null( $objPsProduct->getQamEmployeeId() ) ) ? $arrintSmsReceiverEmployeeIds[$objPsProduct->getQamEmployeeId()] = CTaskReferenceType::SMS_QAM : NULL;
			if( CTaskPriority::IMMEDIATE == $this->getTaskPriorityId() ) {
				( false == is_null( $objPsProduct->getSdmEmployeeId() ) && true == $boolActiveTimeZone ) ? $arrintSmsReceiverEmployeeIds[$objPsProduct->getSdmEmployeeId()] = CTaskReferenceType::SMS_SDM : NULL;
				( false == is_null( $objPsProduct->getSqmEmployeeId() ) && true == $boolActiveTimeZone ) ? $arrintSmsReceiverEmployeeIds[$objPsProduct->getSqmEmployeeId()] = CTaskReferenceType::SMS_SQM : NULL;
			}
		}

		return $arrintSmsReceiverEmployeeIds;
	}

	public function sendEscalationSMS( $intLastReferenceTypeId, $objAdminDatabase, $objSmsDatabase ) {

		$boolActiveTimeZone = CSmsTaskLibrary::validateTimeZone();

		$arrintSmsReceiverEmployeeIds = $arrintTaskReferenceTypes = [];
		$boolChainEnd = false;

		$objPsProduct = CPsProducts::createService()->fetchPsProductById( $this->getPsProductId(), $objAdminDatabase );
		$objPsProductOption = NULL;
		if( false == is_null( $this->getPsProductOptionId() ) ) {
			$objPsProductOption = CPsProductOptions::createService()->fetchPsProductOptionById( $this->getPsProductOptionId(), $objAdminDatabase );
		}

		$objProduct = ( true == is_numeric( $this->getPsProductOptionId() ) && true == valObj( $objPsProductOption, 'CPsProductoption' ) ) ? $objPsProductOption : $objPsProduct;
		$boolIsSendAllDoe = false;
		$intEmployeeId = current( CPsProductEmployees::createService()->fetchAddByPsProductIdByPsProductOptionId( $this->getPsProductId(), $this->getPsProductOptionId(), $objAdminDatabase ) )['add_employee_id'];
		if( true == $boolActiveTimeZone ) {
			if( true == in_array( $intLastReferenceTypeId, CTaskReferenceType::$c_arrintFirstTierReferenceTypesForSms ) ) {
				// send sms to architect of task product
				if( true == is_numeric( $objProduct->getArchitectEmployeeId() ) || true == is_numeric( $intEmployeeId ) ) {
					if( true == is_numeric( $objProduct->getArchitectEmployeeId() ) ) {
						array_push( $arrintSmsReceiverEmployeeIds, $objProduct->getArchitectEmployeeId() );
						$arrintTaskReferenceTypes[$objProduct->getArchitectEmployeeId()] = CTaskReferenceType::SMS_ARCHITECT;
					}
					if( true == is_numeric( $intEmployeeId ) ) {
						array_push( $arrintSmsReceiverEmployeeIds, $intEmployeeId );
						$arrintTaskReferenceTypes[$intEmployeeId] = CTaskReferenceType::SMS_ADD;
					}
					$intHierarchyLevel = 2;
				} elseif( true == is_numeric( $objProduct->getDoeEmployeeId() ) || true == is_numeric( $objProduct->getProductOwnerEmployeeId() ) ) {

					$arrmixResult = $this->getPoAndDoeEmployeeIds( $objProduct, $arrintSmsReceiverEmployeeIds );
					$arrintSmsReceiverEmployeeIds = current( $arrmixResult );
					$arrintTaskReferenceTypes = $arrmixResult[1];
					$intHierarchyLevel = 3;
				} else {
					$intHierarchyLevel = 4;
					$intReferenceTypeId = CTaskReferenceType::SMS_ALL_DOE;
					$boolIsSendAllDoe   = true;
				}

			} elseif( CTaskReferenceType::SMS_ARCHITECT == $intLastReferenceTypeId || CTaskReferenceType::SMS_ADD == $intLastReferenceTypeId ) {
				// send sms to doe and po of task product
				if( false == is_numeric( $objProduct->getDoeEmployeeId() ) && false == is_numeric( $objProduct->getProductOwnerEmployeeId() ) ) {
					$intReferenceTypeId = CTaskReferenceType::SMS_ALL_DOE;
					$boolIsSendAllDoe   = true;
					$intHierarchyLevel = 4;
				} else {
					$arrmixResult = $this->getPoAndDoeEmployeeIds( $objProduct, $arrintSmsReceiverEmployeeIds );
					$arrintSmsReceiverEmployeeIds = current( $arrmixResult );
					$arrintTaskReferenceTypes = $arrmixResult[1];
					$intHierarchyLevel = 3;
				}
			} elseif( CTaskReferenceType::SMS_DOE == $intLastReferenceTypeId || CTaskReferenceType::SMS_PO == $intLastReferenceTypeId ) {
				// search for all doe
				$intReferenceTypeId = CTaskReferenceType::SMS_ALL_DOE;
				$boolIsSendAllDoe   = true;
				$intHierarchyLevel = 4;
			}
		} else {

			if( true == in_array( $intLastReferenceTypeId, [ CTaskReferenceType::SMS_TPM, CTaskReferenceType::SMS_QAM ] ) ) {
				// send sms to add of task product
				if( true == is_numeric( $intEmployeeId ) ) {
					array_push( $arrintSmsReceiverEmployeeIds, $intEmployeeId );
					$intReferenceTypeId = CTaskReferenceType::SMS_ADD;
					$intHierarchyLevel = 2;
				} else if( false == is_numeric( $intEmployeeId ) ) {
						array_push( $arrintSmsReceiverEmployeeIds, CEmployee::ID_SANDEEP_GARUD );
						$intReferenceTypeId = CTaskReferenceType::SMS_SANDY;
					$intHierarchyLevel = 3;
				}
			} else if( CTaskReferenceType::SMS_ADD == $intLastReferenceTypeId && $intEmployeeId != CEmployee::ID_SANDEEP_GARUD ) {
				array_push( $arrintSmsReceiverEmployeeIds, CEmployee::ID_SANDEEP_GARUD );
				$intReferenceTypeId = CTaskReferenceType::SMS_SANDY;
				$intHierarchyLevel = 3;
			} else if( CTaskReferenceType::SMS_SANDY == $intLastReferenceTypeId || ( CTaskReferenceType::SMS_ADD == $intLastReferenceTypeId && $intEmployeeId == CEmployee::ID_SANDEEP_GARUD ) ) {
				$boolChainEnd = true;
				$arrmixResult = $this->getPoAndDoeEmployeeIds( $objProduct, $arrintSmsReceiverEmployeeIds );
				$arrintSmsReceiverEmployeeIds = current( $arrmixResult );
				$arrintTaskReferenceTypes = $arrmixResult[1];
				$intHierarchyLevel = 4;
			}
		}

		$arrintTaskReferenceTypes = ( true == valArr( $arrintTaskReferenceTypes ) ) ? $arrintTaskReferenceTypes : [ $intReferenceTypeId ];
		// need to check first if higher authority does not get any message before.
		$boolValid = true;

		if( valArr( array_filter( $arrintTaskReferenceTypes ) ) ) {
			$objPreviousTaskReference = \Psi\Eos\Admin\CTaskReferences::createService()->fetchTaskReferencesByTaskReferenceTypeIdsByTaskId( $arrintTaskReferenceTypes, $this->getId(), $objAdminDatabase );
		}

		if( true == valArr( $objPreviousTaskReference ) ) {
			$boolValid = false;
		} // else msg already escalate
		$boolSendAllDoe = false;
		// works after it comes from doe
		 if( ( true == $boolIsSendAllDoe && CTaskReferenceType::SMS_ALL_DOE == $intReferenceTypeId ) ) {

			$arrintDoeEmployeeIds					= CEmployees::fetchDirectorEmployeeIds( $objAdminDatabase );
			$arrintLastRespondedEmployeeId = current( \Psi\Eos\Admin\CTaskReferences::createService()->fetchLastRespondedEmployeeIdByTaskReferenceTypeId( $this->getId(), CTaskReferenceType::SMS_REPLY, $objAdminDatabase ) );
			 $boolSendAllDoe                        = true;
			if( true == valArr( $arrintDoeEmployeeIds ) ) {
				$arrintDoeEmployeesIds 				 = array_keys( rekeyArray( 'doe_id', $arrintDoeEmployeeIds ) );
				$arrintUniqueDoeEmployeesIds  = ( true == valArr( $arrintDoeEmployeesIds ) ) ? array_filter( array_unique( $arrintDoeEmployeesIds ) ) : array();
				if( true == valArr( $arrintLastRespondedEmployeeId ) && ( $intKey = array_search( $arrintLastRespondedEmployeeId['employee_id'], $arrintUniqueDoeEmployeesIds ) ) !== false ) unset( $arrintUniqueDoeEmployeesIds[$intKey] );
				foreach( $arrintUniqueDoeEmployeesIds  as $intDoeEmployeeId ) {
					array_push( $arrintSmsReceiverEmployeeIds, $intDoeEmployeeId );
				}
			}
		 }

		if( valObj( $objPsProduct, 'CPsProduct' ) && true == valArr( $arrintSmsReceiverEmployeeIds ) && true == $boolValid ) {
			$arrintSmsReceiverEmployeeIds = array_unique( $arrintSmsReceiverEmployeeIds );
			foreach( $arrintSmsReceiverEmployeeIds as $intEmployeeId ) {
				if( true == valArr( $arrintTaskReferenceTypes ) && true == array_key_exists( $intEmployeeId, $arrintTaskReferenceTypes ) ) $intReferenceTypeId = $arrintTaskReferenceTypes[$intEmployeeId];
				$this->sendSMS( $intEmployeeId, $objPsProduct, $objPsProductOption, $objAdminDatabase, $objSmsDatabase, $intReferenceTypeId, $boolEscalationMessage = true, $boolSendAllDoe, $boolChainEnd, false, $intHierarchyLevel );
			}
		}

		if( valArr( $this->m_arrobjSMSTaskNotes ) && false == \Psi\Eos\Admin\CTaskNotes::createService()->bulkInsert( $this->m_arrobjSMSTaskNotes, CUser::ID_SYSTEM, $objAdminDatabase ) ) {
			return;
		}
	}

	public function sendSMS( $intEmployeeId, $objPsProduct, $objPsProductOption, $objAdminDatabase, $objSmsDatabase, $intReferenceTypeId, $boolEscalationMessage = true, $boolSendAllDoe = false, $boolChainEnd = false, $boolResolve = false, $intHierarchyLevel = NULL, $boolResolved = false ) {

		$objUser = CUsers::fetchUserByEmployeeId( $intEmployeeId, $objAdminDatabase );
		$arrobjPushMessages = $this->createMessages( $intEmployeeId, $objPsProduct, $objPsProductOption, $objAdminDatabase, $objSmsDatabase, $intReferenceTypeId, $boolEscalationMessage = true, $boolSendAllDoe, $boolChainEnd, $boolResolve, $intHierarchyLevel );
		$strTaskNote = '';

		if( valArr( $this->m_arrobjSMSTaskNotes ) && array_key_exists( $this->getId(), $this->m_arrobjSMSTaskNotes ) ) {
			$strTaskNote = $this->m_arrobjSMSTaskNotes[$this->getId()]->getTaskNote();
		}

		if( true == valArr( $arrobjPushMessages ) ) {
			$arrobjEmployees = \Psi\Eos\Admin\CEmployees::createService()->fetchEmployeesByIds( array_filter( array_keys( rekeyObjects( 'EmployeeId', $arrobjPushMessages ) ) ), $objAdminDatabase );

			$objTaskReference = $this->createTaskReference( NULL, $intReferenceTypeId );
			if( false == $objTaskReference->insert( $objUser->getId(), $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();
				return;
			}
			foreach( $arrobjPushMessages as $objMessage ) {
				$objMessage->setTaskReferenceId( $objTaskReference->getId() );
				if( true == $objMessage->validate( 'send' ) && false == $objMessage->insert( $objUser->getId(), NULL, false, false ) ) {
					break;
				}

				$strEmployeeName = ( arrayKeysExistsInArray( [ $objMessage->getEmployeeId() ], $arrobjEmployees ) ) ? $arrobjEmployees[$objMessage->getEmployeeId()]->getPreferredName() : '';
				if( false == valStr( $strTaskNote ) && valStr( $strEmployeeName ) ) {
					if( $boolResolved ) {
						$strTaskNote .= 'Resolved';
					} elseif( true == is_numeric( $intHierarchyLevel ) ) {
						$strTaskNote .= 'Hierarchy level ' . $intHierarchyLevel;
					}
					$strTaskNote .= ' SMS has been sent to ' . $strEmployeeName;
				} elseif( valStr( $strEmployeeName ) ) {
					$strTaskNote .= ', ' . $strEmployeeName;
				}
			}
		}

		if( valStr( $strTaskNote ) && valArr( $this->m_arrobjSMSTaskNotes ) && array_key_exists( $this->getId(), $this->m_arrobjSMSTaskNotes ) ) {
			$this->m_arrobjSMSTaskNotes[$this->getId()]->setTaskNote( $strTaskNote );
		} elseif( valStr( $strTaskNote ) ) {
			$objTaskNote = $this->createTaskNote();
			$objTaskNote->setUserId( CUser::ID_SYSTEM );
			$objTaskNote->setTaskNote( $strTaskNote );
			$objTaskNote->setTaskNoteTypeId( CTaskNoteType::TASK_SMS );
			$objTaskNote->setTaskNoteDatetime( 'now()' );
			$this->m_arrobjSMSTaskNotes[$this->getId()] = $objTaskNote;
		}
	}

	public function getPoAndDoeEmployeeIds( $objProduct, $arrintSmsReceiverEmployeeIds ) {
		$arrintTaskReferenceTypes = [];
		if( true == is_numeric( $objProduct->getDoeEmployeeId() ) ) {
			array_push( $arrintSmsReceiverEmployeeIds, $objProduct->getDoeEmployeeId() );
			$intDoeEmployeeId = $objProduct->getDoeEmployeeId();
			$arrintTaskReferenceTypes[$intDoeEmployeeId] = CTaskReferenceType::SMS_DOE;
		}
		if( true == is_numeric( $objProduct->getProductOwnerEmployeeId() ) && ( false == is_numeric( $intDoeEmployeeId ) || ( true == is_numeric( $intDoeEmployeeId ) && $objProduct->getProductOwnerEmployeeId() != $intDoeEmployeeId ) ) ) {
			array_push( $arrintSmsReceiverEmployeeIds, $objProduct->getProductOwnerEmployeeId() );
			$arrintTaskReferenceTypes[$objProduct->getProductOwnerEmployeeId()] = CTaskReferenceType::SMS_PO;
		}
		return array( $arrintSmsReceiverEmployeeIds, $arrintTaskReferenceTypes );
	}

	public function urlShortner( $strUrl ) {

			// Get API key from : https://console.firebase.google.com/
			$strApiKey 			= CConfig::get( 'Google_Url_Shortner_Key' );

			$arrstrPostData 	= array(
				'dynamicLinkInfo' => [
					'domainUriPrefix' => $this::DOMAIN_URI_PREFIX,
					'link'            => $strUrl
				]
			);
			$arrstrPostData	= json_encode( $arrstrPostData );
			$arrmixParameters['url']					= $this::FIREBASE_SHORTLINK_URI . $strApiKey;
			$arrmixParameters['ssl_verify_peer']		= 0;
			$arrmixParameters['receive_header_info']	= 0;
			$arrmixParameters['header_info']			= array( 'Content-type: application/json' );
			$arrmixParameters['request_method']			= 'POST';
			$arrmixParameters['request_data']			= $arrstrPostData;

			$objExternalRequest	= new CExternalRequest();
			$objExternalRequest->setParameters( $arrmixParameters );
			$strCurlOutput		= $objExternalRequest->execute( $boolGetResponse = true );

			// Change the response json string to object
			$objJson 	= json_decode( $strCurlOutput );

			return $objJson->shortLink;
	}

	public function generateAccessKey( $intEmployeeId, $objAdminDatabase ) {
			$strCurrentDateTime = date( 'Y-m-d H:i:s' );
			$intTokenLength 	= $this::EMPLOYEE_TOKEN_LENGTH;
			$intExpireMinutes 	= $this::EMPLOYEE_TOKEN_EXPIRATION;
			$strAccessKey 		= generateRandomId( $intTokenLength );
			$strToken 			= base64_encode( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strAccessKey, CONFIG_SODIUM_KEY_ID ) );

			$objEmployeeToken = new CEmployeeToken();
			$objEmployeeToken->setToken( $strAccessKey );
			$objEmployeeToken->setEmployeeId( $intEmployeeId );
			$objEmployeeToken->setExpiredOn( date( 'Y-m-d H:i:s', strtotime( $strCurrentDateTime . ' +' . $intExpireMinutes . ' minutes' ) ) );
			$objEmployeeToken->setEmployeeTokenTypeId( CEmployeeTokenType::TASK_SMS_NOTIFICATION );
			$objEmployeeToken->setCreatedOn( $strCurrentDateTime );

			if( false == $objEmployeeToken->insert( CUser::ID_SYSTEM, $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();
			}

			return $strToken;
	}

	public function addTaskNoteForTaskFollowUp( $strPreviousFollowUpTime, $strNewFollowUpTime, $intUserId ) {

		if( strtotime( $strPreviousFollowUpTime ) == strtotime( $strNewFollowUpTime ) || true == is_null( $strNewFollowUpTime ) ) {
			return NULL;
		}

		$strFollowUpToTime = ( ( false == is_null( $strNewFollowUpTime ) ) ? date( 'm/d/Y h:i A', strtotime( $strNewFollowUpTime ) ): ' NULL ' );
		$strNotes = ( false == is_null( $strPreviousFollowUpTime ) ) ? 'Task Follow-up time is updated from ' . date( 'm/d/Y H:i A', strtotime( $strPreviousFollowUpTime ) ) . ' to ' . $strFollowUpToTime : ' Task Follow-up time is added to ' . $strFollowUpToTime;

		// Insert Records into task notes
		$objTaskNote = $this->createTaskNote();
		$objTaskNote->setUserId( $intUserId );
		$objTaskNote->setTaskNote( $strNotes );
		$objTaskNote->setTaskNoteDatetime( 'now()' );

		return $objTaskNote;
	}

	public function setTaskFollowUpDateTime( $strFollowUpTime, $objDatabase ) {

		if( ( ( CTaskType::SUPPORT == $this->getTaskTypeId() && false == $this->getIsPublished() ) || CTaskType::RP_FEEDBACK == $this->getTaskTypeId() ) && false == is_numeric( $this->getCid() ) ) {
			return;
		}

		$strCurrentTimeZone = date_default_timezone_get(); // previous time zone

		date_default_timezone_set( 'America/Denver' ); // setting America/Denver timezone to calculate follow-up

		$arrstrFollowUpTime = explode( ':', $strFollowUpTime );
		$intDays			= intval( $arrstrFollowUpTime[0] / self::WORKING_HOUR );
		$intHours			= $arrstrFollowUpTime[0] % self::WORKING_HOUR;
		$strDateTime		= date( 'm/d/Y H:i:s' );

		$intCurrentDayPaidHoliday = CPaidHolidays::fetchPaidHolidaysCountByDatesByCountryCode( date( 'm/d/y' ), date( 'm/d/Y' ), CCountry::CODE_USA, $objDatabase );

		if( self::FOLLOW_UP_END_TIME <= date( 'H' ) || self::FOLLOW_UP_START_TIME > date( 'H' ) || ( 0 != $intCurrentDayPaidHoliday ) || ( true == in_array( date( 'N', strtotime( date( 'm/d/Y' ) ) ), array( self::DAY_OF_WEEK_SUNDAY, self::DAY_OF_WEEK_SATURDAY ) ) ) ) {
			$intHours = $intHours + self::FOLLOW_UP_START_TIME;
			if( ( self::FOLLOW_UP_END_TIME <= date( 'H' ) && self::ONE_DAY_HOUR > date( 'H' ) ) || ( 0 != $intCurrentDayPaidHoliday ) || ( true == in_array( date( 'N', strtotime( date( 'm/d/Y' ) ) ), array( self::DAY_OF_WEEK_SUNDAY, self::DAY_OF_WEEK_SATURDAY ) ) ) ) {
				$intWeekDays = 1;
				if( 0 != $intCurrentDayPaidHoliday ) {
					$intWeekDays = $intCurrentDayPaidHoliday;
				}
				$strDateTime = date( 'm/d/Y', strtotime( '+' . $intWeekDays . 'weekdays', strtotime( date( 'm/d/Y' ) ) ) );
			}
		} else {
			$intHours				= $intHours + ( int ) date( 'H' );
			$arrstrFollowUpTime[1]	= $arrstrFollowUpTime[1] + date( 'i' );
		}

		$strFollowUpDateTime = date( 'Y-m-d H:i:s', strtotime( ' + ' . ( int ) $intDays . ' weekdays' . ' + ' . ( int ) $intHours . ' hours' . $arrstrFollowUpTime[1] . ' minutes', strtotime( $strDateTime ) ) );

	 	$intMinutes = intval( $arrstrFollowUpTime[1] / SELF::MINUTE_SEGMENT );
		if( self::HALF_MINUTE_SEGMENT <= ( $arrstrFollowUpTime[1] % SELF::MINUTE_SEGMENT ) ) $intMinutes += 1;
		$arrstrFollowUpTime[1] = $intMinutes * SELF::MINUTE_SEGMENT;

		if( self::FOLLOW_UP_END_TIME < date( 'H', strtotime( $strFollowUpDateTime ) ) || ( self::FOLLOW_UP_END_TIME == date( 'H', strtotime( $strFollowUpDateTime ) ) && 0 != date( 'i', strtotime( $strFollowUpDateTime ) ) ) ) {
			$intHours = $intHours + self::WORKING_HOUR;
		}

		$intPaidHolidayCount = CPaidHolidays::fetchPaidHolidaysCountByDatesByCountryCode( date( 'm/d/y', strtotime( $strDateTime ) ), date( 'm/d/y', strtotime( $strFollowUpDateTime ) ), CCountry::CODE_USA, $objDatabase );
		while( 0 < $intPaidHolidayCount ) {
			$strFollowUpDateTime = date( 'Y-m-d H:i:s', strtotime( ' + ' . ( int ) $intPaidHolidayCount . ' weekdays' . ' + ' . ( int ) $intHours . ' hours' . $arrstrFollowUpTime[1] . ' minutes', strtotime( $strFollowUpDateTime ) ) );
			$intPaidHolidayCount = CPaidHolidays::fetchPaidHolidaysCountByDatesByCountryCode( date( 'm/d/y', strtotime( $strFollowUpDateTime ) ), date( 'm/d/y', strtotime( $strFollowUpDateTime ) ), CCountry::CODE_USA, $objDatabase );
		}

		$this->setFollowUpDateTime( $strFollowUpDateTime );

		date_default_timezone_set( $strCurrentTimeZone ); // setting back to previous time zone after calculating follow-up
	}

	public static function validateCreditCardNumber( $strDescription ) : bool {

		$boolValidDescription = true;

		$objValidation = new CValidation();

		if( true == valStr( $strDescription ) ) {

			$strDescription = strip_tags( $strDescription );

			\Psi\CStringService::singleton()->preg_match_all( '/\b([0-9]{12,20})|(\d{4}(-|\s-|-\s|\s-\s)\d{4}(-|\s-|-\s|\s-\s)\d{1,4}(-|\s-|-\s|\s-\s)\d{1,4})|(\d{4}\s\d{4}\s\d{1,4}\s\d{1,4})|(\d{4}(\.|\s\.|\.\s|\s\.\s)\d{4}(\.|\s\.|\.\s|\s\.\s)\d{1,4}(\.|\s\.|\.\s|\s\.\s)\d{1,4})\b/', $strDescription, $arrstrDigits );

			if( true == valArr( $arrstrDigits ) ) {
				$arrstrDigits = current( $arrstrDigits );

				foreach( $arrstrDigits as $intKey => $strDigit ) {
					if( true == is_null( $strDigit ) ) {
						unset( $intKey );
					}
				}

				if( true == valArr( $arrstrDigits ) ) {
					foreach( $arrstrDigits as $strDigit ) {

						$intNumber = str_replace( array( '-', ',', ' ', '.' ), '', $strDigit );
						if( true == is_numeric( $intNumber ) && true == valObj( $objValidation, CValidation::class ) ) {
							$boolResult = $objValidation->validateCreditCardLuhnCheck( $intNumber );

							if( true == $boolResult ) {

								$boolValidDescription = false;
							}
						}
					}
				}
			}
		}
		return $boolValidDescription;
	}

	public function cancelDefects( $objAdminDatabase ) {

		$arrobjTaskDetails = ( array ) \Psi\Eos\Admin\CTasks::createService()->fetchDefectTasksByParentTaskId( $this->getId(), $objAdminDatabase );
		if( true == valArr( $arrobjTaskDetails ) ) {
			foreach( $arrobjTaskDetails as $arrobjTaskDetail ) {
				if( CTaskType::DEFECT == $arrobjTaskDetail->getTaskTypeId() ) {
					$arrobjTaskDetail->setTaskStatusId( CTaskStatus::CANCELLED );
				}
			}
		}

		return $arrobjTaskDetails;
	}

	public function checkDiagnosticId( $objLogDatabase ) {
		$arrobjDiagnostic = \Psi\Eos\Logs\CDiagnostics::createService()->fetchDiagnosticsByLastErrorTaskId( $this->getId(), $objLogDatabase );
		if( true == valArr( $arrobjDiagnostic ) ) {
			return key( $arrobjDiagnostic );
		}
		return NULL;
	}

	public function getEscalatedEmployeeName( $objPsProduct, $objPsProductOption, $objAdminDatabase, $intTaskReferenceTypeId = CTaskReferenceType::SMS_TPM ) {

		$boolActiveTimeZone = CSmsTaskLibrary::validateTimeZone();

		$arrintEmployeeIds = [];

		if( true == valObj( $objPsProductOption, 'CPsProductoption' ) ) {
			$objPsProduct = $objPsProductOption;
		}

		$strEscalatedText = '';

		if( true == $boolActiveTimeZone ) {
			if( true == in_array( $intTaskReferenceTypeId, CTaskReferenceType::$c_arrintFirstTierReferenceTypesForSms ) ) {
				// architect name
				$intEmployeeId = current( CPsProductEmployees::createService()->fetchAddByPsProductIdByPsProductOptionId( $this->getPsProductId(), $this->getPsProductOptionId(), $objAdminDatabase ) )['add_employee_id'];

				if( true == is_numeric( $objPsProduct->getArchitectEmployeeId() ) ) array_push( $arrintEmployeeIds, $objPsProduct->getArchitectEmployeeId() );
				if( true == is_numeric( $intEmployeeId ) ) array_push( $arrintEmployeeIds, $intEmployeeId );

				if( false == valArr( $arrintEmployeeIds ) ) {
					if( true == is_numeric( $objPsProduct->getDoeEmployeeId() ) ) array_push( $arrintEmployeeIds, $objPsProduct->getDoeEmployeeId() );
					if( true == is_numeric( $objPsProduct->getPoEmployeeId() ) ) array_push( $arrintEmployeeIds, $objPsProduct->getPoEmployeeId() );
				}
				if( false == valArr( $arrintEmployeeIds ) ) {
					$strEscalatedText = 'All DOE\'s';
				}
			} elseif( $intTaskReferenceTypeId == CTaskReferenceType::SMS_ARCHITECT || $intTaskReferenceTypeId == CTaskReferenceType::SMS_ADD ) {
				if( true == is_numeric( $objPsProduct->getDoeEmployeeId() ) ) array_push( $arrintEmployeeIds, $objPsProduct->getDoeEmployeeId() );
				if( true == is_numeric( $objPsProduct->getProductOwnerEmployeeId() ) ) array_push( $arrintEmployeeIds, $objPsProduct->getProductOwnerEmployeeId() );
				if( false == valArr( $arrintEmployeeIds ) ) $strEscalatedText = 'All DOE\'s';
			} elseif( $intTaskReferenceTypeId == CTaskReferenceType::SMS_DOE || $intTaskReferenceTypeId == CTaskReferenceType::SMS_PO ) {
				// need to escalate to all Doe
				$strEscalatedText = 'All DOE\'s';
			}
		} else {

			$intEmployeeId = current( CPsProductEmployees::createService()->fetchAddByPsProductIdByPsProductOptionId( $this->getPsProductId(), $this->getPsProductOptionId(), $objAdminDatabase ) )['add_employee_id'];
			if( true == in_array( $intTaskReferenceTypeId, [ CTaskReferenceType::SMS_TPM, CTaskReferenceType::SMS_QAM ] ) ) {
				// get Add
				( true == is_numeric( $intEmployeeId ) ) ? array_push( $arrintEmployeeIds, $intEmployeeId ) : array_push( $arrintEmployeeIds, CEmployee::ID_SANDEEP_GARUD );

			} else if( $intTaskReferenceTypeId == CTaskReferenceType::SMS_ADD && $intEmployeeId != CEmployee::ID_SANDEEP_GARUD ) {
				array_push( $arrintEmployeeIds, CEmployee::ID_SANDEEP_GARUD );
			} else if( $intTaskReferenceTypeId == CTaskReferenceType::SMS_SANDY || ( $intTaskReferenceTypeId == CTaskReferenceType::SMS_ADD && $intEmployeeId == CEmployee::ID_SANDEEP_GARUD ) ) {
				if( true == is_numeric( $objPsProduct->getDoeEmployeeId() ) ) array_push( $arrintEmployeeIds, $objPsProduct->getDoeEmployeeId() );
				if( true == is_numeric( $objPsProduct->getProductOwnerEmployeeId() ) ) array_push( $arrintEmployeeIds, $objPsProduct->getProductOwnerEmployeeId() );
			}
		}

		if( true == valArr( $arrintEmployeeIds ) ) {
			$arrintEmployeeIds = array_filter( array_unique( $arrintEmployeeIds ) );
			$arrmixEmployees = rekeyArray( 'preferred_name', ( array ) CEmployees::fetchEmployeesByEmployeeIds( $arrintEmployeeIds, $objAdminDatabase ) );
			if( true == valArr( $arrmixEmployees ) ) $strEscalatedText .= implode( ' & ', array_keys( $arrmixEmployees ) );
		}

		return $strEscalatedText;
	}

	public function loadManagersDetails( $objProduct, $objAdminDatabase ) {
		$strDefaultEmployeeName = 'No Employee Set';
		if( true == valObj( $objProduct, CPsProductOption::class ) ) {
			$arrstrManagerDetails  = rekeyArray( 'reference', ( array ) CPsProductOptions::createService()->fetchAllManagerEmployeeNamesById( $objProduct->getId(), $objAdminDatabase ) );
		} else {
			$arrstrManagerDetails  = rekeyArray( 'reference', ( array ) CPsProducts::createService()->fetchAllManagerEmployeeNamesById( $objProduct->getId(), $objAdminDatabase ) );
		}

		$arrintEmployeeId = CPsProductEmployees::createService()->fetchAddByPsProductIdByPsProductOptionId( $this->getPsProductId(), $this->getPsProductOptionId(), $objAdminDatabase );
		if( true == valArr( $arrintEmployeeId ) ) {
			$intAddEmployeeId = $arrintEmployeeId[0]['add_employee_id'];
		}
		$arrstrManagerDetail['Architect'] = ( true == valStr( $arrstrManagerDetails['Architect']['preferred_name'] ) ) ? $arrstrManagerDetails['Architect']['preferred_name']: $strDefaultEmployeeName;
		$arrstrManagerDetail['TPM'] = ( true == valStr( $arrstrManagerDetails['TPM']['preferred_name'] ) ) ? $arrstrManagerDetails['TPM']['preferred_name']: $strDefaultEmployeeName;
		$arrstrManagerDetail['QAM'] = ( true == valStr( $arrstrManagerDetails['QAM']['preferred_name'] ) ) ? $arrstrManagerDetails['QAM']['preferred_name']: $strDefaultEmployeeName;
		$arrstrManagerDetail['SDM'] = ( true == valStr( $arrstrManagerDetails['SDM']['preferred_name'] ) ) ? $arrstrManagerDetails['SDM']['preferred_name']: $strDefaultEmployeeName;
		$arrstrManagerDetail['SQM'] = ( true == valStr( $arrstrManagerDetails['SQM']['preferred_name'] ) ) ? $arrstrManagerDetails['SQM']['preferred_name']: $strDefaultEmployeeName;
		if( true == valId( $intAddEmployeeId ) ) {
			$objEmployeeAdd = CEmployees::fetchEmployeeById( $intAddEmployeeId, $objAdminDatabase );
			$arrstrManagerDetail['ADD'] = ( true == valObj( $objEmployeeAdd, CEmployee::class ) ) ? $objEmployeeAdd->getPreferredName(): $strDefaultEmployeeName;
		} else {
			$arrstrManagerDetail['ADD'] = $strDefaultEmployeeName;
		}
		$arrstrManagerDetail['DOE'] = ( true == valStr( $arrstrManagerDetails['DOE']['preferred_name'] ) ) ? $arrstrManagerDetails['DOE']['preferred_name']: $strDefaultEmployeeName;
		return $arrstrManagerDetail;
	}

	public function sendResolveTicketSMS( $intLastReferenceTypeId, $intLastEmployeeId, $objPsProduct, $objPsProductOption, $objAdminDatabase, $objSmsDatabase, $arrintDoeEmployeeIds ) {

		$boolActiveTimeZone = CSmsTaskLibrary::validateTimeZone();

		$objProduct = $objProduct = $objPsProduct;
		if( true == valObj( $objPsProductOption, 'CPsProductoption' ) ) {
			$objPsProduct = $objPsProductOption;
		}

		$intAddEmployeeId = current( CPsProductEmployees::createService()->fetchAddByPsProductIdByPsProductOptionId( $this->getPsProductId(), $this->getPsProductOptionId(), $objAdminDatabase ) )['add_employee_id'];

		$arrintEmployeeIds = [];

		if( true == $boolActiveTimeZone ) {
			if( ( $intLastReferenceTypeId == CTaskReferenceType::SMS_ARCHITECT || $intLastReferenceTypeId == CTaskReferenceType::SMS_ADD ) || ( true == in_array( $intLastReferenceTypeId, CTaskReferenceType::$c_arrintFirstTierReferenceTypesForSms ) ) ) {
				$arrintEmployeeIds = array( CTaskReferenceType::SMS_SDM => $objPsProduct->getSdmEmployeeId(), CTaskReferenceType::SMS_SQM => $objPsProduct->getSqmEmployeeId(), CTaskReferenceType::SMS_QAM => $objPsProduct->getQamEmployeeId(), CTaskReferenceType::SMS_TPM => $objPsProduct->getTpmEmployeeId() );
				if( $intLastReferenceTypeId == CTaskReferenceType::SMS_ARCHITECT || $intLastReferenceTypeId == CTaskReferenceType::SMS_ADD ) {
					$arrintEmployeeIds[CTaskReferenceType::SMS_ARCHITECT] = $objPsProduct->getArchitectEmployeeId();
					$arrintEmployeeIds[CTaskReferenceType::SMS_ADD] = $intAddEmployeeId;
				}
			} elseif( $intLastReferenceTypeId == CTaskReferenceType::SMS_DOE || $intLastReferenceTypeId == CTaskReferenceType::SMS_PO ) {

				$arrintEmployeeIds = array( CTaskReferenceType::SMS_PO => $objPsProduct->getProductOwnerEmployeeId(), CTaskReferenceType::SMS_DOE => $objPsProduct->getDoeEmployeeId(), CTaskReferenceType::SMS_ARCHITECT => $objPsProduct->getArchitectEmployeeId(), CTaskReferenceType::SMS_ADD => $intAddEmployeeId, CTaskReferenceType::SMS_SQM => $objPsProduct->getSqmEmployeeId(), CTaskReferenceType::SMS_SDM => $objPsProduct->getSdmEmployeeId(), CTaskReferenceType::SMS_TPM => $objPsProduct->getTpmEmployeeId(), CTaskReferenceType::SMS_QAM => $objPsProduct->getQamEmployeeId() );
			} elseif( $intLastReferenceTypeId == CTaskReferenceType::SMS_ALL_DOE ) {
				$arrintEmployeeIds = array( CTaskReferenceType::SMS_PO => $objPsProduct->getProductOwnerEmployeeId(), CTaskReferenceType::SMS_ADD => $intAddEmployeeId, CTaskReferenceType::SMS_ARCHITECT => $objPsProduct->getArchitectEmployeeId(), CTaskReferenceType::SMS_SQM => $objPsProduct->getSqmEmployeeId(), CTaskReferenceType::SMS_SDM => $objPsProduct->getSdmEmployeeId(), CTaskReferenceType::SMS_TPM => $objPsProduct->getTpmEmployeeId(), CTaskReferenceType::SMS_QAM => $objPsProduct->getQamEmployeeId() );
			}
		} else {
			if( ( true == in_array( $intLastReferenceTypeId, array( CTaskReferenceType::SMS_TPM, CTaskReferenceType::SMS_QAM ) ) ) || $intLastReferenceTypeId == CTaskReferenceType::SMS_ADD ) {
				$arrintEmployeeIds = array( CTaskReferenceType::SMS_TPM => $objPsProduct->getTpmEmployeeId(), CTaskReferenceType::SMS_QAM => $objPsProduct->getQamEmployeeId() );
				if( $intLastReferenceTypeId == CTaskReferenceType::SMS_ADD ) {
					$arrintEmployeeIds[CTaskReferenceType::SMS_ADD] = $intAddEmployeeId;
				}
			} elseif( $intLastReferenceTypeId == CTaskReferenceType::SMS_SANDY ) {
				$arrintEmployeeIds = array( CTaskReferenceType::SMS_SANDY => CEmployee::ID_SANDEEP_GARUD, CTaskReferenceType::SMS_ADD => $intAddEmployeeId, CTaskReferenceType::SMS_TPM => $objPsProduct->getTpmEmployeeId(), CTaskReferenceType::SMS_QAM => $objPsProduct->getQamEmployeeId() );
			} elseif( $intLastReferenceTypeId == CTaskReferenceType::SMS_DOE || $intLastReferenceTypeId == CTaskReferenceType::SMS_PO ) {
				$arrintEmployeeIds = array( CTaskReferenceType::SMS_PO => $objPsProduct->getProductOwnerEmployeeId(), CTaskReferenceType::SMS_DOE => $objPsProduct->getDoeEmployeeId(), CTaskReferenceType::SMS_ADD => $intAddEmployeeId, CTaskReferenceType::SMS_SANDY => CEmployee::ID_SANDEEP_GARUD, CTaskReferenceType::SMS_TPM => $objPsProduct->getTpmEmployeeId(), CTaskReferenceType::SMS_QAM => $objPsProduct->getQamEmployeeId() );
			}
		}

		if ( ( $intKey = array_search( $intLastEmployeeId, $arrintEmployeeIds ) ) !== false ) unset( $arrintEmployeeIds[$intKey] );
		$arrintEmployeeIds = array_unique( array_filter( $arrintEmployeeIds ) );

		foreach( $arrintEmployeeIds as $intReferenceType => $intEmployeeId ) {
			$this->sendSMS( $intEmployeeId, $objPsProduct, $objPsProductOption, $objAdminDatabase, $objSmsDatabase, $intReferenceType, false, false, false, true, NULL, true );
		}

		if( $intLastReferenceTypeId == CTaskReferenceType::SMS_ALL_DOE ) {
			$arrintDoeEmployeeIds = array_filter( array_unique( $arrintDoeEmployeeIds ) );
			if ( ( $intKey = array_search( $intLastEmployeeId, $arrintDoeEmployeeIds ) ) !== false ) unset( $arrintDoeEmployeeIds[$intKey] );
			foreach( $arrintDoeEmployeeIds as $intKey => $intDoeEmployeeId ) {
				$this->sendSMS( $intDoeEmployeeId, $objProduct, $objPsProductOption, $objAdminDatabase, $objSmsDatabase, CTaskReferenceType::SMS_ALL_DOE, false, false, false, true, NULL, true );
			}
		}

		if( valArr( $this->m_arrobjSMSTaskNotes ) && false == \Psi\Eos\Admin\CTaskNotes::createService()->bulkInsert( $this->m_arrobjSMSTaskNotes, CUser::ID_SYSTEM, $objAdminDatabase ) ) {
			return;
		}

	}

	public function resolveTaskDetails( $intEmployeeId, $intLastReferenceTypeId, $objSmsDatabase, $objAdminDatabase ) {

		$objPsProduct = CPsProducts::createService()->fetchPsProductById( $this->getPsProductId(), $objAdminDatabase );
		$objPsProductOption = ( true == is_numeric( $this->getPsProductOptionId() ) ) ? CPsProductOptions::createService()->fetchPsProductOptionById( $this->getPsProductOptionId(), $objAdminDatabase ) : NULL;
		$arrintDoeEmployeeIds  = rekeyArray( 'doe_id', CEmployees::fetchDirectorEmployeeIds( $objAdminDatabase ) );
		$arrintDoeEmployeeIds = ( true == valArr( $arrintDoeEmployeeIds ) ) ? array_keys( $arrintDoeEmployeeIds ) : [];
		$this->sendResolveTicketSMS( $intLastReferenceTypeId, $intEmployeeId, $objPsProduct, $objPsProductOption, $objAdminDatabase, $objSmsDatabase, $arrintDoeEmployeeIds );

	}

	public function checkUserByPreferences( $intUserId, $objAdminDatabase ) {

		$boolSendEmailToUser = true;
		$arrstrJsonDecodedData = ( true == valStr( $this->getJsonTaskData() ) ) ? ( array ) json_decode( $this->getJsonTaskData(), true ) : [];
		if( isset( $arrstrJsonDecodedData['updated_by'] ) && $intUserId == $arrstrJsonDecodedData['updated_by'] ) return false;

		$objTaskUserSetting	 = CTaskUserSettings::fetchTaskUserSettingByUserId( $intUserId, $objAdminDatabase );
		if( false == valObj( $objTaskUserSetting, 'CTaskUserSetting' ) ) {
			return $boolSendEmailToUser;
		}

		$arrmixUserSettings = json_decode( $objTaskUserSetting->getSettingDetails(), true );
		if( false == valArr( $arrmixUserSettings ) ) return $boolSendEmailToUser;

		$intAssignedToUser = ( true == array_key_exists( 'user_id', $arrstrJsonDecodedData ) ) ? $arrstrJsonDecodedData['user_id'] : $this->getUserId();

		if( true == valArrKeyExists( $arrstrJsonDecodedData, 'task_note' ) ) {
			if( ( ( $this->getUserId() == $intUserId && false == array_key_exists( 'A_NOTE_IS_ADDED_ATM', $arrmixUserSettings ) ) ) || false == array_key_exists( 'A_NOTE_IS_ADDED_STAKEHOLDER', $arrmixUserSettings ) ) {
				return true;
			} else {
				$boolSendEmailToUser = false;
			}
		}

		if( true == valArrKeyExists( $arrstrJsonDecodedData, 'description' ) ) {
			if( ( ( $this->getUserId() == $intUserId && false == array_key_exists( 'DESCRIPTION_CHANGES_ATM', $arrmixUserSettings ) ) ) || false == array_key_exists( 'DESCRIPTION_CHANGES_STAKEHOLDER', $arrmixUserSettings ) ) {
				return true;
			} else {
				$boolSendEmailToUser = false;
			}
		}

		if( true == valArrKeyExists( $arrstrJsonDecodedData, 'title' ) ) {
			if( ( ( $this->getUserId() == $intUserId && false == array_key_exists( 'TITLE_CHANGES_ATM', $arrmixUserSettings ) ) ) || false == array_key_exists( 'TITLE_CHANGES_STAKEHOLDER', $arrmixUserSettings ) ) {
				return true;
			} else {
				$boolSendEmailToUser = false;
			}
		}

		if( true == valArrKeyExists( $arrstrJsonDecodedData, 'acceptance_criteria' ) ) {
			if( ( ( $this->getUserId() == $intUserId && false == array_key_exists( 'ACCEPTANCE_CRITERIA_CHANGES_ATM', $arrmixUserSettings ) ) ) || false == array_key_exists( 'ACCEPTANCE_CRITERIA_CHANGES_STAKEHOLDER', $arrmixUserSettings ) ) {
				return true;
			} else {
				$boolSendEmailToUser = false;
			}
		}

		if( true == valArrKeyExists( $arrstrJsonDecodedData, 'task_release_id' ) ) {
			if( ( ( $this->getUserId() == $intUserId && false == array_key_exists( 'RELEASE_DATE_CHANGES_ATM', $arrmixUserSettings ) ) ) || false == array_key_exists( 'RELEASE_DATE_CHANGES_STAKEHOLDER', $arrmixUserSettings ) ) {
				return true;
			} else {
				$boolSendEmailToUser = false;
			}
		}

		if( true == valArrKeyExists( $arrstrJsonDecodedData, 'user_id' ) ) {
			if( ( ( $this->getUserId() == $intUserId && false == array_key_exists( 'TASK_ASSIGNMENT_CHANGES_ATM', $arrmixUserSettings ) ) ) || false == array_key_exists( 'TASK_ASSIGNMENT_CHANGES_STAKEHOLDER', $arrmixUserSettings ) ) {
				return true;
			} else {
				$boolSendEmailToUser = false;
			}
		}

		if( true == valArrKeyExists( $arrstrJsonDecodedData, 'task_priority_id' ) ) {
			if( ( ( $this->getUserId() == $intUserId && false == array_key_exists( 'PRIORITY_CHANGES_ATM', $arrmixUserSettings ) ) ) || false == array_key_exists( 'PRIORITY_CHANGES_STAKEHOLDER', $arrmixUserSettings ) ) {
				return true;
			} else {
				$boolSendEmailToUser = false;
			}
		}

		if( true == valArrKeyExists( $arrstrJsonDecodedData, 'task_status_id' ) ) {
			if( ( ( $this->getUserId() == $intUserId && false == array_key_exists( 'STATUS_HAS_BEEN_CHANGED_ATM', $arrmixUserSettings ) ) ) || false == array_key_exists( 'STATUS_HAS_BEEN_CHANGED_STAKEHOLDER', $arrmixUserSettings ) ) {
			    $objTaskStatus = \Psi\Eos\Admin\CTaskStatuses::createService()->fetchTaskStatusByStatusId( $this->getTaskStatusId(), $objAdminDatabase );
				if( true == valObj( $objTaskStatus, 'CTaskStatus' ) ) $strTaskStatus = str_replace( ' ', '_', strtoupper( $objTaskStatus->getName() ) );
				if( true == array_key_exists( $strTaskStatus, $arrmixUserSettings ) ) return false;
				return true;
			} else {
				$boolSendEmailToUser = false;
			}
		}

		return $boolSendEmailToUser;

	}

	public function getTollFreeNumberByLocal( $strLocalCode ) {
		switch( $strLocalCode ) {
			case CLocale::SPANISH_SPAIN:
				return CMessage::SPAIN_HELP_LINE_NUMBER;
				break;

			case CLocale::ENGLISH_IRELAND:
				return CMessage::IRELAND_HELP_LINE_NUMBER;
				break;

			case CLocale::ENGLISH_GREAT_BRITAIN:
				return CMessage::UK_HELP_LINE_NUMBER;
				break;

			case CLocale::DEFAULT_LOCALE:
				return CMessage::HELP_LINE_NUMBER;
				break;

			case CLocale::FRENCH_FRANCE:
				return CMessage::FRANCE_HELP_LINE_NUMBER;
				break;
			default:
				return CMessage::HELP_LINE_NUMBER;
				break;
		}

	}

}

?>