<?php

class CPsDocumentType extends CBasePsDocumentType {

	const DOCUMENT_TYPE_INVOICE									= 1;
	const DOCUMENT_TYPE_UNDERWRITING_PAPERWORK					= 2;
	const DOCUMENT_TYPE_EMPLOYEE_AGREEMENTS						= 3;
	const DOCUMENT_TYPE_NON_DISCLOSURE_AGREEMENT 				= 4;
	const DOCUMENT_TYPE_LICENSE_AGREEMENT 						= 6;
	const DOCUMENT_TYPE_EMPLOYEE_PHOTO							= 8;
	const DOCUMENT_TYPE_EMPLOYEE_MAIN_PHOTO						= 9;
	const DOCUMENT_TYPE_APPROVED_MERCHANT_SIGNATURES			= 10;
	const DOCUMENT_TYPE_SUPPORT_DOCUMENT						= 11;
	const DOCUMENT_TYPE_AD										= 12;
	const DOCUMENT_TYPE_REIMBURSEMENT_PROOF						= 13;
	const DOCUMENT_TYPE_EMPLOYEE_APPLICATION_RESUME				= 14;
	const DOCUMENT_TYPE_NEW_HIRE_DOCUMENTS						= 15;
	const DOCUMENT_TYPE_COMPENSATION							= 16;
	const DOCUMENT_TYPE_PERFORMANCE_REVIEW_DISCIPLINE			= 17;
	const DOCUMENT_TYPE_TERMINATION								= 18;
	const DOCUMENT_TYPE_LEGAL_DOCUMENTS							= 19;
	const DOCUMENT_TYPE_EMPLOYEE_HANDBOOK						= 20;
	const DOCUMENT_TYPE_EMPLOYEE_CERTIFICATE					= 21;
	const DOCUMENT_TYPE_SECURITY_POLICY							= 22;
	const DOCUMENT_TYPE_EDUCATIONAL								= 23;
	const DOCUMENT_TYPE_EMPLOYMENT								= 24;
	const DOCUMENT_TYPE_TAX_RELATED								= 25;
	const DOCUMENT_TYPE_HEALTH_RELATED							= 26;
	const DOCUMENT_TYPE_EXTRA_DOCUMENTS							= 27;
	const DOCUMENT_TYPE_TRAINING_SESSION_STUDY_MATERIAL			= 28;
	const DOCUMENT_TYPE_CONSULTATION_SUMMARY					= 29;
	const DOCUMENT_TYPE_CLOSE_OUT_SUMMARY						= 30;
	const DOCUMENT_TYPE_IMPLEMENTATION							= 31;
	const DOCUMENT_TYPE_OTHER									= 32;
	const DOCUMENT_TYPE_MASS_EMAILS								= 33;
	const DOCUMENT_TYPE_PS_NOTIFICATION							= 34;
	const DOCUMENT_TYPE_PS_ASSETS								= 35;
	const DOCUMENT_TYPE_IT_POLICY								= 36;
	const DOCUMENT_TYPE_SUCESS_REVIEW							= 37;
	const DOCUMENT_TYPE_EMPLOYEE_POLICY							= 38;
	const EMPLOYEE_APPLICATION_TEST_SCORE						= 39;
	const DOCUMENT_TYPE_ORDER_FORMS								= 40;
	const MASTER_POLICY											= 41;
	const DOCUMENT_TYPE_DIGITAL_MARKETING_DOCUMENT				= 43;
	const CONTRACT_DRAFT										= 44;
	const COMPANY_VALUE_NOMINATION                              = 45;
	const PS_VENDOR_AGREEMENTS                                  = 46;
	const DOCUMENT_TYPE_RESIGNATION_LETTER						= 47;
	const DOCUMENT_TYPE_PROBLEMS_TO_SOLVE                       = 49;
	const DOCUMENT_TYPE_CROSS_PRODUCT_REQUEST                   = 50;
	const DOCUMENT_TYPE_QUESTIONS_TO_ANSWER                     = 51;
	const DOCUMENT_TYPE_AMENDMENT								= 52;
	const DOCUMENT_TYPE_FEEDBACK                                = 55;
	const DOCUMENT_TYPE_DESIGN_ASSET    						= 54;
	const TRAINING                                              = 48;
	const DOCUMENT_COMPANY_PAYMENT_FAILURE                      = 53;
	const EMPLOYEE_APPLICATION_PROFILE_PHOTO					= 56;
	const DOCUMENT_TYPE_ACTION_ITEM                             = 57;

	public static $c_arrintPsLeadDocumentTypeIds = array(
		self::DOCUMENT_TYPE_SUPPORT_DOCUMENT,
		self::DOCUMENT_TYPE_UNDERWRITING_PAPERWORK,
		self::DOCUMENT_TYPE_NON_DISCLOSURE_AGREEMENT,
		self::DOCUMENT_TYPE_LICENSE_AGREEMENT,
		self::DOCUMENT_TYPE_APPROVED_MERCHANT_SIGNATURES,
		self::DOCUMENT_TYPE_CONSULTATION_SUMMARY,
		self::DOCUMENT_TYPE_CLOSE_OUT_SUMMARY,
		self::DOCUMENT_TYPE_IMPLEMENTATION,
		self::DOCUMENT_TYPE_OTHER,
		self::DOCUMENT_TYPE_SUCESS_REVIEW,
		self::DOCUMENT_TYPE_ORDER_FORMS,
		self::DOCUMENT_TYPE_TERMINATION,
		self::DOCUMENT_TYPE_AMENDMENT,
		self::TRAINING
	);
	public static $c_arrintEmployeeDocumentTypeIds = array(
		self::DOCUMENT_TYPE_EMPLOYEE_AGREEMENTS,
		self::DOCUMENT_TYPE_NEW_HIRE_DOCUMENTS,
		self::DOCUMENT_TYPE_COMPENSATION,
		self::DOCUMENT_TYPE_PERFORMANCE_REVIEW_DISCIPLINE,
		self::DOCUMENT_TYPE_TERMINATION,
		self::DOCUMENT_TYPE_LEGAL_DOCUMENTS,
		self::DOCUMENT_TYPE_EMPLOYEE_CERTIFICATE,
		self::DOCUMENT_TYPE_SECURITY_POLICY,
		self::DOCUMENT_TYPE_EDUCATIONAL,
		self::DOCUMENT_TYPE_EMPLOYMENT,
		self::DOCUMENT_TYPE_TAX_RELATED,
		self::DOCUMENT_TYPE_HEALTH_RELATED,
		self::DOCUMENT_TYPE_EXTRA_DOCUMENTS,
		self::DOCUMENT_TYPE_EMPLOYEE_POLICY,
		self::COMPANY_VALUE_NOMINATION,
		self::EMPLOYEE_APPLICATION_TEST_SCORE
	);

	public static $c_arrintEmployeeApplicationDocumentTypeIds = [
		CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_APPLICATION_RESUME,
		CPsDocumentType::DOCUMENT_TYPE_EDUCATIONAL,
		CPsDocumentType::DOCUMENT_TYPE_EMPLOYMENT,
		CPsDocumentType::DOCUMENT_TYPE_EXTRA_DOCUMENTS,
		CPsDocumentType::DOCUMENT_TYPE_HEALTH_RELATED,
		CPsDocumentType::EMPLOYEE_APPLICATION_TEST_SCORE,
		CPsDocumentType::DOCUMENT_TYPE_TAX_RELATED,
		CPsDocumentType::EMPLOYEE_APPLICATION_PROFILE_PHOTO
	];

	public static $c_arrintClientSummaryRequiredDocument = [
		self::DOCUMENT_TYPE_CONSULTATION_SUMMARY,
		self::DOCUMENT_TYPE_CLOSE_OUT_SUMMARY,
		self::DOCUMENT_TYPE_IMPLEMENTATION
	];

	public static $c_arrmixProductManagementDocument = [
		self::DOCUMENT_TYPE_PROBLEMS_TO_SOLVE		=> 'Problem To Solve',
		self::DOCUMENT_TYPE_CROSS_PRODUCT_REQUEST	=> 'Cross Product Request',
		self::DOCUMENT_TYPE_QUESTIONS_TO_ANSWER		=> 'Questions To Answer',
		self::DOCUMENT_TYPE_FEEDBACK 				=> 'Feedback',
		self::DOCUMENT_TYPE_DESIGN_ASSET			=> 'Design Asset',
		self::DOCUMENT_TYPE_ACTION_ITEM             => 'Action Item'
	];

	const DOCUMENT_RESUME			= 'resume';
	const DOCUMENT_COVER_LETTER		= 'cover_letter';

	/**
	 * Get Functions
	 */

	public static function getDocumentTypePermissioning( $intDocumentTypeId ) {

		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_INVOICE]								= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_UNDERWRITING_PAPERWORK] 				= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLSX, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOCX, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );

		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_EMPLOYEE_AGREEMENTS]					= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );

		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_NON_DISCLOSURE_AGREEMENT]				= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLSX, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOCX, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_APPROVED_MERCHANT_SIGNATURES]			= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLSX, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOCX, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_LICENSE_AGREEMENT]						= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLSX, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOCX, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_EMPLOYEE_PHOTO]						= array( CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_EMPLOYEE_MAIN_PHOTO]					= array( CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_SUPPORT_DOCUMENT]						= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG, CFileExtension::APPLICATION_DOCX );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_AD]									= array( CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_REIMBURSEMENT_PROOF]					= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_NEW_HIRE_DOCUMENTS]					= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_COMPENSATION]							= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_PERFORMANCE_REVIEW_DISCIPLINE]			= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_TERMINATION]							= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_LEGAL_DOCUMENTS]						= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_EMPLOYEE_HANDBOOK]						= array( CFileExtension::APPLICATION_PDF );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_EMPLOYEE_CERTIFICATE]					= array( CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_SECURITY_POLICY]						= array( CFileExtension::APPLICATION_PDF );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_EDUCATIONAL]							= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_EMPLOYMENT]							= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_TAX_RELATED]							= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_HEALTH_RELATED]						= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_EXTRA_DOCUMENTS]						= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_TRAINING_SESSION_STUDY_MATERIAL]		= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::APPLICATION_ODS, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG, CFileExtension::APPLICATION_DOCX, CFileExtension::APPLICATION_ODT, CFileExtension::APPLICATION_ODP );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_MASS_EMAILS]							= array( CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_XLSX, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_DOCX, CFileExtension::APPLICATION_PDF,CFileExtension::APPLICATION_ODS, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG, CFileExtension::IMAGE_BMP, CFileExtension::APPLICATION_ODT, CFileExtension::APPLICATION_PPTX );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_EMPLOYEE_POLICY]						= array( CFileExtension::APPLICATION_PDF );
		$arrintDocumentPermissioning[self::DOCUMENT_TYPE_DIGITAL_MARKETING_DOCUMENT]			= [ CFileExtension::APPLICATION_PDF ];
		$arrintDocumentPermissioning[self::COMPANY_VALUE_NOMINATION]                            = array_merge( CFileExtension::$c_arrintImageExtensionIds, CFileExtension::$c_arrintVideoExtensionIds, [ CFileExtension::APPLICATION_PDF ] );
		$arrintDocumentPermissioning[self::EMPLOYEE_APPLICATION_TEST_SCORE]						= [ CFileExtension::APPLICATION_PDF ];

		if( false == is_null( $intDocumentTypeId ) && true == is_numeric( $intDocumentTypeId ) ) {
			return  $arrintDocumentPermissioning[$intDocumentTypeId];
		} else {
			return  false;
		}
	}

	public static function determinePsDocumentTypeNameByPsDocumentTypeId( $intPsDocumentTypeId ) {

		$arrstrPsDocumentTypes	 = array(
			self::DOCUMENT_TYPE_INVOICE 						=> 'Invoices',
			self::DOCUMENT_TYPE_UNDERWRITING_PAPERWORK 			=> 'Underwriting Paperwork',
			self::DOCUMENT_TYPE_EMPLOYEE_AGREEMENTS 			=> 'Employee Agreements',
			self::DOCUMENT_TYPE_NON_DISCLOSURE_AGREEMENT 		=> 'Non-disclosure Agreement',
			self::DOCUMENT_TYPE_LICENSE_AGREEMENT 				=> 'License Agreement',
			self::DOCUMENT_TYPE_EMPLOYEE_PHOTO 					=> 'Employee Photo',
			self::DOCUMENT_TYPE_EMPLOYEE_MAIN_PHOTO 			=> 'Employee Main Photo',
			self::DOCUMENT_TYPE_APPROVED_MERCHANT_SIGNATURES	=> 'Approved Merchant Signatures',
			self::DOCUMENT_TYPE_SUPPORT_DOCUMENT 				=> 'Support Item',
			self::DOCUMENT_TYPE_AD 								=> 'Ad',
			self::DOCUMENT_TYPE_REIMBURSEMENT_PROOF				=> 'Reimbursement Proof',
			self::DOCUMENT_TYPE_EMPLOYEE_APPLICATION_RESUME		=> 'Employee Application Resume',
			self::DOCUMENT_TYPE_NEW_HIRE_DOCUMENTS				=> 'New Hire Documents',
			self::DOCUMENT_TYPE_COMPENSATION					=> 'Compensation (Pay)',
			self::DOCUMENT_TYPE_PERFORMANCE_REVIEW_DISCIPLINE	=> 'Performance Review/ Discipline',
			self::DOCUMENT_TYPE_TERMINATION 					=> 'Termination',
			self::DOCUMENT_TYPE_LEGAL_DOCUMENTS 				=> 'Legal',
			self::DOCUMENT_TYPE_EMPLOYEE_HANDBOOK 				=> 'Employee Handbook',
			self::DOCUMENT_TYPE_SECURITY_POLICY 				=> 'Security Policy',
			self::DOCUMENT_TYPE_EDUCATIONAL 					=> 'Educational',
			self::DOCUMENT_TYPE_EMPLOYMENT 						=> 'Employment',
			self::DOCUMENT_TYPE_TAX_RELATED 					=> 'Tax Related',
			self::DOCUMENT_TYPE_HEALTH_RELATED 					=> 'Health Related',
			self::DOCUMENT_TYPE_EXTRA_DOCUMENTS 				=> 'Extra Documents'

		);

		return ( true == array_key_exists( $intPsDocumentTypeId, $arrstrPsDocumentTypes ) ) ? $arrstrPsDocumentTypes[$intPsDocumentTypeId] : $arrstrPsDocumentTypes[key( $arrstrPsDocumentTypes )];

	}

}
?>