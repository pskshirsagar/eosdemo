<?php

class CSurveyRuleType extends CBaseSurveyRuleType {

	const DISPLAY		= 1;
	const VALIDATION	= 2;
	const ACTION		= 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>