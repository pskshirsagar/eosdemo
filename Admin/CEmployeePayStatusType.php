<?php

class CEmployeePayStatusType extends CBaseEmployeePayStatusType {

	const NOT_SUBMITTED				= 1;
	const PENDING_APPROVAL			= 2;
	const APPROVED					= 3;
	const REJECTED 					= 4;
	const ASSIGNED_TO_PAY_PERIOD	= 5;
	const ALLOCATED_TO_ADP 			= 6;
	const ALLOCATED_TO_CC 			= 7;
	const APPROVED_BY_MANAGER 		= 8;
	const REJECTED_BY_MANAGER 		= 9;
	const COMPLETED 				= 10;

	public static $c_arrintStatusTypeForIndianReimbursement = array(
		self::NOT_SUBMITTED,
		self::PENDING_APPROVAL,
		self::APPROVED,
		self::REJECTED,
		self::COMPLETED
	);

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function loadTemplateConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['NOT_SUBMITTED']				= CEmployeePayStatusType::NOT_SUBMITTED;
		$arrmixTemplateParameters['PENDING_APPROVAL']			= CEmployeePayStatusType::PENDING_APPROVAL;
		$arrmixTemplateParameters['APPROVED']					= CEmployeePayStatusType::APPROVED;
		$arrmixTemplateParameters['REJECTED']					= CEmployeePayStatusType::REJECTED;
		$arrmixTemplateParameters['ASSIGNED_TO_PAY_PERIOD']		= CEmployeePayStatusType::ASSIGNED_TO_PAY_PERIOD;
		$arrmixTemplateParameters['ALLOCATED_TO_ADP']			= CEmployeePayStatusType::ALLOCATED_TO_ADP;
		$arrmixTemplateParameters['ALLOCATED_TO_CC']			= CEmployeePayStatusType::ALLOCATED_TO_CC;
		$arrmixTemplateParameters['COMPLETED']					= CEmployeePayStatusType::COMPLETED;

		return $arrmixTemplateParameters;
	}

}
?>