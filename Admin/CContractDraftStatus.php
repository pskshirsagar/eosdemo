<?php

class CContractDraftStatus extends CBaseContractDraftStatus {

	const NEW = 1;
	const APPROVED = 2;
	const WAITING_FOR_RVP = 3;
	const REMIND_RVP = 4;
	const REDLINE = 5;
	const AWAITING_CLIENT_SIGNATURE = 6;
	const AWAITING_ENTRATA_SIGNATURE = 7;
	const CANCELLED = 8;
	const COMPLETED = 9;
	const AWAITING_MANUAL_APPROVAL = 10;
	const IN_PROGRESS = 11;
	const CONTRACT_READY_TO_PROCESS = 12;

	public static $c_arrstrContractDraftStatuses = [
		self::NEW							=> 'New',
		self::APPROVED						=> 'Approved',
		self::WAITING_FOR_RVP				=> 'Waiting for RVP',
		self::REMIND_RVP					=> 'Remind RVP',
		self::REDLINE						=> 'Redline',
		self::AWAITING_CLIENT_SIGNATURE		=> 'Awaiting Client Signature',
		self::AWAITING_ENTRATA_SIGNATURE	=> 'Awaiting Entrata Signature',
		self::CANCELLED						=> 'Cancelled',
		self::COMPLETED						=> 'Completed',
		self::AWAITING_MANUAL_APPROVAL		=> 'Awaiting Manual Approval',
		self::IN_PROGRESS					=> 'In Progress',
		self::CONTRACT_READY_TO_PROCESS		=> 'Contract Ready To Process '
	];

	public static $c_arrintActiveContractDraftStatusIds = [
		self::NEW,
		self::APPROVED,
		self::WAITING_FOR_RVP,
		self::REDLINE,
		self::AWAITING_CLIENT_SIGNATURE,
		self::AWAITING_ENTRATA_SIGNATURE
	];

	public static $c_arrintDraftingContractDraftStatusIds = [
		self::NEW,
		self::APPROVED,
		self::WAITING_FOR_RVP,
		self::REDLINE,
		self::IN_PROGRESS
	];

	public static $c_arrintProcessingContractDraftStatusIds = [
		self::REMIND_RVP,
		self::AWAITING_CLIENT_SIGNATURE,
		self::AWAITING_ENTRATA_SIGNATURE,
		self::AWAITING_MANUAL_APPROVAL,
		self::CONTRACT_READY_TO_PROCESS
	];

	public static $c_arrintContractDraftStatusIds = [
		self::REMIND_RVP,
		self::REDLINE,
		self::AWAITING_CLIENT_SIGNATURE
	];

	public static $c_arrintEntrataContractDraftStatusIds = [
		self::AWAITING_CLIENT_SIGNATURE,
		self::AWAITING_ENTRATA_SIGNATURE
	];

	public static $c_arrintSkipManualContractDraftStatusIds = [ self::APPROVED, self::REDLINE ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>