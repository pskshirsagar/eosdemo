<?php

class CTaxDocumentCategoryAmount extends CBaseTaxDocumentCategoryAmount {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeDocumentCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialYearId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaximumAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubAmounts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>