<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationResponses
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationResponses extends CBaseEmployeeApplicationResponses {

	public static function fetchEmployeeApplicationResponseIdsByEmployeeApplicationIds( $arrintEmployeeApplicationIds, $objDatabase ) {
		$strSql = 'SELECT * FROM employee_application_responses WHERE employee_application_id IN( ' . implode( ',', $arrintEmployeeApplicationIds ) . ')';
		return self::fetchEmployeeApplicationResponses( $strSql, $objDatabase );
	}

}
?>