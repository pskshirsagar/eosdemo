<?php

class CContractTerminationEvent extends CBaseContractTerminationEvent {

	protected $m_strBundleName;
	protected $m_strProductName;
	protected $m_strPropertyName;
	protected $m_strContractTerminationEventType;
	protected $m_strContractTerminationEventStatus;

	public function setDefaults() {
		$this->setContractTerminationEventTypeId( CContractTerminationEventType::TERMINATE_PRODUCT );
	}

	/**
	 * Set Functions
	 */

	public function setContractTerminationEventType( $strContractTerminationEventType ) {
		$this->m_strContractTerminationEventType = $strContractTerminationEventType;
	}

	public function setContractTerminationEventStatus( $strContractTerminationEventStatus ) {
		$this->m_strContractTerminationEventStatus = $strContractTerminationEventStatus;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setProductName( $strProductName ) {
		$this->m_strProductName = $strProductName;
	}

	public function setBundleName( $strBundleName ) {
		$this->m_strBundleName = $strBundleName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['contract_termination_event_type'] ) ) {
			$this->setContractTerminationEventType( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['contract_termination_event_type'] ) : $arrmixValues['contract_termination_event_type'] );
		}
		if( true == isset( $arrmixValues['contract_termination_event_status'] ) ) {
			$this->setContractTerminationEventStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['contract_termination_event_status'] ) : $arrmixValues['contract_termination_event_status'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_name'] ) : $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['product_name'] ) ) {
			$this->setProductName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['product_name'] ) : $arrmixValues['product_name'] );
		}
		if( true == isset( $arrmixValues['bundle_name'] ) ) {
			$this->setBundleName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bundle_name'] ) : $arrmixValues['bundle_name'] );
		}

	}

	/**
	 * Get Functions
	 */

	public function getContractTerminationEventType() {
		return $this->m_strContractTerminationEventType;
	}

	public function getContractTerminationEventStatus() {
		return $this->m_strContractTerminationEventStatus;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getProductName() {
		return $this->m_strProductName;
	}

	public function getBundleName() {
		return $this->m_strBundleName;
	}

	/**
	 * Validate functions
	 */

	public function valContractTerminationEventTypeId() {
		$boolIsValid = true;

		$objReflectionClass		= new ReflectionClass( 'CContractTerminationEventType' );
		$arrstrConstants 		= $objReflectionClass->getConstants();

		if( false == in_array( $this->getContractTerminationEventTypeId(), $arrstrConstants ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( '', '', 'Invalid contract termination event type id.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$this->valContractTerminationEventTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>