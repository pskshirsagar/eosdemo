<?php

class CTaskFunctionalGroup extends CBaseTaskFunctionalGroup {


	protected $m_strTranslatedFunctionalGroupName;

	public static $c_arrstrDefaultFunctionalGroups = array(
		'15'	=> 'GS-GIG/AA/MX',
		'16'	=> 'GS-Student',
		'17'	=> 'GS-UK'
	);

	public static $c_arrstrGreystarFunctionGroupsEmailAddresses = array(
		'15'		=> 'USMFEntrataSupport@greystar.com',
		'16'		=> 'StudentEntrataSupport@greystar.com',
		'17'		=> 'EUGALOPSupport@greystar.com'
	);

	/**
	 * get Functions
	 */

	public function getTranslatedFunctionalGroupName() {
		return $this->m_strTranslatedFunctionalGroupName;
	}

	/**
	 * Set Functions
	 */

	public function setTranslatedFunctionalGroupName( $strTranslatedFunctionalGroupName ) {
		$this->m_strTranslatedFunctionalGroupName = $strTranslatedFunctionalGroupName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == array_key_exists( 'translated_group_name', $arrmixValues ) ) {
			$this->setTranslatedFunctionalGroupName( $arrmixValues['translated_group_name'] );
		}
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>