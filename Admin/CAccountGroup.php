<?php

class CAccountGroup extends CBaseAccountGroup {

	const TYPE_AMSI 			= 1;
	const TYPE_YARDI 			= 2;
	const TYPE_TIMBERLINE 		= 3;
	const TYPE_REALPAGE 		= 4;
	const TYPE_RIVERSTONE 		= 5;
	const TYPE_AMSI_PS_BILLED 	= 6;

	/**
	 * Validation Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>