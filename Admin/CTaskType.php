<?php

class CTaskType extends CBaseTaskType {

	const WEBSITE							= 1;
	const PROPERTY							= 2;
	const TRAINING							= 3;
	const MERCHANT_ACCOUNT					= 4;
	const ACCOUNTING						= 5;
	const BUG								= 6;
	const FEATURE							= 7;
	const QUESTION_RESEARCH					= 13;

	const TEST					= 15;

	const INFO_REQUEST_SENT_ON				= 50;
	const INFO_REQUEST_RECEIVED_ON			= 51;
	const PACKET_SENT_TO_MERCHANT_ON		= 52;
	const PACKET_RECEIVED_FROM_MERCHANT_ON	= 53;
	const PACKET_SENT_TO_ECHO_ON			= 54;
	const PIN_RECEIVED_ON					= 55;
	const CLIENT_NOTIFIED_ON				= 56;
	const DISCOVER_PIN_REQUESTED			= 57;
	const APPLICATION_COMPLETED_ON			= 58;
	const SUBMIT_SETTLEMENT_INFORMATION_RW	= 59;
	const IT								= 60;

	const SUPPORT							= 61;
	const SYSTEM_BUG						= 70;
	const TERMINATION 						= 83;
	const DESIGN							= 93;
	const TO_DO								= 96;
	const PROJECT							= 97;

	const NPS								= 119;

	const ROOT_CAUSE_ANALYSIS				= 120;
	const AUTOMATION						= 121;
	const TEST_CASES						= 122;

	const OUTAGE							= 123;

	const FOLLOW_UP							= 124;
	const PENDING_DEMOS						= 125;
	const LEAVE_REQUEST						= 126;
	const URGENT							= 129;
	const ASSIGNMENTS						= 127;
	const MIGRATION							= 128;
	const NEW_AUTOMATION					= 135;
	const MODIFY_AUTOMATION					= 136;
	const NEW_TEST_CASE						= 137;
	const MODIFY_TEST_CASE					= 138;

	const HELPDESK							= 146;
	const QUESTION							= 147;
	const INCIDENT							= 148;
	const PROBLEM							= 149;
	const TASK								= 150;
	const CAB_TICKET						= 151;
	const SERVICE_REQUEST				    = 167;
	const CHANGE_REQUEST					= 168;
	const SUSPICION							= 155;
	const MARKETING_CREATIVE_SERVICES		= 156;
	const EVENTS							= 157;
	const VIDEOS							= 158;
	const CREATIVE_DESIGN					= 159;
	const TASK_DATA_REQUEST					= 160;
	const RP_FEEDBACK						= 161;
	const SCHEMA_CHANGE					    = 162;
	const INTERNAL_DESIGN					= 163;
	const WEBSITE_DESIGN					= 164;
	const UX_DESIGN							= 165;

	const DEFECT							= 139;
	const SUB_TASK_LIMIT					= 80;

	const ENTRATA_TEST_CASE					= 166;
	const STORY								= 169;
	const COPY_ONLY							= 170;

	protected $m_arrobjTaskTypes;
	protected $m_arrobjTasks;
	protected $m_strCountryCode;

	protected $m_intIsDisabled;
	protected $m_intIndiaEmployeeId;
	protected $m_intUsEmployeeId;
	protected $m_intEmployeeId;

	protected $m_boolIsChecked;

	public static $c_arrintTaskTypesForReleaseFilter = [
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::TEST_CASES,
		CTaskType::AUTOMATION,
		CTaskType::DESIGN
	];

	public static $c_arrintTaskTypesNotRequiredSdm = [
		CTaskType::DEFECT,
		CTaskType::TEST_CASES
	];

	public static $c_arrintTaskTypesForMultipleParentTaskAssociations = [
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::PROJECT
	];

	public static $c_arrintTaskTypesDefaultVisible = [
		CTaskType::PENDING_DEMOS,
		CTaskType::TO_DO,
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::TERMINATION,
		CTaskType::DESIGN
	];

	public static $c_arrintTaskTypesForDelayedTasks = array(
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::DESIGN
	);

	public static $c_arrintTaskTypesForDisplayingTracks = array(
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::DESIGN,
		CTaskType::PROJECT,
		CTaskType::QUESTION_RESEARCH
	);

	public static $c_arrintTaskTypesForTaskDetails	= array(
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::DESIGN,
		CTaskType::AUTOMATION,
		CTaskType::TEST_CASES,
		CTaskType::QUESTION_RESEARCH,
		CTaskType::PROJECT,
		CTaskType::MIGRATION,
		CTaskType::ROOT_CAUSE_ANALYSIS,
		CTaskType::HELPDESK,
		CTaskType::MARKETING_CREATIVE_SERVICES,
		CTaskType::SUSPICION,
		CTaskType::TASK_DATA_REQUEST,
		CTaskType::SCHEMA_CHANGE
	);

	public static $c_arrintTaskTypesForStoryPoints = array(
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::DESIGN,
		CTaskType::AUTOMATION,
		CTaskType::TEST_CASES,
		CTaskType::QUESTION_RESEARCH,
		CTaskType::TASK_DATA_REQUEST,
		CTaskType::HELPDESK
	);

	public static $c_arrmixSubTaskTypesForHelpDeskTask = array(
		CTaskType::QUESTION 			=> array( 'id' => CTaskType::QUESTION, 'name' => 'Question' ),
		CTaskType::INCIDENT 			=> array( 'id' => CTaskType::INCIDENT, 'name' => 'Incident' ),
		CTaskType::PROBLEM 				=> array( 'id' => CTaskType::PROBLEM, 'name' => 'Problem' ),
		CTaskType::TASK 				=> array( 'id' => CTaskType::TASK, 'name' => 'Task' ),
		CTaskType::CAB_TICKET			=> array( 'id' => CTaskType::CAB_TICKET, 'name' => 'Cab Ticket' )
	);

	public static $c_arrmixIssueTypesForItHelpDeskTask = array(
		CTaskType::INCIDENT 			=> array( 'id' => CTaskType::INCIDENT, 'name' => 'Incident', 'description' => 'An incident is something that is broken and that needs to be resolved.' ),
		CTaskType::TASK 				=> array( 'id' => CTaskType::TASK, 'name' => 'Task', 'description' => 'General IT request or uncategorized query/issue.' ),
		CTaskType::SERVICE_REQUEST		=> array( 'id' => CTaskType::SERVICE_REQUEST, 'name' => 'Service Request', 'description' => 'An employee request for information or advice, or for a standard change (a pre-approved change that is low risk, relatively common and follows a procedure) or for access to an IT service.' ),
		CTaskType::CHANGE_REQUEST		=> array( 'id' => CTaskType::CHANGE_REQUEST, 'name' => 'Change Request', 'description' => 'A formal proposal for an alteration to some product or system. In project management, a change request often arises when the employee wants an addition or alteration to the agreed-upon deliverables for a project.' )
	);

	public static $c_arrmixSubTaskTypeSuspicion = array(
		CTaskType::SUSPICION => array( 'id' => CTaskType::SUSPICION, 'name' => 'Suspicion' )
	);

	public static $c_arrintTaskTypeNotIncludedInPdmDashboard = array(
		CTaskType::TO_DO,
		CTaskType::DEFECT,
		CTaskType::SUPPORT
	);

	public static $c_arrmixTaskTypesForTaskSystem = array(
		CTaskType::ACCOUNTING 			=> array( 'id' => CTaskType::ACCOUNTING, 'name' => 'Accounting' ),
		CTaskType::BUG 					=> array( 'id' => CTaskType::BUG, 'name' => 'Bug' ),
		CTaskType::DESIGN 				=> array( 'id' => CTaskType::DESIGN, 'name' => 'Design' ),
		CTaskType::FEATURE 				=> array( 'id' => CTaskType::FEATURE, 'name' => 'Feature' ),
		CTaskType::HELPDESK				=> array( 'id' => CTaskType::HELPDESK, 'name' => 'HelpDesk' ),
		CTaskType::MARKETING_CREATIVE_SERVICES => array( 'id' => CTaskType::MARKETING_CREATIVE_SERVICES, 'name' => 'Marketing & Creative Services' ),
		CTaskType::MERCHANT_ACCOUNT 	=> array( 'id' => CTaskType::MERCHANT_ACCOUNT, 'name' => 'Merchant Account' ),
		CTaskType::MIGRATION 			=> array( 'id' => CTaskType::MIGRATION, 'name' => 'Migration' ),
		CTaskType::OUTAGE 				=> array( 'id' => CTaskType::OUTAGE, 'name' => 'Outage' ),
		CTaskType::PROJECT 				=> array( 'id' => CTaskType::PROJECT, 'name' => 'Project' ),
		CTaskType::QUESTION_RESEARCH 	=> array( 'id' => CTaskType::QUESTION_RESEARCH, 'name' => 'Question/Research' ),
		CTaskType::RP_FEEDBACK			=> array( 'id' => CTaskType::RP_FEEDBACK, 'name' => 'RP Feedback' ),
		CTaskType::SCHEMA_CHANGE		=> array( 'id' => CTaskType::SCHEMA_CHANGE, 'name' => 'Schema Change' ),
		CTaskType::SUPPORT				=> array( 'id' => CTaskType::SUPPORT, 'name' => 'Support' )
	);

	public static $c_arrintTaskTypesForSprintSummaryReport = [
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::AUTOMATION,
		CTaskType::TEST_CASES
	];

	public static $c_arrintTaskTypesForProducts = [
		CTaskType::OUTAGE,
		CTaskType::SUPPORT,
		CTaskType::RP_FEEDBACK,
		CTaskType::DESIGN,
		CTaskType::FEATURE,
		CTaskType::BUG,
		CTaskType::SYSTEM_BUG,
		CTaskType::PROJECT,
		CTaskType::MIGRATION,
		CTaskType::QUESTION_RESEARCH,
		CTaskType::TASK_DATA_REQUEST,
		CTaskType::SCHEMA_CHANGE
	];

	public static $c_arrintTaskTypesNotForSdms = [
		CTaskType::IT,
		CTaskType::TO_DO,
		CTaskType::SUPPORT,
		CTaskType::RP_FEEDBACK,
		CTaskType::NPS
	];

	public static $c_arrintTaskTypesNotRequiredForCRS = [
		CTaskType::MIGRATION,
		CTaskType::QUESTION_RESEARCH,
		CTaskType::MARKETING_CREATIVE_SERVICES,
		CTaskType::TASK_DATA_REQUEST,
		CTaskType::SCHEMA_CHANGE

	];

	public static $c_arrintTaskTypesRequiredForProducts = [
		CTaskType::SUPPORT,
		CTaskType::RP_FEEDBACK,
		CTaskType::DESIGN,
		CTaskType::OUTAGE,
		CTaskType::FEATURE,
		CTaskType::BUG,
		CTaskType::SYSTEM_BUG,
		CTaskType::PROJECT,
		CTaskType::TEST_CASES,
		CTaskType::AUTOMATION,
		CTaskType::QUESTION_RESEARCH,
		CTaskType::IT,
		cTaskType::MIGRATION,
		CTaskType::TASK_DATA_REQUEST,
		CTaskType::SCHEMA_CHANGE,
	];

	public static $c_arrintTaskTypesNotRequiredForProduct = [
		CTaskType::ACCOUNTING,
		CTaskType::MERCHANT_ACCOUNT,
		CTaskType::IT,
		CTaskType::OUTAGE,
		CTaskType::HELPDESK,
		CTaskType::MARKETING_CREATIVE_SERVICES
	];

	public static $c_arrintReleaseNoteTaskTypes = [
		CTaskType::BUG,
		CTaskType::FEATURE
	];

	public static $c_arrintMarketingSubTaskTypes = [
		CTaskType::CREATIVE_DESIGN => array( 'id' => CTaskType::CREATIVE_DESIGN, 'name' => 'Graphic Design' ),
		CTaskType::EVENTS => array( 'id' => CTaskType::EVENTS, 'name' => 'Events' ),
		CTaskType::VIDEOS => array( 'id' => CTaskType::VIDEOS, 'name' => 'Videos' ),
		CTaskType::COPY_ONLY => array( 'id' => CTaskType::COPY_ONLY, 'name' => 'Copy Only' )
	];

	public static $c_arrintCreativeTaskTypesForUs = [
		CTaskType::MARKETING_CREATIVE_SERVICES,
		CTaskType::SUSPICION
	];

	public static $c_arrintTaskTypesForViewTaskInfo = [
		CTaskType::ACCOUNTING,
		CTaskType::BUG,
		CTaskType::DESIGN,
		CTaskType::FEATURE,
		CTaskType::IT,
		CTaskType::MERCHANT_ACCOUNT,
		CTaskType::MIGRATION,
		CTaskType::OUTAGE,
		CTaskType::PROJECT,
		CTaskType::HELPDESK,
		CTaskType::QUESTION_RESEARCH,
		CTaskType::MARKETING_CREATIVE_SERVICES,
		CTaskType::TASK_DATA_REQUEST,
		CTaskType::SCHEMA_CHANGE
	];

	public static $c_arrintStakeHolderTaskTypes = [
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::ACCOUNTING,
		CTaskType::QUESTION_RESEARCH,
		CTaskType::IT,
		CTaskType::SUPPORT,
		CTaskType::DESIGN,
		CTaskType::PROJECT,
		CTaskType::OUTAGE,
		CTaskType::MIGRATION,
		CTaskType::MERCHANT_ACCOUNT,
		CTaskType::MARKETING_CREATIVE_SERVICES
	];

	public static $c_arrintProductBacklogNotRequiredTaskTypes = [
		CTaskType::SUPPORT,
		CTaskType::PROJECT,
		CTaskType::TO_DO,
		CTaskType::DEFECT,
		CTaskType::AUTOMATION,
		CTaskType::TEST_CASES
	];

	public static $c_arrintWebDesignSubTaskTypes = [
		CTaskType::INTERNAL_DESIGN => array( 'id' => CTaskType::INTERNAL_DESIGN, 'name' => 'Internal Design' ),
		CTaskType::UX_DESIGN => array( 'id' => CTaskType::UX_DESIGN, 'name' => 'UX Design' ),
		CTaskType::WEBSITE_DESIGN => array( 'id' => CTaskType::WEBSITE_DESIGN, 'name' => 'Website Design' )
	];

	public static $c_arrintPIIContainsTaskTypes = [
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::QUESTION_RESEARCH,
		CTaskType::PROJECT,
		CTaskType::SUPPORT
	];

	public function __construct() {
		parent::__construct();

		$this->m_boolIsChecked = false;
		$this->m_intIsDisabled = 0;

		return;
	}

	/**
	 * Add Functions
	 */

	public function addTask( $objTask ) {
		$this->m_arrobjTasks[$objTask->getId()] = $objTask;
	}

	/**
	 * Get Functions
	 */

	public function getTaskTypes() {
		return $this->m_arrobjTaskTypes;
	}

	public function getIsChecked() {
		return $this->m_boolIsChecked;
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function getTasks() {
		return $this->m_arrobjTasks;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getIndiaEmployeeId() {
		return $this->m_intIndiaEmployeeId;
	}

	public function getUsEmployeeId() {
		return $this->m_intUsEmployeeId;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['is_disabled'] ) ) $this->setIsDisabled( $arrmixValues['is_disabled'] );
		if( true == isset( $arrmixValues['employee_id'] ) ) $this->setEmployeeId( $arrmixValues['employee_id'] );
		if( true == isset( $arrmixValues['india_employee_id'] ) ) $this->setIndiaEmployeeId( $arrmixValues['india_employee_id'] );
		if( true == isset( $arrmixValues['us_employee_id'] ) ) $this->setUsEmployeeId( $arrmixValues['us_employee_id'] );
		if( true == isset( $arrmixValues['country_code'] ) ) $this->setCountryCode( $arrmixValues['country_code'] );

		return;
	}

	public function setIsChecked( $boolIsChecked ) {
		$this->m_boolIsChecked = $boolIsChecked;
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->m_intIsDisabled = $intIsDisabled;
	}

	public function setTasks( $arrobjTasks ) {
		$this->m_arrobjTasks = $arrobjTasks;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setIndiaEmployeeId( $intIndiaEmployeeId ) {
		$this->m_intIndiaEmployeeId = $intIndiaEmployeeId;
	}

	public function setUsEmployeeId( $intUsEmployeeId ) {
		$this->m_intUsEmployeeId = $intUsEmployeeId;
	}

	public function setCountryCode( $strCountryCode ) {
	$this->m_strCountryCode = $strCountryCode;
	}

	/**
	 * Add Functions
	 */

	public function addTaskType( $objTaskType ) {

		if( false == valObj( $objTaskType, 'CTaskType' ) ) return false;

		if( false == is_null( $objTaskType->getId() ) ) {
			$this->m_arrobjTaskTypes[$objTaskType->getId()] = $objTaskType;
		} else {
			$this->m_arrobjTaskTypes[] = $objTaskType;
		}

		return true;
	}

	/**
	 * Create Functions
	 */

	public function createSubTaskType() {

		$objSubTaskType = new CTaskType();
		$objSubTaskType->setParentTaskTypeId( $this->m_intId );
		$objSubTaskType->setDefaultTaskPriorityId( $this->m_intDefaultTaskPriorityId );

		return $objSubTaskType;
	}

	public function createTask() {

		$objTask = new CTask();
		$objTask->setDefaults();
		$objTask->setUserId( $this->m_intUserId );
		$objTask->setTaskPriorityId( $this->m_intDefaultTaskPriorityId );
		$objTask->setDepartmentId( $this->m_intDepartmentId );
		$objTask->setTitle( $this->m_strName );

		if( true == $this->m_strDescription ) {
			$objTask->setDescription( $this->m_strDescription );
		} else {
			$objTask->setDescription( $this->m_strName );
		}

		$objTask->setTaskTypeId( $this->m_intId );
		$objTask->setIsClientTask( $this->m_intIsClientTask );

		// set the due date on the task based on the days allocated for the project.
		if( true == is_numeric( $this->m_intDaysAllotted ) ) {
			$intIncrement = ( $this->m_intDaysAllotted * 60 * 60 * 24 );
			$objTask->setDueDate( date( 'm/d/Y', ( time() + $intIncrement ) ) );
		}

		return $objTask;
	}

	public function createRecurringTask() {

		$objRecurringTask = new CRecurringTask();
		$objRecurringTask->setUserId( $this->m_intUserId );
		$objRecurringTask->setTaskPriorityId( $this->m_intDefaultTaskPriorityId );
		$objRecurringTask->setDepartmentId( $this->m_intDepartmentId );
		$objRecurringTask->setTitle( $this->m_strName );
		$objRecurringTask->setDescription( $this->m_strDescription );
		$objRecurringTask->setTaskTypeId( $this->m_intId );

		return $objRecurringTask;
	}

	/**
	 * Validate Functions
	 */

	public function valUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Default assignment is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required' ) );
		}

		if( true == $boolIsValid && true == isset( $objDatabase ) ) {
				$intPreexistingTestCount = CTaskTypes::fetchCompetingTaskTypeCountByNameById( $this->m_strName, $this->m_intId, $objDatabase );
				if( 0 < $intPreexistingTestCount ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Task type name is already in use.' ) );
				}
		}

		return $boolIsValid;
	}

	public function valSupportTaskTypeName( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		$intConflictingSupportContactTypeCount = CTaskTypes::fetchConflictingSupportTaskTypeCount( $this->getName(), $this->getId(), $objAdminDatabase );

		if( 0 < $intConflictingSupportContactTypeCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Support contact type already in use.' ) );
		}

		return $boolIsValid;
	}

	public function valDaysAllotted() {
		$boolIsValid = true;

		if( false == is_null( $this->getDaysAllotted() ) ) {
			if( ( false == is_numeric( $this->getDaysAllotted() ) ) || ( 0 > $this->getDaysAllotted() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_alloted', 'Invalid days alloted' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDelete( $objDatabase ) {
		$boolIsValid = true;

		if( true == $boolIsValid && true == isset( $objDatabase ) ) {
				$intTaskCount = \Psi\Eos\Admin\CTasks::createService()->fetchTaskCountByTaskTypeId( $this->m_intParentTaskTypeId, $objDatabase );
				if( true == isset( $intTaskCount ) && 0 < $intTaskCount ) {
					$boolIsValid = false;
				}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				// $boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valDaysAllotted();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDelete( $objDatabase );
				break;

			case 'support_task_type_insert':
			case 'support_task_type_update':
				$boolIsValid &= $this->valSupportTaskTypeName( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolIsEnabled = false, $boolIsValid = false ) {

		if( false == $boolIsValid ) {
			if( false == $boolIsEnabled ) {
				$this->setDisabledBy( NULL );
				$this->setDisabledOn( NULL );
			} else {
				$this->setDisabledBy( $intUserId );
				$this->setDisabledOn( date( 'm/d/Y' ) );
			}

			$this->setUpdatedBy( $intUserId );
			$this->setUpdatedOn( date( 'm/d/Y' ) );
			$this->update( $intUserId, $objDatabase );
		} else {
			parent::delete( $intUserId, $objDatabase );
		}

		return true;
	}

	public function enableOrDisable( $intUserId, $objAdminDatabase ) {

		if( true == is_null( $this->getDisabledBy() ) && true == is_null( $this->getDisabledOn() ) ) {

			$this->setDisabledBy( $intUserId );
			$this->setDisabledOn( date( 'm/d/Y' ) );
		} else {

			$this->setDisabledBy( NULL );
			$this->setDisabledOn( NULL );
		}

		return $this->update( $intUserId, $objAdminDatabase );
	}

	/**
	 * Fetch Functions
	 */
	/**
	 * If you are making any changes in populateSmartyConstants function then
	 * please make sure the same changes would be applied to populateTemplateConstants function also.
	 */

	public static function populateSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'TASK_TYPE_WEBSITE', 				self::WEBSITE );
		$objSmarty->assign( 'TASK_TYPE_PROPERTY', 				self::PROPERTY );
		$objSmarty->assign( 'TASK_TYPE_ACCOUNTING', 			self::ACCOUNTING );
		$objSmarty->assign( 'TASK_TYPE_BUG', 					self::BUG );
		$objSmarty->assign( 'TASK_TYPE_FEATURE', 				self::FEATURE );
		$objSmarty->assign( 'TASK_TYPE_QUESTION_RESEARCH',		self::QUESTION_RESEARCH );
		$objSmarty->assign( 'TASK_TYPE_DESIGN', 				self::DESIGN );
		$objSmarty->assign( 'TASK_TYPE_SYSTEM_BUG', 			self::SYSTEM_BUG );
		$objSmarty->assign( 'TASK_TYPE_IT',		 				self::IT );
		$objSmarty->assign( 'TASK_TYPE_TO_DO', 					self::TO_DO );
		$objSmarty->assign( 'TASK_TYPE_PROJECT', 				self::PROJECT );
		$objSmarty->assign( 'TASK_TYPE_TEST_CASES',				self::TEST_CASES );
		$objSmarty->assign( 'TASK_TYPE_AUTOMATION',				self::AUTOMATION );
		$objSmarty->assign( 'TASK_TYPE_OUTAGE',					self::OUTAGE );
		$objSmarty->assign( 'TASK_TYPE_MIGRATION',				self::MIGRATION );
		$objSmarty->assign( 'TASK_TYPE_NPS', 					self::NPS );
		$objSmarty->assign( 'TASK_TYPE_SUPPORT',				self::SUPPORT );
		$objSmarty->assign( 'TASK_TYPE_RP_FEEDBACK',			self::RP_FEEDBACK );
		$objSmarty->assign( 'TASK_TYPE_TERMINATION',			self::TERMINATION );
		$objSmarty->assign( 'TASK_TYPE_RCA',					self::ROOT_CAUSE_ANALYSIS );
		$objSmarty->assign( 'TASK_TYPE_MERCHANT_ACCOUNT', 		self::MERCHANT_ACCOUNT );
		$objSmarty->assign( 'TASK_TYPE_TRAINING', 				self::TRAINING );
		$objSmarty->assign( 'TASK_TYPE_FOLLOW_UP',				self::FOLLOW_UP );
		$objSmarty->assign( 'TASK_TYPE_PENDING_DEMO',			self::PENDING_DEMOS );
		$objSmarty->assign( 'TASK_TYPE_HELPDESK', 				self::HELPDESK );
		$objSmarty->assign( 'TASK_TYPE_MARKETING',				self::MARKETING_CREATIVE_SERVICES );
	}

	public static $c_arrintPopulateTemplateConstants	= array(
		'TASK_TYPE_WEBSITE'				=> self::WEBSITE,
		'TASK_TYPE_PROPERTY'			=> self::PROPERTY,
		'TASK_TYPE_ACCOUNTING'			=> self::ACCOUNTING,
		'TASK_TYPE_BUG'					=> self::BUG,
		'TASK_TYPE_FEATURE'				=> self::FEATURE,
		'TASK_TYPE_QUESTION_RESEARCH'	=> self::QUESTION_RESEARCH,
		'TASK_TYPE_DESIGN'				=> self::DESIGN,
		'TASK_TYPE_SYSTEM_BUG'			=> self::SYSTEM_BUG,
		'TASK_TYPE_IT'					=> self::IT,
		'TASK_TYPE_TO_DO'				=> self::TO_DO,
		'TASK_TYPE_PROJECT'				=> self::PROJECT,
		'TASK_TYPE_TEST_CASES'			=> self::TEST_CASES,
		'TASK_TYPE_AUTOMATION'			=> self::AUTOMATION,
		'TASK_TYPE_OUTAGE'				=> self::OUTAGE,
		'TASK_TYPE_MIGRATION'			=> self::MIGRATION,
		'TASK_TYPE_NPS'					=> self::NPS,
		'TASK_TYPE_SUPPORT'				=> self::SUPPORT,
		'TASK_TYPE_RP_FEEDBACK'			=> self::RP_FEEDBACK,
		'TASK_TYPE_TERMINATION'			=> self::TERMINATION,
		'TASK_TYPE_RCA'					=> self::ROOT_CAUSE_ANALYSIS,
		'TASK_TYPE_MERCHANT_ACCOUNT'	=> self::MERCHANT_ACCOUNT,
		'TASK_TYPE_TRAINING'			=> self::TRAINING,
		'TASK_TYPE_FOLLOW_UP'			=> self::FOLLOW_UP,
		'TASK_TYPE_PENDING_DEMO'		=> self::PENDING_DEMOS,
		'TASK_TYPE_HELPDESK'			=> self::HELPDESK,
		'TASK_TYPE_SUSPICION'			=> self::SUSPICION,
		'TASK_TYPE_MARKETING'			=> self::MARKETING_CREATIVE_SERVICES,
		'TASK_TYPE_EVENTS'				=> self::EVENTS,
		'TASK_TYPE_CREATIVE_DESIGN'		=> self::CREATIVE_DESIGN,
		'TASK_TYPE_VIDEOS'				=> self::VIDEOS,
		'TASK_TYPE_DATA_REQUEST'        => self::TASK_DATA_REQUEST,
		'TASK_TYPE_SCHEMA_CHANGE'       => self::SCHEMA_CHANGE,
		'TASK_TYPE_COPY_ONLY'		    => self::COPY_ONLY
	);


	public static $c_arrintTaskTypeName = array(
		self::WEBSITE 			=> 'Website',
		self::PROPERTY 			=> 'Property',
		self::ACCOUNTING		=> 'Accounting',
		self::BUG				=> 'Bug',
		self::FEATURE 			=> 'Feature',
		self::QUESTION_RESEARCH => 'Question Research',
		self::SYSTEM_BUG 		=> 'System Bug',
		self::SUPPORT 			=> 'Support',
		self::PROJECT 			=> 'Project',
		self::TO_DO 			=> 'To-Do',
		self::DESIGN 			=> 'Design',
		self::IT 				=> 'IT',
		self::MERCHANT_ACCOUNT 	=> 'Merchant Account',
		self::OUTAGE 			=> 'Outage',
		self::TRAINING 			=> 'Training',
		self::NPS 				=> 'NPS',
		self::AUTOMATION		=> 'Automation',
		self::TEST_CASES 		=> 'Test cases',
		self::RP_FEEDBACK		=> 'Rp feedback'
	);

	public static $c_arrintSupportTicketTaskTypeIds	= array(
		CTaskType::SUPPORT,
		CTaskType::RP_FEEDBACK,
		CTaskType::BUG,
		CTaskType::FEATURE,
		CTaskType::SYSTEM_BUG,
		CTaskType::PROJECT,
		CTaskType::ACCOUNTING,
		CTaskType::DESIGN,
		CTaskType::MERCHANT_ACCOUNT,
		CTaskType::PROPERTY,
		CTaskType::QUESTION_RESEARCH,
		CTaskType::TRAINING,
		CTaskType::WEBSITE
	);
}
?>