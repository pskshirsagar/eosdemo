<?php

class CEmployeeAssociation extends CBaseEmployeeAssociation {

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPreferredName;

	/**
	 * Get Functions
	 */

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_first'] ) ) {
			$this->setNameFirst( $arrmixValues['name_first'] );
		}

		if( true == isset( $arrmixValues['name_last'] ) ) {
			$this->setNameLast( $arrmixValues['name_last'] );
		}

		if( true == isset( $arrmixValues['preferred_name'] ) ) {
			$this->setPreferredName( $arrmixValues['preferred_name'] );
		}

		return;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setPreferredName( $strPreferredName ) {
		$this->m_strPreferredName = $strPreferredName;
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>