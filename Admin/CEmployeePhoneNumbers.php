<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePhoneNumbers
 * Do not add any new functions to this class.
 */

class CEmployeePhoneNumbers extends CBaseEmployeePhoneNumbers {

	public static function fetchEmployeePhoneNumberByEmployeeIdByPhoneNumberTypeId( $intEmployeeId, $intPhoneNumberTypeId, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM employee_phone_numbers WHERE employee_id=' . ( int ) $intEmployeeId . ' AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . ' LIMIT 1';
		return self::fetchEmployeePhoneNumber( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeePhoneNumbersByEmployeeIdsByPhoneNumberTypeId( $arrintEmployeeIds,  $intPhoneNumberTypeId, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM employee_phone_numbers WHERE employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId;
		return self::fetchEmployeePhoneNumbers( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeePhoneNumbersByEmployeeIdByPhoneNumberTypeIds( $intEmployeeId, $arrintPhoneNumberTypeIds, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM employee_phone_numbers WHERE employee_id = ' . ( int ) $intEmployeeId . ' AND phone_number_type_id IN ( ' . implode( ',', $arrintPhoneNumberTypeIds ) . ' )';
		return self::fetchEmployeePhoneNumbers( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeePhoneNumbersByEmployeeId( $intEmployeeId, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM employee_phone_numbers WHERE employee_id = ' . ( int ) $intEmployeeId . ' ORDER BY phone_number_type_id';
		return self::fetchEmployeePhoneNumbers( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeDetailByPhoneNumber( $strPhoneNumber, $objAdminDatabase ) {
		$strSql  = 'SELECT
						DISTINCT epn.employee_id,
                        u.id as user_id
					FROM
						employee_phone_numbers epn
                        JOIN users u ON (u.employee_id = epn.employee_id)
					WHERE
						NULLIF(regexp_replace(phone_number, \'[^0-9]+\',\'\',\'g\'), \'\')= \'' . ( string ) addslashes( $strPhoneNumber ) . '\'';

		return fetchData( $strSql, $objAdminDatabase );
	}

}
?>