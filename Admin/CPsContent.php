<?php

class CPsContent extends CBasePsContent {

	const INJECTION_FLAWS								= 1;
	const BROKEN_AUTHENTICATION							= 2;
	const CROSS_SITE_SCRIPTING							= 3;
	const INSECURE_DIRECT_OBJECT_REFERENCE				= 4;
	const SECURITY_MISCONFIGURATION						= 5;
	const SENSITIVE_DATA_EXPOSURE						= 6;
	const MISSING_FUNCTION_LEVEL_ACCESS_CONTROL			= 7;
	const CROSS_SITE_REQUEST_FORGERY					= 8;
	const USING_COMPONENTS_WITH_KNOWN_VULNERABILITIES	= 9;
	const UNVALIDATED_REDIRECTS_AND_FORWARDS			= 10;

	public static $c_arrintSecurityChecks 	= array(
		self::INJECTION_FLAWS,
		self::BROKEN_AUTHENTICATION,
		self::CROSS_SITE_SCRIPTING,
		self::INSECURE_DIRECT_OBJECT_REFERENCE,
		self::SECURITY_MISCONFIGURATION,
		self::SENSITIVE_DATA_EXPOSURE,
		self::MISSING_FUNCTION_LEVEL_ACCESS_CONTROL,
		self::CROSS_SITE_REQUEST_FORGERY,
		self::USING_COMPONENTS_WITH_KNOWN_VULNERABILITIES,
		self::UNVALIDATED_REDIRECTS_AND_FORWARDS
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>