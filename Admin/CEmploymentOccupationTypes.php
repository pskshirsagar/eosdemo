<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmploymentOccupationTypes
 * Do not add any new functions to this class.
 */

class CEmploymentOccupationTypes extends CBaseEmploymentOccupationTypes {

	public static function fetchEmploymentOccupationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmploymentOccupationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEmploymentOccupationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmploymentOccupationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedEmploymentOccupationTypes( $objDatabase ) {
		return self::fetchEmploymentOccupationTypes( 'SELECT * FROM employment_occupation_types WHERE is_published = 1 order by name', $objDatabase );
	}
}
?>