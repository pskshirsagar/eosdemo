<?php

class CCommissionBucket extends CBaseCommissionBucket {

	const DEFAULT_CONTRACT_PRODUCT_ID		= 3;

	const NEW_LOGO 							= 1;
	const NEW_PROPERTY_EXISTING_CLIENT		= 2;
	const NEW_PRODUCT_EXISTING_CLIENT		= 3;
	const RENEWAL 							= 4;
	const TRANSFER_PROPERTY					= 13;
	const PRODUCT_COMMISSION				= 14;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>