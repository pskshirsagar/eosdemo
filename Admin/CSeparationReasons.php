<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSeparationReasons
 * Do not add any new functions to this class.
 */

class CSeparationReasons extends CBaseSeparationReasons {

	public static function fetchAllSeparationReasons( $objDatabase, $strCountryCode = NULL ) {

		$strSql = ' SELECT
						*
					FROM
						separation_reasons
					WHERE
						is_visible_to IS NULL OR is_visible_to = \'' . $strCountryCode . '\'
					ORDER BY id ';

		return self::fetchSeparationReasons( $strSql, $objDatabase );
	}

	public static function fetchSeprationReasonNameByIds( $arrintIds, $objDatabase ) {

		$strSql = ' SELECT
						id,
						name
					FROM
						separation_reasons
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}
}
?>