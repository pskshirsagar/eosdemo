<?php

class CQuestionsFilter extends CEosFilter {

	protected $m_arrintQuestionIds;

	protected $m_intQuestionType;
	protected $m_intQuestionGroup;
	protected $m_intQuestionLevel;

	protected $m_arrstrSearchCriteria;

	protected $m_strGroupBy;
	protected $m_strQuestionName;
	protected $m_strOrderByField;
	protected $m_strOrderByType;
	protected $m_strRequiredFields;

	protected $m_boolIsFromExtendedSearch;
	protected $m_boolIsFromListingPage;
	protected $m_boolIsDeleted;
	protected $m_boolIsShowPrivate;
	protected $m_boolIsSecurityTestGreader;
	protected $m_boolIsShowPublished;

	public static $c_arrstrFieldsInfo = array(
		'question_ids' 				=> array( 'type' => 'arrint', 'length' => '', 'default' => 'null' ),
		'question_type' 			=> array( 'type' => 'int', 'length' => '', 'default' => 'null' ),
		'question_group'			=> array( 'type' => 'int', 'length' => '', 'default' => 'null' ),
		'question_level'			=> array( 'type' => 'int', 'length' => '', 'default' => 'null' ),
		'search_criteria'			=> array( 'type' => 'arrstr', 'length' => '-1', 'default' => 'null' ),
		'question_name'				=> array( 'type' => 'str', 'length' => '-1', 'default' => 'null' ),
		'order_by_field'			=> array( 'type' => 'str', 'length' => '-1', 'default' => 'null' ),
		'order_by_type'				=> array( 'type' => 'str', 'length' => '-1', 'default' => 'null' ),
		'required_fields'			=> array( 'type' => 'str', 'length' => '-1', 'default' => 'null' ),
		'group_by'					=> array( 'type' => 'str', 'length' => '-1', 'default' => 'null' ),
		'is_from_extended_search'	=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_deleted'				=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_from_listing_page'		=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_show_private'			=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_security_test_greader'	=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_show_published'			=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' )

	);

	public function __construct() {
		return;
	}

}

?>