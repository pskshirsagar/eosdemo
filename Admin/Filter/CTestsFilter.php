<?php

class CTestsFilter extends CEosFilter {

	protected $m_arrintDepartmentIds;
	protected $m_arrintDesignationIds;
	protected $m_arrintGroupIds;
	protected $m_arrintOfficeIds;
	protected $m_arrintTeamIds;
	protected $m_arrintEmployeeIds;
	protected $m_arrintJobPostingIds;
	protected $m_arrintTestIds;

	protected $m_intPublished;
	protected $m_intUnpublished;
	protected $m_intCreatedBy;
	protected $m_intDeletedBy;
	protected $m_intId;

	protected $m_arrstrRequiredFields;
	protected $m_strSortBy;
	protected $m_strSortType;
	protected $m_strTestName;
	protected $m_strTestNameQuickSearch;
	protected $m_boolIsFalseCondition;
	protected $m_boolIsResetFilter;
	protected $m_boolIsDeleted;

	public static $c_arrstrFieldsInfo = array(
		'id'						=> array( 'type' => 'int', 'length' => '', 'default' => 'NULL' ),
		'created_by' 				=> array( 'type' => 'int', 'length' => '', 'default' => 'NULL' ),
		'deleted_by' 				=> array( 'type' => 'int', 'length' => '', 'default' => 'NULL' ),
		'sort_by' 					=> array( 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ),
		'sort_type' 				=> array( 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ),
		'test_name' 				=> array( 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ),
		'test_name_quick_search'	=> array( 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ),
		'page_no'					=> array( 'type' => 'int', 'length' => '', 'default' => 'NULL' ),
		'page_size'					=> array( 'type' => 'int', 'length' => '', 'default' => 'NULL' ),
		'published' 				=> array( 'type' => 'int', 'length' => '', 'default' => 'NULL' ),
		'unpublished'				=> array( 'type' => 'int', 'length' => '', 'default' => 'NULL' ),
		'department_ids'			=> array( 'type' => 'arrint', 'length' => '', 'default' => 'NULL' ),
		'designation_ids' 			=> array( 'type' => 'arrint', 'length' => '', 'default' => 'NULL' ),
		'group_ids'					=> array( 'type' => 'arrint', 'length' => '', 'default' => 'NULL' ),
		'office_ids'				=> array( 'type' => 'arrint', 'length' => '', 'default' => 'NULL' ),
		'team_ids' 					=> array( 'type' => 'arrint', 'length' => '', 'default' => 'NULL' ),
		'employee_ids'				=> array( 'type' => 'arrint', 'length' => '', 'default' => 'NULL' ),
		'job_posting_ids'			=> array( 'type' => 'arrint', 'length' => '', 'default' => 'NULL' ),
		'test_ids'					=> array( 'type' => 'arrint', 'length' => '', 'default' => 'NULL' ),
		'is_reset_filter'			=> array( 'type' => 'bool', 'length' => '', 'default' => 'NULL' ),
		'required_fields' 			=> array( 'type' => 'arrstr', 'length' => '-1', 'default' => 'NULL' ),
		'is_deleted'				=> array( 'type' => 'bool', 'length' => '', 'default' => 'NULL' ),
		'is_false_condition'		=> array( 'type' => 'bool', 'length' => '', 'default' => 'NULL' ),
	);

}