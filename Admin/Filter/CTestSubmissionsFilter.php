<?php

class CTestSubmissionsFilter extends CEosFilter {

	protected $m_arrintTestSubmissionIds;
	protected $m_arrintGraderTestIds;
	protected $m_arrintEmployeeIds;
	protected $m_arrintGraderEmployeeIds;
	protected $m_arrintTestIds;

	protected $m_intEmployeeApplicationId;
	protected $m_intGraderEmployeeId;
	protected $m_intCreatorUserId;
	protected $m_intEmployeeId;
	protected $m_intTestId;
	protected $m_intTestType;
	protected $m_intYear;

	protected $m_fltFromPercentage;
	protected $m_fltToPercentage;

	protected $m_arrstrSearchCriteria;
	protected $m_arrstrRequiredFields;

	protected $m_strGradedOnStartDate;
	protected $m_strGradedOnEndDate;
	protected $m_strSortType;
	protected $m_strSortBy;
	protected $m_strTestName;

	protected $m_boolIsRequiredFieldScore;
	protected $m_boolIsIgnoredTestsCount;
	protected $m_boolIsFromListingPage;
	protected $m_boolIsGradePending;
	protected $m_boolIsGradedTests;
	protected $m_boolIsFalseCondition;
	protected $m_boolIsQuickSearch;
	protected $m_boolIsExtendedSearch;
	protected $m_boolIsAdmin;

	public static $c_arrstrFieldsInfo = array(
		'test_submission_ids'		=> array( 'type' => 'arrint', 'length' => '', 'default' => 'null' ),
		'grader_test_ids' 			=> array( 'type' => 'arrint', 'length' => '', 'default' => 'null' ),
		'test_ids'					=> array( 'type' => 'arrint', 'length' => '', 'default' => 'null' ),
		'employee_ids'				=> array( 'type' => 'arrint', 'length' => '', 'default' => 'null' ),
		'grader_employee_ids'		=> array( 'type' => 'arrint', 'length' => '', 'default' => 'null' ),
		'employee_application_id'	=> array( 'type' => 'int', 'length' => '', 'default' => 'null' ),
		'grader_employee_id'		=> array( 'type' => 'int', 'length' => '', 'default' => 'null' ),
		'creator_user_id'			=> array( 'type' => 'int', 'length' => '', 'default' => 'null' ),
		'employee_id'				=> array( 'type' => 'int', 'length' => '', 'default' => 'null' ),
		'test_id'					=> array( 'type' => 'int', 'length' => '', 'default' => 'null' ),
		'test_type'					=> array( 'type' => 'int', 'length' => '', 'default' => 'null' ),
		'year'						=> array( 'type' => 'int', 'length' => '', 'default' => 'null' ),
		'from_percentage'			=> array( 'type' => 'flt', 'length' => '', 'default' => 'null' ),
		'to_percentage'				=> array( 'type' => 'flt', 'length' => '', 'default' => 'null' ),
		'search_criteria'			=> array( 'type' => 'arrstr', 'length' => '-1', 'default' => 'null' ),
		'required_fields'			=> array( 'type' => 'arrstr', 'length' => '-1', 'default' => 'null' ),
		'sort_type' 				=> array( 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ),
		'sort_by' 					=> array( 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ),
		'test_name'					=> array( 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ),
		'graded_on_start_date' 		=> array( 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ),
		'graded_on_end_date' 		=> array( 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ),
		'is_required_field_score'	=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_ignored_tests_count'	=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_from_listing_page'		=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_false_condition'		=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_grade_pending'			=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_quick_search'			=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_graded_tests'			=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_admin'					=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' ),
		'is_extended_search'		=> array( 'type' => 'bool', 'length' => '', 'default' => 'null' )
	);
}

?>