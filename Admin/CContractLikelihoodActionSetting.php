<?php

class CContractLikelihoodActionSetting extends CBaseContractLikelihoodActionSetting {

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

			return $boolIsValid;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['action_type_name'] ) ) 	$this->setActionTypeName( $arrmixValues['action_type_name'] );
		if( true == isset( $arrmixValues['action_result_name'] ) ) 	$this->setActionResultName( $arrmixValues['action_result_name'] );
	}

	public function setActionTypeName( $strActionTypeName ) {
		$this->m_strActionTypeName = CStrings::strTrimDef( $strActionTypeName, 50, NULL, true );
	}

	public function sqlActionTypeName() {
		return ( true == isset( $this->m_strActionTypeName ) ) ? ( string ) $this->m_strActionTypeName : 'NULL';
	}

	public function setActionResultName( $strActionResultName ) {
		$this->m_strActionResultName = CStrings::strTrimDef( $strActionResultName, 50, NULL, true );
	}

	public function sqlActionResultName() {
		return ( true == isset( $this->m_strActionResultName ) ) ? ( string ) $this->m_strActionResultName : 'NULL';
	}

	/**
	 * Set Functions
	 *
	 */

	public function getActionResultName() {
		return $this->m_strActionResultName;
	}

	public function getActionTypeName() {
		return $this->m_strActionTypeName;
	}

}
?>