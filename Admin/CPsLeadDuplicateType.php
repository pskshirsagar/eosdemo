<?php

class CPsLeadDuplicateType extends CBasePsLeadDuplicateType {

	const COMPANY_NAME_AND_STATE_CODE			= 1;
	const COMPANY_NAME							= 2;
	const EMAIL_ADDRESS							= 3;
	const FIRST_NAME_LAST_NAME_AND_STATE_CODE	= 4;
	const PHONE_NUMBERS_MATCH					= 5;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>