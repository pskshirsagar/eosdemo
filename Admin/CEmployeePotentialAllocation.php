<?php

class CEmployeePotentialAllocation extends CBaseEmployeePotentialAllocation {

	const PAY_REVIEW_DAY	= '10/01/';
	const CTC_EFFECTIVE_DAY = '11/01/';
	const PAY_REVIEW_DAY_INDIA	= '10/01/';
	const CTC_EFFECTIVE_DAY_INDIA = '11/01/';
	const TERMINATION_CUTOFF_DATE_FOR_REVIEW_INDIA = '11/10/';
	const MIN_DAYS_FOR_REVIEW_INDIA = 120;

	const APPROVER_LEVEL1 	= 1;
	const APPROVER_LEVEL2 	= 2;
	const APPROVER_LEVEL3 	= 3;
	const APPROVER_LEVEL4 	= 4;
	const APPROVER_LEVEL5 	= 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeePayStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPotentialIncreaseEncryptionAssociationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
