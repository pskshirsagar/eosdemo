<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskReasonTypes
 * Do not add any new functions to this class.
 */

class CTaskReasonTypes extends CBaseTaskReasonTypes {

	public static function fetchTaskReasonTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTaskReasonType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchTaskReasonType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTaskReasonType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>