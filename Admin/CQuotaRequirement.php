<?php

class CQuotaRequirement extends CBaseQuotaRequirement {

	protected $m_strOverAchievedImplementationAmount;
	protected $m_strOverAchievedRecurringAmount;
	protected $m_strOverAchievedTransactionalAmount;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['over_achieved_implementation_amount'] ) ) $this->setOverAchievedImplementationAmount( $arrmixValues['over_achieved_implementation_amount'] );
		if( true == isset( $arrmixValues['over_achieved_recurring_amount'] ) ) $this->setOverAchievedRecurringAmount( $arrmixValues['over_achieved_recurring_amount'] );
		if( true == isset( $arrmixValues['over_achieved_transactional_amount'] ) ) $this->setOverAchievedTransactionalAmount( $arrmixValues['over_achieved_transactional_amount'] );
		return;
	}

	/**
	 * get Functions
	 */

	public function getOverAchievedImplementationAmount() {
		return $this->m_strOverAchievedImplementationAmount;
	}

	public function getOverAchievedRecurringAmount() {
		return $this->m_strOverAchievedRecurringAmount;
	}

	public function getOverAchievedTransactionalAmount() {
		return $this->m_strOverAchievedTransactionalAmount;
	}

	/**
	 * Set Functions
	 */

	public function setOverAchievedImplementationAmount( $intOverAchievedImplementationAmount ) {
		$this->m_strOverAchievedImplementationAmount = $intOverAchievedImplementationAmount;
	}

	public function setOverAchievedRecurringAmount( $intOverAchievedRecurringAmount ) {
		$this->m_strOverAchievedRecurringAmount = $intOverAchievedRecurringAmount;
	}

	public function setOverAchievedTransactionalAmount( $intOverAchievedTransactionAmount ) {
		$this->m_strOverAchievedTransactionalAmount = $intOverAchievedTransactionAmount;
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}
}
?>