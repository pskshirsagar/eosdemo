<?php

class CEmployeeOptionAssociation extends CBaseEmployeeOptionAssociation {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						  public.employee_option_associations
						SET ';

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlEmployeeId() ) != $this->getOriginalValueByFieldName( 'employee_id' ) ) {
			$arrstrOriginalValueChanges['employee_id'] = $this->sqlEmployeeId();
			$strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' encrypted_values = ' . $this->sqlEncryptedValues() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlEncryptedValues() ) != $this->getOriginalValueByFieldName( 'encrypted_values' ) ) {
			$arrstrOriginalValueChanges['encrypted_values'] = $this->sqlEncryptedValues();
			$strSql .= ' encrypted_values = ' . $this->sqlEncryptedValues() . ',';
			$boolUpdate = true;
		}

		$strSql = \Psi\CStringService::singleton()->substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
							id = ' . ( int ) $this->sqlId() . '
							AND viewer_employee_id = ' . ( int ) $this->sqlViewerEmployeeId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

}

?>