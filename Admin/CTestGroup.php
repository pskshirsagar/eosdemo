<?php

class CTestGroup extends CBaseTestGroup {

	protected $m_strTestName;

	/**
	 * Get Functions
	 *
	 */

	public function getTestName() {
		return $this->m_strTestName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setTestName( $strName ) {
		$this->m_strTestName = CStrings::strTrimDef( $strName, 50, NULL, true );
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrValues['test_name'] ) ) 	$this->setTestName( $arrValues['test_name'] );
	}

}
?>