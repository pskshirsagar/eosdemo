<?php

class CMassEmailSegment extends CBaseMassEmailSegment {

	public function valName( $objAdminDatabase ) {
		$boolIsValid			= true;
		$arrintIgnoreSegments	= [];

		if( true == valId( $this->getId() ) ) $arrintIgnoreSegments[] = $this->getId();

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Segment name is required. ' ) );
		} elseif( true == valObj( \Psi\Eos\Admin\CMassEmailSegments::createService()->fetchMassEmailSegmentByName( $this->getName(), $objAdminDatabase, $arrintIgnoreSegments ), 'CMassEmailSegment' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Segment with this name already exists. ' ) );
		}

		return $boolIsValid;
	}

	public function valConditions() {
		$boolIsValid = true;

		if( true == is_null( $this->getConditions() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Segment conditions are required. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objAdminDatabase );
				$boolIsValid &= $this->valConditions();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>