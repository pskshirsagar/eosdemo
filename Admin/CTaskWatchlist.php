<?php

class CTaskWatchlist extends CBaseTaskWatchlist {

	protected $m_strEmailAddress;

	// Get Methods

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	// Set Methods

	public function setEmailAddress( $strEmailAddress = NULL ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['email_address'] ) ) 	$this->setEmailAddress( $arrmixValues['email_address'] );
	}

	/**
	 * Validation Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			// default case
			$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>