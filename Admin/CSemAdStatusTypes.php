<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdStatusTypes
 * Do not add any new functions to this class.
 */

class CSemAdStatusTypes extends CBaseSemAdStatusTypes {

	public static function fetchSemAdStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSemAdStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSemAdStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSemAdStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllSemAdStatusTypes( $objAdminDatabase ) {
		return self::fetchSemAdStatusTypes( 'SELECT * FROM sem_ad_status_types', $objAdminDatabase );
	}
}
?>