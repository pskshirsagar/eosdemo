<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDbWatches
 * Do not add any new functions to this class.
 */

class CDbWatches extends CBaseDbWatches {

	public static function fetchAllDbWatches( $objDatabase ) {
		$strSql = 'SELECT * FROM db_watches ORDER BY db_watch_type_id, created_on';

		return self::fetchDbWatches( $strSql, $objDatabase );
	}

	public static function fetchEnabledDbWatches( $objDatabase ) {
		$strSql = 'SELECT * FROM db_watches WHERE is_disabled <> 1 ORDER BY db_watch_type_id, created_on';

		return self::fetchDbWatches( $strSql, $objDatabase );
	}

	public static function fetchEnabledDbWatchesByDbWatchTypeId( $intDbWatchTypeId, $objDatabase ) {
		$strSql = 'SELECT * FROM db_watches WHERE db_watch_type_id = ' . ( int ) $intDbWatchTypeId . ' AND is_disabled <> 1 ORDER BY db_watch_type_id, created_on';

		return self::fetchDbWatches( $strSql, $objDatabase );
	}

	public static function fetchDbWatchesByIds( $arrintDbWatchesIds, $objAdminDatabase ) {

		if( false == valArr( $arrintDbWatchesIds ) ) return NULL;

		$strSql = 'SELECT *
					FROM db_watches
					WHERE id IN ( ' . implode( ',', $arrintDbWatchesIds ) . ' )';

		return parent :: fetchDbWatches( $strSql, $objAdminDatabase );
	}

	public static function fetchDbWatchDetailsById( $intDbWatchId, $objAdminDatabase ) {

		$strSql = 'SELECT *
					FROM db_watches
					WHERE id=' . ( int ) $intDbWatchId;

		return self :: fetchDbWatch( $strSql, $objAdminDatabase );
	}

	public static function fetchPaginatedDbWatches( $intPageNo, $intPageSize, $objDatabase, $strOrderByField, $strOrderByType, $objDbWatchesFilter ) {

		$arrstrWhereParameters = array();

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;
		$strKeyword = $objDbWatchesFilter->getKeyword();
		$intDatabaseId = $objDbWatchesFilter->getDatabaseId();
		$strSubSql = '';

		( false == isset( $objDbWatchesFilter ) || true == empty( $strKeyword ) ) ?  false : array_push( $arrstrWhereParameters, '( name ILIKE \'%' . $objDbWatchesFilter->getKeyword() . '%\' OR description ILIKE \'%' . $objDbWatchesFilter->getKeyword() . '%\' )' );

		$strSql = 'SELECT * FROM db_watches WHERE 1 = 1';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhereParameters );
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $objDbWatchesFilter->getDbWatchTypeIds() ) ) {
			$strSql .= ' AND ';
			$strSql .= 'db_watch_type_id IN ( ' . implode( ', ', $objDbWatchesFilter->getDbWatchTypeIds() ) . ' ) ';
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $objDbWatchesFilter->getDbWatchPriorityIds() ) ) {
			$strSql .= ' AND ';
			$strSql .= 'db_watch_priority_id IN ( ' . implode( ', ', $objDbWatchesFilter->getDbWatchPriorityIds() ) . ' ) ';
		}

		if( 0 != $intDatabaseId ) {
			$strSql .= ' AND database_id = ' . $objDbWatchesFilter->getDatabaseId();
		}

		if( true == $objDbWatchesFilter->getIsManual() ) {
			$strSql .= ' AND is_manual = 1';
		}

		if( true == $objDbWatchesFilter->getIsPublished() ) {
			$strSql .= ' AND is_published = 1';
		}

		if( true == $objDbWatchesFilter->getIsCustomRoutine() ) {
			$strSql .= ' AND is_custom_routine = 1';
		}

		if( true == $objDbWatchesFilter->getIsDisabled() ) {
			$strSql .= ' AND is_disabled = 1';
		}

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$strSubSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		$strSql .= ' ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType . $strSubSql;
		return self::fetchDbWatches( $strSql, $objDatabase );
	}

	public static function fetchPaginatedDbWatchesCount( $objDatabase , $objDbWatchesFilter ) {

		$intDatabaseId = $objDbWatchesFilter->getDatabaseId();
		$arrstrWhereParameters = array();
		$strKeyword = $objDbWatchesFilter->getKeyword();
		( false == isset( $objDbWatchesFilter ) || true == empty( $strKeyword ) )	?  false : array_push( $arrstrWhereParameters, '(name ILIKE \'%' . $objDbWatchesFilter->getKeyword() . '%\' OR description ILIKE \'%' . $objDbWatchesFilter->getKeyword() . '%\' )' );

		$strSql = ' WHERE 1 = 1';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhereParameters );
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $objDbWatchesFilter->getDbWatchTypeIds() ) ) {
			$strSql .= ' AND ';
			$strSql .= 'db_watch_type_id IN ( ' . implode( ', ', $objDbWatchesFilter->getDbWatchTypeIds() ) . ' ) ';
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $objDbWatchesFilter->getDbWatchPriorityIds() ) ) {
			$strSql .= ' AND ';
			$strSql .= 'db_watch_priority_id IN ( ' . implode( ', ', $objDbWatchesFilter->getDbWatchPriorityIds() ) . ' ) ';
		}

		if( 0 != $intDatabaseId ) {
			$strSql .= ' AND database_id = ' . $objDbWatchesFilter->getDatabaseId();
		}

		if( 1 == $objDbWatchesFilter->getIsManual() ) {
			$strSql .= ' AND is_manual = 1';
		}

		if( 1 == $objDbWatchesFilter->getIsPublished() ) {
			$strSql .= ' AND is_published = 1';
		}

		if( 1 == $objDbWatchesFilter->getIsCustomRoutine() ) {
			$strSql .= ' AND is_custom_routine = 1';
		}

		if( 1 == $objDbWatchesFilter->getIsDisabled() ) {
			$strSql .= ' AND is_disabled = 1';
		}

		return self::fetchDbWatchCount( $strSql, $objDatabase );
	}

}
?>