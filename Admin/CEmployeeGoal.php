<?php

class CEmployeeGoal extends CBaseEmployeeGoal {

	protected $m_intDaysTillDueDate;
	protected $m_strEmployeeName;
	protected $m_strEmployeeGoalTypeName;
	protected $m_strCoachEmployeeName;
	protected $m_strCallAgentEmployeeNameFull;
	protected $m_strNameFull;
	protected $m_boolIsDueDatePassed;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		if( false == valId( $this->getEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select employee.' ) );
		}

		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentEmployeeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intCallAgentEmployeeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select call agent.' ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeGoalTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeInteractionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required.' ) );
		}
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFeedback() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDueDate( $boolIsCoachEmployeeGoal = false ) {
		$boolIsValid = true;

		if( false == valStr( $this->m_strDueDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select due date.' ) );
		}

		if( true == $boolIsCoachEmployeeGoal && strtotime( date( 'm/d/Y' ) ) > strtotime( $this->getDueDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Due date can not be passed.' ) );
		}

		return $boolIsValid;
	}

	public function valStartGoalValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndGoalValue() {
		$boolIsValid = true;

		if( false == valId( $this->m_intEndGoalValue ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please insert QA goal.' ) );
		}

		return $boolIsValid;
	}

	public function valAssignedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssignedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCallAgentEmployeeId();
				$boolIsValid &= $this->valDueDate();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDueDate();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_coach_employee_goal':
				$boolIsValid &= $this->valCallAgentEmployeeId();
				$boolIsValid &= $this->valDueDate( true );
				break;

			case 'validate_switch_coach':
				$boolIsValid &= $this->valEmployeeId();
				break;

			case 'validate_employee_goal':
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valDueDate();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Setter Getter Functions
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['days_till_due_date'] ) ) $this->setDaysTillDueDate( $arrmixValues['days_till_due_date'] );
		if( true == isset( $arrmixValues['employee_name'] ) ) $this->setEmployeeName( $arrmixValues['employee_name'] );
		if( true == isset( $arrmixValues['employee_name'] ) ) $this->setEmployeeName( $arrmixValues['employee_name'] );
		if( true == isset( $arrmixValues['employee_goal_type_name'] ) ) $this->setEmployeeGoalTypeName( $arrmixValues['employee_goal_type_name'] );

		if( true == isset( $arrmixValues['coach_employee_name'] ) && true == $boolDirectSet ) {
			$this->set( 'm_strCoachEmployeeName', trim( $arrmixValues['coach_employee_name'] ) );
		} elseif( true == isset( $arrmixValues['coach_employee_name'] ) ) {
			$this->setCoachEmployeeName( $arrmixValues['coach_employee_name'] );
		}

		if( true == isset( $arrmixValues['call_agent_employee_name_full'] ) ) $this->setCallAgentEmployeeNameFull( $arrmixValues['call_agent_employee_name_full'] );
		if( true == isset( $arrmixValues['is_due_date_passed'] ) ) $this->setIsDueDatePassed( 't' == $arrmixValues['is_due_date_passed'] ? 1 : 0 );
		if( true == isset( $arrmixValues['name_full'] ) ) $this->setNameFull( $arrmixValues['name_full'] );
		return true;
	}

	public function setDaysTillDueDate( $intDaysTillDueDate ) {
		$this->m_intDaysTillDueDate = $intDaysTillDueDate;
	}

	public function getDaysTillDueDate() {
		return $this->m_intDaysTillDueDate;
	}

	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = $strEmployeeName;
	}

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	public function setEmployeeGoalTypeName( $strEmployeeGoalTypeName ) {
		$this->m_strEmployeeGoalTypeName = $strEmployeeGoalTypeName;
	}

	public function getEmployeeGoalTypeName() {
		return $this->m_strEmployeeGoalTypeName;
	}

	public function setCoachEmployeeName( $strCoachEmployeeName ) {
		$this->m_strCoachEmployeeName = $strCoachEmployeeName;
	}

	public function getCoachEmployeeName() {
		return $this->m_strCoachEmployeeName;
	}

	public function setCallAgentEmployeeNameFull( $strCallAgentEmployeeNameFull ) {
		$this->m_strCallAgentEmployeeNameFull = $strCallAgentEmployeeNameFull;
	}

	public function getCallAgentEmployeeNameFull() {
		return $this->m_strCallAgentEmployeeNameFull;
	}

	public function getIsDueDatePassed() {
		return $this->m_boolIsDueDatePassed;
	}

	public function setIsDueDatePassed( $boolIsDueDatePassed ) {
		$this->m_boolIsDueDatePassed = $boolIsDueDatePassed;
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

}
?>