<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyPaymentTransactions
 * Do not add any new functions to this class.
 */

class CCompanyPaymentTransactions extends CBaseCompanyPaymentTransactions {

	public static function fetchNextId( $objDatabase ) {

		$objDataset = $objDatabase->createDataset();

		$strSql = "SELECT nextval('company_payment_transactions_id_seq'::text) AS id";

		if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
			$objDataset->cleanup();
			return false;
		}

		$arrValues = $objDataset->fetchArray();
		$intId = $arrValues['id'];

		$objDataset->cleanup();

		return $intId;
	}

	public static function fetchCompanyPaymentAuthCodeByCompanyPaymentIdByPaymentTransactionTypeIds( $intPaymentId, $arrintPaymentTransactionTypeIds, $objDatabase ) {
		$strAuthCode = NULL;

		$strSql = 'SELECT gateway_transaction_number, gateway_approval_code FROM company_payment_transactions WHERE company_payment_id = ' . ( int ) $intPaymentId . ' AND payment_transaction_type_id IN ( ' . implode( ',', $arrintPaymentTransactionTypeIds ) . ') ORDER BY id DESC LIMIT 1;';

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		if( 0 < $objDataset->getRecordCount() ) {

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();

				if( true == isset( $arrmixValues['gateway_transaction_number'] ) ) {
					$strAuthCode = $arrmixValues['gateway_transaction_number'];
				} elseif( true == isset( $arrmixValues['gateway_approval_code'] ) ) {
					$strAuthCode = $arrmixValues['gateway_approval_code'];
				}

				$objDataset->next();
			}
		}

		$objDataset->cleanup();

		return $strAuthCode;
	}
}
?>