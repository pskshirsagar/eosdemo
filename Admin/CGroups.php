<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroups
 * Do not add any new functions to this class.
 */

class CGroups extends CBaseGroups {

	public static function fetchAllGroups( $objDatabase ) {
		return self::fetchGroups( 'SELECT * FROM groups ORDER BY name', $objDatabase );
	}

	public static function fetchActiveGroups( $objDatabase ) {
		return self::fetchGroups( 'SELECT * FROM groups WHERE deleted_by IS NULL ORDER BY name ', $objDatabase );
	}

	public static function fetchGroupsByUserId( $intUserId, $objDatabase ) {
		return self::fetchGroups( 'SELECT g.* FROM groups g, user_groups ug WHERE g.id = ug.group_id AND ug.user_id = ' . ( int ) $intUserId . ' AND ug.deleted_by IS NULL', $objDatabase );
	}

	public static function fetchActiveGroupsByUserId( $intUserId, $objDatabase ) {
		return self::fetchGroups( 'SELECT g.* FROM groups g, user_groups ug WHERE g.deleted_by IS NULL AND g.id = ug.group_id AND ug.user_id = ' . ( int ) $intUserId . ' AND ug.deleted_by IS NULL ORDER BY name', $objDatabase );
	}

	public static function fetchGroupsByGroupTypeId( $intGroupTypeId, $objDatabase ) {
		return self::fetchGroups( 'SELECT * FROM groups WHERE deleted_by IS NULL AND group_type_id = ' . ( int ) $intGroupTypeId . ' ORDER BY name', $objDatabase );
	}

	public static function fetchGroupsByGroupTypeIds( $arrintGroupTypeIds, $objDatabase ) {

		$strSql = 'SELECT
						g.id,
						g.name,
						g.description,
						g.group_type_id,
						g.active_directory_guid,
						COUNT ( u.id ) AS associated_users_count
					FROM
						groups g
						LEFT JOIN user_groups ug ON ( g.id = ug.group_id AND ug.deleted_by IS NULL )
						LEFT JOIN users u ON ( ug.user_id = u.id AND u.is_disabled = 0 )
					WHERE
						g.group_type_id IN ( ' . implode( ',', $arrintGroupTypeIds ) . ' )
						AND g.deleted_by IS NULL
					GROUP BY
						g.id,
						g.group_type_id,
						g.name,
						g.description,
						g.active_directory_guid
					ORDER BY
						g.name';

		return self::fetchGroups( $strSql, $objDatabase );

	}

	public static function fetchGroupsByDbWatchId( $intDbWatchId, $objDatabase ) {
		return self::fetchGroups( 'SELECT g.* FROM groups g, db_watch_groups dwg WHERE g.id = dwg.group_id AND dwg.db_watch_id = ' . ( int ) $intDbWatchId . ' ORDER BY name', $objDatabase );
	}

	public static function fetchGroupByName( $strGroupName, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						groups g
					WHERE
						g.name = \'' . addslashes( $strGroupName ) . '\'';
		return self::fetchGroup( $strSql, $objDatabase );
	}

	public static function fetchGroupsByIds( $arrintGroupIds, $objDatabase ) {

		if( false == valArr( $arrintGroupIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						groups as g
					WHERE
						g.id IN ( ' . implode( ',', $arrintGroupIds ) . ' )
					ORDER BY
						g.name';

		return self::fetchGroups( $strSql, $objDatabase );
	}

	public static function fetchAllGroupsDetails( $objDatabase ) {
		return self::fetchGroups( 'SELECT * FROM groups WHERE deleted_by IS NULL ORDER BY name', $objDatabase );
	}

	public static function fetchPaginatedGroupsByGroupTypeIdByFilter( $objTrainingGroupsFilter, $intGroupTypeId, $objDatabase ) {

		$intOffset = ( 0 < $objTrainingGroupsFilter->getPageNo() ) ? $objTrainingGroupsFilter->getPageSize() * ( $objTrainingGroupsFilter->getPageNo() - 1 ) : 0;
		$intLimit  = ( int ) $objTrainingGroupsFilter->getPageSize();
		$strOrderClause = ' ';
		$strOrder = '';

		if( false == is_null( $objTrainingGroupsFilter ) && false == is_null( $objTrainingGroupsFilter->getOrderByField() ) && false == is_null( $objTrainingGroupsFilter->getOrderByType() ) ) {
			$strOrder = ( 1 == $objTrainingGroupsFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objTrainingGroupsFilter->getOrderByField() . '' . $strOrder;
		}

		$strSql = 'SELECT * FROM groups WHERE group_type_id = ' . ( int ) $intGroupTypeId . ' AND deleted_by  IS NULL' . $strOrderClause . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchGroups( $strSql, $objDatabase );
	}

	public static function fetchTrainingGroupsCountByFilter( $objDatabase ) {

		$strWhere = 'WHERE group_type_id=' . CGroupType::TRAINING . ' AND deleted_by IS NULL';

		$arrintTrainingGroupsCount = self::fetchRowCount( $strWhere, 'groups', $objDatabase );

		return ( true == isset( $arrintTrainingGroupsCount ) ) ? ( int ) $arrintTrainingGroupsCount : 0;

	}

	public static function fetchGroupsByGroupIds( $arrintGroupIds, $objDatabase ) {
		return self::fetchGroups( 'SELECT * FROM groups WHERE id IN ( ' . implode( ',', $arrintGroupIds ) . ' ) ', $objDatabase );
	}

	public static function fetchGroupByTrainingGroupId( $intTrainingGroupId, $objDatabase ) {
		return self::fetchGroup( sprintf( 'SELECT * FROM groups WHERE deleted_by IS NULL AND id = %d ', ( int ) $intTrainingGroupId ), $objDatabase );
	}

	public static function fetchAllGroupsUsersCount( $boolIsObject = false, $objDatabase ) {
		$strSql = 'SELECT
						g.id,
						count( DISTINCT ug.user_id ) associated_users_count
					FROM
						groups g
						JOIN user_groups ug ON g.id = ug.group_id AND ug.deleted_by IS NULL
						LEFT JOIN users u ON u.id = ug.user_id
						LEFT JOIN employees e ON e.id = u.employee_id
						LEFT JOIN employee_addresses AS ea ON u.employee_id = ea.employee_id
					WHERE g.deleted_by IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					GROUP BY
						g.id';

		if( true == $boolIsObject ) {
			return self::fetchGroups( $strSql, $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchAssignedGroupsByTestId( $intTestId, $objDatabase ) {

		$strSql = 'SELECT
						g.id,
						g.name
					FROM
						groups g,
						test_groups tg
					WHERE
						g.id = tg.group_id
						AND tg.test_id::integer = ' . ( int ) $intTestId . '
					ORDER BY
						lower ( g.name )';

		return self::fetchGroups( $strSql, $objDatabase );
	}

	public static function fetchUnAssignedGroupsByTestId( $intTestId, $objDatabase ) {

		$strSql = 'SELECT
						g.*
					FROM
						groups g
					WHERE
						g.id NOT IN (
									SELECT
										DISTINCT g.id
									FROM
										groups g
									JOIN
										test_groups tg ON ( g.id = tg.group_id )
									WHERE
										tg.test_id = ' . ( int ) $intTestId . ' )
					ORDER BY
						g.name';

		return self::fetchGroups( $strSql, $objDatabase );
	}

	public static function fetchAllInitiativeGroups( $objAdminDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						groups
					WHERE
						( NAME ILIKE \'%interviewer%\'
						OR NAME ILIKE \'%trainer%\'
						OR NAME ILIKE \'%xelp%\'
						OR NAME ILIKE \'%xplode%\' )
						AND deleted_by IS NULL
					ORDER BY
						name';

		return self::fetchGroups( $strSql, $objAdminDatabase );
	}

	public static function fetchAllCurrentActiveDirectoryGroups( $objDatabase ) {
		return self::fetchGroups( 'SELECT * FROM groups WHERE deleted_by IS NULL AND active_directory_guid IS NOT NULL ORDER BY name', $objDatabase );
	}

	public static function fetchGroupsDetailByGroupTypeId( $intGroupTypeId, $objDatabase, $boolIsParentGroup = false ) {

		$strWhere = ' AND g.group_type_id = ' . ( int ) $intGroupTypeId;
		if( true == $boolIsParentGroup ) {
			$strWhere .= ' AND g.parent_group_id IS NULL';
		}

		$strSql = 'SELECT
						g.id,
 						g.name,
						array_to_string( array_agg( CASE
														WHEN u.active_directory_guid IS NULL THEN u.svn_username
														ELSE u.username
													END ), \',\' ) AS associated_users
					FROM
						groups g
						LEFT JOIN user_groups ug ON ( g.id = ug.group_id AND ug.deleted_by IS NULL )
						LEFT JOIN users u ON ( ug.user_id = u.id AND u.is_disabled = 0 )
					WHERE
						g.deleted_by IS NULL
						' . $strWhere . '
					GROUP BY
						g.id, g.name
					ORDER BY
						g.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNonPermissionedGroupsByPsModuleId( $objDatabase, $intModuleId, $arrstrFilteredExplodedSearch ) {

		$strSql = 'SELECT
					    g.id,
					    g.name
					FROM
					    groups g
					WHERE
					  	g.deleted_by IS NULL
						AND g.group_type_id = 1
						AND ( g.name ILIKE \'%' . implode( '%\' AND g.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
						AND NOT EXISTS (
								SELECT
									1
								FROM
									(
										SELECT
											id
										FROM
											groups
										WHERE
											id = g.id
											AND deleted_by IS NULL
									) AS g_temp
									JOIN
									(
										SELECT
											ps_module_id AS id
										FROM
											ps_modules
										WHERE id = ' . ( int ) $intModuleId . '
									) pm ON TRUE
									LEFT JOIN group_permissions gp ON ( pm.id = gp.ps_module_id AND g_temp.id = gp.group_id )
								GROUP BY
									pm.id,
									g_temp.id
								HAVING
									max ( gp.is_allowed ) = 0
				    	)
					 AND NOT EXISTS (
							SELECT
								1
							FROM
								group_permissions up
							WHERE
								group_id = g.id
								AND ps_module_id = ' . ( int ) $intModuleId . '
								AND is_allowed = 1
					)
					ORDER BY
						g.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPermissionedGroupsByModuleId( $intModuleId, $objDatabase ) {

		$strSql = 'SELECT
						g.id,
    					g.name
					FROM
    					groups g
    					JOIN group_permissions gp ON ( g.id = gp.group_id )
					WHERE
						g.deleted_on IS NULL
						AND g.deleted_by IS NULL
   						AND gp.ps_module_id = ' . ( int ) $intModuleId . '
                  		AND gp.is_allowed = 1
					ORDER BY
					    g.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchGroupsByGroupTypeIdByUserIdByTitle( $intGroupTypeId, $intReportId, $intUserId, $strTitle, $intGroupId, $boolAddedReport, $objDatabase, $boolReportAdminUser = false ) {
		if( false == valId( $intGroupTypeId ) || false == valId( $intReportId ) ) {
			return NULL;
		}

		$strTitle = [ addslashes( $strTitle ) ];
		$strWhereSql = ' AND rga.id IS NULL AND ( SELECT COUNT( rga1.id ) FROM report_group_associations AS rga1 WHERE rga1.group_id= g.id AND rga1.deleted_on IS NULL ) < ' . ( int ) CReportGroupSchedule::REPORTS_PER_GROUP;
		if( false == $boolAddedReport ) {
			$strWhereSql = ' AND rga.id IS NOT NULL';
		}

		if( true == valStr( $strTitle[0] ) ) {
			$strWhereSql .= ' AND lower( g.name ) LIKE \'%' . \Psi\CStringService::singleton()->strtolower( $strTitle[0] ) . '%\'';
		}

		if( true == valId( $intGroupId ) ) {
			$strWhereSql .= ' AND g.id =' . ( int ) $intGroupId;
		}

		if( true == valId( $intUserId ) && false == $boolReportAdminUser ) {
			$strWhereSql .= ' AND g.created_by =' . ( int ) $intUserId;
		}

		$strSql = 'SELECT
						g.id,
						g.name,
						g.description,
						gst.name as frequncy,
						CASE WHEN rga.id IS NOT NULL THEN 1 ELSE 0 END AS is_associated,
						( SELECT COUNT( rga1.id ) FROM report_group_associations AS rga1 WHERE rga1.group_id= g.id AND rga1.deleted_on IS NULL ) AS group_reports_count
					FROM
						groups g
						JOIN report_group_schedules rgs ON ( rgs.group_id = g.id )
						JOIN group_schedule_types gst ON ( gst.id = rgs.group_schedule_type_id )
						LEFT JOIN report_group_associations rga ON( rga.group_id = g.id and rga.company_report_id = ' . ( int ) $intReportId . ' AND rga.deleted_on IS NULL )
					WHERE
						g.deleted_by IS NULL AND group_type_id = ' . ( int ) $intGroupTypeId . $strWhereSql;

		return fetchData( $strSql, $objDatabase );
	}

}
?>