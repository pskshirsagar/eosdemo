<?php

class CHelpResourceNote extends CBaseHelpResourceNote {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewerEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourceReviewStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>