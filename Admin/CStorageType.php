<?php

class CStorageType extends CBaseStorageType {

	const MOUNTS			    = 1;
	const NON_BACKUP_MOUNTS 	= 2;
	const VOIP_MOUNTS 		    = 3;
	const TEST_CLIENTS 		    = 4;

	public static $c_arrintAllCStorageType = [
		CStorageType::MOUNTS 	            => [ 'id' => CStorageType::MOUNTS, 'name' => 'Mounts' ],
		CStorageType::NON_BACKUP_MOUNTS 	=> [ 'id' => CStorageType::NON_BACKUP_MOUNTS, 'name' => 'NonBackupMounts' ],
		CStorageType::VOIP_MOUNTS 	        => [ 'id' => CStorageType::VOIP_MOUNTS, 'name' => 'VoipMounts' ],
		CStorageType::TEST_CLIENTS 	        => [ 'id' => CStorageType::TEST_CLIENTS, 'name' => 'test_clients' ]
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>