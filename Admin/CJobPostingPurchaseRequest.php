<?php

class CJobPostingPurchaseRequest extends CBaseJobPostingPurchaseRequest {

	public function valPsJobPostingId() {
		$boolIsValid = true;
		if( true == is_null( $this->getPsJobPostingId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_job_posting_id', 'Job Posting is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPurchaseRequestId() {
		$boolIsValid = true;
		if( true == is_null( $this->getPurchaseRequestId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_request_id', 'Purchase Request is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsHrEmployee = false, $strCountryCode = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				if( CCountry::CODE_INDIA == $strCountryCode ) $boolIsValid &= $this->valPsJobPostingId();
				if( false == $boolIsHrEmployee ) $boolIsValid &= $this->valPurchaseRequestId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>