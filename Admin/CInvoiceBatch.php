<?php

use Psi\Eos\Admin\CInvoices;
use Psi\Eos\Admin\CInvoiceBatches;
use Psi\Eos\Admin\CChargeCodes;
use Psi\Eos\Admin\CCommissionBatches;
use Psi\Eos\Admin\CCommissions;
use Psi\Eos\Admin\CTransactions;

use Psi\Libraries\ExternalPrinceWrapper\CPrinceFactory;

/**
 * Class CInvoiceBatch
 * @method getAttachments() attachments stored using TEosDetails
 * @method setAttachments( $arrstrAttachments )
 */
class CInvoiceBatch extends CBaseInvoiceBatch {

	protected $m_intInvoiceCount;

	protected $m_intInvoicesMailed;
	protected $m_intInvoicesEmailed;

	protected $m_intAmountBilledUnsuccessfully;

	protected $m_fltPaymentsInPeriod;
	protected $m_fltChargesInPeriod;

	protected $m_fltTrack1LateFeeChargesInPeriod;
	protected $m_fltTrack2LateFeeChargesInPeriod;

	public function __construct() {
		parent::__construct();

		$this->m_intAmountBilledUnsuccessfully	= 0;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getTrack1LateFeeChargesInPeriod() {
		return $this->m_fltTrack1LateFeeChargesInPeriod;
	}

	public function getTrack2LateFeeChargesInPeriod() {
		return $this->m_fltTrack2LateFeeChargesInPeriod;
	}

	public function getTrack1FormattedInvoiceCount() {
		return( true == is_numeric( $this->m_intTrack1InvoiceCount ) ) ? __( '{%d, 0, p:0}', [ $this->m_intTrack1InvoiceCount ] ) : 0;
	}

	public function getTrack2FormattedInvoiceCount() {
		return( true == is_numeric( $this->m_intTrack2InvoiceCount ) ) ? __( '{%d, 0, p:0}', [ $this->m_intTrack2InvoiceCount ] ) : 0;
	}

	public function getTotalFormattedInvoiceCount() {
		return __( '{%d, 0, p:0}', [ ( ( int ) $this->m_intTrack1InvoiceCount + ( int ) $this->m_intTrack2InvoiceCount ) ] );
	}

	public function getTrack1AmountUnsuccessful() {
		return( true == is_numeric( $this->m_fltTrack1AmountUnsuccessful ) ) ? abs( __( '{%f, 0, p:0}', [ $this->m_fltTrack1AmountUnsuccessful ] ) ) : 0;
	}

	public function getTrack2AmountUnsuccessful() {
		return( true == is_numeric( $this->m_fltTrack2AmountUnsuccessful ) ) ? abs( __( '{%f, 0, p:0}', [ $this->m_fltTrack2AmountUnsuccessful ] ) ) : 0;
	}

	public function getTrack1PaymentsInPeriod() {
		return( true == is_numeric( $this->m_fltTrack1PaymentsInPeriod ) ) ? __( '{%f, 0, p:0}', [ abs( $this->m_fltTrack1PaymentsInPeriod ) ] ) : 0;
	}

	public function getTrack1FormattedPaymentsInPeriod() {
		return( true == is_numeric( $this->m_fltTrack1PaymentsInPeriod ) ) ? abs( $this->m_fltTrack1PaymentsInPeriod ) : 0;
	}

	public function getTrack2PaymentsInPeriod() {
		return( true == is_numeric( $this->m_fltTrack2PaymentsInPeriod ) ) ? __( '{%f, 0, p:0}', [ abs( $this->m_fltTrack2PaymentsInPeriod ) ] ) : 0;
	}

	public function getTrack2FormattedPaymentsInPeriod() {
		return( true == is_numeric( $this->m_fltTrack2PaymentsInPeriod ) ) ? abs( $this->m_fltTrack2PaymentsInPeriod ) : 0;
	}

	public function getTrack1ChargesInPeriod() {
		return( true == is_numeric( $this->m_fltTrack1ChargesInPeriod ) ) ? __( '{%f, 0, p:0}', [ abs( $this->m_fltTrack1ChargesInPeriod ) ] ) : 0;
	}

	public function getTrack1FormattedChargesInPeriod() {
		return( true == is_numeric( $this->m_fltTrack1ChargesInPeriod ) ) ? abs( $this->m_fltTrack1ChargesInPeriod ) : 0;
	}

	public function getTrack2ChargesInPeriod() {
		return( true == is_numeric( $this->m_fltTrack2ChargesInPeriod ) ) ? __( '{%f, 0, p:0}', [ abs( $this->m_fltTrack2ChargesInPeriod ) ] ) : 0;
	}

	public function getTrack2FormattedChargesInPeriod() {
		return( true == is_numeric( $this->m_fltTrack2ChargesInPeriod ) ) ? abs( $this->m_fltTrack2ChargesInPeriod ) : 0;
	}

	public function getTotalChargesInPeriod() {
		return __( '{%f, 0, p:0}', [ ( abs( ( float ) $this->m_fltTrack1ChargesInPeriod ) + abs( ( float ) $this->m_fltTrack2ChargesInPeriod ) ) ] );
	}

	public function getTotalPaymentsInPeriod() {
		return __( '{%f, 0, p:0}', [ ( abs( ( float ) $this->m_fltTrack1PaymentsInPeriod ) + abs( ( float ) $this->m_fltTrack2PaymentsInPeriod ) ) ] );
	}

	public function getPercentTrack1InvoicesMailed() {
		if( 0 < $this->m_intTrack1InvoiceCount ) {
			return __( '{%f, 0, p:1}', [ ( ( $this->m_intTrack1InvoicesMailed / $this->m_intTrack1InvoiceCount ) * 100 ) ] );
		} else {
			return 0;
		}
	}

	public function getPercentTrack1InvoicesEmailed() {
		if( 0 < $this->m_intTrack1InvoiceCount ) {
			return __( '{%f, 0, p:0}', [ ( ( $this->m_intTrack1InvoicesEmailed / $this->m_intTrack1InvoiceCount ) * 100 ) ] );
		} else {
			return 0;
		}
	}

	public function getPercentTrack1InvoicesFtpd() {
		if( 0 < $this->m_intTrack1InvoiceCount ) {
			return __( '{%f, 0, p:1}', [ ( ( $this->m_intTrack1InvoicesFtpd / $this->m_intTrack1InvoiceCount ) * 100 ) ] );
		} else {
			return 0;
		}
	}

	public function getPercentTrack2InvoicesMailed() {
		if( 0 < $this->m_intTrack2InvoiceCount ) {
			return __( '{%f, 0, p:1}', [ ( ( $this->m_intTrack2InvoicesMailed / $this->m_intTrack2InvoiceCount ) * 100 ) ] );
		} else {
			return 0;
		}
	}

	public function getPercentTrack2InvoicesEmailed() {
		if( 0 < $this->m_intTrack2InvoiceCount ) {
			return __( '{%f, 0, p:0}', [ ( ( $this->m_intTrack2InvoicesEmailed / $this->m_intTrack2InvoiceCount ) * 100 ) ] );
		} else {
			return 0;
		}
	}

	public function getPercentTrack2InvoicesFtpd() {
		if( 0 < $this->m_intTrack2InvoiceCount ) {
			return __( '{%f, 0, p:1}', [ ( ( $this->m_intTrack2InvoicesFtpd / $this->m_intTrack2InvoiceCount ) * 100 ) ] );
		} else {
			return 0;
		}
	}

	public function getCurrentInvoiceMethodId() {
		if( date( 'Y-m-d' ) < date( 'Y-m-', strtotime( $this->getBatchDatetime() ) ) . '11' ) {
			return CInvoiceMethod::TRACK_1;
		}

		$arrobjInvoiceBatchProcesses = \Psi\Eos\Admin\CInvoiceBatchProcesses::createService()->fetchInvoiceProcessesByStatusByRunDayOfMonth( $this->m_intId, 3, 1, $this->m_objDatabase );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjInvoiceBatchProcesses ) ) {
			return CInvoiceMethod::TRACK_1;
		}

		return CInvoiceMethod::TRACK_2;
	}

	public function getInvoiceBatchStepName() {
		return CInvoiceBatchStep::getInvoiceBatchStepNameByInvoiceBatchStepId( $this->getInvoiceBatchStepId() );
	}

	/**
	 * Set Functions
	 *
	 */

	public function setTrack1LateFeeChargesInPeriod( $fltTrack1LateFeeChargesInPeriod ) {
		$this->m_fltTrack1LateFeeChargesInPeriod = $fltTrack1LateFeeChargesInPeriod;
	}

	public function setTrack2LateFeeChargesInPeriod( $fltTrack2LateFeeChargesInPeriod ) {
		$this->m_fltTrack2LateFeeChargesInPeriod = $fltTrack2LateFeeChargesInPeriod;
	}

	public function setDefaults( $objDatabase = NULL ) {
		$objLastCompletedInvoiceBatch = CInvoiceBatches::createService()->fetchLastCompletedInvoiceBatch( $objDatabase );

		$this->setBatchDatetime( date( 'm/1/Y', strtotime( '+1 month', strtotime( $objLastCompletedInvoiceBatch->getBatchDatetime() ) ) ) );
		$this->setInvoiceBatchStepId( CInvoiceBatchStep::COMPLETE );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['track1_late_fee_charges_in_period'] ) && $boolDirectSet ) {
			$this->m_intId = trim( $arrmixValues['track1_late_fee_charges_in_period'] );
		} elseif( isset( $arrmixValues['track1_late_fee_charges_in_period'] ) ) {
			$this->setTrack1LateFeeChargesInPeriod( $arrmixValues['track1_late_fee_charges_in_period'] );
		}
		if( isset( $arrmixValues['track2_late_fee_charges_in_period'] ) && $boolDirectSet ) {
			$this->m_intId = trim( $arrmixValues['track2_late_fee_charges_in_period'] );
		} elseif( isset( $arrmixValues['track2_late_fee_charges_in_period'] ) ) {
			$this->setTrack2LateFeeChargesInPeriod( $arrmixValues['track2_late_fee_charges_in_period'] );
		}
	}

	public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {

		if( true == valArr( $arrmixFormFields ) ) {
			$arrmixRequestForm = mergeIntersectArray( $arrmixFormFields, $arrmixRequestForm );
		}

		$this->setValues( $arrmixRequestForm, false );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valBatchDatetime( $objDatabase ) {
		$boolIsValid = true;

		$intConflictingInvoiceBatchCount = CInvoiceBatches::createService()->fetchConflictingInvoiceBatchCountByBatchDate( $this->getBatchDatetime(), $objDatabase );

		if( 0 < $intConflictingInvoiceBatchCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'batch_datetime', 'A batch already exists for the current month.  Only one batch can be created per month.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valDeleteInvoices( $intInvoiceMethodId ) {
		$boolIsValid = true;

		if( ( CInvoiceMethod::TRACK_1 == $intInvoiceMethodId && CInvoiceBatchStep::TRACK_1_INVOICES_GENERATED != $this->getInvoiceBatchStepId() )
				|| ( CInvoiceMethod::TRACK_2 == $intInvoiceMethodId && CInvoiceBatchStep::TRACK_2_INVOICES_GENERATED != $this->getInvoiceBatchStepId() ) ) {

			$this->addErrorMsg( new CErrorMsg( NULL, 'invoice_batch_step_id', 'You cannot delete a batch of invoices that has already been emailed, or billed electronically.', NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valInvoiceBatchDuplicity( $objDatabase ) {
		$boolIsValid = true;

		$intOpenInvoiceBatchCount = CInvoiceBatches::createService()->fetchOpenInvoiceBatchCount( $objDatabase );

		if( 0 != $intOpenInvoiceBatchCount ) {
			$this->addErrorMsg( new CErrorMsg( NULL, '', 'You cannot create a new invoice batch when an existing invoice batch is still open.', NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strValidationType, $objDatabase = NULL, $intInvoiceMethodId = NULL ) {

		$boolIsValid = true;

		switch( $strValidationType ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valInvoiceBatchDuplicity( $objDatabase );
				$boolIsValid &= $this->valBatchDatetime( $objDatabase );
				break;

			case 'validate_delete_invoices':
				$boolIsValid &= $this->valDeleteInvoices( $intInvoiceMethodId );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function generateInvoices( $objDatabase ) {
		return CInvoices::createService()->fetchNewInvoicesByBatchDate( $this->m_strBatchDatetime, $objDatabase );
	}

	public function associateLooseInvoices( $intUserId, $objDatabase ) {

		$arrobjInvoices = CInvoices::createService()->fetchInvoicesUnassociatedToInvoiceBatch( $objDatabase );

		if( true == valArr( $arrobjInvoices ) ) {
			foreach( $arrobjInvoices as $objInvoice ) {

				$objInvoice->setInvoiceBatchId( $this->getId() );

				if( false == $objInvoice->update( $intUserId, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, '', 'Loose invoice failed to associated to invoice batch.', NULL ) );
					return false;
				}
			}

			CTransactions::createService()->combineNewChargesIntoExistingInvoicesByInvoiceIds( array_keys( $arrobjInvoices ), $objDatabase );
		}

		return true;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchUnpostedInvoicesByInvoiceMethodIdByCompanyStatusTypeId( $intInvoiceMethodId, $arrintCompanyStatusTypeIds, $objDatabase, $intCid = NULL ) {
		return CInvoices::createService()->fetchUnpostedInvoicesByInvoiceBatchByInvoiceMethodIdByCompanyStatusTypeId( $this, $intInvoiceMethodId, $arrintCompanyStatusTypeIds, $objDatabase, $intCid );
	}

	public function deleteInvoices( $intInvoiceMethodId, $objDatabase ) {

		// First this query sets all of the company invoice ids in a particular batch of transactions NULL where they are associated to a invoice.
		// Second, the query deletes all company invoices in a particular batch only if the sent on date is NULL.
		// Third, change the status back to INVOICE_BATCH_STEP_TRACK_1_POST_CHARGES;

		$strSql = 'UPDATE
						transactions
					SET
						invoice_id = NULL
					FROM
						invoices,
						accounts
					WHERE
						transactions.invoice_id = invoices.id
						AND invoices.account_id = accounts.id
						AND invoices.invoice_batch_id = ' . $this->getId() . '
						AND invoices.mailed_on IS NULL
						AND invoices.emailed_on IS NULL
						AND invoices.company_payment_id IS NULL
						AND accounts.invoice_method_id = ' . ( int ) $intInvoiceMethodId . '
						AND accounts.account_type_id <> ' . CAccountType::UTILITY_BILLING . '
						AND invoices.invoice_status_type_id <> ' . ( int ) CInvoiceStatusType::INITIAL_BILLING_SUCCESSFUL . ';

					DELETE FROM
							invoices
					WHERE
						id IN ( SELECT i.id FROM
									invoices i,
									accounts a
								WHERE
									i.cid = a.cid
									AND i.account_id = a.id
									AND i.invoice_batch_id  = ' . ( int ) $this->getId() . '
									AND i.emailed_on IS NULL
									AND i.mailed_on IS NULL
									AND i.company_payment_id IS NULL
									AND a.invoice_method_id = ' . ( int ) $intInvoiceMethodId . '
									AND a.account_type_id <> ' . CAccountType::UTILITY_BILLING . '
									AND i.invoice_status_type_id <> ' . ( int ) CInvoiceStatusType::INITIAL_BILLING_SUCCESSFUL . ' );';

		if( CInvoiceMethod::TRACK_1 == $intInvoiceMethodId ) {

			$strSql .= '
						UPDATE
								invoice_batches
							SET
								invoice_batch_step_id = ' . CInvoiceBatchStep::TRACK_1_CHARGES_POSTED . '
							WHERE
								id = ' . ( int ) $this->getId() . '; ';

		} elseif( CInvoiceMethod::TRACK_2 == $intInvoiceMethodId ) {

			$strSql .= '
							UPDATE
								invoice_batches
							SET
								invoice_batch_step_id = ' . CInvoiceBatchStep::TRACK_2_CHARGES_POSTED . '
							WHERE
								id = ' . ( int ) $this->getId() . '; ';
		}

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			trigger_error( 'Error: sql failed to delete invoices and set null the invoice_id field on transactions' );
			return false;

		} else {
			return true;
		}
	}

	public function fetchUnverifiedInvoicesByInvoiceMethodId( $intInvoiceMethodId, $objDatabase ) {
		return CInvoices::createService()->fetchUnverifiedInvoicesByInvoiceBatchIdByInvoiceMethodId( $this->getId(), $intInvoiceMethodId, $objDatabase );
	}

	public function fetchInvoicesByInvoiceMethodId( $intInvoiceMethodId, $objDatabase ) {
		return CInvoices::createService()->fetchInvoicesByInvoiceBatchIdByInvoiceMethodId( $this->m_intId, $intInvoiceMethodId, $objDatabase );
	}

	public function fetchIsEmailedInvoicesByInvoiceMethodId( $intInvoiceMethod, $objDatabase ) {
		return CInvoices::createService()->fetchIsEmailedInvoicesByInvoiceBatchIdByInvoiceMethodId( $this->m_intId, $intInvoiceMethod, $objDatabase, false );
	}

	public function fetchIsFtpdInvoicesByInvoiceMethodId( $intInvoiceMethod, $objDatabase ) {
		return CInvoices::createService()->fetchIsFtpdInvoicesByInvoiceBatchIdByInvoiceMethodId( $this->m_intId, $intInvoiceMethod, $objDatabase, false );
	}

	public function fetchUnEmailedInvoicesByInvoiceMethodId( $intInvoiceMethodId, $objDatabase ) {
		return CInvoices::createService()->fetchIsEmailedInvoicesByInvoiceBatchIdByInvoiceMethodId( $this->getId(), $intInvoiceMethodId, $objDatabase, true );
	}

	public function fetchUnMailedInvoicesByInvoiceMethodId( $intInvoiceMethodId, $objDatabase ) {
		return CInvoices::createService()->fetchIsMailedInvoicesByInvoiceBatchIdByInvoiceMethodId( $this->getId(), $intInvoiceMethodId, $objDatabase, $boolPendingMailingOnly = true );
	}

	public function fetchUnFtpdInvoicesByInvoiceMethodId( $intInvoiceMethod, $objDatabase ) {
		return CInvoices::createService()->fetchIsFtpdInvoicesByInvoiceBatchIdByInvoiceMethodId( $this->m_intId, $intInvoiceMethod, $objDatabase, true );
	}

	public function fetchUnPostedCoupaInvoicesByInvoiceMethodId( $intInvoiceMethod, $objAdminDatabase ) {
		return CInvoices::createService()->fetchCoupaInvoicesByInvoiceBatchIdByInvoiceMethodId( $this->m_intId, $intInvoiceMethod, $objAdminDatabase );
	}

	public function fetchMailedInvoicesByInvoiceMethodId( $intInvoiceMethodId, $objDatabase ) {
		return CInvoices::createService()->fetchIsMailedInvoicesByInvoiceBatchIdByInvoiceMethodId( $this->m_intId, $intInvoiceMethodId, $objDatabase );
	}

	public function confirmNonAutoPayInvoicesByInvoiceMethodID( $objDatabase ) {

		$intInvoiceMethodId = $this->getCurrentInvoiceMethodId();

		// Update all records that have negative balances to status type sucess even though they weren't actually invoiced
		$strSql = '
			UPDATE
				invoices SET invoice_status_type_id = ' . ( int ) CInvoiceStatusType::INITIAL_BILLING_SUCCESSFUL . '
			WHERE
				invoice_amount <= 0
				AND id IN(
					SELECT ci.id
					FROM
						invoices ci,
						accounts a
					WHERE
						ci.account_id = a.id
						AND a.invoice_method_id = ' . ( int ) $intInvoiceMethodId . '
						AND a.payment_type_id < ' . CPaymentType::ACH . '
						AND ci.invoice_batch_id = ' . ( int ) $this->m_intId . '
				)';

		fetchData( $strSql, $objDatabase );

	}

	public function confirmMailedInvoicesByAccountIdsByInvoiceMethodId( $arrintAccountIds, $intInvoiceMethodId, $objDatabase ) {

		if( false == valArr( $arrintAccountIds ) ) {
			return false;
		}

		$strSql = '
			UPDATE
				invoices SET mailed_on = \'' . date( 'm/d/Y' ) . '\'
			FROM
				accounts
			WHERE
				invoices.account_id = accounts.id
				AND invoices.mailed_on IS NULL
				AND invoices.invoice_batch_id = ' . ( int ) $this->getId() . '
				AND invoices.account_id IN ( ' . implode( ',', $arrintAccountIds ) . ' )
				AND accounts.invoice_method_id = ' . ( int ) $intInvoiceMethodId;

		fetchData( $strSql, $objDatabase );

		return true;
	}

	public function confirmEmailedInvoicesByInvoiceIdsByInvoiceMethodId( $arrintInvoiceIds, $intInvoiceMethodId, $objDatabase ) {

		if( false == valArr( $arrintInvoiceIds ) ) {
			return false;
		}

	$strSql = '
			UPDATE
				invoices SET emailed_on = \'' . date( 'm/d/Y' ) . '\'
			FROM accounts
			WHERE
				invoices.account_id = accounts.id
				AND invoices.emailed_on IS NULL
				AND invoices.invoice_batch_id = ' . ( int ) $this->getId() . '
				AND invoices.id IN (' . implode( ',', $arrintInvoiceIds ) . ')
				AND accounts.invoice_method_id = ' . ( int ) $intInvoiceMethodId;

		fetchData( $strSql, $objDatabase );

		return true;
	}

	public function confirmFtpdInvoicesByInvoiceIdsByInvoiceMethodId( $arrintInvoiceIds, $intInvoiceMethodId, $objDatabase ) {

		if( false == valArr( $arrintInvoiceIds ) ) {
			return false;
		}

		$strSql = '
			UPDATE
				invoices SET ftpd_on = \'' . date( 'm/d/Y' ) . '\'
			FROM accounts
			WHERE
				invoices.account_id = accounts.id
				AND invoices.ftpd_on IS NULL
				AND invoices.invoice_batch_id = ' . ( int ) $this->getId() . '
				AND invoices.id IN (' . implode( ',', $arrintInvoiceIds ) . ')
				AND accounts.invoice_method_id = ' . ( int ) $intInvoiceMethodId;

		fetchData( $strSql, $objDatabase );

		return true;
	}

	public function fetchUnPostedAutoBilledInvoicesWithIntermediaryAccountOrClearingAccountOrPositiveBalanceByInvoiceMethodId( $intInvoicMethodId, $objDatabase ) {

		$arrintPaymentTypeIds = [ CPaymentType::ACH, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX ];

		return CInvoices::createService()->fetchInvoicesWithPositiveBalanceOrIntermediaryAccountOrClearingAccountByInvoiceBatchIdByPaymentTypeIdsByInvoiceMethodId( $this->m_intId, $arrintPaymentTypeIds, $intInvoicMethodId, $objDatabase, $boolUnpaidInvoicesOnly = true );
	}

	public function generateInvoiceBatchPdfDocumentByInvoiceMethodId( $intInvoiceMethodId, $objAdminDatabase, $objUtilitiesDatabase, $boolMailedOnly = false, $boolUnverifiedOnly = false, $boolEmailedOnly = false, $boolFtpdOnly = false ) {

		$boolShowPastDueInvoices = false;

		if( true == $boolMailedOnly ) {
			$boolShowPastDueInvoices = true;
			$arrobjInvoices = $this->fetchMailedInvoicesByInvoiceMethodId( $intInvoiceMethodId, $objAdminDatabase );
		} elseif( true == $boolUnverifiedOnly ) {
			$arrobjInvoices = $this->fetchUnverifiedInvoicesByInvoiceMethodId( $intInvoiceMethodId, $objAdminDatabase );
		} elseif( true == $boolEmailedOnly ) {
			$arrobjInvoices = $this->fetchIsEmailedInvoicesByInvoiceMethodId( $intInvoiceMethodId, $objAdminDatabase );
		} elseif( true == $boolFtpdOnly ) {
			$arrobjInvoices = $this->fetchIsFtpdInvoicesByInvoiceMethodId( $intInvoiceMethodId, $objAdminDatabase );
		} else {
			$arrobjInvoices = $this->fetchInvoicesByInvoiceMethodId( $intInvoiceMethodId, $objAdminDatabase );
		}

		$arrobjInvoices = CInvoices::createService()->loadAssociatedInvoiceDataByInvoices( $arrobjInvoices, $objAdminDatabase, $objUtilitiesDatabase );

		$arrstrInvoiceFilePaths = [];
		if( true == valArr( $arrobjInvoices ) ) {

			foreach( $arrobjInvoices as $objInvoice ) {
				if( $boolShowPastDueInvoices ) {
					// Get past due invoices
					$arrobjPastDueInvoices = CInvoices::createService()->fetchPastDuesInvoicesByCidsByAsOfDate( [ $objInvoice->getCid() ], $objInvoice->getInvoiceDate(), $objAdminDatabase, $objInvoice->getAccountId() );

					if( true == valArr( $arrobjPastDueInvoices ) ) {
						$objInvoice->setPastDueInvoices( $arrobjPastDueInvoices );
					}
				}

				$strHtmlContent = $objInvoice->buildHtmlInvoice( $objAdminDatabase, $objUtilitiesDatabase );

				$strFileName = tempnam( CONFIG_CACHE_DIRECTORY, 'pdf' );
				CFileIo::filePutContents( $strFileName, $strHtmlContent );
				$arrstrInvoiceFilePaths[] = $strFileName;
			}
		} else {

			// If invoices are found, Adding blank page in the PDF
			$strFileName = tempnam( CONFIG_CACHE_DIRECTORY, 'pdf' );
			CFileIo::filePutContents( $strFileName, '' );
			$arrstrInvoiceFilePaths[] = $strFileName;
		}

		$strFolderName			= 'bulk_invoices_';
		$strFileName			= date( 'Y-m-d' ) . '-' . CONFIG_COMPANY_NAME . '-Invoices.pdf';

		$strAttachmentFilePath = CObjectStorageUtils::getTemporaryPath( 'invoices', $strFolderName . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' );

		$strAttachmentFile		= $strAttachmentFilePath . $strFileName;

		$objPdfDocument = CPrinceFactory::createPrince();
		$objPdfDocument->setBaseURL( PATH_COMMON );
		$objPdfDocument->setHTML( true );
		$objPdfDocument->setJavaScript( true );

		$arrstrErrorMsgs = [];
		$objPdfDocument->convert_multiple_files( $arrstrInvoiceFilePaths, $strAttachmentFile, $arrstrErrorMsgs );

		foreach( $arrstrInvoiceFilePaths as $strFile ) {
			unlink( realpath( $strFile ) );
		}

		return $strAttachmentFile;
	}

	public function updateInvoiceBatchProcessDetails( $intCurrentUserId, $intInvoiceBatchProcessId, $arrmixDetails ) {

		/** @var CDatabase $objDatabase */
		$objDatabase = $this->getDatabase();

		// have to refresh details and then update in a transaction in case other processes finished since this one began
		if( true == $objDatabase->getIsTransaction() ) {
			$strTrace = '';
			if( $arrmixBacktrace = debug_backtrace() ) {
				foreach( $arrmixBacktrace as $intTraceId => $arrstrCall ) {
					if( array_key_exists( 'file', $arrstrCall ) ) {
						$strTrace .= '#    ' . ( int ) $intTraceId . ' - ' . $arrstrCall['file'] . ' (' . $arrstrCall['line'] . ')' . "\n";
					} else {
						$strTrace .= '#    ' . ( int ) $intTraceId . ' - System' . "\n";
					}
				}
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Transaction already started. Cannot update invoice_batch_process_details' . $strTrace ) );
			return false;
		}

		$objDatabase->begin();

		$this->refreshInvoiceBatchProcessDetails( $objDatabase );
		$jsonInvoiceBatchProcessDetails = $this->getInvoiceBatchProcessDetails();

		if( false == valObj( $jsonInvoiceBatchProcessDetails, 'stdClass' ) ) {
			$jsonInvoiceBatchProcessDetails = new stdClass();
		}

		$arrmixDetails['id'] = $intInvoiceBatchProcessId;

		if( false == isset( $jsonInvoiceBatchProcessDetails->$intInvoiceBatchProcessId ) ) {
			$jsonInvoiceBatchProcessDetails->$intInvoiceBatchProcessId = new stdClass();
		}

		foreach( $arrmixDetails as $strKey => $strValue ) {
			$jsonInvoiceBatchProcessDetails->$intInvoiceBatchProcessId->$strKey = $strValue;
		}

		$this->setInvoiceBatchProcessDetails( $jsonInvoiceBatchProcessDetails );
		$this->update( $intCurrentUserId, $objDatabase );

		return ( false !== $objDatabase->commit() );
	}

	/**
	 * Refreshes invoice_batch_process_details field using FOR UPDATE to prevent concurrent writes
	 * @param $objDatabase CDatabase
	 */
	public function refreshInvoiceBatchProcessDetails( $objDatabase ) {
		$arrmixDetails = fetchData( 'SELECT invoice_batch_process_details FROM invoice_batches WHERE id = ' . $this->getId() . ' FOR UPDATE;', $objDatabase );

		if( true == isset( $arrmixDetails[0]['invoice_batch_process_details'] ) ) {
			$this->setInvoiceBatchProcessDetails( $arrmixDetails[0]['invoice_batch_process_details'] );
		}
	}

}
?>
