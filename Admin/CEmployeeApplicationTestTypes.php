<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationTestTypes
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationTestTypes extends CBaseEmployeeApplicationTestTypes {

	public static function fetchEmployeeApplicationTestTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmployeeApplicationTestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEmployeeApplicationTestType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmployeeApplicationTestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>