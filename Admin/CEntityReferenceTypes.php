<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEntityReferenceTypes
 * Do not add any new functions to this class.
 */

class CEntityReferenceTypes extends CBaseEntityReferenceTypes {

	public static function fetchEntityReferenceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEntityReferenceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEntityReferenceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEntityReferenceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>