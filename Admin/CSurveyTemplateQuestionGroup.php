<?php

class CSurveyTemplateQuestionGroup extends CBaseSurveyTemplateQuestionGroup {

	protected $m_arrobjSurveyTemplateQuestions;

	const PROTOCOL_ADHERENCE								= 1;
	const SOFT_SKILLS										= 2;
	const LEASING_SKILLS									= 3;
	const STANDARD_PROTOCOL_ADHERENCE						= 51;
	const STANDARD_LEASING_SKILLS							= 52;
	const STANDARD_SOFT_SKILLS								= 53;
	const BASIC_EXPECTATIONS								= 150;
	const QUALITY_OF_INTERACTION							= 151;
	const DISQUALIFIERS										= 152;
	const STANDARD_BASIC_EXPECTATIONS						= 154;
	const STANDARD_QUALITY_OF_INTERACTION					= 155;
	const STANDARD_DISQUALIFIERS							= 156;
	const OVERALL_LEASING_CENTER_QA_SURVEY_RESULT			= 168;
	const OVERALL_LEASING_CENTER_QA_STANDARD_SURVEY_RESULT	= 169;

	// Support QA Survey Template Question Groups we may need to update these once finalized.
	const GREETING											= 216;
	const TICKET_CREATION									= 217;
	const ESCALATED_TICKET									= 218;
	const CALL_FLOW											= 219;
	const ETIQUETTE											= 220;
	const TROUBLESHOOTING_AND_ACCURACY						= 221;
	const TRANSFERS_WITHIN_SUPPORT							= 222;
	const CLOSURE											= 223;
	const AUTO_FAILS										= 224;
	const STANDARD_GREETING									= 226;
	const STANDARD_TICKET_CREATION							= 227;
	const STANDARD_ESCALATED_TICKET							= 228;
	const STANDARD_CALL_FLOW								= 229;
	const STANDARD_ETIQUETTE								= 230;
	const STANDARD_TROUBLESHOOTING_AND_ACCURACY				= 231;
	const STANDARD_TRANSFERS_WITHIN_SUPPORT					= 232;
	const STANDARD_CLOSURE									= 233;
	const STANDARD_AUTO_FAILS								= 234;
	const PROTOCOL											= 244;
	const SOFTSKILLS										= 245;
	const ESCALATED_TASKS									= 246;
	const RESOLVED_TASKS									= 247;
	const SUPPORT_TASK_QA_AUTO_FAILS						= 248;
	const STANDARD_PROTOCOL									= 249;
	const STANDARD_SOFTSKILLS								= 250;
	const STANDARD_ESCALATED_TASKS							= 251;
	const STANDARD_RESOLVED_TASKS							= 252;
	const STANDARD_SUPPORT_TASK_QA_AUTO_FAILS				= 253;

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Question group name is required.' ) );
		} else {
			$intConflictingSurveyTemplateQuestionGroupCount = CSurveyTemplateQuestionGroups::fetchConflictingSurveyTemplateQuestionGroupBySurveyTemplateIdByName( $this->getSurveyTemplateId(), $this->sqlName(), $this->getId(), $objDatabase );

			if( 0 < $intConflictingSurveyTemplateQuestionGroupCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Group name already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function getSurveyTemplateQuestions() {
		return $this->m_arrobjSurveyTemplateQuestions;
	}

	public function addSurveyTemplateQuestion( $objSurveyTemplateQuestion ) {
		$this->m_arrobjSurveyTemplateQuestions[$objSurveyTemplateQuestion->getId()] = $objSurveyTemplateQuestion;
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intUserId, $objDatabase, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase );
		}

		return $this->update( $intUserId, $objDatabase );
	}

	/**
	 * fetch Functions
	 */

	public function fetchSurveyTemplateQuestions( $objDatabase ) {
		return CSurveyTemplateQuestions::fetchSurveyTemplateQuestionsBySurveyTemplateQuestionGroupIdBySurveyTemplateId( $this->getId(), $this->getSurveyTemplateId(), $objDatabase );
	}

}
?>