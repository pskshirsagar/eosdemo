<?php

class CEmployeeAssessmentType extends CBaseEmployeeAssessmentType {

	protected $m_strEmployeeAssessmentTypeLevelName;

	const US_THREE_MONTHS 						= 1;
	const US_SIX_MONTHS							= 2;
	const IT_QUARTERLY_REVIEW					= 4;
	const IND_THREE_MONTHS 						= 6;
	const IND_SIX_MONTHS 						= 7;
	const IND_APRIL_OR_OCTOBER 					= 9;
	const IND_OCTOBER_TL_OR_TPM_MONTHS 			= 10;
	const US_OCTOBER_2013 						= 69;
	const DEVELOPER_CONFIRMATORY 				= 87;
	const QA_CONFIRMATORY 						= 88;
	const GENRAL_CONFIRMATORY					= 90;
	const EXIT_INTERVIEW	 					= 97;
	const TRAINEE_DEVELOPER_CONFIRMATORY_REVIEW = 189;
	const CONFIRMATORY_REVIEW_EXTENSION	    	= 196;

	const ROLE_CHANGE_REVIEW_360 				= 200;

	const MANAGER_REQUEST_EMPLOYEE_TERMINATE	= 244;

	public static $c_arrintConfirmatoryReviewTypes 	= array(
		self::DEVELOPER_CONFIRMATORY,
		self::QA_CONFIRMATORY,
		self::GENRAL_CONFIRMATORY,
		Self::CONFIRMATORY_REVIEW_EXTENSION,
		self::TRAINEE_DEVELOPER_CONFIRMATORY_REVIEW
	);

	public static $c_arrintExcludeEmployeeAssessmentTypesForReviewStatus = [
		self::QA_CONFIRMATORY,
		self::TRAINEE_DEVELOPER_CONFIRMATORY_REVIEW,
		self::EXIT_INTERVIEW
	];

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['employee_assessment_type_level_name'] ) ) 	$this->setEmployeeAssessmentTypeLevelName( trim( $arrmixValues['employee_assessment_type_level_name'] ) );

		return;
	}

	public function setEmployeeAssessmentTypeLevelName( $strEmployeeAssessmentTypeLevelName ) {
		$this->m_strEmployeeAssessmentTypeLevelName = $strEmployeeAssessmentTypeLevelName;
	}

	/**
	 * Get Functions
	 */

	public function getEmployeeAssessmentTypeLevelName() {
		return $this->m_strEmployeeAssessmentTypeLevelName;
	}

	/**
	 * Create Functions
	 */

	public function createEmployeeAssessmentQuestion() {

		$objEmployeeAssessmentQuestion = new CEmployeeAssessmentQuestion();

		$objEmployeeAssessmentQuestion->setEmployeeAssessmentTypeId( $this->getId() );

		return $objEmployeeAssessmentQuestion;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPublishedEmployeeAssessmentQuestions( $objDatabase ) {
		return \Psi\Eos\Admin\CEmployeeAssessmentQuestions::createService()->fetchPublishedEmployeeAssessmentQuestionsByEmployeeAssessmentTypeId( $this->getId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeAssessmentTypeLevelId() {
		$boolIsValid = true;

		if( 0 >= $this->getEmployeeAssessmentTypeLevelId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_assessment_type_level_id', 'Category is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCountryCode( $strCountryCode ) {
		$boolIsValid = true;

		if( true == is_null( $strCountryCode ) || false == valArr( $strCountryCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', 'Country is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeDueDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getEmployeeDueDate() ) && false == is_null( $this->getManagerDueDate() ) && strtotime( $this->getEmployeeDueDate() ) > strtotime( $this->getManagerDueDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_due_date', ' Employee Finalized On date should be less than Manager Finalized On date.' ) );
		}

		return $boolIsValid;
	}

	public function valManagerDueDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getCompletionDueDate() ) && false == is_null( $this->getManagerDueDate() ) && $this->getManagerDueDate() > $this->getCompletionDueDate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Manager_due_date', ' Manager Finalized On date should be less than Completion On.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $strCountryCode ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCountryCode( $strCountryCode );
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valEmployeeAssessmentTypeLevelId();
				$boolIsValid &= $this->valEmployeeDueDate();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valName();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Delete Functions
	*/

	public function delete( $intCurrenUserId, $objDataBase, $boolReturnSqlOnly = false ) {

		$this->setIsPublished( 0 );
		$this->setDeletedBy( $intCurrenUserId );
		$this->setDeletedOn( 'NOW()' );

		if( $this->update( $intCurrenUserId, $objDataBase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

}
?>