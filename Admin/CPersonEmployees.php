<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPersonEmployees
 * Do not add any new functions to this class.
 */

class CPersonEmployees extends CBasePersonEmployees {

	public static function fetchPersonEmployeeByPersonIdByEmployeeId( $intPersonId, $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT *
					FROM
						person_employees
					WHERE
						person_id = ' . ( int ) $intPersonId . '
					AND
						employee_id = ' . ( int ) $intEmployeeId . '
					LIMIT 1';

		return self::fetchPersonEmployee( $strSql, $objDatabase );

	}

	public static function fetchConflictingPersonEmployeeCountByPersonEmployee( $objPersonEmployee, $objDatabase ) {

		$strWhereSql = ' WHERE
						person_id = ' . ( int ) $objPersonEmployee->getPersonId() . '
						AND employee_id = ' . ( int ) $objPersonEmployee->getEmployeeId() . '
						AND id <> ' . ( int ) $objPersonEmployee->getId() . '
					LIMIT 1';

		return self::fetchPersonEmployeeCount( $strWhereSql, $objDatabase );
	}

	public static function fetchCustomPersonEmployeesByPersonId( $intPersonId, $objDatabase ) {
		return self::fetchPersonEmployees( sprintf( 'SELECT * FROM person_employees WHERE person_id = %d', ( int ) $intPersonId ), $objDatabase );
	}

	public static function fetchPersonEmployeesByPersonIds( $arrintPersonId, $objDatabase ) {

		if( false == valArr( $arrintPersonId ) ) return NULL;

		$strsql = 'SELECT * FROM person_employees WHERE person_id IN ( ' . implode( ',', $arrintPersonId ) . ' )';
		return self::fetchPersonEmployees( $strsql, $objDatabase );
	}
}
?>