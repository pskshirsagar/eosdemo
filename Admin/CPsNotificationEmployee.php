<?php

class CPsNotificationEmployee extends CBasePsNotificationEmployee {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function updatePsNotifications( $intEmployeeId, $objDatabase, $arrintNotificationIds = NULL ) {

		$strSql = ' UPDATE
						public.ps_notification_employees
					SET
						accessed_on = NOW()
					WHERE
						accessed_on IS NULL
						AND employee_id = ' . ( int ) $intEmployeeId;

		if( false == is_null( $arrintNotificationIds ) && true == valArr( $arrintNotificationIds ) ) {
			$strSql .= ' AND ps_notification_id IN ( ' . implode( ',', $arrintNotificationIds ) . ' );';
		} elseif( false == is_null( $arrintNotificationIds ) && true == valStr( $arrintNotificationIds ) ) {
			$strSql .= ' AND ps_notification_id IN ( ' . $arrintNotificationIds . ' );';
		}

		return $this->executeSql( $strSql, $this, $objDatabase );
	}
}
?>