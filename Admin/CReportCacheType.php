<?php

class CReportCacheType extends CBaseReportCacheType {

	const NEVER			= 1;
	const DAILY			= 2;
	const WEEKLY		= 3;
	const MONTHLY		= 4;
	const EVERYDAY		= 1;
	const WEEK_DAYS		= 7;
	const MONTH_DAYS	= 30;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>