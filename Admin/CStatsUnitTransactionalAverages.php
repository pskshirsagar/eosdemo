<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsUnitTransactionalAverages
 * Do not add any new functions to this class.
 */

class CStatsUnitTransactionalAverages extends CBaseStatsUnitTransactionalAverages {

    /**
     * @param CDatabase $objAdminDatabase
     */
    public static function rebuildStatsUnitTransactionalAverages( $objAdminDatabase ) {
        set_time_limit( 300 );

        $strSql = '
            CREATE SEQUENCE IF NOT EXISTS stats_unit_transactional_averages_id_seq START 1;
            ALTER SEQUENCE stats_unit_transactional_averages_id_seq RESTART WITH 1;

            DROP TABLE IF EXISTS stats_unit_transactional_averages_tmp CASCADE;

            CREATE TABLE stats_unit_transactional_averages_tmp (
                id INTEGER DEFAULT NEXTVAL( \'stats_unit_transactional_averages_id_seq\' ) CONSTRAINT pk_stats_unit_transactional_averages_tmp_id PRIMARY KEY,
                property_portfolio_type_id INTEGER,
                property_portfolio_type VARCHAR( 50 ),
                ps_product_id INTEGER,
                product_name VARCHAR( 50 ),
                month DATE,
                transactional_revenue INTEGER,
                total_units INTEGER,
                total_units_weighted INTEGER,
                unit_transactional_average_by_month NUMERIC,
                unit_transactional_average NUMERIC,
                created_by INTEGER DEFAULT 1,
                created_on TIMESTAMPTZ DEFAULT NOW()
            );

            INSERT INTO stats_unit_transactional_averages_tmp (
                property_portfolio_type_id,
                property_portfolio_type,
                ps_product_id,
                product_name,
                month,
                transactional_revenue,
                total_units,
                total_units_weighted,
                unit_transactional_average_by_month,
                unit_transactional_average
            )

            WITH unit_transactional_averages AS (
                SELECT
                    sub.property_portfolio_type_id::INTEGER,
                    sub.property_portfolio_type::VARCHAR( 50 ),
                    sub.ps_product_id::INTEGER,
                    sub.product_name::VARCHAR( 50 ),
                    sub.month::DATE,
                    sub.transactional_revenue::INTEGER AS transactional_revenue,
                    sub.total_units::INTEGER AS total_units,
                    sub.total_units_weighted::INTEGER AS total_units_weighted,
                    sub.unit_transactional_average_by_month::NUMERIC( 16, 2 ) AS unit_transactional_average_by_month,
                    sub.starting_unit_transactional_average::NUMERIC(16, 2 )
                FROM
                    (
                    SELECT
                        sub2.property_portfolio_type_id,
                        sub2.property_portfolio_type,
                        sub2.ps_product_id,
                        sub2.product_name,
                        sub2.month,
                        COALESCE( SUM( sr2.transactional_revenue ), 0 ) AS transactional_revenue,
                        COALESCE( SUM( sub2.total_units ), 0 ) AS total_units,
                        COALESCE( SUM( sub2.total_units_weighted ), 0 ) AS total_units_weighted,
                        CASE WHEN COALESCE( SUM( sr2.transactional_revenue ), 0 ) > 0 AND COALESCE( SUM( sub2.total_units_weighted ), 0 ) >= 5000 THEN COALESCE( SUM( sr2.transactional_revenue ), 0 )::NUMERIC / COALESCE( NULLIF( SUM( sub2.total_units_weighted ), 0 ), 1 )::NUMERIC ELSE 0 END AS unit_transactional_average_by_month,
                        sub2.starting_unit_transactional_average
                    FROM
                        (
                        SELECT
                            sc3.property_portfolio_type_id,
                            sc3.property_portfolio_type,
                            sc3.cid,
                            sc3.property_id,
                            pp3.id AS ps_product_id,
                            pp3.name AS product_name,
                            d3.date AS month,
                            COALESCE( SUM( CASE WHEN d3.date <= CASE WHEN sc3.contract_property_is_active = 1 AND DATE_TRUNC( \'month\', sc3.contract_property_last_active_date ) = DATE_TRUNC( \'month\', CURRENT_DATE ) THEN DATE_TRUNC( \'month\', CURRENT_DATE ) + INTERVAL \'1 month\' ELSE DATE_TRUNC( \'month\', sc3.contract_property_last_active_date ) END THEN sc3.property_unit_count ELSE 0 END ), 0 ) AS total_units,
                            COALESCE( SUM(
                                CASE
                                    WHEN d3.date = DATE_TRUNC( \'month\', sc3.contract_property_start_date ) THEN sc3.property_unit_count * ( ( DATE_PART( \'days\', d3.date + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) - DATE_PART( \'days\', sc3.contract_property_start_date ) + 1 ) / DATE_PART( \'days\', d3.date + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) )
                                    WHEN d3.date = CASE WHEN sc3.contract_property_is_active = 1 AND DATE_TRUNC( \'month\', sc3.contract_property_last_active_date ) = DATE_TRUNC( \'month\', CURRENT_DATE ) THEN DATE_TRUNC( \'month\', CURRENT_DATE ) + INTERVAL \'1 month\' ELSE DATE_TRUNC( \'month\', sc3.contract_property_last_active_date ) END THEN sc3.property_unit_count * ( ( DATE_PART( \'days\', sc3.contract_property_last_active_date ) ) / DATE_PART( \'days\', d3.date + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) )
                                    ELSE sc3.property_unit_count
                                END
                            ), 0 ) AS total_units_weighted,
                            pp3.starting_unit_transactional_average
                        FROM
                            ps_products AS pp3
                        CROSS JOIN
                            dates AS d3
                        LEFT JOIN
                            stats_contracts AS sc3
                        ON
                            sc3.company_status_type_id IN ( ' . implode( ',', CCompanyStatusType::$c_arrintClosedCompanyStatusTypeIds ) . ' ) AND
                            sc3.contract_status_type_id IN ( ' . implode( ',', CContractStatusType::$c_arrintCompletedContractStatusTypeIds ) . ' ) AND
                            COALESCE( close_date, \'2099-12-31\' ) <= CURRENT_DATE AND
                            sc3.is_last_contract_property_record = 1 AND
                            sc3.ps_product_id = pp3.id AND
                            d3.date BETWEEN DATE_TRUNC( \'month\', sc3.contract_property_start_date ) AND DATE_TRUNC( \'month\', sc3.contract_property_last_active_date )
                        WHERE
                            pp3.is_transactional = 1 AND
                            d3.is_first = 1 AND
                            d3.date BETWEEN \'2003-01-01\' AND CURRENT_DATE
                        GROUP BY
                            sc3.property_portfolio_type_id,
                            sc3.property_portfolio_type,
                            sc3.cid,
                            sc3.property_id,
                            pp3.id,
                            pp3.name,
                            d3.date,
                            pp3.unit_transactional_average,
                            pp3.starting_unit_transactional_average
                        HAVING
                            d3.date >= LEAST( DATE_TRUNC( \'month\', pp3.created_on ), MIN( sc3.contract_property_start_date ) )
                        UNION ALL
                            SELECT
                                0 AS property_portfolio_type_id,
                                \'All\' AS property_portfolio_type,
                                sc3.cid,
                                sc3.property_id,
                                pp3.id AS ps_product_id,
                                pp3.name AS product_name,
                                d3.date AS month,
                                COALESCE( SUM( CASE WHEN d3.date <= CASE WHEN sc3.contract_property_is_active = 1 AND DATE_TRUNC( \'month\', sc3.contract_property_last_active_date ) = DATE_TRUNC( \'month\', CURRENT_DATE ) THEN DATE_TRUNC( \'month\', CURRENT_DATE ) + INTERVAL \'1 month\' ELSE DATE_TRUNC( \'month\', sc3.contract_property_last_active_date ) END THEN sc3.property_unit_count ELSE 0 END ), 0 ) AS total_units,
                                COALESCE( SUM(
                                    CASE
                                        WHEN d3.date = DATE_TRUNC( \'month\', sc3.contract_property_start_date ) THEN sc3.property_unit_count * ( ( DATE_PART( \'days\', d3.date + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) - DATE_PART( \'days\', sc3.contract_property_start_date ) + 1 ) / DATE_PART( \'days\', d3.date + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) )
                                        WHEN d3.date = CASE WHEN sc3.contract_property_is_active = 1 AND DATE_TRUNC( \'month\', sc3.contract_property_last_active_date ) = DATE_TRUNC( \'month\', CURRENT_DATE ) THEN DATE_TRUNC( \'month\', CURRENT_DATE ) + INTERVAL \'1 month\' ELSE DATE_TRUNC( \'month\', sc3.contract_property_last_active_date ) END THEN sc3.property_unit_count * ( ( DATE_PART( \'days\', sc3.contract_property_last_active_date ) ) / DATE_PART( \'days\', d3.date + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) )
                                        ELSE sc3.property_unit_count
                                    END
                                ), 0 ) AS total_units_weighted,
                                pp3.starting_unit_transactional_average
                            FROM
                                ( SELECT DISTINCT property_portfolio_type FROM stats_contracts ) AS ppt3
                            CROSS JOIN
                                ps_products AS pp3
                            CROSS JOIN
                                dates AS d3
                            LEFT JOIN
                                stats_contracts AS sc3
                            ON
                                sc3.company_status_type_id IN ( ' . implode( ',', CCompanyStatusType::$c_arrintClosedCompanyStatusTypeIds ) . ' ) AND
                                sc3.contract_status_type_id IN ( ' . implode( ',', CContractStatusType::$c_arrintCompletedContractStatusTypeIds ) . ' ) AND
                                COALESCE( close_date, \'2099-12-31\' ) <= CURRENT_DATE AND
                                sc3.is_last_contract_property_record = 1 AND
                                sc3.property_portfolio_type = ppt3.property_portfolio_type AND
                                sc3.ps_product_id = pp3.id AND
                                d3.date BETWEEN DATE_TRUNC( \'month\', sc3.contract_property_start_date ) AND DATE_TRUNC( \'month\', sc3.contract_property_last_active_date )
                            WHERE
                                pp3.is_transactional = 1 AND
                                d3.is_first = 1 AND
                                d3.date BETWEEN \'2003-01-01\' AND CURRENT_DATE
                            GROUP BY
                                sc3.cid,
                                sc3.property_id,
                                pp3.id,
                                pp3.name,
                                d3.date,
                                pp3.unit_transactional_average,
                                pp3.starting_unit_transactional_average
                            HAVING
                                d3.date >= LEAST( DATE_TRUNC( \'month\', pp3.created_on ), MIN( sc3.contract_property_start_date ) )
                        ) AS sub2
                    LEFT JOIN
                        (
                        SELECT
                            sr3.cid,
                            sr3.property_id,
                            sr3.ps_product_id,
                            COALESCE( sr3.export_batch_month, sr3.transaction_month ) AS month,
                            SUM( sr3.total_amount ) AS transactional_revenue
                        FROM
                            stats_revenues AS sr3
                        WHERE
                            sr3.revenue_type_id = ' . CRevenueType::TRANSACTIONAL . '
                        GROUP BY
                            sr3.cid,
                            sr3.property_id,
                            sr3.ps_product_id,
                            COALESCE( sr3.export_batch_month, sr3.transaction_month )
                        ) AS sr2
                    ON
                        sr2.cid = sub2.cid AND
                        sr2.property_id = sub2.property_id AND
                        sr2.ps_product_id = sub2.ps_product_id AND
                        sr2.month = sub2.month
                    GROUP BY
                        sub2.property_portfolio_type_id,
                        sub2.property_portfolio_type,
                        sub2.ps_product_id,
                        sub2.product_name,
                        sub2.month,
                        sub2.starting_unit_transactional_average
                    ORDER BY
                        sub2.property_portfolio_type_id,
                        sub2.property_portfolio_type,
                        sub2.ps_product_id,
                        sub2.month
                    ) AS sub
                ORDER BY
                    sub.property_portfolio_type_id,
                    sub.ps_product_id,
                    sub.month
            )
            SELECT
                sub.property_portfolio_type_id,
                sub.property_portfolio_type,
                sub.ps_product_id,
                sub.product_name,
                sub.month,
                sub.transactional_revenue,
                sub.total_units,
                sub.total_units_weighted,
                sub.unit_transactional_average_by_month,
                COALESCE( NULLIF( SUM( sub.sub_unit_transactional_average_by_month * sub.sub_weight ), 0 ), NULLIF( sub.unit_transactional_average_by_month, 0 ), NULLIF( FIRST_VALUE( sub.unit_transactional_average_by_month ) OVER ( PARTITION BY sub.property_portfolio_type_id, sub.ps_product_id ORDER BY sub.unit_transactional_average_by_month = 0, sub.month ), 0 ), sub.starting_unit_transactional_average, 0 )::NUMERIC( 16, 2 ) AS unit_transactional_average
            FROM
                (
                SELECT
                    sub2.property_portfolio_type_id,
                    sub2.property_portfolio_type,
                    sub2.ps_product_id,
                    sub2.product_name,
                    sub2.month,
                    sub2.transactional_revenue,
                    sub2.total_units,
                    sub2.total_units_weighted,
                    sub2.unit_transactional_average_by_month,
                    sub2.starting_unit_transactional_average,
                    sub2.sub_month,
                    sub2.sub_unit_transactional_average_by_month,
                    sub2.sub_month_count,
                    sub2.sub_month_number,
                    ( sub2.sub_month_number )::NUMERIC / COALESCE( NULLIF( ( sub2.sub_month_count::NUMERIC / 2::NUMERIC ) * ( 1 + sub2.sub_month_count ), 0 ), 1 )::NUMERIC AS sub_weight
                FROM
                    (
                    SELECT
                        uta1.property_portfolio_type_id,
                        uta1.property_portfolio_type,
                        uta1.ps_product_id,
                        uta1.product_name,
                        uta1.month,
                        uta1.transactional_revenue,
                        uta1.total_units,
                        uta1.total_units_weighted,
                        uta1.unit_transactional_average_by_month,
                        uta1.starting_unit_transactional_average,
                        uta2.month AS sub_month,
                        uta2.unit_transactional_average_by_month AS sub_unit_transactional_average_by_month,
                        COUNT( uta2.unit_transactional_average_by_month ) OVER ( PARTITION BY uta1.property_portfolio_type_id, uta1.month, uta2.ps_product_id ) AS sub_month_count,
                        ROW_NUMBER( ) OVER ( PARTITION BY uta1.property_portfolio_type_id, uta1.month, uta1.ps_product_id ORDER BY uta1.month ) AS sub_month_number
                    FROM
                        unit_transactional_averages AS uta1
                    LEFT JOIN
                        unit_transactional_averages AS uta2
                    ON
                        uta2.property_portfolio_type_id = uta1.property_portfolio_type_id AND
                        uta2.ps_product_id = uta1.ps_product_id AND
                        uta2.month BETWEEN uta1.month - INTERVAL \'12 months\' AND uta1.month - INTERVAL \'1 month\' AND
                        uta2.unit_transactional_average_by_month != 0
                    ORDER BY
                        uta1.property_portfolio_type_id,
                        uta1.property_portfolio_type,
                        uta1.ps_product_id,
                        uta1.month,
                        uta2.month
                    ) AS sub2
                ) AS sub
            GROUP BY
                sub.property_portfolio_type_id,
                sub.property_portfolio_type,
                sub.ps_product_id,
                sub.product_name,
                sub.month,
                sub.transactional_revenue,
                sub.total_units,
                sub.total_units_weighted,
                sub.unit_transactional_average_by_month,
                sub.starting_unit_transactional_average
            ORDER BY
                sub.property_portfolio_type_id,
                sub.property_portfolio_type,
                sub.ps_product_id,
                sub.month;

            CREATE INDEX idx_stats_unit_transactional_averages_tmp_property_portfolio_type_id ON stats_unit_transactional_averages_tmp ( property_portfolio_type_id );
            CREATE INDEX idx_stats_unit_transactional_averages_tmp_ps_product_id ON stats_unit_transactional_averages_tmp ( ps_product_id );
            CREATE INDEX idx_stats_unit_transactional_averages_tmp_month ON stats_unit_transactional_averages_tmp ( month );

            GRANT SELECT, INSERT, UPDATE, DELETE ON public.stats_unit_transactional_averages_tmp TO "Administrators", "Developers";
            GRANT SELECT ON public.stats_unit_transactional_averages_tmp TO "Viewers";
            GRANT SELECT, UPDATE ON public.stats_unit_transactional_averages_id_seq TO "Administrators", "Developers";
        ';

        $objAdminDatabase->open();

        fetchData( $strSql, $objAdminDatabase );

        $objAdminDatabase->begin();

        $strSql = '
            
            ALTER TABLE IF EXISTS stats_unit_transactional_averages RENAME TO stats_unit_transactional_averages_old;
            ALTER TABLE stats_unit_transactional_averages_tmp RENAME TO stats_unit_transactional_averages;

            ALTER SEQUENCE stats_unit_transactional_averages_id_seq OWNED BY stats_unit_transactional_averages.id;

            DROP TABLE IF EXISTS stats_unit_transactional_averages_old;

            ALTER INDEX pk_stats_unit_transactional_averages_tmp_id RENAME TO pk_stats_unit_transactional_averages_id;
            ALTER INDEX idx_stats_unit_transactional_averages_tmp_property_portfolio_type_id RENAME TO idx_stats_unit_transactional_averages_property_portfolio_type_id;
            ALTER INDEX idx_stats_unit_transactional_averages_tmp_ps_product_id RENAME TO idx_stats_unit_transactional_averages_ps_product_id;
            ALTER INDEX idx_stats_unit_transactional_averages_tmp_month RENAME TO idx_stats_unit_transactional_averages_month;
        ';

        if( false !== fetchData( $strSql, $objAdminDatabase ) ) {
            $objAdminDatabase->commit();
        } else {
            $objAdminDatabase->rollback();
        }

        $objAdminDatabase->close();
    }

}
?>
