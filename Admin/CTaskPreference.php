<?php

class CTaskPreference extends CBaseTaskPreference {

	const TASK_PREFERENCE_PII_KEY = 'IS_CONTAINS_PII';
	  // 0 = To set PII flag false to task
	  // 1 = To set the flag true to task
	  // 2 = To set the PII flag freeze for task after CPIIContainsTaskUpdateScript.class.php script execution.
	const TASK_PREFERENCE_PII_KEY_VALUE_FOR_FREEZE_FLAG = 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>