<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemPropertyDetails
 * Do not add any new functions to this class.
 */

class CSemPropertyDetails extends CBaseSemPropertyDetails {

	public static function fetchSemPropertyDetailByPropertyId( $intPropertyId, $objDatabase ) {
		$strSql = 'SELECT * FROM sem_property_details WHERE property_id = ' . ( int ) $intPropertyId . ' LIMIT 1';
		return self::fetchSemPropertyDetail( $strSql, $objDatabase );
	}

	public static function fetchSemPropertyDetailsByPropertyIds( $arrintPropertyIds, $objAdminDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT * FROM sem_property_details WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchSemPropertyDetails( $strSql, $objAdminDatabase );
	}

}
?>