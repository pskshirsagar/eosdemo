<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CKpiTypes
 * Do not add any new functions to this class.
 */

class CKpiTypes extends CBaseKpiTypes {

	public static function fetchKpiTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CKpiType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchKpiType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CKpiType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>