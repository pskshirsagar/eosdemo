<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationDeclineReasons
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationDeclineReasons extends CBaseEmployeeApplicationDeclineReasons {

	public static function fetchEmployeeApplicationDeclineReasonsByCountryCode( $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						employee_application_decline_reasons
					WHERE
						is_published = true
						AND country_code = \'' . $strCountryCode . '\'
					ORDER BY
						name';

		return parent::fetchEmployeeApplicationDeclineReasons( $strSql, $objDatabase );
	}

}
?>