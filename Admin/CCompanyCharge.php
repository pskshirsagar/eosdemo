<?php

class CCompanyCharge extends CBaseCompanyCharge {

	use TEosDetails;

	const ALL		= 1;
	const CURRENT	= 2;
	const FUTURE	= 3;
	const PAST		= 4;
	const PAUSED	= 5;
	const DELETED	= 6;
	const ACTIVE	= 7;
	const ENABLED	= 8;

	protected $m_objClient;

	protected $m_intEntityId;
	protected $m_intAssociationCount;
	protected $m_intPsProductId;
	protected $m_intPropertyId;
	protected $m_intContractPropertyId;
	protected $m_intNumberOfUnits;
	protected $m_intPricingUpdateCounts;
    protected $m_intContractId;
	protected $m_fltAssociatedContractTotal;
	protected $m_fltMappedAmount;
	protected $m_fltRepriceAmount;

	protected $m_strAccountName;
	protected $m_strChargeCodeName;
	protected $m_strPsProductName;
	protected $m_strPropertyName;
	protected $m_strPilotDueDate;
	protected $m_strCloseDate;
	protected $m_strRepriceDate;
	protected $m_strCurrencyCode;

	/**
	*Create Functions
	*
	*/

	public function createTransaction( $intNextCompanyChargePostDate ) {

		$objTransaction = new CTransaction();

		$objTransaction->setEntityId( $this->getEntityId() );
		$objTransaction->setCid( $this->getCid() );
		$objTransaction->setAccountId( $this->getAccountId() );
		$objTransaction->setChargeCodeId( $this->getChargeCodeId() );
		$objTransaction->setPropertyId( $this->getPropertyId() );
		$objTransaction->setMemo( $this->getChargeMemo() );
		$objTransaction->setCompanyChargeId( $this->getId() );
		$objTransaction->setTransactionDatetime( date( 'm/d/Y', $intNextCompanyChargePostDate ) );
		$objTransaction->setTransactionAmount( $this->getChargeAmount() );

		return $objTransaction;
	}

	/**
	*Get Functions
	*
	**/

	public function getClient() {
		return $this->m_objClient;
	}

	public function getCid() {
		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient->getId();
		} else {
			return parent::getCid();
		}
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function getAssociationCount() {
		return $this->m_intAssociationCount;
	}

	public function getAssociatedContractTotal() {
		return $this->m_fltAssociatedContractTotal;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getChargeCodeName() {
		return $this->m_strChargeCodeName;
	}

	public function getPsProductName() {
		return $this->m_strPsProductName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getContractPropertyId() {
		return $this->m_intContractPropertyId;
	}

	public function getMappedAmount() {
		return $this->m_fltMappedAmount;
	}

	public function getCloseDate() {
		return $this->m_strCloseDate;
	}

	public function getPilotDueDate() {
		return $this->m_strPilotDueDate;
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function getRepriceDate() {
		return $this->m_strRepriceDate;
	}

	public function getRepriceAmount() {
		return $this->m_fltRepriceAmount;
	}

	public function getPricingUpdateCounts() {
		return $this->m_intPricingUpdateCounts;
	}

    public function getContractId() {
        return $this->m_intContractId;
    }

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

        /**
	*Set Functions
	*
	*/

	public function setClient( $objClient ) {
		if( false == valObj( $objClient, 'CClient' ) ) {
			trigger_error( 'Incompatible types: \'CClient\' and \'' . get_class( $objClient ) . '\' - CAccount::setClient()', E_USER_ERROR );
			return false;
		}

		$this->m_objClient = $objClient;
		return true;
	}

	public function setCid( $intCid ) {
		parent::setCid( $intCid );
		// If we are setting the client id directly we unbind the
		// client object to avoid any concurrency issues
		$this->m_objClient = NULL;
	}

	public function setDefaults() {
		$this->setChargeStartDate( date( 'm/d/Y', mktime() ) );
	}

	public function setEntityId( $intEntityId ) {
		$this->m_intEntityId = $intEntityId;
	}

	public function setAssociationCount( $intAssociationCount ) {
		$this->m_intAssociationCount = $intAssociationCount;
	}

	public function setAssociatedContractTotal( $fltAssociatedContractTotal ) {
		$this->m_fltAssociatedContractTotal = $fltAssociatedContractTotal;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = $intPsProductId;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setChargeCodeName( $strChargeCodeName ) {
		$this->m_strChargeCodeName = $strChargeCodeName;
	}

	public function setPsProductName( $strPsProductName ) {
		$this->m_strPsProductName = $strPsProductName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setContractPropertyId( $intContractPropertyId ) {
		$this->m_intContractPropertyId = $intContractPropertyId;
	}

	public function setMappedAmount( $fltMappedAmount ) {
		$this->m_fltMappedAmount = $fltMappedAmount;
	}

	public function setCloseDate( $strCloseDate ) {
		$this->m_strCloseDate = $strCloseDate;
	}

	public function setPilotDueDate( $strPilotDueDate ) {
		$this->m_strPilotDueDate = $strPilotDueDate;
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->m_intNumberOfUnits = $intNumberOfUnits;
	}

	public function setRepriceDate( $strRepriceDate ) {
		$this->m_strRepriceDate = $strRepriceDate;
	}

	public function setRepriceAmount( $fltRepriceAmount ) {
		$this->m_fltRepriceAmount = $fltRepriceAmount;
	}

	public function setPricingUpdateCounts( $intPricingUpdateCounts ) {
		$this->m_intPricingUpdateCounts = $intPricingUpdateCounts;
	}

    public function setContractId( $intContractId ) {
        $this->m_intContractId = ( int ) $intContractId;
    }

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->m_strCurrencyCode = $strCurrencyCode;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['associated_contract_total'] ) ) {
			$this->setAssociatedContractTotal( $arrmixValues['associated_contract_total'] );
		}
		if( true == isset( $arrmixValues['association_count'] ) ) {
			$this->setAssociationCount( $arrmixValues['association_count'] );
		}
		if( true == isset( $arrmixValues['ps_product_id'] ) ) {
			$this->setPsProductId( $arrmixValues['ps_product_id'] );
		}
		if( true == isset( $arrmixValues['account_name'] ) ) {
			$this->setAccountName( $arrmixValues['account_name'] );
		}
		if( true == isset( $arrmixValues['charge_code_name'] ) ) {
			$this->setChargeCodeName( $arrmixValues['charge_code_name'] );
		}
		if( true == isset( $arrmixValues['ps_product_name'] ) ) {
			$this->setPsProductName( $arrmixValues['ps_product_name'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['contract_property_id'] ) ) {
			$this->setContractPropertyId( $arrmixValues['contract_property_id'] );
		}
		if( true == isset( $arrmixValues['mapped_amount'] ) ) {
			$this->setMappedAmount( $arrmixValues['mapped_amount'] );
		}
		if( true == isset( $arrmixValues['close_date'] ) ) {
			$this->setCloseDate( $arrmixValues['close_date'] );
		}
		if( true == isset( $arrmixValues['pilot_due_date'] ) ) {
			$this->setPilotDueDate( $arrmixValues['pilot_due_date'] );
		}
		if( true == isset( $arrmixValues['number_of_units'] ) ) {
			$this->setNumberOfUnits( $arrmixValues['number_of_units'] );
		}
		if( true == isset( $arrmixValues['reprice_date'] ) ) {
			$this->setRepriceDate( $arrmixValues['reprice_date'] );
		}
		if( true == isset( $arrmixValues['reprice_amount'] ) ) {
			$this->setRepriceAmount( $arrmixValues['reprice_amount'] );
		}
		if( true == isset( $arrmixValues['pricing_update_counts'] ) ) {
			$this->setPricingUpdateCounts( $arrmixValues['pricing_update_counts'] );
		}
		if( true == isset( $arrmixValues['contract_id'] ) ) {
			$this->setContractId( $arrmixValues['contract_id'] );
		}
		if( true == isset( $arrmixValues['details'] ) ) {
			$this->setDetails( trim( $arrmixValues['details'] ) );
		}

		if( true == isset( $arrmixValues['currency_code'] ) ) {
			$this->setCurrencyCode( trim( $arrmixValues['currency_code'] ) );
		}
	}

	/**
	*Fetch Functions
	*
	**/

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->getCid(), $objDatabase );
	}

	public function fetchAccount( $objDatabase ) {
		return \Psi\Eos\Admin\CAccounts::createService()->fetchAccountById( $this->getAccountId(), $objDatabase );
	}

	/**
	*Validation Functions
	*
	**/

	public function valAccountId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', 'Please enter an account.' ) );
		}

		return $boolIsValid;
	}

	public function valChargeCodeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intChargeCodeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_id', 'Please select a charge code.' ) );
		}

		return $boolIsValid;
	}

	public function valChargeStartDate() {
		$boolIsValid = true;

		if( false == isset( $this->m_strChargeStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', 'Please enter a charge start date.' ) );
		} else {

			if( false == CValidation::validateDate( $this->m_strChargeStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', 'Invalid charge start date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valChargeEndDate() {
		$boolIsValid = true;

		if( true == isset( $this->m_strChargeEndDate ) && false == CValidation::validateDate( $this->m_strChargeEndDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', 'Invalid charge end date.' ) );
		}

		if( false == is_null( $this->m_strChargeEndDate ) && ( strtotime( $this->m_strChargeEndDate ) < strtotime( $this->m_strChargeStartDate ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', 'Charge end date should be greater than charge start date.' ) );
		}

		return $boolIsValid;
	}

	public function valChargePeriod() {
		$boolIsValid = true;

		if( false == isset( $this->m_intChargePeriod ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_period', 'You must enter a charge period/frequency.' ) );
		}

		return $boolIsValid;
	}

	public function valChargeAmount() {
		$boolIsValid = true;

		if( false == isset( $this->m_fltChargeAmount ) || 0 == $this->m_fltChargeAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_amount', 'Please enter a charge amount.' ) );
		}

		return $boolIsValid;
	}

	public function valChargeMemo() {
		$boolIsValid = true;

		if( false == isset( $this->m_strChargeMemo ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_memo', 'Charge memo is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionStartDate() {
		$boolIsValid = true;

		if( false == isset( $this->m_strCommissionStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_start_date', 'Please enter a commission start date.' ) );
		} else {

			if( false == CValidation::validateDate( $this->m_strCommissionStartDate ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_start_date', 'Invalid commission start date.' ) );
			}

		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valChargePeriod();
				$boolIsValid &= $this->valChargeStartDate();
				$boolIsValid &= $this->valChargeAmount();
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valChargeMemo();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valChargePeriod();
				$boolIsValid &= $this->valChargeStartDate();
				$boolIsValid &= $this->valChargeEndDate();
				$boolIsValid &= $this->valChargeAmount();
				$boolIsValid &= $this->valChargeCodeId();
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valCommissionStartDate();
				$boolIsValid &= $this->valChargeMemo();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	*Other Functions
	*
	**/

	public function sqlChargeEndDate() {
		return  ( true == isset( $this->m_strChargeEndDate ) && 0 < strlen( trim( $this->m_strChargeEndDate ) ) ) ? "'" . $this->m_strChargeEndDate . "'" : 'NULL';
	}

	public function sqlCid() {
		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient->sqlId();
		} else {
			return parent::sqlCid();
		}
	}

	/**
	 *
	 * @param CTransaction $objTransaction
	 */

	public function populateTransaction( $objTransaction ) {
		$objTransaction->setCid( $this->getCid() );
		$objTransaction->setAccountId( $this->getAccountId() );
		$objTransaction->setChargeCodeId( $this->getChargeCodeId() );
		$objTransaction->setCompanyPaymentId( $this->getId() );
		$objTransaction->setTransactionAmount( $this->getPaymentAmount() );
		$objTransaction->setTransactionDatetime( $this->getPaymentDatetime() );
		$objTransaction->setMemo( $this->getPaymentMemo() );
	}

	public function createTransactions( $strInvoiceBatchDateTime = NULL ) {

		if( 1 == $this->m_intIsDisabled ) {
			return [];
		}

		$arrobjTransactions 	= [];
		$intTodayTimeStamp 		= ( false == is_null( $strInvoiceBatchDateTime ) ) ? strtotime( $strInvoiceBatchDateTime ) : strtotime( date( 'm/d/Y' ) );

		if( true == is_null( $this->getLastPostedOn() ) ) {
			$intNextCompanyChargePostDate = strtotime( \Psi\CStringService::singleton()->substr( date( 'm/1/Y', strtotime( $this->m_strChargeStartDate ) ), 0, 11 ) );

		} else {

			$intLastPostedOn				= strtotime( \Psi\CStringService::singleton()->substr( $this->m_strLastPostedOn, 0, 11 ) );
			$intNextCompanyChargePostDate	= strtotime( $this->m_strChargeStartDate );

			switch( $this->getChargePeriod() ) {
				case CFrequency::YEARLY:
					while( $intNextCompanyChargePostDate <= ( $intLastPostedOn ) ) {
						$intNextCompanyChargePostDate = strtotime( '+1 year', $intNextCompanyChargePostDate );
					}
					break;

				case CFrequency::SEMI_ANNUALLY:
					while( $intNextCompanyChargePostDate <= ( $intLastPostedOn ) ) {
						$intNextCompanyChargePostDate = strtotime( '+6 months', $intNextCompanyChargePostDate );
					}
					break;

				case CFrequency::QUARTERLY:
					while( $intNextCompanyChargePostDate <= ( $intLastPostedOn ) ) {
						$intNextCompanyChargePostDate = strtotime( '+3 months', $intNextCompanyChargePostDate );
					}
					break;

				case CFrequency::MONTHLY:
					while( $intNextCompanyChargePostDate <= ( $intLastPostedOn ) ) {
						$intNextCompanyChargePostDate = strtotime( '+1 month', $intNextCompanyChargePostDate );
					}
					break;

				default:
					trigger_error( 'A recurring charge with no charge period was found.' );
					break;
			}
		}

		// If the next date this client should be charged falls after the charge end date, return NULL.
		if( true == isset( $this->m_strChargeEndDate ) && $intNextCompanyChargePostDate > strtotime( \Psi\CStringService::singleton()->substr( $this->m_strChargeEndDate, 0, 11 ) ) ) {
			return [];
		}

		// If the next date this client should be charged falls after the current date, return NULL.
		if( $intNextCompanyChargePostDate > $intTodayTimeStamp ) {
			return [];
		}

		// If the current date is less than the begin date, return null.
		if( $intTodayTimeStamp < strtotime( \Psi\CStringService::singleton()->substr( $this->m_strChargeStartDate, 0, 11 ) ) ) {
			return [];
		}

		// Now the $intNextCompanyChargePostDate date is one period greater than the last_posted_on date.
		while( $intNextCompanyChargePostDate <= $intTodayTimeStamp && ( false == isset ( $this->m_strChargeEndDate ) || ( true == isset ( $this->m_strChargeEndDate ) && $intNextCompanyChargePostDate <= strtotime( \Psi\CStringService::singleton()->substr( $this->m_strChargeEndDate, 0, 11 ) ) ) ) ) {
			$arrobjTransactions[] = $this->createTransaction( $intNextCompanyChargePostDate );
			$this->setLastPostedOn( date( 'm/d/Y', $intNextCompanyChargePostDate ) );

			switch( $this->getChargePeriod() ) {
				case CFrequency::YEARLY:
					$intNextCompanyChargePostDate = strtotime( '+1 year', $intNextCompanyChargePostDate );
					break;

				case CFrequency::SEMI_ANNUALLY:
					$intNextCompanyChargePostDate = strtotime( '+6 months', $intNextCompanyChargePostDate );
					break;

				case CFrequency::QUARTERLY:
					$intNextCompanyChargePostDate = strtotime( '+3 months', $intNextCompanyChargePostDate );
					break;

				case CFrequency::MONTHLY:
					$intNextCompanyChargePostDate = strtotime( '+1 month', $intNextCompanyChargePostDate );
					break;

				default:
					trigger_error( 'A recurring charge with no charge period was found.' );
					break;
			}
		}
		return $arrobjTransactions;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

}
?>