<?php

use Psi\Eos\Admin\CMassEmailPreferences;
use Psi\Eos\Admin\CMassEmailLinks;
use Psi\Eos\Admin\CMassEmailAttachments;
use Psi\Eos\Admin\CMassEmailClicks;
use Psi\Eos\Admin\CMassEmails;
use Psi\Eos\Admin\CEmployees;
use Psi\Eos\Connect\CDatabases;
use Psi\Eos\Admin\CPsLeadFilters;
use Psi\Eos\Admin\CUsers;
use Psi\Eos\Admin\CPersons;
use Psi\Eos\Admin\CEmployeeApplications;
use Psi\Eos\Admin\CActions;

class CMassEmail extends CBaseMassEmail {

	protected $m_strBeginHour;
	protected $m_strTestEmailAddresses;

	protected $m_boolRequirePreviewAndApproval;

	protected $m_intClickCount;
	protected $m_intAttachmentsCount;
	protected $m_intPsProductId;

	protected $m_arrobjMassEmailLinks;

	protected $m_strCreatedByName;

	const MTZ	= 1;
	const LTZ	= 2;

	const DEFAULT_TO_EMAIL_ADDRESS			= CSystemEmail::MASSEMAIL_EMAIL_ADDRESS;
	const DEFAULT_REPLY_TO_EMAIL_ADDRESS	= CSystemEmail::MASSEMAIL_EMAIL_ADDRESS;

	const MERGE_FIELD_USER_EMAIL_ADDRESS 		 				= 'User_Email_Address';
	const MERGE_FIELD_PATTERN_UNSUBSCRIBE_LINK					= '<!--unsubscribe link-->';
	const MERGE_FIELD_PATTERN_SALES_SALUTATION 					= '<!--SALES_SALUTATION-->';
	const MERGE_FIELD_PATTERN_USER_EMAIL_ADDRESS 				= '<!--User_Email_Address-->';
	const MERGE_FIELD_PATTERN_SALES_EMPLOYEE_PHOTO 				= '<!--SALES_EMPLOYEE_PHOTO-->';
	const MERGE_FIELD_PATTERN_SUPPORT_EMPLOYEE_PHOTO 			= '<!--SUPPORT_EMPLOYEE_PHOTO-->';
	const MERGE_FIELD_PATTERN_SALES_MARKETING_BLURB_CONTENT 	= '<!--SALES_MARKETING_BLURB_CONTENT-->';

	const MERGE_FIELD_PATTERN_RECIPIENT_CSM						= '<!--RECIPIENT_CSM-->';
	const MERGE_FIELD_PATTERN_RECIPIENT_NAME					= '<!--RECIPIENT_NAME-->';
	const MERGE_FIELD_PATTERN_RECIPIENT_COMPANY					= '<!--RECIPIENT_COMPANY-->';
	const MERGE_FIELD_PATTERN_RECIPIENT_SALES_CLOSER			= '<!--RECIPIENT_SALES_CLOSER-->';

	public static $c_arrstrMergeFields = [ self::MERGE_FIELD_PATTERN_USER_EMAIL_ADDRESS, self::MERGE_FIELD_PATTERN_RECIPIENT_CSM, self::MERGE_FIELD_PATTERN_RECIPIENT_NAME, self::MERGE_FIELD_PATTERN_RECIPIENT_COMPANY, self::MERGE_FIELD_PATTERN_RECIPIENT_SALES_CLOSER ];

	public static $c_arrstrSegmentFieldNames = [
		'tags'					=> 't.id',
		'states'				=> 's.code',
		'clusters'				=> 'c.database_id',
		'databases'				=> 'c.database_id',
		'ps_products'			=> 'plp.ps_product_id',
		'sales_closers'			=> 'pld.sales_employee_id',
		'company_names'			=> 'pl.id',
		'contact_types'			=> 'pcp.person_contact_type_id',
		'csm_employees'			=> 'pld.support_employee_id',
		'email_addresses'		=> 'e.id',
		'units'					=> 'pl.number_of_units',
		'primary_contacts'		=> 'p.is_primary',
		'offices'				=> 'e.office_id',
		'current_employees'		=> 'e.id',
		'previous_employees'	=> 'e.id',
		'departments'			=> 'e.department_id',
		'employee_applications'	=> 'ea.id',
		'employee_status'		=> 'e.employee_status_type_id',
		'designations'			=> 'e.designation_id',
		'is_managers'			=> 'te.manager_employee_id',
		'teams'					=> 'tm.id',
		'country'				=> 'ead.country_code'
	];

	public function __construct() {
		parent::__construct();

		$this->m_intClickCount = 0;
		$this->m_intAttachmentsCount = 0;
	}

	/**
	 * Get functions.
	 *
	 */

	public function getClickCount() {
		return $this->m_intClickCount;
	}

	public function getAttachmentsCount() {
		return $this->m_intAttachmentsCount;
	}

	public function getBeginHour() {
		return $this->m_strBeginHour;
	}

	public function getTestEmailAddresses() {
		return $this->m_strTestEmailAddresses;
	}

	public function getMassEmailLinks() {
		return $this->m_arrobjMassEmailLinks;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function getRequirePreviewAndApproval() {
		return $this->m_boolRequirePreviewAndApproval;
	}

	public function getCreatedByName() {
		return $this->m_strCreatedByName;
	}

	/**
	 * Set functions.
	 *
	 */

	public function setClickCount( $intClickCount ) {
		$this->m_intClickCount = $intClickCount;
	}

	public function setAttachmentsCount( $intAttachmentsCount ) {
		$this->m_intAttachmentsCount = $intAttachmentsCount;
	}

	public function setBeginHour( $strBeginHour ) {
		$this->m_strBeginHour = $strBeginHour;
	}

	public function setTestEmailAddresses( $strTestEmailAddresses ) {
		$this->m_strTestEmailAddresses = $strTestEmailAddresses;
	}

	public function setMassEmailLinks( $arrobjMassEmailLinks ) {
		$this->m_arrobjMassEmailLinks = $arrobjMassEmailLinks;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = $intPsProductId;
	}

	public function setRequirePreviewAndApproval( $boolRequirePreviewAndApproval ) {
		$this->m_boolRequirePreviewAndApproval = $boolRequirePreviewAndApproval;
	}

	public function setCreatedByName( $strCreatedByName ) {
		$this->m_strCreatedByName = $strCreatedByName;
	}

	public function setDefaults() {
		$this->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$this->setApprovedBy( NULL );
		$this->setApprovedOn( NULL );
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['begin_hour'] ) ) 						$this->setBeginHour( $arrstrValues['begin_hour'] );
		if( true == isset( $arrstrValues['test_email_addresses'] ) ) 			$this->setTestEmailAddresses( $arrstrValues['test_email_addresses'] );
		if( true == isset( $arrstrValues['click_count'] ) ) 					$this->setClickCount( $arrstrValues['click_count'] );
		if( true == isset( $arrstrValues['attachments_count'] ) ) 				$this->setAttachmentsCount( $arrstrValues['attachments_count'] );
		if( true == isset( $arrstrValues['ps_product_id'] ) ) 					$this->setPsProductId( $arrstrValues['ps_product_id'] );
		if( true == isset( $arrstrValues['require_preview_and_approval'] ) ) 	$this->setRequirePreviewAndApproval( $arrstrValues['require_preview_and_approval'] );
		if( true == isset( $arrstrValues['created_by_name'] ) ) 				$this->setCreatedByName( $arrstrValues['created_by_name'] );
	}

	/**
	 * Fetch functions.
	 *
	 */

	public function fetchMassEmailPreferences( $objDatabase ) {
		return CMassEmailPreferences::createService()->fetchMassEmailPreferencesByMassEmailId( $this->m_intId, $objDatabase );
	}

	public function fetchMassEmailLinks( $objDatabase ) {
		return CMassEmailLinks::createService()->fetchMassEmailLinksByMassEmailId( $this->m_intId, $objDatabase );
	}

	public function fetchMassEmailAttachments( $objDatabase ) {
		return CMassEmailAttachments::createService()->fetchMassEmailAttachmentsByMassEmailId( $this->m_intId, $objDatabase );
	}

	public function fetchMassEmailClicksByEmployeeId( $intEmployeeId, $objAdminDatabase ) {
		return CMassEmailClicks::createService()->fetchMassEmailClicksByMassEmailIdByMassEmailLinkIdByEmployeeId( $this->getId(), $intEmployeeId, NULL, $objAdminDatabase );
	}

	/**
	 * Validate functions.
	 *
	 */

	public function valMassEmailTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMassEmailTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mass_email_type_id', 'Mass email type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', 'Frequency is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Campaign name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valSubject() {
		$boolIsValid = true;

		if( true == is_null( $this->getSubject() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subject', 'Subject is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valFromNameFull() {
		$boolIsValid = true;

		if( true == is_null( $this->getFromNameFull() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'from_name_full', 'From name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'product_id', 'Product is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valFromEmailAddress() {
		$boolIsValid = true;

		if( true == is_null( $this->getFromEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'from_email_address', 'From email address is required. ' ) );
		}

		if( false == is_null( $this->getFromEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->getFromEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'from_email_address', 'A valid email address is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valHtmlContent() {
		$boolIsValid    = true;
		$strHtmlContent = $this->getHtmlContent();

		if( true == is_null( $strHtmlContent ) || 0 === strlen( trim( str_replace( '&nbsp;', ' ', strip_tags( $strHtmlContent, '<img>' ) ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'html_content', 'Content is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valBeginDatetime( $boolRequired = true ) {

		$boolIsValid = true;

		if( false == valStr( $this->getBeginDatetime() ) ) {
			if( true == $boolRequired ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_datetime', 'Send date/time is required. ' ) );
			}
			return $boolIsValid;
		}

		$strDate = \Psi\CStringService::singleton()->substr( $this->getBeginDatetime(), 0, 10 );
		$strDate = date( 'm/d/Y', strtotime( $strDate ) );

		if( strtotime( date( 'm/d/Y' ) ) > strtotime( $strDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_datetime', 'Date should be equal to or greater than today\'s date. ' ) );
		} elseif( false == CValidation::validateDate( $strDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_datetime', 'Valid send date/time is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valSendingTimezone() {
		$boolIsValid = true;

		if( true == is_null( $this->getSendingTimezone() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sending_timezone', 'Timezone is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valTestEmailAddresses() {

		$boolIsValid = true;

		if( true == is_null( $this->getTestEmailAddresses() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_email_address', 'Please enter at least one email address. ' ) );
		} elseif( false == CValidation::validateEmailAddresses( $this->getTestEmailAddresses() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_email_address', "Invalid email address. For more than one address please use delimiter ',' or ';'." ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valHtmlContent();
				$boolIsValid &= $this->valMassEmailTypeId();
				$boolIsValid &= $this->valBeginDatetime( $boolRequired = false );
				$boolIsValid &= $this->valSendingTimezone();
				$boolIsValid &= $this->valFrequencyId();
				$boolIsValid &= $this->valFromEmailAddress();
				$boolIsValid &= $this->valFromNameFull();
				break;

			case 'test_email_addresses':
				$boolIsValid &= $this->valTestEmailAddresses();
				break;

			case 'preparation':
				$boolIsValid &= $this->valFromEmailAddress();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valHtmlContent();
				break;

			case 'update_campaign_info':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valMassEmailTypeId();
				$boolIsValid &= $this->valFromEmailAddress();
				$boolIsValid &= $this->valFromNameFull();
				break;

			case 'update_campaign_content':
				$boolIsValid &= $this->valHtmlContent();
				break;

			case 'update_send_options':
				$boolIsValid &= $this->valBeginDatetime();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create functions.
	 *
	 */

	public function createSystemEmail() {

		$strBaseUrl		= ( 'production' == CONFIG_ENVIRONMENT ) ? CONFIG_INSECURE_HOST_PREFIX . 'www.' . CONFIG_COMPANY_BASE_DOMAIN : 'http://www.entrata.lcl';

		$strHtmlContent	= $this->getParsedHtmlContent();

		$strHtmlContent	.= '<img src="' . $strBaseUrl . '/?module=mass_emails&action=update_open_count&mass_email_id=' . $this->getId() . '&extra_params=1" border="0" width="1" height="1">';

		$strFromEmailAddress	= ( true == valStr( $this->getFromEmailAddress() ) ) ? $this->getFromEmailAddress() : CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::MASS_EMAIL, $strSubject = $this->getSubject(), $strHtmlContent, $strToEmailAddress = NULL, $intIsSelfDestruct = 0, $strFromEmailAddress );

		$objSystemEmail->setScheduledSendDatetime( date( 'm/d/Y' ) );

		return $objSystemEmail;
	}

	public function createMassEmailLink() {

		$objMassEmailLink	= new CMassEmailLink();
		$objMassEmailLink->setMassEmailId( $this->getId() );

		return $objMassEmailLink;
	}

	public function createMassEmailAttachment() {

		$objMassEmailAttachment	= new CMassEmailAttachment();
		$objMassEmailAttachment->setMassEmailId( $this->getId() );

		return $objMassEmailAttachment;
	}

	public function createMassEmailPreference() {

		$objMassEmailPreference	= new CMassEmailPreference();
		$objMassEmailPreference->setMassEmailId( $this->getId() );

		return $objMassEmailPreference;
	}

	/**
	 * Add functions.
	 *
	 */

	public function addMassEmailLink( $objMassEmailLink ) {
		$this->m_arrobjMassEmailLinks[$objMassEmailLink->getId()] = $objMassEmailLink;
	}

	/**
	 * Other functions.
	 *
	 */

	public function buildContent() {

		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		$objSmarty->assign( 'mass_email', $this );
		$objSmarty->assign( 'template_type', CMassEmailType::$c_arrstrTemplateTypeNames[$this->getMassEmailTypeId()] );

		if( 'production' == CONFIG_ENVIRONMENT ) {
			$strBaseUrl = CONFIG_INSECURE_HOST_PREFIX . 'www.' . CONFIG_COMPANY_BASE_DOMAIN;
		} else {
			$strBaseUrl = 'http://www.entrata.lcl';
		}

		$objSmarty->assign( 'base_uri',						$strBaseUrl );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME',			CONFIG_COMPANY_NAME );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL',		CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX',	CONFIG_ENTRATA_LOGO_PREFIX );
		$objSmarty->assign( 'CONFIG_COMPANY_BASE_DOMAIN',	CONFIG_COMPANY_BASE_DOMAIN );
		$objSmarty->assign( 'HTTP_COMPANY_BASE_URL',		CONFIG_INSECURE_HOST_PREFIX . 'www.' . CONFIG_COMPANY_BASE_DOMAIN );

		return $objSmarty->fetch( PATH_INTERFACES_COMMON . 'mass_email_templates/' . CMassEmailType::$c_arrstrTemplateTypeNames[$this->getMassEmailTypeId()] . '/main.tpl' );

	}

	public function resetBeginDatetime() {
		$strBeginDatetime	= \Psi\CStringService::singleton()->substr( $this->getBeginDatetime(), 0, 10 ) . ' ' . $this->getBeginHour() . ':00';
		$this->setBeginDatetime( $strBeginDatetime );
	}

	public function incrementOpenCount() {
		$this->setOpenCount( $this->getOpenCount() + 1 );
	}

	public function parseMassEmailLinks( $objAdminDatabase ) {

		$arrobjMassEmailLinks = [];

		if( 0 == strlen( $this->getHtmlContent() ) ) {
			return NULL;
		}

		$strHtmlContent = $this->buildContent();

		libxml_use_internal_errors( true );

		$objDom = new DOMDocument();
		@$objDom->loadHTML( ( true == extension_loaded( 'tidy' ) ) ? @tidy_parse_string( stripslashes( $strHtmlContent ) ) : strip_tags( stripslashes( $strHtmlContent ) ) );

		libxml_use_internal_errors( false );

		$objXpath = new DOMXPath( $objDom );
		$arrobjLinks = $objXpath->evaluate( '/html/body//a' );

		// Find out the unique links for this mass email.
		$arrintPageLinks = [];

		for( $intCount = 0; $intCount < $arrobjLinks->length; $intCount++ ) {
			$objLink = $arrobjLinks->item( $intCount );

			if( 0 < strlen( $objLink->getAttribute( 'href' ) ) && ( false == in_array( pathinfo( $objLink->getAttribute( 'href' ), PATHINFO_EXTENSION ), [ 'jpg', 'gif', 'jpeg', 'png' ] ) ) ) {

				if( false == array_key_exists( \Psi\CStringService::singleton()->strtolower( trim( $objLink->getAttribute( 'href' ) ) ), $arrintPageLinks ) ) {
					$objMassEmailLink = $this->createMassEmailLink();
					$objMassEmailLink->fetchNextId( $objAdminDatabase );
					if( 0 < strlen( $objLink->getAttribute( 'title' ) ) ) {
						$objMassEmailLink->setTitle( $objLink->getAttribute( 'title' ) );
					}

					$objMassEmailLink->setUrl( $objLink->getAttribute( 'href' ) );

					$arrobjMassEmailLinks[$objMassEmailLink->getId()] = $objMassEmailLink;
					$arrintPageLinks[\Psi\CStringService::singleton()->strtolower( trim( $objLink->getAttribute( 'href' ) ) )] = $objMassEmailLink->getId();
				} else {
					$objMassEmailLink = $arrobjMassEmailLinks[$arrintPageLinks[\Psi\CStringService::singleton()->strtolower( trim( $objLink->getAttribute( 'href' ) ) )]];
				}

				// Calculating the click count and the redirecting to the original link
				$strBaseUrl = CONFIG_INSECURE_HOST_PREFIX . 'www.' . CONFIG_COMPANY_BASE_DOMAIN;
				$objLink->setAttribute( 'href', $strBaseUrl . '?module=mass_emails&action=redirect_link&mass_email_link_id=' . $objMassEmailLink->getId() . '&extra_params=1' );

			}
		}

		$this->setParsedHtmlContent( str_replace( '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">', '', $objDom->saveHTML() ) );

		return $arrobjMassEmailLinks;
	}

	public function buildSalesContent( $intEmployeeId, $objAdminDatabase ) {

		// Set the content if the person who is sending is a sales guy.

		if( false == valStr( $this->getHtmlContent() ) ) {
			return;
		}

		$objEmployee = CEmployees::createService()->fetchEmployeeByIdByDeparmentIdsByMarketingBlurb( $intEmployeeId, CDepartment::$c_arrintSalesClosersDepartmentIds, $objAdminDatabase );

		if( false == valObj( $objEmployee, 'CEmployee' ) || false == valStr( $objEmployee->getMarketingBlurb() ) ) {
			return;
		}

		$objEmployeePhoneNumber = $objEmployee->fetchEmployeePhoneNumberByPhoneNumberTypeId( CPhoneNumberType::PRIMARY, $objAdminDatabase );

		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'employee',							$objEmployee );
		$objSmarty->assign( 'base_uri', 						CONFIG_SECURE_HOST_PREFIX . 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN . '/' );
		$objSmarty->assign( 'PATH_MOUNTS_EMPLOYEES_PHOTOS',		PATH_MOUNTS_EMPLOYEES_PHOTOS );
		$objSmarty->assign( 'image_url',	                    CONFIG_COMMON_PATH );

		return $objSmarty->fetch( PATH_INTERFACES_COMMON . 'mass_email_templates/' . CMassEmailType::$c_arrstrTemplateTypeNames[$this->getMassEmailTypeId()] . '/sales_marketing_blurb.tpl' );
	}

	public function buildSalesOrSupportInfo( $intEmployeeId, $objAdminDatabase ) {

		if( false == valStr( $this->getHtmlContent() ) ) {
			return;
		}

		$objEmployee = CEmployees::createService()->fetchEmployeeById( $intEmployeeId, $objAdminDatabase );

		if( false == valObj( $objEmployee, 'CEmployee' ) ) {
			return;
		}

		$objEmployeePhoneNumber = $objEmployee->fetchEmployeePhoneNumberByPhoneNumberTypeId( CPhoneNumberType::PRIMARY, $objAdminDatabase );

		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'employee', 						$objEmployee );
		$objSmarty->assign( 'base_uri', 						CONFIG_SECURE_HOST_PREFIX . 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN . '/' );
		$objSmarty->assign( 'PATH_MOUNTS_EMPLOYEES_PHOTOS',		PATH_MOUNTS_EMPLOYEES_PHOTOS );

		return $objSmarty->fetch( PATH_INTERFACES_COMMON . 'mass_email_templates/' . CMassEmailType::$c_arrstrTemplateTypeNames[$this->getMassEmailTypeId()] . '/photo.tpl' );
	}

	public function determineEmailBlockTypeId() {

		switch( $this->getMassEmailTypeId() ) {
			case CMassEmailType::MONTHLY_NEWSLETTER:
			case CMassEmailType::INTERNAL_NEWSLETTER:
				return CEmailBlockType::PS_NEWSLETTER;
				break;

			case CMassEmailType::SCHEDULED_DOWN_TIME:
				return CEmailBlockType::SCHEDULED_DOWN_TIME;
				break;

			case CMassEmailType::SALES_EMAIL:
			case CMassEmailType::NEW_PRODUCT_ANNOUNCEMENT:
				return CEmailBlockType::NEW_PRODUCT_ANNOUNCEMENT;
				break;

			case CMassEmailType::OTHER:
				return CEmailBlockType::OTHER_MASS_EMAIL;
				break;

			case CMassEmailType::PRODUCT_UPDATE_EMAIL:
				return CEmailBlockType::PRODUCT_UPDATE_EMAIL;
				break;

			case CMassEmailType::PRODUCT_PROMOTION:
				return CEmailBlockType::PRODUCT_PROMOTION;
				break;

			default:
				return NULL;
				break;
		}
	}

	public function determineActionTypeId() {

		switch( $this->getMassEmailTypeId() ) {
			case CMassEmailType::MONTHLY_NEWSLETTER:
			case CMassEmailType::INTERNAL_NEWSLETTER:
			case CMassEmailType::NEW_PRODUCT_ANNOUNCEMENT:
				return CActionType::NEWSLETTER;
				break;

			case CMassEmailType::OTHER:
			case CMassEmailType::CUSTOM:
			case CMassEmailType::SALES_EMAIL:
			case CMassEmailType::SALES_CRM:
			case CMassEmailType::PEEP_EMAILS:
			case CMassEmailType::PRODUCT_UPDATE_EMAIL:
			case CMassEmailType::PRODUCT_PROMOTION:
			case CMassEmailType::SCHEDULED_DOWN_TIME:
				return CActionType::MASS_EMAIL;
			default:
				return NULL;
				break;
		}
	}

	// WE USE FOLLOWING FUNCTION TO DETERMINE WHICH EMAIL ADDRESSES WILL RECEIVE THIS
	// MAS EMAIL. THIS FUNCTION IS USED IN NIGHTLY SCRIPT AS WILL AS MODULE.

	public function determineTargetEmailList( $objAdminDatabase, $boolIsActiveCompaniesOnly = false ) {

		$arrstrMassEmailAddresses 	= [];

		$arrobjMassEmailPreferences = $this->fetchMassEmailPreferences( $objAdminDatabase );

		if( false == valArr( $arrobjMassEmailPreferences ) ) {
			return $arrstrMassEmailAddresses;
		}

		if( true == array_key_exists( 'PROPERTY_PREFERENCES', $arrobjMassEmailPreferences ) || true == array_key_exists( 'PROPERTY_EMAIL_ADDRESS_TYPES', $arrobjMassEmailPreferences ) || true == array_key_exists( 'COMPANY_EMPLOYEE_TYPES', $arrobjMassEmailPreferences ) ) {

			$objConnectDatabase = CDatabases::createService()->createConnectDatabase();

			if( true == valObj( $objConnectDatabase, 'CDatabase' ) ) {
				$arrobjClientDatabases	= CDatabases::createService()->fetchClientDatabasesByDatabaseUserTypeIdByDatabaseTypeId( CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseType::CLIENT, $objConnectDatabase );
			}
		}

		// LOAD PS LEAD FILTERS EMAIL ADDRESSES.
		if( true == array_key_exists( 'PS_LEAD_FILTERS', $arrobjMassEmailPreferences ) ) {

			$objMassEmailPreference = $arrobjMassEmailPreferences['PS_LEAD_FILTERS'];
			$arrstrValues 			= explode( ',', $objMassEmailPreference->getValue() );

			foreach( $arrstrValues as $intPsLeadFilter ) {

				$objPsLeadFilter = CPsLeadFilters::createService()->fetchPsLeadFilterById( $intPsLeadFilter, $objAdminDatabase );

				$objUser 		= CUsers::createService()->fetchUserDetailsById( $this->getCreatedBy(), $objAdminDatabase );
				$arrobjGroups 	= $objUser->fetchGroups( $objAdminDatabase );
				$objEmployee 	= $objUser->fetchEmployee( $objAdminDatabase );

				if( false == valObj( $objPsLeadFilter, 'CPsLeadFilter' )
						|| ( ( 1 != $objUser->getIsAdministrator() )
								&& true == valArr( $arrobjGroups )
								&& true == array_key_exists( CGroup::SALES, $arrobjGroups )
								&& false == array_key_exists( CGroup::MARKETING, $arrobjGroups )
								&& $objEmployee->getId() != $objPsLeadFilter->getEmployees() ) ) {

					continue;
				}

				$objPsLeadFilter->setIsActiveCompaniesOnly( $boolIsActiveCompaniesOnly );

				// This is a targeted list of leads and we'll query them very simply (probably came from the view ps lead persons view "with checked checkboxes" )
				if( 0 < strlen( trim( $objPsLeadFilter->getPersons() ) ) ) {

					$arrmixData = CPersons::createService()->fetchSimplePersonsByPsLeadFilterForSalesCrmMassMail( $objPsLeadFilter, $objAdminDatabase );

					// This is a more wide spread and complex lead filter and is probably a newsletter mass email (not a specific list of leads)
				} else {

					$strSql = '';
					$boolJoinActions			= false;
					$boolJoinMassEmailLinks		= false;

					if( 0 < strlen( trim( $objPsLeadFilter->getActionTypes() ) ) ) 					$boolJoinActions = true;
					if( 0 < strlen( trim( $objPsLeadFilter->getActionResults() ) ) ) 				$boolJoinActions = true;
					if( 0 < strlen( trim( $objPsLeadFilter->getNoteText() ) ) ) 					$boolJoinActions = true;
					if( 0 < strlen( trim( $objPsLeadFilter->getActionStartDate() ) ) ) 				$boolJoinActions = true;
					if( 0 < strlen( trim( $objPsLeadFilter->getActionEndDate() ) ) ) 				$boolJoinActions = true;
					if( 0 < strlen( trim( $objPsLeadFilter->getMassEmails() ) ) ) 					$boolJoinActions = true;
					if( 0 < strlen( trim( $objPsLeadFilter->getAds() ) ) ) 							$boolJoinActions = true;
					if( 0 < strlen( trim( $objPsLeadFilter->getAdPsProductIds() ) ) )				$boolJoinActions = true;
					if( 0 < strlen( trim( $objPsLeadFilter->getAdLocations() ) ) )		 			$boolJoinActions = true;

					if( 0 < strlen( trim( $objPsLeadFilter->getMassEmailLinks() ) ) ) {
						$boolJoinActions = true;
						$boolJoinMassEmailLinks = true;
					}

					if( 1 == $objPsLeadFilter->getIsPeepMailSearch() ) {
						$boolJoinActions = true;
					}

					if( true == $boolJoinActions ) {
						$strSql .= 'SELECT sub_query1.*
									FROM ( ';
					}

					$strSql .= 'SELECT p.id,
									p.ps_lead_id,
									pcp.person_contact_type_id,
									p.name_first,
									p.name_last,
									p.title,
									lower(p.email_address) AS email_address,
									p.city,
									p.state_code,
									p.deleted_by,
									p.deleted_on,
									p.updated_by,
									p.updated_on,
									p.created_by,
									p.created_on,
									pe.id AS person_employee_id,
									pld.support_employee_id AS support_employee_id,
									e.email_address AS employee_email_address,
									e.name_first || \' \' || e.name_last AS employee_full_name,
								CASE
									WHEN pld.sales_employee_id IS NOT NULL
									THEN pld.sales_employee_id
									END AS employee_id';

					$strSql .= CPersons::createService()->buildCommonPersonSearchSql( $objPsLeadFilter, $boolIsFromMassEmail = true, 0 );
					$strSql .= ' GROUP BY
									p.id,
									p.ps_lead_id,
									pcp.person_contact_type_id,
									p.name_first,
									p.name_last,
									p.title,
									lower(p.email_address),
									p.city,
									p.state_code,
									p.deleted_by,
									p.deleted_on,
									p.updated_by,
									p.updated_on,
									p.created_by,
									p.created_on,
									pe.id,
									pld.support_employee_id,
									e.email_address,
									e.name_first || \' \' || e.name_last,
									CASE
									WHEN pld.sales_employee_id IS NOT NULL
									THEN pld.sales_employee_id
									END
								ORDER BY
									p.id';

					if( true == $boolJoinActions ) {
						$strSql .= ' ) as sub_query1 ';

						if( 0 < strlen( trim( $objPsLeadFilter->getMassEmails() ) ) ) {
							$strActionsJoinCondition = 'a.person_id = sub_query1.id AND a.secondary_reference IN ( ' . $objPsLeadFilter->getMassEmails() . ' ) AND a.action_result_id IN( ' . CActionResult::EMAIL_OPENED . ',' . CActionResult::EMAIL_CLICKED . ' ) ';

						} elseif( 0 < strlen( trim( $objPsLeadFilter->getMassEmailLinks() ) ) ) {
							$strActionsJoinCondition = 'a.person_id = sub_query1.id AND a.action_result_id = ' . CActionResult::EMAIL_CLICKED . ' ';

						} elseif( 0 < strlen( trim( $objPsLeadFilter->getAds() ) ) ) {
							$strActionsJoinCondition = 'a.person_id = sub_query1.id AND a.action_type_id = ' . CActionType::ENTRATA_AD . ' AND a.primary_reference IN ( ' . $objPsLeadFilter->getAds() . ' ) ';

						} elseif( 0 < strlen( trim( $objPsLeadFilter->getAdPsProductIds() ) ) ) {
							$strActionsJoinCondition = 'a.person_id = sub_query1.id AND a.ps_product_id IN ( ' . $objPsLeadFilter->getAdPsProductIds() . ' ) ';

						} elseif( 0 < strlen( trim( $objPsLeadFilter->getAdLocations() ) ) || 1 == $objPsLeadFilter->getIsPeepMailSearch() ) {
							$strActionsJoinCondition = 'a.person_id = sub_query1.id ';

						} else {
							$strActionsJoinCondition = 'a.ps_lead_id = sub_query1.ps_lead_id';
						}

						if( true == $boolJoinMassEmailLinks || 0 < strlen( trim( $objPsLeadFilter->getMassEmails() ) ) || 0 < strlen( trim( $objPsLeadFilter->getAds() ) ) ) {
							$strSql .= ' JOIN actions a ON ( ' . $strActionsJoinCondition . ' ) ';
						} else {
							$strSql .= ' LEFT OUTER JOIN actions a ON ( ' . $strActionsJoinCondition . ' ) ';
						}

						// BUILD WHERE CONDITIONS
						$strSql .= ' WHERE 1 = 1 ';

						if( 0 < strlen( trim( $objPsLeadFilter->getActionTypes() ) ) ) 				$strSql .= ' AND a.action_type_id IN ( ' . trim( $objPsLeadFilter->getActionTypes() ) . ' )';
						if( 0 < strlen( trim( $objPsLeadFilter->getActionResults() ) ) ) 			$strSql .= ' AND a.action_result_id IN ( ' . trim( $objPsLeadFilter->getActionResults() ) . ' ) ';
						if( 0 < strlen( trim( $objPsLeadFilter->getNoteText() ) ) ) 				$strSql .= ' AND ( a.notes ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getNoteText() ) ) . '%\' OR pln.note ILIKE \'%' . trim( addslashes( $objPsLeadFilter->getNoteText() ) ) . '%\' )';
						if( 0 < strlen( trim( $objPsLeadFilter->getActionStartDate() ) ) ) 			$strSql .= ' AND a.action_datetime IS NOT NULL AND a.action_datetime >= \'' . $objPsLeadFilter->getActionStartDate() . '\' ';
						if( 0 < strlen( trim( $objPsLeadFilter->getActionEndDate() ) ) ) 			$strSql .= ' AND a.action_datetime IS NOT NULL AND a.action_datetime < \'' . $objPsLeadFilter->getActionEndDate() . '\' ';

						// This section helps us make sure we only load ps lead persons that have received peep mail contacts
						if( 1 == $objPsLeadFilter->getIsPeepMailSearch() && 0 < strlen( trim( $objPsLeadFilter->getPsProducts() ) ) ) {

							$strSql .= ' AND a.ps_product_id IN ( ' . $objPsLeadFilter->getPsProducts() . ' ) ';

							if( 0 < strlen( trim( $objPsLeadFilter->getPeepEmployees() ) ) ) {
								$strSql .= ' AND a.employee_id IN ( ' . $objPsLeadFilter->getPeepEmployees() . ' ) ';
							}

							if( 0 == strlen( trim( $objPsLeadFilter->getActionTypes() ) ) ) {
								$strSql .= ' AND a.action_type_id IN ( ' . CActionType::SUPPORT_PEEP_EMAIL . ',' . CActionType::SALES_PEEP_EMAIL . ' ) ';
							}

							if( 0 < $objPsLeadFilter->getIsInterested() ) {
								$strSql .= ' AND a.person_id = sub_query1.id AND a.action_result_id = ' . $objPsLeadFilter->getIsInterested();
							}
						}
						$strSql .= '
							GROUP BY
								sub_query1.id,
								sub_query1.ps_lead_id,
								sub_query1.person_contact_type_id,
								sub_query1.name_first,
								sub_query1.name_last,
								sub_query1.title,
								sub_query1.email_address,
								sub_query1.city,
								sub_query1.state_code,
								sub_query1.deleted_by,
								sub_query1.deleted_on,
								sub_query1.updated_by,
								sub_query1.updated_on,
								sub_query1.created_by,
								sub_query1.created_on,
								sub_query1.person_employee_id,
								sub_query1.support_employee_id,
								sub_query1.employee_email_address,
								sub_query1.employee_full_name,
								sub_query1.employee_id';
					}

					$arrmixData = fetchData( $strSql, $objAdminDatabase );
				}

				if( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixData ) ) {
					foreach( $arrmixData as $arrstrData ) {
						if( true == CValidation::validateEmailAddresses( $arrstrData['email_address'] ) ) {
							$arrstrMassEmailAddresses[$arrstrData['email_address']] = [
								'type' 						=> 'PS_LEAD_FILTERS',
								'recipient_type_id' 		=> CSystemEmailRecipientType::LEAD,
								'email_address' 			=> $arrstrData['email_address'],
								'ps_lead_id' 				=> $arrstrData['ps_lead_id'],
								'person_id' 				=> $arrstrData['id'],
								'person_first_name' 		=> $arrstrData['name_first'],
								'support_employee_id' 		=> ( true == isset( $arrstrData['support_employee_id'] ) )? $arrstrData['support_employee_id']:'',
								'employee_email_address' 	=> ( true == isset( $arrstrData['employee_email_address'] ) )?$arrstrData['employee_email_address']:'',
								'employee_full_name' 		=> ( true == isset( $arrstrData['employee_full_name'] ) )?$arrstrData['employee_full_name']:'',
								'employee_id' 				=> ( true == isset( $arrstrData['employee_id'] ) )?$arrstrData['employee_id']:''
							];
						}
					}
				}
			}
		}

		// LOAD PROPERTY PREFERENCES EMAIL ADDRESSES.
		if( true == array_key_exists( 'PROPERTY_PREFERENCES', $arrobjMassEmailPreferences ) ) {

			$objMassEmailPreference = $arrobjMassEmailPreferences['PROPERTY_PREFERENCES'];
			$arrstrValues 			= explode( ',', $objMassEmailPreference->getValue() );

			foreach( $arrobjClientDatabases as $objClientDatabase ) {

				$strSql = 'SELECT
								DISTINCT(lower(pf.value) ) AS email_address,
								pf.property_id AS reference_number,
								pf.cid AS cid
							FROM
								property_preferences pf
							JOIN
								clients mc ON ( pf.cid = mc.id AND mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::TERMINATED . ' , ' . CCompanyStatusType::TEST_DATABASE . ' , ' . CCompanyStatusType::CANCELLED . ' ) )
							WHERE
								pf.value LIKE \'%@%\'
								AND pf.value IS NOT NULL
								AND pf.key IN ( \'' . implode( "','", $arrstrValues ) . '\' ) ';

				$objClientDatabase->open();

				$arrstrEmailAddresses = CMassEmails::createService()->fetchEmailAddresses( 'PROPERTY_PREFERENCES', $strSql, $objClientDatabase, CSystemEmailRecipientType::CLIENT );

				$objClientDatabase->close();

				if( true == valArr( $arrstrEmailAddresses ) ) {
					$arrstrMassEmailAddresses = array_merge( $arrstrMassEmailAddresses, $arrstrEmailAddresses );
				}
			}
		}

		// LOAD PROPERTY EMAIL ADDRESS TYPES EMAIL ADDRESSES.
		if( true == array_key_exists( 'PROPERTY_EMAIL_ADDRESS_TYPES', $arrobjMassEmailPreferences ) ) {

			$objMassEmailPreference = $arrobjMassEmailPreferences['PROPERTY_EMAIL_ADDRESS_TYPES'];
			$arrstrValues 			= explode( ',', $objMassEmailPreference->getValue() );

			foreach( $arrobjClientDatabases as $objClientDatabase ) {

				$strSql = 'SELECT DISTINCT( lower( pea.email_address ) ) AS email_address,
								pea.property_id AS reference_number,
								pea.cid AS cid
							FROM
								property_email_addresses pea
							JOIN
								clients mc ON ( pea.cid = mc.id AND mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::TERMINATED . ' , ' . CCompanyStatusType::TEST_DATABASE . ' , ' . CCompanyStatusType::CANCELLED . ' ) )
							WHERE
								pea.email_address LIKE \'%@%\' AND pea.email_address_type_id IN( ' . implode( ',', $arrstrValues ) . ' )
								AND pea.email_address IS NOT NULL ';

				$objClientDatabase->open();

				$arrstrEmailAddresses = CMassEmails::createService()->fetchEmailAddresses( 'PROPERTY_EMAIL_ADDRESS_TYPES', $strSql, $objClientDatabase, CSystemEmailRecipientType::CLIENT );

				$objClientDatabase->close();

				if( true == valArr( $arrstrEmailAddresses ) ) {
					$arrstrMassEmailAddresses = array_merge( $arrstrMassEmailAddresses, $arrstrEmailAddresses );
				}
			}
		}

		// LOAD COMPANY EMPLOYEE TYPES EMAIL ADDRESSES.
		if( true == array_key_exists( 'COMPANY_EMPLOYEE_TYPES', $arrobjMassEmailPreferences ) ) {

			$objMassEmailPreference = $arrobjMassEmailPreferences['COMPANY_EMPLOYEE_TYPES'];
			$arrstrValues 			= explode( ',', $objMassEmailPreference->getValue() );

			foreach( $arrobjClientDatabases as $objClientDatabase ) {

				$arrmixData = [];

				$strSql = 'SELECT
								DISTINCT(lower(ce.email_address) ) AS email_address, ce.id AS reference_number, ce.cid AS cid
							FROM
								company_employees ce
							JOIN
								clients mc ON ( ce.cid = mc.id AND mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::TERMINATED . ' , ' . CCompanyStatusType::TEST_DATABASE . ' , ' . CCompanyStatusType::CANCELLED . ' ) )
							LEFT JOIN
								company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
							WHERE
								ce.email_address LIKE \'%@%\'
								AND ce.employee_status_type_id IN( ' . implode( ',', $arrstrValues ) . ' )
								AND ce.email_address IS NOT NULL
								AND cu.is_disabled <> 1';

				$objClientDatabase->open();

				$arrstrEmailAddresses = CMassEmails::createService()->fetchEmailAddresses( 'COMPANY_EMPLOYEE_TYPES', $strSql, $objClientDatabase, CSystemEmailRecipientType::COMPANY_EMPLOYEE );

				$objClientDatabase->close();

				if( true == valArr( $arrstrEmailAddresses ) ) {
					$arrstrMassEmailAddresses = array_merge( $arrstrMassEmailAddresses, $arrstrEmailAddresses );
				}
			}
		}

		// LOAD PS PERSON TYPES EMAIL ADDRESSES.
		if( true == array_key_exists( 'PERSON_TYPES', $arrobjMassEmailPreferences ) ) {

			$objMassEmailPreference = $arrobjMassEmailPreferences['PERSON_TYPES'];
			$arrstrValues 			= explode( ',', $objMassEmailPreference->getValue() );

			$strSql = 'SELECT DISTINCT(lower(p.email_address) ) AS email_address,
							pcp.person_id AS reference_number,
							p.cid AS cid
						FROM
							person_contact_preferences pcp
							JOIN persons p ON ( p.id = pcp.person_id )
							JOIN clients mc ON ( p.cid = mc.id AND mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::TERMINATED . ' , ' . CCompanyStatusType::TEST_DATABASE . ' , ' . CCompanyStatusType::CANCELLED . ' ) )
						WHERE
							p.email_address LIKE \'%@%\'
							AND pcp.person_contact_type_id IN( ' . implode( ',', $arrstrValues ) . ' )
							AND p.id NOT IN( SELECT DISTINCT(person_id)FROM person_contact_preferences WHERE person_contact_type_id = ' . CPersonContactType::BLOCK_MASS_EMAIL . ')
							AND p.email_address IS NOT NULL
							AND p.is_disabled = 0';

			$arrstrEmailAddresses = CMassEmails::createService()->fetchEmailAddresses( 'PERSON_TYPES', $strSql, $objAdminDatabase, CSystemEmailRecipientType::LEAD );

			if( true == valArr( $arrstrEmailAddresses ) ) {
				$arrstrMassEmailAddresses = array_merge( $arrstrMassEmailAddresses, $arrstrEmailAddresses );
			}
		}

		// LOAD EMPLOYEE TYPES EMAIL ADDRESSES.
		if( true == array_key_exists( 'EMPLOYEE_TYPES', $arrobjMassEmailPreferences ) ) {

			$objMassEmailPreference = $arrobjMassEmailPreferences['EMPLOYEE_TYPES'];
			$arrstrValues 			= explode( ',', $objMassEmailPreference->getValue() );

			$strSql = 'SELECT DISTINCT(lower(email_address) ) AS email_address, id AS reference_number FROM employees WHERE email_address LIKE \'%@%\' AND employee_status_type_id IN( ' . implode( ',', $arrstrValues ) . ' ) AND email_address IS NOT NULL AND date_terminated IS NULL';

			$arrstrEmailAddresses = CMassEmails::createService()->fetchEmailAddresses( 'EMPLOYEE_TYPES', $strSql, $objAdminDatabase, CSystemEmailRecipientType::PSI_EMPLOYEE );

			if( true == valArr( $arrstrEmailAddresses ) ) {
				$arrstrMassEmailAddresses = array_merge( $arrstrMassEmailAddresses, $arrstrEmailAddresses );
			}
		}

		// LOAD EMPLOYEE FILTERS EMAIL ADDRESSES.
		if( true == array_key_exists( 'EMPLOYEE_FILTERS', $arrobjMassEmailPreferences ) ) {

			$objMassEmailPreference = $arrobjMassEmailPreferences['EMPLOYEE_FILTERS'];
			$arrintValues 			= explode( ',', $objMassEmailPreference->getValue() );

			foreach( $arrintValues as $intPsFilterId ) {
				$strSql = CEmployees::createService()->fetchEmployeeEmailAddressesByPsFilterId( $intPsFilterId, $objAdminDatabase, NULL, $boolReturnSql = true );

				$arrstrEmailAddresses = CMassEmails::createService()->fetchEmailAddresses( 'EMPLOYEE_FILTERS', $strSql, $objAdminDatabase, CSystemEmailRecipientType::PSI_EMPLOYEE );

				if( true == valArr( $arrstrEmailAddresses ) ) {
					$arrstrMassEmailAddresses = array_merge( $arrstrMassEmailAddresses, $arrstrEmailAddresses );
				}
			}

		}

		// LOAD EMPLOYEE APPLICATION FILTERS EMAIL ADDRESSES.
		if( true == array_key_exists( 'EMPLOYEE_APPLICATION_FILTERS', $arrobjMassEmailPreferences ) ) {

			$objMassEmailPreference = $arrobjMassEmailPreferences['EMPLOYEE_APPLICATION_FILTERS'];
			$arrintValues 			= explode( ',', $objMassEmailPreference->getValue() );

			foreach( $arrintValues as $intPsFilterId ) {
				$strSql = CEmployeeApplications::createService()->fetchEmployeeApplicationEmailAddressesByPsFilterId( $intPsFilterId, $objAdminDatabase, $boolReturnSql = true );

				$arrstrEmailAddresses = CMassEmails::createService()->fetchEmailAddresses( 'EMPLOYEE_APPLICATION_FILTERS', $strSql, $objAdminDatabase, CSystemEmailRecipientType::EMPLOYEE_APPLICATION );

				if( true == valArr( $arrstrEmailAddresses ) ) {
					$arrstrMassEmailAddresses = array_merge( $arrstrMassEmailAddresses, $arrstrEmailAddresses );
				}
			}

		}

		// LOAD TAGS.
		if( true == array_key_exists( 'TAGS', $arrobjMassEmailPreferences ) ) {

			$objMassEmailPreference = $arrobjMassEmailPreferences['TAGS'];
			$arrstrValues 			= explode( ',', $objMassEmailPreference->getValue() );

			$strSql = 'SELECT
							DISTINCT(lower(p.email_address) ) AS email_address,
							t.id AS reference_number,
							p.cid AS cid
						FROM
							tags t
							JOIN actions a ON ( a.primary_reference = t.id )
							JOIN persons p ON (p.id = a.person_id)
							JOIN person_contact_preferences pcp ON( pcp.person_id = p.id )
							LEFT JOIN clients mc ON (p.cid = mc.id)
						WHERE
							p.email_address LIKE \'%@%\'
							AND p.email_address IS NOT NULL
							AND t.id IN( ' . implode( ',', $arrstrValues ) . ' )
							AND t.is_published = 1
							AND t.action_result_id = ' . CActionResult::PERSON_TAG . '
							AND t.deleted_by IS NULL
							AND p.deleted_by IS NULL
							AND p.is_disabled = 0
							AND p.id NOT IN( SELECT DISTINCT(person_id)FROM person_contact_preferences WHERE person_contact_type_id = ' . CPersonContactType::BLOCK_MASS_EMAIL . ')
							AND ( mc.id IS NULL OR mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::TERMINATED . ' , ' . CCompanyStatusType::TEST_DATABASE . ' , ' . CCompanyStatusType::CANCELLED . ' ) ) ';
			$arrstrEmailAddresses = CMassEmails::createService()->fetchEmailAddresses( 'TAGS', $strSql, $objAdminDatabase, CSystemEmailRecipientType::LEAD );

			if( true == valArr( $arrstrEmailAddresses ) ) {
				$arrstrMassEmailAddresses = array_merge( $arrstrMassEmailAddresses, $arrstrEmailAddresses );
			}
		}

		// LOAD PRIMARY PERSONS.
		if( true == array_key_exists( 'PRIMARY_PERSONS', $arrobjMassEmailPreferences ) ) {

			$objMassEmailPreference = $arrobjMassEmailPreferences['PRIMARY_PERSONS'];

			$strSql = 'SELECT DISTINCT(lower(p.email_address) ) AS email_address,
							p.cid AS cid
						FROM
							persons p
							JOIN clients mc ON ( p.cid = mc.id AND mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::TERMINATED . ' , ' . CCompanyStatusType::TEST_DATABASE . ' , ' . CCompanyStatusType::CANCELLED . ' ) )
						WHERE
							p.email_address LIKE \'%@%\'
							AND p.is_primary = 1
							AND p.id NOT IN( SELECT DISTINCT(person_id)FROM person_contact_preferences WHERE person_contact_type_id = ' . CPersonContactType::BLOCK_MASS_EMAIL . ')
							AND p.email_address IS NOT NULL
							AND p.is_disabled = 0 ';

			$arrstrEmailAddresses = CMassEmails::createService()->fetchEmailAddresses( 'PRIMARY_PERSONS', $strSql, $objAdminDatabase, CSystemEmailRecipientType::LEAD );

			if( true == valArr( $arrstrEmailAddresses ) ) {
				$arrstrMassEmailAddresses = array_merge( $arrstrMassEmailAddresses, $arrstrEmailAddresses );
			}
		}

		return $arrstrMassEmailAddresses;
	}

	public function buildTargetHtmlContent( $strHtmlContent, $arrstrMassEmailAddress, $objAdminDatabase ) {

		// COMMENTED SOME CODE AS WE DONT NEED THE MARKETING CONTENT FOR NOW WE WILL ALLOW THIS ONCE ALL EMPLOYEE PHOTOS ARE UPDATED
		// ADDS THE SALES CONTENT FOR IN NEWSLETTER EMAIL
		if( 0 < strlen( $arrstrMassEmailAddress['employee_id'] ) && true == is_numeric( $arrstrMassEmailAddress['employee_id'] ) ) {

			if( CMassEmailType::MONTHLY_NEWSLETTER == $this->getMassEmailTypeId() ) {
				$strSalesHtmlContent = $this->buildSalesContent( $arrstrMassEmailAddress['employee_id'], $objAdminDatabase );

				if( 0 < strlen( $strSalesHtmlContent ) ) {
					$strHtmlContent = str_replace( '<!-- SALES_MARKETING_BLURB_CONTENT -->', $strSalesHtmlContent, $strHtmlContent );
				}

				if( 0 < strlen( $arrstrMassEmailAddress['person_first_name'] ) ) {
					$strHtmlContent = str_replace( '<!-- SALES_SALUTATION -->',	'Hi ' . $arrstrMassEmailAddress['person_first_name'] . ',<br>', $strHtmlContent );
				}

				// Replace a image for gray left bar shadow if there is no calender
				if( 0 == substr_count( $strHtmlContent, 'calendar_title' ) ) {
					$strHtmlContent = str_replace( 'content_top.jpg', 'sales_message_top.jpg', $strHtmlContent );
					$strHtmlContent = str_replace( 'content_bottom.jpg', 'sales_message_bottom.jpg', $strHtmlContent );
				}
			}
		}

		// ADDS THE SUPPORT PHOTO IN NEWSLETTER EMAIL
		if( 0 < strlen( $arrstrMassEmailAddress['support_employee_id'] ) ) {
			$strSupportHtmlContent = $this->buildSalesOrSupportInfo( $arrstrMassEmailAddress['support_employee_id'], $objAdminDatabase );

			if( 0 < strlen( $strSupportHtmlContent ) ) {
				$strHtmlContent = str_replace( '<!-- SUPPORT_EMPLOYEE_PHOTO -->', $strSupportHtmlContent, $strHtmlContent );
			}
		}

		// ADDS THE SALES PHOTO SALES EMAIL
		if( true == is_numeric( $arrstrMassEmailAddress['employee_id'] ) ) {
			// $strSalesOrSupportInfo = $this->buildSalesOrSupportInfo( $arrstrMassEmailAddress['employee_id'], $objAdminDatabase );

			if( CMassEmailType::SALES_EMAIL == $this->getMassEmailTypeId() && 0 == substr_count( $strHtmlContent,  '<!-- SALES_EMPLOYEE_PHOTO -->' ) ) {
				$strHtmlContent = str_replace( 'content_top.jpg', 'white_round_top.jpg', $strHtmlContent );
				$strHtmlContent = str_replace( 'content_bottom.jpg', 'white_round_bottom.jpg', $strHtmlContent );
				$strHtmlContent = str_replace( 'background-color: rgb(241, 241, 241);', '', $strHtmlContent );
			}

			// if( 0 < strlen( $strSalesOrSupportInfo ) ) {
				// $strHtmlContent = str_replace( '<!-- SALES_EMPLOYEE_PHOTO -->', $strSalesOrSupportInfo, $strHtmlContent );
			// }
		}

		if( CMassEmailType::SALES_CRM == $this->getMassEmailTypeId()
				&& 0 < strlen( trim( $arrstrMassEmailAddress['person_first_name'] ) )
				&& \Psi\CStringService::singleton()->stristr( $strHtmlContent, 'first_name' ) ) {

			$strHtmlContent = str_replace( 'first_name', $arrstrMassEmailAddress['person_first_name'], $strHtmlContent );
		}

		return $strHtmlContent;
	}

	public function fetchActions( $objAdminDatabase ) {
		return CActions::createService()->fetchActionsByMassEmailId( $this->getId(), $objAdminDatabase );
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

	public function duplicateMassEmail( $strMassEmailName, $intEmployeeId, $objDatabase ) {

		if( false == valStr( $strMassEmailName ) || false == valId( $intEmployeeId ) || false == valObj( $objDatabase, 'CDatabase' ) ) {
			return false;
		}

		$objMassEmailClone = new CMassEmail();

		$objMassEmailClone->setId( $objMassEmailClone->fetchNextId( $objDatabase ) );
		$objMassEmailClone->setName( $strMassEmailName );
		$objMassEmailClone->setMassEmailTypeId( $this->getMassEmailTypeId() );
		$objMassEmailClone->setAdditionalRecipients( $this->getAdditionalRecipients() );
		$objMassEmailClone->setMassEmailSegmentIds( $this->getMassEmailSegmentIds() );
		$objMassEmailClone->setFromNameFull( $this->getFromNameFull() );
		$objMassEmailClone->setFromEmailAddress( $this->getFromEmailAddress() );
		$objMassEmailClone->setSubject( $this->getSubject() );
		$objMassEmailClone->setParsedHtmlContent( $this->getParsedHtmlContent() );
		$objMassEmailClone->setHtmlContent( $this->getHtmlContent() );
		$objMassEmailClone->setIsHtmlContent( $this->getIsHtmlContent() );
		$objMassEmailClone->setEmployeeId( $intEmployeeId );
		$objMassEmailClone->setSendingTimezone( CMassEmail::MTZ );
		$objMassEmailClone->setFrequencyId( CFrequency::ONCE );
		$objMassEmailClone->setIsListExcluded( $this->getIsListExcluded() );
		$objMassEmailClone->setIsDisabled( false );
		$objMassEmailClone->setIsNewMassEmail( true );
		$objMassEmailClone->setIsScheduled( true );

		return $objMassEmailClone;
	}

}
?>