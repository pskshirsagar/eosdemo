<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestSubmissionAnswers
 * Do not add any new functions to this class.
 */

class CTestSubmissionAnswers extends CBaseTestSubmissionAnswers {

	public static function fetchTestSubmissionAnswerIds( $intSubmissionId, $objDatabase ) {
		return self::fetchTestSubmissionAnswers( 'SELECT id FROM test_submission_answers WHERE test_submission_id = ' . ( int ) $intSubmissionId, $objDatabase );
	}

	public static function fetchTestSubmissionAnswerByTestSubmissionIdAndByQuestionId( $intSubmissionId, $intTestQuestionId, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						test_submission_answers
					WHERE
						test_submission_id = ' . ( int ) $intSubmissionId . '
						AND test_question_id = ' . ( int ) $intTestQuestionId . '
					ORDER BY
						id ASC
					LIMIT
						1';

		return self::fetchTestSubmissionAnswer( $strSql, $objDatabase );
	}

	public static function fetchTestSubmissionAnswersByTestIds( $intTestSubmissionIds, $objDatabase ) {
		return self::fetchTestSubmissionAnswers( 'SELECT * FROM test_submission_answers WHERE test_submission_id IN ( ' . implode( ',', $intTestSubmissionIds ) . ' ) ', $objDatabase );
	}

	public static function fetchTestSubmissionAnswersByTestId( $intTestId, $intTestSubmissionId, $objDatabase ) {

		$strSql = 'SELECT
						tsa.*
					FROM
						test_submission_answers tsa
						LEFT JOIN test_submissions ts ON tsa.test_submission_id = ts.id
					WHERE
						ts.test_id = ' . ( int ) $intTestId . '
						AND tsa.test_submission_id = ' . ( int ) $intTestSubmissionId;

		return self::fetchTestSubmissionAnswers( $strSql, $objDatabase );
	}

	public static function fetchTestQuestionIdsByIds( $intTestSubmissionAnswerIds, $objDatabase ) {
		return fetchData( 'SELECT id, test_question_id FROM test_submission_answers WHERE id IN ( ' . implode( ',', $intTestSubmissionAnswerIds ) . ' ) ', $objDatabase );
	}

	public static function fetchSimpleTestSubmissionAndQuestionsByTestSubmissionId( $intSubmissionId, $objDatabase ) {

		$strSql = 'SELECT
						tsa.test_question_id,
						tsa.points_granted,
						tq.maximum_points
					FROM
						test_submission_answers tsa
						JOIN test_questions tq ON tsa.test_question_id = tq.id
					WHERE
						test_submission_id = ' . ( int ) $intSubmissionId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestSubmissionQuestionsIdsByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						tsa.test_question_id
					FROM
						test_submission_answers tsa
						JOIN test_submissions ts ON ts.id = tsa.test_submission_id
					WHERE
						ts.employee_id = ' . ( int ) $intEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubmittedTestQuestionsDetailsByTestIds( $arrintTestIds, $objDatabase ) {
		if( false == valArr( $arrintTestIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						result.test_question_id,
						result.question,
						result.maximum_points,
						result.average,
						result.test_id,
						result.name,
						(
						SELECT
							count ( tsa.test_question_id )
						 FROM
							test_submission_answers tsa
							JOIN test_submissions ts ON ts.id = tsa.test_submission_id
							JOIN tests t ON t.id = ts.test_id
						 WHERE
							tsa.test_question_id = result.test_question_id
							AND tsa.submission_answer IS NULL
							AND tsa.test_answer_option_id IS NULL
							AND tsa.file_path IS NULL
							AND ts.test_id IN ( ' . implode( ',', $arrintTestIds ) . ' )
						) AS no_of_unattendies,
						(
						SELECT
							count ( tsa.test_question_id )
						FROM
							test_submission_answers tsa
							JOIN test_submissions ts ON ts.id = tsa.test_submission_id
							JOIN tests t ON t.id = ts.test_id
						 WHERE
							tsa.test_question_id = result.test_question_id
							AND ( tsa.submission_answer IS NOT NULL
							OR tsa.test_answer_option_id IS NOT NULL
							OR tsa.file_path IS NOT NULL )
							AND ts.test_id IN ( ' . implode( ',', $arrintTestIds ) . ' )
						) AS no_of_attendies,
						(
						 SELECT
							count ( tsa.test_question_id )
						 FROM
							test_submission_answers tsa
							JOIN test_submissions ts ON ts.id = tsa.test_submission_id
							JOIN tests t ON t.id = ts.test_id
						 WHERE
							( tsa.points_granted IS NULL OR tsa.points_granted = 0 )
							AND tsa.test_question_id = result.test_question_id
							AND ts.test_id IN ( ' . implode( ',', $arrintTestIds ) . ' )
						) AS count_of_failed_employees
					FROM
						(
						 SELECT
							DISTINCT tsa.test_question_id,
							tq.question,
							tq.maximum_points,
							ts.test_id,
							t.name,
							sum( points_granted ) / count( test_question_id ) AS average
						 FROM
							test_submission_answers tsa
							JOIN test_submissions ts ON ts.id = tsa.test_submission_id
							JOIN test_questions tq ON tq.id = tsa.test_question_id
							JOIN tests t ON( t.id = ts.test_id )
						 WHERE
							ts.test_id IN ( ' . implode( ',', $arrintTestIds ) . ' )' .
								'AND ts.graded_by IS NOT NULL
						 GROUP BY
							tq.question,
							tsa.test_question_id,
							tq.maximum_points,
							tq.question,
							ts.test_id,
							t.name
						) AS result';

		return fetchData( $strSql, $objDatabase );

	}

	public static function deleteSubmittedQuestionByEmployeeIdByTestIdByQuestionIds( $intEmployeeId, $intTestId, $arrintQuestionIds, $objDatabase ) {
		$strSql = 'DELETE FROM test_submission_answers WHERE test_submission_id = ( SELECT id FROM test_submissions WHERE test_id = ' . ( int ) $intTestId . ' AND employee_id = ' . ( int ) $intEmployeeId . ' ) AND test_question_id IN ( ' . implode( ',', $arrintQuestionIds ) . ' ) ';
		fetchData( $strSql, $objDatabase );
		return true;
	}

	public static function fetchAllAttemptedQuestionCountByTestSubmissinId( $intTestSubmissionId, $objDatabase ) {

		$strSql = 'SELECT
					count( tsa.test_question_id )
				From
					test_submission_answers tsa
				WHERE
					tsa.test_submission_id = ' . ( int ) $intTestSubmissionId;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		return 0;

	}

	public static function fetchAllTestSubmissionAnswersByTestId( $intTestId, $objDatabase ) {

		if( true == is_null( $intTestId ) ) return NULL;

		$strSql = 'SELECT
						tsa.*,
						ts.employee_id,
						ts.test_id,
						ts.score
					FROM
						test_submission_answers tsa
						LEFT JOIN test_submissions ts ON tsa.test_submission_id = ts.id
					WHERE
						ts.test_id = ' . ( int ) $intTestId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestSubmissionAnswersByEmployeeApplicationIds( $arrintEmployeeApplicationIds, $objDatabase, $boolTechnicalCodingTest = false, $boolInterviewTab = false ) {

		if( false == valArr( $arrintEmployeeApplicationIds ) ) {
			return false;
		}

		$strWhereClause		= ( true == $boolInterviewTab ) ? ( ( true == $boolTechnicalCodingTest ) ? ' AND tsa.additional_fields IS NOT NULL' : ' AND tsa.additional_fields IS NULL' ) : NULL;

		$strSql = 'SELECT
						DISTINCT ON ( ts.employee_application_id, tsa.created_on::DATE, ts.test_id )
						tsa.id,
						t.name,
						ts.score,
						ts.test_id,
						ts.employee_application_id,
						tsa.additional_fields,
						tsa.created_on,
						tsa.updated_on,
						tsa.points_granted,
						tsa.test_question_id
					FROM
						test_submission_answers AS tsa
						JOIN test_submissions AS ts ON ( ts.id = tsa.test_submission_id )
						JOIN tests AS t ON ( t.id = ts.test_id )
					WHERE
						ts.employee_application_id IN (' . implode( ',', $arrintEmployeeApplicationIds ) . ' )' . $strWhereClause . '
					GROUP BY
							tsa.created_on,
							ts.employee_application_id,
							tsa.id,
							ts.id,
							t.name
					ORDER BY
							tsa.created_on::DATE,
							ts.employee_application_id,
							ts.test_id,
							tsa.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
