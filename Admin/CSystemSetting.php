<?php

class CSystemSetting extends CBaseSystemSetting {

	const PORTALS_GOOGLE_ANALYTICS                    = 'PORTALS_GOOGLE_ANALYTICS';
	const RESIDENT_WORKS_GOOGLE_ANALYTICS             = 'RESIDENT_WORKS_GOOGLE_ANALYTICS';
	const CLIENT_ADMIN_GOOGLE_ANALYTICS               = 'CLIENT_ADMIN_GOOGLE_ANALYTICS';
	const DOLLAR_TO_RUPEE_EXCHANGE_RATE               = 'DOLLAR_TO_RUPEE_EXCHANGE_RATE';
	const PURCHASE_REQUEST_DOUBLE_ENCRYPTION_KEY      = 'PURCHASE_REQUEST_DOUBLE_ENCRYPTION_KEY';
	const KEY_COST_TO_COMPANY_AMOUNT_PUBLIC           = 'KEY_COST_TO_COMPANY_AMOUNT_PUBLIC';
	const KEY_COST_TO_COMPANY_AMOUNT_PRIVATE          = 'KEY_COST_TO_COMPANY_AMOUNT_PRIVATE';
	const SALES_TAX_LOGGING                           = 'SALES_TAX_LOGGING';
	const BOOL_REVENUE_INCREASED                      = 'BOOL_REVENUE_INCREASED';
	const BOOL_AVG_REV_PER_UNIT_INCREASED             = 'BOOL_AVG_REV_PER_UNIT_INCREASED';
	const LAST_MONTH_REVENUE                          = 'LAST_MONTH_REVENUE';
	const LAST_MONTH_NAME                             = 'LAST_MONTH_NAME';
	const AVG_REVENUE_PER_UNIT                        = 'AVG_REVENUE_PER_UNIT';
	const CURRENT_ADDRESS_CHANGE_SETTING              = 'CURRENT_ADDRESS_CHANGE_SETTING';
	const CURRENT_DEPENDENT_CHANGE_SETTING            = 'CURRENT_DEPENDENT_CHANGE_SETTING';
	const LAST_BIT_BUCKET_COMMIT_LOG_API_GMT_DATETIME = 'LAST_BIT_BUCKET_COMMIT_LOG_API_GMT_DATETIME';

	public static $c_arrstrLoginPageBannerKeys = [
		self::BOOL_REVENUE_INCREASED,
		self::BOOL_AVG_REV_PER_UNIT_INCREASED,
		self::LAST_MONTH_REVENUE,
		self::LAST_MONTH_NAME,
		self::AVG_REVENUE_PER_UNIT
	];

	public function valDoubleEncryptionKey( $strEnteredValue ) {

		$boolIsValid = true;

		if( true == is_null( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Secret key is required.' ) );
		}

		if( true == $boolIsValid && $strEnteredValue != $this->getValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Invalid secret key.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $strEnteredValue = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_key':
				$boolIsValid &= $this->valDoubleEncryptionKey( $strEnteredValue );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}

?>