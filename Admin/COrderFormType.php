<?php

class COrderFormType extends CBaseOrderFormType {

	const CHECK_SCANNER									= 1;
	const DOMAIN_ORDER_TRANSFER							= 2;
	const FLOOR_PLAN_PACKAGE							= 3;
	const CUSTOM_LOGO									= 4;
	const SITE_PLAN										= 5;
	const TEMPLATE_LOGO									= 6;
	const DISTRIBUTION_TYPE_CHANGE_REQUEST				= 7;
	const CHECK_SCANNING_MAX_PAYMENT_INCREASE_REQUEST	= 8;

	public static $c_arrintOrderFormNames = [
		self::CHECK_SCANNER									=> 'Check Scanner',
		self::DOMAIN_ORDER_TRANSFER							=> 'Domain Order/Transfer',
		self::FLOOR_PLAN_PACKAGE							=> 'Floor Plan Package',
		self::CUSTOM_LOGO									=> 'Custom Logo',
		self::SITE_PLAN										=> 'Site Plan',
		self::TEMPLATE_LOGO									=> 'Template Logo',
		self::DISTRIBUTION_TYPE_CHANGE_REQUEST				=> 'Distribution Type Change Request',
		self::CHECK_SCANNING_MAX_PAYMENT_INCREASE_REQUEST	=> 'Check Scanning Max Payment Increase Request',
	];

	public static $c_arrintOrderFormFolderNames = [
		self::CHECK_SCANNER									=> 'check_scanner_order_form',
		self::DOMAIN_ORDER_TRANSFER							=> 'domain_order_transfer_form',
		self::FLOOR_PLAN_PACKAGE							=> 'floor_plan_order_form',
		self::TEMPLATE_LOGO									=> 'template_logo_order_form',
		self::DISTRIBUTION_TYPE_CHANGE_REQUEST				=> 'distributed_change_request_order_form',
		self::CHECK_SCANNING_MAX_PAYMENT_INCREASE_REQUEST	=> 'check_scanner_max_payment_increase_order_form',
		self::CUSTOM_LOGO									=> 'custom_logo',
		self::SITE_PLAN										=> 'site_plan_order_form',
	];

	public static $c_arrintOrderFormChargeCodeIds = [
		self::CHECK_SCANNER									=> CChargeCode::CHECK_SCANNERS,
		self::DOMAIN_ORDER_TRANSFER							=> CChargeCode::DOMAIN_NAME_RENEWAL,
		self::FLOOR_PLAN_PACKAGE							=> CChargeCode::FLOOR_PLANS_3D,
		self::TEMPLATE_LOGO									=> CChargeCode::TEMPLATE_LOGO,
		self::SITE_PLAN										=> CChargeCode::SITE_PLAN,
		self::CUSTOM_LOGO									=> CChargeCode::CUSTOM_LOGO
	];

	public static $c_arrintPaymentEnabledOrderForms = [
		self::CHECK_SCANNER,
		self::FLOOR_PLAN_PACKAGE,
		self::TEMPLATE_LOGO,
		self::CUSTOM_LOGO,
		self::SITE_PLAN
	];

	public static $c_arrintOrderFormWithNoSalesTax = [
		self::CUSTOM_LOGO
	];

	public static $c_arrintPropertyAddressAssociatedOrderForms = [
		self::CHECK_SCANNER,
		self::SITE_PLAN,
		self::DISTRIBUTION_TYPE_CHANGE_REQUEST,
		self::CHECK_SCANNING_MAX_PAYMENT_INCREASE_REQUEST
	];

	public static $c_arrintFeedbackApplicableOrderForms = [
		self::CUSTOM_LOGO,
		self::FLOOR_PLAN_PACKAGE,
		self::TEMPLATE_LOGO,
		self::SITE_PLAN
	];

	public static $c_arrintMerchantServicesOrderForms = [
		self::DISTRIBUTION_TYPE_CHANGE_REQUEST,
		self::CHECK_SCANNING_MAX_PAYMENT_INCREASE_REQUEST
	];

	public static $c_arrintTempHideOrderForms = [
		self::SITE_PLAN
	];
}
?>