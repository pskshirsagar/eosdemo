<?php

class CDocumentTemplateComponent extends CBaseDocumentTemplateComponent {

	protected $m_arrobjDocumentTemplateComponents;
	protected $m_arrobjDocumentTemplateParameters;
	protected $m_arrstrSimpleDocumentTemplateParameters;

	/**
	 * Create Functions
	 */

	public function createDocumentComponent( $objDocument ) {

		$objDocumentComponent = new CDocumentComponent();

		$objDocumentComponent->setCid( $objDocument->getCid() );
		$objDocumentComponent->setDocumentId( $objDocument->getId() );
		$objDocumentComponent->setDocumentTemplateId( $this->getDocumentTemplateId() );
		$objDocumentComponent->setDocumentTemplateComponentId( $this->getId() );
		$objDocumentComponent->setComponentType( $this->getComponentType() );
		$objDocumentComponent->setSize( $this->getSize() );
		$objDocumentComponent->setOrderNum( $this->getOrderNum() );
		$objDocumentComponent->setIsModifiable( $this->getIsModifiable() );
		$objDocumentComponent->setIsRemovable( $this->getIsRemovable() );

		return $objDocumentComponent;
	}

	public function createDocumentTemplateParameter() {

		$objDocumentTemplateParameter = new CDocumentTemplateParameter();
		$objDocumentTemplateParameter->setDocumentTemplateId( $this->m_intDocumentTemplateId );
		$objDocumentTemplateParameter->setDocumentTemplateComponentId( $this->m_intId );

		return $objDocumentTemplateParameter;
	}

	/**
	 * Add Functions
	 */

	public function addDocumentTemplateComponent( $objDocumentTemplateComponent ) {
		$this->m_arrobjDocumentTemplateComponents[] = $objDocumentTemplateComponent;
	}

	public function addDocumentTemplateParameter( $objDocumentTemplateParameter ) {
		$this->m_arrobjDocumentTemplateParameters[] = $objDocumentTemplateParameter;
		$this->m_arrstrSimpleDocumentTemplateParameters[$objDocumentTemplateParameter->getKey()] = $objDocumentTemplateParameter->getValue();
	}

	/**
	 * Get Functions
	 */

	public function getDocumentTemplateComponents() {
		return $this->m_arrobjDocumentTemplateComponents;
	}

	public function getDocumentTemplateParameters() {
		return $this->m_arrobjDocumentTemplateParameters;
	}

	public function getDocumentComponents() {
		return $this->m_arrobjDocumentTemplateComponents;
	}

	public function getDocumentParameters() {
		return $this->m_arrobjDocumentTemplateParameters;
	}

	// This array stores an equally agnostic array of string data that prevents the use of if isset statements
	// in the grid engine templates

	public function getSimpleDocumentParameters() {
		return $this->m_arrstrSimpleDocumentTemplateParameters;
	}

	/**
	 * Set Functions
	 */

	public function setDocumentTemplateComponents( $arrobjDocumentTemplateComponents ) {
		$this->m_arrobjDocumentTemplateComponents = $arrobjDocumentTemplateComponents;
	}

	public function setDocumentTemplateParameters( $arrobjDocumentTemplateParameters ) {
		$this->m_arrobjDocumentTemplateParameters = $arrobjDocumentTemplateParameters;
	}

}
?>