<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsEvents
 * Do not add any new functions to this class.
 */

class CPsEvents extends CBasePsEvents {

	public static function fetchPublishedResidentWorksPsEvents( $objDatabase, $intEventId = NULL, $strHelpPortalSystem = NULL ) {

		if( true == valStr( $strHelpPortalSystem ) && CHelpResource::ASSOCIATED_WITH_CLIENTADMIN == $strHelpPortalSystem ) {
			$strWhereClause = ' AND show_in_client_admin = true';
		} else {
			$strWhereClause = ' AND show_in_resident_works = 1';
		}

		if( true == valId( $intEventId ) ) {
			$strWhereClause .= ' AND id = ' . $intEventId . ' ';
		}

		$strSql = 'SELECT
						*
					FROM
						ps_events
					WHERE
						end_datetime >= CURRENT_DATE
						AND deleted_on IS NULL
						' . $strWhereClause . '
					ORDER BY
						start_datetime';

		return self::fetchPsEvents( $strSql, $objDatabase );
	}

	public static function fetchPublishedWebsitePsEvents( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ps_events
					WHERE
						end_datetime >= CURRENT_DATE
						AND show_on_website = 1
						AND deleted_on IS NULL
					ORDER BY
						start_datetime';

		return self::fetchPsEvents( $strSql, $objDatabase );
	}

	public static function fetchCachedPublishedResidentWorksPsEventsAndAnnouncements( $objAdminDatabase ) {

		$strSql = 'SELECT *
					FROM
						ps_events
						WHERE ( ( end_datetime IS NULL AND ( start_datetime >= CURRENT_DATE OR start_datetime IS NULL ) ) OR end_datetime >= CURRENT_DATE )
						AND show_on_website = 1
						AND deleted_on IS NULL
					ORDER BY start_datetime';

		$strKey = 'arr_' . 'CPsEvent' . '_' . $strSql;

		if( false == CONFIG_CACHE_ENABLED || false === ( $arrobjPsEvents = CCache::fetchObject( $strKey ) ) ) {

			$objAdminDatabase->open();
			$arrobjPsEvents = self::fetchObjects( $strSql, 'CPsEvent', $objAdminDatabase );
			$objAdminDatabase->close();

			if( true == CONFIG_CACHE_ENABLED ) {
				CCache::storeObject( $strKey, $arrobjPsEvents, 3600 );
			}
		}
		return $arrobjPsEvents;
	}

	public static function fetchPaginatedSortedPsEventsWithPublishStatus( $strPublishFilter, $intPageNo, $intPageSize, $strOrderByFieldName, $strDirection, $boolShowDisabledData = 0, $objDatabase, $intPsEventTypeId = NULL, $boolShowCount = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strWhereClause = ( false == is_null( $intPsEventTypeId ) && true == is_numeric( $intPsEventTypeId ) ) ? 'ps_event_type_id = ' . ( int ) $intPsEventTypeId : 'ps_event_type_id <> ' . CPsEventType::ANNOUNCEMENT;

		switch( $strPublishFilter ) {
			case 'unpublished':
				$strWhereClause .= ' AND ( show_in_newsletter = 0 AND show_in_resident_works = 0 AND show_on_website = 0 AND show_in_client_admin = false )';
				break;

			case 'published':
				$strWhereClause .= ' AND  ( show_in_newsletter = 1 OR show_in_resident_works = 1 OR show_on_website = 1 OR show_in_client_admin = true ) ';
				break;

			default:
				// Empty Default
				break;
		}

		$strSelectClause = ( true == $boolShowCount ) ? 'id ' : ' *, CASE WHEN ( true = show_in_client_admin OR 1 = show_in_resident_works OR 1 = show_in_newsletter OR 1 = show_on_website ) THEN 1 ELSE 0 END AS is_published ';
		$strSql = ' SELECT ' . $strSelectClause . '
					FROM
						ps_events
					WHERE
						' . $strWhereClause . ( ( 0 == $boolShowDisabledData ) ? ' AND deleted_on IS NULL ' : '' );

		if( false == $boolShowCount ) {
			$strSql .= ' ORDER BY ' . $strOrderByFieldName . ' ' . $strDirection . ' OFFSET ' . ( int ) $intOffset;
			if( true == isset( $intLimit ) && 0 < $intLimit ) {
				$strSql .= '  LIMIT	' . ( int ) $intLimit;
			}
		}

		return self::fetchPsEvents( $strSql, $objDatabase );
	}

	public static function fetchClientAdminPsEventsAndAnnouncements( $objAdminDatabase, $boolEventOnly = false ) {

		$arrintExcludedPsEventTypes = CPsEventType::$c_arrintExcludedPsEventTypes;

		if( true == $boolEventOnly ) {
			array_push( $arrintExcludedPsEventTypes, CPsEventType::ANNOUNCEMENT );
		}

		$strDataLimit = ( true == $boolEventOnly ) ? 'LIMIT 5' : '';

		$strSql = 'SELECT *
					FROM
						ps_events
					WHERE ( ( end_datetime IS NULL AND ( start_datetime >= CURRENT_DATE OR start_datetime IS NULL ) ) OR end_datetime >= CURRENT_DATE )
						AND deleted_on IS NULL
						AND show_in_client_admin = true
						AND ps_event_type_id NOT IN ( ' . implode( ',', $arrintExcludedPsEventTypes ) . ' )
					ORDER BY start_datetime ' . $strDataLimit;

		$strKey = 'arr_' . 'CPsEvent' . '_' . $strSql;

		if( false == CONFIG_CACHE_ENABLED || false === ( $arrobjPsEvents = CCache::fetchObject( $strKey ) ) ) {
			$arrobjPsEvents = self::fetchObjects( $strSql, 'CPsEvent', $objAdminDatabase );
			if( true == CONFIG_CACHE_ENABLED ) {
				CCache::storeObject( $strKey, $arrobjPsEvents, 3600 );
			}
		}

		return $arrobjPsEvents;
	}

}
?>