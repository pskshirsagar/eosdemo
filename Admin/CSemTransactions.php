<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemTransactions
 * Do not add any new functions to this class.
 */

class CSemTransactions extends CBaseSemTransactions {

	public static function fetchSemTransactionByCompanyPaymentId( $intCompanyPaymentId, $objAdminDatabase ) {
		return self::fetchSemTransaction( 'SELECT * FROM sem_transactions WHERE company_payment_id = ' . ( int ) $intCompanyPaymentId . ' LIMIT 1', $objAdminDatabase );
	}

	public static function fetchLatestSemTransactionByRemotePrimaryKeyBySemAdGroupId( $strRemotePrimaryKey, $intSemAdGroupId, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM sem_transactions WHERE sem_ad_group_id = ' . ( int ) $intSemAdGroupId . ' AND remote_primary_key = \'' . ( string ) addslashes( $strRemotePrimaryKey ) . '\' AND transaction_datetime >= ( NOW() - INTERVAL \'1 day\' ) LIMIT 1';
		return self::fetchSemTransaction( $strSql, $objAdminDatabase );
	}

	public static function fetchSemTransactionsBySemSourceIdByDate( $intSemSourceId, $strTransactionDate, $objAdminDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						sem_transactions
					WHERE
						to_char( transaction_datetime, \'MM/DD/YYYY\' )::date = \'' . $strTransactionDate . '\'::date
						AND
						sem_source_id  = ' . ( int ) $intSemSourceId . '
						AND
						is_internal <> 1
						AND
						is_reconciled = 0
						AND
						transaction_amount > 0
						AND
						company_payment_id IS NULL';

		return self::fetchSemTransactions( $strSql, $objAdminDatabase );
	}

	public static function buildSemTransactionSearchCriteria( $objSemTransactionsFilter ) {

		$arrstrWhereParameters = array();

		// Create SQL parameters.
		( false == isset( $objSemTransactionsFilter ) || true == is_null( $objSemTransactionsFilter->getCid() ) || false == $objSemTransactionsFilter->getCid() )	?  false : array_push( $arrstrWhereParameters, 'cid=' . $objSemTransactionsFilter->getCid() );
		( false == isset( $objSemTransactionsFilter ) || true == is_null( $objSemTransactionsFilter->getPropertyId() ) || false == $objSemTransactionsFilter->getPropertyId() )						?  false : array_push( $arrstrWhereParameters, 'property_id=' . $objSemTransactionsFilter->getPropertyId() );
		if( 0 < \Psi\Libraries\UtilFunctions\count( $objSemTransactionsFilter->getSemSources() ) ) 	$arrstrWhereParameters[] = 'sem_source_id IN ( ' . $objSemTransactionsFilter->getSemSources() . ')';
		( false == isset( $objSemTransactionsFilter ) || true == is_null( $objSemTransactionsFilter->getSemKeywordId() ) || false == $objSemTransactionsFilter->getSemKeywordId() )	?  false : array_push( $arrstrWhereParameters, 'sem_keyword_id=' . $objSemTransactionsFilter->getSemKeywordId() );
		if( false == is_null( $objSemTransactionsFilter->getSemTransactionId() ) ) $arrstrWhereParameters[] = 'id = \'' . ( int ) $objSemTransactionsFilter->getSemTransactionId() . '\'';
		if( false == is_null( $objSemTransactionsFilter->getFromDate() ) )			$arrstrWhereParameters[] = 'transaction_datetime >= \'' . date( 'Y-m-d', strtotime( $objSemTransactionsFilter->getFromDate() ) ) . ' 00:00:00\'';
		if( false == is_null( $objSemTransactionsFilter->getToDate() ) )			$arrstrWhereParameters[] = 'transaction_datetime <= \'' . date( 'Y-m-d', strtotime( $objSemTransactionsFilter->getToDate() ) ) . ' 23:59:59\'';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			return $arrstrWhereParameters;
		} else {
			return NULL;
		}
	}

	/**
	 * Function for calculate the total cost in between StartDate and EndDate
	 *
	 *	$intCid required integer
	 *	$arrintPropertyIds required integer
	 *	StartDate and EndDate required String(mm/dd/yyyy)
	 *	$objAdminDatabase required
	 *
	 *	returns the object for the sem transaction
	 */

	public static function fetchTotalCostOnSemTransactionsByStastisticsEmailFilter( $objStastisticsEmailFilter, $objAdminDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) )
			|| ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = 'SELECT
						property_id,
						sum( transaction_amount ) as transaction_amount
					FROM
						sem_transactions st
					WHERE
						cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
					AND
						property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					AND
						( is_internal <> 0  OR ( sem_ad_group_id IS NOT NULL
					AND
						sem_keyword_id IS NOT NULL ) )
					AND
						transaction_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date
					AND
						transaction_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
					GROUP BY
						property_id
					ORDER BY
						property_id';

		 return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchLatestSemTransactionCountByRemotePrimaryKeyBySemAdGroupId( $strRemotePrimaryKey, $intSemAdGroupId, $objAdminDatabase ) {
		$strWhere = 'WHERE sem_ad_group_id = ' . ( int ) $intSemAdGroupId . ' AND remote_primary_key = \'' . ( string ) addslashes( $strRemotePrimaryKey ) . '\' AND transaction_datetime >= ( NOW() - INTERVAL \'1 day\' )';
		return self::fetchSemTransactionCount( $strWhere, $objAdminDatabase );
	}
}
?>