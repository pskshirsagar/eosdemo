<?php

use Psi\Eos\Admin\CClients;
use Psi\Eos\Connect\CDatabases;
use Psi\Eos\Admin\CDelinquencyLogs;
use Psi\Eos\Admin\CPropertyDelinquencies;

class CPropertyDelinquency extends CBasePropertyDelinquency {

	const ZERO		= 0;
	const ONE		= 1;
	const TWO		= 2;

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Account Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		return true;
	}

	public function valDelinquencyLevelTypeId( $boolIsFromDelinquencyDashboard = false, $intNewPropertyDelinquencyId = NULL ) {
		$boolIsValid = true;

		if( '' == \Psi\CStringService::singleton()->preg_replace( '/\s+/', '', $intNewPropertyDelinquencyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Actual level is required.' ) );
		}

		if( true == $boolIsValid && true == $boolIsFromDelinquencyDashboard ) {

			if( $intNewPropertyDelinquencyId > CDelinquencyLevelType::LEVEL_FIVE ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Actual level should not be greater than ' . CDelinquencyLevelType::LEVEL_FIVE . '.' ) );
			}

		}

		return $boolIsValid;
	}

	public function valEligibleDelinquencyLevelTypeId() {
		return true;
	}

	public function valMaxDelinquencyLevelTypeId() {
		return true;
	}

	public function valDelinquencyThresholdAmount() {
		return true;
	}

	public function valDelinquencyChangeDate() {
		return true;
	}

	public function valEligibleDelinquencyChangeDate() {
		return true;
	}

	public function valDeliquencyDelayDays() {
		return true;
	}

	public function valDelayCount() {
		return true;
	}

	public function valDelayBy() {
		return true;
	}

	public function valCompletedOn() {
		return true;
	}

	public function valApprovedBy() {
		return true;
	}

	public function valApprovedOn() {
		return true;
	}

	public function validate( $strAction, $boolIsFromDelinquencyDashboard = false, $intNewPropertyDelinquencyId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAccountId();
				break;

			case 'validate_update_property_delinquency':
				$boolIsValid &= $this->valDelinquencyLevelTypeId( $boolIsFromDelinquencyDashboard, $intNewPropertyDelinquencyId );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function updateLastPrintedOnByAccountIds( $strAccountIds, $objDatabase ) {
		if( false == valStr( $strAccountIds ) ) {
			return NULL;
		}
		$strSql = 'UPDATE
						property_delinquencies pd
					SET
						last_printed_on = NOW ( )
					WHERE
						pd.account_id IN ( ' . $strAccountIds . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function validateAvailabilityOfClientDatabases( $arrintClientIds, $objConnectDatabase, $objAdminDatabase, $intDefaultDatabaseUserTypeId = CDatabaseUserType::CLIENT_ADMIN ) {

		$arrintInactiveAndActiveClientDetails = [];
		$arrintInactiveDBClientIds = [];

		if( true == valArr( $arrintClientIds ) ) {
			$arrstrClients = rekeyArray( 'database_id', CClients::createService()->fetchClientByIds( $arrintClientIds, $objAdminDatabase ), false, true, false );
			$arrobjClientDatabases = CDatabases::createService()->fetchClientDatabasesByCIdsByDatabaseUserTypeIdByDatabaseTypeId( $arrintClientIds, $intDefaultDatabaseUserTypeId, CDatabaseType::CLIENT, $objConnectDatabase );

			foreach( $arrstrClients as $intKeyDatabaseId => $arrmixClientDetails ) {
				if( false == valObj( $arrobjClientDatabases[$intKeyDatabaseId], 'CDatabase' ) ||
				    ( true == valObj( $arrobjClientDatabases[$intKeyDatabaseId], 'CDatabase' ) && true == getIsDbOnMaintenance( $arrobjClientDatabases[$intKeyDatabaseId]->getId() ) ) ||
				    false == $arrobjClientDatabases[$intKeyDatabaseId]->validateAndOpen() ) {
					$arrintInactiveDBClientIds = array_merge( $arrintInactiveDBClientIds, array_values( array_column( $arrmixClientDetails, 'id' ) ) );
				}
			}

			if( true == valArr( $arrintInactiveDBClientIds ) ) {
				$arrstrInactiveDatabaseClients = CClients::createService()->fetchClientByIds( $arrintInactiveDBClientIds, $objAdminDatabase );
				$arrintInactiveAndActiveClientDetails['inactiveDB_client_names'] = implode( ', ', array_column( $arrstrInactiveDatabaseClients, 'company_name' ) );
			}
			$arrintInactiveAndActiveClientDetails['activeDB_clientids'] = array_diff( $arrintClientIds, $arrintInactiveDBClientIds );
		}

		return $arrintInactiveAndActiveClientDetails;
	}

	public static function sendConsolidateAccountDelinquencyNotification( $arrmixAccountDelinquency, $intUserId, $objAdminDatabase, $objEmailDatabase, $arrmixDelinquencyAccountDetails, $arrmixPreviousPropertyDelinquenciesData, $boolIsSendEmail, $boolIsFromDelinquencyDashboard = false, $intActionId = NULL ) {

		$arrobjDelinquecyLog = [];
		$arrmixAccountBasisAmountDue = [];
		$arrmixPropertyBasisAmountDue = [];

		if( true == valArr( $arrmixDelinquencyAccountDetails ) ) {
			$arrintAccountsIds = [];
			$arrintInvoiceIds = [];
			$fltTotalAccountsBalanceAmount = 0;
			$fltTotalAccountsAmountDue = 0;

			foreach( $arrmixDelinquencyAccountDetails as $intKey => $arrmixDelinquencyAccountDetail ) {

				if( false == in_array( $arrmixDelinquencyAccountDetail['account_id'], $arrintAccountsIds ) ) {
					$fltTotalAccountsBalanceAmount	+= $arrmixDelinquencyAccountDetail['total_account_balance_amount'];
				}

				if( date( 'Y/m/d', strtotime( $arrmixDelinquencyAccountDetail['invoice_due_date'] ) ) < date( 'Y/m/d' ) && false == in_array( $arrmixDelinquencyAccountDetail['invoice_id'], $arrintInvoiceIds ) ) {
					$fltTotalAccountsAmountDue += $arrmixDelinquencyAccountDetail['total_amount_due'];
				}

				$arrintAccountsIds[] = $arrmixDelinquencyAccountDetail['account_id'];
				$arrintPropertyIds[] = $arrmixDelinquencyAccountDetail['property_id'];

				if( true == in_array( $arrmixDelinquencyAccountDetail['account_id'], $arrintAccountsIds ) && false == in_array( $arrmixDelinquencyAccountDetail['invoice_id'], $arrintInvoiceIds ) ) {
					$arrmixAccountBasisAmountDue[$arrmixDelinquencyAccountDetail['account_id']] += $arrmixDelinquencyAccountDetail['total_amount_due'];
				}

				$arrintInvoiceIds[] = $arrmixDelinquencyAccountDetail['invoice_id'];

				if( true == in_array( $arrmixDelinquencyAccountDetail['account_id'], $arrintAccountsIds ) && true == in_array( $arrmixDelinquencyAccountDetail['property_id'], $arrintPropertyIds ) ) {
					$arrmixPropertyBasisAmountDue[$arrmixDelinquencyAccountDetail['account_id']][$arrmixDelinquencyAccountDetail['property_id']] += $arrmixDelinquencyAccountDetail['total_amount_due'];
				}

				if( true == valArr( $arrobjDelinquecyLog ) && true == array_key_exists( $arrmixDelinquencyAccountDetail['property_delinquencies_id'], $arrobjDelinquecyLog ) ) {
					$objDelinquencyLog = getArrayElementByKey( $arrmixDelinquencyAccountDetail['property_delinquencies_id'], $arrobjDelinquecyLog );
					$fltAmountOwedByProperty = $arrobjDelinquecyLog[$arrmixDelinquencyAccountDetail['property_delinquencies_id']]->getOverdueAmount() + $arrmixDelinquencyAccountDetail['total_amount_due'];
					$strInvoiceIds = $arrobjDelinquecyLog[$arrmixDelinquencyAccountDetail['property_delinquencies_id']]->getInvoiceIds() . ',' . $arrmixDelinquencyAccountDetail['invoice_id'];
					$arrobjDelinquecyLog[$arrmixDelinquencyAccountDetail['property_delinquencies_id']]->setOverdueAmount( $fltAmountOwedByProperty );
					$arrobjDelinquecyLog[$arrmixDelinquencyAccountDetail['property_delinquencies_id']]->setInvoiceIds( $strInvoiceIds );
				} else {
					$objDelinquencyLog = new CDelinquencyLog();
					$objDelinquencyLog->setOverdueAmount( $arrmixDelinquencyAccountDetail['total_amount_due'] );
					$objDelinquencyLog->setInvoiceIds( $arrmixDelinquencyAccountDetail['invoice_id'] );
				}

				if( true == valArr( $arrmixPreviousPropertyDelinquenciesData ) ) {
					$arrmixPreviousPropertyDelinquenciesData	= rekeyArray( 'account_id', $arrmixPreviousPropertyDelinquenciesData );
					$arrintAccountIds							= array_column( $arrmixPreviousPropertyDelinquenciesData, 'account_id' );
					$strSystemNote = '';
					if( true == in_array( $arrmixDelinquencyAccountDetail['account_id'], $arrintAccountIds ) ) {
						if( $arrmixDelinquencyAccountDetail['delinquency_level_type_id'] > $arrmixPreviousPropertyDelinquenciesData[$arrmixDelinquencyAccountDetail['account_id']]['delinquency_level_type_id'] ) {
							$strSystemNote = 'Delinquency Level changed.';
						} elseif( $arrmixDelinquencyAccountDetail['delinquency_level_type_id'] == $arrmixPreviousPropertyDelinquenciesData[$arrmixDelinquencyAccountDetail['account_id']]['delinquency_level_type_id'] ) {
							$strSystemNote = 'Delinquency Level not changed.';
						}
					}
					$arrmixDelinquencyAccountDetail['system_notes'] = $strSystemNote;
				}

				$arrobjDelinquecyLog[$arrmixDelinquencyAccountDetail['property_delinquencies_id']] = $objDelinquencyLog->createConsolidateDelinquencyLog( $arrmixDelinquencyAccountDetail );
				$arrmixDelinquencyAccountsInvoiceDetails[$arrmixDelinquencyAccountDetail['account_id'] . '^' . $arrmixDelinquencyAccountDetail['account_name']][$arrmixDelinquencyAccountDetail['property_id'] . '^' . $arrmixDelinquencyAccountDetail['property_name'] . '^' . $arrmixDelinquencyAccountDetail['last_payment_date']][$arrmixDelinquencyAccountDetail['invoice_number']] = $arrmixDelinquencyAccountDetail;
			}
		}

		if( $boolIsSendEmail == true ) {

			$arrmixTemplateParameters = [];
			$arrmixTemplateParameters = CDelinquencyLevelType::assignTemplateConstants( $arrmixTemplateParameters );
			$arrmixTemplateParameters['account_details'] = $arrmixDelinquencyAccountsInvoiceDetails;
			$arrmixTemplateParameters['account_delinquency'] = $arrmixAccountDelinquency;
			$arrmixTemplateParameters['account_basis_amount_due'] = $arrmixAccountBasisAmountDue;
			$arrmixTemplateParameters['property_basis_amount_due'] = $arrmixPropertyBasisAmountDue;
			$arrmixTemplateParameters['total_accounts_amount_due'] = $fltTotalAccountsAmountDue;
			$arrmixTemplateParameters['total_account_balance_amount'] = $fltTotalAccountsBalanceAmount;
			$arrmixTemplateParameters['image_path'] = CONFIG_COMMON_PATH . '/images/resident_invoice/entrata_ps_logo_large.jpg';
			$arrmixTemplateParameters['payment_url'] = CConfig::get( 'secure_host_prefix' ) . $arrmixAccountDelinquency['rwx_domain'] . CConfig::get( 'rwx_login_suffix' ) . '/?module=billing_accountsxxx';
			$arrmixTemplateParameters['is_email_content'] = true;
			$arrmixTemplateParameters['LEGAL_EMAIL_ADDRESS'] = CSystemEmail::LEGAL_EMAIL_ADDRESS;
			$arrmixTemplateParameters['ENTRATA_BILLING_EMAIL_ADDRESS'] = CSystemEmail::ENTRATA_BILLING_EMAIL_ADDRESS;

			$objRenderTemplate		= new CRenderTemplate( 'delinquency_notifications_template/consolidate_delinquency_notification_email_letter.tpl', PATH_INTERFACES_COMMON, false, $arrmixTemplateParameters );

			$strHtmlContent	= $objRenderTemplate->nestedFetchTemplate( PATH_INTERFACES_COMMON . 'delinquency_notifications_template/consolidate_delinquency_notification_email_letter.tpl' );

			switch( $arrmixAccountDelinquency['delinquency_level_type_id'] ) {

				// Client and CSM
				case 1:
				case 2:
				case 3:
					$arrstrEmailAddresses	= [ $arrmixAccountDelinquency['billto_email_address'] . ( ( false == is_null( $arrmixAccountDelinquency['support_employee_email_address'] ) && $arrmixAccountDelinquency['support_employee_email_address'] != 'zzzaaamteam@prop12321test.net' ) ? ', ' . $arrmixAccountDelinquency['support_employee_email_address'] : '' ) ];
					$intSystemEmailType 	= CSystemEmailType::DELINQUENCY_NOTIFICATIONS;
					break;

				// Only Legal
				case 4:
				case 5:
					$arrstrEmailAddresses	= [ ( 'production' != CConfig::get( 'environment' ) ) ? 'legal@entrata.lcl' : CSystemEmail::LEGAL_EMAIL_ADDRESS ];
					$intSystemEmailType 	= CSystemEmailType::DELINQUENCY_CRUCIAL_NOTIFICATIONS;
					break;

				default:
					// Not needed.

			}

			if( $arrmixAccountDelinquency['delinquency_level_type_id'] == CDelinquencyLevelType::LEVEL_THREE ) {
				$strFromEmailAddress = CSystemEmail::LEGAL_EMAIL_ADDRESS;
			} else {
				$strFromEmailAddress = CSystemEmail::ENTRATA_BILLING_EMAIL_ADDRESS;
			}

			$strToEmailAddress		= implode( ', ', $arrstrEmailAddresses );

			$strSubject				= 'Request for payment on past-due ' . $arrmixAccountDelinquency['company_name'] . "'s account(s)";

			$objSystemEmailLibrary 	= new CSystemEmailLibrary();
			$objSystemEmail 		= $objSystemEmailLibrary->prepareSystemEmail( $intSystemEmailType, $strSubject, $strHtmlContent, $strToEmailAddress, $intIsSelfDestruct = 0, $strFromEmailAddress );

			$objSystemEmail->setCid( $arrmixAccountDelinquency['cid'] );

			if( false == is_null( $arrmixAccountDelinquency['billing_employee_email_address'] ) ) {
				$objSystemEmail->setReplyToEmailAddress( $strFromEmailAddress . ',' . $arrmixAccountDelinquency['billing_employee_email_address'] );
			}

			if( false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
				return [ 'boolResponse' => false, 'message' => 'Failed to insert system email for ' . $arrmixAccountDelinquency['cid'] . 'and account ids ' . $arrmixAccountDelinquency['account_ids'] ];
			}

			$intSystemEmailId = $objSystemEmail->getId();

			array_walk( $arrobjDelinquecyLog, function( &$objDelinquecyLog, $strKey ) use( $intSystemEmailId, $intActionId, $boolIsFromDelinquencyDashboard, $arrmixAccountDelinquency ) {

				$objDelinquecyLog->setSystemEmailId( $intSystemEmailId );

				if( true == $boolIsFromDelinquencyDashboard ) {
					$objDelinquecyLog->setDelinquencyLevelTypeId( $arrmixAccountDelinquency['delinquency_level_type_id'] );
				}
			} );

			if( false == $boolIsFromDelinquencyDashboard ) {
				$intUserId = CUser::ID_SYSTEM;
			}
			$strSql = 'UPDATE public.property_delinquencies SET last_notification_send_on = NOW() where account_id IN (' . implode( ', ', $arrintAccountsIds ) . ')';

			if( false == fetchData( $strSql, $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();
				return [ 'boolResponse' => false, 'message' => 'Failed to update last notification send on field.' ];
			}
		}
		if( false == CDelinquencyLogs::createService()->bulkInsert( $arrobjDelinquecyLog, $intUserId, $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();
			return [ 'boolResponse' => false, 'message' => 'Failed to insert delinquency logs.' ];
		}
		return [ 'boolResponse' => true, 'message' => NULL ];
	}

	public static function handleSendFinalNotificationForEachProperty( $intUserId, $intAccountId, $intNotificationsLevelFourClient, $objAdminDatabase, $objEmailDatabase ) {
		$arrmixPassDueInvoices		 = [];
		$arrobjDelinquencyLog		 = [];

		$arrmixPropertyDelinquencies = CPropertyDelinquencies::createService()->fetchPropertyDelinquenciesDetailsForLevelFour( $objAdminDatabase, $intAccountId );
		if( valArr( $arrmixPropertyDelinquencies ) ) {
			$arrintAccountIds		 = array_column( $arrmixPropertyDelinquencies, 'account_id' );
			$arrmixPassDueInvoices	 = \Psi\Eos\Admin\CTransactions::createService()->fetchPassDueInvoiceByAccountIds( $arrintAccountIds, $objAdminDatabase );
			$arrmixPassDueInvoices	 = rekeyArray( 'account_id', $arrmixPassDueInvoices );
		} else {
			return [ 'boolResponse' => true, 'message' => NULL ];
		}

		$arrintAccountIdsCount = array_count_values( $arrintAccountIds );

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$arrintPropertyIds					= [];
		$arrmixBulkInsertDelinquencyLogs	= [];

		foreach( $arrmixPropertyDelinquencies as $arrmixPropertyDelinquency ) {
			$arrintPropertyIds[$arrmixPropertyDelinquency['account_id']][$arrmixPropertyDelinquency['property_id']] = $arrmixPropertyDelinquency['property_name'];
			$arrmixBulkInsertDelinquencyLogs[$arrmixPropertyDelinquency['property_id']]['invoice_ids'] = $arrmixPassDueInvoices[$arrmixPropertyDelinquency['account_id']]['invoice_ids'];
			$arrmixBulkInsertDelinquencyLogs[$arrmixPropertyDelinquency['property_id']]['property_delinquencies'] = $arrmixPropertyDelinquency;

			if( \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds[$arrmixPropertyDelinquency['account_id']] ) == $arrintAccountIdsCount[$arrmixPropertyDelinquency['account_id']] ) {

				$arrmixEmailTemplateParameters = [];

				$arrmixEmailTemplateParameters['properties_delinquency'] = $arrmixPropertyDelinquency;
				$arrstrPropertyNamesWithSerialNumber = array_values( $arrintPropertyIds[$arrmixPropertyDelinquency['account_id']] );
				$arrmixEmailTemplateParameters['property_names'] = $arrstrPropertyNamesWithSerialNumber;
				$arrmixEmailTemplateParameters['property_count'] = \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds[$arrmixPropertyDelinquency['account_id']] );
				$arrmixEmailTemplateParameters['pass_due_invoices'] = $arrmixPassDueInvoices;
				$arrmixEmailTemplateParameters['LEGAL_EMAIL_ADDRESS'] = CSystemEmail::LEGAL_EMAIL_ADDRESS;
				$arrmixEmailTemplateParameters['image_path'] = CONFIG_COMMON_PATH . '/images/resident_invoice/entrata_ps_logo_large.jpg';
				$arrmixEmailTemplateParameters['payment_url'] = CConfig::get( 'secure_host_prefix' ) . $arrmixPropertyDelinquency['rwx_domain'] . CConfig::get( 'rwx_login_suffix' ) . '/?module=billing_accountsxxx';
				$arrmixEmailTemplateParameters['logo_url'] = CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG;

				$objRenderTemplate		= new CRenderTemplate( 'email_templates/send_notification_for_delinquency_level_four/delinquency_level_four_for_client.tpl', PATH_INTERFACES_COMMON, false, $arrmixEmailTemplateParameters );

				$strHtmlContent	= $objRenderTemplate->nestedFetchTemplate( PATH_INTERFACES_COMMON . 'email_templates/send_notification_for_delinquency_level_four/delinquency_level_four_for_client.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

				$strSubject						= 'Request for payment on past-due ' . $arrmixPropertyDelinquency['account_name'];
				$strBillToEmailAddress			= ( false == is_null( $arrmixPropertyDelinquency['delinquency_notification_email_address'] ) ) ? $arrmixPropertyDelinquency['delinquency_notification_email_address'] : $arrmixPropertyDelinquency['billto_email_address'];
				$strPrimaryBillToEmailAddress	= ( false == is_null( $arrmixPropertyDelinquency['primary_delinquency_notification_email_address'] ) ) ? $arrmixPropertyDelinquency['primary_delinquency_notification_email_address'] : $arrmixPropertyDelinquency['primary_bill_to_email_address'];
				$strToEmailAddress				= ( false == is_null( $strBillToEmailAddress ) ) ? $strBillToEmailAddress : $strPrimaryBillToEmailAddress;

				$objSystemEmailLibrary			= new CSystemEmailLibrary();
				$objSystemEmail					= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::DELINQUENCY_CRUCIAL_NOTIFICATIONS, $strSubject, $strHtmlContent, $strToEmailAddress, $intIsSelfDestruct = 0, CSystemEmail::LEGAL_EMAIL_ADDRESS );
				$objSystemEmail->setCid( $arrmixPropertyDelinquency['cid'] );

				if( false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
					return [ 'boolResponse' => false, 'message' => 'Failed to send level 4 Notification.' ];
				} else {
					$intNotificationsLevelFourClient++;
				}
				$arrintPropertyIds[$arrmixPropertyDelinquency['account_id']]['system_email_id'] = $objSystemEmail->getId();
			}
		}

		if( true == valArr( $arrmixBulkInsertDelinquencyLogs ) ) {
			foreach( $arrmixBulkInsertDelinquencyLogs as $arrmixBulkInsertDelinquencyLog ) {
				$objDelinquencyLog = new CDelinquencyLog();
				$objDelinquencyLog->createConsolidateDelinquencyLog( $arrmixBulkInsertDelinquencyLog['property_delinquencies'] );
				$objDelinquencyLog->setSystemEmailId( $arrintPropertyIds[$arrmixBulkInsertDelinquencyLog['property_delinquencies']['account_id']]['system_email_id'] );
				$objDelinquencyLog->setOverdueAmount( $arrmixBulkInsertDelinquencyLog['property_delinquencies']['total_amount_due'] );
				$objDelinquencyLog->setInvoiceIds( $arrmixPassDueInvoices[$arrmixBulkInsertDelinquencyLog['invoice_ids']] );
				$objDelinquencyLog->setSystemNote( 'Final Notification for Level 4.' );
				$arrobjDelinquencyLog[] = $objDelinquencyLog;
			}
		}
		if( true == valArr( $arrobjDelinquencyLog ) && false == CDelinquencyLogs::createService()->bulkInsert( $arrobjDelinquencyLog, $intUserId, $objAdminDatabase, false ) ) {
			return [ 'boolResponse' => false, 'message' => 'Failed to insert log for level 4 Notification.' ];
		}
		return [ 'boolResponse' => true, 'message' => 'Final Notification sent.' ];
	}

	public static function updatePropertyPreferences( $intUserId, $objAdminDatabase, $objConnectDatabase, $arrintCids = NULL, $arrintAccountIds = NULL, $intDefaultDatabaseUserTypeId = NULL ) {

		$arrmixPropertyDelinquenciesLevel = CPropertyDelinquencies::createService()->fetchAllPropertyDelinquenciesLevel( $objAdminDatabase, $arrintCids, $arrintAccountIds );

		if( valArr( $arrmixPropertyDelinquenciesLevel ) ) {
			$arrintClientProperties = rekeyArray( 'cid', $arrmixPropertyDelinquenciesLevel, false, true );

			if( !valId( $intDefaultDatabaseUserTypeId ) ) {
				$intDefaultDatabaseUserTypeId = $objAdminDatabase->getDatabaseUserTypeId();
			}

			if( CDatabaseUserType::CLIENT_ADMIN == $objAdminDatabase->getDatabaseUserTypeId() || CDatabaseUserType::INTERNAL_SALES_ACCOUNTING == $objAdminDatabase->getDatabaseUserTypeId() ) {
				$intUserId = CCompanyUser::CLIENT_ADMIN;
			}

			foreach( $arrintClientProperties as $intClientId => $arrmixClientProperty ) {

				$arrobjClientDatabases = CDatabases::createService()->fetchClientDatabasesByCIdByDatabaseUserTypeIdByDatabaseTypeId( $intClientId, $intDefaultDatabaseUserTypeId, CDatabaseType::CLIENT, $objConnectDatabase );

				if( false == valArr( $arrobjClientDatabases ) ) {
					continue;
				}

				$objClientDatabase = current( $arrobjClientDatabases );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) || ( true == valObj( $objClientDatabase, 'CDatabase' ) && true == getIsDbOnMaintenance( $objClientDatabase->getId() ) ) ) {
					continue;
				}

				if( false == $objClientDatabase->validateAndOpen() ) {
					continue;
				}

				$arrintPropertiesLevel = array_column( $arrmixClientProperty, 'delinquency_level_type_id', 'property_id' );

				$arrobjExistedPropertiesPreference = ( array ) \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdsByCid( CPropertyPreference::DELINQUENCY_LEVEL, array_keys( $arrintPropertiesLevel ), $intClientId, $objClientDatabase );
				$arrobjExistedPropertiesPreference = rekeyObjects( 'PropertyId', $arrobjExistedPropertiesPreference );

				$arrintNewPropertiesPreference = ( true == valArr( $arrobjExistedPropertiesPreference ) ) ? array_diff_key( $arrintPropertiesLevel, $arrobjExistedPropertiesPreference ) : $arrintPropertiesLevel;

				$strSql = '';
				foreach( $arrintNewPropertiesPreference as $intPropertyId	=> $intPropertyDelinquencyLevelTypeId ) {
					if( CDelinquencyLevelType::LEVEL_ZERO != $intPropertyDelinquencyLevelTypeId ) {
						$strSql .= 'INSERT INTO public.property_preferences
									(
										cid,
										property_id,
										property_preference_type_id,
										key,
										value,
										updated_by,
										updated_on,
										created_by,
										created_on
									) VALUES (
										' . ( int ) $intClientId . ',
										' . ( int ) $intPropertyId . ',
										' . CPropertyPreferenceType::STANDARD . ',
										\'' . CPropertyPreference::DELINQUENCY_LEVEL . '\',
										' . ( int ) $intPropertyDelinquencyLevelTypeId . ',
										' . ( int ) $intUserId . ',
										NOW(),
										' . ( int ) $intUserId . ',
										NOW()
									);';
						unset( $arrintPropertiesLevel[$intPropertyId] );
					}
				}

				$objClientDatabase->begin();
				if( valArr( $arrintPropertiesLevel ) ) {
					$arrintMinDelinquencyLevelByPropertyIds = rekeyArray( 'property_id', CPropertyDelinquencies::createService()->fetchMinDelinquencyLevelByCidByPropertyIds( $intClientId, array_keys( rekeyArray( 'property_id', $arrmixClientProperty ) ), $objAdminDatabase ) );
					if( valArr( $arrintMinDelinquencyLevelByPropertyIds ) ) {
						foreach( $arrintPropertiesLevel as $intPropertyId => $intPropertyLevelTypeId ) {
							$intMinimumDelinquencyLevel = $arrintMinDelinquencyLevelByPropertyIds[$intPropertyId]['delinquency_level_type_id'];
							$strSql .= 'UPDATE public.property_preferences SET value = ' . ( int ) $intMinimumDelinquencyLevel . ', updated_by = ' . ( int ) $intUserId . ', updated_on = NOW() WHERE key = \'' . CPropertyPreference::DELINQUENCY_LEVEL . '\' AND cid = ' . ( int ) $intClientId . ' AND property_id = ' . ( int ) $intPropertyId . ';';
						}
					} else {
						foreach( $arrintPropertiesLevel as $intPropertyId => $intPropertyLevelTypeId ) {
							if( CDelinquencyLevelType::LEVEL_ZERO == $intPropertyLevelTypeId ) {
								$strSql .= 'UPDATE public.property_preferences SET value = ' . CDelinquencyLevelType::LEVEL_ZERO . ', updated_by = ' . ( int ) $intUserId . ', updated_on = NOW() WHERE key = \'' . CPropertyPreference::DELINQUENCY_LEVEL . '\' AND cid = ' . ( int ) $intClientId . ' AND property_id = ' . ( int ) $intPropertyId . ';';
							}
						}
					}
				}

				if( valStr( $strSql ) && !$objClientDatabase->execute( $strSql ) ) {
					$objClientDatabase->rollback();
					return false;
				}

				$strDeleteSql = 'DELETE FROM public.property_preferences WHERE cid = ' . ( int ) $intClientId . ' AND key = \'' . CPropertyPreference::DELINQUENCY_LEVEL . '\' AND value = \'' . CDelinquencyLevelType::LEVEL_ZERO . '\';';

				if( false == $objClientDatabase->execute( $strDeleteSql ) ) {
					$objClientDatabase->rollback();
					return false;
				}

				$objClientDatabase->commit();
				$objClientDatabase->close();
			}
		}

		return true;
	}

	public static function resetPropertyDelinquency( $arrmixAccounts, $arrmixPaymentDetails, $objAdminDatabase, $objConnectDatabase, $intUserId = CUser::ID_SYSTEM ) {

		$arrintCIds					= [];
		$arrobjDelinquecyLog		= [];
		$arrobjDelinquecyLogDetails	= [];

		foreach( $arrmixAccounts as $arrmixAccount ) {
			if( false == valArr( $arrmixAccount ) ) {
				return true;
			}

			$arrmixProperties									= NULL;
			$intEligibleDelinquencyLevelTypeId					= CDelinquencyLevelType::LEVEL_ZERO;
			$intAccountId										= $arrmixAccount['account_id'];
			$arrintCIds[]										= $arrmixAccount['cid'];

			$arrmixProperties							= CPropertyDelinquencies::createService()->fetchAllPropertyDelinquenciesAmountDue( $objAdminDatabase, [ $intAccountId ], [ $arrmixAccount['cid'] ] );
			$arrmixPreviousPropertyDelinquenciesData	= CPropertyDelinquencies::createService()->fetchPropertyDelinquenciesDataByAccountId( $intAccountId, NULL, $objAdminDatabase );

			$arrintPropertyAmountDueSum = array_unique( array_column( $arrmixProperties, 'property_amount_due' ) );
			// if there is no due amount pending need to reset delinquency for account
			if( false == valArr( $arrmixProperties ) ) {
				$strSql = 'UPDATE public.property_delinquencies
						SET
							delinquency_level_type_id = ' . CDelinquencyLevelType::LEVEL_ZERO . ', eligible_delinquency_level_type_id = ' . CDelinquencyLevelType::LEVEL_ZERO . ', delinquency_change_date = NULL, eligible_delinquency_change_date = NULL, last_notification_send_on = NULL, approved_by = NULL, approved_on = NULL,
							updated_by = ' . ( int ) $intUserId . ', 
							updated_on = NOW()
						WHERE account_id =' . ( int ) $intAccountId . ' AND delinquency_level_type_id > ' . CDelinquencyLevelType::LEVEL_ZERO;
				if( false == valArr( executeSql( $strSql, $objAdminDatabase ) ) ) {
					return false;
				}
				$arrintAccountIds[$intAccountId] = $intAccountId;
			} else {
				// get all open credits
				$fltCreditAmountDue = \Psi\Eos\Admin\CAccounts::createService()->fetchAccountCreditsByCidByAccountId( $arrmixAccount['cid'], $intAccountId, $objAdminDatabase );

				// verify uncleared invoices with unallocated amount
				$arrobjUnClearedInvoices = [];
				if( true == valArr( $arrmixProperties ) && false == is_null( $fltCreditAmountDue ) ) {
					$fltUnAllocatedAmount = abs( $fltCreditAmountDue );
					foreach( $arrmixProperties as $arrmixProperty ) {
						$fltUnAllocatedAmount -= $arrmixProperty['amount_due'];
						if( $fltUnAllocatedAmount < 0 ) {
							$arrobjUnClearedInvoices[$arrmixProperty['invoice_id']] = $arrmixProperty;
						}
					}
				} else {
					$arrobjUnClearedInvoices = $arrmixProperties;
				}

				// find eligible delinqunecy based on invoice due date for oldest uncleared invoice
				if( true == valArr( $arrobjUnClearedInvoices ) ) {
					$arrmixPropertyDelinquency = current( $arrobjUnClearedInvoices );
					if( true == ( $arrmixPropertyDelinquency['delay_days'] >= 90 ) ) {
						$intEligibleDelinquencyLevelTypeId = CDelinquencyLevelType::LEVEL_FIVE;
					} elseif( true == in_array( $arrmixPropertyDelinquency['delay_days'], range( 75, 89 ) ) ) {
						$intEligibleDelinquencyLevelTypeId = CDelinquencyLevelType::LEVEL_FOUR;
					} elseif( true == in_array( $arrmixPropertyDelinquency['delay_days'], range( 60, 74 ) ) ) {
						$intEligibleDelinquencyLevelTypeId = CDelinquencyLevelType::LEVEL_THREE;
					} elseif( true == in_array( $arrmixPropertyDelinquency['delay_days'], range( 45, 59 ) ) ) {
						$intEligibleDelinquencyLevelTypeId = CDelinquencyLevelType::LEVEL_TWO;
					} elseif( true == in_array( $arrmixPropertyDelinquency['delay_days'], range( 30, 44 ) ) ) {
						$intEligibleDelinquencyLevelTypeId = CDelinquencyLevelType::LEVEL_ONE;
					}
				} else {
					$intEligibleDelinquencyLevelTypeId = CDelinquencyLevelType::LEVEL_ZERO;
				}

				$arrobjPropertyDelinquencies = CPropertyDelinquencies::createService()->fetchPropertyDelinquenciesByAccountId( $intAccountId, $objAdminDatabase );
				if( true == valArr( $arrobjPropertyDelinquencies ) ) {
					foreach( $arrobjPropertyDelinquencies as $objPropertyDelinquency ) {
						$objPropertyDelinquency->setPropertyAmountDue( $arrintPropertyAmountDueSum[0] );
						if( $objPropertyDelinquency->getDelinquencyThresholdAmount() > $objPropertyDelinquency->getPropertyAmountDue() ) {
							$intEligibleDelinquencyLevelTypeId = CDelinquencyLevelType::LEVEL_ZERO;
						}
						if( $objPropertyDelinquency->getDelinquencyLevelTypeId() > $intEligibleDelinquencyLevelTypeId ) {
							$objPropertyDelinquency->setDelinquencyLevelTypeId( $intEligibleDelinquencyLevelTypeId );
							$objPropertyDelinquency->setDelinquencyChangeDate( ( $intEligibleDelinquencyLevelTypeId == CDelinquencyLevelType::LEVEL_ZERO ) ? NULL : 'NOW' );
							if( $intEligibleDelinquencyLevelTypeId == CDelinquencyLevelType::LEVEL_ZERO ) {
								$objPropertyDelinquency->setEligibleDelinquencyLevelTypeId( CDelinquencyLevelType::LEVEL_ZERO );
								$objPropertyDelinquency->setEligibleDelinquencyChangeDate( NULL );
							}
						}
						if( false == $objPropertyDelinquency->update( $intUserId, $objAdminDatabase ) ) {
							return false;
						}
						$arrintAccountIds[$intAccountId] = $intAccountId;
					}
				}
			}

			$arrmixPropertyDelinquencyData				= CPropertyDelinquencies::createService()->fetchThresholdAmtAndTotalAmtDueByAccountId( $intAccountId, $objAdminDatabase, $arrintCIds );
			$arrmixPropertyDelinquencyDataByAccountId	= rekeyArray( 'account_id', $arrmixPropertyDelinquencyData );
			$arrmixPropertyDelinquencyDataByPropertyIds	= rekeyArray( 'property_id', $arrmixPropertyDelinquencyData );

			if( true == valArr( $arrmixPropertyDelinquencyDataByAccountId ) && true == valArr( $arrmixPaymentDetails ) ) {
				$objDelinquencyLog		= new CDelinquencyLog();

				if( 1 == $arrmixPaymentDetails[$intAccountId]['is_credit'] ) {
					if( true == isset( $arrmixPaymentDetails[$intAccountId]['notes'] ) && true == $arrmixPaymentDetails[$intAccountId]['notes'] ) {
						$strCreditUserNote = $arrmixPaymentDetails[$intAccountId]['notes'];
					} else {
						$strCreditUserNote = 'Credit Added of';
					}

					if( 0 <= $arrmixPaymentDetails[$intAccountId]['transaction_amount'] ) {
						$strAmount = ' $' . __( '{%f, 0, p:2;nots}', [ $arrmixPaymentDetails[$intAccountId]['transaction_amount'] ] );
					} else {
						$strAmount = ' $(' . __( '{%f, 0, p:2;nots}', [ ( -1 ) * $arrmixPaymentDetails[$intAccountId]['transaction_amount'] ] ) . ')';
					}

					$arrmixPropertyDelinquencyDataByAccountId[$intAccountId]['user_notes'] = $strCreditUserNote . $strAmount;

				} else {
					$arrmixPropertyDelinquencyDataByAccountId[$intAccountId]['system_notes'] = 'Payment made of $(' . __( '{%f, 0, p:2;nots}', [ abs( $arrmixPaymentDetails[$intAccountId]['transaction_amount'] ) ] ) . ')';
				}
				$arrmixPropertyDelinquencyDataByAccountId[$intAccountId]['property_id']					= '';
				$arrmixPropertyDelinquencyDataByAccountId[$intAccountId]['property_delinquencies_id']	= '';
				$arrobjDelinquecyLog[$intAccountId]														= $objDelinquencyLog->createConsolidateDelinquencyLog( $arrmixPropertyDelinquencyDataByAccountId[$intAccountId] );
				$arrobjDelinquecyLog[$intAccountId]->setInvoiceIds( $arrmixPaymentDetails[$intAccountId]['invoice_ids'] );
				$arrobjDelinquecyLog[$intAccountId]->setOverdueAmount( $arrmixPropertyDelinquencyDataByAccountId[$intAccountId]['total_amount_due'] );
			}

			if( true == valArr( $arrmixPreviousPropertyDelinquenciesData ) ) {
				$intPreviousDelinquencyLevelTypeId = implode( ',', array_unique( array_column( $arrmixPreviousPropertyDelinquenciesData, 'delinquency_level_type_id' ) ) );
			}
			$intCurrentDelinquencyLevelTypeId = $arrmixPropertyDelinquencyDataByAccountId[$intAccountId]['delinquency_level_type_id'];

			if( true == isset( $intPreviousDelinquencyLevelTypeId ) && true == isset( $intCurrentDelinquencyLevelTypeId ) && ( $intPreviousDelinquencyLevelTypeId != $intCurrentDelinquencyLevelTypeId ) ) {
				// fetching property delinquency details by account ids
				if( true == valArr( $arrmixPropertyDelinquencyDataByPropertyIds ) ) {
					foreach( $arrmixPropertyDelinquencyDataByPropertyIds as $arrmixPropertyDelinquencyDataByPropertyId ) {
						if( true == valArr( $arrobjDelinquecyLogDetails ) && true == array_key_exists( $arrmixPropertyDelinquencyDataByPropertyId['property_id'], $arrobjDelinquecyLogDetails ) ) {
							$objDelinquencyLogDetail = getArrayElementByKey( $arrmixPropertyDelinquencyDataByPropertyId['property_id'], $arrobjDelinquecyLogDetails );
						} else {
							$objDelinquencyLogDetail = new CDelinquencyLog();
							$objDelinquencyLogDetail->setOverdueAmount( $arrmixPropertyDelinquencyDataByAccountId[$intAccountId]['total_amount_due'] );
						}
						$arrmixPropertyDelinquencyDataByPropertyId['system_notes']								= ' Delinquency Level changed.';
						$arrobjDelinquecyLogDetails[$arrmixPropertyDelinquencyDataByPropertyId['property_id']]	= $objDelinquencyLogDetail->createConsolidateDelinquencyLog( $arrmixPropertyDelinquencyDataByPropertyId );
					}
				}
			}
			$arrobjDelinquecyLog = array_merge( $arrobjDelinquecyLog, $arrobjDelinquecyLogDetails );
		}

		// Updating Delinquency Logs.
		if( true == valArr( $arrobjDelinquecyLog ) && false == CDelinquencyLogs::createService()->bulkInsert( $arrobjDelinquecyLog, $intUserId, $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();
			return [ 'boolResponse' => false, 'message' => 'Failed to insert delinquency logs.' ];
		}

		// Updating property preferences.
		if( false == self::updatePropertyPreferences( $intUserId, $objAdminDatabase, $objConnectDatabase, $arrintCIds, $arrintAccountIds ) ) {
			return false;
		}
		return true;
	}

}
?>