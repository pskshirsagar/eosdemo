<?php

class CTaskRelease extends CBaseTaskRelease {

	protected $m_arrobjTasks;
	protected $m_intTaskCount;

	const RELEASE_DAY = 3;

	/**
	 * Add Functions
	 */

	public function addTask( $objTask ) {
		$this->m_arrobjTasks[$objTask->getId()] = $objTask;
	}

	/**
	 * Get Functions
	 */

	public function getTasks() {
		return $this->m_arrobjTasks;
	}

	public function getTaskCount() {
		return $this->m_intTaskCount;
	}

	/**
	 * Set Functions
	 */

	public function setTasks( $arrobjTasks ) {
		$this->m_arrobjTasks = $arrobjTasks;
	}

	public function setTaskCount( $intTaskCount ) {
		$this->m_intTaskCount = $intTaskCount;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrstrValues['task_count'] ) ) $this->setTaskCount( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['task_count'] ) : $arrstrValues['task_count'] );

 		return;
	}

	/**
	 * Fetch Functions
	 */

	 public function fetchIncompleteTasksCount( $arrmixSprintTaskFilters, $objDatabase ) {

		return \Psi\Eos\Admin\CTasks::createService()->fetchIncompleteTasksCountByTaskReleaseId( $arrmixSprintTaskFilters, $this->getId(), $objDatabase );
	 }

	/**
	 * Validate Functions
	 */

	public function valName( $objDatabase = NULL ) {

		$boolValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		if( false == is_null( trim( $this->getName() ) ) && false == is_null( $objDatabase ) ) {

			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$intCount = CTaskReleases::fetchTaskReleaseCount( ' WHERE  lower( name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'' . $strSqlCondition, $objDatabase );

			if( 0 < $intCount ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'A task release already exists with this release date.' ) );
			}
		}

		return $boolValid;
	}

	public function valIsReleased( $objDatabase = NULL ) {

		$boolValid = true;

		if( true == $this->getIsReleased() && false == is_null( $objDatabase ) ) {

			$intIncompleteTaskCount = \Psi\Eos\Admin\CTasks::createService()->fetchIncompleteTasksCountByTaskReleaseId( $arrmixSprintTaskFilters = NULL, $this->getId(), $objDatabase );

			if( true == $intIncompleteTaskCount ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_released', 'One or more tasks are incomplete.' ) );
			}
		}

		return $boolValid;
	}

	public function valReleaseDatetime() {

		$boolValid = true;

		if( true == is_null( $this->getReleaseDatetime() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'release_datetime', 'Release date is required.' ) );
		} elseif( false == CValidation::validateDate( $this->getReleaseDatetime() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'release_datetime', 'Valid release date is required.' ) );
		}

		return $boolValid;
	}

	public function valSystemDownOn() {

		$boolValid = true;

		if( false == is_null( $this->getSystemDownOn() ) && false == CValidation::validateDate( $this->getSystemDownOn() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_down_on', "Valid 'System Down On' date is required." ) );
		}

		if( false == is_null( $this->getSystemDownOn() ) && false == is_null( $this->getSystemUpOn() ) ) {

			if( $this->getSystemDownOn() > $this->getSystemUpOn() ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_down_on', " 'System Down On' date must be less than 'System Up On' date." ) );
			}
		}

		return $boolValid;
	}

	public function valSystemUpOn() {

		$boolValid = true;

		if( false == is_null( $this->getSystemUpOn() ) && false == CValidation::validateDate( $this->getSystemUpOn() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_up_on', "Valid 'System Up On' date is required." ) );
		}

		return $boolValid;
	}

	public function valReleaseVideoUrl() {
		$boolValid = true;
		if( true == valStr( $this->getReleaseVideoUrl() ) && 0 == \Psi\CStringService::singleton()->preg_match( '/^(http|https):\/\/(?:www\.)/', $this->getReleaseVideoUrl() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'release_video_url', ' Invalid video url.' ) );
		}

		return $boolValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolValid &= $this->valName( $objDatabase );
				$boolValid &= $this->valReleaseDatetime();
				$boolValid &= $this->valSystemDownOn();
				$boolValid &= $this->valSystemUpOn();
				$boolValid &= $this->valIsReleased( $objDatabase );
				$boolValid &= $this->valReleaseVideoUrl();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolValid = true;
				break;
		}

		return $boolValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchTasks( $objDatabase ) {

		return \Psi\Eos\Admin\CTasks::createService()->fetchTasksByTaskReleaseId( $this->getId(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function buildAbbreviatedName() {
		$intMaximumAllowableSize = 240;
		return \Psi\CStringService::singleton()->substr( $this->getName(), 0, $intMaximumAllowableSize );
	}

	public function buildAbbreviatedDescription() {
		$intMaximumAllowableSize = 240;
		$strTrailingCharacters = '';

		if( \Psi\CStringService::singleton()->strlen( $this->getName() ) > $intMaximumAllowableSize ) return NULL;
		if( \Psi\CStringService::singleton()->strlen( $this->getName() . $this->getDescription() ) > $intMaximumAllowableSize ) $strTrailingCharacters = '...';

		return \Psi\CStringService::singleton()->substr( $this->getDescription(), 0, $intMaximumAllowableSize - \Psi\CStringService::singleton()->strlen( $this->getName() ) ) . $strTrailingCharacters;
	}

	public function fetchPaginatedCompletedTasksCountByTaskReleaseId( $arrmixSprintTaskFilters, $objDatabase ) {
		return \Psi\Eos\Admin\CTasks::createService()->fetchCompletedTasksCountByTaskReleaseId( $arrmixSprintTaskFilters, $this->getId(), $objDatabase );
	}

}
?>