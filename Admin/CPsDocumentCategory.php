<?php

class CPsDocumentCategory extends CBasePsDocumentCategory {

	const GENERAL						= 14;
	const RESIDENT_PAY_MARKETING_KITS	= 12;
	const SUMMIT_RESOURCES_2013			= 41;
	const CREATIVE_SERVICE_REQUESTS		= 82;
	const FINANCE_SERVICE_REQUESTS		= 83;
	const OTHER							= 84;
	const SSC							= 85;
	const HSC							= 86;
	const GRADUATION					= 87;
	const POST_GRADUATION				= 88;
	const CERTIFICATION					= 89;
	const OFFER_LETTER					= 90;
	const RELIEVING_LETTER_1			= 91;
	const RELIEVING_LETTER_2			= 92;
	const RELIEVING_LETTER_3			= 93;
	const SALARY_SLIP_1					= 94;
	const SALARY_SLIP_2					= 95;
	const SALARY_SLIP_3					= 96;
	const FORM_16						= 97;
	const PAN_CARD						= 98;
	const PERMANENT_ADDRESS_PROOF		= 99;
	const TEMPORARY_ADDRESS_PROOF		= 100;
	const PASSPORT_PHOTO				= 101;
	const US_REIMBURSEMENT_PROOF		= 102;
	const AMEX_PROOF					= 103;
	const INDIAN_REIMBURSEMENT_PROOF	= 104;
	const MASTER_POLICY                 = 105;
	const BACKGROUND_VERIFICATION_FORM  = 106;
	const AADHAAR_CARD					= 107;

	const DOCUMENT_PASSPORT_PHOTO				= 'passport_photo_document';
	const DOCUMENT_PERMANENT_ADDRESS_PROOF		= 'permanent_address_proof_document';
	const DOCUMENT_TEMPORARY_ADDRESS_PROOF		= 'temporary_address_proof_document';
	const BACKGROUND_VERIFICATION_DOCUMENT		= 'background_verification_document';

	public static $c_arrstrEducationalDocuments = array(
		self::SSC						=> 'ssc_document',
		self::HSC						=> 'hsc_document',
		self::GRADUATION				=> 'graduation_document',
		self::POST_GRADUATION			=> 'post_graduation_document',
		self::CERTIFICATION				=> 'certification_document'
	);

	public static $c_arrstrEmploymentDocuments = array(
		self::OFFER_LETTER				    => 'offer_letter_document',
		self::RELIEVING_LETTER_1		    => 'relieving_letter_1_document',
		self::RELIEVING_LETTER_2		    => 'relieving_letter_2_document',
		self::RELIEVING_LETTER_3		    => 'relieving_letter_3_document',
		self::SALARY_SLIP_1				    => 'salary_slip_1_document',
		self::SALARY_SLIP_2				    => 'salary_slip_2_document',
		self::SALARY_SLIP_3				    => 'salary_slip_3_document',
		self::FORM_16					    => 'form_16_document',
		self::BACKGROUND_VERIFICATION_FORM  => 'background_verification_document'
	);

	public static $c_arrstrPersonalDocuments = array(
		self::PAN_CARD					=> 'pan_card_document',
		self::AADHAAR_CARD				=> 'aadhaar_card_document',
		self::PERMANENT_ADDRESS_PROOF	=> 'permanent_address_proof_document',
		self::TEMPORARY_ADDRESS_PROOF	=> 'temporary_address_proof_document',
		self::PASSPORT_PHOTO			=> 'passport_photo_document'
	);

	public static $c_arrstrPersonalDocumentsForStudents = array(
		self::AADHAAR_CARD				=> 'aadhaar_card_document',
		self::PERMANENT_ADDRESS_PROOF	=> 'permanent_address_proof_document',
		self::TEMPORARY_ADDRESS_PROOF	=> 'temporary_address_proof_document',
		self::PASSPORT_PHOTO			=> 'passport_photo_document'
	);

	public static $c_arrstrCompulsoryEducationalDocuments = array(
		self::SSC						=> 'ssc_document',
		self::HSC						=> 'hsc_document',
		self::GRADUATION				=> 'graduation_document'
	);

	public static $c_arrstrCompulsoryEmploymentDocuments = array(
		self::OFFER_LETTER				    => 'offer_letter_document',
		self::SALARY_SLIP_1				    => 'salary_slip_1_document',
		self::SALARY_SLIP_2				    => 'salary_slip_2_document',
		self::SALARY_SLIP_3				    => 'salary_slip_3_document',
		self::BACKGROUND_VERIFICATION_FORM  => 'background_verification_document'
	);

	protected $m_strDocumentTypeName;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrintValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrintValues, $boolStripSlashes = true, $boolDirectSet = false );
		if( isset( $arrintValues['document_type'] ) && $boolDirectSet ) $this->m_strDocumentTypeName = trim( $arrintValues['document_type'] );
		elseif ( isset( $arrintValues['document_type'] ) ) $this->setDocumentTypeName( $arrintValues['document_type'] );

		return;
	}

	public function setDocumentTypeName( $strDocumentTypeName ) {
		$this->m_strDocumentTypeName = CStrings::strTrimDef( $strDocumentTypeName, NULL, false );
	}

	/**
	 * Get Functions
	 *
	 */

	public function getDocumentTypeName() {
		return $this->m_strDocumentTypeName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valName( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Document category is required. ' ) );
		}
		$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
		$intCount = CPsDocumentCategories::fetchPsDocumentCategoryCount( ' WHERE lower( name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'' . $strSqlCondition, $objAdminDatabase );

		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Document category already exists. ' ) );
		}

		return $boolIsValid;
	}

	public function valPsDocumentTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsDocumentTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document_type_id', 'Document type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		if( true == is_null( $this->getOrderNum() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Order number is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objAdminDatabase ) {

		$boolIsValid = true;

		$objPsDocument = CPsDocuments::fetchPsDocumentsByPsDocumentCategoryId( $this->getId(), $objAdminDatabase );

		if( false != isset( $objPsDocument ) ) {
			$intPsDocumentsCount = $objPsDocument->getPsDocumentsCategoryCount();
			if( 0 < $intPsDocumentsCount ) {
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objAdminDatabase );
				$boolIsValid &= $this->valPsDocumentTypeId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objAdminDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

}
?>