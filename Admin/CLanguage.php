<?php

class CLanguage extends CBaseLanguage {

	const ENGLISH = 'en_US';
	const SPANISH = 'es_ES';

	const ID_ENGLISH = 1;
	const ID_SPANISH = 2;

	public static $c_arrintLanguageIdMapWithCode = array(
	    self::ID_SPANISH => self::SPANISH,
	);

	public static $c_arrintLanguageCodeMapWithName = array(
		self::SPANISH => 'Spanish',
	);

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>