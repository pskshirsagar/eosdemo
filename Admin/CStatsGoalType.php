<?php

class CStatsGoalType extends CBaseStatsGoalType {

	const OPEN_BUG						= 1;
	const PROJECTS_IMPLEMENTED_WEEKLY	= 2;
	const PROJECTS_IMPLEMENTED_MONTHLY	= 3;
	const PROJECTS_IMPLEMENTED_YEARLY	= 4;
	const PRODUCTS_IMPLEMENTED_WEEKLY	= 5;
	const PRODUCTS_IMPLEMENTED_MONTHLY	= 6;
	const PRODUCTS_IMPLEMENTED_YEARLY	= 7;
	const TASKS_IMPLEMENTED_WEEKLY		= 8;
	const TASKS_IMPLEMENTED_MONTHLY		= 9;
	const TASKS_IMPLEMENTED_YEARLY		= 10;
	const BILLABLE_UTILIZATION_PERCENT	= 11;
	const MTD_NPS_SCORE					= 12;
	const YTD_NPS_SCORE					= 13;
	const MTD_NPS_PROMOTERS				= 14;
	const MTD_NPS_PASSIVES				= 15;
	const MTD_NPS_DETRACTORS			= 16;
	const OPEN_BUG_COUNT_BY_DOE			= 17;
	const CSM_CHURN_YEAR_END_GOAL						= 18;
	const CSM_VISITS_YEAR_END_GOAL_PERCENTAGE			= 19;
	const CSM_VISITS_YEAR_END_GOAL_SCORE				= 20;
	const CSM_SUCCESS_REVIEWS_YEAR_END_GOAL_PERCENTAGE	= 21;
	const CSM_SUCCESS_REVIEWS_YEAR_END_GOAL_SCORE		= 22;
	const CSM_REFERENCABILITY_YEAR_END_GOAL_PERCENTAGE	= 23;

	public static $c_arrintStatsGoalDataReportIds = [
		self::OPEN_BUG,
		self::PROJECTS_IMPLEMENTED_WEEKLY,
		self::PROJECTS_IMPLEMENTED_MONTHLY,
		self::PROJECTS_IMPLEMENTED_YEARLY,
		self::PRODUCTS_IMPLEMENTED_WEEKLY,
		self::PRODUCTS_IMPLEMENTED_MONTHLY,
		self::PRODUCTS_IMPLEMENTED_YEARLY,
		self::TASKS_IMPLEMENTED_WEEKLY,
		self::TASKS_IMPLEMENTED_MONTHLY,
		self::TASKS_IMPLEMENTED_YEARLY,
		self::BILLABLE_UTILIZATION_PERCENT,
		self::MTD_NPS_SCORE,
		self::YTD_NPS_SCORE,
		self::MTD_NPS_PROMOTERS,
		self::MTD_NPS_PASSIVES,
		self::MTD_NPS_DETRACTORS
	];

	public static $c_arrintStatsGoalCsmManageGoalIds = [
		self::CSM_CHURN_YEAR_END_GOAL,
		self::CSM_VISITS_YEAR_END_GOAL_PERCENTAGE,
		self::CSM_VISITS_YEAR_END_GOAL_SCORE,
		self::CSM_SUCCESS_REVIEWS_YEAR_END_GOAL_PERCENTAGE,
		self::CSM_SUCCESS_REVIEWS_YEAR_END_GOAL_SCORE,
		self::CSM_REFERENCABILITY_YEAR_END_GOAL_PERCENTAGE
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsHistoricData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStopOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>