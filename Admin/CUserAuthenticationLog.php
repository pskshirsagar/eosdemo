<?php

class CUserAuthenticationLog extends CBaseUserAuthenticationLog {

	public function __construct() {
		parent::__construct();

		$this->setIpAddress( getRemoteIpAddress() );

		return;
	}

	public function valUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'User ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPreviousPassword() {
		$boolIsValid = true;

		if( true == is_null( $this->getPreviousPassword() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_password', 'Previous password is required.' ) );
		}

		return $boolIsValid;
	}

	public function valLoginDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getLoginDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'login_datetime', 'Login date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIpAddress() {
		$boolIsValid = true;

		if( true == is_null( $this->getIpAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP address is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valLoginDatetime();
				$boolIsValid &= $this->valIpAddress();
				break;

			case 'insert_password_rotation':
				$boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valPreviousPassword();
				$boolIsValid &= $this->valIpAddress();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	// other function

	public function calculateTimeSpent() {
		$intLogOutTime 	= strtotime( $this->getLogoutDateTime() );
		$intLoginTime 	= strtotime( $this->getLoginDateTime() );

		$intDiff	= abs( $intLogOutTime - $intLoginTime );
		$intHours 	= floor( $intDiff / ( 60 * 60 ) );
		$intMins 	= floor( ( $intDiff - ( $intHours * 60 * 60 ) ) / ( 60 ) );
		$intSecs 	= floor( ( $intDiff - ( ( $intHours * 60 * 60 ) + ( $intMins * 60 ) ) ) );
		$strResult = sprintf( '%02d', $intHours ) . ':' . sprintf( '%02d', $intMins ) . ':' . sprintf( '%02d', $intSecs );

		return $strResult;
	}

}
?>