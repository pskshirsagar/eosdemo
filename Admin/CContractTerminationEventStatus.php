<?php

class CContractTerminationEventStatus extends CBaseContractTerminationEventStatus {

	const PENDING	= 1;
	const COMPLETED	= 2;
	const FAILED	= 3;
	const CANCELLED	= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>