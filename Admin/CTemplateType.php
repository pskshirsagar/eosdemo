<?php

class CTemplateType extends CBaseTemplateType {

	const MOVE_IN_STATEMENT             = 1;
	const FINANCIAL_MOVE_OUT_STATEMENT  = 2;
	const TENANT_STATEMENT              = 3;
	const GROUP_CONTRACT_USER_GUIDE     = 4;
    const ID_CARD_TEMPLATE              = 5;
    const LEASE_ESA                     = 7;
    const CERTIFICATE_OF_RENT_PAID      = 8;
    const MILITARY_DD_1746              = 9;
    const SELF_PROVISIONAL_CONTRACT_ADDENDUM_TEMPLATE_FIRST_PAGE = 11;
    const SELF_PROVISIONAL_CONTRACT_ADDENDUM_TEMPLATE_LAST_PAGE = 12;
	const CONTRACT_TERMINATION_ADDENDUM_FIRST_PAGE_WITH_RPAY = 13;
	const CONTRACT_TERMINATION_ADDENDUM_FIRST_PAGE_WITHOUT_RPAY = 14;
	const CONTRACT_TERMINATION_ADDENDUM_LAST_PAGE = 15;
	const NO_PHOTO_DEFAULT_IMAGE		= 6;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>