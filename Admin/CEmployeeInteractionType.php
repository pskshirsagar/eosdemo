<?php

class CEmployeeInteractionType extends CBaseEmployeeInteractionType {

	const ONE_ON_ONE				= 1;
	const TOUCH_POINT				= 2;
	const CALL_REVIEW				= 3;
	const SKIP_LEVEL				= 4;
	const COACHING_SESSION			= 5;
	const SHADOW_SESSION			= 6;
	const PERFORMANCE_EVALUATION	= 7;
	const QUARTERLY_REVIEW			= 8;
	const QA_SPECIALIST				= 9;
	const HR_MEETING				= 10;

	public static $c_arrstrEmployeeInteractionTypes = [
		self::ONE_ON_ONE				=> 'One on One',
		self::TOUCH_POINT				=> 'Touchpoint',
		self::CALL_REVIEW				=> 'Call Review',
		self::SKIP_LEVEL				=> 'Skip Level',
		self::COACHING_SESSION			=> 'Coaching Session',
		self::SHADOW_SESSION			=> 'Shadow Session',
		self::PERFORMANCE_EVALUATION	=> 'Performance Evaluation',
		self::QUARTERLY_REVIEW			=> 'Quarterly Review',
		self::QA_SPECIALIST				=> 'QA Session',
		self::HR_MEETING				=> 'HR Meeting'
	];

	public static $c_arrstrEmployeeInteractionTypeImages = [
		self::ONE_ON_ONE				=> 'one-on-one.svg',
		self::TOUCH_POINT				=> 'touchpoint.svg',
		self::CALL_REVIEW				=> 'call-review.svg',
		self::SKIP_LEVEL				=> 'skip-level.svg',
		self::COACHING_SESSION			=> 'coaching.svg',
		self::SHADOW_SESSION			=> 'shadow-session.svg',
		self::PERFORMANCE_EVALUATION	=> 'performance-eval.svg',
		self::QUARTERLY_REVIEW			=> 'quarterly-review.svg',
		self::QA_SPECIALIST				=> 'qa-specialist.svg',
		self::HR_MEETING				=> 'hr.svg'
	];

	public static $c_arrstrEmployeeInteractionTypeTemplates = [
		self::ONE_ON_ONE				=> 'create_one_on_one_interaction.tpl',
		self::TOUCH_POINT				=> 'create_touch_point_interaction.tpl',
		self::CALL_REVIEW				=> 'create_call_review_interaction.tpl',
		self::SKIP_LEVEL				=> 'create_touch_point_interaction.tpl',
		self::COACHING_SESSION			=> 'create_touch_point_interaction.tpl',
		self::SHADOW_SESSION			=> 'create_touch_point_interaction.tpl',
		self::PERFORMANCE_EVALUATION	=> 'create_touch_point_interaction.tpl',
		self::QUARTERLY_REVIEW			=> 'create_touch_point_interaction.tpl',
		self::QA_SPECIALIST				=> 'create_touch_point_interaction.tpl',
		self::HR_MEETING				=> 'create_touch_point_interaction.tpl'
	];

	public static $c_arrintCoachEmployeeInteractionTypeIds = [
		self::ONE_ON_ONE,
		self::TOUCH_POINT,
		self::SHADOW_SESSION,
		self::COACHING_SESSION,
		self::CALL_REVIEW,
		self::QA_SPECIALIST
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>