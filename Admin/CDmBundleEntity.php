<?php

class CDmBundleEntity extends CBaseDmBundleEntity {

	public function valName() {
		$boolIsValid = true;

		if( !valStr( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please enter URL name.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valName();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>