<?php

class COkrKeyResult extends CBaseOkrKeyResult {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOkrObjectiveId() {
		$boolIsValid = true;
		if( false == valId( $this->getOkrObjectiveId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'okr_objective_id', 'Key Result objective id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOkrKeyResultStatusTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getOkrKeyResultStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'okr_key_result_status_type_id', 'Key Result status type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOwnerEmployeeId() {
		$boolIsValid = true;
		if( false == valId( $this->getOwnerEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'owner_employee_id', 'Key Result owner employee is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( false == valStr( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Key Result description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeliveryDate() {
		$boolIsValid = true;
		if( false == valStr( $this->getDeliveryDate() ) || false == \Psi\Libraries\UtilValidation\CValidation::createService()->validateDate( $this->getDeliveryDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delivery_date', 'Key Result delivery date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsOnTrack() {
		$boolIsValid = true;
		if( is_null( $this->getIsOnTrack() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_on_track', 'Key Result on track is required.' ) );
		}

		return $boolIsValid;
	}

	public function valProgress() {
		$boolIsValid = true;
		if( valId( $this->getProgress() ) && ( 0 > $this->getProgress() || 100 < $this->getProgress() || floor( $this->getProgress() ) != $this->getProgress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'progress', 'Key Result progress should be a whole number between 0-100.' ) );
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		if( false == valId( $this->getOrderNum() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Key Result order is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valOkrObjectiveId();
				$boolIsValid &= $this->valOkrKeyResultStatusTypeId();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valOwnerEmployeeId();
				$boolIsValid &= $this->valDeliveryDate();
				$boolIsValid &= $this->valIsOnTrack();
				$boolIsValid &= $this->valProgress();
				$boolIsValid &= $this->valOrderNum();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
