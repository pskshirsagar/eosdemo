<?php

class CPersonOrigin extends CBasePersonOrigin {

	public function valPersonId( $objDatabase ) {
		$boolIsValid = true;

		$intPersonCount = \Psi\Eos\Admin\CPersons::createService()->fetchPersonCount( ' where id = ' . $this->getPersonId(), $objDatabase );

		if( 0 == ( int ) $intPersonCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Error' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolCheckPersonId = false, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				if( true == $boolCheckPersonId ) {
					$boolIsValid = $this->valPersonId( $objDatabase );
					break;
				}
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>