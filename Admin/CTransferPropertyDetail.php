<?php

class CTransferPropertyDetail extends CBaseTransferPropertyDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransferPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreviousContractPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreviousCommissionRateAssociationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreviousOutstandingCommissionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>