<?php

class CDataPrivacyStep extends CBaseDataPrivacyStep {

	const ENTRATA_GOOGLE_SUITE			= 1;
	const XENTO_GOOGLE_SUITE			= 2;
	const ENTRATA_FILE_SERVERS			= 3;
	const XENTO_FILE_SERVERS			= 4;
	const CONTACT_SERVICE_PROVIDERS		= 5;
	const COMPILING_SOURCES				= 6;
	const SEARCH_PARAMETERS				= 7;
	const DISCLOSURE_TO_CLIENT			= 8;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>