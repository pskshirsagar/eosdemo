<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDependantTypes
 * Do not add any new functions to this class.
 */

class CDependantTypes extends CBaseDependantTypes {

	public static function fetchCachedDependantTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDependantType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCachedDependantType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDependantType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllDependantTypes( $objDatabase ) {
		return self::fetchCachedDependantTypes( 'SELECT * FROM dependant_types', $objDatabase );
	}
}
?>