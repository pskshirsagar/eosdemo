<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CProductConfigurationTypes
 * Do not add any new functions to this class.
 */

class CProductConfigurationTypes extends CBaseProductConfigurationTypes {

	public static function fetchProductConfigurationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CProductConfigurationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchProductConfigurationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CProductConfigurationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>