<?php

class CBillingRequestStatusType extends CBaseBillingRequestStatusType {

	const UNUSED							= 1;
	const APPROVED 							= 2;
	const REJECTED							= 3;
	const DOCUMENT_APPROVED					= 4;
	const DOCUMENT_SIGNED					= 5;
	const DOCUMENT_COUNTERSIGNED_COMPLETED	= 6;
	const CANCELLED							= 7;


	public static $c_arrintBillingRequestStatusTypes = [
		'NEW' => self::UNUSED,
		'APPROVED' => self::APPROVED,
		'REJECTED' => self::REJECTED,
		'DOCUMENT_APPROVED' => self::DOCUMENT_APPROVED,
		'DOCUMENT_SIGNED' => self::DOCUMENT_SIGNED,
		'DOCUMENT_COUNTERSIGNED_COMPLETED' => self::DOCUMENT_COUNTERSIGNED_COMPLETED
	];

	public static $c_arrintBillingRequestStatusTypesValues = [
		self::UNUSED                           => 'New',
		self::APPROVED                         => 'Approved',
		self::REJECTED                         => 'Rejected',
		self::DOCUMENT_APPROVED                => 'Document Approved',
		self::DOCUMENT_SIGNED                  => 'Document Signed',
		self::DOCUMENT_COUNTERSIGNED_COMPLETED => 'Document Countersigned',
	];

	public function valId() {
		return true;
	}

	public function valName() {
		return true;
	}

	public function valDescription() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valOrderNumber() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>