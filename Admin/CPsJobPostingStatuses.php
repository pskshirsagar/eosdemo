<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsJobPostingStatuses
 * Do not add any new functions to this class.
 */

class CPsJobPostingStatuses extends CBasePsJobPostingStatuses {

	public static function fetchPsJobPostingStatuses( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPsJobPostingStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPsJobPostingStatus( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPsJobPostingStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedPsJobPostingStatusDetails( $objDatabase ) {
		return self::fetchObjects( 'SELECT * FROM ps_job_posting_statuses WHERE is_published = 1 ', 'CPsJobPostingStatus', $objDatabase );
	}

	public static function fetchAllPsJobPostingStepStatuses( $objDatabase ) {
		return parent::fetchPsJobPostingStatuses( 'SELECT * FROM ps_job_posting_statuses', $objDatabase );

	}

}
?>