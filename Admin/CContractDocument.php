<?php
use Psi\Eos\Admin\CContractDocuments;
use Psi\Eos\Admin\CPsDocuments;

class CContractDocument extends CBaseContractDocument {

	protected $m_objPsDocument;

	protected $m_strTitle;
	protected $m_strFileName;
	protected $m_strContractDatetime;
	protected $m_strContractDocumentTypeName;
	protected $m_intIsFromContractDrafts;
	protected $m_intContractDraftId;
	protected $m_intPsProductId;
	protected $m_intContractTerminationRequestStatusId;

	const PAGE_WIDTH 		= 650;
	const ADJUST_PAGE_WIDTH = 80;
	const MAX_CHAR_COUNT = 45;

	/**
	 * Get Functions
	 */

	public function getPsDocument() {
		return $this->m_objPsDocument;
	}

	public function getContractDatetime() {
		return $this->m_strContractDatetime;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function getContractDocumentTypeName() {
		return $this->m_strContractDocumentTypeName;
	}

	public function getIsFromContractDrafts() {
		return $this->m_intIsFromContractDrafts;
	}

	public function getContractDraftId() {
		return $this->m_intContractDraftId;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function getContractTerminationRequestStatusId() {
		return $this->m_intContractTerminationRequestStatusId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['contract_datetime'] ) )			$this->setContractDatetime( $arrmixValues['contract_datetime'] );
		if( true == isset( $arrmixValues['title'] ) )						$this->setTitle( $arrmixValues['title'] );
		if( true == isset( $arrmixValues['file_name'] ) )					$this->setFileName( $arrmixValues['file_name'] );
		if( true == isset( $arrmixValues['contract_document_type_name'] ) )	$this->setContractDocumentTypeName( $arrmixValues['contract_document_type_name'] );
		if( true == isset( $arrmixValues['is_from_drafts'] ) )				$this->setIsFromContractDrafts( $arrmixValues['is_from_drafts'] );
		if( true == isset( $arrmixValues['contract_draft_id'] ) )			$this->setContractDraftId( $arrmixValues['contract_draft_id'] );
		if( true == isset( $arrmixValues['ps_product_id'] ) )				$this->setPsProductId( $arrmixValues['ps_product_id'] );
		if( true == isset( $arrmixValues['contract_termination_request_status_id'] ) )				$this->setContractTerminationRequestStatusId( $arrmixValues['contract_termination_request_status_id'] );

	}

	public function setContractDatetime( $strContractDatetime ) {
		$this->m_strContractDatetime = CStrings::strTrimDef( $strContractDatetime, -1, NULL, true );
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = CStrings::strTrimDef( $strTitle, 250, NULL, true );
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	public function setContractDocumentTypeName( $strContractDocumentTypeName ) {
		$this->m_strContractDocumentTypeName = $strContractDocumentTypeName;
	}

	public function setPsDocument( $objPsDocument ) {
		$this->m_objPsDocument = $objPsDocument;
	}

	public function setIsFromContractDrafts( $intIsFromContractDraft ) {
		$this->m_intIsFromContractDrafts = $intIsFromContractDraft;
	}

	public function setContractDraftId( $intContractDraftId ) {
		$this->m_intContractDraftId = $intContractDraftId;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = $intPsProductId;
	}

	public function setContractTerminationRequestStatusId( $intContractTerminationRequestStatusId ) {
		$this->m_intContractTerminationRequestStatusId = $intContractTerminationRequestStatusId;
	}

	/**
	 * Other Functions
	 */

	public function valContractDocumentTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getContractDocumentTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_document_type_id', __( 'Contract document type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateContractDocument( $objAdminDatabase ) {

		$boolValid = true;
		$arrobjContractDocuments = CContractDocuments::createService()->fetchActiveContractDocumentsByContractIdByPsDocumentIdByContractDocumentTypeId( $this->getContractId(), $this->getPsDocumentId(), $this->getContractDocumentTypeId(), $objAdminDatabase );

		if( true == valArr( $arrobjContractDocuments ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Contract document with the type selected already associated.' ) );
		}

		return $boolValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valContractDocumentTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_duplicate':
				$boolIsValid &= $this->valDuplicateContractDocument( $objAdminDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function getOrFetchPsDocument( $objDatabase ) {

		if( false == isset( $this->getPsDocument ) || false == valObj( $this->m_objPsDocument, 'CPsDocument' ) ) {

			$this->m_objPsDocument = CPsDocuments::createService()->fetchPsDocumentById( $this->getPsDocumentId(), $objDatabase );
		}

		return $this->m_objPsDocument;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return parent::update( $intCurrentUserId, $objDatabase );

	}

	public function insertContractDraftDocumentAction( $arrmixActionDetails, $objContractDraft, $intEmployeeId, $intUserId, $objAdminDatabase, $boolContractDraftNote = false ) {

		if( false == valId( $this->getId() ) ) {
				return false;
		}

		$objAction = new CAction();
		$objAction->setActionTypeId( CActionType::NOTE );
		$objAction->setActionDatetime( date( 'm/d/Y H:i:s' ) );
		$objAction->setPsLeadId( $objContractDraft->getPsLeadId() );
		$objAction->setContractId( $objContractDraft->getContractId() );
		$objAction->setEmployeeId( $intEmployeeId );
		$objAction->setActionDescription( $arrmixActionDetails['action_description'] );
		$objAction->setNotes( $arrmixActionDetails['notes'] );
		$objAction->setActionCategoryId( CActionCategory::ID_SALES );

		if( false == $objAction->validate( VALIDATE_INSERT ) || false == $objAction->insert( $intUserId, $objAdminDatabase ) ) {
			$this->addErrorMsgs( $objAction->getErrorMsgs() );
			return false;
		}

		if( true == $boolContractDraftNote ) {
		// This reference will be used to fetch contract draft note.
		$objActionReferenceForContractDraft = new CActionReference();
		$objActionReferenceForContractDraft->setActionReferenceTypeId( CActionReferenceType::CONTRACT_DRAFT );
		$objActionReferenceForContractDraft->setReferenceNumber( $this->getId() );
		$objActionReferenceForContractDraft->setActionId( $objAction->getId() );

			if( false == $objActionReferenceForContractDraft->validate( VALIDATE_INSERT ) || false == $objActionReferenceForContractDraft->insert( $intUserId, $objAdminDatabase ) ) {
					$this->addErrorMsgs( $objActionReferenceForContractDraft->getErrorMsgs() );
					return false;
			}
		}

		if( true == valId( $objContractDraft->getContactPersonId() ) ) {
		// This reference will be used to fetch the contact person.
		$objActionReferenceForContractPerson = new CActionReference();
		$objActionReferenceForContractPerson->setActionReferenceTypeId( CActionReferenceType::PERSON );
		$objActionReferenceForContractPerson->setReferenceNumber( $objContractDraft->getContactPersonId() );
		$objActionReferenceForContractPerson->setActionId( $objAction->getId() );

			if( false == $objActionReferenceForContractPerson->validate( VALIDATE_INSERT ) || false == $objActionReferenceForContractPerson->insert( $intUserId, $objAdminDatabase ) ) {
					$this->addErrorMsgs( $objActionReferenceForContractPerson->getErrorMsgs() );
					return false;
			}
		}

		return true;
	}

	public function signContractDocument( $strSignText, $objContractDraft, $strTitle, $objAdminDatabase, $objGatewayStorage, $intCurrentUserId ) {

		if( false == valId( $this->getId() ) ) {
					return false;
		}

		if( false == valId( $objContractDraft->getPsLeadId() ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Ps Lead ID is required.' ) );
					return false;
		}

		$objPsDocument = CPsDocuments::createService()->fetchPsDocumentById( $this->getPsDocumentId(), $objAdminDatabase );
		if( false == valObj( $objPsDocument, 'CPsDocument' ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Ps document not found.' ) );
					return false;
		}

		$strFileNameWithPath = $objPsDocument->downloadObject( $objGatewayStorage, $objPsDocument->getFileName(), 'inline', true );

		if( !valStr( $strFileNameWithPath ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Contract document file not found.' ) );
					return false;
		}

		try {
			$objZendPdf			= Zend_Pdf::load( $strFileNameWithPath );
			$intLastPageIndex	= \Psi\Libraries\UtilFunctions\count( $objZendPdf->pages ) - 1;
			$objZendPdfPage		= $objZendPdf->pages[$intLastPageIndex];

			$intDrawX2Line = 0;
			if( self::PAGE_WIDTH > $objZendPdfPage->getWidth() ) {
				$intDrawX2Line = self::ADJUST_PAGE_WIDTH;
			}

			$resImage			= imagecreatefrompng( PATH_COMMON . '/images/lease_execution/esign_initials.png' );

			$resWhite		= imagecolorallocate( $resImage, 255, 255, 255 );
			$resGrey		= imagecolorallocate( $resImage, 128, 128, 128 );
			$resColorBlack	= imagecolorallocate( $resImage, 0, 0, 0 );

			$strFontFile			= PATH_LIBRARY_PSI_FONTS . 'SWENSON.TTF';
			$strSignatureImagePath	= CONFIG_CACHE_DIRECTORY . 'sign_' . time() . '.png';
			imagettftext( $resImage, $intSize = 20, $intAngle = 0, $intX = 25, $intY = 52, $resColorBlack, $strFontFile, $strSignText );

			$strFontFile = PATH_LIBRARY_PSI_FONTS . 'arial_narrow.ttf';
			imagettftext( $resImage, $intSize = 10, $intAngle = 0, $intX = 31, $intY = 82, $resWhite, $strFontFile, getRemoteIpAddress() );
			imagettftext( $resImage, $intSize = 10, $intAngle = 0, $intX = 10, $intY = 22, $resGrey, $strFontFile, date( 'm/d/y h:i A' ) );
			imageinterlace( $resImage, false );
			imagepng( $resImage, $strSignatureImagePath );

			$objFont			= Zend_Pdf_Font::fontWithName( Zend_Pdf_Font:: FONT_HELVETICA );
			$objZendPdfImage	= Zend_Pdf_Image::imageWithPath( $strSignatureImagePath );

			// Entrata employee counter sign.
			$objZendPdfPage->drawImage( $objZendPdfImage, ( 470 - $intDrawX2Line ), 432, ( 630 - $intDrawX2Line ), 492 ); // Signature
			$objZendPdfPage->setFont( $objFont, 9 )->drawText( $strSignText, ( 470 - $intDrawX2Line ), 412 ); // Printed Name
			$objZendPdfPage->setFont( $objFont, 9 )->drawText( $strTitle, ( 470 - $intDrawX2Line ), 392 ); // Title
			$objZendPdfPage->setFont( $objFont, 9 )->drawText( date( 'F j, Y H:i:s' ), ( 470 - $intDrawX2Line ), 372 ); // Date

			$objZendPdf->save( $strFileNameWithPath );
			imagedestroy( $resImage );

			$strBaseFileName = basename( $strFileNameWithPath );
			$strTemFilePath  = str_replace( $strBaseFileName, '', $strFileNameWithPath );
			rename( $strTemFilePath . $strBaseFileName, $strTemFilePath . $objPsDocument->getFileName() );
			$strFileNameWithPath = $strTemFilePath . $objPsDocument->getFileName();
			if( !$objPsDocument->uploadObject( $objGatewayStorage, $strFileNameWithPath ) || !$objPsDocument->insertOrUpdateStoredObject( $intCurrentUserId, $objAdminDatabase ) ) {
				throw new Exception( 'Unable to upload signed document' );
			}

			if( false == is_null( $strSignatureImagePath ) && false == is_dir( $strSignatureImagePath ) && true == CFileIo::fileExists( $strSignatureImagePath ) ) {
					CFileIo::deleteFile( $strSignatureImagePath );
			}

			return true;

		} catch( Exception $objException ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', sprintf( 'Unable to draw signature image: Error: %s ;', $objException->getMessage() ) ) );
					return false;
		}

	}

}
?>