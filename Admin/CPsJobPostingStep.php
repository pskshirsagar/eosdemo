<?php

class CPsJobPostingStep extends CBasePsJobPostingStep {

	const APPLIED			= 'Applied';
	const OFFER_ACCEPTED	= 'Offer Accepted';
	const OFFER_EXTENDED	= 'Offer Extended';
	const MRAB				= 'MRAB';
	const CCAT				= 'CCAT';
	const TECH_TEST			= 'Tech Test';
	const FIRST_INTERVIEW	= '1st Interview';
	const SECOND_INTERVIEW	= '2nd Interview';
	const HR_INTERVIEW		= 'HR Interview';
	const REFERENCES		= 'References';

	public static $c_arrmixPsJobPostingStepsHideHistory = [
		self::OFFER_ACCEPTED,
		self::OFFER_EXTENDED,
		self::HR_INTERVIEW,
		self::REFERENCES
	];

	protected $m_strPsJobPostingStepTypeName;
	protected $m_strEmailTemplateName;

	/**
	 * Get Functions
	 *
	 */

	public function getPsJobPostingStepTypeName() {
		return $this->m_strPsJobPostingStepTypeName;
	}

	public function getEmailTemplateName() {
		return $this->m_strEmailTemplateName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ps_job_posting_step_type_name'] ) )	$this->setPsJobPostingStepTypeName( $arrmixValues['ps_job_posting_step_type_name'] );
		if( true == isset( $arrmixValues['email_template_name'] ) )	$this->setEmailTemplateName( $arrmixValues['email_template_name'] );

		return;
	}

	public function setPsJobPostingStepTypeName( $strPsJobPostingStepTypeName ) {
		$this->m_strPsJobPostingStepTypeName = $strPsJobPostingStepTypeName;
	}

	public function setEmailTemplateName( $strEmailTemplateName ) {
		$this->m_strEmailTemplateName = $strEmailTemplateName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Step name is required.' ) );
		}

		if( true == $boolIsValid && true == preg_match( '/[^A-Za-z0-9\s]/', $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Step name should not contain special characters.' ) );
		}

		if( true == $boolIsValid && false == valStr( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Step name should contain more than one alphabet.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>