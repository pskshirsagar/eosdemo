<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskPriorities
 * Do not add any new functions to this class.
 */

class CTaskPriorities extends CBaseTaskPriorities {

	public static function fetchTaskPriorities( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTaskPriority', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchTaskPriority( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTaskPriority', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllTaskPriorities( $objDatabase, $intExcludedTaskPriority = NULL ) {

		$strCondition = '';
		if( false == is_null( $intExcludedTaskPriority ) ) {
			$strCondition = ' WHERE id !=' . ( int ) $intExcludedTaskPriority;
		}

		$strSql = ' SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT 
						* 
					FROM 
					task_priorities
							' . $strCondition . ' 
					ORDER BY 
							order_num';

		return self::fetchTaskPriorities( $strSql, $objDatabase );
	}

	public static function fetchTaskPrioritiesByIds( $arrintTaskPrioritiesIds, $objDatabase, $boolFromTestCases = false ) {
		if( false == valArr( $arrintTaskPrioritiesIds ) ) return NULL;

		$strSelect = '*';

		if( true == $boolFromTestCases ) {
			$strSelect = 'id, 
							CASE WHEN id = ' . CTaskPriority::URGENT . ' THEN \'Critical\' 
							ELSE name 
							END AS name';
		}

		$strSql = 'SELECT ' . $strSelect . ' FROM task_priorities WHERE id IN (' . implode( ',', $arrintTaskPrioritiesIds ) . ') ORDER BY order_num';

		return self::fetchTaskPriorities( $strSql, $objDatabase );
	}

	public static function fetchTaskPrioritiesDetails( $objDatabase ) {

		$strSql = 'SELECT id,name FROM task_priorities ORDER BY id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTaskPrioritiesByTaskIds( $arrintTaskIds, $objDatabase ) {

		if( false == valArr( $arrintTaskIds ) ) return NULL;

		$strSql = '	SELECT
						t.id AS task_id,
						tp.name AS task_priority,
						tp.id AS task_priority_id
					FROM
						task_priorities tp
						JOIN tasks t ON ( t.task_priority_id = tp.id )
					WHERE
						t.id IN (' . implode( ',', $arrintTaskIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

}
?>