<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportDataTypes
 * Do not add any new functions to this class.
 */

class CReportDataTypes extends CBaseReportDataTypes {

	public static function fetchReportDataTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReportDataType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchReportDataType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReportDataType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>