<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeCompensations
 * Do not add any new functions to this class.
 */

class CEmployeeCompensations extends CBaseEmployeeCompensations {

	public static function fetchEmployeeCompensationByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeCompensation( sprintf( 'SELECT * FROM employee_compensations WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeCompensationsByEmployeeIds( $intViewerEmployeeId, $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) || true == empty( $intViewerEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						ec.*,
						eea.encrypted_amount AS compensation_encrypted
					FROM
						employee_compensations ec
						LEFT JOIN employee_encryption_associations eea ON ( eea.id = ec.employee_encryption_association_id AND eea.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND eea.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
					WHERE
						ec.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return parent::fetchEmployeeCompensations( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeeCompensationsByEmployeeIds( $intViewerEmployeeId, $arrintEmployeeIds, $arrstrEmployeeCompensationFilter, $strCountryCode, $objAdminDatabase, $boolForCount = false ) {
		if( false == valArr( $arrintEmployeeIds ) || true == empty( $intViewerEmployeeId ) || false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strOrderByField = ( false == empty( $arrstrEmployeeCompensationFilter['order_by_field'] ) ) ? $arrstrEmployeeCompensationFilter['order_by_field'] : '';
		$strOrderByType  = ( false == empty( $arrstrEmployeeCompensationFilter['order_by_type'] ) ) ? $arrstrEmployeeCompensationFilter['order_by_type'] : '';

		$strComma = ( false == empty( $strOrderByField ) ) ? ',' : '';
		$intOffset			= ( false == $boolForCount && false == empty( $arrstrEmployeeCompensationFilter['page_no'] ) && false == empty( $arrstrEmployeeCompensationFilter['page_size'] ) ) ? $arrstrEmployeeCompensationFilter['page_size'] * ( $arrstrEmployeeCompensationFilter['page_no'] - 1 ) : 0;
		$intLimit			= ( false == $boolForCount && true == isset( $arrstrEmployeeCompensationFilter['page_size'] ) ) ? $arrstrEmployeeCompensationFilter['page_size'] : '';
		$strSelectClause 	= ( false == $boolForCount ) ? ' DISTINCT ON ( ' . $strOrderByField . $strComma . 'e.name_full ) ec.*, eea.encrypted_amount AS compensation_encrypted' : ' count( DISTINCT ec.id ) ';
		$strWhereClause		= ' ec.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';
		$strWhereClause		.= ( false == $arrstrEmployeeCompensationFilter['show_disabled_data'] )? 'AND e.employee_status_type_id !=' . CEmployeeStatusType::NO_SHOW :'';
		$strOrderByClause	= ( false == $boolForCount ) ? ' ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . $strComma . 'e.name_full' : '';

		$strWhereClause	.= ( false == empty( $arrstrEmployeeCompensationFilter['department_ids'] ) && true == valArr( array_filter( $arrstrEmployeeCompensationFilter['department_ids'] ) ) ) ? ' AND e.department_id IN ( ' . implode( ',', $arrstrEmployeeCompensationFilter['department_ids'] ) . ') ' : '';
		$strWhereClause	.= ( false == empty( $arrstrEmployeeCompensationFilter['bonus_report'] ) && true == $arrstrEmployeeCompensationFilter['bonus_report'] ) ? ' ' : ' AND ecl.bonus_employee_encryption_association_id IS NULL ';

		if( false == empty( $arrstrEmployeeCompensationFilter['dates'] ) ) {
			if( 1 == $arrstrEmployeeCompensationFilter['dates'] && true == valStr( $arrstrEmployeeCompensationFilter['single_date'] ) ) {
				$strWhereClause	.= ' AND ecl.requested_on BETWEEN \'' . $arrstrEmployeeCompensationFilter['single_date'] . ' 00:00:00 \' AND \'' . $arrstrEmployeeCompensationFilter['single_date'] . ' 23:59:59 \'';
			} elseif( 2 == $arrstrEmployeeCompensationFilter['dates'] && true == valStr( $arrstrEmployeeCompensationFilter['start_date'] ) && true == valStr( $arrstrEmployeeCompensationFilter['end_date'] ) ) {
				$strWhereClause	.= ' AND ecl.requested_on BETWEEN \'' . $arrstrEmployeeCompensationFilter['start_date'] . ' 00:00:00 \' AND \'' . $arrstrEmployeeCompensationFilter['end_date'] . ' 23:59:59\'';
			}
		}

		$strSql = ' SELECT
						' . $strSelectClause . '
					FROM
						employee_compensations ec
						LEFT JOIN employee_encryption_associations eea ON ( ec.employee_encryption_association_id = eea.id AND eea.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND eea.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						JOIN employees e ON ( ec.employee_id = e.id )
						JOIN employee_compensation_logs ecl ON ( ecl.employee_id = e.id AND ecl.is_verified = 1 )
						JOIN employee_addresses ea ON( ec.employee_id = ea.employee_id AND ea.country_code = \'' . $strCountryCode . '\'' . ' AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN departments d ON ( d.id = e.department_id )
					WHERE
						' . $strWhereClause . $strOrderByClause . '
					OFFSET ' . ( int ) $intOffset;

		if( true == $boolForCount ) {
			$arrintEmployeesCount = fetchData( $strSql, $objAdminDatabase );
			return ( true == valArr( $arrintEmployeesCount ) ) ? $arrintEmployeesCount[0]['count'] : 0;
		} else {
			if( 0 < $intLimit ) $strSql .= ' LIMIT ' . ( int ) $intLimit;
			return self::fetchEmployeeCompensations( $strSql, $objAdminDatabase );
		}
	}

	public static function fetchAllEmployeeCompensations( $objDatabase ) {

		$strSql = ' SELECT
						 *
					FROM
						employee_compensations';

		return self::fetchEmployeeCompensations( $strSql, $objDatabase );
	}

	public static function fetchEmployeesDetailsWithCompensations( $arrstrFilteredExplodedSearch, $objDatabase, $arrintEmployeeIds ) {
		if( false == valArr( $arrstrFilteredExplodedSearch ) || false == valArr( $arrintEmployeeIds ) ) return NULL;
		$strSearchCriteria = '';
		if( true == valArr( $arrstrFilteredExplodedSearch, 1 ) && true == isset( $arrstrFilteredExplodedSearch[0] ) && true == is_numeric( $arrstrFilteredExplodedSearch[0] ) ) {
			$strSearchCriteria .= ' ( e.employee_number = ' . ( int ) $arrstrFilteredExplodedSearch[0] . ' ) ';
		} else {
			$strSearchCriteria .= ' ( e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
									OR e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' ';
			$strSearchCriteria .= ' OR e.payroll_remote_primary_key ILIKE \'%' . $arrstrFilteredExplodedSearch[0] . '%\' )';
		}

		$strSql = 'SELECT
						e.id AS employee_id,
						e.name_full AS employee_name,
						e.employee_number AS employee_number,
						e.payroll_remote_primary_key AS payroll_id,
						dp.name AS department_name,
						d.name AS designation_name
					FROM employee_compensations ec
						JOIN employees e ON( e.id = ec.employee_id )
						LEFT JOIN designations d ON( d.id = e.designation_id )
						LEFT JOIN departments dp ON( dp.id = e.department_id )
					WHERE ' . $strSearchCriteria . '
						AND e.id IN( ' . implode( ', ', $arrintEmployeeIds ) . ' )
					ORDER BY
						employee_id,
						employee_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeCompensationDetailsByEmployeeId( $intEmployeeId, $intViewerEmployeeId, $objDatabase ) {
		if( true == empty( $intViewerEmployeeId ) || true == empty( $intEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						ec.*,
						eea.encrypted_amount AS compensation_encrypted
				 	FROM
						employee_compensations ec
						LEFT JOIN employee_encryption_associations eea ON ( ec.employee_encryption_association_id = eea.id AND eea.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND eea.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
					WHERE
						ec.employee_id = ' . ( int ) $intEmployeeId;

		return self::fetchEmployeeCompensation( $strSql, $objDatabase );
	}

}
?>