<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePolicies
 * Do not add any new functions to this class.
 */

class CEmployeePolicies extends CBaseEmployeePolicies {

	public static function fetchEmployeePolicyByEmployeeIdByPolicyId( $intEmployeeId, $intPolicyId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) || false == is_numeric( $intPolicyId ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						employee_policies
					WHERE
						employee_id = ' . $intEmployeeId . '
					AND
						policy_id	= ' . $intPolicyId;

		return self::fetchEmployeePolicy( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeePolicies( $arrmixEmployeePolicyFilter, $intPageNo, $intPageSize, $strOrderByType = NULL, $strOrderByField = NULL, $boolIsDownloadRequest, $objDatabase ) {

		if( false == is_numeric( $intPageNo ) && false == is_numeric( $intPageSize ) ) {
			return NULL;
		}
		$strWhereCondition = '';

		if( ( true == array_key_exists( 'from_date', $arrmixEmployeePolicyFilter ) && true == valStr( $arrmixEmployeePolicyFilter['from_date'] ) )
		    && ( true == array_key_exists( 'to_date', $arrmixEmployeePolicyFilter ) && true == valStr( $arrmixEmployeePolicyFilter['to_date'] ) ) ) {
			$strWhereCondition .= 'AND p.created_on BETWEEN \'' . $arrmixEmployeePolicyFilter['from_date'] . ' 00:00:00\' AND \'' . $arrmixEmployeePolicyFilter['to_date'] . ' 23:59:59\'';
		}

		if( true == array_key_exists( 'policies', $arrmixEmployeePolicyFilter ) && true == valArr( $arrmixEmployeePolicyFilter['policies'] ) ) {
			$arrmixEmployeePolicyFilter['policies'] = str_replace( ' ', '|', $arrmixEmployeePolicyFilter['policies'] );
			$strWhereCondition .= ' AND p.id IN( ' . implode( ',', $arrmixEmployeePolicyFilter['policies'] ) . ' )';
		}

		if( true == array_key_exists( 'policy_status', $arrmixEmployeePolicyFilter ) && true == valStr( $arrmixEmployeePolicyFilter['policy_status'] ) ) {
			$arrmixEmployeePolicyFilter['policy_status'] = str_replace( ' ', '|', $arrmixEmployeePolicyFilter['policy_status'] );
			if( '1' == $arrmixEmployeePolicyFilter['policy_status'] ) {
				$strWhereCondition .= ' AND ep.signed_on IS NOT NULL';
			} else {
				$strWhereCondition .= ' AND ep.signed_on IS NULL';
			}

		}

		if( true == array_key_exists( 'is_published', $arrmixEmployeePolicyFilter ) && true == valStr( $arrmixEmployeePolicyFilter['is_published'] ) ) {
			if( '1' == $arrmixEmployeePolicyFilter['is_published'] ) {
				$strWhereCondition .= ' AND p.is_published = true ';
			} else {
				$strWhereCondition .= ' AND p.is_published = false ';
			}
		}

		$intOffset		= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit 		= ( int ) $intPageSize;

		$strOrderClause = ' ORDER BY  ' . $strOrderByField . ' ' . $strOrderByType;

		$strSql = 'SELECT
						e.id as employee_id,
						e.preferred_name as employee_name,
						d.name as designation,
						p.name as policy_name,
						ep.signed_on,
						e.email_address,
						s.name as state_name,
						( CASE WHEN TRUE = p.is_published THEN 1 ELSE 0 END ) AS is_published
					FROM
						policies p
						JOIN employee_policy_associations epa ON p.id = epa.policy_id
						LEFT JOIN designations d ON d.id = ANY( epa.designation_ids )
						LEFT JOIN employees e ON e.designation_id = d.id AND e.designation_id = ANY( epa.designation_ids )
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employee_policies ep ON ( ep.policy_id = p.id AND e.id = ep.employee_id )
						LEFT JOIN states s ON ( ea.state_code = s.code )
					WHERE
						p.deleted_by IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND p.country_code = \'' . $arrmixEmployeePolicyFilter['login_user_countrycode'] . '\'
						AND 1 =
								CASE
									WHEN epa.state_codes IS NOT NULL AND ea.state_code = ANY( epa.state_codes ) THEN 1
									WHEN epa.state_codes IS NULL THEN 1
									ELSE 0
								END
						' . $strWhereCondition . '
						' . $strOrderClause;

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) && false == $boolIsDownloadRequest ) {
			$strSql .= ' OFFSET	' . ( int ) $intOffset . ' LIMIT	' . ( int ) $intLimit;
		}

		$arrmixEmployeePolicies['data'] = fetchData( $strSql, $objDatabase );

		if( false == $boolIsDownloadRequest ) {
			$strCountSql = 'SELECT
						count( p.id )
					FROM
						policies p
						JOIN employee_policy_associations epa ON p.id = epa.policy_id
						LEFT JOIN designations d ON d.id = ANY( epa.designation_ids )
						LEFT JOIN employees e ON e.designation_id = d.id AND e.designation_id = ANY( epa.designation_ids )
						LEFT JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employee_policies ep ON ep.policy_id = p.id AND e.id = ep.employee_id
						LEFT JOIN states s ON ( ea.state_code = s.code )
					WHERE
						p.deleted_by IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND p.country_code = \'' . $arrmixEmployeePolicyFilter['login_user_countrycode'] . '\'
						AND 1 =
								CASE
									WHEN epa.state_codes IS NOT NULL AND ea.state_code = ANY( epa.state_codes ) THEN 1
									WHEN epa.state_codes IS NULL THEN 1
									ELSE 0
								END
						' . $strWhereCondition;

			$arrintResponse = fetchData( $strCountSql, $objDatabase );
			if( true == isset( $arrintResponse[0]['count'] ) ) $arrmixEmployeePolicies['count'] = $arrintResponse[0]['count'];
		}

		return $arrmixEmployeePolicies;
	}

	public static function fetchEmployeePoliciesByPolicyIdsByEmployeeIds( $arrintPolicyIds, $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintPolicyIds ) ) return NULL;

		$strWhereCondition = '';

		if( true == valArr( $arrintEmployeeIds ) ) {
			$strWhereCondition	= 'ep.employee_id IN (' . implode( ',', $arrintEmployeeIds ) . ') AND ';
		}

		$strSql = 'SELECT
						ep.*
					FROM
						employee_policies ep
					WHERE
						' . $strWhereCondition . ' ep.policy_id IN (' . implode( ',', $arrintPolicyIds ) . ')';

		return self::fetchEmployeePolicies( $strSql, $objDatabase );

	}

}
?>
