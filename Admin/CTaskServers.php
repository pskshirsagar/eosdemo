<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskServers
 * Do not add any new functions to this class.
 */

class CTaskServers extends CBaseTaskServers {

	public static function fetchTaskServersByTaskId( $intTaskId, $objDatabase ) {

		$strSql = 'SELECT * FROM task_servers WHERE task_id = ' . ( int ) $intTaskId;
		return self::fetchTaskServers( $strSql, $objDatabase );
	}

	public static function deleteTaskServersByTaskServerIdsByTaskId( $arrintTaskServerIds, $intTaskId, $objDatabase ) {
		if( false == valArr( $arrintTaskServerIds ) ) return NULL;

		$strSql = 'DELETE FROM task_servers WHERE hardware_id IN ( ' . implode( ',', $arrintTaskServerIds ) . ' ) AND task_id = ' . ( int ) $intTaskId;

		if( false == $objDatabase->execute( $strSql ) ) {
			return false;
		}

		return true;
	}
}
?>