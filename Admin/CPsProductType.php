<?php

class CPsProductType extends CBasePsProductType {

	const LEASING_PRODUCTS			= 1;
	const MANAGEMENT_PRODUCTS		= 2;
	const MARKETING_PRODUCTS		= 3;
	const RESIDENT_PRODUCTS			= 4;
	const MISCELLANEOUS_PRODUCTS	= 5;
	const CUSTOM_BUNDLE				= 6;

	protected $m_arrobjPsProducts;

	/**
	 * Add Functions
	 */

	public function addPsProduct( $objPsProduct ) {
		$this->m_arrobjPsProducts[$objPsProduct->getId()] = $objPsProduct;
	}

	/**
	 * Get Functions
	 */

	public function getPsProducts() {
		return $this->m_arrobjPsProducts;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>