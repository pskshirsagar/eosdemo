<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainings
 * Do not add any new functions to this class.
 */

class CTrainings extends CBaseTrainings {

	public static function fetchTrainingsByIds( $arrintTrainingIds, $objDatabase ) {

		if( false == valArr( $arrintTrainingIds ) ) return NULL;

		$strSql = 'SELECT t.*,
						t1.name AS training_parent_name
					FROM
						trainings t
						LEFT JOIN trainings t1 ON t1.id = t.parent_id
					WHERE t.id IN ( ' . implode( ',', $arrintTrainingIds ) . ' )';

		return self::fetchTrainings( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTrainingsByFilter( $objTrainingsFilter, $objDatabase ) {

		$intOffset = ( 0 < $objTrainingsFilter->getPageNo() ) ? $objTrainingsFilter->getPageSize() * ( $objTrainingsFilter->getPageNo() - 1 ) : 0;
		$intLimit  = ( int ) $objTrainingsFilter->getPageSize();
		$strOrderClause = 'ORDER BY updated_on DESC ';
		$strOrder = '';

		if( false == is_null( $objTrainingsFilter ) && false == is_null( $objTrainingsFilter->getOrderByField() ) && false == is_null( $objTrainingsFilter->getOrderByType() ) ) {
			$strOrder = ( 1 == $objTrainingsFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objTrainingsFilter->getOrderByField() . '' . $strOrder;
		}

		$strSql = '	SELECT t.*,
						e.preferred_name AS name_full,
						d.name as department_name,
						t1.name AS training_parent_name
					FROM trainings t
						LEFT JOIN departments d ON t.department_id = d.id
						JOIN users u ON t.created_by = u.id
						JOIN employees e ON u.employee_id = e.id
						LEFT JOIN trainings t1 ON t1.id = t.parent_id
					WHERE t.deleted_by IS NULL AND t.deleted_on IS NULL ' . $strOrderClause . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchTrainings( $strSql, $objDatabase );
	}

	public static function fetchAllUnDeletedTrainings( $objDatabase ) {

		$strSql = 'SELECT t.*
					FROM trainings as t
					WHERE t.deleted_by IS NULL AND t.deleted_on IS NULL ORDER BY t.name';
		return self::fetchTrainings( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchTraining( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT t.*,
						e.preferred_name AS name_full,
						d.name as department_name
					FROM trainings t
						LEFT JOIN departments d ON t.department_id = d.id
						LEFT JOIN users u ON t.created_by = u.id
						LEFT JOIN employees e ON u.employee_id = e.id
					WHERE t.deleted_by IS NULL AND t.deleted_on IS NULL
						AND t.name ILIKE \'%' . implode( '%\' AND t.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					ORDER BY t.id DESC LIMIT 10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingDetailsById( $intTrainingId, $objDatabase ) {

		$strSql = 'SELECT t.*,
							e.preferred_name AS name_full,
							d.name as department_name,
							t1.name as training_parent_name
					FROM trainings t
							LEFT JOIN departments d ON t.department_id = d.id
							JOIN users u ON t.created_by = u.id
							JOIN employees e ON u.employee_id = e.id
							LEFT JOIN trainings t1 ON t1.id = t.parent_id
					WHERE t.deleted_by IS NULL AND t.deleted_on IS NULL
							AND t.id=' . ( int ) $intTrainingId;

		return self::fetchTraining( $strSql, $objDatabase );
	}

	public static function fetchTrainingDetailsByParentId( $intParentId, $objDatabase ) {

		$strSql = 'SELECT
						t.*,
						t1.name AS training_parent_name
					FROM
						trainings t
						LEFT JOIN trainings t1 ON t1.id = t.parent_id
					WHERE
						t.parent_id = ' . ( int ) $intParentId . '
						AND t.deleted_by IS NULL';

		return self::fetchTrainings( $strSql, $objDatabase );
	}

	public static function fetchParentTrainings( $objDatabase ) {
		return self::fetchTrainings( 'SELECT * FROM trainings WHERE parent_id IS NULL AND deleted_by IS NULL ORDER BY name', $objDatabase );
	}

}
?>