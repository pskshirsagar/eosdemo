<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsDocuments
 * Do not add any new functions to this class.
 */

class CPsDocuments extends CBasePsDocuments {

	const DEFAULT_ORDER_BY_FIELD = 'id';

	public static function fetchClientPsDocuments( $intCid, $objAdminDatabase ) {
		return self::fetchPsDocuments( 'SELECT * FROM ps_documents WHERE id NOT IN (SELECT ps_document_id FROM contract_documents ) AND cid = ' . ( int ) $intCid, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByPsDocumentId( $intPsDocumentId, $objAdminDatabase ) {
		if( false == is_numeric( $intPsDocumentId ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						ps_documents
					WHERE
						id = ' . ( int ) $intPsDocumentId;

		return self::fetchPsDocument( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentByEmployeeId( $intEmployeeId, $objAdminDatabase ) {

		return self::fetchPsDocument( 'SELECT * FROM ps_documents WHERE ps_document_type_id = ' . CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_MAIN_PHOTO . ' AND employee_id = ' . ( int ) $intEmployeeId, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByIds( $arrintPsDocumentIds, $objAdminDatabase, $boolOrderAsPerInClause = false ) {
		if( false == valArr( $arrintPsDocumentIds ) ) return NULL;

		$strOrderBy = 'id';
		if( true == $boolOrderAsPerInClause ) {
			$strOrderBy = 'POSITION( id::text in \'(' . implode( ',', $arrintPsDocumentIds ) . ')\' )';
		}

		$strSql = 'SELECT * FROM ps_documents WHERE id IN ( ' . implode( ',', $arrintPsDocumentIds ) . ' ) ORDER BY ' . $strOrderBy;

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchFileNameByPsDocumentId( $intId, $objAdminDatabase ) {
		$strSql = 'SELECT file_name FROM ps_documents WHERE id = ' . ( int ) $intId;
		return self::fetchPsDocument( $strSql, $objAdminDatabase );
	}

	public static function fetchPaginatedPsDocuments( $intPageNo, $intPageSize, $objAdminDatabase, $intPsDocumentTypeId = NULL ) {

		$intOffset		= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit		= ( int ) $intPageSize;
		$strCondition	= '';

		if( false == is_null( $intPsDocumentTypeId ) ) {
			$strCondition = ' AND ps_document_type_id = ' . ( int ) $intPsDocumentTypeId;
		}

		$strSql = ' SELECT
						*
					FROM
						ps_documents
					WHERE
						employee_id IS NULL
						AND	ps_document_category_id IS NULL ' . $strCondition . '
					ORDER BY
						created_on DESC
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPaginatedPsDocumentsCount( $objAdminDatabase, $intPsDocumentTypeId = NULL ) {
		$strWhere = ' WHERE employee_id IS NULL AND cid IS NULL AND ps_document_category_id IS NULL';
		if( false == is_null( $intPsDocumentTypeId ) ) {
			$strWhere .= ' AND ps_document_type_id = ' . ( int ) $intPsDocumentTypeId;
		}
		return self::fetchPsDocumentCount( $strWhere, $objAdminDatabase );
	}

	public static function fetchPsDocumentById( $intId, $objAdminDatabase ) {
		$strSql = ' SELECT * FROM ps_documents WHERE id = ' . ( int ) $intId;
		return self::fetchPsDocument( $strSql, $objAdminDatabase );
	}

	public static function fetchSupportUserGuides( $intDocumentTypeId, $strProductIds, $objAdminDatabase, $strPsDocumentsOrderBy = 'd.order_num' ) {

		// added by george

		if( false == valArr( $strProductIds ) ) {
			$strCondition = ' AND c.id = ' . CPsDocumentCategory::GENERAL;
		} else {
			$strCondition = ' AND ( d.ps_product_id IN (' . implode( ',', array_keys( $strProductIds ) ) . ') OR c.id = ' . CPsDocumentCategory::GENERAL . ' )';
		}

		// This SQL is used in ResidentWords //
		$strSql = ' SELECT
						d.*
					FROM
						ps_documents d
						INNER JOIN ps_document_categories c	ON d.ps_document_category_id = c.id
					WHERE
						d.ps_document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND d.deleted_on IS NULL
						AND	d.is_published = 1
						AND	c.is_published = 1
						AND d.file_name Like \'%.pdf\'
						AND d.file_name NOT Like \'%qrg.pdf\'
						AND d.file_name NOT Like \'%template.pdf\'
						' . $strCondition . '
					ORDER BY c.name,' . $strPsDocumentsOrderBy . '	ASC';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );

	}

	public static function fetchPsDocumentsByPsDocumentTypeId( $intPsDocumentTypeId, $objAdminDatabase ) {

		$strSql = 'SELECT
						d.*
					FROM
						ps_documents d
					JOIN
						ps_document_categories c ON ( d.ps_document_category_id = c.id )
					WHERE
						d.ps_document_type_id = ' . ( int ) $intPsDocumentTypeId . '
						AND	d.ps_document_category_id IS NOT NULL
						AND d.deleted_by IS NULL
						AND d.deleted_on IS NULL
					ORDER BY
						c.order_num,
						d.order_num ASC';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByPsDocumentCategoryId( $intPsDocumentCategoryId, $objAdminDatabase ) {

		// Used in CPsDocumentCategory.class for valDependantInformation() function
		$strSql = 'SELECT
						COUNT(id) AS ps_documents_category_count
					FROM
						ps_documents
					WHERE
						ps_document_category_id = ' . ( int ) $intPsDocumentCategoryId;

		return self::fetchPsDocument( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByPsDocumentCategoryIds( $arrintPsDocumentCategoryIds, $objAdminDatabase ) {

		$strSql = 'SELECT
						pd.*
					FROM
						ps_documents pd
					WHERE
						pd.ps_document_category_id IN (' . implode( ',', $arrintPsDocumentCategoryIds ) . ' )
						AND pd.deleted_by IS NULL ORDER BY pd.created_on';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchContractPsDocumentsByContractId( $intContractId, $objAdminDatabase, $intContractDocumentTypeId = NULL ) {

		if( valId( $intContractDocumentTypeId ) ) {
			$strWhereCondition = ' AND cd.contract_document_type_id != ' . $intContractDocumentTypeId;
		}
		$strSql = 'SELECT
						pd.*,
						cd.contract_document_type_id,
						cdt.name as contract_document_type_name
					FROM ps_documents pd
						JOIN contract_documents cd ON ( pd.id = cd.ps_document_id )
						JOIN contract_document_types cdt ON ( cdt.id = cd.contract_document_type_id )
					WHERE
						cd.contract_id = ' . ( int ) $intContractId . ' AND pd.deleted_by IS NULL AND cd.deleted_by IS NULL ' . $strWhereCondition .
					' ORDER BY pd.created_on DESC';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsWithSearchCriteriaByEmployeeIdByPsDocumentTypeIds( $arrstrFilteredExplodedSearch, $intEmployeeId, $arrintDocumentTypeIds, $boolIsShowDisabledData, $objAdminDatabase ) {

		$strSql = 'SELECT
							pd.id,
							pd.title,
							pd.file_name,
							dt.name AS document_type_name
						FROM
							ps_documents pd
							LEFT JOIN ps_document_types dt ON ( pd.ps_document_type_id = dt.id )
						WHERE
							pd.ps_document_type_id IN ( ' . implode( ', ', $arrintDocumentTypeIds ) . ' )
							AND pd.employee_id = ' . ( int ) $intEmployeeId . '
							AND pd.deleted_by IS NULL
							AND
								( pd.title ILIKE \'%' . implode( '%\' AND pd.title ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
									OR dt.name ILIKE \'%' . implode( '%\' AND dt.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\')
						';

		if( true == $boolIsShowDisabledData ) {
			$strSql .= ' ORDER BY pd.title ASC LIMIT 10';
		} else {
			$strSql .= ' AND pd.deleted_by IS NULL ORDER BY pd.title ASC LIMIT 10';
		}

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchPaginatedPsDocumentsByCid( $intCid, $arrstrPsDocumentFilter, $objAdminDatabase, $boolCount = false ) {

		$strOrderByCondition	= '';
		$strWhereCondition		= '';
		$strSelectField			= ' count( pd.id ) count ';

		if( false == $boolCount ) {

			$intOffset       = ( false == empty( $arrstrPsDocumentFilter['page_no'] ) && false == empty( $arrstrPsDocumentFilter['page_size'] ) ) ? $arrstrPsDocumentFilter['page_size'] * ( $arrstrPsDocumentFilter['page_no'] - 1 ) : 0;
			$intLimit        = ( false == empty( $arrstrPsDocumentFilter['page_size'] ) ) ? $arrstrPsDocumentFilter['page_size'] : '';
			$strOrderByField = ( false == empty( $arrstrPsDocumentFilter['order_by_field'] ) ) ? $arrstrPsDocumentFilter['order_by_field'] : ' created_on ';

			$strOrderByType = ' DESC';

			if( true == isset( $arrstrPsDocumentFilter['order_by_type'] ) ) {
				$strOrderByType = ( '1' == $arrstrPsDocumentFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}

			if( 'name' == $strOrderByField ) {
				$strOrderByField = 'pdt.name' . $strOrderByType . ', cdt.name';
			} elseif( 'title' == $strOrderByField ) {
				$strOrderByField = 'pd.title' . $strOrderByType . ', pd.file_name';
			} elseif( 'contract_id' == $strOrderByField ) {
				$strOrderByField = 'cd.' . $strOrderByField;
			} else {
				$strOrderByField = 'pd.' . $strOrderByField;
			}

			$strOrderByCondition = ' ORDER BY
										' . $strOrderByField . ' ' . $strOrderByType . '
									OFFSET
										' . ( int ) $intOffset;

			if( false == is_null( $intLimit ) ) {
				$strOrderByCondition .= ' LIMIT	' . ( int ) $intLimit;
			}

			$strSelectField = ' pd.id,
								pd.title,
								pd.file_name,
								pd.description,
								pd.created_on,
								pd.ps_document_type_id,
								cd.contract_document_type_id,
								cd.contract_id,
								pdt.name AS type_name,
								pd.associated_ps_document_id,
								cdt.name AS contract_document_type_name ';
		}

		if( true == valArr( $arrstrPsDocumentFilter['product'] ) ) {

			$strJoinCondition = 'LEFT JOIN contract_properties cp ON ( cp.contract_id = cd.contract_id )';

			$arrintProductIdLists = implode( ',', $arrstrPsDocumentFilter['product'] );
			$strWhereCondition .= ' AND cp.ps_product_id IN ( ' . $arrintProductIdLists . ' ) ';
		}

		if( true == valArr( $arrstrPsDocumentFilter['property'] ) ) {

			$strJoinCondition = 'LEFT JOIN contract_properties cp ON ( cp.contract_id = cd.contract_id )';

			$arrintPropertyIdLists = implode( ',', $arrstrPsDocumentFilter['property'] );
			$strWhereCondition .= ' AND cp.property_id IN ( ' . $arrintPropertyIdLists . ' ) ';
		}

		if( true == valStr( $arrstrPsDocumentFilter['ps_document_type_id'] ) ) {
			$strWhereCondition .= ' AND pd.ps_document_type_id = ' . $arrstrPsDocumentFilter['ps_document_type_id'];
		}

		if( true == valId( $arrstrPsDocumentFilter['contract_document_type_id'] ) ) {
			$strWhereCondition .= ' AND cd.contract_document_type_id = ' . $arrstrPsDocumentFilter['contract_document_type_id'];
		}

		if( true == valStr( $arrstrPsDocumentFilter['create_date'] ) ) {
			$strWhereCondition .= ' AND date_trunc(\'day\', pd.created_on)  = \'' . $arrstrPsDocumentFilter['create_date'] . '\' ';
		}

		$strSql = 'SELECT
						' . $strSelectField . '
					FROM
						ps_documents pd
						JOIN ps_document_types pdt ON ( pd.ps_document_type_id = pdt.id  )
						LEFT JOIN 
							(
								SELECT
									DISTINCT sub_cd.ps_document_id AS ps_document_id,
									sub_cd.contract_id,
									sub_cd.contract_document_type_id
								FROM
									contract_documents sub_cd
									JOIN contracts sub_c ON ( sub_c.id = sub_cd.contract_id  )
									LEFT JOIN contract_properties sub_cp ON ( sub_cd.cid = sub_cp.cid AND sub_cp.contract_id = sub_cd.contract_id )
									WHERE
									sub_cd.cid = ' . ( int ) $intCid . '
									AND sub_cp.renewal_contract_property_id IS NULL
									AND sub_cp.is_last_contract_record = TRUE
									AND sub_cp.termination_date IS NULL
									AND sub_c.contract_status_type_id NOT IN ( ' . implode( ',', CContractStatusType::$c_arrintDeactivatedContractStatusTypeIds ) . ' )
							) cd ON ( cd.ps_document_id = pd.id )
						LEFT JOIN contract_document_types cdt ON ( cd.contract_document_type_id = cdt.id AND pd.ps_document_type_id = ' . CPsDocumentType::DOCUMENT_TYPE_SUPPORT_DOCUMENT . ' )
						' . $strJoinCondition . '
					WHERE
						pd.cid = ' . ( int ) $intCid . '
						AND pd.associated_ps_document_id IS NULL
						AND pd.ps_document_type_id IN( ' . implode( ',', CPsDocumentType::$c_arrintPsLeadDocumentTypeIds ) . ' )
						AND pd.deleted_on IS NULL
						AND pd.id NOT IN (
											SELECT
												DISTINCT ps_document_id
											FROM
												contract_documents cd
											WHERE
												( contract_document_type_id = ' . CContractDocumentType:: INITIAL_CONTRACT . '
												AND contract_id IN (
													SELECT
														contract_id
													FROM
														contract_documents
													WHERE
														cid = ' . ( int ) $intCid . '
														AND deleted_on IS NULL
														AND contract_document_type_id IN ( ' . implode( ',', CContractDocumentType::$c_arrintSignedContractDocumentTypeIds ) . ' )
													GROUP BY contract_id
												) )
									)
						AND pd.file_name NOT IN 
						( 
							SELECT 
								document.value AS file_name 
							FROM 
								contract_termination_events cte
								JOIN JSONB_EACH_TEXT ( ( cte.event_parameters::jsonb ->> \'DOCUMENT_NAME\' )::jsonb ) AS document ON TRUE
								JOIN contract_termination_requests ctr ON ( ctr.id = cte.contract_termination_request_id ) 
							WHERE 
								cte.cid = ' . ( int ) $intCid . '
								AND ctr.contract_termination_request_status_id NOT IN ( ' . sqlIntImplode( CContractTerminationRequestStatus::$c_arrintCompletedTerminationRequestStatusIds ) . ' )
								AND cte.contract_termination_event_type_id = ' . CContractTerminationEventType::TERMINATION_DOCUMENT . ' 
						)			
						' . $strWhereCondition . '
					GROUP BY
						pd.id,
						pd.associated_ps_document_id,
						pd.title,
						pd.description,
						cd.contract_id,
						pd.file_name,
						cd.contract_document_type_id,
						pdt.name,
						cdt.name ' . $strOrderByCondition;

		if( false == $boolCount ) {
			return self::fetchPsDocuments( $strSql, $objAdminDatabase );
		} else {
			$arrintResponse = fetchData( $strSql, $objAdminDatabase );

			if( true == valArr( $arrintResponse ) ) return \Psi\Libraries\UtilFunctions\count( $arrintResponse );
			return 0;
		}
	}

	public static function fetchContractIdsByCid( $intCid, $arrstrPsDocumentFilter, $objAdminDatabase ) {

		$strOrderByField	= ( false == empty( $arrstrPsDocumentFilter['order_by_field'] ) ) ? $arrstrPsDocumentFilter['order_by_field'] : ' contract_id ';
		$strOrderByType		= ' DESC';

		if( true == isset( $arrstrPsDocumentFilter['order_by_type'] ) ) {
			$strOrderByType = ( '1' == $arrstrPsDocumentFilter['order_by_type'] ) ? ' ASC' : ' DESC';
		}

		$strSql = 'SELECT
						DISTINCT ( cd.contract_id ) as contract_id
					FROM
						ps_documents pd
						JOIN ps_document_types pdt ON ( pd.ps_document_type_id = pdt.id )
						LEFT JOIN contract_documents cd ON ( cd.ps_document_id = pd.id )
						LEFT JOIN contract_properties cp ON ( cp.contract_id = cd.contract_id )
						LEFT JOIN contract_document_types cdt ON ( cd.contract_document_type_id = cdt.id AND pd.ps_document_type_id = ' . CPsDocumentType::DOCUMENT_TYPE_SUPPORT_DOCUMENT . ' )
						LEFT JOIN
							(
							SELECT
								id,
								associated_ps_document_id
							FROM
								ps_documents
							WHERE
								deleted_on IS NULL
							GROUP BY
							  id
						) apd ON ( apd.associated_ps_document_id = pd.id )
					WHERE
						pd.cid = ' . ( int ) $intCid . '
						AND pd.associated_ps_document_id IS NULL
						AND pd.deleted_by IS NULL
						AND pd.deleted_on IS NULL
						AND cd.contract_id IS NOT NULL
					ORDER BY
						' . $strOrderByField . ' ' . $strOrderByType;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchQuickSearchClientPsDocuments( $intCid, $objAdminDatabase, $arrstrFilteredExplodedSearch ) {

		if( false == valArr( $arrstrFilteredExplodedSearch ) ) return NULL;

		$strSearchCriteria = ' AND( title ILIKE \'%' . implode( '%\' AND title ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )';

		$strSql = 'SELECT
						*
					FROM
						ps_documents
					WHERE
						id NOT IN (
									SELECT
										ps_document_id
									FROM
										contract_documents )
						AND cid = ' . ( int ) $intCid . $strSearchCriteria . ' AND deleted_by IS NULL AND deleted_ON IS NULL LIMIT 10';
		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentByEmployeeIdByPsDocumentTypeId( $intEmployeeId, $intPsDocumentTypeId, $objAdminDatabase ) {

		$strSql = '	SELECT
						*
					FROM
						ps_documents
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND ps_document_type_id = ' . ( int ) $intPsDocumentTypeId . '
						AND deleted_by IS NULL
					ORDER BY
						id DESC LIMIT 1';

		return self::fetchPsDocument( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentByIdsByEmployeeIds( $arrintIds, $arrintEmployeeIds, $objAdminDatabase ) {

		if( false == valArr( $arrintIds ) || false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						ps_documents
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND deleted_by IS NULL';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByTrainingSessionIdByPsDocumentTypeId( $intTrainingSessionId, $intPsDocumentTypeId, $objAdminDatabase ) {

		if( true == is_null( $intTrainingSessionId ) || true == is_null( $intPsDocumentTypeId ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						ps_documents
					WHERE
						training_session_id = ' . ( int ) $intTrainingSessionId . '
						AND ps_document_type_id = ' . ( int ) $intPsDocumentTypeId . '
						AND is_published = 1
						AND deleted_by IS NULL
					ORDER BY
						updated_on DESC';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchTrainingSessionPsDocumentsCountByDocumentTypeId( $intPsDocumentType, $objAdminDatabase ) {

		if( true == is_null( $intPsDocumentType ) ) return NULL;

		$strSql = 'SELECT
					pd.file_name,
					count ( pd.id )
				FROM
					ps_documents pd
				WHERE
					pd.deleted_on IS NULL
					AND pd.is_published = 1
					AND pd.ps_document_type_id = ' . ( int ) $intPsDocumentType . '
					AND pd.training_session_id IS NOT NULL
				GROUP BY
					pd.file_name';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchPublishedPsDocumentsByTrainingSessionIds( $arrintTrainingSessionIds, $objAdminDatabase ) {
		$strSql = 'SELECT
					*
				FROM
					ps_documents pd
				WHERE
					pd.training_session_id IN ( ' . implode( ',', $arrintTrainingSessionIds ) . ' )
					AND pd.is_published =1
					AND pd.deleted_on IS NULL';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByPsDocumentTypeIdByEmployeeId( $intPsDocumentTypeId, $intEmployeeId, $objAdminDatabase ) {

		if( true == is_null( $intPsDocumentTypeId ) || true == is_null( $intEmployeeId ) ) return false;

		$strSql = 'SELECT
						d.*
					FROM
						ps_documents d
					JOIN
						ps_document_categories c ON ( d.ps_document_category_id = c.id )
					WHERE
						d.ps_document_type_id = ' . ( int ) $intPsDocumentTypeId . '
						AND d.employee_id = ' . ( int ) $intEmployeeId . '
						AND d.deleted_by IS NULL
						AND d.deleted_on IS NULL
					ORDER BY
						c.order_num,
						d.order_num ASC';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsOnlyByCid( $intCid, $objAdminDatabase ) {

		$strSql = 'SELECT
						id,
						file_name
					FROM
						ps_documents pd
					WHERE
						cid = ' . ( int ) $intCid . '
						AND associated_ps_document_id IS NULL
						AND id NOT IN (
										SELECT
											ps_document_id
										FROM
											contract_documents
						)
						AND deleted_on IS NULL
						AND ps_document_type_id <> ' . CPsDocumentType::CONTRACT_DRAFT . '
					ORDER BY
						created_on DESC';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchAssociatedPsDocumentById( $intPsDocumentId, $intCid, $objAdminDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ps_documents
					WHERE
						associated_ps_document_id = ' . ( int ) $intPsDocumentId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL';

		return self::fetchPsDocument( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByEmployeeIdByPsDocumentTypeId( $intEmployeeId, $intPsDocumentTypeId, $intRequestTypeId = CRequestType::REIMBURSEMENT, $strOrderByFieldName = self::DEFAULT_ORDER_BY_FIELD, $strOrderField = NULL, $intPageNo, $intPageSize, $boolIsShowAssociatedExpense = false, $objAdminDatabase ) {

		if( false == valId( $intEmployeeId ) || false == valId( $intPsDocumentTypeId ) ) return NULL;

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		if( false == $boolIsShowAssociatedExpense ) {
			$strWhereClause = 'AND redoc.is_expense_associated IS NULL';
		}
		$strSql = 'SELECT
						psdoc.id,
						psdoc.file_name,
						psdoc.file_extension_id,
						psdoc.description,
						( CASE WHEN TRUE = redoc.is_expense_associated THEN 1 ELSE 0 END ) AS is_expense_associated,
						rr.title,
						rr.id AS reimbursement_request_id,
						' . ( int ) $intEmployeeId . ' AS requested_by,
						psdoc.updated_on,
						epst.name AS status,
						epst.id AS status_type_id,
						( CASE WHEN TRUE = rr.is_locked THEN 1 ELSE 0 END ) AS is_locked
					FROM
						ps_documents psdoc
						LEFT JOIN reimbursement_documents redoc ON ( psdoc.id = redoc.ps_document_id )
						LEFT JOIN reimbursement_expenses re ON ( redoc.reference_id = re.id AND redoc.is_expense_associated = true )
						LEFT JOIN reimbursement_requests rr ON ( (re.reimbursement_request_id = rr.id AND redoc.is_expense_associated = true ) or ( rr.id = redoc.reference_id AND redoc.is_expense_associated = false ) )
						LEFT JOIN employee_pay_status_types epst ON ( epst.id = rr.employee_pay_status_type_id )
					WHERE
						psdoc.employee_id = ' . ( int ) $intEmployeeId . '
						AND psdoc.ps_document_type_id = ' . ( int ) $intPsDocumentTypeId . '
						AND psdoc.deleted_by IS NULL
						AND ( rr.request_type_id IS NULL OR rr.request_type_id = ' . ( int ) $intRequestTypeId . ' )
						' . $strWhereClause;

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$strOrderByFieldName = ( false == valStr( $strOrderByFieldName ) ) ? 'id' : $strOrderByFieldName;

		if( 'status' == $strOrderByFieldName || 'title' == $strOrderByFieldName ) {
			$strOrderBy = ' ORDER BY ' . ' ' . $strOrderByFieldName;
		} else {
			$strOrderBy = ' ORDER BY ' . ' psdoc.' . $strOrderByFieldName;
		}

		$strSql .= $strOrderBy . ' ' . $strOrderField . ' ' . $strSubSql;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByEmployeeIdPsDocumentTypeIdCount( $intEmployeeId, $intPsDocumentTypeId, $boolIsShowAssociatedExpense, $intRequestTypeId = CRequestType::REIMBURSEMENT, $objAdminDatabase ) {

		if( false == valId( $intEmployeeId ) || false == valId( $intPsDocumentTypeId ) ) return NULL;

		if( false == $boolIsShowAssociatedExpense ) {
			$strWhereClause = 'AND redoc.is_expense_associated IS NULL';
		}

		$strSql = '	SELECT
						count(psdoc.id)
					FROM
						ps_documents psdoc
						LEFT JOIN reimbursement_documents redoc ON ( psdoc.id = redoc.ps_document_id )
						LEFT JOIN reimbursement_expenses re ON ( redoc.reference_id = re.id AND redoc.is_expense_associated = true )
						LEFT JOIN reimbursement_requests rr ON ( (re.reimbursement_request_id = rr.id AND redoc.is_expense_associated = true ) or ( rr.id = redoc.reference_id AND redoc.is_expense_associated = false ) )
						LEFT JOIN employee_pay_status_types epst ON ( epst.id = rr.employee_pay_status_type_id )
					WHERE
						psdoc.employee_id = ' . ( int ) $intEmployeeId . '
						AND psdoc.ps_document_type_id = ' . ( int ) $intPsDocumentTypeId . '
						AND psdoc.deleted_by IS NULL
						AND rr.deleted_by IS NULL
						AND re.deleted_by IS NULL
						AND ( rr.request_type_id IS NULL OR rr.request_type_id = ' . ( int ) $intRequestTypeId . ' )
						' . $strWhereClause;

		$arrstrData = fetchData( $strSql, $objAdminDatabase );
		if( true == valArr( $arrstrData ) ) {
			return $arrstrData[0]['count'];
		} else {
			return NULL;
		}

	}

	public static function fetchPsDocumentByEmployeeIdByPsDocumentTypeIdById( $intEmployeeId, $intPsDocumentTypeId, $intPsDocumentId, $objAdminDatabase ) {

		if( false == valId( $intEmployeeId ) || false == valId( $intPsDocumentTypeId ) || false == valId( $intPsDocumentId ) ) return NULL;

		$strSql = 'SELECT
						psdoc.*
					FROM
						ps_documents psdoc
					WHERE
						psdoc.employee_id = ' . ( int ) $intEmployeeId . '
						AND psdoc.ps_document_type_id = ' . ( int ) $intPsDocumentTypeId . '
						AND psdoc.id = ' . ( int ) $intPsDocumentId . '
						AND psdoc.deleted_by IS NULL';

		return self::fetchPsDocument( $strSql, $objAdminDatabase );
	}

	public static function fetchFreePsDocumentsByEmployeeIdByPsDocumentTypeId( $intEmployeeId, $intPsDocumentTypeId, $objAdminDatabase ) {

		if( false == is_numeric( $intEmployeeId ) || false == is_numeric( $intPsDocumentTypeId ) ) return NULL;

		$strSql = 'SELECT
						psdoc.*
					FROM
						(
						SELECT
							*
						FROM
							ps_documents
						WHERE
							ps_document_type_id = ' . ( int ) $intPsDocumentTypeId . '
							AND employee_id = ' . ( int ) $intEmployeeId . '
							AND deleted_by IS NULL
						) psdoc
						LEFT JOIN reimbursement_documents redoc ON( psdoc.id = redoc.ps_document_id )
					WHERE
						reference_id IS NULL';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchAllPsDocumentsByEmployeeIdByPsDocumentTypeIdByReimbursementExpenseIdByPsDocumentId( $intEmployeeId, $intPsDocumentTypeId, $objAdminDatabase, $boolIsLocked = false, $intPsDocumentId = NULL, $intReimbursementExpenseId = NULL ) {

		if( false == is_numeric( $intEmployeeId ) || false == is_numeric( $intPsDocumentTypeId ) ) return NULL;

		if( true == is_numeric( $intPsDocumentId ) ) {
			$strWhereCondition = 'AND ( rd.ps_document_id = ' . ( int ) $intPsDocumentId . ' OR rd.ps_document_id IS NULL )';
		} elseif( true == $boolIsLocked && true == is_numeric( $intReimbursementExpenseId ) ) {
			$strWhereCondition = 'AND ( re.id = ' . ( int ) $intReimbursementExpenseId . ' )';
		} elseif( true == is_numeric( $intReimbursementExpenseId ) ) {
			$strWhereCondition = 'AND ( re.id = ' . ( int ) $intReimbursementExpenseId . ' OR rd.ps_document_id IS NULL )';
		} else {
			$strWhereCondition = 'AND ( rd.ps_document_id IS NULL )';
		}

		$strSql = 'SELECT
					pd.*,
					(	CASE
							WHEN rd.ps_document_id IS NOT NULL THEN 1
							ELSE 0
						END	) AS is_associated
						FROM
					ps_documents AS pd
					LEFT JOIN reimbursement_documents AS rd ON ( pd.id = rd.ps_document_id )
					LEFT JOIN reimbursement_expenses AS re ON ( rd.reference_id = re.id AND rd.is_expense_associated = true AND re.deleted_by IS NULL )
					LEFT JOIN reimbursement_requests AS rr ON ( ( re.reimbursement_request_id = rr.id AND rd.is_expense_associated = true AND rr.deleted_by IS NULL ) or ( rr.id = rd.reference_id AND rd.is_expense_associated = false AND rr.deleted_by IS NULL ) )
						WHERE
					pd.employee_id = ' . ( int ) $intEmployeeId . '
					AND pd.ps_document_type_id = ' . ( int ) $intPsDocumentTypeId . '
					AND pd.deleted_by IS NULL
					' . $strWhereCondition . ' ';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByPsNotificationIds( $arrintPsNotificationIds, $objAdminDatabase ) {
		if( false == valArr( $arrintPsNotificationIds ) ) return NULL;

		$strSql = 'SELECT
					pd.*
				FROM
					ps_notifications pn
					join ps_documents pd ON( pn.ps_document_id = pd.id )
				WHERE
					pn.id IN ( ' . implode( ',', $arrintPsNotificationIds ) . ' )
					AND pd.deleted_on IS NULL';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByEmployeeIdPsDocumentTypeIds( $intEmployeeId, $arrintPsDocumentTypeIds, $objAdminDatabase ) {
		if( false == is_numeric( $intEmployeeId ) || false == valArr( $arrintPsDocumentTypeIds ) ) return false;

		$strSql = 'SELECT
						pd.id,
						pd.title,
						pd.file_name,
						dt.name AS document_type_name,
						dt.ps_document_type_id,
						pd.deleted_by,
						pd.is_shared,
						pd.description
					FROM
						ps_documents pd
						JOIN ps_document_types dt ON ( pd.ps_document_type_id = dt.id )
					WHERE
						pd.ps_document_type_id IN ( ' . implode( ', ', $arrintPsDocumentTypeIds ) . ' )
						AND pd.deleted_by IS NULL
						AND pd.employee_id = ' . ( int ) $intEmployeeId;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByActionIdsByCid( $arrintActionIds, $objAdminDatabase, $boolIsFetchObject = false ) {
		if( false == valArr( $arrintActionIds ) ) return false;

		$strSql = 'SELECT
						pd.id,
						pd.cid,
						pd.ps_lead_id,
						pd.title,
						pd.file_name,
						ar.action_id
					FROM
						ps_documents pd
						JOIN action_references ar ON ( pd.id = ar.reference_number)
						JOIN actions a ON ( a.id = ar.action_id AND a.ps_lead_id = pd.ps_lead_id )
					WHERE
						ar.action_id IN ( ' . implode( ', ', $arrintActionIds ) . ' )
						AND pd.ps_document_type_id = ' . CPsDocumentType::DOCUMENT_TYPE_SUCESS_REVIEW . '
						AND pd.deleted_by IS NULL';

		if( true == $boolIsFetchObject ) {
			return self::fetchPsDocuments( $strSql, $objAdminDatabase );
		} else {
			return fetchData( $strSql, $objAdminDatabase );
		}

	}

	public static function fetchPsDocumentsByCidByPsDocumentTypeIds( $intCid, $objAdminDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						subq.*,
						cdt.name AS contract_document_type_name,
						cd.contract_document_type_id
					FROM
						contract_documents cd
						JOIN contracts c ON ( c.id = cd.contract_id )
						LEFT JOIN contract_properties cp ON ( cd.cid = cp.cid AND cp.contract_id = cd.contract_id )
						JOIN contract_document_types cdt ON ( cd.contract_document_type_id = cdt.id )
						JOIN
							(
							SELECT
								pd.id,
								pd.title,
								pd.file_name,
								pd.ps_document_type_id,
								pdt.name AS document_type,
								pd.updated_on,
								e.preferred_name AS updated_by
							FROM
								ps_documents pd
								JOIN ps_document_types pdt ON ( pd.ps_document_type_id = pdt.id )
								JOIN users AS u ON ( u.id = pd.updated_by )
								JOIN employees AS e ON ( e.id = u.employee_id )
							WHERE
								pd.cid = ' . ( int ) $intCid . '
								AND pd.ps_document_type_id = ' . CPsDocumentType::DOCUMENT_TYPE_SUPPORT_DOCUMENT . '
								AND pd.associated_ps_document_id IS NULL
								AND pd.deleted_on IS NULL
								) AS subq ON ( subq.id = cd.ps_document_id )
					WHERE
						cd.contract_document_type_id IN ( ' . implode( ',', CContractDocumentType::$c_arrintClientSummaryDocumentTypes ) . ' )
						AND cp.renewal_contract_property_id IS NULL
						AND cp.is_last_contract_record = TRUE
						AND cp.termination_date IS NULL
						AND c.contract_status_type_id NOT IN ( ' . implode( ',', CContractStatusType::$c_arrintDeactivatedContractStatusTypeIds ) . ' )
					UNION
						SELECT
							pd.id,
							pd.title,
							pd.file_name,
							pd.ps_document_type_id,
							pdt.name AS document_type,
							pd.updated_on,
							e.preferred_name AS updated_by,
							NULL AS name,
							NULL AS contract_document_type_id
						FROM
							ps_documents pd
							JOIN ps_document_types pdt ON ( pd.ps_document_type_id = pdt.id )
							JOIN users AS u ON ( u.id = pd.updated_by )
							JOIN employees AS e ON ( e.id = u.employee_id )
						WHERE
							pd.cid = ' . ( int ) $intCid . '
							AND pd.ps_document_type_id IN ( ' . implode( ',', CPsDocumentType::$c_arrintClientSummaryRequiredDocument ) . ' )
							AND pd.associated_ps_document_id IS NULL
							AND pd.deleted_on IS NULL
						ORDER BY
							updated_on DESC';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentsByEmployeeIdByPsDocumentCategoryId( $arrintEmployeeIds, $arrintPsDocumentCategoryIds, $objAdminDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) || false == valArr( $arrintPsDocumentCategoryIds ) ) return NULL;

		$strSql = 'SELECT
						pd.*
					FROM
						ps_documents pd
					WHERE
						pd.employee_id IN (' . implode( ',', $arrintEmployeeIds ) . ' )
						AND pd.ps_document_category_id IN (' . implode( ',', $arrintPsDocumentCategoryIds ) . ' )';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentIdsByEmployeeApplicationIds( $arrintEmployeeApplicationIds, $objAdminDatabase ) {
		if( false == valArr( $arrintEmployeeApplicationIds ) ) return false;

		$strSql = '	SELECT
						pd.*
					FROM
						ps_documents pd
						LEFT JOIN employee_applications ea ON( ea.ps_document_id = pd.id )
					 WHERE ea.id IN( ' . implode( ',', $arrintEmployeeApplicationIds ) . ' )
						or pd.employee_application_id IN( ' . implode( ',', $arrintEmployeeApplicationIds ) . ' )';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

	public static function fetchPaginatedPsDocumentsByEmployeeId( $intEmployeeId, $intPageNo, $intPageSize, $objAdminDatabase, $arrintPsDocumentTypeIds = NULL ) {

		if( false == valId( $intEmployeeId ) ) return NULL;
		$strWhere = ( true == valArr( $arrintPsDocumentTypeIds ) ) ? ' AND pd.ps_document_type_id NOT IN ( ' . sqlIntImplode( $arrintPsDocumentTypeIds ) . ' )' : '';

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
						pd.id,
						pd.title,
						pd.file_name,
						pd.description,
						pd.updated_on as updated_date,
						dt.name as document_type
					FROM
						ps_documents pd
						LEFT JOIN ps_document_types dt ON( dt.id = pd.ps_document_type_id )
					WHERE
						pd.employee_id = ' . ( int ) $intEmployeeId . $strWhere . '
					ORDER BY
						updated_date DESC
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchPsDocumentDetailsByContractIdByContractDocumentTypeId( $intContractId, $intContractDocumentTypeId, $objAdminDatabase ) {

		if( false == valId( $intContractId ) ) {
			return false;
		}

		$strSql = 'SELECT
					pd.file_name
				FROM
					ps_documents pd
					JOIN contract_documents cd ON pd.id = cd.ps_document_id
				WHERE
					cd.contract_document_type_id = ' . ( int ) $intContractDocumentTypeId . '
					AND cd.deleted_by IS NULL
					AND cd.contract_id = ' . ( int ) $intContractId;

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchPsDocumentByEmployeeIdsByPsDocumentTypeId( $arrintEmployeeIds, $intPsDocumentTypeId, $objAdminDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return false;
		}

		if( false == valId( $intPsDocumentTypeId ) ) {
			return false;
		}

		$strSql = '	SELECT
						*
					FROM
						ps_documents
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND ps_document_type_id = ' . ( int ) $intPsDocumentTypeId . '
						AND deleted_by IS NULL';

		return self::fetchPsDocuments( $strSql, $objAdminDatabase );
	}

}
?>