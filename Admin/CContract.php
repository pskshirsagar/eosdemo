<?php

use Psi\Eos\Admin\CContractProperties;
use Psi\Eos\Admin\CContractAgreementTemplates;
use Psi\Eos\Admin\CContractWatchlists;
use Psi\Eos\Admin\CContractLikelihoodActionSettings;
use Psi\Eos\Admin\CContractLikelihoodSettings;
use Psi\Eos\Admin\CContractPropertyPricings;
use Psi\Eos\Admin\CPsProducts;
use Psi\Eos\Admin\CPsProductRelationships;
use Psi\Eos\Admin\CContractDocuments;
use Psi\Eos\Admin\CContractStatusTypes;
use Psi\Eos\Admin\CCommissionStructures;
use Psi\Eos\Admin\CCommissionRateAssociations;
use Psi\Eos\Admin\CContractDetails;
use Psi\Eos\Admin\CContracts;
use Psi\Eos\Admin\CPsLeads;
use Psi\Eos\Admin\CEntities;
use Psi\Eos\Admin\CActions;
use Psi\Eos\Admin\CEmployees;
use Psi\Eos\Admin\CActionTypes;
use Psi\Eos\Admin\CPsLeadDetails;
use Psi\Eos\Admin\CClients;
use Psi\Eos\Admin\CAccounts;
use Psi\Eos\Connect\CDatabases;
use Psi\Libraries\ExternalFileUpload\CFileUpload;
use Psi\Eos\Admin\CPsProductOptions;
use Psi\Eos\Admin\CContractProductOptions;
use Psi\Eos\Admin\CContractProductStakeholders;
use Psi\Eos\Admin\CUsers;
use Psi\Eos\Admin\CContractProducts;
use Psi\Eos\Admin\CCommissionBuckets;
use Psi\Eos\Admin\CPsDocuments;

class CContract extends CBaseContract {

	protected $m_objPsLead;
	protected $m_objPsDocument;
	protected $m_objContractDetail;
	protected $m_objContractProduct;

	protected $m_fltTotalNewAcv;
	protected $m_fltRecurringAmount;
	protected $m_fltTotalSetUpAmount;
	protected $m_fltPercentageRenewed;
	protected $m_fltTransactionalAmount;
	protected $m_fltImplementationAmount;
	protected $m_fltTotalPilotContingent;
	protected $m_fltTotalMonthlyRecurringAmount;
	protected $m_fltTotalMonthlyTransactionalAmount;
	protected $m_fltAcvAmount;
	protected $m_fltTransferPropertyAcvAmount;

	protected $m_intAppId;
	protected $m_intIsLocked;
	protected $m_intIsCommit;
	protected $m_intUnitCount;
	protected $m_intAutoRenews;
	protected $m_intPropertyCount;
	protected $m_intProductsCount;
	protected $m_intHasCustomTerms;
	protected $m_intPriorityNumber;
	protected $m_intSalesEngineerId;
	protected $m_intEntrataCoreUnits;
	protected $m_intSupportEmployeeId;
	protected $m_intTrainerEmployeeId;
	protected $m_intRenewalContractId;
	protected $m_intContractTermMonths;
	protected $m_intEstimatedUnitCount;
	protected $m_intActivePropertyCount;
	protected $m_intActivePropertyAmount;
	protected $m_intTerminationApprovedBy;
	protected $m_intResponsibleEmployeeId;
	protected $m_intEstimatedPropertyCount;
	protected $m_intContractPropertyCount;
	protected $m_intNumberOfUnits;
	protected $m_intTerminatedPropertyCount;
	protected $m_intTerminatedPropertyAmount;
	protected $m_intImplementationEmployeeId;
	protected $m_intImplementationWeightScore;
	protected $m_intContractTerminationRequestId;
	protected $m_intPsDocumentId;
	protected $m_intContractDocumentId;
	protected $m_intContractTerminationReasonId;
	protected $m_intCompanyUserId;
	protected $m_intCommittedByUserId;
	protected $m_intClientRiskStatus;
	protected $m_intKeyClientAcvRank;
	protected $m_intOnHoldReasonId;

	protected $m_strCity;
	protected $m_strNameLast;
	protected $m_strNameFirst;
	protected $m_strStateCode;
	protected $m_strCloseDate;
	protected $m_strPilotResult;
	protected $m_strCompanyName;
	protected $m_strPhoneNumber1;
	protected $m_strEmailAddress;
	protected $m_strPsProductNames;
	protected $m_strContractStatus;
	protected $m_strTerminationDate;
	protected $m_strContractTypeName;
	protected $m_strRenewalContractIds;
	protected $m_strProductDescription;
	protected $m_strTerminationPostedOn;
	protected $m_strTerminationApprovedOn;
	protected $m_strCompanyUserNameFull;
	protected $m_strCompanyContact;
	protected $m_strDeactivationDate;
	protected $m_strContractDocumentFileName;
	protected $m_strCompanyStatusType;
	protected $m_strPsLeadPropertyTypes;
	protected $m_strContractRejectType;
	protected $m_strPhoneNumberExtension;
	protected $m_strSalesEmployeeName;

	protected $m_boolIsPilot;
	protected $m_boolIsOpportunity;
	protected $m_boolHasContractWatchlist;
	protected $m_boolIsTransferredProperties;
	protected $m_boolIsNonTransferredProperties;

	protected $m_arrobjActions;
	protected $m_arrobjEmployees;
	protected $m_arrobjContractProducts;
	protected $m_arrobjContractProperties;
	protected $m_arrobjContractProductOptions;

	const MAX_DEFAULT_LIKELIHOOD = 100;
	const BIG_SALE_EMAIL_ACV_LIMIT = 50000;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['acv_amount'] ) ) 							$this->setAcvAmount( $arrmixValues['acv_amount'] );
		if( true == isset( $arrmixValues['transfer_property_acv_amount'] ) ) 		$this->setTransferPropertyAcvAmount( $arrmixValues['transfer_property_acv_amount'] );
		if( true == isset( $arrmixValues['total_set_up_amount'] ) ) 				$this->setTotalSetUpAmount( $arrmixValues['total_set_up_amount'] );
		if( true == isset( $arrmixValues['total_monthly_recurring_amount'] ) ) 		$this->setTotalMonthlyRecurringAmount( $arrmixValues['total_monthly_recurring_amount'] );
		if( true == isset( $arrmixValues['total_monthly_transactional_amount'] ) ) 	$this->setTotalMonthlyTransactionalAmount( $arrmixValues['total_monthly_transactional_amount'] );
		if( true == isset( $arrmixValues['property_count'] ) ) 						$this->setPropertyCount( $arrmixValues['property_count'] );
		if( true == isset( $arrmixValues['unit_count'] ) ) 							$this->setUnitCount( $arrmixValues['unit_count'] );
		if( true == isset( $arrmixValues['active_properties_count'] ) ) 			$this->setActivePropertyCount( $arrmixValues['active_properties_count'] );
		if( true == isset( $arrmixValues['active_properties_amount'] ) ) 			$this->setActivePropertyAmount( $arrmixValues['active_properties_amount'] );
		if( true == isset( $arrmixValues['terminated_properties_count'] ) ) 		$this->setTerminatedPropertyCount( $arrmixValues['terminated_properties_count'] );
		if( true == isset( $arrmixValues['terminated_properties_amount'] ) ) 		$this->setTerminatedPropertyAmount( $arrmixValues['terminated_properties_amount'] );
		if( true == isset( $arrmixValues['contract_type_name'] ) ) 					$this->setContractTypeName( $arrmixValues['contract_type_name'] );
		if( true == isset( $arrmixValues['products_count'] ) ) 						$this->setProductsCount( $arrmixValues['products_count'] );
		if( true == isset( $arrmixValues['termination_posted_on'] ) ) 				$this->setTerminationPostedOn( $arrmixValues['termination_posted_on'] );

		if( true == isset( $arrmixValues['termination_approved_by'] ) ) 			$this->setTerminationApprovedBy( $arrmixValues['termination_approved_by'] );
		if( true == isset( $arrmixValues['termination_approved_on'] ) ) 			$this->setTerminationApprovedOn( $arrmixValues['termination_approved_on'] );
		if( true == isset( $arrmixValues['has_custom_terms'] ) ) 					$this->setHasCustomTerms( $arrmixValues['has_custom_terms'] );
		if( true == isset( $arrmixValues['auto_renews'] ) ) 						$this->setAutoRenews( $arrmixValues['auto_renews'] );
		if( true == isset( $arrmixValues['contract_term_months'] ) ) 				$this->setContractTermMonths( $arrmixValues['contract_term_months'] );

		if( true == isset( $arrmixValues['support_employee_id'] ) ) 				$this->setSupportEmployeeId( $arrmixValues['support_employee_id'] );
		if( true == isset( $arrmixValues['responsible_employee_id'] ) ) 			$this->setResponsibleEmployeeId( $arrmixValues['responsible_employee_id'] );
		if( true == isset( $arrmixValues['is_pilot'] ) ) 							$this->setIsPilot( $arrmixValues['is_pilot'] );
		if( true == isset( $arrmixValues['pilot_result'] ) ) 						$this->setPilotResult( $arrmixValues['pilot_result'] );
		if( true == isset( $arrmixValues['is_opportunity'] ) ) 						$this->setIsOpportunity( $arrmixValues['is_opportunity'] );

		// For Opportunity listing
		if( true == isset( $arrmixValues['has_contract_watchlist'] ) ) 				$this->setHasContractWatchlist( $arrmixValues['has_contract_watchlist'] );
		if( true == isset( $arrmixValues['implementation_amount'] ) )				$this->m_fltImplementationAmount = trim( $arrmixValues['implementation_amount'] );
		if( true == isset( $arrmixValues['recurring_amount'] ) )					$this->m_fltRecurringAmount = trim( $arrmixValues['recurring_amount'] );
		if( true == isset( $arrmixValues['transactional_amount'] ) )				$this->m_fltTransactionalAmount = trim( $arrmixValues['transactional_amount'] );
		if( true == isset( $arrmixValues['estimated_property_count'] ) )			$this->m_intEstimatedPropertyCount = trim( $arrmixValues['estimated_property_count'] );
		if( true == isset( $arrmixValues['number_of_units'] ) )						$this->m_intNumberOfUnits = trim( $arrmixValues['number_of_units'] );
		if( true == isset( $arrmixValues['contract_property_count'] ) )				$this->m_intContractPropertyCount = trim( $arrmixValues['contract_property_count'] );
		if( true == isset( $arrmixValues['estimated_unit_count'] ) )				$this->m_intEstimatedUnitCount = trim( $arrmixValues['estimated_unit_count'] );
		if( true == isset( $arrmixValues['company_name'] ) )						$this->m_strCompanyName = trim( stripcslashes( $arrmixValues['company_name'] ) );
		if( true == isset( $arrmixValues['ps_product_names'] ) )					$this->m_strPsProductNames = trim( stripcslashes( $arrmixValues['ps_product_names'] ) );
		if( true == isset( $arrmixValues['implementation_employee_id'] ) ) 			$this->setImplementationEmployeeId( $arrmixValues['implementation_employee_id'] );
		if( true == isset( $arrmixValues['trainer_employee_id'] ) ) 				$this->setTrainerEmployeeId( $arrmixValues['trainer_employee_id'] );
		if( true == isset( $arrmixValues['sales_engineer_id'] ) ) 					$this->setSalesEngineerId( $arrmixValues['sales_engineer_id'] );
		if( true == isset( $arrmixValues['renewal_contract_id'] ) ) 				$this->setRenewalContractId( $arrmixValues['renewal_contract_id'] );

		if( true == isset( $arrmixValues['contract_status'] ) ) 					$this->setContractStatus( $arrmixValues['contract_status'] );
		if( true == isset( $arrmixValues['priority_number'] ) ) 					$this->setPriorityNumber( $arrmixValues['priority_number'] );
		if( true == isset( $arrmixValues['name_first'] ) ) 							$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) 							$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['phone_number1'] ) ) 						$this->setPhoneNumber1( $arrmixValues['phone_number1'] );
		if( true == isset( $arrmixValues['phone_number_extension'] ) ) 				$this->setPhoneNumberExtension( $arrmixValues['phone_number_extension'] );
		if( true == isset( $arrmixValues['email_address'] ) ) 						$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['city'] ) ) 								$this->setCity( $arrmixValues['city'] );
		if( true == isset( $arrmixValues['state_code'] ) ) 							$this->setStateCode( $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['product_description'] ) ) 				$this->setProductDescription( $arrmixValues['product_description'] );
		if( true == isset( $arrmixValues['is_locked'] ) ) 							$this->setIsLocked( $arrmixValues['is_locked'] );
		if( true == isset( $arrmixValues['is_commit'] ) ) 							$this->setIsCommit( $arrmixValues['is_commit'] );
		if( true == isset( $arrmixValues['total_new_acv'] ) ) 						$this->setTotalNewAcv( $arrmixValues['total_new_acv'] );
		if( true == isset( $arrmixValues['total_pilot_contingent'] ) ) 				$this->setTotalPilotContingent( $arrmixValues['total_pilot_contingent'] );
		if( true == isset( $arrmixValues['block_dialer_calls'] ) ) 					$this->setBlockDialerCalls( $arrmixValues['block_dialer_calls'] );
		if( true == isset( $arrmixValues['implementation_weight_score'] ) )			$this->setImplementationWeightScore( $arrmixValues['implementation_weight_score'] );
		if( true == isset( $arrmixValues['entrata_core_units'] ) )					$this->setEntrataCoreUnits( $arrmixValues['entrata_core_units'] );
		if( true == isset( $arrmixValues['close_date'] ) )							$this->setCloseDate( $arrmixValues['close_date'] );
		if( true == isset( $arrmixValues['termination_date'] ) )					$this->setTerminationDate( $arrmixValues['termination_date'] );
		if( true == isset( $arrmixValues['deactivation_date'] ) )					$this->setDeactivationDate( $arrmixValues['deactivation_date'] );
		if( true == isset( $arrmixValues['contract_termination_request_id'] ) )		$this->setContractTerminationRequestId( $arrmixValues['contract_termination_request_id'] );
		if( true == isset( $arrmixValues['percentage_renewed'] ) ) 					$this->setPercentageRenewed( $arrmixValues['percentage_renewed'] );
		if( true == isset( $arrmixValues['renewal_contract_ids'] ) ) 				$this->setRenewalContractIds( $arrmixValues['renewal_contract_ids'] );
		if( true == isset( $arrmixValues['committed_by_user_id'] ) ) 				$this->setCommittedByUserId( $arrmixValues['committed_by_user_id'] );

		if( true == isset( $arrmixValues['company_status_type'] ) )					$this->setCompanyStatusType( $arrmixValues['company_status_type'] );
		if( true == isset( $arrmixValues['ps_lead_property_type'] ) ) 				$this->setPsLeadPropertyTypes( $arrmixValues['ps_lead_property_type'] );
		if( true == isset( $arrmixValues['contract_reject_type'] ) ) 				$this->setContractRejectType( $arrmixValues['contract_reject_type'] );
		if( true == isset( $arrmixValues['is_transferred_properties'] ) ) 			$this->setIsNonTransferredProperties( $arrmixValues['is_transferred_properties'] );
		if( true == isset( $arrmixValues['is_non_transferred_properties'] ) ) 		$this->setIsNonTransferredProperties( $arrmixValues['is_non_transferred_properties'] );

		if( true == isset( $arrmixValues['sales_employee_name'] ) ) 				$this->setSalesEmployeeName( $arrmixValues['sales_employee_name'] );
		if( true == isset( $arrmixValues['client_risk_status'] ) )					$this->setClientRiskStatus( $arrmixValues['client_risk_status'] );
		if( true == isset( $arrmixValues['key_client_acv_rank'] ) )					$this->setKeyClientAcvRank( $arrmixValues['key_client_acv_rank'] );
		if( true == isset( $arrmixValues['on_hold_reason_id'] ) )					$this->setOnHoldReasonId( $arrmixValues['on_hold_reason_id'] );
	}

	public function setContractStatus( $strContractStatus ) {
		$this->m_strContractStatus = $strContractStatus;
	}

	public function setPriorityNumber( $strPriorityNumber ) {
		$this->m_intPriorityNumber = $strPriorityNumber;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setPilotResult( $strPilotResult ) {
		$this->m_strPilotResult = $strPilotResult;
	}

	public function setPhoneNumber1( $strPhoneNumber1 ) {
		$this->m_strPhoneNumber1 = $strPhoneNumber1;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = $strCity;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setProductDescription( $strProductDescription ) {
		$this->m_strProductDescription = $strProductDescription;
	}

	public function setIsLocked( $intIsLocked ) {
		$this->m_intIsLocked = $intIsLocked;
	}

	public function setIsCommit( $intIsCommit ) {
		$this->m_intIsCommit = $intIsCommit;
	}

	public function setTotalNewAcv( $fltTotalNewAcv ) {
		$this->m_fltTotalNewAcv = $fltTotalNewAcv;
	}

	public function setTotalPilotContingent( $fltTotalPilotContingent ) {
		$this->m_fltTotalPilotContingent = $fltTotalPilotContingent;
	}

	public function setTotalSetUpAmount( $fltTotalSetUpAmount ) {
		$this->m_fltTotalSetUpAmount = $fltTotalSetUpAmount;
	}

	public function setPercentageRenewed( $fltPercentageRenewed ) {
		$this->m_fltPercentageRenewed = $fltPercentageRenewed;
	}

	public function setTotalMonthlyRecurringAmount( $fltTotalMonthlyRecurringAmount ) {
		$this->m_fltTotalMonthlyRecurringAmount = $fltTotalMonthlyRecurringAmount;
	}

	public function setTotalMonthlyTransactionalAmount( $fltTotalMonthlyTransactionalAmount ) {
		$this->m_fltTotalMonthlyTransactionalAmount = $fltTotalMonthlyTransactionalAmount;
	}

	public function setAcvAmount( $fltAcvAmount ) {
		$this->m_fltAcvAmount = $fltAcvAmount;
	}

	public function setTransferPropertyAcvAmount( $fltTransferPropertyAcvAmount ) {
		$this->m_fltTransferPropertyAcvAmount = $fltTransferPropertyAcvAmount;
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function setUnitCount( $intUnitCount ) {
		$this->m_intUnitCount = $intUnitCount;
	}

	public function setContractProperties( $arrobjContractProperties ) {
		$this->m_arrobjContractProperties = $arrobjContractProperties;
	}

	public function setActivePropertyCount( $intActivePropertyCount ) {
		$this->m_intActivePropertyCount = $intActivePropertyCount;
	}

	public function setTerminatedPropertyCount( $intTerminatedPropertyCount ) {
		$this->m_intTerminatedPropertyCount = $intTerminatedPropertyCount;
	}

	public function setActivePropertyAmount( $intActivePropertyAmount ) {
		$this->m_intActivePropertyAmount = $intActivePropertyAmount;
	}

	public function setTerminatedPropertyAmount( $intTerminatedPropertyAmount ) {
		$this->m_intTerminatedPropertyAmount = $intTerminatedPropertyAmount;
	}

	public function setPsLead( $objPsLead ) {
		$this->m_objPsLead = $objPsLead;
	}

	public function setContractTypeName( $strContractTypeName ) {
		$this->m_strContractTypeName = $strContractTypeName;
	}

	public function setRenewalContractIds( $strRenewalContractIds ) {
		$this->m_strRenewalContractIds = $strRenewalContractIds;
	}

	public function setCommittedByUserId( $intCommittedByUserId ) {
		$this->m_intCommittedByUserId = $intCommittedByUserId;
	}

	public function setProductsCount( $intProductsCount ) {
		$this->m_intProductsCount = $intProductsCount;
	}

	public function setTerminationApprovedBy( $intTerminationApprovedBy ) {
		$this->m_intTerminationApprovedBy = $intTerminationApprovedBy;
	}

	public function setTerminationApprovedOn( $strTerminationApprovedOn ) {
		$this->m_strTerminationApprovedOn = $strTerminationApprovedOn;
	}

	public function setHasContractWatchlist( $boolHasContractWatchlist ) {
		$this->m_boolHasContractWatchlist = $boolHasContractWatchlist;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = CStrings::strTrimDef( $strCompanyName, 100, NULL, true );
	}

	public function setSupportEmployeeId( $intSupportEmployeeId ) {
		$this->m_intSupportEmployeeId = $intSupportEmployeeId;
	}

	public function setResponsibleEmployeeId( $intResponsibleEmployeeId ) {
		$this->m_intResponsibleEmployeeId = $intResponsibleEmployeeId;
	}

	public function setIsPilot( $boolIsPilot ) {
		$this->m_boolIsPilot = $boolIsPilot;
	}

	public function setIsOpportunity( $boolIsOpportunity ) {
		$this->m_boolIsOpportunity = $boolIsOpportunity;
	}

	public function setTerminationPostedOn( $strTerminationPostedOn ) {
		$this->m_strTerminationPostedOn = $strTerminationPostedOn;
	}

	public function setCloseDate( $strCloseDate ) {
		$this->m_strCloseDate = CStrings::strTrimDef( $strCloseDate, -1, NULL, true );
	}

	public function setTerminationDate( $strTerminationDate ) {
		$this->m_strTerminationDate = CStrings::strTrimDef( $strTerminationDate, -1, NULL, true );
	}

	public function setContractTerminationRequestId( $intContractTerminationRequestId ) {
		$this->m_intContractTerminationRequestId = $intContractTerminationRequestId;
	}

	public function setContractTerminationReasonId( $intContractTerminationReasonId ) {
		$this->m_intContractTerminationReasonId = $intContractTerminationReasonId;
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = $intCompanyUserId;
	}

	public function setCompanyUserNameFull( $strCompanyUserNameFull ) {
		$this->m_strCompanyUserNameFull = $strCompanyUserNameFull;
	}

	public function setCompanyContact( $strCompanyContact ) {
		$this->m_strCompanyContact = $strCompanyContact;
	}

	public function setDeactivationDate( $strDeactivationDate ) {
		$this->m_strDeactivationDate = $strDeactivationDate;
	}

	public function setPhoneNumberExtension( $strPhoneNumberExtension ) {
		$this->m_strPhoneNumberExtension = $strPhoneNumberExtension;
	}

	// oppotunity listing

	public function setImplementationAmount( $fltImplementationAmount ) {
		$this->m_fltImplementationAmount = CStrings::strToFloatDef( $fltImplementationAmount, NULL, false, 2 );
	}

	public function setTransactionalAmount( $fltTransactionalAmount ) {
		$this->m_fltTransactionalAmount = CStrings::strToFloatDef( $fltTransactionalAmount, NULL, false, 2 );
	}

	public function setRecurringAmount( $fltRecurringAmount ) {
		$this->m_fltRecurringAmount = CStrings::strToFloatDef( $fltRecurringAmount, NULL, false, 2 );
	}

	public function setEstimatedPropertyCount( $intEstimatedPropertyCount ) {
		$this->m_intEstimatedPropertyCount = CStrings::strToIntDef( $intEstimatedPropertyCount );
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->m_intNumberOfUnits = CStrings::strToIntDef( $intNumberOfUnits );
	}

	public function setContractPropertyCount( $intContractPropertyCount ) {
		$this->m_intContractPropertyCount = CStrings::strToIntDef( $intContractPropertyCount );
	}

	public function setEstimatedUnitCount( $intEstimatedUnitCount ) {
		$this->m_intEstimatedUnitCount = CStrings::strToIntDef( $intEstimatedUnitCount );
	}

	public function setEntrataCoreUnits( $intEntrataCoreUnits ) {
		$this->m_intEntrataCoreUnits = CStrings::strToIntDef( $intEntrataCoreUnits );
	}

	public function setHasCustomTerms( $intHasCustomTerms ) {
		$this->m_intHasCustomTerms = $intHasCustomTerms;
	}

	public function setAutoRenews( $intAutoRenews ) {
		$this->m_intAutoRenews = $intAutoRenews;
	}

	public function setContractTermMonths( $intContractTermMonths ) {
		$this->m_intContractTermMonths = $intContractTermMonths;
	}

	public function setActions( $arrobjActions ) {
		$this->m_arrobjActions = $arrobjActions;
	}

	public function setContractDetail( $objContractDetail ) {
		$this->m_objContractDetail = $objContractDetail;
	}

	public function setContractProduct( $objContractProduct ) {
		$this->m_objContractProduct = $objContractProduct;
	}

	public function setAppId( $intAppId ) {
		$this->m_intAppId = $intAppId;
	}

	public function setPsProductNames( $strPsProductNames ) {
		$this->m_strPsProductNames = $strPsProductNames;
	}

	public function setImplementationEmployeeId( $intImplementationEmployeeId ) {
		$this->m_intImplementationEmployeeId = $intImplementationEmployeeId;
	}

	public function setTrainerEmployeeId( $intTrainerEmployeeId ) {
		$this->m_intTrainerEmployeeId = $intTrainerEmployeeId;
	}

	public function setBlockDialerCalls( $boolBlockDialerCalls ) {
		$this->m_boolBlockDialerCalls = CStrings::strToIntDef( $boolBlockDialerCalls );
	}

	public function setImplementationWeightScore( $intImplementationWeightScore ) {
		$this->m_intImplementationWeightScore = CStrings::strToIntDef( $intImplementationWeightScore );
	}

	public function setSalesEngineerId( $intSalesEngineerId ) {
		$this->m_intSalesEngineerId = $intSalesEngineerId;
	}

	public function setRenewalContractId( $intRenewalContractId ) {
		$this->m_intRenewalContractId = $intRenewalContractId;
	}

	public function setPsDocumentId( $intPsDocumentId ) {
		$this->m_intPsDocumentId = $intPsDocumentId;
	}

	public function setContractDocumentId( $intContractDocumentId ) {
		$this->m_intContractDocumentId = $intContractDocumentId;
	}

	public function setContractDocumentFileName( $strContractDocumentFileName ) {
		$this->m_strContractDocumentFileName = $strContractDocumentFileName;
	}

	public function setCompanyStatusType( $strCompanyStatusType ) {
		$this->m_strCompanyStatusType = $strCompanyStatusType;
	}

	public function setPsLeadPropertyTypes( $strPsLeadPropertyTypes ) {
		$this->m_strPsLeadPropertyTypes = $strPsLeadPropertyTypes;
	}

	public function setContractRejectType( $strContractRejectType ) {
		$this->m_strContractRejectType = $strContractRejectType;
	}

	public function setIsTransferredProperties( $boolIsTransferredProperties ) {
		$this->m_boolIsTransferredProperties = $boolIsTransferredProperties;
	}

	public function setIsNonTransferredProperties( $boolIsNonTransferredProperties ) {
		$this->m_boolIsNonTransferredProperties = $boolIsNonTransferredProperties;
	}

	public function setSalesEmployeeName( $strSalesEmployeeName ) {
		$this->m_strSalesEmployeeName = $strSalesEmployeeName;
	}

	public function setClientRiskStatus( $intClientRiskStatus ) {
		$this->m_intClientRiskStatus = $intClientRiskStatus;
	}

	public function setKeyClientAcvRank( $intKeyClientAcvRank ) {
		$this->m_intKeyClientAcvRank = $intKeyClientAcvRank;
	}

	public function setOnHoldReasonId( $intOnHoldReasonId ) {
		$this->m_intOnHoldReasonId = $intOnHoldReasonId;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getPilotResult() {
		return $this->m_strPilotResult;
	}

	public function getBlockDialerCalls() {
		return $this->m_boolBlockDialerCalls;
	}

	public function getTotalNewAcv() {
		return $this->m_fltTotalNewAcv;
	}

	public function getTotalPilotContingent() {
		return $this->m_fltTotalPilotContingent;
	}

	public function getContractStatus() {
		return $this->m_strContractStatus;
	}

	public function getPriorityNumber() {
		return $this->m_intPriorityNumber;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getPhoneNumber1() {
		return $this->m_strPhoneNumber1;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getProductDescription() {
		return $this->m_strProductDescription;
	}

	public function getIsLocked() {
		return $this->m_intIsLocked;
	}

	public function getIsCommit() {
		return $this->m_intIsCommit;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getTotalSetUpAmount() {
		return $this->m_fltTotalSetUpAmount;
	}

	public function getPercentageRenewed() {
		return $this->m_fltPercentageRenewed;
	}

	public function getTotalMonthlyRecurringAmount() {
		return $this->m_fltTotalMonthlyRecurringAmount;
	}

	public function getTotalMonthlyTransactionalAmount() {
		return $this->m_fltTotalMonthlyTransactionalAmount;
	}

	public function getAcvAmount() {
		return $this->m_fltAcvAmount;
	}

	public function getTransferPropertyAcvAmount() {
		return $this->m_fltTransferPropertyAcvAmount;
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function getUnitCount() {
		return $this->m_intUnitCount;
	}

	public function getContractProperties() {
		return $this->m_arrobjContractProperties;
	}

	public function getActivePropertyCount() {
		return $this->m_intActivePropertyCount;
	}

	public function getTerminatedPropertyCount() {
		return $this->m_intTerminatedPropertyCount;
	}

	public function getActivePropertyAmount() {
		return $this->m_intActivePropertyAmount;
	}

	public function getTerminatedPropertyAmount() {
		return $this->m_intTerminatedPropertyAmount;
	}

	public function getPsLead() {
		return $this->m_objPsLead;
	}

	public function getContractTypeName() {
		return $this->m_strContractTypeName;
	}

	public function getRenewalContractIds() {
		return $this->m_strRenewalContractIds;
	}

	public function getCommittedByUserId() {
		return $this->m_intCommittedByUserId;
	}

	public function getProductsCount() {
		return $this->m_intProductsCount;
	}

	public function getTerminationApprovedBy() {
		return $this->m_intTerminationApprovedBy;
	}

	public function getTerminationApprovedOn() {
		return $this->m_strTerminationApprovedOn;
	}


	public function getSupportEmployeeId() {
		return $this->m_intSupportEmployeeId;
	}

	public function getResponsibleEmployeeId() {
		return $this->m_intResponsibleEmployeeId;
	}

	public function getContractProducts() {
		return $this->m_arrobjContractProducts;
	}

	public function getPhoneNumberExtension() {
		return $this->m_strPhoneNumberExtension;
	}

	// oppotunity listing

	public function getRecurringAmount() {
		return $this->m_fltRecurringAmount;
	}

	public function getTransactionalAmount() {
		return $this->m_fltTransactionalAmount;
	}

	public function getImplementationAmount() {
		return $this->m_fltImplementationAmount;
	}

	public function getEstimatedPropertyCount() {
		return $this->m_intEstimatedPropertyCount;
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function getContractPropertyCount() {
		return $this->m_intContractPropertyCount;
	}

	public function getEstimatedUnitCount() {
		return $this->m_intEstimatedUnitCount;
	}

	public function getEntrataCoreUnits() {
		return $this->m_intEntrataCoreUnits;
	}

	public function getHasContractWatchlist() {
		return $this->m_boolHasContractWatchlist;
	}

	public function getCondensedOpenDatetime() {
		return date( 'n-j-y', strtotime( $this->getCreatedOn() ) );
	}

	public function getCondensedCloseDate() {
		if( false == is_null( $this->getCloseDate() ) ) {
			return date( 'n-j-y', strtotime( $this->getCloseDate() ) );
		}
		return NULL;
	}

	public function getCondensedAnticipatedCloseDate() {
		if( false == is_null( $this->getAnticipatedCloseDate() ) ) {
			return date( 'm/d/y', strtotime( $this->getAnticipatedCloseDate() ) );
		}
		return NULL;
	}

	public function getIsPilot() {
		return $this->m_boolIsPilot;
	}

	public function getIsOpportunity() {
		return $this->m_boolIsOpportunity;
	}

	public function getHasCustomTerms() {
		return $this->m_intHasCustomTerms;
	}

	public function getAutoRenews() {
		return $this->m_intAutoRenews;
	}

	public function getContractTermMonths() {
		return $this->m_intContractTermMonths;
	}

	public function getActions() {
		return $this->m_arrobjActions;
	}

	public function getContractDetail() {
		return $this->m_objContractDetail;
	}

	public function getContractProduct() {
		return $this->m_objContractProduct;
	}

	public function getAppId() {
		return $this->m_intAppId;
	}

	public function getPsProductNames() {
		return $this->m_strPsProductNames;
	}

	public function getImplementationEmployeeId() {
		return $this->m_intImplementationEmployeeId;
	}

	public function getTrainerEmployeeId() {
		return $this->m_intTrainerEmployeeId;
	}

	public function getImplementationWeightScore() {
		return $this->m_intImplementationWeightScore;
	}

	public function getSalesEngineerId() {
		return $this->m_intSalesEngineerId;
	}

	public function getRenewalContractId() {
		return $this->m_intRenewalContractId;
	}

	public function getPsDocumentId() {
		return $this->m_intPsDocumentId;
	}

	public function getContractDocumentId() {
		return $this->m_intContractDocumentId;
	}

	public function getContractTerminationReasonId() {
		return $this->m_intContractTerminationReasonId;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getCompanyUserNameFull() {
		return $this->m_strCompanyUserNameFull;
	}

	public function getCompanyContact() {
		return $this->m_strCompanyContact;
	}

	public function getDeactivationDate() {
		return $this->m_strDeactivationDate;
	}

	public function getContractDocumentFileName() {
		return $this->m_strContractDocumentFileName;
	}

	public function getCompanyStatusType() {
		return $this->m_strCompanyStatusType;
	}

	public function getPsLeadPropertyTypes() {
		return $this->m_strPsLeadPropertyTypes;
	}

	public function getContractRejectType() {
		return $this->m_strContractRejectType;
	}

	public function getIsTransferredProperties() {
		return $this->m_boolIsTransferredProperties;
	}

	public function getIsNonTransferredProperties() {
		return $this->m_boolIsNonTransferredProperties;
	}

	public function getSalesEmployeeName() {
		return $this->m_strSalesEmployeeName;
	}

	public function getClientRiskStatus() {
		return $this->m_intClientRiskStatus;
	}

	public function getKeyClientAcvRank() {
		return $this->m_intKeyClientAcvRank;
	}

	public function getOnHoldReasonId() {
		return $this->m_intOnHoldReasonId;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addContractProperty( $objContractProperty ) {
		$this->m_arrobjContractProperties[$objContractProperty->getId()] = $objContractProperty;
	}

	public function addContractProduct( $objContractProduct ) {
		$this->m_arrobjContractProducts[$objContractProduct->getId()] = $objContractProduct;
	}

	public function getOrFetchPsLead( $objDatabase ) {

		if( false == isset( $this->m_objPsLead ) ) {
			$this->m_objPsLead = CPsLeads::createService()->fetchPsLeadById( $this->getPsLeadId(), $objDatabase );
		}

		return $this->m_objPsLead;
	}

	public function getOrFetchContractProducts( $objDatabase ) {

		if( false == valArr( $this->m_arrobjContractProducts ) ) {
			$this->m_arrobjContractProducts = \Psi\Eos\Admin\CContractProducts::createService()->fetchContractProductsByContractId( $this->getId(), $objDatabase );
		}

		return $this->m_arrobjContractProducts;
	}

	public function getTerminationPostedOn() {
		return $this->m_strTerminationPostedOn;
	}

	public function getCloseDate() {
		return $this->m_strCloseDate;
	}

	public function getTerminationDate() {
		return $this->m_strTerminationDate;
	}

	public function getContractTerminationRequestId() {
		return $this->m_intContractTerminationRequestId;
	}

	// For opportunity

	public function addAction( $objAction ) {
		$this->m_arrobjActions[$objAction->getId()] = $objAction;
	}

	/**
	 * Other Functions
	 *
	 */

	public function generateContractAddendum( $strAddendumPropertyServicesTablePdfContent, $objDatabase, $objObjectStorageGateway, $objPsDocument = NULL ) {

		$objLocaleContainer = CLocaleContainer::createService();

		if( 'en_US' == $objLocaleContainer->getLocaleCode() ) {
			$strContractTerminationPdfName = '';
		} elseif( true == valStr( $objLocaleContainer->getLocaleCode() ) ) {
			$strContractTerminationPdfName = $objLocaleContainer->getLocaleCode() . '_';
		}

		$strFirstPageFileName = $this->getSystemTemplateTemporaryFullFilePath( CTemplateType::SELF_PROVISIONAL_CONTRACT_ADDENDUM_TEMPLATE_FIRST_PAGE, $objObjectStorageGateway, $objDatabase );
		$strSignaturePageFileName = $this->getSystemTemplateTemporaryFullFilePath( CTemplateType::SELF_PROVISIONAL_CONTRACT_ADDENDUM_TEMPLATE_LAST_PAGE, $objObjectStorageGateway, $objDatabase );

		$arrintPropertyCount = [];
		foreach( $this->getContractProducts() as $objContractProduct ) {
			foreach( $objContractProduct->getContractProperties() as $objContractProperty ) {
				$arrintPropertyCount[$objContractProperty->getPropertyId()] = $objContractProperty->getPropertyId();
			}
		}

		$strCompanyName = \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->preg_replace( '/[^a-zA-Z0-9_-]/s', '_', $this->getCompanyName() ) );
		$strFileName	= date( 'm-d-y' ) . '_' . $strCompanyName . '_add_' . \Psi\Libraries\UtilFunctions\count( $arrintPropertyCount ) . '_' . time() . '_unsigned.pdf';

		$objFont = Zend_Pdf_Font::fontWithName( Zend_Pdf_Font:: FONT_HELVETICA );

		$objContractAddendumZendPdf = Zend_Pdf::load( $strFirstPageFileName );
		$objContractAddendumZendPdfPage = $objContractAddendumZendPdf->pages[0];

		$strAutoContractName = \Psi\Eos\Admin\CClientNames::createService()->fetchClientNameByClientNameTypeIdByCid( CClientNameType::AUTO_CONTRACT_NAME, $this->getCid(), $objDatabase );

		$strCustomerName = ( true == valStr( $strAutoContractName ) ? $strAutoContractName : $this->getCompanyName() );

		$objContractAddendumZendPdfPage->setFont( $objFont, 10 )->drawText( $strCustomerName, 153, 528 );

		$objPropertyServicesTableZendPdf = Zend_Pdf::parse( $strAddendumPropertyServicesTablePdfContent );

		foreach( $objPropertyServicesTableZendPdf->pages as $objZendPdfPage ) {
			$objClonedZendPdfPage = clone $objZendPdfPage;
			$objNewZendPdfPage = new Zend_Pdf_Page( $objClonedZendPdfPage );

			$strImageFilePath = PATH_COMMON . 'images/app_store/' . CONFIG_ENTRATA_LOGO_PREFIX . 'psi_logo_original.png';
			$objNewZendPdfPage->drawImage( Zend_Pdf_Image::imageWithPath( $strImageFilePath ), 40, 545, 200, 600 );

			if( 'en_US' == $objLocaleContainer->getLocaleCode() ) {
				$objNewZendPdfPage->setFont( $objFont, 13 )->drawText( html_entity_decode( 'Agreement Amendment &#8212; Add Services', ENT_COMPAT, 'UTF-8' ), 515, 565, 'UTF-8' );
			} else {
				$objNewZendPdfPage->setFont( $objFont, 13 )->drawText( __( 'Agreement Amendment - Add Services' ), 515, 565 );
			}

			$objContractAddendumZendPdf->pages[] = $objNewZendPdfPage;
		}

		$objSignatureTemplateZendPdf = Zend_Pdf::load( $strSignaturePageFileName );
		foreach( $objSignatureTemplateZendPdf->pages as $objZendPdfPage ) {
			$objClonedZendPdfPage = clone $objZendPdfPage;
			$objNewZendPdfPage = new Zend_Pdf_Page( $objClonedZendPdfPage );
			$objNewZendPdfPage->setFont( $objFont, 9 ) ->drawText( \Psi\CStringService::singleton()->strtoupper( $strCustomerName ), 55, 313 );
			$objContractAddendumZendPdf->pages[] = $objNewZendPdfPage;
		}

		$strUploadPath = CObjectStorageUtils::getTemporaryPath( PATH_MOUNTS_FILES, '/' );

		if( !valstr( $strUploadPath ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_document_directoty', __( 'Failed to create directory for contract document.' ) ) );
			return false;
		}

		$intPageNo = 1;

		foreach( $objContractAddendumZendPdf->pages as $objZendPdfPage ) {
			$objZendPdfPage->setFont( $objFont, 9 ) ->drawText( __( 'Amnd Add, ' ) . __( '{%t, 0, DATE_NUMERIC_MONTH}-{%t, 0, DATE_NUMERIC_YEAR }', [ CInternationalDateTime::create( date( 'm/d/y' ) ) ] ) . __( ', Page {%d, 0} of {%d, 1}', [ intval( $intPageNo ), \Psi\Libraries\UtilFunctions\count( $objContractAddendumZendPdf->pages ) ] ), 634, 29 );
			$intPageNo++;
		}

		$objContractAddendumZendPdf->save( $strUploadPath . $strFileName );

		if( !valObj( $objPsDocument, 'CPsDocument' ) ) {
			$objPsDocument = new CPsDocument();
		}

		$objPsDocument->setCid( $this->getCid() );
		$objPsDocument->setTitle( 'Contract Document' );
		$objPsDocument->setPsDocumentTypeId( CPsDocumentType::DOCUMENT_TYPE_SUPPORT_DOCUMENT );		// Used DOCUMENT_TYPE_SUPPORT_DOCUMENT as per old contract system
		$objPsDocument->setFileName( $strFileName );
		$objPsDocument->setCid( $this->getCid() );
		$objPsDocument->setPsLeadId( $this->getPsLeadId() );

		$objContractDocument = $this->createContractDocument();
		$objContractDocument->setContractDocumentTypeId( CContractDocumentType::INITIAL_CONTRACT );

		$boolIsValid = false;
		switch( NULL ) {

			default:
				$boolIsValid = true;

				$boolIsValid &= $objPsDocument->validate( 'validate_contract_document', $objDatabase );
				$boolIsValid &= $objContractDocument->validate( VALIDATE_INSERT );

				if( false == $boolIsValid ) {
					$this->addErrorMsg( $objPsDocument->getErrorMsgs() );
					$this->addErrorMsg( $objContractDocument->getErrorMsgs() );
					break;
				}

				if( !$objPsDocument->uploadObject( $objObjectStorageGateway, $strUploadPath . $strFileName ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document', __( 'Failed to upload contract document.' ) ) );
					$objDatabase->rollback();
					break;
				}

				if( false == $objPsDocument->insert( SYSTEM_USER_ID, $objDatabase ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document', __( 'Failed to insert record into ps_documents.' ) ) );
					$objDatabase->rollback();
					break;
				}

				$objContractDocument->setPsDocumentId( $objPsDocument->getId() );

				if( false == $objContractDocument->insert( SYSTEM_USER_ID, $objDatabase ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pdf_document', __( 'Failed to insert records into contract_documents.' ) ) );
					$objDatabase->rollback();
					break;
				}

				$this->setPsDocumentId( $objContractDocument->getPsDocumentId() );
				$this->setContractDocumentId( $objContractDocument->getId() );
		}

		if( false == $boolIsValid ) {
			return false;
		}

		return true;
	}

	public function generateContractTerminationDocument( $objDatabase, $objObjectStorageGateway, $objTempHelper ) {

		$arrmixTerminatedProducts = [];
		$strCompanyContact = NULL;
		$boolIsResidentPay = false;

		if( false == valArr( $this->getContractProperties() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_property', __( 'Failed to load contract properties to generate document.' ) ) );
			return false;
		}

		foreach( $this->getContractProperties() as $objContractProperty ) {

			if( CPsProduct::RESIDENT_PAY == $objContractProperty->getPsProductId() ) {
				$boolIsResidentPay = true;
			}

			$arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['property_name']		= $objContractProperty->getPropertyName();
			if( false == isset( $arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['products'] ) ) {
				$arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['products']		= $objContractProperty->getProductName();
			} elseif( false == \Psi\CStringService::singleton()->stristr( $arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['products'], $objContractProperty->getProductName() ) ) {
				$arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['products']		= $arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['products'] . ', ' . $objContractProperty->getProductName();
			}
			if( false !== \Psi\CStringService::singleton()->strpos( $arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['products'], ', ' ) ) {
				$arrstrProducts = explode( ', ', $arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['products'] );
				\Psi\CStringService::singleton()->sort( $arrstrProducts );
				$arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['products'] = implode( ', ', $arrstrProducts );
			}
			$arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['company_name']		= $this->getCompanyName();
			$arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['number_of_units']		= $objContractProperty->getNumberOfUnits();
			$arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['company_contacts']	= $this->getCompanyContact();
			$arrmixTerminatedProducts[$objContractProperty->getPropertyId()]['effective_date']		= date( 'm/d/Y', strtotime( $this->getDeactivationDate() ) );
		}
		if( \Psi\Libraries\UtilFunctions\count( $arrmixTerminatedProducts ) > 1 ) {
			$strPropertyNames = \Psi\Libraries\UtilFunctions\count( $arrmixTerminatedProducts ) . '_properties';
		} else {
			if( true == valArr( $arrmixTerminatedProducts ) ) {
				$strPropertyNames = array_values( $arrmixTerminatedProducts )[0]['property_name'];
			}
		}

		$objEntity = CEntities::createService()->fetchEntityById( reset( $this->getContractProperties() )->getEntityId(), $objDatabase );

		// i18n changes
		$objLocaleContainer = CLocaleContainer::createService();

		if( 'en_US' == $objLocaleContainer->getLocaleCode() ) {
			$strContractTerminationPdfName = '';
		} elseif( true == valStr( $objLocaleContainer->getLocaleCode() ) ) {
			$strContractTerminationPdfName = $objLocaleContainer->getLocaleCode() . '_';
		}

		if( false != $boolIsResidentPay ) {
			$strFirstPageFileName = $this->getSystemTemplateTemporaryFullFilePath( CTemplateType::CONTRACT_TERMINATION_ADDENDUM_FIRST_PAGE_WITH_RPAY, $objObjectStorageGateway, $objDatabase );
		} else {
			$strFirstPageFileName = $this->getSystemTemplateTemporaryFullFilePath( CTemplateType::CONTRACT_TERMINATION_ADDENDUM_FIRST_PAGE_WITHOUT_RPAY, $objObjectStorageGateway, $objDatabase );
		}

		$strLastPageFileName = $this->getSystemTemplateTemporaryFullFilePath( CTemplateType::CONTRACT_TERMINATION_ADDENDUM_LAST_PAGE, $objObjectStorageGateway, $objDatabase );
		$strCompanyName			= \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->preg_replace( '/[^a-zA-Z0-9_-]/s', '_', $this->getCompanyName() ) );
		$strFileName			= date( 'm-d-Y', strtotime( $this->getDeactivationDate() ) ) . '_' . CFileUpload::cleanFilename( $strPropertyNames ) . '_term_' . $this->getId() . '_' . time() . '.pdf';

		if( !file_exists( $strFirstPageFileName ) || !file_exists( $strLastPageFileName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pdf_document', __( 'Contract termination template files not found.' ) ) );
			return false;
		}

		$objSmarty = new CPsSmarty( PATH_INTERFACES_ENTRATA, false );
		$objSmarty->assign( 'terminated_products', $arrmixTerminatedProducts );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME', CONFIG_COMPANY_NAME );
		$strTerminateProductTableHtmlContent = $objSmarty->fetch( PATH_INTERFACES_ENTRATA . 'app_store/apps/terminated_products_template.tpl' );

		$strTempDir = $objObjectStorageGateway->calcTemporaryPath( PATH_MOUNTS_DOCUMENTS , '/mpdf/');

		$objMpdf = new \Mpdf\Mpdf( [ 'margin_top' => 25, 'format' => 'letter', 'default_font' => 'helvetica', 'default_font_size' => 9, 'tempDir' => $strTempDir ] );
		$objMpdf->AddPage( 'L' );
		$objMpdf->WriteHTML( $strTerminateProductTableHtmlContent );

		// Adds a new page in Landscape orientation
		$strTerminateProductTablePdfContent = $objMpdf->Output( '', 'S' );

		$objFont = Zend_Pdf_Font::fontWithName( Zend_Pdf_Font:: FONT_HELVETICA );

		$objContractTerminateZendPdf = Zend_Pdf::load( $strFirstPageFileName );
		$objContractTerminateZendPdfPage = $objContractTerminateZendPdf->pages[0];

		if( true == is_null( $objEntity->getCompanyName() ) ) {
			$strCompanyName = $this->getCompanyName();
		} else {
			$strCompanyName = $objEntity->getCompanyName();
		}

		if( false != $boolIsResidentPay ) {
			$objContractTerminateZendPdfPage->setFont( $objFont, 9 )->drawText( __( '{%s, 0}', [ $this->getCompanyName() ] ), 150, 528 );
		} else {
			$objContractTerminateZendPdfPage->setFont( $objFont, 9 )->drawText( __( '{%s, 0}', [ $this->getCompanyName() ] ), 155, 525 );
		}

		$objTerminateProductTableZendPdf = Zend_Pdf::parse( $strTerminateProductTablePdfContent );
		foreach( $objTerminateProductTableZendPdf->pages as $objZendPdfPage ) {
			$objClonedZendPdfPage = clone $objZendPdfPage;
			$objNewZendPdfPage = new Zend_Pdf_Page( $objClonedZendPdfPage );

			$strImageFilePath = PATH_COMMON . 'images/app_store/' . CONFIG_ENTRATA_LOGO_PREFIX . 'psi_logo_original.png';
			$objNewZendPdfPage->drawImage( Zend_Pdf_Image::imageWithPath( $strImageFilePath ), 40, 545, 200, 600 );

			if( 'en_US' == $objLocaleContainer->getLocaleCode() ) {
				$objNewZendPdfPage->setFont( $objFont, 13 )->drawText( html_entity_decode( 'Agreement Amendment&#8212;Terminate Services', ENT_COMPAT, 'UTF-8' ), 475, 569, 'UTF-8' );
			} else {
				$objNewZendPdfPage->setFont( $objFont, 13 )->drawText( __( 'Agreement Amendment - Terminate Services' ), 475, 569 );
			}

			$objContractTerminateZendPdf->pages[] = $objNewZendPdfPage;
		}

		$objSignatureZendPdf			= Zend_Pdf::load( $strLastPageFileName );
		$objSignatureZendPdfPage		= $objSignatureZendPdf->pages[0];
		$objClonedSignatureZendPdfPage	= clone $objSignatureZendPdfPage;
		$objNewSignatureZendPdfPage		= new Zend_Pdf_Page( $objClonedSignatureZendPdfPage );

		$arrstrContractTerminationReasons = CContractTerminationReason::createService()->getContractTerminationReasons();
		$objNewSignatureZendPdfPage->setFont( $objFont, 10 )->drawText( __( '{%s, 0}', [ $arrstrContractTerminationReasons[$this->getContractTerminationReasonId()] ] ), 165, 510 );
		$objNewSignatureZendPdfPage->setFont( $objFont, 10 )->drawText( __( '{%s, 0}', [ $this->getCompanyUserNameFull() ] ), 165, 490 );
		$objNewSignatureZendPdfPage->setFont( $objFont, 10 )->drawText( __( '{%d, 0}', $this->getCompanyUserId() ), 165, 469 );
		$objNewSignatureZendPdfPage->setFont( $objFont, 10 )->drawText( date( 'F j, Y H:i:s' ), 165, 449 );
		$objNewSignatureZendPdfPage->setFont( $objFont, 10 )->drawText( __( '{%s, 0}', [ getRemoteIpAddress() ] ), 165, 429 );

		$objContractTerminateZendPdf->pages[] = $objNewSignatureZendPdfPage;

		$strUploadPath = $objObjectStorageGateway->calcTemporaryPath( PATH_MOUNTS_DOCUMENTS, $this->getPsLeadId() );

		if( false == is_dir( $strUploadPath ) && false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_document_directoty', __( 'Failed to create directory for contract document.' ) ) );
			return false;
		}

		$intPageNo = 1;

		foreach( $objContractTerminateZendPdf->pages as $objZendPdfPage ) {
			$objZendPdfPage->setFont( $objFont, 9 ) ->drawText( __( 'Amnd Add, {%t, 0, DATE_NUMERIC_MONTH}-{%t, 0, DATE_NUMERIC_YEAR }', [ CInternationalDateTime::create( date( 'm/d/y' ) ) ] ), 674, 29 );
			$objZendPdfPage->setFont( $objFont, 9 ) ->drawText( __( 'Page {%d, 0} of {%d, 1}', [ intval( $intPageNo ), \Psi\Libraries\UtilFunctions\count( $objContractTerminateZendPdf->pages ) ] ), 360, 29 );
			$intPageNo++;
		}

		try {
			$arrmixRequest						= [];
			$arrmixRequest['key'] = sprintf( '%d/%s/%d/%d/%d/%s', $this->getCid(), 'documents', date( 'Y' ), date( 'm' ), date( 'd' ), $strFileName ); // object key
			$arrmixRequest['data'] = $objContractTerminateZendPdf->render();
			$arrmixRequest['expires']			= ( new \DateTime() )->add( new \DateInterval( sprintf( 'P%dD', 15 ) ) );
			$objObjectStorageGatewayResponse	= $objTempHelper->putObject( $arrmixRequest );
			if( $objObjectStorageGatewayResponse->hasErrors() ) {
				trigger_error( __( 'Failed to put storage object.' ), E_USER_WARNING );
				return false;
			}
			return $strFileName;
		} catch ( \Exception $objException ) {
			trigger_error( __( 'Failed to save Object. Error:' . $objException->getMessage() ), E_USER_WARNING );
			return false;
		}

	}

	public function insertContractDocument( $objDatabase, $strFullPath, $objObjectStorageGateway ) {

		if( false == valStr( $this->getContractDocumentFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pdf_document', 'Invalid file name.' ) );
			return false;
		}

		$objPsDocument = new CPsDocument();
		$objPsDocument->setCid( $this->getCid() );
		$objPsDocument->setTitle( 'Contract Document' );
		$objPsDocument->setPsDocumentTypeId( CPsDocumentType::DOCUMENT_TYPE_SUPPORT_DOCUMENT );		// Used DOCUMENT_TYPE_SUPPORT_DOCUMENT as per old contract system
		$objPsDocument->setFileName( $this->getContractDocumentFileName() );
		$objPsDocument->setCid( $this->getCid() );
		$objPsDocument->setPsLeadId( $this->getPsLeadId() );

		$objContractDocument = $this->createContractDocument();
		$objContractDocument->setContractDocumentTypeId( CContractDocumentType::SIGNED_CONTRACT );

		$boolIsValid = true;
		switch( NULL ) {
			default:
				$boolIsValid &= $objPsDocument->validate( 'validate_contract_document', $objDatabase );
				$boolIsValid &= $objContractDocument->validate( VALIDATE_INSERT );

				if( false == $boolIsValid ) {
					$this->addErrorMsg( $objPsDocument->getErrorMsgs() );
					$this->addErrorMsg( $objContractDocument->getErrorMsgs() );
					break;
				}

				if( !$objPsDocument->uploadObject( $objObjectStorageGateway, $strFullPath ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( 'Failed to upload termination document.' ) );
					break;
				}

				if( false == $objPsDocument->insert( SYSTEM_USER_ID, $objDatabase ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document', 'Failed to insert record into ps_documents.' ) );
					break;
				}

				$objContractDocument->setPsDocumentId( $objPsDocument->getId() );

				if( false == $objContractDocument->insert( SYSTEM_USER_ID, $objDatabase ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pdf_document', 'Failed to insert record into contract_documents.' ) );
					break;
				}
		}

		return $boolIsValid;
	}

	public function createPsDocument( $arrstrFilePathinfo ) {

		$objPsDocument = new CPsDocument();
		$objPsDocument->setTitle( 'Contract Document' );
		$objPsDocument->setPsDocumentTypeId( CPsDocumentType::DOCUMENT_TYPE_SUPPORT_DOCUMENT );		// Used DOCUMENT_TYPE_SUPPORT_DOCUMENT as per old contract system
		$objPsDocument->setFileName( \Psi\CStringService::singleton()->strtolower( $arrstrFilePathinfo['basename'] ) );
		$objPsDocument->setFileExtensionId( $arrstrFilePathinfo['extension'] );
		$objPsDocument->setContractId( $this->getId() );
		$objPsDocument->setCid( $this->getCid() );
		$objPsDocument->setPsLeadId( $this->getPsLeadId() );

		return $objPsDocument;
	}

	public function createContractAgreementTemplate() {

		$objContractAgreementTemplate = new CContractAgreementTemplate();
		$objContractAgreementTemplate->setPsLeadId( $this->getCid() );
		$objContractAgreementTemplate->setContractId( $this->getId() );

		return $objContractAgreementTemplate;
	}

	public function createContractDocument() {

		$objContractDocument = new CContractDocument();
		$objContractDocument->setContractId( $this->getId() );
		$objContractDocument->setContractDocumentTypeId( CContractDocumentType::SIGNED_CONTRACT );
		$objContractDocument->setCid( $this->getCid() );

		return $objContractDocument;
	}

	public function createContractProperty( $intDefaultDatabaseUserTypeId = CDatabaseUserType::PS_DEVELOPER ) {

		$objContractProperty = new CContractProperty();
		$objContractProperty->setCid( $this->getCid() );
		$objContractProperty->setContractId( $this->getId() );
		$objContractProperty->setCurrencyCode( $this->getCurrencyCode() );

		// getting Implementation Contingent for contract
		$boolIsImplementationContingent = false;

		$objAdminDatabase = CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::ADMIN, $intDefaultDatabaseUserTypeId );

		if( valObj( $objAdminDatabase, 'CDatabase' ) ) {
			$arrobjExistingContractProperties = $this->fetchContractProperties( $objAdminDatabase );
			if( valArr( $arrobjExistingContractProperties ) ) {
				foreach( $arrobjExistingContractProperties as $objExistingContractProperty ) {
					if( $objExistingContractProperty->getIsImplementationContingent() ) {
						$boolIsImplementationContingent = true;
						break;
					}
				}
			}
		}

		if( true == $boolIsImplementationContingent ) {
			$objContractProperty->setIsImplementationContingent( $boolIsImplementationContingent );
			$objContractProperty->setBillingStartDate( date( 'Y-m-1', strtotime( '+90 days', strtotime( date( 'Y-m-d' ) ) ) ) );
		}

		return $objContractProperty;
	}

	public function createContractProduct( $intPsProductId = NULL ) {

		$objContractProduct = new CContractProduct();
		$objContractProduct->setCid( $this->getCid() );
		$objContractProduct->setContractId( $this->getId() );
		$objContractProduct->setCurrencyCode( $this->getCurrencyCode() );
		$objContractProduct->setItemDatetime( 'NOW()' );
		$objContractProduct->setImplementationHours( 0 );
		$objContractProduct->setUpdatedOn( 'NOW()' );

		if( false == is_null( $intPsProductId ) ) {
			$objContractProduct->setPsProductId( $intPsProductId );
		}

		return $objContractProduct;
	}

	public function createPackageContractProduct( $arrmixDetails = [] ) {

		$objContractProduct = $this->createContractProduct();
		$objContractProduct->setPackageProductId( isset( $arrmixDetails['package_product_id'] ) ? $arrmixDetails['package_product_id'] : NULL );
		$objContractProduct->setIsUniversal( isset( $arrmixDetails['is_universal'] ) ? $arrmixDetails['is_universal'] : 0 );
		$objContractProduct->setPsProductId( isset( $arrmixDetails['ps_product_id'] ) ? $arrmixDetails['ps_product_id'] : NULL );
		$objContractProduct->setBundlePsProductId( isset( $arrmixDetails['bundle_ps_product_id'] ) ? $arrmixDetails['bundle_ps_product_id'] : NULL );
		$objContractProduct->setEstimatedPropertyCount( isset( $arrmixDetails['property_count'] ) ? $arrmixDetails['property_count'] : NULL );
		$objContractProduct->setEstimatedUnitCount( isset( $arrmixDetails['unit_count'] ) ? $arrmixDetails['unit_count'] : NULL );

		return $objContractProduct;
	}

	public function createContractTerminationRequest() {

		$objContractTerminationRequest = new CContractTerminationRequest();
		$objContractTerminationRequest->setCid( $this->getCid() );
		$objContractTerminationRequest->setRequestDatetime( 'NOW()' );

		return $objContractTerminationRequest;
	}

	public function createContractPropertyFilter() {

		$objContractPropertyFilter = new CContractPropertyFilter();
		$objContractPropertyFilter->setContractId( $this->getId() );
		$objContractPropertyFilter->setCid( $this->getCid() );

		return $objContractPropertyFilter;
	}

	public function createContractWatchlist() {

		$objContractWatchlist = new CContractWatchlist();
		$objContractWatchlist->setPsLeadId( $this->m_intPsLeadId );
		$objContractWatchlist->setContractId( $this->m_intId );

		return $objContractWatchlist;
	}

	public function createAction() {

		$objAction = new CAction();
		$objAction->setPsLeadId( $this->getPsLeadId() );
		$objAction->setContractId( $this->getId() );
		$objAction->setActionDatetime( date( 'm/d/Y H:i:s' ) );
		$objAction->setActionTypeId( CActionType::CALL );

		return $objAction;
	}

	public function createContractDetail() {

		$objContractDetail = new CContractDetail();
		$objContractDetail->setCid( $this->getCid() );
		$objContractDetail->setContractId( $this->getId() );

		if( CContractType::RENEWAL != $this->getContractTypeId() && true == \valId( $this->getCid() ) ) {
			$objContractDetail->setImplementationWeightScore( CContractDetail::NEW_CONTRACT_WEIGHT_SCORE );
		} else {
			$objContractDetail->setImplementationWeightScore( 0 );
		}

		return $objContractDetail;
	}

	public function createContractEmployee() {

		$objContractEmployee = new CContractEmployee();
		$objContractEmployee->setCid( $this->getCid() );
		$objContractEmployee->setContractId( $this->getId() );

		return $objContractEmployee;
	}

	public function valPsLeadId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsLeadId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_lead_id', 'Lead is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContractDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getContractDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_datetime', __( 'Contract date is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valContractStartDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getContractStartDate() ) && true != CValidation::validateDate( $this->getContractStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_start_date', __( 'Contract start date is not properly formatted.' ) ) );
		}

		return $boolIsValid;
	}

	public function valContractTypeId() {

		$boolIsValid = true;

		// Allowing Original Opportunity to be null temporarily.
		if( false == isset( $this->m_intContractTypeId ) || false == is_numeric( $this->m_intContractTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_type_id', __( 'Contract type required.' ) ) );
		}
		// elseif( CContractType::RENEWAL == $this->getContractTypeId() && ( false == isset( $this->m_intRenewalContractId ) || false == is_numeric( $this->m_intRenewalContractId ) ) ) {
		//	$boolIsValid = false;
		// $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_contract_id', 'Original opportunity required for upgraded contract.' ) );
		// }

		return $boolIsValid;
	}

	public function valTerminationDate( $boolIsUnterminate = false ) {
		$boolIsValid = true;

		if( true == $boolIsUnterminate ) {

			if( false == is_null( $this->getTerminationDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_reason', 'Termination date is not required.' ) );
			}

			return $boolIsValid;
		}

		if( true == is_null( $this->getTerminationDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_date', 'Valid termination date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContractStatusTypeId( $boolIsDemoRequired = false, $objDatabase = NULL ) {

		$boolIsValid = true;

		if( true == is_null( $this->getContractStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_status_type_id', 'Contract status type is required.' ) );
		} elseif( true == $boolIsDemoRequired ) {
			$arrstrDemoActions = CActions::createService()->fetchActionsByActionTypeIdByActionResultIdByPsLeadIds( CActionType::DEMO, CActionResult::COMPLETED, [ $this->getPsLeadId() ], $objDatabase );
			if( false == valArr( $arrstrDemoActions ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_status_type_id', 'You cannot change the contract status to proposal sent unless the demo is completed.' ) );
			}
		}

		return $boolIsValid;

	}

	public function valSalesEmployeeId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getSalesEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sales_employee_id', __( 'Sales closer is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPercentCloseLikelihood() {
		$boolIsValid = true;

		if( false == is_null( $this->getPercentCloseLikelihood() ) && ( 0 > $this->getPercentCloseLikelihood() || 100 < $this->getPercentCloseLikelihood() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percent_close_likelihood', 'Please enter valid percent likelihood.' ) );
		}

		return $boolIsValid;
	}

	public function valCloseDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getCloseDate() ) && true != CValidation::validateDate( $this->getCloseDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'close_date', 'Close date is not properly formatted.' ) );
		}

		return $boolIsValid;
	}

	public function valAnticipatedCloseDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getAnticipatedCloseDate() ) && true != CValidation::validateDate( $this->getAnticipatedCloseDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'anticipated_close_date', 'Expected close date is not properly formatted.' ) );
		}

		if( true == is_null( $this->getAnticipatedCloseDate() ) && $this->getContractStatusTypeId() == CContractStatusType::PROPOSAL_SENT ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'anticipated_close_date', 'Expected close date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContractApproved( $objDatabase = NULL ) {

		$boolIsValid = true;

		if ( CContractStatusType::CONTRACT_APPROVED != $this->getContractStatusTypeId() || NULL == $objDatabase ) return true;

		$objPsLead		= $this->getOrFetchPsLead( $objDatabase );
		$objClient		= $this->fetchClient( $objDatabase );

		if( false == valObj( $objPsLead, 'CPsLead' ) ) {
			return false;
		}

		if( CContractStatusType::CONTRACT_APPROVED == $this->getContractStatusTypeId() && false == in_array( $objPsLead->getCompanyStatusTypeId(), CCompanyStatusType::$c_arrintAllowApproveContractCompanyStatusTypes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_status_type', 'Company status should be Client, Test Database or Sales Demo to approve the contract.' ) );
		}

		if( CContractStatusType::CONTRACT_APPROVED == $this->getContractStatusTypeId() && CContractType::RENEWAL != $this->getContractTypeId() && true == valObj( $objClient, 'CClient' ) && CCompanyStatusType::TEST_DATABASE != $objClient->getCompanyStatusTypeId() ) {

			$arrobjContractDocuments = CContractDocuments::createService()->fetchActiveContractDocumentsByContractIdsByCidByContractDocumentTypes( [ $this->getId() ], $this->getCid(), CContractDocumentType::$c_arrintSignedContractDocumentTypeIds, $objDatabase );

			if( !valArr( $arrobjContractDocuments ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_status_type_id', 'Please upload the signed / eSigned license agreement / contract before flagging as contract approved.' ) );
			}
		}

		$strErrorMessage = 'contract approved';

		if( true == is_null( $objPsLead->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_status_type_id', 'Please associate a client before flagging as ' . $strErrorMessage . '.' ) );
		}

		if( CContractStatusType::CONTRACT_APPROVED == $this->getContractStatusTypeId() ) {
			// Make sure close date is set if status is set to Contract Approved
			if( true == is_null( $this->getCloseDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'close_date', 'Close date is required for this status type.' ) );
			}

			// Make sure contract start date is set if status is set to Contract Approved
			if( true == is_null( $this->getContractStartDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_start_date', 'Contract start date is required for this status type.' ) );
			}
		}

		if( CContractType::RENEWAL != $this->getContractTypeId() && CContractStatusType::CONTRACT_APPROVED == $this->getContractStatusTypeId() ) {

			if( true == is_null( $objPsLead->getPropertyCount() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_count', 'Total portfolio property count is required.' ) );
			}

			if( true == is_null( $objPsLead->getNumberOfUnits() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_units', 'Total portfolio unit count is required.' ) );
			} elseif( 0 >= $objPsLead->getNumberOfUnits() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_units', 'Total portfolio unit count cannot be zero.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validateDuplicateContractProperties( $arrobjContractProperties, $objAdminDatabase, $intExcludeContractId = NULL, $boolIsActive = false ) {

		if( true == valArr( $arrobjContractProperties ) ) {
			$arrintPsProductIds	= array_filter( array_keys( rekeyObjects( 'PsProductId', $arrobjContractProperties ) ) );
			$arrintPropertyIds	= array_filter( array_keys( rekeyObjects( 'PropertyId', $arrobjContractProperties ) ) );
		}

		$arrobjActiveContractProperties	= CContractProperties::createService()->fetchActiveContractPropertiesByPropertyIdsByPsProductIdsByCid( $arrintPropertyIds, $arrintPsProductIds, $this->getCid(), $objAdminDatabase, $intExcludeContractId, $boolIsActive );

		$strMsg = '';
		if( true == valArr( $arrobjActiveContractProperties ) ) {
			foreach( $arrobjActiveContractProperties as $objActiveContractProperty ) {

				$strBundleName = ( false == is_null( $objActiveContractProperty->getBundleProductName() ) ) ? ' in bundle ' . $objActiveContractProperty->getBundleProductName() : '';
				$strMsg = $strMsg . $objActiveContractProperty->getProductName() . ' product is already associated with active contract ' . $objActiveContractProperty->getContractId() . $strBundleName . '.';
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', $strMsg ) );
			return false;
		}

		return true;
	}

	public function valOnHoldReasonId() {
		$boolIsValid = true;

		if( CContractStatusType::ON_HOLD == $this->getContractStatusTypeId() && false == valId( $this->getOnHoldReasonId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'on_hold_reasom_id', 'On Hold Reason is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsDemoRequired = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valContractDatetime();
				$boolIsValid &= $this->valContractStartDate();
				$boolIsValid &= $this->valContractTypeId();
				$boolIsValid &= $this->valSalesEmployeeId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid = true;
				break;

			case 'validate_termination':
				$boolIsValid &= $this->valTerminationDate();
				break;

			case 'validate_untermination':
				$boolIsValid &= $this->valTerminationDate( $boolIsUnterminate = true );
				break;

			case 'contract_renew':
				$boolIsValid &= $this->valContractDatetime();
				$boolIsValid &= $this->valContractStartDate();
				break;

			case 'create_contract':
				$boolIsValid &= $this->valContractTypeId();
				$boolIsValid &= $this->valContractStatusTypeId( $boolIsDemoRequired, $objDatabase );
				$boolIsValid &= $this->valPsLeadId();
				$boolIsValid &= $this->valSalesEmployeeId();
				$boolIsValid &= $this->valPercentCloseLikelihood();
				$boolIsValid &= $this->valCloseDate();
				$boolIsValid &= $this->valAnticipatedCloseDate();
				$boolIsValid &= $this->valContractApproved( $objDatabase );
				$boolIsValid &= $this->valContractDatetime();
				$boolIsValid &= $this->valContractStartDate();
				$boolIsValid &= $this->valOnHoldReasonId();
				break;

			case 'app_store_contract':
				$boolIsValid &= $this->valContractDatetime();
				$boolIsValid &= $this->valContractStartDate();
				$boolIsValid &= $this->valContractTypeId();
				break;

			case 'ps_website_request':
				$boolIsValid &= $this->valContractStatusTypeId();
				$boolIsValid &= $this->valContractDatetime();

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	// Set contract product data to the contract properties. once we update contract product we are calling this function. eg update product, assign product to properties and remove exception from contract properties

	public function applyContractProductData( $arrobjContractProperties, $objAdminDatabase, $arrobjContractProducts = NULL, $arrobjProperties = NULL, $arrobjContractPropertyPricingData = NULL, $strNewEffectiveBillingDate= NULL, $intUserId ) {

		$arrobjInsertContractPropertyPricings = [];
		$arrobjUpdateContractPropertyPricings = [];
		$arrobjSecondLastRecordUpdateContractPropertyPricings = [];
		$strOldBillingDate = NULL;
		$strSecondLastBillingDate = NULL;

		if( false == valArr( $arrobjContractProperties ) ) {
			return $arrobjContractProperties;
		}

		if( false == valArr( $arrobjContractProducts ) ) {
			$arrobjContractProducts = $this->fetchContractProducts( $objAdminDatabase, $boolIsFromAddProperty = true );
		}

		if( false == valArr( $arrobjContractProducts ) ) {
			return $arrobjContractProperties;
		}

		if( false == valArr( $arrobjProperties ) ) {
			$arrintPropertyIds = array_keys( rekeyObjects( 'PropertyId', $arrobjContractProperties ) );
			$arrobjProperties = \Psi\Eos\Admin\CProperties::createService()->fetchAllPropertiesByIdsByCid( $arrintPropertyIds, $this->getCid(), $objAdminDatabase );
		}

		if( false == valArr( $arrobjProperties ) ) {
			return $arrobjContractProperties;
		}
		// contract property pricing related data
		if( true == valArr( $arrobjContractPropertyPricingData ) ) {
			$arrobjContractPropertyPricings						= rekeyObjects( 'ContractPropertyId', $arrobjContractPropertyPricingData );
			$arrobjSecondLastRecordOfContractpropertyPricings	= rekeyObjects( 'ContractPropertyId', CContractPropertyPricings::createService()->fetchSecondLastRecordFromContractPropertyPricngsByContractPropertyIds( array_keys( $arrobjContractPropertyPricings ), $objAdminDatabase ), false, true );
		}
		$strNewEffectiveBillingDate = ( true == valStr( $strNewEffectiveBillingDate ) ) ? date( 'Y-m-01', strtotime( $strNewEffectiveBillingDate ) ) : NULL;

		foreach( $arrobjContractProperties as $objContractProperty ) {

			$objContractProduct = NULL;
			foreach( $arrobjContractProducts as $objContractProduct ) {

				if( $objContractProperty->getBundlePsProductId() == $objContractProduct->getBundlePsProductId() && $objContractProperty->getPsProductId() == $objContractProduct->getPsProductId() && true == valObj( $objContractProduct, 'CContractProduct' ) ) {

					// Set contract property data from template contract product
					// Calculate recurring and transactional amount by unit
					$intNumberOfUnits = $arrobjProperties[$objContractProperty->getPropertyId()]->getNumberOfUnits();

					$objContractProperty->setTransactionalAmount( $objContractProduct->getTransactionalAmount() * $intNumberOfUnits );

					if( 0 < ( INT ) $objContractProduct->getMaxUnits() ) {
						$intNumberOfUnits = ( ( int ) $objContractProduct->getMaxUnits() < ( int ) $intNumberOfUnits ) ? $objContractProduct->getMaxUnits() : $intNumberOfUnits;
					}

					if( 0 < ( INT ) $objContractProduct->getMinUnits() ) {
						$intNumberOfUnits = ( ( int ) $objContractProduct->getMinUnits() > ( int ) $intNumberOfUnits ) ? $objContractProduct->getMinUnits() : $intNumberOfUnits;
					}

					if( valId( $objContractProduct->getPackageProductId() ) && CContractProduct::PAYMENT_TYPE_PER_PROPERTY == $objContractProduct->getPaymentTypeId() ) {
						$intNumberOfUnits = 1;
					}

					$fltNewMonthlyRecurringAmount  = $objContractProduct->getRecurringAmount() * $intNumberOfUnits;
					$fltNewMonthlyRecurringAmount = __( '{%f, 0, p:2}', [ $fltNewMonthlyRecurringAmount ] );

					if( true == valArr( $arrobjContractPropertyPricings ) && true == valObj( $arrobjContractPropertyPricings[$objContractProperty->getId()], 'CContractPropertyPricing' ) ) {
						$objOldContractPropertyPricing = $arrobjContractPropertyPricings[$objContractProperty->getId()];
						$objSecondLastRecordOfContractPropertyPricing = NULL;
						if( true == valArr( $arrobjSecondLastRecordOfContractpropertyPricings ) && true == valObj( $arrobjSecondLastRecordOfContractpropertyPricings[$objContractProperty->getId()], 'CContractPropertyPricing' ) ) {
							$objSecondLastRecordOfContractPropertyPricing = $arrobjSecondLastRecordOfContractpropertyPricings[$objContractProperty->getId()];
						}
						// if charges are not posted yet
						if( true == is_null( $objContractProperty->getlastPostedOn() ) ) {
							$strCalculatedNewBillingStartDate = ( true == valStr( $strNewEffectiveBillingDate ) ) ? date( 'Y-m-01', strtotime( $strNewEffectiveBillingDate ) ) : date( 'Y-m-01', strtotime( '+1 month' ) );
							$strCalculatedNewBillingStartDate = $objContractProperty->getCalculatedNewBillingStartDateByEffectiveDate( $strCalculatedNewBillingStartDate );

							if( 0 == $objOldContractPropertyPricing->getPrice() && ( ( $objContractProduct->getPercentWeight() > 0 ) || true == is_null( $objContractProduct->getPercentWeight() ) ) && $fltNewMonthlyRecurringAmount != $objContractProperty->getMonthlyRecurringAmount() ) {
								$objOldContractPropertyPricing->setEndDate( date( 'Y-m-t', strtotime( '-1 month', strtotime( $strCalculatedNewBillingStartDate ) ) ) );
								$arrobjUpdateContractPropertyPricings[$objOldContractPropertyPricing->getId()] = $objOldContractPropertyPricing;

								$objDetails																= $objOldContractPropertyPricing->getDetailsField( [] );
								$strPPCStartDate														= $objOldContractPropertyPricing->getPPCStartDate();
								$arrobjInsertContractPropertyPricings[$objContractProperty->getId()]	= $objContractProperty->createDefaultContractPropertyPricing( $fltNewMonthlyRecurringAmount, $fltNewMonthlyRecurringAmount - $objOldContractPropertyPricing->getPrice(), NULL, $strCalculatedNewBillingStartDate, $strPPCStartDate, $objDetails );
								$strOldBillingDate = $objOldContractPropertyPricing->getStartDate();

							} else {
								if( true == valObj( $objSecondLastRecordOfContractPropertyPricing, 'CContractPropertyPricing' ) ) {
									if( 0 == $objSecondLastRecordOfContractPropertyPricing->getPrice() && 0 == $fltNewMonthlyRecurringAmount ) {
										$objSecondLastRecordOfContractPropertyPricing->setStartDate( $strCalculatedNewBillingStartDate );
										$objSecondLastRecordOfContractPropertyPricing->setEndDate( NULL );
										$arrobjDeleteOldContractPropertyPricings[$objOldContractPropertyPricing->getId()] = $objOldContractPropertyPricing;
										$objContractProperty->setBillingStartDate( $strCalculatedNewBillingStartDate );
									} else {
										$objSecondLastRecordOfContractPropertyPricing->setEndDate( date( 'Y-m-t', strtotime( '-1 month', strtotime( $strCalculatedNewBillingStartDate ) ) ) );
										$arrobjUpdateContractPropertyPricings[$objOldContractPropertyPricing->getId()] = $objOldContractPropertyPricing->updateOldContractPropertyPricings( $fltNewMonthlyRecurringAmount, $strCalculatedNewBillingStartDate, $fltNewMonthlyRecurringAmount, NULL, $boolIsDetails = true );
									}
									$arrobjSecondLastRecordUpdateContractPropertyPricings[$objSecondLastRecordOfContractPropertyPricing->getId()] = $objSecondLastRecordOfContractPropertyPricing;
									$strSecondLastBillingDate = $objSecondLastRecordOfContractPropertyPricing->getStartDate();
								} else if( $fltNewMonthlyRecurringAmount != $objContractProperty->getMonthlyRecurringAmount() ) {
									$arrobjUpdateContractPropertyPricings[$objOldContractPropertyPricing->getId()] = $objOldContractPropertyPricing->updateOldContractPropertyPricings( $fltNewMonthlyRecurringAmount, $strCalculatedNewBillingStartDate, $fltNewMonthlyRecurringAmount, NULL, $boolIsDetails = true );
								}
							}

							if( $fltNewMonthlyRecurringAmount != $objContractProperty->getMonthlyRecurringAmount() ) {
								// If two old and new pricings have different date then need to set min date as billing_start_date
								$objContractProperty->setMinimumBillingStartDate( $strCalculatedNewBillingStartDate, $strOldBillingDate, $strSecondLastBillingDate );
								if( 0 != $objContractProperty->getMonthlyRecurringAmount() ) {
									$objContractProperty->setMonthlyRecurringAmount( $fltNewMonthlyRecurringAmount );
								}
							}
						} else {
							// If Charges are posted
							if( ( $fltNewMonthlyRecurringAmount == $objContractProperty->getMonthlyRecurringAmount() && ( false == is_null( $fltNewMonthlyRecurringAmount ) ) && ( 0 != $fltNewMonthlyRecurringAmount ) ) || ( 0 == $fltNewMonthlyRecurringAmount && 0 == $objContractProperty->getMonthlyRecurringAmount() && ( false == is_null( $objContractProperty->getlastPostedOn() ) ) && ( 0 != $objOldContractPropertyPricing->getPrice() ) ) ) {
								if( true == valObj( $objSecondLastRecordOfContractPropertyPricing, 'CContractPropertyPricing' ) ) {
									$objSecondLastRecordOfContractPropertyPricing->setEndDate( NULL );
									$arrobjSecondLastRecordUpdateContractPropertyPricings[$objSecondLastRecordOfContractPropertyPricing->getId()] = $objSecondLastRecordOfContractPropertyPricing;
									$arrobjDeleteOldContractPropertyPricings[$objOldContractPropertyPricing->getId()] = $objOldContractPropertyPricing;
								}
							} else if( $fltNewMonthlyRecurringAmount != $objContractProperty->getMonthlyRecurringAmount() ) {
								$objLastPostedOn = new DateTime( $objContractProperty->getlastPostedOn() );
								$strCalculatedNewBillingStartDate = $objContractProperty->getCalculatedNewBillingStartDateByEffectiveDate( $strNewEffectiveBillingDate );

								$fltMonthlyChangeAmt = ( true == valObj( $objSecondLastRecordOfContractPropertyPricing, 'CContractPropertyPricing' ) ) ? $fltNewMonthlyRecurringAmount - $objSecondLastRecordOfContractPropertyPricing->getPrice() : $fltNewMonthlyRecurringAmount;
								// user selected future effective date and one active record in cpp
								if( $objLastPostedOn->format( 'Y-m-d' ) < $strCalculatedNewBillingStartDate && false == valObj( $objSecondLastRecordOfContractPropertyPricing, 'CContractPropertyPricing' ) && date( 'Y-m-d', strtotime( $objOldContractPropertyPricing->getStartDate() ) ) < $strCalculatedNewBillingStartDate ) {
									$objOldContractPropertyPricing->setEndDate( date( 'Y-m-t', strtotime( '-1 month', strtotime( $strCalculatedNewBillingStartDate ) ) ) );
									$arrobjUpdateContractPropertyPricings[$objOldContractPropertyPricing->getId()] = $objOldContractPropertyPricing;
									$objDetails                                                                    = $objOldContractPropertyPricing->getDetailsField( [] );
									$strPPCStartDate                                                               = $objOldContractPropertyPricing->getPPCStartDate();
									$arrobjInsertContractPropertyPricings[$objContractProperty->getId()]           = $objContractProperty->createDefaultContractPropertyPricing( $fltNewMonthlyRecurringAmount, $fltNewMonthlyRecurringAmount - $objOldContractPropertyPricing->getPrice(), NULL, $strCalculatedNewBillingStartDate, $strPPCStartDate, $objDetails );
								} else {
									if( date( 'Y-m-d', strtotime( $objOldContractPropertyPricing->getStartDate() ) ) == $strCalculatedNewBillingStartDate ) {
										$arrobjUpdateContractPropertyPricings[$objOldContractPropertyPricing->getId()] = $objOldContractPropertyPricing->updateOldContractPropertyPricings( $fltNewMonthlyRecurringAmount, $strCalculatedNewBillingStartDate, $fltMonthlyChangeAmt, NULL, $boolIsDetails = true );
									} else {
										if( true == valObj( $objSecondLastRecordOfContractPropertyPricing, 'CContractPropertyPricing' ) ) {
											// if last posted on set and charges are posted
											if( $objLastPostedOn->format( 'Y-m-d' ) >= date( 'Y-m-d', strtotime( $objOldContractPropertyPricing->getStartDate() ) ) ) {
												$objOldContractPropertyPricing->setEndDate( date( 'Y-m-t', strtotime( '-1 month', strtotime( $strCalculatedNewBillingStartDate ) ) ) );
												$arrobjUpdateContractPropertyPricings[$objOldContractPropertyPricing->getId()]	= $objOldContractPropertyPricing;
												$objDetails																		= $objOldContractPropertyPricing->getDetailsField( [] );
												$strPPCStartDate																= $objOldContractPropertyPricing->getPPCStartDate();
												$arrobjInsertContractPropertyPricings[$objContractProperty->getId()]			= $objContractProperty->createDefaultContractPropertyPricing( $fltNewMonthlyRecurringAmount, $fltNewMonthlyRecurringAmount - $objOldContractPropertyPricing->getPrice(), NULL, $strCalculatedNewBillingStartDate, $strPPCStartDate, $objDetails );
											} else if( ( false == is_null( $strNewEffectiveBillingDate ) ) || ( true == is_null( $strNewEffectiveBillingDate ) && $fltNewMonthlyRecurringAmount != $objOldContractPropertyPricing->getPrice() ) ) {
												// if last posted on set and less than old billing start date means charges are not posted yet
												$objSecondLastRecordOfContractPropertyPricing->setEndDate( date( 'Y-m-t', strtotime( '-1 month', strtotime( $strCalculatedNewBillingStartDate ) ) ) );
												$arrobjUpdateContractPropertyPricings[$objOldContractPropertyPricing->getId()]                                = $objOldContractPropertyPricing->updateOldContractPropertyPricings( $fltNewMonthlyRecurringAmount, $strCalculatedNewBillingStartDate, $fltMonthlyChangeAmt, NULL, $boolIsDetails = true );
												$arrobjSecondLastRecordUpdateContractPropertyPricings[$objSecondLastRecordOfContractPropertyPricing->getId()] = $objSecondLastRecordOfContractPropertyPricing;
											}
										}
									}
								}
							}
						}

					} else {
						$objContractProperty->setMonthlyRecurringAmount( $fltNewMonthlyRecurringAmount );
					}

					$objContractProperty->setImplementationAmount( $objContractProduct->getImplementationAmount() );
					$objContractProperty->setCommissionBucketId( $objContractProduct->getCommissionBucketId() );

					// to set amount as 0 in case of renewal contract for existing properties.
					if( true == $objContractProperty->getBoolApplyProductSetUpAmount() ) {
						$objContractProperty->setImplementationAmount( 0 );
					}

					$objContractProperty->setIsUnlimited( $objContractProduct->getIsUniversal() );
				}
			}

		}

		if( true == valArr( $arrobjSecondLastRecordUpdateContractPropertyPricings ) && false == CContractPropertyPricings::createService()->bulkUpdate( $arrobjSecondLastRecordUpdateContractPropertyPricings, [ 'start_date', 'end_date' ], $intUserId, $objAdminDatabase ) ) {
			return false;
		}
		if( true == valArr( $arrobjUpdateContractPropertyPricings ) && false == CContractPropertyPricings::createService()->bulkUpdate( $arrobjUpdateContractPropertyPricings, [ 'price', 'start_date', 'end_date', 'monthly_change_amount', 'details' ], $intUserId, $objAdminDatabase ) ) {
			return false;
		}
		if( true == valArr( $arrobjInsertContractPropertyPricings ) && false == CContractPropertyPricings::createService()->bulkInsert( $arrobjInsertContractPropertyPricings, $intUserId, $objAdminDatabase ) ) {
			return false;
		}
		if( true == valArr( $arrobjDeleteOldContractPropertyPricings ) && false == CContractPropertyPricings::createService()->bulkDelete( $arrobjDeleteOldContractPropertyPricings, $objAdminDatabase ) ) {
			return false;
		}

		return $arrobjContractProperties;
	}

	public function applyBundlePsProductData( $intBundlePsProductId, $arrobjContractProducts, $arrmixContractProductData, $objAdminDatabase, $arrobjPsProductRelationships = NULL ) {

		if( false == is_numeric( $intBundlePsProductId ) || false == valArr( $arrobjContractProducts ) ) {
			return $arrobjContractProducts;
		}

		if( false == valArr( $arrobjPsProductRelationships ) ) {
			$arrobjPsProductRelationships = CPsProductRelationships::createService()->fetchPsProductRelationshipsByBundlePsProductIdByPsLeadId( $intBundlePsProductId, $this->getPsLeadId(), $objAdminDatabase );
		}

		if( false == valArr( $arrobjPsProductRelationships ) ) {
			return $arrobjContractProducts;
		}

		$arrmixContractProductAmounts = [
			'transactional_amount' 				=> 0,
			'recurring_amount'					=> 0,
			'implementation_amount'				=> 0,
		];

		$arrobjContractProducts		= rekeyObjects( 'PsProductId', $arrobjContractProducts );

		$arrmixContractProductData	= array_merge( $arrmixContractProductAmounts, $arrmixContractProductData );
		$intBundleProductCount = 1;
		$fltLargeRecurringAmount = 0;
		$fltImplemetationAmount = 0;
		$arrobjPsProducts = CPsProducts::createService()->fetchPsProductsByIds( array_keys( rekeyObjects( 'PsProductId', $arrobjPsProductRelationships ) ), $objAdminDatabase );
		foreach( $arrobjPsProductRelationships as $objPsProductRelationship ) {

			$fltPercentWeight	= $objPsProductRelationship->getPercentWeight();
			$intPsProductId		= $objPsProductRelationship->getPsProductId();

			if( false == isset( $arrobjContractProducts[$intPsProductId] ) ) {
				continue;
			}

			if( $intBundleProductCount < \Psi\Libraries\UtilFunctions\count( $arrobjContractProducts ) ) {

				$intBundleProductCount++;
				$fltLargeRecurringAmount += round( $arrmixContractProductData['recurring_amount'] * $fltPercentWeight / 100, 2 );
				$fltImplemetationAmount += round( $arrmixContractProductData['implementation_amount'] * $fltPercentWeight / 100, 2 );

				$arrobjContractProducts[$intPsProductId]->setRecurringAmount( round( ( $arrmixContractProductData['recurring_amount'] * $fltPercentWeight / 100 ), 2 ) );
				$arrobjContractProducts[$intPsProductId]->setImplementationAmount( round( ( $arrmixContractProductData['implementation_amount'] * $fltPercentWeight / 100 ), 2 ) );
			} else {
				$arrobjContractProducts[$intPsProductId]->setRecurringAmount( $arrmixContractProductData['recurring_amount'] - round( $fltLargeRecurringAmount, 2 ) );
				$arrobjContractProducts[$intPsProductId]->setImplementationAmount( $arrmixContractProductData['implementation_amount'] - round( $fltImplemetationAmount, 2 ) );
			}

			if( true == $arrobjPsProducts[$intPsProductId]->getIsTransactional() && true == isset( $arrmixContractProductData['transactional_amount' . $intPsProductId] ) ) {
				$arrobjContractProducts[$intPsProductId]->setTransactionalAmount( $arrmixContractProductData['transactional_amount' . $intPsProductId] );
			} else {
				$arrobjContractProducts[$intPsProductId]->setTransactionalAmount( 0 );
			}
		}

		return $arrobjContractProducts;
	}

	// Prepare array of property product data for all active contracts exlude this contract

	public function loadExistingActiveContractProperties( $objAdminDatabase ) {

		$arrobjContractProperties			= [];
		return CContractProperties::createService()->fetchActiveContractPropertiesByCid( $this->getCid(), $objAdminDatabase );
	}

	public function handleLogging( $intUserId, $objDatabase, $boolIsInsert = false ) {

		$boolIsValid = true;
		$arrobjActions = [];

		$objContract = CContracts::createService()->fetchContractById( $this->getId(), $objDatabase );

		if( false == valObj( $objContract, 'CContract' ) ) {
			$objContract = $this;
		}

		if( true == valObj( $objContract, 'CContract' ) && $objContract->getSalesEmployeeId() != $this->getSalesEmployeeId() ) {

			$objAction = $this->createAction();
			$objAction->setActionTypeId( CActionType::SALES_REP_CHANGES );

			$strNote						= 'Changed sales rep ';
			$arrintSaleEmployees			= [];
			$arrobjSalesEmployees			= [];

			if( true == is_numeric( $objContract->getSalesEmployeeId() ) ) {
				$arrintSaleEmployees[] = $objContract->getSalesEmployeeId();
			}

			if( true == is_numeric( $this->getSalesEmployeeId() ) ) {
				$arrintSaleEmployees[] = $this->getSalesEmployeeId();
			}

			if( true == valArr( $arrintSaleEmployees ) ) $arrobjSalesEmployees = CEmployees::createService()->fetchEmployeesByIds( $arrintSaleEmployees, $objDatabase );

			if( true == valArr( $arrobjSalesEmployees ) ) {
				$strNote 	.= ( true == isset( $arrobjSalesEmployees[$objContract->getSalesEmployeeId()] ) ) ? ' from ' . $arrobjSalesEmployees[$objContract->getSalesEmployeeId()]->getNameFirst() . ' ' . $arrobjSalesEmployees[$objContract->getSalesEmployeeId()]->getNameLast() : '';
				$strNote 	.= ( true == isset( $arrobjSalesEmployees[$this->getSalesEmployeeId()] ) ) ? ' to ' . $arrobjSalesEmployees[$this->getSalesEmployeeId()]->getNameFirst() . ' ' . $arrobjSalesEmployees[$this->getSalesEmployeeId()]->getNameLast() : '';
			}

			$objActionType = CActionTypes::createService()->fetchActionTypeById( CActionType::SALES_REP_CHANGES, $objDatabase );

			$objAction->setActionDescription( '<strong>' . $objActionType->getName() . '</strong>' );

			$objAction->setNotes( $strNote );
			$arrobjActions[] = $objAction;

		}

		$arrintEligbleActionTypeIds = [];
		$boolIsOnHoldAction = false;
		$intOnHoldActionId = $objContract->getOnHoldActionId();

		if( true == valId( $intOnHoldActionId ) ) {

			$objOnHoldAction = CActions::createService()->fetchActionById( $intOnHoldActionId, $objDatabase );
			if( true == valObj( $objOnHoldAction, CAction::class ) ) {
				$objContract->setOnHoldReasonId( $objOnHoldAction->getOnHoldReasonId() );
			}

		}

		// By default action will create for OPPORTUNITY_ADDED action type while creating opportunity
		if( true == $boolIsInsert ) {
			$arrintEligbleActionTypeIds[] = CActionType::OPPORTUNITY_ADDED;
		}

		switch( $this->getContractStatusTypeId() ) {

			case CContractStatusType::ON_HOLD:
				$arrintEligbleActionTypeIds[] = CActionType::ON_HOLD;
				$boolIsOnHoldAction = true;
				break;

			case CContractStatusType::CONTACTED:
				$arrintEligbleActionTypeIds[] = CActionType::CONTACTED;
				break;

			case CContractStatusType::QUALIFIED:
				$arrintEligbleActionTypeIds[] = CActionType::QUALIFIED;
				break;

			case CContractStatusType::DISCOVERY:
				$arrintEligbleActionTypeIds[] = CActionType::DISCOVERY;
				break;

			case CContractStatusType::DEMOED:
				$arrintEligbleActionTypeIds[] = CActionType::DEMOED;
				break;

			case CContractStatusType::PROPOSAL_SENT:
				$arrintEligbleActionTypeIds[] = CActionType::PROPOSAL_SENT;
				break;

			case CContractStatusType::CONTRACT_NEGOTIATION:
				$arrintEligbleActionTypeIds[] = CActionType::CONTRACT_NEGOTIATION;
				break;

			case CContractStatusType::CONTRACT_SENT:
				$arrintEligbleActionTypeIds[] = CActionType::CONTRACT_SENT;
				break;

			case CContractStatusType::CONTRACT_APPROVED:
				$arrintEligbleActionTypeIds[] = CActionType::CONTRACT_APPROVED;
				break;

			case CContractStatusType::CONTRACT_TERMINATED:
				$arrintEligbleActionTypeIds[] = CActionType::CONTRACT_TERMINATED;
				break;

			case CContractStatusType::LOST:
				$arrintEligbleActionTypeIds[] = CActionType::LOST;
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		$arrobjActionTypes 			= CActionTypes::createService()->fetchAllActionTypes( $objDatabase );
		$arrobjContractStatusTypes	= CContractStatusTypes::createService()->fetchAllContractStatusTypes( $objDatabase );
		$intOldActionTypeId			= NULL;

		if( true == valArr( $arrintEligbleActionTypeIds ) && ( true == $boolIsInsert || $this->getContractStatusTypeId() <> $objContract->getContractStatusTypeId() || ( $boolIsOnHoldAction && $this->getOnHoldReasonId() <> $objContract->getOnHoldReasonId() ) ) ) {

			foreach( $arrintEligbleActionTypeIds as $intActionTypeId ) {

				$objAction = $this->createAction();
				$objAction->setActionTypeId( $intActionTypeId );
				$objAction->setActionDescription( $arrobjActionTypes[$intActionTypeId]->getName() );
				$objAction->setNewStatusTypeId( $arrobjActionTypes[$intActionTypeId]->getStatusTypeId() );

				if( CActionType::ON_HOLD == $intActionTypeId ) {
					$objAction->setOnHoldReasonId( $this->getOnHoldReasonId() );
				}

				if( false == $boolIsInsert ) {
					$objAction->setOldStatusTypeId( $objContract->getContractStatusTypeId() );
					$objAction->setNotes( 'Contract Status type changed from ' . $arrobjContractStatusTypes[$objContract->getContractStatusTypeId()]->getName() . ' to ' . $arrobjActionTypes[$intActionTypeId]->getName() );
				} else {
					if( CActionType::OPPORTUNITY_ADDED == $objAction->getActionTypeId() ) {
						$objAction->setNotes( 'Contract added with status type ' . $arrobjActionTypes[$intActionTypeId]->getName() );
					} else {
						$objAction->setOldStatusTypeId( $arrobjActionTypes[$intOldActionTypeId]->getStatusTypeId() );
						$objAction->setNotes( 'Contract Status type changed from ' . $arrobjActionTypes[$intOldActionTypeId]->getName() . ' to ' . $arrobjActionTypes[$intActionTypeId]->getName() );
					}
					$intOldActionTypeId = $intActionTypeId;
				}
				$arrobjActions[] = $objAction;
			}
		}

		if( true == isset( $arrobjActions ) && true == valArr( $arrobjActions ) ) {
			foreach( $arrobjActions as $objAction ) {
				$boolIsValid &= $objAction->insert( $intUserId, $objDatabase );

				switch( $objAction->getActionTypeId() ) {

					case CActionType::ON_HOLD:
						if( true == $boolIsInsert ) {
							$objContract->setOnHoldActionId( $objAction->getId() );
							$objContract->setOnHoldDate( $objAction->getActionDatetime() );
						} else {
							$this->setOnHoldActionId( $objAction->getId() );
							$this->setOnHoldDate( $objAction->getActionDatetime() );
						}
						break;

					case CActionType::CONTACTED:
						if( true == $boolIsInsert ) {
							$objContract->setContactedActionId( $objAction->getId() );
							$objContract->setContactedDate( $objAction->getActionDatetime() );
						} else {
							$this->setContactedActionId( $objAction->getId() );
							$this->setContactedDate( $objAction->getActionDatetime() );
						}
						break;

					case CActionType::QUALIFIED:
						if( true == $boolIsInsert ) {
							$objContract->setQualifiedActionId( $objAction->getId() );
							$objContract->setQualifiedDate( $objAction->getActionDatetime() );
						} else {
							$this->setQualifiedActionId( $objAction->getId() );
							$this->setQualifiedDate( $objAction->getActionDatetime() );
						}
						break;

					case CActionType::DISCOVERY:
						if( true == $boolIsInsert ) {
							$objContract->setDiscoveryActionId( $objAction->getId() );
							$objContract->setDiscoveryDate( $objAction->getActionDatetime() );
						} else {
							$this->setDiscoveryActionId( $objAction->getId() );
							$this->setDiscoveryDate( $objAction->getActionDatetime() );
						}
						break;

					case CActionType::DEMOED:
						if( true == $boolIsInsert ) {
							$objContract->setDemoActionId( $objAction->getId() );
							$objContract->setDemoDate( $objAction->getActionDatetime() );
						} else {
							$this->setDemoActionId( $objAction->getId() );
							$this->setDemoDate( $objAction->getActionDatetime() );
						}
						break;

					case CActionType::PROPOSAL_SENT:
						if( true == $boolIsInsert ) {
							$objContract->setProposalSentActionId( $objAction->getId() );
							$objContract->setProposalSentDate( $objAction->getActionDatetime() );
						} else {
							$this->setProposalSentActionId( $objAction->getId() );
							$this->setProposalSentDate( $objAction->getActionDatetime() );
						}
						break;

					case CActionType::CONTRACT_NEGOTIATION:
						if( true == $boolIsInsert ) {
							$objContract->setContractNegotiationActionId( $objAction->getId() );
							$objContract->setContractNegotiationDate( $objAction->getActionDatetime() );
						} else {
							$this->setContractNegotiationActionId( $objAction->getId() );
							$this->setContractNegotiationDate( $objAction->getActionDatetime() );
						}
						break;

					case CActionType::CONTRACT_SENT:
						if( true == $boolIsInsert ) {
							$objContract->setContractSentActionId( $objAction->getId() );
							$objContract->setContractSentDate( $objAction->getActionDatetime() );
						} else {
							$this->setContractSentActionId( $objAction->getId() );
							$this->setContractSentDate( $objAction->getActionDatetime() );
						}
						break;

					case CActionType::CONTRACT_TERMINATED:
						$this->setContractTerminatedActionId( $objAction->getId() );
						$this->setContractTerminatedDate( $objAction->getActionDatetime() );
						break;

					case CActionType::CONTRACT_APPROVED:
						$this->setContractApprovedActionId( $objAction->getId() );
						$this->setContractApprovedDate( $objAction->getActionDatetime() );
						break;

					case CActionType::LOST:
						if( true == $boolIsInsert ) {
							$objContract->setLeadLostActionId( $objAction->getId() );
							$objContract->setLeadLostDate( $objAction->getActionDatetime() );
						} else {
							$this->setLeadLostActionId( $objAction->getId() );
							$this->setLeadLostDate( $objAction->getActionDatetime() );
						}
						break;

					default:
						$boolIsValid = true;
						break;
				}

				if( true == $boolIsInsert ) {
					$objContract->setLastActionId( $objAction->getId() );
					$objContract->setLastActionDate( $objAction->getActionDatetime() );
				} else {
					$this->setLastActionId( $objAction->getId() );
					$this->setLastActionDate( $objAction->getActionDatetime() );
				}

				if( true == $boolIsInsert && CActionType::OPPORTUNITY_ADDED == $objAction->getActionTypeId() ) {
					$objContract->setEnteredPipelineActionId( $objAction->getId() );
					$objContract->setEnteredPipelineOn( date( 'm/d/Y' ) );
				}
			}

			if( true == $boolIsValid && true == $boolIsInsert && false == $objContract->update( $intUserId, $objDatabase, false, true ) ) {
				$boolIsValid = false;
			}
		}
		return $boolIsValid;
	}

	/**
	 * Database Functions
	 *
	 */

	public function sendNewSaleOpportunityAddedEmail( $intUserId, $objDatabase, $intDefaultDatabaseUserTypeId = CDatabaseUserType::PS_DEVELOPER ) {

		$objEmailDatabase = CDatabases::createService()->createDatabase( $intDefaultDatabaseUserTypeId, CDatabaseType::EMAIL, 1 );

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			return false;
		}

		$arrobjPsProducts			= CPsProducts::createService()->fetchAllPsProducts( $objDatabase );
		$this->m_arrobjEmployees	= CEmployees::createService()->fetchAllEmployees( $objDatabase );

		$objPsLead = CPsLeads::createService()->fetchPsLeadById( $this->getPsLeadId(), $objDatabase );
		$objPsLeadDetail = CPsLeadDetails::createService()->fetchPsLeadDetailByPsLeadId( $this->getPsLeadId(), $objDatabase );

		$arrobjSoldContractProducts = $this->getContractProducts();

		if( 0 < strlen( trim( $objPsLead->getCompanyName() ) ) ) {
			$strSubject = 'New Opportunity added for ' . $objPsLead->getCompanyName();
		} elseif( 0 < strlen( trim( $this->getTitle() ) ) ) {
			$strSubject = 'New Opportunity added: ' . $this->getTitle();
		} else {
			$strSubject = 'New Opportunity added';
		}

		// FixMe: As we don't have constant for entrata.com in ireland cloud keeping it hard coded, we need find a alternative to use entrata.com in different cloud
		$strTld = ( 'production' == CONFIG_ENVIRONMENT ) ? 'entrata.com' : CONFIG_COMPANY_BASE_DOMAIN;
		$strBaseUri = CONFIG_SECURE_HOST_PREFIX . 'clientadmin.' . $strTld;

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		$objSmarty->assign( 'ps_lead',				$objPsLead );
		$objSmarty->assign( 'contract',				$this );
		$objSmarty->assign( 'ps_products',			$arrobjPsProducts );
		$objSmarty->assign( 'employees',			$this->m_arrobjEmployees );
		$objSmarty->assign( 'contract_products',	$arrobjSoldContractProducts );

		$objSmarty->assign( 'CONTRACT_STATUS_TYPE_CONTRACT_APPROVED', CContractStatusType::CONTRACT_APPROVED );

		$objSmarty->assign( 'base_uri', $strBaseUri );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'sales/contracts/new_sale_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objEmployee = CEmployees::createService()->fetchEmployeeByUserId( $intUserId, $objDatabase );

		$arrstrToEmailAddress = [];

		if( true == valObj( $objEmployee, 'CEmployee' ) && true == CValidation::validateEmailAddresses( $objEmployee->getEmailAddress() ) ) {
			$arrstrToEmailAddress[] = $objEmployee->getEmailAddress();
			$strFromEmailAddress	= $objEmployee->getEmailAddress();
		} else {
			$strFromEmailAddress = CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;
		}

		$strSalesEmployeeEmailAddress = $this->getEmployeeEmailAddress( $this->getSalesEmployeeId() );
		if( 0 < strlen( $strSalesEmployeeEmailAddress ) ) {
			$arrstrToEmailAddress[] = $strSalesEmployeeEmailAddress;
		}

		$strResponsibleEmployeeEmailAddress = $this->getEmployeeEmailAddress( $this->getResponsibleEmployeeId() );
		if( 0 < strlen( $strResponsibleEmployeeEmailAddress ) ) {
			$arrstrToEmailAddress[] = $strResponsibleEmployeeEmailAddress;
		}

		$strSalesEngineerEmailAddress = $this->getEmployeeEmailAddress( $this->getSalesEngineerId() );
		if( 0 < strlen( $strSalesEngineerEmailAddress ) ) {
			$arrstrToEmailAddress[] = $strSalesEngineerEmailAddress;
		}

		$boolIsSuccess = true;

		if( true == valArr( $arrstrToEmailAddress ) ) {

			$arrstrToEmailAddress = array_unique( $arrstrToEmailAddress );

			$objSystemEmailLibrary = new CSystemEmailLibrary();

			$objSystemEmail = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::NEW_SALE, $strSubject, $strHtmlContent, $strToEmailAddress = NULL, 0, $strFromEmailAddress );
			$objSystemEmail->setToEmailAddress( implode( ', ', $arrstrToEmailAddress ) );

			if( false == $objSystemEmail->validate( VALIDATE_INSERT ) || false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
				$boolIsSuccess &= false;
			}
		}

		return $boolIsSuccess;
	}

	public function getEmployeeEmailAddress( $intEmployeeId ) {

		$strEmployeeEmailAddress = '';

		if( 0 < $intEmployeeId && true == array_key_exists( $intEmployeeId, $this->m_arrobjEmployees ) && false == is_null( $this->m_arrobjEmployees[$intEmployeeId]->getEmailAddress() ) ) {
			$strEmployeeEmailAddress = trim( $this->m_arrobjEmployees[$intEmployeeId]->getEmailAddress() );
		}

		return $strEmployeeEmailAddress;
	}

	public function insert( $intUserId, $objDatabase, $boolIsSendNewSaleEmail = true, $intDefaultDatabaseUserTypeId = CDatabaseUserType::PS_DEVELOPER ) {

		$boolIsInserted = true;

		$boolIsInserted &= parent::insert( $intUserId, $objDatabase );

		$boolIsInserted &= $this->handleLogging( $intUserId, $objDatabase, true );

		if( false != $boolIsInserted && ( true == in_array( $this->getContractStatusTypeId(), CContractStatusType::$c_arrintSuccessRevieweContractStatusTypeIds ) ) ) {

			$objPsLead = CPsLeads::createService()->fetchPsLeadById( $this->getPsLeadId(), $objDatabase );

			$objClient = CClients::createService()->fetchClientById( $objPsLead->getCid(), $objDatabase );

			$arrintCompanyStatusTypeIds = [
				CCompanyStatusType::TEST_DATABASE,
				CCompanyStatusType::SALES_DEMO,
				CCompanyStatusType::TERMINATED,
				CCompanyStatusType::TEMPLATE
			];

			if( true == $boolIsSendNewSaleEmail && ( false == valObj( $objClient, 'CClient' ) || ( true == valObj( $objClient, 'CClient' ) && false == in_array( $objClient->getCompanyStatusTypeId(), $arrintCompanyStatusTypeIds ) ) ) ) {
				$this->sendNewSaleOpportunityAddedEmail( $intUserId, $objDatabase, $intDefaultDatabaseUserTypeId );
			}
		}

		return $boolIsInserted;
	}

	public function update( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSupressLogging = false ) {

		// If status type is not contract approved, don't allow a closed on date to be set.
		// If status type is contract approved and close date is null then set close date as created_on

		if( true == $boolReturnSqlOnly ) {
			return parent::update( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( false == $boolSupressLogging ) {
			$this->handleLogging( $intUserId, $objDatabase );
		}

		if( false == parent::update( $intUserId, $objDatabase ) ) {
			return false;
		}

		// Needs to understand and refactor the code.
		// if contract status is not approved, then delete log of contract_approved record from actions
		$arrobjActions = CActions::createService()->fetchNonContractApprovedActionsByContractId( $this->getId(), $objDatabase );

		if( true == valArr( $arrobjActions ) ) {
			foreach( $arrobjActions as $objAction ) {
				if( false == $objAction->delete( $intUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		// action_datetime is greater than close date, update action_datetime if contract status is contract_approved
		if( false == $this->updateContractActions( $objDatabase ) ) return false;

		return true;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchContractDocuments( $objAdminDatabase ) {
		return CContractDocuments::createService()->fetchContractDocumentsByContractId( $this->getId(), $objAdminDatabase );
	}

	public function fetchContractProperties( $objAdminDatabase, $boolIsOnlyNonTerminatedPsProducts = false ) {
		return CContractProperties::createService()->fetchContractPropertiesByContractIdByCid( $this->getId(), $this->getCid(), $objAdminDatabase, $boolIsOnlyNonTerminatedPsProducts );
	}

	public function fetchActiveContractProperties( $objAdminDatabase, $boolIsTerminationRequested = false ) {
		return CContractProperties::createService()->fetchActiveContractPropertiesByContractId( $this->getId(), $objAdminDatabase, $boolIsTerminationRequested );
	}

	public function fetchContractPropertiesByPropertyIds( $arrintPropertyIds, $objAdminDatabase ) {
		return CContractProperties::createService()->fetchContractPropertiesByContractIdByPropertyIds( $this->getId(), $arrintPropertyIds, $objAdminDatabase );
	}

	public function fetchPilotedContractProperties( $objAdminDatabase ) {
		return CContractProperties::createService()->fetchPilotedContractPropertiesByContractId( $this->getId(), $objAdminDatabase );
	}

	public function fetchContractAgreementTemplates( $objAdminDatabase ) {
		return CContractAgreementTemplates::createService()->fetchContractAgreementTemplatesByContractId( $this->getId(), $objAdminDatabase );
	}

	public function fetchContractDocumentById( $intContractDocumentId, $objAdminDatabase ) {
		return CContractDocuments::createService()->fetchContractDocumentByIdByContractId( $intContractDocumentId, $this->getId(), $objAdminDatabase );
	}

	public function fetchClient( $objAdminDatabase ) {
		return CClients::createService()->fetchClientById( $this->getCid(), $objAdminDatabase );
	}

	public function fetchContractPostChargesData( $objAdminDatabase ) {
		return CContractProperties::createService()->fetchContractPostChargesDataByContractIdByCid( $this->getId(), $this->getCid(), $objAdminDatabase );
	}

	public function fetchNonExistingProperties( $objAdminDatabase, $boolFetchManagerialDisabled = true, $boolIsFromPackagePropertiesTab= false ) {

		$strWhereCondition = 'p.cid = ' . $this->getCid() . ' AND p.property_type_id NOT IN( ' . implode( ', ', CPropertyType::$c_arrintExcludePropertyTypeIdsForContracts ) . ' ) AND cp.property_id IS NULL AND p.termination_date IS NULL AND p.is_test = 0';
		$strExtraWhereCondition = '';

		// If true, will fetch managerial disabled otherwise will fetch all disabled along with the normal properties.
		if( true == $boolFetchManagerialDisabled ) {
			$strWhereCondition .= ' AND ( p.is_managerial = 1 OR p.is_disabled = 0 )';
		}

		$strJoin  = 'LEFT JOIN contract_properties as cp ON ( p.id = cp.property_id AND cp.termination_date IS NULL AND cp.contract_id = ' . ( int ) $this->getId() . ' )';

		if( true == $boolIsFromPackagePropertiesTab ) {
			$strJoin = 'LEFT JOIN contract_property_associations cpa ON ( p.id = cpa.property_id AND cpa.contract_id = ' . ( int ) $this->getId() . ')';
			$strExtraWhereCondition = ' AND cpa.property_id IS NULL';
			$strWhereCondition = 'p.cid = ' . $this->getCid() . ' AND p.property_type_id NOT IN( ' . implode( ', ', CPropertyType::$c_arrintExcludePropertyTypeIdsForContracts ) . ' ) AND p.termination_date IS NULL AND p.is_test = 0';
		}
		$strSql	= 'SELECT
						p.id
					FROM
						properties as p
						' . $strJoin . '
					WHERE
						' . $strWhereCondition . ' ' . $strExtraWhereCondition . ' ';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public function fetchUnAssociatedPsProducts( $objAdminDatabase ) {

		$strWhereConditionForBundle = ' pb.ps_lead_id IS NULL ';
		$strWhereConditionForProduct = ' pp.ps_lead_id IS NULL ';

		if( true == valId( $this->getPsLeadId() ) ) {
			$strWhereConditionForBundle = ' COALESCE ( pb.ps_lead_id, ' . $this->getPsLeadId() . ' ) = ' . $this->getPsLeadId();
			$strWhereConditionForProduct = ' COALESCE ( pp.ps_lead_id, ' . $this->getPsLeadId() . ' ) = ' . $this->getPsLeadId();
		}

		// Added condition for parent bundle.

		$strSql = 'SELECT
						subSql.*
					FROM
						(
							SELECT
								pp.id,
								NULL AS product_bundle_id,
								pp.is_transactional,
								pp.default_billing_frequency_id,
								0 AS is_bundle,
								pp.unit_monthly_recurring,
								pp.property_setup,
								pp.unit_transactional_average,
								pp.min_units,
								pp.max_units,
								pp.short_name,
								pp.parent_ps_product_bundle_id,
								pp.is_for_sale
							FROM
								ps_products pp
								LEFT JOIN contract_products cpp ON ( pp.id = cpp.ps_product_id AND cpp.bundle_ps_product_id IS NULL AND cpp.contract_id = ' . $this->getId() . ' )
							WHERE
								cpp.id IS NULL
								AND pp.is_for_sale = 1
								AND ' . $strWhereConditionForProduct . '
								AND pp.ps_product_display_type_id <> ' . CPsProductDisplayType::HIDE_ALL . '
							UNION
								SELECT
									NULL AS id,
									pb.id AS product_bundle_id,
									1 AS is_transactional,
									NULL AS default_billing_frequency_id,
									1 AS is_bundle,
									pb.unit_monthly_recurring,
									pb.property_setup,
									NULL AS unit_transactional_average,
									pb.min_units,
									pb.max_units,
									pb.name As short_name,
									pb.product_bundle_id AS parent_ps_product_bundle_id,
									1 AS is_for_sale
								FROM
									product_bundles pb
									LEFT JOIN contract_products cpp ON ( pb.id = cpp.bundle_ps_product_id AND cpp.contract_id = ' . $this->getId() . ' )
								WHERE
									cpp.id IS NULL
									AND ' . $strWhereConditionForBundle . '
									AND pb.ps_product_display_type_id <> ' . CPsProductDisplayType::HIDE_ALL . '
									AND pb.id NOT IN (
														SELECT
															pb_global.id AS bundle_ps_product_id
														FROM
															product_bundles pb_global
															JOIN product_bundles pb ON ( pb_global.id = pb.product_bundle_id )
														WHERE
															pb_global.ps_lead_id IS NULL
															AND pb.ps_lead_id = ' . $this->getPsLeadId() . '
														)
						) AS subSql
					ORDER BY
						lower ( subSql.short_name )';

		return CPsProducts::createService()->fetchSerialIndexedPsProducts( $strSql, $objAdminDatabase );
	}

	public function fetchContractProductByProductId( $intPsProductId, $objAdminDatabase, $boolShowBundlePsProductId = false ) {
		return \Psi\Eos\Admin\CContractProducts::createService()->fetchContractProductByContractIdByProductId( $this->getId(), $intPsProductId, $objAdminDatabase, $boolShowBundlePsProductId );
	}

	public function fetchGroupedContractProductByBundlePsProductId( $intBundlePsProductId, $objAdminDatabase ) {
		return \Psi\Eos\Admin\CContractProducts::createService()->fetchGroupedContractProductByContractIdByBundlePsProductId( $this->getId(), $intBundlePsProductId, $objAdminDatabase );
	}

	public function fetchContractProductsByBundlePsProductId( $intBundlePsProductId, $objAdminDatabase ) {
		return \Psi\Eos\Admin\CContractProducts::createService()->fetchContractProductsByContractIdByBundlePsProductId( $this->getId(), $intBundlePsProductId, $objAdminDatabase );
	}

	public function fetchContractPropertiesByPsProductId( $intPsProductId, $objAdminDatabase, $boolIsBundle = false ) {
		return CContractProperties::createService()->fetchContractPropertiesByContractIdByPsProductIdByCid( $this->getId(), $intPsProductId, $this->getCid(), $objAdminDatabase, $boolIsBundle );
	}

	public function fetchContractProducts( $objAdminDatabase, $boolIsFromAddProperty = false ) {

		$arrobjContractProducts = [];

		$arrobjContractProducts = \Psi\Eos\Admin\CContractProducts::createService()->fetchContractProductsWithProductNameByContractId( $this->getId(), $objAdminDatabase );

		if( false == $boolIsFromAddProperty && true == valArr( $arrobjContractProducts ) ) {
			$arrobjContractProducts = rekeyObjects( 'PsProductId', $arrobjContractProducts );
		}

		return $arrobjContractProducts;
	}

	public function fetchUnexceptionalContractPropertiesByPsProductIds( $arrintPsProductIds, $objAdminDatabase ) {

		return CContractProperties::createService()->fetchUnexceptionalContractPropertiesByContractIdByPsProductIds( $this->getId(), $arrintPsProductIds, $objAdminDatabase );
	}

	public function fetchAssociatedCommissionStructuresByCid( $intCid, $objDatabase ) {
		return CCommissionStructures::createService()->fetchCommissionStructuresByCidByContractId( $intCid, $this->getId(), $objDatabase );
	}

	public function fetchCommissionRateAssociationsByCommissionStructureId( $intCommissionStructureId, $objDatabase, $boolIsView = false ) {
		return CCommissionRateAssociations::createService()->fetchCommissionRateAssociationsByContractIdByCommissionStructureId( $this->getId(), $intCommissionStructureId, $objDatabase, $boolIsView );
	}

	public function fetchCommissionRateAssociations( $objDatabase, $intCommissionStructureId = NULL, $boolFetchDeletedAssociations = false ) {
		return CCommissionRateAssociations::createService()->fetchCommissionRateAssociationsByContractId( $this->getId(), $objDatabase, $intCommissionStructureId, $boolFetchDeletedAssociations );
	}

	public function loadContractPropertiesAndProductAssosciation( $arrobjContractProperties, $objDatabase ) {

		// This is a common function that can be used while assosciating products with properties.

		if( true == valArr( $arrobjContractProperties ) ) {

			$this->m_arrobjContractProperties = $arrobjContractProperties;

			$arrintPropertyIds = array_keys( rekeyObjects( 'PropertyId', $this->m_arrobjContractProperties ) );

			if( true == valArr( $arrintPropertyIds ) ) {
				$this->m_arrobjProperties = \Psi\Eos\Admin\CProperties::createService()->fetchPropertiesByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
			}

			foreach( $this->m_arrobjContractProperties as $objContractProperty ) {

				if( false == isset( $arrintContractPropertyProducts[$objContractProperty->getPsProductId()]['property_ids'] ) ) $arrintContractPropertyProducts[$objContractProperty->getPsProductId()]['property_ids'] = [];
				if( false == is_numeric( $objContractProperty->getPropertyId() ) ) {
					$arrintContractPropertyProducts[$objContractProperty->getPsProductId()]['property_ids'] = array_unique( array_merge( $arrintContractPropertyProducts[$objContractProperty->getPsProductId()]['property_ids'], $arrintPropertyIds ) );
				} else {
					$arrintContractPropertyProducts[$objContractProperty->getPsProductId()]['property_ids'][$objContractProperty->getPropertyId()] = $objContractProperty;
				}
			}

			return $arrintContractPropertyProducts;
		}
	}

	public function fetchContract( $objAdminDatabase ) {
		return CContracts::createService()->fetchContractById( $this->getId(), $objAdminDatabase );
	}

	public function fetchAccounts( $objAdminDatabase, $arrintAssociatedAccountIds = NULL ) {
		$arrintAccountTypeIds = [ CAccountType::STANDARD, CAccountType::PRIMARY, CAccountType::UTILITY_BILLING ];
		return CAccounts::createService()->fetchAccountsByCidByAccountTypeIds( $this->getCid(), $arrintAccountTypeIds, $objAdminDatabase, $boolExcludeMAAccounts = true, $arrintAssociatedAccountIds, $boolExcludeIntermediaryAccounts = false, array( $this->getCurrencyCode() ) );
	}

	public function fetchActivitiyes( $objDatabase ) {
		return CActions::createService()->fetchActionsByContractId( $this->getId(), $objDatabase );
	}

	public function fetchAllActions( $objDatabase ) {

		$arrstrActionsFilter = [
			'contract_ids'		=> [ $this->getId() ],
			'action_type_ids'	=> CActionType::$c_arrintStatusChangeActionTypeIds
		];

		return CActions::createService()->fetchPaginatedActions( $objDatabase, $arrstrActionsFilter );
	}

	public function createContract( $objAdminDatabase = NULL ) {

		$objContract = new CContract();
		$objContract->setPsLeadId( $this->getPsLeadId() );
		$objContract->setSalesEmployeeId( $this->getSalesEmployeeId() );

		if( true == valObj( $objAdminDatabase, 'CDatabase' ) ) {

			if( false == valObj( $this->m_objPsLead, 'CPsLead' ) ) {
				$this->m_objPsLead = CPsLeads::createService()->fetchPsLeadById( $this->getPsLeadId(), $objAdminDatabase );
			}

			$objContract->setCid( $this->m_objPsLead->getCid() );

		}

		return $objContract;
	}

	public function fetchActions( $objDatabase ) {
		return CActions::createService()->fetchActionsByContractIdByPsLeadId( $this->getId(), $this->getPsLeadId(), $objDatabase );
	}

	public function fetchContractDetail( $objAdminDatabase ) {
		return CContractDetails::createService()->fetchContractDetailByContractId( $this->getId(), $objAdminDatabase );
	}

	public function fetchContractWatchlists( $objDatabase ) {
		return CContractWatchlists::createService()->fetchContractWatchlistsByContractId( $this->getId(), $objDatabase );
	}

	public function fetchContractWatchlistByEmployeeId( $intEmployeeId, $objDatabase ) {
		return CContractWatchlists::createService()->fetchContractWatchlistByContractIdByEmployeeId( $this->getId(), $intEmployeeId, $objDatabase );
	}

	public function setContractPropertiesDates( $objDatabase ) {
		$strSql = 'UPDATE
						contract_properties
					SET
						training_post_date = CASE
											WHEN sub_query.contract_close_date >= DATE ( now ( ) ) THEN sub_query.contract_close_date
											ELSE DATE ( now ( ) )
											END
					FROM
						(
						SELECT
							min ( cp.close_date ) AS contract_close_date
						FROM
							contract_properties cp
						WHERE
							contract_id = ' . $this->getId() . '
							AND cp.close_date IS NOT NULL
						) AS sub_query
					WHERE
						contract_id = ' . $this->getId() . '
						AND training_post_date IS NULL;

					UPDATE
						contract_properties
					SET
						implementation_post_date = CASE
													WHEN sub_query.contract_close_date >= DATE ( now ( ) ) THEN sub_query.contract_close_date
													ELSE DATE ( now ( ) )
												END
					FROM
						(
						SELECT
							min ( cp.close_date ) AS contract_close_date
						FROM
							contract_properties cp
						WHERE
							contract_id = ' . $this->getId() . '
							AND cp.close_date IS NOT NULL
						) AS sub_query
					WHERE
						contract_id = ' . $this->getId() . '
						AND implementation_post_date IS NULL;';

		if( true == $objDatabase->execute( $strSql ) ) {
			return true;
		}

		return false;
	}

	public function setContractCloseDate( $objDatabase ) {
		$strCloseDate = NULL;

		$strSql = ' SELECT
						min ( cp.close_date ) AS close_date
					FROM
						contract_properties cp
					WHERE
						contract_id = ' . $this->getId() . '
						AND cp.close_date IS NOT NULL ';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			$strCloseDate = key( rekeyArray( 'close_date', $arrstrData ) );
		}

		return $this->setCloseDate( $strCloseDate );
	}

	public function rebuildMonthlyChangeAmount( $objDatabase ) {

		$strSql = 'UPDATE
						contract_properties
					SET
						monthly_change_amount = COALESCE( monthly_recurring_amount, 0 )
					WHERE
						original_contract_property_id IS NULL
						AND contract_id = ' . $this->getId() . ';

					UPDATE
						contract_properties cp
					SET
						monthly_change_amount = COALESCE( sub.monthly_change_amount, 0 )
					FROM
						(
						SELECT
							cp_renewal.cid,
							cp_renewal.id AS contract_property_id,
							( cp_renewal.monthly_recurring_amount - cp_original.monthly_recurring_amount ) AS monthly_change_amount,
							row_number() OVER( PARTITION BY cp_renewal.cid, cp_renewal.id ORDER BY cp_original.id ) AS rownum
						FROM
							contract_properties cp_original
							JOIN contract_properties cp_renewal ON cp_original.cid = cp_renewal.cid AND cp_original.temp_renewal_contract_property_id = cp_renewal.id
						WHERE cp_renewal.contract_id = ' . $this->getId() . '
						) AS sub
					WHERE
						cp.cid = sub.cid
						AND cp.id = sub.contract_property_id
						AND sub.rownum = 1
						AND COALESCE( cp.monthly_change_amount, 0 ) <> COALESCE( sub.monthly_change_amount, 0 )
						AND contract_id = ' . $this->getId();

		if( true == $objDatabase->execute( $strSql ) ) {
			return true;
		}

		return false;
	}

	public function rebuildTransactionalChangeAmount( $objDatabase ) {

		$strSql = '
			UPDATE
				contract_properties
			SET
				transactional_change_amount = COALESCE( transactional_amount, 0 )
			WHERE
				transactional_amount <> 0
				AND original_contract_property_id IS NULL
				AND contract_id = ' . $this->getId() . ';

			UPDATE
				contract_properties cp
			SET
				transactional_change_amount = COALESCE( sub.transactional_change_amount, 0 )
			FROM
				(
				SELECT
					cp_renewal.cid,
					cp_renewal.id AS contract_property_id,
					( cp_renewal.transactional_amount - cp_original.transactional_amount ) AS transactional_change_amount,
					row_number() OVER( PARTITION BY cp_renewal.cid, cp_renewal.id ORDER BY cp_original.id ) AS rownum
				FROM
					contract_properties cp_original
					JOIN contract_properties cp_renewal ON cp_original.cid = cp_renewal.cid AND cp_original.temp_renewal_contract_property_id = cp_renewal.id
				WHERE cp_renewal.contract_id = ' . $this->getId() . '
				) AS sub
			WHERE
				cp.cid = sub.cid
				AND cp.id = sub.contract_property_id
				AND sub.rownum = 1
				AND COALESCE( cp.transactional_change_amount, 0 ) <> COALESCE( sub.transactional_change_amount, 0 )
				AND contract_id = ' . $this->getId();

		if( true == $objDatabase->execute( $strSql ) ) {
			return true;
		}

		return false;
	}

	public function rebuildCurrencyCode( $intUserId, $objAdminDatabase ) {

		if( !valId( $intUserId ) || !valId( $this->getId() ) || !valStr( $this->getCurrencyCode() ) ) {
			return;
		}

		$strSql = 'UPDATE
						contract_products
					SET
						currency_code = \'' . $this->getCurrencyCode() . '\',
						updated_by = ' . $intUserId . ',
						updated_on = NOW()
					WHERE
						contract_id = ' . $this->getId() . '
						AND currency_code <> \'' . $this->getCurrencyCode() . '\';

					UPDATE
						contract_properties
					SET
						currency_code = \'' . $this->getCurrencyCode() . '\',
						updated_by = ' . $intUserId . ',
						updated_on = NOW()
					WHERE
						contract_id = ' . $this->getId() . '
						AND currency_code <> \'' . $this->getCurrencyCode() . '\';';

		$objAdminDatabase->execute( $strSql );
	}

	// If all contract_properties are terminated then min termination date of contract properties will set as contract termination date to show on UI.

	public function setContractTerminationDate( $objDatabase ) {

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) return NULL;

		$this->setTerminationDate( NULL );

		$strSql = ' SELECT
						COUNT( cp.contract_id ) as cnt_contract_propties,
							 SUM( CASE
								WHEN cp.termination_date IS NOT NULL THEN 1
								ELSE 0
								END ) as terminated_contract_propties,
							 MIN(cp.termination_date) as termination_date
					FROM
						contract_properties cp
					WHERE
						cp.contract_id = ' . $this->getId();

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrstrData ) && $arrstrData[0]['cnt_contract_propties'] == $arrstrData[0]['terminated_contract_propties'] ) {
			$strTerminationDate = $arrstrData[0]['termination_date'];
			$this->setContractTerminationRequestId( $strTerminationDate );
		}
	}

	// If all contract_properties are requested to terminate then min contract_termination_request_id of contract properties will set as contract_termination_request_id.

	public function setMinContractTerminationRequestId( $objDatabase ) {

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) return NULL;

		$this->setContractTerminationRequestId( NULL );

		$strSql = ' SELECT
						COUNT( cp.contract_id ) as cnt_contract_propties,
							SUM(CASE
								WHEN cp.contract_termination_request_id IS NOT NULL THEN 1
								ELSE 0
								END) as termination_requested_contract_propties,
							MIN(cp.contract_termination_request_id) as termination_request_id
					FROM contract_properties cp
					WHERE cp.contract_id = ' . $this->getId();

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrstrData ) && $arrstrData[0]['cnt_contract_propties'] == $arrstrData[0]['termination_requested_contract_propties'] ) {
			$intContractTerminationRequestId = $arrstrData[0]['termination_request_id'];
			$this->setContractTerminationRequestId( $intContractTerminationRequestId );
		}
	}

	// If all contract_properties are marked as transferred properties.

	public function setTransferredProperties( $objDatabase ) {

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) return NULL;

		$strSql = ' SELECT
						SUM(CASE
								WHEN cp.commission_bucket_id <> ' . CCommissionBucket::TRANSFER_PROPERTY . '
								THEN 1
								ELSE 0
							END) as cnt_non_transferred_properties,
						SUM(CASE
								WHEN cp.commission_bucket_id = ' . CCommissionBucket::TRANSFER_PROPERTY . '
								THEN 1
								ELSE 0
							END) as cnt_transferred_properties
					FROM contract_properties cp
					WHERE
						cp.contract_termination_request_id IS NULL
						AND cp.commission_bucket_id IS NOT NULL
						AND cp.contract_id = ' . $this->getId();

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			( 0 < $arrstrData[0]['cnt_non_transferred_properties'] ) ?$this->setIsNonTransferredProperties( true ):$this->setIsNonTransferredProperties( false );
			( 0 < $arrstrData[0]['cnt_transferred_properties'] ) ?$this->setIsTransferredProperties( true ):$this->setIsTransferredProperties( false );
		}

	}

	public function updateContractActions( $objDatabase ) {

		$strSql = 'UPDATE
						actions a SET action_datetime = sub.close_date
				FROM
					( SELECT
								a.id as action_id,
								close_date
						FROM
								actions a,
								contracts co,
									(
										SELECT
										contract_id,
										MIN ( close_date ) AS close_date
										FROM
										contract_properties
										WHERE
										close_date IS NOT NULL
										GROUP BY
										contract_id
									) AS sub_cp
						WHERE
								co.id = a.contract_id
								AND co.id = sub_cp.contract_id
								AND a.contract_id = ' . $this->getId() . '
								AND co.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
								AND a.action_datetime > sub_cp.close_date
								AND sub_cp.close_date IS NOT NULL ) As sub
				WHERE action_id = a.id';

		if( true == $objDatabase->execute( $strSql ) ) {
			return true;
		}

		return false;
	}

	public function updateImplementationWeightScore( $intCurrentUserId, $objDatabase ) {

		$objDataset = $objDatabase->createDataset();

		$intContractId 	= $this->getId();

		$strSql = 'SELECT * FROM update_implementation_weight_score( ' . ( int ) $intContractId . ',' . ( int ) $intCurrentUserId . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update implementation weight score. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function fetchPsProductIds( $objAdminDatabase ) {
		return CContractProperties::createService()->fetchPsProductIdsByContractId( $this->getId(), $objAdminDatabase );
	}

	public function fetchContractPropertyProductAssociations( $objAdminDatabase ) {
		return CContractProperties::createService()->fetchContractPropertyProductAssociationsByContractId( $this->getId(), $objAdminDatabase );
	}

	public function fetchAllPsProductsWithBundles( $objAdminDatabase ) {
		return CPsProducts::createService()->fetchAllPsProductsWithBundlesByContractId( $this->getId(), $objAdminDatabase );
	}

	public function fetchAllProperties( $objAdminDatabase, $boolFetchAllDisabled = false ) {
		return \Psi\Eos\Admin\CProperties::createService()->fetchAllPropertiesByContractId( $this->getId(), $objAdminDatabase, $boolFetchAllDisabled, $this->getCid() );
	}

	public function fetchNonImplementedContractProperties( $objAdminDatabase ) {
		return CContractProperties::createService()->fetchNonImplementedContractPropertiesByContractId( $this->getId(), $objAdminDatabase );
	}

	// TODO:: This method is non-static but called statically. Need to refactor.

	public function resetRenewedContract( $arrintOldRenewedContractIds, $objContract, $objAdminDatabase, $intUserId = NULL ) {

		if( true == is_null( $intUserId ) ) {
			$this->addErrorMsgs( $objAction->getErrorMsgs() );
			exit;
		}

		if( false == valArr( $arrintOldRenewedContractIds ) ) return false;
		if( false == valObj( $objContract, 'CContract' ) ) return false;

		$arrobjContractDetails		= NULL;
		$arrintPercentageRenewed	= [];

		$arrintActiveContractPropertiesCounts = [];

		$arrintContractPropertiesCounts = CContractProperties::createService()->fetchActiveContractPropertiesCountByContractIds( $arrintOldRenewedContractIds, $objAdminDatabase );

		if( true == valArr( $arrintContractPropertiesCounts ) ) {
			$arrintActiveContractPropertiesCounts = $arrintContractPropertiesCounts[1];

			foreach( $arrintActiveContractPropertiesCounts as $arrintActiveContractPropertiesCount ) {
				$arrintActiveContractPropertiesTotalCounts[$arrintActiveContractPropertiesCount['contract_id']] = $arrintActiveContractPropertiesCount['contract_property_count'];
			}
		}

		$arrobjContractDetails = CContractDetails::createService()->fetchContractDetailsByContractIds( $arrintOldRenewedContractIds, $objAdminDatabase );

		$strSqlWhereContdtion = 'WHERE id IN( SELECT
											original_contract_property_id
										FROM
											contract_properties
										WHERE termination_date IS NULL AND contract_id = ' . $objContract->getId() . ' )';

		$intRenewedPropertiesCount = CContractProperties::createService()->fetchRowCount( $strSqlWhereContdtion, 'contract_properties', $objAdminDatabase );

		if( true == valArr( $arrobjContractDetails ) ) {
			foreach( $arrobjContractDetails as $objContractDetail ) {
				if( false == is_null( $intRenewedPropertiesCount ) && true == valArr( $arrintActiveContractPropertiesTotalCounts ) ) {
					$intPercentageRenewed = ( $intRenewedPropertiesCount / $arrintActiveContractPropertiesTotalCounts[$objContractDetail->getContractId()] ) * 100;
					$arrintPercentageRenewed[$objContractDetail->getContractId()] = $objContractDetail->getPercentageRenewed() - $intPercentageRenewed;
				}
			}
		}
		$strSql .= 'UPDATE
						contract_properties
					SET
						temp_renewal_contract_property_id = NULL,
						updated_by = ' . ( int ) $intUserId . ',
						updated_on = NOW()
					WHERE
						(
							original_contract_property_id IN (
																SELECT
																	original_contract_property_id
																FROM
																	contract_properties
																WHERE
																	contract_id = ' . $objContract->getId() . '
															)
							OR
							id IN (
									SELECT
										original_contract_property_id
									FROM
										contract_properties
									WHERE
										contract_id = ' . $objContract->getId() . '
									)
						)
						AND contract_id IN ( ' . implode( ',', $arrintOldRenewedContractIds ) . ' ); ';

		foreach( $arrintOldRenewedContractIds as $intOldRenewedContractId ) {
			$strSql .= 'UPDATE
							contract_details
						SET
							percentage_renewed =
							CASE
								WHEN ' . $arrintPercentageRenewed[$intOldRenewedContractId] . '>= 0 THEN '
			           . $arrintPercentageRenewed[$intOldRenewedContractId] . '
								ELSE 0
							END ,
							updated_by =' . ( int ) $intUserId . ',
							updated_on = NOW()
						WHERE contract_id =' . ( int ) $intOldRenewedContractId . ';';
		}

		$strSql .= 'DELETE
					FROM
						contract_renewals
					WHERE
						renewal_contract_id =' . $objContract->getId() . ';';

		$arrstrData = fetchData( $strSql, $objAdminDatabase );

		foreach( $arrintOldRenewedContractIds as $intOldRenewedContractId ) {
			$objAction = new CAction();
			$objAction->setContractId( $intOldRenewedContractId );
			$objAction->setActionTypeId( CActionType::NOTE );
			$objAction->setActionDescription( 'Contract ' . ( int ) $intOldRenewedContractId . ' re-activated after cancelling renewed opportunity ' . $objContract->getId() );

			if( false == $objAction->insert( $intUserId, $objAdminDatabase ) ) {
				$this->addErrorMsgs( $objAction->getErrorMsgs() );
				exit;
			}
		}

		$objAction = new CAction();
		$objAction->setContractId( $objContract->getId() );
		$objAction->setActionTypeId( CActionType::NOTE );
		$objAction->setActionDescription( 'Renewed opportunity cancelled, property product association related to opportunity ' . $objContract->getId() . ' are deleted.' );

		if( false == $objAction->insert( $intUserId, $objAdminDatabase ) ) {
			$this->addErrorMsgs( $objAction->getErrorMsgs() );
			exit;
		}

		return true;
	}

	public function updateContractLikelihoodPercentage( $intCurrentUserId, $objAdminDatabase, $boolIsFromContractInfo = false ) {

		$intTotalAcv					= 0;
		$intTotalLikelihood 			= 0;
		$intTotalActivityAdjustment		= 0;
		$intDaysFromLastStatusUpdated	= 0;
		$intLikelihoodPercentagePerAcv 	= 0;
		$intLikelihoodPercentagePerDays = 0;
		$intCId							= $this->getCid();
		$intContractId					= $this->getId();
		$strSql							= '';

		if( CContractStatustype::CONTRACT_APPROVED == $this->getContractStatusTypeId() ) {
			$intTotalLikelihood = self::MAX_DEFAULT_LIKELIHOOD;

		} elseif( false == in_array( $this->getContractStatusTypeId(), CContractStatustype::$c_arrintDeactivatedContractStatusTypeIds ) ) {

			if( true == $boolIsFromContractInfo ) {
				$objContract = CContracts::createService()->fetchTotalNewACVForOpportunities( $objAdminDatabase, $intContractId );
				$this->setTotalNewAcv( $objContract->getTotalNewAcv() );
			}

			$objAction = CActions::createService()->fetchActionForlatestContactstatusChangedByContractIdByPsLeadId( $intContractId, $this->getPsLeadId(), $objAdminDatabase );

			$strCurrentDate = new DateTime( date( 'Y/m/d' ) );

			if( false == is_null( $objAction ) ) {
				$strLastStatusChangeDate		= new DateTime( $objAction->getActionDatetime() );
				$intDaysFromLastStatusUpdated	= date_diff( $strCurrentDate, $strLastStatusChangeDate )->days;

				if( false == is_null( $intDaysFromLastStatusUpdated ) ) {
					$arrstrActions = CActions::createService()->fetchActionsByActionTypeIdsByContractId( $intContractId, $intDaysFromLastStatusUpdated, $objAdminDatabase );

					if( true == valArr( $arrstrActions ) ) {
						foreach( $arrstrActions as $arrstrAction ) {

							$objContractLikelihoodActionSetting = CContractLikelihoodActionSettings::createService()->fetchContractLikelihoodActionSettingsByActionTypeIdByActionResultId( $arrstrAction, $objAdminDatabase );

							if( true == valObj( $objContractLikelihoodActionSetting, 'CContractLikelihoodActionSetting' ) ) {
								if( 1 == $objContractLikelihoodActionSetting->getAdjustmentStatus() ) {
									$intTotalActivityAdjustment += $objContractLikelihoodActionSetting->getPercentageAdjustment();
								} else {
									$intTotalActivityAdjustment -= $objContractLikelihoodActionSetting->getPercentageAdjustment();
								}
							}
						}
					}
				}
			}

			$intTotalAcv	= ( true == is_null( $this->getTotalNewAcv() ) ) ? 0 : $this->getTotalNewAcv();
			$intTotalLikelihood = 0;

			$objContractLikelihoodSetting = CContractLikelihoodSettings::createService()->fetchLikelihoodSettingsByContractStatusTypeId( $this->getContractStatusTypeId(), $objAdminDatabase );

			if( true == valObj( $objContractLikelihoodSetting, 'CContractLikelihoodSetting' ) ) {
				$intWatingPeriod 	= $objContractLikelihoodSetting->getDaysAfterStart();

				if( $intTotalAcv >= $objContractLikelihoodSetting->getAcvLimitPerAdditionalDays() && 0 < $objContractLikelihoodSetting->getAcvLimitPerAdditionalDays() ) {
					$intWatingPeriod = $objContractLikelihoodSetting->getDaysAfterStart() + ( floor( $intTotalAcv / $objContractLikelihoodSetting->getAcvLimitPerAdditionalDays() ) * $objContractLikelihoodSetting->getAdditionalDaysAfterStart() );
				}

				if( $intWatingPeriod <= $intDaysFromLastStatusUpdated ) {

					if( 0 < $objContractLikelihoodSetting->getAcvLimitPerAdditionalDays() ) {
						$intLikelihoodPercentagePerAcv = ( floor( $intTotalAcv / $objContractLikelihoodSetting->getAcvLimitPerAdditionalDays() ) );
					}

					if( 0 < $objContractLikelihoodSetting->getDaysPerLossPercentage() ) {
						$intLikelihoodPercentagePerDays = ( floor( ( $intDaysFromLastStatusUpdated - $intWatingPeriod ) / $objContractLikelihoodSetting->getDaysPerLossPercentage() ) * $objContractLikelihoodSetting->getLossPercentage() );
					}

					if( 0 < $intLikelihoodPercentagePerAcv ) {
						$intLikelihoodPercentagePerDays *= $intLikelihoodPercentagePerAcv;
					}
				}
				$intTotalLikelihood = $objContractLikelihoodSetting->getBasePercentage();
			}

			$intTotalLikelihood = ( $intTotalLikelihood + $intTotalActivityAdjustment ) - ( $intLikelihoodPercentagePerDays );

			if( $intTotalLikelihood > self::MAX_DEFAULT_LIKELIHOOD ) {
				$intTotalLikelihood = self::MAX_DEFAULT_LIKELIHOOD;
			} elseif( $intTotalLikelihood < 0 ) {
				$intTotalLikelihood = 0;
			}
		}

		$this->setPercentCloseLikelihood( $intTotalLikelihood );

		$strSql .= 'UPDATE
						contracts
					SET
						percent_close_likelihood=' . ( int ) $intTotalLikelihood . ',
						updated_by =' . ( int ) $intCurrentUserId . ',
						updated_on = NOW()
					WHERE id=' . $this->getId() . ';';

		if( false == $boolIsFromContractInfo ) {
			return $strSql;
		}

		return fetchData( $strSql, $objAdminDatabase );

	}

	public function deleteContractPropertiesByContractId( $objAdminDatabase ) {
		$strSql = 'SELECT cp.id FROM contract_properties as cp where cp.contract_id IN ( ' . $this->getId() . ' ) AND cp.renewal_contract_property_id IS NULL AND cp.is_last_contract_record = true;';
		$arrmixData = fetchData( $strSql, $objAdminDatabase );

		if( true == valArr( $arrmixData ) ) {
			$strDeleteContractPropertyPricings = 'DELETE FROM contract_property_pricings where contract_property_id IN ( ' . implode( ',', array_column( $arrmixData, 'id' ) ) . ' );';
			fetchData( $strDeleteContractPropertyPricings, $objAdminDatabase );
		}

		$strDeleteContractProperties = 'DELETE FROM contract_properties as cp where cp.contract_id IN ( ' . $this->getId() . ' ) AND cp.renewal_contract_property_id IS NULL AND cp.is_last_contract_record = true;';
		fetchData( $strDeleteContractProperties, $objAdminDatabase );
	}

	public function validateNotAssociateDuplicateContractProperties( $arrobjContractProperties, $objAdminDatabase, $intExcludeContractId = NULL ) {

		if( true == valArr( $arrobjContractProperties ) ) {
			$arrintPsProductIds	= array_filter( array_keys( rekeyObjects( 'PsProductId', $arrobjContractProperties ) ) );
			$arrintPropertyIds	= array_filter( array_keys( rekeyObjects( 'PropertyId', $arrobjContractProperties ) ) );
		}

		$arrobjActiveContractProperties	= CContractProperties::createService()->fetchNotAssociateActiveContractPropertiesByPropertyIdsByPsProductIdsByCid( $arrintPropertyIds, $arrintPsProductIds, $this->getCid(), $objAdminDatabase, $intExcludeContractId );

		$strMsg = '';
		if( true == valArr( $arrobjActiveContractProperties ) ) {
			foreach( $arrobjActiveContractProperties as $objActiveContractProperty ) {

				$strBundleName = ( false == is_null( $objActiveContractProperty->getBundleProductName() ) ) ? ' in bundle ' . $objActiveContractProperty->getBundleProductName() : '';
				$strMsg = $strMsg . $objActiveContractProperty->getProductName() . ' product is already associated with active contract ' . $objActiveContractProperty->getContractId() . $strBundleName . '.';
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', $strMsg ) );
			return false;
		}

		return true;
	}

	public function validatePropertyProductAssignment( $arrobjContractProperties, $intCid, $objAdminDatabase, $boolCheckFromSameContract = true ) {

		$arrboolProductAssignmentError['is_associate_in_same_contract']			= false;
		$arrboolProductAssignmentError['is_associate_in_different_contract']	= false;

		// checking assignment in different contracts
		$arrobjActiveContractProperties	= CContractProperties::createService()->fetchAssociatedActiveContractPropertiesByContractPropertyIds( $arrobjContractProperties, $intCid, $objAdminDatabase, $this->getId() );

		if( true == valArr( $arrobjActiveContractProperties ) ) {

			foreach( $arrobjActiveContractProperties as $objActiveContractProperty ) {

				$strProductBundleName = ( false == is_null( $objActiveContractProperty->getBundleProductName() ) ) ? ' in Product bundle ' . $objActiveContractProperty->getBundleProductName() : '';
				$strMsg = $strMsg . $objActiveContractProperty->getProductName() . ' product is already associated with ' . $objActiveContractProperty->getPropertyName() . ' of active contract ' . $objActiveContractProperty->getContractId() . $strProductBundleName . '.';

			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', $strMsg ) );

			$arrboolProductAssignmentError['is_associate_in_different_contract'] = true;
		}

		// checking assignment in Same Contracts
		if( true == $boolCheckFromSameContract ) {

			$arrobjActiveContractProperties = CContractProperties::createService()->fetchAssociatedActiveContractPropertiesByContractPropertyIds( $arrobjContractProperties, $intCid, $objAdminDatabase, $this->getId(), false );

			if( true == valArr( $arrobjActiveContractProperties ) ) {

				foreach( $arrobjActiveContractProperties as $objActiveContractProperty ) {

					$strProductBundleName = ( false == is_null( $objActiveContractProperty->getBundleProductName() ) ) ? ' in Product bundle ' . $objActiveContractProperty->getBundleProductName() : '';
					$strMsg               = $strMsg . $objActiveContractProperty->getProductName() . ' product is already associated with ' . $objActiveContractProperty->getPropertyName() . ' of active contract ' . $objActiveContractProperty->getContractId() . $strProductBundleName . '.';

				}

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', $strMsg ) );

				$arrboolProductAssignmentError['is_associate_in_same_contract'] = true;
			}
		}

		return $arrboolProductAssignmentError;
	}

	public function fetchActiveContractPropertiesByPropertyIds( $arrintPropertyIds, $objAdminDatabase, $arrmixContractTerminationData ) {
		return CContractProperties::createService()->fetchActiveContractPropertiesByContractIdByPropertyIds( $this->getId(), $arrintPropertyIds, $objAdminDatabase, $arrmixContractTerminationData );
	}

	// Set task details and create a new sale task for given product.

	public function createNewSaleTask( $objTask, $arrobjTaskWatchLists, $objContract, $objUser, $objAdminDatabase, $arrobjTaskAttachments = NULL, $boolFromEntrata, $objObjectStorageGateway = NULL ) {

		$boolIsValid   = true;
		$boolIsSuccess = true;
		$intUserId = $boolFromEntrata ? ENTRATA_USER_ID : $objUser->getId();

		switch( NULL ) {
			default:
				$boolIsValid &= $objTask->validate( VALIDATE_INSERT, true, $objAdminDatabase, $objUser );

				// Task Attachments.
				foreach( ( array ) $arrobjTaskAttachments as $objTaskAttachment ) {
					if( valObj( $objTaskAttachment, 'CTaskAttachment' ) && false == $objTaskAttachment->validate( VALIDATE_INSERT, $objAdminDatabase ) ) {
						$this->addErrorMsgs( $objTaskAttachment->getErrorMsgs() );
						$boolIsValid &= false;
					}
				}

				// Stake Holders.
				foreach( ( array ) $arrobjTaskWatchLists as $objTaskWatchList ) {
					if( valObj( $objTaskWatchList, 'CTaskAssignment' ) ) {
						if( false == $objTaskWatchList->validate( VALIDATE_INSERT ) ) {
							$this->addErrorMsgs( $objTaskWatchList->getErrorMsgs() );
							$boolIsValid &= false;
						}
					}
				}

				if( !$boolIsValid ) {
					$this->addErrorMsgs( 'Error in task, task attachment or stack holder allocation.' );
					return false;
				}

				if( !$objTask->insert( $intUserId, $objAdminDatabase ) ) {
					$this->addErrorMsgs( 'Error in task creation.' );
					$boolIsSuccess &= false;

					return $boolIsSuccess;
				}

				foreach( ( array ) $arrobjTaskWatchLists as $objTaskWatchList ) {
					$objTaskWatchList->setTaskId( $objTask->getId() );
					if( false == $objTaskWatchList->insert( $intUserId, $objAdminDatabase ) ) {
						$this->addErrorMsgs( 'Error in stack holder allocation.' );
						$boolIsSuccess &= false;

						return $boolIsSuccess;
					}
				}

				// Assigning client to the task..
				if( $boolIsSuccess && is_numeric( $objContract->getCid() ) ) {
					$objTaskCompany = $objTask->createTaskCompany();
					$objTaskCompany->setCid( $objContract->getCid() );
					$objTaskCompany->setLatestUpdate( 'New task added.' );
					if( !$objTaskCompany->insert( $intUserId, $objAdminDatabase ) ) {
						$this->addErrorMsgs( 'Error in assigning client to the task.' );
					}
				}

				if( valArr( $arrobjTaskAttachments ) ) {

					foreach( $arrobjTaskAttachments as $objTaskAttachment ) {

						if( false == is_null( $objTaskAttachment->getUrl() ) && 0 < strlen( $objTaskAttachment->getUrl() ) ) {
							$objTaskAttachment->setTaskId( $objTask->getId() );

							$intId = $objTaskAttachment->fetchNextId( $objAdminDatabase );
							$objTaskAttachment->setUrl( $intId . '_' . CFileUpload::cleanFilename( $objTaskAttachment->getFileName() ) );

							$strFileTmpPath = NULL;

							$objPsDocument = CPsDocuments::createService()->fetchPsDocumentByFileNameByPsLeadId( $objTaskAttachment->getFileName(), $objContract->getPsLeadId(), $objAdminDatabase );

							if( true == valObj( $objPsDocument, 'CPsDocument' ) ) {
								$strFileTmpPath = $objPsDocument->downloadObject( $objObjectStorageGateway, $objTaskAttachment->getFileName(), 'attachment', true );
							}

							$objTaskAttachment->setFileTmpName( $strFileTmpPath );
							$objTaskAttachment->setObjectStorageGateway( $objObjectStorageGateway );

							if( valStr( $objTaskAttachment->getFileTmpName() ) && !$objTaskAttachment->insert( $intUserId, $objAdminDatabase, true ) ) {
								$this->addErrorMsgs( 'Error in task attachment.' );
								$boolIsSuccess &= false;

								return $boolIsSuccess;
							}
						}
					}
				}
				$objTask->setCreatedBy( $intUserId );
				if( $boolFromEntrata ) {
					$objTask->sendEmailAlert( $objUser, $objAdminDatabase, $arrobjOldTaskCompanies = NULL, $boolIsRequestFromEntrata = true, $boolIsFromUpdateTask = false, $strTaskTypePriority = NULL, $strSecureBaseUrl = NULL );
				} else {
					$objTask->sendEmailAlert( $objUser, $objAdminDatabase );
				}

				return $boolIsSuccess;
		}
	}

	public function sendNewSaleEmail( $objPsLead, $objCurrency, $objUser, $objAdminDatabase, $objEmailDatabase, $boolFromEntrata = true, $objObjectStorageGateway = NULL ) {

		$arrobjPsProducts		= CPsProducts::createService()->fetchAllPsProductsAndBundle( $objAdminDatabase );
		$arrobjEmployees		= CEmployees::createService()->fetchAllEmployees( $objAdminDatabase );
		$arrobjPsProductOptions	= CPsProductOptions::createService()->fetchAllPsProductOptions( $objAdminDatabase );
		$fltPreviousClientAcv = CContracts::createService()->fetchClientAcvByCidByContractid( $this->getCid(), $this->getId(), $objAdminDatabase );

		$objContractDetail = CContractDetails::createService()->fetchContractDetailByContractId( $this->getId(), $objAdminDatabase );

		$this->m_objPsLeadDetail	= CPsLeadDetails::createService()->fetchPsLeadDetail( 'SELECT * FROM ps_lead_details WHERE cid = ' . $this->getCid(), $objAdminDatabase );
		$this->m_objActiveContract	= CContracts::createService()->fetchCustomActiveContractById( $this->getId(), $objAdminDatabase );

		// Checking For New Big Sale Emails
		if( !$boolFromEntrata ) {
			$boolIsNewBigSaleEmail = false;
			if( self::BIG_SALE_EMAIL_ACV_LIMIT <= $this->m_objActiveContract->getAcvAmount() ) {
				$boolIsNewBigSaleEmail = true;
			}
		}

		$arrobjContractProperties = [];

		if( valObj( $this->m_objActiveContract, 'CContract' ) ) {
			$arrobjContractProperties = CContractProperties::createService()->fetchContractPropertiesByContractIdByCid( $this->getId(), $this->getCid(), $objAdminDatabase );
		}

		$arrstrContractPropertyNames = $arrintSoldContractProductIds = $arrobjSoldContractProducts = $arrintPsProductsWithProperties	= $arrobjSoldContractProductOptions	= [];

		$boolIsBundle = 0;
		if( valArr( $arrobjContractProperties ) ) {
			foreach( $arrobjContractProperties as $objContractProperty ) {

				$arrintPsProductsWithProperties[$objContractProperty->getPsProductId()][$objContractProperty->getPropertyId()] = $objContractProperty;
				$arrstrContractPropertyNames[$objContractProperty->getPropertyId()] = $objContractProperty->getPropertyName();
				$arrintSoldContractProductIds[$objContractProperty->getPsProductId()] = $objContractProperty->getPsProductId();

				if( true != is_null( $objContractProperty->getBundlePsProductId() ) ) {
					$boolIsBundle = 1;
				}
			}
		}

		$this->m_arrobjContractProducts = CContractProducts::createService()->fetchContractProductsKeyedByPsProductIdByContractId( $this->getId(), $objAdminDatabase );

		if( valArr( $this->m_arrobjContractProducts ) ) {
			foreach( $this->m_arrobjContractProducts as $objContractProduct ) {
				if( array_key_exists( $objContractProduct->getPsProductId(), $arrintSoldContractProductIds ) ) {
					array_push( $arrobjSoldContractProducts, $objContractProduct );
				}
			}
		}

		$intPropertycount = 0;
		$strPropertyNames = '';
		foreach( $arrstrContractPropertyNames as $strContractPropertyName ) {
			if( is_null( $strContractPropertyName ) ) {
				continue;
			}
			$intPropertycount++;
			$strPropertyNames .= '<br/>' . $intPropertycount . '. ' . $strContractPropertyName;
		}

		if( valArr( $arrobjSoldContractProducts ) ) {
			$arrobjSoldContractProducts = rekeyObjects( 'PsProductId', $arrobjSoldContractProducts );
			$arrobjContractProductStakeholders = CContractProductStakeholders::createService()->fetchActiveContractProductNewSalesStakeholdersByProductIds( array_keys( $arrobjSoldContractProducts ), $objAdminDatabase );

			if( valArr( $arrobjContractProductStakeholders ) ) {
				$arrintUserIds = array_keys( rekeyObjects( 'userId', $arrobjContractProductStakeholders ) );
				$arrobjContractProductStakeholders = rekeyObjects( 'productId', $arrobjContractProductStakeholders, true );
			}
		}

		if( valArr( $arrintUserIds ) ) {
			$arrobjUsers = CUsers::createService()->fetchUsersByIds( $arrintUserIds, $objAdminDatabase );
		}

		$this->m_arrobjContractProductOptions = CContractProductOptions::createService()->fetchContractProductOptionsByPsProductOptionIdByContractId( $this->getId(), $objAdminDatabase );

		if( valArr( $this->m_arrobjContractProductOptions ) ) {
			$arrobjSoldContractProductOptions = rekeyObjects( 'PsProductOptionId', $this->m_arrobjContractProductOptions );
		}

		$this->m_arrobjCommissionBuckets	= CCommissionBuckets::createService()->fetchPublishedCommissionBuckets( $objAdminDatabase );
		$strTld = ( 'production' == CONFIG_ENVIRONMENT ) ? 'entrata.com' : CONFIG_COMPANY_BASE_DOMAIN;
		$strBaseUri = CONFIG_SECURE_HOST_PREFIX . 'clientadmin.' . $strTld;

		$arrstrToEmailAddress = [];

		// Generate a task of Accounting type
		$this->m_objTask = new CTask();
		$this->m_objTask->setDefaults();

		$this->m_objTask->setTaskTypeId( CTaskType::ACCOUNTING );
		$this->m_objTask->setUserId( CUser::ID_BILLING_BILLING );
		$this->m_objTask->setTitle( 'New Sale: ' . $objPsLead->getCompanyName() . '(Opportunity Id: ' . $this->getId() . ')' );

		$arrmixTemplateParameters = [];

		$arrmixTemplateParameters['ps_lead']									= $objPsLead;
		$arrmixTemplateParameters['contract']									= $this;
		$arrmixTemplateParameters['contract_detail']							= $objContractDetail;
		$arrmixTemplateParameters['active_contract']							= $this->m_objActiveContract;
		$arrmixTemplateParameters['ps_lead_detail']								= $this->m_objPsLeadDetail;
		$arrmixTemplateParameters['ps_products']								= $arrobjPsProducts;
		$arrmixTemplateParameters['ps_product_options']							= $arrobjPsProductOptions;
		$arrmixTemplateParameters['employees']									= $arrobjEmployees;
		$arrmixTemplateParameters['sold_contract_products']						= $arrobjSoldContractProducts;
		$arrmixTemplateParameters['sold_contract_product_options']				= $arrobjSoldContractProductOptions;
		$arrmixTemplateParameters['products_properties']						= $arrintPsProductsWithProperties;
		$arrmixTemplateParameters['contract_property_names']					= $arrstrContractPropertyNames;
		$arrmixTemplateParameters['commission_buckets']							= $this->m_arrobjCommissionBuckets;
		$arrmixTemplateParameters['contract_products']							= $this->m_arrobjContractProducts;
		$arrmixTemplateParameters['users']										= $arrobjUsers;
		$arrmixTemplateParameters['new_sale_email_users']						= $arrintUserIds;
		$arrmixTemplateParameters['previous_client_acv']						= $fltPreviousClientAcv;
		$arrmixTemplateParameters['transfer_property_acv_amount']				= $this->m_objActiveContract->getTransferPropertyAcvAmount();
		$arrmixTemplateParameters['CONTRACT_STATUS_TYPE_CONTRACT_APPROVED']		= CContractStatusType::CONTRACT_APPROVED;
		$arrmixTemplateParameters['PS_PRODUCT_RESIDENT_UTILITY']				= CPsProduct::RESIDENT_UTILITY;
		$arrmixTemplateParameters['PS_PRODUCT_RESIDENT_INSURE']					= CPsProduct::RESIDENT_INSURE;
		$arrmixTemplateParameters['PS_PRODUCT_RESIDENT_VERIFY']					= CPsProduct::RESIDENT_VERIFY;
		$arrmixTemplateParameters['PS_PRODUCT_UTILITY_EXPENSE_MANAGEMENT']		= CPsProduct::UTILITY_EXPENSE_MANAGEMENT;
		$arrmixTemplateParameters['PS_PRODUCT_INVOICE_PROCESSING']				= CPsProduct::INVOICE_PROCESSING;
		$arrmixTemplateParameters['PS_PRODUCT_ENTRATA_MILITARY']				= CPsProductOption::PS_PRODUCT_ENTRATA_MILITARY;
		$arrmixTemplateParameters['PS_PRODUCT_ENTRATA_STUDENT']					= CPsProductOption::PS_PRODUCT_ENTRATA_STUDENT;
		$arrmixTemplateParameters['base_uri']									= $strBaseUri;
		$arrmixTemplateParameters['is_bundle']									= $boolIsBundle;
		$arrmixTemplateParameters['contract_product_stakeholders']				= $arrobjContractProductStakeholders;
		$arrmixTemplateParameters['currency_symbol']				            = ( true == valObj( $objCurrency, 'CCurrency' ) ? $objCurrency->getSymbol() : CCurrency::SYMBOL_USD );

		$objRenderTemplate = new CRenderTemplate( 'sales/contracts/view_sale_email_contents.tpl', NULL, false, $arrmixTemplateParameters );

		$strHtmlContent 	= $objRenderTemplate->fetchTemplate( PATH_INTERFACES_CLIENT_ADMIN . 'sales/contracts/view_sale_email_contents.tpl' );
		$this->m_objTask->setDescription( $strHtmlContent );

		// Handle Creation of stakeholders
		$this->m_arrobjTaskWatchLists = [];

		$intUserId = $boolFromEntrata ? ENTRATA_USER_ID : $objUser->getId();

		// Adding task created_by user as stakeholder
		$objTaskWatchList	= $this->m_objTask->createTaskWatchList();
		$objTaskWatchList->setUserId( $intUserId );
		array_push( $this->m_arrobjTaskWatchLists, $objTaskWatchList );

		if( !$boolFromEntrata ) {
			// Adding Sales Closer
			if( array_key_exists( $this->m_objActiveContract->getSalesEmployeeId(), $arrobjEmployees ) && valObj( $arrobjEmployees[$this->m_objActiveContract->getSalesEmployeeId()], 'CEmployee' ) ) {
				if( CEmployeeStatusType::PREVIOUS != $arrobjEmployees[$this->m_objActiveContract->getSalesEmployeeId()]->getEmployeeStatusTypeId() && is_null( $arrobjEmployees[$this->m_objActiveContract->getSalesEmployeeId()]->getDateTerminated() ) ) {
					$arrstrToEmailAddress[] = $arrobjEmployees[$this->m_objActiveContract->getSalesEmployeeId()]->getEmailAddress();
				}
			}
			// Adding Sales Manager
			if( array_key_exists( $this->m_objActiveContract->getSalesManagerEmployeeId(), $arrobjEmployees ) && valObj( $arrobjEmployees[$this->m_objActiveContract->getSalesManagerEmployeeId()], 'CEmployee' ) ) {
				if( CEmployeeStatusType::PREVIOUS != $arrobjEmployees[$this->m_objActiveContract->getSalesManagerEmployeeId()]->getEmployeeStatusTypeId() && is_null( $arrobjEmployees[$this->m_objActiveContract->getSalesManagerEmployeeId()]->getDateTerminated() ) ) {
					$arrstrToEmailAddress[] = $arrobjEmployees[$this->m_objActiveContract->getSalesManagerEmployeeId()]->getEmailAddress();
				}
			}

			// Adding created_by user as stakeholder to received new sale email.
			if( is_numeric( $objUser->getEmployeeId() ) && array_key_exists( $objUser->getEmployeeId(), $arrobjEmployees ) && valObj( $arrobjEmployees[$objUser->getEmployeeId()], 'CEmployee' ) ) {
				$arrstrToEmailAddress[] = $arrobjEmployees[$objUser->getEmployeeId()]->getEmailAddress();
			}
		}

		if( valArr( $arrobjContractProductStakeholders ) ) {
			foreach( $arrobjContractProductStakeholders As $intProductId => $objContractProductStakeholderDetails ) {
				foreach( $objContractProductStakeholderDetails As $objContractProductStakeholder ) {

					if( !valId( $objContractProductStakeholder->getProductOptionId() ) ) {
						$objCloneTaskWatchList = clone $objTaskWatchList;
						$objCloneTaskWatchList->setUserId( $objContractProductStakeholder->getUserId() );

						if( $objContractProductStakeholder->getTaskRecipient() ) {
							array_push( $this->m_arrobjTaskWatchLists, $objCloneTaskWatchList );
						}

						if( array_key_exists( $objContractProductStakeholder->getUserId(), $arrobjUsers ) && array_key_exists( $arrobjUsers[$objContractProductStakeholder->getUserId()]->getEmployeeId(), $arrobjEmployees ) && valObj( $arrobjEmployees[$arrobjUsers[$objContractProductStakeholder->getUserId()]->getEmployeeId()], 'CEmployee' ) ) {
							$arrstrToEmailAddress[] = $arrobjEmployees[$arrobjUsers[$objContractProductStakeholder->getUserId()]->getEmployeeId()]->getEmailAddress();
						}
					} else {
						if( array_key_exists( $objContractProductStakeholder->getProductOptionId(), $arrobjSoldContractProductOptions ) ) {
							$objTaskWatchList = $this->m_objTask->createTaskWatchList();
							$objTaskWatchList->setUserId( $objContractProductStakeholder->getUserId() );

							array_push( $this->m_arrobjTaskWatchLists, $objTaskWatchList );

							if( array_key_exists( $objContractProductStakeholder->getUserId(), $arrobjUsers ) && array_key_exists( $arrobjUsers[$objContractProductStakeholder->getUserId()]->getEmployeeId(), $arrobjEmployees ) && valObj( $arrobjEmployees[$arrobjUsers[$objContractProductStakeholder->getUserId()]->getEmployeeId()], 'CEmployee' ) ) {
								$arrstrToEmailAddress[] = $arrobjEmployees[$arrobjUsers[$objContractProductStakeholder->getUserId()]->getEmployeeId()]->getEmailAddress();
							}
						}
					}
				}
			}
		}
		if( $boolFromEntrata ) {
			$arrobjPsDocuments	= CPsDocuments::createService()->fetchContractPsDocumentsByContractId( $this->getId(), $objAdminDatabase, $intContractDocumentTypeId = CContractDocumentType:: INITIAL_CONTRACT );
		} else {
			$arrobjPsDocuments = CPsDocuments::createService()->fetchContractPsDocumentsByContractId( $this->getId(), $objAdminDatabase );
		}
		$arrobjTaskAttachments 	= [];
		if( 0 < count( $arrobjPsDocuments ) ) {

			$objTaskAttachmentclone	 = $this->m_objTask->createTaskAttachment();
			foreach( $arrobjPsDocuments as $objPsDocument ) {
				$objTaskAttachment	 = clone $objTaskAttachmentclone;
				$objTaskAttachment->setFileTmpName( $objPsDocument->getFileName() );
				$objTaskAttachment->setFileName( $objPsDocument->getFileName() );
				$objTaskAttachment->setUrl( $objPsDocument->getFileName() );
				$arrobjTaskAttachments[] = $objTaskAttachment;
			}
		}
		if( !$this->createNewSaleTask( $this->m_objTask, $this->m_arrobjTaskWatchLists, $this, $objUser, $objAdminDatabase, $arrobjTaskAttachments, $boolFromEntrata, $objObjectStorageGateway ) ) {
			return false;
		}
		$boolIsSuccess = true;
			if( !$boolFromEntrata ) {
				// Send New Sales Email
				if( !valObj( $objEmailDatabase, 'CDatabase' ) ) {
					$this->displayViewError( 'Failed to load email database object', true );
					return false;
				}

				$strSubject = 'New Sale: ';
				$strSubject .= $objPsLead->getCompanyName() . ', ';
				$strSubject .= $this->m_objActiveContract->getPropertyCount() . ' Props, ' . $this->m_objActiveContract->getUnitCount() . ' Units, ';
				$strSubject .= $arrmixTemplateParameters['currency_symbol'] . round( $this->m_objActiveContract->getAcvAmount() ) . ' in new ACV.';

				$arrmixTemplateParameters['logo_url'] = CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG;

				//$objRenderTemplate = new CRenderTemplate( 'sales/contracts/new_sale_email.tpl', 'user_administration/client_admin_email_template.tpl',  true, $arrmixTemplateParameters );
				$objRenderTemplate = new CRenderTemplate( 'sales/contracts/new_sale_email.tpl', NULL, false, $arrmixTemplateParameters );
				$strHtmlContent    = $objRenderTemplate->nestedFetchTemplate( PATH_INTERFACES_CLIENT_ADMIN . 'sales/contracts/new_sale_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

				if( $boolIsNewBigSaleEmail ) {
					$arrstrToEmailAddress[] = CSystemEmail::BIGSALE_EMAIL_ADDRESS;
				}

				$arrstrToEmailAddress[] = CSystemEmail::NEWSALE_EMAIL_ADDRESS;

				$objSystemEmailLibrary = new CSystemEmailLibrary();
				$objSystemEmail        = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::NEW_SALE, $strSubject, $strHtmlContent, $strToEmailAddress = NULL, $intIsSelfDestruct = 0, $strFromEmailAddress = $objUser->getEmployee()->getEmailAddress() );

				$boolIsSuccess = true;

				if( valArr( $arrstrToEmailAddress ) ) {

					$arrstrToEmailAddress = array_unique( $arrstrToEmailAddress );

					$objSystemEmail->setToEmailAddress( implode( ', ', $arrstrToEmailAddress ) );
					if( !$objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
						$boolIsSuccess &= false;

						return $boolIsSuccess;
					}
				}
			}
		return $boolIsSuccess;

	}

	private function getSystemTemplateTemporaryFullFilePath( $intTemplateType, $objObjectStorageGateway, $objDatabase ) {

		$objSystemTemplate = $this->getSystemTemplate( $intTemplateType, $objDatabase );

		if( false == valObj( $objSystemTemplate, \CSystemTemplate::class ) ) {
			return false;
		}

		$arrmixGatewayRequest = $objSystemTemplate->fetchStoredObject( $objDatabase )->createGatewayRequest( [ 'outputFile' => 'temp' ] );
		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );

		if( true == $objObjectStorageGatewayResponse['hasError'] ) {
			return false;
		}

		return $objObjectStorageGatewayResponse['outputFile'];
	}

	private function getSystemTemplate( $intTemplateType, $objDatabase ) {
		return \Psi\Eos\Admin\CSystemTemplates::createService()->fetchSystemTemplateByTemplateTypeId( $intTemplateType, $objDatabase );
	}

}
?>