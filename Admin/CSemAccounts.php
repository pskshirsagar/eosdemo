<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAccounts
 * Do not add any new functions to this class.
 */

class CSemAccounts extends CBaseSemAccounts {

	public static function fetchAllSemAccounts( $objAdminDatabase ) {
		return self::fetchSemAccounts( 'SELECT * FROM sem_accounts', $objAdminDatabase );
	}

	public static function fetchRandomSemAccountBySemSourceId( $intSemSourceId, $objDatabase ) {
		return self::fetchSemAccount( sprintf( 'SELECT * FROM sem_accounts WHERE sem_source_id = %d LIMIT 1', ( int ) $intSemSourceId ), $objDatabase );
	}

}
?>