<?php

class CRemittanceAddress extends CBaseRemittanceAddress {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddressTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdministrativeArea() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBilltoCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrencyCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>