<?php

class CSalesTaxTransmissionType extends CBaseSalesTaxTransmissionType {

	const CALCULATE_REQUEST 				= 1;
	const GEOBLOCK_REQUEST_ACCOUNT			= 2;
	const GEOBLOCK_REQUEST_PROPERTY 		= 3;
	const UNATTRIBUTED_RETURN_REQUEST		= 4;
	const ATTRIBUTED_FULL_RETURN_REQUEST 	= 5;
	const PARTIAL_RETURN_REQUEST			= 6;
	const SYSTEM_DATE_TIME_REQUEST			= 7;
	const CREATE_SKU 						= 8;
	const CANCEL_REQUEST					= 9;
	const FINALIZE_REQUEST					= 10;
	const GEOBLOCK_REQUEST_ALL				= 11;
	const TAX_ADJUSTMENT_REQUEST			= 12;
	const GET_TRANSACTION_TAX				= 13;
	const GET_TAX_RATES 					= 14;
	const GET_DATA_VALUES					= 15;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>