<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupPermissions
 * Do not add any new functions to this class.
 */

class CGroupPermissions extends CBaseGroupPermissions {

	public static function fetchGroupPermissionsByGroupId( $intGroupId, $objDatabase ) {
		// Objects are going to be sorted by module name in the array.

		$strSql = sprintf( 'SELECT * FROM %s WHERE  group_id = %d', 'group_permissions', ( int ) $intGroupId );

		$arrobjGroupPermissions = NULL;
		$objGroupPermission = NULL;

		$objDataset = $objDatabase->createDataset();

		if( !$objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		if( $objDataset->getRecordCount() > 0 ) {
			$arrobjGroupPermissions = array();

			while( !$objDataset->eof() ) {
				$arrstrValues = $objDataset->fetchArray();

				$objGroupPermission = new CGroupPermission();
				$objGroupPermission->setValues( $arrstrValues );
				$arrobjGroupPermissions[$objGroupPermission->getPsModuleId()] = $objGroupPermission;

				$objDataset->next();
			}
		}

		$objDataset->cleanup();
		return $arrobjGroupPermissions;
	}

	public static function fetchGroupPermissionsByUserId( $intUserId, $objDatabase ) {

		$strSql = 'SELECT
						gp.ps_module_id
					FROM
						group_permissions gp
						JOIN user_groups ug ON ( ug.group_id = gp.group_id AND ug.user_id =  ' . ( int ) $intUserId . ' )
					WHERE
						ug.deleted_by IS NULL
						AND gp.is_allowed = 1
					GROUP BY
						gp.ps_module_id';

		return self::fetchGroupPermissions( $strSql, $objDatabase );
	}

	public static function fetchGroupPermissionsByGroupsIdsByPsModuleId( $arrintGroupsIds, $intPsModuleId, $objDatabase ) {

		if( false == valArr( $arrintGroupsIds ) || false == valId( $intPsModuleId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						group_permissions
					WHERE
						group_id IN ( ' . implode( ',', $arrintGroupsIds ) . ' )
						AND ps_module_id = ' . ( int ) $intPsModuleId;

		return self::fetchGroupPermissions( $strSql, $objDatabase );
	}

	public static function fetchGroupPermissionsByGroupIdByPsModuleIds( $arrintGroupModuleIds, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						group_permissions
					WHERE
						( group_id, ps_module_id ) IN ( ' . sqlIntMultiImplode( $arrintGroupModuleIds ) . ' )';

		return self::fetchGroupPermissions( $strSql, $objDatabase );
	}

	public static function fetchPsModuleIdsByUserId( $intUserId, $objDatabase ) {
		$strSql = 'SELECT
						gp.ps_module_id
					FROM
						group_permissions gp JOIN ps_modules pm ON( gp.ps_module_id = pm.id AND gp.is_allowed = 1 AND
						pm.is_published = 1 AND pm.is_public <> 1 )
					WHERE
						( gp.group_id ) IN (
								SELECT group_id
								FROM user_groups
								WHERE user_id = ' . ( int ) $intUserId . '
								AND deleted_by IS NULL
								ORDER BY group_id
						)ORDER BY gp.ps_module_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPsModuleIdsByGroupIds( $arrintGroupIds, $objDatabase ) {
		if( false == valArr( $arrintGroupIds ) ) return NULL;

		$strSql = 'SELECT
						gp.ps_module_id
					FROM
						group_permissions gp JOIN ps_modules pm ON( gp.ps_module_id = pm.id AND gp.is_allowed = 1 AND
						pm.is_published = 1 AND pm.is_public <> 1 )
					WHERE
						( gp.group_id ) IN ( ' . implode( ',', $arrintGroupIds ) . ' )
					ORDER BY gp.ps_module_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>