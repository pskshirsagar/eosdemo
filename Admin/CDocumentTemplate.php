<?php

use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;

class CDocumentTemplate extends CBaseDocumentTemplate {

	protected $m_arrobjDocumentTemplateComponents;
	protected $m_arrobjDocumentTemplateParameters;
	protected $m_arrobjDocumentTemplateAddendas;
	protected $m_arrobjDocumentTemplateLibraries;
	protected $m_arrobjDocumentTemplatePlugins;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjDocumentTemplateComponents 	= array();
		$this->m_arrobjDocumentTemplateParameters 	= array();
		$this->m_arrobjDocumentTemplateAddendas 	= array();
		$this->m_arrobjDocumentTemplateLibraries 	= array();
		$this->m_arrobjDocumentTemplatePlugins 		= array();

		return;
	}

	/**
	 * Create Functions
	 */

	public function createDocumentTemplateAddenda() {

		$objDocumentTemplateAddenda = new CDocumentTemplateAddenda();
		$objDocumentTemplateAddenda->setLeaseDocumentTemplateId( $this->getId() );

		return $objDocumentTemplateAddenda;
	}

	public function copyDocumentTemplateAddenda( $objCopyDocumentTemplateAddenda ) {

		$objDocumentTemplateAddenda = clone $objCopyDocumentTemplateAddenda;
		$objDocumentTemplateAddenda->setLeaseDocumentTemplateId( $this->getId() );
		$objDocumentTemplateAddenda->setId( NULL );
		$objDocumentTemplateAddenda->setArchivedBy( NULL );
		$objDocumentTemplateAddenda->setArchivedOn( NULL );
		$objDocumentTemplateAddenda->setUpdatedBy( NULL );
		$objDocumentTemplateAddenda->setUpdatedOn( NULL );
		$objDocumentTemplateAddenda->setCreatedBy( NULL );
		$objDocumentTemplateAddenda->setCreatedOn( NULL );

		return $objDocumentTemplateAddenda;
	}

	public function createDocumentTemplateComponent() {

		$objDocumentTemplateComponent = new CDocumentTemplateComponent();
		$objDocumentTemplateComponent->setDocumentTemplateId( $this->m_intId );

		return $objDocumentTemplateComponent;
	}

	/**
	 * Add Functions
	 */

	public function addDocumentTemplateParameter( $objDocumentTemplateParameter ) {
		$this->m_arrobjDocumentTemplateParameters[] = $objDocumentTemplateParameter;
	}

	public function addDocumentTemplateAddenda( $objDocumentTemplateAddenda ) {
		$this->m_arrobjDocumentTemplateAddendas[] = $objDocumentTemplateAddenda;
	}

	/**
	 * Get Functions
	 */

	public function getDocumentTemplateParameters() {
		return $this->m_arrobjDocumentTemplateParameters;
	}

	public function getDocumentTemplateComponents() {
		return $this->m_arrobjDocumentTemplateComponents;
	}

	public function getDocumentTemplateAddendas() {
		return $this->m_arrobjDocumentTemplateAddendas;
	}

	public function getDocumentComponents() {
		return $this->m_arrobjDocumentTemplateComponents;
	}

	public function getDocumentLibraries() {
		return $this->m_arrobjDocumentTemplateLibraries;
	}

	public function getDocumentPlugins() {
		return $this->m_arrobjDocumentTemplatePlugins;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {
		$this->setIsPublished( 1 );
		$this->setRequirePrintedForm( 0 );
	}

	public function setDocumentTemplateParameters( $arrobjDocumentTemplateParameters ) {
		$this->m_arrobjDocumentTemplateParameters = $arrobjDocumentTemplateParameters;
	}

	public function setDocumentTemplateComponents( $arrobjDocumentTemplateComponents ) {
		$this->m_arrobjDocumentTemplateComponents = $arrobjDocumentTemplateComponents;
	}

	public function setDocumentTemplateAddendas( $arrobjDocumentTemplateAddendas ) {
		$this->m_arrobjDocumentTemplateAddendas = $arrobjDocumentTemplateAddendas;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchDocumentTemplateComponents( $objDatabase ) {

		$this->m_arrobjDocumentTemplateComponents = CDocumentTemplateComponents::fetchDocumentTemplateComponentsByDocumentTemplateId( $this->getId(), $objDatabase );

		return $this->m_arrobjDocumentTemplateComponents;
	}

	public function fetchDocumentTemplateParameters( $objDatabase ) {

		$this->m_arrobjDocumentTemplateParameters = CDocumentTemplateParameters::fetchDocumentTemplateParametersByDocumentTemplateId( $this->getId(), $objDatabase );

		return $this->m_arrobjDocumentTemplateParameters;
	}

	public function fetchDocumentTemplateAddendas( $objDatabase ) {

		$this->m_arrobjDocumentTemplateAddendas = CDocumentTemplateAddendas::fetchNonArchivedDocumentTemplateAddendasByLeaseDocumentTemplateId( $this->getId(), $objDatabase );

		return $this->m_arrobjDocumentTemplateAddendas;
	}

	public function fetchDocumentTemplateParametersByDocumentTemplateComponentIds( $arrintDocumentTemplateComponentIds, $objDatabase ) {
		$this->m_arrobjDocumentTemplateParameters = CDocumentTemplateParameters::fetchDocumentTemplateParametersByDocumentTemplateComponentIdsByDocumentTemplateId( $arrintDocumentTemplateComponentIds, $this->getId(), $objDatabase );
		return $this->m_arrobjDocumentTemplateParameters;
	}

	public function fetchPublishedDocuments( $objClientDatabase, $intLimit ) {
		return CDocuments::fetchCustomPublishedDocumentsByDocumentTemplateId( $this->getId(), $objClientDatabase );
	}

	/**
	 * Other Functions
	 */

	public function nestDocumentTemplateComponentsAndDocumentTemplateParameters() {

		$this->nestDocumentTemplateComponents();
		CObjectModifiers::createService()->nestObjects( $this->m_arrobjDocumentTemplateParameters, $this->m_arrobjDocumentTemplateComponents );

		if( true == valArr( $this->m_arrobjDocumentTemplateComponents ) ) {
			foreach( $this->m_arrobjDocumentTemplateComponents as $intKey => $objDocumentTemplateComponent ) {
				if( true == is_numeric( $objDocumentTemplateComponent->getDocumentTemplateComponentId() ) ) {
					unset( $this->m_arrobjDocumentTemplateComponents[$intKey] );
				}
			}
		}

		return true;
	}

	public function nestDocumentTemplateComponents() {

		if( true == valArr( $this->m_arrobjDocumentTemplateComponents ) ) {
			foreach( $this->m_arrobjDocumentTemplateComponents as $objDocumentTemplateComponent ) {
				if( false == is_null( $objDocumentTemplateComponent->getDocumentTemplateComponentId() ) && true == valObj( $this->m_arrobjDocumentTemplateComponents[$objDocumentTemplateComponent->getDocumentTemplateComponentId()], 'CDocumentTemplateComponent' ) ) {
					$this->m_arrobjDocumentTemplateComponents[$objDocumentTemplateComponent->getDocumentTemplateComponentId()]->addDocumentTemplateComponent( $objDocumentTemplateComponent );
				}
			}
		}
	}

	public function loadGridEngineData( $objDatabase ) {
		$this->fetchDocumentTemplateComponents( $objDatabase );

		if( true == valArr( $this->m_arrobjDocumentTemplateComponents ) ) {
			$this->m_arrobjDocumentTemplateParameters = $this->fetchDocumentTemplateParametersByDocumentTemplateComponentIds( array_keys( $this->m_arrobjDocumentTemplateComponents ), $objDatabase );
		} else {
			$this->m_strContent = NULL;
			return;
		}

		$this->nestDocumentTemplateComponents();
		CObjectModifiers::createService()->nestObjects( $this->m_arrobjDocumentTemplateParameters, $this->m_arrobjDocumentTemplateComponents );
	}

	public function loadGridEngineDisplayContent( $objDatabase, $boolIsPopup = 0 ) {

		$boolShowSecureData = false;

		$this->loadGridEngineData( $objDatabase );

		if( true == valArr( $this->m_arrobjDocumentTemplateComponents ) ) {

			$arrobjStates = \Psi\Eos\Admin\CStates::createService()->fetchAllStates( $objDatabase );

			$objGridEngineControl = new CGridEngineControl();
			$objGridEngineControl->setDocumentComponents( $this->m_arrobjDocumentTemplateComponents );
			$objGridEngineControl->setIsPrint( false );
			$objGridEngineControl->setShowSecureData( true );
			$objGridEngineControl->setStates( $arrobjStates );
			$objGridEngineControl->setDocumentSubTypeId( $this->m_intDocumentSubTypeId );
			$objGridEngineControl->setLeaseEditor( true );

			if( 'development' != CONFIG_ENVIRONMENT && 'stage' != CONFIG_ENVIRONMENT ) {
				if( false == $boolShowSecureData ) {
					// This is a hack and should be fixed at some point to use the local system while in dev environement.
					$objGridEngineControl->setFileBaseName( 'https://files.entrata.com' );
				}
			}

			$objGridEngineControl->generateDisplayHtml( $boolIsPopup );
			$this->m_strContent = $objGridEngineControl->getDisplayHtml();
		}
	}

	/**
	 * Validate Functions
	 */

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) || 0 == strlen( trim( $this->getTitle() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Title is required.', NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) || 0 == strlen( trim( $this->getDescription() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Description is required.', NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getStateCode() ) || 0 == strlen( trim( $this->getStateCode() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'State code is required.', NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valStateCode();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Database Functions
	 */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$boolIsValid = true;

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$boolIsValid &= $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );

		return $boolIsValid;
	}

	public function deleteDocumentTemplateParameters( $intUserId, $objDatabase ) {
		$strSql = 'DELETE FROM document_template_parameters WHERE document_template_id = ' . ( int ) $this->getId();
		fetchData( $strSql, $objDatabase );

		return true;
	}

	public function deleteDocumentTemplateComponents( $intUserId, $objDatabase ) {

		$strSql = 'DELETE FROM document_template_components WHERE document_template_id = ' . ( int ) $this->getId() . ' AND document_template_component_id IS NOT NULL';
		fetchData( $strSql, $objDatabase );

		$strSql = 'DELETE FROM document_template_components WHERE document_template_id = ' . ( int ) $this->getId() . ' AND document_template_component_id IS NULL';
		fetchData( $strSql, $objDatabase );

		return true;
	}

}
?>