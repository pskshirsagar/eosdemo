<?php

class CTranslationKey extends CBaseTranslationKey {

	private $m_strTranslatedValue;

	public function setTranslatedValue( $strTranslatedValue ) {
		$this->m_strTranslatedValue = $strTranslatedValue;
	}

	public function getTranslatedValue() {
		return $this->m_strTranslatedValue;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['translated_value'] ) ) {
			$this->setTranslatedValue( $arrValues['translated_value'] );
		}

		$this->setAllowDifferentialUpdate( true );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationName() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strApplicationName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_name', 'Please select Application Name.' ) );
		}
		return $boolIsValid;
	}

	public function valNamespace() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strNamespace ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'namespace', 'Please select Namespace.' ) );
		}
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strKey ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key',  'Please enter Translation Key.' ) );
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPublishedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPublishedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			$boolIsValid &= $this->valApplicationName();
			$boolIsValid &= $this->valNamespace();
			$boolIsValid &= $this->valKey();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( 'NOW()' );
		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>
