<?php

class CTeam extends CBaseTeam {

	const FREE_SWITCH_AND_VOIP_DEV_I						= 11;
	const SYSTEMS_DEV_VI									= 14;
	const SITE_TABLET_IOS									= 63;
	const US_HR												= 76;
	const INDIA_RECRUITMENT									= 92;
	const LEASING_CENTER_CALL_TRACKING_ANALYSIS_FREESWITCH	= 113;
	const BLAKE_WEBSTER										= 153;
	const ID_TRAINING										= 228;
	const LEASING_CENTER_DEV_I								= 296;
	const SUPPORT_DOMAIN_MARKETING							= 308;
	const SUPPORT_DOMAIN_RESIDENT							= 309;
	const SUPPORT_DOMAIN_LEASE								= 310;
	const SUPPORT_DOMAIN_ENTRATA_CORE						= 311;
	const JAMES_DRAPER										= 320;
	const NEW_JOINEE_INDIA									= 337;
	const ADMIN_2 											= 357;
	const HR_AND_GENERAL_DEV								= 455;
	const CLIENT_ADMIN_HR_GENERAL_DEV_I						= 455;
	const OUTSIDE_SALES_TEAM_518							= 518;
	const SITE_TABLET_ANDROID								= 531;
	const GILBERT_THOMAS									= 616;
	const CLIENT_ADMIN_HR_GENERAL_DEV_III					= 653;
	const CLIENT_ADMIN_HR_GENERAL_DEV_IV					= 659;
	const RESIDENT_UTILITY									= 672;
	const SALES_AND_MARKETING_QA							= 676;
	const OUTSIDE_SALES_TEAM_723							= 723;
	const OUTSIDE_SALES_TEAM_724							= 724;
	const OUTSIDE_SALES_TEAM_725							= 725;
	const OUTSIDE_SALES_TEAM_726							= 726;
	const OUTSIDE_SALES_TEAM_727							= 727;
	const CLIENT_ADMIN_QA_III								= 742;
	const GILBERT_THOMAS_2									= 744;
	const OUTSIDE_SALES_TEAM_747							= 747;
	const SITE_TABLET_DEV_I									= 772;
	const CLIENT_ADMIN_QA_II								= 859;
	const OUTSIDE_SALES_TEAM_911							= 911;
	const AUTOMATION_EFFORTS_QA								= 997;
	const TEAM_LISA_FAWCETT									= 1038;
	const TEAM_DALE_BROWN									= 1040;
	const AUTOMATION_EFFORTS_QA1							= 1069;
	const AUTOMATION_EFFORTS_QA2							= 1071;
	const AUTOMATION_TEAM									= 1119;
	const NEW_JOINEE_US										= 1085;
	const TEAM_INTERNAL_SYSTEM_ADMIN_1						= 888;
	const OUTSIDE_SALES_TEAM_1202							= 1202;
	const OUTSIDE_SALES_TEAM_1224							= 1224;

	const TEAM_NEW_JOINEE_INDIA								= 'New Joinee India';
	const TEAM_NEW_JOINEE_US								= 'New Joinee US';

	const TEAM_CHRIS_MARTIN									= 366;

	const AR_AND_AP_TEAM_ID									= 816;
	const EHD_TEAM_ID										= 1102;
	const MARKETING_TEAM_ID									= 976;
	const LEASING_TEAM_ID									= 884;
	const RESIDENTS_TEAM_ID									= 17;
	const RESIDENT_VERIFY_TEAM_ID							= 946;
	const INTERNATIONAL_TEAM_ID								= 1191;
	const DEVOPS_AUTOMATION_TEAM_ID							= 129;

	public static $c_arrmixSupportTeamIds = [
		'ar'			=> self::AR_AND_AP_TEAM_ID,
		'ap'			=> self::AR_AND_AP_TEAM_ID,
		'ehd'			=> self::EHD_TEAM_ID,
		'marketing' 	=> self::MARKETING_TEAM_ID,
		'leasing'		=> self::LEASING_TEAM_ID,
		'residents'		=> self::RESIDENTS_TEAM_ID,
		'rv'			=> self::RESIDENT_VERIFY_TEAM_ID,
		'international'	=> self::INTERNATIONAL_TEAM_ID
	];

	public static $c_arrintXmppSyncTeam		= [
		self::SUPPORT_DOMAIN_ENTRATA_CORE,
		self::SUPPORT_DOMAIN_LEASE,
		self::SUPPORT_DOMAIN_MARKETING,
		self::SUPPORT_DOMAIN_RESIDENT
	];

	protected $m_boolIsPrimaryTeam;

	protected $m_intCountTeam;
	protected $m_intTeamManagerId;
	protected $m_intCountTeamMember;

	protected $m_strNameLast;
	protected $m_strNameFirst;
	protected $m_strManagerName;
	protected $m_strProductName;
	protected $m_strPreferredName;
	protected $m_strDepartmentName;
	protected $m_strProjectManagerName;
	protected $m_strADDName;
	protected $m_strHrRepresentativeName;

	protected $m_arrintTeamMemberIds;

	/**
	 * Create Functions
	 */

	public function createTeamEmployee() {
		$this->m_objTeamEmployee = new CTeamEmployee();
		$this->m_objTeamEmployee->setTeamId( $this->getId() );
		$this->m_objTeamEmployee->setManagerEmployeeId( $this->getManagerEmployeeId() );
		return $this->m_objTeamEmployee;
	}

	/**
	 * Get Functions
	 */

	public function getIsPrimaryTeam() {
		return $this->m_boolIsPrimaryTeam;
	}

	public function getCountTeam() {
		return $this->m_intCountTeam;
	}

	public function getCountTeamMember() {
		return $this->m_intCountTeamMember;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getManagerName() {
		return $this->m_strManagerName;
	}

	public function getProductName() {
		return $this->m_strProductName;
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getProjectManagerName() {
		return $this->m_strProjectManagerName;
	}

	public function getADDName() {
		return $this->m_strADDName;
	}

	public function getHrRepresentativeName() {
		return $this->m_strHrRepresentativeName;
	}

	public function getTeamMemberIds() {
		return $this->m_arrintTeamMemberIds;
	}

	public function getTeamDepartmentName() {
		return $this->m_strTeamDepartmentName;
	}

	public function getTeamManagerId() {
		return $this->m_intTeamManagerId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_first'] ) ) 				$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) 				$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['preferred_name'] ) ) 			$this->setPreferredName( $arrmixValues['preferred_name'] );
		if( true == isset( $arrmixValues['is_primary_team'] ) ) 		$this->setIsPrimaryTeam( $arrmixValues['is_primary_team'] );
		if( true == isset( $arrmixValues['my_team_members'] ) ) 		$this->setCountTeamMember( $arrmixValues['my_team_members'] );
		if( true == isset( $arrmixValues['my_teams'] ) ) 				$this->setCountTeam( $arrmixValues['my_teams'] );
		if( true == isset( $arrmixValues['my_team_member_ids'] ) ) 		$this->setTeamMemberIds( $arrmixValues['my_team_member_ids'] );
		if( true == isset( $arrmixValues['department_name'] ) ) 		$this->setDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['manager_name'] ) )			$this->setManagerName( $arrmixValues['manager_name'] );
		if( true == isset( $arrmixValues['project_manager_name'] ) )	$this->setProjectManagerName( $arrmixValues['project_manager_name'] );
		if( true == isset( $arrmixValues['add_name'] ) )				$this->setADDName( $arrmixValues['add_name'] );
		if( true == isset( $arrmixValues['hr_representative_name'] ) )	$this->setHrRepresentativeName( $arrmixValues['hr_representative_name'] );
		if( true == isset( $arrmixValues['department_name'] ) )			$this->setTeamDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['product_name'] ) )			$this->setProductName( $arrmixValues['product_name'] );
		if( true == isset( $arrmixValues['team_manager_employee_id'] ) )	$this->setTeamManagerId( $arrmixValues['team_manager_employee_id'] );

		return;
	}

	public function setIsPrimaryTeam( $boolIsPrimaryTeam ) {
		$this->m_boolIsPrimaryTeam = $boolIsPrimaryTeam;
	}

	public function setCountTeam( $intCountTeam ) {
		$this->m_intCountTeam = $intCountTeam;
	}

	public function setTeamManagerId( $intTeamManagerId ) {
		$this->m_intTeamManagerId = $intTeamManagerId;
	}

	public function setCountTeamMember( $intCountTeamMember ) {
		$this->m_intCountTeamMember = $intCountTeamMember;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setManagerName( $strManagerName ) {
		$this->m_strManagerName = $strManagerName;
	}

	public function setProductName( $strProductName ) {
		$this->m_strProductName = $strProductName;
	}

	public function setPreferredName( $strPreferredName ) {
		$this->m_strPreferredName = CStrings::strTrimDef( $strPreferredName, 100, NULL, true );
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setProjectManagerName( $strProjectManagerName ) {
		$this->m_strProjectManagerName = $strProjectManagerName;
	}

	public function setADDName( $strADDName ) {
		$this->m_strADDName = $strADDName;
	}

	public function setHrRepresentativeName( $strHrRepresentativeName ) {
		$this->m_strHrRepresentativeName = $strHrRepresentativeName;
	}

	public function setTeamMemberIds( $arrintTeamMemberIds ) {
		$this->m_arrintTeamMemberIds = $arrintTeamMemberIds;
	}

	public function setTeamDepartmentName( $strTeamDepartmentName ) {
		$this->m_strTeamDepartmentName = $strTeamDepartmentName;
	}

	/**
	 * Validation Functions
	 */

	public function validateTeamEmployee( $intTeamId, $intEmployeeId, $objDatabase ) {

		$boolIsPresent = true;
		$objTeamEmployee = CTeamEmployees::fetchTeamEmployeesByTeamIdByEmployeeId( $intTeamId, $intEmployeeId, $objDatabase );

		if( true == valObj( $objTeamEmployee, 'CTeamEmployee' ) ) {

			$boolIsPresent = false;
		}
		return $boolIsPresent;
	}

	public function valManagerEmployeeId() {
		$boolValid = true;

		if( true == is_null( $this->getManagerEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'manager_employee_id', 'Manager is required. ' ) );
		}

		return $boolValid;
	}

	public function valProjectEmployeeId( $objDatabase ) {
		$boolValid = true;

		if( true == is_null( $this->getSdmEmployeeId() ) && ( false == in_array( $this->getManagerEmployeeId(), array_keys( ( array ) rekeyObjects( 'Id', CEmployees::fetchAllADDs( $objDatabase ) ) ) ) ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'project_employee_id', 'Software development manager is required. ' ) );
		}

		return $boolValid;
	}

	public function valHrRepresentativeEmployeeId() {
		$boolValid = true;

		if( true == is_null( $this->getHrRepresentativeEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hr_representative_employee_id', 'HR Representative is required. ' ) );
		}

		return $boolValid;
	}

	public function valName( $objDatabase ) {
		$boolValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Team name is required. ' ) );
		}

		if( true == $boolValid ) {
			$intConflictingTeamCount = CTeams::fetchConflictingTeamCountByName( $this->getName(), $this->getId(), $this->getManagerEmployeeId(), $objDatabase );

			if( 0 < $intConflictingTeamCount ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Team name already exists. ' ) );
			}
		}

		return $boolValid;
	}

	public function valDepartmentId() {
		$boolValid = true;

		if( true == is_null( $this->getDepartmentId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_id', 'Department is required. ' ) );
		}

		return $boolValid;
	}

	public function valTeamTypeId() {
		$boolValid = true;

		if( true == is_null( $this->getTeamTypeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'team_type_id', 'Team type is required. ' ) );
		}

		return $boolValid;
	}

	public function valTpmEmployeeId() {
		$boolValid = true;

		if( true == is_null( $this->getTpmEmployeeId() ) &&  false == is_numeric( $this->getTpmEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tpm_employee_id', 'Technical Project Manager is required. ' ) );
		}

		return $boolValid;
	}

	public function valVdbaEmployeeId() {
		$boolValid = true;

		if( true == is_null( $this->getVdbaEmployeeId() ) && false == is_numeric( $this->getVdbaEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vdba_employee_id', 'Virtual DBA is required. ' ) );
		}

		return $boolValid;
	}

	public function valDeleteTeam( $objDatabase ) {
		$boolValid = true;

		$arrintTeamEmployeesCount = CTeamEmployees::fetchCurrentTeamEmployeesCountByTeamIds( array( $this->getId() => $this->getId() ), $objDatabase, $boolIsPrimaryTeam = true );
		if( true == valArr( $arrintTeamEmployeesCount ) && 0 < $arrintTeamEmployeesCount[0]['member_count'] ) {
			$boolValid = false;
		}

		return $boolValid;
	}

	public function valQaEmployeeId() {
		$boolValid = true;
		if( true == is_null( $this->getQaEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qa_employee_id', 'QA Mentor is required. ' ) );
		}
		return $boolValid;
	}

	public function valQaDepartmentId( $objDatabase ) {
		$boolValid = true;

		$objDepartment = CDepartments::fetchDepartmentByEmployeeId( $this->getManagerEmployeeId(), $objDatabase );
		$intManagerDepartmentId = ( true == valObj( $objDepartment, 'CDepartment' ) )? $objDepartment->getId() : NULL;

		if( true == in_array( CDepartment::QA, [ $this->getDepartmentId(), $intManagerDepartmentId ] ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_id', 'QA team can\'t  be created. ' ) );
		}

		return $boolValid;
	}

	public function valTeamCreation( $objDatabase ) {
		$boolValid = true;

		$arrobjAllAdd			= CEmployees::fetchAllADDs( $objDatabase );
		$arrobjTpmEmployees		= CEmployees::fetchAllTPMsOfIndiaOffice( $objDatabase );

		if( true == in_array( $this->getDepartmentId(), [ CDepartment::DEVELOPMENT, CDepartment::CREATIVE, CDepartment::MOBILE_TECHNOLOGY ] ) && false == valObj( $arrobjAllAdd[$this->getManagerEmployeeId()], 'CEmployee' ) && false == valObj( $arrobjTpmEmployees[$this->getManagerEmployeeId()], 'CEmployee' ) && false == valId( $this->getId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'manager_employee_id', 'Team will be created upto TL Level.' ) );
		}
		return $boolValid;
	}

	public function validate( $strAction, $objDatabase, $strCountryCode = NULL, $strLoginUserCountryCode = NULL ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolValid &= $this->valManagerEmployeeId();

				if( CCountry::CODE_INDIA == $strCountryCode ) {
					// when selected manager is ADD then SDM is mandatory
					$boolValid &= $this->valProjectEmployeeId( $objDatabase );
					$boolValid &= $this->valHrRepresentativeEmployeeId();

					if( true == in_array( $this->getDepartmentId(), [ CDepartment::DEVELOPMENT, CDepartment::CREATIVE, CDepartment::MOBILE_TECHNOLOGY ] ) ) {
						$boolValid &= $this->valQaEmployeeId();
						$boolValid &= $this->valTpmEmployeeId();
						$boolValid &= $this->valVdbaEmployeeId();

					}
				}

			// QA team will not get created.
				if( CCountry::CODE_INDIA == $strLoginUserCountryCode ) {
					$boolValid &= $this->valQaDepartmentId( $objDatabase );
				}

				$boolValid &= $this->valName( $objDatabase );
				$boolValid &= $this->valTeamTypeId();
				$boolValid &= $this->valDepartmentId();

				$strCountryCode = ( true == is_null( $strCountryCode ) ) ? $strLoginUserCountryCode : $strCountryCode;
				break;

			case VALIDATE_DELETE:
				$boolValid &= $this->valDeleteTeam( $objDatabase );
				break;

			case 'validate_duplicate_preferred_name':
				$boolValid &= $this->isDuplicatePreferredName( $objDatabase );
				break;

			default:
				$boolValid = true;
				break;
		}

		return $boolValid;
	}

	public function isDuplicatePreferredName( $objAdminDatabase ) {
		$boolValid = true;

		$strSql = 'SELECT
						1
					FROM
						teams
					WHERE
						id <> ' . ( int ) $this->getId() . '
						AND preferred_name like \'' . $this->getPreferredName() . '\'';

		$boolValid = valArr( fetchData( $strSql, $objAdminDatabase ) );

		if( true == $boolValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'preferred_name', 'Preferred Name already exists' ) );
		}

		return $boolValid;
	}

}
?>