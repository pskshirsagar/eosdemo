<?php

class CCompanyKeyValue extends CBaseCompanyKeyValue {

	const PRICING_ACTIVE_TEST_CLIENT = 'PRICING_ACTIVE_TEST_CLIENT';
	const NO_FREQUENCY                                  = '0';
	const DAILY_FREQUENCY                               = '1';
	const WEEKLY_FREQUENCY                              = '7';

	public static $c_arrstrTicketEmailFrequencies = [
		CCompanyKeyValue::NO_FREQUENCY      => [ 'id' => CCompanyKeyValue::NO_FREQUENCY, 'name' => 'NONE' ],
		CCompanyKeyValue::DAILY_FREQUENCY   => [ 'id' => CCompanyKeyValue::DAILY_FREQUENCY, 'name' => 'DAILY' ],
		CCompanyKeyValue::WEEKLY_FREQUENCY  => [ 'id' => CCompanyKeyValue::WEEKLY_FREQUENCY, 'name' => 'WEEKLY' ]
	];

	/**
	 * Validation Functions
	 *
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			case 'insert_or_update':
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>