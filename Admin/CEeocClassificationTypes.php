<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEeocClassificationTypes
 * Do not add any new functions to this class.
 */

class CEeocClassificationTypes extends CBaseEeocClassificationTypes {

	public static function fetchEeocClassificationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEeocClassificationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEeocClassificationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEeocClassificationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllEeocClassificationTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM eeoc_classification_types';
		return self::fetchEeocClassificationTypes( $strSql, $objDatabase );
	}
}
?>