<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActions
 * Do not add any new functions to this class.
 */

class CActions extends CBaseActions {

	public static function fetchActionByIdByPsLeadId( $intActionId, $intPsLeadId, $objDatabase ) {
		return self::fetchAction( 'SELECT * FROM actions WHERE id = ' . ( int ) $intActionId . ' AND ps_lead_id = ' . ( int ) $intPsLeadId . ' ORDER BY action_datetime DESC', $objDatabase );
	}

	public static function fetchActionsByContractId( $intContractId, $objDatabase ) {
		return self::fetchActions( 'SELECT * FROM actions WHERE contract_id = ' . ( int ) $intContractId . ' ORDER BY action_datetime DESC', $objDatabase );
	}

	public static function fetchSimpleSalesFollowUps( $objPsLeadFilter, $objDatabase ) {

		if( false == valStr( $objPsLeadFilter->getPsLeadIds() ) ) {
			return [];
		}

		$strSql = 'SELECT * FROM (
						SELECT
							a.id,
							a.ps_lead_id,
							a.contract_id,
							e.id AS sales_employee_id,
							( e.name_first || \' \' || e.name_last ) AS employee_name,
							at.name AS action_type,
							generate_age ( a.action_datetime::DATE ) AS age_description,
							a.action_datetime::DATE AS action_date,
							' . ( ( true == $objPsLeadFilter->getIsDemoing() ) ? ' rank ( ) OVER ( PARTITION BY a.ps_lead_id ORDER BY a.action_datetime ASC, a.action_result_id NULLS FIRST, a.id ) AS rank ' : ' rank ( ) OVER ( PARTITION BY a.contract_id ORDER BY a.action_datetime DESC, a.action_result_id NULLS FIRST, a.id ) AS rank ' ) . '

						FROM
							actions a
							JOIN action_types at ON ( at.id = a.action_type_id )
							JOIN employees e ON ( a.employee_id = e.id )
							JOIN contracts c ON ( a.contract_id = c.id )
						WHERE
							' . ( ( 0 < strlen( trim( $objPsLeadFilter->getContracts() ) ) ) ? ' a.contract_id IN ( ' . $objPsLeadFilter->getContracts() . ' ) ' : ' a.ps_lead_id IN ( ' . $objPsLeadFilter->getPsLeadIds() . ' ) ' ) . '
							' . ( ( true == is_null( $objPsLeadFilter->getSupportEmployees() ) ) ? ' AND e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintSalesDepartmentIds ) . ' )' : '' ) . '
							AND a.action_type_id IN ( ' . implode( ',', CActionType::$c_arrintFollowUpActionTypeIds ) . ' )
							AND a.action_result_id IS NULL
							' . ( ( true == $objPsLeadFilter->getIsDemoing() ) ? ' AND a.action_datetime >= NOW() ' : '' ) . ' ) AS sub
					WHERE sub.rank = 1';

		$arrstrActions = fetchData( $strSql, $objDatabase );

		return rekeyArray( 'contract_id', $arrstrActions );
	}

	public static function fetchSimpleLastSalesActions( $objPsLeadFilter, $objDatabase ) {

		if( false == valStr( $objPsLeadFilter->getPsLeadIds() ) ) {
			return [];
		}

		$strSql = 'SELECT *
						FROM ((
								SELECT *
									FROM (
										SELECT
											a.id,
											a.ps_lead_id,
											a.contract_id,
											e.id AS sales_employee_id,
											( e.name_first || \' \' || e.name_last ) AS employee_name,
											at.name AS action_type,
											generate_age ( COALESCE( a.result_datetime::DATE, a.action_datetime::DATE ) ) AS age_description,
											a.action_datetime::DATE AS action_date,
											date_part(\'days\', now()- a.action_datetime::DATE ) AS no_of_days,
											rank ( ) OVER ( PARTITION BY a.ps_lead_id ORDER BY a.action_datetime DESC, a.action_result_id NULLS FIRST, a.id ) AS rank
										FROM
											actions a
											JOIN action_types at ON ( at.id = a.action_type_id )
											JOIN employees e ON ( a.employee_id = e.id )
											JOIN contracts c ON ( a.contract_id = c.id )
										WHERE
											' . ( ( 0 < strlen( trim( $objPsLeadFilter->getContracts() ) ) ) ? ' a.contract_id IN ( ' . $objPsLeadFilter->getContracts() . ' ) ' : ' a.ps_lead_id IN ( ' . $objPsLeadFilter->getPsLeadIds() . ' ) ' ) . '
											' . ( ( true == is_null( $objPsLeadFilter->getSupportEmployees() ) ) ? ' AND e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintSalesDepartmentIds ) . ' )' : '' ) . '
											AND a.action_type_id IN ( ' . implode( ',', CActionType::$c_arrintPrimaryActionTypeIds ) . ' )
											AND ( a.action_type_id NOT IN ( ' . implode( ',', CActionType::$c_arrintFollowUpActionTypeIds ) . ' ) OR a.action_result_id IS NOT NULL )
											) AS sub
									WHERE sub.rank = 1) sub_query
								FULL JOIN (
									SELECT
											a.ps_lead_id as activity_ps_lead_id,
										COUNT( a.id ) as total_call_email
										FROM
											actions a
										WHERE
											a.ps_lead_id IN ( ' . $objPsLeadFilter->getPsLeadIds() . ' )
											AND a.action_type_id IN ( ' . CActionType::CALL . ',' . CActionType::EMAIL . ' )
										GROUP BY a.ps_lead_id
									) as total_call_email ON (sub_query.ps_lead_id = total_call_email.activity_ps_lead_id ) )';

		return fetchData( $strSql, $objDatabase );
	}

	// This function needs to be recoded!

	public static function fetchActionDataByPsLeadFilter( $objPsLeadFilter, $objDatabase ) {
		$arrstrFinalizedData = [];

		$strSql = 'SELECT
						count(a.id) as count,
						a.action_type_id as action_type_id,
						at.name as name,
						co.sales_employee_id,
						at.order_num
					FROM
						actions a
						JOIN action_types at ON ( a.action_type_id = at.id )
						JOIN contracts co ON ( co.id = a.contract_id )
						LEFT OUTER JOIN clients c ON ( c.id = co.cid )
					WHERE
						( c.id IS NULL OR c.company_status_type_id NOT IN ( ' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::TEMPLATE . ' ) )
						AND co.contract_type_id <> ' . CContractType::RENEWAL . '
						AND DATE_TRUNC( \'day\', a.action_datetime ) >= \'' . $objPsLeadFilter->getActionStartDate() . '\'
						AND DATE_TRUNC( \'day\', a.action_datetime ) <= \'' . $objPsLeadFilter->getActionEndDate() . '\' ';

		if( false == is_null( trim( $objPsLeadFilter->getSalesEmployees() ) ) ) {
			$strSql .= ' AND co.sales_employee_id IN ( ' . $objPsLeadFilter->getSalesEmployees() . ' ) ';
		}

		$strSql .= '
					GROUP BY
						a.action_type_id,
						co.sales_employee_id,
						at.name,
						at.order_num
					ORDER BY
						at.order_num';

		$arrstrLeadLogData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrLeadLogData ) ) {
			foreach( $arrstrLeadLogData as $arrstrLeadLogDataBlock ) {
				$arrstrFinalizedData[$arrstrLeadLogDataBlock['sales_employee_id']]['action_types'][$arrstrLeadLogDataBlock['action_type_id']] = $arrstrLeadLogDataBlock['count'];
			}
		}

		$strSql = 'SELECT
						count( a.id ) as count,
						a.action_result_id as action_result_id,
						plcr.name as name,
						co.sales_employee_id,
						plcr.order_num
					FROM
						actions a
						JOIN contracts co ON ( co.id = a.contract_id AND co.contract_status_type_id <> ' . CContractStatusType::CONTRACT_TERMINATED . ' )
						LEFT JOIN clients c ON ( c.id = co.cid )
						JOIN action_results plcr ON ( a.action_result_id = plcr.id )
					WHERE
						( c.id IS NULL OR c.company_status_type_id NOT IN ( ' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::TEMPLATE . ' ) )
						AND co.contract_type_id <> ' . CContractType::RENEWAL . '
						AND DATE_TRUNC( \'day\', a.action_datetime ) >= \'' . $objPsLeadFilter->getActionStartDate() . '\'
						AND DATE_TRUNC( \'day\', a.action_datetime ) <= \'' . $objPsLeadFilter->getActionEndDate() . '\' ';

		if( false == is_null( trim( $objPsLeadFilter->getSalesEmployees() ) ) ) {
			$strSql .= ' AND co.sales_employee_id IN ( ' . $objPsLeadFilter->getSalesEmployees() . ' ) ';
		}

		$strSql .= '
					GROUP BY
						a.action_result_id,
						co.sales_employee_id,
						plcr.name,
						plcr.order_num
					ORDER BY
						plcr.order_num';

		$arrstrLeadResultData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrLeadResultData ) ) {
			foreach( $arrstrLeadResultData as $arrstrPsLeadResultBlock ) {
				$arrstrFinalizedData[$arrstrPsLeadResultBlock['sales_employee_id']]['ps_lead_results'][$arrstrPsLeadResultBlock['action_result_id']] = $arrstrPsLeadResultBlock['count'];
			}
		}

		return $arrstrFinalizedData;
	}

	public static function fetchRecentTwoActionsByContractIds( $arrintContractIds, $objDatabase ) {

		if( false == valArr( $arrintContractIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sub_query.*
					FROM (
							SELECT
								a.*,
								rank() over( partition by a.contract_id order by a.id desc ) as latest_logs_count
							FROM
								actions a
							WHERE
								a.contract_id IN ( ' . implode( ',', $arrintContractIds ) . ' )
								AND a.action_type_id IN ( ' . implode( ',', CActionType::$c_arrintStatusChangeActionTypeIds ) . ' )

						 ) sub_query
					WHERE sub_query.latest_logs_count <=2';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchNonContractApprovedActionsByContractId( $intContractId, $objDatabase ) {

		$strSql = 'SELECT
						a.*
		 			FROM
		 				actions a,
		 				contracts c
					WHERE
						c.id = a.contract_id
						AND a.contract_id = ' . ( int ) $intContractId . '
						AND c.contract_status_type_id <> ' . CContractStatusType::CONTRACT_APPROVED . '
						AND a.action_type_id = ' . CActionType::CONTRACT_APPROVED;

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByPsLeadIds( $arrintPsLeadIds, $objDatabase ) {

		if( false == valArr( $arrintPsLeadIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT * FROM actions WHERE ps_lead_id IN ( ' . implode( ',', $arrintPsLeadIds ) . ' )';
		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionById( $intActionId, $objDatabase ) {
		if( false == is_numeric( $intActionId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.*
					FROM
						actions a
					WHERE
						a.id = ' . ( int ) $intActionId;

		return self::fetchAction( $strSql, $objDatabase );
	}

	public static function fetchPaginatedActivitiesDetailsByEmployeeId( $intEmployeeId, $arrstrActivityFilter, $objDatabase ) {
		if( true == empty( $intEmployeeId ) || true == empty( $arrstrActivityFilter ) ) {
			return NULL;
		}

		$intOffset 				= ( true == isset( $arrstrActivityFilter['page_no'] ) && true == isset( $arrstrActivityFilter['page_size'] ) ) ? $arrstrActivityFilter['page_size'] * ( $arrstrActivityFilter['page_no'] - 1 ) : 0;
		$intLimit 				= ( true == isset( $arrstrActivityFilter['page_size'] ) ) ? $arrstrActivityFilter['page_size'] : '';
		$strOrderByField 		= ' MAX( a.action_datetime )';
		$strOrderByType 		= ' DESC';

		if( true == isset( $arrstrActivityFilter['order_by_field'] ) ) {
			switch( $arrstrActivityFilter['order_by_field'] ) {
				case 'action_datetime':
					$strOrderByField = ' MAX( a.action_datetime)';
					break;

				case 'company_name':
					$strOrderByField = ' MAX( pl.company_name )';
					break;

				default:
					$strOrderByField = ' MAX( a.action_datetime)';
					break;
			}
		}

		$strOrderByType 				= ( true == isset( $arrstrActivityFilter['order_by_type'] ) && 'asc' == $arrstrActivityFilter['order_by_type'] ) ? ' ASC' : ' DESC';
		$strSalesEmployeeJoinCondition	= '';

		if( true == isset( $arrstrActivityFilter['is_client_executive'] ) && true == $arrstrActivityFilter['is_client_executive'] ) {
			$strJoinClause					= ' JOIN ps_lead_details pld ON ( pl.id = pld.ps_lead_id ) ';
			$strWhereCondition				= ' pld.support_employee_id = ' . ( int ) $intEmployeeId;
		} else {
			$strSalesEmployeeJoinCondition	= ' AND c.sales_employee_id IN ( ' . ( int ) $intEmployeeId . ' ) ';
			$strJoinClause					= '';
			$strWhereCondition				= ' a.employee_id = ' . ( int ) $intEmployeeId;
		}

		$strWhereCondition				.= ( false == empty( $arrstrActivityFilter['action_type_id'] ) ) ? ' AND a.action_type_id = ' . $arrstrActivityFilter['action_type_id'] : '';

		if( false == empty( $arrstrActivityFilter['selected_subtab'] ) && ( 2 == $arrstrActivityFilter['selected_subtab'] || 3 == $arrstrActivityFilter['selected_subtab'] ) ) {
			if( 2 == $arrstrActivityFilter['selected_subtab'] ) {
				$strWhereCondition .= ' AND a.action_result_id = ' . CActionResult::COMPLETED;
			} elseif( 3 == $arrstrActivityFilter['selected_subtab'] ) {
				$strWhereCondition .= ' AND a.action_result_id IN ( ' . CActionResult::MISSED_BY_LEAD . ', ' . CActionResult::MISSED_BY_PS . ' ) ';
			}
		} else {
			$strWhereCondition .= ' AND ( a.action_result_id IS NULL ) ';
		}

		$strSql = '	SELECT
						a.id AS action_id,
						pl.id AS ps_lead_id,
						MAX( a.action_datetime ) AS action_datetime,
						MAX( a.action_type_id ) AS action_type_id,
						MAX( pl.company_name ) AS ps_lead_company_name,
						MAX( pl.number_of_units ) AS number_of_units,
						MAX( pl.priority_number ) AS priority_number,
						MAX( a.employee_id ) AS employee_id,
						MAX( a.action_result_id ) AS action_result_id,
						MAX( c.id ) AS contract_id,
						( COUNT( c.id ) - 1 ) AS total_open_opp
					FROM
						actions a
						JOIN ps_leads pl ON ( a.ps_lead_id = pl.id AND pl.deleted_by IS NULL AND pl.company_status_type_id NOT IN ( ' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::TERMINATED . ',' . CCompanyStatusType::TEMPLATE . ' ) )
						' . $strJoinClause . '
						LEFT JOIN contracts c ON ( pl.id = c.ps_lead_id ' . $strSalesEmployeeJoinCondition . ' AND c.contract_status_type_id NOT IN ( ' . implode( ',', CContractStatusType::$c_arrintCompletedWithOnHoldContractStatusTypeIds ) . ' ) )
						LEFT JOIN contract_properties cp ON ( c.id = cp.contract_id AND cp.renewal_contract_property_id IS NULL AND cp.is_last_contract_record = TRUE AND ( cp.termination_date IS NULL OR cp.termination_date > NOW() ) )
					WHERE
						' . $strWhereCondition . '
					GROUP BY
						a.id,
						pl.id
					ORDER BY ' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( false == is_null( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedActivitiesDetailsCountByEmployeeId( $intEmployeeId, $arrstrActivityFilter, $objDatabase ) {
		if( true == empty( $intEmployeeId ) || true == empty( $arrstrActivityFilter ) ) {
			return NULL;
		}

		$strSalesEmployeeJoinCondition	= '';

		if( true == isset( $arrstrActivityFilter['is_client_executive'] ) && true == $arrstrActivityFilter['is_client_executive'] ) {
			$strJoinClause		= ' JOIN ps_lead_details pld ON ( pl.id = pld.ps_lead_id ) ';
			$strWhereCondition	= ' pld.support_employee_id = ' . ( int ) $intEmployeeId;
		} else {
			$strSalesEmployeeJoinCondition	= ' AND c.sales_employee_id IN ( ' . ( int ) $intEmployeeId . ' ) ';
			$strJoinClause		= '';
			$strWhereCondition	= ' a.employee_id = ' . ( int ) $intEmployeeId;
		}

		$strWhereCondition .= ( false == empty( $arrstrActivityFilter['action_type_id'] ) ) ? ' AND a.action_type_id = ' . $arrstrActivityFilter['action_type_id'] : '';

		if( false == empty( $arrstrActivityFilter['selected_subtab'] ) && ( 2 == $arrstrActivityFilter['selected_subtab'] || 3 == $arrstrActivityFilter['selected_subtab'] ) ) {
			if( 2 == $arrstrActivityFilter['selected_subtab'] ) {
				$strWhereCondition .= ' AND a.action_result_id = ' . CActionResult::COMPLETED;
			} elseif( 3 == $arrstrActivityFilter['selected_subtab'] ) {
				$strWhereCondition .= ' AND a.action_result_id IN ( ' . CActionResult::MISSED_BY_LEAD . ', ' . CActionResult::MISSED_BY_PS . ' ) ';
			}
		} else {
			$strWhereCondition .= ' AND ( a.action_result_id IS NULL ) ';
		}

		$strSql = '	SELECT
						count(subSql.activity_id)
					FROM (
							SELECT
								a.id as activity_id
							FROM
								actions a
								JOIN ps_leads pl ON ( a.ps_lead_id = pl.id AND pl.deleted_by IS NULL AND pl.company_status_type_id NOT IN ( ' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::TERMINATED . ',' . CCompanyStatusType::TEMPLATE . ' ) )
								' . $strJoinClause . '
								LEFT JOIN contracts c ON ( pl.id = c.ps_lead_id ' . $strSalesEmployeeJoinCondition . ' AND c.contract_status_type_id NOT IN ( ' . implode( ',', CContractStatusType::$c_arrintCompletedWithOnHoldContractStatusTypeIds ) . ' ) )
								LEFT JOIN contract_properties cp ON ( c.id = cp.contract_id AND cp.renewal_contract_property_id IS NULL AND cp.is_last_contract_record = TRUE AND ( cp.termination_date IS NULL OR cp.termination_date > NOW() ) )
							WHERE
								' . $strWhereCondition . '
							GROUP BY
								a.id,
								pl.id
						)as subSql';

		$arrstrActivityCount = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrActivityCount ) ) {
			return $arrstrActivityCount[0]['count'];
		}

		return 0;
	}

	public static function fetchLatestActionsByPsLeadIds( $arrintPsLeads, $objDatabase ) {
		if( false == valArr( $arrintPsLeads ) ) {
			return NULL;
		}

		$arrintPublishedActionTypeIds = array_merge( CActionType::$c_arrintPublishedActionTypeIds, CActionType::$c_arrintNotEditableActionTypeIds, CActionType::$c_arrintPeepActionTypeIds );

		$strSql = ' SELECT
						id,
						person_id,
						ps_lead_id,
						action_type_id,
						employee_id,
						action_datetime
					FROM
						actions a
						JOIN (
								SELECT
									MAX( a.id ) AS action_id
								FROM
									actions a
								WHERE
									a.ps_lead_id IN ( ' . implode( ',', $arrintPsLeads ) . ' )
									AND a.action_type_id IN ( ' . implode( ',', $arrintPublishedActionTypeIds ) . ' )
								GROUP BY
									a.ps_lead_id
							) AS subq ON ( a.id = subq.action_id )';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByMassEmailId( $intMassEmailId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						actions
					WHERE
						secondary_reference = ' . ( int ) $intMassEmailId . '
						AND action_type_id = ' . CActionType::MASS_EMAIL . ';';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedActionsByPsLeadId( $intPsLeadId, $objDatabase, $arrstrPsLeadActivityFilter = NULL ) {

		if( !valId( $intPsLeadId ) ) {
			return NULL;
		}

		$strWhereClause = self::loadWhereClauseForPaginatedActionsByPsLeadId( $arrstrPsLeadActivityFilter );

		$intOffset	= ( false == empty( $arrstrPsLeadActivityFilter['page_no'] ) && false == empty( $arrstrPsLeadActivityFilter['page_size'] ) ) ? $arrstrPsLeadActivityFilter['page_size'] * ( $arrstrPsLeadActivityFilter['page_no'] - 1 ) : 0;
		$intLimit	= ( true == isset( $arrstrPsLeadActivityFilter['page_size'] ) ) ? $arrstrPsLeadActivityFilter['page_size'] : '';

		$strSql = ' SELECT
						a.*
					FROM
						actions AS a
					WHERE
						a.ps_lead_id = ' . ( int ) $intPsLeadId . $strWhereClause . '
					ORDER BY
						a.created_on DESC
					OFFSET ' . ( int ) $intOffset;

		if( false == is_null( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedActionsCountByPsLeadId( $intPsLeadId, $objDatabase, $arrstrPsLeadActivityFilter = NULL ) {
		if( !valId( $intPsLeadId ) ) {
			return NULL;
		}

		$strWhereClause = self::loadWhereClauseForPaginatedActionsByPsLeadId( $arrstrPsLeadActivityFilter );

		$strSql = ' SELECT
						COUNT( a.id )
					FROM
						actions AS a
					WHERE
						a.ps_lead_id = ' . ( int ) $intPsLeadId . $strWhereClause;

		$arrintActivitiesCount = fetchData( $strSql, $objDatabase );

		if( valArr( $arrintActivitiesCount ) ) {
			return $arrintActivitiesCount[0]['count'];
		}

		return 0;
	}

	public static function fetchActionsByActionTypeIdByActionResultIdByPsLeadIds( $intActionTypeId, $intActionResultId, $arrintPsLeadIds, $objDatabase ) {

		if( true == empty( $intActionTypeId ) || true == empty( $intActionResultId ) || false == valArr( $arrintPsLeadIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						actions a
					WHERE
						a.action_type_id 		= ' . ( int ) $intActionTypeId . '
						AND a.action_result_id 	=' . ( int ) $intActionResultId . '
						AND a.ps_lead_id IN ( ' . implode( ',', $arrintPsLeadIds ) . ' )';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchAllActionsByActionTypeIdByActionResultIdByPsLeadId( $intActionTypeId, $intActionResultId, $intPsLeadId, $objDatabase ) {

		if( false == is_numeric( $intActionTypeId ) || false == is_numeric( $intActionResultId ) || false == is_numeric( $intPsLeadId ) ) {
			return false;
		}

		$strSql = 'SELECT
						a.*
					FROM
						actions a
					WHERE
						a.ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.action_result_id = ' . ( int ) $intActionResultId . '
					ORDER BY
						a.created_on DESC';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchAllActionsWithEmployeeNameByActionTypeIdByActionCategoryIdByPsLeadId( $intActionTypeId, $intActionCategoryId, $intPsLeadId, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
						e.preferred_name AS employee_name
					FROM
						actions a
						LEFT JOIN users u ON u.id = a.updated_by
						LEFT JOIN employees e ON e.id = u.employee_id
					WHERE
						a.ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.action_category_id = ' . ( int ) $intActionCategoryId . '
					ORDER BY
						a.created_on DESC';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedActions( $objDatabase, $arrstrActionsFilter = NULL ) {

		$strWhereClause	= '';

		$intOffset		= ( false == empty( $arrstrActionsFilter['page_no'] ) && false == empty( $arrstrActionsFilter['page_size'] ) ) ? $arrstrActionsFilter['page_size'] * ( $arrstrActionsFilter['page_no'] - 1 ) : 0;
		$intLimit		= ( true == isset( $arrstrActionsFilter['page_size'] ) )			? $arrstrActionsFilter['page_size'] : '';

		$strWhereClause	.= ( false == empty( $arrstrActionsFilter['start_datetime'] ) ) 	? ' AND a.created_on >= \'' . $arrstrActionsFilter['start_datetime'] . ' 00:00:00\'' : '';
		$strWhereClause	.= ( false == empty( $arrstrActionsFilter['end_datetime'] ) )		? ' AND a.created_on <= \'' . $arrstrActionsFilter['end_datetime'] . ' 23:59:59\'' : '';

		$strWhereClause	.= ( false == empty( $arrstrActionsFilter['action_type_ids'] ) )		? ' AND a.action_type_id IN ( ' . implode( ',', $arrstrActionsFilter['action_type_ids'] ) . ' ) ' : '';
		$strWhereClause	.= ( false == empty( $arrstrActionsFilter['action_result_ids'] ) )		? ' AND a.action_result_id IN ( ' . implode( ',', $arrstrActionsFilter['action_result_ids'] ) . ' ) ' : '';
		$strWhereClause	.= ( false == empty( $arrstrActionsFilter['ps_lead_ids'] ) )			? ' AND a.ps_lead_id IN ( ' . implode( ',', $arrstrActionsFilter['ps_lead_ids'] ) . ' ) ' : '';
		$strWhereClause	.= ( false == empty( $arrstrActionsFilter['person_ids'] ) )				? ' AND a.person_id IN ( ' . implode( ',', $arrstrActionsFilter['person_ids'] ) . ' )' : '';
		$strWhereClause	.= ( false == empty( $arrstrActionsFilter['contract_ids'] ) )			? ' AND a.contract_id IN ( ' . implode( ',', $arrstrActionsFilter['contract_ids'] ) . ' )' : '';

		if( false == empty( $arrstrActionsFilter['employee_ids'] ) && true == valArr( $arrstrActionsFilter['employee_ids'] ) && false == is_null( $arrstrActionsFilter['employee_ids'][0] ) ) {
			$arrstrActionsFilter['employee_ids'] = array_filter( $arrstrActionsFilter['employee_ids'], 'strlen' );
			$strWhereClause .= ' AND a.employee_id IN ( ' . implode( ',', $arrstrActionsFilter['employee_ids'] ) . ' )';
		}

		$strWhereClause .= ( false == empty( $arrstrActionsFilter['primary_references'] ) )		? ' AND a.primary_reference IN ( ' . implode( ',', $arrstrActionsFilter['primary_references'] ) . ' )' : '';
		$strWhereClause .= ( false == empty( $arrstrActionsFilter['secondary_references'] ) )	? ' AND a.secondary_reference IN ( ' . implode( ',', $arrstrActionsFilter['secondary_references'] ) . ' )' : '';

		$strSql = ' SELECT
						a.*
					FROM
						actions a
					WHERE
						1 = 1 ' . $strWhereClause . '
					ORDER BY
						a.created_on DESC
					OFFSET ' . ( int ) $intOffset;

		if( true == isset( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByContractIdByActionTypeId( $intContractId, $intActionTypeId, $objDatabase, $boolIsLatestAction = false ) {

		$strSelectSql		= 'a.id ';
		$strWhereCondition	= ' AND a.action_result_id = ' . CActionResult::COMPLETED;

		if( true == $boolIsLatestAction ) {
			$strSelectSql		= 'max(a.id) AS id, max( a.action_datetime ) AS action_datetime ';
			$strWhereCondition	= '';
		}

		$strSql = ' SELECT '
						. $strSelectSql .
					'FROM
						actions a
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.contract_id =' . ( int ) $intContractId . $strWhereCondition . '
						LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActionsByCidsByActionTypeIds( $arrintCids, $arrintActionTypeId, $objDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintActionTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.ps_lead_id,
						a.id
					FROM
						actions a
						JOIN clients c ON ( a.ps_lead_id = c.ps_lead_id )
					WHERE
						a.action_type_id IN ( ' . implode( ',', $arrintActionTypeId ) . ' )
						AND c.id IN ( ' . implode( ',', $arrintCids ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActionsByActionResultIdByPersonIds( $intActionResultId, $arrintPersonIds, $objDatabase ) {

		if( true == empty( $intActionResultId ) || false == valArr( $arrintPersonIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						actions a
					WHERE
						a.action_result_id = ' . ( int ) $intActionResultId . '
						AND a.person_id IN ( ' . implode( ',', $arrintPersonIds ) . ' )';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByPersonIds( $arrintPersonIds, $objDatabase ) {

		if( false == valArr( $arrintPersonIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM actions WHERE person_id IN ( ' . implode( ',', $arrintPersonIds ) . ' )';
		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsDetailsByIds( $arrintActionsIds, $objDatabase ) {

		if( false == valArr( $arrintActionsIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.id,
						a.project_title,
						a.projected_end_date
					FROM
						actions a
					WHERE a.id IN ( ' . implode( ',', $arrintActionsIds ) . ' )';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByIds( $arrintActionsIds, $objDatabase ) {
		if( false == valArr( $arrintActionsIds ) ) {
			return NULL;
		}

		return self::fetchActions( 'SELECT id, person_id, ps_lead_id, action_type_id, employee_id, action_datetime FROM actions WHERE id IN ( ' . implode( ',', $arrintActionsIds ) . ' )', $objDatabase );
	}

	public static function fetchActionsCountByMassEmailIds( $arrintMassEmailIds, $objDatabase ) {
		if( false == valArr( $arrintMassEmailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						me.id as mass_email_id,
						( SELECT count(id) FROM actions a WHERE me.id = a.secondary_reference AND a.action_result_id = ' . CActionResult::EMAIL_RETURNED . ' ) interested_count,
						( SELECT count(id) FROM actions a WHERE me.id = a.secondary_reference AND a.action_result_id = ' . CActionResult::NO_RESPONSE . ' ) non_interested_count
					FROM
						mass_emails me
					WHERE
						me.id IN ( ' . implode( ',', $arrintMassEmailIds ) . ' )';

		$arrstrData = fetchData( $strSql, $objDatabase );

		$arrstrReformattedData = [];

		if( true == valArr( $arrstrData ) ) {
			foreach( $arrstrData as $strData ) {
				$arrstrReformattedData[$strData['mass_email_id']]['interested_count'] = $strData['interested_count'];
				$arrstrReformattedData[$strData['mass_email_id']]['non_interested_count'] = $strData['non_interested_count'];
			}
		}

		return $arrstrReformattedData;
	}

	public static function fetchSimpleActionTotalsByPsLeadFilter( $objPsLeadFilter, $objDatabase ) {

		if( 0 == strlen( trim( $objPsLeadFilter->getActionTypes() ) ) ) {
			return NULL;
		}
		if( 0 == strlen( trim( $objPsLeadFilter->getPersons() ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						count( a.id ) as contact_count,
						person_id
					FROM
						actions a
					WHERE
						person_id IN ( ' . $objPsLeadFilter->getPersons() . ' )
						AND action_type_id IN ( ' . $objPsLeadFilter->getActionTypes() . ' )
						AND action_datetime >= \'' . $objPsLeadFilter->getActionStartDate() . '\'
					GROUP BY
						person_id ';

		$arrstrActionTotals = fetchData( $strSql, $objDatabase );
		$arrstrRekeyedActionTotals = [];

		if( true == valArr( $arrstrActionTotals ) ) {
			foreach( $arrstrActionTotals as $arrstrActionTotal ) {
				$arrstrRekeyedActionTotals[$arrstrActionTotal['person_id']] = $arrstrActionTotal;
			}
		}

		return $arrstrRekeyedActionTotals;
	}

	public static function fetchLatestActionBycontractIds( $arrintContractIds, $objDatabase ) {
		if( false == valArr( $arrintContractIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT ON ( a.contract_id ) a.*
					FROM
						actions a
					WHERE
						a.contract_id IN ( ' . implode( ',', $arrintContractIds ) . ' )
						AND a.action_type_id = ' . CActionType::NOTE . '
					ORDER BY
						a.contract_id,
						a.created_on DESC';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByActionResultIdBySecondaryReference( $intActionResultId, $intSecondaryReference, $objDatabase ) {

		$strSql = ' SELECT * FROM actions WHERE action_result_id = ' . ( int ) $intActionResultId . ' AND secondary_reference = ' . ( int ) $intSecondaryReference;

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchDistinctActionsByPersonIds( $arrintPersonIds, $objDatabase ) {
		if( false == valArr( $arrintPersonIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON (person_id)
						a.*
					FROM
						actions a,
	 					mass_emails me
					WHERE
						a.primary_reference IS NOT NULL
						AND me.id = a.secondary_reference
						AND me.mass_email_type_id = ' . CMassEmailType::PEEP_EMAILS . '
						AND a.action_type_id NOT IN ( ' . CActionType::ENTRATA_AD . ' )
					AND a.person_id IN ( ' . implode( ',', $arrintPersonIds ) . ' )
				ORDER BY
						a.person_id,
						a.id desc';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeActionsCountByCreatedBy( $intCreatedBy, $objDatabase ) {
		$strSql = 'SELECT employee_id, count( id ) OVER ( PARTITION BY employee_id ) AS actions_count FROM actions WHERE created_by = ' . ( int ) $intCreatedBy . 'and action_result_id NOT IN ( ' . CActionResult::LIKE . ',' . CActionResult::ATTENDANCE_REMARK . ' ) AND action_type_id = ' . CActionType::EMPLOYEE;

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeActionsByCreatedByByEmployeeId( $intCreatedBy, $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						a.*,
						ar.name as action_result_name
					FROM
						actions a
						JOIN action_results ar ON ( a.action_result_id = ar.id )
					WHERE
						created_by = ' . ( int ) $intCreatedBy . '
						AND employee_id = ' . ( int ) $intEmployeeId . '
						AND a.action_type_id = ' . CActionType::EMPLOYEE . '
						AND a.action_result_id NOT IN ( ' . CActionResult::LIKE . ', ' . CActionResult::ATTENDANCE_REMARK . ' )
					ORDER BY
							id DESC ';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeLikeNoteByCreatedByByEmployeeId( $intCreatedBy, $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						a.*
					FROM
						actions a
						JOIN action_results ar ON ( a.action_result_id = ar.id )
					WHERE
						created_by = ' . ( int ) $intCreatedBy . '
						AND employee_id = ' . ( int ) $intEmployeeId . '
						AND a.action_type_id = ' . CActionType::EMPLOYEE . '
						AND a.action_result_id = ' . CActionResult::LIKE . '
					ORDER BY
						id DESC
					LIMIT 1';
		return self::fetchAction( $strSql, $objDatabase );
	}

	public static function fetchEmployeeActionsByCreatedBy( $intCreatedBy, $objDatabase ) {

		return self::fetchActions( sprintf( 'SELECT
												a.*,
												ar.name,
												ac.name AS category_name
											FROM
												actions a
												LEFT JOIN action_results ar ON a.action_result_id = ar.id
												LEFT JOIN action_categories ac ON a.action_category_id = ac.id
												JOIN employees e ON a.employee_id = e.id
											WHERE
												a.action_type_id =' . CActionType::EMPLOYEE . '  
												AND ( a.action_result_id NOT IN( ' . CActionResult::LIKE . ',' . CActionResult::ATTENDANCE_REMARK . ' ) OR a.action_category_id = ' . CActionCategory::ID_GENERAL . ' )
												AND a.created_by = %d', ( int ) $intCreatedBy ), $objDatabase );
	}

	public static function fetchAllPaginatedActionsByCreatedBy( $intCreatedBy, $intPageNo, $intPageSize, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strOrderBy = '';

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY id DESC';
		}

		$strSql = 'SELECT
						a.*,
						ar.name,
						ac.name AS category_name,
						e1.preferred_name AS employee_name
					FROM
						actions a
						LEFT JOIN action_results ar ON a.action_result_id = ar.id
						LEFT JOIN action_categories ac ON a.action_category_id = ac.id
						JOIN employees e ON a.employee_id = e.id
						JOIN users u on a.created_by = u.id
						JOIN employees e1 on e1.id = u.employee_id
					WHERE
						a.action_type_id = ' . CActionType::EMPLOYEE . '
						AND ( a.action_result_id IN ( ' . CActionResult::EMPLOYEE_APPLICATION . ', ' . CActionResult::REVIEW . ' ) OR a.action_category_id = ' . CActionCategory::ID_GENERAL . ' )
						AND a.created_by = ' . ( int ) $intCreatedBy . ' ' . $strOrderBy . '
						OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedActionsByCreatedByByEmployeeId( $intCreatedBy, $intEmployeeId, $intPageNo, $intPageSize, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL, $boolShowAllNotes ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strOrderBy = '';

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY id DESC';
		}

		$strCondition = ( false == $boolShowAllNotes ) ? 'AND a.created_by = ' . ( int ) $intCreatedBy : '';

		$strSql = 'SELECT
						a.*,
						ar.name,
						ac.name AS category_name,
						a.employee_id,
						e1.preferred_name AS employee_name
					FROM
						actions a
						LEFT JOIN action_results ar ON a.action_result_id = ar.id
						LEFT JOIN action_categories ac ON a.action_category_id = ac.id
						JOIN employees e ON a.employee_id = e.id
						JOIN users u on a.created_by = u.id
						JOIN employees e1 on e1.id = u.employee_id
					WHERE
						a.action_type_id = ' . CActionType::EMPLOYEE . $strCondition . '
						AND ( a.action_result_id IN ( ' . CActionResult::EMPLOYEE_APPLICATION . ', ' . CActionResult::REVIEW . ' ) OR a.action_category_id = ' . CActionCategory::ID_GENERAL . ' )
						AND a.employee_id = ' . ( int ) $intEmployeeId . $strOrderBy . '
						OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchSearchedActions( $arrstrFilteredExplodedSearch, $intUserId, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
						n.name,
						ac.name AS category_name,
						e.name_full
					FROM
						actions a
						LEFT JOIN action_results n ON a.action_result_id = n.id
						LEFT JOIN action_categories ac ON a.action_category_id = ac.id
						JOIN employees e ON a.employee_id = e.id
					WHERE
						a.created_by= ' . ( int ) $intUserId . '
						AND ( a.action_result_id IN ( ' . CActionResult::EMPLOYEE_APPLICATION . ', ' . CActionResult::REVIEW . ' ) OR a.action_category_id = ' . CActionCategory::ID_GENERAL . ' )
						AND a.action_type_id = ' . CActionType::EMPLOYEE . '
						AND ( n.name ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_full ILIKE \'%' . implode( '%\' AND n.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR ac.name ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
					ORDER By
						id
					LIMIT
						10';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchSearchedSingleActions( $arrstrFilteredExplodedSearch, $intUserId, $intEmployeeId=NULL, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
						n.name,
						ac.name AS category_name,
						e.name_full
					FROM
						actions a
						LEFT JOIN action_results n ON a.action_result_id = n.id
						LEFT JOIN action_categories ac ON a.action_category_id = ac.id
						JOIN employees e ON a.employee_id = e.id
					WHERE
						a.created_by= ' . ( int ) $intUserId . '
						AND a.employee_id=' . ( int ) $intEmployeeId . '
						AND a.action_type_id = ' . CActionType::EMPLOYEE . '
						AND ( a.action_result_id IN ( ' . CActionResult::EMPLOYEE_APPLICATION . ', ' . CActionResult::REVIEW . ' ) OR a.action_category_id = ' . CActionCategory::ID_GENERAL . ' )
						AND ( n.name ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_full ILIKE \'%' . implode( '%\' AND n.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' OR ac.name ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\')
					ORDER By
						id
					LIMIT
						10';
		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeeLikesByEmployeeId( $intPageNo, $intPageSize, $intEmployeeId, $objDatabase, $strPageOrderBy, $strPageOrderType ) {

		if( false == is_numeric( $intPageNo ) || false == is_numeric( $intPageSize ) || false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$intOffset = ( 1 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
						a.*,
						e.name_full as employee_name
					FROM
						actions a
						JOIN users u ON u.id = a.created_by
						JOIN employees e ON u.employee_id = e.id
					WHERE
						a.employee_id = ' . ( int ) $intEmployeeId . '
						AND a.action_result_id = ' . CActionResult::LIKE . '
					ORDER BY ' . $strPageOrderBy . ' ' . $strPageOrderType . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeLikeCountByEmployeeId( $intEmployeeId, $objDatabase ) {
		if( true == is_null( $intEmployeeId ) || false == is_numeric( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
							COUNT(a.id)
						FROM
							actions a
						WHERE
							a.employee_id = ' . ( int ) $intEmployeeId . '
							AND a.action_result_id = ' . CActionResult::LIKE;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
	}

	public static function fetchEmployeeAttendanceRemarkNotesByDateRange( $strBeginDate, $strEndDate, $objDatabase, $intEmployeeId = NULL ) {
		$strSql = 'SELECT
						a.id,
						a.employee_id,
						a.notes,
						date(a.action_datetime) as action_datetime
					FROM
						actions a
					WHERE
						a.action_datetime BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\'
						AND a.action_result_id = ' . CActionResult::ATTENDANCE_REMARK;

		if( true == is_numeric( $intEmployeeId ) ) {
			$strSql .= ' AND a.employee_id = ' . ( int ) $intEmployeeId;
		}

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByPsLeadIdByDepartmentIds( $intPsLeadId, $arrintDepartmentIds, $objDatabase ) {

		$strSql = '	SELECT
						a.*,
						e.department_id
					FROM
						actions a
						LEFT JOIN employees e ON ( a.employee_id = e.id )
					WHERE
						a.ps_lead_id = ' . ( int ) $intPsLeadId . ' AND
						a.action_type_id IN ( ' . CActionType::DEMO . ', ' . CActionType::FOLLOW_UP . ' ) AND
						a.action_result_id IS NULL AND
						a.contract_id IS NULL AND
						e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )';

		return self::fetchActions( $strSql, $objDatabase );

	}

	public static function fetchLatestActionsByActionTypeIdByPsLeadIdByPersonIds( $intActionTypeId, $intPsLeadId, $arrintPersonIds, $objDatabase ) {

		if( true == empty( $intActionTypeId ) || true == empty( $intPsLeadId ) || false == valArr( $arrintPersonIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT *
					FROM (
							SELECT
			 					a.notes,
								a.person_id ,
								rank() OVER(PARTITION BY a.person_id ORDER BY a.action_datetime DESC, a.action_result_id NULLS FIRST, a.id) AS rank
							FROM actions a
								JOIN employees e ON (a.employee_id = e.id)
								LEFT JOIN contracts c ON (a.contract_id = c.id)
							WHERE a.ps_lead_id IS NOT NULL
								AND a.ps_lead_id = ' . ( int ) $intPsLeadId . '
								AND a.action_type_id = ' . ( int ) $intActionTypeId . '
								AND a.person_id IN ( ' . implode( ',', $arrintPersonIds ) . ' )
								AND a.notes NOT ILIKE \'%Manually created adjustment log%\'
						) AS sub
					WHERE sub.rank = 1';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByActionTypeIdSecondaryReferenceId( $intActionTypeId, $intSecondaryRefId, $objDatabase ) {

		if( true == is_null( $intSecondaryRefId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						a.id,
						a.primary_reference,
						a.secondary_reference,
						a.action_description,
						a.notes
					FROM
						actions a
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND( a.secondary_reference = ' . ( int ) $intSecondaryRefId . '
						OR a.id = ' . ( int ) $intSecondaryRefId . ' )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchActionsCountByActionTypeIdByEmployeeId( $objPsLeadFilter, $objDatabase ) {

		if( 'week' == $objPsLeadFilter->getDisplayType() ) {
			$strDateLogic = 'DATE_TRUNC ( \'week\', a.action_datetime::DATE + 1 )::DATE - 1';
			$strPeriod = 'week';
		}
		else {
			$strDateLogic = 'DATE_TRUNC ( \'month\', a.action_datetime )';
			$strPeriod = 'month';
		}

		$strSql					= '';
		$boolIsAppendConditions	= false;

		// for getting completed demos and follow up count
		if( 0 < strlen( $objPsLeadFilter->getActionResults() ) && false == in_array( $objPsLeadFilter->getActionResults(), [ CActionResult::REMOTE, CActionResult::OFFICE, CActionResult::MEAL, CActionResult::EVENT ] ) ) {
			$boolIsAppendConditions = true;
		}

		if( true == $boolIsAppendConditions ) {
			$strSql .= ' SELECT
								SUM( sub_sql.action_count ) AS action_count,
								sub_sql.id
							FROM ( ';
		}

		$strSql .= 'SELECT
						count ( a.id ) as action_count,
						pl.id';

		if( true == $boolIsAppendConditions ) {
			$strSql .= ', RANK() OVER(	PARTITION BY DATE_TRUNC(\'month\', a.action_datetime),
										a.action_type_id, a.action_result_id, a.employee_id,
										a.ps_lead_id
										ORDER BY
										a.id
									)';
		}

		$strSql .= ' FROM
						actions a
						JOIN ps_leads pl ON ( a.ps_lead_id = pl.id AND pl.company_status_type_id NOT IN ( ' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEMPLATE . ' ) )
						JOIN employees e ON( a.employee_id = e.id )
						JOIN
							(
							SELECT
								max ( ' . $strPeriod . ' ) AS ' . ( $strPeriod ) . ' ,
								max ( employee_id ) AS employee_id
							FROM
								stats_sales_closer_employees';
		if( false == is_null( $objPsLeadFilter->getActionEmployees() ) ) {
			$strSql .= ' WHERE employee_id = ' . $objPsLeadFilter->getActionEmployees();
		}

		$strSql .= ' ) AS ssce ON ( a.employee_id = ssce.employee_id )';
		if( false == is_null( $objPsLeadFilter->getActionTypes() ) || is_null( $objPsLeadFilter->getActionTypes() ) ) {
			$strSql .= 'WHERE 1 = 1 ';
			if( false == is_null( $objPsLeadFilter->getActionTypes() ) ) {
				$strSql .= ' AND a.action_type_id IN ( ' . $objPsLeadFilter->getActionTypes() . ' ) AND a.employee_id = ' . $objPsLeadFilter->getActionEmployees();
			}
		}
		if( true == $boolIsAppendConditions ) {

			$strSql .= ' AND ' . $strDateLogic . ' <= ssce.' . $strPeriod;

			if( 0 < strlen( trim( $objPsLeadFilter->getActionStartDate() ) ) ) {
				$strSql .= ' AND a.action_datetime IS NOT NULL AND ' . $strDateLogic . ' >= \'' . $objPsLeadFilter->getActionStartDate() . '\' ';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getActionEndDate() ) ) ) {
				$strSql .= ' AND a.action_datetime IS NOT NULL AND ' . $strDateLogic . ' <= \'' . $objPsLeadFilter->getActionEndDate() . '\' ';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getResultStartDate() ) ) ) {
				$strSql .= ' AND a.action_datetime IS NOT NULL AND ' . $strDateLogic . ' >= \'' . $objPsLeadFilter->getResultStartDate() . '\' ';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getResultEndDate() ) ) ) {
				$strSql .= ' AND a.action_datetime IS NOT NULL AND ' . $strDateLogic . ' <= \'' . $objPsLeadFilter->getResultEndDate() . '\' ';
			}
			if( 0 < strlen( $objPsLeadFilter->getActionResults() ) ) {
				$strSql .= ' AND a.action_result_id IN ( ' . $objPsLeadFilter->getActionResults() . ' ) ';
			}
			$strSql .= ' GROUP BY
								pl.id,
								a.id
						) sub_sql
					GROUP BY
							sub_sql.id;';
		} else {
			$strSql .= ' AND ' . $strDateLogic . ' <= ssce.' . $strPeriod;
			if( 0 < strlen( trim( $objPsLeadFilter->getActionStartDate() ) ) ) {
				$strSql .= ' AND a.action_datetime IS NOT NULL AND ' . $strDateLogic . ' >= \'' . $objPsLeadFilter->getActionStartDate() . '\' ';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getActionEndDate() ) ) ) {
				$strSql .= ' AND a.action_datetime IS NOT NULL AND ' . $strDateLogic . ' <= \'' . $objPsLeadFilter->getActionEndDate() . '\' ';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getResultStartDate() ) ) ) {
				$strSql .= ' AND a.action_datetime IS NOT NULL AND ' . $strDateLogic . ' >= \'' . $objPsLeadFilter->getResultStartDate() . '\' ';
			}
			if( 0 < strlen( trim( $objPsLeadFilter->getResultEndDate() ) ) ) {
				$strSql .= ' AND a.action_datetime IS NOT NULL AND ' . $strDateLogic . ' <= \'' . $objPsLeadFilter->getResultEndDate() . '\' ';
			}
			if( 0 < strlen( $objPsLeadFilter->getActionResults() ) ) {
				$strSql .= ' AND a.action_result_id IN ( ' . $objPsLeadFilter->getActionResults() . ' ) ';
			}
			$strSql .= ' GROUP BY
						pl.id';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestImplementationDelayTypeByContractId( $arrstrContractFilter, $objAdminDatabase ) {

		$strSql = 'SELECT';

		if( false == $arrstrContractFilter['is_download'] ) {
			$strSql .= ' array_to_string ( array_agg ( DISTINCT idt.name ), \', \' ) AS name,
						max ( a.ps_lead_id ) AS ps_lead_id,';

			$strFromSql = 'JOIN actions a ON ( cp.contract_id = a.contract_id AND cp.implementation_delay_type_id = a.implementation_delay_type_id )';
		} else {
			$strSql .= ' DISTINCT idt.name,
						a1.implementation_delay_type_id,
						a1.ps_lead_id,
						a1.notes,';

			$strFromSql = 'JOIN
							(
							SELECT
								a.implementation_delay_type_id,
								a.contract_id,
								max ( a.id ) AS action_id
							FROM
								actions a
							WHERE
								a.implementation_delay_type_id IS NOT NULL
							GROUP BY
								a.implementation_delay_type_id,
								a.contract_id
							) a ON ( cp.contract_id = a.contract_id AND cp.implementation_delay_type_id = a.implementation_delay_type_id )
							JOIN actions a1 ON ( a.action_id = a1.id )';
		}

		$strSql .= '	cp.contract_id
					FROM
						contract_properties cp
						' . $strFromSql . '
						JOIN implementation_delay_types idt ON ( a.implementation_delay_type_id = idt.id )
					WHERE
						cp.implementation_delay_type_id IS NOT NULL
						AND cp.implementation_delay_type_id IN ( ' . implode( ',', $arrstrContractFilter['delay_type'] ) . ' )
						AND cp.implemented_on IS NULL';

		if( false == $arrstrContractFilter['is_download'] ) {
			$strSql .= ' GROUP BY
							cp.contract_id';
		}

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchLatestActionByActionTypeIdByPsLeadId( $intActionTypeId, $intPsLeadId, $objDatabase ) {
		if( true == empty( $intActionTypeId ) || true == empty( $intPsLeadId ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						a.*
					FROM
						actions a
					WHERE
						a.ps_lead_id IS NOT NULL
						AND a.ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND a.action_type_id = ' . ( int ) $intActionTypeId . '
					ORDER BY
						a.id::BIGINT DESC
					LIMIT 1';

		return self::fetchAction( $strSql, $objDatabase );
	}

	public static function fetchActionsByCIdByActionTypeId( $intCId, $intActionTypeId, $objDatabase ) {

		$strSql = '	SELECT
						a.*
					FROM
						actions a
						JOIN Clients c ON ( c.ps_lead_id = a.ps_lead_id )
					WHERE
						c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND c.id = ' . ( int ) $intCId . '
						AND a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.primary_reference = 1
					ORDER BY
						a.updated_on DESC ';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchAllActionsWithEmployeeNameByActionTypeIdByActionCategoryIdByPsLeadIdByAccountId( $intActionTypeId, $intActionCategoryId, $intPsLeadId, $intAccountId, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
						e.preferred_name AS employee_name
					FROM
						actions a
						JOIN users u ON u.id = a.updated_by
						LEFT JOIN employees e ON e.id = u.employee_id
					WHERE
						a.ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.action_category_id = ' . ( int ) $intActionCategoryId . '
						AND a.account_id = ' . ( int ) $intAccountId . '
					ORDER BY
						a.created_on DESC';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByActionTypeIdPrimaryReferenceId( $intActionTypeId, $intPrimaryReferenceId, $objDatabase ) {

		if( false == valId( $intActionTypeId ) || false == valId( $intPrimaryReferenceId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.id,
						coalesce( e.name_first, \'\' )||\' \'||coalesce( e.name_last, \'\' ) AS employee_name,
						a.primary_reference,
						a.action_description,
						a.notes,
						to_char ( a.action_datetime, \'FMMon DD, YYYY. HH12:MI am\' ) as action_datetime
					FROM
						actions a
						JOIN employees e ON ( e.id = a.employee_id )
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.primary_reference = ' . ( int ) $intPrimaryReferenceId . '
					ORDER BY a.created_on DESC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchActionsByPrimaryReferenceByActionTypeId( $intPrimaryReferenceId, $intActionTypeId, $objDatabase ) {

		if( false == valId( $intPrimaryReferenceId ) || false == valId( $intActionTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					 FROM
						actions a
					 WHERE
						a.primary_reference = ' . ( int ) $intPrimaryReferenceId . '
						AND a.action_type_id = ' . ( int ) $intActionTypeId;

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByPrimaryReferenceIdsByActionTypeId( $arrintPrimaryReferenceIds, $intActionTypeId, $objDatabase ) {

		if( false == valArr( $arrintPrimaryReferenceIds ) || false == valId( $intActionTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						actions a
					WHERE
						a.primary_reference IN ( ' . implode( ',', $arrintPrimaryReferenceIds ) . ' )
						AND a.action_type_id = ' . ( int ) $intActionTypeId;

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByActionTypeIdByActionResultId( $intActionTypeId, $intActionResultId, $objDatabase ) {

		if( false == is_numeric( $intActionTypeId ) || false == is_numeric( $intActionResultId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.*,
						e.name_full AS assigned_employee_name,
						e.email_address AS assigned_employee_email_address,
						e1.department_id,
						e1.name_full AS selected_employee_name,
						e1.office_id,
						e1.office_desk_id,
						e1.desk_allocation_office_desk_id
					FROM
						actions a
						LEFT JOIN employees e ON ( a.primary_reference = e.id )
						LEFT JOIN employees e1 ON ( a.employee_id = e1.id )
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.action_result_id = ' . ( int ) $intActionResultId . '
						AND action_datetime > NOW() - INTERVAL \'24 hours\'
						AND action_datetime < NOW()';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomActionByActionTypeIdByActionResultIdByPsLeadId( $intActionTypeId, $arrintActionResultIds, $intPsLeadId, $objDatabase, $boolListAll = false, $strActionDate = NULL ) {

		$strWhereCondition = '';
		if( true == valStr( $strActionDate ) ) {
			$strWhereCondition = ' AND a.action_datetime::DATE =\'' . $strActionDate . '\'::DATE';
		}

		if( false == $boolListAll ) {
			$strWhereCondition = ' AND a.updated_on::DATE BETWEEN updated_pld.current_renewal_date::DATE - INTERVAL \'120 DAYS\' AND updated_pld.current_renewal_date::DATE ';
		}

		$strSql = 'SELECT
						*
					FROM
						actions a
						JOIN
						(
						SELECT
							ps_lead_id,
							renewal_date,
							CASE
								WHEN to_char ( ( renewal_date || \'/\' || EXTRACT ( YEAR
						FROM
							NOW ( ) ) )::DATE, \'MM\' )::INTEGER >= EXTRACT ( MONTH
						FROM
							NOW ( ) )::INTEGER THEN renewal_date || \'/\' || EXTRACT ( YEAR
						FROM
							NOW ( ) )
							ELSE renewal_date || \'/\' || EXTRACT ( YEAR
						FROM
							NOW ( ) + INTERVAL \'1 YEAR\' )
							END AS current_renewal_date
						FROM
							ps_lead_details
						WHERE
							renewal_date IS NOT NULL
						) AS updated_pld ON updated_pld.ps_lead_id = a.ps_lead_id
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.action_result_id IN ( ' . implode( ',', $arrintActionResultIds ) . ' )
						AND a.ps_lead_id = ' . ( int ) $intPsLeadId . $strWhereCondition;

		return self::fetchAction( $strSql, $objDatabase );
	}

	public static function fetchActionsByActionTypeIdsByContractId( $intContractId, $intDaysFromLastStatusUpdated, $objDatabase ) {

		if( false == is_numeric( $intContractId ) ) {
			return NULL;
		}

		if( true == is_numeric( $intDaysFromLastStatusUpdated ) ) {
			$intDaysFromLastStatusUpdated = $intDaysFromLastStatusUpdated + 1;
		}

		$strSql = 'SELECT
						a.action_type_id,
						a.action_result_id
					FROM
						actions a
					WHERE
						a.contract_id =' . ( int ) $intContractId . '
						AND a.action_type_id IN ( ' . implode( ',', CActionType::$c_arrintLikelihoodSettingsActionTypeIds ) . ' )
						AND ( a.action_result_id IN( ' . implode( ',', CActionResult::$c_arrintLikelihoodSettingsActionResultIds ) . ' ) OR a.action_result_id IS NULL )
						AND a.action_datetime > ( CURRENT_DATE - INTERVAL \'' . ( int ) $intDaysFromLastStatusUpdated . ' day\' )::date
					GROUP BY
						a.action_type_id,
						a.action_result_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActionForlatestContactstatusChangedByContractIdByPsLeadId( $intContractId, $intPsLeadId, $objDatabase ) {

		if( false == is_numeric( $intContractId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.action_datetime
					FROM
						actions a
					WHERE
						a.contract_id = ' . ( int ) $intContractId . '
						AND a.action_type_id IN ( ' . implode( ',', CActionType::$c_arrintStatusChangeActionTypeIds ) . ' )
						AND ps_lead_id = ' . ( int ) $intPsLeadId . '
					ORDER BY
						a.action_datetime DESC
						LIMIT 1';

		return CActions::fetchAction( $strSql, $objDatabase );
	}

	public static function fetchCustomActionsByActionTypeIdByPsLeadId( $intActionTypeId, $intPsLeadId, $objDatabase ) {

		if( false == is_numeric( $intPsLeadId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						actions a
						JOIN
						(
						SELECT
							ps_lead_id,
							renewal_date,
							CASE
								WHEN to_char ( ( renewal_date || \'/\' || EXTRACT ( YEAR
						FROM
							NOW ( ) ) )::DATE, \'MM\' )::INTEGER >= EXTRACT ( MONTH
						FROM
							NOW ( ) )::INTEGER THEN renewal_date || \'/\' || EXTRACT ( YEAR
						FROM
							NOW ( ) )
							ELSE renewal_date || \'/\' || EXTRACT ( YEAR
						FROM
							NOW ( ) + INTERVAL \'1 YEAR\' )
							END AS current_renewal_date
						FROM
							ps_lead_details
						WHERE
							renewal_date IS NOT NULL
						) AS updated_pld ON updated_pld.ps_lead_id = a.ps_lead_id
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND a.updated_on::DATE BETWEEN updated_pld.current_renewal_date::DATE - INTERVAL \'120 DAYS\' AND updated_pld.current_renewal_date::DATE';

		return CActions::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionsByActionTypeIdByActionResultIdsByPsLeadId( $intActionTypeId, $arrintActionResultIds, $intPsLeadId, $objDatabase ) {

		if( true == empty( $intActionTypeId ) || true == empty( $intPsLeadId ) || false == valArr( $arrintActionResultIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						actions a
						JOIN
						(
							SELECT
								ps_lead_id,
								renewal_date,
								CASE
									WHEN to_char ( ( renewal_date || \'/\' || EXTRACT ( YEAR
							FROM
								NOW ( ) ) )::DATE, \'MM\' )::INTEGER >= EXTRACT ( MONTH
							FROM
								NOW ( ) )::INTEGER THEN renewal_date || \'/\' || EXTRACT ( YEAR
							FROM
								NOW ( ) )
								ELSE renewal_date || \'/\' || EXTRACT ( YEAR
							FROM
								NOW ( ) + INTERVAL \'1 YEAR\' )
								END AS current_renewal_date
							FROM
								ps_lead_details
							WHERE
								renewal_date IS NOT NULL
						) AS updated_pld ON updated_pld.ps_lead_id = a.ps_lead_id
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.action_result_id IN ( ' . implode( ',', $arrintActionResultIds ) . ' )
						AND a.ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND a.updated_on::DATE BETWEEN updated_pld.current_renewal_date::DATE - INTERVAL \'120 DAYS\' AND updated_pld.current_renewal_date::DATE ';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionByActionTypeIdByActionResultIdPrimaryReferenceId( $intActionTypeId, $intActionResultId, $intMassEmailId, $objDatabase, $boolIsFromDelinquencyDashboard = false ) {

		if( true == empty( $intMassEmailId ) || true == empty( $intActionTypeId ) || true == empty( $intActionResultId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						actions a
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.action_result_id = ' . ( int ) $intActionResultId . '
						AND a.primary_reference = ' . ( int ) $intMassEmailId;

		if( true == $boolIsFromDelinquencyDashboard ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchAction( $strSql, $objDatabase );
		}
	}

	public static function fetchActionByUserIdByPrimaryReferenceIdByActionTypeIdBySecondaryReferenceId( $intUserId, $intPrimaryReferenceId, $intActionTypeId, $intSecondaryReferenceId, $objDatabase ) {

		if( false == is_numeric( $intUserId ) || false == is_numeric( $intPrimaryReferenceId ) || false == is_numeric( $intActionTypeId ) || false == is_numeric( $intSecondaryReferenceId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.action_datetime,
						u.employee_id,
						u.is_super_user
					FROM
						actions a
						JOIN users u ON u.id = ' . ( int ) $intUserId . '
					WHERE
						a.primary_reference = ' . ( int ) $intPrimaryReferenceId . '
						AND a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.secondary_reference = ' . ( int ) $intSecondaryReferenceId . '
					ORDER BY
						a.created_on DESC LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllowedAllocationMonths( $strStartDate, $objDatabase ) {

		if( true == empty( $strStartDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						month_list.month_date_year AS allowed_month
					FROM
						(
							SELECT
								month_date_year
							FROM
								generate_series ( \'' . $strStartDate . '\'::DATE, \'' . date( 'Y-m-1' ) . '\'::DATE, \'1 month\'::INTERVAL ) month_date_year
						) month_list
						LEFT JOIN actions a ON ( month_list.month_date_year = a.action_datetime AND a.action_type_id = ' . CActionType::RECONCILIATION_BATCH . ' )
					WHERE
						a.action_datetime IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActionsByActionTypeIdByDate( $intActionTypeId, $strStartDate, $objDatabase, $boolOnlyOne = false ) {

		if( true == empty( $intActionTypeId ) || true == empty( $strStartDate ) ) {
			return NULL;
		}

		$strWhereCondition = 'AND a.action_datetime >= \'' . $strStartDate . '\'';

		if( true == $boolOnlyOne ) {
			$strWhereCondition = 'AND a.action_datetime = \'' . $strStartDate . '\'';
		}

		$strSql = 'SELECT
						a.id,
						a.action_datetime::DATE
					FROM
						actions a
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . $strWhereCondition;

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchActionByPrimaryReferencesByActionTypeIdBySecondaryReference( $arrintPrimaryReferenceIds, $intActionTypeId, $intSecondaryReferenceId, $objDatabase ) {

		if( false == valArr( $arrintPrimaryReferenceIds ) || false == is_numeric( $intActionTypeId ) || false == is_numeric( $intSecondaryReferenceId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						primary_reference,
						 max(action_datetime) as action_datetime,
						secondary_reference
					FROM
						actions
					WHERE
						primary_reference IN ( ' . implode( ',', $arrintPrimaryReferenceIds ) . ' )
						AND action_type_id = ' . ( int ) $intActionTypeId . '
						AND secondary_reference = ' . ( int ) $intSecondaryReferenceId . '
					GROUP BY
						primary_reference,
						secondary_reference';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPaginatedActivitiesDetailsByActionTypeId( $objDatabase, $arrstrPaperlessActivityFilter = NULL ) {

		$intOffset			= ( false == empty( $arrstrPaperlessActivityFilter['page_no'] ) && false == empty( $arrstrPaperlessActivityFilter['page_size'] ) ) ? $arrstrPaperlessActivityFilter['page_size'] * ( $arrstrPaperlessActivityFilter['page_no'] - 1 ) : 0;
		$intLimit			= ( false == empty( $arrstrPaperlessActivityFilter['page_size'] ) ) ? $arrstrPaperlessActivityFilter['page_size'] : '';
		$strOrderByField	= ( false == empty( $arrstrPaperlessActivityFilter['order_by_field'] ) ) ? $arrstrPaperlessActivityFilter['order_by_field'] : ' company_name ';
		$strOrderByType		= ( false == empty( $arrstrPaperlessActivityFilter['order_by'] ) ) ? $arrstrPaperlessActivityFilter['order_by'] : ' ASC ';
		$strJoinClause		= '';
		$strWhereClause		= '';

		if( false == empty( $arrstrPaperlessActivityFilter['product_ids'] ) ) {
			$strJoinClause	.= ' LEFT JOIN action_references ar1 ON (ar1.action_id = a.id and ar1.action_reference_type_id = ' . CActionReferenceType::PRODUCT . ' )';
			$strWhereClause	.= ' AND ar1.reference_number IN ( ' . implode( ',', $arrstrPaperlessActivityFilter['product_ids'] ) . ' ) ';
		}

		$strWhereClause	.= ( false == empty( $arrstrPaperlessActivityFilter['action_result_id'] ) ) ? ' AND a.action_result_id = ' . $arrstrPaperlessActivityFilter['action_result_id'] : '';
		$strWhereClause	.= ( false == empty( $arrstrPaperlessActivityFilter['created_on'] ) ) ? ' AND a.created_on >= \'' . $arrstrPaperlessActivityFilter['created_on'] . ' 00:00:00\' AND a.created_on <= \'' . $arrstrPaperlessActivityFilter['created_on'] . ' 23:59:59\'' : '';

		$strSql = '	SELECT
						MAX(subq.note_count) AS note_count,
						MAX(subq.company_name) AS company_name,
						subq.client_id AS client_id,
						MAX(subq.database_id) AS database_id,
 						MAX(subq.company_status) AS company_status,
						MAX( e.name_full ) AS CSM,
		 				MAX( e1.name_full ) AS RVP,
						MAX(subq.ps_lead_id) AS ps_lead_id,
						MAX(subq.property_count) AS property_count,
						SUM(CASE WHEN p.id = ANY( subq.total_property_ids ) THEN p.number_of_units ELSE 0 END) AS units_count,
						MAX(subq.paperless_property_count) AS paperless_property_count,
						SUM(CASE WHEN p.id= ANY( subq.paperless_property_ids ) THEN p.number_of_units ELSE 0 END) AS paperless_unit_count
					FROM(
						SELECT
							c.id AS client_id,
							COUNT(DISTINCT a.id) AS note_count,
							MAX(c.company_name) AS company_name,
							MAX(c.database_id) AS database_id,
							MAX ( cst.name ) AS company_status,
							MAX(c.ps_lead_id) AS ps_lead_id,
							ARRAY_AGG(DISTINCT p.id) as total_property_ids,
							COUNT(DISTINCT p.id) AS property_count,
							COUNT( DISTINCT ar.reference_number ) AS paperless_property_count,
 							ARRAY_AGG(DISTINCT ar.reference_number) as paperless_property_ids
						FROM
							actions a
							JOIN clients c ON (c.ps_lead_id = a.ps_lead_id AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
							JOIN company_status_types cst ON(c.company_status_type_id = cst.id )
							LEFT JOIN action_references ar ON (ar.action_id = a.id and ar.action_reference_type_id = ' . CActionReferenceType::PROPERTY . ' )
							LEFT JOIN properties p ON (p.cid = c.id AND p.is_disabled = 0 AND ( p.termination_date IS NULL OR p.termination_date > NOW ( ) ) ) ' . $strJoinClause . '
						WHERE
							upper(a.project_title) like \'%PAPERLESS%\'
							AND a.action_type_id = ' . CActionType::ADOPTION . $strWhereClause . '
						GROUP BY
							client_id
						) AS subq
						JOIN ps_lead_details pld ON ( pld.ps_lead_id = subq.ps_lead_id )
						LEFT JOIN employees e ON ( e.id = pld.support_employee_id )
						LEFT JOIN employees e1 ON ( e1.id = pld.sales_employee_id )
						LEFT JOIN properties p ON ( ( p.id = ANY( subq.paperless_property_ids ) OR p.id = ANY( subq.total_property_ids ) ) AND p.is_disabled = 0 AND ( p.termination_date IS NULL OR p.termination_date > NOW ( ) ) )
					GROUP BY
						client_id
					ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedActivitiesCount( $objDatabase, $arrstrPaperlessActivityFilter = NULL ) {

		$strJoinClause	= '';
		$strWhereClause	= '';

		if( false == empty( $arrstrPaperlessActivityFilter['product_ids'] ) ) {
			$strJoinClause	.= ' LEFT JOIN action_references ar1 ON (ar1.action_id = a.id and ar1.action_reference_type_id = ' . CActionReferenceType::PRODUCT . ' )';
			$strWhereClause	.= ' AND ar1.reference_number IN ( ' . implode( ',', $arrstrPaperlessActivityFilter['product_ids'] ) . ' ) ';
		}

		$strWhereClause	.= ( false == empty( $arrstrPaperlessActivityFilter['action_result_id'] ) ) ? ' AND a.action_result_id = ' . $arrstrPaperlessActivityFilter['action_result_id'] : '';
		$strWhereClause	.= ( false == empty( $arrstrPaperlessActivityFilter['created_on'] ) ) ? ' AND a.created_on >= \'' . $arrstrPaperlessActivityFilter['created_on'] . ' 00:00:00\' AND a.created_on <= \'' . $arrstrPaperlessActivityFilter['created_on'] . ' 23:59:59\'' : '';

		$strSql = 'SELECT
						COUNT( DISTINCT( c.id ) )
					FROM
						actions a
						JOIN clients c ON ( c.ps_lead_id = a.ps_lead_id AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
						JOIN company_status_types cst ON( c.company_status_type_id = cst.id )
						LEFT JOIN action_references ar ON ( ar.action_id = a.id and ar.action_reference_type_id = ' . CActionReferenceType::PROPERTY . ' )
						LEFT JOIN properties p ON ( p.cid = c.id AND p.is_disabled = 0 AND ( p.termination_date IS NULL OR p.termination_date > NOW ( ) ) ) ' . $strJoinClause . '
					WHERE
						upper( a.project_title ) like \'%PAPERLESS%\'
						AND a.action_type_id = ' . CActionType::ADOPTION . $strWhereClause;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchAdoptionNotesDetails( $intPsLeadId, $objDatabase ) {
		$strSql = '	SELECT
						a.id,
						count ( ar.reference_number ) as property_count,
						to_char( a.created_on ::DATE, \'YYYY/MM/DD\' ) AS created_on,
						to_char( a.updated_on ::DATE, \'YYYY/MM/DD\' ) AS updated_on,
						to_char( a.action_datetime ::DATE, \'YYYY/MM/DD\' ) AS action_datetime,
						to_char( a.projected_end_date ::DATE, \'YYYY/MM/DD\' ) AS projected_end_date,
						MAX(are.name) as status,
						a.action_description,
						a.notes,
						MAX(e.name_full) AS name_full
					FROM
						actions a
						LEFT JOIN action_references ar ON ( ar.action_id = a.id AND ar.action_reference_type_id = 2 )
						JOIN users u ON (u.id =a.updated_by)
						JOIN employees e ON (e.id = u.employee_id)
						JOIN action_results are ON (are.id = a.action_result_id)
					 WHERE
						upper(a.project_title) like \'%PAPERLESS%\'
						AND a.action_type_id = ' . CActionType::ADOPTION . '
						AND	a.ps_lead_id = ' . ( int ) $intPsLeadId . '
					GROUP BY
						a.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLastUpdatedOnByActionTypeId( $intActionTypeId, $objTaskFilter, $strSelectedTab, $boolStandardClient = false, $boolIsProductUpdate = false, $objDatabase ) {

		if( true == empty( $intActionTypeId ) ) {
			return NULL;
		}

		if( false == is_null( $strSelectedTab ) ) {

			switch( $strSelectedTab ) {
				case NULL:
					break;

				case 'coming_soon':
					$strAdditionalFetchClause	= ', (CASE WHEN t.is_highlight_feature IS NULL THEN 0 ELSE t.is_highlight_feature END) AS highlight_feature , pp.name AS ps_product_name ';
					$strOrderClause				= '';
					$strDistinctOnClause		= ' DISTINCT ON ( pp.name, t.id, highlight_feature ) ';
					break;

				case 'near_future':
				case 'under_consideration':
					$strDistinctOnClause	= '';
					$strOrderClause			= '';
					$strDistinctOnClause	= ' DISTINCT ON ( t.task_release_id , pp.name, t.id ) ';
					$strOrderClause			= ' ORDER BY t.task_release_id ASC, pp.name ASC ';
					break;

				default:
					// default.
					break;
			}
		}

		$strFromClause = ' FROM tasks t ';
		if( true == valObj( $objTaskFilter, 'CTaskFilter' ) ) {

			if( false == valStr( $objTaskFilter->getTaskTypes() ) ) {
				$strCaseCondition = ' WHERE t.task_type_id NOT IN ( SELECT id FROM task_types WHERE id NOT IN ( ' . CTaskType::SUPPORT . ', ' . CTaskType::SYSTEM_BUG . ' ) AND is_support = 1 ) ';
			} else {
				$strCaseCondition = ' WHERE 1 = 1 ';
			}
			if( false == is_null( $strSelectedTab ) ) {
				if( true == valStr( $objTaskFilter->getPsProducts() ) && true == valStr( $objTaskFilter->getPsProductOptions() ) ) {

					$arrstrAndSearchParameters[] = ' ( t.ps_product_option_id IN ( ' . $objTaskFilter->getPsProductOptions() . ' ) OR t.ps_product_id IN( ' . $objTaskFilter->getPsProducts() . ' ) AND ( t.ps_product_option_id IN( ' . $objTaskFilter->getPsProductOptions() . ' ) OR t.ps_product_option_id IS NULL ) )';
				} else if( true == valStr( $objTaskFilter->getPsProducts() ) ) {

					$strSql = ' t.ps_product_id IN( ' . $objTaskFilter->getPsProducts() . ' ) ';

					$arrstrAndSearchParameters[] = $strSql;
				}
			} else if( true == valStr( $objTaskFilter->getPsProducts() ) && false == valStr( $objTaskFilter->getPsProductOptions() ) ) {

				$strSql = ' t.ps_product_id IN( ' . $objTaskFilter->getPsProducts() . ' ) ';

				$arrstrAndSearchParameters[] = $strSql;

			} else if( true == valStr( $objTaskFilter->getPsProducts() ) && true == valStr( $objTaskFilter->getPsProductOptions() ) ) {
				$arrstrAndSearchParameters[] = ' ( t.ps_product_option_id IN ( ' . $objTaskFilter->getPsProductOptions() . ' ) OR t.ps_product_id IN( ' . $objTaskFilter->getPsProducts() . ' ) AND ( t.ps_product_option_id IN( ' . $objTaskFilter->getPsProductOptions() . ' ) OR t.ps_product_option_id IS NULL ) )';

			} else if( false == valStr( $objTaskFilter->getPsProducts() ) && true == valStr( $objTaskFilter->getPsProductOptions() ) ) {
				$arrstrAndSearchParameters[] = ' t.ps_product_option_id IN( ' . $objTaskFilter->getPsProductOptions() . ' ) ';
			}

			if( true == valStr( $objTaskFilter->getTaskStatuses() ) ) {

				if( true == valStr( $objTaskFilter->getTaskStatuses() ) ) {
					if( true == $boolStandardClient && true == $boolIsProductUpdate ) {
						$arrstrAndSearchParameters[] = ' CASE WHEN t.task_standard_release_id < ( SELECT id FROM task_releases WHERE cluster_id = ' . ( int ) CCluster::STANDARD . ' AND release_datetime > now() AND is_released = 0 ORDER BY release_datetime ASC LIMIT 1 ) THEN ( t.task_status_id IN ( ' . trim( $objTaskFilter->getTaskStatuses() ) . ' ) ) ELSE t.task_status_id IN ( ' . trim( $objTaskFilter->getTaskStatuses() ) . ', ' . CTaskStatus::COMPLETED . ',' . CTaskStatus::RELEASED_ON_RAPID . ', ' . CTaskStatus::VERIFIED_ON_RAPID . ' ) END';
					} else if( false == $boolStandardClient && true == $boolIsProductUpdate ) {
						$arrstrAndSearchParameters[] = ' CASE WHEN t.task_release_id < ( SELECT id FROM task_releases WHERE cluster_id = ' . ( int ) CCluster::RAPID . ' AND release_datetime > now() AND is_released = 0 ORDER BY release_datetime ASC LIMIT 1 ) THEN ( t.task_status_id IN ( ' . trim( $objTaskFilter->getTaskStatuses() ) . ' ) ) ELSE t.task_status_id IN ( ' . trim( $objTaskFilter->getTaskStatuses() ) . ', ' . CTaskStatus::COMPLETED . ',' . CTaskStatus::RELEASED_ON_RAPID . ', ' . CTaskStatus::VERIFIED_ON_RAPID . ' ) END';
					} else {
						$arrstrAndSearchParameters[] = ' t.task_status_id IN ( ' . trim( $objTaskFilter->getTaskStatuses() ) . ' )';
					}
				}

			}

			if( true == valStr( $objTaskFilter->getTaskReleases() ) ) {

				if( false !== \Psi\CStringService::singleton()->strpos( $objTaskFilter->getTaskReleases(), 'NULL' ) ) {

					$arrstrAndSearchParameters[] = '( ( t.task_release_id IN( ' . trim( $objTaskFilter->getTaskReleases() ) . ' ) OR t.task_standard_release_id IN( ' . trim( $objTaskFilter->getTaskReleases() ) . ' ) OR t.task_release_id IS NULL ) ) ';
				} elseif( 'coming_soon' == $strSelectedTab ) {
					$arrstrAndSearchParameters[] = '( ( t.task_release_id IN( ' . trim( $objTaskFilter->getTaskReleases() ) . ' ) OR t.task_standard_release_id IN( ' . trim( $objTaskFilter->getTaskReleases() ) . ' ) OR pdm_task_release_id IN( ' . trim( $objTaskFilter->getTaskReleases() ) . ' ) ) ) ';
				} else {
					$arrstrAndSearchParameters[] = '(t.task_release_id IN( ' . trim( $objTaskFilter->getTaskReleases() ) . ' ) OR t.task_standard_release_id IN( ' . trim( $objTaskFilter->getTaskReleases() ) . ' ) )';
				}
			}

			if( 1 == $objTaskFilter->getShowNullMarketingDescription() ) {
				$arrstrAndSearchParameters[] = 'rn.description IS NOT NULL';
			}

			if( 1 == $objTaskFilter->getIsPurchasedProducts() && true == is_numeric( $objTaskFilter->getIsInternalRelease() ) && 0 == $objTaskFilter->getIsInternalRelease() ) {
				$arrstrAndSearchParameters[] = '( pp.is_for_sale = 1 OR t.ps_product_id IN ( ' . implode( ',', CPsProduct::$c_arrintReleaseNotesForNonSaleableProducts ) . ' ) ) AND pp.is_published = 1';
			} else if( 1 == $objTaskFilter->getIsPurchasedProducts() ) {
				$arrstrAndSearchParameters[] = 'pp.is_for_sale = 1 AND pp.is_published = 1';
			}

			if( 1 == $objTaskFilter->getIsPublished() ) {
				$arrstrAndSearchParameters[] = 't.is_published = 1 ';
			}
			if( 1 == $objTaskFilter->getIsUpcomingFeature() ) {
				$arrstrAndSearchParameters[] = 'td.is_release_note_approved = 1';
			}

			if( true == valStr( $objTaskFilter->getIsInternalRelease() ) ) {
				if( 0 == $objTaskFilter->getIsInternalRelease() ) {
					$arrstrAndSearchParameters[] = 't.is_internal_release = 0 AND t.released_externally_on IS NOT NULL';
				} else {
					$arrstrAndSearchParameters[] = 't.is_internal_release =' . ( int ) $objTaskFilter->getIsInternalRelease() . ' AND t.released_internally_on IS NOT NULL';
				}
			}

			if( true == valStr( $objTaskFilter->getTaskImpactTypeIds() ) || true == valStr( $objTaskFilter->getIsReleaseNoteApproved() ) ) {
				$strFromClause .= ' INNER JOIN task_details td ON ( t.id = td.task_id ) ';
			}

			if( false == is_null( $strSelectedTab ) ) {
				$strFromClause .= ' LEFT JOIN ps_product_options ppo ON t.ps_product_option_id = ppo.id ';
			}

			if( true == valStr( $objTaskFilter->getTaskReleaseClient() ) ) {
				$strFromClause .= ' LEFT JOIN task_companies tc ON ( t.id = tc.task_id AND ( t.cid = ' . ( int ) $objTaskFilter->getTaskReleaseClient() . ' OR tc.cid = ' . ( int ) $objTaskFilter->getTaskReleaseClient() . ' ) )';
			}

			if( true == valStr( $objTaskFilter->getIsPurchasedProducts() || 1 == $objTaskFilter->getIsInternalRelease() ) ) {
				$strFromClause .= ' JOIN ps_products pp ON t.ps_product_id = pp.id ';
			}

			if( true == valStr( $objTaskFilter->getTaskTypes() ) ) {
				$strAdditionalConditions .= ' AND t.task_type_id IN ( ' . $objTaskFilter->getTaskTypes() . ' ) ';
			}

			if( true == valStr( $objTaskFilter->getTaskImpactTypeIds() ) ) {
				$strAdditionalConditions .= ' AND td.task_impact_type_id IN ( ' . $objTaskFilter->getTaskImpactTypeIds() . ' ) ';
			}

			if( true == valStr( $objTaskFilter->getIsReleaseNoteApproved() ) ) {
				$strAdditionalConditions .= ' AND td.is_release_note_approved = ' . $objTaskFilter->getIsReleaseNoteApproved();
			}

		}

		$strSql = 'SELECT
						last_updated_on
					FROM
						( SELECT
								t.id,
								ar.updated_on AS last_updated_on,
								row_number ( ) over ( PARTITION BY t.id
														ORDER BY
															ar.updated_on DESC ) AS row_number' .
					$strAdditionalFetchClause . $strFromClause .
					'	LEFT JOIN action_references ar ON ( ar.reference_number = t.id )
						LEFT JOIN actions a ON ( ar.action_id = a.id )
						LEFT JOIN release_notes rn ON ( rn.task_id = t.id )'
					 . $strCaseCondition
					 . ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' : '' )
					 . ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : '' )
					 . $strAdditionalConditions . ' AND a.action_type_id = ' . CActionType::RELEASE_NOTES . ' ) AS sub_query
					WHERE
						row_number = 1
					ORDER BY
						last_updated_on DESC
					LIMIT
						1';

		$arrstrData	= fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['last_updated_on'] : NULL;
	}

	public static function fetchActionsByActionTypeIdBySecondaryReferenceId( $intActionTypeId, $intSecondaryRefId, $objDatabase ) {

		if( true == is_null( $intSecondaryRefId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.*
					FROM
						actions a
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.secondary_reference = ' . ( int ) $intSecondaryRefId;

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchAllAllocationMonths( $objDatabase ) {

		$strSql = 'SELECT
						month_list.month_date_year AS allowed_month
					FROM
						(
							SELECT
								month_date_year
							FROM
								generate_series ( \'' . date( 'Y-m-1', strtotime( '-11 months' ) ) . '\'::DATE, \'' . date( 'Y-m-1' ) . '\'::DATE, \'1 month\'::INTERVAL ) month_date_year
						) month_list';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveAllocationMonthsByActionTypeId( $intActionTypeId, $objDatabase ) {

		if( true == empty( $intActionTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( a.action_datetime ) a.*
					FROM
						actions a
					WHERE
						a.action_type_id = ' . ( int ) $intActionTypeId . '
					ORDER BY
						a.action_datetime,
						a.updated_on DESC';

		return self::fetchActions( $strSql, $objDatabase );
	}

	public static function fetchLatestActionDatesByPsLeadIdsAndByActionTypeId( $arrintPsLeadIds, $intActionTypeId, $objDatabase ) {

		if( false == valArr( $arrintPsLeadIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						ps_lead_id,
						to_char( MAX( action_datetime )::DATE, \'MM/DD/YY\' ) AS action_datetime
					FROM
						actions
					WHERE
						action_type_id =' . ( int ) $intActionTypeId . '
						AND ps_lead_id IN ( ' . implode( ',', $arrintPsLeadIds ) . ' )
					GROUP BY
						ps_lead_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchBillingRequestActionsByBillingRequestId( $intBillingRequestId, $objAdminDatabase, $boolProjectUpdate = false, $boolInternalNote = false ) {

		if( false == valId( $intBillingRequestId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.id AS note_id,
						a.created_on AS date_time,
						a.created_by AS user_id,
						a.created_on,
						a.created_by,
						a.notes AS billing_note,
						CASE
							WHEN e.preferred_name IS NOT NULL THEN e.preferred_name
						ELSE
							( e.name_first || \' \' || e.name_last )
						END AS note_user,
						CASE
						WHEN a.action_type_id = ' . CActionType::BILLING_REQUEST_NOTE . ' THEN
							row_number() over ( partition by a.action_type_id order by a.created_on )
						END AS internal_note_number,
						a.action_type_id
					FROM
						actions a
						JOIN action_references ar ON ( a.id = ar.action_id AND ar.action_reference_type_id = ' . CActionReferenceType::BILLING_REQUEST . ' )
						LEFT JOIN employees e ON ( e.id = a.employee_id )
					WHERE
						ar.reference_number = ' . ( int ) $intBillingRequestId . ' ';

		if( false == $boolProjectUpdate ) {
			$strSql .= ' AND a.action_type_id = ' . CActionType::BILLING_REQUEST_NOTE;
		}

		if( false == $boolInternalNote ) {
			$strSql .= ' AND a.action_type_id != ' . CActionType::BILLING_REQUEST_NOTE;
		}

		$strSql .= ' ORDER BY
						date_time DESC';

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchFavoriteReportActionByActionResultIdByEmployeeId( $intActionResultId, $intEmployeeId, $intCompanyReportId, $objDatabase ) {

		if( false == valId( $intActionResultId ) && false == valId( $intEmployeeId ) && false == valId( $intCompanyReportId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.*
					FROM
						actions a
						JOIN action_references ar ON (ar.action_id = a.id AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' )
					WHERE
						a.action_result_id = ' . ( int ) $intActionResultId . '
						AND ar.reference_number = ' . ( int ) $intCompanyReportId . '
						AND a.employee_id = ' . ( int ) $intEmployeeId;

		return self::fetchAction( $strSql, $objDatabase );
	}

	public static function fetchReportsByActionResultIdByEmployeeIdByGroupIdByDepartmentId( $intActionResultId, $intEmployeeId, $arrintGroupIds, $intDepartmentId, $intLimit, $intOffset, $strSortBy, $intSortOrder, $objDatabase ) {
		$strLimit = '';
		if( true == is_numeric( $intLimit ) ) {
			$strLimit = ' limit ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;
		}

		$strWhereClauseSubQuery = '';
		if( true == valArr( $arrintGroupIds ) ) {
			$strWhereClauseSubQuery .= ' OR group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )';
		}
		if( true == valId( $intDepartmentId ) ) {
			$strWhereClauseSubQuery .= ' OR rp.department_id =' . ( int ) $intDepartmentId;
		}

		if( true == valStr( $strSortBy ) ) {
			$strSortDirection = ( 1 == $intSortOrder ) ? ' DESC' : ' ASC';
			switch( $strSortBy ) {
				case 'name':
					$strOrderClause = ' ORDER BY cr.name' . $strSortDirection;
					break;

				case 'department':
					$strOrderClause = ' ORDER BY departments' . $strSortDirection;
					break;

				case 'tags':
					$strOrderClause = ' ORDER BY tags' . $strSortDirection;
					break;

				case 'owner':
					$strOrderClause = ' ORDER BY owner' . $strSortDirection;
					break;

				default:
					$strOrderClause = ' ORDER BY a.updated_on desc ';
			}
		} else {
			$strOrderClause = ' ORDER BY a.updated_on desc ';
		}

		$strSql = 'SELECT

						cr.id,
						cr.name,
						cr.url,
						cr.description,
						cr.ps_module_id,
						cr.is_shared_access,
						pm.url as ps_module_url,
						a.updated_on,
						array_to_string(array_agg(DISTINCT( d.name ) ),\', \' ) as departments,
						array_to_string(array_agg(DISTINCT ( rda.department_id ) ),\', \' ) as department_ids,
						rsd.query,
						array_to_string(array_agg(DISTINCT( rsdb.database_id ) ),\',\' ) as database_ids,
						( SELECT count( a.id ) FROM actions a JOIN action_references ar ON (ar.action_id = a.id AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' ) where ar.reference_number = cr.id and action_result_id=' . CAction::ACTION_RESULT_VIEW . ' ) as views,
						( SELECT count( a.id ) FROM actions a JOIN action_references ar ON (ar.action_id = a.id AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' ) where ar.reference_number = cr.id and action_result_id=' . CAction::ACTION_RESULT_DOWNLOAD . ' ) as downloads,
						( SELECT count( a.id ) FROM actions a JOIN action_references ar ON (ar.action_id = a.id AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' ) where ar.reference_number = cr.id and action_result_id=' . CAction::ACTION_RESULT_FAVORITE . ' ) as favorites,
						( SELECT count( rc.id ) FROM report_comments rc where rc.company_report_id = cr.id ) as comments,
						( select count( a.id ) from actions a JOIN action_references ar ON (ar.action_id = a.id AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' ) where ar.reference_number = cr.id AND employee_id = ' . ( int ) $intEmployeeId . ' AND action_result_id=' . CAction::ACTION_RESULT_FAVORITE . ' ) as fav_report,
						( SELECT array_to_string(array_agg(t.name),\', \' ) FROM tags_associations ta
								JOIN tags As t ON ( t.id = ta.tag_id )
							WHERE ta.company_report_id = cr.id ) as tags,
						rp.employee_id as owner_id,
						e. preferred_name as owner,
						( SELECT CASE WHEN employee_id = ' . ( int ) $intEmployeeId . ' AND is_primary_owner = true THEN 1
								 WHEN employee_id = ' . ( int ) $intEmployeeId . ' AND is_secondary_owner = true THEN 2
								 WHEN employee_id = ' . ( int ) $intEmployeeId . $strWhereClauseSubQuery . ' THEN 3
								 WHEN NOT EXISTS ( SELECT id FROM report_permissions WHERE is_primary_owner = false AND is_secondary_owner = false AND company_report_id = cr.id ) THEN 3
								 END as permission
						FROM report_permissions rp1
							WHERE rp1.company_report_id = cr.id GROUP BY rp1.department_id, rp1.employee_id,rp1.is_primary_owner,rp1.is_secondary_owner,rp1.group_id ORDER BY permission LIMIT 1 ) AS permission
					FROM
						report_department_associations rda
						LEFT JOIN departments AS d ON ( rda.department_id = d.id )
						RIGHT JOIN company_reports AS cr ON ( rda.company_report_id = cr.id )
						LEFT JOIN action_references AS ar ON ( ar.reference_number = cr.id )
						LEFT JOIN actions a ON ( ar.action_id = a.id AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' )
						LEFT JOIN report_permissions AS rp ON ( cr.id = rp.company_report_id AND rp.is_primary_owner = true )
						LEFT JOIN employees AS e On ( rp.employee_id = e.id )
						LEFT JOIN report_details AS rdd ON ( rdd.company_report_id = cr.id AND rdd.report_data_type_id = ' . CReportDataType::SQL . ' AND rdd.is_current_version = true)
						LEFT JOIN report_sql_details AS rsd ON ( rsd.id = rdd.report_sql_detail_id )
						LEFT JOIN report_sql_databases AS rsdb ON ( rsdb.report_sql_detail_id = rsd.id )
						LEFT JOIN ps_modules pm ON( cr.ps_module_id = pm.id )
					WHERE
						cr.deleted_by IS NULL AND
						a.action_result_id = ' . ( int ) $intActionResultId . '
						AND a.employee_id = ' . ( int ) $intEmployeeId . '
					GROUP BY
						cr.id, cr.name, e.preferred_name, rp.employee_id,a.updated_on,rsd.query,rp.department_id,cr.ps_module_id,pm.url '
					. $strOrderClause . $strLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActionsByActionResultIdByEmployeeIdByCompanyReportId( $intActionResultId, $intEmployeeId, $intCompanyReportId, $objDatabase ) {

		if( false == is_numeric( $intActionResultId ) || false == is_numeric( $intEmployeeId ) || false == is_numeric( $intCompanyReportId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					a.*
				FROM
					actions a
					JOIN action_references ar ON ( ar.action_id = a.id AND ar.action_reference_type_id = ' . CActionReferenceType::REPORT . ' )
				WHERE
					a.action_result_id = ' . ( int ) $intActionResultId . '
					AND a.employee_id = ' . ( int ) $intEmployeeId . '
					AND ar.reference_number = ' . ( int ) $intCompanyReportId;

		return self::fetchAction( $strSql, $objDatabase );
	}

	public static function fetchPreviousRefereeEmployeeId( $intEmployeeApplicationId, $objDatabase ) {
		if( !is_numeric( $intEmployeeApplicationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						primary_reference
					FROM 
						actions
					WHERE 
						action_result_id = ' . ( int ) CActionResult::EMPLOYEE_APPLICATION . ' and action_minutes = ' . ( int ) CEmployeeApplicationSource::EMPLOYEE_REFERRALS . ' and secondary_reference = ' . ( int ) $intEmployeeApplicationId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchContractDraftNoteByContractDraftId( $intContractDraftId, $objDatabase ) {

		$strSql = 'SELECT
						a.id AS action_id,
						a.notes AS note,
						ar.id AS action_reference_id,
						to_char ( a.action_datetime, \'FMMon DD, YYYY. HH12:MI am\' ) AS action_datetime,
						COALESCE( e.name_first, \'\' )||\' \'||COALESCE( e.name_last, \'\' ) AS employee_name
					FROM
						actions a
						JOIN action_references ar ON ( ar.action_id = a.id )
						JOIN employees e ON ( e.id = a.employee_id )
					WHERE
						a.action_type_id = ' . CActionType::NOTE . '
						AND ar.action_reference_type_id = ' . CActionReferenceType::CONTRACT_DRAFT . '
						AND ar.reference_number = ' . ( int ) $intContractDraftId . '
					ORDER BY
						a.id DESC
					LIMIT
						1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllActionsByActionTypeIdByActionResultIdByPsLeadIdWithEmployeeName( $intActionTypeId, $intActionResultId, $intPsLeadId, $objDatabase ) {

		if( !valId( $intPsLeadId ) || !valId( $intActionTypeId ) || !valId( $intActionResultId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.id,
						a.notes,
						a.updated_on,
						e.preferred_name AS employee_name
					FROM
						actions AS a
						JOIN users AS u ON u.id = a.updated_by
						JOIN employees AS e ON e.id = u.employee_id
					WHERE
						a.ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND a.action_type_id = ' . ( int ) $intActionTypeId . '
						AND a.action_result_id = ' . ( int ) $intActionResultId . '
					ORDER BY
						a.updated_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestActionByActionTypeIdByActionResultIdByCidWithEmployeeName( $intActionTypeId, $intActionResultId, $intCid, $objDatabase ) {

		if( !valId( $intActionTypeId ) || !valId( $intActionResultId ) || !valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						a.id,
						a.notes,
						a.updated_on,
						e.preferred_name AS employee_name
					FROM
						actions AS a
						JOIN clients AS c ON ( c.ps_lead_id = a.ps_lead_id )
						JOIN users AS u ON ( u.id = a.updated_by )
						JOIN employees AS e ON ( e.id = u.employee_id )
					WHERE
						c.id = ' . $intCid . '
						AND a.action_type_id = ' . $intActionTypeId . '
						AND a.action_result_id = ' . $intActionResultId . '
					ORDER BY
						a.updated_on DESC
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function loadWhereClauseForPaginatedActionsByPsLeadId( $arrstrPsLeadActivityFilter = NULL ) {

		$strExclude		= 'Manually created adjustment log';
		$strWhereClause	= '';

		$boolHasActionActionResultTypes	= valArr( $arrstrPsLeadActivityFilter['action_result_type_ids'] );
		$boolValidActionTypes			= valArr( $arrstrPsLeadActivityFilter['action_type_ids'] );

		$arrintActionTypes = $arrstrPsLeadActivityFilter['action_type_ids'];
		 // we are making action result as optional if action type is note.
//		if( $boolValidActionTypes && in_array( CActionType::NOTE, $arrintActionTypes ) ) {
//			$arrintActionTypes = array_diff( $arrintActionTypes, [ CActionType::NOTE ] );
//			$boolValidActionTypes = valArr( $arrintActionTypes );
//		}

		$strActionTypeCondition			= $boolValidActionTypes ? ' a.action_type_id IN ( ' . implode( ',', $arrintActionTypes ) . ' )' : '';
		$strActionResultTypeCondition	= $boolHasActionActionResultTypes ? ' a.action_result_id IN ( ' . implode( ',', $arrstrPsLeadActivityFilter['action_result_type_ids'] ) . ' ) ' : '';

		if( $boolValidActionTypes ) {
			if( $boolHasActionActionResultTypes ) {
				$strWhereClause .= ' AND ( ' . $strActionTypeCondition . ' OR ' . $strActionResultTypeCondition . ' )';
			} else {
				$strWhereClause .= ' AND ' . $strActionTypeCondition;
			}
		} elseif( $boolHasActionActionResultTypes ) {
			$strWhereClause .= ' AND ' . $strActionResultTypeCondition;
		}

		if( isset( $arrstrPsLeadActivityFilter['action_category_ids'] ) && valArr( $arrstrPsLeadActivityFilter['action_category_ids'] ) ) {
			$strWhereClause .= ' AND a.action_category_id IN ( ' . sqlIntImplode( $arrstrPsLeadActivityFilter['action_category_ids'] ) . ' )';
		}

		$strWhereClause	.= ( false == empty( $arrstrPsLeadActivityFilter['start_datetime'] ) ) ? ' AND a.created_on >= \'' . $arrstrPsLeadActivityFilter['start_datetime'] . ' 00:00:00\'' : '';
		$strWhereClause	.= ( false == empty( $arrstrPsLeadActivityFilter['end_datetime'] ) ) ? ' AND a.created_on <= \'' . $arrstrPsLeadActivityFilter['end_datetime'] . ' 23:59:59\'' : '';
		$strWhereClause	.= ( false == empty( $arrstrPsLeadActivityFilter['employee_ids'] ) ) ? ' AND a.employee_id IN ( ' . implode( ',', $arrstrPsLeadActivityFilter['employee_ids'] ) . ' )' : '';
		$strWhereClause	.= ( false == empty( $arrstrPsLeadActivityFilter['person_ids'] ) ) ? ' AND a.id IN ( SELECT action_id FROM action_references WHERE reference_number in ( ' . implode( ',', $arrstrPsLeadActivityFilter['person_ids'] ) . ' ) AND action_reference_type_id = ' . CActionReferenceType::PERSON . ' )' : '';
		$strWhereClause	.= ( false == empty( $arrstrPsLeadActivityFilter['contract_ids'] ) ) ? '  AND a.contract_id IN ( ' . implode( ',', $arrstrPsLeadActivityFilter['contract_ids'] ) . ' )' : '';
		$strWhereClause	.= ' AND ( a.notes NOT ILIKE \'%' . $strExclude . '%\' OR a.notes IS NULL ) ';

		return $strWhereClause;
	}

}

?>
