<?php

class CTeamType extends CBaseTeamType {

	const GENERAL 			= 1;
	const OUTBOUND_SUPPORT	= 2;
	const SCRUM_TEAM        = 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default action
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>