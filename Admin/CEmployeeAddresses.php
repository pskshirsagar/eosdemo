<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAddresses
 * Do not add any new functions to this class.
 */

class CEmployeeAddresses extends CBaseEmployeeAddresses {

	public static function fetchEmployeeAddressByEmployeeIdByAddressTypeId( $intEmployeeId, $intAddressTypeId, $objAdminDatabase ) {
		if( false == is_numeric( $intEmployeeId ) || false == is_numeric( $intAddressTypeId ) ) return NULL;
		$strSql = 'SELECT * FROM employee_addresses WHERE employee_id=' . ( int ) $intEmployeeId . ' AND address_type_id = ' . ( int ) $intAddressTypeId . ' LIMIT 1';
		return self::fetchEmployeeAddress( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeAddressesByEmployeeIdsByAddressTypeId( $arrintEmployeeIds, $intAddressTypeId, $objAdminDatabase ) {
		if( false == is_numeric( $intAddressTypeId ) || false == valArr( $arrintEmployeeIds ) ) return NULL;
		$strSql = 'SELECT * FROM employee_addresses WHERE employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND address_type_id = ' . ( int ) $intAddressTypeId;
		return self::fetchEmployeeAddresses( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeAddressesByEmployeeIdByAddressTypeIds( $intEmployeeId, $arrintAddressTypeIds, $objAdminDatabase ) {
		if( false == is_numeric( $intEmployeeId ) || false == valArr( $arrintAddressTypeIds ) ) return NULL;
		$strSql = 'SELECT ea.*, cs.state_code AS country_state_code FROM employee_addresses ea LEFT JOIN country_states cs ON ( cs.id = ea.country_state_id ) WHERE ea.employee_id = ' . ( int ) $intEmployeeId . ' AND ea.address_type_id IN ( ' . implode( ',', $arrintAddressTypeIds ) . ' )';
		return self::fetchEmployeeAddresses( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeAddressByEmployeeId( $intEmployeeId, $objAdminDatabase, $intAddressTypeId = NULL ) {
		if( false == is_numeric( $intEmployeeId ) ) return NULL;
		if( false == is_null( $intAddressTypeId ) ) {
			$strSql = 'SELECT * FROM employee_addresses WHERE employee_id = ' . ( int ) $intEmployeeId . '  AND address_type_id = ' . ( int ) $intAddressTypeId . ' LIMIT 1';
		} else {
			$strSql = 'SELECT * FROM employee_addresses WHERE employee_id = ' . ( int ) $intEmployeeId . '  LIMIT 1';
		}

		return self::fetchEmployeeAddress( $strSql, $objAdminDatabase );
	}

	public static function fetchSimpleEmployeeAddressesByEmployeeIdsByAddressTypeId( $arrintEmployeeIds, $intAddressTypeId, $objAdminDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) || false == is_numeric( $intAddressTypeId ) ) return NULL;

		$strSql = 'SELECT ea.employee_id, ea.country_code FROM employee_addresses ea WHERE ea.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND ea.address_type_id = ' . ( int ) $intAddressTypeId;

		return rekeyArray( 'employee_id', fetchData( $strSql, $objAdminDatabase ) );
	}

	public static function fetchEmployeeCountryCodeByEmployeeIdByAddressTypeId( $intEmployeeId, $intAddressTypeId, $objAdminDatabase ) {
		if( false == is_numeric( $intEmployeeId ) || false == is_numeric( $intAddressTypeId ) ) return NULL;
		$strSql = 'SELECT ea.employee_id, ea.country_code FROM employee_addresses ea WHERE ea.employee_id = ' . ( int ) $intEmployeeId . ' AND ea.address_type_id = ' . ( int ) $intAddressTypeId;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchSimpleEmailAddressesByGroupIdByCountryCode( $intGroupId, $strCountryCode, $objDatabase ) {
		if( false == is_numeric( $intGroupId ) ) return NULL;
		$strSql = ' SELECT
						e.email_address
					FROM
						employees e,
						employee_addresses eaddr,
						users u,
						user_groups ug
					WHERE
						u.employee_id = e.id
						AND ug.user_id = u.id
						AND ug.deleted_by IS NULL
						AND e.employee_status_type_id = \'' . CEmployeeStatusType::CURRENT . '\'
						AND eaddr.employee_id = e.id
						AND ug.group_id = ' . ( int ) $intGroupId;

		if( CCountry::CODE_USA == $strCountryCode ) {
			$strSql .= 'AND eaddr.country_code = \'' . CCountry::CODE_USA . '\'';

		} else {
			$strSql .= 'AND eaddr.country_code = \'' . CCountry::CODE_INDIA . '\'';
		}

		$arrstrEmailAddresses = fetchData( $strSql, $objDatabase );
		$arrstrFinalEmailAddreses = array();

		if( true == valArr( $arrstrEmailAddresses ) ) {
			foreach( $arrstrEmailAddresses as $arrstrEmailAddress ) {
				if( true == CValidation::validateEmailAddresses( $arrstrEmailAddress['email_address'] ) ) {
					$arrstrFinalEmailAddreses[$arrstrEmailAddress['email_address']] = $arrstrEmailAddress['email_address'];
				}
			}
		}

		return $arrstrFinalEmailAddreses;
	}

}
?>