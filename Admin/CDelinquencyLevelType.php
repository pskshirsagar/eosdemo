<?php

class CDelinquencyLevelType extends CBaseDelinquencyLevelType {

	const LEVEL_ZERO		= 0;
	const LEVEL_ONE			= 1;
	const LEVEL_TWO			= 2;
	const LEVEL_THREE		= 3;
	const LEVEL_FOUR		= 4;
	const LEVEL_FIVE		= 5;

	public static $c_arrintCriticalDelinquencyLevelTypeIds = [ self::LEVEL_FOUR, self::LEVEL_FIVE ];
	public static $c_arrintNoticeDelinquencyLevelTypeIds 	= [ self::LEVEL_THREE, self::LEVEL_FOUR, self::LEVEL_FIVE ];

	public function valId() {
		return true;
	}

	public function valName() {
		return true;
	}

	public function valDescription() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valOrderNum() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>