<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationLogs
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationLogs extends CBaseEmployeeApplicationLogs {

	public static function fetchEmployeeApplicationLogByDate( $strDate, $objDatabase, $strField = NULL ) {

		if( false == valStr( $strDate ) ) return NULL;

		$strWhereCondition = ( true == valStr( $strField ) ) ? ' AND ' . $strField . ' IS NOT NULL' : '';

		$strSql = ' SELECT
						*
					FROM
						employee_application_logs
					WHERE
						created_on::DATE = \'' . $strDate . '\'' . $strWhereCondition;

		return parent::fetchEmployeeApplicationLog( $strSql, $objDatabase );
	}

	public static function fetchEmployeeApplicationLogsByDateByCaseForHiring( $strStartDate, $strEndDate, $strCaseForHiring, $objDatabase, $boolIsDownload = false ) {

		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) || false == ( $strCaseForHiring ) ) return NULL;

		$strSql = ' SELECT
						COALESCE ( SUM ( eal.employee_application_email_count ), 0 ) AS employee_application_email_count,
						COALESCE ( SUM ( eal.employee_application_update_count ), 0 ) AS employee_application_update_count,
						COALESCE ( SUM ( eal.employee_application_delete_count ), 0 ) AS employee_application_delete_count,
						(
							SELECT
								count ( ea.id )
							FROM
								employee_applications ea
								JOIN ps_job_posting_step_statuses pjpss ON ( ea.ps_job_posting_step_status_id = pjpss.id )
								LEFT JOIN ps_job_posting_steps pjps ON ( pjps.id = pjpss.ps_job_posting_step_id )
								LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = ea.id )
							WHERE
								ea.country_code = \'' . CCountry::CODE_INDIA . '\'
								AND pjpss.ps_job_posting_status_id = ' . CPsJobPostingStatus::IN_PROCESS . '
								AND pjps.ps_job_posting_step_type_id = ' . CPsJobPostingStepType::OFFER . '
								AND pjps.deleted_on IS NULL
								AND ea.employee_application_status_type_id NOT IN ( ' . implode( ',', array( CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_JOINED, CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_ABSCOND, CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_EXIT_PROCESS, CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_NO_SHOW, CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_RETAINED ) ) . ' )
								AND ead.case_for_hiring ILIKE \'%' . $strCaseForHiring . '%\'
								AND ea.applicant_offerred_on::DATE BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
						 ) AS offer_extended_count
					FROM
						employee_application_logs eal
					WHERE
						eal.created_on::DATE BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'';

		if( true == $boolIsDownload ) {
			return fetchData( $strSql, $objDatabase );
		}

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) && true == isset( $arrmixData[0] ) ) {
			return $arrmixData[0];
		}

		return NULL;
	}

}
?>