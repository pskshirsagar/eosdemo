<?php

class CContractLikelihoodSetting extends CBaseContractLikelihoodSetting {

	protected $m_strContractStatusTypeName;

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}
		return $boolIsValid;
	}

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['contract_status_type_name'] ) ) 	$this->setContractStatusTypeName( $arrmixValues['contract_status_type_name'] );
	}

	public function setContractStatusTypeName( $strContractStatusTypeName ) {
		$this->m_strContractStatusTypeName = CStrings::strTrimDef( $strContractStatusTypeName, 50, NULL, true );
	}

	public function sqlContractStatusTypeName() {
		return ( true == isset( $this->m_strContractStatusTypeName ) ) ? '\'' . addslashes( $this->m_strContractStatusTypeName ) . '\'' : 'NULL';
	}

	/**
	* Get Functions
	*
	*/

	public function getContractStatusTypeName() {
		return $this->m_strContractStatusTypeName;
	}

}
?>