<?php

class CEmployeeApplicationLog extends CBaseEmployeeApplicationLog {

	const HAPPY_TO_CONNECT = 'Happy To Connect';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeApplicationEmailCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeApplicationUpdateCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeApplicationDeleteCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function insertOrUpdateEmployeeApplicationLog( $strField, $intCount, $intUserId, $objDatabase ) {

		$intTotalCount = $intCount;

		if( 'employee_application_email_count' == $strField ) {
			$strMethod = 'EmployeeApplicationEmailCount';
		} elseif( 'employee_application_update_count' == $strField ) {
			$strMethod = 'EmployeeApplicationUpdateCount';
		} elseif( 'employee_application_delete_count' == $strField ) {
			$strMethod = 'EmployeeApplicationDeleteCount';
		}

		$objEmployeeApplicationLog = CEmployeeApplicationLogs::fetchEmployeeApplicationLogByDate( date( 'Y-m-d' ), $objDatabase, $strField );

		if( false == valObj( $objEmployeeApplicationLog, 'CEmployeeApplicationLog' ) ) {
			$objEmployeeApplicationLog = new CEmployeeApplicationLog();
		} else {
			$intTotalCount = $intCount + $objEmployeeApplicationLog->{'get' . $strMethod}();
		}

		$objEmployeeApplicationLog->{'set' . $strMethod}( $intTotalCount );

		if( false == $objEmployeeApplicationLog->insertOrUpdate( $intUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>