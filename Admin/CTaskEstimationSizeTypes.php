<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskEstimationSizeTypes
 * Do not add any new functions to this class.
 */

class CTaskEstimationSizeTypes extends CBaseTaskEstimationSizeTypes {

	public static function fetchTaskEstimationSizeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CTaskEstimationSizeType::class, $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchTaskEstimationSizeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CTaskEstimationSizeType::class, $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static  function fetchAllTaskEstimationSizeTypes( $objAdminDatabase ) {
		$strSql = ' SELECT
						*
					FROM
						task_estimation_size_types';

		return self::fetchTaskEstimationSizeTypes( $strSql, $objAdminDatabase );
	}

}
?>