<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemKeywordAssociations
 * Do not add any new functions to this class.
 */

class CSemKeywordAssociations extends CBaseSemKeywordAssociations {

	public static function fetchSemKeywordAssociationsBySemAdGroupIdByPropertyIdByCid( $intSemAdGroupId, $intPropertyId, $intCid, $objAdminDatbase ) {
		$strSql = 'SELECT * FROM sem_keyword_associations WHERE sem_ad_group_id = ' . ( int ) $intSemAdGroupId . ' AND property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchSemKeywordAssociations( $strSql, $objAdminDatbase );
	}

	public static function fetchSemKeywordAssociationBySemKeywordIdByPropertyIdByCid( $intSemKeywordId, $intPropertyId, $intCid, $objAdminDatbase ) {
		$strSql = 'SELECT * FROM sem_keyword_associations WHERE sem_keyword_id = ' . ( int ) $intSemKeywordId . ' AND property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' LIMIT 1';
		return self::fetchSemKeywordAssociation( $strSql, $objAdminDatbase );
	}

}
?>