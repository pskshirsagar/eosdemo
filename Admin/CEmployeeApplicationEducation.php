<?php

class CEmployeeApplicationEducation extends CBaseEmployeeApplicationEducation {

	public function valName( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );

			return $boolIsValid;
		}

		if( true == isset ( $objDatabase ) ) {
			$intConflictingEmployeeApplicationEducationCount = \Psi\Eos\Admin\CEmployeeApplicationEducations::createService()->fetchEmployeeApplicationEducationsCountByNameByCountryCodeById( $this->getName(), $this->getCountryCode(), $objDatabase, $this->getId() );

			if( 0 < $intConflictingEmployeeApplicationEducationCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Qualification with this name and country already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCountryCode( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == isset( $objDatabase ) ) {
			$intResourceRequisitionCount		= \Psi\Eos\Admin\CResourceRequisitions::createService()->fetchResourceRequisitionCountByRequiredQualification( $this->getId(), $objDatabase );
			$objEmployeeApplicationEducation 	= \Psi\Eos\Admin\CEmployeeApplicationEducations::createService()->fetchEmployeeApplicationEducationById( $this->getId(), $objDatabase );

			if( 0 < $intResourceRequisitionCount && $this->getCountryCode() != $objEmployeeApplicationEducation->getCountryCode() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country code', 'Cannot change the country code because purchase request is associated with it.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valCountryCode( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

}
?>