<?php

class CSemAdGroup extends CBaseSemAdGroup {

	protected $m_strStateCode;
	protected $m_strCampaignName;
	protected $m_intFromAddress;
	protected $m_strStreet;
	protected $m_strCity;
	protected $m_strPostalCode;

	protected $m_fltEstimatedClickCost;

	/**
	 * Create Functions
	 */

	public function createSemKeyword( $objAdminDatabase ) {

		$objSemKeyword = new CSemKeyword();

		$objSemKeyword->setSemCampaignId( $this->getSemCampaignId() );
		$objSemKeyword->setSemAdGroupId( $this->getId() );
		$objSemKeyword->setKeywordDatetime( ' NOW() ' );

		// We need to set the State code from Campaing.
		$objSemCampaign = CSemCampaigns::fetchSemCampaignById( $this->getSemCampaignId(), $objAdminDatabase );

		if( false == valObj( $objSemCampaign, 'CSemCampaign' ) ) {
			trigger_error( 'Unable to load campaign from database.', E_USER_ERROR );
		}

		$objSemKeyword->setStateCode( $objSemCampaign->getStateCode() );

		return $objSemKeyword;
	}

	public function createSemAd() {

		$objSemAd = new CSemAd();

		$objSemAd->setSemCampaignId( $this->getSemCampaignId() );
		$objSemAd->setSemAdGroupId( $this->getId() );
		$objSemAd->setAdDatetime( 'NOW()' );

		return $objSemAd;

	}

	public function createSemAdGroupSource() {

		$objSemAdGroupSource = new CSemAdGroupSource();

		$objSemAdGroupSource->setSemAdGroupId( $this->getId() );

		$objSemAdGroupSource->setSemStatusTypeId( CSemStatusType::PAUSED );
		$objSemAdGroupSource->setMaxCostPerClick( 2 );

		return $objSemAdGroupSource;
	}

	public function createSemAdSource() {

		$objSemAdSource = new CSemAdSource();

		$objSemAdSource->setSemAdGroupId( $this->getId() );

		return $objSemAdSource;
	}

	public function createSemKeywordSource() {

		$objSemKeywordSource = new CSemKeywordSource();

		$objSemKeywordSource->setSemAdGroupId( $this->getId() );

		return $objSemKeywordSource;

	}

	public function createSemKeywordAssociation() {

		$objSemKeywordAssociation = new CSemKeywordAssociation();

		$objSemKeywordAssociation->setSemAdGroupId( $this->getId() );
		$objSemKeywordAssociation->setAssociationDatetime( 'NOW()' );
		$objSemKeywordAssociation->setSemCampaignId( $this->getSemCampaignId() );

		return $objSemKeywordAssociation;

	}

	public function createSemAdGroupAssociation() {

		$objSemAdGroupAssociation = new CSemAdGroupAssociation();

		$objSemAdGroupAssociation->setSemAdGroupId( $this->getId() );
		$objSemAdGroupAssociation->setAssociationDatetime( 'NOW()' );
		$objSemAdGroupAssociation->setSemCampaignId( $this->getSemCampaignId() );

		return $objSemAdGroupAssociation;
	}

	/**
	 * Val Functions
	 */

	public function valSemCampaignId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemCampaignId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_campaign_id', 'Campaign is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSemLocationTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemLocationTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_location_type_id', 'Location type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} else {

			$intRowCount = CSemAdGroups::fetchPreExistingSemAdGroup( $this, $objAdminDatabase );

			if( 0 < $intRowCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Ad group name is already being used.', 319 ) );
			}
		}

		return $boolIsValid;
	}

	public function valLongitude() {
		$boolIsValid = true;

		if( true == is_null( $this->getLongitude() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'longitude', 'Longitude is required.' ) );
		}

		return $boolIsValid;
	}

	public function valLatitude() {
		$boolIsValid = true;

		if( true == is_null( $this->getLatitude() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'latitude', 'Latitude is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction,  $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSemCampaignId();
				$boolIsValid &= $this->valSemLocationTypeId();
				$boolIsValid &= $this->valName( $objAdminDatabase );
				if( 1 == $this->getIsSystem() ) {
					$boolIsValid &= $this->valLongitude();
					$boolIsValid &= $this->valLatitude();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objAdminDatabase, $boolReturnSqlOnly = false ) {
		$boolIsValid = true;

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objAdminDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Set Functions
	 */

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setCampaignName( $strCampaignName ) {
		$this->m_strCampaignName = $strCampaignName;
	}

 	public function setFromAddress( $intFromAddress ) {
 		$this->m_intFromAddress = CStrings::strToIntDef( $intFromAddress, NULL, false );
 	}

 	public function setStreet( $strStreet ) {
		$this->m_strStreet = CStrings::strTrimDef( $strStreet, 50, NULL, true );
 	}

	public function setCity( $strCity ) {
		$this->m_strCity = CStrings::strTrimDef( $strCity, 50, NULL, true );
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = CStrings::strTrimDef( $strPostalCode, 50, NULL, true );
	}

	public function setEstimatedClickCost( $fltEstimatedClickCost ) {
		$this->m_fltEstimatedClickCost = $fltEstimatedClickCost;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['state_code'] ) ) $this->setStateCode( $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['campaign_name'] ) ) $this->setCampaignName( $arrmixValues['campaign_name'] );
		if( true == isset( $arrmixValues['from_address'] ) ) $this->setFromAddress( $arrmixValues['from_address'] );
		if( true == isset( $arrmixValues['street'] ) ) $this->setStreet( $arrmixValues['street'] );
		if( true == isset( $arrmixValues['city'] ) ) $this->setCity( $arrmixValues['city'] );
		if( true == isset( $arrmixValues['postal_code'] ) ) $this->setPostalCode( $arrmixValues['postal_code'] );
		if( true == isset( $arrmixValues['estimated_click_cost'] ) ) $this->setEstimatedClickCost( $arrmixValues['estimated_click_cost'] );
		return;
	}

	/**
	 * Get Functions
	 */

	public function getSeoName() {
		// return \Psi\CStringService::singleton()->strtolower( preg_replace( array( '/[^a-zA-Z0-9\s_'. "'" .'\-]+/', '/(\s|-)+/' ), array( '', '-' ), $this->getName() ));
		return trim( \Psi\CStringService::singleton()->strtolower( preg_replace( array( '/[^a-zA-Z0-9\s_\-]+/', '/(\s|-|_)+/' ), array( '', '-' ), $this->getName() ) ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getUrlFormattedFullStateName() {
		return \Psi\CStringService::singleton()->strtolower( str_replace( ' ', '-', \Psi\Eos\Admin\CStates::createService()->determineStateNameByStateCode( $this->getStateCode() ) ) );
	}

	public function getUrlFormattedName() {
		return \Psi\CStringService::singleton()->strtolower( str_replace( ' ', '-', $this->getName() ) );
	}

	public function getCampaignName() {
		return $this->m_strCampaignName;
	}

	public function getFromAddress() {
		return $this->m_intFromAddress;
	}

	public function getStreet() {
		return $this->m_strStreet;
	}

 	public function getCity() {
		return $this->m_strCity;
 	}

 	public function getPostalCode() {
		return $this->m_strPostalCode;
 	}

	public function getEstimatedClickCost() {
		return $this->m_fltEstimatedClickCost;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchSemKeywordById( $intSemKeywordId, $objAdminDatabase ) {
		return CSemKeywords::fetchSemKeywordBySemAdGroupIdById( $this->getId(), $intSemKeywordId, $objAdminDatabase );
	}

	public function fetchSemKeywords( $objAdminDatabase ) {
		return CSemKeywords::fetchSemKeywordsBySemAdGroupId( $this->getId(), $objAdminDatabase );
	}

	public function fetchSemAds( $objAdminDatabase ) {
		return CSemAds::fetchSemAdsBySemAdGroupId( $this->getId(), $objAdminDatabase );
	}

	public function fetchSemKeywordsByIsSystem( $boolIsSystemKeyword, $objAdminDatabase ) {
		return CSemKeywords::fetchSemKeywordsBySemAdGroupIdByIsSystem( $this->getId(), $boolIsSystemKeyword, $objAdminDatabase );
	}

	public function fetchSemCampaign( $objAdminDatabase ) {
		return CSemCampaigns::fetchSemCampaignById( $this->getSemCampaignId(), $objAdminDatabase );
	}

	public function fetchSemAdGroupSources( $objAdminDatabase ) {
		return CSemAdGroupSources::fetchSemAdGroupSourcesBySemAdGroupId( $this->getId(), $objAdminDatabase );
	}

	public function fetchSemCampaignSourceBySemSourceId( $intSemSourceId, $objAdminDatabase ) {
		return CSemCampaignSources::fetchSemCampaignSourceBySemSourceIdBySemCampignId( $intSemSourceId, $this->getSemCampaignId(), $objAdminDatabase );
	}

	public function fetchSemAdGroupAssociations( $objAdminDatabase ) {
		return CSemAdGroupAssociations::fetchSemAdGroupAssociationsBySemAdGroupId( $this->getId(), $objAdminDatabase );
	}

	public function fetchSemAdSourcesBySemAdId( $intSemAdId, $objAdminDatabase ) {
		return CSemAdSources::fetchSemAdSourceBySemAdGroupIdBySemAdId( $this->getId(), $intSemAdId, $objAdminDatabase );
	}

}
?>