<?php

class CRootCauseAssociation extends CBaseRootCauseAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRootCauseTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>