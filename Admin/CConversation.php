<?php

class CConversation extends CBaseConversation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectAttributeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConversationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessage() {
		$boolIsValid = true;

		if( false == valStr( $this->getMessage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', 'Message is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMessage();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>