<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsSdmEmployees
 * Do not add any new functions to this class.
 */

class CStatsSdmEmployees extends CBaseStatsSdmEmployees {

	public static function fetchSdmEmployeeStatsStackRank( $objStdFilter, $objDatabase ) {

		$strSql = '
				SELECT *
				FROM (
					SELECT
						totals.employee_id,
						totals.date_started,
						totals.preferred_name,
						totals.num_employees,
						totals.story_points_released,
						calculate_grade(
							CASE
								WHEN totals.story_points_released > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( ORDER BY totals.story_points_released ) ) * 10 )
								ELSE 1
							END::INTEGER
						) AS story_points_released_grade,
						totals.bug_severity::numeric( 15, 1 ),
						calculate_grade(
							CASE
								WHEN totals.bug_severity > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( ORDER BY totals.bug_severity DESC ) ) * 10 )
								ELSE 10
							END::INTEGER
						) AS bug_severity_grade,
						totals.bugs_resolved,
						calculate_grade(
							CASE
								WHEN totals.bugs_resolved > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( ORDER BY totals.bugs_resolved ) ) * 10 )
								ELSE 1
							END::INTEGER
						) AS bugs_resolved_grade,
						totals.features_released,
						calculate_grade(
							CASE
								WHEN totals.features_released > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( ORDER BY totals.features_released ) ) * 10 )
								ELSE 1
							END::INTEGER
						) AS features_released_grade
					FROM
						(
							SELECT
								e.id AS employee_id,
								e.preferred_name,
								e.date_started,
								COUNT( sde.id ) AS num_employees,
								SUM( sde.story_points_released ) AS story_points_released,
								CASE
									WHEN SUM( sde.bugs_caused ) > 0 THEN ( SUM( sde.bug_severity * sde.bugs_caused ) / SUM( sde.bugs_caused ) )
									ELSE 0
								END AS bug_severity,
								SUM( sde.bugs_resolved ) AS bugs_resolved,
								SUM( sde.features_released ) AS features_released
							FROM
								stats_developer_employees sde
								JOIN team_employees te ON te.employee_id = sde.employee_id AND te.is_primary_team = 1
								JOIN teams t ON t.id = te.team_id
								JOIN employees e ON e.id = t.sdm_employee_id
							WHERE
								sde.week > ( NOW() - INTERVAL \'90 days\' )
							GROUP BY
								e.id,
								e.preferred_name,
								e.date_started
						) AS totals
					ORDER BY ' . ( ( false == valStr( $objStdFilter->sort_by ) ) ? 'employee_id' : $objStdFilter->sort_by . ' ' . $objStdFilter->sort_direction ) . '
				) AS full_result_set
				WHERE ( 1 = 1 )
				' . ( ( false == is_null( $objStdFilter->employees ) ) ? ' AND employee_id IN ( ' . $objStdFilter->employees . ' ) ' : '' ) . ';';

		return fetchData( $strSql, $objDatabase );
	}

	public static function rebuildSdmEmployeeStats( $intSdmEmployeeId, $objAdminDatabase ) {

		set_time_limit( 600 );

		if( true == is_numeric( $intSdmEmployeeId ) ) {
			$strSdmEmployeeCondition = ' = ' . $intSdmEmployeeId;
		} else {
			$strSdmEmployeeCondition = ' <> 0 ';
		}

		$objAdminDatabase->begin();
		if( false === fetchData( self::buildSdmEmployeeStatsInsertSql( $strSdmEmployeeCondition ), $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();
			return false;
		}
		$objAdminDatabase->commit();

		$strSql = self::buildSdmEmployeeTicketsSql( $strSdmEmployeeCondition );
		$strSql .= self::buildSdmEmployeeModulesSql( $strSdmEmployeeCondition );

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function buildSdmEmployeeStatsInsertSql( $strSdmEmployeeCondition ) {

		$strSql = '
				DELETE FROM stats_sdm_employees WHERE employee_id ' . $strSdmEmployeeCondition . ';

				INSERT INTO
					public.stats_sdm_employees ( id, employee_id, ps_product_id, week, created_by, created_on )

					SELECT
						nextval( \'stats_sdm_employees_id_seq\' ) AS id,
						se.employee_id,
						pp.id AS ps_product_id,
						d.week,
						1 AS created_by,
						NOW() AS created_on
					FROM
						(
							SELECT
								e.id AS employee_id,
								e.date_started,
								COALESCE( e.date_terminated, now() )::DATE AS date_terminated
							FROM
								employees e
							WHERE
								e.designation_id IN ( ' . implode( ',', CDesignation::$c_arrintSdmDesignations ) . ' )
						) se
						JOIN ps_products pp ON pp.sdm_employee_id = se.employee_id
						JOIN (
							SELECT DISTINCT
								DATE_TRUNC ( \'week\', d.date ) AS week
							FROM
								dates d
						) d ON ( DATE_TRUNC ( \'week\', d.week ) <= DATE_TRUNC ( \'week\', NOW ( ) ) )
					WHERE
						d.week BETWEEN se.date_started AND se.date_terminated
						AND se.employee_id ' . $strSdmEmployeeCondition . ';';

		$strSql .= '
				INSERT INTO
					public.stats_sdm_employees ( id, employee_id, week, created_by, created_on )

					SELECT
						nextval( \'stats_sdm_employees_id_seq\' ) AS id,
						se.employee_id,
						d.week,
						1 AS created_by,
						NOW() AS created_on
					FROM
						(
							SELECT
								e.id AS employee_id,
								e.date_started,
								COALESCE( e.date_terminated, now() )::DATE AS date_terminated
							FROM
								employees e
							WHERE
								e.designation_id IN ( ' . implode( ',', CDesignation::$c_arrintSdmDesignations ) . ' )
						) se
						JOIN (
							SELECT DISTINCT
								DATE_TRUNC ( \'week\', d.date ) AS week
							FROM
								dates d
						) d ON ( DATE_TRUNC ( \'week\', d.week ) <= DATE_TRUNC ( \'week\', NOW ( ) ) )
					WHERE
						d.week BETWEEN se.date_started AND se.date_terminated
						AND se.employee_id ' . $strSdmEmployeeCondition . ';
				';

		return $strSql;
	}

	public static function buildSdmEmployeeTicketsSql( $strSdmEmployeeCondition ) {

		$strSql = '
				UPDATE
					stats_sdm_employees sse
				SET
					ticket_average_aging_bug = data.ticket_average_aging_bug,
					ticket_average_aging_feature = data.ticket_average_aging_feature,
					ticket_average_aging_support = data.ticket_average_aging_support,
					tickets_open_bug = data.tickets_open_bug,
					tickets_open_feature = data.tickets_open_feature,
					tickets_open_support = data.tickets_open_support
				FROM
					(
						SELECT
							employee_id,
							ps_product_id,
							week,
							COUNT( t.id ) AS ticket_count,
							CASE
								WHEN SUM( CASE WHEN t.task_type_id = ' . CTaskType::BUG . ' THEN 1 ELSE 0 END ) > 0
								THEN ( SUM( CASE WHEN t.task_type_id = ' . CTaskType::BUG . ' THEN EXTRACT( EPOCH FROM( COALESCE( t.completed_on, t.week + INTERVAL \'7 days\' ) - t.task_datetime ) ) ELSE 0 END ) / SUM( CASE WHEN t.task_type_id = ' . CTaskType::BUG . ' THEN 1 ELSE 0 END ) )::INTEGER
								ELSE 0
							END AS ticket_average_aging_bug,
							CASE
								WHEN SUM( CASE WHEN t.task_type_id = ' . CTaskType::FEATURE . ' THEN 1 ELSE 0 END ) > 0
								THEN ( SUM( CASE WHEN t.task_type_id = ' . CTaskType::FEATURE . ' THEN EXTRACT( EPOCH FROM( COALESCE( t.completed_on, t.week + INTERVAL \'7 days\' ) - t.task_datetime ) ) ELSE 0 END ) / SUM( CASE WHEN t.task_type_id = ' . CTaskType::FEATURE . ' THEN 1 ELSE 0 END ) )::INTEGER
								ELSE 0
							END AS ticket_average_aging_feature,
							CASE
								WHEN SUM( CASE WHEN t.task_type_id = ' . CTaskType::SUPPORT . ' THEN 1 ELSE 0 END ) > 0
								THEN ( SUM( CASE WHEN t.task_type_id = ' . CTaskType::SUPPORT . ' THEN EXTRACT( EPOCH FROM( COALESCE( t.completed_on, t.week + INTERVAL \'7 days\' ) - t.task_datetime ) ) ELSE 0 END ) / SUM( CASE WHEN t.task_type_id = ' . CTaskType::SUPPORT . ' THEN 1 ELSE 0 END ) )::INTEGER
								ELSE 0
							END AS ticket_average_aging_support,
							SUM( CASE WHEN t.task_type_id = ' . CTaskType::BUG . ' THEN 1 ELSE 0 END ) AS tickets_open_bug,
							SUM( CASE WHEN t.task_type_id = ' . CTaskType::FEATURE . ' THEN 1 ELSE 0 END ) AS tickets_open_feature,
							SUM( CASE WHEN t.task_type_id = ' . CTaskType::SUPPORT . ' THEN 1 ELSE 0 END ) AS tickets_open_support
						FROM
							(
								SELECT
									pp.sdm_employee_id AS employee_id,
									pp.id AS ps_product_id,
									generate_series( DATE_TRUNC( \'week\', t.task_datetime), COALESCE( t.completed_on, NOW() ), \'1 week\' ) AS week,
									t.id,
									t.task_type_id,
									t.task_datetime,
									t.completed_on
								FROM
									tasks t
									JOIN ps_products pp ON pp.id = t.ps_product_id
								WHERE
									t.task_type_id IN ( ' . CTaskType::BUG . ', ' . CTaskType::FEATURE . ', ' . CTaskType::SUPPORT . ' )
									AND t.task_status_id <> ' . CTaskStatus::CANCELLED . '
									AND pp.sdm_employee_id IS NOT NULL
							) t
						GROUP BY
							week,
							employee_id,
							ps_product_id
					) data
				WHERE
					sse.employee_id = data.employee_id
					AND sse.ps_product_id = data.ps_product_id
					AND sse.week = data.week
					AND sse.employee_id ' . $strSdmEmployeeCondition . ';';

		return $strSql;
	}

	public static function buildSdmEmployeeModulesSql( $strSdmEmployeeCondition ) {

		$strSql = '
				UPDATE
					stats_sdm_employees sse
				SET
					modules_accessed = data.modules_accessed
				FROM
					(
						SELECT
							u.employee_id,
							DATE_TRUNC( \'week\', uml.hour ) AS week,
							SUM( uml.click_count ) AS modules_accessed
						FROM
							user_module_logs uml
							JOIN users u ON u.id = uml.user_id
						GROUP BY
							u.employee_id,
							week
					) data
				WHERE
					sse.employee_id = data.employee_id
					AND sse.ps_product_id IS NULL
					AND sse.week = data.week
					AND sse.employee_id ' . $strSdmEmployeeCondition . ';';

		return $strSql;
	}

}
?>