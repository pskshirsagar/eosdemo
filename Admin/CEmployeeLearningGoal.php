<?php

class CEmployeeLearningGoal extends CBaseEmployeeLearningGoal {
	protected $m_intSurveyTypeId;

	public function setSurveyTypeId( $intSurveyTypeId ) {
		$this->set( 'm_intSurveyTypeId', CStrings::strToIntDef( $intSurveyTypeId, NULL, false ) );
	}

	public function getSurveyTypeId() {
		return $this->m_intSurveyTypeId;
	}

	public function sqlSurveyTypeId() {
		return ( true == isset( $this->m_intSurveyTypeId ) ) ? ( string ) $this->m_intSurveyTypeId : 'NULL';
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['survey_type_id'] ) ) {
			$this->setSurveyTypeId( trim( $arrmixValues['survey_type_id'] ) );
		}
		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSurveyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLearningGoalId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLearningGoalProficiencyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLearningGoalIds( $arrintEmployeeLearningGoalIds, $objAdminDatabase ) {
		$boolIsValid = true;
		$arrintCountEmployeeLearningGoalIds = array_count_values( $arrintEmployeeLearningGoalIds );
		if( false == valId( $this->getLearningGoalId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'learning_goal_id', 'Learning Goal is required.' ) );
		} else if( $arrintCountEmployeeLearningGoalIds[$this->getLearningGoalId()] > 1 ) {
			$objLearningGoal    = \Psi\Eos\Admin\CLearningGoals::createService()->fetchLearningGoalById( $this->getLearningGoalId(), $objAdminDatabase );
			if( true == valObj( $objLearningGoal, 'CLearningGoal' ) && 'Other' != $objLearningGoal->getName() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'learning_goal_id', 'Please select unique Learning Goal.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valLearningGoalProficiencyIds( $objAdminDatabase ) {
		$boolIsValid        = true;
		if( true == valId( $this->getLearningGoalId() ) ) {
			$objLearningGoal = \Psi\Eos\Admin\CLearningGoals::createService()->fetchLearningGoalById( $this->getLearningGoalId(), $objAdminDatabase );
		}
		if( false == valId( $this->getLearningGoalProficiencyId() ) && true == valObj( $objLearningGoal, 'CLearningGoal' ) && 'Other' != $objLearningGoal->getName() && 'NA' != $objLearningGoal->getName() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'learning_goal_proficiency_id', 'Proficiency is required.' ) );
		} else if( false == valStr( $this->getNote() ) && true == valObj( $objLearningGoal, 'CLearningGoal' ) && 'Other' == $objLearningGoal->getName() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL, $arrintEmployeeLearningGoalIds = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case 'insert_employee_learning_goals':
				$boolIsValid &= $this->valLearningGoalIds( $arrintEmployeeLearningGoalIds, $objAdminDatabase );
				$boolIsValid &= $this->valLearningGoalProficiencyIds( $objAdminDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
