<?php

class CDbWatch extends CBaseDbWatch {

	protected $m_intTestFailed;

	protected $m_strProcessingTime;
	protected $m_strOutputText;

	/**
	 * Get Functions
	 */

	public function getOutputText() {
		return $this->m_strOutputText;
	}

	public function getProcessingTime() {
		return $this->m_strProcessingTime;
	}

	public function getTestFailed() {
		return $this->m_intTestFailed;
	}

	public function getOrFetchDatabase() {
		// Initialize Database

		if( true == valObj( $this->m_objDatabase, 'CDatabases' ) ) {
			return $this->m_objDatabase;
		}

		$objDatabase = CDatabases::fetchDatabaseById( $this->getDatabaseId(), CDatabases::createConnectDatabase() );

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			return false;
		}

		$intDatabaseUserTypeId = CDatabaseUserType::PS_DEVELOPER;

		if( CDatabaseType::CLIENT == $objDatabase->getDatabaseTypeId() ) {
			$intDatabaseUserTypeId = CDatabaseUserType::PS_PROPERTYMANAGER;
		}

		$this->m_objDatabase = CDatabases::createDatabase( $intDatabaseUserTypeId, $objDatabase->getDatabaseTypeId(), $objDatabase->getDatabaseServerTypeId() );

		return $this->m_objDatabase;
	}

	/**
	 * Validation Functions
	 */

	public function valDbWatchTypeId() {
		$boolIsValid = true;

		if( 0 == $this->getDbWatchTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'db_watch_type_id', 'Watch type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDatabaseId() {
		$boolIsValid = true;

		if( 0 == $this->getDatabaseId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_id', 'Database is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDbWatchPriorityId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDbWatchPriorityId() ) || 0 == $this->getDbWatchPriorityId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'db_watch_priority_id', 'Priority type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} elseif( false == is_null( $this->getName() ) && true == is_numeric( $this->getName() ) ) {
		   $boolIsValid = false;
		   $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Valid Name is requied.' ) );
		}

		return $boolIsValid;
	}

	public function valDbWatchSql() {
		$boolIsValid = true;

		if( true == is_null( $this->getDbWatchSql() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'db_watch_sql', 'DB watch sql is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDbWatchTypeId();
				$boolIsValid &= $this->valDbWatchPriorityId();
				$boolIsValid &= $this->valDatabaseId();
				$boolIsValid &= $this->valDbWatchSql();
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function process() {

		$this->getOrFetchDatabase();

		$time_start = time();

		$strBaseSql = str_replace( '`', "'", $this->m_strDbWatchSql );
		$strBaseSql = str_replace( ';', '', $strBaseSql );

		$strConditionalSql = ' SELECT
								CASE
									WHEN ( ' . $this->m_strErrorConditionSql . ' )
									THEN 1
									ELSE 0
								END
								FROM ( ' . $strBaseSql . ' ) as conditional_test_query';

		$arrstrTestStatus = $this->fetchProcessData( $strConditionalSql );

		if( 1 == $arrstrTestStatus[0]['case'] ) {
			$this->m_intTestFailed = 1;
		} else {
			$this->m_intTestFailed = 0;
		}

		$arrstrResult = $this->fetchProcessData( $strBaseSql );

		if( true == valArr( $arrstrResult ) ) {
			$this->m_strOutputText = $this->m_strOutputTextIntro;

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrResult ) ) {
				if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrResult[0] ) ) {
					$this->m_strOutputText .= array_pop( $arrstrResult[0] );
				} else {
					foreach( $arrstrResult[0] as $key => $strResult ) {
						$this->m_strOutputText .= $key . ':' . $strResult . '<br/>';
					}
				}

			} else {
				foreach( $arrstrResult as $key => $strResult ) {
					if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrResult[0] ) ) {
						$this->m_strOutputText .= array_pop( $arrstrResult[0] ) . '<br/>';
					} else {
						foreach( $arrstrResult[0] as $key => $strResult ) {
							$this->m_strOutputText .= $key . ':' . $strResult . '<br/>';
						}
					}
				}
			}

			$this->m_strOutputText .= $this->m_strOutputTextClosing;
		}

		$time_end = time();
		$this->m_strProcessingTime = $time_end - $time_start;
		return;
	}

	public function fetchDbWatchGroups( $objDatabase ) {

		return CDbWatchGroups::fetchDbWatchGroupsByDbWatchId( $this->getId(), $objDatabase );
	}

	public function fetchProcessData( $strSql ) {

		$arrmixData = array();

		$objDataset = $this->m_objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) || 0 > $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to process "' . $this->getName() . '" db watch. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $this->m_objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			while( false == $objDataset->eof() ) {
				$arrmixData[] = $objDataset->fetchArray();
				$objDataset->next();
			}
		}

		return $arrmixData;
	}

	public function createSystemEmail( $strToEmailAddress ) {

		$strHtmlContent		= '';
		$strHtmlContent	   .= 'Processing time :- ' . $this->m_strProcessingTime . '<br /> # of Result :- ' . $this->m_strOutputText;
		$strSubject			= $this->getName() . ' Db Watch Failed.';

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, $strHtmlContent, $strToEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::URGENT );

		return $objSystemEmail;
	}

	public function createMessage( $strPhoneNumber, $intMessageOperatorId ) {

		$strHtmlEmailOutput = '';

		$strMessage = $this->getName() . ' Db Watch Failed . ';

		$strMessage .= 'Processing time :- ' . $this->m_strProcessingTime . ', # of Result :- ' . $this->m_strOutputText;

		$strMessage = \Psi\CStringService::singleton()->substr( $strMessage, 0, 160 );

		$objMessage = new CMessage();
		$objMessage->setDefaults();
		$objMessage->setMessageOperatorId( $intMessageOperatorId );
		$objMessage->setMessageTypeId( CMessageType::MONITOR );
		$objMessage->setMessageStatusTypeId( CMessageStatusType::PENDING_SEND );
		$objMessage->setMessage( $strMessage );
		$objMessage->setPhoneNumber( $strPhoneNumber );
		$objMessage->setMessageDatetime( date( 'Y-m-d H:i:s' ) );
		$objMessage->setIsInbound( 0 );

		return $objMessage;
	}

}
?>