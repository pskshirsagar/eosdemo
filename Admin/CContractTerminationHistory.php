<?php

class CContractTerminationHistory extends CBaseContractTerminationHistory {

	protected $m_strPropertyName;
	protected $m_strProductName;
	protected $m_strRequestStatusName;
	protected $m_strPostedOn;
	protected $m_strSalesApprovedOn;
	protected $m_strContractTerminationRequestidStatus;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractTerminationRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 */

	public function setRequestStatusName( $strRequestStatusName ) {
		$this->m_strRequestStatusName = $strRequestStatusName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setProductName( $strProductName ) {
		$this->m_strProductName = $strProductName;
	}

	public function setPostedOn( $strPostedOn ) {
		$this->m_strPostedOn = $strPostedOn;
	}

	public function setSalesApprovedOn( $strSalesApprovedOn ) {
		$this->m_strSalesApprovedOn = $strSalesApprovedOn;
	}

	public function setContractTerminationRequestidStatus( $strContractTerminationRequestidStatus ) {
		$this->m_strContractTerminationRequestidStatus = $strContractTerminationRequestidStatus;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['request_status_name'] ) ) $this->setRequestStatusName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['request_status_name'] ) : $arrmixValues['request_status_name'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_name'] ) : $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['product_name'] ) ) $this->setProductName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['product_name'] ) : $arrmixValues['product_name'] );
		if( true == isset( $arrmixValues['posted_on'] ) ) $this->setPostedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['posted_on'] ) : $arrmixValues['posted_on'] );
		if( true == isset( $arrmixValues['sales_approved_on'] ) ) $this->setSalesApprovedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['sales_approved_on'] ) : $arrmixValues['sales_approved_on'] );
		if( true == isset( $arrmixValues['contract_termination_requestid_status'] ) ) $this->setContractTerminationRequestidStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['contract_termination_requestid_status'] ) : $arrmixValues['contract_termination_requestid_status'] );

	}

	/**
	 * Get Functions
	 */

	public function getRequestStatusName() {
		return $this->m_strRequestStatusName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getProductName() {
		return $this->m_strProductName;
	}

	public function getPostedOn() {
		return $this->m_strPostedOn;
	}

	public function getSalesApprovedOn() {
		return $this->m_strSalesApprovedOn;
	}

	public function getContractTerminationRequestidStatus() {
		return $this->m_strContractTerminationRequestidStatus;
	}

}
?>