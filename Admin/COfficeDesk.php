<?php

class COfficeDesk extends CBaseOfficeDesk {

	protected $m_intEmployeeId;
	protected $m_intEmployeeDepartmentId;
	protected $m_strEmployeeNameFull;
	protected $m_intProposedOfficeDeskId;

	const RESERVED_DESK			= 1;
	const ACTIVE_DESK			= 1;
	const NOT_RESERVED_DESK		= 0;

	const MAC_PC				= 'MAC_PC';
	const TASK_TITLE			= 'Desk Shifting';
	const VACANT_DESK			= 'VACANT';

	const DESK_SHIFTING_REQUEST_ADMIN_PENDING	= '0';
	const DESK_SHIFTING_REQUEST_IT_PENDING		= '1';
	const DESK_SHIFTING_REQUEST_COMPLETED		= NULL;
	const DESK_SHIFTING_REQUEST_IT_PENDING_FOR_ASSETS = 2;

	public static $c_arrstrConfernceRooms = array(
		'Telephonic',
		CInterviewType::GOOGLE_MEET_INTERVIEW,
		'Interviewer\'s Cabin',
		'Tower 8 - BUGATTI',
		'Tower 8 - MAYBACH',
		'Tower 8 - FERRARI',
		'Tower 9 - HYUNDAI',
		'Tower 9 - AUDI',
		'Tower 9 - HONDA',
		'Tower 9 - NISSAN',
		'Tower 9 - MERCEDES',
		'Tower 9 - ASTON MARTIN',
		'Tower 9 - FORD',
		'Tower 9 - TOYOTA',
		'Tower 9 - BMW',
		'Tower 9 - ROLLS ROYCE',
		'Tower 9 - LAMBORGHINI',
		'Tower 9 - GM'
	);

	public static $c_arrstrVoidDesks = [
		'1999' => 'D363 SERVER ROOM',
		'2000' => 'D364 MCLAREN F1',
		'2001' => 'D365 GURUKUL T8',
		'2005' => 'D369 BUGGATI',
		'2006' => 'D370 RECEPTION',
		'2039' => 'D371 XTREME',
		'1998' => 'AD356 RECEPTION',
		'2009' => 'BD247 GURUKUL',
		'2010' => 'BD248 CAFETERIA 9',
		'2008' => 'BD246 ASTON MARTIN',
		'2007' => 'BD245 SERVER ROOM',
		'1997' => 'BD244 FORD',
		'2012' => 'AD358 HUMMER',
		'1993' => 'AD352 MERCEDES',
		'1994' => 'AD353 GM',
		'1995' => 'AD354 LAMBORGHINI',
		'1996' => 'AD355 HYUNDAI',
		'2011' => 'AD357 SERVER ROOM',
		'345' => 'AD030 VOID DESK',
		'1934' => 'AD 172 VOID DESK',
		'142' => 'AD200 VOID DESK',
		'277' => 'AD232 VOID DESK',
		'57'  => 'BD033 VOID DESK',
		'262' => 'BD079 VOID DESK',
		'500' => 'BD176 VOID DESK',
		'404' => 'BD026 VOID DESK',
		'246' => 'BD088 VOID DESK',
		'491' => 'BD152 VOID DESK',
		'444' => 'BD112 VOID DESK'
	];

	public function valOfficeId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intOfficeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_id', 'Proposed office is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDeskNumber() {
		 $boolIsValid = true;
		if( false == isset( $this->m_strDeskNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'desk_number', 'Proposed desk number is required.' ) );
		}
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		if( true == empty( $this->m_intEmployeeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valOfficeId();
				$boolIsValid &= $this->valDeskNumber();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			default:
				// Default statement should be here.
				break;
		}

		return $boolIsValid;
	}

	/**
	* 			Set Functions
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['employee_id'] ) ) 						$this->setEmployeeId( $arrmixValues['employee_id'] );
		if( true == isset( $arrmixValues['employee_name_full'] ) ) 				$this->setEmployeeNameFull( $arrmixValues['employee_name_full'] );
		if( true == isset( $arrmixValues['desk_allocation_office_desk_id'] ) ) 	$this->setDeskAllocationOfficeDeskId( $arrmixValues['desk_allocation_office_desk_id'] );
		if( true == isset( $arrmixValues['employee_department_id'] ) ) 			$this->setEmployeeDepartmentId( $arrmixValues['employee_department_id'] );

	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setEmployeeNameFull( $strEmployeeNameFull ) {
		$this->m_strEmployeeNameFull = $strEmployeeNameFull;
	}

	public function setDeskAllocationOfficeDeskId( $intProposedOfficeDeskId ) {
		$this->m_intProposedOfficeDeskId = $intProposedOfficeDeskId;
	}

	public function setEmployeeDepartmentId( $intEmployeeDepartmentId ) {
		$this->m_intEmployeeDepartmentId = $intEmployeeDepartmentId;
	}

	/**
	 * Get Functions
	 */

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getEmployeeNameFull() {
		return $this->m_strEmployeeNameFull;
	}

	public function getDeskAllocationOfficeDeskId() {
		return $this->m_intProposedOfficeDeskId;
	}

	public function getEmployeeDepartmentId() {
		return $this->m_intEmployeeDepartmentId;
	}

}
?>