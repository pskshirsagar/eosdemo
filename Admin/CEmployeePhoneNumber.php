<?php

class CEmployeePhoneNumber extends CBaseEmployeePhoneNumber {

	/**
	 * Validation Functions
	 */

	public function valEmployeeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intEmployeeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumberTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPhoneNumberTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_type_id', 'Phone number type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber( $strCountryCode ) {
		$boolIsValid			= true;
		$strPhoneType			= ( CPhoneNumberType::MOBILE == $this->m_intPhoneNumberTypeId ) ? 'Contact' : 'Phone';
		$this->m_strPhoneNumber = ( CCountry::CODE_USA == $strCountryCode ) ? preg_replace( '/\s*[-\.\s+]\s*/', '-', trim( $this->m_strPhoneNumber ) ) :  $this->m_strPhoneNumber;

		if( false == isset( $this->m_strPhoneNumber ) || 0 == strlen( trim( $this->m_strPhoneNumber ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', $strPhoneType . ' number is required.' ) );
		} elseif( false == CValidation::checkPhoneNumber7digit( $this->m_strPhoneNumber ) || ( CCountry::CODE_INDIA == $strCountryCode && 10 < strlen( trim( str_replace( ' ', '', $this->m_strPhoneNumber ) ) ) ) || ( CCountry::CODE_USA == $strCountryCode && ( 10 != preg_match_all( '/[0-9]/', $this->m_strPhoneNumber ) && false == preg_match( '/^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/', $this->m_strPhoneNumber ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', $strPhoneType . ' number is invalid.' ) );
		}

		return $boolIsValid;
	}

	public function valContactPersonDetails() {
		$boolValid = true;
		$arrstrPhoneNumberDetails = json_decode( $this->m_strPhoneNumberDetails, true );
		if( false == valStr( $arrstrPhoneNumberDetails['emergency_contact_name'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_details', ' Contact person name is required.' ) );
			$boolValid = false;
		}
		if( true == $boolValid && 1 >= strlen( $arrstrPhoneNumberDetails['emergency_contact_name'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_details', ' Contact person name should contain more than one character.' ) );
			$boolValid = false;
		}

		if( true == $boolValid && false == preg_match( '/^[a-zA-Z\s]+$/', $arrstrPhoneNumberDetails['emergency_contact_name'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_details', ' Contact person name should contain only characters.' ) );
			$boolValid = false;
		}

		if( false == valStr( $arrstrPhoneNumberDetails['emergency_contact_relationship'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_details', ' Contact relationship is required.' ) );
			$boolValid = false;
		}

		return $boolValid;
	}

	public function validate( $strAction, $strCountryCode = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPhoneNumberTypeId();
				$boolIsValid &= $this->valPhoneNumber( $strCountryCode );
				if( CCountry::CODE_INDIA == $strCountryCode && CPhoneNumberType::MOBILE == $this->getPhoneNumberTypeId() ) {
					$boolIsValid &= $this->valContactPersonDetails();
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valPhoneNumberTypeId();
				$boolIsValid &= $this->valPhoneNumber( $strCountryCode );
				if( CCountry::CODE_INDIA == $strCountryCode && CPhoneNumberType::MOBILE == $this->getPhoneNumberTypeId() ) {
					$boolIsValid &= $this->valContactPersonDetails();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>