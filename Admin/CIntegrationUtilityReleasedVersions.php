<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CIntegrationUtilityReleasedVersions
 * Do not add any new functions to this class.
 */

class CIntegrationUtilityReleasedVersions extends CBaseIntegrationUtilityReleasedVersions {

	public static function fetchAllIntegrationReleasedLatestVersions( $objDatabase ) {

		$strSql = 'SELECT
						id,
						integration_client_type_id,
						name,
						released_version
				   FROM
						integration_utility_released_versions
					ORDER BY name';

		return CIntegrationUtilityReleasedVersions::fetchIntegrationUtilityReleasedVersions( $strSql, $objDatabase );
	}

	public static function fetchSearchIntegrationUitiliesReleasedVersions( $arrmixFilteredExplodedSearch, $objDatabase ) {

 		$strSelectCondition = ',
 			CASE
 				WHEN iurv.name ILIKE E\'' . implode( '%\' AND iurv.name ILIKE E\'%', $arrmixFilteredExplodedSearch ) . '%\' THEN 1
 				END AS order_first ';
 		$strOrderByCondition = 'order_first,';

		$strSql	= '
			SELECT
				iurv.id,
				iurv.integration_client_type_id,
				iurv.name,
				iurv.released_version
				' . $strSelectCondition . '
			FROM
				integration_utility_released_versions iurv
			WHERE
				(
					iurv.name ILIKE \'%' . implode( '%\' AND iurv.name ILIKE \'%', $arrmixFilteredExplodedSearch ) . '%\'
				)';

		$strSql .= '
			ORDER BY
				' . $strOrderByCondition . ' iurv.id
			LIMIT
				10';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPaginatedIntegrationUtilityReleasedVersionsDetails( $intPageNo = 0, $intPageSize = NULL,  $objDatabase ) {

		$strLimitCondition = '';

		if( false == is_null( $intPageSize ) ) $strLimitCondition = ' LIMIT ' . ( int ) $intPageSize;

		$intOffset		= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql = 'SELECT
						id,
						integration_client_type_id,
						name,
						released_version
				   FROM
						integration_utility_released_versions
					ORDER BY name  OFFSET ' . ( int ) $intOffset . $strLimitCondition;

		return CIntegrationUtilityReleasedVersions::fetchIntegrationUtilityReleasedVersions( $strSql, $objDatabase );

	}

}
?>