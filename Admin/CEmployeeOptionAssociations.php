<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeOptionAssociations
 * Do not add any new functions to this class.
 */

class CEmployeeOptionAssociations extends CBaseEmployeeOptionAssociations {

	public static function fetchEmployeeOptionAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeOptionAssociation', $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchEmployeeOptionAssociationsByIds( $arrintIds, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						*
				  FROM
						employee_option_associations
				  WHERE
						id IN( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchEmployeeOptionAssociations( $strSql, $objDatabase );
	}

}
?>