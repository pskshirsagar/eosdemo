<?php

class CSocialAccountDetail extends CBaseSocialAccountDetail {

	const FACEBOOK_SOCIAL_ACCOUNT_TYPE_ID	= 1;
	const GOOGLE_SOCIAL_ACCOUNT_TYPE_ID		= 2;
	const BING_SOCIAL_ACCOUNT_TYPE_ID		= 4;
	const SIMPLI_FI_SOCIAL_ACCOUNT_TYPE_ID	= 5;


	/**
	 * If you are making any changes in populateSocialAccountSmartyConstants function then
	 * please make sure the same changes would be applied to populateSocialAccountTemplateConstants function also.
	 */

	public static function populateSocialAccountSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'FACEBOOK_SOCIAL_ACCOUNT_TYPE',		self:: FACEBOOK_SOCIAL_ACCOUNT_TYPE_ID );
		$objSmarty->assign( 'GOOGLE_SOCIAL_ACCOUNT_TYPE',		self:: GOOGLE_SOCIAL_ACCOUNT_TYPE_ID );
		$objSmarty->assign( 'BING_SOCIAL_ACCOUNT_TYPE',			self:: BING_SOCIAL_ACCOUNT_TYPE_ID );

	}

	public static function populateSocialAccountTemplateConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['FACEBOOK_SOCIAL_ACCOUNT_TYPE']	= self:: FACEBOOK_SOCIAL_ACCOUNT_TYPE_ID;
		$arrmixTemplateParameters['GOOGLE_SOCIAL_ACCOUNT_TYPE']		= self:: GOOGLE_SOCIAL_ACCOUNT_TYPE_ID;
		$arrmixTemplateParameters['BING_SOCIAL_ACCOUNT_TYPE']		= self:: BING_SOCIAL_ACCOUNT_TYPE_ID;
		$arrmixTemplateParameters['SIMPLI_FI_SOCIAL_ACCOUNT_TYPE']	= self:: SIMPLI_FI_SOCIAL_ACCOUNT_TYPE_ID;
		return $arrmixTemplateParameters;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>