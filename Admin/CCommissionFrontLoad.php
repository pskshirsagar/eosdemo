<?php

class CCommissionFrontLoad extends CBaseCommissionFrontLoad {

	/**
	 * Validation Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAccountId();
				$boolIsValid &= $this->valChargeCodeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}
}
?>