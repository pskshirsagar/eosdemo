<?php
use Psi\Eos\Admin\CPsAssets;

class CPsAsset extends CBasePsAsset {

	const HARDWARE_DEPRECATION_RATE 	= 5;
	const PURCHASE_REQUEST_DATE 		= 'purchase_date';
	const EXPIRY_DATE					= 'expiry_date';

	protected $m_strName;
	protected $m_strBrandName;
	protected $m_strCode;
	protected $m_intEmployeeId;

	public function valId( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Asset  is  required.' ) );
		} else {

			$objPsAsset	= CPsAssets::createService()->fetchPsAssetById( $this->getId(), $objDatabase );

			if( CPsAssetStatusType::CHANGE_REQUESTED == $objPsAsset->getPsAssetStatusTypeId() || CPsAssetStatusType::DAMAGE_REQUESTED == $objPsAsset->getPsAssetStatusTypeId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Asset is already requested.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPsAssetStatusTypeId() {

		$boolIsValid = true;

		if( true == is_null( $this->getPsAssetStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_type_id', 'Status is  required.' ) );
		}

		return $boolIsValid;
	}

	public function valOfficeId() {
		$boolIsValid = true;
			if( true == is_null( $this->getOfficeId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_id', 'Office is required.' ) );
			}
			return $boolIsValid;
	}

	public function valPsAssetTypeId( $boolEntrataEquipment = false ) {
		$boolIsValid = true;
		$strTypeName = ( true == $boolEntrataEquipment ) ? 'Equipment' : 'Asset';

		if( true == is_null( $this->getPsAssetTypeId() ) || '0' == $this->getPsAssetTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_type_id', $strTypeName . ' type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPsAssetPurchaseSourcesId() {
		$boolIsValid = true;

		if( ( true == is_null( $this->getPsAssetPurchaseSourcesId() ) || '0' == $this->getPsAssetPurchaseSourcesId() ) && CCountry::CODE_INDIA == $this->getCountryCode() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_purchase_sources_id', 'Purchase From is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPsAssetBrandId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsAssetBrandId() ) || '0' == $this->getPsAssetBrandId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_brand_id', 'Brand is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPsAssetModelId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsAssetModelId() ) || '0' == $this->getPsAssetModelId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_model_id', 'Model is required.' ) );
		}
		return $boolIsValid;
	}

	public function valServiceTag( $strAction, $objDatabase ) {
		$boolIsValid = true;

		$objPsAssetType	= CBasePsAssetTypes::fetchPsAssetTypeById( $this->getPsAssetTypeId(), $objDatabase );
		if( false == is_null( $this->getServiceTag() ) && true == valObj( $objPsAssetType, 'CPsAssetType' ) ) {

				if( CPsAssetType::PC == $this->getPsAssetTypeId() || CPsAssetType::SOFTWARE == $objPsAssetType->getPsAssetTypeId() ) {

					$objConflictingPsServiceTag = CPsAssets::createService()->fetchConflictingPsServiceTag( $this->getServiceTag(), $objDatabase );

					if( $strAction == VALIDATE_INSERT ) {
						if( false == is_null( $objConflictingPsServiceTag ) ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_tag', 'Service tag/Serial number is already in use.' ) );
							return $boolIsValid;
						}
					} else {
						if( false == is_null( $objConflictingPsServiceTag ) ) {
							if( $this->getId() != $objConflictingPsServiceTag->getId() ) {
								$boolIsValid = false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_tag', 'Service tag/Serial number is already in use.' ) );
								return $boolIsValid;
							}
						}
					}

					if( false == \Psi\CStringService::singleton()->preg_match( '/^([a-zA-Z0-9-]){2,70}$/', $this->getServiceTag() ) ) {

						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_tag', 'Service tag/Serial number is not valid.' ) );
						return $boolIsValid;
					}

				}
		}
		return $boolIsValid;
	}

	public function valMacTag( $strAction, $objDatabase ) {
		$boolIsValid = true;

		if( CPsAssetType::PC == $this->getPsAssetTypeId() ) {

			if( false == is_null( $this->getMacTag() ) ) {
				$objConflictingPsMacTag = CPsAssets::createService()->fetchConflictingPsMacTag( $this->getMacTag(), $objDatabase );

				if( $strAction == VALIDATE_INSERT ) {
					if( false == is_null( $objConflictingPsMacTag ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mac_tag', 'MAC Address is already in use.' ) );
						return $boolIsValid;
					}
				} else {
					if( false == is_null( $objConflictingPsMacTag ) ) {
						if( $this->getId() != $objConflictingPsMacTag->getId() ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mac_tag', 'MAC Address is already in use.' ) );
							return $boolIsValid;
						}
					}
				}
				if( false == \Psi\CStringService::singleton()->preg_match( '/^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/', $this->getMacTag() ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mac_tag', 'MAC Address is not valid.' ) );
					return $boolIsValid;
				}
			}
		}

			return $boolIsValid;
	}

	public function valAssetTag( $strAction, $objDatabase ) {
		$boolIsValid = true;

		$objPsAssetType = new CPsAssetType();
		$objPsAssetType	= CBasePsAssetTypes::fetchPsAssetTypeById( $this->getPsAssetTypeId(), $objDatabase );

		if( true == valObj( $objPsAssetType, 'CPsAssetType' ) ) {

		 	if( CPsAssetType::HARDWARE == $objPsAssetType->getPsAssetTypeId() && CCountry::CODE_INDIA != $this->getCountryCode() ) {

				if( true == is_null( $this->getAssetTag() ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_tag', 'Asset tag is required.' ) );
					return $boolIsValid;
				}

				$objConflictingPsAssetTag = CPsAssets::createService()->fetchConflictingPsAssetTag( $this->getAssetTag(), $objDatabase );

				if( $strAction == VALIDATE_INSERT ) {
					if( false == is_null( $objConflictingPsAssetTag ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_tag', 'Asset tag is already in use.' ) );
						return $boolIsValid;
					}
				} else {
					if( false == is_null( $objConflictingPsAssetTag ) && $this->getId() != $objConflictingPsAssetTag->getId() ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_tag', 'Asset tag is already in use.' ) );
						return $boolIsValid;
					}
				}
				if( $this->getCountryCode() != CCountry::CODE_INDIA ) {
					if( false == \Psi\CStringService::singleton()->preg_match( '/^([a-zA-Z0-9\/]){2,12}$/', $this->getAssetTag() ) ) {

						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_tag', 'Asset tag is not valid.' ) );
						return $boolIsValid;
					}
				}
			}
		}
		return $boolIsValid;
	}

	public function valPurchasedCost( $boolXentoEquipment = false ) {
		$boolIsValid = true;

		if( false == $boolXentoEquipment && true == is_null( $this->getPurchasedCost() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchased_cost', 'Purchase cost is required.' ) );
			return $boolIsValid;
		}

		if( valStr( $this->getPurchasedCost() ) && ( false == ( \Psi\CStringService::singleton()->preg_match( '/^[0-9]([0-9]){0,12}(\.\d{1,2})?$/', $this->getPurchasedCost() ) ) ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchased_cost', 'Purchase cost is not valid.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valCpuCoreNum( $intAssetType ) {
		$boolIsValid = true;
		if( CPsAssetType::SERVER == $this->getPsAssetTypeId() && $intAssetType == NULL ) {
			if( true == is_null( $this->getCpuCoreNum() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cpu_core_num', 'CPU Core is required.' ) );
				return $boolIsValid;
			}

			if( ( false == is_nan( $this->getCpuCoreNum() ) && CAssetTypes::START_OF_CPU_CORE > $this->getCpuCoreNum() || CAssetTypes::END_OF_CPU_CORE < $this->getCpuCoreNum() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cpu_core_num', 'Please enter CPU Core between 1 to 256' ) );
				return $boolIsValid;
			}
		}
	return $boolIsValid;
	}

	public function valLicenceNo( $strAction, $objDatabase ) {

		$boolIsValid = true;

		$objPsAssetType = new CPsAssetType();
		$objPsAssetType = CBasePsAssetTypes::fetchPsAssetTypeById( $this->getPsAssetTypeId(), $objDatabase );

		if( false == is_null( $objPsAssetType ) ) {
			if( CPsAssetType::SOFTWARE == $objPsAssetType->getPsAssetTypeId() ) {
				if( true == is_null( $this->getLicenceNo() ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'licence_no', 'Licence number is required.' ) );
					return $boolIsValid;
				}

				$objConflictingPsLicenceNo = CPsAssets::createService()->fetchConflictingPsLicenceNo( $this->getLicenceNo(), $objDatabase );

				if( $strAction == VALIDATE_INSERT ) {
					if( false == is_null( $objConflictingPsLicenceNo ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'licence_no', 'Licence number is already in use.' ) );
						return $boolIsValid;
					}
				} else {
					if( false == is_null( $objConflictingPsLicenceNo ) && $this->getId() != $objConflictingPsLicenceNo->getId() ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'licence_no', 'Licence number is already in use.' ) );
						return $boolIsValid;
					}
				}
				if( false == \Psi\CStringService::singleton()->preg_match( '/^([a-zA-Z0-9-]){2,70}$/', $this->getLicenceNo() ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'licence_no', 'Licence number is not valid.' ) );
					return $boolIsValid;
				}

			}
		}
		return $boolIsValid;
	}

	public function valUserLimit( $objDatabase ) {
		$boolIsValid = true;

		$objPsAssetType = new CPsAssetType();
		$objPsAssetType = CBasePsAssetTypes::fetchPsAssetTypeById( $this->getPsAssetTypeId(), $objDatabase );

		if( false == is_null( $objPsAssetType ) ) {
			if( CPsAssetType::SOFTWARE == $objPsAssetType->getPsAssetTypeId() ) {

				if( true == is_null( $this->getUserLimit() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_limit', 'User limit is required.' ) );
					return $boolIsValid;
				}
				if( false == \Psi\CStringService::singleton()->preg_match( '/^[0-9]{1,5}$/', $this->getUserLimit() ) || 0 == $this->getUserLimit() ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_limit', 'User limit is not valid.' ) );
					return $boolIsValid;
				}
			}
		}

		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getCountryCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', 'Country is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPurchaseDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getPurchaseDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_date', 'Purchase date is required.' ) );
			return $boolIsValid;
		}

		$strPurchasedDate 	= new DateTime( $this->getPurchaseDate() );
		$strCurrentDate 	= new DateTime( date( 'm/d/Y', time() ) );
		$objDateDiff 		= date_diff( $strPurchasedDate, $strCurrentDate );

		if( 0 < ( $objDateDiff->invert ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_date', 'Purcahse date must not be future date.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valExpiryDate() {
		$boolIsValid = true;

		$strExpiryDate 		= new DateTime( $this->getExpiryDate() );
		$strPurchasedDate	= new DateTime( $this->getPurchaseDate() );

		$objDateDiff = date_diff( $strExpiryDate, $strPurchasedDate );

		if( true != ( $objDateDiff->invert ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expiry_date', 'Expiry date should be greater than purcahse date.' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valInvoiceNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_number', 'Invoice Number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBulkAssets( $objDatabase ) {

		$boolIsValid = true;
		$objPsAssetBrands = \Psi\Eos\Admin\CPsAssetBrands::createService()->fetchPsAssetBrandsByPsAssetTypeId( $this->getPsAssetTypeId(), $objDatabase );

		if( false == valArr( $objPsAssetBrands ) || false == array_key_exists( $this->getPsAssetBrandId(), $objPsAssetBrands ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'brand_id', 'Selected Brand is not associated with Asset type.' ) );
			return $boolIsValid;
		}

		$objPsAssetModels = \Psi\Eos\Admin\CPsAssetModels::createService()->fetchPsAssetModelsByPsAssetBrandId( $this->getPsAssetBrandId(), $objDatabase );
		if( false == valArr( $objPsAssetModels ) || false == array_key_exists( $this->getPsAssetModelId(), $objPsAssetModels ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'model_id', 'Selected Model is not associated with selected Brand.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPsAssetStatus( $objDatabase ) {

		$boolIsValid = true;

		$objPsAssetType = new CPsAssetType();
		$objPsAssetType = CBasePsAssetTypes::fetchPsAssetTypeById( $this->getPsAssetTypeId(), $objDatabase );

		if( false == is_null( $objPsAssetType ) ) {

			if( CPsAssetType::SOFTWARE == $objPsAssetType->getPsAssetTypeId() ) {
				$arrintAssignedAssets	= \Psi\Eos\Admin\CEmployeeAssets::createService()->fetchEmployeeAssetsByPsAssetId( $this->getId(), $objDatabase );
				if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintAssignedAssets ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_satus_type_id', 'Selected asset is assigned to some employee.' ) );
					$boolIsValid = false;
				}

			} else {

				if( CPsAssetStatusType::ASSIGNED == $this->getPsAssetStatusTypeId() || CPsAssetStatusType::CHANGE_REQUESTED == $this->getPsAssetStatusTypeId() || CPsAssetStatusType::DAMAGE_REQUESTED == $this->getPsAssetStatusTypeId() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_satus_type_id', 'Selected asset is assigned to some employee.' ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valPsAssetRequestTypeId() {

		$boolIsValid = true;

		if( true == is_null( $this->getPsAssetStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_asset_type_id', 'Request is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsAssetId() {

		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Asset is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIpAddress() {
		$boolIsValid = true;

			if( false == is_null( $this->getIpAddress() ) && false == CValidation::validateIpAddress( $this->getIpAddress() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'Invalid IP address entered.' ) );
				$boolIsValid = false;
			}
		return $boolIsValid;
	}

	public function valIpSubnet( $intAssetType ) {
		$boolIsValid = true;
		if( CPsAssetType::PS_ASSET_TYPE_SWITCH == $this->getPsAssetTypeId() && $intAssetType == NULL ) {
			if( true == is_null( $this->getIpSubnet() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_subnet', 'Please enter IP Subnet' ) );
				$boolIsValid = false;
			}
			if( false == is_null( $this->getIpSubnet() ) && false == CValidation::validateIpSubnet( $this->getIpSubnet() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_subnet', 'Invalid IP Subnet entered.' ) );
				$boolIsValid = false;
			}
		}
		return $boolIsValid;
	}

	public function valInvoiceAttachment() {

		$boolIsValid = true;

		if( true == valArr( $_FILES ) ) {
			if( false == ( ( valStr( $_FILES['asset_invoice']['name'] ) ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document_id', 'Please attach Invoice File.' ) );
			}

			if( true == valStr( $_FILES['asset_invoice']['name'] ) ) {
				if( $_FILES['asset_invoice']['type'] != 'application/pdf' ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document_id', 'Please insert only PDF file.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $intAssetType= NULL, $intPrevDocumentId= false, $boolXentoEquipment = false ) {

		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valInvoiceNumber();
				$boolIsValid &= $this->valInvoiceAttachment( $intPrevDocumentId );
				$boolIsValid &= $this->valPsAssetTypeId();
				$boolIsValid &= $this->valPsAssetBrandId();
				$boolIsValid &= $this->valPsAssetModelId();
				$boolIsValid &= $this->valCountryCode();
				$boolIsValid &= $this->valOfficeId();
				$boolIsValid &= $this->valPurchaseDate();
				$boolIsValid &= $this->valPurchasedCost( $boolXentoEquipment );
				$boolIsValid &= $this->valPsAssetPurchaseSourcesId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCountryCode();
				if( 'validate_import_assets' != $strAction ) {
					$boolIsValid &= $this->valOfficeId();
				}
				$boolIsValid &= $this->valPsAssetTypeId();
				$boolIsValid &= $this->valPsAssetBrandId();
				$boolIsValid &= $this->valPurchaseDate();
				$boolIsValid &= $this->valExpiryDate();
				$boolIsValid &= $this->valIpAddress();
				$boolIsValid &= $this->valIpSubnet( $intAssetType );
				$boolIsValid &= $this->valCpuCoreNum( $intAssetType );
				$boolIsValid &= $this->valServiceTag( $strAction, $objDatabase );
				$boolIsValid &= $this->valUserLimit( $objDatabase );
				$boolIsValid &= $this->valLicenceNo( $strAction, $objDatabase );
				$boolIsValid &= $this->valAssetTag( $strAction, $objDatabase );
				$boolIsValid &= $this->valPurchasedCost( $boolXentoEquipment );
				if( $boolXentoEquipment ) {
					$boolIsValid &= $this->valInvoiceNumber();
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valPsAssetStatus( $objDatabase );
				break;

			case 'validate_import_assets':
				$boolIsValid &= $this->valOfficeId();
				$boolIsValid &= $this->valPurchaseDate();
				$boolIsValid &= $this->valInvoiceNumber();
				break;

			case 'validate_bulk_assets':
				$boolIsValid &= $this->valBulkAssets( $objDatabase );
				break;

			case 'VALIDATE_UPDATE_REQUEST':
				$boolIsValid &= $this->valPsAssetRequestTypeId();
				$boolIsValid &= $this->valId( $objDatabase );
				break;

			case 'VALIDATE_STATUS':
				$boolIsValid &= $this->valPsAssetStatusTypeId();
				break;

			case 'VALIDATE_ASSETS':
				$boolIsValid &= $this->valPsAssetId();
				break;

			case 'validate_filter_bulk_assets':
				$boolIsValid &= $this->valPsAssetTypeId();
				$boolIsValid &= $this->valPurchaseDate();
				$boolIsValid &= $this->valOfficeId();
				break;

			case 'validate_edit_bulk_assets':
				$boolIsValid &= $this->valPsAssetTypeId();
				$boolIsValid &= $this->valAssetTag( $strAction, $objDatabase );
				$boolIsValid &= $this->valMacTag( $strAction, $objDatabase );
				break;

			case 'VALIDATE_INSERT_IT_EQUIPMENT':
				$boolIsValid &= $this->valInvoiceNumber();
				$boolIsValid &= $this->valPsAssetTypeId();
				$boolIsValid &= $this->valPsAssetBrandId();
				$boolIsValid &= $this->valCountryCode();
				$boolIsValid &= $this->valOfficeId();
				$boolIsValid &= $this->valPurchaseDate();
				break;

			case 'VALIDATE_INSERT_ENTRATA_EQUIPMENT':
			case 'VALIDATE_UPDATE_ENTRATA_EQUIPMENT':
				$boolIsValid &= $this->valPsAssetTypeId( true );
				$boolIsValid &= $this->valPsAssetBrandId();
				$boolIsValid &= $this->valCountryCode();
				$boolIsValid &= $this->valOfficeId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );
		if( true == isset( $arrmixValues['asset_name'] ) ) 			$this->setName( $arrmixValues['asset_name'] );
		if( true == isset( $arrmixValues['brand_name'] ) ) 			$this->setBrandName( $arrmixValues['brand_name'] );
		if( true == isset( $arrmixValues['code'] ) ) 				$this->setCode( $arrmixValues['code'] );
		if( true == isset( $arrmixValues['employee_id'] ) ) 		$this->setEmployeeId( $arrmixValues['employee_id'] );
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = CStrings::strToIntDef( $intEmployeeId, NULL, false );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function setName( $strAssetName ) {
		$this->m_strName = CStrings::strTrimDef( $strAssetName, 50, NULL, true );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function setBrandName( $strBrandName ) {
		$this->m_strBrandName = CStrings::strTrimDef( $strBrandName, 50, NULL, true );
	}

	public function getBrandName() {
		return $this->m_strBrandName;
	}

	public function setCode( $strCode ) {
		$this->m_strCode = CStrings::strTrimDef( $strCode, 10, NULL, true );
	}

	public function getCode() {
		return $this->m_strCode;
	}

	public function generateAssetTag( $strPsAssetTypeCode, $boolIsIncrement = true, $objDatabase ) {
		$strAssetTag			= $this->getAssetTag();
		$strSql					= 'SELECT * FROM ps_assets WHERE id = ( SELECT max( id ) from ps_assets WHERE ps_asset_type_id = ' . ( int ) $this->getPsAssetTypeId() . ' AND country_code = \'' . $this->getCountryCode() . '\'' . ')';
		$arrmixPsAsset			= fetchData( $strSql, $objDatabase );

		if( true == $boolIsIncrement ) {
			if( true == valArr( $arrmixPsAsset ) ) {
				$arrmixPsAssetTag	= explode( '/', $arrmixPsAsset['0']['asset_tag'] );
				$intAssetCount		= $arrmixPsAssetTag['4'] + 1;
			} else {
				$intAssetCount		= 1;
			}
		} else {
			$arrmixAssetTag			= explode( '/', $strAssetTag );
			$intAssetCount			= $arrmixAssetTag['4'];
		}
		$objOffice				= \Psi\Eos\Admin\COffices::createService()->fetchOfficeById( $this->getOfficeId(), $objDatabase );

		if( is_null( $objOffice ) ) {
			$boolIsValid = false;
			return;
		}

		$strOffice	= explode( ' ', $objOffice->getName() );

		list( $intMonth, $intDay, $intYear ) = explode( '/', $this->getPurchaseDate() );
		$arrintYear = \Psi\CStringService::singleton()->str_split( $intYear, 2 );

		$strDate = round( $intMonth ) . '-' . round( $arrintYear[1] );

		$strPsAssetTag = 'DBX' . $strOffice[3] . '/IT/' . \Psi\CStringService::singleton()->strtoupper( $strPsAssetTypeCode ) . '/' . $strDate . '/' . round( $intAssetCount );
		$this->setAssetTag( $strPsAssetTag );

	}

	public function createEmployeeAsset() {

		$this->objEmployeeAsset = new CEmployeeAsset();

		return $this->objEmployeeAsset;
	}

	public function createAssetLog() {

		$this->objAssetLog = new CPsAssetLog();

		return $this->objAssetLog;
	}

	/**
	 * Other Functions
	 *
	 */

 	public function setUserLimit( $intUserLimit ) {
		( 0 < $intUserLimit ) ? $this->m_intUserLimit = $intUserLimit : $this->m_intUserLimit = NULL;
	}

}
?>