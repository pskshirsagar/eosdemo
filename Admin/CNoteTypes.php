<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CNoteTypes
 * Do not add any new functions to this class.
 */

class CNoteTypes extends CBaseNoteTypes {

	public static function fetchNoteTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CNoteType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchNoteType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CNoteType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

 	public static function fetchAllNoteTypes( $objDatabase ) {
		$strSql = 'SELECT *	FROM note_types WHERE is_published = 1';
		return self::fetchNoteTypes( $strSql, $objDatabase );
 	}
}
?>