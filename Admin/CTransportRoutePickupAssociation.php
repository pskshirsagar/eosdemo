<?php

class CTransportRoutePickupAssociation extends CBaseTransportRoutePickupAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransportRouteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransportPickupPointId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>