<?php

class CSurveyConditionType extends CBaseSurveyConditionType {

	const SELECTED					= 1;
	const NOT_SELECTED				= 2;
	const EQUAL						= 3;
	const NOT_EQUAL_TO				= 4;
	const GREATER_THAN				= 5;
	const GREATER_THAN_OR_EQUAL_TO	= 6;
	const LESS_THAN					= 7;
	const LESS_THAN_OR_EQUAL_TO		= 8;
	// const EMPTY                       = 9; Need to rename value as EMPTY is a keyword in PHP
	const NOT_EMPTY					= 10;
	const CONTAINS					= 11;
	const DOES_NOT_CONTAIN			= 12;
	const MATCHES_REGEX				= 13;
	const COUNT						= 14;
	const MINIMUM_LENGTH			= 15;
	const MAXIMUM_LENGTH			= 16;
	const CHARACTER_RANGE			= 17;
	const CONTENT_VALIDATION		= 18;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>