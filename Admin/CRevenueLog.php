<?php

class CRevenueLog extends CBaseRevenueLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChargeCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>