<?php

class CSemAndOrganicStat extends CBaseSemAndOrganicStat {

	protected $m_strSemAdGroupName;
	protected $m_strSemKeywordName;

	protected $m_intCallsCount;

	public function setSemAdGroupName( $strSemAdGroupName ) {
		$this->m_strSemAdGroupName = $strSemAdGroupName;
	}

	public function setSemKeywordName( $strSemAKeywordName ) {
		$this->m_strSemKeywordName = $strSemAKeywordName;
	}

	public function setCallsCount( $intCallsCount ) {
		$this->m_intCallsCount = $intCallsCount;
	}

	public function getSemAdGroupName() {
		return $this->m_strSemAdGroupName;
	}

	public function getSemKeywordName() {
		return $this->m_strSemKeywordName;
	}

	public function getCallsCount() {
		return $this->m_intCallsCount;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['sem_ad_group_name'] ) ) 		$this->setSemAdGroupName( $arrmixValues['sem_ad_group_name'] );
		if( true == isset( $arrmixValues['sem_keyword_name'] ) ) 		$this->setSemKeywordName( $arrmixValues['sem_keyword_name'] );
		if( true == isset( $arrmixValues['calls_count'] ) ) 			$this->setCallsCount( $arrmixValues['calls_count'] );

		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>