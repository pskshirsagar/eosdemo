<?php

class CPersonContactPreference extends CBasePersonContactPreference {

	public function getPerson() {
		return $this->m_objPerson;
	}

	public function setPerson( $objPerson ) {
		$this->m_objPerson = $objPerson;
	}

	public function valPersonTypeIdForDelete( $intPrimaryPersonsCount, $intIsPrimary ) {
		$boolIsValid = true;
		if( CPerson::PRIMARY_PERSON == $intIsPrimary && 1 == $intPrimaryPersonsCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_type_id', 'You cannot delete the primary contact.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $intPrimaryPersonsCount = NULL, $intIsPrimary = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valPersonTypeIdForDelete( $intPrimaryPersonsCount, $intIsPrimary );
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}
}
?>