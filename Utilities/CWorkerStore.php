<?php

class CWorkerStore extends CBaseWorkerStore {

	protected $m_strLocationName;

	public function valId() {
		return true;
	}

	public function valVendorId() {
		$boolIsValid = true;

		if( false == valId( $this->getVendorId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_id', 'Failed to load vendor' ) );
		}
		return $boolIsValid;
	}

	public function valWorkerId() {
		return true;
	}

	public function valStoreId() {
		$boolIsValid = true;

		if( false == valId( $this->getStoreId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'store_id', 'Failed to load store object' ) );
		}
		return $boolIsValid;
	}

	public function valIsPrimary() {
		return true;
	}

	// Setter functions

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = $strLocationName;
	}

	// Getter functions

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['location_name'] ) ) {
			$this->setLocationName( $arrmixValues['location_name'] );
		}
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valVendorId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>