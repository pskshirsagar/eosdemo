<?php

class CUtilityProviderDocument extends CBaseUtilityProviderDocument {

	protected $m_objUtilityDocument;

	public function getUtilityDocument() {
		return $this->m_objUtilityDocument;
	}

	public function setUtilityDocument( $objUtilityDocument ) {
		$this->m_objUtilityDocument = $objUtilityDocument;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityProviderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityProviderEventTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>