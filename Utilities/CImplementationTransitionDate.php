<?php

class CImplementationTransitionDate extends CBaseImplementationTransitionDate {

	protected $m_boolIsReschedule;
	protected $m_strGoalMonth;

	public function setIsReschedule( $boolIsReschedule ) {
		$this->m_boolIsReschedule = ( bool ) $boolIsReschedule;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Cid is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property id is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valImplementationMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->getImplementationMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'implementation_month', 'Please select Implementation Month.' ) );
		}

		return $boolIsValid;
	}

	public function valTransitionGoalDate() {
		$boolIsValid = true;
		if( true == is_null( $this->getTransitionGoalDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transition_goal_date', 'Transition Goal Date is invalid.' ) );
		}
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		if( true == is_null( $this->getNote() ) || false == valStr( $this->getNote() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note is required.' ) );
		}
		return $boolIsValid;
	}

	public function valIsInternalError() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsExternalError() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransitionDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsUnScheduleRequest = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPsProductId();

				if( false == $boolIsUnScheduleRequest ) {
					$boolIsValid &= $this->valImplementationMonth();
					$boolIsValid &= $this->valTransitionGoalDate();
				}

				if( true == $this->m_boolIsReschedule ) {
					$boolIsValid &= $this->valNote();
				}
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setGoalMonth( $strGoalMonth ) {
		$this->set( 'm_strGoalMonth', CStrings::strTrimDef( $strGoalMonth, -1, NULL, true ) );
	}

	public function getGoalMonth() {
		return $this->m_strGoalMonth;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['goal_month'] ) ) {
			$this->setGoalMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['goal_month'] ) : $arrmixValues['goal_month'] );
		}
	}

}
?>