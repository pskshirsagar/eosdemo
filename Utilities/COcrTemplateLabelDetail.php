<?php

class COcrTemplateLabelDetail extends CBaseOcrTemplateLabelDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrTemplateLabelId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillTemplateChargeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAccountTemplateChargeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsNotAvailable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLabelValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValuePosition() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>