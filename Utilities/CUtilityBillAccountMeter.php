<?php

class CUtilityBillAccountMeter extends CBaseUtilityBillAccountMeter {

	protected $m_arrintPropertyUtilityTypeIds;
	protected $m_strAccountNumber;
	protected $m_strDatabaseTransactionType;
	protected $m_arrintCommodityIds;

	public function __construct() {
		parent::__construct();
		$this->m_strDatabaseTransactionType = 'insert';
	}

	public function getPropertyUtilityTypeIds() {
		return $this->m_arrintPropertyUtilityTypeIds;
	}

	public function getCommodityIds() {
		return $this->m_arrintCommodityIds;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function setPropertyUtilityTypeIds( $arrintPropertyUtilityTypeIds ) {
		$this->m_arrintPropertyUtilityTypeIds = CStrings::strToArrIntDef( $arrintPropertyUtilityTypeIds );
	}

	public function setCommodityIds( $arrintCommodityIds ) {
		$this->m_arrintCommodityIds = CStrings::strToArrIntDef( $arrintCommodityIds );
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client Id is required.' ) );
		}
		return $boolIsValid;
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = $strAccountNumber;
	}

	public function getDatabaseTransactionType() {
		return $this->m_strDatabaseTransactionType;
	}

	public function setDatabaseTransactionType( $strDatabaseTransactionType ) {
		$this->m_strDatabaseTransactionType = $strDatabaseTransactionType;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property Id is required.' ) );
		}
		return $boolIsValid;
	}

	public function valMeterNumber() {
		$boolIsValid = true;

		if( false == \valStr( $this->getMeterNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_number', 'Meter Number is required.' ) );
		}
		return $boolIsValid;
	}

	public function valUtilityConsumptionTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityConsumptionTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_consumption_type_id', 'Consumption Type is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valUtilityConsumptionTypeId();
				$boolIsValid &= $this->valMeterNumber();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMeterNumber();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valMeterNumber();
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_utility_type_ids'] ) ) {
			$this->setPropertyUtilityTypeIds( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_utility_type_ids'] ) : $arrmixValues['property_utility_type_ids'] );
		}
		if( true == isset( $arrmixValues['account_number'] ) ) {
			$this->setAccountNumber( $arrmixValues['account_number'] );
		}
		if( true == isset( $arrmixValues['commodity_ids'] ) ) {
			$this->setCommodityIds( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['commodity_ids'] ) : $arrmixValues['commodity_ids'] );
		}
	}

}
?>