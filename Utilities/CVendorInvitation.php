<?php

class CVendorInvitation extends CBaseVendorInvitation {

	protected $m_strCompanyEmployeeName;

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valApPayeeId() {
		return true;
	}

	public function valApLegalEntityId() {
		return true;
	}

	public function valCompanyEmployeeId() {
		return true;
	}

	public function valApPayeeContactId() {
		return true;
	}

	public function valVendorInvitationId() {
		return true;
	}

	public function valRecipientName( $intCode ) {
		$boolIsValid = true;

		if( true == is_null( $this->getRecipientName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recipient_name' . $intCode, 'Name is required.' ) );
		}

		$strRecipientName = preg_replace( '/[^a-zA-Z\s]/', '', $this->getRecipientName() );

		if( \Psi\CStringService::singleton()->strlen( $this->getRecipientName() ) != \Psi\CStringService::singleton()->strlen( $strRecipientName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recipient_name' . $intCode, __( 'Only alphabetical characters allowed.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valRecipientEmailAddress( $intCode ) {

		if( false == valStr( $this->getRecipientEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recipient_email_address' . $intCode, 'Email is required.' ) );
		} elseif( false == CValidation::validateEmailAddresses( $this->m_strRecipientEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recipient_email_address' . $intCode, 'Email address is not valid.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valTaxNumberEncrypted() {
		return true;
	}

	public function valInvitationDatetime() {
		return true;
	}

	public function valIsSynced() {
		return true;
	}

	public function validate( $strAction, $intCode = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valRecipientEmailAddress( $intCode );
				$boolIsValid &= $this->valRecipientName( $intCode );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function setCompanyEmployeeName( $strCompanyEmployeeName ) {
		$this->m_strCompanyEmployeeName = CStrings::strTrimDef( $strCompanyEmployeeName, 100, NULL, true );
	}

	public function getCompanyEmployeeName() {
		return $this->m_strCompanyEmployeeName;
	}

	public function setRecipientEmailAddress( $strRecipientEmailAddress ) {
		$this->m_strRecipientEmailAddress = preg_replace( '/[ ,:;]\s*/', '', CStrings::strTrimDef( $strRecipientEmailAddress, 240, NULL, true ) );
	}

	public function setTaxNumberHash( $strTaxNumberDecrypted ) {
		$intPlainTaxNumber = Psi\Libraries\UtilStrings\CStrings::createService()->strTrimDef( str_replace( '-', '', $strTaxNumberDecrypted ) );

		if( true == is_numeric( $intPlainTaxNumber ) ) {
			parent::setTaxNumberHash( \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $intPlainTaxNumber, CONFIG_SODIUM_KEY_BLIND_INDEX ) );
		} else {
			parent::setTaxNumberHash( $intPlainTaxNumber );
		}
	}

}
?>