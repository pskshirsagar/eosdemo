<?php

class CUtilityInvoiceMessage extends CBaseUtilityInvoiceMessage {

	protected $m_objUtilityInvoiceMessage;

    /**
     * Set Methods
     */

    public function setLastUtilityInvoiceMessage( $objLastUtilityInvoiceMessage ) {
    	$this->m_objUtilityInvoiceMessage = $objLastUtilityInvoiceMessage;
    }

    public function setUtilityAccountId( $intUtilityAccountid ) {
    	$this->m_intUtilityAccountId = $intUtilityAccountid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['utility_account_id'] ) ) {
    		$this->setUtilityAccountId( $arrmixValues['utility_account_id'] );
	    }

    	return true;
    }

	/**
	 * Get Methods
	 */

    public function getLastUtilityInvoiceMessage() {
    	return $this->m_objUtilityInvoiceMessage;
    }

    public function valStartDate() {
        $boolIsValid = true;
        if( true == is_null( $this->getStartDate() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Please provide start date.' ) ) );

        } else {
        	$objLastUtilityInvoiceMessage = $this->getLastUtilityInvoiceMessage();

			if( true == valObj( $objLastUtilityInvoiceMessage, 'CUtilityInvoiceMessage' ) && $objLastUtilityInvoiceMessage->getId() != $this->getId()
	        	&& ( false == empty( $objLastUtilityInvoiceMessage ) && false == is_null( $objLastUtilityInvoiceMessage->getEndDate() ) )
	        	&& ( true == is_null( $this->getId() ) || $this->getId() >= $objLastUtilityInvoiceMessage->getId() )
	        	&& ( strtotime( $this->getStartDate() ) <= strtotime( $objLastUtilityInvoiceMessage->getEndDate() ) ) ) {
                        $boolIsValid = false;
                        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Please provide start date greater than previous message end date( %s, 0 ) ', [ $objLastUtilityInvoiceMessage->getEndDate() ] ) ) );
            }
        }

        return $boolIsValid;
    }

    public function valEndDate( $boolIsUpdate, $objDatabase ) {

	    $strMessage = '';
	    $boolIsValid = true;

	    $intUtilityInvoiceMessageCount = \Psi\Eos\Utilities\CUtilityInvoiceMessages::createService()->fetchOverlappedUtilityInvoiceMessagesByUtilityAccountIdById( $this->getId(), $this->getUtilityAccountId(), $this->getStartDate(), $this->getEndDate(), $objDatabase );

	    if( true == is_null( $this->getEndDate() ) ) {
        	if( true == $boolIsUpdate ) {
        		$strMessage = __( 'Please provide End Date' );
	        }
        } else {
        	if( strtotime( $this->getEndDate() ) < strtotime( $this->getStartDate() ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'Please provide end date greater than start date.' ) ) );
        	} else {
        		$strMessage = __( 'Start Date and End Date should not overlap with other message\'s dates.' );
	        }
        }

	    if( 0 < $intUtilityInvoiceMessageCount ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', $strMessage ) );
	    }

        return $boolIsValid;
    }

    public function valMessage() {
        $boolIsValid = true;

        if( true == is_null( $this->getMessage() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Please enter a message to show on invoice.' ) ) );
        }

        return $boolIsValid;
    }

    public function valImagePath() {
        $boolIsValid = true;

        if( true == is_null( $this->getImagePath() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'image_path', __( 'Please select any image to insert.' ) ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase, $strMessageType ) {

    	$boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:

	        $boolIsUpdate = false;
            	if( VALIDATE_UPDATE == $strAction ) {
            		$boolIsUpdate = true;
	            }
            	$boolIsValid &= $this->valStartDate();
            	$boolIsValid &= $this->valEndDate( $boolIsUpdate, $objDatabase );

            	if( 'TEXT' == $strMessageType ) {
            		$boolIsValid &= $this->valMessage();
            	} elseif( 'IMAGE' == $strMessageType ) {
            		$boolIsValid &= $this->valImagePath();
            	}
            	break;

            case VALIDATE_UPDATE_FOR_PREBILL:
            	if( 'TEXT' == $strMessageType ) {
            		$boolIsValid &= $this->valMessage();
            	} elseif( 'IMAGE' == $strMessageType ) {
            		$boolIsValid &= $this->valImagePath();
            	}
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>