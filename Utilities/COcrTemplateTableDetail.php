<?php

class COcrTemplateTableDetail extends CBaseOcrTemplateTableDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrTemplateLabelId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTableNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valColumnNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>