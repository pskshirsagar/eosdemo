<?php

class CUtilityConsumptionType extends CBaseUtilityConsumptionType {

	const GALLONS       = 5;
	const KWH           = 12;
	const GENERIC_UNITS = 3;
	const CF            = 61;
	const DTH_10THERMS  = 111;
	const HU            = 123;

	public function valCommodityId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommodityId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commodity_id', 'Commodity is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} elseif( true == preg_match( '/[\@|\#|\$|\1%|\^|\&|\!|\*]/', $this->getName() ) ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Invalid consumption type name.' ) );
        }

		return $boolIsValid;
	}

    public function valDuplicateName( $objUtilitiesDatabase ) {

    	$boolIsValid = true;
    	$strCondition = '';

    	if( false == is_null( $this->getId() ) ) {
    		$strCondition = ' AND id <> ' . $this->getId();
    	}

    	$intCount = \Psi\Eos\Utilities\CUtilityConsumptionTypes::createService()->fetchUtilityConsumptionTypeCount( 'WHERE lower( name ) = \'' . \Psi\CStringService::singleton()->strtolower( $this->getName() ) . '\' AND commodity_id  = ' . ( int ) $this->getCommodityId() . $strCondition, $objUtilitiesDatabase );

    	if( 0 != $intCount ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' Utility Consumption Type ( ' . $this->m_strName . ' ) is already exist.' ) );
    	}

    	return $boolIsValid;
    }

	public function valDependantInformation( $objUtilitiesDatabase ) {

		$boolIsValid = true;

		$intCount = \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchPropertyUtilityTypeCount( 'WHERE utility_consumption_type_id = ' . $this->getId(), $objUtilitiesDatabase );
        $intCountOfBillAccountMeters = \Psi\Eos\Utilities\CUtilityBillAccountMeters::createService()->fetchUtilityBillAccountMeterCount('WHERE utility_consumption_type_id = ' . $this->getId(), $objUtilitiesDatabase );

		if( 0 != $intCount || 0 != $intCountOfBillAccountMeters ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  $this->m_strName . ' utility consumption type cannot be removed because property_utility_type(s) & utility_bill_account_metere(s) depends on it.' ) );
		}

        $intCountOfUtilityTransactions = \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUtilityTransactionCount( 'WHERE utility_consumption_type_id = ' . $this->getId(), $objUtilitiesDatabase );
        $intCountOfMeterPartTypes = \Psi\Eos\Utilities\CMeterPartTypes::createService()->fetchMeterPartTypeCount('WHERE utility_consumption_type_id = ' . $this->getId(), $objUtilitiesDatabase );
        $intCountOfUtilityTypeBatches = \Psi\Eos\Utilities\CUtilityTypeBatches::createService()->fetchUtilityTypeBatchCount('WHERE utility_consumption_type_id = ' . $this->getId(), $objUtilitiesDatabase );

        if( 0 != $intCountOfUtilityTransactions || 0 != $intCountOfMeterPartTypes || 0!= $intCountOfUtilityTypeBatches ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  $this->m_strName . ' utility consumption type cannot be removed because utility_transaction(s), meter_part_type(s) & utility_type_batche(s) depends on it.' ) );
        }
		return $boolIsValid;
	}

	public function validate( $strAction,  $objUtilitiesDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCommodityId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDuplicateName( $objUtilitiesDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objUtilitiesDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public static function getStaticUtilityConsumptionTypes( $intUtilityTypeId = NULL ) {

		$arrintUtilityConsumptionTypes = [ CUtilityType::HOT_WATER => self::GALLONS, CUtilityType::COLD_WATER => self::GALLONS, CUtilityType::WATER => self::GALLONS, CUtilityType::SEWER => self::GALLONS,CUtilityType::GAS => self::CF,CUtilityType::ELECTRIC => self::KWH ];

		if( false == is_null( $intUtilityTypeId ) ) {
			if( true == array_key_exists( $intUtilityTypeId, $arrintUtilityConsumptionTypes ) ) {
				return $arrintUtilityConsumptionTypes[$intUtilityTypeId];
			}
			return NULL;
		}

		return $arrintUtilityConsumptionTypes;
	}

	public static function getStaticUtilityConsumptionTypeName( $intUtilityTypeId, $boolIsShowUnitText = true ) {

		$boolIsDefaultCase = false;

		switch( $intUtilityTypeId ) {
			case CUtilityType::WATER:
			case CUtilityType::COLD_WATER:
			case CUtilityType::HOT_WATER:
			case CUtilityType::SEWER:
				$strUtilityConsumptionTypeName = 'Gallons';
				break;

			case CUtilityType::GAS:
				$strUtilityConsumptionTypeName = 'CF\'s';
				break;

			case CUtilityType::ELECTRIC:
				$strUtilityConsumptionTypeName = 'KWH';
				break;

			default:
				$boolIsDefaultCase = true;
				$strUtilityConsumptionTypeName = 'Unit';
		}

		if( false == $boolIsDefaultCase && true == $boolIsShowUnitText ) {
			$strUtilityConsumptionTypeName .= ' / unit';
		}

		return $strUtilityConsumptionTypeName;
	}

}
?>