<?php

class COcrTemplateChargeLabelDetail extends CBaseOcrTemplateChargeLabelDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrLabelsTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLabelAvailable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLabelValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValuePosition() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>