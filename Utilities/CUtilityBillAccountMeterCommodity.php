<?php

class CUtilityBillAccountMeterCommodity extends CBaseUtilityBillAccountMeterCommodity {

	protected $m_strMeterNumber;
	protected $m_strAccountNumber;

	protected $m_intCommodityId;
	protected $m_intPropertyUtilityTypeId;

	/**
	 * SET FUNCTIONS
	 */

	public function setCommodityId( $intCommodityId ) {
		$this->m_intCommodityId = $intCommodityId;
	}

	public function setMeterNumber( $strMeterNumber ) {
		$this->m_strMeterNumber = $strMeterNumber;
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = $strAccountNumber;
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
	    $this->m_intPropertyUtilityTypeId = $intPropertyUtilityTypeId;
    }

	/**
	 * GET FUNCTIONS
	 */

	public function getCommodityId() {
		return $this->m_intCommodityId;
	}

	public function getMeterNumber() {
		return $this->m_strMeterNumber;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getPropertyUtilityTypeId() {
	    return $this->m_intPropertyUtilityTypeId;
    }

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_type_id'] ) ) {
			$this->setCommodityId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['commodity_id'] ) : $arrmixValues['commodity_id'] );
		}

		if( true == isset( $arrmixValues['meter_number'] ) ) {
			$this->setMeterNumber( $arrmixValues['meter_number'] );
		}

		if( true == isset( $arrmixValues['account_number'] ) ) {
			$this->setAccountNumber( $arrmixValues['account_number'] );
		}

        if( true == isset( $arrmixValues['property_utility_type_id'] ) ) {
            $this->setPropertyUtilityTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_utility_type_id'] ) : $arrmixValues['property_utility_type_id'] );
        }

		return true;
	}

}
?>