<?php

class CUtilityBillAuditStep extends CBaseUtilityBillAuditStep {

	const VERIFY_DATA					= 1;
	const VERIFY_BILL_ACCOUNT_DATA		= 2;
	const NEW_SERVICE_PERFORMED			= 3;
	const DUPLICATE_INVOICE				= 4;
	const CORRECTED_OR_FINAL_INVOICE	= 5;
	const PROVIDER_ERROR 				= 6;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAuditTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>