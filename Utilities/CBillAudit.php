<?php
class CBillAudit extends CBaseBillAudit {

	protected $m_strBillAuditCategoryName;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setBillAuditCategoryName( $strBillAuditCategoryName ) {
		$this->m_strBillAuditCategoryName = $strBillAuditCategoryName;
	}

	public function getBillAuditCategoryName() {
		return $this->m_strBillAuditCategoryName;
	}

}
?>