<?php

class CUtilityBillBatch extends CBaseUtilityBillBatch {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPageCounts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionCounts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>