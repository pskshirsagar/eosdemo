<?php

class CBillEscalation extends CBaseBillEscalation {

	protected $m_strBillEscalateTypeName;
	protected $m_strMemo;

	/**
	 *Set Methods
	 */

	public function setValues( $arrmixValues,$boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['bill_escalate_type_name'] ) ) {
			$this->setBillEscalateTypeName( $arrmixValues['bill_escalate_type_name'] );
		}
		if( true == isset( $arrmixValues['memo'] ) ) {
			$this->setMemo( $arrmixValues['memo'] );
		}
	}

	public function setBillEscalateTypeName( $strBillEscalateTypeName ) {
		$this->m_strBillEscalateTypeName = $strBillEscalateTypeName;
	}

	public function setMemo( $strMemo ) {
		$this->m_strMemo = $strMemo;
	}

	/**
	 *Get Methods
	 */

	public function getBillEscalateTypeName() {
		return $this->m_strBillEscalateTypeName;
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objBillEventLibrary = new CBillEventLibrary();
		$objBillEvent        = $objBillEventLibrary->createBillEvent( CBillEventType::ESCALATION, CBillEventSubType::ESCALATION_CREATED, CBillEventReference::ESCALATION, $this->getUtilityBillId() );
		$objBillEvent->setNewData( json_encode( [ 'bill_escalate_type_id' => $this->getBillEscalateTypeId() ] ) );

		if( false == $objBillEventLibrary->insertBillEvent( $objBillEvent, $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		// Un-setting ignore queue on utility bill if escalation is completed and there is no pending escalation.
		if( CUemBillStatusType::COMPLETED == $this->getUemBillStatusTypeId() ) {
			$intUnresolvedBillEscalationCount = \Psi\Eos\Utilities\CBillEscalations::createService()->fetchCustomUnresolvedBillEscalationCountByUtilityBillId( $this->getUtilityBillId(), $objDatabase );

			if( 0 == $intUnresolvedBillEscalationCount ) {
				fetchData( 'UPDATE utility_bills SET is_queue_ignored = false WHERE id = ' . ( int ) $this->getUtilityBillId(), $objDatabase, true );
			}
		}

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>