<?php

class CUtilityDistribution extends CBaseUtilityDistribution {

	protected $m_boolIsTestDistribution;

	protected $m_strPropertyNames;
	protected $m_strUtilityInvoiceTypeNames;

   public function valUtilityDistributionStepId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityDistributionStepId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_distribution_step_id', 'Please select utility distribution step id.' ) );
        }

        return $boolIsValid;
   }

   public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
   		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

   		if( true == isset( $arrmixValues['is_test_distribution'] ) ) {
   			$this->setIsTestDistribution( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_test_distribution'] ) : $arrmixValues['utility_type_name'] );
	    }

   }

   public function setIsTestDistribution( $boolIsTestDistribution ) {
   		$this->m_boolIsTestDistribution = CStrings::strToBool( $boolIsTestDistribution );
   }

   public function setPropertyNames( $strPropertyNames ) {
		$this->m_strPropertyNames = $strPropertyNames;
   }

   public function setUtilityInvoiceTypeNames( $strUtilityInvoiceTypeNames ) {
   		$this->m_strUtilityInvoiceTypeNames = $strUtilityInvoiceTypeNames;
   }

   public function getUtilityInvoiceTypeNames() {
   		return $this->m_strUtilityInvoiceTypeNames;
   }

   public function getPropertyNames() {
   		return $this->m_strPropertyNames;
   }

   public function getIsTestDistribution() {
   	 	$this->m_boolIsTestDistribution;
   }

   public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            		$boolIsValid &= $this->valUtilityDistributionStepId();
    			break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
   }

   public function fetchUtilityInvoices( $objDatabase ) {

	   return \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchUtilityInvoicesByUtilityDistributionId( $this->getId(), $objDatabase );
   }

   public function fetchUtilityBatches( $objDatabase ) {

	  return \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchUtilityBatchesByUtilityDistributionId( $this->getId(), $objDatabase );
   }

}
?>