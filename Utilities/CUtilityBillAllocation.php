<?php

class CUtilityBillAllocation extends CBaseUtilityBillAllocation {

	public $m_intUtilityBatchStepId;

	public $m_arrobjUtilityBillCharges;

	protected $m_arrobjUtilityBillChargeAllocations;
	protected $m_boolIsNewAllocation;

	public function setValues( $arrmixValues,$boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_batch_step_id'] ) ) {
			$this->setUtilityBatchStepId( $arrmixValues['utility_batch_step_id'] );
		}

	}

	public function createUtilityBillChargeAllocation() {

		$objUtilityBillChargeAllocation = new CUtilityBillChargeAllocation();
		$objUtilityBillChargeAllocation->setCid( $this->getCid() );
		$objUtilityBillChargeAllocation->setPropertyId( $this->getPropertyId() );
		$objUtilityBillChargeAllocation->setUtilityBillAllocationId( $this->getId() );
		return $objUtilityBillChargeAllocation;
	}

	public function createUtilityBillAllocation() {

		$objUtilityBillAllocation = new CUtilityBillAllocation();
		return $objUtilityBillAllocation;
	}

	public function getUtilityBatchStepId() {
		return $this->m_intUtilityBatchStepId;
	}

	public function setUtilityBatchStepId( $intUtilityBatchStepId ) {
		$this->m_intUtilityBatchStepId = $intUtilityBatchStepId;
	}

	public function setUtilityBillChargeAllocation( $objUtilityBillChargeAllocation ) {
		$this->m_arrobjUtilityBillChargeAllocations[] = $objUtilityBillChargeAllocation;
	}

	public function getUtilityBillChargeAllocation() {
		return $this->m_arrobjUtilityBillCharges;
	}

	public function setUtilityBillChargeAllocations( $arrobjUtilityBillChargeAllocations ) {
		$this->m_arrobjUtilityBillChargeAllocations = $arrobjUtilityBillChargeAllocations;
	}

	public function getUtilityBillChargeAllocations() {
		return $this->m_arrobjUtilityBillChargeAllocations;
	}

	public function getIsNewAllocation() {
		return $this->m_boolIsNewAllocation;
	}

	public function setIsNewAllocation( $boolIsNewAllocation = false ) {
		$this->m_boolIsNewAllocation = $boolIsNewAllocation;
	}

	public function unsetAllErrorMsgs() {
		$this->m_arrobjErrorMsgs = NULL;
	}

}
?>