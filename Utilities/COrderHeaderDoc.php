<?php

class COrderHeaderDoc extends CBaseOrderHeaderDoc {


	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strFileTitle;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['file_name'] ) && $boolDirectSet ) {
			$this->m_strFileName = trim( stripcslashes( $arrmixValues['file_name'] ) );
		} elseif( isset( $arrmixValues['file_name'] ) ) {
			$this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['file_name'] ) : $arrmixValues['file_name'] );
		}

		if( isset( $arrmixValues['file_path'] ) && $boolDirectSet ) {
			$this->m_strFilePath = trim( stripcslashes( $arrmixValues['file_path'] ) );
		} elseif( isset( $arrmixValues['file_path'] ) ) {
			$this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['file_path'] ) : $arrmixValues['file_path'] );
		}

		if( isset( $arrmixValues['title'] ) && $boolDirectSet ) {
			$this->m_strFileTitle = trim( stripcslashes( $arrmixValues['title'] ) );
		} elseif( isset( $arrmixValues['title'] ) ) {
			$this->setFileTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['title'] ) : $arrmixValues['title'] );
		}
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	public function setFileTitle( $strFileTitle ) {
		$this->m_strFileTitle = $strFileTitle;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function getFileTitle() {
		return $this->m_strFileTitle;
	}

}
?>