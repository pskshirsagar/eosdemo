<?php

class CPropertyCommodityDocument extends CBasePropertyCommodityDocument {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valUtilityDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valNewUtilityDocumentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
                $boolIsValid = false;
                break;
        }

        return $boolIsValid;
    }

}
?>