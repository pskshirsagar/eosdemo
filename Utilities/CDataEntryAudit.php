<?php

class CDataEntryAudit extends CBaseDataEntryAudit {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDataEntryAuditDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartAuditDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndAuditDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastAuditedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCorrectDataPoints() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalDataPoints() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsError() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>