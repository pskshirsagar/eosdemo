<?php

class CBillEventType extends CBaseBillEventType {

	const CREATED       = 1;
	const SPLIT         = 2;
	const ASSOCIATION   = 3;
	const PROCESSING    = 4;
	const ESCALATION    = 5;
	const AUDITS		= 6;
	const REVIEW		= 7;
	const EXPORTED		= 8;
	const DELETED		= 9;
	const QUEUE_SWITCH	= 10;
	const ESTIMATED     = 11;

	public static function getBillEventTypeName( $intBillEventTypeId ) {

		$arrmixBillEventTypes = [
			self::CREATED     => 'Created',
			self::SPLIT       => 'Split',
			self::ASSOCIATION => 'Association',
			self::PROCESSING  => 'Processing',
			self::ESCALATION  => 'Escalation',
			self::AUDITS      => 'Audit',
			self::REVIEW      => 'Review',
		];

		return getArrayElementByKey( $intBillEventTypeId, $arrmixBillEventTypes );
	}

}
?>