<?php

class CUtilityBillTemplate extends CBaseUtilityBillTemplate {

    public function valId() {
        $boolIsValid = true;

        if( true == is_null( $this->getId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Id doesn\'t exists.' ) );
        }

        return $boolIsValid;
    }

    public function valUtilityProviderId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityProviderId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_provider_id', 'Utility Bill Provider is required.' ) );
        }

        return $boolIsValid;
    }

    public function valSampleUtilityDocumentId() {
        $boolIsValid = true;

        if( true == is_null( $this->getSampleUtilityDocumentId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sample_utility_document_id', 'Please Upload a Sample Bill Template.' ) );
        }

        return $boolIsValid;
    }

    public function valBillTemplate( $objDatabase ) {

    	$boolIsValid = true;

    	$arrobjUtilityBillAccounts = \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchUtilityBillAccountsByUtilityBillTemplateId( $this->getId(), $objDatabase );

    	if( true == valArr( $arrobjUtilityBillAccounts ) ) {
    		$boolIsValid = false;

    		foreach( $arrobjUtilityBillAccounts as $objUtilityBillAccount ) {
    			$arrstrUtilityBillAccountNames[] = $objUtilityBillAccount->getName();
    		}

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Unable to Delete, its mapped with following Utility Bill Account(s). Unlink first to delete it. <br /><br /> \'' . implode( ',', $arrstrUtilityBillAccountNames ) . '\'.' ) );
    	}

        $intCountOfTemplatecharges = \Psi\Eos\Utilities\CUtilityBillTemplateCharges::createService()->fetchUtilityBillTemplateChargeCount( 'WHERE utility_bill_template_id = ' . $this->getId(), $objDatabase );
        $intCountOfUtilityBills = \Psi\Eos\Utilities\CUtilityBills::createService()->fetchUtilityBillCount('WHERE utility_bill_template_id = ' . $this->getId(), $objDatabase );

        if( 0 != $intCountOfTemplatecharges || 0 != $intCountOfUtilityBills ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  $this->m_strName . ' utility bill template cannot be removed because utility_bill_template_charge(s) & utility_bill(s) depends on it.' ) );
        }

    	return $boolIsValid;
    }

    public function valName( $objDatabase = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Template Name is required.' ) );
        }

        if( true == isset( $objDatabase ) && 0 < \Psi\Eos\Utilities\CUtilityBillTemplates::createService()->fetchUtilityBillTemplateByNameByUtilityProviderId( $this->getId(), $this->getName(), $this->getUtilityProviderId(), $objDatabase ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Template Name already exists for same provider.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {

            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valUtilityProviderId();
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId();
            	$boolIsValid &= $this->valBillTemplate( $objDatabase );
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function fetchUtilityDocument( $objDatabase ) {
    	return \Psi\Eos\Utilities\CUtilityDocuments::createService()->fetchUtilityDocumentById( $this->getSampleUtilityDocumentId(), $objDatabase );
    }

	public function fetchUtilityBillTemplateCharges( $objDatabase ) {
    	return \Psi\Eos\Utilities\CUtilityBillTemplateCharges::createService()->fetchUtilityBillTemplateChargesByUtilityBillTemplateId( $this->getId(), $objDatabase );
    }

    public function fetchUtilityProvider( $objDatabase ) {
    	return \Psi\Eos\Utilities\CUtilityProviders::createService()->fetchUtilityProviderByUtilityBillTemplateId( $this->getId(), $objDatabase );
    }

	public function fetchUtilityBillTemplateChargesByUtilityBillAccountId( $intUtilityBillAccountId, $objDatabase ) {
    	return \Psi\Eos\Utilities\CUtilityBillTemplateCharges::createService()->fetchUtilityBillTemplateChargesByUtilityBillTemplateIdByUtilityBillAccountId( $this->getId(), $intUtilityBillAccountId, $objDatabase );
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

    	$arrstrTempOriginalValues 	= $arrstrOriginalValues = $arrstrOriginalValueChanges = [];
	 	$boolChangeFlag				= false;

    	$arrmixOriginalValues = fetchData( 'SELECT * FROM utility_bill_templates WHERE id = ' . ( int ) $this->getId(), $objDatabase );

    	$arrstrTempOriginalValues = $arrmixOriginalValues[0];

    	foreach( $arrstrTempOriginalValues as $strKey => $strMixOriginalValue ) {

    		$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
    		$strFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

    		if( CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $strMixOriginalValue ) {
    			$arrstrOriginalValueChanges[$strKey] 	= $this->$strFunctionName();
    			$arrstrOriginalValues[$strKey] 			= $strMixOriginalValue;
    			$boolChangeFlag 						= true;
    		}
    	}

    	$boolUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

    	if( false != $boolUpdateStatus ) {

    		if( false == $boolSkipLog && false != $boolChangeFlag ) {
    			$objUtilitySettingLog = new CUtilitySettingLog();

    			$objUtilitySettingLog->setTableName( 'utility_bill_templates' );
    			$objUtilitySettingLog->setReferenceId( $this->getId() );
    			$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
    			$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValueChanges ) ) );
    			$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

    			if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
    				return false;
    			}
    		}

    		return $boolUpdateStatus;
    	}

    	return true;
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

    	$arrmixColumnNames = fetchData( 'select column_name from information_schema.columns where table_name = \'utility_bill_templates\' ', $objDatabase );

    	foreach( $arrmixColumnNames as $arrmixColumnName ) {

    		$strFunctionName 										= 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $arrmixColumnName['column_name'] ) ) );
    		$arrstrOriginalValues[$arrmixColumnName['column_name']] = $this->$strFunctionName();
			if( 'template_datetime' == $arrmixColumnName['column_name'] ) {

				$arrstrOriginalValues[$arrmixColumnName['column_name']] = date( 'Y-m-d H:i:s T' );
			}

    	}

    	$boolInsertStatus = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

    	if( false != $boolInsertStatus && false == $boolSkipLog ) {

    		$objUtilitySettingLog = new CUtilitySettingLog();
    		$objUtilitySettingLog->setTableName( 'utility_bill_templates' );
    		$objUtilitySettingLog->setReferenceId( $this->getId() );
    		$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
    		$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

    		if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
    			return false;
    		}

    		return $boolInsertStatus;

    	}

    	return true;

    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

     	$arrmixOriginalValues 		= fetchData( 'SELECT * FROM utility_bill_templates WHERE id = ' . ( int ) $this->getId(), $objDatabase );
     	$arrstrTempOriginalValues	= $arrmixOriginalValues[0];
     	foreach( $arrstrTempOriginalValues as $strKey => $strMixOriginalValue ) {

     		$arrstrOriginalValues[$strKey]	= $strMixOriginalValue;

     	}

     	$boolDeleteStatus = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

     	if( false != $boolDeleteStatus ) {

     		if( false == $boolSkipLog ) {

     			$objUtilitySettingLog = new CUtilitySettingLog();
     			$objUtilitySettingLog->setTableName( 'utility_bill_templates' );
     			$objUtilitySettingLog->setReferenceId( $this->getId() );
     			$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
     			$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

     			if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
     				return false;
     			}

     		}
     		return $boolDeleteStatus;
     	}

     	return true;
    }

}
?>