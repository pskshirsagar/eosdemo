<?php

class CUemReportData extends CBaseUemReportData {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillsCreatedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillsExportedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDueDateExportTenDayCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDueDateExportFiveDayCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDueDateExportAverageDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReceiptToExportAverageTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReceiptBillDateAverageDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReceiptBillDateFiveDayCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDueDateExportLateCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDueDateBillCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillDateBillCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisconnectNoticesCreatedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisconnectNoticesCompletedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisconnectNoticesLateCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisconnectNoticesAverageTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLateFeeUtilityBillCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationScheduledPropertyCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationOnTimePropertyCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationLatePropertyCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationAccountsCreated() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationAccountsCreatedPostGoLive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedBillCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedBillAverageTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedBillOnTimeCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedBillOcrCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedBillCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedBillAverageTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedBillOnTimeCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedBillErrorCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAuditsCreatedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAuditsCompletedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAuditsAverageTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAuditsCompletedOnTimeCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBillsCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReceiptToExportTimelinessCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRteTimelinessTwentyFourHoursCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAccountsCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAccountsCoaConfirmedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>