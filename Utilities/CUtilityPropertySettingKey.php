<?php

class CUtilityPropertySettingKey extends CBaseUtilityPropertySettingKey {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityPropertySettingGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsImplementationRequired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDefaultCopy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsBulkEditable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsClientSpecific() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>