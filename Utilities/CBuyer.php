<?php

class CBuyer extends CBaseBuyer {

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valCompanyName() {
		return true;
	}

	public function valDisabledBy() {
		return true;
	}

	public function valDisabledOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>