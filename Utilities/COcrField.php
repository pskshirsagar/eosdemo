<?php

class COcrField extends CBaseOcrField {

	const CID		= 1;
	const ACCOUNT_NUMBER	= 2;
	const BILL_DATE		= 3;
	const DUE_DATE		= 4;
	const SERVICE_PERIOD			= 5;
	const START_READ		= 6;
	const END_READ			= 7;
	const PREVIOUS_BALANCE			= 8;
	const CURRENT_AMOUNT			= 9;
	const BILL_CHARGES			= 11;
	const DUE_UPON_RECEIPT  = 12;
	const DO_NOT_PAY = 13;

	public static function getProviderOcrFields() {
		return [
			self::ACCOUNT_NUMBER => 'Account Number',
			self::BILL_DATE      => 'Bill Date',
			self::DUE_DATE       => 'Due Date',
			self::DUE_UPON_RECEIPT => 'Due Upon Receipt',
			self::DO_NOT_PAY => 'Do Not Pay',
			self::CURRENT_AMOUNT => 'Total Amount'
		];
	}

	public static function getOcrFieldName( $intBillEventTypeId ) {

		$arrmixBillEventTypes = [
			self::CID            => 'Cid',
			self::ACCOUNT_NUMBER => 'AccountNumber',
			self::BILL_DATE      => 'BillDate',
			self::DUE_DATE       => 'DueDate',
			self::CURRENT_AMOUNT => 'TotalAmount',
		];

		return getArrayElementByKey( $intBillEventTypeId, $arrmixBillEventTypes );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>