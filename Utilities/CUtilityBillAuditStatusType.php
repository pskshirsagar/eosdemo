<?php

class CUtilityBillAuditStatusType extends CBaseUtilityBillAuditStatusType {

	const NEW						 = 1;
	const IN_PROGRESS				 = 2;
	const AWAITING_CLIENT_RESPONSE	 = 3;
	const AWAITING_PROVIDER_RESPONSE = 4;
	const COMPLETE					 = 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function loadUtilityBillAuditStatusTypeNames() {
		return [
			self::NEW							=> 'New',
			self::IN_PROGRESS					=> 'In Progress',
			self::AWAITING_CLIENT_RESPONSE		=> 'Awaiting Client Response',
			self::AWAITING_PROVIDER_RESPONSE	=> 'Awaiting Provider Response',
			self::COMPLETE						=> 'Complete'
		];
	}

}
?>