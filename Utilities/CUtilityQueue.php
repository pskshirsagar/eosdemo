<?php

class CUtilityQueue extends CBaseUtilityQueue {

	const RU_WEB_RETRIEVAL						= 1;
	const UNASSOCIATED_UTILITY_BILLS			= 2;
	const UNASSOCIATED_UTILITY_BILLS_IN_REVIEW	= 3;
	const RU_BILLS_IN_REVIEW					= 4;
	const UNPROCESSED_UEM_BILLS					= 5;
	const UEM_BILLS_IN_REVIEW					= 6;
	const UNPROCCESSED_RU_BILLS					= 7;
	const UNASSOCIATED_IP_BILLS					= 8;
	const UNASSOCIATED_IP_BILLS_IN_REVIEW		= 9;
	const UNPROCESSED_IP_BILLS					= 10;
	const IP_BILLS_IN_REVIEW					= 11;
	const UTILITY_DATA_ENTRY_AUDIT				= 12;
	const IP_DATA_ENTRY_AUDIT					= 13;
	const GLOBAL_QUEUE_ACCESS					= 14;
	const UEM_WEB_RETRIEVAL					    = 15;
	const SUPER_USER                            = 16;

	public static $c_arrintUtilitiesQueues = [
		self::RU_WEB_RETRIEVAL,
		self::UNASSOCIATED_UTILITY_BILLS,
		self::UNASSOCIATED_UTILITY_BILLS_IN_REVIEW,
		self::RU_BILLS_IN_REVIEW,
		self::UNPROCESSED_UEM_BILLS,
		self::UEM_BILLS_IN_REVIEW,
		self::UNPROCCESSED_RU_BILLS,
		self::UEM_WEB_RETRIEVAL
	];

	public static $c_arrintInvoiceProcessingQueues = [
		self::UNASSOCIATED_IP_BILLS,
		self::UNASSOCIATED_IP_BILLS_IN_REVIEW,
		self::UNPROCESSED_IP_BILLS,
		self::IP_BILLS_IN_REVIEW
	];

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getUtilityQueues() {
		return [
			self::RU_WEB_RETRIEVAL                     => 'RU Web Retrieval',
			self::UNASSOCIATED_UTILITY_BILLS           => 'Unassociated Utility Bills',
			self::UNASSOCIATED_UTILITY_BILLS_IN_REVIEW => 'Unassociated Utility Bills In Review',
			self::RU_BILLS_IN_REVIEW                   => 'RU Bills In Review',
			self::UNPROCESSED_UEM_BILLS                => 'Unprocessed UEM Bills',
			self::UEM_BILLS_IN_REVIEW                  => 'UEM Bills In Review',
			self::UNPROCCESSED_RU_BILLS                => 'Unprocessed RU Bills',
			self::UNASSOCIATED_IP_BILLS                => 'Unassociated IP Bills',
			self::UNASSOCIATED_IP_BILLS_IN_REVIEW      => 'Unassociated IP Bills In Review',
			self::UNPROCESSED_IP_BILLS                 => 'Unprocessed IP Bills',
			self::IP_BILLS_IN_REVIEW                   => 'IP Bills In Review',
			self::UTILITY_DATA_ENTRY_AUDIT             => 'Utility Data Entry Audit',
			self::IP_DATA_ENTRY_AUDIT                  => 'IP Data Entry Audit',
			self::UEM_WEB_RETRIEVAL                    => 'UEM Web Retrieval',
		];
	}

}

?>