<?php

class CUtilityInvoiceAdditionalInformation extends CBaseUtilityInvoiceAdditionalInformation {

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Please insert title.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Please insert description.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valImagePath() {
		$boolIsValid = true;

		if( true == is_null( $this->getImagePath() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'image_path', 'Please upload valid image.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valUrl() {
		$boolIsValid = true;

		if( true == is_null( $this->getUrl() ) || false == filter_var( $this->getUrl(), FILTER_VALIDATE_URL ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Please insert valid URL.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSavings() {
		$boolIsValid = true;

		if( false == is_null( $this->getSavings() ) && ( false == is_numeric( $this->getSavings() ) || 0 == $this->getSavings() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Savings Must Be Numeric Value And Should Not Be Zero.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valImagePath();
				$boolIsValid &= $this->valUrl();
				$boolIsValid &= $this->valSavings();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>