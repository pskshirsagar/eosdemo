<?php

class CAdditionalInsurer extends CBaseAdditionalInsurer {

	protected $m_intPropertyId;
	protected $m_intPropertyCount;

	protected $m_arrintApPayeePropertyGroups;

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valName() {
		$boolIsValid = true;

		if( false != is_null( $this->getName() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;

		if( false != is_null( $this->getStreetLine1() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Street Information is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valStreetLine2() {
		return true;
	}

	public function valCity() {
		$boolIsValid = true;

		if( true == is_null( $this->getCity() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valStateCode( $objDatabase = NULL ) {

		if( false != is_null( $this->getStateCode() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'State is required.' ) ) );
			$boolIsValid = false;
		} elseif( false == is_null( $objDatabase ) && false == valObj( \Psi\Eos\Utilities\CStates::createService()->fetchStateByCode( $this->getStateCode(), $objDatabase ), 'CState' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'Please select valid state.' ) ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valPostalCode() {

		$intLength = strlen( str_replace( '-', '', $this->getPostalCode() ) );

		if( false != is_null( $this->getPostalCode() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Zip Code is required.' ) ) );
			$boolIsValid = false;
		} elseif( 0 < $intLength && ( false == preg_match( '/^\d{5,5}?$/', $this->getPostalCode() ) && false == preg_match( '/^\d{5,5}-\d{4,4}?$/', $this->getPostalCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Zip code must be {%d, 0} or {%d, 1} characters in XXXXX or XXXXX-XXXX format.', [ 5, 10 ] ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valStateCode( $objDatabase );
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valApPayeePropertyGroups();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Validation Functions
	 */

	public function valApPayeePropertyGroups() {
		$boolIsValid = true;

		if( false == valArr( $this->getApPayeePropertyGroups() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property', __( 'At least one (1) property or property group must be selected.' ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_count'] ) && $boolDirectSet ) {
			$this->m_intPropertyCount = trim( stripcslashes( $arrmixValues['property_count'] ) );
		} elseif( true == isset( $arrmixValues['property_count'] ) ) {
			$this->setPropertyCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_count'] ) : $arrmixValues['property_count'] );
		}

		if( true == isset( $arrmixValues['property_id'] ) && $boolDirectSet ) {
			$this->m_intPropertyId = trim( stripcslashes( $arrmixValues['property_id'] ) );
		} elseif( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_id'] ) : $arrmixValues['property_id'] );
		}
	}

	public function setApPayeePropertyGroups( $arrintApPayeePropertyGroups ) {
		$this->m_arrintApPayeePropertyGroups = ( array ) $arrintApPayeePropertyGroups;
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = CStrings::strToIntDef( $intPropertyCount );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId );
	}

	/**
	 * Get Functions
	 */

	public function getApPayeePropertyGroups() {
		return $this->m_arrintApPayeePropertyGroups;
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getAdditionalInsurerAddress() {

		$strAddress = '<br>' . $this->getStreetLine1();

		if( false == is_null( $this->getStreetLine2() ) ) {
			$strAddress .= '<br>' . $this->getStreetLine2();
		}

		$strAddress .= '<br>' . $this->getCity() . ' ' . $this->getStateCode() . ' ' . $this->getPostalCode();

		return $strAddress;
	}

}
?>