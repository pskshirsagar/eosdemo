<?php

class CUtilityBillAccount extends CBaseUtilityBillAccount {

	protected $m_boolIsFromInterface;

	protected $m_strNewAccountNumber;
	protected $m_strUtilityProviderName;
	protected $m_strSummaryAccountNumber;
	protected $m_strPropertyUtilityTypeName;
	protected $m_strUtilityBillReceiptTypeName;
	protected $m_strUtilityBillPaymentTypeName;

	protected $m_intApRemittanceId;
	protected $m_intPropertyUnitId;
	protected $m_intApPayeeStatusTypeId;
	protected $m_intUtilityCredentialId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intUtilityBillReceiptTypeId;
	protected $m_intUtilityBillAccountsCount;

	protected $m_objUtilityBillAccountDetail;

	protected $m_arrintPropertyBuildingIds;
	protected $m_strDatabaseTransactionType;

	public static $c_arrmixAllocationFrequencies = [
		CFrequency::FREQUENCY_NAME_MONTHLY			=> 1,
		CFrequency::FREQUENCY_NAME_BI_MONTHLY		=> 2,
		CFrequency::FREQUENCY_NAME_QUARTERLY		=> 3,
		CFrequency::FREQUENCY_NAME_BI_ANNUALLY		=> 6,
		CFrequency::FREQUENCY_NAME_SEMI_ANNUALLY	=> 6,
		CFrequency::FREQUENCY_NAME_YEARLY			=> 12
	];

	const DEFAULT_REQUIRED_BILLS_IN_PERIOD = 1;
	const VALIDATE_INSERT_SMART_BILL_ACCOUNT = 'validate_insert_smart_bill_account';
	const VALIDATE_UTILITY_BILL_ACCOUNT_CHARGE_TAB = 'utility_bill_account_charges';

	const STR_ACTIVE_TAB_TXT					= 'active';
	const STR_INACTIVE_TAB_TXT					= 'inactive';
	const STR_UNCONFIGURED_TAB_TXT				= 'unconfigured';
	const STR_IN_REVIEW_TAB_TXT					= 'in_review';
	const STR_TEMPLATES_TAB_TXT					= 'templates';

	const VALIDATE_UBA_RECEIPT_SETTINGS_TAB		= 'receipt_settings';
	const VALIDATE_UBA_UEM_RU_SETTINGS_TAB		= 'uem_and_ru_settings';
	const VALIDATE_UBA_INFORMATION_SETTINGS_TAB	= 'information';

	const INFORMATION_SETTINGS_TAB				= 'information';
	const RECEIPT_SETTINGS_TAB					= 'receipt_settings';
	const UEM_RU_SETTINGS_TAB					= 'uem_and_ru_settings';
	const UBA_METERS_TAB						= 'utility_bill_account_meters';
	const UBA_CHARGES_TAB						= 'utility_bill_account_charges';

	public function __construct() {
		parent::__construct();

		$this->m_strUtilityProviderName 		= NULL;
		$this->m_strPropertyUtilityTypeName 	= NULL;
		$this->m_strUtilityBillReceiptTypeName 	= NULL;
		$this->m_strUtilityBillPaymentTypeName 	= NULL;
		$this->m_strDatabaseTransactionType = 'insert';

	}

	/**
	 * Get Methods
	 */

	public function getUtilityProviderName() {
		return $this->m_strUtilityProviderName;
	}

	public function getPropertyUtilityTypeName() {
		return $this->m_strPropertyUtilityTypeName;
	}

	public function getUtilityBillReceiptTypeName() {
		return $this->m_strUtilityBillReceiptTypeName;
	}

	public function getUtilityBillPaymentTypeName() {
		return $this->m_strUtilityBillPaymentTypeName;
	}

	public function getUtilityBillEmailAddress() {
		return 'utility_bill_' . $this->getId() . '@residentutility.com';
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function getUtilityBillAccountsCount() {
		return $this->m_intUtilityBillAccountsCount;
	}

	public function getApPayeeStatusTypeId() {
		return $this->m_intApPayeeStatusTypeId;
	}

	public function getIsFromInterface() {
		return $this->m_boolIsFromInterface;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getPropertyBuildingIds() {
		return $this->m_arrintPropertyBuildingIds;
	}

	public function getSummaryAccountNumber() {
		return $this->m_strSummaryAccountNumber;
	}

	public function getNewAccountNumber() {
		return $this->m_strNewAccountNumber;
	}

	public function getApRemittanceId() {
		return $this->m_intApRemittanceId;
	}

	public function getUtilityCredentialId() {
		return $this->m_intUtilityCredentialId;
	}

	public function getDatabaseTransactionType() {
		return $this->m_strDatabaseTransactionType;
	}

	public function getUtilityBillAccountDetail() {
		return $this->m_objUtilityBillAccountDetail;
	}

	public function getIsFromFdgAccount() {
		return $this->m_boolIsFromFdgAccount;
	}

	/**
	 * Set Methods
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_provider_name'] ) ) {
			$this->setUtilityProviderName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_provider_name'] ) : $arrmixValues['utility_provider_name'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_name'] ) ) {
			$this->setPropertyUtilityTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_utility_type_name'] ) : $arrmixValues['property_utility_type_name'] );
		}
		if( true == isset( $arrmixValues['utility_bill_receipt_type_name'] ) ) {
			$this->setUtilityBillReceiptTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_receipt_type_name'] ) : $arrmixValues['utility_bill_receipt_type_name'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_id'] ) ) {
			$this->setPropertyUtilityTypeId( $arrmixValues['property_utility_type_id'] );
		}
		if( true == isset( $arrmixValues['utility_bill_accounts_count'] ) ) {
			$this->setUtilityBillAccountsCount( $arrmixValues['utility_bill_accounts_count'] );
		}
		if( true == isset( $arrmixValues['ap_payee_status_type_id'] ) ) {
			$this->setApPayeeStatusTypeId( $arrmixValues['ap_payee_status_type_id'] );
		}
		if( true == isset( $arrmixValues['property_unit_id'] ) ) {
			$this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		}
		if( true == isset( $arrmixValues['property_building_ids'] ) ) {
			$this->setPropertyBuildingIds( $arrmixValues['property_building_ids'] );
		}
		if( true == isset( $arrmixValues['summary_account_number'] ) ) {
			$this->setSummaryAccountNumber( $arrmixValues['summary_account_number'] );
		}
		if( true == isset( $arrmixValues['new_account_number'] ) ) {
			$this->setNewAccountNumber( $arrmixValues['new_account_number'] );
		}
		if( true == isset( $arrmixValues['ap_remittance_id'] ) ) {
			$this->setApRemittanceId( $arrmixValues['ap_remittance_id'] );
		}

		if( true == isset( $arrmixValues['utility_credential_id'] ) ) {
			$this->setUtilityCredentialId( $arrmixValues['utility_credential_id'] );
		}

	}

	public function setUtilityProviderName( $strUtilityProviderName ) {
		$this->m_strUtilityProviderName = $strUtilityProviderName;
	}

	public function setUtilityBillReceiptTypeName( $strUtilityBillReceiptTypeName ) {
		$this->m_strUtilityBillReceiptTypeName = $strUtilityBillReceiptTypeName;
	}

	public function setPropertyUtilityTypeName( $strPropertyUtilityTypeName ) {
		$this->m_strPropertyUtilityTypeName = $strPropertyUtilityTypeName;
	}

	public function setUtilityBillPaymentTypeName( $strUtilityBillPaymentTypeName ) {
		$this->m_strUtilityBillPaymentTypeName = $strUtilityBillPaymentTypeName;
	}

	public function setUtilityBillReceiptTypeId( $intUtilityBillReceiptTypeId ) {
		$this->m_intUtilityBillReceiptTypeId = $intUtilityBillReceiptTypeId;
	}

	public function getUtilityBillReceiptTypeId() {
		return $this->m_intUtilityBillReceiptTypeId;
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->m_intPropertyUtilityTypeId = $intPropertyUtilityTypeId;
	}

	public function setUtilityBillAccountsCount( $intUtilityBillAccountsCount ) {
		$this->m_intUtilityBillAccountsCount = $intUtilityBillAccountsCount;
	}

	public function setApPayeeStatusTypeId( $intApPayeeStatusTypeId ) {
		$this->m_intApPayeeStatusTypeId = $intApPayeeStatusTypeId;
	}

	public function setIsFromInterface( $boolIsFromInterface ) {
		$this->m_boolIsFromInterface = $boolIsFromInterface;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setAccountNumber( $strAccountNumber ) {
		$strAccountNumber = str_replace( ' ', '', $strAccountNumber );
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 240, NULL, true ) );
	}

	public function setPropertyBuildingIds( $arrintPropertyBuildingIds ) {
		$this->m_arrintPropertyBuildingIds = CStrings::strToArrIntDef( $arrintPropertyBuildingIds );
	}

	public function setSummaryAccountNumber( $strSummaryAccountNumber ) {
		$this->m_strSummaryAccountNumber = $strSummaryAccountNumber;
	}

	public function setNewAccountNumber( $strNewAccountNumber ) {
		$this->m_strNewAccountNumber = $strNewAccountNumber;
	}

	public function setApRemittanceId( $intApRemittanceId ) {
		$this->m_intApRemittanceId = CStrings::strToIntDef( $intApRemittanceId, NULL, false );
	}

	public function setUtilityCredentialId( $intUtilityCredentialId ) {
		return $this->m_intUtilityCredentialId = CStrings::strToIntDef( $intUtilityCredentialId, NULL, false );
	}

	public function setDatabaseTransactionType( $strDatabaseTransactionType ) {
		$this->m_strDatabaseTransactionType = $strDatabaseTransactionType;
	}

	public function setUtilityBillAccountDetail( $objUtilityBillAccountDetail ) {
		$this->m_objUtilityBillAccountDetail = $objUtilityBillAccountDetail;
	}

	public function setIsFromFdgAccount( $boolIsFromFdgAccount ) {
		$this->m_boolIsFromFdgAccount = $boolIsFromFdgAccount;
	}

	/**
	 * Validation Methods
	 */

	public function valUtilityProviderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityProviderId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_provider_id', 'Utility provider name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUtilityTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyUtilityTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_utility_type_id', 'Property utility is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityBillReceiptTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityBillReceiptTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_receipt_type_id', 'Receipt type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Account name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAccountNumberEntered() {
		$boolIsValid = true;

		if( false == \valStr( $this->getAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', 'Account number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsDuplicateAccountNumber( $objDatabase ) {

		if( true == $this->getIsTemplate() || CUtilityBillType::SAMPLE == $this->getUtilityBillTypeId() ) {
			return true;
		}

		if( false == valObj( $this->getUtilityBillAccountDetail(), 'CUtilityBillAccountDetail' ) || false == valId( $this->getUtilityBillAccountDetail()->getUtilityProviderId() ) ) {
			return false;
		}

		$arrobjUtilityAccounts = \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchUtilityBillAccountsByAccountNumberPropertyIdUtilityProviderId( $this->getAccountNumber(), $this->getPropertyId(), $this->getUtilityBillAccountDetail()->getUtilityProviderId(), $objDatabase, $this->getId() );

		if( false === $arrobjUtilityAccounts || true == valArr( $arrobjUtilityAccounts ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', 'Account number ' . $this->getAccountNumber() . ' already exists for provider ' . current( $arrobjUtilityAccounts )->getUtilityProviderName() ) );
			return false;
		}

		return true;
	}

	public function valUtilityBillTemplateId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityBillTemplateId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_template', 'Bill template is required.' ) );
		}
		return $boolIsValid;
	}

	public function valRecoveryPercentage() {
		$boolIsValid = true;

		if( true == is_null( $this->getRecoveryPercentage() ) || 0 >= $this->getRecoveryPercentage() || 100 < $this->getRecoveryPercentage() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recovery_percentage', 'Recovery percentage is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDependencies( $objDatabase ) {

		$boolIsValid = true;

		if( false == is_null( \Psi\Eos\Utilities\CUtilityBills::createService()->fetchUtilityBillsByUtilityBillAccountId( $this->getId(), $objDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Failed to delete the utility bill account, it is being associated with utility bill.' ) );
		}
		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedOn( 'NOW()' );
		$this->setDeletedBy( $intCurrentUserId );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function valApPayeeId( $objUtilitiesDatabase ) {

		$boolIsValid = true;

		if( false == is_null( $this->getApPayeeStatusTypeId() ) && $this->getApPayeeStatusTypeId() != CApPayeeStatusType::ACTIVE ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Select active Ap Payee.' ) );
		} elseif( false == is_null( $this->getApPayeeStatusTypeId() ) && false == is_null( $this->getAccountNumber() ) && false == is_null( \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchUtilityBillAccountsByApPayeeIdByCidByAccountIdByPropertyId( $this->getApPayeeId(), $this->getCid(), $this->getAccountNumber(), $this->getPropertyId(), $objUtilitiesDatabase, $this->getId() ) ) ) {
			$boolIsValid = false;
			$strErrorMessage = 'Vendor is already associated with account - ' . $this->getAccountNumber() . '.Please choose other.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valAllocationCount() {
		$boolIsValid = true;

		if( true == is_null( $this->getAllocationCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_count', 'Enter the number of allocations.' ) );
		} elseif( false == is_null( $this->getAllocationCount() ) && true == preg_match( '/^[a-zA-Z]*$/', $this->getAllocationCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_count', 'Invalid number of allocations.' ) );
		} elseif( false == is_null( $this->getAllocationCount() ) && 0 >= $this->getAllocationCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'allocation_count', 'Minimum number of allocations should be one.' ) );
		}

		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;

		if( true == $this->getRequireNoteAction() && true == is_null( $this->getNotes() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note_required', 'Note is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSummaryUtilityBillAccountId( $objUtilitiesDatabase ) {

		$boolIsValid = true;
		$arrintSummaryUtilityBillAccountIds = $this->getTemplateSummaryUtilityBillAccountIds();

		if( true == $this->getIsTemplate() && true == valIntArr( $arrintSummaryUtilityBillAccountIds ) ) {

			$strUpdateCondition = '';
			if( false == is_null( $this->getId() ) ) {
				$strUpdateCondition = ' AND id <> ' . $this->getId();
			}

			$strWhere = 'WHERE
	   					template_summary_utility_bill_account_ids && ARRAY[ ' . sqlIntImplode( $arrintSummaryUtilityBillAccountIds ) . ']
	   					AND property_id = ' . ( int ) $this->getPropertyId() . '
						AND cid = ' . ( int ) $this->getCid() . '
	   					AND is_template IS TRUE
	   					AND deleted_on IS NULL
					 	AND deleted_by IS NULL' . $strUpdateCondition;

			$intCount = ( int ) \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchUtilityBillAccountCount( $strWhere, $objUtilitiesDatabase );
			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'summary_utility_bill_account_id', 'Utility bill account template already exists for a selected summary account number.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valApPayeeLocation() {
		$boolIsValid = true;

		if( true == $this->m_boolIsFromInterface && false == is_null( $this->getApPayeeId() ) && true == is_null( $this->getApPayeeLocationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_required', 'Vendor location is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRequiredBillsInPeriod() {
		$boolIsValid = true;

		if( true == is_null( $this->getRequiredBillsInPeriod() ) || 0 >= $this->getRequiredBillsInPeriod() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_bills_in_period_required', 'Required bills in period must be greater than zero.' ) );
		}

		return $boolIsValid;
	}

	public function valVendor() {

		$boolIsValid = true;

		if( true == is_null( $this->getApPayeeId() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_required', 'Vendor is Required.' ) );
		}
		return $boolIsValid;
	}

	public function valFrequency() {

		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyId() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_required', 'Frequency is Required.' ) );
		}
		return $boolIsValid;
	}

	public function valServiceStreetLine1() {
		$boolIsValid = true;

		if( false == valStr( $this->getServiceStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_street_line1', ' Service Street Line1 is required' ) );
		}

		return $boolIsValid;
	}

	public function validateUbaReceiptTabSettings( $boolIsRU, $boolIsUEM ) {

		$boolIsValid = true;

		if( ( true == $boolIsUEM && CUtilityBillType::VACANT_UNIT != $this->getUtilityBillTypeId() ) || ( true == $boolIsRU && CUtilityBillType::VACANT_UNIT != $this->getUtilityBillTypeId() ) ) {
			$boolIsValid &= $this->valFrequency();
			$boolIsValid &= $this->valRequiredBillsInPeriod();
		}

		if( true == $boolIsRU && CUtilityBillType::VACANT_UNIT != $this->getUtilityBillTypeId() ) {
			$boolIsValid &= $this->valAllocationCount();
		}

		return $boolIsValid;
	}

	public function validateUbaRuUemTabSettings( $boolIsUEM, $boolIsIP, $boolIsCloneAccount, $objUtilitiesDatabase ) {

		$boolIsValid = true;

		if( true == $boolIsUEM || $boolIsIP ) {
			$boolIsValid &= $this->valApPayeeId( $objUtilitiesDatabase );
			$boolIsValid &= $this->valApPayeeLocation();
		}

		$boolIsValid &= $this->valSummaryUtilityBillAccountId( $objUtilitiesDatabase );

		if( ( true == $boolIsUEM || true == $boolIsIP ) && false == $boolIsCloneAccount ) {
			if( true == in_array( $this->getUtilityBillTypeId(), CUtilityBillType::$c_arrintUemValidateBillTypeIds ) ) {
				$boolIsValid &= $this->valVendor();
			}
		}

		return $boolIsValid;
	}

	public function validateUbaInformationTabSettings( $boolIsCloneAccount, $objUtilitiesDatabase ) {

		$boolIsValid = true;

		if( false == $this->getIsTemplate() ) {
			$boolIsValid &= $this->valAccountNumberEntered();

			if( true == valId( $this->getId() ) ) {
				$boolIsValid &= $this->valIsDuplicateAccountNumber( $objUtilitiesDatabase );
			}

			if( true == $boolIsCloneAccount ) {
				$boolIsValid &= $this->valServiceStreetLine1();
			}
		}

		if( false == $boolIsCloneAccount && false == $this->getIsFromFdgAccount() ) {
			$boolIsValid &= $this->valName();
		}
		$boolIsValid &= $this->valNotes();

		return $boolIsValid;
	}

	public function valUtilityBillTypeId() {

		$boolIsValid = true;

		if( true == is_null( $this->getUtilityBillTypeId() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_type_id_required', 'Bill Type is Required.' ) );
		}
		return $boolIsValid;

	}

	/**
	 * SQL Validation Methods
	 */

	public function validate( $strAction, $objUtilitiesDatabase = NULL, $boolIsCloneAccount = false, $boolIsUEM = false, $boolIsRU = false, $boolIsIP = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				$boolIsValid &= $this->validateUbaInformationTabSettings( $boolIsCloneAccount, $objUtilitiesDatabase );
				$boolIsValid &= $this->validateUbaRuUemTabSettings( $boolIsUEM, $boolIsIP, $boolIsCloneAccount, $objUtilitiesDatabase );
				$boolIsValid &= $this->validateUbaReceiptTabSettings( $boolIsRU, $boolIsUEM );
				$boolIsValid &= $this->valUtilityBillTypeId();
				break;

			case self::VALIDATE_UTILITY_BILL_ACCOUNT_CHARGE_TAB:
				$boolIsValid &= $this->valIsDuplicateAccountNumber( $objUtilitiesDatabase );
				break;

			case self::VALIDATE_INSERT_SMART_BILL_ACCOUNT:
				$boolIsValid &= $this->validateUbaInformationTabSettings( $boolIsCloneAccount, $objUtilitiesDatabase );
				$boolIsValid &= $this->validateUbaReceiptTabSettings( $boolIsRU, $boolIsUEM );

				$boolIsValid &= $this->valApPayeeId( $objUtilitiesDatabase );
				$boolIsValid &= $this->valSummaryUtilityBillAccountId( $objUtilitiesDatabase );
				$boolIsValid &= $this->valApPayeeLocation();
				break;

			case self::VALIDATE_UBA_INFORMATION_SETTINGS_TAB:
				$boolIsValid &= $this->validateUbaInformationTabSettings( $boolIsCloneAccount, $objUtilitiesDatabase );
				$boolIsValid &= $this->valUtilityBillTypeId();
				break;

			case self::VALIDATE_UBA_UEM_RU_SETTINGS_TAB:
				$boolIsValid &= $this->validateUbaRuUemTabSettings( $boolIsUEM, $boolIsIP, $boolIsCloneAccount, $objUtilitiesDatabase );
				break;

			case self::VALIDATE_UBA_RECEIPT_SETTINGS_TAB:
				$boolIsValid &= $this->validateUbaReceiptTabSettings( $boolIsRU, $boolIsUEM );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependencies( $objUtilitiesDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Methods
	 */

	public function createUtilityBillAccountTemplateCharge() {

		$objUtilityBillAccountTemplateCharge = new CUtilityBillAccountTemplateCharge();
		$objUtilityBillAccountTemplateCharge->setCid( $this->getCid() );
		$objUtilityBillAccountTemplateCharge->setPropertyId( $this->getPropertyId() );
		$objUtilityBillAccountTemplateCharge->setUtilityBillAccountId( $this->getId() );

		return $objUtilityBillAccountTemplateCharge;
	}

	public function createUtilityBillAccountUnit() {

		$objUtilityBillAccountUnit = new CUtilityBillAccountUnit();
		$objUtilityBillAccountUnit->setCid( $this->getCid() );
		$objUtilityBillAccountUnit->setPropertyId( $this->getPropertyId() );
		$objUtilityBillAccountUnit->setUtilityBillAccountId( $this->getId() );

		return $objUtilityBillAccountUnit;

	}

	public function fetchUtilityBillTemplate( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBillTemplates::createService()->fetchUtilityBillTemplateById( $this->getUtilityBillTemplateId(), $objDatabase );
	}

	public function fetchUtilityBillAccountCommodities( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBillAccountCommodities::createService()->fetchUtilityBillAccountCommoditiesByUtilityBillAccountId( $this->getId(), $objUtilitiesDatabase );
	}

	public function fetchUtilityBillAccountDetail( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBillAccountDetails::createService()->fetchUtilityBillAccountDetailById( $this->getUtilityBillAccountDetailId(), $objUtilitiesDatabase );
	}

	public function fetchUtilityBillTemplateCharges( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBillTemplateCharges::createService()->fetchUtilityBillTemplateChargesByUtilityBillTemplateIdByUtilityBillAccountId( $this->getUtilityBillTemplateId(), $this->getId(), $objDatabase );
	}

	public function fetchUtilityBillAccountTemplateCharges( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBillAccountTemplateCharges::createService()->fetchUtilityBillAccountTemplateChargesByUtilityBillAccountId( $this->getId(), $objDatabase );
	}

	public function fetchProcessedUtilityBillCountInPeriod( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBills::createService()->fetchUtilityBillCountByUtilityBillAccountIdByFrequencyId( $this->getId(), $this->getFrequencyId(), $objUtilitiesDatabase );
	}

	public function fetchUtilityBillAccountUnits( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBillAccountUnits::createService()->fetchUtilityAccountUnitsByCidByPropertyIdByUtilityBillAccountId( $this->getCid(), $this->getPropertyId(), $this->getId(), $objUtilitiesDatabase );
	}

	public static function getNumberOfAllocationsByFrequency( $intFrequency ) {

		switch( $intFrequency ) {

			case CFrequency::MONTHLY:
				$intAllocationCount = self::$c_arrmixAllocationFrequencies[CFrequency::FREQUENCY_NAME_MONTHLY];
				break;

			case CFrequency::BI_MONTHLY;
				$intAllocationCount = self::$c_arrmixAllocationFrequencies[CFrequency::FREQUENCY_NAME_BI_MONTHLY];
				break;

			case CFrequency::QUARTERLY:
				$intAllocationCount = self::$c_arrmixAllocationFrequencies[CFrequency::FREQUENCY_NAME_QUARTERLY];
				break;

			case CFrequency::SEMI_ANNUALLY:
				$intAllocationCount = self::$c_arrmixAllocationFrequencies[CFrequency::FREQUENCY_NAME_SEMI_ANNUALLY];
				break;

			case CFrequency::YEARLY:
				$intAllocationCount = self::$c_arrmixAllocationFrequencies[CFrequency::FREQUENCY_NAME_YEARLY];
				break;

			default:
				$intAllocationCount = self::$c_arrmixAllocationFrequencies[CFrequency::FREQUENCY_NAME_MONTHLY];
				break;
		}

		return $intAllocationCount;
	}

	public function validateIsIncompleteAccount( $objUtilityBillAccountDetail, $objPropertyUtilitySetting ) {

		if( false == valObj( $objUtilityBillAccountDetail, 'CUtilityBillAccountDetail' ) ) {
			return true;
		}

		if( false == valObj( $objPropertyUtilitySetting, 'CPropertyUtilitySetting' ) ) {
			return true;
		}

		$boolIsIncompleteAccount		= false;

		if( false == is_numeric( $objUtilityBillAccountDetail->getUtilityBillReceiptTypeId() ) ||
		    false == is_numeric( $this->getUtilityBillTypeId() ) ||
		    false == is_numeric( $this->getFrequencyId() ) ||
		    ( true == is_numeric( $objUtilityBillAccountDetail->getUtilityBillReceiptTypeId() ) && CUtilityBillReceiptType::WEB_RETRIEVAL == $objUtilityBillAccountDetail->getUtilityBillReceiptTypeId()
		      && ( true == is_null( $objUtilityBillAccountDetail->getUtilityCredentialId() ) ) ) ||
		    ( ( true == $objPropertyUtilitySetting->getIsUemProperty() || true == $objPropertyUtilitySetting->getIsIpProperty() )
		      && ( ( true == in_array( $this->getUtilityBillTypeId(), CUtilityBillType::$c_arrintUemValidateBillTypeIds ) && true == is_null( $this->getApPayeeId() ) ) ||
		           ( false == is_null( $this->getApPayeeId() ) && true == is_null( $this->getApPayeeLocationId() ) ) ) ) ) {

			$boolIsIncompleteAccount = true;
		}

		return $boolIsIncompleteAccount;
	}

	public function createUtilityBillAccountMeter( $strMeterNumber, $intUtilityConsumptionTypeId ) {

		$objUtilityBillAccountMeter = new CUtilityBillAccountMeter();
		$objUtilityBillAccountMeter->setCid( $this->getCid() );
		$objUtilityBillAccountMeter->setPropertyId( $this->getPropertyId() );
		$objUtilityBillAccountMeter->setUtilityBillAccountId( $this->getId() );
		$objUtilityBillAccountMeter->setMeterNumber( $strMeterNumber );
		$objUtilityBillAccountMeter->setUtilityConsumptionTypeId( $intUtilityConsumptionTypeId );

		return $objUtilityBillAccountMeter;

	}

	public function createUtilityBillAccountMeterCommodity() {
		$objUtilityBillAccountMeterUtilityType = new CUtilityBillAccountMeterCommodity();
		$objUtilityBillAccountMeterUtilityType->setPropertyId( $this->getPropertyId() );
		$objUtilityBillAccountMeterUtilityType->setCid( $this->getCid() );
		return $objUtilityBillAccountMeterUtilityType;
	}

	public function createUtilityBillAccountMeterTemplateCharge() {

		$objUtilityBillAccountMeterTemplateCharge = new CUtilityBillAccountMeterTemplateCharge();
		$objUtilityBillAccountMeterTemplateCharge->setCid( $this->getCid() );
		$objUtilityBillAccountMeterTemplateCharge->setPropertyId( $this->getPropertyId() );

		return $objUtilityBillAccountMeterTemplateCharge;
	}

	public function checkHasUemService() {

		$boolHasUemService = true;

		$arrmixPropertyCommodities = ( array ) \Psi\Eos\Utilities\CPropertyCommodities::createService()->fetchCustomPropertyCommoditiesByUtilityBillAccountIdByPropertyId( $this->getId(), $this->getPropertyId(), $this->getDatabase() );

		foreach( $arrmixPropertyCommodities as $arrmixPropertyCommodity ) {
			if( false == CStrings::strToBool( $arrmixPropertyCommodity['has_uem_service'] ) ) {
				$boolHasUemService = false;
				break;
			}
		}

		return $boolHasUemService;
	}

	public function validateActivateAccountSettings( $objClientDatabase ) {

		$boolValidate = true;

		if( true == is_null( $this->getApPayeeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_required', 'Vendor is required.' ) );
			$boolValidate &= false;
		} else {
			$objApPayee = \Psi\Eos\Entrata\CApPayees::createService()->fetchActiveApPayeeByIdByCid( $this->getApPayeeId(), $this->getCid(), $objClientDatabase );

			if( false == valObj( $objApPayee, 'CApPayee' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_account', 'The Vendor set on account no longer exists in Entrata.' ) );
				$boolValidate &= false;
			}
		}

		if( true == is_null( $this->getApPayeeLocationId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_required', 'Vendor location is required.' ) );
			$boolValidate &= false;
		} else {
			$objApPayeeLocation = \Psi\Eos\Entrata\CApPayeeLocations::createService()->fetchApPayeeLocationDetailsByIdByApPayeeIdByCid( $this->getApPayeeId(), $this->getApPayeeLocationId(), $this->getCid(), $objClientDatabase );

			if( false == valObj( $objApPayeeLocation, 'CApPayeeLocation' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_account', 'The Vendor location set on account no longer exists in Entrata.' ) );
				$boolValidate &= false;
			}
		}

		return $boolValidate;

	}

}
?>