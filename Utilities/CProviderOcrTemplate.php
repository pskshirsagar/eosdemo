<?php

class CProviderOcrTemplate extends CBaseProviderOcrTemplate {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityProviderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrFieldId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLabel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLabelVertices() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValueVertices() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValuePosition() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValueParts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriority() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>