<?php

class CUtilityCompanyPreference extends CBaseUtilityCompanyPreference {

	const ACTIVATE_THIRD_PARTY_DATA_ENTRY = 'ACTIVATE_THIRD_PARTY_DATA_ENTRY';
	const ACTIVATE_THIRD_PARTY_DATA_ENTRY_IMPLEMENTATION = 'ACTIVATE_THIRD_PARTY_DATA_ENTRY_IMPLEMENTATION';

	public static $c_arrstrActivateThirdPartyDataEntryPreferences = [
		self::ACTIVATE_THIRD_PARTY_DATA_ENTRY,
		self::ACTIVATE_THIRD_PARTY_DATA_ENTRY_IMPLEMENTATION
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>