<?php

class CUtilityTransmissionDetail extends CBaseUtilityTransmissionDetail {

    const VALIDATE_INSERT_FOR_BATCH_PROCESS = 'insert_for_batch_process';
	protected $m_intCountFactor;
    protected $m_intConsumptionAmount;
    protected $m_intOccupentsCount;
    protected $m_intEndStartDateDiff;
    protected $m_intMeterTransmissionBatchId;
    protected $m_intEndUtilityTransmissionId;
    protected $m_intPropertyUtilityTypeId;
    protected $m_intMeterErrorTypeId;

    protected $m_boolIsFromUtilityBillCharges;
    protected $m_boolEndUtilityTransmissionDetailIsAdjusted;
    protected $m_boolIsMoveInInConsumptionPeriod;
    protected $m_boolIsMoveOutInConsumptionPeriod;
    protected $m_boolIsStartEstimated;
    protected $m_boolIsEndEstimated;
    protected $m_boolIsFromLastBatch;

	protected $m_fltTotalEstimatedConsumption;
	protected $m_intConsecutiveEstimationCount;
	protected $m_boolIsAdjustedUp;
	protected $m_boolIsEstimated;
	protected $m_boolIsCorrected;
	protected $m_boolIsTrueUp;
	protected $m_boolIsLessThanTrueUpPercent;

    protected $m_strMoveInDate;
    protected $m_strOriginalLeaseMoveInDate;

	public function __construct() {
        parent::__construct();

        $this->m_intMeterTransmissionBatchId = NULL;

        $this->m_boolIsFromUtilityBillCharges     = false;
		$this->m_boolIsMoveInInConsumptionPeriod  = false;
		$this->m_boolIsMoveOutInConsumptionPeriod = false;
		$this->m_boolIsStartEstimated 			  = false;
		$this->m_boolIsEndEstimated 			  = false;
		$this->m_boolIsFromLastBatch 			  = false;

    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['count_factor'] ) ) {
    		$this->setCountFactor( $arrmixValues['count_factor'] );
	    }
    	if( isset( $arrmixValues['property_utility_type_id'] ) ) {
    		$this->setPropertyUtilityTypeId( $arrmixValues['property_utility_type_id'] );
	    }
    	if( true == isset( $arrmixValues['meter_error_type_id'] ) ) {
    		$this->setMeterErrorTypeId( $arrmixValues['meter_error_type_id'] );
	    }

    }

    /*
     * Validate Functions
     */

    public function valMeterNumber() {
        $boolIsValid = true;

    	if( ( false == is_null( $this->getMeterStartRead() ) || false == is_null( $this->getMeterEndRead() ) ) && true == is_null( $this->getMeterNumber() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_number', 'Meter number is required.' ) );
        }

        return $boolIsValid;
    }

    public function valEndDatetime() {
        $boolIsValid = true;
        if( false == is_null( $this->getEndDatetime() ) && false == is_null( $this->getStartDatetime() ) ) {
        	if( strtotime( $this->getEndDatetime() ) < strtotime( $this->getStartDatetime() ) ) {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', 'End date must be greater than Start Date.' ) );
        		$boolIsValid = false;
        	}
        }

        return $boolIsValid;
    }

    public function valMeterStartRead() {
        $boolIsValid = true;

        if( true == is_null( $this->getMeterStartRead() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_start_read', 'Meter start read required.' ) );
        }

        return $boolIsValid;
    }

	public function valMeterEndRead() {
        $boolIsValid = true;

        if( true == is_null( $this->getMeterEndRead() ) ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_end_read', 'Meter end read required.' ) );
        }

        return $boolIsValid;
    }

    public function valMeterStartEndRead() {
        $boolIsValid = true;

        if( false == is_null( $this->getMeterStartRead() ) && false == is_null( $this->getMeterEndRead() ) && ( $this->getMeterEndRead() < $this->getMeterStartRead() ) ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_end_read', 'Meter Start Read must be less than Meter End Read.' ) );
        }

        return $boolIsValid;
    }

    public function valStatus() {
	    return true;
    }

	public function valDetail() {
        $boolIsValid = true;

        if( true == is_null( $this->getMeterNumber() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_number', 'Transmission details are required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valMeterNumber();
            	$boolIsValid &= $this->valMeterStartEndRead();
            	$boolIsValid &= $this->valEndDatetime();
            	break;

            case self::VALIDATE_INSERT_FOR_BATCH_PROCESS:
            	$boolIsValid &= $this->valMeterStartRead();
            	$boolIsValid &= $this->valEndDatetime();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid = true;
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /*
     * Set Functions
     */

    public function setCountFactor( $intCountFactor ) {
    	return $this->m_intCountFactor = $intCountFactor;
    }

    public function setConsumptionAmount( $intConsumptionAmount ) {
    	$this->m_intConsumptionAmount = $intConsumptionAmount;
    }

    public function setOccupantsCount( $intOccupenstCount ) {
    	$this->m_intOccupentsCount = $intOccupenstCount;
    }

    public function setEndStartDateDiff( $intEndStartDateDiff ) {
    	$this->m_intEndStartDateDiff = $intEndStartDateDiff;
    }

    public function setIsFromUtilityBillCharges( $boolIsFromUtilityBillCharges ) {
    	$this->m_boolIsFromUtilityBillCharges = $boolIsFromUtilityBillCharges;
    }

    public function setEndUtilityTransmissionId( $intEndUtilityTransmissionId ) {
    	$this->m_intEndUtilityTransmissionId = $intEndUtilityTransmissionId;
    }

    public function setEndUtilityTransmissionDetailIsAdjusted( $boolIsAdjusted ) {
    	$this->m_boolEndUtilityTransmissionDetailIsAdjusted = $boolIsAdjusted;
    }

    public function setMeterTransmissionBatchId( $intMeterTransmissionBatchId ) {
    	$this->m_intMeterTransmissionBatchId = $intMeterTransmissionBatchId;
    }

    public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
    	$this->m_intPropertyUtilityTypeId = $intPropertyUtilityTypeId;
    }

    public function setMoveInDate( $strMoveInDate ) {
    	return $this->m_strMoveInDate = $strMoveInDate;
    }

    public function setIsMoveInInConsumptionPeriod( $boolIsMoveInInConsumptionPeriod ) {
    	$this->m_boolIsMoveInInConsumptionPeriod = $boolIsMoveInInConsumptionPeriod;
    }

    public function setIsMoveOutInConsumptionPeriod( $boolIsMoveOutInConsumptionPeriod ) {
    	$this->m_boolIsMoveOutInConsumptionPeriod = $boolIsMoveOutInConsumptionPeriod;
    }

    public function setIsStartEstimated( $boolIsStartEstimated ) {
    	$this->m_boolIsStartEstimated = $boolIsStartEstimated;
    }

    public function setIsEndEstimated( $boolIsEndEstimated ) {
    	$this->m_boolIsEndEstimated = $boolIsEndEstimated;
    }

    public function setIsFromLastBatch( $boolIsFromLastBatch ) {
    	$this->m_boolIsFromLastBatch = $boolIsFromLastBatch;
    }

    public function setOriginalLeaseMoveInDate( $strOriginalLeaseMoveInDate ) {
    	$this->m_strOriginalLeaseMoveInDate = $strOriginalLeaseMoveInDate;
    }

    public function setMeterErrorTypeId( $intMeterErrorTypeId ) {
    	$this->m_intMeterErrorTypeId = $intMeterErrorTypeId;
    }

	public function setTotalEstimatedConsumption( $fltTotalEstimatedConsumption ) {
		$this->set( 'm_fltTotalEstimatedConsumption', CStrings::strToFloatDef( $fltTotalEstimatedConsumption, NULL, false, 2 ) );
	}

	public function setConsecutiveEstimationCount( $intConsecutiveEstimationCount ) {
		$this->set( 'm_intConsecutiveEstimationCount', CStrings::strToIntDef( $intConsecutiveEstimationCount ) );
	}

	public function setIsAdjustedUp( $boolIsAdjustedUp ) {
		$this->set( 'm_boolIsAdjustedUp', CStrings::strToBool( $boolIsAdjustedUp ) );
	}

	public function setIsEstimated( $boolIsEstimated ) {
		$this->set( 'm_boolIsEstimated', CStrings::strToBool( $boolIsEstimated ) );
	}

	public function setIsCorrected( $boolIsCorrected ) {
		$this->set( 'm_boolIsCorrected', CStrings::strToBool( $boolIsCorrected ) );
	}

	public function setIsTrueUp( $boolIsTrueUp ) {
		$this->set( 'm_boolIsTrueUp', CStrings::strToBool( $boolIsTrueUp ) );
	}

	public function setIsLessThanTrueUpPercent( $boolIsLessThanTrueUpPercent ) {
		$this->set( 'm_boolIsLessThanTrueUpPercent', CStrings::strToBool( $boolIsLessThanTrueUpPercent ) );
	}

	/*
	 * Get Functions
	 */

    public function getCountFactor() {
    	return $this->m_intCountFactor;
    }

	public function getConsumptionAmount() {
    	return $this->m_intConsumptionAmount;
    }

	public function getOccupantsCount() {
    	return $this->m_intOccupentsCount;
    }

	public function getEndStartDateDiff() {
    	return $this->m_intEndStartDateDiff;
    }

	public function getIsFromUtilityBillCharges() {
    	return $this->m_boolIsFromUtilityBillCharges;
    }

	public function getEndUtilityTransmissionId() {
    	return $this->m_intEndUtilityTransmissionId;
    }

	public function getEndUtilityTransmissionDetailIsAdjusted() {
    	return $this->m_boolEndUtilityTransmissionDetailIsAdjusted;
    }

	public function getMeterTransmissionBatchId() {
    	return $this->m_intMeterTransmissionBatchId;
    }

	public function getPropertyUtilityTypeId() {
    	return $this->m_intPropertyUtilityTypeId;
    }

    public function getMoveInDate() {
    	return $this->m_strMoveInDate;
    }

    public function getIsMoveInInConsumptionPeriod() {
    	return $this->m_boolIsMoveInInConsumptionPeriod;
    }

    public function getIsMoveOutInConsumptionPeriod() {
    	return $this->m_boolIsMoveOutInConsumptionPeriod;
    }

    public function getIsStartEstimated() {
    	return $this->m_boolIsStartEstimated;
    }

    public function getIsEndEstimated() {
    	return $this->m_boolIsEndEstimated;
    }

    public function getIsFromLastBatch() {
    	return $this->m_boolIsFromLastBatch;
    }

    public function getOriginalLeaseMoveInDate() {
    	return $this->m_strOriginalLeaseMoveInDate;
    }

    public function getMeterErrorTypeId() {
    	return $this->m_intMeterErrorTypeId;
    }

	public function getTotalEstimatedConsumption() {
		return $this->m_fltTotalEstimatedConsumption;
	}

	public function getConsecutiveEstimationCount() {
		return $this->m_intConsecutiveEstimationCount;
	}

	public function getIsAdjustedUp() {
		return $this->m_boolIsAdjustedUp;
	}

	public function getIsEstimated() {
		return $this->m_boolIsEstimated;
	}

	public function getIsCorrected() {
		return $this->m_boolIsCorrected;
	}

	public function getIsTrueUp() {
		return $this->m_boolIsTrueUp;
	}

	public function getIsLessThanTrueUpPercent() {
		return $this->m_boolIsLessThanTrueUpPercent;
	}

}
?>