<?php

class CUtilitySettingLog extends CBaseUtilitySettingLog {

	/**
	 * Other Functions
	 */

	public function createDeleteLog( $strTableName, $intReferenceId, $intCurrentUserId, $objDatabase ) {

		$arrmixOriginalValues 		= fetchData( 'SELECT * FROM ' . $strTableName . ' WHERE id = ' . ( int ) $intReferenceId, $objDatabase );

		if( false == valArr( $arrmixOriginalValues ) ) {
			return false;
		}

		foreach( $arrmixOriginalValues[0] as $strKey => $strOriginalValue ) {
			$arrstrOriginalValues[$strKey] = $strOriginalValue;
		}

		$this->setTableName( $strTableName );
		$this->setReferenceId( $intReferenceId );
		$this->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
		$this->setCompanyUserId( $intCurrentUserId );

		return true;
	}

	public function createInsertLog( $strTableName, $objReference, $intCurrentUserId, $objDatabase ) {

		$arrmixColumnNames = fetchData( 'select column_name from information_schema.columns where table_name = \'' . $strTableName . '\' ', $objDatabase );

		foreach( $arrmixColumnNames as $arrmixColumnName ) {

			$strFunctionName 	= 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $arrmixColumnName['column_name'] ) ) );
			$arrstrOriginalValues[$arrmixColumnName['column_name']] = $objReference->$strFunctionName();
		}

		$this->setTableName( $strTableName );
		$this->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
		$this->setCompanyUserId( $intCurrentUserId );

		return true;

	}

	public function createUpdateLog( $strTableName, $objReference, $intCurrentUserId, $objDatabase ) {

		$boolChangeFlag = false;
		$arrmixOriginalValue = fetchData( 'SELECT * FROM ' . $strTableName . ' WHERE id = ' . ( int ) $objReference->getId(), $objDatabase );

		if( false == valArr( $arrmixOriginalValue ) ) {
			return false;
		}

		foreach( $arrmixOriginalValue[0] as $strKey => $strMixOriginalValue ) {

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			if( CStrings::reverseSqlFormat( $objReference->$strSqlFunctionName() ) != $strMixOriginalValue ) {
				$arrstrOriginalValueChanges[$strKey] 	= $objReference->$strFunctionName();
				$arrstrOriginalValues[$strKey] 			= $strMixOriginalValue;
				$boolChangeFlag 						= true;
			}
		}

		if( false == $boolChangeFlag ) {
			return false;
		}

		$this->setTableName( $strTableName );
		$this->setReferenceId( $objReference->getId() );
		$this->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
		$this->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValueChanges ) ) );
		$this->setCompanyUserId( $intCurrentUserId );

		return $boolChangeFlag;

	}

}
?>