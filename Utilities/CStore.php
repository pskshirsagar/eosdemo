<?php

class CStore extends CBaseStore {

	const CORPORATE = 'Corporate';

	protected $m_intRemittanceId;

	protected $m_strRemittanceName;
	protected $m_strRemittanceCity;
	protected $m_strRemittanceStateCode;
	protected $m_strWorkerEmailAddress;

	protected $m_strRemittanceStreetLine1;
	protected $m_strRemittanceStreetLine2;
	protected $m_strRemittanceStreetLine3;
	protected $m_strRemittancePostalCode;

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valDefaultVendorEntityId() {
		$boolIsValid = true;

		if( false == valId( $this->getDefaultVendorEntityId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_entity_id', __( 'Entity is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valLocationName( $strAction, $objUtilitiesDatabase ) {

		$boolIsValid = true;

		if( false == valStr( $this->getLocationName() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'location_name', __( 'Location name is required.' ) ) );
		} elseif( VALIDATE_INSERT == $strAction ) {

			$intStoreCount = \Psi\Eos\Utilities\CStores::createService()->fetchStoreCountByVendorIdByLocationName( $this->getVendorId(), $this->getLocationName(), $objUtilitiesDatabase );

			if( 0 < $intStoreCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Location name already exists.' ) ) );
			}
		} elseif( VALIDATE_UPDATE == $strAction ) {

			$intDuplicateStoreCount = \Psi\Eos\Utilities\CStores::createService()->fetchDuplicateStoreCountByIdByVendorIdByLocationName( $this->getId(), $this->getVendorId(), $this->getLocationName(), $objUtilitiesDatabase );

			if( 0 < $intDuplicateStoreCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Location name already exists.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valLocationDatetime() {
		return true;
	}

	public function valPhoneNumber() {

		if( true == is_null( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is required.' ) ) );
		} elseif( ( CWorker::MIN_PHONE_NUMBER_SIZE > strlen( $this->getPhoneNumber() ) || CWorker::MAX_PHONE_NUMBER_SIZE < strlen( $this->getPhoneNumber() ) ) || false == CValidation::validateFullPhoneNumber( $this->getPhoneNumber(), false ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Please enter valid phone number.' ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valStreetLine1() {
		return true;
	}

	public function valStreetLine2() {
		return true;
	}

	public function valStreetLine3() {
		return true;
	}

	public function valCity() {

		$boolIsValid = true;

		if( true == is_numeric( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City can not be numeric.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		return true;
	}

	public function valProvince() {
		return true;
	}

	public function valPostalCode() {
		$boolIsValid = true;

		if( true == valStr( $this->getPostalCode() ) && false == CValidation::validatePostalCode( $this->getPostalCode(), CCountry::CODE_USA ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Valid postal code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCountryCode() {
		return true;
	}

	public function valNotes() {
		return true;
	}

	public function valIsPrimary() {
		return true;
	}

	public function valDisabledBy() {
		return true;
	}

	public function valDisabledOn() {
		return true;
	}

	public function validate( $strAction, $objUtilitiesDatabase = NULL, $boolIsFromSetup = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valLocationName( $strAction, $objUtilitiesDatabase );
				$boolIsValid &= $this->valDefaultVendorEntityId();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valPostalCode();
				if( false == $boolIsFromSetup ) {
					$boolIsValid &= $this->valPhoneNumber();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['email_address'] ) && $boolDirectSet ) {
			$this->m_strWorkerEmailAddress = trim( stripcslashes( $arrmixValues['email_address'] ) );
		} elseif( isset( $arrmixValues['email_address'] ) ) {
			$this->setWorkerEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['email_address'] ) : $arrmixValues['email_address'] );
		}

		if( isset( $arrmixValues['remittance_name'] ) && $boolDirectSet ) {
			$this->m_strRemittanceName = trim( stripcslashes( $arrmixValues['remittance_name'] ) );
		} elseif( isset( $arrmixValues['remittance_name'] ) ) {
			$this->setRemittanceName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['remittance_name'] ) : $arrmixValues['remittance_name'] );
		}

		if( isset( $arrmixValues['remittance_id'] ) && $boolDirectSet ) {
			$this->m_intRemittanceId = trim( stripcslashes( $arrmixValues['remittance_id'] ) );
		} elseif( isset( $arrmixValues['remittance_id'] ) ) {
			$this->setRemittanceId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['remittance_id'] ) : $arrmixValues['remittance_id'] );
		}

		if( isset( $arrmixValues['remittance_street_line1'] ) && $boolDirectSet ) {
			$this->m_strRemittanceStreetLine1 = trim( stripcslashes( $arrmixValues['remittance_street_line1'] ) );
		} elseif( isset( $arrmixValues['remittance_street_line1'] ) ) {
			$this->setRemittanceStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['remittance_street_line1'] ) : $arrmixValues['remittance_street_line1'] );
		}

		if( isset( $arrmixValues['remittance_street_line2'] ) && $boolDirectSet ) {
			$this->m_strRemittanceStreetLine2 = trim( stripcslashes( $arrmixValues['remittance_street_line2'] ) );
		} elseif( isset( $arrmixValues['remittance_street_line2'] ) ) {
			$this->setRemittanceStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['remittance_street_line2'] ) : $arrmixValues['remittance_street_line2'] );
		}

		if( isset( $arrmixValues['remittance_street_line3'] ) && $boolDirectSet ) {
			$this->m_strRemittanceStreetLine3 = trim( stripcslashes( $arrmixValues['remittance_street_line3'] ) );
		} elseif( isset( $arrmixValues['remittance_street_line3'] ) ) {
			$this->setRemittanceStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['remittance_street_line3'] ) : $arrmixValues['remittance_street_line3'] );
		}

		if( isset( $arrmixValues['remittance_city'] ) && $boolDirectSet ) {
			$this->m_strRemittanceCity = trim( stripcslashes( $arrmixValues['remittance_city'] ) );
		} elseif( isset( $arrmixValues['remittance_city'] ) ) {
			$this->setRemittanceCity( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['remittance_city'] ) : $arrmixValues['remittance_city'] );
		}

		if( isset( $arrmixValues['remittance_state_code'] ) && $boolDirectSet ) {
			$this->m_strRemittanceStateCode = trim( stripcslashes( $arrmixValues['remittance_state_code'] ) );
		} elseif( isset( $arrmixValues['remittance_state_code'] ) ) {
			$this->setRemittanceStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['remittance_state_code'] ) : $arrmixValues['remittance_state_code'] );
		}

		if( isset( $arrmixValues['remittance_postal_code'] ) && $boolDirectSet ) {
			$this->m_strRemittancePostalCode = trim( stripcslashes( $arrmixValues['remittance_postal_code'] ) );
		} elseif( isset( $arrmixValues['remittance_postal_code'] ) ) {
			$this->setRemittancePostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['remittance_postal_code'] ) : $arrmixValues['remittance_postal_code'] );
		}
	}

	public function setWorkerEmailAddress( $strWorkerEmailAddress ) {
		$this->m_strWorkerEmailAddress = CStrings::strTrimDef( $strWorkerEmailAddress, 240, NULL, true );
	}

	public function getWorkerEmailAddress() {
		return $this->m_strWorkerEmailAddress;
	}

	public function setRemittanceName( $strRemittanceName ) {
		$this->m_strRemittanceName = CStrings::strTrimDef( $strRemittanceName, 200, NULL, true );
	}

	public function getRemittanceName() {
		return $this->m_strRemittanceName;
	}

	public function setRemittanceId( $strRemittanceId ) {
		$this->m_intRemittanceId = $strRemittanceId;
	}

	public function getRemittanceId() {
		return $this->m_intRemittanceId;
	}

	public function setRemittanceStreetLine1( $strRemittanceStreetLine1 ) {
		$this->m_strRemittanceStreetLine1 = CStrings::strTrimDef( $strRemittanceStreetLine1, 200, NULL, true );
	}

	public function getRemittanceStreetLine1() {
		return $this->m_strRemittanceStreetLine1;
	}

	public function setRemittanceStreetLine2( $strRemittanceStreetLine2 ) {
		$this->m_strRemittanceStreetLine2 = CStrings::strTrimDef( $strRemittanceStreetLine2, 200, NULL, true );
	}

	public function getRemittanceStreetLine2() {
		return $this->m_strRemittanceStreetLine2;
	}

	public function setRemittanceStreetLine3( $strRemittanceStreetLine3 ) {
		$this->m_strRemittanceStreetLine3 = CStrings::strTrimDef( $strRemittanceStreetLine3, 200, NULL, true );
	}

	public function getRemittanceStreetLine3() {
		return $this->m_strRemittanceStreetLine3;
	}

	public function setRemittanceCity( $strRemittanceCity ) {
		$this->m_strRemittanceCity = CStrings::strTrimDef( $strRemittanceCity, 50, NULL, true );
	}

	public function getRemittanceCity() {
		return $this->m_strRemittanceCity;
	}

	public function setRemittanceStateCode( $strRemittanceStateCode ) {
		$this->m_strRemittanceStateCode = CStrings::strTrimDef( $strRemittanceStateCode, 2, NULL, true );
	}

	public function getRemittanceStateCode() {
		return $this->m_strRemittanceStateCode;
	}

	public function setRemittancePostalCode( $strRemittancePostalCode ) {
		$this->m_strRemittancePostalCode = CStrings::strTrimDef( $strRemittancePostalCode, 20, NULL, true );
	}

	public function getRemittancePostalCode() {
		return $this->m_strRemittancePostalCode;
	}

	public function delete( $intUserId, $objDatabase, $boolIsSoftDelete = false, $boolReturnSqlOnly = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDisabledBy( $intUserId );
			$this->setDisabledOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objDatabase );
	}

	public function getAddress() {

		$strAddress = '';

		$strAddress .= $this->getStreetLine1();

		if( true == valStr( $this->getStreetLine1() && true == $this->getStreetLine2() ) ) {
			$strAddress .= ',' . $this->getStreetLine2();
		}

		if( true == valStr( $this->getStreetLine3() ) ) {
			$strAddress .= ',' . $this->getStreetLine3();
		}

		if( true == valStr( $strAddress ) ) {
			$strAddress .= '<br>';
		}

		if( true == valStr( $this->getCity() ) ) {
			$strAddress .= $this->getCity();
		}

		if( true == valStr( $this->getCity() ) && true == valStr( $this->getStateCode() ) ) {
			$strAddress .= ',';
		}

		if( true == valStr( $this->getStateCode() ) ) {
			$strAddress .= $this->getStateCode();
		}

		if( true == valStr( $this->getPostalCode() ) ) {
			$strAddress .= ' ' . $this->getPostalCode();
		}

		return $strAddress;
	}

}
?>