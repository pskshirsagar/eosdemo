<?php

class CUtilityPropertySetting extends CBaseUtilityPropertySetting {

	protected $m_boolIsInImplementation;

	public function setIsInImplementation( $boolIsInImplementation ) {
		$this->m_boolIsInImplementation = $boolIsInImplementation;
	}

	public function valUtilityRolloutTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityRolloutTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_rollout_type_id', '! Utility Rollout Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valWeightedValue() {
		$boolIsValid = true;

		if( false == is_null( $this->getWeightedValue() ) ) {
			if( true == 5 < strlen( floor( $this->getWeightedValue() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'weighted_value', 'Weighted value should not be greater than five digits before decimal.' ) );
			} elseif( true == preg_match( '/^\d+\.\d+$/', $this->getWeightedValue() ) && true == 2 < strcspn( \Psi\CStringService::singleton()->strrev( $this->getWeightedValue() ), '.' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'weighted_value', 'Weighted value should have maximum two decimal places.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsVcrOnly( $objUtilitiesDatabase ) {
		$boolIsValid = true;

		$objCurrentActiveBatch = \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchActiveUtilityBatchByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );

		if( true == valObj( $objCurrentActiveBatch, 'CUtilityBatch' ) && true == $this->getIsVcrOnly() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_vcr_only', 'Property has active batch.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objUtilitiesDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valWeightedValue();
				$boolIsValid &= $this->valIsVcrOnly( $objUtilitiesDatabase );
				if( false == $this->m_boolIsInImplementation ) {
					$boolIsValid &= $this->valUtilityRolloutTypeId();
				}
				break;

			case VALIDATE_INSERT:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		// utility Property setting
		$arrstrTempUtilityPropertySettingOriginalValues = $arrstrUtilityPropertySettingOriginalValueChanges = $arrstrUtilityPropertySettingOriginalValues = [];
		$boolUtilityPropertySettingChangeFlag = false;

		$arrmixUtilityPropertySettingOriginalValues = fetchData( 'SELECT * FROM utility_property_settings WHERE property_utility_setting_id = ' . ( int ) $this->getPropertyUtilitySettingId(), $objDatabase );

		$arrstrTempUtilityPropertySettingOriginalValues = $arrmixUtilityPropertySettingOriginalValues[0];

		foreach( $arrstrTempUtilityPropertySettingOriginalValues as $strKey => $mixUtilityPropertySettingOriginalValue ) {

			if( 'utility_bill_per_unit_fee' == $strKey ) {
				continue;
			}

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strFunctionName    = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			if( ( false == CStrings::strToBool( $mixUtilityPropertySettingOriginalValue ) && '' == $this->$strFunctionName() ) || ( true == CStrings::strToBool( $mixUtilityPropertySettingOriginalValue ) && '1' == $this->$strFunctionName() ) ) {
				continue;
			}

			if( CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $mixUtilityPropertySettingOriginalValue ) {
				$arrstrUtilityPropertySettingOriginalValueChanges[$strKey] = $this->$strFunctionName();
				$arrstrUtilityPropertySettingOriginalValues[$strKey]       = $mixUtilityPropertySettingOriginalValue;
				$boolUtilityPropertySettingChangeFlag                      = true;
			}
		}

		$boolUtilityPropertySettingUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUtilityPropertySettingUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolUtilityPropertySettingChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempUtilityPropertySettingOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempUtilityPropertySettingOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'utility_property_settings' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrUtilityPropertySettingOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrUtilityPropertySettingOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}

		}

		if( true == $boolUtilityPropertySettingUpdateStatus ) {
			return true;
		} else {
			return false;
		}

	}

	public function updatePropertyWeightedValue( $objDatabase ) {

		$arrmixWeightedValue = current( \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyWeightedValueFromPropertyUtilitySettingsByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objDatabase ) );
		$this->setWeightedValue( $arrmixWeightedValue['sum'] );
	}

}
?>