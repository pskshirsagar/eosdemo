<?php

class CBillingServicePeriodType extends CBaseBillingServicePeriodType {

	const PROVIDER_SERVICE_PERIOD			= 1;
	const FIRST_TO_END_OF_MONTH				= 2;
	const FIRST_TO_FIRST					= 3;
	const MANUAL							= 4;
	const MANUAL_PLUS_ONE					= 5;
	const DIRECT_METERED					= 6;
	const MATCH								= 7;
	const CUSTOM							= 8;

	/**
	 * Other Functions
	 * */

	public static function loadBillingServicePeriodTypes() {
		$arrmixBillingServicePeriodTypes = [];

		$arrmixBillingServicePeriodTypes[PROVIDER_SERVICE_PERIOD]	 = 'Provider Service Period';
		$arrmixBillingServicePeriodTypes[FIRST_TO_END_OF_MONTH]		 = '1st To End of Month';
		$arrmixBillingServicePeriodTypes[FIRST_TO_FIRST]			 = '1st To 1st';
		$arrmixBillingServicePeriodTypes[MANUAL]					 = 'Manual';
		$arrmixBillingServicePeriodTypes[MANUAL_PLUS_ONE]			 = 'Manual +1';
		$arrmixBillingServicePeriodTypes[DIRECT_METERED]			 = 'Direct Metered';

		return $arrmixBillingServicePeriodTypes;
	}

	public static function populateTemplateConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['PROVIDER_SERVICE_PERIOD']	= self::PROVIDER_SERVICE_PERIOD;
		$arrmixTemplateParameters['FIRST_TO_END_OF_MONTH']		= self::FIRST_TO_END_OF_MONTH;
		$arrmixTemplateParameters['FIRST_TO_FIRST']				= self::FIRST_TO_FIRST;
		$arrmixTemplateParameters['MANUAL']						= self::MANUAL;
		$arrmixTemplateParameters['MANUAL_PLUS_ONE']			= self::MANUAL_PLUS_ONE;
		$arrmixTemplateParameters['DIRECT_METERED']				= self::DIRECT_METERED;
		$arrmixTemplateParameters['MATCH']						= self::MATCH;
		$arrmixTemplateParameters['CUSTOM']						= self::CUSTOM;

		return $arrmixTemplateParameters;
	}

	public static $c_arrintIncrementingServicePeriodTypes = [
		self::FIRST_TO_END_OF_MONTH,
		self::FIRST_TO_FIRST,
		self::CUSTOM
	];

}
?>