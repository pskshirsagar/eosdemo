<?php

class CUtilityDocumentType extends CBaseUtilityDocumentType {

	const DOCUMENT_TYPE_BILL              = 1;
	const DOCUMENT_TYPE_NOTICE            = 2;
	const DOCUMENT_TYPE_REGISTRATION      = 3;
	const DOCUMENT_TYPE_WEB_RETRIEVAL     = 4;
	const DOCUMENT_TYPE_TEMPLATE          = 5;
	const DOCUMENT_TYPE_DISCONNECT_NOTICE = 6;
	const DOCUMENT_TYPE_DISABLE_UEM_DOCUMENTATION = 7;
	const DOCUMENT_TYPE_PROVIDER = 8;
}
?>