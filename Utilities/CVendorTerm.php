<?php

class CVendorTerm extends CBaseVendorTerm {

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valName() {
		return true;
	}

	public function valSystemCode() {
		return true;
	}

	public function valPercentage() {
		return true;
	}

	public function valDays() {
		return true;
	}

	public function valDiscountPeriodDays() {
		return true;
	}

	public function valIsFromInvoiceDate() {
		return true;
	}

	public function valIsSystem() {
		return true;
	}

	public function valOrderNum() {
		return true;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>