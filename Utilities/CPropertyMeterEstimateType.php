<?php

class CPropertyMeterEstimateType extends CBasePropertyMeterEstimateType {

	const PROPERTY_AVERAGE					 = 1;
	const OCCUPANCY_AVERAGE					 = 2;
	const BEDROOM_AVERAGE					 = 3;
	const PREVIOUS_MONTH_UNIT_AVERAGE		 = 4;
	const PREVIOUS_THREE_MONTHS_UNIT_AVERAGE = 5;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getAllPublishPropertyMeterEstimateTypes() {

		$arrmixPropertyMeterEstimateTypes = [
		    self::PROPERTY_AVERAGE 					 => 'Property Average',
		    self::OCCUPANCY_AVERAGE 				 => 'Occupancy Average',
		    self::BEDROOM_AVERAGE 					 => 'Bedroom Average',
			self::PREVIOUS_MONTH_UNIT_AVERAGE 		 => 'Unit\'s previous month\'s usage',
		    self::PREVIOUS_THREE_MONTHS_UNIT_AVERAGE => 'Unit\'s 3 month rolling average'
		];

		return $arrmixPropertyMeterEstimateTypes;
	}

}
?>