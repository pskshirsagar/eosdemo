<?php

class CCommodity extends CBaseCommodity {

    const ELECTRIC   = 1;
    const NATURALGAS = 2;
    const WATER      = 3;
    const SEWER      = 4;
    const REFUSE     = 5;
	const UNKNOWN	 = 38;
	const COOKING_GAS = 34;

    public static $c_arrintDefaultCommodities	= [
        self::ELECTRIC,
        self::NATURALGAS,
        self::WATER,
        self::SEWER,
        self::REFUSE
    ];

	public static $c_arrintDefaultGasCommodities	= [
		self::NATURALGAS,
		self::COOKING_GAS
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentCommodityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function extractCommoditiesFromEscalationNote( $strEscalationNote ) {

		$arrstrEscalationNote = explode( 'account number :', $strEscalationNote );
		$arrstrCommodities    = [];
		foreach( $arrstrEscalationNote AS $strTmpEscalationNote ) {

			if( false === stripos( $strTmpEscalationNote, 'mismatch utilities' ) ) {
				continue;
			}

			$arrstrTempCommodities = explode( 'mismatch utilities', $strTmpEscalationNote );

			$arrstrCommodities = array_merge( $arrstrCommodities, explode( ',', $arrstrTempCommodities[\Psi\Libraries\UtilFunctions\count( $arrstrTempCommodities ) - 1] ) );

		}

		$arrstrCommodities = array_map( function ( $strCommodity ) {
			$strCommodity = trim( $strCommodity );
			$strCommodity = strtoupper( $strCommodity );

			return $strCommodity;
		}, $arrstrCommodities );

		return $arrstrCommodities;

	}

}
?>