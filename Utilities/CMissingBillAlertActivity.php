<?php

class CMissingBillAlertActivity extends CBaseMissingBillAlertActivity {

    public function valMissingBillAlertEventId() {
        $boolIsValid = true;
        if( true == is_null( $this->getMissingBillAlertEventId() ) ) {
            $boolIsValid &= false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'missing_bill_alert_event_id', 'Please select Activity.' ) );
        }
        return $boolIsValid;
    }

    public function valMemo() {
        $boolIsValid = true;
        if( false == valStr( $this->getMemo() ) && ( CMissingBillAlertEvent::CONTACTED_PROVIDER_PENDING == $this->getMissingBillAlertEventId() || CMissingBillAlertEvent::CONTACTED_CLIENT_PENDING == $this->getMissingBillAlertEventId() || CMissingBillAlertEvent::OTHER == $this->getMissingBillAlertEventId() ) ) {
            $boolIsValid &= false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'memo', 'Please Enter Memo.' ) );
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	    $boolIsValid &= $this->valMissingBillAlertEventId();
        	    $boolIsValid &= $this->valMemo();
        	    break;

        	case VALIDATE_UPDATE:
        	    $boolIsValid &= $this->valMissingBillAlertEventId();
        	    break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>