<?php

class CCustomResidentCharge extends CBaseCustomResidentCharge {

	protected $m_intUtilitytransactionId;

    public function setValues( $arrMixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrMixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrMixValues['utility_transaction_id'] ) ) {
    		$this->setUtilitytransactionId( $arrMixValues['utility_transaction_id'] );
	    }

    }

    public function getUtilitytransactionId() {
    	return $this->m_intUtilitytransactionId;
    }

    public function setUtilitytransactionId( $intUtilitytransactionId ) {
    	return $this->m_intUtilitytransactionId = $intUtilitytransactionId;
    }

    public function addErrorMsgs( $arrobjErrorMsgs ) {
    	if( true == valArr( $arrobjErrorMsgs ) ) {
    		foreach( $arrobjErrorMsgs as $objErrorMsg ) {
    			$this->addErrorMsg( $objErrorMsg );
    		}
    	}
    }

    public function valCid() {

    	$boolIsValid = true;

    	if( true == is_null( $this->getCid() ) ) {

    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required.' ) );
    	}

        return $boolIsValid;
    }

    public function valPropertyId() {

    	$boolIsValid = true;

    	if( true == is_null( $this->getPropertyId() ) ) {

    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valPropertyUtilityTypeId() {

    	$boolIsValid = true;

    	if( true == is_null( $this->getPropertyUtilityTypeId() ) ) {

    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_utility_type_id', 'Property Utility Type  is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valArCodeId() {

    	$boolIsValid = true;

    	if( true == is_null( $this->getArCodeId() ) ) {

    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Charge code  is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valChargeName() {

    	$boolIsValid = true;

    	if( true == is_null( $this->getChargeName() ) ) {

    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_name', 'Charge name  is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valRate() {

        $boolIsValid = true;

    	if( true == is_null( $this->getRate() ) ) {

    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Rate  is required.' ) );
    	}

    	if( true == is_null( $this->getRate() ) || 0 == $this->getRate() ) {

    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Enter valid rate.' ) );
    	}
    	return $boolIsValid;
    }

    public function validate( $strAction, $objCustomResidentCharge, $objUtilitiesDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valPropertyUtilityTypeId();
            	$boolIsValid &= $this->valArCodeId();
            	$boolIsValid &= $this->valChargeName();
            	$boolIsValid &= $this->valRate();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $boolIsValid;
            	break;

            default:
				// default case
				$boolIsValid = true;
				break;
        }

        return $boolIsValid;
    }

}
?>