<?php

class CBaseUtilityPropertySetting extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_property_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUtilitySettingId;
	protected $m_intUtilityRolloutTypeId;
	protected $m_fltWeightedValue;
	protected $m_boolHideBuilding;
	protected $m_boolIsVcrOnly;
	protected $m_boolIsDisplayDirectMeteredInvoiceInRp;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolHideBuilding = false;
		$this->m_boolIsVcrOnly = false;
		$this->m_boolIsDisplayDirectMeteredInvoiceInRp = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_utility_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilitySettingId', trim( $arrValues['property_utility_setting_id'] ) ); elseif( isset( $arrValues['property_utility_setting_id'] ) ) $this->setPropertyUtilitySettingId( $arrValues['property_utility_setting_id'] );
		if( isset( $arrValues['utility_rollout_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityRolloutTypeId', trim( $arrValues['utility_rollout_type_id'] ) ); elseif( isset( $arrValues['utility_rollout_type_id'] ) ) $this->setUtilityRolloutTypeId( $arrValues['utility_rollout_type_id'] );
		if( isset( $arrValues['weighted_value'] ) && $boolDirectSet ) $this->set( 'm_fltWeightedValue', trim( $arrValues['weighted_value'] ) ); elseif( isset( $arrValues['weighted_value'] ) ) $this->setWeightedValue( $arrValues['weighted_value'] );
		if( isset( $arrValues['hide_building'] ) && $boolDirectSet ) $this->set( 'm_boolHideBuilding', trim( stripcslashes( $arrValues['hide_building'] ) ) ); elseif( isset( $arrValues['hide_building'] ) ) $this->setHideBuilding( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_building'] ) : $arrValues['hide_building'] );
		if( isset( $arrValues['is_vcr_only'] ) && $boolDirectSet ) $this->set( 'm_boolIsVcrOnly', trim( stripcslashes( $arrValues['is_vcr_only'] ) ) ); elseif( isset( $arrValues['is_vcr_only'] ) ) $this->setIsVcrOnly( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_vcr_only'] ) : $arrValues['is_vcr_only'] );
		if( isset( $arrValues['is_display_direct_metered_invoice_in_rp'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisplayDirectMeteredInvoiceInRp', trim( stripcslashes( $arrValues['is_display_direct_metered_invoice_in_rp'] ) ) ); elseif( isset( $arrValues['is_display_direct_metered_invoice_in_rp'] ) ) $this->setIsDisplayDirectMeteredInvoiceInRp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_display_direct_metered_invoice_in_rp'] ) : $arrValues['is_display_direct_metered_invoice_in_rp'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUtilitySettingId( $intPropertyUtilitySettingId ) {
		$this->set( 'm_intPropertyUtilitySettingId', CStrings::strToIntDef( $intPropertyUtilitySettingId, NULL, false ) );
	}

	public function getPropertyUtilitySettingId() {
		return $this->m_intPropertyUtilitySettingId;
	}

	public function sqlPropertyUtilitySettingId() {
		return ( true == isset( $this->m_intPropertyUtilitySettingId ) ) ? ( string ) $this->m_intPropertyUtilitySettingId : 'NULL';
	}

	public function setUtilityRolloutTypeId( $intUtilityRolloutTypeId ) {
		$this->set( 'm_intUtilityRolloutTypeId', CStrings::strToIntDef( $intUtilityRolloutTypeId, NULL, false ) );
	}

	public function getUtilityRolloutTypeId() {
		return $this->m_intUtilityRolloutTypeId;
	}

	public function sqlUtilityRolloutTypeId() {
		return ( true == isset( $this->m_intUtilityRolloutTypeId ) ) ? ( string ) $this->m_intUtilityRolloutTypeId : 'NULL';
	}

	public function setWeightedValue( $fltWeightedValue ) {
		$this->set( 'm_fltWeightedValue', CStrings::strToFloatDef( $fltWeightedValue, NULL, false, 2 ) );
	}

	public function getWeightedValue() {
		return $this->m_fltWeightedValue;
	}

	public function sqlWeightedValue() {
		return ( true == isset( $this->m_fltWeightedValue ) ) ? ( string ) $this->m_fltWeightedValue : 'NULL';
	}

	public function setHideBuilding( $boolHideBuilding ) {
		$this->set( 'm_boolHideBuilding', CStrings::strToBool( $boolHideBuilding ) );
	}

	public function getHideBuilding() {
		return $this->m_boolHideBuilding;
	}

	public function sqlHideBuilding() {
		return ( true == isset( $this->m_boolHideBuilding ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideBuilding ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsVcrOnly( $boolIsVcrOnly ) {
		$this->set( 'm_boolIsVcrOnly', CStrings::strToBool( $boolIsVcrOnly ) );
	}

	public function getIsVcrOnly() {
		return $this->m_boolIsVcrOnly;
	}

	public function sqlIsVcrOnly() {
		return ( true == isset( $this->m_boolIsVcrOnly ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVcrOnly ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisplayDirectMeteredInvoiceInRp( $boolIsDisplayDirectMeteredInvoiceInRp ) {
		$this->set( 'm_boolIsDisplayDirectMeteredInvoiceInRp', CStrings::strToBool( $boolIsDisplayDirectMeteredInvoiceInRp ) );
	}

	public function getIsDisplayDirectMeteredInvoiceInRp() {
		return $this->m_boolIsDisplayDirectMeteredInvoiceInRp;
	}

	public function sqlIsDisplayDirectMeteredInvoiceInRp() {
		return ( true == isset( $this->m_boolIsDisplayDirectMeteredInvoiceInRp ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisplayDirectMeteredInvoiceInRp ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_setting_id, utility_rollout_type_id, weighted_value, hide_building, is_vcr_only, is_display_direct_metered_invoice_in_rp, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUtilitySettingId() . ', ' .
						$this->sqlUtilityRolloutTypeId() . ', ' .
						$this->sqlWeightedValue() . ', ' .
						$this->sqlHideBuilding() . ', ' .
						$this->sqlIsVcrOnly() . ', ' .
						$this->sqlIsDisplayDirectMeteredInvoiceInRp() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilitySettingId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_rollout_type_id = ' . $this->sqlUtilityRolloutTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityRolloutTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_rollout_type_id = ' . $this->sqlUtilityRolloutTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' weighted_value = ' . $this->sqlWeightedValue(). ',' ; } elseif( true == array_key_exists( 'WeightedValue', $this->getChangedColumns() ) ) { $strSql .= ' weighted_value = ' . $this->sqlWeightedValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_building = ' . $this->sqlHideBuilding(). ',' ; } elseif( true == array_key_exists( 'HideBuilding', $this->getChangedColumns() ) ) { $strSql .= ' hide_building = ' . $this->sqlHideBuilding() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_vcr_only = ' . $this->sqlIsVcrOnly(). ',' ; } elseif( true == array_key_exists( 'IsVcrOnly', $this->getChangedColumns() ) ) { $strSql .= ' is_vcr_only = ' . $this->sqlIsVcrOnly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_display_direct_metered_invoice_in_rp = ' . $this->sqlIsDisplayDirectMeteredInvoiceInRp(). ',' ; } elseif( true == array_key_exists( 'IsDisplayDirectMeteredInvoiceInRp', $this->getChangedColumns() ) ) { $strSql .= ' is_display_direct_metered_invoice_in_rp = ' . $this->sqlIsDisplayDirectMeteredInvoiceInRp() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_utility_setting_id' => $this->getPropertyUtilitySettingId(),
			'utility_rollout_type_id' => $this->getUtilityRolloutTypeId(),
			'weighted_value' => $this->getWeightedValue(),
			'hide_building' => $this->getHideBuilding(),
			'is_vcr_only' => $this->getIsVcrOnly(),
			'is_display_direct_metered_invoice_in_rp' => $this->getIsDisplayDirectMeteredInvoiceInRp(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>