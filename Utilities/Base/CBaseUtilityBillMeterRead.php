<?php

class CBaseUtilityBillMeterRead extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_meter_reads';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillId;
	protected $m_intUtilityBillAccountMeterId;
	protected $m_intExternalId;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_fltStartRead;
	protected $m_fltEndRead;
	protected $m_fltConsumptionUnits;
	protected $m_fltUnitConsumptionAmount;
	protected $m_strScheduledExportDate;
	protected $m_boolIsEstimated;
	protected $m_intMeterMultiplier;
	protected $m_intExportedBy;
	protected $m_strExportedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltDemand;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsEstimated = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['utility_bill_account_meter_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountMeterId', trim( $arrValues['utility_bill_account_meter_id'] ) ); elseif( isset( $arrValues['utility_bill_account_meter_id'] ) ) $this->setUtilityBillAccountMeterId( $arrValues['utility_bill_account_meter_id'] );
		if( isset( $arrValues['external_id'] ) && $boolDirectSet ) $this->set( 'm_intExternalId', trim( $arrValues['external_id'] ) ); elseif( isset( $arrValues['external_id'] ) ) $this->setExternalId( $arrValues['external_id'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['start_read'] ) && $boolDirectSet ) $this->set( 'm_fltStartRead', trim( $arrValues['start_read'] ) ); elseif( isset( $arrValues['start_read'] ) ) $this->setStartRead( $arrValues['start_read'] );
		if( isset( $arrValues['end_read'] ) && $boolDirectSet ) $this->set( 'm_fltEndRead', trim( $arrValues['end_read'] ) ); elseif( isset( $arrValues['end_read'] ) ) $this->setEndRead( $arrValues['end_read'] );
		if( isset( $arrValues['consumption_units'] ) && $boolDirectSet ) $this->set( 'm_fltConsumptionUnits', trim( $arrValues['consumption_units'] ) ); elseif( isset( $arrValues['consumption_units'] ) ) $this->setConsumptionUnits( $arrValues['consumption_units'] );
		if( isset( $arrValues['unit_consumption_amount'] ) && $boolDirectSet ) $this->set( 'm_fltUnitConsumptionAmount', trim( $arrValues['unit_consumption_amount'] ) ); elseif( isset( $arrValues['unit_consumption_amount'] ) ) $this->setUnitConsumptionAmount( $arrValues['unit_consumption_amount'] );
		if( isset( $arrValues['scheduled_export_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledExportDate', trim( $arrValues['scheduled_export_date'] ) ); elseif( isset( $arrValues['scheduled_export_date'] ) ) $this->setScheduledExportDate( $arrValues['scheduled_export_date'] );
		if( isset( $arrValues['is_estimated'] ) && $boolDirectSet ) $this->set( 'm_boolIsEstimated', trim( stripcslashes( $arrValues['is_estimated'] ) ) ); elseif( isset( $arrValues['is_estimated'] ) ) $this->setIsEstimated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_estimated'] ) : $arrValues['is_estimated'] );
		if( isset( $arrValues['meter_multiplier'] ) && $boolDirectSet ) $this->set( 'm_intMeterMultiplier', trim( $arrValues['meter_multiplier'] ) ); elseif( isset( $arrValues['meter_multiplier'] ) ) $this->setMeterMultiplier( $arrValues['meter_multiplier'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['demand'] ) && $boolDirectSet ) $this->set( 'm_fltDemand', trim( $arrValues['demand'] ) ); elseif( isset( $arrValues['demand'] ) ) $this->setDemand( $arrValues['demand'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setUtilityBillAccountMeterId( $intUtilityBillAccountMeterId ) {
		$this->set( 'm_intUtilityBillAccountMeterId', CStrings::strToIntDef( $intUtilityBillAccountMeterId, NULL, false ) );
	}

	public function getUtilityBillAccountMeterId() {
		return $this->m_intUtilityBillAccountMeterId;
	}

	public function sqlUtilityBillAccountMeterId() {
		return ( true == isset( $this->m_intUtilityBillAccountMeterId ) ) ? ( string ) $this->m_intUtilityBillAccountMeterId : 'NULL';
	}

	public function setExternalId( $intExternalId ) {
		$this->set( 'm_intExternalId', CStrings::strToIntDef( $intExternalId, NULL, false ) );
	}

	public function getExternalId() {
		return $this->m_intExternalId;
	}

	public function sqlExternalId() {
		return ( true == isset( $this->m_intExternalId ) ) ? ( string ) $this->m_intExternalId : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setStartRead( $fltStartRead ) {
		$this->set( 'm_fltStartRead', CStrings::strToFloatDef( $fltStartRead, NULL, false, 4 ) );
	}

	public function getStartRead() {
		return $this->m_fltStartRead;
	}

	public function sqlStartRead() {
		return ( true == isset( $this->m_fltStartRead ) ) ? ( string ) $this->m_fltStartRead : 'NULL';
	}

	public function setEndRead( $fltEndRead ) {
		$this->set( 'm_fltEndRead', CStrings::strToFloatDef( $fltEndRead, NULL, false, 4 ) );
	}

	public function getEndRead() {
		return $this->m_fltEndRead;
	}

	public function sqlEndRead() {
		return ( true == isset( $this->m_fltEndRead ) ) ? ( string ) $this->m_fltEndRead : 'NULL';
	}

	public function setConsumptionUnits( $fltConsumptionUnits ) {
		$this->set( 'm_fltConsumptionUnits', CStrings::strToFloatDef( $fltConsumptionUnits, NULL, false, 4 ) );
	}

	public function getConsumptionUnits() {
		return $this->m_fltConsumptionUnits;
	}

	public function sqlConsumptionUnits() {
		return ( true == isset( $this->m_fltConsumptionUnits ) ) ? ( string ) $this->m_fltConsumptionUnits : 'NULL';
	}

	public function setUnitConsumptionAmount( $fltUnitConsumptionAmount ) {
		$this->set( 'm_fltUnitConsumptionAmount', CStrings::strToFloatDef( $fltUnitConsumptionAmount, NULL, false, 4 ) );
	}

	public function getUnitConsumptionAmount() {
		return $this->m_fltUnitConsumptionAmount;
	}

	public function sqlUnitConsumptionAmount() {
		return ( true == isset( $this->m_fltUnitConsumptionAmount ) ) ? ( string ) $this->m_fltUnitConsumptionAmount : 'NULL';
	}

	public function setScheduledExportDate( $strScheduledExportDate ) {
		$this->set( 'm_strScheduledExportDate', CStrings::strTrimDef( $strScheduledExportDate, -1, NULL, true ) );
	}

	public function getScheduledExportDate() {
		return $this->m_strScheduledExportDate;
	}

	public function sqlScheduledExportDate() {
		return ( true == isset( $this->m_strScheduledExportDate ) ) ? '\'' . $this->m_strScheduledExportDate . '\'' : 'NULL';
	}

	public function setIsEstimated( $boolIsEstimated ) {
		$this->set( 'm_boolIsEstimated', CStrings::strToBool( $boolIsEstimated ) );
	}

	public function getIsEstimated() {
		return $this->m_boolIsEstimated;
	}

	public function sqlIsEstimated() {
		return ( true == isset( $this->m_boolIsEstimated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEstimated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMeterMultiplier( $intMeterMultiplier ) {
		$this->set( 'm_intMeterMultiplier', CStrings::strToIntDef( $intMeterMultiplier, NULL, false ) );
	}

	public function getMeterMultiplier() {
		return $this->m_intMeterMultiplier;
	}

	public function sqlMeterMultiplier() {
		return ( true == isset( $this->m_intMeterMultiplier ) ) ? ( string ) $this->m_intMeterMultiplier : 'NULL';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDemand( $fltDemand ) {
		$this->set( 'm_fltDemand', CStrings::strToFloatDef( $fltDemand, NULL, false, 4 ) );
	}

	public function getDemand() {
		return $this->m_fltDemand;
	}

	public function sqlDemand() {
		return ( true == isset( $this->m_fltDemand ) ) ? ( string ) $this->m_fltDemand : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_id, utility_bill_account_meter_id, external_id, start_date, end_date, start_read, end_read, consumption_units, unit_consumption_amount, scheduled_export_date, is_estimated, meter_multiplier, exported_by, exported_on, updated_by, updated_on, created_by, created_on, demand )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlUtilityBillAccountMeterId() . ', ' .
						$this->sqlExternalId() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlStartRead() . ', ' .
						$this->sqlEndRead() . ', ' .
						$this->sqlConsumptionUnits() . ', ' .
						$this->sqlUnitConsumptionAmount() . ', ' .
						$this->sqlScheduledExportDate() . ', ' .
						$this->sqlIsEstimated() . ', ' .
						$this->sqlMeterMultiplier() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDemand() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_meter_id = ' . $this->sqlUtilityBillAccountMeterId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountMeterId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_meter_id = ' . $this->sqlUtilityBillAccountMeterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_id = ' . $this->sqlExternalId(). ',' ; } elseif( true == array_key_exists( 'ExternalId', $this->getChangedColumns() ) ) { $strSql .= ' external_id = ' . $this->sqlExternalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_read = ' . $this->sqlStartRead(). ',' ; } elseif( true == array_key_exists( 'StartRead', $this->getChangedColumns() ) ) { $strSql .= ' start_read = ' . $this->sqlStartRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_read = ' . $this->sqlEndRead(). ',' ; } elseif( true == array_key_exists( 'EndRead', $this->getChangedColumns() ) ) { $strSql .= ' end_read = ' . $this->sqlEndRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumption_units = ' . $this->sqlConsumptionUnits(). ',' ; } elseif( true == array_key_exists( 'ConsumptionUnits', $this->getChangedColumns() ) ) { $strSql .= ' consumption_units = ' . $this->sqlConsumptionUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_consumption_amount = ' . $this->sqlUnitConsumptionAmount(). ',' ; } elseif( true == array_key_exists( 'UnitConsumptionAmount', $this->getChangedColumns() ) ) { $strSql .= ' unit_consumption_amount = ' . $this->sqlUnitConsumptionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_export_date = ' . $this->sqlScheduledExportDate(). ',' ; } elseif( true == array_key_exists( 'ScheduledExportDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_export_date = ' . $this->sqlScheduledExportDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_estimated = ' . $this->sqlIsEstimated(). ',' ; } elseif( true == array_key_exists( 'IsEstimated', $this->getChangedColumns() ) ) { $strSql .= ' is_estimated = ' . $this->sqlIsEstimated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_multiplier = ' . $this->sqlMeterMultiplier(). ',' ; } elseif( true == array_key_exists( 'MeterMultiplier', $this->getChangedColumns() ) ) { $strSql .= ' meter_multiplier = ' . $this->sqlMeterMultiplier() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy(). ',' ; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demand = ' . $this->sqlDemand(). ',' ; } elseif( true == array_key_exists( 'Demand', $this->getChangedColumns() ) ) { $strSql .= ' demand = ' . $this->sqlDemand() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'utility_bill_account_meter_id' => $this->getUtilityBillAccountMeterId(),
			'external_id' => $this->getExternalId(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'start_read' => $this->getStartRead(),
			'end_read' => $this->getEndRead(),
			'consumption_units' => $this->getConsumptionUnits(),
			'unit_consumption_amount' => $this->getUnitConsumptionAmount(),
			'scheduled_export_date' => $this->getScheduledExportDate(),
			'is_estimated' => $this->getIsEstimated(),
			'meter_multiplier' => $this->getMeterMultiplier(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'demand' => $this->getDemand()
		);
	}

}
?>