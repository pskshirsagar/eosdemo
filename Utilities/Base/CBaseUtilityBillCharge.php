<?php

class CBaseUtilityBillCharge extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_charges';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intUtilityBillId;
	protected $m_intApTransactionDetailId;
	protected $m_intUtilityBillTemplateChargeId;
	protected $m_intApDetailId;
	protected $m_intPoApDetailId;
	protected $m_intApCodeId;
	protected $m_intUnitOfMeasureId;
	protected $m_intUnitCount;
	protected $m_intConsumptionUnits;
	protected $m_intDemand;
	protected $m_strChargeDatetime;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltAmount;
	protected $m_fltRate;
	protected $m_fltQuantity;
	protected $m_fltMeterStartRead;
	protected $m_fltMeterEndRead;
	protected $m_fltUnitConsumptionAmount;
	protected $m_strBillingStartDate;
	protected $m_strBillingEndDate;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_boolIsLateFee;
	protected $m_boolIsCurrent;
	protected $m_boolIsBaseFee;
	protected $m_boolIsRecovered;
	protected $m_boolIsEstimated;
	protected $m_boolIsTimeOfUse;
	protected $m_boolIsTax;
	protected $m_boolIsLateFeeLiable;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intJobPhaseId;
	protected $m_intApContractId;
	protected $m_intUtilityBillAccountTemplateChargeId;
	protected $m_intCopyUtilityBillChargeId;
	protected $m_fltUsage;
	protected $m_intCommodityId;
	protected $m_boolIsBudgetAdjustment;

	public function __construct() {
		parent::__construct();

		$this->m_fltAmount = '0';
		$this->m_boolIsLateFee = false;
		$this->m_boolIsCurrent = true;
		$this->m_boolIsBaseFee = false;
		$this->m_boolIsRecovered = true;
		$this->m_boolIsEstimated = false;
		$this->m_boolIsTimeOfUse = false;
		$this->m_boolIsTax = false;
		$this->m_boolIsLateFeeLiable = false;
		$this->m_boolIsBudgetAdjustment = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['ap_transaction_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intApTransactionDetailId', trim( $arrValues['ap_transaction_detail_id'] ) ); elseif( isset( $arrValues['ap_transaction_detail_id'] ) ) $this->setApTransactionDetailId( $arrValues['ap_transaction_detail_id'] );
		if( isset( $arrValues['utility_bill_template_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillTemplateChargeId', trim( $arrValues['utility_bill_template_charge_id'] ) ); elseif( isset( $arrValues['utility_bill_template_charge_id'] ) ) $this->setUtilityBillTemplateChargeId( $arrValues['utility_bill_template_charge_id'] );
		if( isset( $arrValues['ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intApDetailId', trim( $arrValues['ap_detail_id'] ) ); elseif( isset( $arrValues['ap_detail_id'] ) ) $this->setApDetailId( $arrValues['ap_detail_id'] );
		if( isset( $arrValues['po_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intPoApDetailId', trim( $arrValues['po_ap_detail_id'] ) ); elseif( isset( $arrValues['po_ap_detail_id'] ) ) $this->setPoApDetailId( $arrValues['po_ap_detail_id'] );
		if( isset( $arrValues['ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeId', trim( $arrValues['ap_code_id'] ) ); elseif( isset( $arrValues['ap_code_id'] ) ) $this->setApCodeId( $arrValues['ap_code_id'] );
		if( isset( $arrValues['unit_of_measure_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitOfMeasureId', trim( $arrValues['unit_of_measure_id'] ) ); elseif( isset( $arrValues['unit_of_measure_id'] ) ) $this->setUnitOfMeasureId( $arrValues['unit_of_measure_id'] );
		if( isset( $arrValues['unit_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitCount', trim( $arrValues['unit_count'] ) ); elseif( isset( $arrValues['unit_count'] ) ) $this->setUnitCount( $arrValues['unit_count'] );
		if( isset( $arrValues['consumption_units'] ) && $boolDirectSet ) $this->set( 'm_intConsumptionUnits', trim( $arrValues['consumption_units'] ) ); elseif( isset( $arrValues['consumption_units'] ) ) $this->setConsumptionUnits( $arrValues['consumption_units'] );
		if( isset( $arrValues['demand'] ) && $boolDirectSet ) $this->set( 'm_intDemand', trim( $arrValues['demand'] ) ); elseif( isset( $arrValues['demand'] ) ) $this->setDemand( $arrValues['demand'] );
		if( isset( $arrValues['charge_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChargeDatetime', trim( $arrValues['charge_datetime'] ) ); elseif( isset( $arrValues['charge_datetime'] ) ) $this->setChargeDatetime( $arrValues['charge_datetime'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['rate'] ) && $boolDirectSet ) $this->set( 'm_fltRate', trim( $arrValues['rate'] ) ); elseif( isset( $arrValues['rate'] ) ) $this->setRate( $arrValues['rate'] );
		if( isset( $arrValues['quantity'] ) && $boolDirectSet ) $this->set( 'm_fltQuantity', trim( $arrValues['quantity'] ) ); elseif( isset( $arrValues['quantity'] ) ) $this->setQuantity( $arrValues['quantity'] );
		if( isset( $arrValues['meter_start_read'] ) && $boolDirectSet ) $this->set( 'm_fltMeterStartRead', trim( $arrValues['meter_start_read'] ) ); elseif( isset( $arrValues['meter_start_read'] ) ) $this->setMeterStartRead( $arrValues['meter_start_read'] );
		if( isset( $arrValues['meter_end_read'] ) && $boolDirectSet ) $this->set( 'm_fltMeterEndRead', trim( $arrValues['meter_end_read'] ) ); elseif( isset( $arrValues['meter_end_read'] ) ) $this->setMeterEndRead( $arrValues['meter_end_read'] );
		if( isset( $arrValues['unit_consumption_amount'] ) && $boolDirectSet ) $this->set( 'm_fltUnitConsumptionAmount', trim( $arrValues['unit_consumption_amount'] ) ); elseif( isset( $arrValues['unit_consumption_amount'] ) ) $this->setUnitConsumptionAmount( $arrValues['unit_consumption_amount'] );
		if( isset( $arrValues['billing_start_date'] ) && $boolDirectSet ) $this->set( 'm_strBillingStartDate', trim( $arrValues['billing_start_date'] ) ); elseif( isset( $arrValues['billing_start_date'] ) ) $this->setBillingStartDate( $arrValues['billing_start_date'] );
		if( isset( $arrValues['billing_end_date'] ) && $boolDirectSet ) $this->set( 'm_strBillingEndDate', trim( $arrValues['billing_end_date'] ) ); elseif( isset( $arrValues['billing_end_date'] ) ) $this->setBillingEndDate( $arrValues['billing_end_date'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['is_late_fee'] ) && $boolDirectSet ) $this->set( 'm_boolIsLateFee', trim( stripcslashes( $arrValues['is_late_fee'] ) ) ); elseif( isset( $arrValues['is_late_fee'] ) ) $this->setIsLateFee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_late_fee'] ) : $arrValues['is_late_fee'] );
		if( isset( $arrValues['is_current'] ) && $boolDirectSet ) $this->set( 'm_boolIsCurrent', trim( stripcslashes( $arrValues['is_current'] ) ) ); elseif( isset( $arrValues['is_current'] ) ) $this->setIsCurrent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_current'] ) : $arrValues['is_current'] );
		if( isset( $arrValues['is_base_fee'] ) && $boolDirectSet ) $this->set( 'm_boolIsBaseFee', trim( stripcslashes( $arrValues['is_base_fee'] ) ) ); elseif( isset( $arrValues['is_base_fee'] ) ) $this->setIsBaseFee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_base_fee'] ) : $arrValues['is_base_fee'] );
		if( isset( $arrValues['is_recovered'] ) && $boolDirectSet ) $this->set( 'm_boolIsRecovered', trim( stripcslashes( $arrValues['is_recovered'] ) ) ); elseif( isset( $arrValues['is_recovered'] ) ) $this->setIsRecovered( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_recovered'] ) : $arrValues['is_recovered'] );
		if( isset( $arrValues['is_estimated'] ) && $boolDirectSet ) $this->set( 'm_boolIsEstimated', trim( stripcslashes( $arrValues['is_estimated'] ) ) ); elseif( isset( $arrValues['is_estimated'] ) ) $this->setIsEstimated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_estimated'] ) : $arrValues['is_estimated'] );
		if( isset( $arrValues['is_time_of_use'] ) && $boolDirectSet ) $this->set( 'm_boolIsTimeOfUse', trim( stripcslashes( $arrValues['is_time_of_use'] ) ) ); elseif( isset( $arrValues['is_time_of_use'] ) ) $this->setIsTimeOfUse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_time_of_use'] ) : $arrValues['is_time_of_use'] );
		if( isset( $arrValues['is_tax'] ) && $boolDirectSet ) $this->set( 'm_boolIsTax', trim( stripcslashes( $arrValues['is_tax'] ) ) ); elseif( isset( $arrValues['is_tax'] ) ) $this->setIsTax( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_tax'] ) : $arrValues['is_tax'] );
		if( isset( $arrValues['is_late_fee_liable'] ) && $boolDirectSet ) $this->set( 'm_boolIsLateFeeLiable', trim( stripcslashes( $arrValues['is_late_fee_liable'] ) ) ); elseif( isset( $arrValues['is_late_fee_liable'] ) ) $this->setIsLateFeeLiable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_late_fee_liable'] ) : $arrValues['is_late_fee_liable'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['job_phase_id'] ) && $boolDirectSet ) $this->set( 'm_intJobPhaseId', trim( $arrValues['job_phase_id'] ) ); elseif( isset( $arrValues['job_phase_id'] ) ) $this->setJobPhaseId( $arrValues['job_phase_id'] );
		if( isset( $arrValues['ap_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intApContractId', trim( $arrValues['ap_contract_id'] ) ); elseif( isset( $arrValues['ap_contract_id'] ) ) $this->setApContractId( $arrValues['ap_contract_id'] );
		if( isset( $arrValues['utility_bill_account_template_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountTemplateChargeId', trim( $arrValues['utility_bill_account_template_charge_id'] ) ); elseif( isset( $arrValues['utility_bill_account_template_charge_id'] ) ) $this->setUtilityBillAccountTemplateChargeId( $arrValues['utility_bill_account_template_charge_id'] );
		if( isset( $arrValues['copy_utility_bill_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intCopyUtilityBillChargeId', trim( $arrValues['copy_utility_bill_charge_id'] ) ); elseif( isset( $arrValues['copy_utility_bill_charge_id'] ) ) $this->setCopyUtilityBillChargeId( $arrValues['copy_utility_bill_charge_id'] );
		if( isset( $arrValues['usage'] ) && $boolDirectSet ) $this->set( 'm_fltUsage', trim( $arrValues['usage'] ) ); elseif( isset( $arrValues['usage'] ) ) $this->setUsage( $arrValues['usage'] );
		if( isset( $arrValues['commodity_id'] ) && $boolDirectSet ) $this->set( 'm_intCommodityId', trim( $arrValues['commodity_id'] ) ); elseif( isset( $arrValues['commodity_id'] ) ) $this->setCommodityId( $arrValues['commodity_id'] );
		if( isset( $arrValues['is_budget_adjustment'] ) && $boolDirectSet ) $this->set( 'm_boolIsBudgetAdjustment', trim( stripcslashes( $arrValues['is_budget_adjustment'] ) ) ); elseif( isset( $arrValues['is_budget_adjustment'] ) ) $this->setIsBudgetAdjustment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_budget_adjustment'] ) : $arrValues['is_budget_adjustment'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setApTransactionDetailId( $intApTransactionDetailId ) {
		$this->set( 'm_intApTransactionDetailId', CStrings::strToIntDef( $intApTransactionDetailId, NULL, false ) );
	}

	public function getApTransactionDetailId() {
		return $this->m_intApTransactionDetailId;
	}

	public function sqlApTransactionDetailId() {
		return ( true == isset( $this->m_intApTransactionDetailId ) ) ? ( string ) $this->m_intApTransactionDetailId : 'NULL';
	}

	public function setUtilityBillTemplateChargeId( $intUtilityBillTemplateChargeId ) {
		$this->set( 'm_intUtilityBillTemplateChargeId', CStrings::strToIntDef( $intUtilityBillTemplateChargeId, NULL, false ) );
	}

	public function getUtilityBillTemplateChargeId() {
		return $this->m_intUtilityBillTemplateChargeId;
	}

	public function sqlUtilityBillTemplateChargeId() {
		return ( true == isset( $this->m_intUtilityBillTemplateChargeId ) ) ? ( string ) $this->m_intUtilityBillTemplateChargeId : 'NULL';
	}

	public function setApDetailId( $intApDetailId ) {
		$this->set( 'm_intApDetailId', CStrings::strToIntDef( $intApDetailId, NULL, false ) );
	}

	public function getApDetailId() {
		return $this->m_intApDetailId;
	}

	public function sqlApDetailId() {
		return ( true == isset( $this->m_intApDetailId ) ) ? ( string ) $this->m_intApDetailId : 'NULL';
	}

	public function setPoApDetailId( $intPoApDetailId ) {
		$this->set( 'm_intPoApDetailId', CStrings::strToIntDef( $intPoApDetailId, NULL, false ) );
	}

	public function getPoApDetailId() {
		return $this->m_intPoApDetailId;
	}

	public function sqlPoApDetailId() {
		return ( true == isset( $this->m_intPoApDetailId ) ) ? ( string ) $this->m_intPoApDetailId : 'NULL';
	}

	public function setApCodeId( $intApCodeId ) {
		$this->set( 'm_intApCodeId', CStrings::strToIntDef( $intApCodeId, NULL, false ) );
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function sqlApCodeId() {
		return ( true == isset( $this->m_intApCodeId ) ) ? ( string ) $this->m_intApCodeId : 'NULL';
	}

	public function setUnitOfMeasureId( $intUnitOfMeasureId ) {
		$this->set( 'm_intUnitOfMeasureId', CStrings::strToIntDef( $intUnitOfMeasureId, NULL, false ) );
	}

	public function getUnitOfMeasureId() {
		return $this->m_intUnitOfMeasureId;
	}

	public function sqlUnitOfMeasureId() {
		return ( true == isset( $this->m_intUnitOfMeasureId ) ) ? ( string ) $this->m_intUnitOfMeasureId : 'NULL';
	}

	public function setUnitCount( $intUnitCount ) {
		$this->set( 'm_intUnitCount', CStrings::strToIntDef( $intUnitCount, NULL, false ) );
	}

	public function getUnitCount() {
		return $this->m_intUnitCount;
	}

	public function sqlUnitCount() {
		return ( true == isset( $this->m_intUnitCount ) ) ? ( string ) $this->m_intUnitCount : 'NULL';
	}

	public function setConsumptionUnits( $intConsumptionUnits ) {
		$this->set( 'm_intConsumptionUnits', CStrings::strToIntDef( $intConsumptionUnits, NULL, false ) );
	}

	public function getConsumptionUnits() {
		return $this->m_intConsumptionUnits;
	}

	public function sqlConsumptionUnits() {
		return ( true == isset( $this->m_intConsumptionUnits ) ) ? ( string ) $this->m_intConsumptionUnits : 'NULL';
	}

	public function setDemand( $intDemand ) {
		$this->set( 'm_intDemand', CStrings::strToIntDef( $intDemand, NULL, false ) );
	}

	public function getDemand() {
		return $this->m_intDemand;
	}

	public function sqlDemand() {
		return ( true == isset( $this->m_intDemand ) ) ? ( string ) $this->m_intDemand : 'NULL';
	}

	public function setChargeDatetime( $strChargeDatetime ) {
		$this->set( 'm_strChargeDatetime', CStrings::strTrimDef( $strChargeDatetime, -1, NULL, true ) );
	}

	public function getChargeDatetime() {
		return $this->m_strChargeDatetime;
	}

	public function sqlChargeDatetime() {
		return ( true == isset( $this->m_strChargeDatetime ) ) ? '\'' . $this->m_strChargeDatetime . '\'' : 'NOW()';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 2000, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 2 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : '0';
	}

	public function setRate( $fltRate ) {
		$this->set( 'm_fltRate', CStrings::strToFloatDef( $fltRate, NULL, false, 2 ) );
	}

	public function getRate() {
		return $this->m_fltRate;
	}

	public function sqlRate() {
		return ( true == isset( $this->m_fltRate ) ) ? ( string ) $this->m_fltRate : 'NULL';
	}

	public function setQuantity( $fltQuantity ) {
		$this->set( 'm_fltQuantity', CStrings::strToFloatDef( $fltQuantity, NULL, false, 2 ) );
	}

	public function getQuantity() {
		return $this->m_fltQuantity;
	}

	public function sqlQuantity() {
		return ( true == isset( $this->m_fltQuantity ) ) ? ( string ) $this->m_fltQuantity : 'NULL';
	}

	public function setMeterStartRead( $fltMeterStartRead ) {
		$this->set( 'm_fltMeterStartRead', CStrings::strToFloatDef( $fltMeterStartRead, NULL, false, 0 ) );
	}

	public function getMeterStartRead() {
		return $this->m_fltMeterStartRead;
	}

	public function sqlMeterStartRead() {
		return ( true == isset( $this->m_fltMeterStartRead ) ) ? ( string ) $this->m_fltMeterStartRead : 'NULL';
	}

	public function setMeterEndRead( $fltMeterEndRead ) {
		$this->set( 'm_fltMeterEndRead', CStrings::strToFloatDef( $fltMeterEndRead, NULL, false, 0 ) );
	}

	public function getMeterEndRead() {
		return $this->m_fltMeterEndRead;
	}

	public function sqlMeterEndRead() {
		return ( true == isset( $this->m_fltMeterEndRead ) ) ? ( string ) $this->m_fltMeterEndRead : 'NULL';
	}

	public function setUnitConsumptionAmount( $fltUnitConsumptionAmount ) {
		$this->set( 'm_fltUnitConsumptionAmount', CStrings::strToFloatDef( $fltUnitConsumptionAmount, NULL, false, 4 ) );
	}

	public function getUnitConsumptionAmount() {
		return $this->m_fltUnitConsumptionAmount;
	}

	public function sqlUnitConsumptionAmount() {
		return ( true == isset( $this->m_fltUnitConsumptionAmount ) ) ? ( string ) $this->m_fltUnitConsumptionAmount : 'NULL';
	}

	public function setBillingStartDate( $strBillingStartDate ) {
		$this->set( 'm_strBillingStartDate', CStrings::strTrimDef( $strBillingStartDate, -1, NULL, true ) );
	}

	public function getBillingStartDate() {
		return $this->m_strBillingStartDate;
	}

	public function sqlBillingStartDate() {
		return ( true == isset( $this->m_strBillingStartDate ) ) ? '\'' . $this->m_strBillingStartDate . '\'' : 'NULL';
	}

	public function setBillingEndDate( $strBillingEndDate ) {
		$this->set( 'm_strBillingEndDate', CStrings::strTrimDef( $strBillingEndDate, -1, NULL, true ) );
	}

	public function getBillingEndDate() {
		return $this->m_strBillingEndDate;
	}

	public function sqlBillingEndDate() {
		return ( true == isset( $this->m_strBillingEndDate ) ) ? '\'' . $this->m_strBillingEndDate . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setIsLateFee( $boolIsLateFee ) {
		$this->set( 'm_boolIsLateFee', CStrings::strToBool( $boolIsLateFee ) );
	}

	public function getIsLateFee() {
		return $this->m_boolIsLateFee;
	}

	public function sqlIsLateFee() {
		return ( true == isset( $this->m_boolIsLateFee ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLateFee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCurrent( $boolIsCurrent ) {
		$this->set( 'm_boolIsCurrent', CStrings::strToBool( $boolIsCurrent ) );
	}

	public function getIsCurrent() {
		return $this->m_boolIsCurrent;
	}

	public function sqlIsCurrent() {
		return ( true == isset( $this->m_boolIsCurrent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCurrent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsBaseFee( $boolIsBaseFee ) {
		$this->set( 'm_boolIsBaseFee', CStrings::strToBool( $boolIsBaseFee ) );
	}

	public function getIsBaseFee() {
		return $this->m_boolIsBaseFee;
	}

	public function sqlIsBaseFee() {
		return ( true == isset( $this->m_boolIsBaseFee ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBaseFee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRecovered( $boolIsRecovered ) {
		$this->set( 'm_boolIsRecovered', CStrings::strToBool( $boolIsRecovered ) );
	}

	public function getIsRecovered() {
		return $this->m_boolIsRecovered;
	}

	public function sqlIsRecovered() {
		return ( true == isset( $this->m_boolIsRecovered ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRecovered ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsEstimated( $boolIsEstimated ) {
		$this->set( 'm_boolIsEstimated', CStrings::strToBool( $boolIsEstimated ) );
	}

	public function getIsEstimated() {
		return $this->m_boolIsEstimated;
	}

	public function sqlIsEstimated() {
		return ( true == isset( $this->m_boolIsEstimated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEstimated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTimeOfUse( $boolIsTimeOfUse ) {
		$this->set( 'm_boolIsTimeOfUse', CStrings::strToBool( $boolIsTimeOfUse ) );
	}

	public function getIsTimeOfUse() {
		return $this->m_boolIsTimeOfUse;
	}

	public function sqlIsTimeOfUse() {
		return ( true == isset( $this->m_boolIsTimeOfUse ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTimeOfUse ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTax( $boolIsTax ) {
		$this->set( 'm_boolIsTax', CStrings::strToBool( $boolIsTax ) );
	}

	public function getIsTax() {
		return $this->m_boolIsTax;
	}

	public function sqlIsTax() {
		return ( true == isset( $this->m_boolIsTax ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTax ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLateFeeLiable( $boolIsLateFeeLiable ) {
		$this->set( 'm_boolIsLateFeeLiable', CStrings::strToBool( $boolIsLateFeeLiable ) );
	}

	public function getIsLateFeeLiable() {
		return $this->m_boolIsLateFeeLiable;
	}

	public function sqlIsLateFeeLiable() {
		return ( true == isset( $this->m_boolIsLateFeeLiable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLateFeeLiable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setJobPhaseId( $intJobPhaseId ) {
		$this->set( 'm_intJobPhaseId', CStrings::strToIntDef( $intJobPhaseId, NULL, false ) );
	}

	public function getJobPhaseId() {
		return $this->m_intJobPhaseId;
	}

	public function sqlJobPhaseId() {
		return ( true == isset( $this->m_intJobPhaseId ) ) ? ( string ) $this->m_intJobPhaseId : 'NULL';
	}

	public function setApContractId( $intApContractId ) {
		$this->set( 'm_intApContractId', CStrings::strToIntDef( $intApContractId, NULL, false ) );
	}

	public function getApContractId() {
		return $this->m_intApContractId;
	}

	public function sqlApContractId() {
		return ( true == isset( $this->m_intApContractId ) ) ? ( string ) $this->m_intApContractId : 'NULL';
	}

	public function setUtilityBillAccountTemplateChargeId( $intUtilityBillAccountTemplateChargeId ) {
		$this->set( 'm_intUtilityBillAccountTemplateChargeId', CStrings::strToIntDef( $intUtilityBillAccountTemplateChargeId, NULL, false ) );
	}

	public function getUtilityBillAccountTemplateChargeId() {
		return $this->m_intUtilityBillAccountTemplateChargeId;
	}

	public function sqlUtilityBillAccountTemplateChargeId() {
		return ( true == isset( $this->m_intUtilityBillAccountTemplateChargeId ) ) ? ( string ) $this->m_intUtilityBillAccountTemplateChargeId : 'NULL';
	}

	public function setCopyUtilityBillChargeId( $intCopyUtilityBillChargeId ) {
		$this->set( 'm_intCopyUtilityBillChargeId', CStrings::strToIntDef( $intCopyUtilityBillChargeId, NULL, false ) );
	}

	public function getCopyUtilityBillChargeId() {
		return $this->m_intCopyUtilityBillChargeId;
	}

	public function sqlCopyUtilityBillChargeId() {
		return ( true == isset( $this->m_intCopyUtilityBillChargeId ) ) ? ( string ) $this->m_intCopyUtilityBillChargeId : 'NULL';
	}

	public function setUsage( $fltUsage ) {
		$this->set( 'm_fltUsage', CStrings::strToFloatDef( $fltUsage, NULL, false, 4 ) );
	}

	public function getUsage() {
		return $this->m_fltUsage;
	}

	public function sqlUsage() {
		return ( true == isset( $this->m_fltUsage ) ) ? ( string ) $this->m_fltUsage : 'NULL';
	}

	public function setCommodityId( $intCommodityId ) {
		$this->set( 'm_intCommodityId', CStrings::strToIntDef( $intCommodityId, NULL, false ) );
	}

	public function getCommodityId() {
		return $this->m_intCommodityId;
	}

	public function sqlCommodityId() {
		return ( true == isset( $this->m_intCommodityId ) ) ? ( string ) $this->m_intCommodityId : 'NULL';
	}

	public function setIsBudgetAdjustment( $boolIsBudgetAdjustment ) {
		$this->set( 'm_boolIsBudgetAdjustment', CStrings::strToBool( $boolIsBudgetAdjustment ) );
	}

	public function getIsBudgetAdjustment() {
		return $this->m_boolIsBudgetAdjustment;
	}

	public function sqlIsBudgetAdjustment() {
		return ( true == isset( $this->m_boolIsBudgetAdjustment ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBudgetAdjustment ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, property_utility_type_id, utility_bill_id, ap_transaction_detail_id, utility_bill_template_charge_id, ap_detail_id, po_ap_detail_id, ap_code_id, unit_of_measure_id, unit_count, consumption_units, demand, charge_datetime, name, description, amount, rate, quantity, meter_start_read, meter_end_read, unit_consumption_amount, billing_start_date, billing_end_date, start_date, end_date, is_late_fee, is_current, is_base_fee, is_recovered, is_estimated, is_time_of_use, is_tax, is_late_fee_liable, updated_by, updated_on, created_by, created_on, job_phase_id, ap_contract_id, utility_bill_account_template_charge_id, copy_utility_bill_charge_id, usage, commodity_id, is_budget_adjustment )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlApTransactionDetailId() . ', ' .
						$this->sqlUtilityBillTemplateChargeId() . ', ' .
						$this->sqlApDetailId() . ', ' .
						$this->sqlPoApDetailId() . ', ' .
						$this->sqlApCodeId() . ', ' .
						$this->sqlUnitOfMeasureId() . ', ' .
						$this->sqlUnitCount() . ', ' .
						$this->sqlConsumptionUnits() . ', ' .
						$this->sqlDemand() . ', ' .
						$this->sqlChargeDatetime() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlRate() . ', ' .
						$this->sqlQuantity() . ', ' .
						$this->sqlMeterStartRead() . ', ' .
						$this->sqlMeterEndRead() . ', ' .
						$this->sqlUnitConsumptionAmount() . ', ' .
						$this->sqlBillingStartDate() . ', ' .
						$this->sqlBillingEndDate() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlIsLateFee() . ', ' .
						$this->sqlIsCurrent() . ', ' .
						$this->sqlIsBaseFee() . ', ' .
						$this->sqlIsRecovered() . ', ' .
						$this->sqlIsEstimated() . ', ' .
						$this->sqlIsTimeOfUse() . ', ' .
						$this->sqlIsTax() . ', ' .
						$this->sqlIsLateFeeLiable() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlJobPhaseId() . ', ' .
						$this->sqlApContractId() . ', ' .
						$this->sqlUtilityBillAccountTemplateChargeId() . ', ' .
						$this->sqlCopyUtilityBillChargeId() . ', ' .
						$this->sqlUsage() . ', ' .
						$this->sqlCommodityId() . ', ' .
						$this->sqlIsBudgetAdjustment() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_transaction_detail_id = ' . $this->sqlApTransactionDetailId(). ',' ; } elseif( true == array_key_exists( 'ApTransactionDetailId', $this->getChangedColumns() ) ) { $strSql .= ' ap_transaction_detail_id = ' . $this->sqlApTransactionDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_template_charge_id = ' . $this->sqlUtilityBillTemplateChargeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillTemplateChargeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_template_charge_id = ' . $this->sqlUtilityBillTemplateChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId(). ',' ; } elseif( true == array_key_exists( 'ApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' po_ap_detail_id = ' . $this->sqlPoApDetailId(). ',' ; } elseif( true == array_key_exists( 'PoApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' po_ap_detail_id = ' . $this->sqlPoApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId(). ',' ; } elseif( true == array_key_exists( 'ApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_of_measure_id = ' . $this->sqlUnitOfMeasureId(). ',' ; } elseif( true == array_key_exists( 'UnitOfMeasureId', $this->getChangedColumns() ) ) { $strSql .= ' unit_of_measure_id = ' . $this->sqlUnitOfMeasureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount(). ',' ; } elseif( true == array_key_exists( 'UnitCount', $this->getChangedColumns() ) ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumption_units = ' . $this->sqlConsumptionUnits(). ',' ; } elseif( true == array_key_exists( 'ConsumptionUnits', $this->getChangedColumns() ) ) { $strSql .= ' consumption_units = ' . $this->sqlConsumptionUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demand = ' . $this->sqlDemand(). ',' ; } elseif( true == array_key_exists( 'Demand', $this->getChangedColumns() ) ) { $strSql .= ' demand = ' . $this->sqlDemand() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_datetime = ' . $this->sqlChargeDatetime(). ',' ; } elseif( true == array_key_exists( 'ChargeDatetime', $this->getChangedColumns() ) ) { $strSql .= ' charge_datetime = ' . $this->sqlChargeDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount(). ',' ; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate = ' . $this->sqlRate(). ',' ; } elseif( true == array_key_exists( 'Rate', $this->getChangedColumns() ) ) { $strSql .= ' rate = ' . $this->sqlRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quantity = ' . $this->sqlQuantity(). ',' ; } elseif( true == array_key_exists( 'Quantity', $this->getChangedColumns() ) ) { $strSql .= ' quantity = ' . $this->sqlQuantity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_start_read = ' . $this->sqlMeterStartRead(). ',' ; } elseif( true == array_key_exists( 'MeterStartRead', $this->getChangedColumns() ) ) { $strSql .= ' meter_start_read = ' . $this->sqlMeterStartRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_end_read = ' . $this->sqlMeterEndRead(). ',' ; } elseif( true == array_key_exists( 'MeterEndRead', $this->getChangedColumns() ) ) { $strSql .= ' meter_end_read = ' . $this->sqlMeterEndRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_consumption_amount = ' . $this->sqlUnitConsumptionAmount(). ',' ; } elseif( true == array_key_exists( 'UnitConsumptionAmount', $this->getChangedColumns() ) ) { $strSql .= ' unit_consumption_amount = ' . $this->sqlUnitConsumptionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_start_date = ' . $this->sqlBillingStartDate(). ',' ; } elseif( true == array_key_exists( 'BillingStartDate', $this->getChangedColumns() ) ) { $strSql .= ' billing_start_date = ' . $this->sqlBillingStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_end_date = ' . $this->sqlBillingEndDate(). ',' ; } elseif( true == array_key_exists( 'BillingEndDate', $this->getChangedColumns() ) ) { $strSql .= ' billing_end_date = ' . $this->sqlBillingEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_late_fee = ' . $this->sqlIsLateFee(). ',' ; } elseif( true == array_key_exists( 'IsLateFee', $this->getChangedColumns() ) ) { $strSql .= ' is_late_fee = ' . $this->sqlIsLateFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_current = ' . $this->sqlIsCurrent(). ',' ; } elseif( true == array_key_exists( 'IsCurrent', $this->getChangedColumns() ) ) { $strSql .= ' is_current = ' . $this->sqlIsCurrent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_base_fee = ' . $this->sqlIsBaseFee(). ',' ; } elseif( true == array_key_exists( 'IsBaseFee', $this->getChangedColumns() ) ) { $strSql .= ' is_base_fee = ' . $this->sqlIsBaseFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recovered = ' . $this->sqlIsRecovered(). ',' ; } elseif( true == array_key_exists( 'IsRecovered', $this->getChangedColumns() ) ) { $strSql .= ' is_recovered = ' . $this->sqlIsRecovered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_estimated = ' . $this->sqlIsEstimated(). ',' ; } elseif( true == array_key_exists( 'IsEstimated', $this->getChangedColumns() ) ) { $strSql .= ' is_estimated = ' . $this->sqlIsEstimated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_time_of_use = ' . $this->sqlIsTimeOfUse(). ',' ; } elseif( true == array_key_exists( 'IsTimeOfUse', $this->getChangedColumns() ) ) { $strSql .= ' is_time_of_use = ' . $this->sqlIsTimeOfUse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_tax = ' . $this->sqlIsTax(). ',' ; } elseif( true == array_key_exists( 'IsTax', $this->getChangedColumns() ) ) { $strSql .= ' is_tax = ' . $this->sqlIsTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_late_fee_liable = ' . $this->sqlIsLateFeeLiable(). ',' ; } elseif( true == array_key_exists( 'IsLateFeeLiable', $this->getChangedColumns() ) ) { $strSql .= ' is_late_fee_liable = ' . $this->sqlIsLateFeeLiable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId(). ',' ; } elseif( true == array_key_exists( 'JobPhaseId', $this->getChangedColumns() ) ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_contract_id = ' . $this->sqlApContractId(). ',' ; } elseif( true == array_key_exists( 'ApContractId', $this->getChangedColumns() ) ) { $strSql .= ' ap_contract_id = ' . $this->sqlApContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_template_charge_id = ' . $this->sqlUtilityBillAccountTemplateChargeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountTemplateChargeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_template_charge_id = ' . $this->sqlUtilityBillAccountTemplateChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' copy_utility_bill_charge_id = ' . $this->sqlCopyUtilityBillChargeId(). ',' ; } elseif( true == array_key_exists( 'CopyUtilityBillChargeId', $this->getChangedColumns() ) ) { $strSql .= ' copy_utility_bill_charge_id = ' . $this->sqlCopyUtilityBillChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' usage = ' . $this->sqlUsage(). ',' ; } elseif( true == array_key_exists( 'Usage', $this->getChangedColumns() ) ) { $strSql .= ' usage = ' . $this->sqlUsage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commodity_id = ' . $this->sqlCommodityId(). ',' ; } elseif( true == array_key_exists( 'CommodityId', $this->getChangedColumns() ) ) { $strSql .= ' commodity_id = ' . $this->sqlCommodityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_budget_adjustment = ' . $this->sqlIsBudgetAdjustment(). ',' ; } elseif( true == array_key_exists( 'IsBudgetAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' is_budget_adjustment = ' . $this->sqlIsBudgetAdjustment() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'ap_transaction_detail_id' => $this->getApTransactionDetailId(),
			'utility_bill_template_charge_id' => $this->getUtilityBillTemplateChargeId(),
			'ap_detail_id' => $this->getApDetailId(),
			'po_ap_detail_id' => $this->getPoApDetailId(),
			'ap_code_id' => $this->getApCodeId(),
			'unit_of_measure_id' => $this->getUnitOfMeasureId(),
			'unit_count' => $this->getUnitCount(),
			'consumption_units' => $this->getConsumptionUnits(),
			'demand' => $this->getDemand(),
			'charge_datetime' => $this->getChargeDatetime(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'amount' => $this->getAmount(),
			'rate' => $this->getRate(),
			'quantity' => $this->getQuantity(),
			'meter_start_read' => $this->getMeterStartRead(),
			'meter_end_read' => $this->getMeterEndRead(),
			'unit_consumption_amount' => $this->getUnitConsumptionAmount(),
			'billing_start_date' => $this->getBillingStartDate(),
			'billing_end_date' => $this->getBillingEndDate(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'is_late_fee' => $this->getIsLateFee(),
			'is_current' => $this->getIsCurrent(),
			'is_base_fee' => $this->getIsBaseFee(),
			'is_recovered' => $this->getIsRecovered(),
			'is_estimated' => $this->getIsEstimated(),
			'is_time_of_use' => $this->getIsTimeOfUse(),
			'is_tax' => $this->getIsTax(),
			'is_late_fee_liable' => $this->getIsLateFeeLiable(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'job_phase_id' => $this->getJobPhaseId(),
			'ap_contract_id' => $this->getApContractId(),
			'utility_bill_account_template_charge_id' => $this->getUtilityBillAccountTemplateChargeId(),
			'copy_utility_bill_charge_id' => $this->getCopyUtilityBillChargeId(),
			'usage' => $this->getUsage(),
			'commodity_id' => $this->getCommodityId(),
			'is_budget_adjustment' => $this->getIsBudgetAdjustment()
		);
	}

}
?>