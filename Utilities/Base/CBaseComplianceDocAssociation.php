<?php

class CBaseComplianceDocAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.compliance_doc_associations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intComplianceDocId;
	protected $m_intComplianceLevelId;
	protected $m_intEntrataReferenceId;
	protected $m_intVendorReferenceId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['compliance_doc_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceDocId', trim( $arrValues['compliance_doc_id'] ) ); elseif( isset( $arrValues['compliance_doc_id'] ) ) $this->setComplianceDocId( $arrValues['compliance_doc_id'] );
		if( isset( $arrValues['compliance_level_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceLevelId', trim( $arrValues['compliance_level_id'] ) ); elseif( isset( $arrValues['compliance_level_id'] ) ) $this->setComplianceLevelId( $arrValues['compliance_level_id'] );
		if( isset( $arrValues['entrata_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intEntrataReferenceId', trim( $arrValues['entrata_reference_id'] ) ); elseif( isset( $arrValues['entrata_reference_id'] ) ) $this->setEntrataReferenceId( $arrValues['entrata_reference_id'] );
		if( isset( $arrValues['vendor_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorReferenceId', trim( $arrValues['vendor_reference_id'] ) ); elseif( isset( $arrValues['vendor_reference_id'] ) ) $this->setVendorReferenceId( $arrValues['vendor_reference_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setComplianceDocId( $intComplianceDocId ) {
		$this->set( 'm_intComplianceDocId', CStrings::strToIntDef( $intComplianceDocId, NULL, false ) );
	}

	public function getComplianceDocId() {
		return $this->m_intComplianceDocId;
	}

	public function sqlComplianceDocId() {
		return ( true == isset( $this->m_intComplianceDocId ) ) ? ( string ) $this->m_intComplianceDocId : 'NULL';
	}

	public function setComplianceLevelId( $intComplianceLevelId ) {
		$this->set( 'm_intComplianceLevelId', CStrings::strToIntDef( $intComplianceLevelId, NULL, false ) );
	}

	public function getComplianceLevelId() {
		return $this->m_intComplianceLevelId;
	}

	public function sqlComplianceLevelId() {
		return ( true == isset( $this->m_intComplianceLevelId ) ) ? ( string ) $this->m_intComplianceLevelId : 'NULL';
	}

	public function setEntrataReferenceId( $intEntrataReferenceId ) {
		$this->set( 'm_intEntrataReferenceId', CStrings::strToIntDef( $intEntrataReferenceId, NULL, false ) );
	}

	public function getEntrataReferenceId() {
		return $this->m_intEntrataReferenceId;
	}

	public function sqlEntrataReferenceId() {
		return ( true == isset( $this->m_intEntrataReferenceId ) ) ? ( string ) $this->m_intEntrataReferenceId : 'NULL';
	}

	public function setVendorReferenceId( $intVendorReferenceId ) {
		$this->set( 'm_intVendorReferenceId', CStrings::strToIntDef( $intVendorReferenceId, NULL, false ) );
	}

	public function getVendorReferenceId() {
		return $this->m_intVendorReferenceId;
	}

	public function sqlVendorReferenceId() {
		return ( true == isset( $this->m_intVendorReferenceId ) ) ? ( string ) $this->m_intVendorReferenceId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, compliance_doc_id, compliance_level_id, entrata_reference_id, vendor_reference_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlComplianceDocId() . ', ' .
 						$this->sqlComplianceLevelId() . ', ' .
 						$this->sqlEntrataReferenceId() . ', ' .
 						$this->sqlVendorReferenceId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_doc_id = ' . $this->sqlComplianceDocId() . ','; } elseif( true == array_key_exists( 'ComplianceDocId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_doc_id = ' . $this->sqlComplianceDocId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_level_id = ' . $this->sqlComplianceLevelId() . ','; } elseif( true == array_key_exists( 'ComplianceLevelId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_level_id = ' . $this->sqlComplianceLevelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_reference_id = ' . $this->sqlEntrataReferenceId() . ','; } elseif( true == array_key_exists( 'EntrataReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' entrata_reference_id = ' . $this->sqlEntrataReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_reference_id = ' . $this->sqlVendorReferenceId() . ','; } elseif( true == array_key_exists( 'VendorReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_reference_id = ' . $this->sqlVendorReferenceId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'compliance_doc_id' => $this->getComplianceDocId(),
			'compliance_level_id' => $this->getComplianceLevelId(),
			'entrata_reference_id' => $this->getEntrataReferenceId(),
			'vendor_reference_id' => $this->getVendorReferenceId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>