<?php

class CBasePoolPermission extends CEosSingularBase {

	const TABLE_NAME = 'public.pool_permissions';

	protected $m_intId;
	protected $m_intVendorId;
	protected $m_intPoolId;
	protected $m_intControllerId;
	protected $m_boolIsAllowed;
	protected $m_strOptions;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAllowed = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['pool_id'] ) && $boolDirectSet ) $this->set( 'm_intPoolId', trim( $arrValues['pool_id'] ) ); elseif( isset( $arrValues['pool_id'] ) ) $this->setPoolId( $arrValues['pool_id'] );
		if( isset( $arrValues['controller_id'] ) && $boolDirectSet ) $this->set( 'm_intControllerId', trim( $arrValues['controller_id'] ) ); elseif( isset( $arrValues['controller_id'] ) ) $this->setControllerId( $arrValues['controller_id'] );
		if( isset( $arrValues['is_allowed'] ) && $boolDirectSet ) $this->set( 'm_boolIsAllowed', trim( stripcslashes( $arrValues['is_allowed'] ) ) ); elseif( isset( $arrValues['is_allowed'] ) ) $this->setIsAllowed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_allowed'] ) : $arrValues['is_allowed'] );
		if( isset( $arrValues['options'] ) && $boolDirectSet ) $this->set( 'm_strOptions', trim( stripcslashes( $arrValues['options'] ) ) ); elseif( isset( $arrValues['options'] ) ) $this->setOptions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['options'] ) : $arrValues['options'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setPoolId( $intPoolId ) {
		$this->set( 'm_intPoolId', CStrings::strToIntDef( $intPoolId, NULL, false ) );
	}

	public function getPoolId() {
		return $this->m_intPoolId;
	}

	public function sqlPoolId() {
		return ( true == isset( $this->m_intPoolId ) ) ? ( string ) $this->m_intPoolId : 'NULL';
	}

	public function setControllerId( $intControllerId ) {
		$this->set( 'm_intControllerId', CStrings::strToIntDef( $intControllerId, NULL, false ) );
	}

	public function getControllerId() {
		return $this->m_intControllerId;
	}

	public function sqlControllerId() {
		return ( true == isset( $this->m_intControllerId ) ) ? ( string ) $this->m_intControllerId : 'NULL';
	}

	public function setIsAllowed( $boolIsAllowed ) {
		$this->set( 'm_boolIsAllowed', CStrings::strToBool( $boolIsAllowed ) );
	}

	public function getIsAllowed() {
		return $this->m_boolIsAllowed;
	}

	public function sqlIsAllowed() {
		return ( true == isset( $this->m_boolIsAllowed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAllowed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOptions( $strOptions ) {
		$this->set( 'm_strOptions', CStrings::strTrimDef( $strOptions, -1, NULL, true ) );
	}

	public function getOptions() {
		return $this->m_strOptions;
	}

	public function sqlOptions() {
		return ( true == isset( $this->m_strOptions ) ) ? '\'' . addslashes( $this->m_strOptions ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_id, pool_id, controller_id, is_allowed, options, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlVendorId() . ', ' .
 						$this->sqlPoolId() . ', ' .
 						$this->sqlControllerId() . ', ' .
 						$this->sqlIsAllowed() . ', ' .
 						$this->sqlOptions() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pool_id = ' . $this->sqlPoolId() . ','; } elseif( true == array_key_exists( 'PoolId', $this->getChangedColumns() ) ) { $strSql .= ' pool_id = ' . $this->sqlPoolId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' controller_id = ' . $this->sqlControllerId() . ','; } elseif( true == array_key_exists( 'ControllerId', $this->getChangedColumns() ) ) { $strSql .= ' controller_id = ' . $this->sqlControllerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_allowed = ' . $this->sqlIsAllowed() . ','; } elseif( true == array_key_exists( 'IsAllowed', $this->getChangedColumns() ) ) { $strSql .= ' is_allowed = ' . $this->sqlIsAllowed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' options = ' . $this->sqlOptions() . ','; } elseif( true == array_key_exists( 'Options', $this->getChangedColumns() ) ) { $strSql .= ' options = ' . $this->sqlOptions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_id' => $this->getVendorId(),
			'pool_id' => $this->getPoolId(),
			'controller_id' => $this->getControllerId(),
			'is_allowed' => $this->getIsAllowed(),
			'options' => $this->getOptions(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>