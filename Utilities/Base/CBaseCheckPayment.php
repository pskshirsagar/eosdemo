<?php

class CBaseCheckPayment extends CEosSingularBase {

	const TABLE_NAME = 'public.check_payments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intApPaymentId;
	protected $m_intBankAccountId;
	protected $m_intCheckPaymentDistributionId;
	protected $m_intTransactionId;
	protected $m_strCheckDatetime;
	protected $m_strDontPrintUntil;
	protected $m_strExpectedPrintDate;
	protected $m_fltAmount;
	protected $m_fltCompanyChargeAmount;
	protected $m_strAccountNumber;
	protected $m_strCheckNumber;
	protected $m_strBankName;
	protected $m_strBankRoutingNumber;
	protected $m_strBankFractionalRoutingNumber;
	protected $m_strBankAccountNumberEncrypted;
	protected $m_strPayerName;
	protected $m_strPayerAddressLine1;
	protected $m_strPayerAddressLine2;
	protected $m_strPayerCity;
	protected $m_strPayerState;
	protected $m_strPayerZip;
	protected $m_strSignerName;
	protected $m_strSignerImage;
	protected $m_strPayeeName;
	protected $m_strPayeeAddressLine1;
	protected $m_strPayeeAddressLine2;
	protected $m_strPayeeCity;
	protected $m_strPayeeState;
	protected $m_strPayeeZip;
	protected $m_strMemo;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltCompanyChargeAmount = '0.0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intApPaymentId', trim( $arrValues['ap_payment_id'] ) ); elseif( isset( $arrValues['ap_payment_id'] ) ) $this->setApPaymentId( $arrValues['ap_payment_id'] );
		if( isset( $arrValues['bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountId', trim( $arrValues['bank_account_id'] ) ); elseif( isset( $arrValues['bank_account_id'] ) ) $this->setBankAccountId( $arrValues['bank_account_id'] );
		if( isset( $arrValues['check_payment_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckPaymentDistributionId', trim( $arrValues['check_payment_distribution_id'] ) ); elseif( isset( $arrValues['check_payment_distribution_id'] ) ) $this->setCheckPaymentDistributionId( $arrValues['check_payment_distribution_id'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['check_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCheckDatetime', trim( $arrValues['check_datetime'] ) ); elseif( isset( $arrValues['check_datetime'] ) ) $this->setCheckDatetime( $arrValues['check_datetime'] );
		if( isset( $arrValues['dont_print_until'] ) && $boolDirectSet ) $this->set( 'm_strDontPrintUntil', trim( $arrValues['dont_print_until'] ) ); elseif( isset( $arrValues['dont_print_until'] ) ) $this->setDontPrintUntil( $arrValues['dont_print_until'] );
		if( isset( $arrValues['expected_print_date'] ) && $boolDirectSet ) $this->set( 'm_strExpectedPrintDate', trim( $arrValues['expected_print_date'] ) ); elseif( isset( $arrValues['expected_print_date'] ) ) $this->setExpectedPrintDate( $arrValues['expected_print_date'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['company_charge_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCompanyChargeAmount', trim( $arrValues['company_charge_amount'] ) ); elseif( isset( $arrValues['company_charge_amount'] ) ) $this->setCompanyChargeAmount( $arrValues['company_charge_amount'] );
		if( isset( $arrValues['account_number'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumber', trim( stripcslashes( $arrValues['account_number'] ) ) ); elseif( isset( $arrValues['account_number'] ) ) $this->setAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number'] ) : $arrValues['account_number'] );
		if( isset( $arrValues['check_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckNumber', trim( stripcslashes( $arrValues['check_number'] ) ) ); elseif( isset( $arrValues['check_number'] ) ) $this->setCheckNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_number'] ) : $arrValues['check_number'] );
		if( isset( $arrValues['bank_name'] ) && $boolDirectSet ) $this->set( 'm_strBankName', trim( stripcslashes( $arrValues['bank_name'] ) ) ); elseif( isset( $arrValues['bank_name'] ) ) $this->setBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_name'] ) : $arrValues['bank_name'] );
		if( isset( $arrValues['bank_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strBankRoutingNumber', trim( stripcslashes( $arrValues['bank_routing_number'] ) ) ); elseif( isset( $arrValues['bank_routing_number'] ) ) $this->setBankRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_routing_number'] ) : $arrValues['bank_routing_number'] );
		if( isset( $arrValues['bank_fractional_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strBankFractionalRoutingNumber', trim( stripcslashes( $arrValues['bank_fractional_routing_number'] ) ) ); elseif( isset( $arrValues['bank_fractional_routing_number'] ) ) $this->setBankFractionalRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_fractional_routing_number'] ) : $arrValues['bank_fractional_routing_number'] );
		if( isset( $arrValues['bank_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBankAccountNumberEncrypted', trim( stripcslashes( $arrValues['bank_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['bank_account_number_encrypted'] ) ) $this->setBankAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_account_number_encrypted'] ) : $arrValues['bank_account_number_encrypted'] );
		if( isset( $arrValues['payer_name'] ) && $boolDirectSet ) $this->set( 'm_strPayerName', trim( stripcslashes( $arrValues['payer_name'] ) ) ); elseif( isset( $arrValues['payer_name'] ) ) $this->setPayerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payer_name'] ) : $arrValues['payer_name'] );
		if( isset( $arrValues['payer_address_line1'] ) && $boolDirectSet ) $this->set( 'm_strPayerAddressLine1', trim( stripcslashes( $arrValues['payer_address_line1'] ) ) ); elseif( isset( $arrValues['payer_address_line1'] ) ) $this->setPayerAddressLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payer_address_line1'] ) : $arrValues['payer_address_line1'] );
		if( isset( $arrValues['payer_address_line2'] ) && $boolDirectSet ) $this->set( 'm_strPayerAddressLine2', trim( stripcslashes( $arrValues['payer_address_line2'] ) ) ); elseif( isset( $arrValues['payer_address_line2'] ) ) $this->setPayerAddressLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payer_address_line2'] ) : $arrValues['payer_address_line2'] );
		if( isset( $arrValues['payer_city'] ) && $boolDirectSet ) $this->set( 'm_strPayerCity', trim( stripcslashes( $arrValues['payer_city'] ) ) ); elseif( isset( $arrValues['payer_city'] ) ) $this->setPayerCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payer_city'] ) : $arrValues['payer_city'] );
		if( isset( $arrValues['payer_state'] ) && $boolDirectSet ) $this->set( 'm_strPayerState', trim( stripcslashes( $arrValues['payer_state'] ) ) ); elseif( isset( $arrValues['payer_state'] ) ) $this->setPayerState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payer_state'] ) : $arrValues['payer_state'] );
		if( isset( $arrValues['payer_zip'] ) && $boolDirectSet ) $this->set( 'm_strPayerZip', trim( stripcslashes( $arrValues['payer_zip'] ) ) ); elseif( isset( $arrValues['payer_zip'] ) ) $this->setPayerZip( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payer_zip'] ) : $arrValues['payer_zip'] );
		if( isset( $arrValues['signer_name'] ) && $boolDirectSet ) $this->set( 'm_strSignerName', trim( stripcslashes( $arrValues['signer_name'] ) ) ); elseif( isset( $arrValues['signer_name'] ) ) $this->setSignerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['signer_name'] ) : $arrValues['signer_name'] );
		if( isset( $arrValues['signer_image'] ) && $boolDirectSet ) $this->set( 'm_strSignerImage', trim( stripcslashes( $arrValues['signer_image'] ) ) ); elseif( isset( $arrValues['signer_image'] ) ) $this->setSignerImage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['signer_image'] ) : $arrValues['signer_image'] );
		if( isset( $arrValues['payee_name'] ) && $boolDirectSet ) $this->set( 'm_strPayeeName', trim( stripcslashes( $arrValues['payee_name'] ) ) ); elseif( isset( $arrValues['payee_name'] ) ) $this->setPayeeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payee_name'] ) : $arrValues['payee_name'] );
		if( isset( $arrValues['payee_address_line1'] ) && $boolDirectSet ) $this->set( 'm_strPayeeAddressLine1', trim( stripcslashes( $arrValues['payee_address_line1'] ) ) ); elseif( isset( $arrValues['payee_address_line1'] ) ) $this->setPayeeAddressLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payee_address_line1'] ) : $arrValues['payee_address_line1'] );
		if( isset( $arrValues['payee_address_line2'] ) && $boolDirectSet ) $this->set( 'm_strPayeeAddressLine2', trim( stripcslashes( $arrValues['payee_address_line2'] ) ) ); elseif( isset( $arrValues['payee_address_line2'] ) ) $this->setPayeeAddressLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payee_address_line2'] ) : $arrValues['payee_address_line2'] );
		if( isset( $arrValues['payee_city'] ) && $boolDirectSet ) $this->set( 'm_strPayeeCity', trim( stripcslashes( $arrValues['payee_city'] ) ) ); elseif( isset( $arrValues['payee_city'] ) ) $this->setPayeeCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payee_city'] ) : $arrValues['payee_city'] );
		if( isset( $arrValues['payee_state'] ) && $boolDirectSet ) $this->set( 'm_strPayeeState', trim( stripcslashes( $arrValues['payee_state'] ) ) ); elseif( isset( $arrValues['payee_state'] ) ) $this->setPayeeState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payee_state'] ) : $arrValues['payee_state'] );
		if( isset( $arrValues['payee_zip'] ) && $boolDirectSet ) $this->set( 'm_strPayeeZip', trim( stripcslashes( $arrValues['payee_zip'] ) ) ); elseif( isset( $arrValues['payee_zip'] ) ) $this->setPayeeZip( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payee_zip'] ) : $arrValues['payee_zip'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPaymentId( $intApPaymentId ) {
		$this->set( 'm_intApPaymentId', CStrings::strToIntDef( $intApPaymentId, NULL, false ) );
	}

	public function getApPaymentId() {
		return $this->m_intApPaymentId;
	}

	public function sqlApPaymentId() {
		return ( true == isset( $this->m_intApPaymentId ) ) ? ( string ) $this->m_intApPaymentId : 'NULL';
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->set( 'm_intBankAccountId', CStrings::strToIntDef( $intBankAccountId, NULL, false ) );
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function sqlBankAccountId() {
		return ( true == isset( $this->m_intBankAccountId ) ) ? ( string ) $this->m_intBankAccountId : 'NULL';
	}

	public function setCheckPaymentDistributionId( $intCheckPaymentDistributionId ) {
		$this->set( 'm_intCheckPaymentDistributionId', CStrings::strToIntDef( $intCheckPaymentDistributionId, NULL, false ) );
	}

	public function getCheckPaymentDistributionId() {
		return $this->m_intCheckPaymentDistributionId;
	}

	public function sqlCheckPaymentDistributionId() {
		return ( true == isset( $this->m_intCheckPaymentDistributionId ) ) ? ( string ) $this->m_intCheckPaymentDistributionId : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setCheckDatetime( $strCheckDatetime ) {
		$this->set( 'm_strCheckDatetime', CStrings::strTrimDef( $strCheckDatetime, -1, NULL, true ) );
	}

	public function getCheckDatetime() {
		return $this->m_strCheckDatetime;
	}

	public function sqlCheckDatetime() {
		return ( true == isset( $this->m_strCheckDatetime ) ) ? '\'' . $this->m_strCheckDatetime . '\'' : 'NULL';
	}

	public function setDontPrintUntil( $strDontPrintUntil ) {
		$this->set( 'm_strDontPrintUntil', CStrings::strTrimDef( $strDontPrintUntil, -1, NULL, true ) );
	}

	public function getDontPrintUntil() {
		return $this->m_strDontPrintUntil;
	}

	public function sqlDontPrintUntil() {
		return ( true == isset( $this->m_strDontPrintUntil ) ) ? '\'' . $this->m_strDontPrintUntil . '\'' : 'NULL';
	}

	public function setExpectedPrintDate( $strExpectedPrintDate ) {
		$this->set( 'm_strExpectedPrintDate', CStrings::strTrimDef( $strExpectedPrintDate, -1, NULL, true ) );
	}

	public function getExpectedPrintDate() {
		return $this->m_strExpectedPrintDate;
	}

	public function sqlExpectedPrintDate() {
		return ( true == isset( $this->m_strExpectedPrintDate ) ) ? '\'' . $this->m_strExpectedPrintDate . '\'' : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 2 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : 'NULL';
	}

	public function setCompanyChargeAmount( $fltCompanyChargeAmount ) {
		$this->set( 'm_fltCompanyChargeAmount', CStrings::strToFloatDef( $fltCompanyChargeAmount, NULL, false, 2 ) );
	}

	public function getCompanyChargeAmount() {
		return $this->m_fltCompanyChargeAmount;
	}

	public function sqlCompanyChargeAmount() {
		return ( true == isset( $this->m_fltCompanyChargeAmount ) ) ? ( string ) $this->m_fltCompanyChargeAmount : '0.0';
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 50, NULL, true ) );
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function sqlAccountNumber() {
		return ( true == isset( $this->m_strAccountNumber ) ) ? '\'' . addslashes( $this->m_strAccountNumber ) . '\'' : 'NULL';
	}

	public function setCheckNumber( $strCheckNumber ) {
		$this->set( 'm_strCheckNumber', CStrings::strTrimDef( $strCheckNumber, 100, NULL, true ) );
	}

	public function getCheckNumber() {
		return $this->m_strCheckNumber;
	}

	public function sqlCheckNumber() {
		return ( true == isset( $this->m_strCheckNumber ) ) ? '\'' . addslashes( $this->m_strCheckNumber ) . '\'' : 'NULL';
	}

	public function setBankName( $strBankName ) {
		$this->set( 'm_strBankName', CStrings::strTrimDef( $strBankName, 50, NULL, true ) );
	}

	public function getBankName() {
		return $this->m_strBankName;
	}

	public function sqlBankName() {
		return ( true == isset( $this->m_strBankName ) ) ? '\'' . addslashes( $this->m_strBankName ) . '\'' : 'NULL';
	}

	public function setBankRoutingNumber( $strBankRoutingNumber ) {
		$this->set( 'm_strBankRoutingNumber', CStrings::strTrimDef( $strBankRoutingNumber, 100, NULL, true ) );
	}

	public function getBankRoutingNumber() {
		return $this->m_strBankRoutingNumber;
	}

	public function sqlBankRoutingNumber() {
		return ( true == isset( $this->m_strBankRoutingNumber ) ) ? '\'' . addslashes( $this->m_strBankRoutingNumber ) . '\'' : 'NULL';
	}

	public function setBankFractionalRoutingNumber( $strBankFractionalRoutingNumber ) {
		$this->set( 'm_strBankFractionalRoutingNumber', CStrings::strTrimDef( $strBankFractionalRoutingNumber, 100, NULL, true ) );
	}

	public function getBankFractionalRoutingNumber() {
		return $this->m_strBankFractionalRoutingNumber;
	}

	public function sqlBankFractionalRoutingNumber() {
		return ( true == isset( $this->m_strBankFractionalRoutingNumber ) ) ? '\'' . addslashes( $this->m_strBankFractionalRoutingNumber ) . '\'' : 'NULL';
	}

	public function setBankAccountNumberEncrypted( $strBankAccountNumberEncrypted ) {
		$this->set( 'm_strBankAccountNumberEncrypted', CStrings::strTrimDef( $strBankAccountNumberEncrypted, 100, NULL, true ) );
	}

	public function getBankAccountNumberEncrypted() {
		return $this->m_strBankAccountNumberEncrypted;
	}

	public function sqlBankAccountNumberEncrypted() {
		return ( true == isset( $this->m_strBankAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strBankAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setPayerName( $strPayerName ) {
		$this->set( 'm_strPayerName', CStrings::strTrimDef( $strPayerName, 100, NULL, true ) );
	}

	public function getPayerName() {
		return $this->m_strPayerName;
	}

	public function sqlPayerName() {
		return ( true == isset( $this->m_strPayerName ) ) ? '\'' . addslashes( $this->m_strPayerName ) . '\'' : 'NULL';
	}

	public function setPayerAddressLine1( $strPayerAddressLine1 ) {
		$this->set( 'm_strPayerAddressLine1', CStrings::strTrimDef( $strPayerAddressLine1, 100, NULL, true ) );
	}

	public function getPayerAddressLine1() {
		return $this->m_strPayerAddressLine1;
	}

	public function sqlPayerAddressLine1() {
		return ( true == isset( $this->m_strPayerAddressLine1 ) ) ? '\'' . addslashes( $this->m_strPayerAddressLine1 ) . '\'' : 'NULL';
	}

	public function setPayerAddressLine2( $strPayerAddressLine2 ) {
		$this->set( 'm_strPayerAddressLine2', CStrings::strTrimDef( $strPayerAddressLine2, 100, NULL, true ) );
	}

	public function getPayerAddressLine2() {
		return $this->m_strPayerAddressLine2;
	}

	public function sqlPayerAddressLine2() {
		return ( true == isset( $this->m_strPayerAddressLine2 ) ) ? '\'' . addslashes( $this->m_strPayerAddressLine2 ) . '\'' : 'NULL';
	}

	public function setPayerCity( $strPayerCity ) {
		$this->set( 'm_strPayerCity', CStrings::strTrimDef( $strPayerCity, 100, NULL, true ) );
	}

	public function getPayerCity() {
		return $this->m_strPayerCity;
	}

	public function sqlPayerCity() {
		return ( true == isset( $this->m_strPayerCity ) ) ? '\'' . addslashes( $this->m_strPayerCity ) . '\'' : 'NULL';
	}

	public function setPayerState( $strPayerState ) {
		$this->set( 'm_strPayerState', CStrings::strTrimDef( $strPayerState, 100, NULL, true ) );
	}

	public function getPayerState() {
		return $this->m_strPayerState;
	}

	public function sqlPayerState() {
		return ( true == isset( $this->m_strPayerState ) ) ? '\'' . addslashes( $this->m_strPayerState ) . '\'' : 'NULL';
	}

	public function setPayerZip( $strPayerZip ) {
		$this->set( 'm_strPayerZip', CStrings::strTrimDef( $strPayerZip, 100, NULL, true ) );
	}

	public function getPayerZip() {
		return $this->m_strPayerZip;
	}

	public function sqlPayerZip() {
		return ( true == isset( $this->m_strPayerZip ) ) ? '\'' . addslashes( $this->m_strPayerZip ) . '\'' : 'NULL';
	}

	public function setSignerName( $strSignerName ) {
		$this->set( 'm_strSignerName', CStrings::strTrimDef( $strSignerName, 100, NULL, true ) );
	}

	public function getSignerName() {
		return $this->m_strSignerName;
	}

	public function sqlSignerName() {
		return ( true == isset( $this->m_strSignerName ) ) ? '\'' . addslashes( $this->m_strSignerName ) . '\'' : 'NULL';
	}

	public function setSignerImage( $strSignerImage ) {
		$this->set( 'm_strSignerImage', CStrings::strTrimDef( $strSignerImage, 100, NULL, true ) );
	}

	public function getSignerImage() {
		return $this->m_strSignerImage;
	}

	public function sqlSignerImage() {
		return ( true == isset( $this->m_strSignerImage ) ) ? '\'' . addslashes( $this->m_strSignerImage ) . '\'' : 'NULL';
	}

	public function setPayeeName( $strPayeeName ) {
		$this->set( 'm_strPayeeName', CStrings::strTrimDef( $strPayeeName, 100, NULL, true ) );
	}

	public function getPayeeName() {
		return $this->m_strPayeeName;
	}

	public function sqlPayeeName() {
		return ( true == isset( $this->m_strPayeeName ) ) ? '\'' . addslashes( $this->m_strPayeeName ) . '\'' : 'NULL';
	}

	public function setPayeeAddressLine1( $strPayeeAddressLine1 ) {
		$this->set( 'm_strPayeeAddressLine1', CStrings::strTrimDef( $strPayeeAddressLine1, 100, NULL, true ) );
	}

	public function getPayeeAddressLine1() {
		return $this->m_strPayeeAddressLine1;
	}

	public function sqlPayeeAddressLine1() {
		return ( true == isset( $this->m_strPayeeAddressLine1 ) ) ? '\'' . addslashes( $this->m_strPayeeAddressLine1 ) . '\'' : 'NULL';
	}

	public function setPayeeAddressLine2( $strPayeeAddressLine2 ) {
		$this->set( 'm_strPayeeAddressLine2', CStrings::strTrimDef( $strPayeeAddressLine2, 100, NULL, true ) );
	}

	public function getPayeeAddressLine2() {
		return $this->m_strPayeeAddressLine2;
	}

	public function sqlPayeeAddressLine2() {
		return ( true == isset( $this->m_strPayeeAddressLine2 ) ) ? '\'' . addslashes( $this->m_strPayeeAddressLine2 ) . '\'' : 'NULL';
	}

	public function setPayeeCity( $strPayeeCity ) {
		$this->set( 'm_strPayeeCity', CStrings::strTrimDef( $strPayeeCity, 100, NULL, true ) );
	}

	public function getPayeeCity() {
		return $this->m_strPayeeCity;
	}

	public function sqlPayeeCity() {
		return ( true == isset( $this->m_strPayeeCity ) ) ? '\'' . addslashes( $this->m_strPayeeCity ) . '\'' : 'NULL';
	}

	public function setPayeeState( $strPayeeState ) {
		$this->set( 'm_strPayeeState', CStrings::strTrimDef( $strPayeeState, 100, NULL, true ) );
	}

	public function getPayeeState() {
		return $this->m_strPayeeState;
	}

	public function sqlPayeeState() {
		return ( true == isset( $this->m_strPayeeState ) ) ? '\'' . addslashes( $this->m_strPayeeState ) . '\'' : 'NULL';
	}

	public function setPayeeZip( $strPayeeZip ) {
		$this->set( 'm_strPayeeZip', CStrings::strTrimDef( $strPayeeZip, 100, NULL, true ) );
	}

	public function getPayeeZip() {
		return $this->m_strPayeeZip;
	}

	public function sqlPayeeZip() {
		return ( true == isset( $this->m_strPayeeZip ) ) ? '\'' . addslashes( $this->m_strPayeeZip ) . '\'' : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, 200, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, ap_payment_id, bank_account_id, check_payment_distribution_id, transaction_id, check_datetime, dont_print_until, expected_print_date, amount, company_charge_amount, account_number, check_number, bank_name, bank_routing_number, bank_fractional_routing_number, bank_account_number_encrypted, payer_name, payer_address_line1, payer_address_line2, payer_city, payer_state, payer_zip, signer_name, signer_image, payee_name, payee_address_line1, payee_address_line2, payee_city, payee_state, payee_zip, memo, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlApPaymentId() . ', ' .
 						$this->sqlBankAccountId() . ', ' .
 						$this->sqlCheckPaymentDistributionId() . ', ' .
 						$this->sqlTransactionId() . ', ' .
 						$this->sqlCheckDatetime() . ', ' .
 						$this->sqlDontPrintUntil() . ', ' .
 						$this->sqlExpectedPrintDate() . ', ' .
 						$this->sqlAmount() . ', ' .
 						$this->sqlCompanyChargeAmount() . ', ' .
 						$this->sqlAccountNumber() . ', ' .
 						$this->sqlCheckNumber() . ', ' .
 						$this->sqlBankName() . ', ' .
 						$this->sqlBankRoutingNumber() . ', ' .
 						$this->sqlBankFractionalRoutingNumber() . ', ' .
 						$this->sqlBankAccountNumberEncrypted() . ', ' .
 						$this->sqlPayerName() . ', ' .
 						$this->sqlPayerAddressLine1() . ', ' .
 						$this->sqlPayerAddressLine2() . ', ' .
 						$this->sqlPayerCity() . ', ' .
 						$this->sqlPayerState() . ', ' .
 						$this->sqlPayerZip() . ', ' .
 						$this->sqlSignerName() . ', ' .
 						$this->sqlSignerImage() . ', ' .
 						$this->sqlPayeeName() . ', ' .
 						$this->sqlPayeeAddressLine1() . ', ' .
 						$this->sqlPayeeAddressLine2() . ', ' .
 						$this->sqlPayeeCity() . ', ' .
 						$this->sqlPayeeState() . ', ' .
 						$this->sqlPayeeZip() . ', ' .
 						$this->sqlMemo() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId() . ','; } elseif( true == array_key_exists( 'ApPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; } elseif( true == array_key_exists( 'BankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_payment_distribution_id = ' . $this->sqlCheckPaymentDistributionId() . ','; } elseif( true == array_key_exists( 'CheckPaymentDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' check_payment_distribution_id = ' . $this->sqlCheckPaymentDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_datetime = ' . $this->sqlCheckDatetime() . ','; } elseif( true == array_key_exists( 'CheckDatetime', $this->getChangedColumns() ) ) { $strSql .= ' check_datetime = ' . $this->sqlCheckDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_print_until = ' . $this->sqlDontPrintUntil() . ','; } elseif( true == array_key_exists( 'DontPrintUntil', $this->getChangedColumns() ) ) { $strSql .= ' dont_print_until = ' . $this->sqlDontPrintUntil() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_print_date = ' . $this->sqlExpectedPrintDate() . ','; } elseif( true == array_key_exists( 'ExpectedPrintDate', $this->getChangedColumns() ) ) { $strSql .= ' expected_print_date = ' . $this->sqlExpectedPrintDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_charge_amount = ' . $this->sqlCompanyChargeAmount() . ','; } elseif( true == array_key_exists( 'CompanyChargeAmount', $this->getChangedColumns() ) ) { $strSql .= ' company_charge_amount = ' . $this->sqlCompanyChargeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; } elseif( true == array_key_exists( 'AccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_number = ' . $this->sqlCheckNumber() . ','; } elseif( true == array_key_exists( 'CheckNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_number = ' . $this->sqlCheckNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_name = ' . $this->sqlBankName() . ','; } elseif( true == array_key_exists( 'BankName', $this->getChangedColumns() ) ) { $strSql .= ' bank_name = ' . $this->sqlBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_routing_number = ' . $this->sqlBankRoutingNumber() . ','; } elseif( true == array_key_exists( 'BankRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' bank_routing_number = ' . $this->sqlBankRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_fractional_routing_number = ' . $this->sqlBankFractionalRoutingNumber() . ','; } elseif( true == array_key_exists( 'BankFractionalRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' bank_fractional_routing_number = ' . $this->sqlBankFractionalRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_number_encrypted = ' . $this->sqlBankAccountNumberEncrypted() . ','; } elseif( true == array_key_exists( 'BankAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_number_encrypted = ' . $this->sqlBankAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_name = ' . $this->sqlPayerName() . ','; } elseif( true == array_key_exists( 'PayerName', $this->getChangedColumns() ) ) { $strSql .= ' payer_name = ' . $this->sqlPayerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_address_line1 = ' . $this->sqlPayerAddressLine1() . ','; } elseif( true == array_key_exists( 'PayerAddressLine1', $this->getChangedColumns() ) ) { $strSql .= ' payer_address_line1 = ' . $this->sqlPayerAddressLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_address_line2 = ' . $this->sqlPayerAddressLine2() . ','; } elseif( true == array_key_exists( 'PayerAddressLine2', $this->getChangedColumns() ) ) { $strSql .= ' payer_address_line2 = ' . $this->sqlPayerAddressLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_city = ' . $this->sqlPayerCity() . ','; } elseif( true == array_key_exists( 'PayerCity', $this->getChangedColumns() ) ) { $strSql .= ' payer_city = ' . $this->sqlPayerCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_state = ' . $this->sqlPayerState() . ','; } elseif( true == array_key_exists( 'PayerState', $this->getChangedColumns() ) ) { $strSql .= ' payer_state = ' . $this->sqlPayerState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_zip = ' . $this->sqlPayerZip() . ','; } elseif( true == array_key_exists( 'PayerZip', $this->getChangedColumns() ) ) { $strSql .= ' payer_zip = ' . $this->sqlPayerZip() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signer_name = ' . $this->sqlSignerName() . ','; } elseif( true == array_key_exists( 'SignerName', $this->getChangedColumns() ) ) { $strSql .= ' signer_name = ' . $this->sqlSignerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signer_image = ' . $this->sqlSignerImage() . ','; } elseif( true == array_key_exists( 'SignerImage', $this->getChangedColumns() ) ) { $strSql .= ' signer_image = ' . $this->sqlSignerImage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payee_name = ' . $this->sqlPayeeName() . ','; } elseif( true == array_key_exists( 'PayeeName', $this->getChangedColumns() ) ) { $strSql .= ' payee_name = ' . $this->sqlPayeeName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payee_address_line1 = ' . $this->sqlPayeeAddressLine1() . ','; } elseif( true == array_key_exists( 'PayeeAddressLine1', $this->getChangedColumns() ) ) { $strSql .= ' payee_address_line1 = ' . $this->sqlPayeeAddressLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payee_address_line2 = ' . $this->sqlPayeeAddressLine2() . ','; } elseif( true == array_key_exists( 'PayeeAddressLine2', $this->getChangedColumns() ) ) { $strSql .= ' payee_address_line2 = ' . $this->sqlPayeeAddressLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payee_city = ' . $this->sqlPayeeCity() . ','; } elseif( true == array_key_exists( 'PayeeCity', $this->getChangedColumns() ) ) { $strSql .= ' payee_city = ' . $this->sqlPayeeCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payee_state = ' . $this->sqlPayeeState() . ','; } elseif( true == array_key_exists( 'PayeeState', $this->getChangedColumns() ) ) { $strSql .= ' payee_state = ' . $this->sqlPayeeState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payee_zip = ' . $this->sqlPayeeZip() . ','; } elseif( true == array_key_exists( 'PayeeZip', $this->getChangedColumns() ) ) { $strSql .= ' payee_zip = ' . $this->sqlPayeeZip() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payment_id' => $this->getApPaymentId(),
			'bank_account_id' => $this->getBankAccountId(),
			'check_payment_distribution_id' => $this->getCheckPaymentDistributionId(),
			'transaction_id' => $this->getTransactionId(),
			'check_datetime' => $this->getCheckDatetime(),
			'dont_print_until' => $this->getDontPrintUntil(),
			'expected_print_date' => $this->getExpectedPrintDate(),
			'amount' => $this->getAmount(),
			'company_charge_amount' => $this->getCompanyChargeAmount(),
			'account_number' => $this->getAccountNumber(),
			'check_number' => $this->getCheckNumber(),
			'bank_name' => $this->getBankName(),
			'bank_routing_number' => $this->getBankRoutingNumber(),
			'bank_fractional_routing_number' => $this->getBankFractionalRoutingNumber(),
			'bank_account_number_encrypted' => $this->getBankAccountNumberEncrypted(),
			'payer_name' => $this->getPayerName(),
			'payer_address_line1' => $this->getPayerAddressLine1(),
			'payer_address_line2' => $this->getPayerAddressLine2(),
			'payer_city' => $this->getPayerCity(),
			'payer_state' => $this->getPayerState(),
			'payer_zip' => $this->getPayerZip(),
			'signer_name' => $this->getSignerName(),
			'signer_image' => $this->getSignerImage(),
			'payee_name' => $this->getPayeeName(),
			'payee_address_line1' => $this->getPayeeAddressLine1(),
			'payee_address_line2' => $this->getPayeeAddressLine2(),
			'payee_city' => $this->getPayeeCity(),
			'payee_state' => $this->getPayeeState(),
			'payee_zip' => $this->getPayeeZip(),
			'memo' => $this->getMemo(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>