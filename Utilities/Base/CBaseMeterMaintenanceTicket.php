<?php

class CBaseMeterMaintenanceTicket extends CEosSingularBase {

	const TABLE_NAME = 'public.meter_maintenance_tickets';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intUtilityAccountId;
	protected $m_intMeterMaintenanceTypeId;
	protected $m_intPropertyMeterDeviceId;
	protected $m_intCompanyTransctionId;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strWorkPlan;
	protected $m_strTicketDatetime;
	protected $m_strScheduledCompletionDate;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_boolIsProposal;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intBilledBy;
	protected $m_strBilledOn;
	protected $m_intClosedBy;
	protected $m_strClosedOn;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsProposal = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['meter_maintenance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMeterMaintenanceTypeId', trim( $arrValues['meter_maintenance_type_id'] ) ); elseif( isset( $arrValues['meter_maintenance_type_id'] ) ) $this->setMeterMaintenanceTypeId( $arrValues['meter_maintenance_type_id'] );
		if( isset( $arrValues['property_meter_device_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyMeterDeviceId', trim( $arrValues['property_meter_device_id'] ) ); elseif( isset( $arrValues['property_meter_device_id'] ) ) $this->setPropertyMeterDeviceId( $arrValues['property_meter_device_id'] );
		if( isset( $arrValues['company_transction_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyTransctionId', trim( $arrValues['company_transction_id'] ) ); elseif( isset( $arrValues['company_transction_id'] ) ) $this->setCompanyTransctionId( $arrValues['company_transction_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['work_plan'] ) && $boolDirectSet ) $this->set( 'm_strWorkPlan', trim( stripcslashes( $arrValues['work_plan'] ) ) ); elseif( isset( $arrValues['work_plan'] ) ) $this->setWorkPlan( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['work_plan'] ) : $arrValues['work_plan'] );
		if( isset( $arrValues['ticket_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTicketDatetime', trim( $arrValues['ticket_datetime'] ) ); elseif( isset( $arrValues['ticket_datetime'] ) ) $this->setTicketDatetime( $arrValues['ticket_datetime'] );
		if( isset( $arrValues['scheduled_completion_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledCompletionDate', trim( $arrValues['scheduled_completion_date'] ) ); elseif( isset( $arrValues['scheduled_completion_date'] ) ) $this->setScheduledCompletionDate( $arrValues['scheduled_completion_date'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['is_proposal'] ) && $boolDirectSet ) $this->set( 'm_boolIsProposal', trim( stripcslashes( $arrValues['is_proposal'] ) ) ); elseif( isset( $arrValues['is_proposal'] ) ) $this->setIsProposal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_proposal'] ) : $arrValues['is_proposal'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['billed_by'] ) && $boolDirectSet ) $this->set( 'm_intBilledBy', trim( $arrValues['billed_by'] ) ); elseif( isset( $arrValues['billed_by'] ) ) $this->setBilledBy( $arrValues['billed_by'] );
		if( isset( $arrValues['billed_on'] ) && $boolDirectSet ) $this->set( 'm_strBilledOn', trim( $arrValues['billed_on'] ) ); elseif( isset( $arrValues['billed_on'] ) ) $this->setBilledOn( $arrValues['billed_on'] );
		if( isset( $arrValues['closed_by'] ) && $boolDirectSet ) $this->set( 'm_intClosedBy', trim( $arrValues['closed_by'] ) ); elseif( isset( $arrValues['closed_by'] ) ) $this->setClosedBy( $arrValues['closed_by'] );
		if( isset( $arrValues['closed_on'] ) && $boolDirectSet ) $this->set( 'm_strClosedOn', trim( $arrValues['closed_on'] ) ); elseif( isset( $arrValues['closed_on'] ) ) $this->setClosedOn( $arrValues['closed_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setMeterMaintenanceTypeId( $intMeterMaintenanceTypeId ) {
		$this->set( 'm_intMeterMaintenanceTypeId', CStrings::strToIntDef( $intMeterMaintenanceTypeId, NULL, false ) );
	}

	public function getMeterMaintenanceTypeId() {
		return $this->m_intMeterMaintenanceTypeId;
	}

	public function sqlMeterMaintenanceTypeId() {
		return ( true == isset( $this->m_intMeterMaintenanceTypeId ) ) ? ( string ) $this->m_intMeterMaintenanceTypeId : 'NULL';
	}

	public function setPropertyMeterDeviceId( $intPropertyMeterDeviceId ) {
		$this->set( 'm_intPropertyMeterDeviceId', CStrings::strToIntDef( $intPropertyMeterDeviceId, NULL, false ) );
	}

	public function getPropertyMeterDeviceId() {
		return $this->m_intPropertyMeterDeviceId;
	}

	public function sqlPropertyMeterDeviceId() {
		return ( true == isset( $this->m_intPropertyMeterDeviceId ) ) ? ( string ) $this->m_intPropertyMeterDeviceId : 'NULL';
	}

	public function setCompanyTransctionId( $intCompanyTransctionId ) {
		$this->set( 'm_intCompanyTransctionId', CStrings::strToIntDef( $intCompanyTransctionId, NULL, false ) );
	}

	public function getCompanyTransctionId() {
		return $this->m_intCompanyTransctionId;
	}

	public function sqlCompanyTransctionId() {
		return ( true == isset( $this->m_intCompanyTransctionId ) ) ? ( string ) $this->m_intCompanyTransctionId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setWorkPlan( $strWorkPlan ) {
		$this->set( 'm_strWorkPlan', CStrings::strTrimDef( $strWorkPlan, 240, NULL, true ) );
	}

	public function getWorkPlan() {
		return $this->m_strWorkPlan;
	}

	public function sqlWorkPlan() {
		return ( true == isset( $this->m_strWorkPlan ) ) ? '\'' . addslashes( $this->m_strWorkPlan ) . '\'' : 'NULL';
	}

	public function setTicketDatetime( $strTicketDatetime ) {
		$this->set( 'm_strTicketDatetime', CStrings::strTrimDef( $strTicketDatetime, -1, NULL, true ) );
	}

	public function getTicketDatetime() {
		return $this->m_strTicketDatetime;
	}

	public function sqlTicketDatetime() {
		return ( true == isset( $this->m_strTicketDatetime ) ) ? '\'' . $this->m_strTicketDatetime . '\'' : 'NOW()';
	}

	public function setScheduledCompletionDate( $strScheduledCompletionDate ) {
		$this->set( 'm_strScheduledCompletionDate', CStrings::strTrimDef( $strScheduledCompletionDate, -1, NULL, true ) );
	}

	public function getScheduledCompletionDate() {
		return $this->m_strScheduledCompletionDate;
	}

	public function sqlScheduledCompletionDate() {
		return ( true == isset( $this->m_strScheduledCompletionDate ) ) ? '\'' . $this->m_strScheduledCompletionDate . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 300, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setIsProposal( $boolIsProposal ) {
		$this->set( 'm_boolIsProposal', CStrings::strToBool( $boolIsProposal ) );
	}

	public function getIsProposal() {
		return $this->m_boolIsProposal;
	}

	public function sqlIsProposal() {
		return ( true == isset( $this->m_boolIsProposal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsProposal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setBilledBy( $intBilledBy ) {
		$this->set( 'm_intBilledBy', CStrings::strToIntDef( $intBilledBy, NULL, false ) );
	}

	public function getBilledBy() {
		return $this->m_intBilledBy;
	}

	public function sqlBilledBy() {
		return ( true == isset( $this->m_intBilledBy ) ) ? ( string ) $this->m_intBilledBy : 'NULL';
	}

	public function setBilledOn( $strBilledOn ) {
		$this->set( 'm_strBilledOn', CStrings::strTrimDef( $strBilledOn, -1, NULL, true ) );
	}

	public function getBilledOn() {
		return $this->m_strBilledOn;
	}

	public function sqlBilledOn() {
		return ( true == isset( $this->m_strBilledOn ) ) ? '\'' . $this->m_strBilledOn . '\'' : 'NULL';
	}

	public function setClosedBy( $intClosedBy ) {
		$this->set( 'm_intClosedBy', CStrings::strToIntDef( $intClosedBy, NULL, false ) );
	}

	public function getClosedBy() {
		return $this->m_intClosedBy;
	}

	public function sqlClosedBy() {
		return ( true == isset( $this->m_intClosedBy ) ) ? ( string ) $this->m_intClosedBy : 'NULL';
	}

	public function setClosedOn( $strClosedOn ) {
		$this->set( 'm_strClosedOn', CStrings::strTrimDef( $strClosedOn, -1, NULL, true ) );
	}

	public function getClosedOn() {
		return $this->m_strClosedOn;
	}

	public function sqlClosedOn() {
		return ( true == isset( $this->m_strClosedOn ) ) ? '\'' . $this->m_strClosedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, utility_account_id, meter_maintenance_type_id, property_meter_device_id, company_transction_id, title, description, work_plan, ticket_datetime, scheduled_completion_date, file_name, file_path, is_proposal, approved_by, approved_on, billed_by, billed_on, closed_by, closed_on, updated_on, updated_by, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlUtilityAccountId() . ', ' .
 						$this->sqlMeterMaintenanceTypeId() . ', ' .
 						$this->sqlPropertyMeterDeviceId() . ', ' .
 						$this->sqlCompanyTransctionId() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlWorkPlan() . ', ' .
 						$this->sqlTicketDatetime() . ', ' .
 						$this->sqlScheduledCompletionDate() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlIsProposal() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlBilledBy() . ', ' .
 						$this->sqlBilledOn() . ', ' .
 						$this->sqlClosedBy() . ', ' .
 						$this->sqlClosedOn() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_maintenance_type_id = ' . $this->sqlMeterMaintenanceTypeId() . ','; } elseif( true == array_key_exists( 'MeterMaintenanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' meter_maintenance_type_id = ' . $this->sqlMeterMaintenanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_meter_device_id = ' . $this->sqlPropertyMeterDeviceId() . ','; } elseif( true == array_key_exists( 'PropertyMeterDeviceId', $this->getChangedColumns() ) ) { $strSql .= ' property_meter_device_id = ' . $this->sqlPropertyMeterDeviceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_transction_id = ' . $this->sqlCompanyTransctionId() . ','; } elseif( true == array_key_exists( 'CompanyTransctionId', $this->getChangedColumns() ) ) { $strSql .= ' company_transction_id = ' . $this->sqlCompanyTransctionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' work_plan = ' . $this->sqlWorkPlan() . ','; } elseif( true == array_key_exists( 'WorkPlan', $this->getChangedColumns() ) ) { $strSql .= ' work_plan = ' . $this->sqlWorkPlan() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ticket_datetime = ' . $this->sqlTicketDatetime() . ','; } elseif( true == array_key_exists( 'TicketDatetime', $this->getChangedColumns() ) ) { $strSql .= ' ticket_datetime = ' . $this->sqlTicketDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_completion_date = ' . $this->sqlScheduledCompletionDate() . ','; } elseif( true == array_key_exists( 'ScheduledCompletionDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_completion_date = ' . $this->sqlScheduledCompletionDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_proposal = ' . $this->sqlIsProposal() . ','; } elseif( true == array_key_exists( 'IsProposal', $this->getChangedColumns() ) ) { $strSql .= ' is_proposal = ' . $this->sqlIsProposal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billed_by = ' . $this->sqlBilledBy() . ','; } elseif( true == array_key_exists( 'BilledBy', $this->getChangedColumns() ) ) { $strSql .= ' billed_by = ' . $this->sqlBilledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billed_on = ' . $this->sqlBilledOn() . ','; } elseif( true == array_key_exists( 'BilledOn', $this->getChangedColumns() ) ) { $strSql .= ' billed_on = ' . $this->sqlBilledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_by = ' . $this->sqlClosedBy() . ','; } elseif( true == array_key_exists( 'ClosedBy', $this->getChangedColumns() ) ) { $strSql .= ' closed_by = ' . $this->sqlClosedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_on = ' . $this->sqlClosedOn() . ','; } elseif( true == array_key_exists( 'ClosedOn', $this->getChangedColumns() ) ) { $strSql .= ' closed_on = ' . $this->sqlClosedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'meter_maintenance_type_id' => $this->getMeterMaintenanceTypeId(),
			'property_meter_device_id' => $this->getPropertyMeterDeviceId(),
			'company_transction_id' => $this->getCompanyTransctionId(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'work_plan' => $this->getWorkPlan(),
			'ticket_datetime' => $this->getTicketDatetime(),
			'scheduled_completion_date' => $this->getScheduledCompletionDate(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'is_proposal' => $this->getIsProposal(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'billed_by' => $this->getBilledBy(),
			'billed_on' => $this->getBilledOn(),
			'closed_by' => $this->getClosedBy(),
			'closed_on' => $this->getClosedOn(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>