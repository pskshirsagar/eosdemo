<?php

class CBaseWebRetrieval extends CEosSingularBase {

	const TABLE_NAME = 'public.web_retrievals';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUtilityBillAccountId;
	protected $m_intUtilityDocumentId;
	protected $m_intUtilityBillId;
	protected $m_intWebRetrievalEscalationTypeId;
	protected $m_intAttemptCount;
	protected $m_strWebRetrievalDate;
	protected $m_strReminderDate;
	protected $m_strEscalationNote;
	protected $m_strProcessStartDatetime;
	protected $m_strProcessEndDatetime;
	protected $m_intEscalatedBy;
	protected $m_strEscalatedOn;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strServicePeriodStartDate;
	protected $m_strExpectedDueDate;
	protected $m_intChildUtilityBillAccountId;
	protected $m_strExpectedBillDate;

	public function __construct() {
		parent::__construct();

		$this->m_intAttemptCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['utility_document_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDocumentId', trim( $arrValues['utility_document_id'] ) ); elseif( isset( $arrValues['utility_document_id'] ) ) $this->setUtilityDocumentId( $arrValues['utility_document_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['web_retrieval_escalation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWebRetrievalEscalationTypeId', trim( $arrValues['web_retrieval_escalation_type_id'] ) ); elseif( isset( $arrValues['web_retrieval_escalation_type_id'] ) ) $this->setWebRetrievalEscalationTypeId( $arrValues['web_retrieval_escalation_type_id'] );
		if( isset( $arrValues['attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intAttemptCount', trim( $arrValues['attempt_count'] ) ); elseif( isset( $arrValues['attempt_count'] ) ) $this->setAttemptCount( $arrValues['attempt_count'] );
		if( isset( $arrValues['web_retrieval_date'] ) && $boolDirectSet ) $this->set( 'm_strWebRetrievalDate', trim( $arrValues['web_retrieval_date'] ) ); elseif( isset( $arrValues['web_retrieval_date'] ) ) $this->setWebRetrievalDate( $arrValues['web_retrieval_date'] );
		if( isset( $arrValues['reminder_date'] ) && $boolDirectSet ) $this->set( 'm_strReminderDate', trim( $arrValues['reminder_date'] ) ); elseif( isset( $arrValues['reminder_date'] ) ) $this->setReminderDate( $arrValues['reminder_date'] );
		if( isset( $arrValues['escalation_note'] ) && $boolDirectSet ) $this->set( 'm_strEscalationNote', trim( $arrValues['escalation_note'] ) ); elseif( isset( $arrValues['escalation_note'] ) ) $this->setEscalationNote( $arrValues['escalation_note'] );
		if( isset( $arrValues['process_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strProcessStartDatetime', trim( $arrValues['process_start_datetime'] ) ); elseif( isset( $arrValues['process_start_datetime'] ) ) $this->setProcessStartDatetime( $arrValues['process_start_datetime'] );
		if( isset( $arrValues['process_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strProcessEndDatetime', trim( $arrValues['process_end_datetime'] ) ); elseif( isset( $arrValues['process_end_datetime'] ) ) $this->setProcessEndDatetime( $arrValues['process_end_datetime'] );
		if( isset( $arrValues['escalated_by'] ) && $boolDirectSet ) $this->set( 'm_intEscalatedBy', trim( $arrValues['escalated_by'] ) ); elseif( isset( $arrValues['escalated_by'] ) ) $this->setEscalatedBy( $arrValues['escalated_by'] );
		if( isset( $arrValues['escalated_on'] ) && $boolDirectSet ) $this->set( 'm_strEscalatedOn', trim( $arrValues['escalated_on'] ) ); elseif( isset( $arrValues['escalated_on'] ) ) $this->setEscalatedOn( $arrValues['escalated_on'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['service_period_start_date'] ) && $boolDirectSet ) $this->set( 'm_strServicePeriodStartDate', trim( $arrValues['service_period_start_date'] ) ); elseif( isset( $arrValues['service_period_start_date'] ) ) $this->setServicePeriodStartDate( $arrValues['service_period_start_date'] );
		if( isset( $arrValues['expected_due_date'] ) && $boolDirectSet ) $this->set( 'm_strExpectedDueDate', trim( $arrValues['expected_due_date'] ) ); elseif( isset( $arrValues['expected_due_date'] ) ) $this->setExpectedDueDate( $arrValues['expected_due_date'] );
		if( isset( $arrValues['child_utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intChildUtilityBillAccountId', trim( $arrValues['child_utility_bill_account_id'] ) ); elseif( isset( $arrValues['child_utility_bill_account_id'] ) ) $this->setChildUtilityBillAccountId( $arrValues['child_utility_bill_account_id'] );
		if( isset( $arrValues['expected_bill_date'] ) && $boolDirectSet ) $this->set( 'm_strExpectedBillDate', trim( $arrValues['expected_bill_date'] ) ); elseif( isset( $arrValues['expected_bill_date'] ) ) $this->setExpectedBillDate( $arrValues['expected_bill_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setUtilityDocumentId( $intUtilityDocumentId ) {
		$this->set( 'm_intUtilityDocumentId', CStrings::strToIntDef( $intUtilityDocumentId, NULL, false ) );
	}

	public function getUtilityDocumentId() {
		return $this->m_intUtilityDocumentId;
	}

	public function sqlUtilityDocumentId() {
		return ( true == isset( $this->m_intUtilityDocumentId ) ) ? ( string ) $this->m_intUtilityDocumentId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setWebRetrievalEscalationTypeId( $intWebRetrievalEscalationTypeId ) {
		$this->set( 'm_intWebRetrievalEscalationTypeId', CStrings::strToIntDef( $intWebRetrievalEscalationTypeId, NULL, false ) );
	}

	public function getWebRetrievalEscalationTypeId() {
		return $this->m_intWebRetrievalEscalationTypeId;
	}

	public function sqlWebRetrievalEscalationTypeId() {
		return ( true == isset( $this->m_intWebRetrievalEscalationTypeId ) ) ? ( string ) $this->m_intWebRetrievalEscalationTypeId : 'NULL';
	}

	public function setAttemptCount( $intAttemptCount ) {
		$this->set( 'm_intAttemptCount', CStrings::strToIntDef( $intAttemptCount, NULL, false ) );
	}

	public function getAttemptCount() {
		return $this->m_intAttemptCount;
	}

	public function sqlAttemptCount() {
		return ( true == isset( $this->m_intAttemptCount ) ) ? ( string ) $this->m_intAttemptCount : '0';
	}

	public function setWebRetrievalDate( $strWebRetrievalDate ) {
		$this->set( 'm_strWebRetrievalDate', CStrings::strTrimDef( $strWebRetrievalDate, -1, NULL, true ) );
	}

	public function getWebRetrievalDate() {
		return $this->m_strWebRetrievalDate;
	}

	public function sqlWebRetrievalDate() {
		return ( true == isset( $this->m_strWebRetrievalDate ) ) ? '\'' . $this->m_strWebRetrievalDate . '\'' : 'NULL';
	}

	public function setReminderDate( $strReminderDate ) {
		$this->set( 'm_strReminderDate', CStrings::strTrimDef( $strReminderDate, -1, NULL, true ) );
	}

	public function getReminderDate() {
		return $this->m_strReminderDate;
	}

	public function sqlReminderDate() {
		return ( true == isset( $this->m_strReminderDate ) ) ? '\'' . $this->m_strReminderDate . '\'' : 'NULL';
	}

	public function setEscalationNote( $strEscalationNote ) {
		$this->set( 'm_strEscalationNote', CStrings::strTrimDef( $strEscalationNote, -1, NULL, true ) );
	}

	public function getEscalationNote() {
		return $this->m_strEscalationNote;
	}

	public function sqlEscalationNote() {
		return ( true == isset( $this->m_strEscalationNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEscalationNote ) : '\'' . addslashes( $this->m_strEscalationNote ) . '\'' ) : 'NULL';
	}

	public function setProcessStartDatetime( $strProcessStartDatetime ) {
		$this->set( 'm_strProcessStartDatetime', CStrings::strTrimDef( $strProcessStartDatetime, -1, NULL, true ) );
	}

	public function getProcessStartDatetime() {
		return $this->m_strProcessStartDatetime;
	}

	public function sqlProcessStartDatetime() {
		return ( true == isset( $this->m_strProcessStartDatetime ) ) ? '\'' . $this->m_strProcessStartDatetime . '\'' : 'NULL';
	}

	public function setProcessEndDatetime( $strProcessEndDatetime ) {
		$this->set( 'm_strProcessEndDatetime', CStrings::strTrimDef( $strProcessEndDatetime, -1, NULL, true ) );
	}

	public function getProcessEndDatetime() {
		return $this->m_strProcessEndDatetime;
	}

	public function sqlProcessEndDatetime() {
		return ( true == isset( $this->m_strProcessEndDatetime ) ) ? '\'' . $this->m_strProcessEndDatetime . '\'' : 'NULL';
	}

	public function setEscalatedBy( $intEscalatedBy ) {
		$this->set( 'm_intEscalatedBy', CStrings::strToIntDef( $intEscalatedBy, NULL, false ) );
	}

	public function getEscalatedBy() {
		return $this->m_intEscalatedBy;
	}

	public function sqlEscalatedBy() {
		return ( true == isset( $this->m_intEscalatedBy ) ) ? ( string ) $this->m_intEscalatedBy : 'NULL';
	}

	public function setEscalatedOn( $strEscalatedOn ) {
		$this->set( 'm_strEscalatedOn', CStrings::strTrimDef( $strEscalatedOn, -1, NULL, true ) );
	}

	public function getEscalatedOn() {
		return $this->m_strEscalatedOn;
	}

	public function sqlEscalatedOn() {
		return ( true == isset( $this->m_strEscalatedOn ) ) ? '\'' . $this->m_strEscalatedOn . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setServicePeriodStartDate( $strServicePeriodStartDate ) {
		$this->set( 'm_strServicePeriodStartDate', CStrings::strTrimDef( $strServicePeriodStartDate, -1, NULL, true ) );
	}

	public function getServicePeriodStartDate() {
		return $this->m_strServicePeriodStartDate;
	}

	public function sqlServicePeriodStartDate() {
		return ( true == isset( $this->m_strServicePeriodStartDate ) ) ? '\'' . $this->m_strServicePeriodStartDate . '\'' : 'NULL';
	}

	public function setExpectedDueDate( $strExpectedDueDate ) {
		$this->set( 'm_strExpectedDueDate', CStrings::strTrimDef( $strExpectedDueDate, -1, NULL, true ) );
	}

	public function getExpectedDueDate() {
		return $this->m_strExpectedDueDate;
	}

	public function sqlExpectedDueDate() {
		return ( true == isset( $this->m_strExpectedDueDate ) ) ? '\'' . $this->m_strExpectedDueDate . '\'' : 'NULL';
	}

	public function setChildUtilityBillAccountId( $intChildUtilityBillAccountId ) {
		$this->set( 'm_intChildUtilityBillAccountId', CStrings::strToIntDef( $intChildUtilityBillAccountId, NULL, false ) );
	}

	public function getChildUtilityBillAccountId() {
		return $this->m_intChildUtilityBillAccountId;
	}

	public function sqlChildUtilityBillAccountId() {
		return ( true == isset( $this->m_intChildUtilityBillAccountId ) ) ? ( string ) $this->m_intChildUtilityBillAccountId : 'NULL';
	}

	public function setExpectedBillDate( $strExpectedBillDate ) {
		$this->set( 'm_strExpectedBillDate', CStrings::strTrimDef( $strExpectedBillDate, -1, NULL, true ) );
	}

	public function getExpectedBillDate() {
		return $this->m_strExpectedBillDate;
	}

	public function sqlExpectedBillDate() {
		return ( true == isset( $this->m_strExpectedBillDate ) ) ? '\'' . $this->m_strExpectedBillDate . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, utility_bill_account_id, utility_document_id, utility_bill_id, web_retrieval_escalation_type_id, attempt_count, web_retrieval_date, reminder_date, escalation_note, process_start_datetime, process_end_datetime, escalated_by, escalated_on, completed_by, completed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, service_period_start_date, expected_due_date, child_utility_bill_account_id, expected_bill_date )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlUtilityBillAccountId() . ', ' .
						$this->sqlUtilityDocumentId() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlWebRetrievalEscalationTypeId() . ', ' .
						$this->sqlAttemptCount() . ', ' .
						$this->sqlWebRetrievalDate() . ', ' .
						$this->sqlReminderDate() . ', ' .
						$this->sqlEscalationNote() . ', ' .
						$this->sqlProcessStartDatetime() . ', ' .
						$this->sqlProcessEndDatetime() . ', ' .
						$this->sqlEscalatedBy() . ', ' .
						$this->sqlEscalatedOn() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlServicePeriodStartDate() . ', ' .
						$this->sqlExpectedDueDate() . ', ' .
						$this->sqlChildUtilityBillAccountId() . ', ' .
						$this->sqlExpectedBillDate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId(). ',' ; } elseif( true == array_key_exists( 'UtilityDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_retrieval_escalation_type_id = ' . $this->sqlWebRetrievalEscalationTypeId(). ',' ; } elseif( true == array_key_exists( 'WebRetrievalEscalationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' web_retrieval_escalation_type_id = ' . $this->sqlWebRetrievalEscalationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount(). ',' ; } elseif( true == array_key_exists( 'AttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_retrieval_date = ' . $this->sqlWebRetrievalDate(). ',' ; } elseif( true == array_key_exists( 'WebRetrievalDate', $this->getChangedColumns() ) ) { $strSql .= ' web_retrieval_date = ' . $this->sqlWebRetrievalDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reminder_date = ' . $this->sqlReminderDate(). ',' ; } elseif( true == array_key_exists( 'ReminderDate', $this->getChangedColumns() ) ) { $strSql .= ' reminder_date = ' . $this->sqlReminderDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalation_note = ' . $this->sqlEscalationNote(). ',' ; } elseif( true == array_key_exists( 'EscalationNote', $this->getChangedColumns() ) ) { $strSql .= ' escalation_note = ' . $this->sqlEscalationNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' process_start_datetime = ' . $this->sqlProcessStartDatetime(). ',' ; } elseif( true == array_key_exists( 'ProcessStartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' process_start_datetime = ' . $this->sqlProcessStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' process_end_datetime = ' . $this->sqlProcessEndDatetime(). ',' ; } elseif( true == array_key_exists( 'ProcessEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' process_end_datetime = ' . $this->sqlProcessEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_by = ' . $this->sqlEscalatedBy(). ',' ; } elseif( true == array_key_exists( 'EscalatedBy', $this->getChangedColumns() ) ) { $strSql .= ' escalated_by = ' . $this->sqlEscalatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_on = ' . $this->sqlEscalatedOn(). ',' ; } elseif( true == array_key_exists( 'EscalatedOn', $this->getChangedColumns() ) ) { $strSql .= ' escalated_on = ' . $this->sqlEscalatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_period_start_date = ' . $this->sqlServicePeriodStartDate(). ',' ; } elseif( true == array_key_exists( 'ServicePeriodStartDate', $this->getChangedColumns() ) ) { $strSql .= ' service_period_start_date = ' . $this->sqlServicePeriodStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_due_date = ' . $this->sqlExpectedDueDate(). ',' ; } elseif( true == array_key_exists( 'ExpectedDueDate', $this->getChangedColumns() ) ) { $strSql .= ' expected_due_date = ' . $this->sqlExpectedDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' child_utility_bill_account_id = ' . $this->sqlChildUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'ChildUtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' child_utility_bill_account_id = ' . $this->sqlChildUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_bill_date = ' . $this->sqlExpectedBillDate(). ',' ; } elseif( true == array_key_exists( 'ExpectedBillDate', $this->getChangedColumns() ) ) { $strSql .= ' expected_bill_date = ' . $this->sqlExpectedBillDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'utility_document_id' => $this->getUtilityDocumentId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'web_retrieval_escalation_type_id' => $this->getWebRetrievalEscalationTypeId(),
			'attempt_count' => $this->getAttemptCount(),
			'web_retrieval_date' => $this->getWebRetrievalDate(),
			'reminder_date' => $this->getReminderDate(),
			'escalation_note' => $this->getEscalationNote(),
			'process_start_datetime' => $this->getProcessStartDatetime(),
			'process_end_datetime' => $this->getProcessEndDatetime(),
			'escalated_by' => $this->getEscalatedBy(),
			'escalated_on' => $this->getEscalatedOn(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'service_period_start_date' => $this->getServicePeriodStartDate(),
			'expected_due_date' => $this->getExpectedDueDate(),
			'child_utility_bill_account_id' => $this->getChildUtilityBillAccountId(),
			'expected_bill_date' => $this->getExpectedBillDate()
		);
	}

}
?>