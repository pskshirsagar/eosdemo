<?php

class CBaseTestBillBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.test_bill_batches';

	protected $m_intId;
	protected $m_intTestBatchTypeId;
	protected $m_strBatchStatus;
	protected $m_strExpectedResult;
	protected $m_strActualResult;
	protected $m_strNotes;
	protected $m_strErrors;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['test_batch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTestBatchTypeId', trim( $arrValues['test_batch_type_id'] ) ); elseif( isset( $arrValues['test_batch_type_id'] ) ) $this->setTestBatchTypeId( $arrValues['test_batch_type_id'] );
		if( isset( $arrValues['batch_status'] ) && $boolDirectSet ) $this->set( 'm_strBatchStatus', trim( $arrValues['batch_status'] ) ); elseif( isset( $arrValues['batch_status'] ) ) $this->setBatchStatus( $arrValues['batch_status'] );
		if( isset( $arrValues['expected_result'] ) && $boolDirectSet ) $this->set( 'm_strExpectedResult', trim( stripcslashes( $arrValues['expected_result'] ) ) ); elseif( isset( $arrValues['expected_result'] ) ) $this->setExpectedResult( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['expected_result'] ) : $arrValues['expected_result'] );
		if( isset( $arrValues['actual_result'] ) && $boolDirectSet ) $this->set( 'm_strActualResult', trim( stripcslashes( $arrValues['actual_result'] ) ) ); elseif( isset( $arrValues['actual_result'] ) ) $this->setActualResult( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['actual_result'] ) : $arrValues['actual_result'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['errors'] ) && $boolDirectSet ) $this->set( 'm_strErrors', trim( $arrValues['errors'] ) ); elseif( isset( $arrValues['errors'] ) ) $this->setErrors( $arrValues['errors'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTestBatchTypeId( $intTestBatchTypeId ) {
		$this->set( 'm_intTestBatchTypeId', CStrings::strToIntDef( $intTestBatchTypeId, NULL, false ) );
	}

	public function getTestBatchTypeId() {
		return $this->m_intTestBatchTypeId;
	}

	public function sqlTestBatchTypeId() {
		return ( true == isset( $this->m_intTestBatchTypeId ) ) ? ( string ) $this->m_intTestBatchTypeId : 'NULL';
	}

	public function setBatchStatus( $strBatchStatus ) {
		$this->set( 'm_strBatchStatus', CStrings::strTrimDef( $strBatchStatus, 15, NULL, true ) );
	}

	public function getBatchStatus() {
		return $this->m_strBatchStatus;
	}

	public function sqlBatchStatus() {
		return ( true == isset( $this->m_strBatchStatus ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBatchStatus ) : '\'' . addslashes( $this->m_strBatchStatus ) . '\'' ) : 'NULL';
	}

	public function setExpectedResult( $strExpectedResult ) {
		$this->set( 'm_strExpectedResult', CStrings::strTrimDef( $strExpectedResult, -1, NULL, true ) );
	}

	public function getExpectedResult() {
		return $this->m_strExpectedResult;
	}

	public function sqlExpectedResult() {
		return ( true == isset( $this->m_strExpectedResult ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExpectedResult ) : '\'' . addslashes( $this->m_strExpectedResult ) . '\'' ) : 'NULL';
	}

	public function setActualResult( $strActualResult ) {
		$this->set( 'm_strActualResult', CStrings::strTrimDef( $strActualResult, -1, NULL, true ) );
	}

	public function getActualResult() {
		return $this->m_strActualResult;
	}

	public function sqlActualResult() {
		return ( true == isset( $this->m_strActualResult ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strActualResult ) : '\'' . addslashes( $this->m_strActualResult ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setErrors( $strErrors ) {
		$this->set( 'm_strErrors', CStrings::strTrimDef( $strErrors, -1, NULL, true ) );
	}

	public function getErrors() {
		return $this->m_strErrors;
	}

	public function sqlErrors() {
		return ( true == isset( $this->m_strErrors ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strErrors ) : '\'' . addslashes( $this->m_strErrors ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, test_batch_type_id, batch_status, expected_result, actual_result, notes, errors, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTestBatchTypeId() . ', ' .
						$this->sqlBatchStatus() . ', ' .
						$this->sqlExpectedResult() . ', ' .
						$this->sqlActualResult() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlErrors() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_batch_type_id = ' . $this->sqlTestBatchTypeId(). ',' ; } elseif( true == array_key_exists( 'TestBatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' test_batch_type_id = ' . $this->sqlTestBatchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_status = ' . $this->sqlBatchStatus(). ',' ; } elseif( true == array_key_exists( 'BatchStatus', $this->getChangedColumns() ) ) { $strSql .= ' batch_status = ' . $this->sqlBatchStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_result = ' . $this->sqlExpectedResult(). ',' ; } elseif( true == array_key_exists( 'ExpectedResult', $this->getChangedColumns() ) ) { $strSql .= ' expected_result = ' . $this->sqlExpectedResult() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_result = ' . $this->sqlActualResult(). ',' ; } elseif( true == array_key_exists( 'ActualResult', $this->getChangedColumns() ) ) { $strSql .= ' actual_result = ' . $this->sqlActualResult() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' errors = ' . $this->sqlErrors(). ',' ; } elseif( true == array_key_exists( 'Errors', $this->getChangedColumns() ) ) { $strSql .= ' errors = ' . $this->sqlErrors() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'test_batch_type_id' => $this->getTestBatchTypeId(),
			'batch_status' => $this->getBatchStatus(),
			'expected_result' => $this->getExpectedResult(),
			'actual_result' => $this->getActualResult(),
			'notes' => $this->getNotes(),
			'errors' => $this->getErrors(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>