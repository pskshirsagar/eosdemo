<?php

class CBaseEnergyStarCustomerProperty extends CEosSingularBase {

	const TABLE_NAME = 'public.energy_star_customer_properties';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intEnergyStarCustomerAccountId;
	protected $m_intExternalId;
	protected $m_strPropertyName;
	protected $m_strAddressLine;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strYearBuilt;
	protected $m_strPrimaryFunction;
	protected $m_intGrossFloorArea;
	protected $m_intOccupancyPercentage;
	protected $m_boolIsDisabled;
	protected $m_strLastSyncOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['energy_star_customer_account_id'] ) && $boolDirectSet ) $this->set( 'm_intEnergyStarCustomerAccountId', trim( $arrValues['energy_star_customer_account_id'] ) ); elseif( isset( $arrValues['energy_star_customer_account_id'] ) ) $this->setEnergyStarCustomerAccountId( $arrValues['energy_star_customer_account_id'] );
		if( isset( $arrValues['external_id'] ) && $boolDirectSet ) $this->set( 'm_intExternalId', trim( $arrValues['external_id'] ) ); elseif( isset( $arrValues['external_id'] ) ) $this->setExternalId( $arrValues['external_id'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['address_line'] ) && $boolDirectSet ) $this->set( 'm_strAddressLine', trim( stripcslashes( $arrValues['address_line'] ) ) ); elseif( isset( $arrValues['address_line'] ) ) $this->setAddressLine( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address_line'] ) : $arrValues['address_line'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['year_built'] ) && $boolDirectSet ) $this->set( 'm_strYearBuilt', trim( stripcslashes( $arrValues['year_built'] ) ) ); elseif( isset( $arrValues['year_built'] ) ) $this->setYearBuilt( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['year_built'] ) : $arrValues['year_built'] );
		if( isset( $arrValues['primary_function'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryFunction', trim( stripcslashes( $arrValues['primary_function'] ) ) ); elseif( isset( $arrValues['primary_function'] ) ) $this->setPrimaryFunction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['primary_function'] ) : $arrValues['primary_function'] );
		if( isset( $arrValues['gross_floor_area'] ) && $boolDirectSet ) $this->set( 'm_intGrossFloorArea', trim( $arrValues['gross_floor_area'] ) ); elseif( isset( $arrValues['gross_floor_area'] ) ) $this->setGrossFloorArea( $arrValues['gross_floor_area'] );
		if( isset( $arrValues['occupancy_percentage'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyPercentage', trim( $arrValues['occupancy_percentage'] ) ); elseif( isset( $arrValues['occupancy_percentage'] ) ) $this->setOccupancyPercentage( $arrValues['occupancy_percentage'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['last_sync_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSyncOn', trim( $arrValues['last_sync_on'] ) ); elseif( isset( $arrValues['last_sync_on'] ) ) $this->setLastSyncOn( $arrValues['last_sync_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setEnergyStarCustomerAccountId( $intEnergyStarCustomerAccountId ) {
		$this->set( 'm_intEnergyStarCustomerAccountId', CStrings::strToIntDef( $intEnergyStarCustomerAccountId, NULL, false ) );
	}

	public function getEnergyStarCustomerAccountId() {
		return $this->m_intEnergyStarCustomerAccountId;
	}

	public function sqlEnergyStarCustomerAccountId() {
		return ( true == isset( $this->m_intEnergyStarCustomerAccountId ) ) ? ( string ) $this->m_intEnergyStarCustomerAccountId : 'NULL';
	}

	public function setExternalId( $intExternalId ) {
		$this->set( 'm_intExternalId', CStrings::strToIntDef( $intExternalId, NULL, false ) );
	}

	public function getExternalId() {
		return $this->m_intExternalId;
	}

	public function sqlExternalId() {
		return ( true == isset( $this->m_intExternalId ) ) ? ( string ) $this->m_intExternalId : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setAddressLine( $strAddressLine ) {
		$this->set( 'm_strAddressLine', CStrings::strTrimDef( $strAddressLine, 200, NULL, true ) );
	}

	public function getAddressLine() {
		return $this->m_strAddressLine;
	}

	public function sqlAddressLine() {
		return ( true == isset( $this->m_strAddressLine ) ) ? '\'' . addslashes( $this->m_strAddressLine ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 20, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setYearBuilt( $strYearBuilt ) {
		$this->set( 'm_strYearBuilt', CStrings::strTrimDef( $strYearBuilt, 10, NULL, true ) );
	}

	public function getYearBuilt() {
		return $this->m_strYearBuilt;
	}

	public function sqlYearBuilt() {
		return ( true == isset( $this->m_strYearBuilt ) ) ? '\'' . addslashes( $this->m_strYearBuilt ) . '\'' : 'NULL';
	}

	public function setPrimaryFunction( $strPrimaryFunction ) {
		$this->set( 'm_strPrimaryFunction', CStrings::strTrimDef( $strPrimaryFunction, 50, NULL, true ) );
	}

	public function getPrimaryFunction() {
		return $this->m_strPrimaryFunction;
	}

	public function sqlPrimaryFunction() {
		return ( true == isset( $this->m_strPrimaryFunction ) ) ? '\'' . addslashes( $this->m_strPrimaryFunction ) . '\'' : 'NULL';
	}

	public function setGrossFloorArea( $intGrossFloorArea ) {
		$this->set( 'm_intGrossFloorArea', CStrings::strToIntDef( $intGrossFloorArea, NULL, false ) );
	}

	public function getGrossFloorArea() {
		return $this->m_intGrossFloorArea;
	}

	public function sqlGrossFloorArea() {
		return ( true == isset( $this->m_intGrossFloorArea ) ) ? ( string ) $this->m_intGrossFloorArea : 'NULL';
	}

	public function setOccupancyPercentage( $intOccupancyPercentage ) {
		$this->set( 'm_intOccupancyPercentage', CStrings::strToIntDef( $intOccupancyPercentage, NULL, false ) );
	}

	public function getOccupancyPercentage() {
		return $this->m_intOccupancyPercentage;
	}

	public function sqlOccupancyPercentage() {
		return ( true == isset( $this->m_intOccupancyPercentage ) ) ? ( string ) $this->m_intOccupancyPercentage : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLastSyncOn( $strLastSyncOn ) {
		$this->set( 'm_strLastSyncOn', CStrings::strTrimDef( $strLastSyncOn, -1, NULL, true ) );
	}

	public function getLastSyncOn() {
		return $this->m_strLastSyncOn;
	}

	public function sqlLastSyncOn() {
		return ( true == isset( $this->m_strLastSyncOn ) ) ? '\'' . $this->m_strLastSyncOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, energy_star_customer_account_id, external_id, property_name, address_line, city, state_code, postal_code, year_built, primary_function, gross_floor_area, occupancy_percentage, is_disabled, last_sync_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlEnergyStarCustomerAccountId() . ', ' .
 						$this->sqlExternalId() . ', ' .
 						$this->sqlPropertyName() . ', ' .
 						$this->sqlAddressLine() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlYearBuilt() . ', ' .
 						$this->sqlPrimaryFunction() . ', ' .
 						$this->sqlGrossFloorArea() . ', ' .
 						$this->sqlOccupancyPercentage() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
 						$this->sqlLastSyncOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' energy_star_customer_account_id = ' . $this->sqlEnergyStarCustomerAccountId() . ','; } elseif( true == array_key_exists( 'EnergyStarCustomerAccountId', $this->getChangedColumns() ) ) { $strSql .= ' energy_star_customer_account_id = ' . $this->sqlEnergyStarCustomerAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_id = ' . $this->sqlExternalId() . ','; } elseif( true == array_key_exists( 'ExternalId', $this->getChangedColumns() ) ) { $strSql .= ' external_id = ' . $this->sqlExternalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_line = ' . $this->sqlAddressLine() . ','; } elseif( true == array_key_exists( 'AddressLine', $this->getChangedColumns() ) ) { $strSql .= ' address_line = ' . $this->sqlAddressLine() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year_built = ' . $this->sqlYearBuilt() . ','; } elseif( true == array_key_exists( 'YearBuilt', $this->getChangedColumns() ) ) { $strSql .= ' year_built = ' . $this->sqlYearBuilt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_function = ' . $this->sqlPrimaryFunction() . ','; } elseif( true == array_key_exists( 'PrimaryFunction', $this->getChangedColumns() ) ) { $strSql .= ' primary_function = ' . $this->sqlPrimaryFunction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gross_floor_area = ' . $this->sqlGrossFloorArea() . ','; } elseif( true == array_key_exists( 'GrossFloorArea', $this->getChangedColumns() ) ) { $strSql .= ' gross_floor_area = ' . $this->sqlGrossFloorArea() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_percentage = ' . $this->sqlOccupancyPercentage() . ','; } elseif( true == array_key_exists( 'OccupancyPercentage', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_percentage = ' . $this->sqlOccupancyPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn() . ','; } elseif( true == array_key_exists( 'LastSyncOn', $this->getChangedColumns() ) ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'energy_star_customer_account_id' => $this->getEnergyStarCustomerAccountId(),
			'external_id' => $this->getExternalId(),
			'property_name' => $this->getPropertyName(),
			'address_line' => $this->getAddressLine(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'postal_code' => $this->getPostalCode(),
			'year_built' => $this->getYearBuilt(),
			'primary_function' => $this->getPrimaryFunction(),
			'gross_floor_area' => $this->getGrossFloorArea(),
			'occupancy_percentage' => $this->getOccupancyPercentage(),
			'is_disabled' => $this->getIsDisabled(),
			'last_sync_on' => $this->getLastSyncOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>