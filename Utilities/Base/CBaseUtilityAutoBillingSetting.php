<?php

class CBaseUtilityAutoBillingSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_auto_billing_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUtilitySettingId;
	protected $m_boolRequireBillPeriodApproval;
	protected $m_boolRequireTransmissionApproval;
	protected $m_boolRequireBillSelectionApproval;
	protected $m_boolRequireVerifyInvoiceApproval;
	protected $m_boolRequirePreBillAnalystApproval;
	protected $m_boolRequirePreBillCalculationApproval;
	protected $m_boolRequireQcApproval;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolRequireBillPeriodApproval = true;
		$this->m_boolRequireTransmissionApproval = true;
		$this->m_boolRequireBillSelectionApproval = true;
		$this->m_boolRequireVerifyInvoiceApproval = true;
		$this->m_boolRequirePreBillAnalystApproval = true;
		$this->m_boolRequirePreBillCalculationApproval = true;
		$this->m_boolRequireQcApproval = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_utility_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilitySettingId', trim( $arrValues['property_utility_setting_id'] ) ); elseif( isset( $arrValues['property_utility_setting_id'] ) ) $this->setPropertyUtilitySettingId( $arrValues['property_utility_setting_id'] );
		if( isset( $arrValues['require_bill_period_approval'] ) && $boolDirectSet ) $this->set( 'm_boolRequireBillPeriodApproval', trim( stripcslashes( $arrValues['require_bill_period_approval'] ) ) ); elseif( isset( $arrValues['require_bill_period_approval'] ) ) $this->setRequireBillPeriodApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_bill_period_approval'] ) : $arrValues['require_bill_period_approval'] );
		if( isset( $arrValues['require_transmission_approval'] ) && $boolDirectSet ) $this->set( 'm_boolRequireTransmissionApproval', trim( stripcslashes( $arrValues['require_transmission_approval'] ) ) ); elseif( isset( $arrValues['require_transmission_approval'] ) ) $this->setRequireTransmissionApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_transmission_approval'] ) : $arrValues['require_transmission_approval'] );
		if( isset( $arrValues['require_bill_selection_approval'] ) && $boolDirectSet ) $this->set( 'm_boolRequireBillSelectionApproval', trim( stripcslashes( $arrValues['require_bill_selection_approval'] ) ) ); elseif( isset( $arrValues['require_bill_selection_approval'] ) ) $this->setRequireBillSelectionApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_bill_selection_approval'] ) : $arrValues['require_bill_selection_approval'] );
		if( isset( $arrValues['require_verify_invoice_approval'] ) && $boolDirectSet ) $this->set( 'm_boolRequireVerifyInvoiceApproval', trim( stripcslashes( $arrValues['require_verify_invoice_approval'] ) ) ); elseif( isset( $arrValues['require_verify_invoice_approval'] ) ) $this->setRequireVerifyInvoiceApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_verify_invoice_approval'] ) : $arrValues['require_verify_invoice_approval'] );
		if( isset( $arrValues['require_pre_bill_analyst_approval'] ) && $boolDirectSet ) $this->set( 'm_boolRequirePreBillAnalystApproval', trim( stripcslashes( $arrValues['require_pre_bill_analyst_approval'] ) ) ); elseif( isset( $arrValues['require_pre_bill_analyst_approval'] ) ) $this->setRequirePreBillAnalystApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_pre_bill_analyst_approval'] ) : $arrValues['require_pre_bill_analyst_approval'] );
		if( isset( $arrValues['require_pre_bill_calculation_approval'] ) && $boolDirectSet ) $this->set( 'm_boolRequirePreBillCalculationApproval', trim( stripcslashes( $arrValues['require_pre_bill_calculation_approval'] ) ) ); elseif( isset( $arrValues['require_pre_bill_calculation_approval'] ) ) $this->setRequirePreBillCalculationApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_pre_bill_calculation_approval'] ) : $arrValues['require_pre_bill_calculation_approval'] );
		if( isset( $arrValues['require_qc_approval'] ) && $boolDirectSet ) $this->set( 'm_boolRequireQcApproval', trim( stripcslashes( $arrValues['require_qc_approval'] ) ) ); elseif( isset( $arrValues['require_qc_approval'] ) ) $this->setRequireQcApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_qc_approval'] ) : $arrValues['require_qc_approval'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUtilitySettingId( $intPropertyUtilitySettingId ) {
		$this->set( 'm_intPropertyUtilitySettingId', CStrings::strToIntDef( $intPropertyUtilitySettingId, NULL, false ) );
	}

	public function getPropertyUtilitySettingId() {
		return $this->m_intPropertyUtilitySettingId;
	}

	public function sqlPropertyUtilitySettingId() {
		return ( true == isset( $this->m_intPropertyUtilitySettingId ) ) ? ( string ) $this->m_intPropertyUtilitySettingId : 'NULL';
	}

	public function setRequireBillPeriodApproval( $boolRequireBillPeriodApproval ) {
		$this->set( 'm_boolRequireBillPeriodApproval', CStrings::strToBool( $boolRequireBillPeriodApproval ) );
	}

	public function getRequireBillPeriodApproval() {
		return $this->m_boolRequireBillPeriodApproval;
	}

	public function sqlRequireBillPeriodApproval() {
		return ( true == isset( $this->m_boolRequireBillPeriodApproval ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireBillPeriodApproval ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireTransmissionApproval( $boolRequireTransmissionApproval ) {
		$this->set( 'm_boolRequireTransmissionApproval', CStrings::strToBool( $boolRequireTransmissionApproval ) );
	}

	public function getRequireTransmissionApproval() {
		return $this->m_boolRequireTransmissionApproval;
	}

	public function sqlRequireTransmissionApproval() {
		return ( true == isset( $this->m_boolRequireTransmissionApproval ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireTransmissionApproval ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireBillSelectionApproval( $boolRequireBillSelectionApproval ) {
		$this->set( 'm_boolRequireBillSelectionApproval', CStrings::strToBool( $boolRequireBillSelectionApproval ) );
	}

	public function getRequireBillSelectionApproval() {
		return $this->m_boolRequireBillSelectionApproval;
	}

	public function sqlRequireBillSelectionApproval() {
		return ( true == isset( $this->m_boolRequireBillSelectionApproval ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireBillSelectionApproval ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireVerifyInvoiceApproval( $boolRequireVerifyInvoiceApproval ) {
		$this->set( 'm_boolRequireVerifyInvoiceApproval', CStrings::strToBool( $boolRequireVerifyInvoiceApproval ) );
	}

	public function getRequireVerifyInvoiceApproval() {
		return $this->m_boolRequireVerifyInvoiceApproval;
	}

	public function sqlRequireVerifyInvoiceApproval() {
		return ( true == isset( $this->m_boolRequireVerifyInvoiceApproval ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireVerifyInvoiceApproval ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequirePreBillAnalystApproval( $boolRequirePreBillAnalystApproval ) {
		$this->set( 'm_boolRequirePreBillAnalystApproval', CStrings::strToBool( $boolRequirePreBillAnalystApproval ) );
	}

	public function getRequirePreBillAnalystApproval() {
		return $this->m_boolRequirePreBillAnalystApproval;
	}

	public function sqlRequirePreBillAnalystApproval() {
		return ( true == isset( $this->m_boolRequirePreBillAnalystApproval ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequirePreBillAnalystApproval ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequirePreBillCalculationApproval( $boolRequirePreBillCalculationApproval ) {
		$this->set( 'm_boolRequirePreBillCalculationApproval', CStrings::strToBool( $boolRequirePreBillCalculationApproval ) );
	}

	public function getRequirePreBillCalculationApproval() {
		return $this->m_boolRequirePreBillCalculationApproval;
	}

	public function sqlRequirePreBillCalculationApproval() {
		return ( true == isset( $this->m_boolRequirePreBillCalculationApproval ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequirePreBillCalculationApproval ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireQcApproval( $boolRequireQcApproval ) {
		$this->set( 'm_boolRequireQcApproval', CStrings::strToBool( $boolRequireQcApproval ) );
	}

	public function getRequireQcApproval() {
		return $this->m_boolRequireQcApproval;
	}

	public function sqlRequireQcApproval() {
		return ( true == isset( $this->m_boolRequireQcApproval ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireQcApproval ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_setting_id, require_bill_period_approval, require_transmission_approval, require_bill_selection_approval, require_verify_invoice_approval, require_pre_bill_analyst_approval, require_pre_bill_calculation_approval, require_qc_approval, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyUtilitySettingId() . ', ' .
 						$this->sqlRequireBillPeriodApproval() . ', ' .
 						$this->sqlRequireTransmissionApproval() . ', ' .
 						$this->sqlRequireBillSelectionApproval() . ', ' .
 						$this->sqlRequireVerifyInvoiceApproval() . ', ' .
 						$this->sqlRequirePreBillAnalystApproval() . ', ' .
 						$this->sqlRequirePreBillCalculationApproval() . ', ' .
 						$this->sqlRequireQcApproval() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId() . ','; } elseif( true == array_key_exists( 'PropertyUtilitySettingId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_bill_period_approval = ' . $this->sqlRequireBillPeriodApproval() . ','; } elseif( true == array_key_exists( 'RequireBillPeriodApproval', $this->getChangedColumns() ) ) { $strSql .= ' require_bill_period_approval = ' . $this->sqlRequireBillPeriodApproval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_transmission_approval = ' . $this->sqlRequireTransmissionApproval() . ','; } elseif( true == array_key_exists( 'RequireTransmissionApproval', $this->getChangedColumns() ) ) { $strSql .= ' require_transmission_approval = ' . $this->sqlRequireTransmissionApproval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_bill_selection_approval = ' . $this->sqlRequireBillSelectionApproval() . ','; } elseif( true == array_key_exists( 'RequireBillSelectionApproval', $this->getChangedColumns() ) ) { $strSql .= ' require_bill_selection_approval = ' . $this->sqlRequireBillSelectionApproval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_verify_invoice_approval = ' . $this->sqlRequireVerifyInvoiceApproval() . ','; } elseif( true == array_key_exists( 'RequireVerifyInvoiceApproval', $this->getChangedColumns() ) ) { $strSql .= ' require_verify_invoice_approval = ' . $this->sqlRequireVerifyInvoiceApproval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_pre_bill_analyst_approval = ' . $this->sqlRequirePreBillAnalystApproval() . ','; } elseif( true == array_key_exists( 'RequirePreBillAnalystApproval', $this->getChangedColumns() ) ) { $strSql .= ' require_pre_bill_analyst_approval = ' . $this->sqlRequirePreBillAnalystApproval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_pre_bill_calculation_approval = ' . $this->sqlRequirePreBillCalculationApproval() . ','; } elseif( true == array_key_exists( 'RequirePreBillCalculationApproval', $this->getChangedColumns() ) ) { $strSql .= ' require_pre_bill_calculation_approval = ' . $this->sqlRequirePreBillCalculationApproval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_qc_approval = ' . $this->sqlRequireQcApproval() . ','; } elseif( true == array_key_exists( 'RequireQcApproval', $this->getChangedColumns() ) ) { $strSql .= ' require_qc_approval = ' . $this->sqlRequireQcApproval() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_utility_setting_id' => $this->getPropertyUtilitySettingId(),
			'require_bill_period_approval' => $this->getRequireBillPeriodApproval(),
			'require_transmission_approval' => $this->getRequireTransmissionApproval(),
			'require_bill_selection_approval' => $this->getRequireBillSelectionApproval(),
			'require_verify_invoice_approval' => $this->getRequireVerifyInvoiceApproval(),
			'require_pre_bill_analyst_approval' => $this->getRequirePreBillAnalystApproval(),
			'require_pre_bill_calculation_approval' => $this->getRequirePreBillCalculationApproval(),
			'require_qc_approval' => $this->getRequireQcApproval(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>