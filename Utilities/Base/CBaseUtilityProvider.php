<?php

class CBaseUtilityProvider extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_providers';

	protected $m_intId;
	protected $m_intUtilityTypeId;
	protected $m_intPhoneNumberTypeId;
	protected $m_strProviderDatetime;
	protected $m_strName;
	protected $m_intPriority;
	protected $m_strDescription;
	protected $m_strWebsiteUrl;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strEmailAddress;
	protected $m_strTitle;
	protected $m_strPhoneNumber;
	protected $m_strNotes;
	protected $m_boolEmailsBills;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsOcrEnabled;
	protected $m_strVendorCode;
	protected $m_strOcrKeys;
	protected $m_boolIsRequiredSpecificLoaForm;
	protected $m_boolIsRequiredSpecificCoaForm;
	protected $m_boolIsRequiredSpecificRtoForm;
	protected $m_boolIsRequiredAccountNumberForCoaRequests;
	protected $m_boolIsRequiredServiceAddressForCoaRequests;
	protected $m_boolIsRequiredAccountNumberForRtoRequests;
	protected $m_boolIsRequiredServiceAddressForRtoRequests;
	protected $m_boolIsContactPreferenceSameAsCoa;
	protected $m_boolIsRequiredSeparateRtoRequest;
	protected $m_strDoNotPayLabel;
	protected $m_strDueUponReceiptLabel;

	public function __construct() {
		parent::__construct();

		$this->m_intPriority = '1';
		$this->m_boolEmailsBills = false;
		$this->m_boolIsOcrEnabled = false;
		$this->m_boolIsRequiredSpecificLoaForm = false;
		$this->m_boolIsRequiredSpecificCoaForm = false;
		$this->m_boolIsRequiredSpecificRtoForm = false;
		$this->m_boolIsRequiredAccountNumberForCoaRequests = false;
		$this->m_boolIsRequiredServiceAddressForCoaRequests = false;
		$this->m_boolIsRequiredAccountNumberForRtoRequests = false;
		$this->m_boolIsRequiredServiceAddressForRtoRequests = false;
		$this->m_boolIsContactPreferenceSameAsCoa = false;
		$this->m_boolIsRequiredSeparateRtoRequest = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeId', trim( $arrValues['utility_type_id'] ) ); elseif( isset( $arrValues['utility_type_id'] ) ) $this->setUtilityTypeId( $arrValues['utility_type_id'] );
		if( isset( $arrValues['phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPhoneNumberTypeId', trim( $arrValues['phone_number_type_id'] ) ); elseif( isset( $arrValues['phone_number_type_id'] ) ) $this->setPhoneNumberTypeId( $arrValues['phone_number_type_id'] );
		if( isset( $arrValues['provider_datetime'] ) && $boolDirectSet ) $this->set( 'm_strProviderDatetime', trim( $arrValues['provider_datetime'] ) ); elseif( isset( $arrValues['provider_datetime'] ) ) $this->setProviderDatetime( $arrValues['provider_datetime'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['priority'] ) && $boolDirectSet ) $this->set( 'm_intPriority', trim( $arrValues['priority'] ) ); elseif( isset( $arrValues['priority'] ) ) $this->setPriority( $arrValues['priority'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['website_url'] ) && $boolDirectSet ) $this->set( 'm_strWebsiteUrl', trim( $arrValues['website_url'] ) ); elseif( isset( $arrValues['website_url'] ) ) $this->setWebsiteUrl( $arrValues['website_url'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['emails_bills'] ) && $boolDirectSet ) $this->set( 'm_boolEmailsBills', trim( stripcslashes( $arrValues['emails_bills'] ) ) ); elseif( isset( $arrValues['emails_bills'] ) ) $this->setEmailsBills( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['emails_bills'] ) : $arrValues['emails_bills'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_ocr_enabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsOcrEnabled', trim( stripcslashes( $arrValues['is_ocr_enabled'] ) ) ); elseif( isset( $arrValues['is_ocr_enabled'] ) ) $this->setIsOcrEnabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ocr_enabled'] ) : $arrValues['is_ocr_enabled'] );
		if( isset( $arrValues['vendor_code'] ) && $boolDirectSet ) $this->set( 'm_strVendorCode', trim( $arrValues['vendor_code'] ) ); elseif( isset( $arrValues['vendor_code'] ) ) $this->setVendorCode( $arrValues['vendor_code'] );
		if( isset( $arrValues['ocr_keys'] ) && $boolDirectSet ) $this->set( 'm_strOcrKeys', trim( $arrValues['ocr_keys'] ) ); elseif( isset( $arrValues['ocr_keys'] ) ) $this->setOcrKeys( $arrValues['ocr_keys'] );
		if( isset( $arrValues['is_required_specific_loa_form'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequiredSpecificLoaForm', trim( stripcslashes( $arrValues['is_required_specific_loa_form'] ) ) ); elseif( isset( $arrValues['is_required_specific_loa_form'] ) ) $this->setIsRequiredSpecificLoaForm( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required_specific_loa_form'] ) : $arrValues['is_required_specific_loa_form'] );
		if( isset( $arrValues['is_required_specific_coa_form'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequiredSpecificCoaForm', trim( stripcslashes( $arrValues['is_required_specific_coa_form'] ) ) ); elseif( isset( $arrValues['is_required_specific_coa_form'] ) ) $this->setIsRequiredSpecificCoaForm( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required_specific_coa_form'] ) : $arrValues['is_required_specific_coa_form'] );
		if( isset( $arrValues['is_required_specific_rto_form'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequiredSpecificRtoForm', trim( stripcslashes( $arrValues['is_required_specific_rto_form'] ) ) ); elseif( isset( $arrValues['is_required_specific_rto_form'] ) ) $this->setIsRequiredSpecificRtoForm( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required_specific_rto_form'] ) : $arrValues['is_required_specific_rto_form'] );
		if( isset( $arrValues['is_required_account_number_for_coa_requests'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequiredAccountNumberForCoaRequests', trim( stripcslashes( $arrValues['is_required_account_number_for_coa_requests'] ) ) ); elseif( isset( $arrValues['is_required_account_number_for_coa_requests'] ) ) $this->setIsRequiredAccountNumberForCoaRequests( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required_account_number_for_coa_requests'] ) : $arrValues['is_required_account_number_for_coa_requests'] );
		if( isset( $arrValues['is_required_service_address_for_coa_requests'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequiredServiceAddressForCoaRequests', trim( stripcslashes( $arrValues['is_required_service_address_for_coa_requests'] ) ) ); elseif( isset( $arrValues['is_required_service_address_for_coa_requests'] ) ) $this->setIsRequiredServiceAddressForCoaRequests( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required_service_address_for_coa_requests'] ) : $arrValues['is_required_service_address_for_coa_requests'] );
		if( isset( $arrValues['is_required_account_number_for_rto_requests'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequiredAccountNumberForRtoRequests', trim( stripcslashes( $arrValues['is_required_account_number_for_rto_requests'] ) ) ); elseif( isset( $arrValues['is_required_account_number_for_rto_requests'] ) ) $this->setIsRequiredAccountNumberForRtoRequests( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required_account_number_for_rto_requests'] ) : $arrValues['is_required_account_number_for_rto_requests'] );
		if( isset( $arrValues['is_required_service_address_for_rto_requests'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequiredServiceAddressForRtoRequests', trim( stripcslashes( $arrValues['is_required_service_address_for_rto_requests'] ) ) ); elseif( isset( $arrValues['is_required_service_address_for_rto_requests'] ) ) $this->setIsRequiredServiceAddressForRtoRequests( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required_service_address_for_rto_requests'] ) : $arrValues['is_required_service_address_for_rto_requests'] );
		if( isset( $arrValues['is_contact_preference_same_as_coa'] ) && $boolDirectSet ) $this->set( 'm_boolIsContactPreferenceSameAsCoa', trim( stripcslashes( $arrValues['is_contact_preference_same_as_coa'] ) ) ); elseif( isset( $arrValues['is_contact_preference_same_as_coa'] ) ) $this->setIsContactPreferenceSameAsCoa( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_contact_preference_same_as_coa'] ) : $arrValues['is_contact_preference_same_as_coa'] );
		if( isset( $arrValues['is_required_separate_rto_request'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequiredSeparateRtoRequest', trim( stripcslashes( $arrValues['is_required_separate_rto_request'] ) ) ); elseif( isset( $arrValues['is_required_separate_rto_request'] ) ) $this->setIsRequiredSeparateRtoRequest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required_separate_rto_request'] ) : $arrValues['is_required_separate_rto_request'] );
		if( isset( $arrValues['do_not_pay_label'] ) && $boolDirectSet ) $this->set( 'm_strDoNotPayLabel', trim( $arrValues['do_not_pay_label'] ) ); elseif( isset( $arrValues['do_not_pay_label'] ) ) $this->setDoNotPayLabel( $arrValues['do_not_pay_label'] );
		if( isset( $arrValues['due_upon_receipt_label'] ) && $boolDirectSet ) $this->set( 'm_strDueUponReceiptLabel', trim( $arrValues['due_upon_receipt_label'] ) ); elseif( isset( $arrValues['due_upon_receipt_label'] ) ) $this->setDueUponReceiptLabel( $arrValues['due_upon_receipt_label'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->set( 'm_intUtilityTypeId', CStrings::strToIntDef( $intUtilityTypeId, NULL, false ) );
	}

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function sqlUtilityTypeId() {
		return ( true == isset( $this->m_intUtilityTypeId ) ) ? ( string ) $this->m_intUtilityTypeId : 'NULL';
	}

	public function setPhoneNumberTypeId( $intPhoneNumberTypeId ) {
		$this->set( 'm_intPhoneNumberTypeId', CStrings::strToIntDef( $intPhoneNumberTypeId, NULL, false ) );
	}

	public function getPhoneNumberTypeId() {
		return $this->m_intPhoneNumberTypeId;
	}

	public function sqlPhoneNumberTypeId() {
		return ( true == isset( $this->m_intPhoneNumberTypeId ) ) ? ( string ) $this->m_intPhoneNumberTypeId : 'NULL';
	}

	public function setProviderDatetime( $strProviderDatetime ) {
		$this->set( 'm_strProviderDatetime', CStrings::strTrimDef( $strProviderDatetime, -1, NULL, true ) );
	}

	public function getProviderDatetime() {
		return $this->m_strProviderDatetime;
	}

	public function sqlProviderDatetime() {
		return ( true == isset( $this->m_strProviderDatetime ) ) ? '\'' . $this->m_strProviderDatetime . '\'' : 'NOW()';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setPriority( $intPriority ) {
		$this->set( 'm_intPriority', CStrings::strToIntDef( $intPriority, NULL, false ) );
	}

	public function getPriority() {
		return $this->m_intPriority;
	}

	public function sqlPriority() {
		return ( true == isset( $this->m_intPriority ) ) ? ( string ) $this->m_intPriority : '1';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setWebsiteUrl( $strWebsiteUrl ) {
		$this->set( 'm_strWebsiteUrl', CStrings::strTrimDef( $strWebsiteUrl, 300, NULL, true ) );
	}

	public function getWebsiteUrl() {
		return $this->m_strWebsiteUrl;
	}

	public function sqlWebsiteUrl() {
		return ( true == isset( $this->m_strWebsiteUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebsiteUrl ) : '\'' . addslashes( $this->m_strWebsiteUrl ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setEmailsBills( $boolEmailsBills ) {
		$this->set( 'm_boolEmailsBills', CStrings::strToBool( $boolEmailsBills ) );
	}

	public function getEmailsBills() {
		return $this->m_boolEmailsBills;
	}

	public function sqlEmailsBills() {
		return ( true == isset( $this->m_boolEmailsBills ) ) ? '\'' . ( true == ( bool ) $this->m_boolEmailsBills ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsOcrEnabled( $boolIsOcrEnabled ) {
		$this->set( 'm_boolIsOcrEnabled', CStrings::strToBool( $boolIsOcrEnabled ) );
	}

	public function getIsOcrEnabled() {
		return $this->m_boolIsOcrEnabled;
	}

	public function sqlIsOcrEnabled() {
		return ( true == isset( $this->m_boolIsOcrEnabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOcrEnabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setVendorCode( $strVendorCode ) {
		$this->set( 'm_strVendorCode', CStrings::strTrimDef( $strVendorCode, -1, NULL, true ) );
	}

	public function getVendorCode() {
		return $this->m_strVendorCode;
	}

	public function sqlVendorCode() {
		return ( true == isset( $this->m_strVendorCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVendorCode ) : '\'' . addslashes( $this->m_strVendorCode ) . '\'' ) : 'NULL';
	}

	public function setOcrKeys( $strOcrKeys ) {
		$this->set( 'm_strOcrKeys', CStrings::strTrimDef( $strOcrKeys, -1, NULL, true ) );
	}

	public function getOcrKeys() {
		return $this->m_strOcrKeys;
	}

	public function sqlOcrKeys() {
		return ( true == isset( $this->m_strOcrKeys ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOcrKeys ) : '\'' . addslashes( $this->m_strOcrKeys ) . '\'' ) : 'NULL';
	}

	public function setIsRequiredSpecificLoaForm( $boolIsRequiredSpecificLoaForm ) {
		$this->set( 'm_boolIsRequiredSpecificLoaForm', CStrings::strToBool( $boolIsRequiredSpecificLoaForm ) );
	}

	public function getIsRequiredSpecificLoaForm() {
		return $this->m_boolIsRequiredSpecificLoaForm;
	}

	public function sqlIsRequiredSpecificLoaForm() {
		return ( true == isset( $this->m_boolIsRequiredSpecificLoaForm ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequiredSpecificLoaForm ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequiredSpecificCoaForm( $boolIsRequiredSpecificCoaForm ) {
		$this->set( 'm_boolIsRequiredSpecificCoaForm', CStrings::strToBool( $boolIsRequiredSpecificCoaForm ) );
	}

	public function getIsRequiredSpecificCoaForm() {
		return $this->m_boolIsRequiredSpecificCoaForm;
	}

	public function sqlIsRequiredSpecificCoaForm() {
		return ( true == isset( $this->m_boolIsRequiredSpecificCoaForm ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequiredSpecificCoaForm ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequiredSpecificRtoForm( $boolIsRequiredSpecificRtoForm ) {
		$this->set( 'm_boolIsRequiredSpecificRtoForm', CStrings::strToBool( $boolIsRequiredSpecificRtoForm ) );
	}

	public function getIsRequiredSpecificRtoForm() {
		return $this->m_boolIsRequiredSpecificRtoForm;
	}

	public function sqlIsRequiredSpecificRtoForm() {
		return ( true == isset( $this->m_boolIsRequiredSpecificRtoForm ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequiredSpecificRtoForm ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequiredAccountNumberForCoaRequests( $boolIsRequiredAccountNumberForCoaRequests ) {
		$this->set( 'm_boolIsRequiredAccountNumberForCoaRequests', CStrings::strToBool( $boolIsRequiredAccountNumberForCoaRequests ) );
	}

	public function getIsRequiredAccountNumberForCoaRequests() {
		return $this->m_boolIsRequiredAccountNumberForCoaRequests;
	}

	public function sqlIsRequiredAccountNumberForCoaRequests() {
		return ( true == isset( $this->m_boolIsRequiredAccountNumberForCoaRequests ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequiredAccountNumberForCoaRequests ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequiredServiceAddressForCoaRequests( $boolIsRequiredServiceAddressForCoaRequests ) {
		$this->set( 'm_boolIsRequiredServiceAddressForCoaRequests', CStrings::strToBool( $boolIsRequiredServiceAddressForCoaRequests ) );
	}

	public function getIsRequiredServiceAddressForCoaRequests() {
		return $this->m_boolIsRequiredServiceAddressForCoaRequests;
	}

	public function sqlIsRequiredServiceAddressForCoaRequests() {
		return ( true == isset( $this->m_boolIsRequiredServiceAddressForCoaRequests ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequiredServiceAddressForCoaRequests ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequiredAccountNumberForRtoRequests( $boolIsRequiredAccountNumberForRtoRequests ) {
		$this->set( 'm_boolIsRequiredAccountNumberForRtoRequests', CStrings::strToBool( $boolIsRequiredAccountNumberForRtoRequests ) );
	}

	public function getIsRequiredAccountNumberForRtoRequests() {
		return $this->m_boolIsRequiredAccountNumberForRtoRequests;
	}

	public function sqlIsRequiredAccountNumberForRtoRequests() {
		return ( true == isset( $this->m_boolIsRequiredAccountNumberForRtoRequests ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequiredAccountNumberForRtoRequests ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequiredServiceAddressForRtoRequests( $boolIsRequiredServiceAddressForRtoRequests ) {
		$this->set( 'm_boolIsRequiredServiceAddressForRtoRequests', CStrings::strToBool( $boolIsRequiredServiceAddressForRtoRequests ) );
	}

	public function getIsRequiredServiceAddressForRtoRequests() {
		return $this->m_boolIsRequiredServiceAddressForRtoRequests;
	}

	public function sqlIsRequiredServiceAddressForRtoRequests() {
		return ( true == isset( $this->m_boolIsRequiredServiceAddressForRtoRequests ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequiredServiceAddressForRtoRequests ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsContactPreferenceSameAsCoa( $boolIsContactPreferenceSameAsCoa ) {
		$this->set( 'm_boolIsContactPreferenceSameAsCoa', CStrings::strToBool( $boolIsContactPreferenceSameAsCoa ) );
	}

	public function getIsContactPreferenceSameAsCoa() {
		return $this->m_boolIsContactPreferenceSameAsCoa;
	}

	public function sqlIsContactPreferenceSameAsCoa() {
		return ( true == isset( $this->m_boolIsContactPreferenceSameAsCoa ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsContactPreferenceSameAsCoa ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequiredSeparateRtoRequest( $boolIsRequiredSeparateRtoRequest ) {
		$this->set( 'm_boolIsRequiredSeparateRtoRequest', CStrings::strToBool( $boolIsRequiredSeparateRtoRequest ) );
	}

	public function getIsRequiredSeparateRtoRequest() {
		return $this->m_boolIsRequiredSeparateRtoRequest;
	}

	public function sqlIsRequiredSeparateRtoRequest() {
		return ( true == isset( $this->m_boolIsRequiredSeparateRtoRequest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequiredSeparateRtoRequest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDoNotPayLabel( $strDoNotPayLabel ) {
		$this->set( 'm_strDoNotPayLabel', CStrings::strTrimDef( $strDoNotPayLabel, -1, NULL, true ) );
	}

	public function getDoNotPayLabel() {
		return $this->m_strDoNotPayLabel;
	}

	public function sqlDoNotPayLabel() {
		return ( true == isset( $this->m_strDoNotPayLabel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDoNotPayLabel ) : '\'' . addslashes( $this->m_strDoNotPayLabel ) . '\'' ) : 'NULL';
	}

	public function setDueUponReceiptLabel( $strDueUponReceiptLabel ) {
		$this->set( 'm_strDueUponReceiptLabel', CStrings::strTrimDef( $strDueUponReceiptLabel, -1, NULL, true ) );
	}

	public function getDueUponReceiptLabel() {
		return $this->m_strDueUponReceiptLabel;
	}

	public function sqlDueUponReceiptLabel() {
		return ( true == isset( $this->m_strDueUponReceiptLabel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDueUponReceiptLabel ) : '\'' . addslashes( $this->m_strDueUponReceiptLabel ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_type_id, phone_number_type_id, provider_datetime, name, priority, description, website_url, name_first, name_last, email_address, title, phone_number, notes, emails_bills, updated_by, updated_on, created_by, created_on, is_ocr_enabled, vendor_code, ocr_keys, is_required_specific_loa_form, is_required_specific_coa_form, is_required_specific_rto_form, is_required_account_number_for_coa_requests, is_required_service_address_for_coa_requests, is_required_account_number_for_rto_requests, is_required_service_address_for_rto_requests, is_contact_preference_same_as_coa, is_required_separate_rto_request, do_not_pay_label, due_upon_receipt_label )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityTypeId() . ', ' .
						$this->sqlPhoneNumberTypeId() . ', ' .
						$this->sqlProviderDatetime() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlPriority() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlWebsiteUrl() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlEmailsBills() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsOcrEnabled() . ', ' .
						$this->sqlVendorCode() . ', ' .
						$this->sqlOcrKeys() . ', ' .
						$this->sqlIsRequiredSpecificLoaForm() . ', ' .
						$this->sqlIsRequiredSpecificCoaForm() . ', ' .
						$this->sqlIsRequiredSpecificRtoForm() . ', ' .
						$this->sqlIsRequiredAccountNumberForCoaRequests() . ', ' .
						$this->sqlIsRequiredServiceAddressForCoaRequests() . ', ' .
						$this->sqlIsRequiredAccountNumberForRtoRequests() . ', ' .
						$this->sqlIsRequiredServiceAddressForRtoRequests() . ', ' .
						$this->sqlIsContactPreferenceSameAsCoa() . ', ' .
						$this->sqlIsRequiredSeparateRtoRequest() . ', ' .
						$this->sqlDoNotPayLabel() . ', ' .
						$this->sqlDueUponReceiptLabel() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number_type_id = ' . $this->sqlPhoneNumberTypeId(). ',' ; } elseif( true == array_key_exists( 'PhoneNumberTypeId', $this->getChangedColumns() ) ) { $strSql .= ' phone_number_type_id = ' . $this->sqlPhoneNumberTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' provider_datetime = ' . $this->sqlProviderDatetime(). ',' ; } elseif( true == array_key_exists( 'ProviderDatetime', $this->getChangedColumns() ) ) { $strSql .= ' provider_datetime = ' . $this->sqlProviderDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' priority = ' . $this->sqlPriority(). ',' ; } elseif( true == array_key_exists( 'Priority', $this->getChangedColumns() ) ) { $strSql .= ' priority = ' . $this->sqlPriority() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl(). ',' ; } elseif( true == array_key_exists( 'WebsiteUrl', $this->getChangedColumns() ) ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emails_bills = ' . $this->sqlEmailsBills(). ',' ; } elseif( true == array_key_exists( 'EmailsBills', $this->getChangedColumns() ) ) { $strSql .= ' emails_bills = ' . $this->sqlEmailsBills() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ocr_enabled = ' . $this->sqlIsOcrEnabled(). ',' ; } elseif( true == array_key_exists( 'IsOcrEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_ocr_enabled = ' . $this->sqlIsOcrEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_code = ' . $this->sqlVendorCode(). ',' ; } elseif( true == array_key_exists( 'VendorCode', $this->getChangedColumns() ) ) { $strSql .= ' vendor_code = ' . $this->sqlVendorCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_keys = ' . $this->sqlOcrKeys(). ',' ; } elseif( true == array_key_exists( 'OcrKeys', $this->getChangedColumns() ) ) { $strSql .= ' ocr_keys = ' . $this->sqlOcrKeys() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required_specific_loa_form = ' . $this->sqlIsRequiredSpecificLoaForm(). ',' ; } elseif( true == array_key_exists( 'IsRequiredSpecificLoaForm', $this->getChangedColumns() ) ) { $strSql .= ' is_required_specific_loa_form = ' . $this->sqlIsRequiredSpecificLoaForm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required_specific_coa_form = ' . $this->sqlIsRequiredSpecificCoaForm(). ',' ; } elseif( true == array_key_exists( 'IsRequiredSpecificCoaForm', $this->getChangedColumns() ) ) { $strSql .= ' is_required_specific_coa_form = ' . $this->sqlIsRequiredSpecificCoaForm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required_specific_rto_form = ' . $this->sqlIsRequiredSpecificRtoForm(). ',' ; } elseif( true == array_key_exists( 'IsRequiredSpecificRtoForm', $this->getChangedColumns() ) ) { $strSql .= ' is_required_specific_rto_form = ' . $this->sqlIsRequiredSpecificRtoForm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required_account_number_for_coa_requests = ' . $this->sqlIsRequiredAccountNumberForCoaRequests(). ',' ; } elseif( true == array_key_exists( 'IsRequiredAccountNumberForCoaRequests', $this->getChangedColumns() ) ) { $strSql .= ' is_required_account_number_for_coa_requests = ' . $this->sqlIsRequiredAccountNumberForCoaRequests() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required_service_address_for_coa_requests = ' . $this->sqlIsRequiredServiceAddressForCoaRequests(). ',' ; } elseif( true == array_key_exists( 'IsRequiredServiceAddressForCoaRequests', $this->getChangedColumns() ) ) { $strSql .= ' is_required_service_address_for_coa_requests = ' . $this->sqlIsRequiredServiceAddressForCoaRequests() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required_account_number_for_rto_requests = ' . $this->sqlIsRequiredAccountNumberForRtoRequests(). ',' ; } elseif( true == array_key_exists( 'IsRequiredAccountNumberForRtoRequests', $this->getChangedColumns() ) ) { $strSql .= ' is_required_account_number_for_rto_requests = ' . $this->sqlIsRequiredAccountNumberForRtoRequests() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required_service_address_for_rto_requests = ' . $this->sqlIsRequiredServiceAddressForRtoRequests(). ',' ; } elseif( true == array_key_exists( 'IsRequiredServiceAddressForRtoRequests', $this->getChangedColumns() ) ) { $strSql .= ' is_required_service_address_for_rto_requests = ' . $this->sqlIsRequiredServiceAddressForRtoRequests() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_contact_preference_same_as_coa = ' . $this->sqlIsContactPreferenceSameAsCoa(). ',' ; } elseif( true == array_key_exists( 'IsContactPreferenceSameAsCoa', $this->getChangedColumns() ) ) { $strSql .= ' is_contact_preference_same_as_coa = ' . $this->sqlIsContactPreferenceSameAsCoa() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required_separate_rto_request = ' . $this->sqlIsRequiredSeparateRtoRequest(). ',' ; } elseif( true == array_key_exists( 'IsRequiredSeparateRtoRequest', $this->getChangedColumns() ) ) { $strSql .= ' is_required_separate_rto_request = ' . $this->sqlIsRequiredSeparateRtoRequest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' do_not_pay_label = ' . $this->sqlDoNotPayLabel(). ',' ; } elseif( true == array_key_exists( 'DoNotPayLabel', $this->getChangedColumns() ) ) { $strSql .= ' do_not_pay_label = ' . $this->sqlDoNotPayLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_upon_receipt_label = ' . $this->sqlDueUponReceiptLabel(). ',' ; } elseif( true == array_key_exists( 'DueUponReceiptLabel', $this->getChangedColumns() ) ) { $strSql .= ' due_upon_receipt_label = ' . $this->sqlDueUponReceiptLabel() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_type_id' => $this->getUtilityTypeId(),
			'phone_number_type_id' => $this->getPhoneNumberTypeId(),
			'provider_datetime' => $this->getProviderDatetime(),
			'name' => $this->getName(),
			'priority' => $this->getPriority(),
			'description' => $this->getDescription(),
			'website_url' => $this->getWebsiteUrl(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'email_address' => $this->getEmailAddress(),
			'title' => $this->getTitle(),
			'phone_number' => $this->getPhoneNumber(),
			'notes' => $this->getNotes(),
			'emails_bills' => $this->getEmailsBills(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_ocr_enabled' => $this->getIsOcrEnabled(),
			'vendor_code' => $this->getVendorCode(),
			'ocr_keys' => $this->getOcrKeys(),
			'is_required_specific_loa_form' => $this->getIsRequiredSpecificLoaForm(),
			'is_required_specific_coa_form' => $this->getIsRequiredSpecificCoaForm(),
			'is_required_specific_rto_form' => $this->getIsRequiredSpecificRtoForm(),
			'is_required_account_number_for_coa_requests' => $this->getIsRequiredAccountNumberForCoaRequests(),
			'is_required_service_address_for_coa_requests' => $this->getIsRequiredServiceAddressForCoaRequests(),
			'is_required_account_number_for_rto_requests' => $this->getIsRequiredAccountNumberForRtoRequests(),
			'is_required_service_address_for_rto_requests' => $this->getIsRequiredServiceAddressForRtoRequests(),
			'is_contact_preference_same_as_coa' => $this->getIsContactPreferenceSameAsCoa(),
			'is_required_separate_rto_request' => $this->getIsRequiredSeparateRtoRequest(),
			'do_not_pay_label' => $this->getDoNotPayLabel(),
			'due_upon_receipt_label' => $this->getDueUponReceiptLabel()
		);
	}

}
?>