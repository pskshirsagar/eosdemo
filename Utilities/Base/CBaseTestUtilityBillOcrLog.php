<?php

class CBaseTestUtilityBillOcrLog extends CEosSingularBase {

	const TABLE_NAME = 'public.test_utility_bill_ocr_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillId;
	protected $m_intUtilityBillAccountId;
	protected $m_strUtilityBillAccountNumber;
	protected $m_boolIsEscalated;
	protected $m_strLogDate;
	protected $m_strOcrLogNotes;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strOcrType;
	protected $m_strStartOcrDatetime;
	protected $m_strEndOcrDatetime;
	protected $m_intDisconnectNoticeId;
	protected $m_intUtilityProviderId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsEscalated = false;
		$this->m_strOcrType = 'google';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['utility_bill_account_number'] ) && $boolDirectSet ) $this->set( 'm_strUtilityBillAccountNumber', trim( $arrValues['utility_bill_account_number'] ) ); elseif( isset( $arrValues['utility_bill_account_number'] ) ) $this->setUtilityBillAccountNumber( $arrValues['utility_bill_account_number'] );
		if( isset( $arrValues['is_escalated'] ) && $boolDirectSet ) $this->set( 'm_boolIsEscalated', trim( stripcslashes( $arrValues['is_escalated'] ) ) ); elseif( isset( $arrValues['is_escalated'] ) ) $this->setIsEscalated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_escalated'] ) : $arrValues['is_escalated'] );
		if( isset( $arrValues['log_date'] ) && $boolDirectSet ) $this->set( 'm_strLogDate', trim( $arrValues['log_date'] ) ); elseif( isset( $arrValues['log_date'] ) ) $this->setLogDate( $arrValues['log_date'] );
		if( isset( $arrValues['ocr_log_notes'] ) && $boolDirectSet ) $this->set( 'm_strOcrLogNotes', trim( $arrValues['ocr_log_notes'] ) ); elseif( isset( $arrValues['ocr_log_notes'] ) ) $this->setOcrLogNotes( $arrValues['ocr_log_notes'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ocr_type'] ) && $boolDirectSet ) $this->set( 'm_strOcrType', trim( stripcslashes( $arrValues['ocr_type'] ) ) ); elseif( isset( $arrValues['ocr_type'] ) ) $this->setOcrType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ocr_type'] ) : $arrValues['ocr_type'] );
		if( isset( $arrValues['start_ocr_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartOcrDatetime', trim( $arrValues['start_ocr_datetime'] ) ); elseif( isset( $arrValues['start_ocr_datetime'] ) ) $this->setStartOcrDatetime( $arrValues['start_ocr_datetime'] );
		if( isset( $arrValues['end_ocr_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndOcrDatetime', trim( $arrValues['end_ocr_datetime'] ) ); elseif( isset( $arrValues['end_ocr_datetime'] ) ) $this->setEndOcrDatetime( $arrValues['end_ocr_datetime'] );
		if( isset( $arrValues['disconnect_notice_id'] ) && $boolDirectSet ) $this->set( 'm_intDisconnectNoticeId', trim( $arrValues['disconnect_notice_id'] ) ); elseif( isset( $arrValues['disconnect_notice_id'] ) ) $this->setDisconnectNoticeId( $arrValues['disconnect_notice_id'] );
		if( isset( $arrValues['utility_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityProviderId', trim( $arrValues['utility_provider_id'] ) ); elseif( isset( $arrValues['utility_provider_id'] ) ) $this->setUtilityProviderId( $arrValues['utility_provider_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setUtilityBillAccountNumber( $strUtilityBillAccountNumber ) {
		$this->set( 'm_strUtilityBillAccountNumber', CStrings::strTrimDef( $strUtilityBillAccountNumber, 250, NULL, true ) );
	}

	public function getUtilityBillAccountNumber() {
		return $this->m_strUtilityBillAccountNumber;
	}

	public function sqlUtilityBillAccountNumber() {
		return ( true == isset( $this->m_strUtilityBillAccountNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUtilityBillAccountNumber ) : '\'' . addslashes( $this->m_strUtilityBillAccountNumber ) . '\'' ) : 'NULL';
	}

	public function setIsEscalated( $boolIsEscalated ) {
		$this->set( 'm_boolIsEscalated', CStrings::strToBool( $boolIsEscalated ) );
	}

	public function getIsEscalated() {
		return $this->m_boolIsEscalated;
	}

	public function sqlIsEscalated() {
		return ( true == isset( $this->m_boolIsEscalated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEscalated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLogDate( $strLogDate ) {
		$this->set( 'm_strLogDate', CStrings::strTrimDef( $strLogDate, -1, NULL, true ) );
	}

	public function getLogDate() {
		return $this->m_strLogDate;
	}

	public function sqlLogDate() {
		return ( true == isset( $this->m_strLogDate ) ) ? '\'' . $this->m_strLogDate . '\'' : 'NULL';
	}

	public function setOcrLogNotes( $strOcrLogNotes ) {
		$this->set( 'm_strOcrLogNotes', CStrings::strTrimDef( $strOcrLogNotes, -1, NULL, true ) );
	}

	public function getOcrLogNotes() {
		return $this->m_strOcrLogNotes;
	}

	public function sqlOcrLogNotes() {
		return ( true == isset( $this->m_strOcrLogNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOcrLogNotes ) : '\'' . addslashes( $this->m_strOcrLogNotes ) . '\'' ) : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOcrType( $strOcrType ) {
		$this->set( 'm_strOcrType', CStrings::strTrimDef( $strOcrType, -1, NULL, true ) );
	}

	public function getOcrType() {
		return $this->m_strOcrType;
	}

	public function sqlOcrType() {
		return ( true == isset( $this->m_strOcrType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOcrType ) : '\'' . addslashes( $this->m_strOcrType ) . '\'' ) : '\'google\'';
	}

	public function setStartOcrDatetime( $strStartOcrDatetime ) {
		$this->set( 'm_strStartOcrDatetime', CStrings::strTrimDef( $strStartOcrDatetime, -1, NULL, true ) );
	}

	public function getStartOcrDatetime() {
		return $this->m_strStartOcrDatetime;
	}

	public function sqlStartOcrDatetime() {
		return ( true == isset( $this->m_strStartOcrDatetime ) ) ? '\'' . $this->m_strStartOcrDatetime . '\'' : 'NULL';
	}

	public function setEndOcrDatetime( $strEndOcrDatetime ) {
		$this->set( 'm_strEndOcrDatetime', CStrings::strTrimDef( $strEndOcrDatetime, -1, NULL, true ) );
	}

	public function getEndOcrDatetime() {
		return $this->m_strEndOcrDatetime;
	}

	public function sqlEndOcrDatetime() {
		return ( true == isset( $this->m_strEndOcrDatetime ) ) ? '\'' . $this->m_strEndOcrDatetime . '\'' : 'NULL';
	}

	public function setDisconnectNoticeId( $intDisconnectNoticeId ) {
		$this->set( 'm_intDisconnectNoticeId', CStrings::strToIntDef( $intDisconnectNoticeId, NULL, false ) );
	}

	public function getDisconnectNoticeId() {
		return $this->m_intDisconnectNoticeId;
	}

	public function sqlDisconnectNoticeId() {
		return ( true == isset( $this->m_intDisconnectNoticeId ) ) ? ( string ) $this->m_intDisconnectNoticeId : 'NULL';
	}

	public function setUtilityProviderId( $intUtilityProviderId ) {
		$this->set( 'm_intUtilityProviderId', CStrings::strToIntDef( $intUtilityProviderId, NULL, false ) );
	}

	public function getUtilityProviderId() {
		return $this->m_intUtilityProviderId;
	}

	public function sqlUtilityProviderId() {
		return ( true == isset( $this->m_intUtilityProviderId ) ) ? ( string ) $this->m_intUtilityProviderId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_id, utility_bill_account_id, utility_bill_account_number, is_escalated, log_date, ocr_log_notes, created_by, created_on, ocr_type, start_ocr_datetime, end_ocr_datetime, disconnect_notice_id, utility_provider_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlUtilityBillAccountId() . ', ' .
						$this->sqlUtilityBillAccountNumber() . ', ' .
						$this->sqlIsEscalated() . ', ' .
						$this->sqlLogDate() . ', ' .
						$this->sqlOcrLogNotes() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlOcrType() . ', ' .
						$this->sqlStartOcrDatetime() . ', ' .
						$this->sqlEndOcrDatetime() . ', ' .
						$this->sqlDisconnectNoticeId() . ', ' .
						$this->sqlUtilityProviderId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_number = ' . $this->sqlUtilityBillAccountNumber(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_number = ' . $this->sqlUtilityBillAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_escalated = ' . $this->sqlIsEscalated(). ',' ; } elseif( true == array_key_exists( 'IsEscalated', $this->getChangedColumns() ) ) { $strSql .= ' is_escalated = ' . $this->sqlIsEscalated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_date = ' . $this->sqlLogDate(). ',' ; } elseif( true == array_key_exists( 'LogDate', $this->getChangedColumns() ) ) { $strSql .= ' log_date = ' . $this->sqlLogDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_log_notes = ' . $this->sqlOcrLogNotes(). ',' ; } elseif( true == array_key_exists( 'OcrLogNotes', $this->getChangedColumns() ) ) { $strSql .= ' ocr_log_notes = ' . $this->sqlOcrLogNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_type = ' . $this->sqlOcrType(). ',' ; } elseif( true == array_key_exists( 'OcrType', $this->getChangedColumns() ) ) { $strSql .= ' ocr_type = ' . $this->sqlOcrType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_ocr_datetime = ' . $this->sqlStartOcrDatetime(). ',' ; } elseif( true == array_key_exists( 'StartOcrDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_ocr_datetime = ' . $this->sqlStartOcrDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_ocr_datetime = ' . $this->sqlEndOcrDatetime(). ',' ; } elseif( true == array_key_exists( 'EndOcrDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_ocr_datetime = ' . $this->sqlEndOcrDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disconnect_notice_id = ' . $this->sqlDisconnectNoticeId(). ',' ; } elseif( true == array_key_exists( 'DisconnectNoticeId', $this->getChangedColumns() ) ) { $strSql .= ' disconnect_notice_id = ' . $this->sqlDisconnectNoticeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId() ; } elseif( true == array_key_exists( 'UtilityProviderId', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'utility_bill_account_number' => $this->getUtilityBillAccountNumber(),
			'is_escalated' => $this->getIsEscalated(),
			'log_date' => $this->getLogDate(),
			'ocr_log_notes' => $this->getOcrLogNotes(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ocr_type' => $this->getOcrType(),
			'start_ocr_datetime' => $this->getStartOcrDatetime(),
			'end_ocr_datetime' => $this->getEndOcrDatetime(),
			'disconnect_notice_id' => $this->getDisconnectNoticeId(),
			'utility_provider_id' => $this->getUtilityProviderId()
		);
	}

}
?>