<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Utilities\CUtilityInvoiceBatches
 * Do not add any new functions to this class.
 */

class CBaseUtilityInvoiceBatches extends CEosPluralBase {

	/**
	 * @return CUtilityInvoiceBatch[]
	 */
	public static function fetchUtilityInvoiceBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUtilityInvoiceBatch', $objDatabase );
	}

	/**
	 * @return CUtilityInvoiceBatch
	 */
	public static function fetchUtilityInvoiceBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUtilityInvoiceBatch', $objDatabase );
	}

	public static function fetchUtilityInvoiceBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'utility_invoice_batches', $objDatabase );
	}

	public static function fetchUtilityInvoiceBatchById( $intId, $objDatabase ) {
		return self::fetchUtilityInvoiceBatch( sprintf( 'SELECT * FROM utility_invoice_batches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>