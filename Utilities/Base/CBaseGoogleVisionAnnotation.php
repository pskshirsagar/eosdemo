<?php

class CBaseGoogleVisionAnnotation extends CEosSingularBase {

	const TABLE_NAME = 'public.google_vision_annotations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUtilityBillId;
	protected $m_intComplianceDocId;
	protected $m_strImageFileName;
	protected $m_strAnnotations;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDisconnectNoticeId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['compliance_doc_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceDocId', trim( $arrValues['compliance_doc_id'] ) ); elseif( isset( $arrValues['compliance_doc_id'] ) ) $this->setComplianceDocId( $arrValues['compliance_doc_id'] );
		if( isset( $arrValues['image_file_name'] ) && $boolDirectSet ) $this->set( 'm_strImageFileName', trim( $arrValues['image_file_name'] ) ); elseif( isset( $arrValues['image_file_name'] ) ) $this->setImageFileName( $arrValues['image_file_name'] );
		if( isset( $arrValues['annotations'] ) && $boolDirectSet ) $this->set( 'm_strAnnotations', trim( $arrValues['annotations'] ) ); elseif( isset( $arrValues['annotations'] ) ) $this->setAnnotations( $arrValues['annotations'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['disconnect_notice_id'] ) && $boolDirectSet ) $this->set( 'm_intDisconnectNoticeId', trim( $arrValues['disconnect_notice_id'] ) ); elseif( isset( $arrValues['disconnect_notice_id'] ) ) $this->setDisconnectNoticeId( $arrValues['disconnect_notice_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setComplianceDocId( $intComplianceDocId ) {
		$this->set( 'm_intComplianceDocId', CStrings::strToIntDef( $intComplianceDocId, NULL, false ) );
	}

	public function getComplianceDocId() {
		return $this->m_intComplianceDocId;
	}

	public function sqlComplianceDocId() {
		return ( true == isset( $this->m_intComplianceDocId ) ) ? ( string ) $this->m_intComplianceDocId : 'NULL';
	}

	public function setImageFileName( $strImageFileName ) {
		$this->set( 'm_strImageFileName', CStrings::strTrimDef( $strImageFileName, 300, NULL, true ) );
	}

	public function getImageFileName() {
		return $this->m_strImageFileName;
	}

	public function sqlImageFileName() {
		return ( true == isset( $this->m_strImageFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strImageFileName ) : '\'' . addslashes( $this->m_strImageFileName ) . '\'' ) : 'NULL';
	}

	public function setAnnotations( $strAnnotations ) {
		$this->set( 'm_strAnnotations', CStrings::strTrimDef( $strAnnotations, -1, NULL, true ) );
	}

	public function getAnnotations() {
		return $this->m_strAnnotations;
	}

	public function sqlAnnotations() {
		return ( true == isset( $this->m_strAnnotations ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAnnotations ) : '\'' . addslashes( $this->m_strAnnotations ) . '\'' ) : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDisconnectNoticeId( $intDisconnectNoticeId ) {
		$this->set( 'm_intDisconnectNoticeId', CStrings::strToIntDef( $intDisconnectNoticeId, NULL, false ) );
	}

	public function getDisconnectNoticeId() {
		return $this->m_intDisconnectNoticeId;
	}

	public function sqlDisconnectNoticeId() {
		return ( true == isset( $this->m_intDisconnectNoticeId ) ) ? ( string ) $this->m_intDisconnectNoticeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, utility_bill_id, compliance_doc_id, image_file_name, annotations, created_by, created_on, disconnect_notice_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlComplianceDocId() . ', ' .
						$this->sqlImageFileName() . ', ' .
						$this->sqlAnnotations() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDisconnectNoticeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_doc_id = ' . $this->sqlComplianceDocId(). ',' ; } elseif( true == array_key_exists( 'ComplianceDocId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_doc_id = ' . $this->sqlComplianceDocId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_file_name = ' . $this->sqlImageFileName(). ',' ; } elseif( true == array_key_exists( 'ImageFileName', $this->getChangedColumns() ) ) { $strSql .= ' image_file_name = ' . $this->sqlImageFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' annotations = ' . $this->sqlAnnotations(). ',' ; } elseif( true == array_key_exists( 'Annotations', $this->getChangedColumns() ) ) { $strSql .= ' annotations = ' . $this->sqlAnnotations() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disconnect_notice_id = ' . $this->sqlDisconnectNoticeId() ; } elseif( true == array_key_exists( 'DisconnectNoticeId', $this->getChangedColumns() ) ) { $strSql .= ' disconnect_notice_id = ' . $this->sqlDisconnectNoticeId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'compliance_doc_id' => $this->getComplianceDocId(),
			'image_file_name' => $this->getImageFileName(),
			'annotations' => $this->getAnnotations(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'disconnect_notice_id' => $this->getDisconnectNoticeId()
		);
	}

}
?>