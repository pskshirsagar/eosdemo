<?php

class CBaseUtilityTransmissionDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_transmission_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUtilityTransmissionId;
	protected $m_intPropertyMeterDeviceId;
	protected $m_intPropertyId;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intUtilityTransactionId;
	protected $m_strUnitNumber;
	protected $m_strBuildingNumber;
	protected $m_strRemotePrimaryKey;
	protected $m_strTransmissionDatetime;
	protected $m_strUtilityTypeCode;
	protected $m_strMeterNumber;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_intMeterStartRead;
	protected $m_intMeterEndRead;
	protected $m_strStatus;
	protected $m_strNotes;
	protected $m_boolIsAdjusted;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAdjusted = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTransmissionId', trim( $arrValues['utility_transmission_id'] ) ); elseif( isset( $arrValues['utility_transmission_id'] ) ) $this->setUtilityTransmissionId( $arrValues['utility_transmission_id'] );
		if( isset( $arrValues['property_meter_device_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyMeterDeviceId', trim( $arrValues['property_meter_device_id'] ) ); elseif( isset( $arrValues['property_meter_device_id'] ) ) $this->setPropertyMeterDeviceId( $arrValues['property_meter_device_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['utility_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTransactionId', trim( $arrValues['utility_transaction_id'] ) ); elseif( isset( $arrValues['utility_transaction_id'] ) ) $this->setUtilityTransactionId( $arrValues['utility_transaction_id'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['building_number'] ) && $boolDirectSet ) $this->set( 'm_strBuildingNumber', trim( stripcslashes( $arrValues['building_number'] ) ) ); elseif( isset( $arrValues['building_number'] ) ) $this->setBuildingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['building_number'] ) : $arrValues['building_number'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['transmission_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransmissionDatetime', trim( $arrValues['transmission_datetime'] ) ); elseif( isset( $arrValues['transmission_datetime'] ) ) $this->setTransmissionDatetime( $arrValues['transmission_datetime'] );
		if( isset( $arrValues['utility_type_code'] ) && $boolDirectSet ) $this->set( 'm_strUtilityTypeCode', trim( stripcslashes( $arrValues['utility_type_code'] ) ) ); elseif( isset( $arrValues['utility_type_code'] ) ) $this->setUtilityTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['utility_type_code'] ) : $arrValues['utility_type_code'] );
		if( isset( $arrValues['meter_number'] ) && $boolDirectSet ) $this->set( 'm_strMeterNumber', trim( stripcslashes( $arrValues['meter_number'] ) ) ); elseif( isset( $arrValues['meter_number'] ) ) $this->setMeterNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['meter_number'] ) : $arrValues['meter_number'] );
		if( isset( $arrValues['start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartDatetime', trim( $arrValues['start_datetime'] ) ); elseif( isset( $arrValues['start_datetime'] ) ) $this->setStartDatetime( $arrValues['start_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['meter_start_read'] ) && $boolDirectSet ) $this->set( 'm_intMeterStartRead', trim( $arrValues['meter_start_read'] ) ); elseif( isset( $arrValues['meter_start_read'] ) ) $this->setMeterStartRead( $arrValues['meter_start_read'] );
		if( isset( $arrValues['meter_end_read'] ) && $boolDirectSet ) $this->set( 'm_intMeterEndRead', trim( $arrValues['meter_end_read'] ) ); elseif( isset( $arrValues['meter_end_read'] ) ) $this->setMeterEndRead( $arrValues['meter_end_read'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['is_adjusted'] ) && $boolDirectSet ) $this->set( 'm_boolIsAdjusted', trim( stripcslashes( $arrValues['is_adjusted'] ) ) ); elseif( isset( $arrValues['is_adjusted'] ) ) $this->setIsAdjusted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_adjusted'] ) : $arrValues['is_adjusted'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUtilityTransmissionId( $intUtilityTransmissionId ) {
		$this->set( 'm_intUtilityTransmissionId', CStrings::strToIntDef( $intUtilityTransmissionId, NULL, false ) );
	}

	public function getUtilityTransmissionId() {
		return $this->m_intUtilityTransmissionId;
	}

	public function sqlUtilityTransmissionId() {
		return ( true == isset( $this->m_intUtilityTransmissionId ) ) ? ( string ) $this->m_intUtilityTransmissionId : 'NULL';
	}

	public function setPropertyMeterDeviceId( $intPropertyMeterDeviceId ) {
		$this->set( 'm_intPropertyMeterDeviceId', CStrings::strToIntDef( $intPropertyMeterDeviceId, NULL, false ) );
	}

	public function getPropertyMeterDeviceId() {
		return $this->m_intPropertyMeterDeviceId;
	}

	public function sqlPropertyMeterDeviceId() {
		return ( true == isset( $this->m_intPropertyMeterDeviceId ) ) ? ( string ) $this->m_intPropertyMeterDeviceId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setUtilityTransactionId( $intUtilityTransactionId ) {
		$this->set( 'm_intUtilityTransactionId', CStrings::strToIntDef( $intUtilityTransactionId, NULL, false ) );
	}

	public function getUtilityTransactionId() {
		return $this->m_intUtilityTransactionId;
	}

	public function sqlUtilityTransactionId() {
		return ( true == isset( $this->m_intUtilityTransactionId ) ) ? ( string ) $this->m_intUtilityTransactionId : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setBuildingNumber( $strBuildingNumber ) {
		$this->set( 'm_strBuildingNumber', CStrings::strTrimDef( $strBuildingNumber, 50, NULL, true ) );
	}

	public function getBuildingNumber() {
		return $this->m_strBuildingNumber;
	}

	public function sqlBuildingNumber() {
		return ( true == isset( $this->m_strBuildingNumber ) ) ? '\'' . addslashes( $this->m_strBuildingNumber ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setTransmissionDatetime( $strTransmissionDatetime ) {
		$this->set( 'm_strTransmissionDatetime', CStrings::strTrimDef( $strTransmissionDatetime, -1, NULL, true ) );
	}

	public function getTransmissionDatetime() {
		return $this->m_strTransmissionDatetime;
	}

	public function sqlTransmissionDatetime() {
		return ( true == isset( $this->m_strTransmissionDatetime ) ) ? '\'' . $this->m_strTransmissionDatetime . '\'' : 'NOW()';
	}

	public function setUtilityTypeCode( $strUtilityTypeCode ) {
		$this->set( 'm_strUtilityTypeCode', CStrings::strTrimDef( $strUtilityTypeCode, 10, NULL, true ) );
	}

	public function getUtilityTypeCode() {
		return $this->m_strUtilityTypeCode;
	}

	public function sqlUtilityTypeCode() {
		return ( true == isset( $this->m_strUtilityTypeCode ) ) ? '\'' . addslashes( $this->m_strUtilityTypeCode ) . '\'' : 'NULL';
	}

	public function setMeterNumber( $strMeterNumber ) {
		$this->set( 'm_strMeterNumber', CStrings::strTrimDef( $strMeterNumber, 20, NULL, true ) );
	}

	public function getMeterNumber() {
		return $this->m_strMeterNumber;
	}

	public function sqlMeterNumber() {
		return ( true == isset( $this->m_strMeterNumber ) ) ? '\'' . addslashes( $this->m_strMeterNumber ) . '\'' : 'NULL';
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->set( 'm_strStartDatetime', CStrings::strTrimDef( $strStartDatetime, -1, NULL, true ) );
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function sqlStartDatetime() {
		return ( true == isset( $this->m_strStartDatetime ) ) ? '\'' . $this->m_strStartDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setMeterStartRead( $intMeterStartRead ) {
		$this->set( 'm_intMeterStartRead', CStrings::strToIntDef( $intMeterStartRead, NULL, false ) );
	}

	public function getMeterStartRead() {
		return $this->m_intMeterStartRead;
	}

	public function sqlMeterStartRead() {
		return ( true == isset( $this->m_intMeterStartRead ) ) ? ( string ) $this->m_intMeterStartRead : 'NULL';
	}

	public function setMeterEndRead( $intMeterEndRead ) {
		$this->set( 'm_intMeterEndRead', CStrings::strToIntDef( $intMeterEndRead, NULL, false ) );
	}

	public function getMeterEndRead() {
		return $this->m_intMeterEndRead;
	}

	public function sqlMeterEndRead() {
		return ( true == isset( $this->m_intMeterEndRead ) ) ? ( string ) $this->m_intMeterEndRead : 'NULL';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, 10, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? '\'' . addslashes( $this->m_strStatus ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setIsAdjusted( $boolIsAdjusted ) {
		$this->set( 'm_boolIsAdjusted', CStrings::strToBool( $boolIsAdjusted ) );
	}

	public function getIsAdjusted() {
		return $this->m_boolIsAdjusted;
	}

	public function sqlIsAdjusted() {
		return ( true == isset( $this->m_boolIsAdjusted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAdjusted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, utility_transmission_id, property_meter_device_id, property_id, property_building_id, property_unit_id, unit_space_id, utility_transaction_id, unit_number, building_number, remote_primary_key, transmission_datetime, utility_type_code, meter_number, start_datetime, end_datetime, meter_start_read, meter_end_read, status, notes, is_adjusted, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlUtilityTransmissionId() . ', ' .
 						$this->sqlPropertyMeterDeviceId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyBuildingId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlUtilityTransactionId() . ', ' .
 						$this->sqlUnitNumber() . ', ' .
 						$this->sqlBuildingNumber() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlTransmissionDatetime() . ', ' .
 						$this->sqlUtilityTypeCode() . ', ' .
 						$this->sqlMeterNumber() . ', ' .
 						$this->sqlStartDatetime() . ', ' .
 						$this->sqlEndDatetime() . ', ' .
 						$this->sqlMeterStartRead() . ', ' .
 						$this->sqlMeterEndRead() . ', ' .
 						$this->sqlStatus() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlIsAdjusted() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_transmission_id = ' . $this->sqlUtilityTransmissionId() . ','; } elseif( true == array_key_exists( 'UtilityTransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_transmission_id = ' . $this->sqlUtilityTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_meter_device_id = ' . $this->sqlPropertyMeterDeviceId() . ','; } elseif( true == array_key_exists( 'PropertyMeterDeviceId', $this->getChangedColumns() ) ) { $strSql .= ' property_meter_device_id = ' . $this->sqlPropertyMeterDeviceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_transaction_id = ' . $this->sqlUtilityTransactionId() . ','; } elseif( true == array_key_exists( 'UtilityTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_transaction_id = ' . $this->sqlUtilityTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_number = ' . $this->sqlBuildingNumber() . ','; } elseif( true == array_key_exists( 'BuildingNumber', $this->getChangedColumns() ) ) { $strSql .= ' building_number = ' . $this->sqlBuildingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_datetime = ' . $this->sqlTransmissionDatetime() . ','; } elseif( true == array_key_exists( 'TransmissionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transmission_datetime = ' . $this->sqlTransmissionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_code = ' . $this->sqlUtilityTypeCode() . ','; } elseif( true == array_key_exists( 'UtilityTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_code = ' . $this->sqlUtilityTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_number = ' . $this->sqlMeterNumber() . ','; } elseif( true == array_key_exists( 'MeterNumber', $this->getChangedColumns() ) ) { $strSql .= ' meter_number = ' . $this->sqlMeterNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; } elseif( true == array_key_exists( 'StartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_start_read = ' . $this->sqlMeterStartRead() . ','; } elseif( true == array_key_exists( 'MeterStartRead', $this->getChangedColumns() ) ) { $strSql .= ' meter_start_read = ' . $this->sqlMeterStartRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_end_read = ' . $this->sqlMeterEndRead() . ','; } elseif( true == array_key_exists( 'MeterEndRead', $this->getChangedColumns() ) ) { $strSql .= ' meter_end_read = ' . $this->sqlMeterEndRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_adjusted = ' . $this->sqlIsAdjusted() . ','; } elseif( true == array_key_exists( 'IsAdjusted', $this->getChangedColumns() ) ) { $strSql .= ' is_adjusted = ' . $this->sqlIsAdjusted() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'utility_transmission_id' => $this->getUtilityTransmissionId(),
			'property_meter_device_id' => $this->getPropertyMeterDeviceId(),
			'property_id' => $this->getPropertyId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'utility_transaction_id' => $this->getUtilityTransactionId(),
			'unit_number' => $this->getUnitNumber(),
			'building_number' => $this->getBuildingNumber(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'transmission_datetime' => $this->getTransmissionDatetime(),
			'utility_type_code' => $this->getUtilityTypeCode(),
			'meter_number' => $this->getMeterNumber(),
			'start_datetime' => $this->getStartDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'meter_start_read' => $this->getMeterStartRead(),
			'meter_end_read' => $this->getMeterEndRead(),
			'status' => $this->getStatus(),
			'notes' => $this->getNotes(),
			'is_adjusted' => $this->getIsAdjusted(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>