<?php

class CBaseDataEntryAudit extends CEosSingularBase {

	const TABLE_NAME = 'public.data_entry_audits';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUtilityBillId;
	protected $m_strDataEntryAuditDatetime;
	protected $m_strStartAuditDatetime;
	protected $m_strEndAuditDatetime;
	protected $m_intLastAuditedBy;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intCorrectDataPoints;
	protected $m_intTotalDataPoints;
	protected $m_boolIsError;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsError = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['data_entry_audit_datetime'] ) && $boolDirectSet ) $this->set( 'm_strDataEntryAuditDatetime', trim( $arrValues['data_entry_audit_datetime'] ) ); elseif( isset( $arrValues['data_entry_audit_datetime'] ) ) $this->setDataEntryAuditDatetime( $arrValues['data_entry_audit_datetime'] );
		if( isset( $arrValues['start_audit_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartAuditDatetime', trim( $arrValues['start_audit_datetime'] ) ); elseif( isset( $arrValues['start_audit_datetime'] ) ) $this->setStartAuditDatetime( $arrValues['start_audit_datetime'] );
		if( isset( $arrValues['end_audit_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndAuditDatetime', trim( $arrValues['end_audit_datetime'] ) ); elseif( isset( $arrValues['end_audit_datetime'] ) ) $this->setEndAuditDatetime( $arrValues['end_audit_datetime'] );
		if( isset( $arrValues['last_audited_by'] ) && $boolDirectSet ) $this->set( 'm_intLastAuditedBy', trim( $arrValues['last_audited_by'] ) ); elseif( isset( $arrValues['last_audited_by'] ) ) $this->setLastAuditedBy( $arrValues['last_audited_by'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['correct_data_points'] ) && $boolDirectSet ) $this->set( 'm_intCorrectDataPoints', trim( $arrValues['correct_data_points'] ) ); elseif( isset( $arrValues['correct_data_points'] ) ) $this->setCorrectDataPoints( $arrValues['correct_data_points'] );
		if( isset( $arrValues['total_data_points'] ) && $boolDirectSet ) $this->set( 'm_intTotalDataPoints', trim( $arrValues['total_data_points'] ) ); elseif( isset( $arrValues['total_data_points'] ) ) $this->setTotalDataPoints( $arrValues['total_data_points'] );
		if( isset( $arrValues['is_error'] ) && $boolDirectSet ) $this->set( 'm_boolIsError', trim( stripcslashes( $arrValues['is_error'] ) ) ); elseif( isset( $arrValues['is_error'] ) ) $this->setIsError( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_error'] ) : $arrValues['is_error'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setDataEntryAuditDatetime( $strDataEntryAuditDatetime ) {
		$this->set( 'm_strDataEntryAuditDatetime', CStrings::strTrimDef( $strDataEntryAuditDatetime, -1, NULL, true ) );
	}

	public function getDataEntryAuditDatetime() {
		return $this->m_strDataEntryAuditDatetime;
	}

	public function sqlDataEntryAuditDatetime() {
		return ( true == isset( $this->m_strDataEntryAuditDatetime ) ) ? '\'' . $this->m_strDataEntryAuditDatetime . '\'' : 'NOW()';
	}

	public function setStartAuditDatetime( $strStartAuditDatetime ) {
		$this->set( 'm_strStartAuditDatetime', CStrings::strTrimDef( $strStartAuditDatetime, -1, NULL, true ) );
	}

	public function getStartAuditDatetime() {
		return $this->m_strStartAuditDatetime;
	}

	public function sqlStartAuditDatetime() {
		return ( true == isset( $this->m_strStartAuditDatetime ) ) ? '\'' . $this->m_strStartAuditDatetime . '\'' : 'NULL';
	}

	public function setEndAuditDatetime( $strEndAuditDatetime ) {
		$this->set( 'm_strEndAuditDatetime', CStrings::strTrimDef( $strEndAuditDatetime, -1, NULL, true ) );
	}

	public function getEndAuditDatetime() {
		return $this->m_strEndAuditDatetime;
	}

	public function sqlEndAuditDatetime() {
		return ( true == isset( $this->m_strEndAuditDatetime ) ) ? '\'' . $this->m_strEndAuditDatetime . '\'' : 'NULL';
	}

	public function setLastAuditedBy( $intLastAuditedBy ) {
		$this->set( 'm_intLastAuditedBy', CStrings::strToIntDef( $intLastAuditedBy, NULL, false ) );
	}

	public function getLastAuditedBy() {
		return $this->m_intLastAuditedBy;
	}

	public function sqlLastAuditedBy() {
		return ( true == isset( $this->m_intLastAuditedBy ) ) ? ( string ) $this->m_intLastAuditedBy : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setCorrectDataPoints( $intCorrectDataPoints ) {
		$this->set( 'm_intCorrectDataPoints', CStrings::strToIntDef( $intCorrectDataPoints, NULL, false ) );
	}

	public function getCorrectDataPoints() {
		return $this->m_intCorrectDataPoints;
	}

	public function sqlCorrectDataPoints() {
		return ( true == isset( $this->m_intCorrectDataPoints ) ) ? ( string ) $this->m_intCorrectDataPoints : 'NULL';
	}

	public function setTotalDataPoints( $intTotalDataPoints ) {
		$this->set( 'm_intTotalDataPoints', CStrings::strToIntDef( $intTotalDataPoints, NULL, false ) );
	}

	public function getTotalDataPoints() {
		return $this->m_intTotalDataPoints;
	}

	public function sqlTotalDataPoints() {
		return ( true == isset( $this->m_intTotalDataPoints ) ) ? ( string ) $this->m_intTotalDataPoints : 'NULL';
	}

	public function setIsError( $boolIsError ) {
		$this->set( 'm_boolIsError', CStrings::strToBool( $boolIsError ) );
	}

	public function getIsError() {
		return $this->m_boolIsError;
	}

	public function sqlIsError() {
		return ( true == isset( $this->m_boolIsError ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsError ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, utility_bill_id, data_entry_audit_datetime, start_audit_datetime, end_audit_datetime, last_audited_by, completed_by, completed_on, correct_data_points, total_data_points, is_error, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlDataEntryAuditDatetime() . ', ' .
						$this->sqlStartAuditDatetime() . ', ' .
						$this->sqlEndAuditDatetime() . ', ' .
						$this->sqlLastAuditedBy() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlCorrectDataPoints() . ', ' .
						$this->sqlTotalDataPoints() . ', ' .
						$this->sqlIsError() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_entry_audit_datetime = ' . $this->sqlDataEntryAuditDatetime(). ',' ; } elseif( true == array_key_exists( 'DataEntryAuditDatetime', $this->getChangedColumns() ) ) { $strSql .= ' data_entry_audit_datetime = ' . $this->sqlDataEntryAuditDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_audit_datetime = ' . $this->sqlStartAuditDatetime(). ',' ; } elseif( true == array_key_exists( 'StartAuditDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_audit_datetime = ' . $this->sqlStartAuditDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_audit_datetime = ' . $this->sqlEndAuditDatetime(). ',' ; } elseif( true == array_key_exists( 'EndAuditDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_audit_datetime = ' . $this->sqlEndAuditDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_audited_by = ' . $this->sqlLastAuditedBy(). ',' ; } elseif( true == array_key_exists( 'LastAuditedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_audited_by = ' . $this->sqlLastAuditedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' correct_data_points = ' . $this->sqlCorrectDataPoints(). ',' ; } elseif( true == array_key_exists( 'CorrectDataPoints', $this->getChangedColumns() ) ) { $strSql .= ' correct_data_points = ' . $this->sqlCorrectDataPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_data_points = ' . $this->sqlTotalDataPoints(). ',' ; } elseif( true == array_key_exists( 'TotalDataPoints', $this->getChangedColumns() ) ) { $strSql .= ' total_data_points = ' . $this->sqlTotalDataPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_error = ' . $this->sqlIsError(). ',' ; } elseif( true == array_key_exists( 'IsError', $this->getChangedColumns() ) ) { $strSql .= ' is_error = ' . $this->sqlIsError() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'data_entry_audit_datetime' => $this->getDataEntryAuditDatetime(),
			'start_audit_datetime' => $this->getStartAuditDatetime(),
			'end_audit_datetime' => $this->getEndAuditDatetime(),
			'last_audited_by' => $this->getLastAuditedBy(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'correct_data_points' => $this->getCorrectDataPoints(),
			'total_data_points' => $this->getTotalDataPoints(),
			'is_error' => $this->getIsError(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>