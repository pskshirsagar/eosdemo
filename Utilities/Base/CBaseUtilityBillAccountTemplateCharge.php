<?php

class CBaseUtilityBillAccountTemplateCharge extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_account_template_charges';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillAccountId;
	protected $m_intUtilityBillTemplateChargeId;
	protected $m_strName;
	protected $m_fltDefaultAmount;
	protected $m_boolIsRecovered;
	protected $m_boolIsBaseFee;
	protected $m_intOrderNum;
	protected $m_boolIsIgnored;
	protected $m_boolIsDemandCharge;
	protected $m_boolIsTimeOfUseCharge;
	protected $m_boolIsLoadFactorPenalty;
	protected $m_boolIsTax;
	protected $m_boolIsCredit;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intCommodityId;
	protected $m_intPropertyUtilityTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsRecovered = true;
		$this->m_boolIsBaseFee = false;
		$this->m_intOrderNum = '0';
		$this->m_boolIsIgnored = false;
		$this->m_boolIsDemandCharge = false;
		$this->m_boolIsTimeOfUseCharge = false;
		$this->m_boolIsLoadFactorPenalty = false;
		$this->m_boolIsTax = false;
		$this->m_boolIsCredit = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['utility_bill_template_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillTemplateChargeId', trim( $arrValues['utility_bill_template_charge_id'] ) ); elseif( isset( $arrValues['utility_bill_template_charge_id'] ) ) $this->setUtilityBillTemplateChargeId( $arrValues['utility_bill_template_charge_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['default_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDefaultAmount', trim( $arrValues['default_amount'] ) ); elseif( isset( $arrValues['default_amount'] ) ) $this->setDefaultAmount( $arrValues['default_amount'] );
		if( isset( $arrValues['is_recovered'] ) && $boolDirectSet ) $this->set( 'm_boolIsRecovered', trim( stripcslashes( $arrValues['is_recovered'] ) ) ); elseif( isset( $arrValues['is_recovered'] ) ) $this->setIsRecovered( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_recovered'] ) : $arrValues['is_recovered'] );
		if( isset( $arrValues['is_base_fee'] ) && $boolDirectSet ) $this->set( 'm_boolIsBaseFee', trim( stripcslashes( $arrValues['is_base_fee'] ) ) ); elseif( isset( $arrValues['is_base_fee'] ) ) $this->setIsBaseFee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_base_fee'] ) : $arrValues['is_base_fee'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsIgnored', trim( stripcslashes( $arrValues['is_ignored'] ) ) ); elseif( isset( $arrValues['is_ignored'] ) ) $this->setIsIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ignored'] ) : $arrValues['is_ignored'] );
		if( isset( $arrValues['is_demand_charge'] ) && $boolDirectSet ) $this->set( 'm_boolIsDemandCharge', trim( stripcslashes( $arrValues['is_demand_charge'] ) ) ); elseif( isset( $arrValues['is_demand_charge'] ) ) $this->setIsDemandCharge( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_demand_charge'] ) : $arrValues['is_demand_charge'] );
		if( isset( $arrValues['is_time_of_use_charge'] ) && $boolDirectSet ) $this->set( 'm_boolIsTimeOfUseCharge', trim( stripcslashes( $arrValues['is_time_of_use_charge'] ) ) ); elseif( isset( $arrValues['is_time_of_use_charge'] ) ) $this->setIsTimeOfUseCharge( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_time_of_use_charge'] ) : $arrValues['is_time_of_use_charge'] );
		if( isset( $arrValues['is_load_factor_penalty'] ) && $boolDirectSet ) $this->set( 'm_boolIsLoadFactorPenalty', trim( stripcslashes( $arrValues['is_load_factor_penalty'] ) ) ); elseif( isset( $arrValues['is_load_factor_penalty'] ) ) $this->setIsLoadFactorPenalty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_load_factor_penalty'] ) : $arrValues['is_load_factor_penalty'] );
		if( isset( $arrValues['is_tax'] ) && $boolDirectSet ) $this->set( 'm_boolIsTax', trim( stripcslashes( $arrValues['is_tax'] ) ) ); elseif( isset( $arrValues['is_tax'] ) ) $this->setIsTax( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_tax'] ) : $arrValues['is_tax'] );
		if( isset( $arrValues['is_credit'] ) && $boolDirectSet ) $this->set( 'm_boolIsCredit', trim( stripcslashes( $arrValues['is_credit'] ) ) ); elseif( isset( $arrValues['is_credit'] ) ) $this->setIsCredit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_credit'] ) : $arrValues['is_credit'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['commodity_id'] ) && $boolDirectSet ) $this->set( 'm_intCommodityId', trim( $arrValues['commodity_id'] ) ); elseif( isset( $arrValues['commodity_id'] ) ) $this->setCommodityId( $arrValues['commodity_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setUtilityBillTemplateChargeId( $intUtilityBillTemplateChargeId ) {
		$this->set( 'm_intUtilityBillTemplateChargeId', CStrings::strToIntDef( $intUtilityBillTemplateChargeId, NULL, false ) );
	}

	public function getUtilityBillTemplateChargeId() {
		return $this->m_intUtilityBillTemplateChargeId;
	}

	public function sqlUtilityBillTemplateChargeId() {
		return ( true == isset( $this->m_intUtilityBillTemplateChargeId ) ) ? ( string ) $this->m_intUtilityBillTemplateChargeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 2000, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDefaultAmount( $fltDefaultAmount ) {
		$this->set( 'm_fltDefaultAmount', CStrings::strToFloatDef( $fltDefaultAmount, NULL, false, 2 ) );
	}

	public function getDefaultAmount() {
		return $this->m_fltDefaultAmount;
	}

	public function sqlDefaultAmount() {
		return ( true == isset( $this->m_fltDefaultAmount ) ) ? ( string ) $this->m_fltDefaultAmount : 'NULL';
	}

	public function setIsRecovered( $boolIsRecovered ) {
		$this->set( 'm_boolIsRecovered', CStrings::strToBool( $boolIsRecovered ) );
	}

	public function getIsRecovered() {
		return $this->m_boolIsRecovered;
	}

	public function sqlIsRecovered() {
		return ( true == isset( $this->m_boolIsRecovered ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRecovered ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsBaseFee( $boolIsBaseFee ) {
		$this->set( 'm_boolIsBaseFee', CStrings::strToBool( $boolIsBaseFee ) );
	}

	public function getIsBaseFee() {
		return $this->m_boolIsBaseFee;
	}

	public function sqlIsBaseFee() {
		return ( true == isset( $this->m_boolIsBaseFee ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBaseFee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setIsIgnored( $boolIsIgnored ) {
		$this->set( 'm_boolIsIgnored', CStrings::strToBool( $boolIsIgnored ) );
	}

	public function getIsIgnored() {
		return $this->m_boolIsIgnored;
	}

	public function sqlIsIgnored() {
		return ( true == isset( $this->m_boolIsIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDemandCharge( $boolIsDemandCharge ) {
		$this->set( 'm_boolIsDemandCharge', CStrings::strToBool( $boolIsDemandCharge ) );
	}

	public function getIsDemandCharge() {
		return $this->m_boolIsDemandCharge;
	}

	public function sqlIsDemandCharge() {
		return ( true == isset( $this->m_boolIsDemandCharge ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDemandCharge ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTimeOfUseCharge( $boolIsTimeOfUseCharge ) {
		$this->set( 'm_boolIsTimeOfUseCharge', CStrings::strToBool( $boolIsTimeOfUseCharge ) );
	}

	public function getIsTimeOfUseCharge() {
		return $this->m_boolIsTimeOfUseCharge;
	}

	public function sqlIsTimeOfUseCharge() {
		return ( true == isset( $this->m_boolIsTimeOfUseCharge ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTimeOfUseCharge ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLoadFactorPenalty( $boolIsLoadFactorPenalty ) {
		$this->set( 'm_boolIsLoadFactorPenalty', CStrings::strToBool( $boolIsLoadFactorPenalty ) );
	}

	public function getIsLoadFactorPenalty() {
		return $this->m_boolIsLoadFactorPenalty;
	}

	public function sqlIsLoadFactorPenalty() {
		return ( true == isset( $this->m_boolIsLoadFactorPenalty ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLoadFactorPenalty ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTax( $boolIsTax ) {
		$this->set( 'm_boolIsTax', CStrings::strToBool( $boolIsTax ) );
	}

	public function getIsTax() {
		return $this->m_boolIsTax;
	}

	public function sqlIsTax() {
		return ( true == isset( $this->m_boolIsTax ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTax ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCredit( $boolIsCredit ) {
		$this->set( 'm_boolIsCredit', CStrings::strToBool( $boolIsCredit ) );
	}

	public function getIsCredit() {
		return $this->m_boolIsCredit;
	}

	public function sqlIsCredit() {
		return ( true == isset( $this->m_boolIsCredit ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCredit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCommodityId( $intCommodityId ) {
		$this->set( 'm_intCommodityId', CStrings::strToIntDef( $intCommodityId, NULL, false ) );
	}

	public function getCommodityId() {
		return $this->m_intCommodityId;
	}

	public function sqlCommodityId() {
		return ( true == isset( $this->m_intCommodityId ) ) ? ( string ) $this->m_intCommodityId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_account_id, utility_bill_template_charge_id, name, default_amount, is_recovered, is_base_fee, order_num, is_ignored, is_demand_charge, is_time_of_use_charge, is_load_factor_penalty, is_tax, is_credit, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, commodity_id, property_utility_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityBillAccountId() . ', ' .
						$this->sqlUtilityBillTemplateChargeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDefaultAmount() . ', ' .
						$this->sqlIsRecovered() . ', ' .
						$this->sqlIsBaseFee() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlIsIgnored() . ', ' .
						$this->sqlIsDemandCharge() . ', ' .
						$this->sqlIsTimeOfUseCharge() . ', ' .
						$this->sqlIsLoadFactorPenalty() . ', ' .
						$this->sqlIsTax() . ', ' .
						$this->sqlIsCredit() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCommodityId() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_template_charge_id = ' . $this->sqlUtilityBillTemplateChargeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillTemplateChargeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_template_charge_id = ' . $this->sqlUtilityBillTemplateChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_amount = ' . $this->sqlDefaultAmount(). ',' ; } elseif( true == array_key_exists( 'DefaultAmount', $this->getChangedColumns() ) ) { $strSql .= ' default_amount = ' . $this->sqlDefaultAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recovered = ' . $this->sqlIsRecovered(). ',' ; } elseif( true == array_key_exists( 'IsRecovered', $this->getChangedColumns() ) ) { $strSql .= ' is_recovered = ' . $this->sqlIsRecovered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_base_fee = ' . $this->sqlIsBaseFee(). ',' ; } elseif( true == array_key_exists( 'IsBaseFee', $this->getChangedColumns() ) ) { $strSql .= ' is_base_fee = ' . $this->sqlIsBaseFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ignored = ' . $this->sqlIsIgnored(). ',' ; } elseif( true == array_key_exists( 'IsIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_ignored = ' . $this->sqlIsIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_demand_charge = ' . $this->sqlIsDemandCharge(). ',' ; } elseif( true == array_key_exists( 'IsDemandCharge', $this->getChangedColumns() ) ) { $strSql .= ' is_demand_charge = ' . $this->sqlIsDemandCharge() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_time_of_use_charge = ' . $this->sqlIsTimeOfUseCharge(). ',' ; } elseif( true == array_key_exists( 'IsTimeOfUseCharge', $this->getChangedColumns() ) ) { $strSql .= ' is_time_of_use_charge = ' . $this->sqlIsTimeOfUseCharge() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_load_factor_penalty = ' . $this->sqlIsLoadFactorPenalty(). ',' ; } elseif( true == array_key_exists( 'IsLoadFactorPenalty', $this->getChangedColumns() ) ) { $strSql .= ' is_load_factor_penalty = ' . $this->sqlIsLoadFactorPenalty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_tax = ' . $this->sqlIsTax(). ',' ; } elseif( true == array_key_exists( 'IsTax', $this->getChangedColumns() ) ) { $strSql .= ' is_tax = ' . $this->sqlIsTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_credit = ' . $this->sqlIsCredit(). ',' ; } elseif( true == array_key_exists( 'IsCredit', $this->getChangedColumns() ) ) { $strSql .= ' is_credit = ' . $this->sqlIsCredit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commodity_id = ' . $this->sqlCommodityId(). ',' ; } elseif( true == array_key_exists( 'CommodityId', $this->getChangedColumns() ) ) { $strSql .= ' commodity_id = ' . $this->sqlCommodityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'utility_bill_template_charge_id' => $this->getUtilityBillTemplateChargeId(),
			'name' => $this->getName(),
			'default_amount' => $this->getDefaultAmount(),
			'is_recovered' => $this->getIsRecovered(),
			'is_base_fee' => $this->getIsBaseFee(),
			'order_num' => $this->getOrderNum(),
			'is_ignored' => $this->getIsIgnored(),
			'is_demand_charge' => $this->getIsDemandCharge(),
			'is_time_of_use_charge' => $this->getIsTimeOfUseCharge(),
			'is_load_factor_penalty' => $this->getIsLoadFactorPenalty(),
			'is_tax' => $this->getIsTax(),
			'is_credit' => $this->getIsCredit(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'commodity_id' => $this->getCommodityId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId()
		);
	}

}
?>