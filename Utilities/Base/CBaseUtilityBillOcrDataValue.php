<?php

class CBaseUtilityBillOcrDataValue extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_ocr_data_values';

	protected $m_intId;
	protected $m_intUtilityBillId;
	protected $m_intOcrFieldId;
	protected $m_strCapturedLabel;
	protected $m_strCapturedValue;
	protected $m_strLabelVertices;
	protected $m_jsonLabelVertices;
	protected $m_strValueVertices;
	protected $m_jsonValueVertices;
	protected $m_strProcessedOn;
	protected $m_strNotes;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['ocr_field_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrFieldId', trim( $arrValues['ocr_field_id'] ) ); elseif( isset( $arrValues['ocr_field_id'] ) ) $this->setOcrFieldId( $arrValues['ocr_field_id'] );
		if( isset( $arrValues['captured_label'] ) && $boolDirectSet ) $this->set( 'm_strCapturedLabel', trim( $arrValues['captured_label'] ) ); elseif( isset( $arrValues['captured_label'] ) ) $this->setCapturedLabel( $arrValues['captured_label'] );
		if( isset( $arrValues['captured_value'] ) && $boolDirectSet ) $this->set( 'm_strCapturedValue', trim( $arrValues['captured_value'] ) ); elseif( isset( $arrValues['captured_value'] ) ) $this->setCapturedValue( $arrValues['captured_value'] );
		if( isset( $arrValues['label_vertices'] ) ) $this->set( 'm_strLabelVertices', trim( $arrValues['label_vertices'] ) );
		if( isset( $arrValues['value_vertices'] ) ) $this->set( 'm_strValueVertices', trim( $arrValues['value_vertices'] ) );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setOcrFieldId( $intOcrFieldId ) {
		$this->set( 'm_intOcrFieldId', CStrings::strToIntDef( $intOcrFieldId, NULL, false ) );
	}

	public function getOcrFieldId() {
		return $this->m_intOcrFieldId;
	}

	public function sqlOcrFieldId() {
		return ( true == isset( $this->m_intOcrFieldId ) ) ? ( string ) $this->m_intOcrFieldId : 'NULL';
	}

	public function setCapturedLabel( $strCapturedLabel ) {
		$this->set( 'm_strCapturedLabel', CStrings::strTrimDef( $strCapturedLabel, 250, NULL, true ) );
	}

	public function getCapturedLabel() {
		return $this->m_strCapturedLabel;
	}

	public function sqlCapturedLabel() {
		return ( true == isset( $this->m_strCapturedLabel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCapturedLabel ) : '\'' . addslashes( $this->m_strCapturedLabel ) . '\'' ) : 'NULL';
	}

	public function setCapturedValue( $strCapturedValue ) {
		$this->set( 'm_strCapturedValue', CStrings::strTrimDef( $strCapturedValue, 250, NULL, true ) );
	}

	public function getCapturedValue() {
		return $this->m_strCapturedValue;
	}

	public function sqlCapturedValue() {
		return ( true == isset( $this->m_strCapturedValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCapturedValue ) : '\'' . addslashes( $this->m_strCapturedValue ) . '\'' ) : 'NULL';
	}

	public function setLabelVertices( $jsonLabelVertices ) {
		if( true == valObj( $jsonLabelVertices, 'stdClass' ) ) {
			$this->set( 'm_jsonLabelVertices', $jsonLabelVertices );
		} elseif( true == valJsonString( $jsonLabelVertices ) ) {
			$this->set( 'm_jsonLabelVertices', CStrings::strToJson( $jsonLabelVertices ) );
		} else {
			$this->set( 'm_jsonLabelVertices', NULL ); 
		}
		unset( $this->m_strLabelVertices );
	}

	public function getLabelVertices() {
		if( true == isset( $this->m_strLabelVertices ) ) {
			$this->m_jsonLabelVertices = CStrings::strToJson( $this->m_strLabelVertices );
			unset( $this->m_strLabelVertices );
		}
		return $this->m_jsonLabelVertices;
	}

	public function sqlLabelVertices() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getLabelVertices() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getLabelVertices() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getLabelVertices() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setValueVertices( $jsonValueVertices ) {
		if( true == valObj( $jsonValueVertices, 'stdClass' ) ) {
			$this->set( 'm_jsonValueVertices', $jsonValueVertices );
		} elseif( true == valJsonString( $jsonValueVertices ) ) {
			$this->set( 'm_jsonValueVertices', CStrings::strToJson( $jsonValueVertices ) );
		} else {
			$this->set( 'm_jsonValueVertices', NULL ); 
		}
		unset( $this->m_strValueVertices );
	}

	public function getValueVertices() {
		if( true == isset( $this->m_strValueVertices ) ) {
			$this->m_jsonValueVertices = CStrings::strToJson( $this->m_strValueVertices );
			unset( $this->m_strValueVertices );
		}
		return $this->m_jsonValueVertices;
	}

	public function sqlValueVertices() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getValueVertices() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getValueVertices() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getValueVertices() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NOW()';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 500, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_bill_id, ocr_field_id, captured_label, captured_value, label_vertices, value_vertices, processed_on, notes, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlOcrFieldId() . ', ' .
						$this->sqlCapturedLabel() . ', ' .
						$this->sqlCapturedValue() . ', ' .
						$this->sqlLabelVertices() . ', ' .
						$this->sqlValueVertices() . ', ' .
						$this->sqlProcessedOn() . ', ' .
						$this->sqlNotes() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_field_id = ' . $this->sqlOcrFieldId(). ',' ; } elseif( true == array_key_exists( 'OcrFieldId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_field_id = ' . $this->sqlOcrFieldId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' captured_label = ' . $this->sqlCapturedLabel(). ',' ; } elseif( true == array_key_exists( 'CapturedLabel', $this->getChangedColumns() ) ) { $strSql .= ' captured_label = ' . $this->sqlCapturedLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' captured_value = ' . $this->sqlCapturedValue(). ',' ; } elseif( true == array_key_exists( 'CapturedValue', $this->getChangedColumns() ) ) { $strSql .= ' captured_value = ' . $this->sqlCapturedValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label_vertices = ' . $this->sqlLabelVertices(). ',' ; } elseif( true == array_key_exists( 'LabelVertices', $this->getChangedColumns() ) ) { $strSql .= ' label_vertices = ' . $this->sqlLabelVertices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value_vertices = ' . $this->sqlValueVertices(). ',' ; } elseif( true == array_key_exists( 'ValueVertices', $this->getChangedColumns() ) ) { $strSql .= ' value_vertices = ' . $this->sqlValueVertices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn(). ',' ; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'ocr_field_id' => $this->getOcrFieldId(),
			'captured_label' => $this->getCapturedLabel(),
			'captured_value' => $this->getCapturedValue(),
			'label_vertices' => $this->getLabelVertices(),
			'value_vertices' => $this->getValueVertices(),
			'processed_on' => $this->getProcessedOn(),
			'notes' => $this->getNotes(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>