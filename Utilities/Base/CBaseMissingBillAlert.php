<?php

class CBaseMissingBillAlert extends CEosSingularBase {

	const TABLE_NAME = 'public.missing_bill_alerts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intApPayeeAccountId;
	protected $m_intUtilityBillAccountId;
	protected $m_intResolvedBillId;
	protected $m_strAlertDatetime;
	protected $m_strReminderDatetime;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;
	protected $m_strExpectedBillDate;
	protected $m_strServicePeriodStartDate;
	protected $m_strExpectedDueDate;
	protected $m_intChildUtilityBillAccountId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payee_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeAccountId', trim( $arrValues['ap_payee_account_id'] ) ); elseif( isset( $arrValues['ap_payee_account_id'] ) ) $this->setApPayeeAccountId( $arrValues['ap_payee_account_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['resolved_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intResolvedBillId', trim( $arrValues['resolved_bill_id'] ) ); elseif( isset( $arrValues['resolved_bill_id'] ) ) $this->setResolvedBillId( $arrValues['resolved_bill_id'] );
		if( isset( $arrValues['alert_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAlertDatetime', trim( $arrValues['alert_datetime'] ) ); elseif( isset( $arrValues['alert_datetime'] ) ) $this->setAlertDatetime( $arrValues['alert_datetime'] );
		if( isset( $arrValues['reminder_datetime'] ) && $boolDirectSet ) $this->set( 'm_strReminderDatetime', trim( $arrValues['reminder_datetime'] ) ); elseif( isset( $arrValues['reminder_datetime'] ) ) $this->setReminderDatetime( $arrValues['reminder_datetime'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['expected_bill_date'] ) && $boolDirectSet ) $this->set( 'm_strExpectedBillDate', trim( $arrValues['expected_bill_date'] ) ); elseif( isset( $arrValues['expected_bill_date'] ) ) $this->setExpectedBillDate( $arrValues['expected_bill_date'] );
		if( isset( $arrValues['service_period_start_date'] ) && $boolDirectSet ) $this->set( 'm_strServicePeriodStartDate', trim( $arrValues['service_period_start_date'] ) ); elseif( isset( $arrValues['service_period_start_date'] ) ) $this->setServicePeriodStartDate( $arrValues['service_period_start_date'] );
		if( isset( $arrValues['expected_due_date'] ) && $boolDirectSet ) $this->set( 'm_strExpectedDueDate', trim( $arrValues['expected_due_date'] ) ); elseif( isset( $arrValues['expected_due_date'] ) ) $this->setExpectedDueDate( $arrValues['expected_due_date'] );
		if( isset( $arrValues['child_utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intChildUtilityBillAccountId', trim( $arrValues['child_utility_bill_account_id'] ) ); elseif( isset( $arrValues['child_utility_bill_account_id'] ) ) $this->setChildUtilityBillAccountId( $arrValues['child_utility_bill_account_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPayeeAccountId( $intApPayeeAccountId ) {
		$this->set( 'm_intApPayeeAccountId', CStrings::strToIntDef( $intApPayeeAccountId, NULL, false ) );
	}

	public function getApPayeeAccountId() {
		return $this->m_intApPayeeAccountId;
	}

	public function sqlApPayeeAccountId() {
		return ( true == isset( $this->m_intApPayeeAccountId ) ) ? ( string ) $this->m_intApPayeeAccountId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setResolvedBillId( $intResolvedBillId ) {
		$this->set( 'm_intResolvedBillId', CStrings::strToIntDef( $intResolvedBillId, NULL, false ) );
	}

	public function getResolvedBillId() {
		return $this->m_intResolvedBillId;
	}

	public function sqlResolvedBillId() {
		return ( true == isset( $this->m_intResolvedBillId ) ) ? ( string ) $this->m_intResolvedBillId : 'NULL';
	}

	public function setAlertDatetime( $strAlertDatetime ) {
		$this->set( 'm_strAlertDatetime', CStrings::strTrimDef( $strAlertDatetime, -1, NULL, true ) );
	}

	public function getAlertDatetime() {
		return $this->m_strAlertDatetime;
	}

	public function sqlAlertDatetime() {
		return ( true == isset( $this->m_strAlertDatetime ) ) ? '\'' . $this->m_strAlertDatetime . '\'' : 'NULL';
	}

	public function setReminderDatetime( $strReminderDatetime ) {
		$this->set( 'm_strReminderDatetime', CStrings::strTrimDef( $strReminderDatetime, -1, NULL, true ) );
	}

	public function getReminderDatetime() {
		return $this->m_strReminderDatetime;
	}

	public function sqlReminderDatetime() {
		return ( true == isset( $this->m_strReminderDatetime ) ) ? '\'' . $this->m_strReminderDatetime . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setExpectedBillDate( $strExpectedBillDate ) {
		$this->set( 'm_strExpectedBillDate', CStrings::strTrimDef( $strExpectedBillDate, -1, NULL, true ) );
	}

	public function getExpectedBillDate() {
		return $this->m_strExpectedBillDate;
	}

	public function sqlExpectedBillDate() {
		return ( true == isset( $this->m_strExpectedBillDate ) ) ? '\'' . $this->m_strExpectedBillDate . '\'' : 'NULL';
	}

	public function setServicePeriodStartDate( $strServicePeriodStartDate ) {
		$this->set( 'm_strServicePeriodStartDate', CStrings::strTrimDef( $strServicePeriodStartDate, -1, NULL, true ) );
	}

	public function getServicePeriodStartDate() {
		return $this->m_strServicePeriodStartDate;
	}

	public function sqlServicePeriodStartDate() {
		return ( true == isset( $this->m_strServicePeriodStartDate ) ) ? '\'' . $this->m_strServicePeriodStartDate . '\'' : 'NULL';
	}

	public function setExpectedDueDate( $strExpectedDueDate ) {
		$this->set( 'm_strExpectedDueDate', CStrings::strTrimDef( $strExpectedDueDate, -1, NULL, true ) );
	}

	public function getExpectedDueDate() {
		return $this->m_strExpectedDueDate;
	}

	public function sqlExpectedDueDate() {
		return ( true == isset( $this->m_strExpectedDueDate ) ) ? '\'' . $this->m_strExpectedDueDate . '\'' : 'NULL';
	}

	public function setChildUtilityBillAccountId( $intChildUtilityBillAccountId ) {
		$this->set( 'm_intChildUtilityBillAccountId', CStrings::strToIntDef( $intChildUtilityBillAccountId, NULL, false ) );
	}

	public function getChildUtilityBillAccountId() {
		return $this->m_intChildUtilityBillAccountId;
	}

	public function sqlChildUtilityBillAccountId() {
		return ( true == isset( $this->m_intChildUtilityBillAccountId ) ) ? ( string ) $this->m_intChildUtilityBillAccountId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, ap_payee_account_id, utility_bill_account_id, resolved_bill_id, alert_datetime, reminder_datetime, updated_on, updated_by, created_on, created_by, expected_bill_date, service_period_start_date, expected_due_date, child_utility_bill_account_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlApPayeeAccountId() . ', ' .
						$this->sqlUtilityBillAccountId() . ', ' .
						$this->sqlResolvedBillId() . ', ' .
						$this->sqlAlertDatetime() . ', ' .
						$this->sqlReminderDatetime() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlExpectedBillDate() . ', ' .
						$this->sqlServicePeriodStartDate() . ', ' .
						$this->sqlExpectedDueDate() . ', ' .
						$this->sqlChildUtilityBillAccountId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_bill_id = ' . $this->sqlResolvedBillId(). ',' ; } elseif( true == array_key_exists( 'ResolvedBillId', $this->getChangedColumns() ) ) { $strSql .= ' resolved_bill_id = ' . $this->sqlResolvedBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alert_datetime = ' . $this->sqlAlertDatetime(). ',' ; } elseif( true == array_key_exists( 'AlertDatetime', $this->getChangedColumns() ) ) { $strSql .= ' alert_datetime = ' . $this->sqlAlertDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reminder_datetime = ' . $this->sqlReminderDatetime(). ',' ; } elseif( true == array_key_exists( 'ReminderDatetime', $this->getChangedColumns() ) ) { $strSql .= ' reminder_datetime = ' . $this->sqlReminderDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_bill_date = ' . $this->sqlExpectedBillDate(). ',' ; } elseif( true == array_key_exists( 'ExpectedBillDate', $this->getChangedColumns() ) ) { $strSql .= ' expected_bill_date = ' . $this->sqlExpectedBillDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_period_start_date = ' . $this->sqlServicePeriodStartDate(). ',' ; } elseif( true == array_key_exists( 'ServicePeriodStartDate', $this->getChangedColumns() ) ) { $strSql .= ' service_period_start_date = ' . $this->sqlServicePeriodStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_due_date = ' . $this->sqlExpectedDueDate(). ',' ; } elseif( true == array_key_exists( 'ExpectedDueDate', $this->getChangedColumns() ) ) { $strSql .= ' expected_due_date = ' . $this->sqlExpectedDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' child_utility_bill_account_id = ' . $this->sqlChildUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'ChildUtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' child_utility_bill_account_id = ' . $this->sqlChildUtilityBillAccountId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payee_account_id' => $this->getApPayeeAccountId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'resolved_bill_id' => $this->getResolvedBillId(),
			'alert_datetime' => $this->getAlertDatetime(),
			'reminder_datetime' => $this->getReminderDatetime(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy(),
			'expected_bill_date' => $this->getExpectedBillDate(),
			'service_period_start_date' => $this->getServicePeriodStartDate(),
			'expected_due_date' => $this->getExpectedDueDate(),
			'child_utility_bill_account_id' => $this->getChildUtilityBillAccountId()
		);
	}

}
?>