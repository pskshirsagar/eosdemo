<?php

class CBaseUtilityDistribution extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_distributions';

	protected $m_intId;
	protected $m_intUtilityDistributionStepId;
	protected $m_strBatchDatetime;
	protected $m_strScheduledExportDate;
	protected $m_strAutoBilledOn;
	protected $m_intMailedBy;
	protected $m_strMailedOn;
	protected $m_intEmailedBy;
	protected $m_strEmailedOn;
	protected $m_intReceivedBy;
	protected $m_strReceivedOn;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strFtpedOn;
	protected $m_arrstrFtpFiles;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_distribution_step_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDistributionStepId', trim( $arrValues['utility_distribution_step_id'] ) ); elseif( isset( $arrValues['utility_distribution_step_id'] ) ) $this->setUtilityDistributionStepId( $arrValues['utility_distribution_step_id'] );
		if( isset( $arrValues['batch_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBatchDatetime', trim( $arrValues['batch_datetime'] ) ); elseif( isset( $arrValues['batch_datetime'] ) ) $this->setBatchDatetime( $arrValues['batch_datetime'] );
		if( isset( $arrValues['scheduled_export_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledExportDate', trim( $arrValues['scheduled_export_date'] ) ); elseif( isset( $arrValues['scheduled_export_date'] ) ) $this->setScheduledExportDate( $arrValues['scheduled_export_date'] );
		if( isset( $arrValues['auto_billed_on'] ) && $boolDirectSet ) $this->set( 'm_strAutoBilledOn', trim( $arrValues['auto_billed_on'] ) ); elseif( isset( $arrValues['auto_billed_on'] ) ) $this->setAutoBilledOn( $arrValues['auto_billed_on'] );
		if( isset( $arrValues['mailed_by'] ) && $boolDirectSet ) $this->set( 'm_intMailedBy', trim( $arrValues['mailed_by'] ) ); elseif( isset( $arrValues['mailed_by'] ) ) $this->setMailedBy( $arrValues['mailed_by'] );
		if( isset( $arrValues['mailed_on'] ) && $boolDirectSet ) $this->set( 'm_strMailedOn', trim( $arrValues['mailed_on'] ) ); elseif( isset( $arrValues['mailed_on'] ) ) $this->setMailedOn( $arrValues['mailed_on'] );
		if( isset( $arrValues['emailed_by'] ) && $boolDirectSet ) $this->set( 'm_intEmailedBy', trim( $arrValues['emailed_by'] ) ); elseif( isset( $arrValues['emailed_by'] ) ) $this->setEmailedBy( $arrValues['emailed_by'] );
		if( isset( $arrValues['emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strEmailedOn', trim( $arrValues['emailed_on'] ) ); elseif( isset( $arrValues['emailed_on'] ) ) $this->setEmailedOn( $arrValues['emailed_on'] );
		if( isset( $arrValues['received_by'] ) && $boolDirectSet ) $this->set( 'm_intReceivedBy', trim( $arrValues['received_by'] ) ); elseif( isset( $arrValues['received_by'] ) ) $this->setReceivedBy( $arrValues['received_by'] );
		if( isset( $arrValues['received_on'] ) && $boolDirectSet ) $this->set( 'm_strReceivedOn', trim( $arrValues['received_on'] ) ); elseif( isset( $arrValues['received_on'] ) ) $this->setReceivedOn( $arrValues['received_on'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ftped_on'] ) && $boolDirectSet ) $this->set( 'm_strFtpedOn', trim( $arrValues['ftped_on'] ) ); elseif( isset( $arrValues['ftped_on'] ) ) $this->setFtpedOn( $arrValues['ftped_on'] );
		if( isset( $arrValues['ftp_files'] ) && $boolDirectSet ) $this->set( 'm_arrstrFtpFiles', trim( $arrValues['ftp_files'] ) ); elseif( isset( $arrValues['ftp_files'] ) ) $this->setFtpFiles( $arrValues['ftp_files'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityDistributionStepId( $intUtilityDistributionStepId ) {
		$this->set( 'm_intUtilityDistributionStepId', CStrings::strToIntDef( $intUtilityDistributionStepId, NULL, false ) );
	}

	public function getUtilityDistributionStepId() {
		return $this->m_intUtilityDistributionStepId;
	}

	public function sqlUtilityDistributionStepId() {
		return ( true == isset( $this->m_intUtilityDistributionStepId ) ) ? ( string ) $this->m_intUtilityDistributionStepId : 'NULL';
	}

	public function setBatchDatetime( $strBatchDatetime ) {
		$this->set( 'm_strBatchDatetime', CStrings::strTrimDef( $strBatchDatetime, -1, NULL, true ) );
	}

	public function getBatchDatetime() {
		return $this->m_strBatchDatetime;
	}

	public function sqlBatchDatetime() {
		return ( true == isset( $this->m_strBatchDatetime ) ) ? '\'' . $this->m_strBatchDatetime . '\'' : 'NOW()';
	}

	public function setScheduledExportDate( $strScheduledExportDate ) {
		$this->set( 'm_strScheduledExportDate', CStrings::strTrimDef( $strScheduledExportDate, -1, NULL, true ) );
	}

	public function getScheduledExportDate() {
		return $this->m_strScheduledExportDate;
	}

	public function sqlScheduledExportDate() {
		return ( true == isset( $this->m_strScheduledExportDate ) ) ? '\'' . $this->m_strScheduledExportDate . '\'' : 'NULL';
	}

	public function setAutoBilledOn( $strAutoBilledOn ) {
		$this->set( 'm_strAutoBilledOn', CStrings::strTrimDef( $strAutoBilledOn, -1, NULL, true ) );
	}

	public function getAutoBilledOn() {
		return $this->m_strAutoBilledOn;
	}

	public function sqlAutoBilledOn() {
		return ( true == isset( $this->m_strAutoBilledOn ) ) ? '\'' . $this->m_strAutoBilledOn . '\'' : 'NULL';
	}

	public function setMailedBy( $intMailedBy ) {
		$this->set( 'm_intMailedBy', CStrings::strToIntDef( $intMailedBy, NULL, false ) );
	}

	public function getMailedBy() {
		return $this->m_intMailedBy;
	}

	public function sqlMailedBy() {
		return ( true == isset( $this->m_intMailedBy ) ) ? ( string ) $this->m_intMailedBy : 'NULL';
	}

	public function setMailedOn( $strMailedOn ) {
		$this->set( 'm_strMailedOn', CStrings::strTrimDef( $strMailedOn, -1, NULL, true ) );
	}

	public function getMailedOn() {
		return $this->m_strMailedOn;
	}

	public function sqlMailedOn() {
		return ( true == isset( $this->m_strMailedOn ) ) ? '\'' . $this->m_strMailedOn . '\'' : 'NULL';
	}

	public function setEmailedBy( $intEmailedBy ) {
		$this->set( 'm_intEmailedBy', CStrings::strToIntDef( $intEmailedBy, NULL, false ) );
	}

	public function getEmailedBy() {
		return $this->m_intEmailedBy;
	}

	public function sqlEmailedBy() {
		return ( true == isset( $this->m_intEmailedBy ) ) ? ( string ) $this->m_intEmailedBy : 'NULL';
	}

	public function setEmailedOn( $strEmailedOn ) {
		$this->set( 'm_strEmailedOn', CStrings::strTrimDef( $strEmailedOn, -1, NULL, true ) );
	}

	public function getEmailedOn() {
		return $this->m_strEmailedOn;
	}

	public function sqlEmailedOn() {
		return ( true == isset( $this->m_strEmailedOn ) ) ? '\'' . $this->m_strEmailedOn . '\'' : 'NULL';
	}

	public function setReceivedBy( $intReceivedBy ) {
		$this->set( 'm_intReceivedBy', CStrings::strToIntDef( $intReceivedBy, NULL, false ) );
	}

	public function getReceivedBy() {
		return $this->m_intReceivedBy;
	}

	public function sqlReceivedBy() {
		return ( true == isset( $this->m_intReceivedBy ) ) ? ( string ) $this->m_intReceivedBy : 'NULL';
	}

	public function setReceivedOn( $strReceivedOn ) {
		$this->set( 'm_strReceivedOn', CStrings::strTrimDef( $strReceivedOn, -1, NULL, true ) );
	}

	public function getReceivedOn() {
		return $this->m_strReceivedOn;
	}

	public function sqlReceivedOn() {
		return ( true == isset( $this->m_strReceivedOn ) ) ? '\'' . $this->m_strReceivedOn . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setFtpedOn( $strFtpedOn ) {
		$this->set( 'm_strFtpedOn', CStrings::strTrimDef( $strFtpedOn, -1, NULL, true ) );
	}

	public function getFtpedOn() {
		return $this->m_strFtpedOn;
	}

	public function sqlFtpedOn() {
		return ( true == isset( $this->m_strFtpedOn ) ) ? '\'' . $this->m_strFtpedOn . '\'' : 'NULL';
	}

	public function setFtpFiles( $arrstrFtpFiles ) {
		$this->set( 'm_arrstrFtpFiles', CStrings::strToArrIntDef( $arrstrFtpFiles, NULL ) );
	}

	public function getFtpFiles() {
		return $this->m_arrstrFtpFiles;
	}

	public function sqlFtpFiles() {
		return ( true == isset( $this->m_arrstrFtpFiles ) && true == valArr( $this->m_arrstrFtpFiles ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrFtpFiles, NULL ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_distribution_step_id, batch_datetime, scheduled_export_date, auto_billed_on, mailed_by, mailed_on, emailed_by, emailed_on, received_by, received_on, completed_by, completed_on, updated_by, updated_on, created_by, created_on, ftped_on, ftp_files )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUtilityDistributionStepId() . ', ' .
 						$this->sqlBatchDatetime() . ', ' .
 						$this->sqlScheduledExportDate() . ', ' .
 						$this->sqlAutoBilledOn() . ', ' .
 						$this->sqlMailedBy() . ', ' .
 						$this->sqlMailedOn() . ', ' .
 						$this->sqlEmailedBy() . ', ' .
 						$this->sqlEmailedOn() . ', ' .
 						$this->sqlReceivedBy() . ', ' .
 						$this->sqlReceivedOn() . ', ' .
 						$this->sqlCompletedBy() . ', ' .
 						$this->sqlCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlFtpedOn() . ', ' .
 						$this->sqlFtpFiles() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_distribution_step_id = ' . $this->sqlUtilityDistributionStepId() . ','; } elseif( true == array_key_exists( 'UtilityDistributionStepId', $this->getChangedColumns() ) ) { $strSql .= ' utility_distribution_step_id = ' . $this->sqlUtilityDistributionStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; } elseif( true == array_key_exists( 'BatchDatetime', $this->getChangedColumns() ) ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_export_date = ' . $this->sqlScheduledExportDate() . ','; } elseif( true == array_key_exists( 'ScheduledExportDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_export_date = ' . $this->sqlScheduledExportDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_billed_on = ' . $this->sqlAutoBilledOn() . ','; } elseif( true == array_key_exists( 'AutoBilledOn', $this->getChangedColumns() ) ) { $strSql .= ' auto_billed_on = ' . $this->sqlAutoBilledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailed_by = ' . $this->sqlMailedBy() . ','; } elseif( true == array_key_exists( 'MailedBy', $this->getChangedColumns() ) ) { $strSql .= ' mailed_by = ' . $this->sqlMailedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailed_on = ' . $this->sqlMailedOn() . ','; } elseif( true == array_key_exists( 'MailedOn', $this->getChangedColumns() ) ) { $strSql .= ' mailed_on = ' . $this->sqlMailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emailed_by = ' . $this->sqlEmailedBy() . ','; } elseif( true == array_key_exists( 'EmailedBy', $this->getChangedColumns() ) ) { $strSql .= ' emailed_by = ' . $this->sqlEmailedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; } elseif( true == array_key_exists( 'EmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' received_by = ' . $this->sqlReceivedBy() . ','; } elseif( true == array_key_exists( 'ReceivedBy', $this->getChangedColumns() ) ) { $strSql .= ' received_by = ' . $this->sqlReceivedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn() . ','; } elseif( true == array_key_exists( 'ReceivedOn', $this->getChangedColumns() ) ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ftped_on = ' . $this->sqlFtpedOn() . ','; } elseif( true == array_key_exists( 'FtpedOn', $this->getChangedColumns() ) ) { $strSql .= ' ftped_on = ' . $this->sqlFtpedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ftp_files = ' . $this->sqlFtpFiles() . ','; } elseif( true == array_key_exists( 'FtpFiles', $this->getChangedColumns() ) ) { $strSql .= ' ftp_files = ' . $this->sqlFtpFiles() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_distribution_step_id' => $this->getUtilityDistributionStepId(),
			'batch_datetime' => $this->getBatchDatetime(),
			'scheduled_export_date' => $this->getScheduledExportDate(),
			'auto_billed_on' => $this->getAutoBilledOn(),
			'mailed_by' => $this->getMailedBy(),
			'mailed_on' => $this->getMailedOn(),
			'emailed_by' => $this->getEmailedBy(),
			'emailed_on' => $this->getEmailedOn(),
			'received_by' => $this->getReceivedBy(),
			'received_on' => $this->getReceivedOn(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ftped_on' => $this->getFtpedOn(),
			'ftp_files' => $this->getFtpFiles()
		);
	}

}
?>