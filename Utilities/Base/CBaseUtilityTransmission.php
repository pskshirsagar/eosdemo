<?php

class CBaseUtilityTransmission extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_transmissions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intUtilityTypeBatchId;
	protected $m_intUtilityTransmissionTypeId;
	protected $m_strTransmissionDatetime;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strErrorMessage;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUploadedBy;
	protected $m_strUploadedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['utility_type_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeBatchId', trim( $arrValues['utility_type_batch_id'] ) ); elseif( isset( $arrValues['utility_type_batch_id'] ) ) $this->setUtilityTypeBatchId( $arrValues['utility_type_batch_id'] );
		if( isset( $arrValues['utility_transmission_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTransmissionTypeId', trim( $arrValues['utility_transmission_type_id'] ) ); elseif( isset( $arrValues['utility_transmission_type_id'] ) ) $this->setUtilityTransmissionTypeId( $arrValues['utility_transmission_type_id'] );
		if( isset( $arrValues['transmission_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransmissionDatetime', trim( $arrValues['transmission_datetime'] ) ); elseif( isset( $arrValues['transmission_datetime'] ) ) $this->setTransmissionDatetime( $arrValues['transmission_datetime'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['error_message'] ) && $boolDirectSet ) $this->set( 'm_strErrorMessage', trim( stripcslashes( $arrValues['error_message'] ) ) ); elseif( isset( $arrValues['error_message'] ) ) $this->setErrorMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_message'] ) : $arrValues['error_message'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['uploaded_by'] ) && $boolDirectSet ) $this->set( 'm_intUploadedBy', trim( $arrValues['uploaded_by'] ) ); elseif( isset( $arrValues['uploaded_by'] ) ) $this->setUploadedBy( $arrValues['uploaded_by'] );
		if( isset( $arrValues['uploaded_on'] ) && $boolDirectSet ) $this->set( 'm_strUploadedOn', trim( $arrValues['uploaded_on'] ) ); elseif( isset( $arrValues['uploaded_on'] ) ) $this->setUploadedOn( $arrValues['uploaded_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setUtilityTypeBatchId( $intUtilityTypeBatchId ) {
		$this->set( 'm_intUtilityTypeBatchId', CStrings::strToIntDef( $intUtilityTypeBatchId, NULL, false ) );
	}

	public function getUtilityTypeBatchId() {
		return $this->m_intUtilityTypeBatchId;
	}

	public function sqlUtilityTypeBatchId() {
		return ( true == isset( $this->m_intUtilityTypeBatchId ) ) ? ( string ) $this->m_intUtilityTypeBatchId : 'NULL';
	}

	public function setUtilityTransmissionTypeId( $intUtilityTransmissionTypeId ) {
		$this->set( 'm_intUtilityTransmissionTypeId', CStrings::strToIntDef( $intUtilityTransmissionTypeId, NULL, false ) );
	}

	public function getUtilityTransmissionTypeId() {
		return $this->m_intUtilityTransmissionTypeId;
	}

	public function sqlUtilityTransmissionTypeId() {
		return ( true == isset( $this->m_intUtilityTransmissionTypeId ) ) ? ( string ) $this->m_intUtilityTransmissionTypeId : 'NULL';
	}

	public function setTransmissionDatetime( $strTransmissionDatetime ) {
		$this->set( 'm_strTransmissionDatetime', CStrings::strTrimDef( $strTransmissionDatetime, -1, NULL, true ) );
	}

	public function getTransmissionDatetime() {
		return $this->m_strTransmissionDatetime;
	}

	public function sqlTransmissionDatetime() {
		return ( true == isset( $this->m_strTransmissionDatetime ) ) ? '\'' . $this->m_strTransmissionDatetime . '\'' : 'NOW()';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 300, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setErrorMessage( $strErrorMessage ) {
		$this->set( 'm_strErrorMessage', CStrings::strTrimDef( $strErrorMessage, -1, NULL, true ) );
	}

	public function getErrorMessage() {
		return $this->m_strErrorMessage;
	}

	public function sqlErrorMessage() {
		return ( true == isset( $this->m_strErrorMessage ) ) ? '\'' . addslashes( $this->m_strErrorMessage ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUploadedBy( $intUploadedBy ) {
		$this->set( 'm_intUploadedBy', CStrings::strToIntDef( $intUploadedBy, NULL, false ) );
	}

	public function getUploadedBy() {
		return $this->m_intUploadedBy;
	}

	public function sqlUploadedBy() {
		return ( true == isset( $this->m_intUploadedBy ) ) ? ( string ) $this->m_intUploadedBy : 'NULL';
	}

	public function setUploadedOn( $strUploadedOn ) {
		$this->set( 'm_strUploadedOn', CStrings::strTrimDef( $strUploadedOn, -1, NULL, true ) );
	}

	public function getUploadedOn() {
		return $this->m_strUploadedOn;
	}

	public function sqlUploadedOn() {
		return ( true == isset( $this->m_strUploadedOn ) ) ? '\'' . $this->m_strUploadedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_type_id, utility_type_batch_id, utility_transmission_type_id, transmission_datetime, file_name, file_path, error_message, updated_by, updated_on, created_by, created_on, uploaded_by, uploaded_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlUtilityTypeBatchId() . ', ' .
						$this->sqlUtilityTransmissionTypeId() . ', ' .
						$this->sqlTransmissionDatetime() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlFilePath() . ', ' .
						$this->sqlErrorMessage() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUploadedBy() . ', ' .
						$this->sqlUploadedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_batch_id = ' . $this->sqlUtilityTypeBatchId(). ',' ; } elseif( true == array_key_exists( 'UtilityTypeBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_batch_id = ' . $this->sqlUtilityTypeBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_transmission_type_id = ' . $this->sqlUtilityTransmissionTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityTransmissionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_transmission_type_id = ' . $this->sqlUtilityTransmissionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_datetime = ' . $this->sqlTransmissionDatetime(). ',' ; } elseif( true == array_key_exists( 'TransmissionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transmission_datetime = ' . $this->sqlTransmissionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath(). ',' ; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_message = ' . $this->sqlErrorMessage(). ',' ; } elseif( true == array_key_exists( 'ErrorMessage', $this->getChangedColumns() ) ) { $strSql .= ' error_message = ' . $this->sqlErrorMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uploaded_by = ' . $this->sqlUploadedBy(). ',' ; } elseif( true == array_key_exists( 'UploadedBy', $this->getChangedColumns() ) ) { $strSql .= ' uploaded_by = ' . $this->sqlUploadedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uploaded_on = ' . $this->sqlUploadedOn(). ',' ; } elseif( true == array_key_exists( 'UploadedOn', $this->getChangedColumns() ) ) { $strSql .= ' uploaded_on = ' . $this->sqlUploadedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'utility_type_batch_id' => $this->getUtilityTypeBatchId(),
			'utility_transmission_type_id' => $this->getUtilityTransmissionTypeId(),
			'transmission_datetime' => $this->getTransmissionDatetime(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'error_message' => $this->getErrorMessage(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'uploaded_by' => $this->getUploadedBy(),
			'uploaded_on' => $this->getUploadedOn()
		);
	}

}
?>