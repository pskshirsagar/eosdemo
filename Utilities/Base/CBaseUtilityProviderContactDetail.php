<?php

class CBaseUtilityProviderContactDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_provider_contact_details';

	protected $m_intId;
	protected $m_intUtilityProviderId;
	protected $m_intUtilityProviderEventTypeId;
	protected $m_strInstructions;
	protected $m_boolIsIncludeRevertToOwnerAgreement;
	protected $m_boolIsIncludeAccountList;
	protected $m_boolIsIncludeContractCopy;
	protected $m_intUtilityPreferedContactTypeId;
	protected $m_strEmailToAddress;
	protected $m_strEmailSubject;
	protected $m_strEmailContent;
	protected $m_strFaxNumber;
	protected $m_strMailTo;
	protected $m_strMailStreetLineOne;
	protected $m_strMailStreetLineTwo;
	protected $m_strMailCity;
	protected $m_strMailState;
	protected $m_strMailPostalCode;
	protected $m_strPhoneNumber;
	protected $m_strPhoneContact;
	protected $m_strPhoneInstructions;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsIncludeRevertToOwnerAgreement = false;
		$this->m_boolIsIncludeAccountList = false;
		$this->m_boolIsIncludeContractCopy = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityProviderId', trim( $arrValues['utility_provider_id'] ) ); elseif( isset( $arrValues['utility_provider_id'] ) ) $this->setUtilityProviderId( $arrValues['utility_provider_id'] );
		if( isset( $arrValues['utility_provider_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityProviderEventTypeId', trim( $arrValues['utility_provider_event_type_id'] ) ); elseif( isset( $arrValues['utility_provider_event_type_id'] ) ) $this->setUtilityProviderEventTypeId( $arrValues['utility_provider_event_type_id'] );
		if( isset( $arrValues['instructions'] ) && $boolDirectSet ) $this->set( 'm_strInstructions', trim( $arrValues['instructions'] ) ); elseif( isset( $arrValues['instructions'] ) ) $this->setInstructions( $arrValues['instructions'] );
		if( isset( $arrValues['is_include_revert_to_owner_agreement'] ) && $boolDirectSet ) $this->set( 'm_boolIsIncludeRevertToOwnerAgreement', trim( stripcslashes( $arrValues['is_include_revert_to_owner_agreement'] ) ) ); elseif( isset( $arrValues['is_include_revert_to_owner_agreement'] ) ) $this->setIsIncludeRevertToOwnerAgreement( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_include_revert_to_owner_agreement'] ) : $arrValues['is_include_revert_to_owner_agreement'] );
		if( isset( $arrValues['is_include_account_list'] ) && $boolDirectSet ) $this->set( 'm_boolIsIncludeAccountList', trim( stripcslashes( $arrValues['is_include_account_list'] ) ) ); elseif( isset( $arrValues['is_include_account_list'] ) ) $this->setIsIncludeAccountList( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_include_account_list'] ) : $arrValues['is_include_account_list'] );
		if( isset( $arrValues['is_include_contract_copy'] ) && $boolDirectSet ) $this->set( 'm_boolIsIncludeContractCopy', trim( stripcslashes( $arrValues['is_include_contract_copy'] ) ) ); elseif( isset( $arrValues['is_include_contract_copy'] ) ) $this->setIsIncludeContractCopy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_include_contract_copy'] ) : $arrValues['is_include_contract_copy'] );
		if( isset( $arrValues['utility_prefered_contact_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityPreferedContactTypeId', trim( $arrValues['utility_prefered_contact_type_id'] ) ); elseif( isset( $arrValues['utility_prefered_contact_type_id'] ) ) $this->setUtilityPreferedContactTypeId( $arrValues['utility_prefered_contact_type_id'] );
		if( isset( $arrValues['email_to_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailToAddress', trim( $arrValues['email_to_address'] ) ); elseif( isset( $arrValues['email_to_address'] ) ) $this->setEmailToAddress( $arrValues['email_to_address'] );
		if( isset( $arrValues['email_subject'] ) && $boolDirectSet ) $this->set( 'm_strEmailSubject', trim( $arrValues['email_subject'] ) ); elseif( isset( $arrValues['email_subject'] ) ) $this->setEmailSubject( $arrValues['email_subject'] );
		if( isset( $arrValues['email_content'] ) && $boolDirectSet ) $this->set( 'm_strEmailContent', trim( $arrValues['email_content'] ) ); elseif( isset( $arrValues['email_content'] ) ) $this->setEmailContent( $arrValues['email_content'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( $arrValues['fax_number'] ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( $arrValues['fax_number'] );
		if( isset( $arrValues['mail_to'] ) && $boolDirectSet ) $this->set( 'm_strMailTo', trim( $arrValues['mail_to'] ) ); elseif( isset( $arrValues['mail_to'] ) ) $this->setMailTo( $arrValues['mail_to'] );
		if( isset( $arrValues['mail_street_line_one'] ) && $boolDirectSet ) $this->set( 'm_strMailStreetLineOne', trim( $arrValues['mail_street_line_one'] ) ); elseif( isset( $arrValues['mail_street_line_one'] ) ) $this->setMailStreetLineOne( $arrValues['mail_street_line_one'] );
		if( isset( $arrValues['mail_street_line_two'] ) && $boolDirectSet ) $this->set( 'm_strMailStreetLineTwo', trim( $arrValues['mail_street_line_two'] ) ); elseif( isset( $arrValues['mail_street_line_two'] ) ) $this->setMailStreetLineTwo( $arrValues['mail_street_line_two'] );
		if( isset( $arrValues['mail_city'] ) && $boolDirectSet ) $this->set( 'm_strMailCity', trim( $arrValues['mail_city'] ) ); elseif( isset( $arrValues['mail_city'] ) ) $this->setMailCity( $arrValues['mail_city'] );
		if( isset( $arrValues['mail_state'] ) && $boolDirectSet ) $this->set( 'm_strMailState', trim( $arrValues['mail_state'] ) ); elseif( isset( $arrValues['mail_state'] ) ) $this->setMailState( $arrValues['mail_state'] );
		if( isset( $arrValues['mail_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strMailPostalCode', trim( $arrValues['mail_postal_code'] ) ); elseif( isset( $arrValues['mail_postal_code'] ) ) $this->setMailPostalCode( $arrValues['mail_postal_code'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['phone_contact'] ) && $boolDirectSet ) $this->set( 'm_strPhoneContact', trim( $arrValues['phone_contact'] ) ); elseif( isset( $arrValues['phone_contact'] ) ) $this->setPhoneContact( $arrValues['phone_contact'] );
		if( isset( $arrValues['phone_instructions'] ) && $boolDirectSet ) $this->set( 'm_strPhoneInstructions', trim( $arrValues['phone_instructions'] ) ); elseif( isset( $arrValues['phone_instructions'] ) ) $this->setPhoneInstructions( $arrValues['phone_instructions'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityProviderId( $intUtilityProviderId ) {
		$this->set( 'm_intUtilityProviderId', CStrings::strToIntDef( $intUtilityProviderId, NULL, false ) );
	}

	public function getUtilityProviderId() {
		return $this->m_intUtilityProviderId;
	}

	public function sqlUtilityProviderId() {
		return ( true == isset( $this->m_intUtilityProviderId ) ) ? ( string ) $this->m_intUtilityProviderId : 'NULL';
	}

	public function setUtilityProviderEventTypeId( $intUtilityProviderEventTypeId ) {
		$this->set( 'm_intUtilityProviderEventTypeId', CStrings::strToIntDef( $intUtilityProviderEventTypeId, NULL, false ) );
	}

	public function getUtilityProviderEventTypeId() {
		return $this->m_intUtilityProviderEventTypeId;
	}

	public function sqlUtilityProviderEventTypeId() {
		return ( true == isset( $this->m_intUtilityProviderEventTypeId ) ) ? ( string ) $this->m_intUtilityProviderEventTypeId : 'NULL';
	}

	public function setInstructions( $strInstructions ) {
		$this->set( 'm_strInstructions', CStrings::strTrimDef( $strInstructions, -1, NULL, true ) );
	}

	public function getInstructions() {
		return $this->m_strInstructions;
	}

	public function sqlInstructions() {
		return ( true == isset( $this->m_strInstructions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInstructions ) : '\'' . addslashes( $this->m_strInstructions ) . '\'' ) : 'NULL';
	}

	public function setIsIncludeRevertToOwnerAgreement( $boolIsIncludeRevertToOwnerAgreement ) {
		$this->set( 'm_boolIsIncludeRevertToOwnerAgreement', CStrings::strToBool( $boolIsIncludeRevertToOwnerAgreement ) );
	}

	public function getIsIncludeRevertToOwnerAgreement() {
		return $this->m_boolIsIncludeRevertToOwnerAgreement;
	}

	public function sqlIsIncludeRevertToOwnerAgreement() {
		return ( true == isset( $this->m_boolIsIncludeRevertToOwnerAgreement ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIncludeRevertToOwnerAgreement ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsIncludeAccountList( $boolIsIncludeAccountList ) {
		$this->set( 'm_boolIsIncludeAccountList', CStrings::strToBool( $boolIsIncludeAccountList ) );
	}

	public function getIsIncludeAccountList() {
		return $this->m_boolIsIncludeAccountList;
	}

	public function sqlIsIncludeAccountList() {
		return ( true == isset( $this->m_boolIsIncludeAccountList ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIncludeAccountList ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsIncludeContractCopy( $boolIsIncludeContractCopy ) {
		$this->set( 'm_boolIsIncludeContractCopy', CStrings::strToBool( $boolIsIncludeContractCopy ) );
	}

	public function getIsIncludeContractCopy() {
		return $this->m_boolIsIncludeContractCopy;
	}

	public function sqlIsIncludeContractCopy() {
		return ( true == isset( $this->m_boolIsIncludeContractCopy ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIncludeContractCopy ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUtilityPreferedContactTypeId( $intUtilityPreferedContactTypeId ) {
		$this->set( 'm_intUtilityPreferedContactTypeId', CStrings::strToIntDef( $intUtilityPreferedContactTypeId, NULL, false ) );
	}

	public function getUtilityPreferedContactTypeId() {
		return $this->m_intUtilityPreferedContactTypeId;
	}

	public function sqlUtilityPreferedContactTypeId() {
		return ( true == isset( $this->m_intUtilityPreferedContactTypeId ) ) ? ( string ) $this->m_intUtilityPreferedContactTypeId : 'NULL';
	}

	public function setEmailToAddress( $strEmailToAddress ) {
		$this->set( 'm_strEmailToAddress', CStrings::strTrimDef( $strEmailToAddress, -1, NULL, true ) );
	}

	public function getEmailToAddress() {
		return $this->m_strEmailToAddress;
	}

	public function sqlEmailToAddress() {
		return ( true == isset( $this->m_strEmailToAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailToAddress ) : '\'' . addslashes( $this->m_strEmailToAddress ) . '\'' ) : 'NULL';
	}

	public function setEmailSubject( $strEmailSubject ) {
		$this->set( 'm_strEmailSubject', CStrings::strTrimDef( $strEmailSubject, -1, NULL, true ) );
	}

	public function getEmailSubject() {
		return $this->m_strEmailSubject;
	}

	public function sqlEmailSubject() {
		return ( true == isset( $this->m_strEmailSubject ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailSubject ) : '\'' . addslashes( $this->m_strEmailSubject ) . '\'' ) : 'NULL';
	}

	public function setEmailContent( $strEmailContent ) {
		$this->set( 'm_strEmailContent', CStrings::strTrimDef( $strEmailContent, -1, NULL, true ) );
	}

	public function getEmailContent() {
		return $this->m_strEmailContent;
	}

	public function sqlEmailContent() {
		return ( true == isset( $this->m_strEmailContent ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailContent ) : '\'' . addslashes( $this->m_strEmailContent ) . '\'' ) : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, -1, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFaxNumber ) : '\'' . addslashes( $this->m_strFaxNumber ) . '\'' ) : 'NULL';
	}

	public function setMailTo( $strMailTo ) {
		$this->set( 'm_strMailTo', CStrings::strTrimDef( $strMailTo, -1, NULL, true ) );
	}

	public function getMailTo() {
		return $this->m_strMailTo;
	}

	public function sqlMailTo() {
		return ( true == isset( $this->m_strMailTo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailTo ) : '\'' . addslashes( $this->m_strMailTo ) . '\'' ) : 'NULL';
	}

	public function setMailStreetLineOne( $strMailStreetLineOne ) {
		$this->set( 'm_strMailStreetLineOne', CStrings::strTrimDef( $strMailStreetLineOne, -1, NULL, true ) );
	}

	public function getMailStreetLineOne() {
		return $this->m_strMailStreetLineOne;
	}

	public function sqlMailStreetLineOne() {
		return ( true == isset( $this->m_strMailStreetLineOne ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailStreetLineOne ) : '\'' . addslashes( $this->m_strMailStreetLineOne ) . '\'' ) : 'NULL';
	}

	public function setMailStreetLineTwo( $strMailStreetLineTwo ) {
		$this->set( 'm_strMailStreetLineTwo', CStrings::strTrimDef( $strMailStreetLineTwo, -1, NULL, true ) );
	}

	public function getMailStreetLineTwo() {
		return $this->m_strMailStreetLineTwo;
	}

	public function sqlMailStreetLineTwo() {
		return ( true == isset( $this->m_strMailStreetLineTwo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailStreetLineTwo ) : '\'' . addslashes( $this->m_strMailStreetLineTwo ) . '\'' ) : 'NULL';
	}

	public function setMailCity( $strMailCity ) {
		$this->set( 'm_strMailCity', CStrings::strTrimDef( $strMailCity, -1, NULL, true ) );
	}

	public function getMailCity() {
		return $this->m_strMailCity;
	}

	public function sqlMailCity() {
		return ( true == isset( $this->m_strMailCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailCity ) : '\'' . addslashes( $this->m_strMailCity ) . '\'' ) : 'NULL';
	}

	public function setMailState( $strMailState ) {
		$this->set( 'm_strMailState', CStrings::strTrimDef( $strMailState, -1, NULL, true ) );
	}

	public function getMailState() {
		return $this->m_strMailState;
	}

	public function sqlMailState() {
		return ( true == isset( $this->m_strMailState ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailState ) : '\'' . addslashes( $this->m_strMailState ) . '\'' ) : 'NULL';
	}

	public function setMailPostalCode( $strMailPostalCode ) {
		$this->set( 'm_strMailPostalCode', CStrings::strTrimDef( $strMailPostalCode, -1, NULL, true ) );
	}

	public function getMailPostalCode() {
		return $this->m_strMailPostalCode;
	}

	public function sqlMailPostalCode() {
		return ( true == isset( $this->m_strMailPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailPostalCode ) : '\'' . addslashes( $this->m_strMailPostalCode ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, -1, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setPhoneContact( $strPhoneContact ) {
		$this->set( 'm_strPhoneContact', CStrings::strTrimDef( $strPhoneContact, -1, NULL, true ) );
	}

	public function getPhoneContact() {
		return $this->m_strPhoneContact;
	}

	public function sqlPhoneContact() {
		return ( true == isset( $this->m_strPhoneContact ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneContact ) : '\'' . addslashes( $this->m_strPhoneContact ) . '\'' ) : 'NULL';
	}

	public function setPhoneInstructions( $strPhoneInstructions ) {
		$this->set( 'm_strPhoneInstructions', CStrings::strTrimDef( $strPhoneInstructions, -1, NULL, true ) );
	}

	public function getPhoneInstructions() {
		return $this->m_strPhoneInstructions;
	}

	public function sqlPhoneInstructions() {
		return ( true == isset( $this->m_strPhoneInstructions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneInstructions ) : '\'' . addslashes( $this->m_strPhoneInstructions ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_provider_id, utility_provider_event_type_id, instructions, is_include_revert_to_owner_agreement, is_include_account_list, is_include_contract_copy, utility_prefered_contact_type_id, email_to_address, email_subject, email_content, fax_number, mail_to, mail_street_line_one, mail_street_line_two, mail_city, mail_state, mail_postal_code, phone_number, phone_contact, phone_instructions, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityProviderId() . ', ' .
						$this->sqlUtilityProviderEventTypeId() . ', ' .
						$this->sqlInstructions() . ', ' .
						$this->sqlIsIncludeRevertToOwnerAgreement() . ', ' .
						$this->sqlIsIncludeAccountList() . ', ' .
						$this->sqlIsIncludeContractCopy() . ', ' .
						$this->sqlUtilityPreferedContactTypeId() . ', ' .
						$this->sqlEmailToAddress() . ', ' .
						$this->sqlEmailSubject() . ', ' .
						$this->sqlEmailContent() . ', ' .
						$this->sqlFaxNumber() . ', ' .
						$this->sqlMailTo() . ', ' .
						$this->sqlMailStreetLineOne() . ', ' .
						$this->sqlMailStreetLineTwo() . ', ' .
						$this->sqlMailCity() . ', ' .
						$this->sqlMailState() . ', ' .
						$this->sqlMailPostalCode() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlPhoneContact() . ', ' .
						$this->sqlPhoneInstructions() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId(). ',' ; } elseif( true == array_key_exists( 'UtilityProviderId', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_event_type_id = ' . $this->sqlUtilityProviderEventTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityProviderEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_event_type_id = ' . $this->sqlUtilityProviderEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instructions = ' . $this->sqlInstructions(). ',' ; } elseif( true == array_key_exists( 'Instructions', $this->getChangedColumns() ) ) { $strSql .= ' instructions = ' . $this->sqlInstructions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_include_revert_to_owner_agreement = ' . $this->sqlIsIncludeRevertToOwnerAgreement(). ',' ; } elseif( true == array_key_exists( 'IsIncludeRevertToOwnerAgreement', $this->getChangedColumns() ) ) { $strSql .= ' is_include_revert_to_owner_agreement = ' . $this->sqlIsIncludeRevertToOwnerAgreement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_include_account_list = ' . $this->sqlIsIncludeAccountList(). ',' ; } elseif( true == array_key_exists( 'IsIncludeAccountList', $this->getChangedColumns() ) ) { $strSql .= ' is_include_account_list = ' . $this->sqlIsIncludeAccountList() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_include_contract_copy = ' . $this->sqlIsIncludeContractCopy(). ',' ; } elseif( true == array_key_exists( 'IsIncludeContractCopy', $this->getChangedColumns() ) ) { $strSql .= ' is_include_contract_copy = ' . $this->sqlIsIncludeContractCopy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_prefered_contact_type_id = ' . $this->sqlUtilityPreferedContactTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityPreferedContactTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_prefered_contact_type_id = ' . $this->sqlUtilityPreferedContactTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_to_address = ' . $this->sqlEmailToAddress(). ',' ; } elseif( true == array_key_exists( 'EmailToAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_to_address = ' . $this->sqlEmailToAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_subject = ' . $this->sqlEmailSubject(). ',' ; } elseif( true == array_key_exists( 'EmailSubject', $this->getChangedColumns() ) ) { $strSql .= ' email_subject = ' . $this->sqlEmailSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_content = ' . $this->sqlEmailContent(). ',' ; } elseif( true == array_key_exists( 'EmailContent', $this->getChangedColumns() ) ) { $strSql .= ' email_content = ' . $this->sqlEmailContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber(). ',' ; } elseif( true == array_key_exists( 'FaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_to = ' . $this->sqlMailTo(). ',' ; } elseif( true == array_key_exists( 'MailTo', $this->getChangedColumns() ) ) { $strSql .= ' mail_to = ' . $this->sqlMailTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_street_line_one = ' . $this->sqlMailStreetLineOne(). ',' ; } elseif( true == array_key_exists( 'MailStreetLineOne', $this->getChangedColumns() ) ) { $strSql .= ' mail_street_line_one = ' . $this->sqlMailStreetLineOne() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_street_line_two = ' . $this->sqlMailStreetLineTwo(). ',' ; } elseif( true == array_key_exists( 'MailStreetLineTwo', $this->getChangedColumns() ) ) { $strSql .= ' mail_street_line_two = ' . $this->sqlMailStreetLineTwo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_city = ' . $this->sqlMailCity(). ',' ; } elseif( true == array_key_exists( 'MailCity', $this->getChangedColumns() ) ) { $strSql .= ' mail_city = ' . $this->sqlMailCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_state = ' . $this->sqlMailState(). ',' ; } elseif( true == array_key_exists( 'MailState', $this->getChangedColumns() ) ) { $strSql .= ' mail_state = ' . $this->sqlMailState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_postal_code = ' . $this->sqlMailPostalCode(). ',' ; } elseif( true == array_key_exists( 'MailPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' mail_postal_code = ' . $this->sqlMailPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_contact = ' . $this->sqlPhoneContact(). ',' ; } elseif( true == array_key_exists( 'PhoneContact', $this->getChangedColumns() ) ) { $strSql .= ' phone_contact = ' . $this->sqlPhoneContact() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_instructions = ' . $this->sqlPhoneInstructions(). ',' ; } elseif( true == array_key_exists( 'PhoneInstructions', $this->getChangedColumns() ) ) { $strSql .= ' phone_instructions = ' . $this->sqlPhoneInstructions() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_provider_id' => $this->getUtilityProviderId(),
			'utility_provider_event_type_id' => $this->getUtilityProviderEventTypeId(),
			'instructions' => $this->getInstructions(),
			'is_include_revert_to_owner_agreement' => $this->getIsIncludeRevertToOwnerAgreement(),
			'is_include_account_list' => $this->getIsIncludeAccountList(),
			'is_include_contract_copy' => $this->getIsIncludeContractCopy(),
			'utility_prefered_contact_type_id' => $this->getUtilityPreferedContactTypeId(),
			'email_to_address' => $this->getEmailToAddress(),
			'email_subject' => $this->getEmailSubject(),
			'email_content' => $this->getEmailContent(),
			'fax_number' => $this->getFaxNumber(),
			'mail_to' => $this->getMailTo(),
			'mail_street_line_one' => $this->getMailStreetLineOne(),
			'mail_street_line_two' => $this->getMailStreetLineTwo(),
			'mail_city' => $this->getMailCity(),
			'mail_state' => $this->getMailState(),
			'mail_postal_code' => $this->getMailPostalCode(),
			'phone_number' => $this->getPhoneNumber(),
			'phone_contact' => $this->getPhoneContact(),
			'phone_instructions' => $this->getPhoneInstructions(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>