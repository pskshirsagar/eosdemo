<?php

class CBaseBuyerContactLocation extends CEosSingularBase {

	const TABLE_NAME = 'public.buyer_contact_locations';

	protected $m_intId;
	protected $m_intBuyerContactId;
	protected $m_intBuyerLocationId;
	protected $m_boolIsPrimary;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPrimary = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['buyer_contact_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerContactId', trim( $arrValues['buyer_contact_id'] ) ); elseif( isset( $arrValues['buyer_contact_id'] ) ) $this->setBuyerContactId( $arrValues['buyer_contact_id'] );
		if( isset( $arrValues['buyer_location_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerLocationId', trim( $arrValues['buyer_location_id'] ) ); elseif( isset( $arrValues['buyer_location_id'] ) ) $this->setBuyerLocationId( $arrValues['buyer_location_id'] );
		if( isset( $arrValues['is_primary'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrimary', trim( stripcslashes( $arrValues['is_primary'] ) ) ); elseif( isset( $arrValues['is_primary'] ) ) $this->setIsPrimary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_primary'] ) : $arrValues['is_primary'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setBuyerContactId( $intBuyerContactId ) {
		$this->set( 'm_intBuyerContactId', CStrings::strToIntDef( $intBuyerContactId, NULL, false ) );
	}

	public function getBuyerContactId() {
		return $this->m_intBuyerContactId;
	}

	public function sqlBuyerContactId() {
		return ( true == isset( $this->m_intBuyerContactId ) ) ? ( string ) $this->m_intBuyerContactId : 'NULL';
	}

	public function setBuyerLocationId( $intBuyerLocationId ) {
		$this->set( 'm_intBuyerLocationId', CStrings::strToIntDef( $intBuyerLocationId, NULL, false ) );
	}

	public function getBuyerLocationId() {
		return $this->m_intBuyerLocationId;
	}

	public function sqlBuyerLocationId() {
		return ( true == isset( $this->m_intBuyerLocationId ) ) ? ( string ) $this->m_intBuyerLocationId : 'NULL';
	}

	public function setIsPrimary( $boolIsPrimary ) {
		$this->set( 'm_boolIsPrimary', CStrings::strToBool( $boolIsPrimary ) );
	}

	public function getIsPrimary() {
		return $this->m_boolIsPrimary;
	}

	public function sqlIsPrimary() {
		return ( true == isset( $this->m_boolIsPrimary ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrimary ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, buyer_contact_id, buyer_location_id, is_primary, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlBuyerContactId() . ', ' .
 						$this->sqlBuyerLocationId() . ', ' .
 						$this->sqlIsPrimary() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_contact_id = ' . $this->sqlBuyerContactId() . ','; } elseif( true == array_key_exists( 'BuyerContactId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_contact_id = ' . $this->sqlBuyerContactId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_location_id = ' . $this->sqlBuyerLocationId() . ','; } elseif( true == array_key_exists( 'BuyerLocationId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_location_id = ' . $this->sqlBuyerLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary() . ','; } elseif( true == array_key_exists( 'IsPrimary', $this->getChangedColumns() ) ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'buyer_contact_id' => $this->getBuyerContactId(),
			'buyer_location_id' => $this->getBuyerLocationId(),
			'is_primary' => $this->getIsPrimary(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>