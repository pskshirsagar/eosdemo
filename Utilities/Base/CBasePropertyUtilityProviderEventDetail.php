<?php

class CBasePropertyUtilityProviderEventDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.property_utility_provider_event_details';

	protected $m_intId;
	protected $m_intPropertyUtilityProviderEventId;
	protected $m_intUtilityProviderEventTypeId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intConfirmedBy;
	protected $m_strConfirmedOn;
	protected $m_intConfirmUtilityDocumentId;
	protected $m_strFollowUpDate;
	protected $m_strNotes;
	protected $m_strConfirmedNotes;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['property_utility_provider_event_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityProviderEventId', trim( $arrValues['property_utility_provider_event_id'] ) ); elseif( isset( $arrValues['property_utility_provider_event_id'] ) ) $this->setPropertyUtilityProviderEventId( $arrValues['property_utility_provider_event_id'] );
		if( isset( $arrValues['utility_provider_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityProviderEventTypeId', trim( $arrValues['utility_provider_event_type_id'] ) ); elseif( isset( $arrValues['utility_provider_event_type_id'] ) ) $this->setUtilityProviderEventTypeId( $arrValues['utility_provider_event_type_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['confirmed_by'] ) && $boolDirectSet ) $this->set( 'm_intConfirmedBy', trim( $arrValues['confirmed_by'] ) ); elseif( isset( $arrValues['confirmed_by'] ) ) $this->setConfirmedBy( $arrValues['confirmed_by'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['confirm_utility_document_id'] ) && $boolDirectSet ) $this->set( 'm_intConfirmUtilityDocumentId', trim( $arrValues['confirm_utility_document_id'] ) ); elseif( isset( $arrValues['confirm_utility_document_id'] ) ) $this->setConfirmUtilityDocumentId( $arrValues['confirm_utility_document_id'] );
		if( isset( $arrValues['follow_up_date'] ) && $boolDirectSet ) $this->set( 'm_strFollowUpDate', trim( $arrValues['follow_up_date'] ) ); elseif( isset( $arrValues['follow_up_date'] ) ) $this->setFollowUpDate( $arrValues['follow_up_date'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['confirmed_notes'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedNotes', trim( $arrValues['confirmed_notes'] ) ); elseif( isset( $arrValues['confirmed_notes'] ) ) $this->setConfirmedNotes( $arrValues['confirmed_notes'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPropertyUtilityProviderEventId( $intPropertyUtilityProviderEventId ) {
		$this->set( 'm_intPropertyUtilityProviderEventId', CStrings::strToIntDef( $intPropertyUtilityProviderEventId, NULL, false ) );
	}

	public function getPropertyUtilityProviderEventId() {
		return $this->m_intPropertyUtilityProviderEventId;
	}

	public function sqlPropertyUtilityProviderEventId() {
		return ( true == isset( $this->m_intPropertyUtilityProviderEventId ) ) ? ( string ) $this->m_intPropertyUtilityProviderEventId : 'NULL';
	}

	public function setUtilityProviderEventTypeId( $intUtilityProviderEventTypeId ) {
		$this->set( 'm_intUtilityProviderEventTypeId', CStrings::strToIntDef( $intUtilityProviderEventTypeId, NULL, false ) );
	}

	public function getUtilityProviderEventTypeId() {
		return $this->m_intUtilityProviderEventTypeId;
	}

	public function sqlUtilityProviderEventTypeId() {
		return ( true == isset( $this->m_intUtilityProviderEventTypeId ) ) ? ( string ) $this->m_intUtilityProviderEventTypeId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setConfirmedBy( $intConfirmedBy ) {
		$this->set( 'm_intConfirmedBy', CStrings::strToIntDef( $intConfirmedBy, NULL, false ) );
	}

	public function getConfirmedBy() {
		return $this->m_intConfirmedBy;
	}

	public function sqlConfirmedBy() {
		return ( true == isset( $this->m_intConfirmedBy ) ) ? ( string ) $this->m_intConfirmedBy : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setConfirmUtilityDocumentId( $intConfirmUtilityDocumentId ) {
		$this->set( 'm_intConfirmUtilityDocumentId', CStrings::strToIntDef( $intConfirmUtilityDocumentId, NULL, false ) );
	}

	public function getConfirmUtilityDocumentId() {
		return $this->m_intConfirmUtilityDocumentId;
	}

	public function sqlConfirmUtilityDocumentId() {
		return ( true == isset( $this->m_intConfirmUtilityDocumentId ) ) ? ( string ) $this->m_intConfirmUtilityDocumentId : 'NULL';
	}

	public function setFollowUpDate( $strFollowUpDate ) {
		$this->set( 'm_strFollowUpDate', CStrings::strTrimDef( $strFollowUpDate, -1, NULL, true ) );
	}

	public function getFollowUpDate() {
		return $this->m_strFollowUpDate;
	}

	public function sqlFollowUpDate() {
		return ( true == isset( $this->m_strFollowUpDate ) ) ? '\'' . $this->m_strFollowUpDate . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setConfirmedNotes( $strConfirmedNotes ) {
		$this->set( 'm_strConfirmedNotes', CStrings::strTrimDef( $strConfirmedNotes, -1, NULL, true ) );
	}

	public function getConfirmedNotes() {
		return $this->m_strConfirmedNotes;
	}

	public function sqlConfirmedNotes() {
		return ( true == isset( $this->m_strConfirmedNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strConfirmedNotes ) : '\'' . addslashes( $this->m_strConfirmedNotes ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, property_utility_provider_event_id, utility_provider_event_type_id, created_by, created_on, updated_by, updated_on, confirmed_by, confirmed_on, confirm_utility_document_id, follow_up_date, notes, confirmed_notes )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPropertyUtilityProviderEventId() . ', ' .
						$this->sqlUtilityProviderEventTypeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlConfirmedBy() . ', ' .
						$this->sqlConfirmedOn() . ', ' .
						$this->sqlConfirmUtilityDocumentId() . ', ' .
						$this->sqlFollowUpDate() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlConfirmedNotes() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_provider_event_id = ' . $this->sqlPropertyUtilityProviderEventId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityProviderEventId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_provider_event_id = ' . $this->sqlPropertyUtilityProviderEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_event_type_id = ' . $this->sqlUtilityProviderEventTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityProviderEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_event_type_id = ' . $this->sqlUtilityProviderEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_by = ' . $this->sqlConfirmedBy(). ',' ; } elseif( true == array_key_exists( 'ConfirmedBy', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_by = ' . $this->sqlConfirmedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn(). ',' ; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirm_utility_document_id = ' . $this->sqlConfirmUtilityDocumentId(). ',' ; } elseif( true == array_key_exists( 'ConfirmUtilityDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' confirm_utility_document_id = ' . $this->sqlConfirmUtilityDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' follow_up_date = ' . $this->sqlFollowUpDate(). ',' ; } elseif( true == array_key_exists( 'FollowUpDate', $this->getChangedColumns() ) ) { $strSql .= ' follow_up_date = ' . $this->sqlFollowUpDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_notes = ' . $this->sqlConfirmedNotes(). ',' ; } elseif( true == array_key_exists( 'ConfirmedNotes', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_notes = ' . $this->sqlConfirmedNotes() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'property_utility_provider_event_id' => $this->getPropertyUtilityProviderEventId(),
			'utility_provider_event_type_id' => $this->getUtilityProviderEventTypeId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'confirmed_by' => $this->getConfirmedBy(),
			'confirmed_on' => $this->getConfirmedOn(),
			'confirm_utility_document_id' => $this->getConfirmUtilityDocumentId(),
			'follow_up_date' => $this->getFollowUpDate(),
			'notes' => $this->getNotes(),
			'confirmed_notes' => $this->getConfirmedNotes()
		);
	}

}
?>