<?php

class CBasePropertyMeterDevicesFlowCount extends CEosSingularBase {

	const TABLE_NAME = 'public.property_meter_devices_flow_counts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyMeterDeviceId;
	protected $m_fltFlowCount;
	protected $m_strFlowCountDatetime;
	protected $m_boolIsVacant;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_meter_device_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyMeterDeviceId', trim( $arrValues['property_meter_device_id'] ) ); elseif( isset( $arrValues['property_meter_device_id'] ) ) $this->setPropertyMeterDeviceId( $arrValues['property_meter_device_id'] );
		if( isset( $arrValues['flow_count'] ) && $boolDirectSet ) $this->set( 'm_fltFlowCount', trim( $arrValues['flow_count'] ) ); elseif( isset( $arrValues['flow_count'] ) ) $this->setFlowCount( $arrValues['flow_count'] );
		if( isset( $arrValues['flow_count_datetime'] ) && $boolDirectSet ) $this->set( 'm_strFlowCountDatetime', trim( $arrValues['flow_count_datetime'] ) ); elseif( isset( $arrValues['flow_count_datetime'] ) ) $this->setFlowCountDatetime( $arrValues['flow_count_datetime'] );
		if( isset( $arrValues['is_vacant'] ) && $boolDirectSet ) $this->set( 'm_boolIsVacant', trim( stripcslashes( $arrValues['is_vacant'] ) ) ); elseif( isset( $arrValues['is_vacant'] ) ) $this->setIsVacant( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_vacant'] ) : $arrValues['is_vacant'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyMeterDeviceId( $intPropertyMeterDeviceId ) {
		$this->set( 'm_intPropertyMeterDeviceId', CStrings::strToIntDef( $intPropertyMeterDeviceId, NULL, false ) );
	}

	public function getPropertyMeterDeviceId() {
		return $this->m_intPropertyMeterDeviceId;
	}

	public function sqlPropertyMeterDeviceId() {
		return ( true == isset( $this->m_intPropertyMeterDeviceId ) ) ? ( string ) $this->m_intPropertyMeterDeviceId : 'NULL';
	}

	public function setFlowCount( $fltFlowCount ) {
		$this->set( 'm_fltFlowCount', CStrings::strToFloatDef( $fltFlowCount, NULL, false, 0 ) );
	}

	public function getFlowCount() {
		return $this->m_fltFlowCount;
	}

	public function sqlFlowCount() {
		return ( true == isset( $this->m_fltFlowCount ) ) ? ( string ) $this->m_fltFlowCount : 'NULL';
	}

	public function setFlowCountDatetime( $strFlowCountDatetime ) {
		$this->set( 'm_strFlowCountDatetime', CStrings::strTrimDef( $strFlowCountDatetime, -1, NULL, true ) );
	}

	public function getFlowCountDatetime() {
		return $this->m_strFlowCountDatetime;
	}

	public function sqlFlowCountDatetime() {
		return ( true == isset( $this->m_strFlowCountDatetime ) ) ? '\'' . $this->m_strFlowCountDatetime . '\'' : 'NULL';
	}

	public function setIsVacant( $boolIsVacant ) {
		$this->set( 'm_boolIsVacant', CStrings::strToBool( $boolIsVacant ) );
	}

	public function getIsVacant() {
		return $this->m_boolIsVacant;
	}

	public function sqlIsVacant() {
		return ( true == isset( $this->m_boolIsVacant ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVacant ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_meter_device_id, flow_count, flow_count_datetime, is_vacant, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyMeterDeviceId() . ', ' .
 						$this->sqlFlowCount() . ', ' .
 						$this->sqlFlowCountDatetime() . ', ' .
 						$this->sqlIsVacant() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_meter_device_id = ' . $this->sqlPropertyMeterDeviceId() . ','; } elseif( true == array_key_exists( 'PropertyMeterDeviceId', $this->getChangedColumns() ) ) { $strSql .= ' property_meter_device_id = ' . $this->sqlPropertyMeterDeviceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' flow_count = ' . $this->sqlFlowCount() . ','; } elseif( true == array_key_exists( 'FlowCount', $this->getChangedColumns() ) ) { $strSql .= ' flow_count = ' . $this->sqlFlowCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' flow_count_datetime = ' . $this->sqlFlowCountDatetime() . ','; } elseif( true == array_key_exists( 'FlowCountDatetime', $this->getChangedColumns() ) ) { $strSql .= ' flow_count_datetime = ' . $this->sqlFlowCountDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_vacant = ' . $this->sqlIsVacant() . ','; } elseif( true == array_key_exists( 'IsVacant', $this->getChangedColumns() ) ) { $strSql .= ' is_vacant = ' . $this->sqlIsVacant() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_meter_device_id' => $this->getPropertyMeterDeviceId(),
			'flow_count' => $this->getFlowCount(),
			'flow_count_datetime' => $this->getFlowCountDatetime(),
			'is_vacant' => $this->getIsVacant(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>