<?php

class CBaseUtilityBillAudit extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_audits';

	protected $m_intId;
	protected $m_intUtilityBillAuditTypeId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intUtilityBillAuditStatusTypeId;
	protected $m_intUtilityBillAuditResolutionId;
	protected $m_intUtilityBillId;
	protected $m_intAuditUtilityBillId;
	protected $m_intUtilityBillChargeId;
	protected $m_strUtilityBillAuditMemo;
	protected $m_strUtilityBillAuditProgress;
	protected $m_jsonUtilityBillAuditProgress;
	protected $m_fltSavings;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUtilityBillAccountMeterId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_bill_audit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAuditTypeId', trim( $arrValues['utility_bill_audit_type_id'] ) ); elseif( isset( $arrValues['utility_bill_audit_type_id'] ) ) $this->setUtilityBillAuditTypeId( $arrValues['utility_bill_audit_type_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['utility_bill_audit_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAuditStatusTypeId', trim( $arrValues['utility_bill_audit_status_type_id'] ) ); elseif( isset( $arrValues['utility_bill_audit_status_type_id'] ) ) $this->setUtilityBillAuditStatusTypeId( $arrValues['utility_bill_audit_status_type_id'] );
		if( isset( $arrValues['utility_bill_audit_resolution_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAuditResolutionId', trim( $arrValues['utility_bill_audit_resolution_id'] ) ); elseif( isset( $arrValues['utility_bill_audit_resolution_id'] ) ) $this->setUtilityBillAuditResolutionId( $arrValues['utility_bill_audit_resolution_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['audit_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intAuditUtilityBillId', trim( $arrValues['audit_utility_bill_id'] ) ); elseif( isset( $arrValues['audit_utility_bill_id'] ) ) $this->setAuditUtilityBillId( $arrValues['audit_utility_bill_id'] );
		if( isset( $arrValues['utility_bill_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillChargeId', trim( $arrValues['utility_bill_charge_id'] ) ); elseif( isset( $arrValues['utility_bill_charge_id'] ) ) $this->setUtilityBillChargeId( $arrValues['utility_bill_charge_id'] );
		if( isset( $arrValues['utility_bill_audit_memo'] ) && $boolDirectSet ) $this->set( 'm_strUtilityBillAuditMemo', trim( stripcslashes( $arrValues['utility_bill_audit_memo'] ) ) ); elseif( isset( $arrValues['utility_bill_audit_memo'] ) ) $this->setUtilityBillAuditMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['utility_bill_audit_memo'] ) : $arrValues['utility_bill_audit_memo'] );
		if( isset( $arrValues['utility_bill_audit_progress'] ) ) $this->set( 'm_strUtilityBillAuditProgress', trim( $arrValues['utility_bill_audit_progress'] ) );
		if( isset( $arrValues['savings'] ) && $boolDirectSet ) $this->set( 'm_fltSavings', trim( $arrValues['savings'] ) ); elseif( isset( $arrValues['savings'] ) ) $this->setSavings( $arrValues['savings'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['utility_bill_account_meter_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountMeterId', trim( $arrValues['utility_bill_account_meter_id'] ) ); elseif( isset( $arrValues['utility_bill_account_meter_id'] ) ) $this->setUtilityBillAccountMeterId( $arrValues['utility_bill_account_meter_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBillAuditTypeId( $intUtilityBillAuditTypeId ) {
		$this->set( 'm_intUtilityBillAuditTypeId', CStrings::strToIntDef( $intUtilityBillAuditTypeId, NULL, false ) );
	}

	public function getUtilityBillAuditTypeId() {
		return $this->m_intUtilityBillAuditTypeId;
	}

	public function sqlUtilityBillAuditTypeId() {
		return ( true == isset( $this->m_intUtilityBillAuditTypeId ) ) ? ( string ) $this->m_intUtilityBillAuditTypeId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setUtilityBillAuditStatusTypeId( $intUtilityBillAuditStatusTypeId ) {
		$this->set( 'm_intUtilityBillAuditStatusTypeId', CStrings::strToIntDef( $intUtilityBillAuditStatusTypeId, NULL, false ) );
	}

	public function getUtilityBillAuditStatusTypeId() {
		return $this->m_intUtilityBillAuditStatusTypeId;
	}

	public function sqlUtilityBillAuditStatusTypeId() {
		return ( true == isset( $this->m_intUtilityBillAuditStatusTypeId ) ) ? ( string ) $this->m_intUtilityBillAuditStatusTypeId : 'NULL';
	}

	public function setUtilityBillAuditResolutionId( $intUtilityBillAuditResolutionId ) {
		$this->set( 'm_intUtilityBillAuditResolutionId', CStrings::strToIntDef( $intUtilityBillAuditResolutionId, NULL, false ) );
	}

	public function getUtilityBillAuditResolutionId() {
		return $this->m_intUtilityBillAuditResolutionId;
	}

	public function sqlUtilityBillAuditResolutionId() {
		return ( true == isset( $this->m_intUtilityBillAuditResolutionId ) ) ? ( string ) $this->m_intUtilityBillAuditResolutionId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setAuditUtilityBillId( $intAuditUtilityBillId ) {
		$this->set( 'm_intAuditUtilityBillId', CStrings::strToIntDef( $intAuditUtilityBillId, NULL, false ) );
	}

	public function getAuditUtilityBillId() {
		return $this->m_intAuditUtilityBillId;
	}

	public function sqlAuditUtilityBillId() {
		return ( true == isset( $this->m_intAuditUtilityBillId ) ) ? ( string ) $this->m_intAuditUtilityBillId : 'NULL';
	}

	public function setUtilityBillChargeId( $intUtilityBillChargeId ) {
		$this->set( 'm_intUtilityBillChargeId', CStrings::strToIntDef( $intUtilityBillChargeId, NULL, false ) );
	}

	public function getUtilityBillChargeId() {
		return $this->m_intUtilityBillChargeId;
	}

	public function sqlUtilityBillChargeId() {
		return ( true == isset( $this->m_intUtilityBillChargeId ) ) ? ( string ) $this->m_intUtilityBillChargeId : 'NULL';
	}

	public function setUtilityBillAuditMemo( $strUtilityBillAuditMemo ) {
		$this->set( 'm_strUtilityBillAuditMemo', CStrings::strTrimDef( $strUtilityBillAuditMemo, -1, NULL, true ) );
	}

	public function getUtilityBillAuditMemo() {
		return $this->m_strUtilityBillAuditMemo;
	}

	public function sqlUtilityBillAuditMemo() {
		return ( true == isset( $this->m_strUtilityBillAuditMemo ) ) ? '\'' . addslashes( $this->m_strUtilityBillAuditMemo ) . '\'' : 'NULL';
	}

	public function setUtilityBillAuditProgress( $jsonUtilityBillAuditProgress ) {
		if( true == valObj( $jsonUtilityBillAuditProgress, 'stdClass' ) ) {
			$this->set( 'm_jsonUtilityBillAuditProgress', $jsonUtilityBillAuditProgress );
		} elseif( true == valJsonString( $jsonUtilityBillAuditProgress ) ) {
			$this->set( 'm_jsonUtilityBillAuditProgress', CStrings::strToJson( $jsonUtilityBillAuditProgress ) );
		} else {
			$this->set( 'm_jsonUtilityBillAuditProgress', NULL ); 
		}
		unset( $this->m_strUtilityBillAuditProgress );
	}

	public function getUtilityBillAuditProgress() {
		if( true == isset( $this->m_strUtilityBillAuditProgress ) ) {
			$this->m_jsonUtilityBillAuditProgress = CStrings::strToJson( $this->m_strUtilityBillAuditProgress );
			unset( $this->m_strUtilityBillAuditProgress );
		}
		return $this->m_jsonUtilityBillAuditProgress;
	}

	public function sqlUtilityBillAuditProgress() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getUtilityBillAuditProgress() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getUtilityBillAuditProgress() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setSavings( $fltSavings ) {
		$this->set( 'm_fltSavings', CStrings::strToFloatDef( $fltSavings, NULL, false, 4 ) );
	}

	public function getSavings() {
		return $this->m_fltSavings;
	}

	public function sqlSavings() {
		return ( true == isset( $this->m_fltSavings ) ) ? ( string ) $this->m_fltSavings : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUtilityBillAccountMeterId( $intUtilityBillAccountMeterId ) {
		$this->set( 'm_intUtilityBillAccountMeterId', CStrings::strToIntDef( $intUtilityBillAccountMeterId, NULL, false ) );
	}

	public function getUtilityBillAccountMeterId() {
		return $this->m_intUtilityBillAccountMeterId;
	}

	public function sqlUtilityBillAccountMeterId() {
		return ( true == isset( $this->m_intUtilityBillAccountMeterId ) ) ? ( string ) $this->m_intUtilityBillAccountMeterId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_bill_audit_type_id, property_utility_type_id, utility_bill_audit_status_type_id, utility_bill_audit_resolution_id, utility_bill_id, audit_utility_bill_id, utility_bill_charge_id, utility_bill_audit_memo, utility_bill_audit_progress, savings, updated_by, updated_on, created_by, created_on, utility_bill_account_meter_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityBillAuditTypeId() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlUtilityBillAuditStatusTypeId() . ', ' .
						$this->sqlUtilityBillAuditResolutionId() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlAuditUtilityBillId() . ', ' .
						$this->sqlUtilityBillChargeId() . ', ' .
						$this->sqlUtilityBillAuditMemo() . ', ' .
						$this->sqlUtilityBillAuditProgress() . ', ' .
						$this->sqlSavings() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUtilityBillAccountMeterId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_audit_type_id = ' . $this->sqlUtilityBillAuditTypeId() . ','; } elseif( true == array_key_exists( 'UtilityBillAuditTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_audit_type_id = ' . $this->sqlUtilityBillAuditTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_audit_status_type_id = ' . $this->sqlUtilityBillAuditStatusTypeId() . ','; } elseif( true == array_key_exists( 'UtilityBillAuditStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_audit_status_type_id = ' . $this->sqlUtilityBillAuditStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_audit_resolution_id = ' . $this->sqlUtilityBillAuditResolutionId() . ','; } elseif( true == array_key_exists( 'UtilityBillAuditResolutionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_audit_resolution_id = ' . $this->sqlUtilityBillAuditResolutionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_utility_bill_id = ' . $this->sqlAuditUtilityBillId() . ','; } elseif( true == array_key_exists( 'AuditUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' audit_utility_bill_id = ' . $this->sqlAuditUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_charge_id = ' . $this->sqlUtilityBillChargeId() . ','; } elseif( true == array_key_exists( 'UtilityBillChargeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_charge_id = ' . $this->sqlUtilityBillChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_audit_memo = ' . $this->sqlUtilityBillAuditMemo() . ','; } elseif( true == array_key_exists( 'UtilityBillAuditMemo', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_audit_memo = ' . $this->sqlUtilityBillAuditMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_audit_progress = ' . $this->sqlUtilityBillAuditProgress() . ','; } elseif( true == array_key_exists( 'UtilityBillAuditProgress', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_audit_progress = ' . $this->sqlUtilityBillAuditProgress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' savings = ' . $this->sqlSavings() . ','; } elseif( true == array_key_exists( 'Savings', $this->getChangedColumns() ) ) { $strSql .= ' savings = ' . $this->sqlSavings() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_meter_id = ' . $this->sqlUtilityBillAccountMeterId() . ','; } elseif( true == array_key_exists( 'UtilityBillAccountMeterId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_meter_id = ' . $this->sqlUtilityBillAccountMeterId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_bill_audit_type_id' => $this->getUtilityBillAuditTypeId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'utility_bill_audit_status_type_id' => $this->getUtilityBillAuditStatusTypeId(),
			'utility_bill_audit_resolution_id' => $this->getUtilityBillAuditResolutionId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'audit_utility_bill_id' => $this->getAuditUtilityBillId(),
			'utility_bill_charge_id' => $this->getUtilityBillChargeId(),
			'utility_bill_audit_memo' => $this->getUtilityBillAuditMemo(),
			'utility_bill_audit_progress' => $this->getUtilityBillAuditProgress(),
			'savings' => $this->getSavings(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'utility_bill_account_meter_id' => $this->getUtilityBillAccountMeterId()
		);
	}

}
?>