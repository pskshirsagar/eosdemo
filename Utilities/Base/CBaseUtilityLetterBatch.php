<?php

class CBaseUtilityLetterBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_letter_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityDistributionId;
	protected $m_intUtilityDocumentId;
	protected $m_intUtilityNoticeId;
	protected $m_intUtilityTemplateId;
	protected $m_intUtilityBatchId;
	protected $m_intFeeTransactionId;
	protected $m_intPostageTransactionId;
	protected $m_strDistributionDeadline;
	protected $m_intRequiresMerge;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intRequiresMerge = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDistributionId', trim( $arrValues['utility_distribution_id'] ) ); elseif( isset( $arrValues['utility_distribution_id'] ) ) $this->setUtilityDistributionId( $arrValues['utility_distribution_id'] );
		if( isset( $arrValues['utility_document_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDocumentId', trim( $arrValues['utility_document_id'] ) ); elseif( isset( $arrValues['utility_document_id'] ) ) $this->setUtilityDocumentId( $arrValues['utility_document_id'] );
		if( isset( $arrValues['utility_notice_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityNoticeId', trim( $arrValues['utility_notice_id'] ) ); elseif( isset( $arrValues['utility_notice_id'] ) ) $this->setUtilityNoticeId( $arrValues['utility_notice_id'] );
		if( isset( $arrValues['utility_template_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTemplateId', trim( $arrValues['utility_template_id'] ) ); elseif( isset( $arrValues['utility_template_id'] ) ) $this->setUtilityTemplateId( $arrValues['utility_template_id'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['fee_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeTransactionId', trim( $arrValues['fee_transaction_id'] ) ); elseif( isset( $arrValues['fee_transaction_id'] ) ) $this->setFeeTransactionId( $arrValues['fee_transaction_id'] );
		if( isset( $arrValues['postage_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intPostageTransactionId', trim( $arrValues['postage_transaction_id'] ) ); elseif( isset( $arrValues['postage_transaction_id'] ) ) $this->setPostageTransactionId( $arrValues['postage_transaction_id'] );
		if( isset( $arrValues['distribution_deadline'] ) && $boolDirectSet ) $this->set( 'm_strDistributionDeadline', trim( $arrValues['distribution_deadline'] ) ); elseif( isset( $arrValues['distribution_deadline'] ) ) $this->setDistributionDeadline( $arrValues['distribution_deadline'] );
		if( isset( $arrValues['requires_merge'] ) && $boolDirectSet ) $this->set( 'm_intRequiresMerge', trim( $arrValues['requires_merge'] ) ); elseif( isset( $arrValues['requires_merge'] ) ) $this->setRequiresMerge( $arrValues['requires_merge'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityDistributionId( $intUtilityDistributionId ) {
		$this->set( 'm_intUtilityDistributionId', CStrings::strToIntDef( $intUtilityDistributionId, NULL, false ) );
	}

	public function getUtilityDistributionId() {
		return $this->m_intUtilityDistributionId;
	}

	public function sqlUtilityDistributionId() {
		return ( true == isset( $this->m_intUtilityDistributionId ) ) ? ( string ) $this->m_intUtilityDistributionId : 'NULL';
	}

	public function setUtilityDocumentId( $intUtilityDocumentId ) {
		$this->set( 'm_intUtilityDocumentId', CStrings::strToIntDef( $intUtilityDocumentId, NULL, false ) );
	}

	public function getUtilityDocumentId() {
		return $this->m_intUtilityDocumentId;
	}

	public function sqlUtilityDocumentId() {
		return ( true == isset( $this->m_intUtilityDocumentId ) ) ? ( string ) $this->m_intUtilityDocumentId : 'NULL';
	}

	public function setUtilityNoticeId( $intUtilityNoticeId ) {
		$this->set( 'm_intUtilityNoticeId', CStrings::strToIntDef( $intUtilityNoticeId, NULL, false ) );
	}

	public function getUtilityNoticeId() {
		return $this->m_intUtilityNoticeId;
	}

	public function sqlUtilityNoticeId() {
		return ( true == isset( $this->m_intUtilityNoticeId ) ) ? ( string ) $this->m_intUtilityNoticeId : 'NULL';
	}

	public function setUtilityTemplateId( $intUtilityTemplateId ) {
		$this->set( 'm_intUtilityTemplateId', CStrings::strToIntDef( $intUtilityTemplateId, NULL, false ) );
	}

	public function getUtilityTemplateId() {
		return $this->m_intUtilityTemplateId;
	}

	public function sqlUtilityTemplateId() {
		return ( true == isset( $this->m_intUtilityTemplateId ) ) ? ( string ) $this->m_intUtilityTemplateId : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setFeeTransactionId( $intFeeTransactionId ) {
		$this->set( 'm_intFeeTransactionId', CStrings::strToIntDef( $intFeeTransactionId, NULL, false ) );
	}

	public function getFeeTransactionId() {
		return $this->m_intFeeTransactionId;
	}

	public function sqlFeeTransactionId() {
		return ( true == isset( $this->m_intFeeTransactionId ) ) ? ( string ) $this->m_intFeeTransactionId : 'NULL';
	}

	public function setPostageTransactionId( $intPostageTransactionId ) {
		$this->set( 'm_intPostageTransactionId', CStrings::strToIntDef( $intPostageTransactionId, NULL, false ) );
	}

	public function getPostageTransactionId() {
		return $this->m_intPostageTransactionId;
	}

	public function sqlPostageTransactionId() {
		return ( true == isset( $this->m_intPostageTransactionId ) ) ? ( string ) $this->m_intPostageTransactionId : 'NULL';
	}

	public function setDistributionDeadline( $strDistributionDeadline ) {
		$this->set( 'm_strDistributionDeadline', CStrings::strTrimDef( $strDistributionDeadline, -1, NULL, true ) );
	}

	public function getDistributionDeadline() {
		return $this->m_strDistributionDeadline;
	}

	public function sqlDistributionDeadline() {
		return ( true == isset( $this->m_strDistributionDeadline ) ) ? '\'' . $this->m_strDistributionDeadline . '\'' : 'NOW()';
	}

	public function setRequiresMerge( $intRequiresMerge ) {
		$this->set( 'm_intRequiresMerge', CStrings::strToIntDef( $intRequiresMerge, NULL, false ) );
	}

	public function getRequiresMerge() {
		return $this->m_intRequiresMerge;
	}

	public function sqlRequiresMerge() {
		return ( true == isset( $this->m_intRequiresMerge ) ) ? ( string ) $this->m_intRequiresMerge : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_distribution_id, utility_document_id, utility_notice_id, utility_template_id, utility_batch_id, fee_transaction_id, postage_transaction_id, distribution_deadline, requires_merge, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUtilityDistributionId() . ', ' .
 						$this->sqlUtilityDocumentId() . ', ' .
 						$this->sqlUtilityNoticeId() . ', ' .
 						$this->sqlUtilityTemplateId() . ', ' .
 						$this->sqlUtilityBatchId() . ', ' .
 						$this->sqlFeeTransactionId() . ', ' .
 						$this->sqlPostageTransactionId() . ', ' .
 						$this->sqlDistributionDeadline() . ', ' .
 						$this->sqlRequiresMerge() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_distribution_id = ' . $this->sqlUtilityDistributionId() . ','; } elseif( true == array_key_exists( 'UtilityDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_distribution_id = ' . $this->sqlUtilityDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId() . ','; } elseif( true == array_key_exists( 'UtilityDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_notice_id = ' . $this->sqlUtilityNoticeId() . ','; } elseif( true == array_key_exists( 'UtilityNoticeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_notice_id = ' . $this->sqlUtilityNoticeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_template_id = ' . $this->sqlUtilityTemplateId() . ','; } elseif( true == array_key_exists( 'UtilityTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' utility_template_id = ' . $this->sqlUtilityTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_transaction_id = ' . $this->sqlFeeTransactionId() . ','; } elseif( true == array_key_exists( 'FeeTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' fee_transaction_id = ' . $this->sqlFeeTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postage_transaction_id = ' . $this->sqlPostageTransactionId() . ','; } elseif( true == array_key_exists( 'PostageTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' postage_transaction_id = ' . $this->sqlPostageTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_deadline = ' . $this->sqlDistributionDeadline() . ','; } elseif( true == array_key_exists( 'DistributionDeadline', $this->getChangedColumns() ) ) { $strSql .= ' distribution_deadline = ' . $this->sqlDistributionDeadline() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_merge = ' . $this->sqlRequiresMerge() . ','; } elseif( true == array_key_exists( 'RequiresMerge', $this->getChangedColumns() ) ) { $strSql .= ' requires_merge = ' . $this->sqlRequiresMerge() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_distribution_id' => $this->getUtilityDistributionId(),
			'utility_document_id' => $this->getUtilityDocumentId(),
			'utility_notice_id' => $this->getUtilityNoticeId(),
			'utility_template_id' => $this->getUtilityTemplateId(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'fee_transaction_id' => $this->getFeeTransactionId(),
			'postage_transaction_id' => $this->getPostageTransactionId(),
			'distribution_deadline' => $this->getDistributionDeadline(),
			'requires_merge' => $this->getRequiresMerge(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>