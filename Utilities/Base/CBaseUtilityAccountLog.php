<?php

class CBaseUtilityAccountLog extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_account_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUtilityAccountId;
	protected $m_intUtilityAccountLogTypeId;
	protected $m_intUtilityInvoiceId;
	protected $m_intUtilityTransactionId;
	protected $m_intEmloyeeId;
	protected $m_intCompanyEmloyeeId;
	protected $m_intVcrUtilityBillId;
	protected $m_strLogDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['utility_account_log_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountLogTypeId', trim( $arrValues['utility_account_log_type_id'] ) ); elseif( isset( $arrValues['utility_account_log_type_id'] ) ) $this->setUtilityAccountLogTypeId( $arrValues['utility_account_log_type_id'] );
		if( isset( $arrValues['utility_invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityInvoiceId', trim( $arrValues['utility_invoice_id'] ) ); elseif( isset( $arrValues['utility_invoice_id'] ) ) $this->setUtilityInvoiceId( $arrValues['utility_invoice_id'] );
		if( isset( $arrValues['utility_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTransactionId', trim( $arrValues['utility_transaction_id'] ) ); elseif( isset( $arrValues['utility_transaction_id'] ) ) $this->setUtilityTransactionId( $arrValues['utility_transaction_id'] );
		if( isset( $arrValues['emloyee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmloyeeId', trim( $arrValues['emloyee_id'] ) ); elseif( isset( $arrValues['emloyee_id'] ) ) $this->setEmloyeeId( $arrValues['emloyee_id'] );
		if( isset( $arrValues['company_emloyee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmloyeeId', trim( $arrValues['company_emloyee_id'] ) ); elseif( isset( $arrValues['company_emloyee_id'] ) ) $this->setCompanyEmloyeeId( $arrValues['company_emloyee_id'] );
		if( isset( $arrValues['vcr_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intVcrUtilityBillId', trim( $arrValues['vcr_utility_bill_id'] ) ); elseif( isset( $arrValues['vcr_utility_bill_id'] ) ) $this->setVcrUtilityBillId( $arrValues['vcr_utility_bill_id'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setUtilityAccountLogTypeId( $intUtilityAccountLogTypeId ) {
		$this->set( 'm_intUtilityAccountLogTypeId', CStrings::strToIntDef( $intUtilityAccountLogTypeId, NULL, false ) );
	}

	public function getUtilityAccountLogTypeId() {
		return $this->m_intUtilityAccountLogTypeId;
	}

	public function sqlUtilityAccountLogTypeId() {
		return ( true == isset( $this->m_intUtilityAccountLogTypeId ) ) ? ( string ) $this->m_intUtilityAccountLogTypeId : 'NULL';
	}

	public function setUtilityInvoiceId( $intUtilityInvoiceId ) {
		$this->set( 'm_intUtilityInvoiceId', CStrings::strToIntDef( $intUtilityInvoiceId, NULL, false ) );
	}

	public function getUtilityInvoiceId() {
		return $this->m_intUtilityInvoiceId;
	}

	public function sqlUtilityInvoiceId() {
		return ( true == isset( $this->m_intUtilityInvoiceId ) ) ? ( string ) $this->m_intUtilityInvoiceId : 'NULL';
	}

	public function setUtilityTransactionId( $intUtilityTransactionId ) {
		$this->set( 'm_intUtilityTransactionId', CStrings::strToIntDef( $intUtilityTransactionId, NULL, false ) );
	}

	public function getUtilityTransactionId() {
		return $this->m_intUtilityTransactionId;
	}

	public function sqlUtilityTransactionId() {
		return ( true == isset( $this->m_intUtilityTransactionId ) ) ? ( string ) $this->m_intUtilityTransactionId : 'NULL';
	}

	public function setEmloyeeId( $intEmloyeeId ) {
		$this->set( 'm_intEmloyeeId', CStrings::strToIntDef( $intEmloyeeId, NULL, false ) );
	}

	public function getEmloyeeId() {
		return $this->m_intEmloyeeId;
	}

	public function sqlEmloyeeId() {
		return ( true == isset( $this->m_intEmloyeeId ) ) ? ( string ) $this->m_intEmloyeeId : 'NULL';
	}

	public function setCompanyEmloyeeId( $intCompanyEmloyeeId ) {
		$this->set( 'm_intCompanyEmloyeeId', CStrings::strToIntDef( $intCompanyEmloyeeId, NULL, false ) );
	}

	public function getCompanyEmloyeeId() {
		return $this->m_intCompanyEmloyeeId;
	}

	public function sqlCompanyEmloyeeId() {
		return ( true == isset( $this->m_intCompanyEmloyeeId ) ) ? ( string ) $this->m_intCompanyEmloyeeId : 'NULL';
	}

	public function setVcrUtilityBillId( $intVcrUtilityBillId ) {
		$this->set( 'm_intVcrUtilityBillId', CStrings::strToIntDef( $intVcrUtilityBillId, NULL, false ) );
	}

	public function getVcrUtilityBillId() {
		return $this->m_intVcrUtilityBillId;
	}

	public function sqlVcrUtilityBillId() {
		return ( true == isset( $this->m_intVcrUtilityBillId ) ) ? ( string ) $this->m_intVcrUtilityBillId : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, utility_account_id, utility_account_log_type_id, utility_invoice_id, utility_transaction_id, emloyee_id, company_emloyee_id, vcr_utility_bill_id, log_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlUtilityAccountId() . ', ' .
 						$this->sqlUtilityAccountLogTypeId() . ', ' .
 						$this->sqlUtilityInvoiceId() . ', ' .
 						$this->sqlUtilityTransactionId() . ', ' .
 						$this->sqlEmloyeeId() . ', ' .
 						$this->sqlCompanyEmloyeeId() . ', ' .
 						$this->sqlVcrUtilityBillId() . ', ' .
 						$this->sqlLogDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_log_type_id = ' . $this->sqlUtilityAccountLogTypeId() . ','; } elseif( true == array_key_exists( 'UtilityAccountLogTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_log_type_id = ' . $this->sqlUtilityAccountLogTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_invoice_id = ' . $this->sqlUtilityInvoiceId() . ','; } elseif( true == array_key_exists( 'UtilityInvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' utility_invoice_id = ' . $this->sqlUtilityInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_transaction_id = ' . $this->sqlUtilityTransactionId() . ','; } elseif( true == array_key_exists( 'UtilityTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_transaction_id = ' . $this->sqlUtilityTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emloyee_id = ' . $this->sqlEmloyeeId() . ','; } elseif( true == array_key_exists( 'EmloyeeId', $this->getChangedColumns() ) ) { $strSql .= ' emloyee_id = ' . $this->sqlEmloyeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_emloyee_id = ' . $this->sqlCompanyEmloyeeId() . ','; } elseif( true == array_key_exists( 'CompanyEmloyeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_emloyee_id = ' . $this->sqlCompanyEmloyeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vcr_utility_bill_id = ' . $this->sqlVcrUtilityBillId() . ','; } elseif( true == array_key_exists( 'VcrUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' vcr_utility_bill_id = ' . $this->sqlVcrUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'utility_account_log_type_id' => $this->getUtilityAccountLogTypeId(),
			'utility_invoice_id' => $this->getUtilityInvoiceId(),
			'utility_transaction_id' => $this->getUtilityTransactionId(),
			'emloyee_id' => $this->getEmloyeeId(),
			'company_emloyee_id' => $this->getCompanyEmloyeeId(),
			'vcr_utility_bill_id' => $this->getVcrUtilityBillId(),
			'log_datetime' => $this->getLogDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>