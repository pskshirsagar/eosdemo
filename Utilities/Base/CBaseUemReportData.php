<?php

class CBaseUemReportData extends CEosSingularBase {

	const TABLE_NAME = 'public.uem_report_datas';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strReportDate;
	protected $m_intUtilityBillsCreatedCount;
	protected $m_intUtilityBillsExportedCount;
	protected $m_intDueDateExportTenDayCount;
	protected $m_intDueDateExportFiveDayCount;
	protected $m_intDueDateExportAverageDays;
	protected $m_strReceiptToExportAverageTime;
	protected $m_intReceiptBillDateAverageDays;
	protected $m_intReceiptBillDateFiveDayCount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDueDateExportLateCount;
	protected $m_intDueDateBillCount;
	protected $m_intBillDateBillCount;
	protected $m_intDisconnectNoticesCreatedCount;
	protected $m_intDisconnectNoticesCompletedCount;
	protected $m_intDisconnectNoticesLateCount;
	protected $m_strDisconnectNoticesAverageTime;
	protected $m_intLateFeeUtilityBillCount;
	protected $m_intImplementationScheduledPropertyCount;
	protected $m_intImplementationOnTimePropertyCount;
	protected $m_intImplementationLatePropertyCount;
	protected $m_intImplementationAccountsCreated;
	protected $m_intImplementationAccountsCreatedPostGoLive;
	protected $m_intAssociatedBillCount;
	protected $m_strAssociatedBillAverageTime;
	protected $m_intAssociatedBillOnTimeCount;
	protected $m_intAssociatedBillOcrCount;
	protected $m_intProcessedBillCount;
	protected $m_strProcessedBillAverageTime;
	protected $m_intProcessedBillOnTimeCount;
	protected $m_intProcessedBillErrorCount;
	protected $m_intAuditsCreatedCount;
	protected $m_intAuditsCompletedCount;
	protected $m_strAuditsAverageTime;
	protected $m_intAuditsCompletedOnTimeCount;
	protected $m_intDeletedBillsCount;
	protected $m_intReceiptToExportTimelinessCount;
	protected $m_intRteTimelinessTwentyFourHoursCount;
	protected $m_intUtilityBillAccountsCount;
	protected $m_intUtilityBillAccountsCoaConfirmedCount;

	public function __construct() {
		parent::__construct();

		$this->m_intUtilityBillsCreatedCount = '0';
		$this->m_intUtilityBillsExportedCount = '0';
		$this->m_intDueDateExportTenDayCount = '0';
		$this->m_intDueDateExportFiveDayCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['report_date'] ) && $boolDirectSet ) $this->set( 'm_strReportDate', trim( $arrValues['report_date'] ) ); elseif( isset( $arrValues['report_date'] ) ) $this->setReportDate( $arrValues['report_date'] );
		if( isset( $arrValues['utility_bills_created_count'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillsCreatedCount', trim( $arrValues['utility_bills_created_count'] ) ); elseif( isset( $arrValues['utility_bills_created_count'] ) ) $this->setUtilityBillsCreatedCount( $arrValues['utility_bills_created_count'] );
		if( isset( $arrValues['utility_bills_exported_count'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillsExportedCount', trim( $arrValues['utility_bills_exported_count'] ) ); elseif( isset( $arrValues['utility_bills_exported_count'] ) ) $this->setUtilityBillsExportedCount( $arrValues['utility_bills_exported_count'] );
		if( isset( $arrValues['due_date_export_ten_day_count'] ) && $boolDirectSet ) $this->set( 'm_intDueDateExportTenDayCount', trim( $arrValues['due_date_export_ten_day_count'] ) ); elseif( isset( $arrValues['due_date_export_ten_day_count'] ) ) $this->setDueDateExportTenDayCount( $arrValues['due_date_export_ten_day_count'] );
		if( isset( $arrValues['due_date_export_five_day_count'] ) && $boolDirectSet ) $this->set( 'm_intDueDateExportFiveDayCount', trim( $arrValues['due_date_export_five_day_count'] ) ); elseif( isset( $arrValues['due_date_export_five_day_count'] ) ) $this->setDueDateExportFiveDayCount( $arrValues['due_date_export_five_day_count'] );
		if( isset( $arrValues['due_date_export_average_days'] ) && $boolDirectSet ) $this->set( 'm_intDueDateExportAverageDays', trim( $arrValues['due_date_export_average_days'] ) ); elseif( isset( $arrValues['due_date_export_average_days'] ) ) $this->setDueDateExportAverageDays( $arrValues['due_date_export_average_days'] );
		if( isset( $arrValues['receipt_to_export_average_time'] ) && $boolDirectSet ) $this->set( 'm_strReceiptToExportAverageTime', trim( $arrValues['receipt_to_export_average_time'] ) ); elseif( isset( $arrValues['receipt_to_export_average_time'] ) ) $this->setReceiptToExportAverageTime( $arrValues['receipt_to_export_average_time'] );
		if( isset( $arrValues['receipt_bill_date_average_days'] ) && $boolDirectSet ) $this->set( 'm_intReceiptBillDateAverageDays', trim( $arrValues['receipt_bill_date_average_days'] ) ); elseif( isset( $arrValues['receipt_bill_date_average_days'] ) ) $this->setReceiptBillDateAverageDays( $arrValues['receipt_bill_date_average_days'] );
		if( isset( $arrValues['receipt_bill_date_five_day_count'] ) && $boolDirectSet ) $this->set( 'm_intReceiptBillDateFiveDayCount', trim( $arrValues['receipt_bill_date_five_day_count'] ) ); elseif( isset( $arrValues['receipt_bill_date_five_day_count'] ) ) $this->setReceiptBillDateFiveDayCount( $arrValues['receipt_bill_date_five_day_count'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['due_date_export_late_count'] ) && $boolDirectSet ) $this->set( 'm_intDueDateExportLateCount', trim( $arrValues['due_date_export_late_count'] ) ); elseif( isset( $arrValues['due_date_export_late_count'] ) ) $this->setDueDateExportLateCount( $arrValues['due_date_export_late_count'] );
		if( isset( $arrValues['due_date_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intDueDateBillCount', trim( $arrValues['due_date_bill_count'] ) ); elseif( isset( $arrValues['due_date_bill_count'] ) ) $this->setDueDateBillCount( $arrValues['due_date_bill_count'] );
		if( isset( $arrValues['bill_date_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intBillDateBillCount', trim( $arrValues['bill_date_bill_count'] ) ); elseif( isset( $arrValues['bill_date_bill_count'] ) ) $this->setBillDateBillCount( $arrValues['bill_date_bill_count'] );
		if( isset( $arrValues['disconnect_notices_created_count'] ) && $boolDirectSet ) $this->set( 'm_intDisconnectNoticesCreatedCount', trim( $arrValues['disconnect_notices_created_count'] ) ); elseif( isset( $arrValues['disconnect_notices_created_count'] ) ) $this->setDisconnectNoticesCreatedCount( $arrValues['disconnect_notices_created_count'] );
		if( isset( $arrValues['disconnect_notices_completed_count'] ) && $boolDirectSet ) $this->set( 'm_intDisconnectNoticesCompletedCount', trim( $arrValues['disconnect_notices_completed_count'] ) ); elseif( isset( $arrValues['disconnect_notices_completed_count'] ) ) $this->setDisconnectNoticesCompletedCount( $arrValues['disconnect_notices_completed_count'] );
		if( isset( $arrValues['disconnect_notices_late_count'] ) && $boolDirectSet ) $this->set( 'm_intDisconnectNoticesLateCount', trim( $arrValues['disconnect_notices_late_count'] ) ); elseif( isset( $arrValues['disconnect_notices_late_count'] ) ) $this->setDisconnectNoticesLateCount( $arrValues['disconnect_notices_late_count'] );
		if( isset( $arrValues['disconnect_notices_average_time'] ) && $boolDirectSet ) $this->set( 'm_strDisconnectNoticesAverageTime', trim( $arrValues['disconnect_notices_average_time'] ) ); elseif( isset( $arrValues['disconnect_notices_average_time'] ) ) $this->setDisconnectNoticesAverageTime( $arrValues['disconnect_notices_average_time'] );
		if( isset( $arrValues['late_fee_utility_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intLateFeeUtilityBillCount', trim( $arrValues['late_fee_utility_bill_count'] ) ); elseif( isset( $arrValues['late_fee_utility_bill_count'] ) ) $this->setLateFeeUtilityBillCount( $arrValues['late_fee_utility_bill_count'] );
		if( isset( $arrValues['implementation_scheduled_property_count'] ) && $boolDirectSet ) $this->set( 'm_intImplementationScheduledPropertyCount', trim( $arrValues['implementation_scheduled_property_count'] ) ); elseif( isset( $arrValues['implementation_scheduled_property_count'] ) ) $this->setImplementationScheduledPropertyCount( $arrValues['implementation_scheduled_property_count'] );
		if( isset( $arrValues['implementation_on_time_property_count'] ) && $boolDirectSet ) $this->set( 'm_intImplementationOnTimePropertyCount', trim( $arrValues['implementation_on_time_property_count'] ) ); elseif( isset( $arrValues['implementation_on_time_property_count'] ) ) $this->setImplementationOnTimePropertyCount( $arrValues['implementation_on_time_property_count'] );
		if( isset( $arrValues['implementation_late_property_count'] ) && $boolDirectSet ) $this->set( 'm_intImplementationLatePropertyCount', trim( $arrValues['implementation_late_property_count'] ) ); elseif( isset( $arrValues['implementation_late_property_count'] ) ) $this->setImplementationLatePropertyCount( $arrValues['implementation_late_property_count'] );
		if( isset( $arrValues['implementation_accounts_created'] ) && $boolDirectSet ) $this->set( 'm_intImplementationAccountsCreated', trim( $arrValues['implementation_accounts_created'] ) ); elseif( isset( $arrValues['implementation_accounts_created'] ) ) $this->setImplementationAccountsCreated( $arrValues['implementation_accounts_created'] );
		if( isset( $arrValues['implementation_accounts_created_post_go_live'] ) && $boolDirectSet ) $this->set( 'm_intImplementationAccountsCreatedPostGoLive', trim( $arrValues['implementation_accounts_created_post_go_live'] ) ); elseif( isset( $arrValues['implementation_accounts_created_post_go_live'] ) ) $this->setImplementationAccountsCreatedPostGoLive( $arrValues['implementation_accounts_created_post_go_live'] );
		if( isset( $arrValues['associated_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intAssociatedBillCount', trim( $arrValues['associated_bill_count'] ) ); elseif( isset( $arrValues['associated_bill_count'] ) ) $this->setAssociatedBillCount( $arrValues['associated_bill_count'] );
		if( isset( $arrValues['associated_bill_average_time'] ) && $boolDirectSet ) $this->set( 'm_strAssociatedBillAverageTime', trim( $arrValues['associated_bill_average_time'] ) ); elseif( isset( $arrValues['associated_bill_average_time'] ) ) $this->setAssociatedBillAverageTime( $arrValues['associated_bill_average_time'] );
		if( isset( $arrValues['associated_bill_on_time_count'] ) && $boolDirectSet ) $this->set( 'm_intAssociatedBillOnTimeCount', trim( $arrValues['associated_bill_on_time_count'] ) ); elseif( isset( $arrValues['associated_bill_on_time_count'] ) ) $this->setAssociatedBillOnTimeCount( $arrValues['associated_bill_on_time_count'] );
		if( isset( $arrValues['associated_bill_ocr_count'] ) && $boolDirectSet ) $this->set( 'm_intAssociatedBillOcrCount', trim( $arrValues['associated_bill_ocr_count'] ) ); elseif( isset( $arrValues['associated_bill_ocr_count'] ) ) $this->setAssociatedBillOcrCount( $arrValues['associated_bill_ocr_count'] );
		if( isset( $arrValues['processed_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intProcessedBillCount', trim( $arrValues['processed_bill_count'] ) ); elseif( isset( $arrValues['processed_bill_count'] ) ) $this->setProcessedBillCount( $arrValues['processed_bill_count'] );
		if( isset( $arrValues['processed_bill_average_time'] ) && $boolDirectSet ) $this->set( 'm_strProcessedBillAverageTime', trim( $arrValues['processed_bill_average_time'] ) ); elseif( isset( $arrValues['processed_bill_average_time'] ) ) $this->setProcessedBillAverageTime( $arrValues['processed_bill_average_time'] );
		if( isset( $arrValues['processed_bill_on_time_count'] ) && $boolDirectSet ) $this->set( 'm_intProcessedBillOnTimeCount', trim( $arrValues['processed_bill_on_time_count'] ) ); elseif( isset( $arrValues['processed_bill_on_time_count'] ) ) $this->setProcessedBillOnTimeCount( $arrValues['processed_bill_on_time_count'] );
		if( isset( $arrValues['processed_bill_error_count'] ) && $boolDirectSet ) $this->set( 'm_intProcessedBillErrorCount', trim( $arrValues['processed_bill_error_count'] ) ); elseif( isset( $arrValues['processed_bill_error_count'] ) ) $this->setProcessedBillErrorCount( $arrValues['processed_bill_error_count'] );
		if( isset( $arrValues['audits_created_count'] ) && $boolDirectSet ) $this->set( 'm_intAuditsCreatedCount', trim( $arrValues['audits_created_count'] ) ); elseif( isset( $arrValues['audits_created_count'] ) ) $this->setAuditsCreatedCount( $arrValues['audits_created_count'] );
		if( isset( $arrValues['audits_completed_count'] ) && $boolDirectSet ) $this->set( 'm_intAuditsCompletedCount', trim( $arrValues['audits_completed_count'] ) ); elseif( isset( $arrValues['audits_completed_count'] ) ) $this->setAuditsCompletedCount( $arrValues['audits_completed_count'] );
		if( isset( $arrValues['audits_average_time'] ) && $boolDirectSet ) $this->set( 'm_strAuditsAverageTime', trim( $arrValues['audits_average_time'] ) ); elseif( isset( $arrValues['audits_average_time'] ) ) $this->setAuditsAverageTime( $arrValues['audits_average_time'] );
		if( isset( $arrValues['audits_completed_on_time_count'] ) && $boolDirectSet ) $this->set( 'm_intAuditsCompletedOnTimeCount', trim( $arrValues['audits_completed_on_time_count'] ) ); elseif( isset( $arrValues['audits_completed_on_time_count'] ) ) $this->setAuditsCompletedOnTimeCount( $arrValues['audits_completed_on_time_count'] );
		if( isset( $arrValues['deleted_bills_count'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBillsCount', trim( $arrValues['deleted_bills_count'] ) ); elseif( isset( $arrValues['deleted_bills_count'] ) ) $this->setDeletedBillsCount( $arrValues['deleted_bills_count'] );
		if( isset( $arrValues['receipt_to_export_timeliness_count'] ) && $boolDirectSet ) $this->set( 'm_intReceiptToExportTimelinessCount', trim( $arrValues['receipt_to_export_timeliness_count'] ) ); elseif( isset( $arrValues['receipt_to_export_timeliness_count'] ) ) $this->setReceiptToExportTimelinessCount( $arrValues['receipt_to_export_timeliness_count'] );
		if( isset( $arrValues['rte_timeliness_twenty_four_hours_count'] ) && $boolDirectSet ) $this->set( 'm_intRteTimelinessTwentyFourHoursCount', trim( $arrValues['rte_timeliness_twenty_four_hours_count'] ) ); elseif( isset( $arrValues['rte_timeliness_twenty_four_hours_count'] ) ) $this->setRteTimelinessTwentyFourHoursCount( $arrValues['rte_timeliness_twenty_four_hours_count'] );
		if( isset( $arrValues['utility_bill_accounts_count'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountsCount', trim( $arrValues['utility_bill_accounts_count'] ) ); elseif( isset( $arrValues['utility_bill_accounts_count'] ) ) $this->setUtilityBillAccountsCount( $arrValues['utility_bill_accounts_count'] );
		if( isset( $arrValues['utility_bill_accounts_coa_confirmed_count'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountsCoaConfirmedCount', trim( $arrValues['utility_bill_accounts_coa_confirmed_count'] ) ); elseif( isset( $arrValues['utility_bill_accounts_coa_confirmed_count'] ) ) $this->setUtilityBillAccountsCoaConfirmedCount( $arrValues['utility_bill_accounts_coa_confirmed_count'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setReportDate( $strReportDate ) {
		$this->set( 'm_strReportDate', CStrings::strTrimDef( $strReportDate, -1, NULL, true ) );
	}

	public function getReportDate() {
		return $this->m_strReportDate;
	}

	public function sqlReportDate() {
		return ( true == isset( $this->m_strReportDate ) ) ? '\'' . $this->m_strReportDate . '\'' : 'NOW()';
	}

	public function setUtilityBillsCreatedCount( $intUtilityBillsCreatedCount ) {
		$this->set( 'm_intUtilityBillsCreatedCount', CStrings::strToIntDef( $intUtilityBillsCreatedCount, NULL, false ) );
	}

	public function getUtilityBillsCreatedCount() {
		return $this->m_intUtilityBillsCreatedCount;
	}

	public function sqlUtilityBillsCreatedCount() {
		return ( true == isset( $this->m_intUtilityBillsCreatedCount ) ) ? ( string ) $this->m_intUtilityBillsCreatedCount : '0';
	}

	public function setUtilityBillsExportedCount( $intUtilityBillsExportedCount ) {
		$this->set( 'm_intUtilityBillsExportedCount', CStrings::strToIntDef( $intUtilityBillsExportedCount, NULL, false ) );
	}

	public function getUtilityBillsExportedCount() {
		return $this->m_intUtilityBillsExportedCount;
	}

	public function sqlUtilityBillsExportedCount() {
		return ( true == isset( $this->m_intUtilityBillsExportedCount ) ) ? ( string ) $this->m_intUtilityBillsExportedCount : '0';
	}

	public function setDueDateExportTenDayCount( $intDueDateExportTenDayCount ) {
		$this->set( 'm_intDueDateExportTenDayCount', CStrings::strToIntDef( $intDueDateExportTenDayCount, NULL, false ) );
	}

	public function getDueDateExportTenDayCount() {
		return $this->m_intDueDateExportTenDayCount;
	}

	public function sqlDueDateExportTenDayCount() {
		return ( true == isset( $this->m_intDueDateExportTenDayCount ) ) ? ( string ) $this->m_intDueDateExportTenDayCount : '0';
	}

	public function setDueDateExportFiveDayCount( $intDueDateExportFiveDayCount ) {
		$this->set( 'm_intDueDateExportFiveDayCount', CStrings::strToIntDef( $intDueDateExportFiveDayCount, NULL, false ) );
	}

	public function getDueDateExportFiveDayCount() {
		return $this->m_intDueDateExportFiveDayCount;
	}

	public function sqlDueDateExportFiveDayCount() {
		return ( true == isset( $this->m_intDueDateExportFiveDayCount ) ) ? ( string ) $this->m_intDueDateExportFiveDayCount : '0';
	}

	public function setDueDateExportAverageDays( $intDueDateExportAverageDays ) {
		$this->set( 'm_intDueDateExportAverageDays', CStrings::strToIntDef( $intDueDateExportAverageDays, NULL, false ) );
	}

	public function getDueDateExportAverageDays() {
		return $this->m_intDueDateExportAverageDays;
	}

	public function sqlDueDateExportAverageDays() {
		return ( true == isset( $this->m_intDueDateExportAverageDays ) ) ? ( string ) $this->m_intDueDateExportAverageDays : 'NULL';
	}

	public function setReceiptToExportAverageTime( $strReceiptToExportAverageTime ) {
		$this->set( 'm_strReceiptToExportAverageTime', CStrings::strTrimDef( $strReceiptToExportAverageTime, NULL, NULL, true ) );
	}

	public function getReceiptToExportAverageTime() {
		return $this->m_strReceiptToExportAverageTime;
	}

	public function sqlReceiptToExportAverageTime() {
		return ( true == isset( $this->m_strReceiptToExportAverageTime ) ) ? '\'' . addslashes( $this->m_strReceiptToExportAverageTime ) . '\'' : 'NULL';
	}

	public function setReceiptBillDateAverageDays( $intReceiptBillDateAverageDays ) {
		$this->set( 'm_intReceiptBillDateAverageDays', CStrings::strToIntDef( $intReceiptBillDateAverageDays, NULL, false ) );
	}

	public function getReceiptBillDateAverageDays() {
		return $this->m_intReceiptBillDateAverageDays;
	}

	public function sqlReceiptBillDateAverageDays() {
		return ( true == isset( $this->m_intReceiptBillDateAverageDays ) ) ? ( string ) $this->m_intReceiptBillDateAverageDays : 'NULL';
	}

	public function setReceiptBillDateFiveDayCount( $intReceiptBillDateFiveDayCount ) {
		$this->set( 'm_intReceiptBillDateFiveDayCount', CStrings::strToIntDef( $intReceiptBillDateFiveDayCount, NULL, false ) );
	}

	public function getReceiptBillDateFiveDayCount() {
		return $this->m_intReceiptBillDateFiveDayCount;
	}

	public function sqlReceiptBillDateFiveDayCount() {
		return ( true == isset( $this->m_intReceiptBillDateFiveDayCount ) ) ? ( string ) $this->m_intReceiptBillDateFiveDayCount : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDueDateExportLateCount( $intDueDateExportLateCount ) {
		$this->set( 'm_intDueDateExportLateCount', CStrings::strToIntDef( $intDueDateExportLateCount, NULL, false ) );
	}

	public function getDueDateExportLateCount() {
		return $this->m_intDueDateExportLateCount;
	}

	public function sqlDueDateExportLateCount() {
		return ( true == isset( $this->m_intDueDateExportLateCount ) ) ? ( string ) $this->m_intDueDateExportLateCount : 'NULL';
	}

	public function setDueDateBillCount( $intDueDateBillCount ) {
		$this->set( 'm_intDueDateBillCount', CStrings::strToIntDef( $intDueDateBillCount, NULL, false ) );
	}

	public function getDueDateBillCount() {
		return $this->m_intDueDateBillCount;
	}

	public function sqlDueDateBillCount() {
		return ( true == isset( $this->m_intDueDateBillCount ) ) ? ( string ) $this->m_intDueDateBillCount : 'NULL';
	}

	public function setBillDateBillCount( $intBillDateBillCount ) {
		$this->set( 'm_intBillDateBillCount', CStrings::strToIntDef( $intBillDateBillCount, NULL, false ) );
	}

	public function getBillDateBillCount() {
		return $this->m_intBillDateBillCount;
	}

	public function sqlBillDateBillCount() {
		return ( true == isset( $this->m_intBillDateBillCount ) ) ? ( string ) $this->m_intBillDateBillCount : 'NULL';
	}

	public function setDisconnectNoticesCreatedCount( $intDisconnectNoticesCreatedCount ) {
		$this->set( 'm_intDisconnectNoticesCreatedCount', CStrings::strToIntDef( $intDisconnectNoticesCreatedCount, NULL, false ) );
	}

	public function getDisconnectNoticesCreatedCount() {
		return $this->m_intDisconnectNoticesCreatedCount;
	}

	public function sqlDisconnectNoticesCreatedCount() {
		return ( true == isset( $this->m_intDisconnectNoticesCreatedCount ) ) ? ( string ) $this->m_intDisconnectNoticesCreatedCount : 'NULL';
	}

	public function setDisconnectNoticesCompletedCount( $intDisconnectNoticesCompletedCount ) {
		$this->set( 'm_intDisconnectNoticesCompletedCount', CStrings::strToIntDef( $intDisconnectNoticesCompletedCount, NULL, false ) );
	}

	public function getDisconnectNoticesCompletedCount() {
		return $this->m_intDisconnectNoticesCompletedCount;
	}

	public function sqlDisconnectNoticesCompletedCount() {
		return ( true == isset( $this->m_intDisconnectNoticesCompletedCount ) ) ? ( string ) $this->m_intDisconnectNoticesCompletedCount : 'NULL';
	}

	public function setDisconnectNoticesLateCount( $intDisconnectNoticesLateCount ) {
		$this->set( 'm_intDisconnectNoticesLateCount', CStrings::strToIntDef( $intDisconnectNoticesLateCount, NULL, false ) );
	}

	public function getDisconnectNoticesLateCount() {
		return $this->m_intDisconnectNoticesLateCount;
	}

	public function sqlDisconnectNoticesLateCount() {
		return ( true == isset( $this->m_intDisconnectNoticesLateCount ) ) ? ( string ) $this->m_intDisconnectNoticesLateCount : 'NULL';
	}

	public function setDisconnectNoticesAverageTime( $strDisconnectNoticesAverageTime ) {
		$this->set( 'm_strDisconnectNoticesAverageTime', CStrings::strTrimDef( $strDisconnectNoticesAverageTime, NULL, NULL, true ) );
	}

	public function getDisconnectNoticesAverageTime() {
		return $this->m_strDisconnectNoticesAverageTime;
	}

	public function sqlDisconnectNoticesAverageTime() {
		return ( true == isset( $this->m_strDisconnectNoticesAverageTime ) ) ? '\'' . addslashes( $this->m_strDisconnectNoticesAverageTime ) . '\'' : 'NULL';
	}

	public function setLateFeeUtilityBillCount( $intLateFeeUtilityBillCount ) {
		$this->set( 'm_intLateFeeUtilityBillCount', CStrings::strToIntDef( $intLateFeeUtilityBillCount, NULL, false ) );
	}

	public function getLateFeeUtilityBillCount() {
		return $this->m_intLateFeeUtilityBillCount;
	}

	public function sqlLateFeeUtilityBillCount() {
		return ( true == isset( $this->m_intLateFeeUtilityBillCount ) ) ? ( string ) $this->m_intLateFeeUtilityBillCount : 'NULL';
	}

	public function setImplementationScheduledPropertyCount( $intImplementationScheduledPropertyCount ) {
		$this->set( 'm_intImplementationScheduledPropertyCount', CStrings::strToIntDef( $intImplementationScheduledPropertyCount, NULL, false ) );
	}

	public function getImplementationScheduledPropertyCount() {
		return $this->m_intImplementationScheduledPropertyCount;
	}

	public function sqlImplementationScheduledPropertyCount() {
		return ( true == isset( $this->m_intImplementationScheduledPropertyCount ) ) ? ( string ) $this->m_intImplementationScheduledPropertyCount : 'NULL';
	}

	public function setImplementationOnTimePropertyCount( $intImplementationOnTimePropertyCount ) {
		$this->set( 'm_intImplementationOnTimePropertyCount', CStrings::strToIntDef( $intImplementationOnTimePropertyCount, NULL, false ) );
	}

	public function getImplementationOnTimePropertyCount() {
		return $this->m_intImplementationOnTimePropertyCount;
	}

	public function sqlImplementationOnTimePropertyCount() {
		return ( true == isset( $this->m_intImplementationOnTimePropertyCount ) ) ? ( string ) $this->m_intImplementationOnTimePropertyCount : 'NULL';
	}

	public function setImplementationLatePropertyCount( $intImplementationLatePropertyCount ) {
		$this->set( 'm_intImplementationLatePropertyCount', CStrings::strToIntDef( $intImplementationLatePropertyCount, NULL, false ) );
	}

	public function getImplementationLatePropertyCount() {
		return $this->m_intImplementationLatePropertyCount;
	}

	public function sqlImplementationLatePropertyCount() {
		return ( true == isset( $this->m_intImplementationLatePropertyCount ) ) ? ( string ) $this->m_intImplementationLatePropertyCount : 'NULL';
	}

	public function setImplementationAccountsCreated( $intImplementationAccountsCreated ) {
		$this->set( 'm_intImplementationAccountsCreated', CStrings::strToIntDef( $intImplementationAccountsCreated, NULL, false ) );
	}

	public function getImplementationAccountsCreated() {
		return $this->m_intImplementationAccountsCreated;
	}

	public function sqlImplementationAccountsCreated() {
		return ( true == isset( $this->m_intImplementationAccountsCreated ) ) ? ( string ) $this->m_intImplementationAccountsCreated : 'NULL';
	}

	public function setImplementationAccountsCreatedPostGoLive( $intImplementationAccountsCreatedPostGoLive ) {
		$this->set( 'm_intImplementationAccountsCreatedPostGoLive', CStrings::strToIntDef( $intImplementationAccountsCreatedPostGoLive, NULL, false ) );
	}

	public function getImplementationAccountsCreatedPostGoLive() {
		return $this->m_intImplementationAccountsCreatedPostGoLive;
	}

	public function sqlImplementationAccountsCreatedPostGoLive() {
		return ( true == isset( $this->m_intImplementationAccountsCreatedPostGoLive ) ) ? ( string ) $this->m_intImplementationAccountsCreatedPostGoLive : 'NULL';
	}

	public function setAssociatedBillCount( $intAssociatedBillCount ) {
		$this->set( 'm_intAssociatedBillCount', CStrings::strToIntDef( $intAssociatedBillCount, NULL, false ) );
	}

	public function getAssociatedBillCount() {
		return $this->m_intAssociatedBillCount;
	}

	public function sqlAssociatedBillCount() {
		return ( true == isset( $this->m_intAssociatedBillCount ) ) ? ( string ) $this->m_intAssociatedBillCount : 'NULL';
	}

	public function setAssociatedBillAverageTime( $strAssociatedBillAverageTime ) {
		$this->set( 'm_strAssociatedBillAverageTime', CStrings::strTrimDef( $strAssociatedBillAverageTime, NULL, NULL, true ) );
	}

	public function getAssociatedBillAverageTime() {
		return $this->m_strAssociatedBillAverageTime;
	}

	public function sqlAssociatedBillAverageTime() {
		return ( true == isset( $this->m_strAssociatedBillAverageTime ) ) ? '\'' . addslashes( $this->m_strAssociatedBillAverageTime ) . '\'' : 'NULL';
	}

	public function setAssociatedBillOnTimeCount( $intAssociatedBillOnTimeCount ) {
		$this->set( 'm_intAssociatedBillOnTimeCount', CStrings::strToIntDef( $intAssociatedBillOnTimeCount, NULL, false ) );
	}

	public function getAssociatedBillOnTimeCount() {
		return $this->m_intAssociatedBillOnTimeCount;
	}

	public function sqlAssociatedBillOnTimeCount() {
		return ( true == isset( $this->m_intAssociatedBillOnTimeCount ) ) ? ( string ) $this->m_intAssociatedBillOnTimeCount : 'NULL';
	}

	public function setAssociatedBillOcrCount( $intAssociatedBillOcrCount ) {
		$this->set( 'm_intAssociatedBillOcrCount', CStrings::strToIntDef( $intAssociatedBillOcrCount, NULL, false ) );
	}

	public function getAssociatedBillOcrCount() {
		return $this->m_intAssociatedBillOcrCount;
	}

	public function sqlAssociatedBillOcrCount() {
		return ( true == isset( $this->m_intAssociatedBillOcrCount ) ) ? ( string ) $this->m_intAssociatedBillOcrCount : 'NULL';
	}

	public function setProcessedBillCount( $intProcessedBillCount ) {
		$this->set( 'm_intProcessedBillCount', CStrings::strToIntDef( $intProcessedBillCount, NULL, false ) );
	}

	public function getProcessedBillCount() {
		return $this->m_intProcessedBillCount;
	}

	public function sqlProcessedBillCount() {
		return ( true == isset( $this->m_intProcessedBillCount ) ) ? ( string ) $this->m_intProcessedBillCount : 'NULL';
	}

	public function setProcessedBillAverageTime( $strProcessedBillAverageTime ) {
		$this->set( 'm_strProcessedBillAverageTime', CStrings::strTrimDef( $strProcessedBillAverageTime, NULL, NULL, true ) );
	}

	public function getProcessedBillAverageTime() {
		return $this->m_strProcessedBillAverageTime;
	}

	public function sqlProcessedBillAverageTime() {
		return ( true == isset( $this->m_strProcessedBillAverageTime ) ) ? '\'' . addslashes( $this->m_strProcessedBillAverageTime ) . '\'' : 'NULL';
	}

	public function setProcessedBillOnTimeCount( $intProcessedBillOnTimeCount ) {
		$this->set( 'm_intProcessedBillOnTimeCount', CStrings::strToIntDef( $intProcessedBillOnTimeCount, NULL, false ) );
	}

	public function getProcessedBillOnTimeCount() {
		return $this->m_intProcessedBillOnTimeCount;
	}

	public function sqlProcessedBillOnTimeCount() {
		return ( true == isset( $this->m_intProcessedBillOnTimeCount ) ) ? ( string ) $this->m_intProcessedBillOnTimeCount : 'NULL';
	}

	public function setProcessedBillErrorCount( $intProcessedBillErrorCount ) {
		$this->set( 'm_intProcessedBillErrorCount', CStrings::strToIntDef( $intProcessedBillErrorCount, NULL, false ) );
	}

	public function getProcessedBillErrorCount() {
		return $this->m_intProcessedBillErrorCount;
	}

	public function sqlProcessedBillErrorCount() {
		return ( true == isset( $this->m_intProcessedBillErrorCount ) ) ? ( string ) $this->m_intProcessedBillErrorCount : 'NULL';
	}

	public function setAuditsCreatedCount( $intAuditsCreatedCount ) {
		$this->set( 'm_intAuditsCreatedCount', CStrings::strToIntDef( $intAuditsCreatedCount, NULL, false ) );
	}

	public function getAuditsCreatedCount() {
		return $this->m_intAuditsCreatedCount;
	}

	public function sqlAuditsCreatedCount() {
		return ( true == isset( $this->m_intAuditsCreatedCount ) ) ? ( string ) $this->m_intAuditsCreatedCount : 'NULL';
	}

	public function setAuditsCompletedCount( $intAuditsCompletedCount ) {
		$this->set( 'm_intAuditsCompletedCount', CStrings::strToIntDef( $intAuditsCompletedCount, NULL, false ) );
	}

	public function getAuditsCompletedCount() {
		return $this->m_intAuditsCompletedCount;
	}

	public function sqlAuditsCompletedCount() {
		return ( true == isset( $this->m_intAuditsCompletedCount ) ) ? ( string ) $this->m_intAuditsCompletedCount : 'NULL';
	}

	public function setAuditsAverageTime( $strAuditsAverageTime ) {
		$this->set( 'm_strAuditsAverageTime', CStrings::strTrimDef( $strAuditsAverageTime, NULL, NULL, true ) );
	}

	public function getAuditsAverageTime() {
		return $this->m_strAuditsAverageTime;
	}

	public function sqlAuditsAverageTime() {
		return ( true == isset( $this->m_strAuditsAverageTime ) ) ? '\'' . addslashes( $this->m_strAuditsAverageTime ) . '\'' : 'NULL';
	}

	public function setAuditsCompletedOnTimeCount( $intAuditsCompletedOnTimeCount ) {
		$this->set( 'm_intAuditsCompletedOnTimeCount', CStrings::strToIntDef( $intAuditsCompletedOnTimeCount, NULL, false ) );
	}

	public function getAuditsCompletedOnTimeCount() {
		return $this->m_intAuditsCompletedOnTimeCount;
	}

	public function sqlAuditsCompletedOnTimeCount() {
		return ( true == isset( $this->m_intAuditsCompletedOnTimeCount ) ) ? ( string ) $this->m_intAuditsCompletedOnTimeCount : 'NULL';
	}

	public function setDeletedBillsCount( $intDeletedBillsCount ) {
		$this->set( 'm_intDeletedBillsCount', CStrings::strToIntDef( $intDeletedBillsCount, NULL, false ) );
	}

	public function getDeletedBillsCount() {
		return $this->m_intDeletedBillsCount;
	}

	public function sqlDeletedBillsCount() {
		return ( true == isset( $this->m_intDeletedBillsCount ) ) ? ( string ) $this->m_intDeletedBillsCount : 'NULL';
	}

	public function setReceiptToExportTimelinessCount( $intReceiptToExportTimelinessCount ) {
		$this->set( 'm_intReceiptToExportTimelinessCount', CStrings::strToIntDef( $intReceiptToExportTimelinessCount, NULL, false ) );
	}

	public function getReceiptToExportTimelinessCount() {
		return $this->m_intReceiptToExportTimelinessCount;
	}

	public function sqlReceiptToExportTimelinessCount() {
		return ( true == isset( $this->m_intReceiptToExportTimelinessCount ) ) ? ( string ) $this->m_intReceiptToExportTimelinessCount : 'NULL';
	}

	public function setRteTimelinessTwentyFourHoursCount( $intRteTimelinessTwentyFourHoursCount ) {
		$this->set( 'm_intRteTimelinessTwentyFourHoursCount', CStrings::strToIntDef( $intRteTimelinessTwentyFourHoursCount, NULL, false ) );
	}

	public function getRteTimelinessTwentyFourHoursCount() {
		return $this->m_intRteTimelinessTwentyFourHoursCount;
	}

	public function sqlRteTimelinessTwentyFourHoursCount() {
		return ( true == isset( $this->m_intRteTimelinessTwentyFourHoursCount ) ) ? ( string ) $this->m_intRteTimelinessTwentyFourHoursCount : 'NULL';
	}

	public function setUtilityBillAccountsCount( $intUtilityBillAccountsCount ) {
		$this->set( 'm_intUtilityBillAccountsCount', CStrings::strToIntDef( $intUtilityBillAccountsCount, NULL, false ) );
	}

	public function getUtilityBillAccountsCount() {
		return $this->m_intUtilityBillAccountsCount;
	}

	public function sqlUtilityBillAccountsCount() {
		return ( true == isset( $this->m_intUtilityBillAccountsCount ) ) ? ( string ) $this->m_intUtilityBillAccountsCount : 'NULL';
	}

	public function setUtilityBillAccountsCoaConfirmedCount( $intUtilityBillAccountsCoaConfirmedCount ) {
		$this->set( 'm_intUtilityBillAccountsCoaConfirmedCount', CStrings::strToIntDef( $intUtilityBillAccountsCoaConfirmedCount, NULL, false ) );
	}

	public function getUtilityBillAccountsCoaConfirmedCount() {
		return $this->m_intUtilityBillAccountsCoaConfirmedCount;
	}

	public function sqlUtilityBillAccountsCoaConfirmedCount() {
		return ( true == isset( $this->m_intUtilityBillAccountsCoaConfirmedCount ) ) ? ( string ) $this->m_intUtilityBillAccountsCoaConfirmedCount : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, report_date, utility_bills_created_count, utility_bills_exported_count, due_date_export_ten_day_count, due_date_export_five_day_count, due_date_export_average_days, receipt_to_export_average_time, receipt_bill_date_average_days, receipt_bill_date_five_day_count, created_by, created_on, due_date_export_late_count, due_date_bill_count, bill_date_bill_count, disconnect_notices_created_count, disconnect_notices_completed_count, disconnect_notices_late_count, disconnect_notices_average_time, late_fee_utility_bill_count, implementation_scheduled_property_count, implementation_on_time_property_count, implementation_late_property_count, implementation_accounts_created, implementation_accounts_created_post_go_live, associated_bill_count, associated_bill_average_time, associated_bill_on_time_count, associated_bill_ocr_count, processed_bill_count, processed_bill_average_time, processed_bill_on_time_count, processed_bill_error_count, audits_created_count, audits_completed_count, audits_average_time, audits_completed_on_time_count, deleted_bills_count, receipt_to_export_timeliness_count, rte_timeliness_twenty_four_hours_count, utility_bill_accounts_count, utility_bill_accounts_coa_confirmed_count )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlReportDate() . ', ' .
						$this->sqlUtilityBillsCreatedCount() . ', ' .
						$this->sqlUtilityBillsExportedCount() . ', ' .
						$this->sqlDueDateExportTenDayCount() . ', ' .
						$this->sqlDueDateExportFiveDayCount() . ', ' .
						$this->sqlDueDateExportAverageDays() . ', ' .
						$this->sqlReceiptToExportAverageTime() . ', ' .
						$this->sqlReceiptBillDateAverageDays() . ', ' .
						$this->sqlReceiptBillDateFiveDayCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDueDateExportLateCount() . ', ' .
						$this->sqlDueDateBillCount() . ', ' .
						$this->sqlBillDateBillCount() . ', ' .
						$this->sqlDisconnectNoticesCreatedCount() . ', ' .
						$this->sqlDisconnectNoticesCompletedCount() . ', ' .
						$this->sqlDisconnectNoticesLateCount() . ', ' .
						$this->sqlDisconnectNoticesAverageTime() . ', ' .
						$this->sqlLateFeeUtilityBillCount() . ', ' .
						$this->sqlImplementationScheduledPropertyCount() . ', ' .
						$this->sqlImplementationOnTimePropertyCount() . ', ' .
						$this->sqlImplementationLatePropertyCount() . ', ' .
						$this->sqlImplementationAccountsCreated() . ', ' .
						$this->sqlImplementationAccountsCreatedPostGoLive() . ', ' .
						$this->sqlAssociatedBillCount() . ', ' .
						$this->sqlAssociatedBillAverageTime() . ', ' .
						$this->sqlAssociatedBillOnTimeCount() . ', ' .
						$this->sqlAssociatedBillOcrCount() . ', ' .
						$this->sqlProcessedBillCount() . ', ' .
						$this->sqlProcessedBillAverageTime() . ', ' .
						$this->sqlProcessedBillOnTimeCount() . ', ' .
						$this->sqlProcessedBillErrorCount() . ', ' .
						$this->sqlAuditsCreatedCount() . ', ' .
						$this->sqlAuditsCompletedCount() . ', ' .
						$this->sqlAuditsAverageTime() . ', ' .
						$this->sqlAuditsCompletedOnTimeCount() . ', ' .
						$this->sqlDeletedBillsCount() . ', ' .
						$this->sqlReceiptToExportTimelinessCount() . ', ' .
						$this->sqlRteTimelinessTwentyFourHoursCount() . ', ' .
						$this->sqlUtilityBillAccountsCount() . ', ' .
						$this->sqlUtilityBillAccountsCoaConfirmedCount() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_date = ' . $this->sqlReportDate(). ',' ; } elseif( true == array_key_exists( 'ReportDate', $this->getChangedColumns() ) ) { $strSql .= ' report_date = ' . $this->sqlReportDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bills_created_count = ' . $this->sqlUtilityBillsCreatedCount(). ',' ; } elseif( true == array_key_exists( 'UtilityBillsCreatedCount', $this->getChangedColumns() ) ) { $strSql .= ' utility_bills_created_count = ' . $this->sqlUtilityBillsCreatedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bills_exported_count = ' . $this->sqlUtilityBillsExportedCount(). ',' ; } elseif( true == array_key_exists( 'UtilityBillsExportedCount', $this->getChangedColumns() ) ) { $strSql .= ' utility_bills_exported_count = ' . $this->sqlUtilityBillsExportedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date_export_ten_day_count = ' . $this->sqlDueDateExportTenDayCount(). ',' ; } elseif( true == array_key_exists( 'DueDateExportTenDayCount', $this->getChangedColumns() ) ) { $strSql .= ' due_date_export_ten_day_count = ' . $this->sqlDueDateExportTenDayCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date_export_five_day_count = ' . $this->sqlDueDateExportFiveDayCount(). ',' ; } elseif( true == array_key_exists( 'DueDateExportFiveDayCount', $this->getChangedColumns() ) ) { $strSql .= ' due_date_export_five_day_count = ' . $this->sqlDueDateExportFiveDayCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date_export_average_days = ' . $this->sqlDueDateExportAverageDays(). ',' ; } elseif( true == array_key_exists( 'DueDateExportAverageDays', $this->getChangedColumns() ) ) { $strSql .= ' due_date_export_average_days = ' . $this->sqlDueDateExportAverageDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receipt_to_export_average_time = ' . $this->sqlReceiptToExportAverageTime(). ',' ; } elseif( true == array_key_exists( 'ReceiptToExportAverageTime', $this->getChangedColumns() ) ) { $strSql .= ' receipt_to_export_average_time = ' . $this->sqlReceiptToExportAverageTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receipt_bill_date_average_days = ' . $this->sqlReceiptBillDateAverageDays(). ',' ; } elseif( true == array_key_exists( 'ReceiptBillDateAverageDays', $this->getChangedColumns() ) ) { $strSql .= ' receipt_bill_date_average_days = ' . $this->sqlReceiptBillDateAverageDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receipt_bill_date_five_day_count = ' . $this->sqlReceiptBillDateFiveDayCount(). ',' ; } elseif( true == array_key_exists( 'ReceiptBillDateFiveDayCount', $this->getChangedColumns() ) ) { $strSql .= ' receipt_bill_date_five_day_count = ' . $this->sqlReceiptBillDateFiveDayCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date_export_late_count = ' . $this->sqlDueDateExportLateCount(). ',' ; } elseif( true == array_key_exists( 'DueDateExportLateCount', $this->getChangedColumns() ) ) { $strSql .= ' due_date_export_late_count = ' . $this->sqlDueDateExportLateCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date_bill_count = ' . $this->sqlDueDateBillCount(). ',' ; } elseif( true == array_key_exists( 'DueDateBillCount', $this->getChangedColumns() ) ) { $strSql .= ' due_date_bill_count = ' . $this->sqlDueDateBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_date_bill_count = ' . $this->sqlBillDateBillCount(). ',' ; } elseif( true == array_key_exists( 'BillDateBillCount', $this->getChangedColumns() ) ) { $strSql .= ' bill_date_bill_count = ' . $this->sqlBillDateBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disconnect_notices_created_count = ' . $this->sqlDisconnectNoticesCreatedCount(). ',' ; } elseif( true == array_key_exists( 'DisconnectNoticesCreatedCount', $this->getChangedColumns() ) ) { $strSql .= ' disconnect_notices_created_count = ' . $this->sqlDisconnectNoticesCreatedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disconnect_notices_completed_count = ' . $this->sqlDisconnectNoticesCompletedCount(). ',' ; } elseif( true == array_key_exists( 'DisconnectNoticesCompletedCount', $this->getChangedColumns() ) ) { $strSql .= ' disconnect_notices_completed_count = ' . $this->sqlDisconnectNoticesCompletedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disconnect_notices_late_count = ' . $this->sqlDisconnectNoticesLateCount(). ',' ; } elseif( true == array_key_exists( 'DisconnectNoticesLateCount', $this->getChangedColumns() ) ) { $strSql .= ' disconnect_notices_late_count = ' . $this->sqlDisconnectNoticesLateCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disconnect_notices_average_time = ' . $this->sqlDisconnectNoticesAverageTime(). ',' ; } elseif( true == array_key_exists( 'DisconnectNoticesAverageTime', $this->getChangedColumns() ) ) { $strSql .= ' disconnect_notices_average_time = ' . $this->sqlDisconnectNoticesAverageTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_fee_utility_bill_count = ' . $this->sqlLateFeeUtilityBillCount(). ',' ; } elseif( true == array_key_exists( 'LateFeeUtilityBillCount', $this->getChangedColumns() ) ) { $strSql .= ' late_fee_utility_bill_count = ' . $this->sqlLateFeeUtilityBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_scheduled_property_count = ' . $this->sqlImplementationScheduledPropertyCount(). ',' ; } elseif( true == array_key_exists( 'ImplementationScheduledPropertyCount', $this->getChangedColumns() ) ) { $strSql .= ' implementation_scheduled_property_count = ' . $this->sqlImplementationScheduledPropertyCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_on_time_property_count = ' . $this->sqlImplementationOnTimePropertyCount(). ',' ; } elseif( true == array_key_exists( 'ImplementationOnTimePropertyCount', $this->getChangedColumns() ) ) { $strSql .= ' implementation_on_time_property_count = ' . $this->sqlImplementationOnTimePropertyCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_late_property_count = ' . $this->sqlImplementationLatePropertyCount(). ',' ; } elseif( true == array_key_exists( 'ImplementationLatePropertyCount', $this->getChangedColumns() ) ) { $strSql .= ' implementation_late_property_count = ' . $this->sqlImplementationLatePropertyCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_accounts_created = ' . $this->sqlImplementationAccountsCreated(). ',' ; } elseif( true == array_key_exists( 'ImplementationAccountsCreated', $this->getChangedColumns() ) ) { $strSql .= ' implementation_accounts_created = ' . $this->sqlImplementationAccountsCreated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_accounts_created_post_go_live = ' . $this->sqlImplementationAccountsCreatedPostGoLive(). ',' ; } elseif( true == array_key_exists( 'ImplementationAccountsCreatedPostGoLive', $this->getChangedColumns() ) ) { $strSql .= ' implementation_accounts_created_post_go_live = ' . $this->sqlImplementationAccountsCreatedPostGoLive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_bill_count = ' . $this->sqlAssociatedBillCount(). ',' ; } elseif( true == array_key_exists( 'AssociatedBillCount', $this->getChangedColumns() ) ) { $strSql .= ' associated_bill_count = ' . $this->sqlAssociatedBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_bill_average_time = ' . $this->sqlAssociatedBillAverageTime(). ',' ; } elseif( true == array_key_exists( 'AssociatedBillAverageTime', $this->getChangedColumns() ) ) { $strSql .= ' associated_bill_average_time = ' . $this->sqlAssociatedBillAverageTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_bill_on_time_count = ' . $this->sqlAssociatedBillOnTimeCount(). ',' ; } elseif( true == array_key_exists( 'AssociatedBillOnTimeCount', $this->getChangedColumns() ) ) { $strSql .= ' associated_bill_on_time_count = ' . $this->sqlAssociatedBillOnTimeCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_bill_ocr_count = ' . $this->sqlAssociatedBillOcrCount(). ',' ; } elseif( true == array_key_exists( 'AssociatedBillOcrCount', $this->getChangedColumns() ) ) { $strSql .= ' associated_bill_ocr_count = ' . $this->sqlAssociatedBillOcrCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_bill_count = ' . $this->sqlProcessedBillCount(). ',' ; } elseif( true == array_key_exists( 'ProcessedBillCount', $this->getChangedColumns() ) ) { $strSql .= ' processed_bill_count = ' . $this->sqlProcessedBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_bill_average_time = ' . $this->sqlProcessedBillAverageTime(). ',' ; } elseif( true == array_key_exists( 'ProcessedBillAverageTime', $this->getChangedColumns() ) ) { $strSql .= ' processed_bill_average_time = ' . $this->sqlProcessedBillAverageTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_bill_on_time_count = ' . $this->sqlProcessedBillOnTimeCount(). ',' ; } elseif( true == array_key_exists( 'ProcessedBillOnTimeCount', $this->getChangedColumns() ) ) { $strSql .= ' processed_bill_on_time_count = ' . $this->sqlProcessedBillOnTimeCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_bill_error_count = ' . $this->sqlProcessedBillErrorCount(). ',' ; } elseif( true == array_key_exists( 'ProcessedBillErrorCount', $this->getChangedColumns() ) ) { $strSql .= ' processed_bill_error_count = ' . $this->sqlProcessedBillErrorCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audits_created_count = ' . $this->sqlAuditsCreatedCount(). ',' ; } elseif( true == array_key_exists( 'AuditsCreatedCount', $this->getChangedColumns() ) ) { $strSql .= ' audits_created_count = ' . $this->sqlAuditsCreatedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audits_completed_count = ' . $this->sqlAuditsCompletedCount(). ',' ; } elseif( true == array_key_exists( 'AuditsCompletedCount', $this->getChangedColumns() ) ) { $strSql .= ' audits_completed_count = ' . $this->sqlAuditsCompletedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audits_average_time = ' . $this->sqlAuditsAverageTime(). ',' ; } elseif( true == array_key_exists( 'AuditsAverageTime', $this->getChangedColumns() ) ) { $strSql .= ' audits_average_time = ' . $this->sqlAuditsAverageTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audits_completed_on_time_count = ' . $this->sqlAuditsCompletedOnTimeCount(). ',' ; } elseif( true == array_key_exists( 'AuditsCompletedOnTimeCount', $this->getChangedColumns() ) ) { $strSql .= ' audits_completed_on_time_count = ' . $this->sqlAuditsCompletedOnTimeCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_bills_count = ' . $this->sqlDeletedBillsCount(). ',' ; } elseif( true == array_key_exists( 'DeletedBillsCount', $this->getChangedColumns() ) ) { $strSql .= ' deleted_bills_count = ' . $this->sqlDeletedBillsCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receipt_to_export_timeliness_count = ' . $this->sqlReceiptToExportTimelinessCount(). ',' ; } elseif( true == array_key_exists( 'ReceiptToExportTimelinessCount', $this->getChangedColumns() ) ) { $strSql .= ' receipt_to_export_timeliness_count = ' . $this->sqlReceiptToExportTimelinessCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rte_timeliness_twenty_four_hours_count = ' . $this->sqlRteTimelinessTwentyFourHoursCount(). ',' ; } elseif( true == array_key_exists( 'RteTimelinessTwentyFourHoursCount', $this->getChangedColumns() ) ) { $strSql .= ' rte_timeliness_twenty_four_hours_count = ' . $this->sqlRteTimelinessTwentyFourHoursCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_accounts_count = ' . $this->sqlUtilityBillAccountsCount(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountsCount', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_accounts_count = ' . $this->sqlUtilityBillAccountsCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_accounts_coa_confirmed_count = ' . $this->sqlUtilityBillAccountsCoaConfirmedCount() ; } elseif( true == array_key_exists( 'UtilityBillAccountsCoaConfirmedCount', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_accounts_coa_confirmed_count = ' . $this->sqlUtilityBillAccountsCoaConfirmedCount() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'report_date' => $this->getReportDate(),
			'utility_bills_created_count' => $this->getUtilityBillsCreatedCount(),
			'utility_bills_exported_count' => $this->getUtilityBillsExportedCount(),
			'due_date_export_ten_day_count' => $this->getDueDateExportTenDayCount(),
			'due_date_export_five_day_count' => $this->getDueDateExportFiveDayCount(),
			'due_date_export_average_days' => $this->getDueDateExportAverageDays(),
			'receipt_to_export_average_time' => $this->getReceiptToExportAverageTime(),
			'receipt_bill_date_average_days' => $this->getReceiptBillDateAverageDays(),
			'receipt_bill_date_five_day_count' => $this->getReceiptBillDateFiveDayCount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'due_date_export_late_count' => $this->getDueDateExportLateCount(),
			'due_date_bill_count' => $this->getDueDateBillCount(),
			'bill_date_bill_count' => $this->getBillDateBillCount(),
			'disconnect_notices_created_count' => $this->getDisconnectNoticesCreatedCount(),
			'disconnect_notices_completed_count' => $this->getDisconnectNoticesCompletedCount(),
			'disconnect_notices_late_count' => $this->getDisconnectNoticesLateCount(),
			'disconnect_notices_average_time' => $this->getDisconnectNoticesAverageTime(),
			'late_fee_utility_bill_count' => $this->getLateFeeUtilityBillCount(),
			'implementation_scheduled_property_count' => $this->getImplementationScheduledPropertyCount(),
			'implementation_on_time_property_count' => $this->getImplementationOnTimePropertyCount(),
			'implementation_late_property_count' => $this->getImplementationLatePropertyCount(),
			'implementation_accounts_created' => $this->getImplementationAccountsCreated(),
			'implementation_accounts_created_post_go_live' => $this->getImplementationAccountsCreatedPostGoLive(),
			'associated_bill_count' => $this->getAssociatedBillCount(),
			'associated_bill_average_time' => $this->getAssociatedBillAverageTime(),
			'associated_bill_on_time_count' => $this->getAssociatedBillOnTimeCount(),
			'associated_bill_ocr_count' => $this->getAssociatedBillOcrCount(),
			'processed_bill_count' => $this->getProcessedBillCount(),
			'processed_bill_average_time' => $this->getProcessedBillAverageTime(),
			'processed_bill_on_time_count' => $this->getProcessedBillOnTimeCount(),
			'processed_bill_error_count' => $this->getProcessedBillErrorCount(),
			'audits_created_count' => $this->getAuditsCreatedCount(),
			'audits_completed_count' => $this->getAuditsCompletedCount(),
			'audits_average_time' => $this->getAuditsAverageTime(),
			'audits_completed_on_time_count' => $this->getAuditsCompletedOnTimeCount(),
			'deleted_bills_count' => $this->getDeletedBillsCount(),
			'receipt_to_export_timeliness_count' => $this->getReceiptToExportTimelinessCount(),
			'rte_timeliness_twenty_four_hours_count' => $this->getRteTimelinessTwentyFourHoursCount(),
			'utility_bill_accounts_count' => $this->getUtilityBillAccountsCount(),
			'utility_bill_accounts_coa_confirmed_count' => $this->getUtilityBillAccountsCoaConfirmedCount()
		);
	}

}
?>