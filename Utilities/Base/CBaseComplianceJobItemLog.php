<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseComplianceJobItemLog extends CEosSingularBase {

	const TABLE_NAME = 'public.compliance_job_item_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intComplianceJobItemId;
	protected $m_intVendorInvitationId;
	protected $m_intComplianceJobId;
	protected $m_intComplianceRuleId;
	protected $m_intComplianceStatusId;
	protected $m_intComplianceLevelReferenceId;
	protected $m_intVendorId;
	protected $m_strLogDatetime;
	protected $m_strFailureDescription;
	protected $m_boolIsActive;
	protected $m_strLastRequestedOn;
	protected $m_strLastApprovedOn;
	protected $m_intFailedBy;
	protected $m_strFailedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['compliance_job_item_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceJobItemId', trim( $arrValues['compliance_job_item_id'] ) ); elseif( isset( $arrValues['compliance_job_item_id'] ) ) $this->setComplianceJobItemId( $arrValues['compliance_job_item_id'] );
		if( isset( $arrValues['vendor_invitation_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorInvitationId', trim( $arrValues['vendor_invitation_id'] ) ); elseif( isset( $arrValues['vendor_invitation_id'] ) ) $this->setVendorInvitationId( $arrValues['vendor_invitation_id'] );
		if( isset( $arrValues['compliance_job_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceJobId', trim( $arrValues['compliance_job_id'] ) ); elseif( isset( $arrValues['compliance_job_id'] ) ) $this->setComplianceJobId( $arrValues['compliance_job_id'] );
		if( isset( $arrValues['compliance_rule_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceRuleId', trim( $arrValues['compliance_rule_id'] ) ); elseif( isset( $arrValues['compliance_rule_id'] ) ) $this->setComplianceRuleId( $arrValues['compliance_rule_id'] );
		if( isset( $arrValues['compliance_status_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceStatusId', trim( $arrValues['compliance_status_id'] ) ); elseif( isset( $arrValues['compliance_status_id'] ) ) $this->setComplianceStatusId( $arrValues['compliance_status_id'] );
		if( isset( $arrValues['compliance_level_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceLevelReferenceId', trim( $arrValues['compliance_level_reference_id'] ) ); elseif( isset( $arrValues['compliance_level_reference_id'] ) ) $this->setComplianceLevelReferenceId( $arrValues['compliance_level_reference_id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['failure_description'] ) && $boolDirectSet ) $this->set( 'm_strFailureDescription', trim( stripcslashes( $arrValues['failure_description'] ) ) ); elseif( isset( $arrValues['failure_description'] ) ) $this->setFailureDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['failure_description'] ) : $arrValues['failure_description'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['last_requested_on'] ) && $boolDirectSet ) $this->set( 'm_strLastRequestedOn', trim( $arrValues['last_requested_on'] ) ); elseif( isset( $arrValues['last_requested_on'] ) ) $this->setLastRequestedOn( $arrValues['last_requested_on'] );
		if( isset( $arrValues['last_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strLastApprovedOn', trim( $arrValues['last_approved_on'] ) ); elseif( isset( $arrValues['last_approved_on'] ) ) $this->setLastApprovedOn( $arrValues['last_approved_on'] );
		if( isset( $arrValues['failed_by'] ) && $boolDirectSet ) $this->set( 'm_intFailedBy', trim( $arrValues['failed_by'] ) ); elseif( isset( $arrValues['failed_by'] ) ) $this->setFailedBy( $arrValues['failed_by'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setComplianceJobItemId( $intComplianceJobItemId ) {
		$this->set( 'm_intComplianceJobItemId', CStrings::strToIntDef( $intComplianceJobItemId, NULL, false ) );
	}

	public function getComplianceJobItemId() {
		return $this->m_intComplianceJobItemId;
	}

	public function sqlComplianceJobItemId() {
		return ( true == isset( $this->m_intComplianceJobItemId ) ) ? ( string ) $this->m_intComplianceJobItemId : 'NULL';
	}

	public function setVendorInvitationId( $intVendorInvitationId ) {
		$this->set( 'm_intVendorInvitationId', CStrings::strToIntDef( $intVendorInvitationId, NULL, false ) );
	}

	public function getVendorInvitationId() {
		return $this->m_intVendorInvitationId;
	}

	public function sqlVendorInvitationId() {
		return ( true == isset( $this->m_intVendorInvitationId ) ) ? ( string ) $this->m_intVendorInvitationId : 'NULL';
	}

	public function setComplianceJobId( $intComplianceJobId ) {
		$this->set( 'm_intComplianceJobId', CStrings::strToIntDef( $intComplianceJobId, NULL, false ) );
	}

	public function getComplianceJobId() {
		return $this->m_intComplianceJobId;
	}

	public function sqlComplianceJobId() {
		return ( true == isset( $this->m_intComplianceJobId ) ) ? ( string ) $this->m_intComplianceJobId : 'NULL';
	}

	public function setComplianceRuleId( $intComplianceRuleId ) {
		$this->set( 'm_intComplianceRuleId', CStrings::strToIntDef( $intComplianceRuleId, NULL, false ) );
	}

	public function getComplianceRuleId() {
		return $this->m_intComplianceRuleId;
	}

	public function sqlComplianceRuleId() {
		return ( true == isset( $this->m_intComplianceRuleId ) ) ? ( string ) $this->m_intComplianceRuleId : 'NULL';
	}

	public function setComplianceStatusId( $intComplianceStatusId ) {
		$this->set( 'm_intComplianceStatusId', CStrings::strToIntDef( $intComplianceStatusId, NULL, false ) );
	}

	public function getComplianceStatusId() {
		return $this->m_intComplianceStatusId;
	}

	public function sqlComplianceStatusId() {
		return ( true == isset( $this->m_intComplianceStatusId ) ) ? ( string ) $this->m_intComplianceStatusId : 'NULL';
	}

	public function setComplianceLevelReferenceId( $intComplianceLevelReferenceId ) {
		$this->set( 'm_intComplianceLevelReferenceId', CStrings::strToIntDef( $intComplianceLevelReferenceId, NULL, false ) );
	}

	public function getComplianceLevelReferenceId() {
		return $this->m_intComplianceLevelReferenceId;
	}

	public function sqlComplianceLevelReferenceId() {
		return ( true == isset( $this->m_intComplianceLevelReferenceId ) ) ? ( string ) $this->m_intComplianceLevelReferenceId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setFailureDescription( $strFailureDescription ) {
		$this->set( 'm_strFailureDescription', CStrings::strTrimDef( $strFailureDescription, -1, NULL, true ) );
	}

	public function getFailureDescription() {
		return $this->m_strFailureDescription;
	}

	public function sqlFailureDescription() {
		return ( true == isset( $this->m_strFailureDescription ) ) ? '\'' . addslashes( $this->m_strFailureDescription ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLastRequestedOn( $strLastRequestedOn ) {
		$this->set( 'm_strLastRequestedOn', CStrings::strTrimDef( $strLastRequestedOn, -1, NULL, true ) );
	}

	public function getLastRequestedOn() {
		return $this->m_strLastRequestedOn;
	}

	public function sqlLastRequestedOn() {
		return ( true == isset( $this->m_strLastRequestedOn ) ) ? '\'' . $this->m_strLastRequestedOn . '\'' : 'NOW()';
	}

	public function setLastApprovedOn( $strLastApprovedOn ) {
		$this->set( 'm_strLastApprovedOn', CStrings::strTrimDef( $strLastApprovedOn, -1, NULL, true ) );
	}

	public function getLastApprovedOn() {
		return $this->m_strLastApprovedOn;
	}

	public function sqlLastApprovedOn() {
		return ( true == isset( $this->m_strLastApprovedOn ) ) ? '\'' . $this->m_strLastApprovedOn . '\'' : 'NULL';
	}

	public function setFailedBy( $intFailedBy ) {
		$this->set( 'm_intFailedBy', CStrings::strToIntDef( $intFailedBy, NULL, false ) );
	}

	public function getFailedBy() {
		return $this->m_intFailedBy;
	}

	public function sqlFailedBy() {
		return ( true == isset( $this->m_intFailedBy ) ) ? ( string ) $this->m_intFailedBy : 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, compliance_job_item_id, vendor_invitation_id, compliance_job_id, compliance_rule_id, compliance_status_id, compliance_level_reference_id, vendor_id, log_datetime, failure_description, is_active, last_requested_on, last_approved_on, failed_by, failed_on, approved_by, approved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlComplianceJobItemId() . ', ' .
 						$this->sqlVendorInvitationId() . ', ' .
 						$this->sqlComplianceJobId() . ', ' .
 						$this->sqlComplianceRuleId() . ', ' .
 						$this->sqlComplianceStatusId() . ', ' .
 						$this->sqlComplianceLevelReferenceId() . ', ' .
 						$this->sqlVendorId() . ', ' .
 						$this->sqlLogDatetime() . ', ' .
 						$this->sqlFailureDescription() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlLastRequestedOn() . ', ' .
 						$this->sqlLastApprovedOn() . ', ' .
 						$this->sqlFailedBy() . ', ' .
 						$this->sqlFailedOn() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_job_item_id = ' . $this->sqlComplianceJobItemId() . ','; } elseif( true == array_key_exists( 'ComplianceJobItemId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_job_item_id = ' . $this->sqlComplianceJobItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_invitation_id = ' . $this->sqlVendorInvitationId() . ','; } elseif( true == array_key_exists( 'VendorInvitationId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_invitation_id = ' . $this->sqlVendorInvitationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_job_id = ' . $this->sqlComplianceJobId() . ','; } elseif( true == array_key_exists( 'ComplianceJobId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_job_id = ' . $this->sqlComplianceJobId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_rule_id = ' . $this->sqlComplianceRuleId() . ','; } elseif( true == array_key_exists( 'ComplianceRuleId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_rule_id = ' . $this->sqlComplianceRuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_status_id = ' . $this->sqlComplianceStatusId() . ','; } elseif( true == array_key_exists( 'ComplianceStatusId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_status_id = ' . $this->sqlComplianceStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_level_reference_id = ' . $this->sqlComplianceLevelReferenceId() . ','; } elseif( true == array_key_exists( 'ComplianceLevelReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_level_reference_id = ' . $this->sqlComplianceLevelReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failure_description = ' . $this->sqlFailureDescription() . ','; } elseif( true == array_key_exists( 'FailureDescription', $this->getChangedColumns() ) ) { $strSql .= ' failure_description = ' . $this->sqlFailureDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_requested_on = ' . $this->sqlLastRequestedOn() . ','; } elseif( true == array_key_exists( 'LastRequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_requested_on = ' . $this->sqlLastRequestedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_approved_on = ' . $this->sqlLastApprovedOn() . ','; } elseif( true == array_key_exists( 'LastApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_approved_on = ' . $this->sqlLastApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_by = ' . $this->sqlFailedBy() . ','; } elseif( true == array_key_exists( 'FailedBy', $this->getChangedColumns() ) ) { $strSql .= ' failed_by = ' . $this->sqlFailedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'compliance_job_item_id' => $this->getComplianceJobItemId(),
			'vendor_invitation_id' => $this->getVendorInvitationId(),
			'compliance_job_id' => $this->getComplianceJobId(),
			'compliance_rule_id' => $this->getComplianceRuleId(),
			'compliance_status_id' => $this->getComplianceStatusId(),
			'compliance_level_reference_id' => $this->getComplianceLevelReferenceId(),
			'vendor_id' => $this->getVendorId(),
			'log_datetime' => $this->getLogDatetime(),
			'failure_description' => $this->getFailureDescription(),
			'is_active' => $this->getIsActive(),
			'last_requested_on' => $this->getLastRequestedOn(),
			'last_approved_on' => $this->getLastApprovedOn(),
			'failed_by' => $this->getFailedBy(),
			'failed_on' => $this->getFailedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>