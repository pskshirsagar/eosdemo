<?php

class CBaseUtilityInvoice extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_invoices';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intUtilityInvoiceTypeId;
	protected $m_intUtilityAccountId;
	protected $m_intUtilityBatchId;
	protected $m_intUtilityDistributionId;
	protected $m_intUtilityInvoiceMessageId;
	protected $m_fltInvoiceAmount;
	protected $m_strUnitNumber;
	protected $m_strBackPageFooter;
	protected $m_boolIsDueUponReceipt;
	protected $m_boolIsConvergent;
	protected $m_strInvoiceDate;
	protected $m_strInvoiceDueDate;
	protected $m_strScheduledChargesDueDate;
	protected $m_strMailedOn;
	protected $m_strEmailedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intVerifiedBy;
	protected $m_strVerifiedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUtilityAccountAddressId;
	protected $m_intUtilityPropertyUnitId;
	protected $m_strUnitAddress;
	protected $m_jsonUnitAddress;

	public function __construct() {
		parent::__construct();

		$this->m_fltInvoiceAmount = '0';
		$this->m_boolIsDueUponReceipt = false;
		$this->m_boolIsConvergent = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['utility_invoice_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityInvoiceTypeId', trim( $arrValues['utility_invoice_type_id'] ) ); elseif( isset( $arrValues['utility_invoice_type_id'] ) ) $this->setUtilityInvoiceTypeId( $arrValues['utility_invoice_type_id'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['utility_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDistributionId', trim( $arrValues['utility_distribution_id'] ) ); elseif( isset( $arrValues['utility_distribution_id'] ) ) $this->setUtilityDistributionId( $arrValues['utility_distribution_id'] );
		if( isset( $arrValues['utility_invoice_message_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityInvoiceMessageId', trim( $arrValues['utility_invoice_message_id'] ) ); elseif( isset( $arrValues['utility_invoice_message_id'] ) ) $this->setUtilityInvoiceMessageId( $arrValues['utility_invoice_message_id'] );
		if( isset( $arrValues['invoice_amount'] ) && $boolDirectSet ) $this->set( 'm_fltInvoiceAmount', trim( $arrValues['invoice_amount'] ) ); elseif( isset( $arrValues['invoice_amount'] ) ) $this->setInvoiceAmount( $arrValues['invoice_amount'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['back_page_footer'] ) && $boolDirectSet ) $this->set( 'm_strBackPageFooter', trim( stripcslashes( $arrValues['back_page_footer'] ) ) ); elseif( isset( $arrValues['back_page_footer'] ) ) $this->setBackPageFooter( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['back_page_footer'] ) : $arrValues['back_page_footer'] );
		if( isset( $arrValues['is_due_upon_receipt'] ) && $boolDirectSet ) $this->set( 'm_boolIsDueUponReceipt', trim( stripcslashes( $arrValues['is_due_upon_receipt'] ) ) ); elseif( isset( $arrValues['is_due_upon_receipt'] ) ) $this->setIsDueUponReceipt( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_due_upon_receipt'] ) : $arrValues['is_due_upon_receipt'] );
		if( isset( $arrValues['is_convergent'] ) && $boolDirectSet ) $this->set( 'm_boolIsConvergent', trim( stripcslashes( $arrValues['is_convergent'] ) ) ); elseif( isset( $arrValues['is_convergent'] ) ) $this->setIsConvergent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_convergent'] ) : $arrValues['is_convergent'] );
		if( isset( $arrValues['invoice_date'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceDate', trim( $arrValues['invoice_date'] ) ); elseif( isset( $arrValues['invoice_date'] ) ) $this->setInvoiceDate( $arrValues['invoice_date'] );
		if( isset( $arrValues['invoice_due_date'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceDueDate', trim( $arrValues['invoice_due_date'] ) ); elseif( isset( $arrValues['invoice_due_date'] ) ) $this->setInvoiceDueDate( $arrValues['invoice_due_date'] );
		if( isset( $arrValues['scheduled_charges_due_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledChargesDueDate', trim( $arrValues['scheduled_charges_due_date'] ) ); elseif( isset( $arrValues['scheduled_charges_due_date'] ) ) $this->setScheduledChargesDueDate( $arrValues['scheduled_charges_due_date'] );
		if( isset( $arrValues['mailed_on'] ) && $boolDirectSet ) $this->set( 'm_strMailedOn', trim( $arrValues['mailed_on'] ) ); elseif( isset( $arrValues['mailed_on'] ) ) $this->setMailedOn( $arrValues['mailed_on'] );
		if( isset( $arrValues['emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strEmailedOn', trim( $arrValues['emailed_on'] ) ); elseif( isset( $arrValues['emailed_on'] ) ) $this->setEmailedOn( $arrValues['emailed_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['verified_by'] ) && $boolDirectSet ) $this->set( 'm_intVerifiedBy', trim( $arrValues['verified_by'] ) ); elseif( isset( $arrValues['verified_by'] ) ) $this->setVerifiedBy( $arrValues['verified_by'] );
		if( isset( $arrValues['verified_on'] ) && $boolDirectSet ) $this->set( 'm_strVerifiedOn', trim( $arrValues['verified_on'] ) ); elseif( isset( $arrValues['verified_on'] ) ) $this->setVerifiedOn( $arrValues['verified_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['utility_account_address_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountAddressId', trim( $arrValues['utility_account_address_id'] ) ); elseif( isset( $arrValues['utility_account_address_id'] ) ) $this->setUtilityAccountAddressId( $arrValues['utility_account_address_id'] );
		if( isset( $arrValues['utility_property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityPropertyUnitId', trim( $arrValues['utility_property_unit_id'] ) ); elseif( isset( $arrValues['utility_property_unit_id'] ) ) $this->setUtilityPropertyUnitId( $arrValues['utility_property_unit_id'] );
		if( isset( $arrValues['unit_address'] ) ) $this->set( 'm_strUnitAddress', trim( $arrValues['unit_address'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUtilityInvoiceTypeId( $intUtilityInvoiceTypeId ) {
		$this->set( 'm_intUtilityInvoiceTypeId', CStrings::strToIntDef( $intUtilityInvoiceTypeId, NULL, false ) );
	}

	public function getUtilityInvoiceTypeId() {
		return $this->m_intUtilityInvoiceTypeId;
	}

	public function sqlUtilityInvoiceTypeId() {
		return ( true == isset( $this->m_intUtilityInvoiceTypeId ) ) ? ( string ) $this->m_intUtilityInvoiceTypeId : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setUtilityDistributionId( $intUtilityDistributionId ) {
		$this->set( 'm_intUtilityDistributionId', CStrings::strToIntDef( $intUtilityDistributionId, NULL, false ) );
	}

	public function getUtilityDistributionId() {
		return $this->m_intUtilityDistributionId;
	}

	public function sqlUtilityDistributionId() {
		return ( true == isset( $this->m_intUtilityDistributionId ) ) ? ( string ) $this->m_intUtilityDistributionId : 'NULL';
	}

	public function setUtilityInvoiceMessageId( $intUtilityInvoiceMessageId ) {
		$this->set( 'm_intUtilityInvoiceMessageId', CStrings::strToIntDef( $intUtilityInvoiceMessageId, NULL, false ) );
	}

	public function getUtilityInvoiceMessageId() {
		return $this->m_intUtilityInvoiceMessageId;
	}

	public function sqlUtilityInvoiceMessageId() {
		return ( true == isset( $this->m_intUtilityInvoiceMessageId ) ) ? ( string ) $this->m_intUtilityInvoiceMessageId : 'NULL';
	}

	public function setInvoiceAmount( $fltInvoiceAmount ) {
		$this->set( 'm_fltInvoiceAmount', CStrings::strToFloatDef( $fltInvoiceAmount, NULL, false, 4 ) );
	}

	public function getInvoiceAmount() {
		return $this->m_fltInvoiceAmount;
	}

	public function sqlInvoiceAmount() {
		return ( true == isset( $this->m_fltInvoiceAmount ) ) ? ( string ) $this->m_fltInvoiceAmount : '0';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setBackPageFooter( $strBackPageFooter ) {
		$this->set( 'm_strBackPageFooter', CStrings::strTrimDef( $strBackPageFooter, -1, NULL, true ) );
	}

	public function getBackPageFooter() {
		return $this->m_strBackPageFooter;
	}

	public function sqlBackPageFooter() {
		return ( true == isset( $this->m_strBackPageFooter ) ) ? '\'' . addslashes( $this->m_strBackPageFooter ) . '\'' : 'NULL';
	}

	public function setIsDueUponReceipt( $boolIsDueUponReceipt ) {
		$this->set( 'm_boolIsDueUponReceipt', CStrings::strToBool( $boolIsDueUponReceipt ) );
	}

	public function getIsDueUponReceipt() {
		return $this->m_boolIsDueUponReceipt;
	}

	public function sqlIsDueUponReceipt() {
		return ( true == isset( $this->m_boolIsDueUponReceipt ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDueUponReceipt ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsConvergent( $boolIsConvergent ) {
		$this->set( 'm_boolIsConvergent', CStrings::strToBool( $boolIsConvergent ) );
	}

	public function getIsConvergent() {
		return $this->m_boolIsConvergent;
	}

	public function sqlIsConvergent() {
		return ( true == isset( $this->m_boolIsConvergent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConvergent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInvoiceDate( $strInvoiceDate ) {
		$this->set( 'm_strInvoiceDate', CStrings::strTrimDef( $strInvoiceDate, -1, NULL, true ) );
	}

	public function getInvoiceDate() {
		return $this->m_strInvoiceDate;
	}

	public function sqlInvoiceDate() {
		return ( true == isset( $this->m_strInvoiceDate ) ) ? '\'' . $this->m_strInvoiceDate . '\'' : 'NOW()';
	}

	public function setInvoiceDueDate( $strInvoiceDueDate ) {
		$this->set( 'm_strInvoiceDueDate', CStrings::strTrimDef( $strInvoiceDueDate, -1, NULL, true ) );
	}

	public function getInvoiceDueDate() {
		return $this->m_strInvoiceDueDate;
	}

	public function sqlInvoiceDueDate() {
		return ( true == isset( $this->m_strInvoiceDueDate ) ) ? '\'' . $this->m_strInvoiceDueDate . '\'' : 'NULL';
	}

	public function setScheduledChargesDueDate( $strScheduledChargesDueDate ) {
		$this->set( 'm_strScheduledChargesDueDate', CStrings::strTrimDef( $strScheduledChargesDueDate, -1, NULL, true ) );
	}

	public function getScheduledChargesDueDate() {
		return $this->m_strScheduledChargesDueDate;
	}

	public function sqlScheduledChargesDueDate() {
		return ( true == isset( $this->m_strScheduledChargesDueDate ) ) ? '\'' . $this->m_strScheduledChargesDueDate . '\'' : 'NULL';
	}

	public function setMailedOn( $strMailedOn ) {
		$this->set( 'm_strMailedOn', CStrings::strTrimDef( $strMailedOn, -1, NULL, true ) );
	}

	public function getMailedOn() {
		return $this->m_strMailedOn;
	}

	public function sqlMailedOn() {
		return ( true == isset( $this->m_strMailedOn ) ) ? '\'' . $this->m_strMailedOn . '\'' : 'NULL';
	}

	public function setEmailedOn( $strEmailedOn ) {
		$this->set( 'm_strEmailedOn', CStrings::strTrimDef( $strEmailedOn, -1, NULL, true ) );
	}

	public function getEmailedOn() {
		return $this->m_strEmailedOn;
	}

	public function sqlEmailedOn() {
		return ( true == isset( $this->m_strEmailedOn ) ) ? '\'' . $this->m_strEmailedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setVerifiedBy( $intVerifiedBy ) {
		$this->set( 'm_intVerifiedBy', CStrings::strToIntDef( $intVerifiedBy, NULL, false ) );
	}

	public function getVerifiedBy() {
		return $this->m_intVerifiedBy;
	}

	public function sqlVerifiedBy() {
		return ( true == isset( $this->m_intVerifiedBy ) ) ? ( string ) $this->m_intVerifiedBy : 'NULL';
	}

	public function setVerifiedOn( $strVerifiedOn ) {
		$this->set( 'm_strVerifiedOn', CStrings::strTrimDef( $strVerifiedOn, -1, NULL, true ) );
	}

	public function getVerifiedOn() {
		return $this->m_strVerifiedOn;
	}

	public function sqlVerifiedOn() {
		return ( true == isset( $this->m_strVerifiedOn ) ) ? '\'' . $this->m_strVerifiedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUtilityAccountAddressId( $intUtilityAccountAddressId ) {
		$this->set( 'm_intUtilityAccountAddressId', CStrings::strToIntDef( $intUtilityAccountAddressId, NULL, false ) );
	}

	public function getUtilityAccountAddressId() {
		return $this->m_intUtilityAccountAddressId;
	}

	public function sqlUtilityAccountAddressId() {
		return ( true == isset( $this->m_intUtilityAccountAddressId ) ) ? ( string ) $this->m_intUtilityAccountAddressId : 'NULL';
	}

	public function setUtilityPropertyUnitId( $intUtilityPropertyUnitId ) {
		$this->set( 'm_intUtilityPropertyUnitId', CStrings::strToIntDef( $intUtilityPropertyUnitId, NULL, false ) );
	}

	public function getUtilityPropertyUnitId() {
		return $this->m_intUtilityPropertyUnitId;
	}

	public function sqlUtilityPropertyUnitId() {
		return ( true == isset( $this->m_intUtilityPropertyUnitId ) ) ? ( string ) $this->m_intUtilityPropertyUnitId : 'NULL';
	}

	public function setUnitAddress( $jsonUnitAddress ) {
		if( true == valObj( $jsonUnitAddress, 'stdClass' ) ) {
			$this->set( 'm_jsonUnitAddress', $jsonUnitAddress );
		} elseif( true == valJsonString( $jsonUnitAddress ) ) {
			$this->set( 'm_jsonUnitAddress', CStrings::strToJson( $jsonUnitAddress ) );
		} else {
			$this->set( 'm_jsonUnitAddress', NULL ); 
		}
		unset( $this->m_strUnitAddress );
	}

	public function getUnitAddress() {
		if( true == isset( $this->m_strUnitAddress ) ) {
			$this->m_jsonUnitAddress = CStrings::strToJson( $this->m_strUnitAddress );
			unset( $this->m_strUnitAddress );
		}
		return $this->m_jsonUnitAddress;
	}

	public function sqlUnitAddress() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getUnitAddress() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getUnitAddress() ) ) . '\'';
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, utility_invoice_type_id, utility_account_id, utility_batch_id, utility_distribution_id, utility_invoice_message_id, invoice_amount, unit_number, back_page_footer, is_due_upon_receipt, is_convergent, invoice_date, invoice_due_date, scheduled_charges_due_date, mailed_on, emailed_on, approved_by, approved_on, verified_by, verified_on, updated_by, updated_on, created_by, created_on, utility_account_address_id, utility_property_unit_id, unit_address )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUtilityInvoiceTypeId() . ', ' .
						$this->sqlUtilityAccountId() . ', ' .
						$this->sqlUtilityBatchId() . ', ' .
						$this->sqlUtilityDistributionId() . ', ' .
						$this->sqlUtilityInvoiceMessageId() . ', ' .
						$this->sqlInvoiceAmount() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlBackPageFooter() . ', ' .
						$this->sqlIsDueUponReceipt() . ', ' .
						$this->sqlIsConvergent() . ', ' .
						$this->sqlInvoiceDate() . ', ' .
						$this->sqlInvoiceDueDate() . ', ' .
						$this->sqlScheduledChargesDueDate() . ', ' .
						$this->sqlMailedOn() . ', ' .
						$this->sqlEmailedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlVerifiedBy() . ', ' .
						$this->sqlVerifiedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUtilityAccountAddressId() . ', ' .
						$this->sqlUtilityPropertyUnitId() . ', ' .
						$this->sqlUnitAddress() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_invoice_type_id = ' . $this->sqlUtilityInvoiceTypeId() . ','; } elseif( true == array_key_exists( 'UtilityInvoiceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_invoice_type_id = ' . $this->sqlUtilityInvoiceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_distribution_id = ' . $this->sqlUtilityDistributionId() . ','; } elseif( true == array_key_exists( 'UtilityDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_distribution_id = ' . $this->sqlUtilityDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_invoice_message_id = ' . $this->sqlUtilityInvoiceMessageId() . ','; } elseif( true == array_key_exists( 'UtilityInvoiceMessageId', $this->getChangedColumns() ) ) { $strSql .= ' utility_invoice_message_id = ' . $this->sqlUtilityInvoiceMessageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount() . ','; } elseif( true == array_key_exists( 'InvoiceAmount', $this->getChangedColumns() ) ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' back_page_footer = ' . $this->sqlBackPageFooter() . ','; } elseif( true == array_key_exists( 'BackPageFooter', $this->getChangedColumns() ) ) { $strSql .= ' back_page_footer = ' . $this->sqlBackPageFooter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_due_upon_receipt = ' . $this->sqlIsDueUponReceipt() . ','; } elseif( true == array_key_exists( 'IsDueUponReceipt', $this->getChangedColumns() ) ) { $strSql .= ' is_due_upon_receipt = ' . $this->sqlIsDueUponReceipt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_convergent = ' . $this->sqlIsConvergent() . ','; } elseif( true == array_key_exists( 'IsConvergent', $this->getChangedColumns() ) ) { $strSql .= ' is_convergent = ' . $this->sqlIsConvergent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_date = ' . $this->sqlInvoiceDate() . ','; } elseif( true == array_key_exists( 'InvoiceDate', $this->getChangedColumns() ) ) { $strSql .= ' invoice_date = ' . $this->sqlInvoiceDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_due_date = ' . $this->sqlInvoiceDueDate() . ','; } elseif( true == array_key_exists( 'InvoiceDueDate', $this->getChangedColumns() ) ) { $strSql .= ' invoice_due_date = ' . $this->sqlInvoiceDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charges_due_date = ' . $this->sqlScheduledChargesDueDate() . ','; } elseif( true == array_key_exists( 'ScheduledChargesDueDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charges_due_date = ' . $this->sqlScheduledChargesDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailed_on = ' . $this->sqlMailedOn() . ','; } elseif( true == array_key_exists( 'MailedOn', $this->getChangedColumns() ) ) { $strSql .= ' mailed_on = ' . $this->sqlMailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; } elseif( true == array_key_exists( 'EmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_by = ' . $this->sqlVerifiedBy() . ','; } elseif( true == array_key_exists( 'VerifiedBy', $this->getChangedColumns() ) ) { $strSql .= ' verified_by = ' . $this->sqlVerifiedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; } elseif( true == array_key_exists( 'VerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_address_id = ' . $this->sqlUtilityAccountAddressId() . ','; } elseif( true == array_key_exists( 'UtilityAccountAddressId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_address_id = ' . $this->sqlUtilityAccountAddressId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_property_unit_id = ' . $this->sqlUtilityPropertyUnitId() . ','; } elseif( true == array_key_exists( 'UtilityPropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' utility_property_unit_id = ' . $this->sqlUtilityPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_address = ' . $this->sqlUnitAddress() . ','; } elseif( true == array_key_exists( 'UnitAddress', $this->getChangedColumns() ) ) { $strSql .= ' unit_address = ' . $this->sqlUnitAddress() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'utility_invoice_type_id' => $this->getUtilityInvoiceTypeId(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'utility_distribution_id' => $this->getUtilityDistributionId(),
			'utility_invoice_message_id' => $this->getUtilityInvoiceMessageId(),
			'invoice_amount' => $this->getInvoiceAmount(),
			'unit_number' => $this->getUnitNumber(),
			'back_page_footer' => $this->getBackPageFooter(),
			'is_due_upon_receipt' => $this->getIsDueUponReceipt(),
			'is_convergent' => $this->getIsConvergent(),
			'invoice_date' => $this->getInvoiceDate(),
			'invoice_due_date' => $this->getInvoiceDueDate(),
			'scheduled_charges_due_date' => $this->getScheduledChargesDueDate(),
			'mailed_on' => $this->getMailedOn(),
			'emailed_on' => $this->getEmailedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'verified_by' => $this->getVerifiedBy(),
			'verified_on' => $this->getVerifiedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'utility_account_address_id' => $this->getUtilityAccountAddressId(),
			'utility_property_unit_id' => $this->getUtilityPropertyUnitId(),
			'unit_address' => $this->getUnitAddress()
		);
	}

}
?>