<?php

class CBaseController extends CEosSingularBase {

	const TABLE_NAME = 'public.controllers';

	protected $m_intId;
	protected $m_intParentControllerId;
	protected $m_strName;
	protected $m_strTitle;
	protected $m_strUrl;
	protected $m_strDescription;
	protected $m_intIsPublic;
	protected $m_intIsPublished;
	protected $m_boolIsHidden;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_strTitle = NULL;
		$this->m_strUrl = NULL;
		$this->m_intIsPublic = '0';
		$this->m_intIsPublished = '1';
		$this->m_boolIsHidden = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['parent_controller_id'] ) && $boolDirectSet ) $this->set( 'm_intParentControllerId', trim( $arrValues['parent_controller_id'] ) ); elseif( isset( $arrValues['parent_controller_id'] ) ) $this->setParentControllerId( $arrValues['parent_controller_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_public'] ) && $boolDirectSet ) $this->set( 'm_intIsPublic', trim( $arrValues['is_public'] ) ); elseif( isset( $arrValues['is_public'] ) ) $this->setIsPublic( $arrValues['is_public'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_hidden'] ) && $boolDirectSet ) $this->set( 'm_boolIsHidden', trim( stripcslashes( $arrValues['is_hidden'] ) ) ); elseif( isset( $arrValues['is_hidden'] ) ) $this->setIsHidden( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hidden'] ) : $arrValues['is_hidden'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setParentControllerId( $intParentControllerId ) {
		$this->set( 'm_intParentControllerId', CStrings::strToIntDef( $intParentControllerId, NULL, false ) );
	}

	public function getParentControllerId() {
		return $this->m_intParentControllerId;
	}

	public function sqlParentControllerId() {
		return ( true == isset( $this->m_intParentControllerId ) ) ? ( string ) $this->m_intParentControllerId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 150, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 255, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : '\'NULL\'';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 1024, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : '\'NULL\'';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublic( $intIsPublic ) {
		$this->set( 'm_intIsPublic', CStrings::strToIntDef( $intIsPublic, NULL, false ) );
	}

	public function getIsPublic() {
		return $this->m_intIsPublic;
	}

	public function sqlIsPublic() {
		return ( true == isset( $this->m_intIsPublic ) ) ? ( string ) $this->m_intIsPublic : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsHidden( $boolIsHidden ) {
		$this->set( 'm_boolIsHidden', CStrings::strToBool( $boolIsHidden ) );
	}

	public function getIsHidden() {
		return $this->m_boolIsHidden;
	}

	public function sqlIsHidden() {
		return ( true == isset( $this->m_boolIsHidden ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHidden ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'parent_controller_id' => $this->getParentControllerId(),
			'name' => $this->getName(),
			'title' => $this->getTitle(),
			'url' => $this->getUrl(),
			'description' => $this->getDescription(),
			'is_public' => $this->getIsPublic(),
			'is_published' => $this->getIsPublished(),
			'is_hidden' => $this->getIsHidden(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>