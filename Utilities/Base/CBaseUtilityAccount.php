<?php

class CBaseUtilityAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_arrintUnitSpaceIds;
	protected $m_intInvoiceDeliveryTypeId;
	protected $m_intUtilityAccountStatusTypeId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_strRemotePrimaryKey;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_arrstrAdditionalEmailAddresses;
	protected $m_strOriginalLeaseStartDate;
	protected $m_strOriginalLeaseMoveInDate;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_intOccupantCount;
	protected $m_boolIsCareRate;
	protected $m_boolIsMovedOut;
	protected $m_boolIsLocked;
	protected $m_boolIsNotOverrideName;
	protected $m_boolIsSendInvoiceToGuarantor;
	protected $m_boolUseTemporaryAddress;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUtilityMoveOutTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intOccupantCount = '1';
		$this->m_boolIsCareRate = false;
		$this->m_boolIsMovedOut = false;
		$this->m_boolIsLocked = false;
		$this->m_boolIsNotOverrideName = false;
		$this->m_boolIsSendInvoiceToGuarantor = false;
		$this->m_boolUseTemporaryAddress = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintUnitSpaceIds', trim( $arrValues['unit_space_ids'] ) ); elseif( isset( $arrValues['unit_space_ids'] ) ) $this->setUnitSpaceIds( $arrValues['unit_space_ids'] );
		if( isset( $arrValues['invoice_delivery_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceDeliveryTypeId', trim( $arrValues['invoice_delivery_type_id'] ) ); elseif( isset( $arrValues['invoice_delivery_type_id'] ) ) $this->setInvoiceDeliveryTypeId( $arrValues['invoice_delivery_type_id'] );
		if( isset( $arrValues['utility_account_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountStatusTypeId', trim( $arrValues['utility_account_status_type_id'] ) ); elseif( isset( $arrValues['utility_account_status_type_id'] ) ) $this->setUtilityAccountStatusTypeId( $arrValues['utility_account_status_type_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( stripcslashes( $arrValues['name_first'] ) ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first'] ) : $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( stripcslashes( $arrValues['name_last'] ) ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last'] ) : $arrValues['name_last'] );
		if( isset( $arrValues['additional_email_addresses'] ) && $boolDirectSet ) $this->set( 'm_arrstrAdditionalEmailAddresses', trim( $arrValues['additional_email_addresses'] ) ); elseif( isset( $arrValues['additional_email_addresses'] ) ) $this->setAdditionalEmailAddresses( $arrValues['additional_email_addresses'] );
		if( isset( $arrValues['original_lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalLeaseStartDate', trim( $arrValues['original_lease_start_date'] ) ); elseif( isset( $arrValues['original_lease_start_date'] ) ) $this->setOriginalLeaseStartDate( $arrValues['original_lease_start_date'] );
		if( isset( $arrValues['original_lease_move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalLeaseMoveInDate', trim( $arrValues['original_lease_move_in_date'] ) ); elseif( isset( $arrValues['original_lease_move_in_date'] ) ) $this->setOriginalLeaseMoveInDate( $arrValues['original_lease_move_in_date'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDate', trim( $arrValues['lease_end_date'] ) ); elseif( isset( $arrValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrValues['lease_end_date'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['occupant_count'] ) && $boolDirectSet ) $this->set( 'm_intOccupantCount', trim( $arrValues['occupant_count'] ) ); elseif( isset( $arrValues['occupant_count'] ) ) $this->setOccupantCount( $arrValues['occupant_count'] );
		if( isset( $arrValues['is_care_rate'] ) && $boolDirectSet ) $this->set( 'm_boolIsCareRate', trim( stripcslashes( $arrValues['is_care_rate'] ) ) ); elseif( isset( $arrValues['is_care_rate'] ) ) $this->setIsCareRate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_care_rate'] ) : $arrValues['is_care_rate'] );
		if( isset( $arrValues['is_moved_out'] ) && $boolDirectSet ) $this->set( 'm_boolIsMovedOut', trim( stripcslashes( $arrValues['is_moved_out'] ) ) ); elseif( isset( $arrValues['is_moved_out'] ) ) $this->setIsMovedOut( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_moved_out'] ) : $arrValues['is_moved_out'] );
		if( isset( $arrValues['is_locked'] ) && $boolDirectSet ) $this->set( 'm_boolIsLocked', trim( stripcslashes( $arrValues['is_locked'] ) ) ); elseif( isset( $arrValues['is_locked'] ) ) $this->setIsLocked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_locked'] ) : $arrValues['is_locked'] );
		if( isset( $arrValues['is_not_override_name'] ) && $boolDirectSet ) $this->set( 'm_boolIsNotOverrideName', trim( stripcslashes( $arrValues['is_not_override_name'] ) ) ); elseif( isset( $arrValues['is_not_override_name'] ) ) $this->setIsNotOverrideName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_not_override_name'] ) : $arrValues['is_not_override_name'] );
		if( isset( $arrValues['is_send_invoice_to_guarantor'] ) && $boolDirectSet ) $this->set( 'm_boolIsSendInvoiceToGuarantor', trim( stripcslashes( $arrValues['is_send_invoice_to_guarantor'] ) ) ); elseif( isset( $arrValues['is_send_invoice_to_guarantor'] ) ) $this->setIsSendInvoiceToGuarantor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_send_invoice_to_guarantor'] ) : $arrValues['is_send_invoice_to_guarantor'] );
		if( isset( $arrValues['use_temporary_address'] ) && $boolDirectSet ) $this->set( 'm_boolUseTemporaryAddress', trim( stripcslashes( $arrValues['use_temporary_address'] ) ) ); elseif( isset( $arrValues['use_temporary_address'] ) ) $this->setUseTemporaryAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_temporary_address'] ) : $arrValues['use_temporary_address'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['utility_move_out_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityMoveOutTypeId', trim( $arrValues['utility_move_out_type_id'] ) ); elseif( isset( $arrValues['utility_move_out_type_id'] ) ) $this->setUtilityMoveOutTypeId( $arrValues['utility_move_out_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceIds( $arrintUnitSpaceIds ) {
		$this->set( 'm_arrintUnitSpaceIds', CStrings::strToArrIntDef( $arrintUnitSpaceIds, NULL ) );
	}

	public function getUnitSpaceIds() {
		return $this->m_arrintUnitSpaceIds;
	}

	public function sqlUnitSpaceIds() {
		return ( true == isset( $this->m_arrintUnitSpaceIds ) && true == valArr( $this->m_arrintUnitSpaceIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintUnitSpaceIds, NULL ) . '\'' : 'NULL';
	}

	public function setInvoiceDeliveryTypeId( $intInvoiceDeliveryTypeId ) {
		$this->set( 'm_intInvoiceDeliveryTypeId', CStrings::strToIntDef( $intInvoiceDeliveryTypeId, NULL, false ) );
	}

	public function getInvoiceDeliveryTypeId() {
		return $this->m_intInvoiceDeliveryTypeId;
	}

	public function sqlInvoiceDeliveryTypeId() {
		return ( true == isset( $this->m_intInvoiceDeliveryTypeId ) ) ? ( string ) $this->m_intInvoiceDeliveryTypeId : 'NULL';
	}

	public function setUtilityAccountStatusTypeId( $intUtilityAccountStatusTypeId ) {
		$this->set( 'm_intUtilityAccountStatusTypeId', CStrings::strToIntDef( $intUtilityAccountStatusTypeId, NULL, false ) );
	}

	public function getUtilityAccountStatusTypeId() {
		return $this->m_intUtilityAccountStatusTypeId;
	}

	public function sqlUtilityAccountStatusTypeId() {
		return ( true == isset( $this->m_intUtilityAccountStatusTypeId ) ) ? ( string ) $this->m_intUtilityAccountStatusTypeId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? '\'' . addslashes( $this->m_strNameFirst ) . '\'' : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? '\'' . addslashes( $this->m_strNameLast ) . '\'' : 'NULL';
	}

	public function setAdditionalEmailAddresses( $arrstrAdditionalEmailAddresses ) {
		$this->set( 'm_arrstrAdditionalEmailAddresses', CStrings::strToArrIntDef( $arrstrAdditionalEmailAddresses, NULL ) );
	}

	public function getAdditionalEmailAddresses() {
		return $this->m_arrstrAdditionalEmailAddresses;
	}

	public function sqlAdditionalEmailAddresses() {
		return ( true == isset( $this->m_arrstrAdditionalEmailAddresses ) && true == valArr( $this->m_arrstrAdditionalEmailAddresses ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrAdditionalEmailAddresses, NULL ) . '\'' : 'NULL';
	}

	public function setOriginalLeaseStartDate( $strOriginalLeaseStartDate ) {
		$this->set( 'm_strOriginalLeaseStartDate', CStrings::strTrimDef( $strOriginalLeaseStartDate, -1, NULL, true ) );
	}

	public function getOriginalLeaseStartDate() {
		return $this->m_strOriginalLeaseStartDate;
	}

	public function sqlOriginalLeaseStartDate() {
		return ( true == isset( $this->m_strOriginalLeaseStartDate ) ) ? '\'' . $this->m_strOriginalLeaseStartDate . '\'' : 'NOW()';
	}

	public function setOriginalLeaseMoveInDate( $strOriginalLeaseMoveInDate ) {
		$this->set( 'm_strOriginalLeaseMoveInDate', CStrings::strTrimDef( $strOriginalLeaseMoveInDate, -1, NULL, true ) );
	}

	public function getOriginalLeaseMoveInDate() {
		return $this->m_strOriginalLeaseMoveInDate;
	}

	public function sqlOriginalLeaseMoveInDate() {
		return ( true == isset( $this->m_strOriginalLeaseMoveInDate ) ) ? '\'' . $this->m_strOriginalLeaseMoveInDate . '\'' : 'NULL';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NOW()';
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->set( 'm_strLeaseEndDate', CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true ) );
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function sqlLeaseEndDate() {
		return ( true == isset( $this->m_strLeaseEndDate ) ) ? '\'' . $this->m_strLeaseEndDate . '\'' : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NOW()';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setOccupantCount( $intOccupantCount ) {
		$this->set( 'm_intOccupantCount', CStrings::strToIntDef( $intOccupantCount, NULL, false ) );
	}

	public function getOccupantCount() {
		return $this->m_intOccupantCount;
	}

	public function sqlOccupantCount() {
		return ( true == isset( $this->m_intOccupantCount ) ) ? ( string ) $this->m_intOccupantCount : '1';
	}

	public function setIsCareRate( $boolIsCareRate ) {
		$this->set( 'm_boolIsCareRate', CStrings::strToBool( $boolIsCareRate ) );
	}

	public function getIsCareRate() {
		return $this->m_boolIsCareRate;
	}

	public function sqlIsCareRate() {
		return ( true == isset( $this->m_boolIsCareRate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCareRate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsMovedOut( $boolIsMovedOut ) {
		$this->set( 'm_boolIsMovedOut', CStrings::strToBool( $boolIsMovedOut ) );
	}

	public function getIsMovedOut() {
		return $this->m_boolIsMovedOut;
	}

	public function sqlIsMovedOut() {
		return ( true == isset( $this->m_boolIsMovedOut ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMovedOut ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLocked( $boolIsLocked ) {
		$this->set( 'm_boolIsLocked', CStrings::strToBool( $boolIsLocked ) );
	}

	public function getIsLocked() {
		return $this->m_boolIsLocked;
	}

	public function sqlIsLocked() {
		return ( true == isset( $this->m_boolIsLocked ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLocked ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsNotOverrideName( $boolIsNotOverrideName ) {
		$this->set( 'm_boolIsNotOverrideName', CStrings::strToBool( $boolIsNotOverrideName ) );
	}

	public function getIsNotOverrideName() {
		return $this->m_boolIsNotOverrideName;
	}

	public function sqlIsNotOverrideName() {
		return ( true == isset( $this->m_boolIsNotOverrideName ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNotOverrideName ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSendInvoiceToGuarantor( $boolIsSendInvoiceToGuarantor ) {
		$this->set( 'm_boolIsSendInvoiceToGuarantor', CStrings::strToBool( $boolIsSendInvoiceToGuarantor ) );
	}

	public function getIsSendInvoiceToGuarantor() {
		return $this->m_boolIsSendInvoiceToGuarantor;
	}

	public function sqlIsSendInvoiceToGuarantor() {
		return ( true == isset( $this->m_boolIsSendInvoiceToGuarantor ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSendInvoiceToGuarantor ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseTemporaryAddress( $boolUseTemporaryAddress ) {
		$this->set( 'm_boolUseTemporaryAddress', CStrings::strToBool( $boolUseTemporaryAddress ) );
	}

	public function getUseTemporaryAddress() {
		return $this->m_boolUseTemporaryAddress;
	}

	public function sqlUseTemporaryAddress() {
		return ( true == isset( $this->m_boolUseTemporaryAddress ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseTemporaryAddress ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUtilityMoveOutTypeId( $intUtilityMoveOutTypeId ) {
		$this->set( 'm_intUtilityMoveOutTypeId', CStrings::strToIntDef( $intUtilityMoveOutTypeId, NULL, false ) );
	}

	public function getUtilityMoveOutTypeId() {
		return $this->m_intUtilityMoveOutTypeId;
	}

	public function sqlUtilityMoveOutTypeId() {
		return ( true == isset( $this->m_intUtilityMoveOutTypeId ) ) ? ( string ) $this->m_intUtilityMoveOutTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, unit_space_ids, invoice_delivery_type_id, utility_account_status_type_id, customer_id, lease_id, remote_primary_key, name_first, name_last, additional_email_addresses, original_lease_start_date, original_lease_move_in_date, lease_start_date, lease_end_date, move_in_date, move_out_date, occupant_count, is_care_rate, is_moved_out, is_locked, is_not_override_name, is_send_invoice_to_guarantor, use_temporary_address, updated_by, updated_on, created_by, created_on, utility_move_out_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceIds() . ', ' .
						$this->sqlInvoiceDeliveryTypeId() . ', ' .
						$this->sqlUtilityAccountStatusTypeId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlAdditionalEmailAddresses() . ', ' .
						$this->sqlOriginalLeaseStartDate() . ', ' .
						$this->sqlOriginalLeaseMoveInDate() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlLeaseEndDate() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlOccupantCount() . ', ' .
						$this->sqlIsCareRate() . ', ' .
						$this->sqlIsMovedOut() . ', ' .
						$this->sqlIsLocked() . ', ' .
						$this->sqlIsNotOverrideName() . ', ' .
						$this->sqlIsSendInvoiceToGuarantor() . ', ' .
						$this->sqlUseTemporaryAddress() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUtilityMoveOutTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_ids = ' . $this->sqlUnitSpaceIds(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceIds', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_ids = ' . $this->sqlUnitSpaceIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_delivery_type_id = ' . $this->sqlInvoiceDeliveryTypeId(). ',' ; } elseif( true == array_key_exists( 'InvoiceDeliveryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_delivery_type_id = ' . $this->sqlInvoiceDeliveryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_status_type_id = ' . $this->sqlUtilityAccountStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityAccountStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_status_type_id = ' . $this->sqlUtilityAccountStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_email_addresses = ' . $this->sqlAdditionalEmailAddresses(). ',' ; } elseif( true == array_key_exists( 'AdditionalEmailAddresses', $this->getChangedColumns() ) ) { $strSql .= ' additional_email_addresses = ' . $this->sqlAdditionalEmailAddresses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_lease_start_date = ' . $this->sqlOriginalLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'OriginalLeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' original_lease_start_date = ' . $this->sqlOriginalLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_lease_move_in_date = ' . $this->sqlOriginalLeaseMoveInDate(). ',' ; } elseif( true == array_key_exists( 'OriginalLeaseMoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' original_lease_move_in_date = ' . $this->sqlOriginalLeaseMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate(). ',' ; } elseif( true == array_key_exists( 'LeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate(). ',' ; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount(). ',' ; } elseif( true == array_key_exists( 'OccupantCount', $this->getChangedColumns() ) ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_care_rate = ' . $this->sqlIsCareRate(). ',' ; } elseif( true == array_key_exists( 'IsCareRate', $this->getChangedColumns() ) ) { $strSql .= ' is_care_rate = ' . $this->sqlIsCareRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_moved_out = ' . $this->sqlIsMovedOut(). ',' ; } elseif( true == array_key_exists( 'IsMovedOut', $this->getChangedColumns() ) ) { $strSql .= ' is_moved_out = ' . $this->sqlIsMovedOut() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_locked = ' . $this->sqlIsLocked(). ',' ; } elseif( true == array_key_exists( 'IsLocked', $this->getChangedColumns() ) ) { $strSql .= ' is_locked = ' . $this->sqlIsLocked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_not_override_name = ' . $this->sqlIsNotOverrideName(). ',' ; } elseif( true == array_key_exists( 'IsNotOverrideName', $this->getChangedColumns() ) ) { $strSql .= ' is_not_override_name = ' . $this->sqlIsNotOverrideName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_send_invoice_to_guarantor = ' . $this->sqlIsSendInvoiceToGuarantor(). ',' ; } elseif( true == array_key_exists( 'IsSendInvoiceToGuarantor', $this->getChangedColumns() ) ) { $strSql .= ' is_send_invoice_to_guarantor = ' . $this->sqlIsSendInvoiceToGuarantor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_temporary_address = ' . $this->sqlUseTemporaryAddress(). ',' ; } elseif( true == array_key_exists( 'UseTemporaryAddress', $this->getChangedColumns() ) ) { $strSql .= ' use_temporary_address = ' . $this->sqlUseTemporaryAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_move_out_type_id = ' . $this->sqlUtilityMoveOutTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityMoveOutTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_move_out_type_id = ' . $this->sqlUtilityMoveOutTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_ids' => $this->getUnitSpaceIds(),
			'invoice_delivery_type_id' => $this->getInvoiceDeliveryTypeId(),
			'utility_account_status_type_id' => $this->getUtilityAccountStatusTypeId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'additional_email_addresses' => $this->getAdditionalEmailAddresses(),
			'original_lease_start_date' => $this->getOriginalLeaseStartDate(),
			'original_lease_move_in_date' => $this->getOriginalLeaseMoveInDate(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'lease_end_date' => $this->getLeaseEndDate(),
			'move_in_date' => $this->getMoveInDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'occupant_count' => $this->getOccupantCount(),
			'is_care_rate' => $this->getIsCareRate(),
			'is_moved_out' => $this->getIsMovedOut(),
			'is_locked' => $this->getIsLocked(),
			'is_not_override_name' => $this->getIsNotOverrideName(),
			'is_send_invoice_to_guarantor' => $this->getIsSendInvoiceToGuarantor(),
			'use_temporary_address' => $this->getUseTemporaryAddress(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'utility_move_out_type_id' => $this->getUtilityMoveOutTypeId()
		);
	}

}
?>