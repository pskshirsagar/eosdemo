<?php

class CBaseVcrUtilityBill extends CEosSingularBase {

	const TABLE_NAME = 'public.vcr_utility_bills';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intAccountId;
	protected $m_intTransactionId;
	protected $m_intUtilityTypeId;
	protected $m_intUtilityBillId;
	protected $m_intUtilityAccountId;
	protected $m_intUtilityInvoiceId;
	protected $m_intUtilityTransactionId;
	protected $m_intUtilityBatchId;
	protected $m_strRequestDatetime;
	protected $m_strDeadlineDatetime;
	protected $m_strNotes;
	protected $m_strMoveDate;
	protected $m_boolIsExiting;
	protected $m_boolIsViolation;
	protected $m_boolHasHighUsage;
	protected $m_strSatisfiedOn;
	protected $m_intValidatedBy;
	protected $m_strValidationCompletedOn;
	protected $m_intProcessedBy;
	protected $m_strProcessedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsExiting = false;
		$this->m_boolIsViolation = false;
		$this->m_boolHasHighUsage = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeId', trim( $arrValues['utility_type_id'] ) ); elseif( isset( $arrValues['utility_type_id'] ) ) $this->setUtilityTypeId( $arrValues['utility_type_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['utility_invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityInvoiceId', trim( $arrValues['utility_invoice_id'] ) ); elseif( isset( $arrValues['utility_invoice_id'] ) ) $this->setUtilityInvoiceId( $arrValues['utility_invoice_id'] );
		if( isset( $arrValues['utility_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTransactionId', trim( $arrValues['utility_transaction_id'] ) ); elseif( isset( $arrValues['utility_transaction_id'] ) ) $this->setUtilityTransactionId( $arrValues['utility_transaction_id'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['request_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestDatetime', trim( $arrValues['request_datetime'] ) ); elseif( isset( $arrValues['request_datetime'] ) ) $this->setRequestDatetime( $arrValues['request_datetime'] );
		if( isset( $arrValues['deadline_datetime'] ) && $boolDirectSet ) $this->set( 'm_strDeadlineDatetime', trim( $arrValues['deadline_datetime'] ) ); elseif( isset( $arrValues['deadline_datetime'] ) ) $this->setDeadlineDatetime( $arrValues['deadline_datetime'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['move_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveDate', trim( $arrValues['move_date'] ) ); elseif( isset( $arrValues['move_date'] ) ) $this->setMoveDate( $arrValues['move_date'] );
		if( isset( $arrValues['is_exiting'] ) && $boolDirectSet ) $this->set( 'm_boolIsExiting', trim( stripcslashes( $arrValues['is_exiting'] ) ) ); elseif( isset( $arrValues['is_exiting'] ) ) $this->setIsExiting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_exiting'] ) : $arrValues['is_exiting'] );
		if( isset( $arrValues['is_violation'] ) && $boolDirectSet ) $this->set( 'm_boolIsViolation', trim( stripcslashes( $arrValues['is_violation'] ) ) ); elseif( isset( $arrValues['is_violation'] ) ) $this->setIsViolation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_violation'] ) : $arrValues['is_violation'] );
		if( isset( $arrValues['has_high_usage'] ) && $boolDirectSet ) $this->set( 'm_boolHasHighUsage', trim( stripcslashes( $arrValues['has_high_usage'] ) ) ); elseif( isset( $arrValues['has_high_usage'] ) ) $this->setHasHighUsage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_high_usage'] ) : $arrValues['has_high_usage'] );
		if( isset( $arrValues['satisfied_on'] ) && $boolDirectSet ) $this->set( 'm_strSatisfiedOn', trim( $arrValues['satisfied_on'] ) ); elseif( isset( $arrValues['satisfied_on'] ) ) $this->setSatisfiedOn( $arrValues['satisfied_on'] );
		if( isset( $arrValues['validated_by'] ) && $boolDirectSet ) $this->set( 'm_intValidatedBy', trim( $arrValues['validated_by'] ) ); elseif( isset( $arrValues['validated_by'] ) ) $this->setValidatedBy( $arrValues['validated_by'] );
		if( isset( $arrValues['validation_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strValidationCompletedOn', trim( $arrValues['validation_completed_on'] ) ); elseif( isset( $arrValues['validation_completed_on'] ) ) $this->setValidationCompletedOn( $arrValues['validation_completed_on'] );
		if( isset( $arrValues['processed_by'] ) && $boolDirectSet ) $this->set( 'm_intProcessedBy', trim( $arrValues['processed_by'] ) ); elseif( isset( $arrValues['processed_by'] ) ) $this->setProcessedBy( $arrValues['processed_by'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->set( 'm_intUtilityTypeId', CStrings::strToIntDef( $intUtilityTypeId, NULL, false ) );
	}

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function sqlUtilityTypeId() {
		return ( true == isset( $this->m_intUtilityTypeId ) ) ? ( string ) $this->m_intUtilityTypeId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setUtilityInvoiceId( $intUtilityInvoiceId ) {
		$this->set( 'm_intUtilityInvoiceId', CStrings::strToIntDef( $intUtilityInvoiceId, NULL, false ) );
	}

	public function getUtilityInvoiceId() {
		return $this->m_intUtilityInvoiceId;
	}

	public function sqlUtilityInvoiceId() {
		return ( true == isset( $this->m_intUtilityInvoiceId ) ) ? ( string ) $this->m_intUtilityInvoiceId : 'NULL';
	}

	public function setUtilityTransactionId( $intUtilityTransactionId ) {
		$this->set( 'm_intUtilityTransactionId', CStrings::strToIntDef( $intUtilityTransactionId, NULL, false ) );
	}

	public function getUtilityTransactionId() {
		return $this->m_intUtilityTransactionId;
	}

	public function sqlUtilityTransactionId() {
		return ( true == isset( $this->m_intUtilityTransactionId ) ) ? ( string ) $this->m_intUtilityTransactionId : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->set( 'm_strRequestDatetime', CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true ) );
	}

	public function getRequestDatetime() {
		return $this->m_strRequestDatetime;
	}

	public function sqlRequestDatetime() {
		return ( true == isset( $this->m_strRequestDatetime ) ) ? '\'' . $this->m_strRequestDatetime . '\'' : 'NOW()';
	}

	public function setDeadlineDatetime( $strDeadlineDatetime ) {
		$this->set( 'm_strDeadlineDatetime', CStrings::strTrimDef( $strDeadlineDatetime, -1, NULL, true ) );
	}

	public function getDeadlineDatetime() {
		return $this->m_strDeadlineDatetime;
	}

	public function sqlDeadlineDatetime() {
		return ( true == isset( $this->m_strDeadlineDatetime ) ) ? '\'' . $this->m_strDeadlineDatetime . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setMoveDate( $strMoveDate ) {
		$this->set( 'm_strMoveDate', CStrings::strTrimDef( $strMoveDate, -1, NULL, true ) );
	}

	public function getMoveDate() {
		return $this->m_strMoveDate;
	}

	public function sqlMoveDate() {
		return ( true == isset( $this->m_strMoveDate ) ) ? '\'' . $this->m_strMoveDate . '\'' : 'NULL';
	}

	public function setIsExiting( $boolIsExiting ) {
		$this->set( 'm_boolIsExiting', CStrings::strToBool( $boolIsExiting ) );
	}

	public function getIsExiting() {
		return $this->m_boolIsExiting;
	}

	public function sqlIsExiting() {
		return ( true == isset( $this->m_boolIsExiting ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsExiting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsViolation( $boolIsViolation ) {
		$this->set( 'm_boolIsViolation', CStrings::strToBool( $boolIsViolation ) );
	}

	public function getIsViolation() {
		return $this->m_boolIsViolation;
	}

	public function sqlIsViolation() {
		return ( true == isset( $this->m_boolIsViolation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsViolation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasHighUsage( $boolHasHighUsage ) {
		$this->set( 'm_boolHasHighUsage', CStrings::strToBool( $boolHasHighUsage ) );
	}

	public function getHasHighUsage() {
		return $this->m_boolHasHighUsage;
	}

	public function sqlHasHighUsage() {
		return ( true == isset( $this->m_boolHasHighUsage ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasHighUsage ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSatisfiedOn( $strSatisfiedOn ) {
		$this->set( 'm_strSatisfiedOn', CStrings::strTrimDef( $strSatisfiedOn, -1, NULL, true ) );
	}

	public function getSatisfiedOn() {
		return $this->m_strSatisfiedOn;
	}

	public function sqlSatisfiedOn() {
		return ( true == isset( $this->m_strSatisfiedOn ) ) ? '\'' . $this->m_strSatisfiedOn . '\'' : 'NULL';
	}

	public function setValidatedBy( $intValidatedBy ) {
		$this->set( 'm_intValidatedBy', CStrings::strToIntDef( $intValidatedBy, NULL, false ) );
	}

	public function getValidatedBy() {
		return $this->m_intValidatedBy;
	}

	public function sqlValidatedBy() {
		return ( true == isset( $this->m_intValidatedBy ) ) ? ( string ) $this->m_intValidatedBy : 'NULL';
	}

	public function setValidationCompletedOn( $strValidationCompletedOn ) {
		$this->set( 'm_strValidationCompletedOn', CStrings::strTrimDef( $strValidationCompletedOn, -1, NULL, true ) );
	}

	public function getValidationCompletedOn() {
		return $this->m_strValidationCompletedOn;
	}

	public function sqlValidationCompletedOn() {
		return ( true == isset( $this->m_strValidationCompletedOn ) ) ? '\'' . $this->m_strValidationCompletedOn . '\'' : 'NULL';
	}

	public function setProcessedBy( $intProcessedBy ) {
		$this->set( 'm_intProcessedBy', CStrings::strToIntDef( $intProcessedBy, NULL, false ) );
	}

	public function getProcessedBy() {
		return $this->m_intProcessedBy;
	}

	public function sqlProcessedBy() {
		return ( true == isset( $this->m_intProcessedBy ) ) ? ( string ) $this->m_intProcessedBy : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, account_id, transaction_id, utility_type_id, utility_bill_id, utility_account_id, utility_invoice_id, utility_transaction_id, utility_batch_id, request_datetime, deadline_datetime, notes, move_date, is_exiting, is_violation, has_high_usage, satisfied_on, validated_by, validation_completed_on, processed_by, processed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlTransactionId() . ', ' .
 						$this->sqlUtilityTypeId() . ', ' .
 						$this->sqlUtilityBillId() . ', ' .
 						$this->sqlUtilityAccountId() . ', ' .
 						$this->sqlUtilityInvoiceId() . ', ' .
 						$this->sqlUtilityTransactionId() . ', ' .
 						$this->sqlUtilityBatchId() . ', ' .
 						$this->sqlRequestDatetime() . ', ' .
 						$this->sqlDeadlineDatetime() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlMoveDate() . ', ' .
 						$this->sqlIsExiting() . ', ' .
 						$this->sqlIsViolation() . ', ' .
 						$this->sqlHasHighUsage() . ', ' .
 						$this->sqlSatisfiedOn() . ', ' .
 						$this->sqlValidatedBy() . ', ' .
 						$this->sqlValidationCompletedOn() . ', ' .
 						$this->sqlProcessedBy() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; } elseif( true == array_key_exists( 'UtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_invoice_id = ' . $this->sqlUtilityInvoiceId() . ','; } elseif( true == array_key_exists( 'UtilityInvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' utility_invoice_id = ' . $this->sqlUtilityInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_transaction_id = ' . $this->sqlUtilityTransactionId() . ','; } elseif( true == array_key_exists( 'UtilityTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_transaction_id = ' . $this->sqlUtilityTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; } elseif( true == array_key_exists( 'RequestDatetime', $this->getChangedColumns() ) ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deadline_datetime = ' . $this->sqlDeadlineDatetime() . ','; } elseif( true == array_key_exists( 'DeadlineDatetime', $this->getChangedColumns() ) ) { $strSql .= ' deadline_datetime = ' . $this->sqlDeadlineDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_date = ' . $this->sqlMoveDate() . ','; } elseif( true == array_key_exists( 'MoveDate', $this->getChangedColumns() ) ) { $strSql .= ' move_date = ' . $this->sqlMoveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_exiting = ' . $this->sqlIsExiting() . ','; } elseif( true == array_key_exists( 'IsExiting', $this->getChangedColumns() ) ) { $strSql .= ' is_exiting = ' . $this->sqlIsExiting() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_violation = ' . $this->sqlIsViolation() . ','; } elseif( true == array_key_exists( 'IsViolation', $this->getChangedColumns() ) ) { $strSql .= ' is_violation = ' . $this->sqlIsViolation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_high_usage = ' . $this->sqlHasHighUsage() . ','; } elseif( true == array_key_exists( 'HasHighUsage', $this->getChangedColumns() ) ) { $strSql .= ' has_high_usage = ' . $this->sqlHasHighUsage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' satisfied_on = ' . $this->sqlSatisfiedOn() . ','; } elseif( true == array_key_exists( 'SatisfiedOn', $this->getChangedColumns() ) ) { $strSql .= ' satisfied_on = ' . $this->sqlSatisfiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' validated_by = ' . $this->sqlValidatedBy() . ','; } elseif( true == array_key_exists( 'ValidatedBy', $this->getChangedColumns() ) ) { $strSql .= ' validated_by = ' . $this->sqlValidatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' validation_completed_on = ' . $this->sqlValidationCompletedOn() . ','; } elseif( true == array_key_exists( 'ValidationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' validation_completed_on = ' . $this->sqlValidationCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy() . ','; } elseif( true == array_key_exists( 'ProcessedBy', $this->getChangedColumns() ) ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'account_id' => $this->getAccountId(),
			'transaction_id' => $this->getTransactionId(),
			'utility_type_id' => $this->getUtilityTypeId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'utility_invoice_id' => $this->getUtilityInvoiceId(),
			'utility_transaction_id' => $this->getUtilityTransactionId(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'request_datetime' => $this->getRequestDatetime(),
			'deadline_datetime' => $this->getDeadlineDatetime(),
			'notes' => $this->getNotes(),
			'move_date' => $this->getMoveDate(),
			'is_exiting' => $this->getIsExiting(),
			'is_violation' => $this->getIsViolation(),
			'has_high_usage' => $this->getHasHighUsage(),
			'satisfied_on' => $this->getSatisfiedOn(),
			'validated_by' => $this->getValidatedBy(),
			'validation_completed_on' => $this->getValidationCompletedOn(),
			'processed_by' => $this->getProcessedBy(),
			'processed_on' => $this->getProcessedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>