<?php

class CBaseVendorCatalogBuyerLocation extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.vendor_catalog_buyer_locations';

	protected $m_intId;
	protected $m_intVendorId;
	protected $m_intBuyerId;
	protected $m_intVendorCatalogId;
	protected $m_intBuyerLocationId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['buyer_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerId', trim( $arrValues['buyer_id'] ) ); elseif( isset( $arrValues['buyer_id'] ) ) $this->setBuyerId( $arrValues['buyer_id'] );
		if( isset( $arrValues['vendor_catalog_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorCatalogId', trim( $arrValues['vendor_catalog_id'] ) ); elseif( isset( $arrValues['vendor_catalog_id'] ) ) $this->setVendorCatalogId( $arrValues['vendor_catalog_id'] );
		if( isset( $arrValues['buyer_location_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerLocationId', trim( $arrValues['buyer_location_id'] ) ); elseif( isset( $arrValues['buyer_location_id'] ) ) $this->setBuyerLocationId( $arrValues['buyer_location_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setBuyerId( $intBuyerId ) {
		$this->set( 'm_intBuyerId', CStrings::strToIntDef( $intBuyerId, NULL, false ) );
	}

	public function getBuyerId() {
		return $this->m_intBuyerId;
	}

	public function sqlBuyerId() {
		return ( true == isset( $this->m_intBuyerId ) ) ? ( string ) $this->m_intBuyerId : 'NULL';
	}

	public function setVendorCatalogId( $intVendorCatalogId ) {
		$this->set( 'm_intVendorCatalogId', CStrings::strToIntDef( $intVendorCatalogId, NULL, false ) );
	}

	public function getVendorCatalogId() {
		return $this->m_intVendorCatalogId;
	}

	public function sqlVendorCatalogId() {
		return ( true == isset( $this->m_intVendorCatalogId ) ) ? ( string ) $this->m_intVendorCatalogId : 'NULL';
	}

	public function setBuyerLocationId( $intBuyerLocationId ) {
		$this->set( 'm_intBuyerLocationId', CStrings::strToIntDef( $intBuyerLocationId, NULL, false ) );
	}

	public function getBuyerLocationId() {
		return $this->m_intBuyerLocationId;
	}

	public function sqlBuyerLocationId() {
		return ( true == isset( $this->m_intBuyerLocationId ) ) ? ( string ) $this->m_intBuyerLocationId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_id, buyer_id, vendor_catalog_id, buyer_location_id, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlVendorId() . ', ' .
						$this->sqlBuyerId() . ', ' .
						$this->sqlVendorCatalogId() . ', ' .
						$this->sqlBuyerLocationId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId(). ',' ; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_id = ' . $this->sqlBuyerId(). ',' ; } elseif( true == array_key_exists( 'BuyerId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_id = ' . $this->sqlBuyerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_catalog_id = ' . $this->sqlVendorCatalogId(). ',' ; } elseif( true == array_key_exists( 'VendorCatalogId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_catalog_id = ' . $this->sqlVendorCatalogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_location_id = ' . $this->sqlBuyerLocationId(). ',' ; } elseif( true == array_key_exists( 'BuyerLocationId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_location_id = ' . $this->sqlBuyerLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_id' => $this->getVendorId(),
			'buyer_id' => $this->getBuyerId(),
			'vendor_catalog_id' => $this->getVendorCatalogId(),
			'buyer_location_id' => $this->getBuyerLocationId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>