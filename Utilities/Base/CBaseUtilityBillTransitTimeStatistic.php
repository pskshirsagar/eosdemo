<?php

class CBaseUtilityBillTransitTimeStatistic extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_transit_time_statistics';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityProviderId;
	protected $m_intUtilityBillReceiptTypeId;
	protected $m_fltAverageTransitTime;
	protected $m_fltTransitTimeStandardDeviation;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltAverageTransitTime = '0';
		$this->m_fltTransitTimeStandardDeviation = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityProviderId', trim( $arrValues['utility_provider_id'] ) ); elseif( isset( $arrValues['utility_provider_id'] ) ) $this->setUtilityProviderId( $arrValues['utility_provider_id'] );
		if( isset( $arrValues['utility_bill_receipt_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillReceiptTypeId', trim( $arrValues['utility_bill_receipt_type_id'] ) ); elseif( isset( $arrValues['utility_bill_receipt_type_id'] ) ) $this->setUtilityBillReceiptTypeId( $arrValues['utility_bill_receipt_type_id'] );
		if( isset( $arrValues['average_transit_time'] ) && $boolDirectSet ) $this->set( 'm_fltAverageTransitTime', trim( $arrValues['average_transit_time'] ) ); elseif( isset( $arrValues['average_transit_time'] ) ) $this->setAverageTransitTime( $arrValues['average_transit_time'] );
		if( isset( $arrValues['transit_time_standard_deviation'] ) && $boolDirectSet ) $this->set( 'm_fltTransitTimeStandardDeviation', trim( $arrValues['transit_time_standard_deviation'] ) ); elseif( isset( $arrValues['transit_time_standard_deviation'] ) ) $this->setTransitTimeStandardDeviation( $arrValues['transit_time_standard_deviation'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityProviderId( $intUtilityProviderId ) {
		$this->set( 'm_intUtilityProviderId', CStrings::strToIntDef( $intUtilityProviderId, NULL, false ) );
	}

	public function getUtilityProviderId() {
		return $this->m_intUtilityProviderId;
	}

	public function sqlUtilityProviderId() {
		return ( true == isset( $this->m_intUtilityProviderId ) ) ? ( string ) $this->m_intUtilityProviderId : 'NULL';
	}

	public function setUtilityBillReceiptTypeId( $intUtilityBillReceiptTypeId ) {
		$this->set( 'm_intUtilityBillReceiptTypeId', CStrings::strToIntDef( $intUtilityBillReceiptTypeId, NULL, false ) );
	}

	public function getUtilityBillReceiptTypeId() {
		return $this->m_intUtilityBillReceiptTypeId;
	}

	public function sqlUtilityBillReceiptTypeId() {
		return ( true == isset( $this->m_intUtilityBillReceiptTypeId ) ) ? ( string ) $this->m_intUtilityBillReceiptTypeId : 'NULL';
	}

	public function setAverageTransitTime( $fltAverageTransitTime ) {
		$this->set( 'm_fltAverageTransitTime', CStrings::strToFloatDef( $fltAverageTransitTime, NULL, false, 2 ) );
	}

	public function getAverageTransitTime() {
		return $this->m_fltAverageTransitTime;
	}

	public function sqlAverageTransitTime() {
		return ( true == isset( $this->m_fltAverageTransitTime ) ) ? ( string ) $this->m_fltAverageTransitTime : '0';
	}

	public function setTransitTimeStandardDeviation( $fltTransitTimeStandardDeviation ) {
		$this->set( 'm_fltTransitTimeStandardDeviation', CStrings::strToFloatDef( $fltTransitTimeStandardDeviation, NULL, false, 2 ) );
	}

	public function getTransitTimeStandardDeviation() {
		return $this->m_fltTransitTimeStandardDeviation;
	}

	public function sqlTransitTimeStandardDeviation() {
		return ( true == isset( $this->m_fltTransitTimeStandardDeviation ) ) ? ( string ) $this->m_fltTransitTimeStandardDeviation : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_provider_id, utility_bill_receipt_type_id, average_transit_time, transit_time_standard_deviation, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityProviderId() . ', ' .
						$this->sqlUtilityBillReceiptTypeId() . ', ' .
						$this->sqlAverageTransitTime() . ', ' .
						$this->sqlTransitTimeStandardDeviation() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId(). ',' ; } elseif( true == array_key_exists( 'UtilityProviderId', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_receipt_type_id = ' . $this->sqlUtilityBillReceiptTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillReceiptTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_receipt_type_id = ' . $this->sqlUtilityBillReceiptTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_transit_time = ' . $this->sqlAverageTransitTime(). ',' ; } elseif( true == array_key_exists( 'AverageTransitTime', $this->getChangedColumns() ) ) { $strSql .= ' average_transit_time = ' . $this->sqlAverageTransitTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transit_time_standard_deviation = ' . $this->sqlTransitTimeStandardDeviation(). ',' ; } elseif( true == array_key_exists( 'TransitTimeStandardDeviation', $this->getChangedColumns() ) ) { $strSql .= ' transit_time_standard_deviation = ' . $this->sqlTransitTimeStandardDeviation() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_provider_id' => $this->getUtilityProviderId(),
			'utility_bill_receipt_type_id' => $this->getUtilityBillReceiptTypeId(),
			'average_transit_time' => $this->getAverageTransitTime(),
			'transit_time_standard_deviation' => $this->getTransitTimeStandardDeviation(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>