<?php

class CBaseUtilityBillAccountMeterTemplateCharge extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_account_meter_template_charges';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillAccountMeterId;
	protected $m_intUtilityBillAccountTemplateChargeId;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_account_meter_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountMeterId', trim( $arrValues['utility_bill_account_meter_id'] ) ); elseif( isset( $arrValues['utility_bill_account_meter_id'] ) ) $this->setUtilityBillAccountMeterId( $arrValues['utility_bill_account_meter_id'] );
		if( isset( $arrValues['utility_bill_account_template_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountTemplateChargeId', trim( $arrValues['utility_bill_account_template_charge_id'] ) ); elseif( isset( $arrValues['utility_bill_account_template_charge_id'] ) ) $this->setUtilityBillAccountTemplateChargeId( $arrValues['utility_bill_account_template_charge_id'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillAccountMeterId( $intUtilityBillAccountMeterId ) {
		$this->set( 'm_intUtilityBillAccountMeterId', CStrings::strToIntDef( $intUtilityBillAccountMeterId, NULL, false ) );
	}

	public function getUtilityBillAccountMeterId() {
		return $this->m_intUtilityBillAccountMeterId;
	}

	public function sqlUtilityBillAccountMeterId() {
		return ( true == isset( $this->m_intUtilityBillAccountMeterId ) ) ? ( string ) $this->m_intUtilityBillAccountMeterId : 'NULL';
	}

	public function setUtilityBillAccountTemplateChargeId( $intUtilityBillAccountTemplateChargeId ) {
		$this->set( 'm_intUtilityBillAccountTemplateChargeId', CStrings::strToIntDef( $intUtilityBillAccountTemplateChargeId, NULL, false ) );
	}

	public function getUtilityBillAccountTemplateChargeId() {
		return $this->m_intUtilityBillAccountTemplateChargeId;
	}

	public function sqlUtilityBillAccountTemplateChargeId() {
		return ( true == isset( $this->m_intUtilityBillAccountTemplateChargeId ) ) ? ( string ) $this->m_intUtilityBillAccountTemplateChargeId : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_account_meter_id, utility_bill_account_template_charge_id, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUtilityBillAccountMeterId() . ', ' .
 						$this->sqlUtilityBillAccountTemplateChargeId() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_meter_id = ' . $this->sqlUtilityBillAccountMeterId() . ','; } elseif( true == array_key_exists( 'UtilityBillAccountMeterId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_meter_id = ' . $this->sqlUtilityBillAccountMeterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_template_charge_id = ' . $this->sqlUtilityBillAccountTemplateChargeId() . ','; } elseif( true == array_key_exists( 'UtilityBillAccountTemplateChargeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_template_charge_id = ' . $this->sqlUtilityBillAccountTemplateChargeId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_account_meter_id' => $this->getUtilityBillAccountMeterId(),
			'utility_bill_account_template_charge_id' => $this->getUtilityBillAccountTemplateChargeId(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>