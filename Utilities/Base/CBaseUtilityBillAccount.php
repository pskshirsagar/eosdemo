<?php

class CBaseUtilityBillAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillAccountDetailId;
	protected $m_intUtilityBillTypeId;
	protected $m_intUtilityBillTemplateId;
	protected $m_intRemittanceCompanyMerchantAccountId;
	protected $m_intLastUtilityBillId;
	protected $m_intApPayeeId;
	protected $m_intApPayeeLocationId;
	protected $m_intNewUtilityBillAccountId;
	protected $m_intSummaryUtilityBillAccountId;
	protected $m_intFrequencyId;
	protected $m_strName;
	protected $m_intAllocationCount;
	protected $m_strAccountDatetime;
	protected $m_strAccountNumber;
	protected $m_strServiceStreetLine1;
	protected $m_strServiceStreetLine2;
	protected $m_strServiceCity;
	protected $m_strServiceStateCode;
	protected $m_strServicePostalCode;
	protected $m_strNotes;
	protected $m_intRequiredBillsInPeriod;
	protected $m_boolRequireNoteAction;
	protected $m_boolIsIrregular;
	protected $m_boolIsTemplate;
	protected $m_boolIsIgnoreMemorization;
	protected $m_boolIsInactive;
	protected $m_boolIsSkipDataEntry;
	protected $m_boolIsSkipUemAudits;
	protected $m_boolIsNeverEstimate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsCreditAvailable;
	protected $m_boolIsSuggested;
	protected $m_boolDisableUem;
	protected $m_arrintTemplateSummaryUtilityBillAccountIds;
	protected $m_intCopyUtilityBillAccountId;
	protected $m_boolIsBudgetAccount;

	public function __construct() {
		parent::__construct();

		$this->m_intAllocationCount = '1';
		$this->m_intRequiredBillsInPeriod = '0';
		$this->m_boolRequireNoteAction = false;
		$this->m_boolIsIrregular = false;
		$this->m_boolIsTemplate = false;
		$this->m_boolIsIgnoreMemorization = false;
		$this->m_boolIsInactive = false;
		$this->m_boolIsSkipDataEntry = false;
		$this->m_boolIsSkipUemAudits = false;
		$this->m_boolIsNeverEstimate = false;
		$this->m_boolIsCreditAvailable = false;
		$this->m_boolIsSuggested = false;
		$this->m_boolDisableUem = false;
		$this->m_boolIsBudgetAccount = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_account_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountDetailId', trim( $arrValues['utility_bill_account_detail_id'] ) ); elseif( isset( $arrValues['utility_bill_account_detail_id'] ) ) $this->setUtilityBillAccountDetailId( $arrValues['utility_bill_account_detail_id'] );
		if( isset( $arrValues['utility_bill_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillTypeId', trim( $arrValues['utility_bill_type_id'] ) ); elseif( isset( $arrValues['utility_bill_type_id'] ) ) $this->setUtilityBillTypeId( $arrValues['utility_bill_type_id'] );
		if( isset( $arrValues['utility_bill_template_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillTemplateId', trim( $arrValues['utility_bill_template_id'] ) ); elseif( isset( $arrValues['utility_bill_template_id'] ) ) $this->setUtilityBillTemplateId( $arrValues['utility_bill_template_id'] );
		if( isset( $arrValues['remittance_company_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRemittanceCompanyMerchantAccountId', trim( $arrValues['remittance_company_merchant_account_id'] ) ); elseif( isset( $arrValues['remittance_company_merchant_account_id'] ) ) $this->setRemittanceCompanyMerchantAccountId( $arrValues['remittance_company_merchant_account_id'] );
		if( isset( $arrValues['last_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intLastUtilityBillId', trim( $arrValues['last_utility_bill_id'] ) ); elseif( isset( $arrValues['last_utility_bill_id'] ) ) $this->setLastUtilityBillId( $arrValues['last_utility_bill_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['new_utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intNewUtilityBillAccountId', trim( $arrValues['new_utility_bill_account_id'] ) ); elseif( isset( $arrValues['new_utility_bill_account_id'] ) ) $this->setNewUtilityBillAccountId( $arrValues['new_utility_bill_account_id'] );
		if( isset( $arrValues['summary_utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intSummaryUtilityBillAccountId', trim( $arrValues['summary_utility_bill_account_id'] ) ); elseif( isset( $arrValues['summary_utility_bill_account_id'] ) ) $this->setSummaryUtilityBillAccountId( $arrValues['summary_utility_bill_account_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['allocation_count'] ) && $boolDirectSet ) $this->set( 'm_intAllocationCount', trim( $arrValues['allocation_count'] ) ); elseif( isset( $arrValues['allocation_count'] ) ) $this->setAllocationCount( $arrValues['allocation_count'] );
		if( isset( $arrValues['account_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAccountDatetime', trim( $arrValues['account_datetime'] ) ); elseif( isset( $arrValues['account_datetime'] ) ) $this->setAccountDatetime( $arrValues['account_datetime'] );
		if( isset( $arrValues['account_number'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumber', trim( $arrValues['account_number'] ) ); elseif( isset( $arrValues['account_number'] ) ) $this->setAccountNumber( $arrValues['account_number'] );
		if( isset( $arrValues['service_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strServiceStreetLine1', trim( $arrValues['service_street_line1'] ) ); elseif( isset( $arrValues['service_street_line1'] ) ) $this->setServiceStreetLine1( $arrValues['service_street_line1'] );
		if( isset( $arrValues['service_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strServiceStreetLine2', trim( $arrValues['service_street_line2'] ) ); elseif( isset( $arrValues['service_street_line2'] ) ) $this->setServiceStreetLine2( $arrValues['service_street_line2'] );
		if( isset( $arrValues['service_city'] ) && $boolDirectSet ) $this->set( 'm_strServiceCity', trim( $arrValues['service_city'] ) ); elseif( isset( $arrValues['service_city'] ) ) $this->setServiceCity( $arrValues['service_city'] );
		if( isset( $arrValues['service_state_code'] ) && $boolDirectSet ) $this->set( 'm_strServiceStateCode', trim( $arrValues['service_state_code'] ) ); elseif( isset( $arrValues['service_state_code'] ) ) $this->setServiceStateCode( $arrValues['service_state_code'] );
		if( isset( $arrValues['service_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strServicePostalCode', trim( $arrValues['service_postal_code'] ) ); elseif( isset( $arrValues['service_postal_code'] ) ) $this->setServicePostalCode( $arrValues['service_postal_code'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['required_bills_in_period'] ) && $boolDirectSet ) $this->set( 'm_intRequiredBillsInPeriod', trim( $arrValues['required_bills_in_period'] ) ); elseif( isset( $arrValues['required_bills_in_period'] ) ) $this->setRequiredBillsInPeriod( $arrValues['required_bills_in_period'] );
		if( isset( $arrValues['require_note_action'] ) && $boolDirectSet ) $this->set( 'm_boolRequireNoteAction', trim( stripcslashes( $arrValues['require_note_action'] ) ) ); elseif( isset( $arrValues['require_note_action'] ) ) $this->setRequireNoteAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_note_action'] ) : $arrValues['require_note_action'] );
		if( isset( $arrValues['is_irregular'] ) && $boolDirectSet ) $this->set( 'm_boolIsIrregular', trim( stripcslashes( $arrValues['is_irregular'] ) ) ); elseif( isset( $arrValues['is_irregular'] ) ) $this->setIsIrregular( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_irregular'] ) : $arrValues['is_irregular'] );
		if( isset( $arrValues['is_template'] ) && $boolDirectSet ) $this->set( 'm_boolIsTemplate', trim( stripcslashes( $arrValues['is_template'] ) ) ); elseif( isset( $arrValues['is_template'] ) ) $this->setIsTemplate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_template'] ) : $arrValues['is_template'] );
		if( isset( $arrValues['is_ignore_memorization'] ) && $boolDirectSet ) $this->set( 'm_boolIsIgnoreMemorization', trim( stripcslashes( $arrValues['is_ignore_memorization'] ) ) ); elseif( isset( $arrValues['is_ignore_memorization'] ) ) $this->setIsIgnoreMemorization( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ignore_memorization'] ) : $arrValues['is_ignore_memorization'] );
		if( isset( $arrValues['is_inactive'] ) && $boolDirectSet ) $this->set( 'm_boolIsInactive', trim( stripcslashes( $arrValues['is_inactive'] ) ) ); elseif( isset( $arrValues['is_inactive'] ) ) $this->setIsInactive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_inactive'] ) : $arrValues['is_inactive'] );
		if( isset( $arrValues['is_skip_data_entry'] ) && $boolDirectSet ) $this->set( 'm_boolIsSkipDataEntry', trim( stripcslashes( $arrValues['is_skip_data_entry'] ) ) ); elseif( isset( $arrValues['is_skip_data_entry'] ) ) $this->setIsSkipDataEntry( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_skip_data_entry'] ) : $arrValues['is_skip_data_entry'] );
		if( isset( $arrValues['is_skip_uem_audits'] ) && $boolDirectSet ) $this->set( 'm_boolIsSkipUemAudits', trim( stripcslashes( $arrValues['is_skip_uem_audits'] ) ) ); elseif( isset( $arrValues['is_skip_uem_audits'] ) ) $this->setIsSkipUemAudits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_skip_uem_audits'] ) : $arrValues['is_skip_uem_audits'] );
		if( isset( $arrValues['is_never_estimate'] ) && $boolDirectSet ) $this->set( 'm_boolIsNeverEstimate', trim( stripcslashes( $arrValues['is_never_estimate'] ) ) ); elseif( isset( $arrValues['is_never_estimate'] ) ) $this->setIsNeverEstimate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_never_estimate'] ) : $arrValues['is_never_estimate'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_credit_available'] ) && $boolDirectSet ) $this->set( 'm_boolIsCreditAvailable', trim( stripcslashes( $arrValues['is_credit_available'] ) ) ); elseif( isset( $arrValues['is_credit_available'] ) ) $this->setIsCreditAvailable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_credit_available'] ) : $arrValues['is_credit_available'] );
		if( isset( $arrValues['is_suggested'] ) && $boolDirectSet ) $this->set( 'm_boolIsSuggested', trim( stripcslashes( $arrValues['is_suggested'] ) ) ); elseif( isset( $arrValues['is_suggested'] ) ) $this->setIsSuggested( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_suggested'] ) : $arrValues['is_suggested'] );
		if( isset( $arrValues['disable_uem'] ) && $boolDirectSet ) $this->set( 'm_boolDisableUem', trim( stripcslashes( $arrValues['disable_uem'] ) ) ); elseif( isset( $arrValues['disable_uem'] ) ) $this->setDisableUem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['disable_uem'] ) : $arrValues['disable_uem'] );
		if( isset( $arrValues['template_summary_utility_bill_account_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintTemplateSummaryUtilityBillAccountIds', trim( $arrValues['template_summary_utility_bill_account_ids'] ) ); elseif( isset( $arrValues['template_summary_utility_bill_account_ids'] ) ) $this->setTemplateSummaryUtilityBillAccountIds( $arrValues['template_summary_utility_bill_account_ids'] );
		if( isset( $arrValues['copy_utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCopyUtilityBillAccountId', trim( $arrValues['copy_utility_bill_account_id'] ) ); elseif( isset( $arrValues['copy_utility_bill_account_id'] ) ) $this->setCopyUtilityBillAccountId( $arrValues['copy_utility_bill_account_id'] );
		if( isset( $arrValues['is_budget_account'] ) && $boolDirectSet ) $this->set( 'm_boolIsBudgetAccount', trim( stripcslashes( $arrValues['is_budget_account'] ) ) ); elseif( isset( $arrValues['is_budget_account'] ) ) $this->setIsBudgetAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_budget_account'] ) : $arrValues['is_budget_account'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillAccountDetailId( $intUtilityBillAccountDetailId ) {
		$this->set( 'm_intUtilityBillAccountDetailId', CStrings::strToIntDef( $intUtilityBillAccountDetailId, NULL, false ) );
	}

	public function getUtilityBillAccountDetailId() {
		return $this->m_intUtilityBillAccountDetailId;
	}

	public function sqlUtilityBillAccountDetailId() {
		return ( true == isset( $this->m_intUtilityBillAccountDetailId ) ) ? ( string ) $this->m_intUtilityBillAccountDetailId : 'NULL';
	}

	public function setUtilityBillTypeId( $intUtilityBillTypeId ) {
		$this->set( 'm_intUtilityBillTypeId', CStrings::strToIntDef( $intUtilityBillTypeId, NULL, false ) );
	}

	public function getUtilityBillTypeId() {
		return $this->m_intUtilityBillTypeId;
	}

	public function sqlUtilityBillTypeId() {
		return ( true == isset( $this->m_intUtilityBillTypeId ) ) ? ( string ) $this->m_intUtilityBillTypeId : 'NULL';
	}

	public function setUtilityBillTemplateId( $intUtilityBillTemplateId ) {
		$this->set( 'm_intUtilityBillTemplateId', CStrings::strToIntDef( $intUtilityBillTemplateId, NULL, false ) );
	}

	public function getUtilityBillTemplateId() {
		return $this->m_intUtilityBillTemplateId;
	}

	public function sqlUtilityBillTemplateId() {
		return ( true == isset( $this->m_intUtilityBillTemplateId ) ) ? ( string ) $this->m_intUtilityBillTemplateId : 'NULL';
	}

	public function setRemittanceCompanyMerchantAccountId( $intRemittanceCompanyMerchantAccountId ) {
		$this->set( 'm_intRemittanceCompanyMerchantAccountId', CStrings::strToIntDef( $intRemittanceCompanyMerchantAccountId, NULL, false ) );
	}

	public function getRemittanceCompanyMerchantAccountId() {
		return $this->m_intRemittanceCompanyMerchantAccountId;
	}

	public function sqlRemittanceCompanyMerchantAccountId() {
		return ( true == isset( $this->m_intRemittanceCompanyMerchantAccountId ) ) ? ( string ) $this->m_intRemittanceCompanyMerchantAccountId : 'NULL';
	}

	public function setLastUtilityBillId( $intLastUtilityBillId ) {
		$this->set( 'm_intLastUtilityBillId', CStrings::strToIntDef( $intLastUtilityBillId, NULL, false ) );
	}

	public function getLastUtilityBillId() {
		return $this->m_intLastUtilityBillId;
	}

	public function sqlLastUtilityBillId() {
		return ( true == isset( $this->m_intLastUtilityBillId ) ) ? ( string ) $this->m_intLastUtilityBillId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setNewUtilityBillAccountId( $intNewUtilityBillAccountId ) {
		$this->set( 'm_intNewUtilityBillAccountId', CStrings::strToIntDef( $intNewUtilityBillAccountId, NULL, false ) );
	}

	public function getNewUtilityBillAccountId() {
		return $this->m_intNewUtilityBillAccountId;
	}

	public function sqlNewUtilityBillAccountId() {
		return ( true == isset( $this->m_intNewUtilityBillAccountId ) ) ? ( string ) $this->m_intNewUtilityBillAccountId : 'NULL';
	}

	public function setSummaryUtilityBillAccountId( $intSummaryUtilityBillAccountId ) {
		$this->set( 'm_intSummaryUtilityBillAccountId', CStrings::strToIntDef( $intSummaryUtilityBillAccountId, NULL, false ) );
	}

	public function getSummaryUtilityBillAccountId() {
		return $this->m_intSummaryUtilityBillAccountId;
	}

	public function sqlSummaryUtilityBillAccountId() {
		return ( true == isset( $this->m_intSummaryUtilityBillAccountId ) ) ? ( string ) $this->m_intSummaryUtilityBillAccountId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setAllocationCount( $intAllocationCount ) {
		$this->set( 'm_intAllocationCount', CStrings::strToIntDef( $intAllocationCount, NULL, false ) );
	}

	public function getAllocationCount() {
		return $this->m_intAllocationCount;
	}

	public function sqlAllocationCount() {
		return ( true == isset( $this->m_intAllocationCount ) ) ? ( string ) $this->m_intAllocationCount : '1';
	}

	public function setAccountDatetime( $strAccountDatetime ) {
		$this->set( 'm_strAccountDatetime', CStrings::strTrimDef( $strAccountDatetime, -1, NULL, true ) );
	}

	public function getAccountDatetime() {
		return $this->m_strAccountDatetime;
	}

	public function sqlAccountDatetime() {
		return ( true == isset( $this->m_strAccountDatetime ) ) ? '\'' . $this->m_strAccountDatetime . '\'' : 'NOW()';
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 240, NULL, true ) );
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function sqlAccountNumber() {
		return ( true == isset( $this->m_strAccountNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountNumber ) : '\'' . addslashes( $this->m_strAccountNumber ) . '\'' ) : 'NULL';
	}

	public function setServiceStreetLine1( $strServiceStreetLine1 ) {
		$this->set( 'm_strServiceStreetLine1', CStrings::strTrimDef( $strServiceStreetLine1, 50, NULL, true ) );
	}

	public function getServiceStreetLine1() {
		return $this->m_strServiceStreetLine1;
	}

	public function sqlServiceStreetLine1() {
		return ( true == isset( $this->m_strServiceStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceStreetLine1 ) : '\'' . addslashes( $this->m_strServiceStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setServiceStreetLine2( $strServiceStreetLine2 ) {
		$this->set( 'm_strServiceStreetLine2', CStrings::strTrimDef( $strServiceStreetLine2, 50, NULL, true ) );
	}

	public function getServiceStreetLine2() {
		return $this->m_strServiceStreetLine2;
	}

	public function sqlServiceStreetLine2() {
		return ( true == isset( $this->m_strServiceStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceStreetLine2 ) : '\'' . addslashes( $this->m_strServiceStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setServiceCity( $strServiceCity ) {
		$this->set( 'm_strServiceCity', CStrings::strTrimDef( $strServiceCity, 50, NULL, true ) );
	}

	public function getServiceCity() {
		return $this->m_strServiceCity;
	}

	public function sqlServiceCity() {
		return ( true == isset( $this->m_strServiceCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceCity ) : '\'' . addslashes( $this->m_strServiceCity ) . '\'' ) : 'NULL';
	}

	public function setServiceStateCode( $strServiceStateCode ) {
		$this->set( 'm_strServiceStateCode', CStrings::strTrimDef( $strServiceStateCode, 2, NULL, true ) );
	}

	public function getServiceStateCode() {
		return $this->m_strServiceStateCode;
	}

	public function sqlServiceStateCode() {
		return ( true == isset( $this->m_strServiceStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceStateCode ) : '\'' . addslashes( $this->m_strServiceStateCode ) . '\'' ) : 'NULL';
	}

	public function setServicePostalCode( $strServicePostalCode ) {
		$this->set( 'm_strServicePostalCode', CStrings::strTrimDef( $strServicePostalCode, 5, NULL, true ) );
	}

	public function getServicePostalCode() {
		return $this->m_strServicePostalCode;
	}

	public function sqlServicePostalCode() {
		return ( true == isset( $this->m_strServicePostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServicePostalCode ) : '\'' . addslashes( $this->m_strServicePostalCode ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setRequiredBillsInPeriod( $intRequiredBillsInPeriod ) {
		$this->set( 'm_intRequiredBillsInPeriod', CStrings::strToIntDef( $intRequiredBillsInPeriod, NULL, false ) );
	}

	public function getRequiredBillsInPeriod() {
		return $this->m_intRequiredBillsInPeriod;
	}

	public function sqlRequiredBillsInPeriod() {
		return ( true == isset( $this->m_intRequiredBillsInPeriod ) ) ? ( string ) $this->m_intRequiredBillsInPeriod : '0';
	}

	public function setRequireNoteAction( $boolRequireNoteAction ) {
		$this->set( 'm_boolRequireNoteAction', CStrings::strToBool( $boolRequireNoteAction ) );
	}

	public function getRequireNoteAction() {
		return $this->m_boolRequireNoteAction;
	}

	public function sqlRequireNoteAction() {
		return ( true == isset( $this->m_boolRequireNoteAction ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireNoteAction ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsIrregular( $boolIsIrregular ) {
		$this->set( 'm_boolIsIrregular', CStrings::strToBool( $boolIsIrregular ) );
	}

	public function getIsIrregular() {
		return $this->m_boolIsIrregular;
	}

	public function sqlIsIrregular() {
		return ( true == isset( $this->m_boolIsIrregular ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIrregular ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTemplate( $boolIsTemplate ) {
		$this->set( 'm_boolIsTemplate', CStrings::strToBool( $boolIsTemplate ) );
	}

	public function getIsTemplate() {
		return $this->m_boolIsTemplate;
	}

	public function sqlIsTemplate() {
		return ( true == isset( $this->m_boolIsTemplate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTemplate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsIgnoreMemorization( $boolIsIgnoreMemorization ) {
		$this->set( 'm_boolIsIgnoreMemorization', CStrings::strToBool( $boolIsIgnoreMemorization ) );
	}

	public function getIsIgnoreMemorization() {
		return $this->m_boolIsIgnoreMemorization;
	}

	public function sqlIsIgnoreMemorization() {
		return ( true == isset( $this->m_boolIsIgnoreMemorization ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIgnoreMemorization ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInactive( $boolIsInactive ) {
		$this->set( 'm_boolIsInactive', CStrings::strToBool( $boolIsInactive ) );
	}

	public function getIsInactive() {
		return $this->m_boolIsInactive;
	}

	public function sqlIsInactive() {
		return ( true == isset( $this->m_boolIsInactive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInactive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSkipDataEntry( $boolIsSkipDataEntry ) {
		$this->set( 'm_boolIsSkipDataEntry', CStrings::strToBool( $boolIsSkipDataEntry ) );
	}

	public function getIsSkipDataEntry() {
		return $this->m_boolIsSkipDataEntry;
	}

	public function sqlIsSkipDataEntry() {
		return ( true == isset( $this->m_boolIsSkipDataEntry ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSkipDataEntry ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSkipUemAudits( $boolIsSkipUemAudits ) {
		$this->set( 'm_boolIsSkipUemAudits', CStrings::strToBool( $boolIsSkipUemAudits ) );
	}

	public function getIsSkipUemAudits() {
		return $this->m_boolIsSkipUemAudits;
	}

	public function sqlIsSkipUemAudits() {
		return ( true == isset( $this->m_boolIsSkipUemAudits ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSkipUemAudits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsNeverEstimate( $boolIsNeverEstimate ) {
		$this->set( 'm_boolIsNeverEstimate', CStrings::strToBool( $boolIsNeverEstimate ) );
	}

	public function getIsNeverEstimate() {
		return $this->m_boolIsNeverEstimate;
	}

	public function sqlIsNeverEstimate() {
		return ( true == isset( $this->m_boolIsNeverEstimate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNeverEstimate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsCreditAvailable( $boolIsCreditAvailable ) {
		$this->set( 'm_boolIsCreditAvailable', CStrings::strToBool( $boolIsCreditAvailable ) );
	}

	public function getIsCreditAvailable() {
		return $this->m_boolIsCreditAvailable;
	}

	public function sqlIsCreditAvailable() {
		return ( true == isset( $this->m_boolIsCreditAvailable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCreditAvailable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSuggested( $boolIsSuggested ) {
		$this->set( 'm_boolIsSuggested', CStrings::strToBool( $boolIsSuggested ) );
	}

	public function getIsSuggested() {
		return $this->m_boolIsSuggested;
	}

	public function sqlIsSuggested() {
		return ( true == isset( $this->m_boolIsSuggested ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSuggested ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDisableUem( $boolDisableUem ) {
		$this->set( 'm_boolDisableUem', CStrings::strToBool( $boolDisableUem ) );
	}

	public function getDisableUem() {
		return $this->m_boolDisableUem;
	}

	public function sqlDisableUem() {
		return ( true == isset( $this->m_boolDisableUem ) ) ? '\'' . ( true == ( bool ) $this->m_boolDisableUem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTemplateSummaryUtilityBillAccountIds( $arrintTemplateSummaryUtilityBillAccountIds ) {
		$this->set( 'm_arrintTemplateSummaryUtilityBillAccountIds', CStrings::strToArrIntDef( $arrintTemplateSummaryUtilityBillAccountIds, NULL ) );
	}

	public function getTemplateSummaryUtilityBillAccountIds() {
		return $this->m_arrintTemplateSummaryUtilityBillAccountIds;
	}

	public function sqlTemplateSummaryUtilityBillAccountIds() {
		return ( true == isset( $this->m_arrintTemplateSummaryUtilityBillAccountIds ) && true == valArr( $this->m_arrintTemplateSummaryUtilityBillAccountIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintTemplateSummaryUtilityBillAccountIds, NULL ) . '\'' : 'NULL';
	}

	public function setCopyUtilityBillAccountId( $intCopyUtilityBillAccountId ) {
		$this->set( 'm_intCopyUtilityBillAccountId', CStrings::strToIntDef( $intCopyUtilityBillAccountId, NULL, false ) );
	}

	public function getCopyUtilityBillAccountId() {
		return $this->m_intCopyUtilityBillAccountId;
	}

	public function sqlCopyUtilityBillAccountId() {
		return ( true == isset( $this->m_intCopyUtilityBillAccountId ) ) ? ( string ) $this->m_intCopyUtilityBillAccountId : 'NULL';
	}

	public function setIsBudgetAccount( $boolIsBudgetAccount ) {
		$this->set( 'm_boolIsBudgetAccount', CStrings::strToBool( $boolIsBudgetAccount ) );
	}

	public function getIsBudgetAccount() {
		return $this->m_boolIsBudgetAccount;
	}

	public function sqlIsBudgetAccount() {
		return ( true == isset( $this->m_boolIsBudgetAccount ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBudgetAccount ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_account_detail_id, utility_bill_type_id, utility_bill_template_id, remittance_company_merchant_account_id, last_utility_bill_id, ap_payee_id, ap_payee_location_id, new_utility_bill_account_id, summary_utility_bill_account_id, frequency_id, name, allocation_count, account_datetime, account_number, service_street_line1, service_street_line2, service_city, service_state_code, service_postal_code, notes, required_bills_in_period, require_note_action, is_irregular, is_template, is_ignore_memorization, is_inactive, is_skip_data_entry, is_skip_uem_audits, is_never_estimate, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_credit_available, is_suggested, disable_uem, template_summary_utility_bill_account_ids, copy_utility_bill_account_id, is_budget_account )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityBillAccountDetailId() . ', ' .
						$this->sqlUtilityBillTypeId() . ', ' .
						$this->sqlUtilityBillTemplateId() . ', ' .
						$this->sqlRemittanceCompanyMerchantAccountId() . ', ' .
						$this->sqlLastUtilityBillId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlApPayeeLocationId() . ', ' .
						$this->sqlNewUtilityBillAccountId() . ', ' .
						$this->sqlSummaryUtilityBillAccountId() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlAllocationCount() . ', ' .
						$this->sqlAccountDatetime() . ', ' .
						$this->sqlAccountNumber() . ', ' .
						$this->sqlServiceStreetLine1() . ', ' .
						$this->sqlServiceStreetLine2() . ', ' .
						$this->sqlServiceCity() . ', ' .
						$this->sqlServiceStateCode() . ', ' .
						$this->sqlServicePostalCode() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlRequiredBillsInPeriod() . ', ' .
						$this->sqlRequireNoteAction() . ', ' .
						$this->sqlIsIrregular() . ', ' .
						$this->sqlIsTemplate() . ', ' .
						$this->sqlIsIgnoreMemorization() . ', ' .
						$this->sqlIsInactive() . ', ' .
						$this->sqlIsSkipDataEntry() . ', ' .
						$this->sqlIsSkipUemAudits() . ', ' .
						$this->sqlIsNeverEstimate() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsCreditAvailable() . ', ' .
						$this->sqlIsSuggested() . ', ' .
						$this->sqlDisableUem() . ', ' .
						$this->sqlTemplateSummaryUtilityBillAccountIds() . ', ' .
						$this->sqlCopyUtilityBillAccountId() . ', ' .
						$this->sqlIsBudgetAccount() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_detail_id = ' . $this->sqlUtilityBillAccountDetailId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountDetailId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_detail_id = ' . $this->sqlUtilityBillAccountDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_type_id = ' . $this->sqlUtilityBillTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_type_id = ' . $this->sqlUtilityBillTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_template_id = ' . $this->sqlUtilityBillTemplateId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_template_id = ' . $this->sqlUtilityBillTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remittance_company_merchant_account_id = ' . $this->sqlRemittanceCompanyMerchantAccountId(). ',' ; } elseif( true == array_key_exists( 'RemittanceCompanyMerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' remittance_company_merchant_account_id = ' . $this->sqlRemittanceCompanyMerchantAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_utility_bill_id = ' . $this->sqlLastUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'LastUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' last_utility_bill_id = ' . $this->sqlLastUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_utility_bill_account_id = ' . $this->sqlNewUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'NewUtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' new_utility_bill_account_id = ' . $this->sqlNewUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary_utility_bill_account_id = ' . $this->sqlSummaryUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'SummaryUtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' summary_utility_bill_account_id = ' . $this->sqlSummaryUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_count = ' . $this->sqlAllocationCount(). ',' ; } elseif( true == array_key_exists( 'AllocationCount', $this->getChangedColumns() ) ) { $strSql .= ' allocation_count = ' . $this->sqlAllocationCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_datetime = ' . $this->sqlAccountDatetime(). ',' ; } elseif( true == array_key_exists( 'AccountDatetime', $this->getChangedColumns() ) ) { $strSql .= ' account_datetime = ' . $this->sqlAccountDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber(). ',' ; } elseif( true == array_key_exists( 'AccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_street_line1 = ' . $this->sqlServiceStreetLine1(). ',' ; } elseif( true == array_key_exists( 'ServiceStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' service_street_line1 = ' . $this->sqlServiceStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_street_line2 = ' . $this->sqlServiceStreetLine2(). ',' ; } elseif( true == array_key_exists( 'ServiceStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' service_street_line2 = ' . $this->sqlServiceStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_city = ' . $this->sqlServiceCity(). ',' ; } elseif( true == array_key_exists( 'ServiceCity', $this->getChangedColumns() ) ) { $strSql .= ' service_city = ' . $this->sqlServiceCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_state_code = ' . $this->sqlServiceStateCode(). ',' ; } elseif( true == array_key_exists( 'ServiceStateCode', $this->getChangedColumns() ) ) { $strSql .= ' service_state_code = ' . $this->sqlServiceStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_postal_code = ' . $this->sqlServicePostalCode(). ',' ; } elseif( true == array_key_exists( 'ServicePostalCode', $this->getChangedColumns() ) ) { $strSql .= ' service_postal_code = ' . $this->sqlServicePostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_bills_in_period = ' . $this->sqlRequiredBillsInPeriod(). ',' ; } elseif( true == array_key_exists( 'RequiredBillsInPeriod', $this->getChangedColumns() ) ) { $strSql .= ' required_bills_in_period = ' . $this->sqlRequiredBillsInPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_note_action = ' . $this->sqlRequireNoteAction(). ',' ; } elseif( true == array_key_exists( 'RequireNoteAction', $this->getChangedColumns() ) ) { $strSql .= ' require_note_action = ' . $this->sqlRequireNoteAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_irregular = ' . $this->sqlIsIrregular(). ',' ; } elseif( true == array_key_exists( 'IsIrregular', $this->getChangedColumns() ) ) { $strSql .= ' is_irregular = ' . $this->sqlIsIrregular() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate(). ',' ; } elseif( true == array_key_exists( 'IsTemplate', $this->getChangedColumns() ) ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ignore_memorization = ' . $this->sqlIsIgnoreMemorization(). ',' ; } elseif( true == array_key_exists( 'IsIgnoreMemorization', $this->getChangedColumns() ) ) { $strSql .= ' is_ignore_memorization = ' . $this->sqlIsIgnoreMemorization() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_inactive = ' . $this->sqlIsInactive(). ',' ; } elseif( true == array_key_exists( 'IsInactive', $this->getChangedColumns() ) ) { $strSql .= ' is_inactive = ' . $this->sqlIsInactive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_skip_data_entry = ' . $this->sqlIsSkipDataEntry(). ',' ; } elseif( true == array_key_exists( 'IsSkipDataEntry', $this->getChangedColumns() ) ) { $strSql .= ' is_skip_data_entry = ' . $this->sqlIsSkipDataEntry() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_skip_uem_audits = ' . $this->sqlIsSkipUemAudits(). ',' ; } elseif( true == array_key_exists( 'IsSkipUemAudits', $this->getChangedColumns() ) ) { $strSql .= ' is_skip_uem_audits = ' . $this->sqlIsSkipUemAudits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_never_estimate = ' . $this->sqlIsNeverEstimate(). ',' ; } elseif( true == array_key_exists( 'IsNeverEstimate', $this->getChangedColumns() ) ) { $strSql .= ' is_never_estimate = ' . $this->sqlIsNeverEstimate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_credit_available = ' . $this->sqlIsCreditAvailable(). ',' ; } elseif( true == array_key_exists( 'IsCreditAvailable', $this->getChangedColumns() ) ) { $strSql .= ' is_credit_available = ' . $this->sqlIsCreditAvailable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_suggested = ' . $this->sqlIsSuggested(). ',' ; } elseif( true == array_key_exists( 'IsSuggested', $this->getChangedColumns() ) ) { $strSql .= ' is_suggested = ' . $this->sqlIsSuggested() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disable_uem = ' . $this->sqlDisableUem(). ',' ; } elseif( true == array_key_exists( 'DisableUem', $this->getChangedColumns() ) ) { $strSql .= ' disable_uem = ' . $this->sqlDisableUem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_summary_utility_bill_account_ids = ' . $this->sqlTemplateSummaryUtilityBillAccountIds(). ',' ; } elseif( true == array_key_exists( 'TemplateSummaryUtilityBillAccountIds', $this->getChangedColumns() ) ) { $strSql .= ' template_summary_utility_bill_account_ids = ' . $this->sqlTemplateSummaryUtilityBillAccountIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' copy_utility_bill_account_id = ' . $this->sqlCopyUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'CopyUtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' copy_utility_bill_account_id = ' . $this->sqlCopyUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_budget_account = ' . $this->sqlIsBudgetAccount(). ',' ; } elseif( true == array_key_exists( 'IsBudgetAccount', $this->getChangedColumns() ) ) { $strSql .= ' is_budget_account = ' . $this->sqlIsBudgetAccount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_account_detail_id' => $this->getUtilityBillAccountDetailId(),
			'utility_bill_type_id' => $this->getUtilityBillTypeId(),
			'utility_bill_template_id' => $this->getUtilityBillTemplateId(),
			'remittance_company_merchant_account_id' => $this->getRemittanceCompanyMerchantAccountId(),
			'last_utility_bill_id' => $this->getLastUtilityBillId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'new_utility_bill_account_id' => $this->getNewUtilityBillAccountId(),
			'summary_utility_bill_account_id' => $this->getSummaryUtilityBillAccountId(),
			'frequency_id' => $this->getFrequencyId(),
			'name' => $this->getName(),
			'allocation_count' => $this->getAllocationCount(),
			'account_datetime' => $this->getAccountDatetime(),
			'account_number' => $this->getAccountNumber(),
			'service_street_line1' => $this->getServiceStreetLine1(),
			'service_street_line2' => $this->getServiceStreetLine2(),
			'service_city' => $this->getServiceCity(),
			'service_state_code' => $this->getServiceStateCode(),
			'service_postal_code' => $this->getServicePostalCode(),
			'notes' => $this->getNotes(),
			'required_bills_in_period' => $this->getRequiredBillsInPeriod(),
			'require_note_action' => $this->getRequireNoteAction(),
			'is_irregular' => $this->getIsIrregular(),
			'is_template' => $this->getIsTemplate(),
			'is_ignore_memorization' => $this->getIsIgnoreMemorization(),
			'is_inactive' => $this->getIsInactive(),
			'is_skip_data_entry' => $this->getIsSkipDataEntry(),
			'is_skip_uem_audits' => $this->getIsSkipUemAudits(),
			'is_never_estimate' => $this->getIsNeverEstimate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_credit_available' => $this->getIsCreditAvailable(),
			'is_suggested' => $this->getIsSuggested(),
			'disable_uem' => $this->getDisableUem(),
			'template_summary_utility_bill_account_ids' => $this->getTemplateSummaryUtilityBillAccountIds(),
			'copy_utility_bill_account_id' => $this->getCopyUtilityBillAccountId(),
			'is_budget_account' => $this->getIsBudgetAccount()
		);
	}

}
?>