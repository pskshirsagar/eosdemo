<?php

class CBaseUtilityEmailParserLog extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_email_parser_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intApPayeeLocationId;
	protected $m_intApPayeeAccountId;
	protected $m_strFromEmailAddress;
	protected $m_strToEmailAddress;
	protected $m_strEmailDatetime;
	protected $m_strSubject;
	protected $m_boolIsFailed;
	protected $m_arrintUtilityDocumentIds;
	protected $m_intResolvedBy;
	protected $m_strResolvedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUtilityEmailParserLogStatusTypeId;
	protected $m_strMessageNumber;
	protected $m_strReason;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsFailed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['ap_payee_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeAccountId', trim( $arrValues['ap_payee_account_id'] ) ); elseif( isset( $arrValues['ap_payee_account_id'] ) ) $this->setApPayeeAccountId( $arrValues['ap_payee_account_id'] );
		if( isset( $arrValues['from_email_address'] ) && $boolDirectSet ) $this->set( 'm_strFromEmailAddress', trim( $arrValues['from_email_address'] ) ); elseif( isset( $arrValues['from_email_address'] ) ) $this->setFromEmailAddress( $arrValues['from_email_address'] );
		if( isset( $arrValues['to_email_address'] ) && $boolDirectSet ) $this->set( 'm_strToEmailAddress', trim( $arrValues['to_email_address'] ) ); elseif( isset( $arrValues['to_email_address'] ) ) $this->setToEmailAddress( $arrValues['to_email_address'] );
		if( isset( $arrValues['email_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEmailDatetime', trim( $arrValues['email_datetime'] ) ); elseif( isset( $arrValues['email_datetime'] ) ) $this->setEmailDatetime( $arrValues['email_datetime'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( $arrValues['subject'] ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( $arrValues['subject'] );
		if( isset( $arrValues['is_failed'] ) && $boolDirectSet ) $this->set( 'm_boolIsFailed', trim( stripcslashes( $arrValues['is_failed'] ) ) ); elseif( isset( $arrValues['is_failed'] ) ) $this->setIsFailed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_failed'] ) : $arrValues['is_failed'] );
		if( isset( $arrValues['utility_document_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintUtilityDocumentIds', trim( $arrValues['utility_document_ids'] ) ); elseif( isset( $arrValues['utility_document_ids'] ) ) $this->setUtilityDocumentIds( $arrValues['utility_document_ids'] );
		if( isset( $arrValues['resolved_by'] ) && $boolDirectSet ) $this->set( 'm_intResolvedBy', trim( $arrValues['resolved_by'] ) ); elseif( isset( $arrValues['resolved_by'] ) ) $this->setResolvedBy( $arrValues['resolved_by'] );
		if( isset( $arrValues['resolved_on'] ) && $boolDirectSet ) $this->set( 'm_strResolvedOn', trim( $arrValues['resolved_on'] ) ); elseif( isset( $arrValues['resolved_on'] ) ) $this->setResolvedOn( $arrValues['resolved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['utility_email_parser_log_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityEmailParserLogStatusTypeId', trim( $arrValues['utility_email_parser_log_status_type_id'] ) ); elseif( isset( $arrValues['utility_email_parser_log_status_type_id'] ) ) $this->setUtilityEmailParserLogStatusTypeId( $arrValues['utility_email_parser_log_status_type_id'] );
		if( isset( $arrValues['message_number'] ) && $boolDirectSet ) $this->set( 'm_strMessageNumber', trim( $arrValues['message_number'] ) ); elseif( isset( $arrValues['message_number'] ) ) $this->setMessageNumber( $arrValues['message_number'] );
		if( isset( $arrValues['reason'] ) && $boolDirectSet ) $this->set( 'm_strReason', trim( $arrValues['reason'] ) ); elseif( isset( $arrValues['reason'] ) ) $this->setReason( $arrValues['reason'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setApPayeeAccountId( $intApPayeeAccountId ) {
		$this->set( 'm_intApPayeeAccountId', CStrings::strToIntDef( $intApPayeeAccountId, NULL, false ) );
	}

	public function getApPayeeAccountId() {
		return $this->m_intApPayeeAccountId;
	}

	public function sqlApPayeeAccountId() {
		return ( true == isset( $this->m_intApPayeeAccountId ) ) ? ( string ) $this->m_intApPayeeAccountId : 'NULL';
	}

	public function setFromEmailAddress( $strFromEmailAddress ) {
		$this->set( 'm_strFromEmailAddress', CStrings::strTrimDef( $strFromEmailAddress, 200, NULL, true ) );
	}

	public function getFromEmailAddress() {
		return $this->m_strFromEmailAddress;
	}

	public function sqlFromEmailAddress() {
		return ( true == isset( $this->m_strFromEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFromEmailAddress ) : '\'' . addslashes( $this->m_strFromEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setToEmailAddress( $strToEmailAddress ) {
		$this->set( 'm_strToEmailAddress', CStrings::strTrimDef( $strToEmailAddress, 200, NULL, true ) );
	}

	public function getToEmailAddress() {
		return $this->m_strToEmailAddress;
	}

	public function sqlToEmailAddress() {
		return ( true == isset( $this->m_strToEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strToEmailAddress ) : '\'' . addslashes( $this->m_strToEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setEmailDatetime( $strEmailDatetime ) {
		$this->set( 'm_strEmailDatetime', CStrings::strTrimDef( $strEmailDatetime, -1, NULL, true ) );
	}

	public function getEmailDatetime() {
		return $this->m_strEmailDatetime;
	}

	public function sqlEmailDatetime() {
		return ( true == isset( $this->m_strEmailDatetime ) ) ? '\'' . $this->m_strEmailDatetime . '\'' : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, 500, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSubject ) : '\'' . addslashes( $this->m_strSubject ) . '\'' ) : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Apr 22 2020.
	 */
	public function setIsFailed( $boolIsFailed ) {
		$this->set( 'm_boolIsFailed', CStrings::strToBool( $boolIsFailed ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Apr 22 2020.
	 */
	public function getIsFailed() {
		return $this->m_boolIsFailed;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Apr 22 2020.
	 */
	public function sqlIsFailed() {
		return ( true == isset( $this->m_boolIsFailed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFailed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUtilityDocumentIds( $arrintUtilityDocumentIds ) {
		$this->set( 'm_arrintUtilityDocumentIds', CStrings::strToArrIntDef( $arrintUtilityDocumentIds, NULL ) );
	}

	public function getUtilityDocumentIds() {
		return $this->m_arrintUtilityDocumentIds;
	}

	public function sqlUtilityDocumentIds() {
		return ( true == isset( $this->m_arrintUtilityDocumentIds ) && true == valArr( $this->m_arrintUtilityDocumentIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintUtilityDocumentIds, NULL ) . '\'' : 'NULL';
	}

	public function setResolvedBy( $intResolvedBy ) {
		$this->set( 'm_intResolvedBy', CStrings::strToIntDef( $intResolvedBy, NULL, false ) );
	}

	public function getResolvedBy() {
		return $this->m_intResolvedBy;
	}

	public function sqlResolvedBy() {
		return ( true == isset( $this->m_intResolvedBy ) ) ? ( string ) $this->m_intResolvedBy : 'NULL';
	}

	public function setResolvedOn( $strResolvedOn ) {
		$this->set( 'm_strResolvedOn', CStrings::strTrimDef( $strResolvedOn, -1, NULL, true ) );
	}

	public function getResolvedOn() {
		return $this->m_strResolvedOn;
	}

	public function sqlResolvedOn() {
		return ( true == isset( $this->m_strResolvedOn ) ) ? '\'' . $this->m_strResolvedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUtilityEmailParserLogStatusTypeId( $intUtilityEmailParserLogStatusTypeId ) {
		$this->set( 'm_intUtilityEmailParserLogStatusTypeId', CStrings::strToIntDef( $intUtilityEmailParserLogStatusTypeId, NULL, false ) );
	}

	public function getUtilityEmailParserLogStatusTypeId() {
		return $this->m_intUtilityEmailParserLogStatusTypeId;
	}

	public function sqlUtilityEmailParserLogStatusTypeId() {
		return ( true == isset( $this->m_intUtilityEmailParserLogStatusTypeId ) ) ? ( string ) $this->m_intUtilityEmailParserLogStatusTypeId : 'NULL';
	}

	public function setMessageNumber( $strMessageNumber ) {
		$this->set( 'm_strMessageNumber', CStrings::strTrimDef( $strMessageNumber, 256, NULL, true ) );
	}

	public function getMessageNumber() {
		return $this->m_strMessageNumber;
	}

	public function sqlMessageNumber() {
		return ( true == isset( $this->m_strMessageNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMessageNumber ) : '\'' . addslashes( $this->m_strMessageNumber ) . '\'' ) : 'NULL';
	}

	public function setReason( $strReason ) {
		$this->set( 'm_strReason', CStrings::strTrimDef( $strReason, 256, NULL, true ) );
	}

	public function getReason() {
		return $this->m_strReason;
	}

	public function sqlReason() {
		return ( true == isset( $this->m_strReason ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReason ) : '\'' . addslashes( $this->m_strReason ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, ap_payee_location_id, ap_payee_account_id, from_email_address, to_email_address, email_datetime, subject, is_failed, utility_document_ids, resolved_by, resolved_on, updated_by, updated_on, created_by, created_on, utility_email_parser_log_status_type_id, message_number, reason )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlApPayeeLocationId() . ', ' .
						$this->sqlApPayeeAccountId() . ', ' .
						$this->sqlFromEmailAddress() . ', ' .
						$this->sqlToEmailAddress() . ', ' .
						$this->sqlEmailDatetime() . ', ' .
						$this->sqlSubject() . ', ' .
						$this->sqlIsFailed() . ', ' .
						$this->sqlUtilityDocumentIds() . ', ' .
						$this->sqlResolvedBy() . ', ' .
						$this->sqlResolvedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUtilityEmailParserLogStatusTypeId() . ', ' .
						$this->sqlMessageNumber() . ', ' .
						$this->sqlReason() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress(). ',' ; } elseif( true == array_key_exists( 'FromEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_email_address = ' . $this->sqlToEmailAddress(). ',' ; } elseif( true == array_key_exists( 'ToEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' to_email_address = ' . $this->sqlToEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_datetime = ' . $this->sqlEmailDatetime(). ',' ; } elseif( true == array_key_exists( 'EmailDatetime', $this->getChangedColumns() ) ) { $strSql .= ' email_datetime = ' . $this->sqlEmailDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject(). ',' ; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed(). ',' ; } elseif( true == array_key_exists( 'IsFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_document_ids = ' . $this->sqlUtilityDocumentIds(). ',' ; } elseif( true == array_key_exists( 'UtilityDocumentIds', $this->getChangedColumns() ) ) { $strSql .= ' utility_document_ids = ' . $this->sqlUtilityDocumentIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy(). ',' ; } elseif( true == array_key_exists( 'ResolvedBy', $this->getChangedColumns() ) ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn(). ',' ; } elseif( true == array_key_exists( 'ResolvedOn', $this->getChangedColumns() ) ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_email_parser_log_status_type_id = ' . $this->sqlUtilityEmailParserLogStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityEmailParserLogStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_email_parser_log_status_type_id = ' . $this->sqlUtilityEmailParserLogStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_number = ' . $this->sqlMessageNumber(). ',' ; } elseif( true == array_key_exists( 'MessageNumber', $this->getChangedColumns() ) ) { $strSql .= ' message_number = ' . $this->sqlMessageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason = ' . $this->sqlReason(). ',' ; } elseif( true == array_key_exists( 'Reason', $this->getChangedColumns() ) ) { $strSql .= ' reason = ' . $this->sqlReason() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'ap_payee_account_id' => $this->getApPayeeAccountId(),
			'from_email_address' => $this->getFromEmailAddress(),
			'to_email_address' => $this->getToEmailAddress(),
			'email_datetime' => $this->getEmailDatetime(),
			'subject' => $this->getSubject(),
			'is_failed' => $this->getIsFailed(),
			'utility_document_ids' => $this->getUtilityDocumentIds(),
			'resolved_by' => $this->getResolvedBy(),
			'resolved_on' => $this->getResolvedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'utility_email_parser_log_status_type_id' => $this->getUtilityEmailParserLogStatusTypeId(),
			'message_number' => $this->getMessageNumber(),
			'reason' => $this->getReason()
		);
	}

}
?>