<?php

class CBaseTestUtilityBill extends CEosSingularBase {

	const TABLE_NAME = 'public.test_utility_bills';

	protected $m_intId;
	protected $m_intOriginalCid;
	protected $m_intTestCid;
	protected $m_intOriginalUtilityBillId;
	protected $m_intTestUtilityBillId;
	protected $m_strRequestSubmittedOn;
	protected $m_intRequestSubmittedBy;
	protected $m_strRequestCompletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsMasterBill;
	protected $m_intTestBillBatchId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsMasterBill = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['original_cid'] ) && $boolDirectSet ) $this->set( 'm_intOriginalCid', trim( $arrValues['original_cid'] ) ); elseif( isset( $arrValues['original_cid'] ) ) $this->setOriginalCid( $arrValues['original_cid'] );
		if( isset( $arrValues['test_cid'] ) && $boolDirectSet ) $this->set( 'm_intTestCid', trim( $arrValues['test_cid'] ) ); elseif( isset( $arrValues['test_cid'] ) ) $this->setTestCid( $arrValues['test_cid'] );
		if( isset( $arrValues['original_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalUtilityBillId', trim( $arrValues['original_utility_bill_id'] ) ); elseif( isset( $arrValues['original_utility_bill_id'] ) ) $this->setOriginalUtilityBillId( $arrValues['original_utility_bill_id'] );
		if( isset( $arrValues['test_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intTestUtilityBillId', trim( $arrValues['test_utility_bill_id'] ) ); elseif( isset( $arrValues['test_utility_bill_id'] ) ) $this->setTestUtilityBillId( $arrValues['test_utility_bill_id'] );
		if( isset( $arrValues['request_submitted_on'] ) && $boolDirectSet ) $this->set( 'm_strRequestSubmittedOn', trim( $arrValues['request_submitted_on'] ) ); elseif( isset( $arrValues['request_submitted_on'] ) ) $this->setRequestSubmittedOn( $arrValues['request_submitted_on'] );
		if( isset( $arrValues['request_submitted_by'] ) && $boolDirectSet ) $this->set( 'm_intRequestSubmittedBy', trim( $arrValues['request_submitted_by'] ) ); elseif( isset( $arrValues['request_submitted_by'] ) ) $this->setRequestSubmittedBy( $arrValues['request_submitted_by'] );
		if( isset( $arrValues['request_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strRequestCompletedOn', trim( $arrValues['request_completed_on'] ) ); elseif( isset( $arrValues['request_completed_on'] ) ) $this->setRequestCompletedOn( $arrValues['request_completed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_master_bill'] ) && $boolDirectSet ) $this->set( 'm_boolIsMasterBill', trim( stripcslashes( $arrValues['is_master_bill'] ) ) ); elseif( isset( $arrValues['is_master_bill'] ) ) $this->setIsMasterBill( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_master_bill'] ) : $arrValues['is_master_bill'] );
		if( isset( $arrValues['test_bill_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intTestBillBatchId', trim( $arrValues['test_bill_batch_id'] ) ); elseif( isset( $arrValues['test_bill_batch_id'] ) ) $this->setTestBillBatchId( $arrValues['test_bill_batch_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOriginalCid( $intOriginalCid ) {
		$this->set( 'm_intOriginalCid', CStrings::strToIntDef( $intOriginalCid, NULL, false ) );
	}

	public function getOriginalCid() {
		return $this->m_intOriginalCid;
	}

	public function sqlOriginalCid() {
		return ( true == isset( $this->m_intOriginalCid ) ) ? ( string ) $this->m_intOriginalCid : 'NULL';
	}

	public function setTestCid( $intTestCid ) {
		$this->set( 'm_intTestCid', CStrings::strToIntDef( $intTestCid, NULL, false ) );
	}

	public function getTestCid() {
		return $this->m_intTestCid;
	}

	public function sqlTestCid() {
		return ( true == isset( $this->m_intTestCid ) ) ? ( string ) $this->m_intTestCid : 'NULL';
	}

	public function setOriginalUtilityBillId( $intOriginalUtilityBillId ) {
		$this->set( 'm_intOriginalUtilityBillId', CStrings::strToIntDef( $intOriginalUtilityBillId, NULL, false ) );
	}

	public function getOriginalUtilityBillId() {
		return $this->m_intOriginalUtilityBillId;
	}

	public function sqlOriginalUtilityBillId() {
		return ( true == isset( $this->m_intOriginalUtilityBillId ) ) ? ( string ) $this->m_intOriginalUtilityBillId : 'NULL';
	}

	public function setTestUtilityBillId( $intTestUtilityBillId ) {
		$this->set( 'm_intTestUtilityBillId', CStrings::strToIntDef( $intTestUtilityBillId, NULL, false ) );
	}

	public function getTestUtilityBillId() {
		return $this->m_intTestUtilityBillId;
	}

	public function sqlTestUtilityBillId() {
		return ( true == isset( $this->m_intTestUtilityBillId ) ) ? ( string ) $this->m_intTestUtilityBillId : 'NULL';
	}

	public function setRequestSubmittedOn( $strRequestSubmittedOn ) {
		$this->set( 'm_strRequestSubmittedOn', CStrings::strTrimDef( $strRequestSubmittedOn, -1, NULL, true ) );
	}

	public function getRequestSubmittedOn() {
		return $this->m_strRequestSubmittedOn;
	}

	public function sqlRequestSubmittedOn() {
		return ( true == isset( $this->m_strRequestSubmittedOn ) ) ? '\'' . $this->m_strRequestSubmittedOn . '\'' : 'NULL';
	}

	public function setRequestSubmittedBy( $intRequestSubmittedBy ) {
		$this->set( 'm_intRequestSubmittedBy', CStrings::strToIntDef( $intRequestSubmittedBy, NULL, false ) );
	}

	public function getRequestSubmittedBy() {
		return $this->m_intRequestSubmittedBy;
	}

	public function sqlRequestSubmittedBy() {
		return ( true == isset( $this->m_intRequestSubmittedBy ) ) ? ( string ) $this->m_intRequestSubmittedBy : 'NULL';
	}

	public function setRequestCompletedOn( $strRequestCompletedOn ) {
		$this->set( 'm_strRequestCompletedOn', CStrings::strTrimDef( $strRequestCompletedOn, -1, NULL, true ) );
	}

	public function getRequestCompletedOn() {
		return $this->m_strRequestCompletedOn;
	}

	public function sqlRequestCompletedOn() {
		return ( true == isset( $this->m_strRequestCompletedOn ) ) ? '\'' . $this->m_strRequestCompletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsMasterBill( $boolIsMasterBill ) {
		$this->set( 'm_boolIsMasterBill', CStrings::strToBool( $boolIsMasterBill ) );
	}

	public function getIsMasterBill() {
		return $this->m_boolIsMasterBill;
	}

	public function sqlIsMasterBill() {
		return ( true == isset( $this->m_boolIsMasterBill ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMasterBill ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTestBillBatchId( $intTestBillBatchId ) {
		$this->set( 'm_intTestBillBatchId', CStrings::strToIntDef( $intTestBillBatchId, NULL, false ) );
	}

	public function getTestBillBatchId() {
		return $this->m_intTestBillBatchId;
	}

	public function sqlTestBillBatchId() {
		return ( true == isset( $this->m_intTestBillBatchId ) ) ? ( string ) $this->m_intTestBillBatchId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, original_cid, test_cid, original_utility_bill_id, test_utility_bill_id, request_submitted_on, request_submitted_by, request_completed_on, created_by, created_on, is_master_bill, test_bill_batch_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlOriginalCid() . ', ' .
						$this->sqlTestCid() . ', ' .
						$this->sqlOriginalUtilityBillId() . ', ' .
						$this->sqlTestUtilityBillId() . ', ' .
						$this->sqlRequestSubmittedOn() . ', ' .
						$this->sqlRequestSubmittedBy() . ', ' .
						$this->sqlRequestCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsMasterBill() . ', ' .
						$this->sqlTestBillBatchId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_cid = ' . $this->sqlOriginalCid(). ',' ; } elseif( true == array_key_exists( 'OriginalCid', $this->getChangedColumns() ) ) { $strSql .= ' original_cid = ' . $this->sqlOriginalCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_cid = ' . $this->sqlTestCid(). ',' ; } elseif( true == array_key_exists( 'TestCid', $this->getChangedColumns() ) ) { $strSql .= ' test_cid = ' . $this->sqlTestCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_utility_bill_id = ' . $this->sqlOriginalUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'OriginalUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' original_utility_bill_id = ' . $this->sqlOriginalUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_utility_bill_id = ' . $this->sqlTestUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'TestUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' test_utility_bill_id = ' . $this->sqlTestUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_submitted_on = ' . $this->sqlRequestSubmittedOn(). ',' ; } elseif( true == array_key_exists( 'RequestSubmittedOn', $this->getChangedColumns() ) ) { $strSql .= ' request_submitted_on = ' . $this->sqlRequestSubmittedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_submitted_by = ' . $this->sqlRequestSubmittedBy(). ',' ; } elseif( true == array_key_exists( 'RequestSubmittedBy', $this->getChangedColumns() ) ) { $strSql .= ' request_submitted_by = ' . $this->sqlRequestSubmittedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_completed_on = ' . $this->sqlRequestCompletedOn(). ',' ; } elseif( true == array_key_exists( 'RequestCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' request_completed_on = ' . $this->sqlRequestCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_master_bill = ' . $this->sqlIsMasterBill(). ',' ; } elseif( true == array_key_exists( 'IsMasterBill', $this->getChangedColumns() ) ) { $strSql .= ' is_master_bill = ' . $this->sqlIsMasterBill() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_bill_batch_id = ' . $this->sqlTestBillBatchId() ; } elseif( true == array_key_exists( 'TestBillBatchId', $this->getChangedColumns() ) ) { $strSql .= ' test_bill_batch_id = ' . $this->sqlTestBillBatchId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'original_cid' => $this->getOriginalCid(),
			'test_cid' => $this->getTestCid(),
			'original_utility_bill_id' => $this->getOriginalUtilityBillId(),
			'test_utility_bill_id' => $this->getTestUtilityBillId(),
			'request_submitted_on' => $this->getRequestSubmittedOn(),
			'request_submitted_by' => $this->getRequestSubmittedBy(),
			'request_completed_on' => $this->getRequestCompletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_master_bill' => $this->getIsMasterBill(),
			'test_bill_batch_id' => $this->getTestBillBatchId()
		);
	}

}
?>