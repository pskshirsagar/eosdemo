<?php

class CBaseUtilityTypeAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_type_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUtilityAccountId;
	protected $m_intUtilityTypeId;
	protected $m_intSubsidyArCodeId;
	protected $m_fltSubsidyAmount;
	protected $m_fltMaxBillAmount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltSubsidyAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeId', trim( $arrValues['utility_type_id'] ) ); elseif( isset( $arrValues['utility_type_id'] ) ) $this->setUtilityTypeId( $arrValues['utility_type_id'] );
		if( isset( $arrValues['subsidy_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyArCodeId', trim( $arrValues['subsidy_ar_code_id'] ) ); elseif( isset( $arrValues['subsidy_ar_code_id'] ) ) $this->setSubsidyArCodeId( $arrValues['subsidy_ar_code_id'] );
		if( isset( $arrValues['subsidy_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSubsidyAmount', trim( $arrValues['subsidy_amount'] ) ); elseif( isset( $arrValues['subsidy_amount'] ) ) $this->setSubsidyAmount( $arrValues['subsidy_amount'] );
		if( isset( $arrValues['max_bill_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxBillAmount', trim( $arrValues['max_bill_amount'] ) ); elseif( isset( $arrValues['max_bill_amount'] ) ) $this->setMaxBillAmount( $arrValues['max_bill_amount'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->set( 'm_intUtilityTypeId', CStrings::strToIntDef( $intUtilityTypeId, NULL, false ) );
	}

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function sqlUtilityTypeId() {
		return ( true == isset( $this->m_intUtilityTypeId ) ) ? ( string ) $this->m_intUtilityTypeId : 'NULL';
	}

	public function setSubsidyArCodeId( $intSubsidyArCodeId ) {
		$this->set( 'm_intSubsidyArCodeId', CStrings::strToIntDef( $intSubsidyArCodeId, NULL, false ) );
	}

	public function getSubsidyArCodeId() {
		return $this->m_intSubsidyArCodeId;
	}

	public function sqlSubsidyArCodeId() {
		return ( true == isset( $this->m_intSubsidyArCodeId ) ) ? ( string ) $this->m_intSubsidyArCodeId : 'NULL';
	}

	public function setSubsidyAmount( $fltSubsidyAmount ) {
		$this->set( 'm_fltSubsidyAmount', CStrings::strToFloatDef( $fltSubsidyAmount, NULL, false, 2 ) );
	}

	public function getSubsidyAmount() {
		return $this->m_fltSubsidyAmount;
	}

	public function sqlSubsidyAmount() {
		return ( true == isset( $this->m_fltSubsidyAmount ) ) ? ( string ) $this->m_fltSubsidyAmount : '0';
	}

	public function setMaxBillAmount( $fltMaxBillAmount ) {
		$this->set( 'm_fltMaxBillAmount', CStrings::strToFloatDef( $fltMaxBillAmount, NULL, false, 2 ) );
	}

	public function getMaxBillAmount() {
		return $this->m_fltMaxBillAmount;
	}

	public function sqlMaxBillAmount() {
		return ( true == isset( $this->m_fltMaxBillAmount ) ) ? ( string ) $this->m_fltMaxBillAmount : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, utility_account_id, utility_type_id, subsidy_ar_code_id, subsidy_amount, max_bill_amount, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlUtilityAccountId() . ', ' .
 						$this->sqlUtilityTypeId() . ', ' .
 						$this->sqlSubsidyArCodeId() . ', ' .
 						$this->sqlSubsidyAmount() . ', ' .
 						$this->sqlMaxBillAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; } elseif( true == array_key_exists( 'UtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_ar_code_id = ' . $this->sqlSubsidyArCodeId() . ','; } elseif( true == array_key_exists( 'SubsidyArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_ar_code_id = ' . $this->sqlSubsidyArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_amount = ' . $this->sqlSubsidyAmount() . ','; } elseif( true == array_key_exists( 'SubsidyAmount', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_amount = ' . $this->sqlSubsidyAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_bill_amount = ' . $this->sqlMaxBillAmount() . ','; } elseif( true == array_key_exists( 'MaxBillAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_bill_amount = ' . $this->sqlMaxBillAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'utility_type_id' => $this->getUtilityTypeId(),
			'subsidy_ar_code_id' => $this->getSubsidyArCodeId(),
			'subsidy_amount' => $this->getSubsidyAmount(),
			'max_bill_amount' => $this->getMaxBillAmount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>