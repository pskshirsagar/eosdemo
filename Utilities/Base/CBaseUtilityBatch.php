<?php

class CBaseUtilityBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBatchStepId;
	protected $m_intUtilityDistributionId;
	protected $m_intUtilityNoticeId;
	protected $m_strBatchDatetime;
	protected $m_intTotalUnits;
	protected $m_strPreBillDeadline;
	protected $m_strDistributionDeadline;
	protected $m_strPreBillResponseDeadline;
	protected $m_strBillingMonth;
	protected $m_strNotes;
	protected $m_strQcNotes;
	protected $m_strQcBillingNotes;
	protected $m_strMissingVcrNotes;
	protected $m_strBillingAnalystNote;
	protected $m_strLastSyncOn;
	protected $m_intQcApprovedBy;
	protected $m_strQcApprovedOn;
	protected $m_intAnalystApprovedBy;
	protected $m_strAnalystApprovedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intLockedBy;
	protected $m_strLockedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_batch_step_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchStepId', trim( $arrValues['utility_batch_step_id'] ) ); elseif( isset( $arrValues['utility_batch_step_id'] ) ) $this->setUtilityBatchStepId( $arrValues['utility_batch_step_id'] );
		if( isset( $arrValues['utility_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDistributionId', trim( $arrValues['utility_distribution_id'] ) ); elseif( isset( $arrValues['utility_distribution_id'] ) ) $this->setUtilityDistributionId( $arrValues['utility_distribution_id'] );
		if( isset( $arrValues['utility_notice_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityNoticeId', trim( $arrValues['utility_notice_id'] ) ); elseif( isset( $arrValues['utility_notice_id'] ) ) $this->setUtilityNoticeId( $arrValues['utility_notice_id'] );
		if( isset( $arrValues['batch_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBatchDatetime', trim( $arrValues['batch_datetime'] ) ); elseif( isset( $arrValues['batch_datetime'] ) ) $this->setBatchDatetime( $arrValues['batch_datetime'] );
		if( isset( $arrValues['total_units'] ) && $boolDirectSet ) $this->set( 'm_intTotalUnits', trim( $arrValues['total_units'] ) ); elseif( isset( $arrValues['total_units'] ) ) $this->setTotalUnits( $arrValues['total_units'] );
		if( isset( $arrValues['pre_bill_deadline'] ) && $boolDirectSet ) $this->set( 'm_strPreBillDeadline', trim( $arrValues['pre_bill_deadline'] ) ); elseif( isset( $arrValues['pre_bill_deadline'] ) ) $this->setPreBillDeadline( $arrValues['pre_bill_deadline'] );
		if( isset( $arrValues['distribution_deadline'] ) && $boolDirectSet ) $this->set( 'm_strDistributionDeadline', trim( $arrValues['distribution_deadline'] ) ); elseif( isset( $arrValues['distribution_deadline'] ) ) $this->setDistributionDeadline( $arrValues['distribution_deadline'] );
		if( isset( $arrValues['pre_bill_response_deadline'] ) && $boolDirectSet ) $this->set( 'm_strPreBillResponseDeadline', trim( $arrValues['pre_bill_response_deadline'] ) ); elseif( isset( $arrValues['pre_bill_response_deadline'] ) ) $this->setPreBillResponseDeadline( $arrValues['pre_bill_response_deadline'] );
		if( isset( $arrValues['billing_month'] ) && $boolDirectSet ) $this->set( 'm_strBillingMonth', trim( $arrValues['billing_month'] ) ); elseif( isset( $arrValues['billing_month'] ) ) $this->setBillingMonth( $arrValues['billing_month'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['qc_notes'] ) && $boolDirectSet ) $this->set( 'm_strQcNotes', trim( stripcslashes( $arrValues['qc_notes'] ) ) ); elseif( isset( $arrValues['qc_notes'] ) ) $this->setQcNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['qc_notes'] ) : $arrValues['qc_notes'] );
		if( isset( $arrValues['qc_billing_notes'] ) && $boolDirectSet ) $this->set( 'm_strQcBillingNotes', trim( stripcslashes( $arrValues['qc_billing_notes'] ) ) ); elseif( isset( $arrValues['qc_billing_notes'] ) ) $this->setQcBillingNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['qc_billing_notes'] ) : $arrValues['qc_billing_notes'] );
		if( isset( $arrValues['missing_vcr_notes'] ) && $boolDirectSet ) $this->set( 'm_strMissingVcrNotes', trim( stripcslashes( $arrValues['missing_vcr_notes'] ) ) ); elseif( isset( $arrValues['missing_vcr_notes'] ) ) $this->setMissingVcrNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['missing_vcr_notes'] ) : $arrValues['missing_vcr_notes'] );
		if( isset( $arrValues['billing_analyst_note'] ) && $boolDirectSet ) $this->set( 'm_strBillingAnalystNote', trim( stripcslashes( $arrValues['billing_analyst_note'] ) ) ); elseif( isset( $arrValues['billing_analyst_note'] ) ) $this->setBillingAnalystNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billing_analyst_note'] ) : $arrValues['billing_analyst_note'] );
		if( isset( $arrValues['last_sync_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSyncOn', trim( $arrValues['last_sync_on'] ) ); elseif( isset( $arrValues['last_sync_on'] ) ) $this->setLastSyncOn( $arrValues['last_sync_on'] );
		if( isset( $arrValues['qc_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intQcApprovedBy', trim( $arrValues['qc_approved_by'] ) ); elseif( isset( $arrValues['qc_approved_by'] ) ) $this->setQcApprovedBy( $arrValues['qc_approved_by'] );
		if( isset( $arrValues['qc_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strQcApprovedOn', trim( $arrValues['qc_approved_on'] ) ); elseif( isset( $arrValues['qc_approved_on'] ) ) $this->setQcApprovedOn( $arrValues['qc_approved_on'] );
		if( isset( $arrValues['analyst_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intAnalystApprovedBy', trim( $arrValues['analyst_approved_by'] ) ); elseif( isset( $arrValues['analyst_approved_by'] ) ) $this->setAnalystApprovedBy( $arrValues['analyst_approved_by'] );
		if( isset( $arrValues['analyst_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strAnalystApprovedOn', trim( $arrValues['analyst_approved_on'] ) ); elseif( isset( $arrValues['analyst_approved_on'] ) ) $this->setAnalystApprovedOn( $arrValues['analyst_approved_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['locked_by'] ) && $boolDirectSet ) $this->set( 'm_intLockedBy', trim( $arrValues['locked_by'] ) ); elseif( isset( $arrValues['locked_by'] ) ) $this->setLockedBy( $arrValues['locked_by'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBatchStepId( $intUtilityBatchStepId ) {
		$this->set( 'm_intUtilityBatchStepId', CStrings::strToIntDef( $intUtilityBatchStepId, NULL, false ) );
	}

	public function getUtilityBatchStepId() {
		return $this->m_intUtilityBatchStepId;
	}

	public function sqlUtilityBatchStepId() {
		return ( true == isset( $this->m_intUtilityBatchStepId ) ) ? ( string ) $this->m_intUtilityBatchStepId : 'NULL';
	}

	public function setUtilityDistributionId( $intUtilityDistributionId ) {
		$this->set( 'm_intUtilityDistributionId', CStrings::strToIntDef( $intUtilityDistributionId, NULL, false ) );
	}

	public function getUtilityDistributionId() {
		return $this->m_intUtilityDistributionId;
	}

	public function sqlUtilityDistributionId() {
		return ( true == isset( $this->m_intUtilityDistributionId ) ) ? ( string ) $this->m_intUtilityDistributionId : 'NULL';
	}

	public function setUtilityNoticeId( $intUtilityNoticeId ) {
		$this->set( 'm_intUtilityNoticeId', CStrings::strToIntDef( $intUtilityNoticeId, NULL, false ) );
	}

	public function getUtilityNoticeId() {
		return $this->m_intUtilityNoticeId;
	}

	public function sqlUtilityNoticeId() {
		return ( true == isset( $this->m_intUtilityNoticeId ) ) ? ( string ) $this->m_intUtilityNoticeId : 'NULL';
	}

	public function setBatchDatetime( $strBatchDatetime ) {
		$this->set( 'm_strBatchDatetime', CStrings::strTrimDef( $strBatchDatetime, -1, NULL, true ) );
	}

	public function getBatchDatetime() {
		return $this->m_strBatchDatetime;
	}

	public function sqlBatchDatetime() {
		return ( true == isset( $this->m_strBatchDatetime ) ) ? '\'' . $this->m_strBatchDatetime . '\'' : 'NOW()';
	}

	public function setTotalUnits( $intTotalUnits ) {
		$this->set( 'm_intTotalUnits', CStrings::strToIntDef( $intTotalUnits, NULL, false ) );
	}

	public function getTotalUnits() {
		return $this->m_intTotalUnits;
	}

	public function sqlTotalUnits() {
		return ( true == isset( $this->m_intTotalUnits ) ) ? ( string ) $this->m_intTotalUnits : 'NULL';
	}

	public function setPreBillDeadline( $strPreBillDeadline ) {
		$this->set( 'm_strPreBillDeadline', CStrings::strTrimDef( $strPreBillDeadline, -1, NULL, true ) );
	}

	public function getPreBillDeadline() {
		return $this->m_strPreBillDeadline;
	}

	public function sqlPreBillDeadline() {
		return ( true == isset( $this->m_strPreBillDeadline ) ) ? '\'' . $this->m_strPreBillDeadline . '\'' : 'NOW()';
	}

	public function setDistributionDeadline( $strDistributionDeadline ) {
		$this->set( 'm_strDistributionDeadline', CStrings::strTrimDef( $strDistributionDeadline, -1, NULL, true ) );
	}

	public function getDistributionDeadline() {
		return $this->m_strDistributionDeadline;
	}

	public function sqlDistributionDeadline() {
		return ( true == isset( $this->m_strDistributionDeadline ) ) ? '\'' . $this->m_strDistributionDeadline . '\'' : 'NOW()';
	}

	public function setPreBillResponseDeadline( $strPreBillResponseDeadline ) {
		$this->set( 'm_strPreBillResponseDeadline', CStrings::strTrimDef( $strPreBillResponseDeadline, -1, NULL, true ) );
	}

	public function getPreBillResponseDeadline() {
		return $this->m_strPreBillResponseDeadline;
	}

	public function sqlPreBillResponseDeadline() {
		return ( true == isset( $this->m_strPreBillResponseDeadline ) ) ? '\'' . $this->m_strPreBillResponseDeadline . '\'' : 'NULL';
	}

	public function setBillingMonth( $strBillingMonth ) {
		$this->set( 'm_strBillingMonth', CStrings::strTrimDef( $strBillingMonth, -1, NULL, true ) );
	}

	public function getBillingMonth() {
		return $this->m_strBillingMonth;
	}

	public function sqlBillingMonth() {
		return ( true == isset( $this->m_strBillingMonth ) ) ? '\'' . $this->m_strBillingMonth . '\'' : 'NOW()';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setQcNotes( $strQcNotes ) {
		$this->set( 'm_strQcNotes', CStrings::strTrimDef( $strQcNotes, -1, NULL, true ) );
	}

	public function getQcNotes() {
		return $this->m_strQcNotes;
	}

	public function sqlQcNotes() {
		return ( true == isset( $this->m_strQcNotes ) ) ? '\'' . addslashes( $this->m_strQcNotes ) . '\'' : 'NULL';
	}

	public function setQcBillingNotes( $strQcBillingNotes ) {
		$this->set( 'm_strQcBillingNotes', CStrings::strTrimDef( $strQcBillingNotes, -1, NULL, true ) );
	}

	public function getQcBillingNotes() {
		return $this->m_strQcBillingNotes;
	}

	public function sqlQcBillingNotes() {
		return ( true == isset( $this->m_strQcBillingNotes ) ) ? '\'' . addslashes( $this->m_strQcBillingNotes ) . '\'' : 'NULL';
	}

	public function setMissingVcrNotes( $strMissingVcrNotes ) {
		$this->set( 'm_strMissingVcrNotes', CStrings::strTrimDef( $strMissingVcrNotes, -1, NULL, true ) );
	}

	public function getMissingVcrNotes() {
		return $this->m_strMissingVcrNotes;
	}

	public function sqlMissingVcrNotes() {
		return ( true == isset( $this->m_strMissingVcrNotes ) ) ? '\'' . addslashes( $this->m_strMissingVcrNotes ) . '\'' : 'NULL';
	}

	public function setBillingAnalystNote( $strBillingAnalystNote ) {
		$this->set( 'm_strBillingAnalystNote', CStrings::strTrimDef( $strBillingAnalystNote, -1, NULL, true ) );
	}

	public function getBillingAnalystNote() {
		return $this->m_strBillingAnalystNote;
	}

	public function sqlBillingAnalystNote() {
		return ( true == isset( $this->m_strBillingAnalystNote ) ) ? '\'' . addslashes( $this->m_strBillingAnalystNote ) . '\'' : 'NULL';
	}

	public function setLastSyncOn( $strLastSyncOn ) {
		$this->set( 'm_strLastSyncOn', CStrings::strTrimDef( $strLastSyncOn, -1, NULL, true ) );
	}

	public function getLastSyncOn() {
		return $this->m_strLastSyncOn;
	}

	public function sqlLastSyncOn() {
		return ( true == isset( $this->m_strLastSyncOn ) ) ? '\'' . $this->m_strLastSyncOn . '\'' : 'NULL';
	}

	public function setQcApprovedBy( $intQcApprovedBy ) {
		$this->set( 'm_intQcApprovedBy', CStrings::strToIntDef( $intQcApprovedBy, NULL, false ) );
	}

	public function getQcApprovedBy() {
		return $this->m_intQcApprovedBy;
	}

	public function sqlQcApprovedBy() {
		return ( true == isset( $this->m_intQcApprovedBy ) ) ? ( string ) $this->m_intQcApprovedBy : 'NULL';
	}

	public function setQcApprovedOn( $strQcApprovedOn ) {
		$this->set( 'm_strQcApprovedOn', CStrings::strTrimDef( $strQcApprovedOn, -1, NULL, true ) );
	}

	public function getQcApprovedOn() {
		return $this->m_strQcApprovedOn;
	}

	public function sqlQcApprovedOn() {
		return ( true == isset( $this->m_strQcApprovedOn ) ) ? '\'' . $this->m_strQcApprovedOn . '\'' : 'NULL';
	}

	public function setAnalystApprovedBy( $intAnalystApprovedBy ) {
		$this->set( 'm_intAnalystApprovedBy', CStrings::strToIntDef( $intAnalystApprovedBy, NULL, false ) );
	}

	public function getAnalystApprovedBy() {
		return $this->m_intAnalystApprovedBy;
	}

	public function sqlAnalystApprovedBy() {
		return ( true == isset( $this->m_intAnalystApprovedBy ) ) ? ( string ) $this->m_intAnalystApprovedBy : 'NULL';
	}

	public function setAnalystApprovedOn( $strAnalystApprovedOn ) {
		$this->set( 'm_strAnalystApprovedOn', CStrings::strTrimDef( $strAnalystApprovedOn, -1, NULL, true ) );
	}

	public function getAnalystApprovedOn() {
		return $this->m_strAnalystApprovedOn;
	}

	public function sqlAnalystApprovedOn() {
		return ( true == isset( $this->m_strAnalystApprovedOn ) ) ? '\'' . $this->m_strAnalystApprovedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setLockedBy( $intLockedBy ) {
		$this->set( 'm_intLockedBy', CStrings::strToIntDef( $intLockedBy, NULL, false ) );
	}

	public function getLockedBy() {
		return $this->m_intLockedBy;
	}

	public function sqlLockedBy() {
		return ( true == isset( $this->m_intLockedBy ) ) ? ( string ) $this->m_intLockedBy : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_batch_step_id, utility_distribution_id, utility_notice_id, batch_datetime, total_units, pre_bill_deadline, distribution_deadline, pre_bill_response_deadline, billing_month, notes, qc_notes, qc_billing_notes, missing_vcr_notes, billing_analyst_note, last_sync_on, qc_approved_by, qc_approved_on, analyst_approved_by, analyst_approved_on, approved_by, approved_on, updated_by, updated_on, created_by, created_on, locked_by, locked_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUtilityBatchStepId() . ', ' .
 						$this->sqlUtilityDistributionId() . ', ' .
 						$this->sqlUtilityNoticeId() . ', ' .
 						$this->sqlBatchDatetime() . ', ' .
 						$this->sqlTotalUnits() . ', ' .
 						$this->sqlPreBillDeadline() . ', ' .
 						$this->sqlDistributionDeadline() . ', ' .
 						$this->sqlPreBillResponseDeadline() . ', ' .
 						$this->sqlBillingMonth() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlQcNotes() . ', ' .
 						$this->sqlQcBillingNotes() . ', ' .
 						$this->sqlMissingVcrNotes() . ', ' .
 						$this->sqlBillingAnalystNote() . ', ' .
 						$this->sqlLastSyncOn() . ', ' .
 						$this->sqlQcApprovedBy() . ', ' .
 						$this->sqlQcApprovedOn() . ', ' .
 						$this->sqlAnalystApprovedBy() . ', ' .
 						$this->sqlAnalystApprovedOn() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlLockedBy() . ', ' .
						$this->sqlLockedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_step_id = ' . $this->sqlUtilityBatchStepId(). ',' ; } elseif( true == array_key_exists( 'UtilityBatchStepId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_step_id = ' . $this->sqlUtilityBatchStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_distribution_id = ' . $this->sqlUtilityDistributionId(). ',' ; } elseif( true == array_key_exists( 'UtilityDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_distribution_id = ' . $this->sqlUtilityDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_notice_id = ' . $this->sqlUtilityNoticeId(). ',' ; } elseif( true == array_key_exists( 'UtilityNoticeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_notice_id = ' . $this->sqlUtilityNoticeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime(). ',' ; } elseif( true == array_key_exists( 'BatchDatetime', $this->getChangedColumns() ) ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_units = ' . $this->sqlTotalUnits(). ',' ; } elseif( true == array_key_exists( 'TotalUnits', $this->getChangedColumns() ) ) { $strSql .= ' total_units = ' . $this->sqlTotalUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_bill_deadline = ' . $this->sqlPreBillDeadline(). ',' ; } elseif( true == array_key_exists( 'PreBillDeadline', $this->getChangedColumns() ) ) { $strSql .= ' pre_bill_deadline = ' . $this->sqlPreBillDeadline() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_deadline = ' . $this->sqlDistributionDeadline(). ',' ; } elseif( true == array_key_exists( 'DistributionDeadline', $this->getChangedColumns() ) ) { $strSql .= ' distribution_deadline = ' . $this->sqlDistributionDeadline() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_bill_response_deadline = ' . $this->sqlPreBillResponseDeadline(). ',' ; } elseif( true == array_key_exists( 'PreBillResponseDeadline', $this->getChangedColumns() ) ) { $strSql .= ' pre_bill_response_deadline = ' . $this->sqlPreBillResponseDeadline() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_month = ' . $this->sqlBillingMonth(). ',' ; } elseif( true == array_key_exists( 'BillingMonth', $this->getChangedColumns() ) ) { $strSql .= ' billing_month = ' . $this->sqlBillingMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_notes = ' . $this->sqlQcNotes(). ',' ; } elseif( true == array_key_exists( 'QcNotes', $this->getChangedColumns() ) ) { $strSql .= ' qc_notes = ' . $this->sqlQcNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_billing_notes = ' . $this->sqlQcBillingNotes(). ',' ; } elseif( true == array_key_exists( 'QcBillingNotes', $this->getChangedColumns() ) ) { $strSql .= ' qc_billing_notes = ' . $this->sqlQcBillingNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' missing_vcr_notes = ' . $this->sqlMissingVcrNotes(). ',' ; } elseif( true == array_key_exists( 'MissingVcrNotes', $this->getChangedColumns() ) ) { $strSql .= ' missing_vcr_notes = ' . $this->sqlMissingVcrNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_analyst_note = ' . $this->sqlBillingAnalystNote(). ',' ; } elseif( true == array_key_exists( 'BillingAnalystNote', $this->getChangedColumns() ) ) { $strSql .= ' billing_analyst_note = ' . $this->sqlBillingAnalystNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn(). ',' ; } elseif( true == array_key_exists( 'LastSyncOn', $this->getChangedColumns() ) ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_approved_by = ' . $this->sqlQcApprovedBy(). ',' ; } elseif( true == array_key_exists( 'QcApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' qc_approved_by = ' . $this->sqlQcApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_approved_on = ' . $this->sqlQcApprovedOn(). ',' ; } elseif( true == array_key_exists( 'QcApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' qc_approved_on = ' . $this->sqlQcApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' analyst_approved_by = ' . $this->sqlAnalystApprovedBy(). ',' ; } elseif( true == array_key_exists( 'AnalystApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' analyst_approved_by = ' . $this->sqlAnalystApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' analyst_approved_on = ' . $this->sqlAnalystApprovedOn(). ',' ; } elseif( true == array_key_exists( 'AnalystApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' analyst_approved_on = ' . $this->sqlAnalystApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy(). ',' ; } elseif( true == array_key_exists( 'LockedBy', $this->getChangedColumns() ) ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn(). ',' ; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_batch_step_id' => $this->getUtilityBatchStepId(),
			'utility_distribution_id' => $this->getUtilityDistributionId(),
			'utility_notice_id' => $this->getUtilityNoticeId(),
			'batch_datetime' => $this->getBatchDatetime(),
			'total_units' => $this->getTotalUnits(),
			'pre_bill_deadline' => $this->getPreBillDeadline(),
			'distribution_deadline' => $this->getDistributionDeadline(),
			'pre_bill_response_deadline' => $this->getPreBillResponseDeadline(),
			'billing_month' => $this->getBillingMonth(),
			'notes' => $this->getNotes(),
			'qc_notes' => $this->getQcNotes(),
			'qc_billing_notes' => $this->getQcBillingNotes(),
			'missing_vcr_notes' => $this->getMissingVcrNotes(),
			'billing_analyst_note' => $this->getBillingAnalystNote(),
			'last_sync_on' => $this->getLastSyncOn(),
			'qc_approved_by' => $this->getQcApprovedBy(),
			'qc_approved_on' => $this->getQcApprovedOn(),
			'analyst_approved_by' => $this->getAnalystApprovedBy(),
			'analyst_approved_on' => $this->getAnalystApprovedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'locked_by' => $this->getLockedBy(),
			'locked_on' => $this->getLockedOn()
		);
	}

}
?>