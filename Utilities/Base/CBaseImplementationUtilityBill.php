<?php

class CBaseImplementationUtilityBill extends CEosSingularBase {

	const TABLE_NAME = 'public.implementation_utility_bills';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intImplementationWebRetrievalId;
	protected $m_intSummaryImplementationUtilityBillId;
	protected $m_intSummaryImplementationUtilityBillPageNumber;
	protected $m_intReviewImplementationUtilityBillId;
	protected $m_intUtilityDocumentId;
	protected $m_intUtilityBillAccountId;
	protected $m_strBillDatetime;
	protected $m_boolIsQueueIgnored;
	protected $m_strBillProcessingDetails;
	protected $m_jsonBillProcessingDetails;
	protected $m_strStartProcessDatetime;
	protected $m_strEndProcessDatetime;
	protected $m_intLastProcessedBy;
	protected $m_intProcessedBy;
	protected $m_strProcessedOn;
	protected $m_strProcessedOnTime;
	protected $m_intProcessedAgainBy;
	protected $m_strProcessedAgainOn;
	protected $m_strProcessedAgainOnTime;
	protected $m_strStartReviewDatetime;
	protected $m_strEndReviewDatetime;
	protected $m_intLastReviewedBy;
	protected $m_intReviewedBy;
	protected $m_strReviewedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intOcrTemplateId;
	protected $m_intUtilityBillId;
	protected $m_intUtilityBillReceiptTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsQueueIgnored = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['implementation_web_retrieval_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationWebRetrievalId', trim( $arrValues['implementation_web_retrieval_id'] ) ); elseif( isset( $arrValues['implementation_web_retrieval_id'] ) ) $this->setImplementationWebRetrievalId( $arrValues['implementation_web_retrieval_id'] );
		if( isset( $arrValues['summary_implementation_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intSummaryImplementationUtilityBillId', trim( $arrValues['summary_implementation_utility_bill_id'] ) ); elseif( isset( $arrValues['summary_implementation_utility_bill_id'] ) ) $this->setSummaryImplementationUtilityBillId( $arrValues['summary_implementation_utility_bill_id'] );
		if( isset( $arrValues['summary_implementation_utility_bill_page_number'] ) && $boolDirectSet ) $this->set( 'm_intSummaryImplementationUtilityBillPageNumber', trim( $arrValues['summary_implementation_utility_bill_page_number'] ) ); elseif( isset( $arrValues['summary_implementation_utility_bill_page_number'] ) ) $this->setSummaryImplementationUtilityBillPageNumber( $arrValues['summary_implementation_utility_bill_page_number'] );
		if( isset( $arrValues['review_implementation_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewImplementationUtilityBillId', trim( $arrValues['review_implementation_utility_bill_id'] ) ); elseif( isset( $arrValues['review_implementation_utility_bill_id'] ) ) $this->setReviewImplementationUtilityBillId( $arrValues['review_implementation_utility_bill_id'] );
		if( isset( $arrValues['utility_document_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDocumentId', trim( $arrValues['utility_document_id'] ) ); elseif( isset( $arrValues['utility_document_id'] ) ) $this->setUtilityDocumentId( $arrValues['utility_document_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['bill_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBillDatetime', trim( $arrValues['bill_datetime'] ) ); elseif( isset( $arrValues['bill_datetime'] ) ) $this->setBillDatetime( $arrValues['bill_datetime'] );
		if( isset( $arrValues['is_queue_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsQueueIgnored', trim( stripcslashes( $arrValues['is_queue_ignored'] ) ) ); elseif( isset( $arrValues['is_queue_ignored'] ) ) $this->setIsQueueIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_queue_ignored'] ) : $arrValues['is_queue_ignored'] );
		if( isset( $arrValues['bill_processing_details'] ) ) $this->set( 'm_strBillProcessingDetails', trim( $arrValues['bill_processing_details'] ) );
		if( isset( $arrValues['start_process_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartProcessDatetime', trim( $arrValues['start_process_datetime'] ) ); elseif( isset( $arrValues['start_process_datetime'] ) ) $this->setStartProcessDatetime( $arrValues['start_process_datetime'] );
		if( isset( $arrValues['end_process_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndProcessDatetime', trim( $arrValues['end_process_datetime'] ) ); elseif( isset( $arrValues['end_process_datetime'] ) ) $this->setEndProcessDatetime( $arrValues['end_process_datetime'] );
		if( isset( $arrValues['last_processed_by'] ) && $boolDirectSet ) $this->set( 'm_intLastProcessedBy', trim( $arrValues['last_processed_by'] ) ); elseif( isset( $arrValues['last_processed_by'] ) ) $this->setLastProcessedBy( $arrValues['last_processed_by'] );
		if( isset( $arrValues['processed_by'] ) && $boolDirectSet ) $this->set( 'm_intProcessedBy', trim( $arrValues['processed_by'] ) ); elseif( isset( $arrValues['processed_by'] ) ) $this->setProcessedBy( $arrValues['processed_by'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['processed_on_time'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOnTime', trim( $arrValues['processed_on_time'] ) ); elseif( isset( $arrValues['processed_on_time'] ) ) $this->setProcessedOnTime( $arrValues['processed_on_time'] );
		if( isset( $arrValues['processed_again_by'] ) && $boolDirectSet ) $this->set( 'm_intProcessedAgainBy', trim( $arrValues['processed_again_by'] ) ); elseif( isset( $arrValues['processed_again_by'] ) ) $this->setProcessedAgainBy( $arrValues['processed_again_by'] );
		if( isset( $arrValues['processed_again_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedAgainOn', trim( $arrValues['processed_again_on'] ) ); elseif( isset( $arrValues['processed_again_on'] ) ) $this->setProcessedAgainOn( $arrValues['processed_again_on'] );
		if( isset( $arrValues['processed_again_on_time'] ) && $boolDirectSet ) $this->set( 'm_strProcessedAgainOnTime', trim( $arrValues['processed_again_on_time'] ) ); elseif( isset( $arrValues['processed_again_on_time'] ) ) $this->setProcessedAgainOnTime( $arrValues['processed_again_on_time'] );
		if( isset( $arrValues['start_review_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartReviewDatetime', trim( $arrValues['start_review_datetime'] ) ); elseif( isset( $arrValues['start_review_datetime'] ) ) $this->setStartReviewDatetime( $arrValues['start_review_datetime'] );
		if( isset( $arrValues['end_review_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndReviewDatetime', trim( $arrValues['end_review_datetime'] ) ); elseif( isset( $arrValues['end_review_datetime'] ) ) $this->setEndReviewDatetime( $arrValues['end_review_datetime'] );
		if( isset( $arrValues['last_reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intLastReviewedBy', trim( $arrValues['last_reviewed_by'] ) ); elseif( isset( $arrValues['last_reviewed_by'] ) ) $this->setLastReviewedBy( $arrValues['last_reviewed_by'] );
		if( isset( $arrValues['reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intReviewedBy', trim( $arrValues['reviewed_by'] ) ); elseif( isset( $arrValues['reviewed_by'] ) ) $this->setReviewedBy( $arrValues['reviewed_by'] );
		if( isset( $arrValues['reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strReviewedOn', trim( $arrValues['reviewed_on'] ) ); elseif( isset( $arrValues['reviewed_on'] ) ) $this->setReviewedOn( $arrValues['reviewed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ocr_template_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrTemplateId', trim( $arrValues['ocr_template_id'] ) ); elseif( isset( $arrValues['ocr_template_id'] ) ) $this->setOcrTemplateId( $arrValues['ocr_template_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['utility_bill_receipt_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillReceiptTypeId', trim( $arrValues['utility_bill_receipt_type_id'] ) ); elseif( isset( $arrValues['utility_bill_receipt_type_id'] ) ) $this->setUtilityBillReceiptTypeId( $arrValues['utility_bill_receipt_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setImplementationWebRetrievalId( $intImplementationWebRetrievalId ) {
		$this->set( 'm_intImplementationWebRetrievalId', CStrings::strToIntDef( $intImplementationWebRetrievalId, NULL, false ) );
	}

	public function getImplementationWebRetrievalId() {
		return $this->m_intImplementationWebRetrievalId;
	}

	public function sqlImplementationWebRetrievalId() {
		return ( true == isset( $this->m_intImplementationWebRetrievalId ) ) ? ( string ) $this->m_intImplementationWebRetrievalId : 'NULL';
	}

	public function setSummaryImplementationUtilityBillId( $intSummaryImplementationUtilityBillId ) {
		$this->set( 'm_intSummaryImplementationUtilityBillId', CStrings::strToIntDef( $intSummaryImplementationUtilityBillId, NULL, false ) );
	}

	public function getSummaryImplementationUtilityBillId() {
		return $this->m_intSummaryImplementationUtilityBillId;
	}

	public function sqlSummaryImplementationUtilityBillId() {
		return ( true == isset( $this->m_intSummaryImplementationUtilityBillId ) ) ? ( string ) $this->m_intSummaryImplementationUtilityBillId : 'NULL';
	}

	public function setSummaryImplementationUtilityBillPageNumber( $intSummaryImplementationUtilityBillPageNumber ) {
		$this->set( 'm_intSummaryImplementationUtilityBillPageNumber', CStrings::strToIntDef( $intSummaryImplementationUtilityBillPageNumber, NULL, false ) );
	}

	public function getSummaryImplementationUtilityBillPageNumber() {
		return $this->m_intSummaryImplementationUtilityBillPageNumber;
	}

	public function sqlSummaryImplementationUtilityBillPageNumber() {
		return ( true == isset( $this->m_intSummaryImplementationUtilityBillPageNumber ) ) ? ( string ) $this->m_intSummaryImplementationUtilityBillPageNumber : 'NULL';
	}

	public function setReviewImplementationUtilityBillId( $intReviewImplementationUtilityBillId ) {
		$this->set( 'm_intReviewImplementationUtilityBillId', CStrings::strToIntDef( $intReviewImplementationUtilityBillId, NULL, false ) );
	}

	public function getReviewImplementationUtilityBillId() {
		return $this->m_intReviewImplementationUtilityBillId;
	}

	public function sqlReviewImplementationUtilityBillId() {
		return ( true == isset( $this->m_intReviewImplementationUtilityBillId ) ) ? ( string ) $this->m_intReviewImplementationUtilityBillId : 'NULL';
	}

	public function setUtilityDocumentId( $intUtilityDocumentId ) {
		$this->set( 'm_intUtilityDocumentId', CStrings::strToIntDef( $intUtilityDocumentId, NULL, false ) );
	}

	public function getUtilityDocumentId() {
		return $this->m_intUtilityDocumentId;
	}

	public function sqlUtilityDocumentId() {
		return ( true == isset( $this->m_intUtilityDocumentId ) ) ? ( string ) $this->m_intUtilityDocumentId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setBillDatetime( $strBillDatetime ) {
		$this->set( 'm_strBillDatetime', CStrings::strTrimDef( $strBillDatetime, -1, NULL, true ) );
	}

	public function getBillDatetime() {
		return $this->m_strBillDatetime;
	}

	public function sqlBillDatetime() {
		return ( true == isset( $this->m_strBillDatetime ) ) ? '\'' . $this->m_strBillDatetime . '\'' : 'NULL';
	}

	public function setIsQueueIgnored( $boolIsQueueIgnored ) {
		$this->set( 'm_boolIsQueueIgnored', CStrings::strToBool( $boolIsQueueIgnored ) );
	}

	public function getIsQueueIgnored() {
		return $this->m_boolIsQueueIgnored;
	}

	public function sqlIsQueueIgnored() {
		return ( true == isset( $this->m_boolIsQueueIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsQueueIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBillProcessingDetails( $jsonBillProcessingDetails ) {
		if( true == valObj( $jsonBillProcessingDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonBillProcessingDetails', $jsonBillProcessingDetails );
		} elseif( true == valJsonString( $jsonBillProcessingDetails ) ) {
			$this->set( 'm_jsonBillProcessingDetails', CStrings::strToJson( $jsonBillProcessingDetails ) );
		} else {
			$this->set( 'm_jsonBillProcessingDetails', NULL ); 
		}
		unset( $this->m_strBillProcessingDetails );
	}

	public function getBillProcessingDetails() {
		if( true == isset( $this->m_strBillProcessingDetails ) ) {
			$this->m_jsonBillProcessingDetails = CStrings::strToJson( $this->m_strBillProcessingDetails );
			unset( $this->m_strBillProcessingDetails );
		}
		return $this->m_jsonBillProcessingDetails;
	}

	public function sqlBillProcessingDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getBillProcessingDetails() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getBillProcessingDetails() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getBillProcessingDetails() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setStartProcessDatetime( $strStartProcessDatetime ) {
		$this->set( 'm_strStartProcessDatetime', CStrings::strTrimDef( $strStartProcessDatetime, -1, NULL, true ) );
	}

	public function getStartProcessDatetime() {
		return $this->m_strStartProcessDatetime;
	}

	public function sqlStartProcessDatetime() {
		return ( true == isset( $this->m_strStartProcessDatetime ) ) ? '\'' . $this->m_strStartProcessDatetime . '\'' : 'NULL';
	}

	public function setEndProcessDatetime( $strEndProcessDatetime ) {
		$this->set( 'm_strEndProcessDatetime', CStrings::strTrimDef( $strEndProcessDatetime, -1, NULL, true ) );
	}

	public function getEndProcessDatetime() {
		return $this->m_strEndProcessDatetime;
	}

	public function sqlEndProcessDatetime() {
		return ( true == isset( $this->m_strEndProcessDatetime ) ) ? '\'' . $this->m_strEndProcessDatetime . '\'' : 'NULL';
	}

	public function setLastProcessedBy( $intLastProcessedBy ) {
		$this->set( 'm_intLastProcessedBy', CStrings::strToIntDef( $intLastProcessedBy, NULL, false ) );
	}

	public function getLastProcessedBy() {
		return $this->m_intLastProcessedBy;
	}

	public function sqlLastProcessedBy() {
		return ( true == isset( $this->m_intLastProcessedBy ) ) ? ( string ) $this->m_intLastProcessedBy : 'NULL';
	}

	public function setProcessedBy( $intProcessedBy ) {
		$this->set( 'm_intProcessedBy', CStrings::strToIntDef( $intProcessedBy, NULL, false ) );
	}

	public function getProcessedBy() {
		return $this->m_intProcessedBy;
	}

	public function sqlProcessedBy() {
		return ( true == isset( $this->m_intProcessedBy ) ) ? ( string ) $this->m_intProcessedBy : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setProcessedOnTime( $strProcessedOnTime ) {
		$this->set( 'm_strProcessedOnTime', CStrings::strTrimDef( $strProcessedOnTime, NULL, NULL, true ) );
	}

	public function getProcessedOnTime() {
		return $this->m_strProcessedOnTime;
	}

	public function sqlProcessedOnTime() {
		return ( true == isset( $this->m_strProcessedOnTime ) ) ? '\'' . $this->m_strProcessedOnTime . '\'' : 'NULL';
	}

	public function setProcessedAgainBy( $intProcessedAgainBy ) {
		$this->set( 'm_intProcessedAgainBy', CStrings::strToIntDef( $intProcessedAgainBy, NULL, false ) );
	}

	public function getProcessedAgainBy() {
		return $this->m_intProcessedAgainBy;
	}

	public function sqlProcessedAgainBy() {
		return ( true == isset( $this->m_intProcessedAgainBy ) ) ? ( string ) $this->m_intProcessedAgainBy : 'NULL';
	}

	public function setProcessedAgainOn( $strProcessedAgainOn ) {
		$this->set( 'm_strProcessedAgainOn', CStrings::strTrimDef( $strProcessedAgainOn, -1, NULL, true ) );
	}

	public function getProcessedAgainOn() {
		return $this->m_strProcessedAgainOn;
	}

	public function sqlProcessedAgainOn() {
		return ( true == isset( $this->m_strProcessedAgainOn ) ) ? '\'' . $this->m_strProcessedAgainOn . '\'' : 'NULL';
	}

	public function setProcessedAgainOnTime( $strProcessedAgainOnTime ) {
		$this->set( 'm_strProcessedAgainOnTime', CStrings::strTrimDef( $strProcessedAgainOnTime, NULL, NULL, true ) );
	}

	public function getProcessedAgainOnTime() {
		return $this->m_strProcessedAgainOnTime;
	}

	public function sqlProcessedAgainOnTime() {
		return ( true == isset( $this->m_strProcessedAgainOnTime ) ) ? '\'' . $this->m_strProcessedAgainOnTime . '\'' : 'NULL';
	}

	public function setStartReviewDatetime( $strStartReviewDatetime ) {
		$this->set( 'm_strStartReviewDatetime', CStrings::strTrimDef( $strStartReviewDatetime, -1, NULL, true ) );
	}

	public function getStartReviewDatetime() {
		return $this->m_strStartReviewDatetime;
	}

	public function sqlStartReviewDatetime() {
		return ( true == isset( $this->m_strStartReviewDatetime ) ) ? '\'' . $this->m_strStartReviewDatetime . '\'' : 'NULL';
	}

	public function setEndReviewDatetime( $strEndReviewDatetime ) {
		$this->set( 'm_strEndReviewDatetime', CStrings::strTrimDef( $strEndReviewDatetime, -1, NULL, true ) );
	}

	public function getEndReviewDatetime() {
		return $this->m_strEndReviewDatetime;
	}

	public function sqlEndReviewDatetime() {
		return ( true == isset( $this->m_strEndReviewDatetime ) ) ? '\'' . $this->m_strEndReviewDatetime . '\'' : 'NULL';
	}

	public function setLastReviewedBy( $intLastReviewedBy ) {
		$this->set( 'm_intLastReviewedBy', CStrings::strToIntDef( $intLastReviewedBy, NULL, false ) );
	}

	public function getLastReviewedBy() {
		return $this->m_intLastReviewedBy;
	}

	public function sqlLastReviewedBy() {
		return ( true == isset( $this->m_intLastReviewedBy ) ) ? ( string ) $this->m_intLastReviewedBy : 'NULL';
	}

	public function setReviewedBy( $intReviewedBy ) {
		$this->set( 'm_intReviewedBy', CStrings::strToIntDef( $intReviewedBy, NULL, false ) );
	}

	public function getReviewedBy() {
		return $this->m_intReviewedBy;
	}

	public function sqlReviewedBy() {
		return ( true == isset( $this->m_intReviewedBy ) ) ? ( string ) $this->m_intReviewedBy : 'NULL';
	}

	public function setReviewedOn( $strReviewedOn ) {
		$this->set( 'm_strReviewedOn', CStrings::strTrimDef( $strReviewedOn, -1, NULL, true ) );
	}

	public function getReviewedOn() {
		return $this->m_strReviewedOn;
	}

	public function sqlReviewedOn() {
		return ( true == isset( $this->m_strReviewedOn ) ) ? '\'' . $this->m_strReviewedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOcrTemplateId( $intOcrTemplateId ) {
		$this->set( 'm_intOcrTemplateId', CStrings::strToIntDef( $intOcrTemplateId, NULL, false ) );
	}

	public function getOcrTemplateId() {
		return $this->m_intOcrTemplateId;
	}

	public function sqlOcrTemplateId() {
		return ( true == isset( $this->m_intOcrTemplateId ) ) ? ( string ) $this->m_intOcrTemplateId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setUtilityBillReceiptTypeId( $intUtilityBillReceiptTypeId ) {
		$this->set( 'm_intUtilityBillReceiptTypeId', CStrings::strToIntDef( $intUtilityBillReceiptTypeId, NULL, false ) );
	}

	public function getUtilityBillReceiptTypeId() {
		return $this->m_intUtilityBillReceiptTypeId;
	}

	public function sqlUtilityBillReceiptTypeId() {
		return ( true == isset( $this->m_intUtilityBillReceiptTypeId ) ) ? ( string ) $this->m_intUtilityBillReceiptTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, implementation_web_retrieval_id, summary_implementation_utility_bill_id, summary_implementation_utility_bill_page_number, review_implementation_utility_bill_id, utility_document_id, utility_bill_account_id, bill_datetime, is_queue_ignored, bill_processing_details, start_process_datetime, end_process_datetime, last_processed_by, processed_by, processed_on, processed_on_time, processed_again_by, processed_again_on, processed_again_on_time, start_review_datetime, end_review_datetime, last_reviewed_by, reviewed_by, reviewed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, ocr_template_id, utility_bill_id, utility_bill_receipt_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlImplementationWebRetrievalId() . ', ' .
						$this->sqlSummaryImplementationUtilityBillId() . ', ' .
						$this->sqlSummaryImplementationUtilityBillPageNumber() . ', ' .
						$this->sqlReviewImplementationUtilityBillId() . ', ' .
						$this->sqlUtilityDocumentId() . ', ' .
						$this->sqlUtilityBillAccountId() . ', ' .
						$this->sqlBillDatetime() . ', ' .
						$this->sqlIsQueueIgnored() . ', ' .
						$this->sqlBillProcessingDetails() . ', ' .
						$this->sqlStartProcessDatetime() . ', ' .
						$this->sqlEndProcessDatetime() . ', ' .
						$this->sqlLastProcessedBy() . ', ' .
						$this->sqlProcessedBy() . ', ' .
						$this->sqlProcessedOn() . ', ' .
						$this->sqlProcessedOnTime() . ', ' .
						$this->sqlProcessedAgainBy() . ', ' .
						$this->sqlProcessedAgainOn() . ', ' .
						$this->sqlProcessedAgainOnTime() . ', ' .
						$this->sqlStartReviewDatetime() . ', ' .
						$this->sqlEndReviewDatetime() . ', ' .
						$this->sqlLastReviewedBy() . ', ' .
						$this->sqlReviewedBy() . ', ' .
						$this->sqlReviewedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlOcrTemplateId() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlUtilityBillReceiptTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_web_retrieval_id = ' . $this->sqlImplementationWebRetrievalId(). ',' ; } elseif( true == array_key_exists( 'ImplementationWebRetrievalId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_web_retrieval_id = ' . $this->sqlImplementationWebRetrievalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary_implementation_utility_bill_id = ' . $this->sqlSummaryImplementationUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'SummaryImplementationUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' summary_implementation_utility_bill_id = ' . $this->sqlSummaryImplementationUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary_implementation_utility_bill_page_number = ' . $this->sqlSummaryImplementationUtilityBillPageNumber(). ',' ; } elseif( true == array_key_exists( 'SummaryImplementationUtilityBillPageNumber', $this->getChangedColumns() ) ) { $strSql .= ' summary_implementation_utility_bill_page_number = ' . $this->sqlSummaryImplementationUtilityBillPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_implementation_utility_bill_id = ' . $this->sqlReviewImplementationUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'ReviewImplementationUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' review_implementation_utility_bill_id = ' . $this->sqlReviewImplementationUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId(). ',' ; } elseif( true == array_key_exists( 'UtilityDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_datetime = ' . $this->sqlBillDatetime(). ',' ; } elseif( true == array_key_exists( 'BillDatetime', $this->getChangedColumns() ) ) { $strSql .= ' bill_datetime = ' . $this->sqlBillDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_queue_ignored = ' . $this->sqlIsQueueIgnored(). ',' ; } elseif( true == array_key_exists( 'IsQueueIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_queue_ignored = ' . $this->sqlIsQueueIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_processing_details = ' . $this->sqlBillProcessingDetails(). ',' ; } elseif( true == array_key_exists( 'BillProcessingDetails', $this->getChangedColumns() ) ) { $strSql .= ' bill_processing_details = ' . $this->sqlBillProcessingDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_process_datetime = ' . $this->sqlStartProcessDatetime(). ',' ; } elseif( true == array_key_exists( 'StartProcessDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_process_datetime = ' . $this->sqlStartProcessDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_process_datetime = ' . $this->sqlEndProcessDatetime(). ',' ; } elseif( true == array_key_exists( 'EndProcessDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_process_datetime = ' . $this->sqlEndProcessDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_processed_by = ' . $this->sqlLastProcessedBy(). ',' ; } elseif( true == array_key_exists( 'LastProcessedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_processed_by = ' . $this->sqlLastProcessedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy(). ',' ; } elseif( true == array_key_exists( 'ProcessedBy', $this->getChangedColumns() ) ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn(). ',' ; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on_time = ' . $this->sqlProcessedOnTime(). ',' ; } elseif( true == array_key_exists( 'ProcessedOnTime', $this->getChangedColumns() ) ) { $strSql .= ' processed_on_time = ' . $this->sqlProcessedOnTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_again_by = ' . $this->sqlProcessedAgainBy(). ',' ; } elseif( true == array_key_exists( 'ProcessedAgainBy', $this->getChangedColumns() ) ) { $strSql .= ' processed_again_by = ' . $this->sqlProcessedAgainBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_again_on = ' . $this->sqlProcessedAgainOn(). ',' ; } elseif( true == array_key_exists( 'ProcessedAgainOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_again_on = ' . $this->sqlProcessedAgainOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_again_on_time = ' . $this->sqlProcessedAgainOnTime(). ',' ; } elseif( true == array_key_exists( 'ProcessedAgainOnTime', $this->getChangedColumns() ) ) { $strSql .= ' processed_again_on_time = ' . $this->sqlProcessedAgainOnTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_review_datetime = ' . $this->sqlStartReviewDatetime(). ',' ; } elseif( true == array_key_exists( 'StartReviewDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_review_datetime = ' . $this->sqlStartReviewDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_review_datetime = ' . $this->sqlEndReviewDatetime(). ',' ; } elseif( true == array_key_exists( 'EndReviewDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_review_datetime = ' . $this->sqlEndReviewDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_reviewed_by = ' . $this->sqlLastReviewedBy(). ',' ; } elseif( true == array_key_exists( 'LastReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_reviewed_by = ' . $this->sqlLastReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy(). ',' ; } elseif( true == array_key_exists( 'ReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn(). ',' ; } elseif( true == array_key_exists( 'ReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_template_id = ' . $this->sqlOcrTemplateId(). ',' ; } elseif( true == array_key_exists( 'OcrTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_template_id = ' . $this->sqlOcrTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_receipt_type_id = ' . $this->sqlUtilityBillReceiptTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillReceiptTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_receipt_type_id = ' . $this->sqlUtilityBillReceiptTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'implementation_web_retrieval_id' => $this->getImplementationWebRetrievalId(),
			'summary_implementation_utility_bill_id' => $this->getSummaryImplementationUtilityBillId(),
			'summary_implementation_utility_bill_page_number' => $this->getSummaryImplementationUtilityBillPageNumber(),
			'review_implementation_utility_bill_id' => $this->getReviewImplementationUtilityBillId(),
			'utility_document_id' => $this->getUtilityDocumentId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'bill_datetime' => $this->getBillDatetime(),
			'is_queue_ignored' => $this->getIsQueueIgnored(),
			'bill_processing_details' => $this->getBillProcessingDetails(),
			'start_process_datetime' => $this->getStartProcessDatetime(),
			'end_process_datetime' => $this->getEndProcessDatetime(),
			'last_processed_by' => $this->getLastProcessedBy(),
			'processed_by' => $this->getProcessedBy(),
			'processed_on' => $this->getProcessedOn(),
			'processed_on_time' => $this->getProcessedOnTime(),
			'processed_again_by' => $this->getProcessedAgainBy(),
			'processed_again_on' => $this->getProcessedAgainOn(),
			'processed_again_on_time' => $this->getProcessedAgainOnTime(),
			'start_review_datetime' => $this->getStartReviewDatetime(),
			'end_review_datetime' => $this->getEndReviewDatetime(),
			'last_reviewed_by' => $this->getLastReviewedBy(),
			'reviewed_by' => $this->getReviewedBy(),
			'reviewed_on' => $this->getReviewedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ocr_template_id' => $this->getOcrTemplateId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'utility_bill_receipt_type_id' => $this->getUtilityBillReceiptTypeId()
		);
	}

}
?>