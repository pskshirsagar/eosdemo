<?php

class CBaseUtilityChangeRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_change_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityChangeRequestTypeId;
	protected $m_intEmployeeId;
	protected $m_intCompanyEmployeeId;
	protected $m_intManagerCompanyEmployeeId;
	protected $m_intUtilityAccountId;
	protected $m_intUtilityInvoiceId;
	protected $m_intUtilityTransactionId;
	protected $m_intArCodeId;
	protected $m_strChangeRequest;
	protected $m_strAdminNotes;
	protected $m_boolIsClientRequest;
	protected $m_boolIsResidentRequest;
	protected $m_boolIsPsRequest;
	protected $m_boolRequiresApproval;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intProcessedBy;
	protected $m_strProcessedOn;
	protected $m_intRejectedBy;
	protected $m_strRejectedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsClientRequest = false;
		$this->m_boolIsResidentRequest = false;
		$this->m_boolIsPsRequest = false;
		$this->m_boolRequiresApproval = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_change_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityChangeRequestTypeId', trim( $arrValues['utility_change_request_type_id'] ) ); elseif( isset( $arrValues['utility_change_request_type_id'] ) ) $this->setUtilityChangeRequestTypeId( $arrValues['utility_change_request_type_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['manager_company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerCompanyEmployeeId', trim( $arrValues['manager_company_employee_id'] ) ); elseif( isset( $arrValues['manager_company_employee_id'] ) ) $this->setManagerCompanyEmployeeId( $arrValues['manager_company_employee_id'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['utility_invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityInvoiceId', trim( $arrValues['utility_invoice_id'] ) ); elseif( isset( $arrValues['utility_invoice_id'] ) ) $this->setUtilityInvoiceId( $arrValues['utility_invoice_id'] );
		if( isset( $arrValues['utility_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTransactionId', trim( $arrValues['utility_transaction_id'] ) ); elseif( isset( $arrValues['utility_transaction_id'] ) ) $this->setUtilityTransactionId( $arrValues['utility_transaction_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['change_request'] ) && $boolDirectSet ) $this->set( 'm_strChangeRequest', trim( stripcslashes( $arrValues['change_request'] ) ) ); elseif( isset( $arrValues['change_request'] ) ) $this->setChangeRequest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['change_request'] ) : $arrValues['change_request'] );
		if( isset( $arrValues['admin_notes'] ) && $boolDirectSet ) $this->set( 'm_strAdminNotes', trim( stripcslashes( $arrValues['admin_notes'] ) ) ); elseif( isset( $arrValues['admin_notes'] ) ) $this->setAdminNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['admin_notes'] ) : $arrValues['admin_notes'] );
		if( isset( $arrValues['is_client_request'] ) && $boolDirectSet ) $this->set( 'm_boolIsClientRequest', trim( stripcslashes( $arrValues['is_client_request'] ) ) ); elseif( isset( $arrValues['is_client_request'] ) ) $this->setIsClientRequest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_client_request'] ) : $arrValues['is_client_request'] );
		if( isset( $arrValues['is_resident_request'] ) && $boolDirectSet ) $this->set( 'm_boolIsResidentRequest', trim( stripcslashes( $arrValues['is_resident_request'] ) ) ); elseif( isset( $arrValues['is_resident_request'] ) ) $this->setIsResidentRequest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_resident_request'] ) : $arrValues['is_resident_request'] );
		if( isset( $arrValues['is_ps_request'] ) && $boolDirectSet ) $this->set( 'm_boolIsPsRequest', trim( stripcslashes( $arrValues['is_ps_request'] ) ) ); elseif( isset( $arrValues['is_ps_request'] ) ) $this->setIsPsRequest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ps_request'] ) : $arrValues['is_ps_request'] );
		if( isset( $arrValues['requires_approval'] ) && $boolDirectSet ) $this->set( 'm_boolRequiresApproval', trim( stripcslashes( $arrValues['requires_approval'] ) ) ); elseif( isset( $arrValues['requires_approval'] ) ) $this->setRequiresApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requires_approval'] ) : $arrValues['requires_approval'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['processed_by'] ) && $boolDirectSet ) $this->set( 'm_intProcessedBy', trim( $arrValues['processed_by'] ) ); elseif( isset( $arrValues['processed_by'] ) ) $this->setProcessedBy( $arrValues['processed_by'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['rejected_by'] ) && $boolDirectSet ) $this->set( 'm_intRejectedBy', trim( $arrValues['rejected_by'] ) ); elseif( isset( $arrValues['rejected_by'] ) ) $this->setRejectedBy( $arrValues['rejected_by'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityChangeRequestTypeId( $intUtilityChangeRequestTypeId ) {
		$this->set( 'm_intUtilityChangeRequestTypeId', CStrings::strToIntDef( $intUtilityChangeRequestTypeId, NULL, false ) );
	}

	public function getUtilityChangeRequestTypeId() {
		return $this->m_intUtilityChangeRequestTypeId;
	}

	public function sqlUtilityChangeRequestTypeId() {
		return ( true == isset( $this->m_intUtilityChangeRequestTypeId ) ) ? ( string ) $this->m_intUtilityChangeRequestTypeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setManagerCompanyEmployeeId( $intManagerCompanyEmployeeId ) {
		$this->set( 'm_intManagerCompanyEmployeeId', CStrings::strToIntDef( $intManagerCompanyEmployeeId, NULL, false ) );
	}

	public function getManagerCompanyEmployeeId() {
		return $this->m_intManagerCompanyEmployeeId;
	}

	public function sqlManagerCompanyEmployeeId() {
		return ( true == isset( $this->m_intManagerCompanyEmployeeId ) ) ? ( string ) $this->m_intManagerCompanyEmployeeId : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setUtilityInvoiceId( $intUtilityInvoiceId ) {
		$this->set( 'm_intUtilityInvoiceId', CStrings::strToIntDef( $intUtilityInvoiceId, NULL, false ) );
	}

	public function getUtilityInvoiceId() {
		return $this->m_intUtilityInvoiceId;
	}

	public function sqlUtilityInvoiceId() {
		return ( true == isset( $this->m_intUtilityInvoiceId ) ) ? ( string ) $this->m_intUtilityInvoiceId : 'NULL';
	}

	public function setUtilityTransactionId( $intUtilityTransactionId ) {
		$this->set( 'm_intUtilityTransactionId', CStrings::strToIntDef( $intUtilityTransactionId, NULL, false ) );
	}

	public function getUtilityTransactionId() {
		return $this->m_intUtilityTransactionId;
	}

	public function sqlUtilityTransactionId() {
		return ( true == isset( $this->m_intUtilityTransactionId ) ) ? ( string ) $this->m_intUtilityTransactionId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setChangeRequest( $strChangeRequest ) {
		$this->set( 'm_strChangeRequest', CStrings::strTrimDef( $strChangeRequest, -1, NULL, true ) );
	}

	public function getChangeRequest() {
		return $this->m_strChangeRequest;
	}

	public function sqlChangeRequest() {
		return ( true == isset( $this->m_strChangeRequest ) ) ? '\'' . addslashes( $this->m_strChangeRequest ) . '\'' : 'NULL';
	}

	public function setAdminNotes( $strAdminNotes ) {
		$this->set( 'm_strAdminNotes', CStrings::strTrimDef( $strAdminNotes, -1, NULL, true ) );
	}

	public function getAdminNotes() {
		return $this->m_strAdminNotes;
	}

	public function sqlAdminNotes() {
		return ( true == isset( $this->m_strAdminNotes ) ) ? '\'' . addslashes( $this->m_strAdminNotes ) . '\'' : 'NULL';
	}

	public function setIsClientRequest( $boolIsClientRequest ) {
		$this->set( 'm_boolIsClientRequest', CStrings::strToBool( $boolIsClientRequest ) );
	}

	public function getIsClientRequest() {
		return $this->m_boolIsClientRequest;
	}

	public function sqlIsClientRequest() {
		return ( true == isset( $this->m_boolIsClientRequest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClientRequest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResidentRequest( $boolIsResidentRequest ) {
		$this->set( 'm_boolIsResidentRequest', CStrings::strToBool( $boolIsResidentRequest ) );
	}

	public function getIsResidentRequest() {
		return $this->m_boolIsResidentRequest;
	}

	public function sqlIsResidentRequest() {
		return ( true == isset( $this->m_boolIsResidentRequest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResidentRequest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPsRequest( $boolIsPsRequest ) {
		$this->set( 'm_boolIsPsRequest', CStrings::strToBool( $boolIsPsRequest ) );
	}

	public function getIsPsRequest() {
		return $this->m_boolIsPsRequest;
	}

	public function sqlIsPsRequest() {
		return ( true == isset( $this->m_boolIsPsRequest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPsRequest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequiresApproval( $boolRequiresApproval ) {
		$this->set( 'm_boolRequiresApproval', CStrings::strToBool( $boolRequiresApproval ) );
	}

	public function getRequiresApproval() {
		return $this->m_boolRequiresApproval;
	}

	public function sqlRequiresApproval() {
		return ( true == isset( $this->m_boolRequiresApproval ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequiresApproval ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setProcessedBy( $intProcessedBy ) {
		$this->set( 'm_intProcessedBy', CStrings::strToIntDef( $intProcessedBy, NULL, false ) );
	}

	public function getProcessedBy() {
		return $this->m_intProcessedBy;
	}

	public function sqlProcessedBy() {
		return ( true == isset( $this->m_intProcessedBy ) ) ? ( string ) $this->m_intProcessedBy : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setRejectedBy( $intRejectedBy ) {
		$this->set( 'm_intRejectedBy', CStrings::strToIntDef( $intRejectedBy, NULL, false ) );
	}

	public function getRejectedBy() {
		return $this->m_intRejectedBy;
	}

	public function sqlRejectedBy() {
		return ( true == isset( $this->m_intRejectedBy ) ) ? ( string ) $this->m_intRejectedBy : 'NULL';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_change_request_type_id, employee_id, company_employee_id, manager_company_employee_id, utility_account_id, utility_invoice_id, utility_transaction_id, ar_code_id, change_request, admin_notes, is_client_request, is_resident_request, is_ps_request, requires_approval, approved_by, approved_on, processed_by, processed_on, rejected_by, rejected_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUtilityChangeRequestTypeId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlCompanyEmployeeId() . ', ' .
 						$this->sqlManagerCompanyEmployeeId() . ', ' .
 						$this->sqlUtilityAccountId() . ', ' .
 						$this->sqlUtilityInvoiceId() . ', ' .
 						$this->sqlUtilityTransactionId() . ', ' .
 						$this->sqlArCodeId() . ', ' .
 						$this->sqlChangeRequest() . ', ' .
 						$this->sqlAdminNotes() . ', ' .
 						$this->sqlIsClientRequest() . ', ' .
 						$this->sqlIsResidentRequest() . ', ' .
 						$this->sqlIsPsRequest() . ', ' .
 						$this->sqlRequiresApproval() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlProcessedBy() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
 						$this->sqlRejectedBy() . ', ' .
 						$this->sqlRejectedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_change_request_type_id = ' . $this->sqlUtilityChangeRequestTypeId() . ','; } elseif( true == array_key_exists( 'UtilityChangeRequestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_change_request_type_id = ' . $this->sqlUtilityChangeRequestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_company_employee_id = ' . $this->sqlManagerCompanyEmployeeId() . ','; } elseif( true == array_key_exists( 'ManagerCompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' manager_company_employee_id = ' . $this->sqlManagerCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_invoice_id = ' . $this->sqlUtilityInvoiceId() . ','; } elseif( true == array_key_exists( 'UtilityInvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' utility_invoice_id = ' . $this->sqlUtilityInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_transaction_id = ' . $this->sqlUtilityTransactionId() . ','; } elseif( true == array_key_exists( 'UtilityTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_transaction_id = ' . $this->sqlUtilityTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_request = ' . $this->sqlChangeRequest() . ','; } elseif( true == array_key_exists( 'ChangeRequest', $this->getChangedColumns() ) ) { $strSql .= ' change_request = ' . $this->sqlChangeRequest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' admin_notes = ' . $this->sqlAdminNotes() . ','; } elseif( true == array_key_exists( 'AdminNotes', $this->getChangedColumns() ) ) { $strSql .= ' admin_notes = ' . $this->sqlAdminNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_request = ' . $this->sqlIsClientRequest() . ','; } elseif( true == array_key_exists( 'IsClientRequest', $this->getChangedColumns() ) ) { $strSql .= ' is_client_request = ' . $this->sqlIsClientRequest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_resident_request = ' . $this->sqlIsResidentRequest() . ','; } elseif( true == array_key_exists( 'IsResidentRequest', $this->getChangedColumns() ) ) { $strSql .= ' is_resident_request = ' . $this->sqlIsResidentRequest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ps_request = ' . $this->sqlIsPsRequest() . ','; } elseif( true == array_key_exists( 'IsPsRequest', $this->getChangedColumns() ) ) { $strSql .= ' is_ps_request = ' . $this->sqlIsPsRequest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_approval = ' . $this->sqlRequiresApproval() . ','; } elseif( true == array_key_exists( 'RequiresApproval', $this->getChangedColumns() ) ) { $strSql .= ' requires_approval = ' . $this->sqlRequiresApproval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy() . ','; } elseif( true == array_key_exists( 'ProcessedBy', $this->getChangedColumns() ) ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; } elseif( true == array_key_exists( 'RejectedBy', $this->getChangedColumns() ) ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_change_request_type_id' => $this->getUtilityChangeRequestTypeId(),
			'employee_id' => $this->getEmployeeId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'manager_company_employee_id' => $this->getManagerCompanyEmployeeId(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'utility_invoice_id' => $this->getUtilityInvoiceId(),
			'utility_transaction_id' => $this->getUtilityTransactionId(),
			'ar_code_id' => $this->getArCodeId(),
			'change_request' => $this->getChangeRequest(),
			'admin_notes' => $this->getAdminNotes(),
			'is_client_request' => $this->getIsClientRequest(),
			'is_resident_request' => $this->getIsResidentRequest(),
			'is_ps_request' => $this->getIsPsRequest(),
			'requires_approval' => $this->getRequiresApproval(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'processed_by' => $this->getProcessedBy(),
			'processed_on' => $this->getProcessedOn(),
			'rejected_by' => $this->getRejectedBy(),
			'rejected_on' => $this->getRejectedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>