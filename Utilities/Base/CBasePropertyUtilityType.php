<?php

class CBasePropertyUtilityType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_utility_types';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyBuildingId;
	protected $m_intUtilityTypeId;
	protected $m_intUtilityTaxRateId;
	protected $m_intMoveInArCodeId;
	protected $m_intTerminationArCodeId;
	protected $m_intBillingFeeArCodeId;
	protected $m_intCapArCodeId;
	protected $m_intSubsidyArCodeId;
	protected $m_intTaxRateArCodeId;
	protected $m_intArCodeId;
	protected $m_intApPayeeId;
	protected $m_intUtilityBillPeriodId;
	protected $m_intUtilityConsumptionTypeId;
	protected $m_intBenchmarkPropertyUtilityTypeId;
	protected $m_intMoveInFeeExportGroupId;
	protected $m_intTerminationFeeExportGroupId;
	protected $m_intBillingFeeExportGroupId;
	protected $m_intCapExportGroupId;
	protected $m_intSubsidyExportGroupId;
	protected $m_intTaxRateExportGroupId;
	protected $m_intExportGroupId;
	protected $m_intBillingServicePeriodTypeId;
	protected $m_intBillingServicePropertyUtilityTypeId;
	protected $m_intBillingServicePeriodUtilityBillAccountId;
	protected $m_strMoveInFeeDescription;
	protected $m_strTerminationFeeDescription;
	protected $m_strBillingFeeDescription;
	protected $m_strCapDescription;
	protected $m_strSubsidyDescription;
	protected $m_strChargeCodeDescription;
	protected $m_strTaxRateDescription;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strBillingFeeLabel;
	protected $m_fltBaseFeeConsumptionUnits;
	protected $m_fltBaseFeePerUnit;
	protected $m_strBaseFeePerUnitChangeDate;
	protected $m_fltLastRatePerConsumptionUnit;
	protected $m_strNotes;
	protected $m_boolHasBillingService;
	protected $m_boolHasVcrService;
	protected $m_boolIsSubmetered;
	protected $m_boolIsManual;
	protected $m_boolIsFixed;
	protected $m_boolServicePeriodExcludeEndDay;
	protected $m_boolDontRequireBills;
	protected $m_boolBillByBuilding;
	protected $m_boolUseTotalUnits;
	protected $m_boolUseTotalSqft;
	protected $m_boolDontTaxUsage;
	protected $m_boolDontTaxBaseFees;
	protected $m_boolDontTaxBillingFees;
	protected $m_intBillingServicePeriodStartDay;
	protected $m_intBillingServicePeriodEndDay;
	protected $m_intBillingServiceMonth;
	protected $m_fltGrossRecaptureThreshold;
	protected $m_fltBillableRecaptureThreshold;
	protected $m_fltUtilityChargesThreshold;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strRegistrationDate;
	protected $m_strRenewalDate;
	protected $m_arrintUtilityDocumentsIds;
	protected $m_boolBillingPeriodExcludeEndDay;
	protected $m_intUtilityBaseFeeCalculationTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_boolHasBillingService = true;
		$this->m_boolHasVcrService = false;
		$this->m_boolIsSubmetered = false;
		$this->m_boolIsManual = false;
		$this->m_boolIsFixed = false;
		$this->m_boolServicePeriodExcludeEndDay = false;
		$this->m_boolDontRequireBills = false;
		$this->m_boolBillByBuilding = false;
		$this->m_boolUseTotalUnits = false;
		$this->m_boolUseTotalSqft = false;
		$this->m_boolDontTaxUsage = false;
		$this->m_boolDontTaxBaseFees = false;
		$this->m_boolDontTaxBillingFees = false;
		$this->m_fltGrossRecaptureThreshold = '10';
		$this->m_fltUtilityChargesThreshold = '20';
		$this->m_boolBillingPeriodExcludeEndDay = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeId', trim( $arrValues['utility_type_id'] ) ); elseif( isset( $arrValues['utility_type_id'] ) ) $this->setUtilityTypeId( $arrValues['utility_type_id'] );
		if( isset( $arrValues['utility_tax_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTaxRateId', trim( $arrValues['utility_tax_rate_id'] ) ); elseif( isset( $arrValues['utility_tax_rate_id'] ) ) $this->setUtilityTaxRateId( $arrValues['utility_tax_rate_id'] );
		if( isset( $arrValues['move_in_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveInArCodeId', trim( $arrValues['move_in_ar_code_id'] ) ); elseif( isset( $arrValues['move_in_ar_code_id'] ) ) $this->setMoveInArCodeId( $arrValues['move_in_ar_code_id'] );
		if( isset( $arrValues['termination_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intTerminationArCodeId', trim( $arrValues['termination_ar_code_id'] ) ); elseif( isset( $arrValues['termination_ar_code_id'] ) ) $this->setTerminationArCodeId( $arrValues['termination_ar_code_id'] );
		if( isset( $arrValues['billing_fee_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingFeeArCodeId', trim( $arrValues['billing_fee_ar_code_id'] ) ); elseif( isset( $arrValues['billing_fee_ar_code_id'] ) ) $this->setBillingFeeArCodeId( $arrValues['billing_fee_ar_code_id'] );
		if( isset( $arrValues['cap_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intCapArCodeId', trim( $arrValues['cap_ar_code_id'] ) ); elseif( isset( $arrValues['cap_ar_code_id'] ) ) $this->setCapArCodeId( $arrValues['cap_ar_code_id'] );
		if( isset( $arrValues['subsidy_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyArCodeId', trim( $arrValues['subsidy_ar_code_id'] ) ); elseif( isset( $arrValues['subsidy_ar_code_id'] ) ) $this->setSubsidyArCodeId( $arrValues['subsidy_ar_code_id'] );
		if( isset( $arrValues['tax_rate_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxRateArCodeId', trim( $arrValues['tax_rate_ar_code_id'] ) ); elseif( isset( $arrValues['tax_rate_ar_code_id'] ) ) $this->setTaxRateArCodeId( $arrValues['tax_rate_ar_code_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['utility_bill_period_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillPeriodId', trim( $arrValues['utility_bill_period_id'] ) ); elseif( isset( $arrValues['utility_bill_period_id'] ) ) $this->setUtilityBillPeriodId( $arrValues['utility_bill_period_id'] );
		if( isset( $arrValues['utility_consumption_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityConsumptionTypeId', trim( $arrValues['utility_consumption_type_id'] ) ); elseif( isset( $arrValues['utility_consumption_type_id'] ) ) $this->setUtilityConsumptionTypeId( $arrValues['utility_consumption_type_id'] );
		if( isset( $arrValues['benchmark_property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBenchmarkPropertyUtilityTypeId', trim( $arrValues['benchmark_property_utility_type_id'] ) ); elseif( isset( $arrValues['benchmark_property_utility_type_id'] ) ) $this->setBenchmarkPropertyUtilityTypeId( $arrValues['benchmark_property_utility_type_id'] );
		if( isset( $arrValues['move_in_fee_export_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveInFeeExportGroupId', trim( $arrValues['move_in_fee_export_group_id'] ) ); elseif( isset( $arrValues['move_in_fee_export_group_id'] ) ) $this->setMoveInFeeExportGroupId( $arrValues['move_in_fee_export_group_id'] );
		if( isset( $arrValues['termination_fee_export_group_id'] ) && $boolDirectSet ) $this->set( 'm_intTerminationFeeExportGroupId', trim( $arrValues['termination_fee_export_group_id'] ) ); elseif( isset( $arrValues['termination_fee_export_group_id'] ) ) $this->setTerminationFeeExportGroupId( $arrValues['termination_fee_export_group_id'] );
		if( isset( $arrValues['billing_fee_export_group_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingFeeExportGroupId', trim( $arrValues['billing_fee_export_group_id'] ) ); elseif( isset( $arrValues['billing_fee_export_group_id'] ) ) $this->setBillingFeeExportGroupId( $arrValues['billing_fee_export_group_id'] );
		if( isset( $arrValues['cap_export_group_id'] ) && $boolDirectSet ) $this->set( 'm_intCapExportGroupId', trim( $arrValues['cap_export_group_id'] ) ); elseif( isset( $arrValues['cap_export_group_id'] ) ) $this->setCapExportGroupId( $arrValues['cap_export_group_id'] );
		if( isset( $arrValues['subsidy_export_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyExportGroupId', trim( $arrValues['subsidy_export_group_id'] ) ); elseif( isset( $arrValues['subsidy_export_group_id'] ) ) $this->setSubsidyExportGroupId( $arrValues['subsidy_export_group_id'] );
		if( isset( $arrValues['tax_rate_export_group_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxRateExportGroupId', trim( $arrValues['tax_rate_export_group_id'] ) ); elseif( isset( $arrValues['tax_rate_export_group_id'] ) ) $this->setTaxRateExportGroupId( $arrValues['tax_rate_export_group_id'] );
		if( isset( $arrValues['export_group_id'] ) && $boolDirectSet ) $this->set( 'm_intExportGroupId', trim( $arrValues['export_group_id'] ) ); elseif( isset( $arrValues['export_group_id'] ) ) $this->setExportGroupId( $arrValues['export_group_id'] );
		if( isset( $arrValues['billing_service_period_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingServicePeriodTypeId', trim( $arrValues['billing_service_period_type_id'] ) ); elseif( isset( $arrValues['billing_service_period_type_id'] ) ) $this->setBillingServicePeriodTypeId( $arrValues['billing_service_period_type_id'] );
		if( isset( $arrValues['billing_service_property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingServicePropertyUtilityTypeId', trim( $arrValues['billing_service_property_utility_type_id'] ) ); elseif( isset( $arrValues['billing_service_property_utility_type_id'] ) ) $this->setBillingServicePropertyUtilityTypeId( $arrValues['billing_service_property_utility_type_id'] );
		if( isset( $arrValues['billing_service_period_utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingServicePeriodUtilityBillAccountId', trim( $arrValues['billing_service_period_utility_bill_account_id'] ) ); elseif( isset( $arrValues['billing_service_period_utility_bill_account_id'] ) ) $this->setBillingServicePeriodUtilityBillAccountId( $arrValues['billing_service_period_utility_bill_account_id'] );
		if( isset( $arrValues['move_in_fee_description'] ) && $boolDirectSet ) $this->set( 'm_strMoveInFeeDescription', trim( $arrValues['move_in_fee_description'] ) ); elseif( isset( $arrValues['move_in_fee_description'] ) ) $this->setMoveInFeeDescription( $arrValues['move_in_fee_description'] );
		if( isset( $arrValues['termination_fee_description'] ) && $boolDirectSet ) $this->set( 'm_strTerminationFeeDescription', trim( $arrValues['termination_fee_description'] ) ); elseif( isset( $arrValues['termination_fee_description'] ) ) $this->setTerminationFeeDescription( $arrValues['termination_fee_description'] );
		if( isset( $arrValues['billing_fee_description'] ) && $boolDirectSet ) $this->set( 'm_strBillingFeeDescription', trim( $arrValues['billing_fee_description'] ) ); elseif( isset( $arrValues['billing_fee_description'] ) ) $this->setBillingFeeDescription( $arrValues['billing_fee_description'] );
		if( isset( $arrValues['cap_description'] ) && $boolDirectSet ) $this->set( 'm_strCapDescription', trim( $arrValues['cap_description'] ) ); elseif( isset( $arrValues['cap_description'] ) ) $this->setCapDescription( $arrValues['cap_description'] );
		if( isset( $arrValues['subsidy_description'] ) && $boolDirectSet ) $this->set( 'm_strSubsidyDescription', trim( $arrValues['subsidy_description'] ) ); elseif( isset( $arrValues['subsidy_description'] ) ) $this->setSubsidyDescription( $arrValues['subsidy_description'] );
		if( isset( $arrValues['charge_code_description'] ) && $boolDirectSet ) $this->set( 'm_strChargeCodeDescription', trim( $arrValues['charge_code_description'] ) ); elseif( isset( $arrValues['charge_code_description'] ) ) $this->setChargeCodeDescription( $arrValues['charge_code_description'] );
		if( isset( $arrValues['tax_rate_description'] ) && $boolDirectSet ) $this->set( 'm_strTaxRateDescription', trim( $arrValues['tax_rate_description'] ) ); elseif( isset( $arrValues['tax_rate_description'] ) ) $this->setTaxRateDescription( $arrValues['tax_rate_description'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['billing_fee_label'] ) && $boolDirectSet ) $this->set( 'm_strBillingFeeLabel', trim( $arrValues['billing_fee_label'] ) ); elseif( isset( $arrValues['billing_fee_label'] ) ) $this->setBillingFeeLabel( $arrValues['billing_fee_label'] );
		if( isset( $arrValues['base_fee_consumption_units'] ) && $boolDirectSet ) $this->set( 'm_fltBaseFeeConsumptionUnits', trim( $arrValues['base_fee_consumption_units'] ) ); elseif( isset( $arrValues['base_fee_consumption_units'] ) ) $this->setBaseFeeConsumptionUnits( $arrValues['base_fee_consumption_units'] );
		if( isset( $arrValues['base_fee_per_unit'] ) && $boolDirectSet ) $this->set( 'm_fltBaseFeePerUnit', trim( $arrValues['base_fee_per_unit'] ) ); elseif( isset( $arrValues['base_fee_per_unit'] ) ) $this->setBaseFeePerUnit( $arrValues['base_fee_per_unit'] );
		if( isset( $arrValues['base_fee_per_unit_change_date'] ) && $boolDirectSet ) $this->set( 'm_strBaseFeePerUnitChangeDate', trim( $arrValues['base_fee_per_unit_change_date'] ) ); elseif( isset( $arrValues['base_fee_per_unit_change_date'] ) ) $this->setBaseFeePerUnitChangeDate( $arrValues['base_fee_per_unit_change_date'] );
		if( isset( $arrValues['last_rate_per_consumption_unit'] ) && $boolDirectSet ) $this->set( 'm_fltLastRatePerConsumptionUnit', trim( $arrValues['last_rate_per_consumption_unit'] ) ); elseif( isset( $arrValues['last_rate_per_consumption_unit'] ) ) $this->setLastRatePerConsumptionUnit( $arrValues['last_rate_per_consumption_unit'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['has_billing_service'] ) && $boolDirectSet ) $this->set( 'm_boolHasBillingService', trim( stripcslashes( $arrValues['has_billing_service'] ) ) ); elseif( isset( $arrValues['has_billing_service'] ) ) $this->setHasBillingService( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_billing_service'] ) : $arrValues['has_billing_service'] );
		if( isset( $arrValues['has_vcr_service'] ) && $boolDirectSet ) $this->set( 'm_boolHasVcrService', trim( stripcslashes( $arrValues['has_vcr_service'] ) ) ); elseif( isset( $arrValues['has_vcr_service'] ) ) $this->setHasVcrService( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_vcr_service'] ) : $arrValues['has_vcr_service'] );
		if( isset( $arrValues['is_submetered'] ) && $boolDirectSet ) $this->set( 'm_boolIsSubmetered', trim( stripcslashes( $arrValues['is_submetered'] ) ) ); elseif( isset( $arrValues['is_submetered'] ) ) $this->setIsSubmetered( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_submetered'] ) : $arrValues['is_submetered'] );
		if( isset( $arrValues['is_manual'] ) && $boolDirectSet ) $this->set( 'm_boolIsManual', trim( stripcslashes( $arrValues['is_manual'] ) ) ); elseif( isset( $arrValues['is_manual'] ) ) $this->setIsManual( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manual'] ) : $arrValues['is_manual'] );
		if( isset( $arrValues['is_fixed'] ) && $boolDirectSet ) $this->set( 'm_boolIsFixed', trim( stripcslashes( $arrValues['is_fixed'] ) ) ); elseif( isset( $arrValues['is_fixed'] ) ) $this->setIsFixed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_fixed'] ) : $arrValues['is_fixed'] );
		if( isset( $arrValues['service_period_exclude_end_day'] ) && $boolDirectSet ) $this->set( 'm_boolServicePeriodExcludeEndDay', trim( stripcslashes( $arrValues['service_period_exclude_end_day'] ) ) ); elseif( isset( $arrValues['service_period_exclude_end_day'] ) ) $this->setServicePeriodExcludeEndDay( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['service_period_exclude_end_day'] ) : $arrValues['service_period_exclude_end_day'] );
		if( isset( $arrValues['dont_require_bills'] ) && $boolDirectSet ) $this->set( 'm_boolDontRequireBills', trim( stripcslashes( $arrValues['dont_require_bills'] ) ) ); elseif( isset( $arrValues['dont_require_bills'] ) ) $this->setDontRequireBills( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_require_bills'] ) : $arrValues['dont_require_bills'] );
		if( isset( $arrValues['bill_by_building'] ) && $boolDirectSet ) $this->set( 'm_boolBillByBuilding', trim( stripcslashes( $arrValues['bill_by_building'] ) ) ); elseif( isset( $arrValues['bill_by_building'] ) ) $this->setBillByBuilding( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bill_by_building'] ) : $arrValues['bill_by_building'] );
		if( isset( $arrValues['use_total_units'] ) && $boolDirectSet ) $this->set( 'm_boolUseTotalUnits', trim( stripcslashes( $arrValues['use_total_units'] ) ) ); elseif( isset( $arrValues['use_total_units'] ) ) $this->setUseTotalUnits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_total_units'] ) : $arrValues['use_total_units'] );
		if( isset( $arrValues['use_total_sqft'] ) && $boolDirectSet ) $this->set( 'm_boolUseTotalSqft', trim( stripcslashes( $arrValues['use_total_sqft'] ) ) ); elseif( isset( $arrValues['use_total_sqft'] ) ) $this->setUseTotalSqft( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_total_sqft'] ) : $arrValues['use_total_sqft'] );
		if( isset( $arrValues['dont_tax_usage'] ) && $boolDirectSet ) $this->set( 'm_boolDontTaxUsage', trim( stripcslashes( $arrValues['dont_tax_usage'] ) ) ); elseif( isset( $arrValues['dont_tax_usage'] ) ) $this->setDontTaxUsage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_tax_usage'] ) : $arrValues['dont_tax_usage'] );
		if( isset( $arrValues['dont_tax_base_fees'] ) && $boolDirectSet ) $this->set( 'm_boolDontTaxBaseFees', trim( stripcslashes( $arrValues['dont_tax_base_fees'] ) ) ); elseif( isset( $arrValues['dont_tax_base_fees'] ) ) $this->setDontTaxBaseFees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_tax_base_fees'] ) : $arrValues['dont_tax_base_fees'] );
		if( isset( $arrValues['dont_tax_billing_fees'] ) && $boolDirectSet ) $this->set( 'm_boolDontTaxBillingFees', trim( stripcslashes( $arrValues['dont_tax_billing_fees'] ) ) ); elseif( isset( $arrValues['dont_tax_billing_fees'] ) ) $this->setDontTaxBillingFees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_tax_billing_fees'] ) : $arrValues['dont_tax_billing_fees'] );
		if( isset( $arrValues['billing_service_period_start_day'] ) && $boolDirectSet ) $this->set( 'm_intBillingServicePeriodStartDay', trim( $arrValues['billing_service_period_start_day'] ) ); elseif( isset( $arrValues['billing_service_period_start_day'] ) ) $this->setBillingServicePeriodStartDay( $arrValues['billing_service_period_start_day'] );
		if( isset( $arrValues['billing_service_period_end_day'] ) && $boolDirectSet ) $this->set( 'm_intBillingServicePeriodEndDay', trim( $arrValues['billing_service_period_end_day'] ) ); elseif( isset( $arrValues['billing_service_period_end_day'] ) ) $this->setBillingServicePeriodEndDay( $arrValues['billing_service_period_end_day'] );
		if( isset( $arrValues['billing_service_month'] ) && $boolDirectSet ) $this->set( 'm_intBillingServiceMonth', trim( $arrValues['billing_service_month'] ) ); elseif( isset( $arrValues['billing_service_month'] ) ) $this->setBillingServiceMonth( $arrValues['billing_service_month'] );
		if( isset( $arrValues['gross_recapture_threshold'] ) && $boolDirectSet ) $this->set( 'm_fltGrossRecaptureThreshold', trim( $arrValues['gross_recapture_threshold'] ) ); elseif( isset( $arrValues['gross_recapture_threshold'] ) ) $this->setGrossRecaptureThreshold( $arrValues['gross_recapture_threshold'] );
		if( isset( $arrValues['billable_recapture_threshold'] ) && $boolDirectSet ) $this->set( 'm_fltBillableRecaptureThreshold', trim( $arrValues['billable_recapture_threshold'] ) ); elseif( isset( $arrValues['billable_recapture_threshold'] ) ) $this->setBillableRecaptureThreshold( $arrValues['billable_recapture_threshold'] );
		if( isset( $arrValues['utility_charges_threshold'] ) && $boolDirectSet ) $this->set( 'm_fltUtilityChargesThreshold', trim( $arrValues['utility_charges_threshold'] ) ); elseif( isset( $arrValues['utility_charges_threshold'] ) ) $this->setUtilityChargesThreshold( $arrValues['utility_charges_threshold'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['registration_date'] ) && $boolDirectSet ) $this->set( 'm_strRegistrationDate', trim( $arrValues['registration_date'] ) ); elseif( isset( $arrValues['registration_date'] ) ) $this->setRegistrationDate( $arrValues['registration_date'] );
		if( isset( $arrValues['renewal_date'] ) && $boolDirectSet ) $this->set( 'm_strRenewalDate', trim( $arrValues['renewal_date'] ) ); elseif( isset( $arrValues['renewal_date'] ) ) $this->setRenewalDate( $arrValues['renewal_date'] );
		if( isset( $arrValues['utility_documents_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintUtilityDocumentsIds', trim( $arrValues['utility_documents_ids'] ) ); elseif( isset( $arrValues['utility_documents_ids'] ) ) $this->setUtilityDocumentsIds( $arrValues['utility_documents_ids'] );
		if( isset( $arrValues['billing_period_exclude_end_day'] ) && $boolDirectSet ) $this->set( 'm_boolBillingPeriodExcludeEndDay', trim( stripcslashes( $arrValues['billing_period_exclude_end_day'] ) ) ); elseif( isset( $arrValues['billing_period_exclude_end_day'] ) ) $this->setBillingPeriodExcludeEndDay( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billing_period_exclude_end_day'] ) : $arrValues['billing_period_exclude_end_day'] );
		if( isset( $arrValues['utility_base_fee_calculation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBaseFeeCalculationTypeId', trim( $arrValues['utility_base_fee_calculation_type_id'] ) ); elseif( isset( $arrValues['utility_base_fee_calculation_type_id'] ) ) $this->setUtilityBaseFeeCalculationTypeId( $arrValues['utility_base_fee_calculation_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->set( 'm_intUtilityTypeId', CStrings::strToIntDef( $intUtilityTypeId, NULL, false ) );
	}

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function sqlUtilityTypeId() {
		return ( true == isset( $this->m_intUtilityTypeId ) ) ? ( string ) $this->m_intUtilityTypeId : 'NULL';
	}

	public function setUtilityTaxRateId( $intUtilityTaxRateId ) {
		$this->set( 'm_intUtilityTaxRateId', CStrings::strToIntDef( $intUtilityTaxRateId, NULL, false ) );
	}

	public function getUtilityTaxRateId() {
		return $this->m_intUtilityTaxRateId;
	}

	public function sqlUtilityTaxRateId() {
		return ( true == isset( $this->m_intUtilityTaxRateId ) ) ? ( string ) $this->m_intUtilityTaxRateId : 'NULL';
	}

	public function setMoveInArCodeId( $intMoveInArCodeId ) {
		$this->set( 'm_intMoveInArCodeId', CStrings::strToIntDef( $intMoveInArCodeId, NULL, false ) );
	}

	public function getMoveInArCodeId() {
		return $this->m_intMoveInArCodeId;
	}

	public function sqlMoveInArCodeId() {
		return ( true == isset( $this->m_intMoveInArCodeId ) ) ? ( string ) $this->m_intMoveInArCodeId : 'NULL';
	}

	public function setTerminationArCodeId( $intTerminationArCodeId ) {
		$this->set( 'm_intTerminationArCodeId', CStrings::strToIntDef( $intTerminationArCodeId, NULL, false ) );
	}

	public function getTerminationArCodeId() {
		return $this->m_intTerminationArCodeId;
	}

	public function sqlTerminationArCodeId() {
		return ( true == isset( $this->m_intTerminationArCodeId ) ) ? ( string ) $this->m_intTerminationArCodeId : 'NULL';
	}

	public function setBillingFeeArCodeId( $intBillingFeeArCodeId ) {
		$this->set( 'm_intBillingFeeArCodeId', CStrings::strToIntDef( $intBillingFeeArCodeId, NULL, false ) );
	}

	public function getBillingFeeArCodeId() {
		return $this->m_intBillingFeeArCodeId;
	}

	public function sqlBillingFeeArCodeId() {
		return ( true == isset( $this->m_intBillingFeeArCodeId ) ) ? ( string ) $this->m_intBillingFeeArCodeId : 'NULL';
	}

	public function setCapArCodeId( $intCapArCodeId ) {
		$this->set( 'm_intCapArCodeId', CStrings::strToIntDef( $intCapArCodeId, NULL, false ) );
	}

	public function getCapArCodeId() {
		return $this->m_intCapArCodeId;
	}

	public function sqlCapArCodeId() {
		return ( true == isset( $this->m_intCapArCodeId ) ) ? ( string ) $this->m_intCapArCodeId : 'NULL';
	}

	public function setSubsidyArCodeId( $intSubsidyArCodeId ) {
		$this->set( 'm_intSubsidyArCodeId', CStrings::strToIntDef( $intSubsidyArCodeId, NULL, false ) );
	}

	public function getSubsidyArCodeId() {
		return $this->m_intSubsidyArCodeId;
	}

	public function sqlSubsidyArCodeId() {
		return ( true == isset( $this->m_intSubsidyArCodeId ) ) ? ( string ) $this->m_intSubsidyArCodeId : 'NULL';
	}

	public function setTaxRateArCodeId( $intTaxRateArCodeId ) {
		$this->set( 'm_intTaxRateArCodeId', CStrings::strToIntDef( $intTaxRateArCodeId, NULL, false ) );
	}

	public function getTaxRateArCodeId() {
		return $this->m_intTaxRateArCodeId;
	}

	public function sqlTaxRateArCodeId() {
		return ( true == isset( $this->m_intTaxRateArCodeId ) ) ? ( string ) $this->m_intTaxRateArCodeId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setUtilityBillPeriodId( $intUtilityBillPeriodId ) {
		$this->set( 'm_intUtilityBillPeriodId', CStrings::strToIntDef( $intUtilityBillPeriodId, NULL, false ) );
	}

	public function getUtilityBillPeriodId() {
		return $this->m_intUtilityBillPeriodId;
	}

	public function sqlUtilityBillPeriodId() {
		return ( true == isset( $this->m_intUtilityBillPeriodId ) ) ? ( string ) $this->m_intUtilityBillPeriodId : 'NULL';
	}

	public function setUtilityConsumptionTypeId( $intUtilityConsumptionTypeId ) {
		$this->set( 'm_intUtilityConsumptionTypeId', CStrings::strToIntDef( $intUtilityConsumptionTypeId, NULL, false ) );
	}

	public function getUtilityConsumptionTypeId() {
		return $this->m_intUtilityConsumptionTypeId;
	}

	public function sqlUtilityConsumptionTypeId() {
		return ( true == isset( $this->m_intUtilityConsumptionTypeId ) ) ? ( string ) $this->m_intUtilityConsumptionTypeId : 'NULL';
	}

	public function setBenchmarkPropertyUtilityTypeId( $intBenchmarkPropertyUtilityTypeId ) {
		$this->set( 'm_intBenchmarkPropertyUtilityTypeId', CStrings::strToIntDef( $intBenchmarkPropertyUtilityTypeId, NULL, false ) );
	}

	public function getBenchmarkPropertyUtilityTypeId() {
		return $this->m_intBenchmarkPropertyUtilityTypeId;
	}

	public function sqlBenchmarkPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intBenchmarkPropertyUtilityTypeId ) ) ? ( string ) $this->m_intBenchmarkPropertyUtilityTypeId : 'NULL';
	}

	public function setMoveInFeeExportGroupId( $intMoveInFeeExportGroupId ) {
		$this->set( 'm_intMoveInFeeExportGroupId', CStrings::strToIntDef( $intMoveInFeeExportGroupId, NULL, false ) );
	}

	public function getMoveInFeeExportGroupId() {
		return $this->m_intMoveInFeeExportGroupId;
	}

	public function sqlMoveInFeeExportGroupId() {
		return ( true == isset( $this->m_intMoveInFeeExportGroupId ) ) ? ( string ) $this->m_intMoveInFeeExportGroupId : 'NULL';
	}

	public function setTerminationFeeExportGroupId( $intTerminationFeeExportGroupId ) {
		$this->set( 'm_intTerminationFeeExportGroupId', CStrings::strToIntDef( $intTerminationFeeExportGroupId, NULL, false ) );
	}

	public function getTerminationFeeExportGroupId() {
		return $this->m_intTerminationFeeExportGroupId;
	}

	public function sqlTerminationFeeExportGroupId() {
		return ( true == isset( $this->m_intTerminationFeeExportGroupId ) ) ? ( string ) $this->m_intTerminationFeeExportGroupId : 'NULL';
	}

	public function setBillingFeeExportGroupId( $intBillingFeeExportGroupId ) {
		$this->set( 'm_intBillingFeeExportGroupId', CStrings::strToIntDef( $intBillingFeeExportGroupId, NULL, false ) );
	}

	public function getBillingFeeExportGroupId() {
		return $this->m_intBillingFeeExportGroupId;
	}

	public function sqlBillingFeeExportGroupId() {
		return ( true == isset( $this->m_intBillingFeeExportGroupId ) ) ? ( string ) $this->m_intBillingFeeExportGroupId : 'NULL';
	}

	public function setCapExportGroupId( $intCapExportGroupId ) {
		$this->set( 'm_intCapExportGroupId', CStrings::strToIntDef( $intCapExportGroupId, NULL, false ) );
	}

	public function getCapExportGroupId() {
		return $this->m_intCapExportGroupId;
	}

	public function sqlCapExportGroupId() {
		return ( true == isset( $this->m_intCapExportGroupId ) ) ? ( string ) $this->m_intCapExportGroupId : 'NULL';
	}

	public function setSubsidyExportGroupId( $intSubsidyExportGroupId ) {
		$this->set( 'm_intSubsidyExportGroupId', CStrings::strToIntDef( $intSubsidyExportGroupId, NULL, false ) );
	}

	public function getSubsidyExportGroupId() {
		return $this->m_intSubsidyExportGroupId;
	}

	public function sqlSubsidyExportGroupId() {
		return ( true == isset( $this->m_intSubsidyExportGroupId ) ) ? ( string ) $this->m_intSubsidyExportGroupId : 'NULL';
	}

	public function setTaxRateExportGroupId( $intTaxRateExportGroupId ) {
		$this->set( 'm_intTaxRateExportGroupId', CStrings::strToIntDef( $intTaxRateExportGroupId, NULL, false ) );
	}

	public function getTaxRateExportGroupId() {
		return $this->m_intTaxRateExportGroupId;
	}

	public function sqlTaxRateExportGroupId() {
		return ( true == isset( $this->m_intTaxRateExportGroupId ) ) ? ( string ) $this->m_intTaxRateExportGroupId : 'NULL';
	}

	public function setExportGroupId( $intExportGroupId ) {
		$this->set( 'm_intExportGroupId', CStrings::strToIntDef( $intExportGroupId, NULL, false ) );
	}

	public function getExportGroupId() {
		return $this->m_intExportGroupId;
	}

	public function sqlExportGroupId() {
		return ( true == isset( $this->m_intExportGroupId ) ) ? ( string ) $this->m_intExportGroupId : 'NULL';
	}

	public function setBillingServicePeriodTypeId( $intBillingServicePeriodTypeId ) {
		$this->set( 'm_intBillingServicePeriodTypeId', CStrings::strToIntDef( $intBillingServicePeriodTypeId, NULL, false ) );
	}

	public function getBillingServicePeriodTypeId() {
		return $this->m_intBillingServicePeriodTypeId;
	}

	public function sqlBillingServicePeriodTypeId() {
		return ( true == isset( $this->m_intBillingServicePeriodTypeId ) ) ? ( string ) $this->m_intBillingServicePeriodTypeId : 'NULL';
	}

	public function setBillingServicePropertyUtilityTypeId( $intBillingServicePropertyUtilityTypeId ) {
		$this->set( 'm_intBillingServicePropertyUtilityTypeId', CStrings::strToIntDef( $intBillingServicePropertyUtilityTypeId, NULL, false ) );
	}

	public function getBillingServicePropertyUtilityTypeId() {
		return $this->m_intBillingServicePropertyUtilityTypeId;
	}

	public function sqlBillingServicePropertyUtilityTypeId() {
		return ( true == isset( $this->m_intBillingServicePropertyUtilityTypeId ) ) ? ( string ) $this->m_intBillingServicePropertyUtilityTypeId : 'NULL';
	}

	public function setBillingServicePeriodUtilityBillAccountId( $intBillingServicePeriodUtilityBillAccountId ) {
		$this->set( 'm_intBillingServicePeriodUtilityBillAccountId', CStrings::strToIntDef( $intBillingServicePeriodUtilityBillAccountId, NULL, false ) );
	}

	public function getBillingServicePeriodUtilityBillAccountId() {
		return $this->m_intBillingServicePeriodUtilityBillAccountId;
	}

	public function sqlBillingServicePeriodUtilityBillAccountId() {
		return ( true == isset( $this->m_intBillingServicePeriodUtilityBillAccountId ) ) ? ( string ) $this->m_intBillingServicePeriodUtilityBillAccountId : 'NULL';
	}

	public function setMoveInFeeDescription( $strMoveInFeeDescription ) {
		$this->set( 'm_strMoveInFeeDescription', CStrings::strTrimDef( $strMoveInFeeDescription, 240, NULL, true ) );
	}

	public function getMoveInFeeDescription() {
		return $this->m_strMoveInFeeDescription;
	}

	public function sqlMoveInFeeDescription() {
		return ( true == isset( $this->m_strMoveInFeeDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMoveInFeeDescription ) : '\'' . addslashes( $this->m_strMoveInFeeDescription ) . '\'' ) : 'NULL';
	}

	public function setTerminationFeeDescription( $strTerminationFeeDescription ) {
		$this->set( 'm_strTerminationFeeDescription', CStrings::strTrimDef( $strTerminationFeeDescription, 240, NULL, true ) );
	}

	public function getTerminationFeeDescription() {
		return $this->m_strTerminationFeeDescription;
	}

	public function sqlTerminationFeeDescription() {
		return ( true == isset( $this->m_strTerminationFeeDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTerminationFeeDescription ) : '\'' . addslashes( $this->m_strTerminationFeeDescription ) . '\'' ) : 'NULL';
	}

	public function setBillingFeeDescription( $strBillingFeeDescription ) {
		$this->set( 'm_strBillingFeeDescription', CStrings::strTrimDef( $strBillingFeeDescription, 240, NULL, true ) );
	}

	public function getBillingFeeDescription() {
		return $this->m_strBillingFeeDescription;
	}

	public function sqlBillingFeeDescription() {
		return ( true == isset( $this->m_strBillingFeeDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBillingFeeDescription ) : '\'' . addslashes( $this->m_strBillingFeeDescription ) . '\'' ) : 'NULL';
	}

	public function setCapDescription( $strCapDescription ) {
		$this->set( 'm_strCapDescription', CStrings::strTrimDef( $strCapDescription, 240, NULL, true ) );
	}

	public function getCapDescription() {
		return $this->m_strCapDescription;
	}

	public function sqlCapDescription() {
		return ( true == isset( $this->m_strCapDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCapDescription ) : '\'' . addslashes( $this->m_strCapDescription ) . '\'' ) : 'NULL';
	}

	public function setSubsidyDescription( $strSubsidyDescription ) {
		$this->set( 'm_strSubsidyDescription', CStrings::strTrimDef( $strSubsidyDescription, 240, NULL, true ) );
	}

	public function getSubsidyDescription() {
		return $this->m_strSubsidyDescription;
	}

	public function sqlSubsidyDescription() {
		return ( true == isset( $this->m_strSubsidyDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSubsidyDescription ) : '\'' . addslashes( $this->m_strSubsidyDescription ) . '\'' ) : 'NULL';
	}

	public function setChargeCodeDescription( $strChargeCodeDescription ) {
		$this->set( 'm_strChargeCodeDescription', CStrings::strTrimDef( $strChargeCodeDescription, 240, NULL, true ) );
	}

	public function getChargeCodeDescription() {
		return $this->m_strChargeCodeDescription;
	}

	public function sqlChargeCodeDescription() {
		return ( true == isset( $this->m_strChargeCodeDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strChargeCodeDescription ) : '\'' . addslashes( $this->m_strChargeCodeDescription ) . '\'' ) : 'NULL';
	}

	public function setTaxRateDescription( $strTaxRateDescription ) {
		$this->set( 'm_strTaxRateDescription', CStrings::strTrimDef( $strTaxRateDescription, 240, NULL, true ) );
	}

	public function getTaxRateDescription() {
		return $this->m_strTaxRateDescription;
	}

	public function sqlTaxRateDescription() {
		return ( true == isset( $this->m_strTaxRateDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxRateDescription ) : '\'' . addslashes( $this->m_strTaxRateDescription ) . '\'' ) : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setBillingFeeLabel( $strBillingFeeLabel ) {
		$this->set( 'm_strBillingFeeLabel', CStrings::strTrimDef( $strBillingFeeLabel, 50, NULL, true ) );
	}

	public function getBillingFeeLabel() {
		return $this->m_strBillingFeeLabel;
	}

	public function sqlBillingFeeLabel() {
		return ( true == isset( $this->m_strBillingFeeLabel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBillingFeeLabel ) : '\'' . addslashes( $this->m_strBillingFeeLabel ) . '\'' ) : 'NULL';
	}

	public function setBaseFeeConsumptionUnits( $fltBaseFeeConsumptionUnits ) {
		$this->set( 'm_fltBaseFeeConsumptionUnits', CStrings::strToFloatDef( $fltBaseFeeConsumptionUnits, NULL, false, 4 ) );
	}

	public function getBaseFeeConsumptionUnits() {
		return $this->m_fltBaseFeeConsumptionUnits;
	}

	public function sqlBaseFeeConsumptionUnits() {
		return ( true == isset( $this->m_fltBaseFeeConsumptionUnits ) ) ? ( string ) $this->m_fltBaseFeeConsumptionUnits : 'NULL';
	}

	public function setBaseFeePerUnit( $fltBaseFeePerUnit ) {
		$this->set( 'm_fltBaseFeePerUnit', CStrings::strToFloatDef( $fltBaseFeePerUnit, NULL, false, 2 ) );
	}

	public function getBaseFeePerUnit() {
		return $this->m_fltBaseFeePerUnit;
	}

	public function sqlBaseFeePerUnit() {
		return ( true == isset( $this->m_fltBaseFeePerUnit ) ) ? ( string ) $this->m_fltBaseFeePerUnit : 'NULL';
	}

	public function setBaseFeePerUnitChangeDate( $strBaseFeePerUnitChangeDate ) {
		$this->set( 'm_strBaseFeePerUnitChangeDate', CStrings::strTrimDef( $strBaseFeePerUnitChangeDate, -1, NULL, true ) );
	}

	public function getBaseFeePerUnitChangeDate() {
		return $this->m_strBaseFeePerUnitChangeDate;
	}

	public function sqlBaseFeePerUnitChangeDate() {
		return ( true == isset( $this->m_strBaseFeePerUnitChangeDate ) ) ? '\'' . $this->m_strBaseFeePerUnitChangeDate . '\'' : 'NULL';
	}

	public function setLastRatePerConsumptionUnit( $fltLastRatePerConsumptionUnit ) {
		$this->set( 'm_fltLastRatePerConsumptionUnit', CStrings::strToFloatDef( $fltLastRatePerConsumptionUnit, NULL, false, 2 ) );
	}

	public function getLastRatePerConsumptionUnit() {
		return $this->m_fltLastRatePerConsumptionUnit;
	}

	public function sqlLastRatePerConsumptionUnit() {
		return ( true == isset( $this->m_fltLastRatePerConsumptionUnit ) ) ? ( string ) $this->m_fltLastRatePerConsumptionUnit : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setHasBillingService( $boolHasBillingService ) {
		$this->set( 'm_boolHasBillingService', CStrings::strToBool( $boolHasBillingService ) );
	}

	public function getHasBillingService() {
		return $this->m_boolHasBillingService;
	}

	public function sqlHasBillingService() {
		return ( true == isset( $this->m_boolHasBillingService ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasBillingService ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasVcrService( $boolHasVcrService ) {
		$this->set( 'm_boolHasVcrService', CStrings::strToBool( $boolHasVcrService ) );
	}

	public function getHasVcrService() {
		return $this->m_boolHasVcrService;
	}

	public function sqlHasVcrService() {
		return ( true == isset( $this->m_boolHasVcrService ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasVcrService ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSubmetered( $boolIsSubmetered ) {
		$this->set( 'm_boolIsSubmetered', CStrings::strToBool( $boolIsSubmetered ) );
	}

	public function getIsSubmetered() {
		return $this->m_boolIsSubmetered;
	}

	public function sqlIsSubmetered() {
		return ( true == isset( $this->m_boolIsSubmetered ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSubmetered ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsManual( $boolIsManual ) {
		$this->set( 'm_boolIsManual', CStrings::strToBool( $boolIsManual ) );
	}

	public function getIsManual() {
		return $this->m_boolIsManual;
	}

	public function sqlIsManual() {
		return ( true == isset( $this->m_boolIsManual ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManual ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFixed( $boolIsFixed ) {
		$this->set( 'm_boolIsFixed', CStrings::strToBool( $boolIsFixed ) );
	}

	public function getIsFixed() {
		return $this->m_boolIsFixed;
	}

	public function sqlIsFixed() {
		return ( true == isset( $this->m_boolIsFixed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFixed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setServicePeriodExcludeEndDay( $boolServicePeriodExcludeEndDay ) {
		$this->set( 'm_boolServicePeriodExcludeEndDay', CStrings::strToBool( $boolServicePeriodExcludeEndDay ) );
	}

	public function getServicePeriodExcludeEndDay() {
		return $this->m_boolServicePeriodExcludeEndDay;
	}

	public function sqlServicePeriodExcludeEndDay() {
		return ( true == isset( $this->m_boolServicePeriodExcludeEndDay ) ) ? '\'' . ( true == ( bool ) $this->m_boolServicePeriodExcludeEndDay ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontRequireBills( $boolDontRequireBills ) {
		$this->set( 'm_boolDontRequireBills', CStrings::strToBool( $boolDontRequireBills ) );
	}

	public function getDontRequireBills() {
		return $this->m_boolDontRequireBills;
	}

	public function sqlDontRequireBills() {
		return ( true == isset( $this->m_boolDontRequireBills ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontRequireBills ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBillByBuilding( $boolBillByBuilding ) {
		$this->set( 'm_boolBillByBuilding', CStrings::strToBool( $boolBillByBuilding ) );
	}

	public function getBillByBuilding() {
		return $this->m_boolBillByBuilding;
	}

	public function sqlBillByBuilding() {
		return ( true == isset( $this->m_boolBillByBuilding ) ) ? '\'' . ( true == ( bool ) $this->m_boolBillByBuilding ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseTotalUnits( $boolUseTotalUnits ) {
		$this->set( 'm_boolUseTotalUnits', CStrings::strToBool( $boolUseTotalUnits ) );
	}

	public function getUseTotalUnits() {
		return $this->m_boolUseTotalUnits;
	}

	public function sqlUseTotalUnits() {
		return ( true == isset( $this->m_boolUseTotalUnits ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseTotalUnits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseTotalSqft( $boolUseTotalSqft ) {
		$this->set( 'm_boolUseTotalSqft', CStrings::strToBool( $boolUseTotalSqft ) );
	}

	public function getUseTotalSqft() {
		return $this->m_boolUseTotalSqft;
	}

	public function sqlUseTotalSqft() {
		return ( true == isset( $this->m_boolUseTotalSqft ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseTotalSqft ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontTaxUsage( $boolDontTaxUsage ) {
		$this->set( 'm_boolDontTaxUsage', CStrings::strToBool( $boolDontTaxUsage ) );
	}

	public function getDontTaxUsage() {
		return $this->m_boolDontTaxUsage;
	}

	public function sqlDontTaxUsage() {
		return ( true == isset( $this->m_boolDontTaxUsage ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontTaxUsage ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontTaxBaseFees( $boolDontTaxBaseFees ) {
		$this->set( 'm_boolDontTaxBaseFees', CStrings::strToBool( $boolDontTaxBaseFees ) );
	}

	public function getDontTaxBaseFees() {
		return $this->m_boolDontTaxBaseFees;
	}

	public function sqlDontTaxBaseFees() {
		return ( true == isset( $this->m_boolDontTaxBaseFees ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontTaxBaseFees ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontTaxBillingFees( $boolDontTaxBillingFees ) {
		$this->set( 'm_boolDontTaxBillingFees', CStrings::strToBool( $boolDontTaxBillingFees ) );
	}

	public function getDontTaxBillingFees() {
		return $this->m_boolDontTaxBillingFees;
	}

	public function sqlDontTaxBillingFees() {
		return ( true == isset( $this->m_boolDontTaxBillingFees ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontTaxBillingFees ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBillingServicePeriodStartDay( $intBillingServicePeriodStartDay ) {
		$this->set( 'm_intBillingServicePeriodStartDay', CStrings::strToIntDef( $intBillingServicePeriodStartDay, NULL, false ) );
	}

	public function getBillingServicePeriodStartDay() {
		return $this->m_intBillingServicePeriodStartDay;
	}

	public function sqlBillingServicePeriodStartDay() {
		return ( true == isset( $this->m_intBillingServicePeriodStartDay ) ) ? ( string ) $this->m_intBillingServicePeriodStartDay : 'NULL';
	}

	public function setBillingServicePeriodEndDay( $intBillingServicePeriodEndDay ) {
		$this->set( 'm_intBillingServicePeriodEndDay', CStrings::strToIntDef( $intBillingServicePeriodEndDay, NULL, false ) );
	}

	public function getBillingServicePeriodEndDay() {
		return $this->m_intBillingServicePeriodEndDay;
	}

	public function sqlBillingServicePeriodEndDay() {
		return ( true == isset( $this->m_intBillingServicePeriodEndDay ) ) ? ( string ) $this->m_intBillingServicePeriodEndDay : 'NULL';
	}

	public function setBillingServiceMonth( $intBillingServiceMonth ) {
		$this->set( 'm_intBillingServiceMonth', CStrings::strToIntDef( $intBillingServiceMonth, NULL, false ) );
	}

	public function getBillingServiceMonth() {
		return $this->m_intBillingServiceMonth;
	}

	public function sqlBillingServiceMonth() {
		return ( true == isset( $this->m_intBillingServiceMonth ) ) ? ( string ) $this->m_intBillingServiceMonth : 'NULL';
	}

	public function setGrossRecaptureThreshold( $fltGrossRecaptureThreshold ) {
		$this->set( 'm_fltGrossRecaptureThreshold', CStrings::strToFloatDef( $fltGrossRecaptureThreshold, NULL, false, 2 ) );
	}

	public function getGrossRecaptureThreshold() {
		return $this->m_fltGrossRecaptureThreshold;
	}

	public function sqlGrossRecaptureThreshold() {
		return ( true == isset( $this->m_fltGrossRecaptureThreshold ) ) ? ( string ) $this->m_fltGrossRecaptureThreshold : '10';
	}

	public function setBillableRecaptureThreshold( $fltBillableRecaptureThreshold ) {
		$this->set( 'm_fltBillableRecaptureThreshold', CStrings::strToFloatDef( $fltBillableRecaptureThreshold, NULL, false, 2 ) );
	}

	public function getBillableRecaptureThreshold() {
		return $this->m_fltBillableRecaptureThreshold;
	}

	public function sqlBillableRecaptureThreshold() {
		return ( true == isset( $this->m_fltBillableRecaptureThreshold ) ) ? ( string ) $this->m_fltBillableRecaptureThreshold : 'NULL';
	}

	public function setUtilityChargesThreshold( $fltUtilityChargesThreshold ) {
		$this->set( 'm_fltUtilityChargesThreshold', CStrings::strToFloatDef( $fltUtilityChargesThreshold, NULL, false, 2 ) );
	}

	public function getUtilityChargesThreshold() {
		return $this->m_fltUtilityChargesThreshold;
	}

	public function sqlUtilityChargesThreshold() {
		return ( true == isset( $this->m_fltUtilityChargesThreshold ) ) ? ( string ) $this->m_fltUtilityChargesThreshold : '20';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRegistrationDate( $strRegistrationDate ) {
		$this->set( 'm_strRegistrationDate', CStrings::strTrimDef( $strRegistrationDate, -1, NULL, true ) );
	}

	public function getRegistrationDate() {
		return $this->m_strRegistrationDate;
	}

	public function sqlRegistrationDate() {
		return ( true == isset( $this->m_strRegistrationDate ) ) ? '\'' . $this->m_strRegistrationDate . '\'' : 'NULL';
	}

	public function setRenewalDate( $strRenewalDate ) {
		$this->set( 'm_strRenewalDate', CStrings::strTrimDef( $strRenewalDate, -1, NULL, true ) );
	}

	public function getRenewalDate() {
		return $this->m_strRenewalDate;
	}

	public function sqlRenewalDate() {
		return ( true == isset( $this->m_strRenewalDate ) ) ? '\'' . $this->m_strRenewalDate . '\'' : 'NULL';
	}

	public function setUtilityDocumentsIds( $arrintUtilityDocumentsIds ) {
		$this->set( 'm_arrintUtilityDocumentsIds', CStrings::strToArrIntDef( $arrintUtilityDocumentsIds, NULL ) );
	}

	public function getUtilityDocumentsIds() {
		return $this->m_arrintUtilityDocumentsIds;
	}

	public function sqlUtilityDocumentsIds() {
		return ( true == isset( $this->m_arrintUtilityDocumentsIds ) && true == valArr( $this->m_arrintUtilityDocumentsIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintUtilityDocumentsIds, NULL ) . '\'' : 'NULL';
	}

	public function setBillingPeriodExcludeEndDay( $boolBillingPeriodExcludeEndDay ) {
		$this->set( 'm_boolBillingPeriodExcludeEndDay', CStrings::strToBool( $boolBillingPeriodExcludeEndDay ) );
	}

	public function getBillingPeriodExcludeEndDay() {
		return $this->m_boolBillingPeriodExcludeEndDay;
	}

	public function sqlBillingPeriodExcludeEndDay() {
		return ( true == isset( $this->m_boolBillingPeriodExcludeEndDay ) ) ? '\'' . ( true == ( bool ) $this->m_boolBillingPeriodExcludeEndDay ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUtilityBaseFeeCalculationTypeId( $intUtilityBaseFeeCalculationTypeId ) {
		$this->set( 'm_intUtilityBaseFeeCalculationTypeId', CStrings::strToIntDef( $intUtilityBaseFeeCalculationTypeId, NULL, false ) );
	}

	public function getUtilityBaseFeeCalculationTypeId() {
		return $this->m_intUtilityBaseFeeCalculationTypeId;
	}

	public function sqlUtilityBaseFeeCalculationTypeId() {
		return ( true == isset( $this->m_intUtilityBaseFeeCalculationTypeId ) ) ? ( string ) $this->m_intUtilityBaseFeeCalculationTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_building_id, utility_type_id, utility_tax_rate_id, move_in_ar_code_id, termination_ar_code_id, billing_fee_ar_code_id, cap_ar_code_id, subsidy_ar_code_id, tax_rate_ar_code_id, ar_code_id, ap_payee_id, utility_bill_period_id, utility_consumption_type_id, benchmark_property_utility_type_id, move_in_fee_export_group_id, termination_fee_export_group_id, billing_fee_export_group_id, cap_export_group_id, subsidy_export_group_id, tax_rate_export_group_id, export_group_id, billing_service_period_type_id, billing_service_property_utility_type_id, billing_service_period_utility_bill_account_id, move_in_fee_description, termination_fee_description, billing_fee_description, cap_description, subsidy_description, charge_code_description, tax_rate_description, name, description, billing_fee_label, base_fee_consumption_units, base_fee_per_unit, base_fee_per_unit_change_date, last_rate_per_consumption_unit, notes, has_billing_service, has_vcr_service, is_submetered, is_manual, is_fixed, service_period_exclude_end_day, dont_require_bills, bill_by_building, use_total_units, use_total_sqft, dont_tax_usage, dont_tax_base_fees, dont_tax_billing_fees, billing_service_period_start_day, billing_service_period_end_day, billing_service_month, gross_recapture_threshold, billable_recapture_threshold, utility_charges_threshold, disabled_by, disabled_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, registration_date, renewal_date, utility_documents_ids, billing_period_exclude_end_day, utility_base_fee_calculation_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlUtilityTypeId() . ', ' .
						$this->sqlUtilityTaxRateId() . ', ' .
						$this->sqlMoveInArCodeId() . ', ' .
						$this->sqlTerminationArCodeId() . ', ' .
						$this->sqlBillingFeeArCodeId() . ', ' .
						$this->sqlCapArCodeId() . ', ' .
						$this->sqlSubsidyArCodeId() . ', ' .
						$this->sqlTaxRateArCodeId() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlUtilityBillPeriodId() . ', ' .
						$this->sqlUtilityConsumptionTypeId() . ', ' .
						$this->sqlBenchmarkPropertyUtilityTypeId() . ', ' .
						$this->sqlMoveInFeeExportGroupId() . ', ' .
						$this->sqlTerminationFeeExportGroupId() . ', ' .
						$this->sqlBillingFeeExportGroupId() . ', ' .
						$this->sqlCapExportGroupId() . ', ' .
						$this->sqlSubsidyExportGroupId() . ', ' .
						$this->sqlTaxRateExportGroupId() . ', ' .
						$this->sqlExportGroupId() . ', ' .
						$this->sqlBillingServicePeriodTypeId() . ', ' .
						$this->sqlBillingServicePropertyUtilityTypeId() . ', ' .
						$this->sqlBillingServicePeriodUtilityBillAccountId() . ', ' .
						$this->sqlMoveInFeeDescription() . ', ' .
						$this->sqlTerminationFeeDescription() . ', ' .
						$this->sqlBillingFeeDescription() . ', ' .
						$this->sqlCapDescription() . ', ' .
						$this->sqlSubsidyDescription() . ', ' .
						$this->sqlChargeCodeDescription() . ', ' .
						$this->sqlTaxRateDescription() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlBillingFeeLabel() . ', ' .
						$this->sqlBaseFeeConsumptionUnits() . ', ' .
						$this->sqlBaseFeePerUnit() . ', ' .
						$this->sqlBaseFeePerUnitChangeDate() . ', ' .
						$this->sqlLastRatePerConsumptionUnit() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlHasBillingService() . ', ' .
						$this->sqlHasVcrService() . ', ' .
						$this->sqlIsSubmetered() . ', ' .
						$this->sqlIsManual() . ', ' .
						$this->sqlIsFixed() . ', ' .
						$this->sqlServicePeriodExcludeEndDay() . ', ' .
						$this->sqlDontRequireBills() . ', ' .
						$this->sqlBillByBuilding() . ', ' .
						$this->sqlUseTotalUnits() . ', ' .
						$this->sqlUseTotalSqft() . ', ' .
						$this->sqlDontTaxUsage() . ', ' .
						$this->sqlDontTaxBaseFees() . ', ' .
						$this->sqlDontTaxBillingFees() . ', ' .
						$this->sqlBillingServicePeriodStartDay() . ', ' .
						$this->sqlBillingServicePeriodEndDay() . ', ' .
						$this->sqlBillingServiceMonth() . ', ' .
						$this->sqlGrossRecaptureThreshold() . ', ' .
						$this->sqlBillableRecaptureThreshold() . ', ' .
						$this->sqlUtilityChargesThreshold() . ', ' .
						$this->sqlDisabledBy() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlRegistrationDate() . ', ' .
						$this->sqlRenewalDate() . ', ' .
						$this->sqlUtilityDocumentsIds() . ', ' .
						$this->sqlBillingPeriodExcludeEndDay() . ', ' .
						$this->sqlUtilityBaseFeeCalculationTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_tax_rate_id = ' . $this->sqlUtilityTaxRateId(). ',' ; } elseif( true == array_key_exists( 'UtilityTaxRateId', $this->getChangedColumns() ) ) { $strSql .= ' utility_tax_rate_id = ' . $this->sqlUtilityTaxRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_ar_code_id = ' . $this->sqlMoveInArCodeId(). ',' ; } elseif( true == array_key_exists( 'MoveInArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' move_in_ar_code_id = ' . $this->sqlMoveInArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_ar_code_id = ' . $this->sqlTerminationArCodeId(). ',' ; } elseif( true == array_key_exists( 'TerminationArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' termination_ar_code_id = ' . $this->sqlTerminationArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_fee_ar_code_id = ' . $this->sqlBillingFeeArCodeId(). ',' ; } elseif( true == array_key_exists( 'BillingFeeArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' billing_fee_ar_code_id = ' . $this->sqlBillingFeeArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cap_ar_code_id = ' . $this->sqlCapArCodeId(). ',' ; } elseif( true == array_key_exists( 'CapArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' cap_ar_code_id = ' . $this->sqlCapArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_ar_code_id = ' . $this->sqlSubsidyArCodeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_ar_code_id = ' . $this->sqlSubsidyArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_rate_ar_code_id = ' . $this->sqlTaxRateArCodeId(). ',' ; } elseif( true == array_key_exists( 'TaxRateArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' tax_rate_ar_code_id = ' . $this->sqlTaxRateArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_period_id = ' . $this->sqlUtilityBillPeriodId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_period_id = ' . $this->sqlUtilityBillPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_consumption_type_id = ' . $this->sqlUtilityConsumptionTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityConsumptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_consumption_type_id = ' . $this->sqlUtilityConsumptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' benchmark_property_utility_type_id = ' . $this->sqlBenchmarkPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'BenchmarkPropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' benchmark_property_utility_type_id = ' . $this->sqlBenchmarkPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_fee_export_group_id = ' . $this->sqlMoveInFeeExportGroupId(). ',' ; } elseif( true == array_key_exists( 'MoveInFeeExportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' move_in_fee_export_group_id = ' . $this->sqlMoveInFeeExportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_fee_export_group_id = ' . $this->sqlTerminationFeeExportGroupId(). ',' ; } elseif( true == array_key_exists( 'TerminationFeeExportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' termination_fee_export_group_id = ' . $this->sqlTerminationFeeExportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_fee_export_group_id = ' . $this->sqlBillingFeeExportGroupId(). ',' ; } elseif( true == array_key_exists( 'BillingFeeExportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' billing_fee_export_group_id = ' . $this->sqlBillingFeeExportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cap_export_group_id = ' . $this->sqlCapExportGroupId(). ',' ; } elseif( true == array_key_exists( 'CapExportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' cap_export_group_id = ' . $this->sqlCapExportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_export_group_id = ' . $this->sqlSubsidyExportGroupId(). ',' ; } elseif( true == array_key_exists( 'SubsidyExportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_export_group_id = ' . $this->sqlSubsidyExportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_rate_export_group_id = ' . $this->sqlTaxRateExportGroupId(). ',' ; } elseif( true == array_key_exists( 'TaxRateExportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' tax_rate_export_group_id = ' . $this->sqlTaxRateExportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_group_id = ' . $this->sqlExportGroupId(). ',' ; } elseif( true == array_key_exists( 'ExportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' export_group_id = ' . $this->sqlExportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_service_period_type_id = ' . $this->sqlBillingServicePeriodTypeId(). ',' ; } elseif( true == array_key_exists( 'BillingServicePeriodTypeId', $this->getChangedColumns() ) ) { $strSql .= ' billing_service_period_type_id = ' . $this->sqlBillingServicePeriodTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_service_property_utility_type_id = ' . $this->sqlBillingServicePropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'BillingServicePropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' billing_service_property_utility_type_id = ' . $this->sqlBillingServicePropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_service_period_utility_bill_account_id = ' . $this->sqlBillingServicePeriodUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'BillingServicePeriodUtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' billing_service_period_utility_bill_account_id = ' . $this->sqlBillingServicePeriodUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_fee_description = ' . $this->sqlMoveInFeeDescription(). ',' ; } elseif( true == array_key_exists( 'MoveInFeeDescription', $this->getChangedColumns() ) ) { $strSql .= ' move_in_fee_description = ' . $this->sqlMoveInFeeDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_fee_description = ' . $this->sqlTerminationFeeDescription(). ',' ; } elseif( true == array_key_exists( 'TerminationFeeDescription', $this->getChangedColumns() ) ) { $strSql .= ' termination_fee_description = ' . $this->sqlTerminationFeeDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_fee_description = ' . $this->sqlBillingFeeDescription(). ',' ; } elseif( true == array_key_exists( 'BillingFeeDescription', $this->getChangedColumns() ) ) { $strSql .= ' billing_fee_description = ' . $this->sqlBillingFeeDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cap_description = ' . $this->sqlCapDescription(). ',' ; } elseif( true == array_key_exists( 'CapDescription', $this->getChangedColumns() ) ) { $strSql .= ' cap_description = ' . $this->sqlCapDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_description = ' . $this->sqlSubsidyDescription(). ',' ; } elseif( true == array_key_exists( 'SubsidyDescription', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_description = ' . $this->sqlSubsidyDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_code_description = ' . $this->sqlChargeCodeDescription(). ',' ; } elseif( true == array_key_exists( 'ChargeCodeDescription', $this->getChangedColumns() ) ) { $strSql .= ' charge_code_description = ' . $this->sqlChargeCodeDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_rate_description = ' . $this->sqlTaxRateDescription(). ',' ; } elseif( true == array_key_exists( 'TaxRateDescription', $this->getChangedColumns() ) ) { $strSql .= ' tax_rate_description = ' . $this->sqlTaxRateDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_fee_label = ' . $this->sqlBillingFeeLabel(). ',' ; } elseif( true == array_key_exists( 'BillingFeeLabel', $this->getChangedColumns() ) ) { $strSql .= ' billing_fee_label = ' . $this->sqlBillingFeeLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_fee_consumption_units = ' . $this->sqlBaseFeeConsumptionUnits(). ',' ; } elseif( true == array_key_exists( 'BaseFeeConsumptionUnits', $this->getChangedColumns() ) ) { $strSql .= ' base_fee_consumption_units = ' . $this->sqlBaseFeeConsumptionUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_fee_per_unit = ' . $this->sqlBaseFeePerUnit(). ',' ; } elseif( true == array_key_exists( 'BaseFeePerUnit', $this->getChangedColumns() ) ) { $strSql .= ' base_fee_per_unit = ' . $this->sqlBaseFeePerUnit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_fee_per_unit_change_date = ' . $this->sqlBaseFeePerUnitChangeDate(). ',' ; } elseif( true == array_key_exists( 'BaseFeePerUnitChangeDate', $this->getChangedColumns() ) ) { $strSql .= ' base_fee_per_unit_change_date = ' . $this->sqlBaseFeePerUnitChangeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_rate_per_consumption_unit = ' . $this->sqlLastRatePerConsumptionUnit(). ',' ; } elseif( true == array_key_exists( 'LastRatePerConsumptionUnit', $this->getChangedColumns() ) ) { $strSql .= ' last_rate_per_consumption_unit = ' . $this->sqlLastRatePerConsumptionUnit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_billing_service = ' . $this->sqlHasBillingService(). ',' ; } elseif( true == array_key_exists( 'HasBillingService', $this->getChangedColumns() ) ) { $strSql .= ' has_billing_service = ' . $this->sqlHasBillingService() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_vcr_service = ' . $this->sqlHasVcrService(). ',' ; } elseif( true == array_key_exists( 'HasVcrService', $this->getChangedColumns() ) ) { $strSql .= ' has_vcr_service = ' . $this->sqlHasVcrService() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_submetered = ' . $this->sqlIsSubmetered(). ',' ; } elseif( true == array_key_exists( 'IsSubmetered', $this->getChangedColumns() ) ) { $strSql .= ' is_submetered = ' . $this->sqlIsSubmetered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual = ' . $this->sqlIsManual(). ',' ; } elseif( true == array_key_exists( 'IsManual', $this->getChangedColumns() ) ) { $strSql .= ' is_manual = ' . $this->sqlIsManual() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_fixed = ' . $this->sqlIsFixed(). ',' ; } elseif( true == array_key_exists( 'IsFixed', $this->getChangedColumns() ) ) { $strSql .= ' is_fixed = ' . $this->sqlIsFixed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_period_exclude_end_day = ' . $this->sqlServicePeriodExcludeEndDay(). ',' ; } elseif( true == array_key_exists( 'ServicePeriodExcludeEndDay', $this->getChangedColumns() ) ) { $strSql .= ' service_period_exclude_end_day = ' . $this->sqlServicePeriodExcludeEndDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_require_bills = ' . $this->sqlDontRequireBills(). ',' ; } elseif( true == array_key_exists( 'DontRequireBills', $this->getChangedColumns() ) ) { $strSql .= ' dont_require_bills = ' . $this->sqlDontRequireBills() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_by_building = ' . $this->sqlBillByBuilding(). ',' ; } elseif( true == array_key_exists( 'BillByBuilding', $this->getChangedColumns() ) ) { $strSql .= ' bill_by_building = ' . $this->sqlBillByBuilding() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_total_units = ' . $this->sqlUseTotalUnits(). ',' ; } elseif( true == array_key_exists( 'UseTotalUnits', $this->getChangedColumns() ) ) { $strSql .= ' use_total_units = ' . $this->sqlUseTotalUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_total_sqft = ' . $this->sqlUseTotalSqft(). ',' ; } elseif( true == array_key_exists( 'UseTotalSqft', $this->getChangedColumns() ) ) { $strSql .= ' use_total_sqft = ' . $this->sqlUseTotalSqft() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_tax_usage = ' . $this->sqlDontTaxUsage(). ',' ; } elseif( true == array_key_exists( 'DontTaxUsage', $this->getChangedColumns() ) ) { $strSql .= ' dont_tax_usage = ' . $this->sqlDontTaxUsage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_tax_base_fees = ' . $this->sqlDontTaxBaseFees(). ',' ; } elseif( true == array_key_exists( 'DontTaxBaseFees', $this->getChangedColumns() ) ) { $strSql .= ' dont_tax_base_fees = ' . $this->sqlDontTaxBaseFees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_tax_billing_fees = ' . $this->sqlDontTaxBillingFees(). ',' ; } elseif( true == array_key_exists( 'DontTaxBillingFees', $this->getChangedColumns() ) ) { $strSql .= ' dont_tax_billing_fees = ' . $this->sqlDontTaxBillingFees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_service_period_start_day = ' . $this->sqlBillingServicePeriodStartDay(). ',' ; } elseif( true == array_key_exists( 'BillingServicePeriodStartDay', $this->getChangedColumns() ) ) { $strSql .= ' billing_service_period_start_day = ' . $this->sqlBillingServicePeriodStartDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_service_period_end_day = ' . $this->sqlBillingServicePeriodEndDay(). ',' ; } elseif( true == array_key_exists( 'BillingServicePeriodEndDay', $this->getChangedColumns() ) ) { $strSql .= ' billing_service_period_end_day = ' . $this->sqlBillingServicePeriodEndDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_service_month = ' . $this->sqlBillingServiceMonth(). ',' ; } elseif( true == array_key_exists( 'BillingServiceMonth', $this->getChangedColumns() ) ) { $strSql .= ' billing_service_month = ' . $this->sqlBillingServiceMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gross_recapture_threshold = ' . $this->sqlGrossRecaptureThreshold(). ',' ; } elseif( true == array_key_exists( 'GrossRecaptureThreshold', $this->getChangedColumns() ) ) { $strSql .= ' gross_recapture_threshold = ' . $this->sqlGrossRecaptureThreshold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billable_recapture_threshold = ' . $this->sqlBillableRecaptureThreshold(). ',' ; } elseif( true == array_key_exists( 'BillableRecaptureThreshold', $this->getChangedColumns() ) ) { $strSql .= ' billable_recapture_threshold = ' . $this->sqlBillableRecaptureThreshold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_charges_threshold = ' . $this->sqlUtilityChargesThreshold(). ',' ; } elseif( true == array_key_exists( 'UtilityChargesThreshold', $this->getChangedColumns() ) ) { $strSql .= ' utility_charges_threshold = ' . $this->sqlUtilityChargesThreshold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy(). ',' ; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' registration_date = ' . $this->sqlRegistrationDate(). ',' ; } elseif( true == array_key_exists( 'RegistrationDate', $this->getChangedColumns() ) ) { $strSql .= ' registration_date = ' . $this->sqlRegistrationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_date = ' . $this->sqlRenewalDate(). ',' ; } elseif( true == array_key_exists( 'RenewalDate', $this->getChangedColumns() ) ) { $strSql .= ' renewal_date = ' . $this->sqlRenewalDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_documents_ids = ' . $this->sqlUtilityDocumentsIds(). ',' ; } elseif( true == array_key_exists( 'UtilityDocumentsIds', $this->getChangedColumns() ) ) { $strSql .= ' utility_documents_ids = ' . $this->sqlUtilityDocumentsIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_period_exclude_end_day = ' . $this->sqlBillingPeriodExcludeEndDay(). ',' ; } elseif( true == array_key_exists( 'BillingPeriodExcludeEndDay', $this->getChangedColumns() ) ) { $strSql .= ' billing_period_exclude_end_day = ' . $this->sqlBillingPeriodExcludeEndDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_base_fee_calculation_type_id = ' . $this->sqlUtilityBaseFeeCalculationTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBaseFeeCalculationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_base_fee_calculation_type_id = ' . $this->sqlUtilityBaseFeeCalculationTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'utility_type_id' => $this->getUtilityTypeId(),
			'utility_tax_rate_id' => $this->getUtilityTaxRateId(),
			'move_in_ar_code_id' => $this->getMoveInArCodeId(),
			'termination_ar_code_id' => $this->getTerminationArCodeId(),
			'billing_fee_ar_code_id' => $this->getBillingFeeArCodeId(),
			'cap_ar_code_id' => $this->getCapArCodeId(),
			'subsidy_ar_code_id' => $this->getSubsidyArCodeId(),
			'tax_rate_ar_code_id' => $this->getTaxRateArCodeId(),
			'ar_code_id' => $this->getArCodeId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'utility_bill_period_id' => $this->getUtilityBillPeriodId(),
			'utility_consumption_type_id' => $this->getUtilityConsumptionTypeId(),
			'benchmark_property_utility_type_id' => $this->getBenchmarkPropertyUtilityTypeId(),
			'move_in_fee_export_group_id' => $this->getMoveInFeeExportGroupId(),
			'termination_fee_export_group_id' => $this->getTerminationFeeExportGroupId(),
			'billing_fee_export_group_id' => $this->getBillingFeeExportGroupId(),
			'cap_export_group_id' => $this->getCapExportGroupId(),
			'subsidy_export_group_id' => $this->getSubsidyExportGroupId(),
			'tax_rate_export_group_id' => $this->getTaxRateExportGroupId(),
			'export_group_id' => $this->getExportGroupId(),
			'billing_service_period_type_id' => $this->getBillingServicePeriodTypeId(),
			'billing_service_property_utility_type_id' => $this->getBillingServicePropertyUtilityTypeId(),
			'billing_service_period_utility_bill_account_id' => $this->getBillingServicePeriodUtilityBillAccountId(),
			'move_in_fee_description' => $this->getMoveInFeeDescription(),
			'termination_fee_description' => $this->getTerminationFeeDescription(),
			'billing_fee_description' => $this->getBillingFeeDescription(),
			'cap_description' => $this->getCapDescription(),
			'subsidy_description' => $this->getSubsidyDescription(),
			'charge_code_description' => $this->getChargeCodeDescription(),
			'tax_rate_description' => $this->getTaxRateDescription(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'billing_fee_label' => $this->getBillingFeeLabel(),
			'base_fee_consumption_units' => $this->getBaseFeeConsumptionUnits(),
			'base_fee_per_unit' => $this->getBaseFeePerUnit(),
			'base_fee_per_unit_change_date' => $this->getBaseFeePerUnitChangeDate(),
			'last_rate_per_consumption_unit' => $this->getLastRatePerConsumptionUnit(),
			'notes' => $this->getNotes(),
			'has_billing_service' => $this->getHasBillingService(),
			'has_vcr_service' => $this->getHasVcrService(),
			'is_submetered' => $this->getIsSubmetered(),
			'is_manual' => $this->getIsManual(),
			'is_fixed' => $this->getIsFixed(),
			'service_period_exclude_end_day' => $this->getServicePeriodExcludeEndDay(),
			'dont_require_bills' => $this->getDontRequireBills(),
			'bill_by_building' => $this->getBillByBuilding(),
			'use_total_units' => $this->getUseTotalUnits(),
			'use_total_sqft' => $this->getUseTotalSqft(),
			'dont_tax_usage' => $this->getDontTaxUsage(),
			'dont_tax_base_fees' => $this->getDontTaxBaseFees(),
			'dont_tax_billing_fees' => $this->getDontTaxBillingFees(),
			'billing_service_period_start_day' => $this->getBillingServicePeriodStartDay(),
			'billing_service_period_end_day' => $this->getBillingServicePeriodEndDay(),
			'billing_service_month' => $this->getBillingServiceMonth(),
			'gross_recapture_threshold' => $this->getGrossRecaptureThreshold(),
			'billable_recapture_threshold' => $this->getBillableRecaptureThreshold(),
			'utility_charges_threshold' => $this->getUtilityChargesThreshold(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'registration_date' => $this->getRegistrationDate(),
			'renewal_date' => $this->getRenewalDate(),
			'utility_documents_ids' => $this->getUtilityDocumentsIds(),
			'billing_period_exclude_end_day' => $this->getBillingPeriodExcludeEndDay(),
			'utility_base_fee_calculation_type_id' => $this->getUtilityBaseFeeCalculationTypeId()
		);
	}

}
?>