<?php

class CBaseImplementationWebRetrieval extends CEosSingularBase {

	const TABLE_NAME = 'public.implementation_web_retrievals';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intWebRetrievalEscalationTypeId;
	protected $m_arrintImplementationUtilityBillIds;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_strUrl;
	protected $m_strEscalationNote;
	protected $m_strQuestionsAnswers;
	protected $m_strRetrievalInstructions;
	protected $m_strWebRetrievalDate;
	protected $m_strProcessStartDatetime;
	protected $m_strProcessEndDatetime;
	protected $m_intEscalatedBy;
	protected $m_strEscalatedOn;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUtilityCredentialId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['web_retrieval_escalation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWebRetrievalEscalationTypeId', trim( $arrValues['web_retrieval_escalation_type_id'] ) ); elseif( isset( $arrValues['web_retrieval_escalation_type_id'] ) ) $this->setWebRetrievalEscalationTypeId( $arrValues['web_retrieval_escalation_type_id'] );
		if( isset( $arrValues['implementation_utility_bill_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintImplementationUtilityBillIds', trim( $arrValues['implementation_utility_bill_ids'] ) ); elseif( isset( $arrValues['implementation_utility_bill_ids'] ) ) $this->setImplementationUtilityBillIds( $arrValues['implementation_utility_bill_ids'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( $arrValues['username'] ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( $arrValues['password_encrypted'] ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( $arrValues['password_encrypted'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( $arrValues['url'] ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( $arrValues['url'] );
		if( isset( $arrValues['escalation_note'] ) && $boolDirectSet ) $this->set( 'm_strEscalationNote', trim( $arrValues['escalation_note'] ) ); elseif( isset( $arrValues['escalation_note'] ) ) $this->setEscalationNote( $arrValues['escalation_note'] );
		if( isset( $arrValues['questions_answers'] ) && $boolDirectSet ) $this->set( 'm_strQuestionsAnswers', trim( $arrValues['questions_answers'] ) ); elseif( isset( $arrValues['questions_answers'] ) ) $this->setQuestionsAnswers( $arrValues['questions_answers'] );
		if( isset( $arrValues['retrieval_instructions'] ) && $boolDirectSet ) $this->set( 'm_strRetrievalInstructions', trim( $arrValues['retrieval_instructions'] ) ); elseif( isset( $arrValues['retrieval_instructions'] ) ) $this->setRetrievalInstructions( $arrValues['retrieval_instructions'] );
		if( isset( $arrValues['web_retrieval_date'] ) && $boolDirectSet ) $this->set( 'm_strWebRetrievalDate', trim( $arrValues['web_retrieval_date'] ) ); elseif( isset( $arrValues['web_retrieval_date'] ) ) $this->setWebRetrievalDate( $arrValues['web_retrieval_date'] );
		if( isset( $arrValues['process_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strProcessStartDatetime', trim( $arrValues['process_start_datetime'] ) ); elseif( isset( $arrValues['process_start_datetime'] ) ) $this->setProcessStartDatetime( $arrValues['process_start_datetime'] );
		if( isset( $arrValues['process_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strProcessEndDatetime', trim( $arrValues['process_end_datetime'] ) ); elseif( isset( $arrValues['process_end_datetime'] ) ) $this->setProcessEndDatetime( $arrValues['process_end_datetime'] );
		if( isset( $arrValues['escalated_by'] ) && $boolDirectSet ) $this->set( 'm_intEscalatedBy', trim( $arrValues['escalated_by'] ) ); elseif( isset( $arrValues['escalated_by'] ) ) $this->setEscalatedBy( $arrValues['escalated_by'] );
		if( isset( $arrValues['escalated_on'] ) && $boolDirectSet ) $this->set( 'm_strEscalatedOn', trim( $arrValues['escalated_on'] ) ); elseif( isset( $arrValues['escalated_on'] ) ) $this->setEscalatedOn( $arrValues['escalated_on'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['utility_credential_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityCredentialId', trim( $arrValues['utility_credential_id'] ) ); elseif( isset( $arrValues['utility_credential_id'] ) ) $this->setUtilityCredentialId( $arrValues['utility_credential_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setWebRetrievalEscalationTypeId( $intWebRetrievalEscalationTypeId ) {
		$this->set( 'm_intWebRetrievalEscalationTypeId', CStrings::strToIntDef( $intWebRetrievalEscalationTypeId, NULL, false ) );
	}

	public function getWebRetrievalEscalationTypeId() {
		return $this->m_intWebRetrievalEscalationTypeId;
	}

	public function sqlWebRetrievalEscalationTypeId() {
		return ( true == isset( $this->m_intWebRetrievalEscalationTypeId ) ) ? ( string ) $this->m_intWebRetrievalEscalationTypeId : 'NULL';
	}

	public function setImplementationUtilityBillIds( $arrintImplementationUtilityBillIds ) {
		$this->set( 'm_arrintImplementationUtilityBillIds', CStrings::strToArrIntDef( $arrintImplementationUtilityBillIds, NULL ) );
	}

	public function getImplementationUtilityBillIds() {
		return $this->m_arrintImplementationUtilityBillIds;
	}

	public function sqlImplementationUtilityBillIds() {
		return ( true == isset( $this->m_arrintImplementationUtilityBillIds ) && true == valArr( $this->m_arrintImplementationUtilityBillIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintImplementationUtilityBillIds, NULL ) . '\'' : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 50, NULL, true ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function getUsername() {
		return $this->m_strUsername;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUsername ) : '\'' . addslashes( $this->m_strUsername ) . '\'' ) : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPasswordEncrypted ) : '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' ) : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 240, NULL, true ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function getUrl() {
		return $this->m_strUrl;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUrl ) : '\'' . addslashes( $this->m_strUrl ) . '\'' ) : 'NULL';
	}

	public function setEscalationNote( $strEscalationNote ) {
		$this->set( 'm_strEscalationNote', CStrings::strTrimDef( $strEscalationNote, -1, NULL, true ) );
	}

	public function getEscalationNote() {
		return $this->m_strEscalationNote;
	}

	public function sqlEscalationNote() {
		return ( true == isset( $this->m_strEscalationNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEscalationNote ) : '\'' . addslashes( $this->m_strEscalationNote ) . '\'' ) : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function setQuestionsAnswers( $strQuestionsAnswers ) {
		$this->set( 'm_strQuestionsAnswers', CStrings::strTrimDef( $strQuestionsAnswers, -1, NULL, true ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function getQuestionsAnswers() {
		return $this->m_strQuestionsAnswers;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function sqlQuestionsAnswers() {
		return ( true == isset( $this->m_strQuestionsAnswers ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strQuestionsAnswers ) : '\'' . addslashes( $this->m_strQuestionsAnswers ) . '\'' ) : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function setRetrievalInstructions( $strRetrievalInstructions ) {
		$this->set( 'm_strRetrievalInstructions', CStrings::strTrimDef( $strRetrievalInstructions, -1, NULL, true ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function getRetrievalInstructions() {
		return $this->m_strRetrievalInstructions;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Feb 05 2020.
	 */
	public function sqlRetrievalInstructions() {
		return ( true == isset( $this->m_strRetrievalInstructions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRetrievalInstructions ) : '\'' . addslashes( $this->m_strRetrievalInstructions ) . '\'' ) : 'NULL';
	}

	public function setWebRetrievalDate( $strWebRetrievalDate ) {
		$this->set( 'm_strWebRetrievalDate', CStrings::strTrimDef( $strWebRetrievalDate, -1, NULL, true ) );
	}

	public function getWebRetrievalDate() {
		return $this->m_strWebRetrievalDate;
	}

	public function sqlWebRetrievalDate() {
		return ( true == isset( $this->m_strWebRetrievalDate ) ) ? '\'' . $this->m_strWebRetrievalDate . '\'' : 'NULL';
	}

	public function setProcessStartDatetime( $strProcessStartDatetime ) {
		$this->set( 'm_strProcessStartDatetime', CStrings::strTrimDef( $strProcessStartDatetime, -1, NULL, true ) );
	}

	public function getProcessStartDatetime() {
		return $this->m_strProcessStartDatetime;
	}

	public function sqlProcessStartDatetime() {
		return ( true == isset( $this->m_strProcessStartDatetime ) ) ? '\'' . $this->m_strProcessStartDatetime . '\'' : 'NULL';
	}

	public function setProcessEndDatetime( $strProcessEndDatetime ) {
		$this->set( 'm_strProcessEndDatetime', CStrings::strTrimDef( $strProcessEndDatetime, -1, NULL, true ) );
	}

	public function getProcessEndDatetime() {
		return $this->m_strProcessEndDatetime;
	}

	public function sqlProcessEndDatetime() {
		return ( true == isset( $this->m_strProcessEndDatetime ) ) ? '\'' . $this->m_strProcessEndDatetime . '\'' : 'NULL';
	}

	public function setEscalatedBy( $intEscalatedBy ) {
		$this->set( 'm_intEscalatedBy', CStrings::strToIntDef( $intEscalatedBy, NULL, false ) );
	}

	public function getEscalatedBy() {
		return $this->m_intEscalatedBy;
	}

	public function sqlEscalatedBy() {
		return ( true == isset( $this->m_intEscalatedBy ) ) ? ( string ) $this->m_intEscalatedBy : 'NULL';
	}

	public function setEscalatedOn( $strEscalatedOn ) {
		$this->set( 'm_strEscalatedOn', CStrings::strTrimDef( $strEscalatedOn, -1, NULL, true ) );
	}

	public function getEscalatedOn() {
		return $this->m_strEscalatedOn;
	}

	public function sqlEscalatedOn() {
		return ( true == isset( $this->m_strEscalatedOn ) ) ? '\'' . $this->m_strEscalatedOn . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUtilityCredentialId( $intUtilityCredentialId ) {
		$this->set( 'm_intUtilityCredentialId', CStrings::strToIntDef( $intUtilityCredentialId, NULL, false ) );
	}

	public function getUtilityCredentialId() {
		return $this->m_intUtilityCredentialId;
	}

	public function sqlUtilityCredentialId() {
		return ( true == isset( $this->m_intUtilityCredentialId ) ) ? ( string ) $this->m_intUtilityCredentialId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, web_retrieval_escalation_type_id, implementation_utility_bill_ids, username, password_encrypted, url, escalation_note, questions_answers, retrieval_instructions, web_retrieval_date, process_start_datetime, process_end_datetime, escalated_by, escalated_on, completed_by, completed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, utility_credential_id )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlWebRetrievalEscalationTypeId() . ', ' .
		          $this->sqlImplementationUtilityBillIds() . ', ' .
		          $this->sqlUsername() . ', ' .
		          $this->sqlPasswordEncrypted() . ', ' .
		          $this->sqlUrl() . ', ' .
		          $this->sqlEscalationNote() . ', ' .
		          $this->sqlQuestionsAnswers() . ', ' .
		          $this->sqlRetrievalInstructions() . ', ' .
		          $this->sqlWebRetrievalDate() . ', ' .
		          $this->sqlProcessStartDatetime() . ', ' .
		          $this->sqlProcessEndDatetime() . ', ' .
		          $this->sqlEscalatedBy() . ', ' .
		          $this->sqlEscalatedOn() . ', ' .
		          $this->sqlCompletedBy() . ', ' .
		          $this->sqlCompletedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlUtilityCredentialId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_retrieval_escalation_type_id = ' . $this->sqlWebRetrievalEscalationTypeId(). ',' ; } elseif( true == array_key_exists( 'WebRetrievalEscalationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' web_retrieval_escalation_type_id = ' . $this->sqlWebRetrievalEscalationTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_utility_bill_ids = ' . $this->sqlImplementationUtilityBillIds(). ',' ; } elseif( true == array_key_exists( 'ImplementationUtilityBillIds', $this->getChangedColumns() ) ) { $strSql .= ' implementation_utility_bill_ids = ' . $this->sqlImplementationUtilityBillIds() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername(). ',' ; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl(). ',' ; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalation_note = ' . $this->sqlEscalationNote(). ',' ; } elseif( true == array_key_exists( 'EscalationNote', $this->getChangedColumns() ) ) { $strSql .= ' escalation_note = ' . $this->sqlEscalationNote() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' questions_answers = ' . $this->sqlQuestionsAnswers(). ',' ; } elseif( true == array_key_exists( 'QuestionsAnswers', $this->getChangedColumns() ) ) { $strSql .= ' questions_answers = ' . $this->sqlQuestionsAnswers() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retrieval_instructions = ' . $this->sqlRetrievalInstructions(). ',' ; } elseif( true == array_key_exists( 'RetrievalInstructions', $this->getChangedColumns() ) ) { $strSql .= ' retrieval_instructions = ' . $this->sqlRetrievalInstructions() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_retrieval_date = ' . $this->sqlWebRetrievalDate(). ',' ; } elseif( true == array_key_exists( 'WebRetrievalDate', $this->getChangedColumns() ) ) { $strSql .= ' web_retrieval_date = ' . $this->sqlWebRetrievalDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' process_start_datetime = ' . $this->sqlProcessStartDatetime(). ',' ; } elseif( true == array_key_exists( 'ProcessStartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' process_start_datetime = ' . $this->sqlProcessStartDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' process_end_datetime = ' . $this->sqlProcessEndDatetime(). ',' ; } elseif( true == array_key_exists( 'ProcessEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' process_end_datetime = ' . $this->sqlProcessEndDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_by = ' . $this->sqlEscalatedBy(). ',' ; } elseif( true == array_key_exists( 'EscalatedBy', $this->getChangedColumns() ) ) { $strSql .= ' escalated_by = ' . $this->sqlEscalatedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_on = ' . $this->sqlEscalatedOn(). ',' ; } elseif( true == array_key_exists( 'EscalatedOn', $this->getChangedColumns() ) ) { $strSql .= ' escalated_on = ' . $this->sqlEscalatedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_credential_id = ' . $this->sqlUtilityCredentialId(). ',' ; } elseif( true == array_key_exists( 'UtilityCredentialId', $this->getChangedColumns() ) ) { $strSql .= ' utility_credential_id = ' . $this->sqlUtilityCredentialId() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'web_retrieval_escalation_type_id' => $this->getWebRetrievalEscalationTypeId(),
			'implementation_utility_bill_ids' => $this->getImplementationUtilityBillIds(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'url' => $this->getUrl(),
			'escalation_note' => $this->getEscalationNote(),
			'questions_answers' => $this->getQuestionsAnswers(),
			'retrieval_instructions' => $this->getRetrievalInstructions(),
			'web_retrieval_date' => $this->getWebRetrievalDate(),
			'process_start_datetime' => $this->getProcessStartDatetime(),
			'process_end_datetime' => $this->getProcessEndDatetime(),
			'escalated_by' => $this->getEscalatedBy(),
			'escalated_on' => $this->getEscalatedOn(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'utility_credential_id' => $this->getUtilityCredentialId()
		);
	}

}
?>