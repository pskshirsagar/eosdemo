<?php

class CBaseOcrTemplateLabelDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.ocr_template_label_details';

	protected $m_intId;
	protected $m_intOcrTemplateId;
	protected $m_intOcrTemplateLabelId;
	protected $m_intUtilityBillTemplateChargeId;
	protected $m_intUtilityBillAccountTemplateChargeId;
	protected $m_boolIsNotAvailable;
	protected $m_strLabelValue;
	protected $m_strValuePosition;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsNotAvailable = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ocr_template_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrTemplateId', trim( $arrValues['ocr_template_id'] ) ); elseif( isset( $arrValues['ocr_template_id'] ) ) $this->setOcrTemplateId( $arrValues['ocr_template_id'] );
		if( isset( $arrValues['ocr_template_label_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrTemplateLabelId', trim( $arrValues['ocr_template_label_id'] ) ); elseif( isset( $arrValues['ocr_template_label_id'] ) ) $this->setOcrTemplateLabelId( $arrValues['ocr_template_label_id'] );
		if( isset( $arrValues['utility_bill_template_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillTemplateChargeId', trim( $arrValues['utility_bill_template_charge_id'] ) ); elseif( isset( $arrValues['utility_bill_template_charge_id'] ) ) $this->setUtilityBillTemplateChargeId( $arrValues['utility_bill_template_charge_id'] );
		if( isset( $arrValues['utility_bill_account_template_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountTemplateChargeId', trim( $arrValues['utility_bill_account_template_charge_id'] ) ); elseif( isset( $arrValues['utility_bill_account_template_charge_id'] ) ) $this->setUtilityBillAccountTemplateChargeId( $arrValues['utility_bill_account_template_charge_id'] );
		if( isset( $arrValues['is_not_available'] ) && $boolDirectSet ) $this->set( 'm_boolIsNotAvailable', trim( stripcslashes( $arrValues['is_not_available'] ) ) ); elseif( isset( $arrValues['is_not_available'] ) ) $this->setIsNotAvailable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_not_available'] ) : $arrValues['is_not_available'] );
		if( isset( $arrValues['label_value'] ) && $boolDirectSet ) $this->set( 'm_strLabelValue', trim( stripcslashes( $arrValues['label_value'] ) ) ); elseif( isset( $arrValues['label_value'] ) ) $this->setLabelValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['label_value'] ) : $arrValues['label_value'] );
		if( isset( $arrValues['value_position'] ) && $boolDirectSet ) $this->set( 'm_strValuePosition', trim( stripcslashes( $arrValues['value_position'] ) ) ); elseif( isset( $arrValues['value_position'] ) ) $this->setValuePosition( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['value_position'] ) : $arrValues['value_position'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOcrTemplateId( $intOcrTemplateId ) {
		$this->set( 'm_intOcrTemplateId', CStrings::strToIntDef( $intOcrTemplateId, NULL, false ) );
	}

	public function getOcrTemplateId() {
		return $this->m_intOcrTemplateId;
	}

	public function sqlOcrTemplateId() {
		return ( true == isset( $this->m_intOcrTemplateId ) ) ? ( string ) $this->m_intOcrTemplateId : 'NULL';
	}

	public function setOcrTemplateLabelId( $intOcrTemplateLabelId ) {
		$this->set( 'm_intOcrTemplateLabelId', CStrings::strToIntDef( $intOcrTemplateLabelId, NULL, false ) );
	}

	public function getOcrTemplateLabelId() {
		return $this->m_intOcrTemplateLabelId;
	}

	public function sqlOcrTemplateLabelId() {
		return ( true == isset( $this->m_intOcrTemplateLabelId ) ) ? ( string ) $this->m_intOcrTemplateLabelId : 'NULL';
	}

	public function setUtilityBillTemplateChargeId( $intUtilityBillTemplateChargeId ) {
		$this->set( 'm_intUtilityBillTemplateChargeId', CStrings::strToIntDef( $intUtilityBillTemplateChargeId, NULL, false ) );
	}

	public function getUtilityBillTemplateChargeId() {
		return $this->m_intUtilityBillTemplateChargeId;
	}

	public function sqlUtilityBillTemplateChargeId() {
		return ( true == isset( $this->m_intUtilityBillTemplateChargeId ) ) ? ( string ) $this->m_intUtilityBillTemplateChargeId : 'NULL';
	}

	public function setUtilityBillAccountTemplateChargeId( $intUtilityBillAccountTemplateChargeId ) {
		$this->set( 'm_intUtilityBillAccountTemplateChargeId', CStrings::strToIntDef( $intUtilityBillAccountTemplateChargeId, NULL, false ) );
	}

	public function getUtilityBillAccountTemplateChargeId() {
		return $this->m_intUtilityBillAccountTemplateChargeId;
	}

	public function sqlUtilityBillAccountTemplateChargeId() {
		return ( true == isset( $this->m_intUtilityBillAccountTemplateChargeId ) ) ? ( string ) $this->m_intUtilityBillAccountTemplateChargeId : 'NULL';
	}

	public function setIsNotAvailable( $boolIsNotAvailable ) {
		$this->set( 'm_boolIsNotAvailable', CStrings::strToBool( $boolIsNotAvailable ) );
	}

	public function getIsNotAvailable() {
		return $this->m_boolIsNotAvailable;
	}

	public function sqlIsNotAvailable() {
		return ( true == isset( $this->m_boolIsNotAvailable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNotAvailable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLabelValue( $strLabelValue ) {
		$this->set( 'm_strLabelValue', CStrings::strTrimDef( $strLabelValue, 200, NULL, true ) );
	}

	public function getLabelValue() {
		return $this->m_strLabelValue;
	}

	public function sqlLabelValue() {
		return ( true == isset( $this->m_strLabelValue ) ) ? '\'' . addslashes( $this->m_strLabelValue ) . '\'' : 'NULL';
	}

	public function setValuePosition( $strValuePosition ) {
		$this->set( 'm_strValuePosition', CStrings::strTrimDef( $strValuePosition, 100, NULL, true ) );
	}

	public function getValuePosition() {
		return $this->m_strValuePosition;
	}

	public function sqlValuePosition() {
		return ( true == isset( $this->m_strValuePosition ) ) ? '\'' . addslashes( $this->m_strValuePosition ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ocr_template_id, ocr_template_label_id, utility_bill_template_charge_id, utility_bill_account_template_charge_id, is_not_available, label_value, value_position, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlOcrTemplateId() . ', ' .
						$this->sqlOcrTemplateLabelId() . ', ' .
						$this->sqlUtilityBillTemplateChargeId() . ', ' .
						$this->sqlUtilityBillAccountTemplateChargeId() . ', ' .
						$this->sqlIsNotAvailable() . ', ' .
						$this->sqlLabelValue() . ', ' .
						$this->sqlValuePosition() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_template_id = ' . $this->sqlOcrTemplateId(). ',' ; } elseif( true == array_key_exists( 'OcrTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_template_id = ' . $this->sqlOcrTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_template_label_id = ' . $this->sqlOcrTemplateLabelId(). ',' ; } elseif( true == array_key_exists( 'OcrTemplateLabelId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_template_label_id = ' . $this->sqlOcrTemplateLabelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_template_charge_id = ' . $this->sqlUtilityBillTemplateChargeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillTemplateChargeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_template_charge_id = ' . $this->sqlUtilityBillTemplateChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_template_charge_id = ' . $this->sqlUtilityBillAccountTemplateChargeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountTemplateChargeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_template_charge_id = ' . $this->sqlUtilityBillAccountTemplateChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_not_available = ' . $this->sqlIsNotAvailable(). ',' ; } elseif( true == array_key_exists( 'IsNotAvailable', $this->getChangedColumns() ) ) { $strSql .= ' is_not_available = ' . $this->sqlIsNotAvailable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label_value = ' . $this->sqlLabelValue(). ',' ; } elseif( true == array_key_exists( 'LabelValue', $this->getChangedColumns() ) ) { $strSql .= ' label_value = ' . $this->sqlLabelValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value_position = ' . $this->sqlValuePosition(). ',' ; } elseif( true == array_key_exists( 'ValuePosition', $this->getChangedColumns() ) ) { $strSql .= ' value_position = ' . $this->sqlValuePosition() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ocr_template_id' => $this->getOcrTemplateId(),
			'ocr_template_label_id' => $this->getOcrTemplateLabelId(),
			'utility_bill_template_charge_id' => $this->getUtilityBillTemplateChargeId(),
			'utility_bill_account_template_charge_id' => $this->getUtilityBillAccountTemplateChargeId(),
			'is_not_available' => $this->getIsNotAvailable(),
			'label_value' => $this->getLabelValue(),
			'value_position' => $this->getValuePosition(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>