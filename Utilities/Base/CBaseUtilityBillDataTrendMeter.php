<?php

class CBaseUtilityBillDataTrendMeter extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_data_trend_meters';

	protected $m_intId;
	protected $m_intUtilityBillMonthDataTrendId;
	protected $m_intCid;
	protected $m_intUtilityBillAccountMeterId;
	protected $m_strMonth;
	protected $m_fltExpense;
	protected $m_fltConsumption;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_bill_month_data_trend_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillMonthDataTrendId', trim( $arrValues['utility_bill_month_data_trend_id'] ) ); elseif( isset( $arrValues['utility_bill_month_data_trend_id'] ) ) $this->setUtilityBillMonthDataTrendId( $arrValues['utility_bill_month_data_trend_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_bill_account_meter_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountMeterId', trim( $arrValues['utility_bill_account_meter_id'] ) ); elseif( isset( $arrValues['utility_bill_account_meter_id'] ) ) $this->setUtilityBillAccountMeterId( $arrValues['utility_bill_account_meter_id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['expense'] ) && $boolDirectSet ) $this->set( 'm_fltExpense', trim( $arrValues['expense'] ) ); elseif( isset( $arrValues['expense'] ) ) $this->setExpense( $arrValues['expense'] );
		if( isset( $arrValues['consumption'] ) && $boolDirectSet ) $this->set( 'm_fltConsumption', trim( $arrValues['consumption'] ) ); elseif( isset( $arrValues['consumption'] ) ) $this->setConsumption( $arrValues['consumption'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBillMonthDataTrendId( $intUtilityBillMonthDataTrendId ) {
		$this->set( 'm_intUtilityBillMonthDataTrendId', CStrings::strToIntDef( $intUtilityBillMonthDataTrendId, NULL, false ) );
	}

	public function getUtilityBillMonthDataTrendId() {
		return $this->m_intUtilityBillMonthDataTrendId;
	}

	public function sqlUtilityBillMonthDataTrendId() {
		return ( true == isset( $this->m_intUtilityBillMonthDataTrendId ) ) ? ( string ) $this->m_intUtilityBillMonthDataTrendId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUtilityBillAccountMeterId( $intUtilityBillAccountMeterId ) {
		$this->set( 'm_intUtilityBillAccountMeterId', CStrings::strToIntDef( $intUtilityBillAccountMeterId, NULL, false ) );
	}

	public function getUtilityBillAccountMeterId() {
		return $this->m_intUtilityBillAccountMeterId;
	}

	public function sqlUtilityBillAccountMeterId() {
		return ( true == isset( $this->m_intUtilityBillAccountMeterId ) ) ? ( string ) $this->m_intUtilityBillAccountMeterId : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NULL';
	}

	public function setExpense( $fltExpense ) {
		$this->set( 'm_fltExpense', CStrings::strToFloatDef( $fltExpense, NULL, false, 0 ) );
	}

	public function getExpense() {
		return $this->m_fltExpense;
	}

	public function sqlExpense() {
		return ( true == isset( $this->m_fltExpense ) ) ? ( string ) $this->m_fltExpense : 'NULL';
	}

	public function setConsumption( $fltConsumption ) {
		$this->set( 'm_fltConsumption', CStrings::strToFloatDef( $fltConsumption, NULL, false, 0 ) );
	}

	public function getConsumption() {
		return $this->m_fltConsumption;
	}

	public function sqlConsumption() {
		return ( true == isset( $this->m_fltConsumption ) ) ? ( string ) $this->m_fltConsumption : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_bill_month_data_trend_id, cid, utility_bill_account_meter_id, month, expense, consumption, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlUtilityBillMonthDataTrendId() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlUtilityBillAccountMeterId() . ', ' .
		          $this->sqlMonth() . ', ' .
		          $this->sqlExpense() . ', ' .
		          $this->sqlConsumption() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_month_data_trend_id = ' . $this->sqlUtilityBillMonthDataTrendId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillMonthDataTrendId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_month_data_trend_id = ' . $this->sqlUtilityBillMonthDataTrendId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_meter_id = ' . $this->sqlUtilityBillAccountMeterId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountMeterId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_meter_id = ' . $this->sqlUtilityBillAccountMeterId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth(). ',' ; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expense = ' . $this->sqlExpense(). ',' ; } elseif( true == array_key_exists( 'Expense', $this->getChangedColumns() ) ) { $strSql .= ' expense = ' . $this->sqlExpense() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumption = ' . $this->sqlConsumption(). ',' ; } elseif( true == array_key_exists( 'Consumption', $this->getChangedColumns() ) ) { $strSql .= ' consumption = ' . $this->sqlConsumption() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_bill_month_data_trend_id' => $this->getUtilityBillMonthDataTrendId(),
			'cid' => $this->getCid(),
			'utility_bill_account_meter_id' => $this->getUtilityBillAccountMeterId(),
			'month' => $this->getMonth(),
			'expense' => $this->getExpense(),
			'consumption' => $this->getConsumption(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>