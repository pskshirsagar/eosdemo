<?php

class CBaseDisconnectNotice extends CEosSingularBase {

	const TABLE_NAME = 'public.disconnect_notices';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillAccountId;
	protected $m_intUtilityDocumentId;
	protected $m_strDisconnectNoticeDatetime;
	protected $m_intAssociatedBy;
	protected $m_strAssociatedOn;
	protected $m_strResolvedNote;
	protected $m_intResolvedBy;
	protected $m_strResolvedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['utility_document_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDocumentId', trim( $arrValues['utility_document_id'] ) ); elseif( isset( $arrValues['utility_document_id'] ) ) $this->setUtilityDocumentId( $arrValues['utility_document_id'] );
		if( isset( $arrValues['disconnect_notice_datetime'] ) && $boolDirectSet ) $this->set( 'm_strDisconnectNoticeDatetime', trim( $arrValues['disconnect_notice_datetime'] ) ); elseif( isset( $arrValues['disconnect_notice_datetime'] ) ) $this->setDisconnectNoticeDatetime( $arrValues['disconnect_notice_datetime'] );
		if( isset( $arrValues['associated_by'] ) && $boolDirectSet ) $this->set( 'm_intAssociatedBy', trim( $arrValues['associated_by'] ) ); elseif( isset( $arrValues['associated_by'] ) ) $this->setAssociatedBy( $arrValues['associated_by'] );
		if( isset( $arrValues['associated_on'] ) && $boolDirectSet ) $this->set( 'm_strAssociatedOn', trim( $arrValues['associated_on'] ) ); elseif( isset( $arrValues['associated_on'] ) ) $this->setAssociatedOn( $arrValues['associated_on'] );
		if( isset( $arrValues['resolved_note'] ) && $boolDirectSet ) $this->set( 'm_strResolvedNote', trim( $arrValues['resolved_note'] ) ); elseif( isset( $arrValues['resolved_note'] ) ) $this->setResolvedNote( $arrValues['resolved_note'] );
		if( isset( $arrValues['resolved_by'] ) && $boolDirectSet ) $this->set( 'm_intResolvedBy', trim( $arrValues['resolved_by'] ) ); elseif( isset( $arrValues['resolved_by'] ) ) $this->setResolvedBy( $arrValues['resolved_by'] );
		if( isset( $arrValues['resolved_on'] ) && $boolDirectSet ) $this->set( 'm_strResolvedOn', trim( $arrValues['resolved_on'] ) ); elseif( isset( $arrValues['resolved_on'] ) ) $this->setResolvedOn( $arrValues['resolved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setUtilityDocumentId( $intUtilityDocumentId ) {
		$this->set( 'm_intUtilityDocumentId', CStrings::strToIntDef( $intUtilityDocumentId, NULL, false ) );
	}

	public function getUtilityDocumentId() {
		return $this->m_intUtilityDocumentId;
	}

	public function sqlUtilityDocumentId() {
		return ( true == isset( $this->m_intUtilityDocumentId ) ) ? ( string ) $this->m_intUtilityDocumentId : 'NULL';
	}

	public function setDisconnectNoticeDatetime( $strDisconnectNoticeDatetime ) {
		$this->set( 'm_strDisconnectNoticeDatetime', CStrings::strTrimDef( $strDisconnectNoticeDatetime, -1, NULL, true ) );
	}

	public function getDisconnectNoticeDatetime() {
		return $this->m_strDisconnectNoticeDatetime;
	}

	public function sqlDisconnectNoticeDatetime() {
		return ( true == isset( $this->m_strDisconnectNoticeDatetime ) ) ? '\'' . $this->m_strDisconnectNoticeDatetime . '\'' : 'NOW()';
	}

	public function setAssociatedBy( $intAssociatedBy ) {
		$this->set( 'm_intAssociatedBy', CStrings::strToIntDef( $intAssociatedBy, NULL, false ) );
	}

	public function getAssociatedBy() {
		return $this->m_intAssociatedBy;
	}

	public function sqlAssociatedBy() {
		return ( true == isset( $this->m_intAssociatedBy ) ) ? ( string ) $this->m_intAssociatedBy : 'NULL';
	}

	public function setAssociatedOn( $strAssociatedOn ) {
		$this->set( 'm_strAssociatedOn', CStrings::strTrimDef( $strAssociatedOn, -1, NULL, true ) );
	}

	public function getAssociatedOn() {
		return $this->m_strAssociatedOn;
	}

	public function sqlAssociatedOn() {
		return ( true == isset( $this->m_strAssociatedOn ) ) ? '\'' . $this->m_strAssociatedOn . '\'' : 'NULL';
	}

	public function setResolvedNote( $strResolvedNote ) {
		$this->set( 'm_strResolvedNote', CStrings::strTrimDef( $strResolvedNote, -1, NULL, true ) );
	}

	public function getResolvedNote() {
		return $this->m_strResolvedNote;
	}

	public function sqlResolvedNote() {
		return ( true == isset( $this->m_strResolvedNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResolvedNote ) : '\'' . addslashes( $this->m_strResolvedNote ) . '\'' ) : 'NULL';
	}

	public function setResolvedBy( $intResolvedBy ) {
		$this->set( 'm_intResolvedBy', CStrings::strToIntDef( $intResolvedBy, NULL, false ) );
	}

	public function getResolvedBy() {
		return $this->m_intResolvedBy;
	}

	public function sqlResolvedBy() {
		return ( true == isset( $this->m_intResolvedBy ) ) ? ( string ) $this->m_intResolvedBy : 'NULL';
	}

	public function setResolvedOn( $strResolvedOn ) {
		$this->set( 'm_strResolvedOn', CStrings::strTrimDef( $strResolvedOn, -1, NULL, true ) );
	}

	public function getResolvedOn() {
		return $this->m_strResolvedOn;
	}

	public function sqlResolvedOn() {
		return ( true == isset( $this->m_strResolvedOn ) ) ? '\'' . $this->m_strResolvedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_account_id, utility_document_id, disconnect_notice_datetime, associated_by, associated_on, resolved_note, resolved_by, resolved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityBillAccountId() . ', ' .
						$this->sqlUtilityDocumentId() . ', ' .
						$this->sqlDisconnectNoticeDatetime() . ', ' .
						$this->sqlAssociatedBy() . ', ' .
						$this->sqlAssociatedOn() . ', ' .
						$this->sqlResolvedNote() . ', ' .
						$this->sqlResolvedBy() . ', ' .
						$this->sqlResolvedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId(). ',' ; } elseif( true == array_key_exists( 'UtilityDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disconnect_notice_datetime = ' . $this->sqlDisconnectNoticeDatetime(). ',' ; } elseif( true == array_key_exists( 'DisconnectNoticeDatetime', $this->getChangedColumns() ) ) { $strSql .= ' disconnect_notice_datetime = ' . $this->sqlDisconnectNoticeDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_by = ' . $this->sqlAssociatedBy(). ',' ; } elseif( true == array_key_exists( 'AssociatedBy', $this->getChangedColumns() ) ) { $strSql .= ' associated_by = ' . $this->sqlAssociatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_on = ' . $this->sqlAssociatedOn(). ',' ; } elseif( true == array_key_exists( 'AssociatedOn', $this->getChangedColumns() ) ) { $strSql .= ' associated_on = ' . $this->sqlAssociatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_note = ' . $this->sqlResolvedNote(). ',' ; } elseif( true == array_key_exists( 'ResolvedNote', $this->getChangedColumns() ) ) { $strSql .= ' resolved_note = ' . $this->sqlResolvedNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy(). ',' ; } elseif( true == array_key_exists( 'ResolvedBy', $this->getChangedColumns() ) ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn(). ',' ; } elseif( true == array_key_exists( 'ResolvedOn', $this->getChangedColumns() ) ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'utility_document_id' => $this->getUtilityDocumentId(),
			'disconnect_notice_datetime' => $this->getDisconnectNoticeDatetime(),
			'associated_by' => $this->getAssociatedBy(),
			'associated_on' => $this->getAssociatedOn(),
			'resolved_note' => $this->getResolvedNote(),
			'resolved_by' => $this->getResolvedBy(),
			'resolved_on' => $this->getResolvedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>