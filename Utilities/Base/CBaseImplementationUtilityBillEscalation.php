<?php

class CBaseImplementationUtilityBillEscalation extends CEosSingularBase {

	const TABLE_NAME = 'public.implementation_utility_bill_escalations';

	protected $m_intId;
	protected $m_intImplementationUtilityBillId;
	protected $m_intBillEscalateTypeId;
	protected $m_strEscalationNote;
	protected $m_intEscalatedBy;
	protected $m_strEscalatedOn;
	protected $m_strCompletionNote;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['implementation_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationUtilityBillId', trim( $arrValues['implementation_utility_bill_id'] ) ); elseif( isset( $arrValues['implementation_utility_bill_id'] ) ) $this->setImplementationUtilityBillId( $arrValues['implementation_utility_bill_id'] );
		if( isset( $arrValues['bill_escalate_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillEscalateTypeId', trim( $arrValues['bill_escalate_type_id'] ) ); elseif( isset( $arrValues['bill_escalate_type_id'] ) ) $this->setBillEscalateTypeId( $arrValues['bill_escalate_type_id'] );
		if( isset( $arrValues['escalation_note'] ) && $boolDirectSet ) $this->set( 'm_strEscalationNote', trim( stripcslashes( $arrValues['escalation_note'] ) ) ); elseif( isset( $arrValues['escalation_note'] ) ) $this->setEscalationNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['escalation_note'] ) : $arrValues['escalation_note'] );
		if( isset( $arrValues['escalated_by'] ) && $boolDirectSet ) $this->set( 'm_intEscalatedBy', trim( $arrValues['escalated_by'] ) ); elseif( isset( $arrValues['escalated_by'] ) ) $this->setEscalatedBy( $arrValues['escalated_by'] );
		if( isset( $arrValues['escalated_on'] ) && $boolDirectSet ) $this->set( 'm_strEscalatedOn', trim( $arrValues['escalated_on'] ) ); elseif( isset( $arrValues['escalated_on'] ) ) $this->setEscalatedOn( $arrValues['escalated_on'] );
		if( isset( $arrValues['completion_note'] ) && $boolDirectSet ) $this->set( 'm_strCompletionNote', trim( stripcslashes( $arrValues['completion_note'] ) ) ); elseif( isset( $arrValues['completion_note'] ) ) $this->setCompletionNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['completion_note'] ) : $arrValues['completion_note'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setImplementationUtilityBillId( $intImplementationUtilityBillId ) {
		$this->set( 'm_intImplementationUtilityBillId', CStrings::strToIntDef( $intImplementationUtilityBillId, NULL, false ) );
	}

	public function getImplementationUtilityBillId() {
		return $this->m_intImplementationUtilityBillId;
	}

	public function sqlImplementationUtilityBillId() {
		return ( true == isset( $this->m_intImplementationUtilityBillId ) ) ? ( string ) $this->m_intImplementationUtilityBillId : 'NULL';
	}

	public function setBillEscalateTypeId( $intBillEscalateTypeId ) {
		$this->set( 'm_intBillEscalateTypeId', CStrings::strToIntDef( $intBillEscalateTypeId, NULL, false ) );
	}

	public function getBillEscalateTypeId() {
		return $this->m_intBillEscalateTypeId;
	}

	public function sqlBillEscalateTypeId() {
		return ( true == isset( $this->m_intBillEscalateTypeId ) ) ? ( string ) $this->m_intBillEscalateTypeId : 'NULL';
	}

	public function setEscalationNote( $strEscalationNote ) {
		$this->set( 'm_strEscalationNote', CStrings::strTrimDef( $strEscalationNote, -1, NULL, true ) );
	}

	public function getEscalationNote() {
		return $this->m_strEscalationNote;
	}

	public function sqlEscalationNote() {
		return ( true == isset( $this->m_strEscalationNote ) ) ? '\'' . addslashes( $this->m_strEscalationNote ) . '\'' : 'NULL';
	}

	public function setEscalatedBy( $intEscalatedBy ) {
		$this->set( 'm_intEscalatedBy', CStrings::strToIntDef( $intEscalatedBy, NULL, false ) );
	}

	public function getEscalatedBy() {
		return $this->m_intEscalatedBy;
	}

	public function sqlEscalatedBy() {
		return ( true == isset( $this->m_intEscalatedBy ) ) ? ( string ) $this->m_intEscalatedBy : 'NULL';
	}

	public function setEscalatedOn( $strEscalatedOn ) {
		$this->set( 'm_strEscalatedOn', CStrings::strTrimDef( $strEscalatedOn, -1, NULL, true ) );
	}

	public function getEscalatedOn() {
		return $this->m_strEscalatedOn;
	}

	public function sqlEscalatedOn() {
		return ( true == isset( $this->m_strEscalatedOn ) ) ? '\'' . $this->m_strEscalatedOn . '\'' : 'NULL';
	}

	public function setCompletionNote( $strCompletionNote ) {
		$this->set( 'm_strCompletionNote', CStrings::strTrimDef( $strCompletionNote, -1, NULL, true ) );
	}

	public function getCompletionNote() {
		return $this->m_strCompletionNote;
	}

	public function sqlCompletionNote() {
		return ( true == isset( $this->m_strCompletionNote ) ) ? '\'' . addslashes( $this->m_strCompletionNote ) . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, implementation_utility_bill_id, bill_escalate_type_id, escalation_note, escalated_by, escalated_on, completion_note, completed_by, completed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlImplementationUtilityBillId() . ', ' .
						$this->sqlBillEscalateTypeId() . ', ' .
						$this->sqlEscalationNote() . ', ' .
						$this->sqlEscalatedBy() . ', ' .
						$this->sqlEscalatedOn() . ', ' .
						$this->sqlCompletionNote() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_utility_bill_id = ' . $this->sqlImplementationUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'ImplementationUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_utility_bill_id = ' . $this->sqlImplementationUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_escalate_type_id = ' . $this->sqlBillEscalateTypeId(). ',' ; } elseif( true == array_key_exists( 'BillEscalateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bill_escalate_type_id = ' . $this->sqlBillEscalateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalation_note = ' . $this->sqlEscalationNote(). ',' ; } elseif( true == array_key_exists( 'EscalationNote', $this->getChangedColumns() ) ) { $strSql .= ' escalation_note = ' . $this->sqlEscalationNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_by = ' . $this->sqlEscalatedBy(). ',' ; } elseif( true == array_key_exists( 'EscalatedBy', $this->getChangedColumns() ) ) { $strSql .= ' escalated_by = ' . $this->sqlEscalatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_on = ' . $this->sqlEscalatedOn(). ',' ; } elseif( true == array_key_exists( 'EscalatedOn', $this->getChangedColumns() ) ) { $strSql .= ' escalated_on = ' . $this->sqlEscalatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completion_note = ' . $this->sqlCompletionNote(). ',' ; } elseif( true == array_key_exists( 'CompletionNote', $this->getChangedColumns() ) ) { $strSql .= ' completion_note = ' . $this->sqlCompletionNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'implementation_utility_bill_id' => $this->getImplementationUtilityBillId(),
			'bill_escalate_type_id' => $this->getBillEscalateTypeId(),
			'escalation_note' => $this->getEscalationNote(),
			'escalated_by' => $this->getEscalatedBy(),
			'escalated_on' => $this->getEscalatedOn(),
			'completion_note' => $this->getCompletionNote(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>