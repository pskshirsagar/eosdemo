<?php

class CBaseUtilityInvoiceSetting extends CEosSingularBase {

    const TABLE_NAME = 'public.utility_invoice_settings';

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPropertyId;
    protected $m_intPropertyUtilitySettingId;
    protected $m_intUtilityInvoiceDeliveryTypeId;
    protected $m_intInvoiceDueDay;
    protected $m_intInvoiceMinimumDueDays;
    protected $m_strNonResidentPortalUrl;
    protected $m_strBackPageFooter;
    protected $m_boolHideOnlinePayment;
    protected $m_boolDontSendUndeliveredInvoicesNotification;
    protected $m_boolMergeBillingFee;
    protected $m_boolIsBillingFeeItemize;
    protected $m_boolExcludePaymentAddress;
    protected $m_boolHideResidentInvoiceSpanish;
    protected $m_boolHideResidentInvoiceBillingDisputes;
    protected $m_boolIsHideUsageHistory;
    protected $m_boolIsHideEcheck;
    protected $m_boolIsHideMasterCard;
    protected $m_boolIsHideVisa;
    protected $m_boolIsHideDiscover;
    protected $m_boolIsHideAmericanExpress;
    protected $m_boolIsAllowMoneyGram;
    protected $m_boolIsExcludeWeekends;
    protected $m_boolIsExcludeHolidays;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;
    protected $m_boolDisplayLeaseId;

    public function __construct() {
        parent::__construct();

        $this->m_intInvoiceDueDay = '1';
        $this->m_boolHideOnlinePayment = false;
        $this->m_boolDontSendUndeliveredInvoicesNotification = false;
        $this->m_boolMergeBillingFee = false;
        $this->m_boolIsBillingFeeItemize = false;
        $this->m_boolExcludePaymentAddress = false;
        $this->m_boolHideResidentInvoiceSpanish = false;
        $this->m_boolHideResidentInvoiceBillingDisputes = false;
        $this->m_boolIsHideUsageHistory = false;
        $this->m_boolIsHideEcheck = false;
        $this->m_boolIsHideMasterCard = false;
        $this->m_boolIsHideVisa = false;
        $this->m_boolIsHideDiscover = false;
        $this->m_boolIsHideAmericanExpress = false;
        $this->m_boolIsAllowMoneyGram = false;
        $this->m_boolIsExcludeWeekends = false;
        $this->m_boolIsExcludeHolidays = false;
        $this->m_boolDisplayLeaseId = false;

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['property_utility_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilitySettingId', trim( $arrValues['property_utility_setting_id'] ) ); elseif( isset( $arrValues['property_utility_setting_id'] ) ) $this->setPropertyUtilitySettingId( $arrValues['property_utility_setting_id'] );
        if( isset( $arrValues['utility_invoice_delivery_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityInvoiceDeliveryTypeId', trim( $arrValues['utility_invoice_delivery_type_id'] ) ); elseif( isset( $arrValues['utility_invoice_delivery_type_id'] ) ) $this->setUtilityInvoiceDeliveryTypeId( $arrValues['utility_invoice_delivery_type_id'] );
        if( isset( $arrValues['invoice_due_day'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceDueDay', trim( $arrValues['invoice_due_day'] ) ); elseif( isset( $arrValues['invoice_due_day'] ) ) $this->setInvoiceDueDay( $arrValues['invoice_due_day'] );
        if( isset( $arrValues['invoice_minimum_due_days'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceMinimumDueDays', trim( $arrValues['invoice_minimum_due_days'] ) ); elseif( isset( $arrValues['invoice_minimum_due_days'] ) ) $this->setInvoiceMinimumDueDays( $arrValues['invoice_minimum_due_days'] );
        if( isset( $arrValues['non_resident_portal_url'] ) && $boolDirectSet ) $this->set( 'm_strNonResidentPortalUrl', trim( $arrValues['non_resident_portal_url'] ) ); elseif( isset( $arrValues['non_resident_portal_url'] ) ) $this->setNonResidentPortalUrl( $arrValues['non_resident_portal_url'] );
        if( isset( $arrValues['back_page_footer'] ) && $boolDirectSet ) $this->set( 'm_strBackPageFooter', trim( $arrValues['back_page_footer'] ) ); elseif( isset( $arrValues['back_page_footer'] ) ) $this->setBackPageFooter( $arrValues['back_page_footer'] );
        if( isset( $arrValues['hide_online_payment'] ) && $boolDirectSet ) $this->set( 'm_boolHideOnlinePayment', trim( stripcslashes( $arrValues['hide_online_payment'] ) ) ); elseif( isset( $arrValues['hide_online_payment'] ) ) $this->setHideOnlinePayment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_online_payment'] ) : $arrValues['hide_online_payment'] );
        if( isset( $arrValues['dont_send_undelivered_invoices_notification'] ) && $boolDirectSet ) $this->set( 'm_boolDontSendUndeliveredInvoicesNotification', trim( stripcslashes( $arrValues['dont_send_undelivered_invoices_notification'] ) ) ); elseif( isset( $arrValues['dont_send_undelivered_invoices_notification'] ) ) $this->setDontSendUndeliveredInvoicesNotification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_send_undelivered_invoices_notification'] ) : $arrValues['dont_send_undelivered_invoices_notification'] );
        if( isset( $arrValues['merge_billing_fee'] ) && $boolDirectSet ) $this->set( 'm_boolMergeBillingFee', trim( stripcslashes( $arrValues['merge_billing_fee'] ) ) ); elseif( isset( $arrValues['merge_billing_fee'] ) ) $this->setMergeBillingFee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['merge_billing_fee'] ) : $arrValues['merge_billing_fee'] );
        if( isset( $arrValues['is_billing_fee_itemize'] ) && $boolDirectSet ) $this->set( 'm_boolIsBillingFeeItemize', trim( stripcslashes( $arrValues['is_billing_fee_itemize'] ) ) ); elseif( isset( $arrValues['is_billing_fee_itemize'] ) ) $this->setIsBillingFeeItemize( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_billing_fee_itemize'] ) : $arrValues['is_billing_fee_itemize'] );
        if( isset( $arrValues['exclude_payment_address'] ) && $boolDirectSet ) $this->set( 'm_boolExcludePaymentAddress', trim( stripcslashes( $arrValues['exclude_payment_address'] ) ) ); elseif( isset( $arrValues['exclude_payment_address'] ) ) $this->setExcludePaymentAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exclude_payment_address'] ) : $arrValues['exclude_payment_address'] );
        if( isset( $arrValues['hide_resident_invoice_spanish'] ) && $boolDirectSet ) $this->set( 'm_boolHideResidentInvoiceSpanish', trim( stripcslashes( $arrValues['hide_resident_invoice_spanish'] ) ) ); elseif( isset( $arrValues['hide_resident_invoice_spanish'] ) ) $this->setHideResidentInvoiceSpanish( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_resident_invoice_spanish'] ) : $arrValues['hide_resident_invoice_spanish'] );
        if( isset( $arrValues['hide_resident_invoice_billing_disputes'] ) && $boolDirectSet ) $this->set( 'm_boolHideResidentInvoiceBillingDisputes', trim( stripcslashes( $arrValues['hide_resident_invoice_billing_disputes'] ) ) ); elseif( isset( $arrValues['hide_resident_invoice_billing_disputes'] ) ) $this->setHideResidentInvoiceBillingDisputes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_resident_invoice_billing_disputes'] ) : $arrValues['hide_resident_invoice_billing_disputes'] );
        if( isset( $arrValues['is_hide_usage_history'] ) && $boolDirectSet ) $this->set( 'm_boolIsHideUsageHistory', trim( stripcslashes( $arrValues['is_hide_usage_history'] ) ) ); elseif( isset( $arrValues['is_hide_usage_history'] ) ) $this->setIsHideUsageHistory( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hide_usage_history'] ) : $arrValues['is_hide_usage_history'] );
        if( isset( $arrValues['is_hide_echeck'] ) && $boolDirectSet ) $this->set( 'm_boolIsHideEcheck', trim( stripcslashes( $arrValues['is_hide_echeck'] ) ) ); elseif( isset( $arrValues['is_hide_echeck'] ) ) $this->setIsHideEcheck( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hide_echeck'] ) : $arrValues['is_hide_echeck'] );
        if( isset( $arrValues['is_hide_master_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsHideMasterCard', trim( stripcslashes( $arrValues['is_hide_master_card'] ) ) ); elseif( isset( $arrValues['is_hide_master_card'] ) ) $this->setIsHideMasterCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hide_master_card'] ) : $arrValues['is_hide_master_card'] );
        if( isset( $arrValues['is_hide_visa'] ) && $boolDirectSet ) $this->set( 'm_boolIsHideVisa', trim( stripcslashes( $arrValues['is_hide_visa'] ) ) ); elseif( isset( $arrValues['is_hide_visa'] ) ) $this->setIsHideVisa( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hide_visa'] ) : $arrValues['is_hide_visa'] );
        if( isset( $arrValues['is_hide_discover'] ) && $boolDirectSet ) $this->set( 'm_boolIsHideDiscover', trim( stripcslashes( $arrValues['is_hide_discover'] ) ) ); elseif( isset( $arrValues['is_hide_discover'] ) ) $this->setIsHideDiscover( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hide_discover'] ) : $arrValues['is_hide_discover'] );
        if( isset( $arrValues['is_hide_american_express'] ) && $boolDirectSet ) $this->set( 'm_boolIsHideAmericanExpress', trim( stripcslashes( $arrValues['is_hide_american_express'] ) ) ); elseif( isset( $arrValues['is_hide_american_express'] ) ) $this->setIsHideAmericanExpress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hide_american_express'] ) : $arrValues['is_hide_american_express'] );
        if( isset( $arrValues['is_allow_money_gram'] ) && $boolDirectSet ) $this->set( 'm_boolIsAllowMoneyGram', trim( stripcslashes( $arrValues['is_allow_money_gram'] ) ) ); elseif( isset( $arrValues['is_allow_money_gram'] ) ) $this->setIsAllowMoneyGram( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_allow_money_gram'] ) : $arrValues['is_allow_money_gram'] );
        if( isset( $arrValues['is_exclude_weekends'] ) && $boolDirectSet ) $this->set( 'm_boolIsExcludeWeekends', trim( stripcslashes( $arrValues['is_exclude_weekends'] ) ) ); elseif( isset( $arrValues['is_exclude_weekends'] ) ) $this->setIsExcludeWeekends( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_exclude_weekends'] ) : $arrValues['is_exclude_weekends'] );
        if( isset( $arrValues['is_exclude_holidays'] ) && $boolDirectSet ) $this->set( 'm_boolIsExcludeHolidays', trim( stripcslashes( $arrValues['is_exclude_holidays'] ) ) ); elseif( isset( $arrValues['is_exclude_holidays'] ) ) $this->setIsExcludeHolidays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_exclude_holidays'] ) : $arrValues['is_exclude_holidays'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
        if( isset( $arrValues['display_lease_id'] ) && $boolDirectSet ) $this->set( 'm_boolDisplayLeaseId', trim( stripcslashes( $arrValues['display_lease_id'] ) ) ); elseif( isset( $arrValues['display_lease_id'] ) ) $this->setDisplayLeaseId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['display_lease_id'] ) : $arrValues['display_lease_id'] );
        $this->m_boolInitialized = true;
    }

    public function setId( $intId ) {
        $this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
    }

    public function setPropertyUtilitySettingId( $intPropertyUtilitySettingId ) {
        $this->set( 'm_intPropertyUtilitySettingId', CStrings::strToIntDef( $intPropertyUtilitySettingId, NULL, false ) );
    }

    public function getPropertyUtilitySettingId() {
        return $this->m_intPropertyUtilitySettingId;
    }

    public function sqlPropertyUtilitySettingId() {
        return ( true == isset( $this->m_intPropertyUtilitySettingId ) ) ? ( string ) $this->m_intPropertyUtilitySettingId : 'NULL';
    }

    public function setUtilityInvoiceDeliveryTypeId( $intUtilityInvoiceDeliveryTypeId ) {
        $this->set( 'm_intUtilityInvoiceDeliveryTypeId', CStrings::strToIntDef( $intUtilityInvoiceDeliveryTypeId, NULL, false ) );
    }

    public function getUtilityInvoiceDeliveryTypeId() {
        return $this->m_intUtilityInvoiceDeliveryTypeId;
    }

    public function sqlUtilityInvoiceDeliveryTypeId() {
        return ( true == isset( $this->m_intUtilityInvoiceDeliveryTypeId ) ) ? ( string ) $this->m_intUtilityInvoiceDeliveryTypeId : 'NULL';
    }

    public function setInvoiceDueDay( $intInvoiceDueDay ) {
        $this->set( 'm_intInvoiceDueDay', CStrings::strToIntDef( $intInvoiceDueDay, NULL, false ) );
    }

    public function getInvoiceDueDay() {
        return $this->m_intInvoiceDueDay;
    }

    public function sqlInvoiceDueDay() {
        return ( true == isset( $this->m_intInvoiceDueDay ) ) ? ( string ) $this->m_intInvoiceDueDay : '1';
    }

    public function setInvoiceMinimumDueDays( $intInvoiceMinimumDueDays ) {
        $this->set( 'm_intInvoiceMinimumDueDays', CStrings::strToIntDef( $intInvoiceMinimumDueDays, NULL, false ) );
    }

    public function getInvoiceMinimumDueDays() {
        return $this->m_intInvoiceMinimumDueDays;
    }

    public function sqlInvoiceMinimumDueDays() {
        return ( true == isset( $this->m_intInvoiceMinimumDueDays ) ) ? ( string ) $this->m_intInvoiceMinimumDueDays : 'NULL';
    }

    public function setNonResidentPortalUrl( $strNonResidentPortalUrl ) {
        $this->set( 'm_strNonResidentPortalUrl', CStrings::strTrimDef( $strNonResidentPortalUrl, 350, NULL, true ) );
    }

    public function getNonResidentPortalUrl() {
        return $this->m_strNonResidentPortalUrl;
    }

    public function sqlNonResidentPortalUrl() {
        return ( true == isset( $this->m_strNonResidentPortalUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNonResidentPortalUrl ) : '\'' . addslashes( $this->m_strNonResidentPortalUrl ) . '\'' ) : 'NULL';
    }

    public function setBackPageFooter( $strBackPageFooter ) {
        $this->set( 'm_strBackPageFooter', CStrings::strTrimDef( $strBackPageFooter, -1, NULL, true ) );
    }

    public function getBackPageFooter() {
        return $this->m_strBackPageFooter;
    }

    public function sqlBackPageFooter() {
        return ( true == isset( $this->m_strBackPageFooter ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBackPageFooter ) : '\'' . addslashes( $this->m_strBackPageFooter ) . '\'' ) : 'NULL';
    }

    public function setHideOnlinePayment( $boolHideOnlinePayment ) {
        $this->set( 'm_boolHideOnlinePayment', CStrings::strToBool( $boolHideOnlinePayment ) );
    }

    public function getHideOnlinePayment() {
        return $this->m_boolHideOnlinePayment;
    }

    public function sqlHideOnlinePayment() {
        return ( true == isset( $this->m_boolHideOnlinePayment ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideOnlinePayment ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setDontSendUndeliveredInvoicesNotification( $boolDontSendUndeliveredInvoicesNotification ) {
        $this->set( 'm_boolDontSendUndeliveredInvoicesNotification', CStrings::strToBool( $boolDontSendUndeliveredInvoicesNotification ) );
    }

    public function getDontSendUndeliveredInvoicesNotification() {
        return $this->m_boolDontSendUndeliveredInvoicesNotification;
    }

    public function sqlDontSendUndeliveredInvoicesNotification() {
        return ( true == isset( $this->m_boolDontSendUndeliveredInvoicesNotification ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontSendUndeliveredInvoicesNotification ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setMergeBillingFee( $boolMergeBillingFee ) {
        $this->set( 'm_boolMergeBillingFee', CStrings::strToBool( $boolMergeBillingFee ) );
    }

    public function getMergeBillingFee() {
        return $this->m_boolMergeBillingFee;
    }

    public function sqlMergeBillingFee() {
        return ( true == isset( $this->m_boolMergeBillingFee ) ) ? '\'' . ( true == ( bool ) $this->m_boolMergeBillingFee ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsBillingFeeItemize( $boolIsBillingFeeItemize ) {
        $this->set( 'm_boolIsBillingFeeItemize', CStrings::strToBool( $boolIsBillingFeeItemize ) );
    }

    public function getIsBillingFeeItemize() {
        return $this->m_boolIsBillingFeeItemize;
    }

    public function sqlIsBillingFeeItemize() {
        return ( true == isset( $this->m_boolIsBillingFeeItemize ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBillingFeeItemize ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setExcludePaymentAddress( $boolExcludePaymentAddress ) {
        $this->set( 'm_boolExcludePaymentAddress', CStrings::strToBool( $boolExcludePaymentAddress ) );
    }

    public function getExcludePaymentAddress() {
        return $this->m_boolExcludePaymentAddress;
    }

    public function sqlExcludePaymentAddress() {
        return ( true == isset( $this->m_boolExcludePaymentAddress ) ) ? '\'' . ( true == ( bool ) $this->m_boolExcludePaymentAddress ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setHideResidentInvoiceSpanish( $boolHideResidentInvoiceSpanish ) {
        $this->set( 'm_boolHideResidentInvoiceSpanish', CStrings::strToBool( $boolHideResidentInvoiceSpanish ) );
    }

    public function getHideResidentInvoiceSpanish() {
        return $this->m_boolHideResidentInvoiceSpanish;
    }

    public function sqlHideResidentInvoiceSpanish() {
        return ( true == isset( $this->m_boolHideResidentInvoiceSpanish ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideResidentInvoiceSpanish ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setHideResidentInvoiceBillingDisputes( $boolHideResidentInvoiceBillingDisputes ) {
        $this->set( 'm_boolHideResidentInvoiceBillingDisputes', CStrings::strToBool( $boolHideResidentInvoiceBillingDisputes ) );
    }

    public function getHideResidentInvoiceBillingDisputes() {
        return $this->m_boolHideResidentInvoiceBillingDisputes;
    }

    public function sqlHideResidentInvoiceBillingDisputes() {
        return ( true == isset( $this->m_boolHideResidentInvoiceBillingDisputes ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideResidentInvoiceBillingDisputes ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsHideUsageHistory( $boolIsHideUsageHistory ) {
        $this->set( 'm_boolIsHideUsageHistory', CStrings::strToBool( $boolIsHideUsageHistory ) );
    }

    public function getIsHideUsageHistory() {
        return $this->m_boolIsHideUsageHistory;
    }

    public function sqlIsHideUsageHistory() {
        return ( true == isset( $this->m_boolIsHideUsageHistory ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHideUsageHistory ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsHideEcheck( $boolIsHideEcheck ) {
        $this->set( 'm_boolIsHideEcheck', CStrings::strToBool( $boolIsHideEcheck ) );
    }

    public function getIsHideEcheck() {
        return $this->m_boolIsHideEcheck;
    }

    public function sqlIsHideEcheck() {
        return ( true == isset( $this->m_boolIsHideEcheck ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHideEcheck ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsHideMasterCard( $boolIsHideMasterCard ) {
        $this->set( 'm_boolIsHideMasterCard', CStrings::strToBool( $boolIsHideMasterCard ) );
    }

    public function getIsHideMasterCard() {
        return $this->m_boolIsHideMasterCard;
    }

    public function sqlIsHideMasterCard() {
        return ( true == isset( $this->m_boolIsHideMasterCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHideMasterCard ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsHideVisa( $boolIsHideVisa ) {
        $this->set( 'm_boolIsHideVisa', CStrings::strToBool( $boolIsHideVisa ) );
    }

    public function getIsHideVisa() {
        return $this->m_boolIsHideVisa;
    }

    public function sqlIsHideVisa() {
        return ( true == isset( $this->m_boolIsHideVisa ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHideVisa ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsHideDiscover( $boolIsHideDiscover ) {
        $this->set( 'm_boolIsHideDiscover', CStrings::strToBool( $boolIsHideDiscover ) );
    }

    public function getIsHideDiscover() {
        return $this->m_boolIsHideDiscover;
    }

    public function sqlIsHideDiscover() {
        return ( true == isset( $this->m_boolIsHideDiscover ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHideDiscover ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsHideAmericanExpress( $boolIsHideAmericanExpress ) {
        $this->set( 'm_boolIsHideAmericanExpress', CStrings::strToBool( $boolIsHideAmericanExpress ) );
    }

    public function getIsHideAmericanExpress() {
        return $this->m_boolIsHideAmericanExpress;
    }

    public function sqlIsHideAmericanExpress() {
        return ( true == isset( $this->m_boolIsHideAmericanExpress ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHideAmericanExpress ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsAllowMoneyGram( $boolIsAllowMoneyGram ) {
        $this->set( 'm_boolIsAllowMoneyGram', CStrings::strToBool( $boolIsAllowMoneyGram ) );
    }

    public function getIsAllowMoneyGram() {
        return $this->m_boolIsAllowMoneyGram;
    }

    public function sqlIsAllowMoneyGram() {
        return ( true == isset( $this->m_boolIsAllowMoneyGram ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAllowMoneyGram ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsExcludeWeekends( $boolIsExcludeWeekends ) {
        $this->set( 'm_boolIsExcludeWeekends', CStrings::strToBool( $boolIsExcludeWeekends ) );
    }

    public function getIsExcludeWeekends() {
        return $this->m_boolIsExcludeWeekends;
    }

    public function sqlIsExcludeWeekends() {
        return ( true == isset( $this->m_boolIsExcludeWeekends ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsExcludeWeekends ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setIsExcludeHolidays( $boolIsExcludeHolidays ) {
        $this->set( 'm_boolIsExcludeHolidays', CStrings::strToBool( $boolIsExcludeHolidays ) );
    }

    public function getIsExcludeHolidays() {
        return $this->m_boolIsExcludeHolidays;
    }

    public function sqlIsExcludeHolidays() {
        return ( true == isset( $this->m_boolIsExcludeHolidays ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsExcludeHolidays ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function setDisplayLeaseId( $boolDisplayLeaseId ) {
        $this->set( 'm_boolDisplayLeaseId', CStrings::strToBool( $boolDisplayLeaseId ) );
    }

    public function getDisplayLeaseId() {
        return $this->m_boolDisplayLeaseId;
    }

    public function sqlDisplayLeaseId() {
        return ( true == isset( $this->m_boolDisplayLeaseId ) ) ? '\'' . ( true == ( bool ) $this->m_boolDisplayLeaseId ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $this->setDatabase( $objDatabase );

        $strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

        $strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_setting_id, utility_invoice_delivery_type_id, invoice_due_day, invoice_minimum_due_days, non_resident_portal_url, back_page_footer, hide_online_payment, dont_send_undelivered_invoices_notification, merge_billing_fee, is_billing_fee_itemize, exclude_payment_address, hide_resident_invoice_spanish, hide_resident_invoice_billing_disputes, is_hide_usage_history, is_hide_echeck, is_hide_master_card, is_hide_visa, is_hide_discover, is_hide_american_express, is_allow_money_gram, is_exclude_weekends, is_exclude_holidays, updated_by, updated_on, created_by, created_on, display_lease_id )
					VALUES ( ' .
            $strId . ', ' .
            $this->sqlCid() . ', ' .
            $this->sqlPropertyId() . ', ' .
            $this->sqlPropertyUtilitySettingId() . ', ' .
            $this->sqlUtilityInvoiceDeliveryTypeId() . ', ' .
            $this->sqlInvoiceDueDay() . ', ' .
            $this->sqlInvoiceMinimumDueDays() . ', ' .
            $this->sqlNonResidentPortalUrl() . ', ' .
            $this->sqlBackPageFooter() . ', ' .
            $this->sqlHideOnlinePayment() . ', ' .
            $this->sqlDontSendUndeliveredInvoicesNotification() . ', ' .
            $this->sqlMergeBillingFee() . ', ' .
            $this->sqlIsBillingFeeItemize() . ', ' .
            $this->sqlExcludePaymentAddress() . ', ' .
            $this->sqlHideResidentInvoiceSpanish() . ', ' .
            $this->sqlHideResidentInvoiceBillingDisputes() . ', ' .
            $this->sqlIsHideUsageHistory() . ', ' .
            $this->sqlIsHideEcheck() . ', ' .
            $this->sqlIsHideMasterCard() . ', ' .
            $this->sqlIsHideVisa() . ', ' .
            $this->sqlIsHideDiscover() . ', ' .
            $this->sqlIsHideAmericanExpress() . ', ' .
            $this->sqlIsAllowMoneyGram() . ', ' .
            $this->sqlIsExcludeWeekends() . ', ' .
            $this->sqlIsExcludeHolidays() . ', ' .
            ( int ) $intCurrentUserId . ', ' .
            $this->sqlUpdatedOn() . ', ' .
            ( int ) $intCurrentUserId . ', ' .
            $this->sqlCreatedOn() . ', ' .
            $this->sqlDisplayLeaseId() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
            return $strSql;
        } else {
            return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $this->setDatabase( $objDatabase );

        if( false == $this->getAllowDifferentialUpdate() ) {
            $boolUpdate = true;
        } else {
            $boolUpdate = false;
        }

        $strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilitySettingId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_invoice_delivery_type_id = ' . $this->sqlUtilityInvoiceDeliveryTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityInvoiceDeliveryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_invoice_delivery_type_id = ' . $this->sqlUtilityInvoiceDeliveryTypeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_due_day = ' . $this->sqlInvoiceDueDay(). ',' ; } elseif( true == array_key_exists( 'InvoiceDueDay', $this->getChangedColumns() ) ) { $strSql .= ' invoice_due_day = ' . $this->sqlInvoiceDueDay() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_minimum_due_days = ' . $this->sqlInvoiceMinimumDueDays(). ',' ; } elseif( true == array_key_exists( 'InvoiceMinimumDueDays', $this->getChangedColumns() ) ) { $strSql .= ' invoice_minimum_due_days = ' . $this->sqlInvoiceMinimumDueDays() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_resident_portal_url = ' . $this->sqlNonResidentPortalUrl(). ',' ; } elseif( true == array_key_exists( 'NonResidentPortalUrl', $this->getChangedColumns() ) ) { $strSql .= ' non_resident_portal_url = ' . $this->sqlNonResidentPortalUrl() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' back_page_footer = ' . $this->sqlBackPageFooter(). ',' ; } elseif( true == array_key_exists( 'BackPageFooter', $this->getChangedColumns() ) ) { $strSql .= ' back_page_footer = ' . $this->sqlBackPageFooter() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_online_payment = ' . $this->sqlHideOnlinePayment(). ',' ; } elseif( true == array_key_exists( 'HideOnlinePayment', $this->getChangedColumns() ) ) { $strSql .= ' hide_online_payment = ' . $this->sqlHideOnlinePayment() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_send_undelivered_invoices_notification = ' . $this->sqlDontSendUndeliveredInvoicesNotification(). ',' ; } elseif( true == array_key_exists( 'DontSendUndeliveredInvoicesNotification', $this->getChangedColumns() ) ) { $strSql .= ' dont_send_undelivered_invoices_notification = ' . $this->sqlDontSendUndeliveredInvoicesNotification() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_billing_fee = ' . $this->sqlMergeBillingFee(). ',' ; } elseif( true == array_key_exists( 'MergeBillingFee', $this->getChangedColumns() ) ) { $strSql .= ' merge_billing_fee = ' . $this->sqlMergeBillingFee() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_billing_fee_itemize = ' . $this->sqlIsBillingFeeItemize(). ',' ; } elseif( true == array_key_exists( 'IsBillingFeeItemize', $this->getChangedColumns() ) ) { $strSql .= ' is_billing_fee_itemize = ' . $this->sqlIsBillingFeeItemize() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exclude_payment_address = ' . $this->sqlExcludePaymentAddress(). ',' ; } elseif( true == array_key_exists( 'ExcludePaymentAddress', $this->getChangedColumns() ) ) { $strSql .= ' exclude_payment_address = ' . $this->sqlExcludePaymentAddress() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_resident_invoice_spanish = ' . $this->sqlHideResidentInvoiceSpanish(). ',' ; } elseif( true == array_key_exists( 'HideResidentInvoiceSpanish', $this->getChangedColumns() ) ) { $strSql .= ' hide_resident_invoice_spanish = ' . $this->sqlHideResidentInvoiceSpanish() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_resident_invoice_billing_disputes = ' . $this->sqlHideResidentInvoiceBillingDisputes(). ',' ; } elseif( true == array_key_exists( 'HideResidentInvoiceBillingDisputes', $this->getChangedColumns() ) ) { $strSql .= ' hide_resident_invoice_billing_disputes = ' . $this->sqlHideResidentInvoiceBillingDisputes() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hide_usage_history = ' . $this->sqlIsHideUsageHistory(). ',' ; } elseif( true == array_key_exists( 'IsHideUsageHistory', $this->getChangedColumns() ) ) { $strSql .= ' is_hide_usage_history = ' . $this->sqlIsHideUsageHistory() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hide_echeck = ' . $this->sqlIsHideEcheck(). ',' ; } elseif( true == array_key_exists( 'IsHideEcheck', $this->getChangedColumns() ) ) { $strSql .= ' is_hide_echeck = ' . $this->sqlIsHideEcheck() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hide_master_card = ' . $this->sqlIsHideMasterCard(). ',' ; } elseif( true == array_key_exists( 'IsHideMasterCard', $this->getChangedColumns() ) ) { $strSql .= ' is_hide_master_card = ' . $this->sqlIsHideMasterCard() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hide_visa = ' . $this->sqlIsHideVisa(). ',' ; } elseif( true == array_key_exists( 'IsHideVisa', $this->getChangedColumns() ) ) { $strSql .= ' is_hide_visa = ' . $this->sqlIsHideVisa() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hide_discover = ' . $this->sqlIsHideDiscover(). ',' ; } elseif( true == array_key_exists( 'IsHideDiscover', $this->getChangedColumns() ) ) { $strSql .= ' is_hide_discover = ' . $this->sqlIsHideDiscover() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hide_american_express = ' . $this->sqlIsHideAmericanExpress(). ',' ; } elseif( true == array_key_exists( 'IsHideAmericanExpress', $this->getChangedColumns() ) ) { $strSql .= ' is_hide_american_express = ' . $this->sqlIsHideAmericanExpress() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_allow_money_gram = ' . $this->sqlIsAllowMoneyGram(). ',' ; } elseif( true == array_key_exists( 'IsAllowMoneyGram', $this->getChangedColumns() ) ) { $strSql .= ' is_allow_money_gram = ' . $this->sqlIsAllowMoneyGram() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_exclude_weekends = ' . $this->sqlIsExcludeWeekends(). ',' ; } elseif( true == array_key_exists( 'IsExcludeWeekends', $this->getChangedColumns() ) ) { $strSql .= ' is_exclude_weekends = ' . $this->sqlIsExcludeWeekends() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_exclude_holidays = ' . $this->sqlIsExcludeHolidays(). ',' ; } elseif( true == array_key_exists( 'IsExcludeHolidays', $this->getChangedColumns() ) ) { $strSql .= ' is_exclude_holidays = ' . $this->sqlIsExcludeHolidays() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_lease_id = ' . $this->sqlDisplayLeaseId(). ',' ; } elseif( true == array_key_exists( 'DisplayLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' display_lease_id = ' . $this->sqlDisplayLeaseId() . ','; $boolUpdate = true; }
        $strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
        $strSql .= ' updated_on = \'NOW()\' ';

        $strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {
            return ( true == $boolUpdate ) ? $strSql : false;
        } else {
            if( true == $boolUpdate ) {
                if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
                    if( true == $this->getAllowDifferentialUpdate() ) {
                        $this->resetChangedColumns();
                        return true;
                    }
                } else {
                    return false;
                }
            }
            return true;
        }
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $this->setDatabase( $objDatabase );

        $strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {
            return $strSql;
        } else {
            return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function toArray() {
        return array(
            'id' => $this->getId(),
            'cid' => $this->getCid(),
            'property_id' => $this->getPropertyId(),
            'property_utility_setting_id' => $this->getPropertyUtilitySettingId(),
            'utility_invoice_delivery_type_id' => $this->getUtilityInvoiceDeliveryTypeId(),
            'invoice_due_day' => $this->getInvoiceDueDay(),
            'invoice_minimum_due_days' => $this->getInvoiceMinimumDueDays(),
            'non_resident_portal_url' => $this->getNonResidentPortalUrl(),
            'back_page_footer' => $this->getBackPageFooter(),
            'hide_online_payment' => $this->getHideOnlinePayment(),
            'dont_send_undelivered_invoices_notification' => $this->getDontSendUndeliveredInvoicesNotification(),
            'merge_billing_fee' => $this->getMergeBillingFee(),
            'is_billing_fee_itemize' => $this->getIsBillingFeeItemize(),
            'exclude_payment_address' => $this->getExcludePaymentAddress(),
            'hide_resident_invoice_spanish' => $this->getHideResidentInvoiceSpanish(),
            'hide_resident_invoice_billing_disputes' => $this->getHideResidentInvoiceBillingDisputes(),
            'is_hide_usage_history' => $this->getIsHideUsageHistory(),
            'is_hide_echeck' => $this->getIsHideEcheck(),
            'is_hide_master_card' => $this->getIsHideMasterCard(),
            'is_hide_visa' => $this->getIsHideVisa(),
            'is_hide_discover' => $this->getIsHideDiscover(),
            'is_hide_american_express' => $this->getIsHideAmericanExpress(),
            'is_allow_money_gram' => $this->getIsAllowMoneyGram(),
            'is_exclude_weekends' => $this->getIsExcludeWeekends(),
            'is_exclude_holidays' => $this->getIsExcludeHolidays(),
            'updated_by' => $this->getUpdatedBy(),
            'updated_on' => $this->getUpdatedOn(),
            'created_by' => $this->getCreatedBy(),
            'created_on' => $this->getCreatedOn(),
            'display_lease_id' => $this->getDisplayLeaseId()
        );
    }

}
?>