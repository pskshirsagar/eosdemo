<?php

class CBaseVendorClientScreening extends CEosSingularBase {

	const TABLE_NAME = 'public.vendor_client_screenings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intVendorId;
	protected $m_intComplianceItemId;
	protected $m_intVendorEntityId;
	protected $m_intWorkerId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['compliance_item_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceItemId', trim( $arrValues['compliance_item_id'] ) ); elseif( isset( $arrValues['compliance_item_id'] ) ) $this->setComplianceItemId( $arrValues['compliance_item_id'] );
		if( isset( $arrValues['vendor_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorEntityId', trim( $arrValues['vendor_entity_id'] ) ); elseif( isset( $arrValues['vendor_entity_id'] ) ) $this->setVendorEntityId( $arrValues['vendor_entity_id'] );
		if( isset( $arrValues['worker_id'] ) && $boolDirectSet ) $this->set( 'm_intWorkerId', trim( $arrValues['worker_id'] ) ); elseif( isset( $arrValues['worker_id'] ) ) $this->setWorkerId( $arrValues['worker_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setComplianceItemId( $intComplianceItemId ) {
		$this->set( 'm_intComplianceItemId', CStrings::strToIntDef( $intComplianceItemId, NULL, false ) );
	}

	public function getComplianceItemId() {
		return $this->m_intComplianceItemId;
	}

	public function sqlComplianceItemId() {
		return ( true == isset( $this->m_intComplianceItemId ) ) ? ( string ) $this->m_intComplianceItemId : 'NULL';
	}

	public function setVendorEntityId( $intVendorEntityId ) {
		$this->set( 'm_intVendorEntityId', CStrings::strToIntDef( $intVendorEntityId, NULL, false ) );
	}

	public function getVendorEntityId() {
		return $this->m_intVendorEntityId;
	}

	public function sqlVendorEntityId() {
		return ( true == isset( $this->m_intVendorEntityId ) ) ? ( string ) $this->m_intVendorEntityId : 'NULL';
	}

	public function setWorkerId( $intWorkerId ) {
		$this->set( 'm_intWorkerId', CStrings::strToIntDef( $intWorkerId, NULL, false ) );
	}

	public function getWorkerId() {
		return $this->m_intWorkerId;
	}

	public function sqlWorkerId() {
		return ( true == isset( $this->m_intWorkerId ) ) ? ( string ) $this->m_intWorkerId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, vendor_id, compliance_item_id, vendor_entity_id, worker_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlVendorId() . ', ' .
 						$this->sqlComplianceItemId() . ', ' .
 						$this->sqlVendorEntityId() . ', ' .
 						$this->sqlWorkerId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_item_id = ' . $this->sqlComplianceItemId() . ','; } elseif( true == array_key_exists( 'ComplianceItemId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_item_id = ' . $this->sqlComplianceItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_entity_id = ' . $this->sqlVendorEntityId() . ','; } elseif( true == array_key_exists( 'VendorEntityId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_entity_id = ' . $this->sqlVendorEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' worker_id = ' . $this->sqlWorkerId() . ','; } elseif( true == array_key_exists( 'WorkerId', $this->getChangedColumns() ) ) { $strSql .= ' worker_id = ' . $this->sqlWorkerId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'vendor_id' => $this->getVendorId(),
			'compliance_item_id' => $this->getComplianceItemId(),
			'vendor_entity_id' => $this->getVendorEntityId(),
			'worker_id' => $this->getWorkerId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>