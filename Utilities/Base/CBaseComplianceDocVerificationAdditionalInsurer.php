<?php

class CBaseComplianceDocVerificationAdditionalInsurer extends CEosSingularBase {

	const TABLE_NAME = 'public.compliance_doc_verification_additional_insurers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intComplianceDocVerificationId;
	protected $m_intAdditionalInsurerId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['compliance_doc_verification_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceDocVerificationId', trim( $arrValues['compliance_doc_verification_id'] ) ); elseif( isset( $arrValues['compliance_doc_verification_id'] ) ) $this->setComplianceDocVerificationId( $arrValues['compliance_doc_verification_id'] );
		if( isset( $arrValues['additional_insurer_id'] ) && $boolDirectSet ) $this->set( 'm_intAdditionalInsurerId', trim( $arrValues['additional_insurer_id'] ) ); elseif( isset( $arrValues['additional_insurer_id'] ) ) $this->setAdditionalInsurerId( $arrValues['additional_insurer_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setComplianceDocVerificationId( $intComplianceDocVerificationId ) {
		$this->set( 'm_intComplianceDocVerificationId', CStrings::strToIntDef( $intComplianceDocVerificationId, NULL, false ) );
	}

	public function getComplianceDocVerificationId() {
		return $this->m_intComplianceDocVerificationId;
	}

	public function sqlComplianceDocVerificationId() {
		return ( true == isset( $this->m_intComplianceDocVerificationId ) ) ? ( string ) $this->m_intComplianceDocVerificationId : 'NULL';
	}

	public function setAdditionalInsurerId( $intAdditionalInsurerId ) {
		$this->set( 'm_intAdditionalInsurerId', CStrings::strToIntDef( $intAdditionalInsurerId, NULL, false ) );
	}

	public function getAdditionalInsurerId() {
		return $this->m_intAdditionalInsurerId;
	}

	public function sqlAdditionalInsurerId() {
		return ( true == isset( $this->m_intAdditionalInsurerId ) ) ? ( string ) $this->m_intAdditionalInsurerId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, compliance_doc_verification_id, additional_insurer_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlComplianceDocVerificationId() . ', ' .
 						$this->sqlAdditionalInsurerId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_doc_verification_id = ' . $this->sqlComplianceDocVerificationId() . ','; } elseif( true == array_key_exists( 'ComplianceDocVerificationId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_doc_verification_id = ' . $this->sqlComplianceDocVerificationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_insurer_id = ' . $this->sqlAdditionalInsurerId() . ','; } elseif( true == array_key_exists( 'AdditionalInsurerId', $this->getChangedColumns() ) ) { $strSql .= ' additional_insurer_id = ' . $this->sqlAdditionalInsurerId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'compliance_doc_verification_id' => $this->getComplianceDocVerificationId(),
			'additional_insurer_id' => $this->getAdditionalInsurerId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>