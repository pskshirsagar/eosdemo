<?php

class CBaseBillAuditTypeStep extends CEosSingularBase {

	const TABLE_NAME = 'public.bill_audit_type_steps';

	protected $m_intId;
	protected $m_intBillAuditTypeId;
	protected $m_intBillAuditStepId;
	protected $m_intOrderNum;
	protected $m_boolIsResolutionStep;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsResolutionStep = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['bill_audit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillAuditTypeId', trim( $arrValues['bill_audit_type_id'] ) ); elseif( isset( $arrValues['bill_audit_type_id'] ) ) $this->setBillAuditTypeId( $arrValues['bill_audit_type_id'] );
		if( isset( $arrValues['bill_audit_step_id'] ) && $boolDirectSet ) $this->set( 'm_intBillAuditStepId', trim( $arrValues['bill_audit_step_id'] ) ); elseif( isset( $arrValues['bill_audit_step_id'] ) ) $this->setBillAuditStepId( $arrValues['bill_audit_step_id'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_resolution_step'] ) && $boolDirectSet ) $this->set( 'm_boolIsResolutionStep', trim( stripcslashes( $arrValues['is_resolution_step'] ) ) ); elseif( isset( $arrValues['is_resolution_step'] ) ) $this->setIsResolutionStep( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_resolution_step'] ) : $arrValues['is_resolution_step'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setBillAuditTypeId( $intBillAuditTypeId ) {
		$this->set( 'm_intBillAuditTypeId', CStrings::strToIntDef( $intBillAuditTypeId, NULL, false ) );
	}

	public function getBillAuditTypeId() {
		return $this->m_intBillAuditTypeId;
	}

	public function sqlBillAuditTypeId() {
		return ( true == isset( $this->m_intBillAuditTypeId ) ) ? ( string ) $this->m_intBillAuditTypeId : 'NULL';
	}

	public function setBillAuditStepId( $intBillAuditStepId ) {
		$this->set( 'm_intBillAuditStepId', CStrings::strToIntDef( $intBillAuditStepId, NULL, false ) );
	}

	public function getBillAuditStepId() {
		return $this->m_intBillAuditStepId;
	}

	public function sqlBillAuditStepId() {
		return ( true == isset( $this->m_intBillAuditStepId ) ) ? ( string ) $this->m_intBillAuditStepId : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setIsResolutionStep( $boolIsResolutionStep ) {
		$this->set( 'm_boolIsResolutionStep', CStrings::strToBool( $boolIsResolutionStep ) );
	}

	public function getIsResolutionStep() {
		return $this->m_boolIsResolutionStep;
	}

	public function sqlIsResolutionStep() {
		return ( true == isset( $this->m_boolIsResolutionStep ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResolutionStep ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, bill_audit_type_id, bill_audit_step_id, order_num, is_resolution_step, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlBillAuditTypeId() . ', ' .
 						$this->sqlBillAuditStepId() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlIsResolutionStep() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_type_id = ' . $this->sqlBillAuditTypeId() . ','; } elseif( true == array_key_exists( 'BillAuditTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_type_id = ' . $this->sqlBillAuditTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_step_id = ' . $this->sqlBillAuditStepId() . ','; } elseif( true == array_key_exists( 'BillAuditStepId', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_step_id = ' . $this->sqlBillAuditStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_resolution_step = ' . $this->sqlIsResolutionStep() . ','; } elseif( true == array_key_exists( 'IsResolutionStep', $this->getChangedColumns() ) ) { $strSql .= ' is_resolution_step = ' . $this->sqlIsResolutionStep() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'bill_audit_type_id' => $this->getBillAuditTypeId(),
			'bill_audit_step_id' => $this->getBillAuditStepId(),
			'order_num' => $this->getOrderNum(),
			'is_resolution_step' => $this->getIsResolutionStep(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>