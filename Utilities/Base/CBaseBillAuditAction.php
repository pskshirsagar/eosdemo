<?php

class CBaseBillAuditAction extends CEosSingularBase {

	const TABLE_NAME = 'public.bill_audit_actions';

	protected $m_intId;
	protected $m_intBillAuditId;
	protected $m_intBillAuditStepId;
	protected $m_strMemo;
	protected $m_strBillAuditActionDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['bill_audit_id'] ) && $boolDirectSet ) $this->set( 'm_intBillAuditId', trim( $arrValues['bill_audit_id'] ) ); elseif( isset( $arrValues['bill_audit_id'] ) ) $this->setBillAuditId( $arrValues['bill_audit_id'] );
		if( isset( $arrValues['bill_audit_step_id'] ) && $boolDirectSet ) $this->set( 'm_intBillAuditStepId', trim( $arrValues['bill_audit_step_id'] ) ); elseif( isset( $arrValues['bill_audit_step_id'] ) ) $this->setBillAuditStepId( $arrValues['bill_audit_step_id'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['bill_audit_action_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBillAuditActionDatetime', trim( $arrValues['bill_audit_action_datetime'] ) ); elseif( isset( $arrValues['bill_audit_action_datetime'] ) ) $this->setBillAuditActionDatetime( $arrValues['bill_audit_action_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setBillAuditId( $intBillAuditId ) {
		$this->set( 'm_intBillAuditId', CStrings::strToIntDef( $intBillAuditId, NULL, false ) );
	}

	public function getBillAuditId() {
		return $this->m_intBillAuditId;
	}

	public function sqlBillAuditId() {
		return ( true == isset( $this->m_intBillAuditId ) ) ? ( string ) $this->m_intBillAuditId : 'NULL';
	}

	public function setBillAuditStepId( $intBillAuditStepId ) {
		$this->set( 'm_intBillAuditStepId', CStrings::strToIntDef( $intBillAuditStepId, NULL, false ) );
	}

	public function getBillAuditStepId() {
		return $this->m_intBillAuditStepId;
	}

	public function sqlBillAuditStepId() {
		return ( true == isset( $this->m_intBillAuditStepId ) ) ? ( string ) $this->m_intBillAuditStepId : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, 240, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setBillAuditActionDatetime( $strBillAuditActionDatetime ) {
		$this->set( 'm_strBillAuditActionDatetime', CStrings::strTrimDef( $strBillAuditActionDatetime, -1, NULL, true ) );
	}

	public function getBillAuditActionDatetime() {
		return $this->m_strBillAuditActionDatetime;
	}

	public function sqlBillAuditActionDatetime() {
		return ( true == isset( $this->m_strBillAuditActionDatetime ) ) ? '\'' . $this->m_strBillAuditActionDatetime . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, bill_audit_id, bill_audit_step_id, memo, bill_audit_action_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlBillAuditId() . ', ' .
 						$this->sqlBillAuditStepId() . ', ' .
 						$this->sqlMemo() . ', ' .
 						$this->sqlBillAuditActionDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_id = ' . $this->sqlBillAuditId() . ','; } elseif( true == array_key_exists( 'BillAuditId', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_id = ' . $this->sqlBillAuditId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_step_id = ' . $this->sqlBillAuditStepId() . ','; } elseif( true == array_key_exists( 'BillAuditStepId', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_step_id = ' . $this->sqlBillAuditStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_action_datetime = ' . $this->sqlBillAuditActionDatetime() . ','; } elseif( true == array_key_exists( 'BillAuditActionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_action_datetime = ' . $this->sqlBillAuditActionDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'bill_audit_id' => $this->getBillAuditId(),
			'bill_audit_step_id' => $this->getBillAuditStepId(),
			'memo' => $this->getMemo(),
			'bill_audit_action_datetime' => $this->getBillAuditActionDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>