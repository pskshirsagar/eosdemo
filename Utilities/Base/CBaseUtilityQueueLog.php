<?php

class CBaseUtilityQueueLog extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_queue_logs';

	protected $m_intId;
	protected $m_intUtilityQueueId;
	protected $m_strLogDatetime;
	protected $m_intItemCount;
	protected $m_intLateCount;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		$this->m_intItemCount = '0';
		$this->m_intLateCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityQueueId', trim( $arrValues['utility_queue_id'] ) ); elseif( isset( $arrValues['utility_queue_id'] ) ) $this->setUtilityQueueId( $arrValues['utility_queue_id'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['item_count'] ) && $boolDirectSet ) $this->set( 'm_intItemCount', trim( $arrValues['item_count'] ) ); elseif( isset( $arrValues['item_count'] ) ) $this->setItemCount( $arrValues['item_count'] );
		if( isset( $arrValues['late_count'] ) && $boolDirectSet ) $this->set( 'm_intLateCount', trim( $arrValues['late_count'] ) ); elseif( isset( $arrValues['late_count'] ) ) $this->setLateCount( $arrValues['late_count'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityQueueId( $intUtilityQueueId ) {
		$this->set( 'm_intUtilityQueueId', CStrings::strToIntDef( $intUtilityQueueId, NULL, false ) );
	}

	public function getUtilityQueueId() {
		return $this->m_intUtilityQueueId;
	}

	public function sqlUtilityQueueId() {
		return ( true == isset( $this->m_intUtilityQueueId ) ) ? ( string ) $this->m_intUtilityQueueId : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setItemCount( $intItemCount ) {
		$this->set( 'm_intItemCount', CStrings::strToIntDef( $intItemCount, NULL, false ) );
	}

	public function getItemCount() {
		return $this->m_intItemCount;
	}

	public function sqlItemCount() {
		return ( true == isset( $this->m_intItemCount ) ) ? ( string ) $this->m_intItemCount : '0';
	}

	public function setLateCount( $intLateCount ) {
		$this->set( 'm_intLateCount', CStrings::strToIntDef( $intLateCount, NULL, false ) );
	}

	public function getLateCount() {
		return $this->m_intLateCount;
	}

	public function sqlLateCount() {
		return ( true == isset( $this->m_intLateCount ) ) ? ( string ) $this->m_intLateCount : '0';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_queue_id, log_datetime, item_count, late_count, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityQueueId() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlItemCount() . ', ' .
						$this->sqlLateCount() . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_queue_id = ' . $this->sqlUtilityQueueId(). ',' ; } elseif( true == array_key_exists( 'UtilityQueueId', $this->getChangedColumns() ) ) { $strSql .= ' utility_queue_id = ' . $this->sqlUtilityQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_count = ' . $this->sqlItemCount(). ',' ; } elseif( true == array_key_exists( 'ItemCount', $this->getChangedColumns() ) ) { $strSql .= ' item_count = ' . $this->sqlItemCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' late_count = ' . $this->sqlLateCount() ; } elseif( true == array_key_exists( 'LateCount', $this->getChangedColumns() ) ) { $strSql .= ' late_count = ' . $this->sqlLateCount() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_queue_id' => $this->getUtilityQueueId(),
			'log_datetime' => $this->getLogDatetime(),
			'item_count' => $this->getItemCount(),
			'late_count' => $this->getLateCount(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>