<?php

class CBaseUtilityPropertySettingKey extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_property_setting_keys';

	protected $m_intId;
	protected $m_intUtilityPropertySettingGroupId;
	protected $m_strKey;
	protected $m_boolIsImplementationRequired;
	protected $m_boolIsDefaultCopy;
	protected $m_boolIsDisabled;
	protected $m_boolIsBulkEditable;
	protected $m_boolIsClientSpecific;
	protected $m_strImplementationDescription;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsImplementationRequired = false;
		$this->m_boolIsDefaultCopy = true;
		$this->m_boolIsDisabled = false;
		$this->m_boolIsBulkEditable = true;
		$this->m_boolIsClientSpecific = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_property_setting_group_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityPropertySettingGroupId', trim( $arrValues['utility_property_setting_group_id'] ) ); elseif( isset( $arrValues['utility_property_setting_group_id'] ) ) $this->setUtilityPropertySettingGroupId( $arrValues['utility_property_setting_group_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['is_implementation_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsImplementationRequired', trim( stripcslashes( $arrValues['is_implementation_required'] ) ) ); elseif( isset( $arrValues['is_implementation_required'] ) ) $this->setIsImplementationRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_implementation_required'] ) : $arrValues['is_implementation_required'] );
		if( isset( $arrValues['is_default_copy'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefaultCopy', trim( stripcslashes( $arrValues['is_default_copy'] ) ) ); elseif( isset( $arrValues['is_default_copy'] ) ) $this->setIsDefaultCopy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default_copy'] ) : $arrValues['is_default_copy'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['is_bulk_editable'] ) && $boolDirectSet ) $this->set( 'm_boolIsBulkEditable', trim( stripcslashes( $arrValues['is_bulk_editable'] ) ) ); elseif( isset( $arrValues['is_bulk_editable'] ) ) $this->setIsBulkEditable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_bulk_editable'] ) : $arrValues['is_bulk_editable'] );
		if( isset( $arrValues['is_client_specific'] ) && $boolDirectSet ) $this->set( 'm_boolIsClientSpecific', trim( stripcslashes( $arrValues['is_client_specific'] ) ) ); elseif( isset( $arrValues['is_client_specific'] ) ) $this->setIsClientSpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_client_specific'] ) : $arrValues['is_client_specific'] );
		if( isset( $arrValues['implementation_description'] ) && $boolDirectSet ) $this->set( 'm_strImplementationDescription', trim( stripcslashes( $arrValues['implementation_description'] ) ) ); elseif( isset( $arrValues['implementation_description'] ) ) $this->setImplementationDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['implementation_description'] ) : $arrValues['implementation_description'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityPropertySettingGroupId( $intUtilityPropertySettingGroupId ) {
		$this->set( 'm_intUtilityPropertySettingGroupId', CStrings::strToIntDef( $intUtilityPropertySettingGroupId, NULL, false ) );
	}

	public function getUtilityPropertySettingGroupId() {
		return $this->m_intUtilityPropertySettingGroupId;
	}

	public function sqlUtilityPropertySettingGroupId() {
		return ( true == isset( $this->m_intUtilityPropertySettingGroupId ) ) ? ( string ) $this->m_intUtilityPropertySettingGroupId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 50, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setIsImplementationRequired( $boolIsImplementationRequired ) {
		$this->set( 'm_boolIsImplementationRequired', CStrings::strToBool( $boolIsImplementationRequired ) );
	}

	public function getIsImplementationRequired() {
		return $this->m_boolIsImplementationRequired;
	}

	public function sqlIsImplementationRequired() {
		return ( true == isset( $this->m_boolIsImplementationRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsImplementationRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefaultCopy( $boolIsDefaultCopy ) {
		$this->set( 'm_boolIsDefaultCopy', CStrings::strToBool( $boolIsDefaultCopy ) );
	}

	public function getIsDefaultCopy() {
		return $this->m_boolIsDefaultCopy;
	}

	public function sqlIsDefaultCopy() {
		return ( true == isset( $this->m_boolIsDefaultCopy ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefaultCopy ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsBulkEditable( $boolIsBulkEditable ) {
		$this->set( 'm_boolIsBulkEditable', CStrings::strToBool( $boolIsBulkEditable ) );
	}

	public function getIsBulkEditable() {
		return $this->m_boolIsBulkEditable;
	}

	public function sqlIsBulkEditable() {
		return ( true == isset( $this->m_boolIsBulkEditable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBulkEditable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsClientSpecific( $boolIsClientSpecific ) {
		$this->set( 'm_boolIsClientSpecific', CStrings::strToBool( $boolIsClientSpecific ) );
	}

	public function getIsClientSpecific() {
		return $this->m_boolIsClientSpecific;
	}

	public function sqlIsClientSpecific() {
		return ( true == isset( $this->m_boolIsClientSpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClientSpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setImplementationDescription( $strImplementationDescription ) {
		$this->set( 'm_strImplementationDescription', CStrings::strTrimDef( $strImplementationDescription, 250, NULL, true ) );
	}

	public function getImplementationDescription() {
		return $this->m_strImplementationDescription;
	}

	public function sqlImplementationDescription() {
		return ( true == isset( $this->m_strImplementationDescription ) ) ? '\'' . addslashes( $this->m_strImplementationDescription ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_property_setting_group_id, key, is_implementation_required, is_default_copy, is_disabled, is_bulk_editable, is_client_specific, implementation_description, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlUtilityPropertySettingGroupId() . ', ' .
		          $this->sqlKey() . ', ' .
		          $this->sqlIsImplementationRequired() . ', ' .
		          $this->sqlIsDefaultCopy() . ', ' .
		          $this->sqlIsDisabled() . ', ' .
		          $this->sqlIsBulkEditable() . ', ' .
		          $this->sqlIsClientSpecific() . ', ' .
		          $this->sqlImplementationDescription() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_property_setting_group_id = ' . $this->sqlUtilityPropertySettingGroupId(). ',' ; } elseif( true == array_key_exists( 'UtilityPropertySettingGroupId', $this->getChangedColumns() ) ) { $strSql .= ' utility_property_setting_group_id = ' . $this->sqlUtilityPropertySettingGroupId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey(). ',' ; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_implementation_required = ' . $this->sqlIsImplementationRequired(). ',' ; } elseif( true == array_key_exists( 'IsImplementationRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_implementation_required = ' . $this->sqlIsImplementationRequired() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default_copy = ' . $this->sqlIsDefaultCopy(). ',' ; } elseif( true == array_key_exists( 'IsDefaultCopy', $this->getChangedColumns() ) ) { $strSql .= ' is_default_copy = ' . $this->sqlIsDefaultCopy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_bulk_editable = ' . $this->sqlIsBulkEditable(). ',' ; } elseif( true == array_key_exists( 'IsBulkEditable', $this->getChangedColumns() ) ) { $strSql .= ' is_bulk_editable = ' . $this->sqlIsBulkEditable() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_specific = ' . $this->sqlIsClientSpecific(). ',' ; } elseif( true == array_key_exists( 'IsClientSpecific', $this->getChangedColumns() ) ) { $strSql .= ' is_client_specific = ' . $this->sqlIsClientSpecific() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_description = ' . $this->sqlImplementationDescription(). ',' ; } elseif( true == array_key_exists( 'ImplementationDescription', $this->getChangedColumns() ) ) { $strSql .= ' implementation_description = ' . $this->sqlImplementationDescription() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_property_setting_group_id' => $this->getUtilityPropertySettingGroupId(),
			'key' => $this->getKey(),
			'is_implementation_required' => $this->getIsImplementationRequired(),
			'is_default_copy' => $this->getIsDefaultCopy(),
			'is_disabled' => $this->getIsDisabled(),
			'is_bulk_editable' => $this->getIsBulkEditable(),
			'is_client_specific' => $this->getIsClientSpecific(),
			'implementation_description' => $this->getImplementationDescription(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>