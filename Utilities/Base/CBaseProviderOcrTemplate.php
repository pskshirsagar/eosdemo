<?php

class CBaseProviderOcrTemplate extends CEosSingularBase {

	const TABLE_NAME = 'public.provider_ocr_templates';

	protected $m_intId;
	protected $m_intUtilityProviderId;
	protected $m_intOcrFieldId;
	protected $m_strLabel;
	protected $m_strLabelVertices;
	protected $m_jsonLabelVertices;
	protected $m_strValueVertices;
	protected $m_jsonValueVertices;
	protected $m_strValuePosition;
	protected $m_intValueParts;
	protected $m_intPriority;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strValueDataPosition;
	protected $m_strValueDelimiter;
	protected $m_strValueDelimiterEnd;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityProviderId', trim( $arrValues['utility_provider_id'] ) ); elseif( isset( $arrValues['utility_provider_id'] ) ) $this->setUtilityProviderId( $arrValues['utility_provider_id'] );
		if( isset( $arrValues['ocr_field_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrFieldId', trim( $arrValues['ocr_field_id'] ) ); elseif( isset( $arrValues['ocr_field_id'] ) ) $this->setOcrFieldId( $arrValues['ocr_field_id'] );
		if( isset( $arrValues['label'] ) && $boolDirectSet ) $this->set( 'm_strLabel', trim( $arrValues['label'] ) ); elseif( isset( $arrValues['label'] ) ) $this->setLabel( $arrValues['label'] );
		if( isset( $arrValues['label_vertices'] ) ) $this->set( 'm_strLabelVertices', trim( $arrValues['label_vertices'] ) );
		if( isset( $arrValues['value_vertices'] ) ) $this->set( 'm_strValueVertices', trim( $arrValues['value_vertices'] ) );
		if( isset( $arrValues['value_position'] ) && $boolDirectSet ) $this->set( 'm_strValuePosition', trim( $arrValues['value_position'] ) ); elseif( isset( $arrValues['value_position'] ) ) $this->setValuePosition( $arrValues['value_position'] );
		if( isset( $arrValues['value_parts'] ) && $boolDirectSet ) $this->set( 'm_intValueParts', trim( $arrValues['value_parts'] ) ); elseif( isset( $arrValues['value_parts'] ) ) $this->setValueParts( $arrValues['value_parts'] );
		if( isset( $arrValues['priority'] ) && $boolDirectSet ) $this->set( 'm_intPriority', trim( $arrValues['priority'] ) ); elseif( isset( $arrValues['priority'] ) ) $this->setPriority( $arrValues['priority'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['value_data_position'] ) && $boolDirectSet ) $this->set( 'm_strValueDataPosition', trim( stripcslashes( $arrValues['value_data_position'] ) ) ); elseif( isset( $arrValues['value_data_position'] ) ) $this->setValueDataPosition( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['value_data_position'] ) : $arrValues['value_data_position'] );
		if( isset( $arrValues['value_delimiter'] ) && $boolDirectSet ) $this->set( 'm_strValueDelimiter', trim( $arrValues['value_delimiter'] ) ); elseif( isset( $arrValues['value_delimiter'] ) ) $this->setValueDelimiter( $arrValues['value_delimiter'] );
		if( isset( $arrValues['value_delimiter_end'] ) && $boolDirectSet ) $this->set( 'm_strValueDelimiterEnd', trim( $arrValues['value_delimiter_end'] ) ); elseif( isset( $arrValues['value_delimiter_end'] ) ) $this->setValueDelimiterEnd( $arrValues['value_delimiter_end'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityProviderId( $intUtilityProviderId ) {
		$this->set( 'm_intUtilityProviderId', CStrings::strToIntDef( $intUtilityProviderId, NULL, false ) );
	}

	public function getUtilityProviderId() {
		return $this->m_intUtilityProviderId;
	}

	public function sqlUtilityProviderId() {
		return ( true == isset( $this->m_intUtilityProviderId ) ) ? ( string ) $this->m_intUtilityProviderId : 'NULL';
	}

	public function setOcrFieldId( $intOcrFieldId ) {
		$this->set( 'm_intOcrFieldId', CStrings::strToIntDef( $intOcrFieldId, NULL, false ) );
	}

	public function getOcrFieldId() {
		return $this->m_intOcrFieldId;
	}

	public function sqlOcrFieldId() {
		return ( true == isset( $this->m_intOcrFieldId ) ) ? ( string ) $this->m_intOcrFieldId : 'NULL';
	}

	public function setLabel( $strLabel ) {
		$this->set( 'm_strLabel', CStrings::strTrimDef( $strLabel, 250, NULL, true ) );
	}

	public function getLabel() {
		return $this->m_strLabel;
	}

	public function sqlLabel() {
		return ( true == isset( $this->m_strLabel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLabel ) : '\'' . addslashes( $this->m_strLabel ) . '\'' ) : 'NULL';
	}

	public function setLabelVertices( $jsonLabelVertices ) {
		if( true == valObj( $jsonLabelVertices, 'stdClass' ) ) {
			$this->set( 'm_jsonLabelVertices', $jsonLabelVertices );
		} elseif( true == valJsonString( $jsonLabelVertices ) ) {
			$this->set( 'm_jsonLabelVertices', CStrings::strToJson( $jsonLabelVertices ) );
		} else {
			$this->set( 'm_jsonLabelVertices', NULL ); 
		}
		unset( $this->m_strLabelVertices );
	}

	public function getLabelVertices() {
		if( true == isset( $this->m_strLabelVertices ) ) {
			$this->m_jsonLabelVertices = CStrings::strToJson( $this->m_strLabelVertices );
			unset( $this->m_strLabelVertices );
		}
		return $this->m_jsonLabelVertices;
	}

	public function sqlLabelVertices() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getLabelVertices() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getLabelVertices() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getLabelVertices() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setValueVertices( $jsonValueVertices ) {
		if( true == valObj( $jsonValueVertices, 'stdClass' ) ) {
			$this->set( 'm_jsonValueVertices', $jsonValueVertices );
		} elseif( true == valJsonString( $jsonValueVertices ) ) {
			$this->set( 'm_jsonValueVertices', CStrings::strToJson( $jsonValueVertices ) );
		} else {
			$this->set( 'm_jsonValueVertices', NULL ); 
		}
		unset( $this->m_strValueVertices );
	}

	public function getValueVertices() {
		if( true == isset( $this->m_strValueVertices ) ) {
			$this->m_jsonValueVertices = CStrings::strToJson( $this->m_strValueVertices );
			unset( $this->m_strValueVertices );
		}
		return $this->m_jsonValueVertices;
	}

	public function sqlValueVertices() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getValueVertices() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getValueVertices() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getValueVertices() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setValuePosition( $strValuePosition ) {
		$this->set( 'm_strValuePosition', CStrings::strTrimDef( $strValuePosition, 100, NULL, true ) );
	}

	public function getValuePosition() {
		return $this->m_strValuePosition;
	}

	public function sqlValuePosition() {
		return ( true == isset( $this->m_strValuePosition ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strValuePosition ) : '\'' . addslashes( $this->m_strValuePosition ) . '\'' ) : 'NULL';
	}

	public function setValueParts( $intValueParts ) {
		$this->set( 'm_intValueParts', CStrings::strToIntDef( $intValueParts, NULL, false ) );
	}

	public function getValueParts() {
		return $this->m_intValueParts;
	}

	public function sqlValueParts() {
		return ( true == isset( $this->m_intValueParts ) ) ? ( string ) $this->m_intValueParts : 'NULL';
	}

	public function setPriority( $intPriority ) {
		$this->set( 'm_intPriority', CStrings::strToIntDef( $intPriority, NULL, false ) );
	}

	public function getPriority() {
		return $this->m_intPriority;
	}

	public function sqlPriority() {
		return ( true == isset( $this->m_intPriority ) ) ? ( string ) $this->m_intPriority : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setValueDataPosition( $strValueDataPosition ) {
		$this->set( 'm_strValueDataPosition', CStrings::strTrimDef( $strValueDataPosition, -1, NULL, true ) );
	}

	public function getValueDataPosition() {
		return $this->m_strValueDataPosition;
	}

	public function sqlValueDataPosition() {
		return ( true == isset( $this->m_strValueDataPosition ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strValueDataPosition ) : '\'' . addslashes( $this->m_strValueDataPosition ) . '\'' ) : 'NULL';
	}

	public function setValueDelimiter( $strValueDelimiter ) {
		$this->set( 'm_strValueDelimiter', CStrings::strTrimDef( $strValueDelimiter, -1, NULL, true ) );
	}

	public function getValueDelimiter() {
		return $this->m_strValueDelimiter;
	}

	public function sqlValueDelimiter() {
		return ( true == isset( $this->m_strValueDelimiter ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strValueDelimiter ) : '\'' . addslashes( $this->m_strValueDelimiter ) . '\'' ) : 'NULL';
	}

	public function setValueDelimiterEnd( $strValueDelimiterEnd ) {
		$this->set( 'm_strValueDelimiterEnd', CStrings::strTrimDef( $strValueDelimiterEnd, -1, NULL, true ) );
	}

	public function getValueDelimiterEnd() {
		return $this->m_strValueDelimiterEnd;
	}

	public function sqlValueDelimiterEnd() {
		return ( true == isset( $this->m_strValueDelimiterEnd ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strValueDelimiterEnd ) : '\'' . addslashes( $this->m_strValueDelimiterEnd ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_provider_id, ocr_field_id, label, label_vertices, value_vertices, value_position, value_parts, priority, created_by, created_on, value_data_position, value_delimiter, value_delimiter_end )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityProviderId() . ', ' .
						$this->sqlOcrFieldId() . ', ' .
						$this->sqlLabel() . ', ' .
						$this->sqlLabelVertices() . ', ' .
						$this->sqlValueVertices() . ', ' .
						$this->sqlValuePosition() . ', ' .
						$this->sqlValueParts() . ', ' .
						$this->sqlPriority() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlValueDataPosition() . ', ' .
						$this->sqlValueDelimiter() . ', ' .
						$this->sqlValueDelimiterEnd() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId(). ',' ; } elseif( true == array_key_exists( 'UtilityProviderId', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_field_id = ' . $this->sqlOcrFieldId(). ',' ; } elseif( true == array_key_exists( 'OcrFieldId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_field_id = ' . $this->sqlOcrFieldId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label = ' . $this->sqlLabel(). ',' ; } elseif( true == array_key_exists( 'Label', $this->getChangedColumns() ) ) { $strSql .= ' label = ' . $this->sqlLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label_vertices = ' . $this->sqlLabelVertices(). ',' ; } elseif( true == array_key_exists( 'LabelVertices', $this->getChangedColumns() ) ) { $strSql .= ' label_vertices = ' . $this->sqlLabelVertices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value_vertices = ' . $this->sqlValueVertices(). ',' ; } elseif( true == array_key_exists( 'ValueVertices', $this->getChangedColumns() ) ) { $strSql .= ' value_vertices = ' . $this->sqlValueVertices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value_position = ' . $this->sqlValuePosition(). ',' ; } elseif( true == array_key_exists( 'ValuePosition', $this->getChangedColumns() ) ) { $strSql .= ' value_position = ' . $this->sqlValuePosition() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value_parts = ' . $this->sqlValueParts(). ',' ; } elseif( true == array_key_exists( 'ValueParts', $this->getChangedColumns() ) ) { $strSql .= ' value_parts = ' . $this->sqlValueParts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' priority = ' . $this->sqlPriority(). ',' ; } elseif( true == array_key_exists( 'Priority', $this->getChangedColumns() ) ) { $strSql .= ' priority = ' . $this->sqlPriority() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value_data_position = ' . $this->sqlValueDataPosition(). ',' ; } elseif( true == array_key_exists( 'ValueDataPosition', $this->getChangedColumns() ) ) { $strSql .= ' value_data_position = ' . $this->sqlValueDataPosition() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value_delimiter = ' . $this->sqlValueDelimiter(). ',' ; } elseif( true == array_key_exists( 'ValueDelimiter', $this->getChangedColumns() ) ) { $strSql .= ' value_delimiter = ' . $this->sqlValueDelimiter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value_delimiter_end = ' . $this->sqlValueDelimiterEnd() ; } elseif( true == array_key_exists( 'ValueDelimiterEnd', $this->getChangedColumns() ) ) { $strSql .= ' value_delimiter_end = ' . $this->sqlValueDelimiterEnd() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_provider_id' => $this->getUtilityProviderId(),
			'ocr_field_id' => $this->getOcrFieldId(),
			'label' => $this->getLabel(),
			'label_vertices' => $this->getLabelVertices(),
			'value_vertices' => $this->getValueVertices(),
			'value_position' => $this->getValuePosition(),
			'value_parts' => $this->getValueParts(),
			'priority' => $this->getPriority(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'value_data_position' => $this->getValueDataPosition(),
			'value_delimiter' => $this->getValueDelimiter(),
			'value_delimiter_end' => $this->getValueDelimiterEnd()
		);
	}

}
?>