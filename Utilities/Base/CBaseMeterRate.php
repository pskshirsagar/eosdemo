<?php

class CBaseMeterRate extends CEosSingularBase {

	const TABLE_NAME = 'public.meter_rates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intPropertyMeteringSettingId;
	protected $m_intStartCriteria;
	protected $m_intEndCriteria;
	protected $m_fltRate;
	protected $m_boolIsCareRate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsCareRate = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['property_metering_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyMeteringSettingId', trim( $arrValues['property_metering_setting_id'] ) ); elseif( isset( $arrValues['property_metering_setting_id'] ) ) $this->setPropertyMeteringSettingId( $arrValues['property_metering_setting_id'] );
		if( isset( $arrValues['start_criteria'] ) && $boolDirectSet ) $this->set( 'm_intStartCriteria', trim( $arrValues['start_criteria'] ) ); elseif( isset( $arrValues['start_criteria'] ) ) $this->setStartCriteria( $arrValues['start_criteria'] );
		if( isset( $arrValues['end_criteria'] ) && $boolDirectSet ) $this->set( 'm_intEndCriteria', trim( $arrValues['end_criteria'] ) ); elseif( isset( $arrValues['end_criteria'] ) ) $this->setEndCriteria( $arrValues['end_criteria'] );
		if( isset( $arrValues['rate'] ) && $boolDirectSet ) $this->set( 'm_fltRate', trim( $arrValues['rate'] ) ); elseif( isset( $arrValues['rate'] ) ) $this->setRate( $arrValues['rate'] );
		if( isset( $arrValues['is_care_rate'] ) && $boolDirectSet ) $this->set( 'm_boolIsCareRate', trim( stripcslashes( $arrValues['is_care_rate'] ) ) ); elseif( isset( $arrValues['is_care_rate'] ) ) $this->setIsCareRate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_care_rate'] ) : $arrValues['is_care_rate'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setPropertyMeteringSettingId( $intPropertyMeteringSettingId ) {
		$this->set( 'm_intPropertyMeteringSettingId', CStrings::strToIntDef( $intPropertyMeteringSettingId, NULL, false ) );
	}

	public function getPropertyMeteringSettingId() {
		return $this->m_intPropertyMeteringSettingId;
	}

	public function sqlPropertyMeteringSettingId() {
		return ( true == isset( $this->m_intPropertyMeteringSettingId ) ) ? ( string ) $this->m_intPropertyMeteringSettingId : 'NULL';
	}

	public function setStartCriteria( $intStartCriteria ) {
		$this->set( 'm_intStartCriteria', CStrings::strToIntDef( $intStartCriteria, NULL, false ) );
	}

	public function getStartCriteria() {
		return $this->m_intStartCriteria;
	}

	public function sqlStartCriteria() {
		return ( true == isset( $this->m_intStartCriteria ) ) ? ( string ) $this->m_intStartCriteria : 'NULL';
	}

	public function setEndCriteria( $intEndCriteria ) {
		$this->set( 'm_intEndCriteria', CStrings::strToIntDef( $intEndCriteria, NULL, false ) );
	}

	public function getEndCriteria() {
		return $this->m_intEndCriteria;
	}

	public function sqlEndCriteria() {
		return ( true == isset( $this->m_intEndCriteria ) ) ? ( string ) $this->m_intEndCriteria : 'NULL';
	}

	public function setRate( $fltRate ) {
		$this->set( 'm_fltRate', CStrings::strToFloatDef( $fltRate, NULL, false, 5 ) );
	}

	public function getRate() {
		return $this->m_fltRate;
	}

	public function sqlRate() {
		return ( true == isset( $this->m_fltRate ) ) ? ( string ) $this->m_fltRate : 'NULL';
	}

	public function setIsCareRate( $boolIsCareRate ) {
		$this->set( 'm_boolIsCareRate', CStrings::strToBool( $boolIsCareRate ) );
	}

	public function getIsCareRate() {
		return $this->m_boolIsCareRate;
	}

	public function sqlIsCareRate() {
		return ( true == isset( $this->m_boolIsCareRate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCareRate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_utility_type_id, property_metering_setting_id, start_criteria, end_criteria, rate, is_care_rate, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyUtilityTypeId() . ', ' .
 						$this->sqlPropertyMeteringSettingId() . ', ' .
 						$this->sqlStartCriteria() . ', ' .
 						$this->sqlEndCriteria() . ', ' .
 						$this->sqlRate() . ', ' .
 						$this->sqlIsCareRate() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_metering_setting_id = ' . $this->sqlPropertyMeteringSettingId() . ','; } elseif( true == array_key_exists( 'PropertyMeteringSettingId', $this->getChangedColumns() ) ) { $strSql .= ' property_metering_setting_id = ' . $this->sqlPropertyMeteringSettingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_criteria = ' . $this->sqlStartCriteria() . ','; } elseif( true == array_key_exists( 'StartCriteria', $this->getChangedColumns() ) ) { $strSql .= ' start_criteria = ' . $this->sqlStartCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_criteria = ' . $this->sqlEndCriteria() . ','; } elseif( true == array_key_exists( 'EndCriteria', $this->getChangedColumns() ) ) { $strSql .= ' end_criteria = ' . $this->sqlEndCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate = ' . $this->sqlRate() . ','; } elseif( true == array_key_exists( 'Rate', $this->getChangedColumns() ) ) { $strSql .= ' rate = ' . $this->sqlRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_care_rate = ' . $this->sqlIsCareRate() . ','; } elseif( true == array_key_exists( 'IsCareRate', $this->getChangedColumns() ) ) { $strSql .= ' is_care_rate = ' . $this->sqlIsCareRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'property_metering_setting_id' => $this->getPropertyMeteringSettingId(),
			'start_criteria' => $this->getStartCriteria(),
			'end_criteria' => $this->getEndCriteria(),
			'rate' => $this->getRate(),
			'is_care_rate' => $this->getIsCareRate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>