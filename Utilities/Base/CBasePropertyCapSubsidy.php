<?php

class CBasePropertyCapSubsidy extends CEosSingularBase {

	const TABLE_NAME = 'public.property_cap_subsidies';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intUtilityAccountId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intUnitTypeId;
	protected $m_intCapSubsidyUtilityTypeId;
	protected $m_intOccupantCount;
	protected $m_intBedroomCount;
	protected $m_fltSquareFeetStartCriteria;
	protected $m_fltSquareFeetEndCriteria;
	protected $m_fltCapAmount;
	protected $m_fltSubsidyAmount;
	protected $m_boolDontChargeUsage;
	protected $m_boolDontChargeFees;
	protected $m_boolDontGenerateVcrInvoice;
	protected $m_boolDontApplyCap;
	protected $m_boolDontProrateSubsidy;
	protected $m_boolExcludeFromUsageFactor;
	protected $m_boolExcludeFromTotalBillCap;
	protected $m_boolExcludeFromCalculation;
	protected $m_boolExcludeFromAutomaticSubmeterEstimating;
	protected $m_boolIsVcr;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolDontApplySubsidy;

	public function __construct() {
		parent::__construct();

		$this->m_boolDontChargeUsage = false;
		$this->m_boolDontChargeFees = false;
		$this->m_boolDontGenerateVcrInvoice = false;
		$this->m_boolDontApplyCap = false;
		$this->m_boolDontProrateSubsidy = false;
		$this->m_boolExcludeFromUsageFactor = false;
		$this->m_boolExcludeFromTotalBillCap = false;
		$this->m_boolExcludeFromCalculation = false;
		$this->m_boolExcludeFromAutomaticSubmeterEstimating = false;
		$this->m_boolIsVcr = false;
		$this->m_boolDontApplySubsidy = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['cap_subsidy_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCapSubsidyUtilityTypeId', trim( $arrValues['cap_subsidy_utility_type_id'] ) ); elseif( isset( $arrValues['cap_subsidy_utility_type_id'] ) ) $this->setCapSubsidyUtilityTypeId( $arrValues['cap_subsidy_utility_type_id'] );
		if( isset( $arrValues['occupant_count'] ) && $boolDirectSet ) $this->set( 'm_intOccupantCount', trim( $arrValues['occupant_count'] ) ); elseif( isset( $arrValues['occupant_count'] ) ) $this->setOccupantCount( $arrValues['occupant_count'] );
		if( isset( $arrValues['bedroom_count'] ) && $boolDirectSet ) $this->set( 'm_intBedroomCount', trim( $arrValues['bedroom_count'] ) ); elseif( isset( $arrValues['bedroom_count'] ) ) $this->setBedroomCount( $arrValues['bedroom_count'] );
		if( isset( $arrValues['square_feet_start_criteria'] ) && $boolDirectSet ) $this->set( 'm_fltSquareFeetStartCriteria', trim( $arrValues['square_feet_start_criteria'] ) ); elseif( isset( $arrValues['square_feet_start_criteria'] ) ) $this->setSquareFeetStartCriteria( $arrValues['square_feet_start_criteria'] );
		if( isset( $arrValues['square_feet_end_criteria'] ) && $boolDirectSet ) $this->set( 'm_fltSquareFeetEndCriteria', trim( $arrValues['square_feet_end_criteria'] ) ); elseif( isset( $arrValues['square_feet_end_criteria'] ) ) $this->setSquareFeetEndCriteria( $arrValues['square_feet_end_criteria'] );
		if( isset( $arrValues['cap_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCapAmount', trim( $arrValues['cap_amount'] ) ); elseif( isset( $arrValues['cap_amount'] ) ) $this->setCapAmount( $arrValues['cap_amount'] );
		if( isset( $arrValues['subsidy_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSubsidyAmount', trim( $arrValues['subsidy_amount'] ) ); elseif( isset( $arrValues['subsidy_amount'] ) ) $this->setSubsidyAmount( $arrValues['subsidy_amount'] );
		if( isset( $arrValues['dont_charge_usage'] ) && $boolDirectSet ) $this->set( 'm_boolDontChargeUsage', trim( stripcslashes( $arrValues['dont_charge_usage'] ) ) ); elseif( isset( $arrValues['dont_charge_usage'] ) ) $this->setDontChargeUsage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_charge_usage'] ) : $arrValues['dont_charge_usage'] );
		if( isset( $arrValues['dont_charge_fees'] ) && $boolDirectSet ) $this->set( 'm_boolDontChargeFees', trim( stripcslashes( $arrValues['dont_charge_fees'] ) ) ); elseif( isset( $arrValues['dont_charge_fees'] ) ) $this->setDontChargeFees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_charge_fees'] ) : $arrValues['dont_charge_fees'] );
		if( isset( $arrValues['dont_generate_vcr_invoice'] ) && $boolDirectSet ) $this->set( 'm_boolDontGenerateVcrInvoice', trim( stripcslashes( $arrValues['dont_generate_vcr_invoice'] ) ) ); elseif( isset( $arrValues['dont_generate_vcr_invoice'] ) ) $this->setDontGenerateVcrInvoice( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_generate_vcr_invoice'] ) : $arrValues['dont_generate_vcr_invoice'] );
		if( isset( $arrValues['dont_apply_cap'] ) && $boolDirectSet ) $this->set( 'm_boolDontApplyCap', trim( stripcslashes( $arrValues['dont_apply_cap'] ) ) ); elseif( isset( $arrValues['dont_apply_cap'] ) ) $this->setDontApplyCap( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_apply_cap'] ) : $arrValues['dont_apply_cap'] );
		if( isset( $arrValues['dont_prorate_subsidy'] ) && $boolDirectSet ) $this->set( 'm_boolDontProrateSubsidy', trim( stripcslashes( $arrValues['dont_prorate_subsidy'] ) ) ); elseif( isset( $arrValues['dont_prorate_subsidy'] ) ) $this->setDontProrateSubsidy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_prorate_subsidy'] ) : $arrValues['dont_prorate_subsidy'] );
		if( isset( $arrValues['exclude_from_usage_factor'] ) && $boolDirectSet ) $this->set( 'm_boolExcludeFromUsageFactor', trim( stripcslashes( $arrValues['exclude_from_usage_factor'] ) ) ); elseif( isset( $arrValues['exclude_from_usage_factor'] ) ) $this->setExcludeFromUsageFactor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exclude_from_usage_factor'] ) : $arrValues['exclude_from_usage_factor'] );
		if( isset( $arrValues['exclude_from_total_bill_cap'] ) && $boolDirectSet ) $this->set( 'm_boolExcludeFromTotalBillCap', trim( stripcslashes( $arrValues['exclude_from_total_bill_cap'] ) ) ); elseif( isset( $arrValues['exclude_from_total_bill_cap'] ) ) $this->setExcludeFromTotalBillCap( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exclude_from_total_bill_cap'] ) : $arrValues['exclude_from_total_bill_cap'] );
		if( isset( $arrValues['exclude_from_calculation'] ) && $boolDirectSet ) $this->set( 'm_boolExcludeFromCalculation', trim( stripcslashes( $arrValues['exclude_from_calculation'] ) ) ); elseif( isset( $arrValues['exclude_from_calculation'] ) ) $this->setExcludeFromCalculation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exclude_from_calculation'] ) : $arrValues['exclude_from_calculation'] );
		if( isset( $arrValues['exclude_from_automatic_submeter_estimating'] ) && $boolDirectSet ) $this->set( 'm_boolExcludeFromAutomaticSubmeterEstimating', trim( stripcslashes( $arrValues['exclude_from_automatic_submeter_estimating'] ) ) ); elseif( isset( $arrValues['exclude_from_automatic_submeter_estimating'] ) ) $this->setExcludeFromAutomaticSubmeterEstimating( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exclude_from_automatic_submeter_estimating'] ) : $arrValues['exclude_from_automatic_submeter_estimating'] );
		if( isset( $arrValues['is_vcr'] ) && $boolDirectSet ) $this->set( 'm_boolIsVcr', trim( stripcslashes( $arrValues['is_vcr'] ) ) ); elseif( isset( $arrValues['is_vcr'] ) ) $this->setIsVcr( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_vcr'] ) : $arrValues['is_vcr'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['dont_apply_subsidy'] ) && $boolDirectSet ) $this->set( 'm_boolDontApplySubsidy', trim( stripcslashes( $arrValues['dont_apply_subsidy'] ) ) ); elseif( isset( $arrValues['dont_apply_subsidy'] ) ) $this->setDontApplySubsidy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_apply_subsidy'] ) : $arrValues['dont_apply_subsidy'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setCapSubsidyUtilityTypeId( $intCapSubsidyUtilityTypeId ) {
		$this->set( 'm_intCapSubsidyUtilityTypeId', CStrings::strToIntDef( $intCapSubsidyUtilityTypeId, NULL, false ) );
	}

	public function getCapSubsidyUtilityTypeId() {
		return $this->m_intCapSubsidyUtilityTypeId;
	}

	public function sqlCapSubsidyUtilityTypeId() {
		return ( true == isset( $this->m_intCapSubsidyUtilityTypeId ) ) ? ( string ) $this->m_intCapSubsidyUtilityTypeId : 'NULL';
	}

	public function setOccupantCount( $intOccupantCount ) {
		$this->set( 'm_intOccupantCount', CStrings::strToIntDef( $intOccupantCount, NULL, false ) );
	}

	public function getOccupantCount() {
		return $this->m_intOccupantCount;
	}

	public function sqlOccupantCount() {
		return ( true == isset( $this->m_intOccupantCount ) ) ? ( string ) $this->m_intOccupantCount : 'NULL';
	}

	public function setBedroomCount( $intBedroomCount ) {
		$this->set( 'm_intBedroomCount', CStrings::strToIntDef( $intBedroomCount, NULL, false ) );
	}

	public function getBedroomCount() {
		return $this->m_intBedroomCount;
	}

	public function sqlBedroomCount() {
		return ( true == isset( $this->m_intBedroomCount ) ) ? ( string ) $this->m_intBedroomCount : 'NULL';
	}

	public function setSquareFeetStartCriteria( $fltSquareFeetStartCriteria ) {
		$this->set( 'm_fltSquareFeetStartCriteria', CStrings::strToFloatDef( $fltSquareFeetStartCriteria, NULL, false, 4 ) );
	}

	public function getSquareFeetStartCriteria() {
		return $this->m_fltSquareFeetStartCriteria;
	}

	public function sqlSquareFeetStartCriteria() {
		return ( true == isset( $this->m_fltSquareFeetStartCriteria ) ) ? ( string ) $this->m_fltSquareFeetStartCriteria : 'NULL';
	}

	public function setSquareFeetEndCriteria( $fltSquareFeetEndCriteria ) {
		$this->set( 'm_fltSquareFeetEndCriteria', CStrings::strToFloatDef( $fltSquareFeetEndCriteria, NULL, false, 4 ) );
	}

	public function getSquareFeetEndCriteria() {
		return $this->m_fltSquareFeetEndCriteria;
	}

	public function sqlSquareFeetEndCriteria() {
		return ( true == isset( $this->m_fltSquareFeetEndCriteria ) ) ? ( string ) $this->m_fltSquareFeetEndCriteria : 'NULL';
	}

	public function setCapAmount( $fltCapAmount ) {
		$this->set( 'm_fltCapAmount', CStrings::strToFloatDef( $fltCapAmount, NULL, false, 4 ) );
	}

	public function getCapAmount() {
		return $this->m_fltCapAmount;
	}

	public function sqlCapAmount() {
		return ( true == isset( $this->m_fltCapAmount ) ) ? ( string ) $this->m_fltCapAmount : 'NULL';
	}

	public function setSubsidyAmount( $fltSubsidyAmount ) {
		$this->set( 'm_fltSubsidyAmount', CStrings::strToFloatDef( $fltSubsidyAmount, NULL, false, 4 ) );
	}

	public function getSubsidyAmount() {
		return $this->m_fltSubsidyAmount;
	}

	public function sqlSubsidyAmount() {
		return ( true == isset( $this->m_fltSubsidyAmount ) ) ? ( string ) $this->m_fltSubsidyAmount : 'NULL';
	}

	public function setDontChargeUsage( $boolDontChargeUsage ) {
		$this->set( 'm_boolDontChargeUsage', CStrings::strToBool( $boolDontChargeUsage ) );
	}

	public function getDontChargeUsage() {
		return $this->m_boolDontChargeUsage;
	}

	public function sqlDontChargeUsage() {
		return ( true == isset( $this->m_boolDontChargeUsage ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontChargeUsage ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontChargeFees( $boolDontChargeFees ) {
		$this->set( 'm_boolDontChargeFees', CStrings::strToBool( $boolDontChargeFees ) );
	}

	public function getDontChargeFees() {
		return $this->m_boolDontChargeFees;
	}

	public function sqlDontChargeFees() {
		return ( true == isset( $this->m_boolDontChargeFees ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontChargeFees ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontGenerateVcrInvoice( $boolDontGenerateVcrInvoice ) {
		$this->set( 'm_boolDontGenerateVcrInvoice', CStrings::strToBool( $boolDontGenerateVcrInvoice ) );
	}

	public function getDontGenerateVcrInvoice() {
		return $this->m_boolDontGenerateVcrInvoice;
	}

	public function sqlDontGenerateVcrInvoice() {
		return ( true == isset( $this->m_boolDontGenerateVcrInvoice ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontGenerateVcrInvoice ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontApplyCap( $boolDontApplyCap ) {
		$this->set( 'm_boolDontApplyCap', CStrings::strToBool( $boolDontApplyCap ) );
	}

	public function getDontApplyCap() {
		return $this->m_boolDontApplyCap;
	}

	public function sqlDontApplyCap() {
		return ( true == isset( $this->m_boolDontApplyCap ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontApplyCap ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontProrateSubsidy( $boolDontProrateSubsidy ) {
		$this->set( 'm_boolDontProrateSubsidy', CStrings::strToBool( $boolDontProrateSubsidy ) );
	}

	public function getDontProrateSubsidy() {
		return $this->m_boolDontProrateSubsidy;
	}

	public function sqlDontProrateSubsidy() {
		return ( true == isset( $this->m_boolDontProrateSubsidy ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontProrateSubsidy ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setExcludeFromUsageFactor( $boolExcludeFromUsageFactor ) {
		$this->set( 'm_boolExcludeFromUsageFactor', CStrings::strToBool( $boolExcludeFromUsageFactor ) );
	}

	public function getExcludeFromUsageFactor() {
		return $this->m_boolExcludeFromUsageFactor;
	}

	public function sqlExcludeFromUsageFactor() {
		return ( true == isset( $this->m_boolExcludeFromUsageFactor ) ) ? '\'' . ( true == ( bool ) $this->m_boolExcludeFromUsageFactor ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setExcludeFromTotalBillCap( $boolExcludeFromTotalBillCap ) {
		$this->set( 'm_boolExcludeFromTotalBillCap', CStrings::strToBool( $boolExcludeFromTotalBillCap ) );
	}

	public function getExcludeFromTotalBillCap() {
		return $this->m_boolExcludeFromTotalBillCap;
	}

	public function sqlExcludeFromTotalBillCap() {
		return ( true == isset( $this->m_boolExcludeFromTotalBillCap ) ) ? '\'' . ( true == ( bool ) $this->m_boolExcludeFromTotalBillCap ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setExcludeFromCalculation( $boolExcludeFromCalculation ) {
		$this->set( 'm_boolExcludeFromCalculation', CStrings::strToBool( $boolExcludeFromCalculation ) );
	}

	public function getExcludeFromCalculation() {
		return $this->m_boolExcludeFromCalculation;
	}

	public function sqlExcludeFromCalculation() {
		return ( true == isset( $this->m_boolExcludeFromCalculation ) ) ? '\'' . ( true == ( bool ) $this->m_boolExcludeFromCalculation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setExcludeFromAutomaticSubmeterEstimating( $boolExcludeFromAutomaticSubmeterEstimating ) {
		$this->set( 'm_boolExcludeFromAutomaticSubmeterEstimating', CStrings::strToBool( $boolExcludeFromAutomaticSubmeterEstimating ) );
	}

	public function getExcludeFromAutomaticSubmeterEstimating() {
		return $this->m_boolExcludeFromAutomaticSubmeterEstimating;
	}

	public function sqlExcludeFromAutomaticSubmeterEstimating() {
		return ( true == isset( $this->m_boolExcludeFromAutomaticSubmeterEstimating ) ) ? '\'' . ( true == ( bool ) $this->m_boolExcludeFromAutomaticSubmeterEstimating ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsVcr( $boolIsVcr ) {
		$this->set( 'm_boolIsVcr', CStrings::strToBool( $boolIsVcr ) );
	}

	public function getIsVcr() {
		return $this->m_boolIsVcr;
	}

	public function sqlIsVcr() {
		return ( true == isset( $this->m_boolIsVcr ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVcr ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDontApplySubsidy( $boolDontApplySubsidy ) {
		$this->set( 'm_boolDontApplySubsidy', CStrings::strToBool( $boolDontApplySubsidy ) );
	}

	public function getDontApplySubsidy() {
		return $this->m_boolDontApplySubsidy;
	}

	public function sqlDontApplySubsidy() {
		return ( true == isset( $this->m_boolDontApplySubsidy ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontApplySubsidy ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_type_id, utility_account_id, property_unit_id, unit_space_id, unit_type_id, cap_subsidy_utility_type_id, occupant_count, bedroom_count, square_feet_start_criteria, square_feet_end_criteria, cap_amount, subsidy_amount, dont_charge_usage, dont_charge_fees, dont_generate_vcr_invoice, dont_apply_cap, dont_prorate_subsidy, exclude_from_usage_factor, exclude_from_total_bill_cap, exclude_from_calculation, exclude_from_automatic_submeter_estimating, is_vcr, updated_by, updated_on, created_by, created_on, dont_apply_subsidy )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyUtilityTypeId() . ', ' .
 						$this->sqlUtilityAccountId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlUnitTypeId() . ', ' .
 						$this->sqlCapSubsidyUtilityTypeId() . ', ' .
 						$this->sqlOccupantCount() . ', ' .
 						$this->sqlBedroomCount() . ', ' .
 						$this->sqlSquareFeetStartCriteria() . ', ' .
 						$this->sqlSquareFeetEndCriteria() . ', ' .
 						$this->sqlCapAmount() . ', ' .
 						$this->sqlSubsidyAmount() . ', ' .
 						$this->sqlDontChargeUsage() . ', ' .
 						$this->sqlDontChargeFees() . ', ' .
 						$this->sqlDontGenerateVcrInvoice() . ', ' .
 						$this->sqlDontApplyCap() . ', ' .
 						$this->sqlDontProrateSubsidy() . ', ' .
 						$this->sqlExcludeFromUsageFactor() . ', ' .
 						$this->sqlExcludeFromTotalBillCap() . ', ' .
 						$this->sqlExcludeFromCalculation() . ', ' .
 						$this->sqlExcludeFromAutomaticSubmeterEstimating() . ', ' .
 						$this->sqlIsVcr() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlDontApplySubsidy() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cap_subsidy_utility_type_id = ' . $this->sqlCapSubsidyUtilityTypeId() . ','; } elseif( true == array_key_exists( 'CapSubsidyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' cap_subsidy_utility_type_id = ' . $this->sqlCapSubsidyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount() . ','; } elseif( true == array_key_exists( 'OccupantCount', $this->getChangedColumns() ) ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bedroom_count = ' . $this->sqlBedroomCount() . ','; } elseif( true == array_key_exists( 'BedroomCount', $this->getChangedColumns() ) ) { $strSql .= ' bedroom_count = ' . $this->sqlBedroomCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' square_feet_start_criteria = ' . $this->sqlSquareFeetStartCriteria() . ','; } elseif( true == array_key_exists( 'SquareFeetStartCriteria', $this->getChangedColumns() ) ) { $strSql .= ' square_feet_start_criteria = ' . $this->sqlSquareFeetStartCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' square_feet_end_criteria = ' . $this->sqlSquareFeetEndCriteria() . ','; } elseif( true == array_key_exists( 'SquareFeetEndCriteria', $this->getChangedColumns() ) ) { $strSql .= ' square_feet_end_criteria = ' . $this->sqlSquareFeetEndCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cap_amount = ' . $this->sqlCapAmount() . ','; } elseif( true == array_key_exists( 'CapAmount', $this->getChangedColumns() ) ) { $strSql .= ' cap_amount = ' . $this->sqlCapAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_amount = ' . $this->sqlSubsidyAmount() . ','; } elseif( true == array_key_exists( 'SubsidyAmount', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_amount = ' . $this->sqlSubsidyAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_charge_usage = ' . $this->sqlDontChargeUsage() . ','; } elseif( true == array_key_exists( 'DontChargeUsage', $this->getChangedColumns() ) ) { $strSql .= ' dont_charge_usage = ' . $this->sqlDontChargeUsage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_charge_fees = ' . $this->sqlDontChargeFees() . ','; } elseif( true == array_key_exists( 'DontChargeFees', $this->getChangedColumns() ) ) { $strSql .= ' dont_charge_fees = ' . $this->sqlDontChargeFees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_generate_vcr_invoice = ' . $this->sqlDontGenerateVcrInvoice() . ','; } elseif( true == array_key_exists( 'DontGenerateVcrInvoice', $this->getChangedColumns() ) ) { $strSql .= ' dont_generate_vcr_invoice = ' . $this->sqlDontGenerateVcrInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_apply_cap = ' . $this->sqlDontApplyCap() . ','; } elseif( true == array_key_exists( 'DontApplyCap', $this->getChangedColumns() ) ) { $strSql .= ' dont_apply_cap = ' . $this->sqlDontApplyCap() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_prorate_subsidy = ' . $this->sqlDontProrateSubsidy() . ','; } elseif( true == array_key_exists( 'DontProrateSubsidy', $this->getChangedColumns() ) ) { $strSql .= ' dont_prorate_subsidy = ' . $this->sqlDontProrateSubsidy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exclude_from_usage_factor = ' . $this->sqlExcludeFromUsageFactor() . ','; } elseif( true == array_key_exists( 'ExcludeFromUsageFactor', $this->getChangedColumns() ) ) { $strSql .= ' exclude_from_usage_factor = ' . $this->sqlExcludeFromUsageFactor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exclude_from_total_bill_cap = ' . $this->sqlExcludeFromTotalBillCap() . ','; } elseif( true == array_key_exists( 'ExcludeFromTotalBillCap', $this->getChangedColumns() ) ) { $strSql .= ' exclude_from_total_bill_cap = ' . $this->sqlExcludeFromTotalBillCap() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exclude_from_calculation = ' . $this->sqlExcludeFromCalculation() . ','; } elseif( true == array_key_exists( 'ExcludeFromCalculation', $this->getChangedColumns() ) ) { $strSql .= ' exclude_from_calculation = ' . $this->sqlExcludeFromCalculation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exclude_from_automatic_submeter_estimating = ' . $this->sqlExcludeFromAutomaticSubmeterEstimating() . ','; } elseif( true == array_key_exists( 'ExcludeFromAutomaticSubmeterEstimating', $this->getChangedColumns() ) ) { $strSql .= ' exclude_from_automatic_submeter_estimating = ' . $this->sqlExcludeFromAutomaticSubmeterEstimating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_vcr = ' . $this->sqlIsVcr() . ','; } elseif( true == array_key_exists( 'IsVcr', $this->getChangedColumns() ) ) { $strSql .= ' is_vcr = ' . $this->sqlIsVcr() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_apply_subsidy = ' . $this->sqlDontApplySubsidy() . ','; } elseif( true == array_key_exists( 'DontApplySubsidy', $this->getChangedColumns() ) ) { $strSql .= ' dont_apply_subsidy = ' . $this->sqlDontApplySubsidy() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'cap_subsidy_utility_type_id' => $this->getCapSubsidyUtilityTypeId(),
			'occupant_count' => $this->getOccupantCount(),
			'bedroom_count' => $this->getBedroomCount(),
			'square_feet_start_criteria' => $this->getSquareFeetStartCriteria(),
			'square_feet_end_criteria' => $this->getSquareFeetEndCriteria(),
			'cap_amount' => $this->getCapAmount(),
			'subsidy_amount' => $this->getSubsidyAmount(),
			'dont_charge_usage' => $this->getDontChargeUsage(),
			'dont_charge_fees' => $this->getDontChargeFees(),
			'dont_generate_vcr_invoice' => $this->getDontGenerateVcrInvoice(),
			'dont_apply_cap' => $this->getDontApplyCap(),
			'dont_prorate_subsidy' => $this->getDontProrateSubsidy(),
			'exclude_from_usage_factor' => $this->getExcludeFromUsageFactor(),
			'exclude_from_total_bill_cap' => $this->getExcludeFromTotalBillCap(),
			'exclude_from_calculation' => $this->getExcludeFromCalculation(),
			'exclude_from_automatic_submeter_estimating' => $this->getExcludeFromAutomaticSubmeterEstimating(),
			'is_vcr' => $this->getIsVcr(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'dont_apply_subsidy' => $this->getDontApplySubsidy()
		);
	}

}
?>