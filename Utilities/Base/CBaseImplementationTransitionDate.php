<?php

class CBaseImplementationTransitionDate extends CEosSingularBase {

	const TABLE_NAME = 'public.implementation_transition_dates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_strImplementationMonth;
	protected $m_strTransitionGoalDate;
	protected $m_strNote;
	protected $m_boolIsInternalError;
	protected $m_boolIsExternalError;
	protected $m_strTransitionDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsInternalError = false;
		$this->m_boolIsExternalError = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['implementation_month'] ) && $boolDirectSet ) $this->set( 'm_strImplementationMonth', trim( $arrValues['implementation_month'] ) ); elseif( isset( $arrValues['implementation_month'] ) ) $this->setImplementationMonth( $arrValues['implementation_month'] );
		if( isset( $arrValues['transition_goal_date'] ) && $boolDirectSet ) $this->set( 'm_strTransitionGoalDate', trim( $arrValues['transition_goal_date'] ) ); elseif( isset( $arrValues['transition_goal_date'] ) ) $this->setTransitionGoalDate( $arrValues['transition_goal_date'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['is_internal_error'] ) && $boolDirectSet ) $this->set( 'm_boolIsInternalError', trim( stripcslashes( $arrValues['is_internal_error'] ) ) ); elseif( isset( $arrValues['is_internal_error'] ) ) $this->setIsInternalError( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_internal_error'] ) : $arrValues['is_internal_error'] );
		if( isset( $arrValues['is_external_error'] ) && $boolDirectSet ) $this->set( 'm_boolIsExternalError', trim( stripcslashes( $arrValues['is_external_error'] ) ) ); elseif( isset( $arrValues['is_external_error'] ) ) $this->setIsExternalError( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_external_error'] ) : $arrValues['is_external_error'] );
		if( isset( $arrValues['transition_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransitionDatetime', trim( $arrValues['transition_datetime'] ) ); elseif( isset( $arrValues['transition_datetime'] ) ) $this->setTransitionDatetime( $arrValues['transition_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setImplementationMonth( $strImplementationMonth ) {
		$this->set( 'm_strImplementationMonth', CStrings::strTrimDef( $strImplementationMonth, -1, NULL, true ) );
	}

	public function getImplementationMonth() {
		return $this->m_strImplementationMonth;
	}

	public function sqlImplementationMonth() {
		return ( true == isset( $this->m_strImplementationMonth ) ) ? '\'' . $this->m_strImplementationMonth . '\'' : 'NULL';
	}

	public function setTransitionGoalDate( $strTransitionGoalDate ) {
		$this->set( 'm_strTransitionGoalDate', CStrings::strTrimDef( $strTransitionGoalDate, -1, NULL, true ) );
	}

	public function getTransitionGoalDate() {
		return $this->m_strTransitionGoalDate;
	}

	public function sqlTransitionGoalDate() {
		return ( true == isset( $this->m_strTransitionGoalDate ) ) ? '\'' . $this->m_strTransitionGoalDate . '\'' : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, 250, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setIsInternalError( $boolIsInternalError ) {
		$this->set( 'm_boolIsInternalError', CStrings::strToBool( $boolIsInternalError ) );
	}

	public function getIsInternalError() {
		return $this->m_boolIsInternalError;
	}

	public function sqlIsInternalError() {
		return ( true == isset( $this->m_boolIsInternalError ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInternalError ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsExternalError( $boolIsExternalError ) {
		$this->set( 'm_boolIsExternalError', CStrings::strToBool( $boolIsExternalError ) );
	}

	public function getIsExternalError() {
		return $this->m_boolIsExternalError;
	}

	public function sqlIsExternalError() {
		return ( true == isset( $this->m_boolIsExternalError ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsExternalError ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTransitionDatetime( $strTransitionDatetime ) {
		$this->set( 'm_strTransitionDatetime', CStrings::strTrimDef( $strTransitionDatetime, -1, NULL, true ) );
	}

	public function getTransitionDatetime() {
		return $this->m_strTransitionDatetime;
	}

	public function sqlTransitionDatetime() {
		return ( true == isset( $this->m_strTransitionDatetime ) ) ? '\'' . $this->m_strTransitionDatetime . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ps_product_id, implementation_month, transition_goal_date, note, is_internal_error, is_external_error, transition_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlImplementationMonth() . ', ' .
						$this->sqlTransitionGoalDate() . ', ' .
						$this->sqlNote() . ', ' .
						$this->sqlIsInternalError() . ', ' .
						$this->sqlIsExternalError() . ', ' .
						$this->sqlTransitionDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_month = ' . $this->sqlImplementationMonth(). ',' ; } elseif( true == array_key_exists( 'ImplementationMonth', $this->getChangedColumns() ) ) { $strSql .= ' implementation_month = ' . $this->sqlImplementationMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transition_goal_date = ' . $this->sqlTransitionGoalDate(). ',' ; } elseif( true == array_key_exists( 'TransitionGoalDate', $this->getChangedColumns() ) ) { $strSql .= ' transition_goal_date = ' . $this->sqlTransitionGoalDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_internal_error = ' . $this->sqlIsInternalError(). ',' ; } elseif( true == array_key_exists( 'IsInternalError', $this->getChangedColumns() ) ) { $strSql .= ' is_internal_error = ' . $this->sqlIsInternalError() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_external_error = ' . $this->sqlIsExternalError(). ',' ; } elseif( true == array_key_exists( 'IsExternalError', $this->getChangedColumns() ) ) { $strSql .= ' is_external_error = ' . $this->sqlIsExternalError() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transition_datetime = ' . $this->sqlTransitionDatetime(). ',' ; } elseif( true == array_key_exists( 'TransitionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transition_datetime = ' . $this->sqlTransitionDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'implementation_month' => $this->getImplementationMonth(),
			'transition_goal_date' => $this->getTransitionGoalDate(),
			'note' => $this->getNote(),
			'is_internal_error' => $this->getIsInternalError(),
			'is_external_error' => $this->getIsExternalError(),
			'transition_datetime' => $this->getTransitionDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>