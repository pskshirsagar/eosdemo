<?php

class CBaseOcrBillAnchor extends CEosSingularBase {

	const TABLE_NAME = 'public.ocr_bill_anchors';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillId;
	protected $m_strLabelValue;
	protected $m_strLabelVertices;
	protected $m_jsonLabelVertices;
	protected $m_strAnchorDetails;
	protected $m_jsonAnchorDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intOcrFieldId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['label_value'] ) && $boolDirectSet ) $this->set( 'm_strLabelValue', trim( $arrValues['label_value'] ) ); elseif( isset( $arrValues['label_value'] ) ) $this->setLabelValue( $arrValues['label_value'] );
		if( isset( $arrValues['label_vertices'] ) ) $this->set( 'm_strLabelVertices', trim( $arrValues['label_vertices'] ) );
		if( isset( $arrValues['anchor_details'] ) ) $this->set( 'm_strAnchorDetails', trim( $arrValues['anchor_details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ocr_field_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrFieldId', trim( $arrValues['ocr_field_id'] ) ); elseif( isset( $arrValues['ocr_field_id'] ) ) $this->setOcrFieldId( $arrValues['ocr_field_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setLabelValue( $strLabelValue ) {
		$this->set( 'm_strLabelValue', CStrings::strTrimDef( $strLabelValue, 250, NULL, true ) );
	}

	public function getLabelValue() {
		return $this->m_strLabelValue;
	}

	public function sqlLabelValue() {
		return ( true == isset( $this->m_strLabelValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLabelValue ) : '\'' . addslashes( $this->m_strLabelValue ) . '\'' ) : 'NULL';
	}

	public function setLabelVertices( $jsonLabelVertices ) {
		if( true == valObj( $jsonLabelVertices, 'stdClass' ) ) {
			$this->set( 'm_jsonLabelVertices', $jsonLabelVertices );
		} elseif( true == valJsonString( $jsonLabelVertices ) ) {
			$this->set( 'm_jsonLabelVertices', CStrings::strToJson( $jsonLabelVertices ) );
		} else {
			$this->set( 'm_jsonLabelVertices', NULL ); 
		}
		unset( $this->m_strLabelVertices );
	}

	public function getLabelVertices() {
		if( true == isset( $this->m_strLabelVertices ) ) {
			$this->m_jsonLabelVertices = CStrings::strToJson( $this->m_strLabelVertices );
			unset( $this->m_strLabelVertices );
		}
		return $this->m_jsonLabelVertices;
	}

	public function sqlLabelVertices() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getLabelVertices() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getLabelVertices() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getLabelVertices() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setAnchorDetails( $jsonAnchorDetails ) {
		if( true == valObj( $jsonAnchorDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonAnchorDetails', $jsonAnchorDetails );
		} elseif( true == valJsonString( $jsonAnchorDetails ) ) {
			$this->set( 'm_jsonAnchorDetails', CStrings::strToJson( $jsonAnchorDetails ) );
		} else {
			$this->set( 'm_jsonAnchorDetails', NULL ); 
		}
		unset( $this->m_strAnchorDetails );
	}

	public function getAnchorDetails() {
		if( true == isset( $this->m_strAnchorDetails ) ) {
			$this->m_jsonAnchorDetails = CStrings::strToJson( $this->m_strAnchorDetails );
			unset( $this->m_strAnchorDetails );
		}
		return $this->m_jsonAnchorDetails;
	}

	public function sqlAnchorDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAnchorDetails() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getAnchorDetails() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getAnchorDetails() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOcrFieldId( $intOcrFieldId ) {
		$this->set( 'm_intOcrFieldId', CStrings::strToIntDef( $intOcrFieldId, NULL, false ) );
	}

	public function getOcrFieldId() {
		return $this->m_intOcrFieldId;
	}

	public function sqlOcrFieldId() {
		return ( true == isset( $this->m_intOcrFieldId ) ) ? ( string ) $this->m_intOcrFieldId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_id, label_value, label_vertices, anchor_details, created_by, created_on, ocr_field_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlLabelValue() . ', ' .
						$this->sqlLabelVertices() . ', ' .
						$this->sqlAnchorDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlOcrFieldId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label_value = ' . $this->sqlLabelValue(). ',' ; } elseif( true == array_key_exists( 'LabelValue', $this->getChangedColumns() ) ) { $strSql .= ' label_value = ' . $this->sqlLabelValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label_vertices = ' . $this->sqlLabelVertices(). ',' ; } elseif( true == array_key_exists( 'LabelVertices', $this->getChangedColumns() ) ) { $strSql .= ' label_vertices = ' . $this->sqlLabelVertices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anchor_details = ' . $this->sqlAnchorDetails(). ',' ; } elseif( true == array_key_exists( 'AnchorDetails', $this->getChangedColumns() ) ) { $strSql .= ' anchor_details = ' . $this->sqlAnchorDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_field_id = ' . $this->sqlOcrFieldId() ; } elseif( true == array_key_exists( 'OcrFieldId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_field_id = ' . $this->sqlOcrFieldId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'label_value' => $this->getLabelValue(),
			'label_vertices' => $this->getLabelVertices(),
			'anchor_details' => $this->getAnchorDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ocr_field_id' => $this->getOcrFieldId()
		);
	}

}
?>