<?php

class CBaseMeterTransmissionBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.meter_transmission_batches';

	protected $m_intId;
	protected $m_intUtilityBatchId;
	protected $m_intPropertyMeterDeviceId;
	protected $m_intPropertyUnitId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intStartUtilityTransmissionId;
	protected $m_strMeterStartDate;
	protected $m_intMeterStartRead;
	protected $m_intEndUtilityTransmissionId;
	protected $m_strMeterEndDate;
	protected $m_intMeterEndRead;
	protected $m_fltAdjustedConsumptionAmount;
	protected $m_fltConsumptionAmount;
	protected $m_fltTotalEstimatedConsumption;
	protected $m_intConsecutiveEstimationCount;
	protected $m_intOccupantCount;
	protected $m_boolIsAdjustedUp;
	protected $m_boolIsEstimated;
	protected $m_boolIsCorrected;
	protected $m_boolIsTrueUp;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strNotes;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAdjustedUp = false;
		$this->m_boolIsEstimated = false;
		$this->m_boolIsCorrected = false;
		$this->m_boolIsTrueUp = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['property_meter_device_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyMeterDeviceId', trim( $arrValues['property_meter_device_id'] ) ); elseif( isset( $arrValues['property_meter_device_id'] ) ) $this->setPropertyMeterDeviceId( $arrValues['property_meter_device_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['start_utility_transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intStartUtilityTransmissionId', trim( $arrValues['start_utility_transmission_id'] ) ); elseif( isset( $arrValues['start_utility_transmission_id'] ) ) $this->setStartUtilityTransmissionId( $arrValues['start_utility_transmission_id'] );
		if( isset( $arrValues['meter_start_date'] ) && $boolDirectSet ) $this->set( 'm_strMeterStartDate', trim( $arrValues['meter_start_date'] ) ); elseif( isset( $arrValues['meter_start_date'] ) ) $this->setMeterStartDate( $arrValues['meter_start_date'] );
		if( isset( $arrValues['meter_start_read'] ) && $boolDirectSet ) $this->set( 'm_intMeterStartRead', trim( $arrValues['meter_start_read'] ) ); elseif( isset( $arrValues['meter_start_read'] ) ) $this->setMeterStartRead( $arrValues['meter_start_read'] );
		if( isset( $arrValues['end_utility_transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intEndUtilityTransmissionId', trim( $arrValues['end_utility_transmission_id'] ) ); elseif( isset( $arrValues['end_utility_transmission_id'] ) ) $this->setEndUtilityTransmissionId( $arrValues['end_utility_transmission_id'] );
		if( isset( $arrValues['meter_end_date'] ) && $boolDirectSet ) $this->set( 'm_strMeterEndDate', trim( $arrValues['meter_end_date'] ) ); elseif( isset( $arrValues['meter_end_date'] ) ) $this->setMeterEndDate( $arrValues['meter_end_date'] );
		if( isset( $arrValues['meter_end_read'] ) && $boolDirectSet ) $this->set( 'm_intMeterEndRead', trim( $arrValues['meter_end_read'] ) ); elseif( isset( $arrValues['meter_end_read'] ) ) $this->setMeterEndRead( $arrValues['meter_end_read'] );
		if( isset( $arrValues['adjusted_consumption_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustedConsumptionAmount', trim( $arrValues['adjusted_consumption_amount'] ) ); elseif( isset( $arrValues['adjusted_consumption_amount'] ) ) $this->setAdjustedConsumptionAmount( $arrValues['adjusted_consumption_amount'] );
		if( isset( $arrValues['consumption_amount'] ) && $boolDirectSet ) $this->set( 'm_fltConsumptionAmount', trim( $arrValues['consumption_amount'] ) ); elseif( isset( $arrValues['consumption_amount'] ) ) $this->setConsumptionAmount( $arrValues['consumption_amount'] );
		if( isset( $arrValues['total_estimated_consumption'] ) && $boolDirectSet ) $this->set( 'm_fltTotalEstimatedConsumption', trim( $arrValues['total_estimated_consumption'] ) ); elseif( isset( $arrValues['total_estimated_consumption'] ) ) $this->setTotalEstimatedConsumption( $arrValues['total_estimated_consumption'] );
		if( isset( $arrValues['consecutive_estimation_count'] ) && $boolDirectSet ) $this->set( 'm_intConsecutiveEstimationCount', trim( $arrValues['consecutive_estimation_count'] ) ); elseif( isset( $arrValues['consecutive_estimation_count'] ) ) $this->setConsecutiveEstimationCount( $arrValues['consecutive_estimation_count'] );
		if( isset( $arrValues['occupant_count'] ) && $boolDirectSet ) $this->set( 'm_intOccupantCount', trim( $arrValues['occupant_count'] ) ); elseif( isset( $arrValues['occupant_count'] ) ) $this->setOccupantCount( $arrValues['occupant_count'] );
		if( isset( $arrValues['is_adjusted_up'] ) && $boolDirectSet ) $this->set( 'm_boolIsAdjustedUp', trim( stripcslashes( $arrValues['is_adjusted_up'] ) ) ); elseif( isset( $arrValues['is_adjusted_up'] ) ) $this->setIsAdjustedUp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_adjusted_up'] ) : $arrValues['is_adjusted_up'] );
		if( isset( $arrValues['is_estimated'] ) && $boolDirectSet ) $this->set( 'm_boolIsEstimated', trim( stripcslashes( $arrValues['is_estimated'] ) ) ); elseif( isset( $arrValues['is_estimated'] ) ) $this->setIsEstimated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_estimated'] ) : $arrValues['is_estimated'] );
		if( isset( $arrValues['is_corrected'] ) && $boolDirectSet ) $this->set( 'm_boolIsCorrected', trim( stripcslashes( $arrValues['is_corrected'] ) ) ); elseif( isset( $arrValues['is_corrected'] ) ) $this->setIsCorrected( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_corrected'] ) : $arrValues['is_corrected'] );
		if( isset( $arrValues['is_true_up'] ) && $boolDirectSet ) $this->set( 'm_boolIsTrueUp', trim( stripcslashes( $arrValues['is_true_up'] ) ) ); elseif( isset( $arrValues['is_true_up'] ) ) $this->setIsTrueUp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_true_up'] ) : $arrValues['is_true_up'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setPropertyMeterDeviceId( $intPropertyMeterDeviceId ) {
		$this->set( 'm_intPropertyMeterDeviceId', CStrings::strToIntDef( $intPropertyMeterDeviceId, NULL, false ) );
	}

	public function getPropertyMeterDeviceId() {
		return $this->m_intPropertyMeterDeviceId;
	}

	public function sqlPropertyMeterDeviceId() {
		return ( true == isset( $this->m_intPropertyMeterDeviceId ) ) ? ( string ) $this->m_intPropertyMeterDeviceId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setStartUtilityTransmissionId( $intStartUtilityTransmissionId ) {
		$this->set( 'm_intStartUtilityTransmissionId', CStrings::strToIntDef( $intStartUtilityTransmissionId, NULL, false ) );
	}

	public function getStartUtilityTransmissionId() {
		return $this->m_intStartUtilityTransmissionId;
	}

	public function sqlStartUtilityTransmissionId() {
		return ( true == isset( $this->m_intStartUtilityTransmissionId ) ) ? ( string ) $this->m_intStartUtilityTransmissionId : 'NULL';
	}

	public function setMeterStartDate( $strMeterStartDate ) {
		$this->set( 'm_strMeterStartDate', CStrings::strTrimDef( $strMeterStartDate, -1, NULL, true ) );
	}

	public function getMeterStartDate() {
		return $this->m_strMeterStartDate;
	}

	public function sqlMeterStartDate() {
		return ( true == isset( $this->m_strMeterStartDate ) ) ? '\'' . $this->m_strMeterStartDate . '\'' : 'NOW()';
	}

	public function setMeterStartRead( $intMeterStartRead ) {
		$this->set( 'm_intMeterStartRead', CStrings::strToIntDef( $intMeterStartRead, NULL, false ) );
	}

	public function getMeterStartRead() {
		return $this->m_intMeterStartRead;
	}

	public function sqlMeterStartRead() {
		return ( true == isset( $this->m_intMeterStartRead ) ) ? ( string ) $this->m_intMeterStartRead : 'NULL';
	}

	public function setEndUtilityTransmissionId( $intEndUtilityTransmissionId ) {
		$this->set( 'm_intEndUtilityTransmissionId', CStrings::strToIntDef( $intEndUtilityTransmissionId, NULL, false ) );
	}

	public function getEndUtilityTransmissionId() {
		return $this->m_intEndUtilityTransmissionId;
	}

	public function sqlEndUtilityTransmissionId() {
		return ( true == isset( $this->m_intEndUtilityTransmissionId ) ) ? ( string ) $this->m_intEndUtilityTransmissionId : 'NULL';
	}

	public function setMeterEndDate( $strMeterEndDate ) {
		$this->set( 'm_strMeterEndDate', CStrings::strTrimDef( $strMeterEndDate, -1, NULL, true ) );
	}

	public function getMeterEndDate() {
		return $this->m_strMeterEndDate;
	}

	public function sqlMeterEndDate() {
		return ( true == isset( $this->m_strMeterEndDate ) ) ? '\'' . $this->m_strMeterEndDate . '\'' : 'NOW()';
	}

	public function setMeterEndRead( $intMeterEndRead ) {
		$this->set( 'm_intMeterEndRead', CStrings::strToIntDef( $intMeterEndRead, NULL, false ) );
	}

	public function getMeterEndRead() {
		return $this->m_intMeterEndRead;
	}

	public function sqlMeterEndRead() {
		return ( true == isset( $this->m_intMeterEndRead ) ) ? ( string ) $this->m_intMeterEndRead : 'NULL';
	}

	public function setAdjustedConsumptionAmount( $fltAdjustedConsumptionAmount ) {
		$this->set( 'm_fltAdjustedConsumptionAmount', CStrings::strToFloatDef( $fltAdjustedConsumptionAmount, NULL, false, 2 ) );
	}

	public function getAdjustedConsumptionAmount() {
		return $this->m_fltAdjustedConsumptionAmount;
	}

	public function sqlAdjustedConsumptionAmount() {
		return ( true == isset( $this->m_fltAdjustedConsumptionAmount ) ) ? ( string ) $this->m_fltAdjustedConsumptionAmount : 'NULL';
	}

	public function setConsumptionAmount( $fltConsumptionAmount ) {
		$this->set( 'm_fltConsumptionAmount', CStrings::strToFloatDef( $fltConsumptionAmount, NULL, false, 2 ) );
	}

	public function getConsumptionAmount() {
		return $this->m_fltConsumptionAmount;
	}

	public function sqlConsumptionAmount() {
		return ( true == isset( $this->m_fltConsumptionAmount ) ) ? ( string ) $this->m_fltConsumptionAmount : 'NULL';
	}

	public function setTotalEstimatedConsumption( $fltTotalEstimatedConsumption ) {
		$this->set( 'm_fltTotalEstimatedConsumption', CStrings::strToFloatDef( $fltTotalEstimatedConsumption, NULL, false, 2 ) );
	}

	public function getTotalEstimatedConsumption() {
		return $this->m_fltTotalEstimatedConsumption;
	}

	public function sqlTotalEstimatedConsumption() {
		return ( true == isset( $this->m_fltTotalEstimatedConsumption ) ) ? ( string ) $this->m_fltTotalEstimatedConsumption : 'NULL';
	}

	public function setConsecutiveEstimationCount( $intConsecutiveEstimationCount ) {
		$this->set( 'm_intConsecutiveEstimationCount', CStrings::strToIntDef( $intConsecutiveEstimationCount, NULL, false ) );
	}

	public function getConsecutiveEstimationCount() {
		return $this->m_intConsecutiveEstimationCount;
	}

	public function sqlConsecutiveEstimationCount() {
		return ( true == isset( $this->m_intConsecutiveEstimationCount ) ) ? ( string ) $this->m_intConsecutiveEstimationCount : 'NULL';
	}

	public function setOccupantCount( $intOccupantCount ) {
		$this->set( 'm_intOccupantCount', CStrings::strToIntDef( $intOccupantCount, NULL, false ) );
	}

	public function getOccupantCount() {
		return $this->m_intOccupantCount;
	}

	public function sqlOccupantCount() {
		return ( true == isset( $this->m_intOccupantCount ) ) ? ( string ) $this->m_intOccupantCount : 'NULL';
	}

	public function setIsAdjustedUp( $boolIsAdjustedUp ) {
		$this->set( 'm_boolIsAdjustedUp', CStrings::strToBool( $boolIsAdjustedUp ) );
	}

	public function getIsAdjustedUp() {
		return $this->m_boolIsAdjustedUp;
	}

	public function sqlIsAdjustedUp() {
		return ( true == isset( $this->m_boolIsAdjustedUp ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAdjustedUp ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsEstimated( $boolIsEstimated ) {
		$this->set( 'm_boolIsEstimated', CStrings::strToBool( $boolIsEstimated ) );
	}

	public function getIsEstimated() {
		return $this->m_boolIsEstimated;
	}

	public function sqlIsEstimated() {
		return ( true == isset( $this->m_boolIsEstimated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEstimated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCorrected( $boolIsCorrected ) {
		$this->set( 'm_boolIsCorrected', CStrings::strToBool( $boolIsCorrected ) );
	}

	public function getIsCorrected() {
		return $this->m_boolIsCorrected;
	}

	public function sqlIsCorrected() {
		return ( true == isset( $this->m_boolIsCorrected ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCorrected ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTrueUp( $boolIsTrueUp ) {
		$this->set( 'm_boolIsTrueUp', CStrings::strToBool( $boolIsTrueUp ) );
	}

	public function getIsTrueUp() {
		return $this->m_boolIsTrueUp;
	}

	public function sqlIsTrueUp() {
		return ( true == isset( $this->m_boolIsTrueUp ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTrueUp ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 100, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_batch_id, property_meter_device_id, property_unit_id, property_utility_type_id, start_utility_transmission_id, meter_start_date, meter_start_read, end_utility_transmission_id, meter_end_date, meter_end_read, adjusted_consumption_amount, consumption_amount, total_estimated_consumption, consecutive_estimation_count, occupant_count, is_adjusted_up, is_estimated, is_corrected, is_true_up, updated_by, updated_on, created_by, created_on, notes )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityBatchId() . ', ' .
						$this->sqlPropertyMeterDeviceId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlStartUtilityTransmissionId() . ', ' .
						$this->sqlMeterStartDate() . ', ' .
						$this->sqlMeterStartRead() . ', ' .
						$this->sqlEndUtilityTransmissionId() . ', ' .
						$this->sqlMeterEndDate() . ', ' .
						$this->sqlMeterEndRead() . ', ' .
						$this->sqlAdjustedConsumptionAmount() . ', ' .
						$this->sqlConsumptionAmount() . ', ' .
						$this->sqlTotalEstimatedConsumption() . ', ' .
						$this->sqlConsecutiveEstimationCount() . ', ' .
						$this->sqlOccupantCount() . ', ' .
						$this->sqlIsAdjustedUp() . ', ' .
						$this->sqlIsEstimated() . ', ' .
						$this->sqlIsCorrected() . ', ' .
						$this->sqlIsTrueUp() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlNotes() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId(). ',' ; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_meter_device_id = ' . $this->sqlPropertyMeterDeviceId(). ',' ; } elseif( true == array_key_exists( 'PropertyMeterDeviceId', $this->getChangedColumns() ) ) { $strSql .= ' property_meter_device_id = ' . $this->sqlPropertyMeterDeviceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_utility_transmission_id = ' . $this->sqlStartUtilityTransmissionId(). ',' ; } elseif( true == array_key_exists( 'StartUtilityTransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' start_utility_transmission_id = ' . $this->sqlStartUtilityTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_start_date = ' . $this->sqlMeterStartDate(). ',' ; } elseif( true == array_key_exists( 'MeterStartDate', $this->getChangedColumns() ) ) { $strSql .= ' meter_start_date = ' . $this->sqlMeterStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_start_read = ' . $this->sqlMeterStartRead(). ',' ; } elseif( true == array_key_exists( 'MeterStartRead', $this->getChangedColumns() ) ) { $strSql .= ' meter_start_read = ' . $this->sqlMeterStartRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_utility_transmission_id = ' . $this->sqlEndUtilityTransmissionId(). ',' ; } elseif( true == array_key_exists( 'EndUtilityTransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' end_utility_transmission_id = ' . $this->sqlEndUtilityTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_end_date = ' . $this->sqlMeterEndDate(). ',' ; } elseif( true == array_key_exists( 'MeterEndDate', $this->getChangedColumns() ) ) { $strSql .= ' meter_end_date = ' . $this->sqlMeterEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_end_read = ' . $this->sqlMeterEndRead(). ',' ; } elseif( true == array_key_exists( 'MeterEndRead', $this->getChangedColumns() ) ) { $strSql .= ' meter_end_read = ' . $this->sqlMeterEndRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjusted_consumption_amount = ' . $this->sqlAdjustedConsumptionAmount(). ',' ; } elseif( true == array_key_exists( 'AdjustedConsumptionAmount', $this->getChangedColumns() ) ) { $strSql .= ' adjusted_consumption_amount = ' . $this->sqlAdjustedConsumptionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumption_amount = ' . $this->sqlConsumptionAmount(). ',' ; } elseif( true == array_key_exists( 'ConsumptionAmount', $this->getChangedColumns() ) ) { $strSql .= ' consumption_amount = ' . $this->sqlConsumptionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_estimated_consumption = ' . $this->sqlTotalEstimatedConsumption(). ',' ; } elseif( true == array_key_exists( 'TotalEstimatedConsumption', $this->getChangedColumns() ) ) { $strSql .= ' total_estimated_consumption = ' . $this->sqlTotalEstimatedConsumption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consecutive_estimation_count = ' . $this->sqlConsecutiveEstimationCount(). ',' ; } elseif( true == array_key_exists( 'ConsecutiveEstimationCount', $this->getChangedColumns() ) ) { $strSql .= ' consecutive_estimation_count = ' . $this->sqlConsecutiveEstimationCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount(). ',' ; } elseif( true == array_key_exists( 'OccupantCount', $this->getChangedColumns() ) ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_adjusted_up = ' . $this->sqlIsAdjustedUp(). ',' ; } elseif( true == array_key_exists( 'IsAdjustedUp', $this->getChangedColumns() ) ) { $strSql .= ' is_adjusted_up = ' . $this->sqlIsAdjustedUp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_estimated = ' . $this->sqlIsEstimated(). ',' ; } elseif( true == array_key_exists( 'IsEstimated', $this->getChangedColumns() ) ) { $strSql .= ' is_estimated = ' . $this->sqlIsEstimated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_corrected = ' . $this->sqlIsCorrected(). ',' ; } elseif( true == array_key_exists( 'IsCorrected', $this->getChangedColumns() ) ) { $strSql .= ' is_corrected = ' . $this->sqlIsCorrected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_true_up = ' . $this->sqlIsTrueUp(). ',' ; } elseif( true == array_key_exists( 'IsTrueUp', $this->getChangedColumns() ) ) { $strSql .= ' is_true_up = ' . $this->sqlIsTrueUp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
						$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'property_meter_device_id' => $this->getPropertyMeterDeviceId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'start_utility_transmission_id' => $this->getStartUtilityTransmissionId(),
			'meter_start_date' => $this->getMeterStartDate(),
			'meter_start_read' => $this->getMeterStartRead(),
			'end_utility_transmission_id' => $this->getEndUtilityTransmissionId(),
			'meter_end_date' => $this->getMeterEndDate(),
			'meter_end_read' => $this->getMeterEndRead(),
			'adjusted_consumption_amount' => $this->getAdjustedConsumptionAmount(),
			'consumption_amount' => $this->getConsumptionAmount(),
			'total_estimated_consumption' => $this->getTotalEstimatedConsumption(),
			'consecutive_estimation_count' => $this->getConsecutiveEstimationCount(),
			'occupant_count' => $this->getOccupantCount(),
			'is_adjusted_up' => $this->getIsAdjustedUp(),
			'is_estimated' => $this->getIsEstimated(),
			'is_corrected' => $this->getIsCorrected(),
			'is_true_up' => $this->getIsTrueUp(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'notes' => $this->getNotes()
		);
	}

}
?>