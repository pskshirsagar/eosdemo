<?php

class CBaseUtilityBillLog extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_logs';

	protected $m_intId;
	protected $m_strLogDate;
	protected $m_intNewBillCount;
	protected $m_intUnassociatedBillCount;
	protected $m_intAssociatedBillCount;
	protected $m_intUnprocessedBillCount;
	protected $m_intProcessedBillCount;
	protected $m_intProcessedIpCount;
	protected $m_intEscalatedBillCount;
	protected $m_intPendingEscalationBillCount;
	protected $m_intAuditBillCount;
	protected $m_intExportedBillCount;
	protected $m_intUnexportedBillCount;
	protected $m_intNewMissingInvoiceAlertCount;
	protected $m_intMissingInvoiceAlertCount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['log_date'] ) && $boolDirectSet ) $this->set( 'm_strLogDate', trim( $arrValues['log_date'] ) ); elseif( isset( $arrValues['log_date'] ) ) $this->setLogDate( $arrValues['log_date'] );
		if( isset( $arrValues['new_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intNewBillCount', trim( $arrValues['new_bill_count'] ) ); elseif( isset( $arrValues['new_bill_count'] ) ) $this->setNewBillCount( $arrValues['new_bill_count'] );
		if( isset( $arrValues['unassociated_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intUnassociatedBillCount', trim( $arrValues['unassociated_bill_count'] ) ); elseif( isset( $arrValues['unassociated_bill_count'] ) ) $this->setUnassociatedBillCount( $arrValues['unassociated_bill_count'] );
		if( isset( $arrValues['associated_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intAssociatedBillCount', trim( $arrValues['associated_bill_count'] ) ); elseif( isset( $arrValues['associated_bill_count'] ) ) $this->setAssociatedBillCount( $arrValues['associated_bill_count'] );
		if( isset( $arrValues['unprocessed_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intUnprocessedBillCount', trim( $arrValues['unprocessed_bill_count'] ) ); elseif( isset( $arrValues['unprocessed_bill_count'] ) ) $this->setUnprocessedBillCount( $arrValues['unprocessed_bill_count'] );
		if( isset( $arrValues['processed_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intProcessedBillCount', trim( $arrValues['processed_bill_count'] ) ); elseif( isset( $arrValues['processed_bill_count'] ) ) $this->setProcessedBillCount( $arrValues['processed_bill_count'] );
		if( isset( $arrValues['processed_ip_count'] ) && $boolDirectSet ) $this->set( 'm_intProcessedIpCount', trim( $arrValues['processed_ip_count'] ) ); elseif( isset( $arrValues['processed_ip_count'] ) ) $this->setProcessedIpCount( $arrValues['processed_ip_count'] );
		if( isset( $arrValues['escalated_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intEscalatedBillCount', trim( $arrValues['escalated_bill_count'] ) ); elseif( isset( $arrValues['escalated_bill_count'] ) ) $this->setEscalatedBillCount( $arrValues['escalated_bill_count'] );
		if( isset( $arrValues['pending_escalation_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intPendingEscalationBillCount', trim( $arrValues['pending_escalation_bill_count'] ) ); elseif( isset( $arrValues['pending_escalation_bill_count'] ) ) $this->setPendingEscalationBillCount( $arrValues['pending_escalation_bill_count'] );
		if( isset( $arrValues['audit_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intAuditBillCount', trim( $arrValues['audit_bill_count'] ) ); elseif( isset( $arrValues['audit_bill_count'] ) ) $this->setAuditBillCount( $arrValues['audit_bill_count'] );
		if( isset( $arrValues['exported_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intExportedBillCount', trim( $arrValues['exported_bill_count'] ) ); elseif( isset( $arrValues['exported_bill_count'] ) ) $this->setExportedBillCount( $arrValues['exported_bill_count'] );
		if( isset( $arrValues['unexported_bill_count'] ) && $boolDirectSet ) $this->set( 'm_intUnexportedBillCount', trim( $arrValues['unexported_bill_count'] ) ); elseif( isset( $arrValues['unexported_bill_count'] ) ) $this->setUnexportedBillCount( $arrValues['unexported_bill_count'] );
		if( isset( $arrValues['new_missing_invoice_alert_count'] ) && $boolDirectSet ) $this->set( 'm_intNewMissingInvoiceAlertCount', trim( $arrValues['new_missing_invoice_alert_count'] ) ); elseif( isset( $arrValues['new_missing_invoice_alert_count'] ) ) $this->setNewMissingInvoiceAlertCount( $arrValues['new_missing_invoice_alert_count'] );
		if( isset( $arrValues['missing_invoice_alert_count'] ) && $boolDirectSet ) $this->set( 'm_intMissingInvoiceAlertCount', trim( $arrValues['missing_invoice_alert_count'] ) ); elseif( isset( $arrValues['missing_invoice_alert_count'] ) ) $this->setMissingInvoiceAlertCount( $arrValues['missing_invoice_alert_count'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setLogDate( $strLogDate ) {
		$this->set( 'm_strLogDate', CStrings::strTrimDef( $strLogDate, -1, NULL, true ) );
	}

	public function getLogDate() {
		return $this->m_strLogDate;
	}

	public function sqlLogDate() {
		return ( true == isset( $this->m_strLogDate ) ) ? '\'' . $this->m_strLogDate . '\'' : 'NOW()';
	}

	public function setNewBillCount( $intNewBillCount ) {
		$this->set( 'm_intNewBillCount', CStrings::strToIntDef( $intNewBillCount, NULL, false ) );
	}

	public function getNewBillCount() {
		return $this->m_intNewBillCount;
	}

	public function sqlNewBillCount() {
		return ( true == isset( $this->m_intNewBillCount ) ) ? ( string ) $this->m_intNewBillCount : 'NULL';
	}

	public function setUnassociatedBillCount( $intUnassociatedBillCount ) {
		$this->set( 'm_intUnassociatedBillCount', CStrings::strToIntDef( $intUnassociatedBillCount, NULL, false ) );
	}

	public function getUnassociatedBillCount() {
		return $this->m_intUnassociatedBillCount;
	}

	public function sqlUnassociatedBillCount() {
		return ( true == isset( $this->m_intUnassociatedBillCount ) ) ? ( string ) $this->m_intUnassociatedBillCount : 'NULL';
	}

	public function setAssociatedBillCount( $intAssociatedBillCount ) {
		$this->set( 'm_intAssociatedBillCount', CStrings::strToIntDef( $intAssociatedBillCount, NULL, false ) );
	}

	public function getAssociatedBillCount() {
		return $this->m_intAssociatedBillCount;
	}

	public function sqlAssociatedBillCount() {
		return ( true == isset( $this->m_intAssociatedBillCount ) ) ? ( string ) $this->m_intAssociatedBillCount : 'NULL';
	}

	public function setUnprocessedBillCount( $intUnprocessedBillCount ) {
		$this->set( 'm_intUnprocessedBillCount', CStrings::strToIntDef( $intUnprocessedBillCount, NULL, false ) );
	}

	public function getUnprocessedBillCount() {
		return $this->m_intUnprocessedBillCount;
	}

	public function sqlUnprocessedBillCount() {
		return ( true == isset( $this->m_intUnprocessedBillCount ) ) ? ( string ) $this->m_intUnprocessedBillCount : 'NULL';
	}

	public function setProcessedBillCount( $intProcessedBillCount ) {
		$this->set( 'm_intProcessedBillCount', CStrings::strToIntDef( $intProcessedBillCount, NULL, false ) );
	}

	public function getProcessedBillCount() {
		return $this->m_intProcessedBillCount;
	}

	public function sqlProcessedBillCount() {
		return ( true == isset( $this->m_intProcessedBillCount ) ) ? ( string ) $this->m_intProcessedBillCount : 'NULL';
	}

	public function setProcessedIpCount( $intProcessedIpCount ) {
		$this->set( 'm_intProcessedIpCount', CStrings::strToIntDef( $intProcessedIpCount, NULL, false ) );
	}

	public function getProcessedIpCount() {
		return $this->m_intProcessedIpCount;
	}

	public function sqlProcessedIpCount() {
		return ( true == isset( $this->m_intProcessedIpCount ) ) ? ( string ) $this->m_intProcessedIpCount : 'NULL';
	}

	public function setEscalatedBillCount( $intEscalatedBillCount ) {
		$this->set( 'm_intEscalatedBillCount', CStrings::strToIntDef( $intEscalatedBillCount, NULL, false ) );
	}

	public function getEscalatedBillCount() {
		return $this->m_intEscalatedBillCount;
	}

	public function sqlEscalatedBillCount() {
		return ( true == isset( $this->m_intEscalatedBillCount ) ) ? ( string ) $this->m_intEscalatedBillCount : 'NULL';
	}

	public function setPendingEscalationBillCount( $intPendingEscalationBillCount ) {
		$this->set( 'm_intPendingEscalationBillCount', CStrings::strToIntDef( $intPendingEscalationBillCount, NULL, false ) );
	}

	public function getPendingEscalationBillCount() {
		return $this->m_intPendingEscalationBillCount;
	}

	public function sqlPendingEscalationBillCount() {
		return ( true == isset( $this->m_intPendingEscalationBillCount ) ) ? ( string ) $this->m_intPendingEscalationBillCount : 'NULL';
	}

	public function setAuditBillCount( $intAuditBillCount ) {
		$this->set( 'm_intAuditBillCount', CStrings::strToIntDef( $intAuditBillCount, NULL, false ) );
	}

	public function getAuditBillCount() {
		return $this->m_intAuditBillCount;
	}

	public function sqlAuditBillCount() {
		return ( true == isset( $this->m_intAuditBillCount ) ) ? ( string ) $this->m_intAuditBillCount : 'NULL';
	}

	public function setExportedBillCount( $intExportedBillCount ) {
		$this->set( 'm_intExportedBillCount', CStrings::strToIntDef( $intExportedBillCount, NULL, false ) );
	}

	public function getExportedBillCount() {
		return $this->m_intExportedBillCount;
	}

	public function sqlExportedBillCount() {
		return ( true == isset( $this->m_intExportedBillCount ) ) ? ( string ) $this->m_intExportedBillCount : 'NULL';
	}

	public function setUnexportedBillCount( $intUnexportedBillCount ) {
		$this->set( 'm_intUnexportedBillCount', CStrings::strToIntDef( $intUnexportedBillCount, NULL, false ) );
	}

	public function getUnexportedBillCount() {
		return $this->m_intUnexportedBillCount;
	}

	public function sqlUnexportedBillCount() {
		return ( true == isset( $this->m_intUnexportedBillCount ) ) ? ( string ) $this->m_intUnexportedBillCount : 'NULL';
	}

	public function setNewMissingInvoiceAlertCount( $intNewMissingInvoiceAlertCount ) {
		$this->set( 'm_intNewMissingInvoiceAlertCount', CStrings::strToIntDef( $intNewMissingInvoiceAlertCount, NULL, false ) );
	}

	public function getNewMissingInvoiceAlertCount() {
		return $this->m_intNewMissingInvoiceAlertCount;
	}

	public function sqlNewMissingInvoiceAlertCount() {
		return ( true == isset( $this->m_intNewMissingInvoiceAlertCount ) ) ? ( string ) $this->m_intNewMissingInvoiceAlertCount : 'NULL';
	}

	public function setMissingInvoiceAlertCount( $intMissingInvoiceAlertCount ) {
		$this->set( 'm_intMissingInvoiceAlertCount', CStrings::strToIntDef( $intMissingInvoiceAlertCount, NULL, false ) );
	}

	public function getMissingInvoiceAlertCount() {
		return $this->m_intMissingInvoiceAlertCount;
	}

	public function sqlMissingInvoiceAlertCount() {
		return ( true == isset( $this->m_intMissingInvoiceAlertCount ) ) ? ( string ) $this->m_intMissingInvoiceAlertCount : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, log_date, new_bill_count, unassociated_bill_count, associated_bill_count, unprocessed_bill_count, processed_bill_count, processed_ip_count, escalated_bill_count, pending_escalation_bill_count, audit_bill_count, exported_bill_count, unexported_bill_count, new_missing_invoice_alert_count, missing_invoice_alert_count, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlLogDate() . ', ' .
 						$this->sqlNewBillCount() . ', ' .
 						$this->sqlUnassociatedBillCount() . ', ' .
 						$this->sqlAssociatedBillCount() . ', ' .
 						$this->sqlUnprocessedBillCount() . ', ' .
 						$this->sqlProcessedBillCount() . ', ' .
 						$this->sqlProcessedIpCount() . ', ' .
 						$this->sqlEscalatedBillCount() . ', ' .
 						$this->sqlPendingEscalationBillCount() . ', ' .
 						$this->sqlAuditBillCount() . ', ' .
 						$this->sqlExportedBillCount() . ', ' .
 						$this->sqlUnexportedBillCount() . ', ' .
 						$this->sqlNewMissingInvoiceAlertCount() . ', ' .
 						$this->sqlMissingInvoiceAlertCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_date = ' . $this->sqlLogDate() . ','; } elseif( true == array_key_exists( 'LogDate', $this->getChangedColumns() ) ) { $strSql .= ' log_date = ' . $this->sqlLogDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_bill_count = ' . $this->sqlNewBillCount() . ','; } elseif( true == array_key_exists( 'NewBillCount', $this->getChangedColumns() ) ) { $strSql .= ' new_bill_count = ' . $this->sqlNewBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unassociated_bill_count = ' . $this->sqlUnassociatedBillCount() . ','; } elseif( true == array_key_exists( 'UnassociatedBillCount', $this->getChangedColumns() ) ) { $strSql .= ' unassociated_bill_count = ' . $this->sqlUnassociatedBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_bill_count = ' . $this->sqlAssociatedBillCount() . ','; } elseif( true == array_key_exists( 'AssociatedBillCount', $this->getChangedColumns() ) ) { $strSql .= ' associated_bill_count = ' . $this->sqlAssociatedBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unprocessed_bill_count = ' . $this->sqlUnprocessedBillCount() . ','; } elseif( true == array_key_exists( 'UnprocessedBillCount', $this->getChangedColumns() ) ) { $strSql .= ' unprocessed_bill_count = ' . $this->sqlUnprocessedBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_bill_count = ' . $this->sqlProcessedBillCount() . ','; } elseif( true == array_key_exists( 'ProcessedBillCount', $this->getChangedColumns() ) ) { $strSql .= ' processed_bill_count = ' . $this->sqlProcessedBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_ip_count = ' . $this->sqlProcessedIpCount() . ','; } elseif( true == array_key_exists( 'ProcessedIpCount', $this->getChangedColumns() ) ) { $strSql .= ' processed_ip_count = ' . $this->sqlProcessedIpCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_bill_count = ' . $this->sqlEscalatedBillCount() . ','; } elseif( true == array_key_exists( 'EscalatedBillCount', $this->getChangedColumns() ) ) { $strSql .= ' escalated_bill_count = ' . $this->sqlEscalatedBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pending_escalation_bill_count = ' . $this->sqlPendingEscalationBillCount() . ','; } elseif( true == array_key_exists( 'PendingEscalationBillCount', $this->getChangedColumns() ) ) { $strSql .= ' pending_escalation_bill_count = ' . $this->sqlPendingEscalationBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_bill_count = ' . $this->sqlAuditBillCount() . ','; } elseif( true == array_key_exists( 'AuditBillCount', $this->getChangedColumns() ) ) { $strSql .= ' audit_bill_count = ' . $this->sqlAuditBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_bill_count = ' . $this->sqlExportedBillCount() . ','; } elseif( true == array_key_exists( 'ExportedBillCount', $this->getChangedColumns() ) ) { $strSql .= ' exported_bill_count = ' . $this->sqlExportedBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unexported_bill_count = ' . $this->sqlUnexportedBillCount() . ','; } elseif( true == array_key_exists( 'UnexportedBillCount', $this->getChangedColumns() ) ) { $strSql .= ' unexported_bill_count = ' . $this->sqlUnexportedBillCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_missing_invoice_alert_count = ' . $this->sqlNewMissingInvoiceAlertCount() . ','; } elseif( true == array_key_exists( 'NewMissingInvoiceAlertCount', $this->getChangedColumns() ) ) { $strSql .= ' new_missing_invoice_alert_count = ' . $this->sqlNewMissingInvoiceAlertCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' missing_invoice_alert_count = ' . $this->sqlMissingInvoiceAlertCount() . ','; } elseif( true == array_key_exists( 'MissingInvoiceAlertCount', $this->getChangedColumns() ) ) { $strSql .= ' missing_invoice_alert_count = ' . $this->sqlMissingInvoiceAlertCount() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'log_date' => $this->getLogDate(),
			'new_bill_count' => $this->getNewBillCount(),
			'unassociated_bill_count' => $this->getUnassociatedBillCount(),
			'associated_bill_count' => $this->getAssociatedBillCount(),
			'unprocessed_bill_count' => $this->getUnprocessedBillCount(),
			'processed_bill_count' => $this->getProcessedBillCount(),
			'processed_ip_count' => $this->getProcessedIpCount(),
			'escalated_bill_count' => $this->getEscalatedBillCount(),
			'pending_escalation_bill_count' => $this->getPendingEscalationBillCount(),
			'audit_bill_count' => $this->getAuditBillCount(),
			'exported_bill_count' => $this->getExportedBillCount(),
			'unexported_bill_count' => $this->getUnexportedBillCount(),
			'new_missing_invoice_alert_count' => $this->getNewMissingInvoiceAlertCount(),
			'missing_invoice_alert_count' => $this->getMissingInvoiceAlertCount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>