<?php

class CBaseComplianceDocVerification extends CEosSingularBase {

	const TABLE_NAME = 'public.compliance_doc_verifications';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intComplianceDocId;
	protected $m_intComplianceDocDenyTypeId;
	protected $m_strDeniedReason;
	protected $m_boolIsIgnored;
	protected $m_intVerifiedBy;
	protected $m_strVerifiedOn;
	protected $m_intDeniedBy;
	protected $m_strDeniedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsIgnored = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['compliance_doc_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceDocId', trim( $arrValues['compliance_doc_id'] ) ); elseif( isset( $arrValues['compliance_doc_id'] ) ) $this->setComplianceDocId( $arrValues['compliance_doc_id'] );
		if( isset( $arrValues['compliance_doc_deny_type_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceDocDenyTypeId', trim( $arrValues['compliance_doc_deny_type_id'] ) ); elseif( isset( $arrValues['compliance_doc_deny_type_id'] ) ) $this->setComplianceDocDenyTypeId( $arrValues['compliance_doc_deny_type_id'] );
		if( isset( $arrValues['denied_reason'] ) && $boolDirectSet ) $this->set( 'm_strDeniedReason', trim( stripcslashes( $arrValues['denied_reason'] ) ) ); elseif( isset( $arrValues['denied_reason'] ) ) $this->setDeniedReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['denied_reason'] ) : $arrValues['denied_reason'] );
		if( isset( $arrValues['is_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsIgnored', trim( stripcslashes( $arrValues['is_ignored'] ) ) ); elseif( isset( $arrValues['is_ignored'] ) ) $this->setIsIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ignored'] ) : $arrValues['is_ignored'] );
		if( isset( $arrValues['verified_by'] ) && $boolDirectSet ) $this->set( 'm_intVerifiedBy', trim( $arrValues['verified_by'] ) ); elseif( isset( $arrValues['verified_by'] ) ) $this->setVerifiedBy( $arrValues['verified_by'] );
		if( isset( $arrValues['verified_on'] ) && $boolDirectSet ) $this->set( 'm_strVerifiedOn', trim( $arrValues['verified_on'] ) ); elseif( isset( $arrValues['verified_on'] ) ) $this->setVerifiedOn( $arrValues['verified_on'] );
		if( isset( $arrValues['denied_by'] ) && $boolDirectSet ) $this->set( 'm_intDeniedBy', trim( $arrValues['denied_by'] ) ); elseif( isset( $arrValues['denied_by'] ) ) $this->setDeniedBy( $arrValues['denied_by'] );
		if( isset( $arrValues['denied_on'] ) && $boolDirectSet ) $this->set( 'm_strDeniedOn', trim( $arrValues['denied_on'] ) ); elseif( isset( $arrValues['denied_on'] ) ) $this->setDeniedOn( $arrValues['denied_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setComplianceDocId( $intComplianceDocId ) {
		$this->set( 'm_intComplianceDocId', CStrings::strToIntDef( $intComplianceDocId, NULL, false ) );
	}

	public function getComplianceDocId() {
		return $this->m_intComplianceDocId;
	}

	public function sqlComplianceDocId() {
		return ( true == isset( $this->m_intComplianceDocId ) ) ? ( string ) $this->m_intComplianceDocId : 'NULL';
	}

	public function setComplianceDocDenyTypeId( $intComplianceDocDenyTypeId ) {
		$this->set( 'm_intComplianceDocDenyTypeId', CStrings::strToIntDef( $intComplianceDocDenyTypeId, NULL, false ) );
	}

	public function getComplianceDocDenyTypeId() {
		return $this->m_intComplianceDocDenyTypeId;
	}

	public function sqlComplianceDocDenyTypeId() {
		return ( true == isset( $this->m_intComplianceDocDenyTypeId ) ) ? ( string ) $this->m_intComplianceDocDenyTypeId : 'NULL';
	}

	public function setDeniedReason( $strDeniedReason ) {
		$this->set( 'm_strDeniedReason', CStrings::strTrimDef( $strDeniedReason, -1, NULL, true ) );
	}

	public function getDeniedReason() {
		return $this->m_strDeniedReason;
	}

	public function sqlDeniedReason() {
		return ( true == isset( $this->m_strDeniedReason ) ) ? '\'' . addslashes( $this->m_strDeniedReason ) . '\'' : 'NULL';
	}

	public function setIsIgnored( $boolIsIgnored ) {
		$this->set( 'm_boolIsIgnored', CStrings::strToBool( $boolIsIgnored ) );
	}

	public function getIsIgnored() {
		return $this->m_boolIsIgnored;
	}

	public function sqlIsIgnored() {
		return ( true == isset( $this->m_boolIsIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setVerifiedBy( $intVerifiedBy ) {
		$this->set( 'm_intVerifiedBy', CStrings::strToIntDef( $intVerifiedBy, NULL, false ) );
	}

	public function getVerifiedBy() {
		return $this->m_intVerifiedBy;
	}

	public function sqlVerifiedBy() {
		return ( true == isset( $this->m_intVerifiedBy ) ) ? ( string ) $this->m_intVerifiedBy : 'NULL';
	}

	public function setVerifiedOn( $strVerifiedOn ) {
		$this->set( 'm_strVerifiedOn', CStrings::strTrimDef( $strVerifiedOn, -1, NULL, true ) );
	}

	public function getVerifiedOn() {
		return $this->m_strVerifiedOn;
	}

	public function sqlVerifiedOn() {
		return ( true == isset( $this->m_strVerifiedOn ) ) ? '\'' . $this->m_strVerifiedOn . '\'' : 'NULL';
	}

	public function setDeniedBy( $intDeniedBy ) {
		$this->set( 'm_intDeniedBy', CStrings::strToIntDef( $intDeniedBy, NULL, false ) );
	}

	public function getDeniedBy() {
		return $this->m_intDeniedBy;
	}

	public function sqlDeniedBy() {
		return ( true == isset( $this->m_intDeniedBy ) ) ? ( string ) $this->m_intDeniedBy : 'NULL';
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->set( 'm_strDeniedOn', CStrings::strTrimDef( $strDeniedOn, -1, NULL, true ) );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function sqlDeniedOn() {
		return ( true == isset( $this->m_strDeniedOn ) ) ? '\'' . $this->m_strDeniedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, compliance_doc_id, compliance_doc_deny_type_id, denied_reason, is_ignored, verified_by, verified_on, denied_by, denied_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlComplianceDocId() . ', ' .
 						$this->sqlComplianceDocDenyTypeId() . ', ' .
 						$this->sqlDeniedReason() . ', ' .
 						$this->sqlIsIgnored() . ', ' .
 						$this->sqlVerifiedBy() . ', ' .
 						$this->sqlVerifiedOn() . ', ' .
 						$this->sqlDeniedBy() . ', ' .
 						$this->sqlDeniedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_doc_id = ' . $this->sqlComplianceDocId() . ','; } elseif( true == array_key_exists( 'ComplianceDocId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_doc_id = ' . $this->sqlComplianceDocId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_doc_deny_type_id = ' . $this->sqlComplianceDocDenyTypeId() . ','; } elseif( true == array_key_exists( 'ComplianceDocDenyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_doc_deny_type_id = ' . $this->sqlComplianceDocDenyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_reason = ' . $this->sqlDeniedReason() . ','; } elseif( true == array_key_exists( 'DeniedReason', $this->getChangedColumns() ) ) { $strSql .= ' denied_reason = ' . $this->sqlDeniedReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ignored = ' . $this->sqlIsIgnored() . ','; } elseif( true == array_key_exists( 'IsIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_ignored = ' . $this->sqlIsIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_by = ' . $this->sqlVerifiedBy() . ','; } elseif( true == array_key_exists( 'VerifiedBy', $this->getChangedColumns() ) ) { $strSql .= ' verified_by = ' . $this->sqlVerifiedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; } elseif( true == array_key_exists( 'VerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; } elseif( true == array_key_exists( 'DeniedBy', $this->getChangedColumns() ) ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; } elseif( true == array_key_exists( 'DeniedOn', $this->getChangedColumns() ) ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'compliance_doc_id' => $this->getComplianceDocId(),
			'compliance_doc_deny_type_id' => $this->getComplianceDocDenyTypeId(),
			'denied_reason' => $this->getDeniedReason(),
			'is_ignored' => $this->getIsIgnored(),
			'verified_by' => $this->getVerifiedBy(),
			'verified_on' => $this->getVerifiedOn(),
			'denied_by' => $this->getDeniedBy(),
			'denied_on' => $this->getDeniedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>