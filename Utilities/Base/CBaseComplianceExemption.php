<?php

class CBaseComplianceExemption extends CEosSingularBase {

	const TABLE_NAME = 'public.compliance_exemptions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intApLegalEntityId;
	protected $m_intPropertyId;
	protected $m_strExemptionDatetime;
	protected $m_strExemptUntilDate;
	protected $m_strExemptionReason;
	protected $m_boolIsExemptAllItems;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsExemptAllItems = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_legal_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intApLegalEntityId', trim( $arrValues['ap_legal_entity_id'] ) ); elseif( isset( $arrValues['ap_legal_entity_id'] ) ) $this->setApLegalEntityId( $arrValues['ap_legal_entity_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['exemption_datetime'] ) && $boolDirectSet ) $this->set( 'm_strExemptionDatetime', trim( $arrValues['exemption_datetime'] ) ); elseif( isset( $arrValues['exemption_datetime'] ) ) $this->setExemptionDatetime( $arrValues['exemption_datetime'] );
		if( isset( $arrValues['exempt_until_date'] ) && $boolDirectSet ) $this->set( 'm_strExemptUntilDate', trim( $arrValues['exempt_until_date'] ) ); elseif( isset( $arrValues['exempt_until_date'] ) ) $this->setExemptUntilDate( $arrValues['exempt_until_date'] );
		if( isset( $arrValues['exemption_reason'] ) && $boolDirectSet ) $this->set( 'm_strExemptionReason', trim( stripcslashes( $arrValues['exemption_reason'] ) ) ); elseif( isset( $arrValues['exemption_reason'] ) ) $this->setExemptionReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exemption_reason'] ) : $arrValues['exemption_reason'] );
		if( isset( $arrValues['is_exempt_all_items'] ) && $boolDirectSet ) $this->set( 'm_boolIsExemptAllItems', trim( stripcslashes( $arrValues['is_exempt_all_items'] ) ) ); elseif( isset( $arrValues['is_exempt_all_items'] ) ) $this->setIsExemptAllItems( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_exempt_all_items'] ) : $arrValues['is_exempt_all_items'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApLegalEntityId( $intApLegalEntityId ) {
		$this->set( 'm_intApLegalEntityId', CStrings::strToIntDef( $intApLegalEntityId, NULL, false ) );
	}

	public function getApLegalEntityId() {
		return $this->m_intApLegalEntityId;
	}

	public function sqlApLegalEntityId() {
		return ( true == isset( $this->m_intApLegalEntityId ) ) ? ( string ) $this->m_intApLegalEntityId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setExemptionDatetime( $strExemptionDatetime ) {
		$this->set( 'm_strExemptionDatetime', CStrings::strTrimDef( $strExemptionDatetime, -1, NULL, true ) );
	}

	public function getExemptionDatetime() {
		return $this->m_strExemptionDatetime;
	}

	public function sqlExemptionDatetime() {
		return ( true == isset( $this->m_strExemptionDatetime ) ) ? '\'' . $this->m_strExemptionDatetime . '\'' : 'NOW()';
	}

	public function setExemptUntilDate( $strExemptUntilDate ) {
		$this->set( 'm_strExemptUntilDate', CStrings::strTrimDef( $strExemptUntilDate, -1, NULL, true ) );
	}

	public function getExemptUntilDate() {
		return $this->m_strExemptUntilDate;
	}

	public function sqlExemptUntilDate() {
		return ( true == isset( $this->m_strExemptUntilDate ) ) ? '\'' . $this->m_strExemptUntilDate . '\'' : 'NULL';
	}

	public function setExemptionReason( $strExemptionReason ) {
		$this->set( 'm_strExemptionReason', CStrings::strTrimDef( $strExemptionReason, -1, NULL, true ) );
	}

	public function getExemptionReason() {
		return $this->m_strExemptionReason;
	}

	public function sqlExemptionReason() {
		return ( true == isset( $this->m_strExemptionReason ) ) ? '\'' . addslashes( $this->m_strExemptionReason ) . '\'' : 'NULL';
	}

	public function setIsExemptAllItems( $boolIsExemptAllItems ) {
		$this->set( 'm_boolIsExemptAllItems', CStrings::strToBool( $boolIsExemptAllItems ) );
	}

	public function getIsExemptAllItems() {
		return $this->m_boolIsExemptAllItems;
	}

	public function sqlIsExemptAllItems() {
		return ( true == isset( $this->m_boolIsExemptAllItems ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsExemptAllItems ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, ap_legal_entity_id, property_id, exemption_datetime, exempt_until_date, exemption_reason, is_exempt_all_items, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlApLegalEntityId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlExemptionDatetime() . ', ' .
 						$this->sqlExemptUntilDate() . ', ' .
 						$this->sqlExemptionReason() . ', ' .
 						$this->sqlIsExemptAllItems() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_legal_entity_id = ' . $this->sqlApLegalEntityId() . ','; } elseif( true == array_key_exists( 'ApLegalEntityId', $this->getChangedColumns() ) ) { $strSql .= ' ap_legal_entity_id = ' . $this->sqlApLegalEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exemption_datetime = ' . $this->sqlExemptionDatetime() . ','; } elseif( true == array_key_exists( 'ExemptionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' exemption_datetime = ' . $this->sqlExemptionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exempt_until_date = ' . $this->sqlExemptUntilDate() . ','; } elseif( true == array_key_exists( 'ExemptUntilDate', $this->getChangedColumns() ) ) { $strSql .= ' exempt_until_date = ' . $this->sqlExemptUntilDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exemption_reason = ' . $this->sqlExemptionReason() . ','; } elseif( true == array_key_exists( 'ExemptionReason', $this->getChangedColumns() ) ) { $strSql .= ' exemption_reason = ' . $this->sqlExemptionReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_exempt_all_items = ' . $this->sqlIsExemptAllItems() . ','; } elseif( true == array_key_exists( 'IsExemptAllItems', $this->getChangedColumns() ) ) { $strSql .= ' is_exempt_all_items = ' . $this->sqlIsExemptAllItems() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_legal_entity_id' => $this->getApLegalEntityId(),
			'property_id' => $this->getPropertyId(),
			'exemption_datetime' => $this->getExemptionDatetime(),
			'exempt_until_date' => $this->getExemptUntilDate(),
			'exemption_reason' => $this->getExemptionReason(),
			'is_exempt_all_items' => $this->getIsExemptAllItems(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>