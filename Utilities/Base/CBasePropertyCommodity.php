<?php

class CBasePropertyCommodity extends CEosSingularBase {

	const TABLE_NAME = 'public.property_commodities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCommodityId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_boolHasUemService;
	protected $m_strUemGoLiveDate;
	protected $m_intPropertyGlAccountId;
	protected $m_intVacantUnitGlAccountId;
	protected $m_intUnitGlAccountId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolHasUemService = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['commodity_id'] ) && $boolDirectSet ) $this->set( 'm_intCommodityId', trim( $arrValues['commodity_id'] ) ); elseif( isset( $arrValues['commodity_id'] ) ) $this->setCommodityId( $arrValues['commodity_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['has_uem_service'] ) && $boolDirectSet ) $this->set( 'm_boolHasUemService', trim( stripcslashes( $arrValues['has_uem_service'] ) ) ); elseif( isset( $arrValues['has_uem_service'] ) ) $this->setHasUemService( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_uem_service'] ) : $arrValues['has_uem_service'] );
		if( isset( $arrValues['uem_go_live_date'] ) && $boolDirectSet ) $this->set( 'm_strUemGoLiveDate', trim( $arrValues['uem_go_live_date'] ) ); elseif( isset( $arrValues['uem_go_live_date'] ) ) $this->setUemGoLiveDate( $arrValues['uem_go_live_date'] );
		if( isset( $arrValues['property_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyGlAccountId', trim( $arrValues['property_gl_account_id'] ) ); elseif( isset( $arrValues['property_gl_account_id'] ) ) $this->setPropertyGlAccountId( $arrValues['property_gl_account_id'] );
		if( isset( $arrValues['vacant_unit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intVacantUnitGlAccountId', trim( $arrValues['vacant_unit_gl_account_id'] ) ); elseif( isset( $arrValues['vacant_unit_gl_account_id'] ) ) $this->setVacantUnitGlAccountId( $arrValues['vacant_unit_gl_account_id'] );
		if( isset( $arrValues['unit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitGlAccountId', trim( $arrValues['unit_gl_account_id'] ) ); elseif( isset( $arrValues['unit_gl_account_id'] ) ) $this->setUnitGlAccountId( $arrValues['unit_gl_account_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCommodityId( $intCommodityId ) {
		$this->set( 'm_intCommodityId', CStrings::strToIntDef( $intCommodityId, NULL, false ) );
	}

	public function getCommodityId() {
		return $this->m_intCommodityId;
	}

	public function sqlCommodityId() {
		return ( true == isset( $this->m_intCommodityId ) ) ? ( string ) $this->m_intCommodityId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setHasUemService( $boolHasUemService ) {
		$this->set( 'm_boolHasUemService', CStrings::strToBool( $boolHasUemService ) );
	}

	public function getHasUemService() {
		return $this->m_boolHasUemService;
	}

	public function sqlHasUemService() {
		return ( true == isset( $this->m_boolHasUemService ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasUemService ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUemGoLiveDate( $strUemGoLiveDate ) {
		$this->set( 'm_strUemGoLiveDate', CStrings::strTrimDef( $strUemGoLiveDate, -1, NULL, true ) );
	}

	public function getUemGoLiveDate() {
		return $this->m_strUemGoLiveDate;
	}

	public function sqlUemGoLiveDate() {
		return ( true == isset( $this->m_strUemGoLiveDate ) ) ? '\'' . $this->m_strUemGoLiveDate . '\'' : 'NULL';
	}

	public function setPropertyGlAccountId( $intPropertyGlAccountId ) {
		$this->set( 'm_intPropertyGlAccountId', CStrings::strToIntDef( $intPropertyGlAccountId, NULL, false ) );
	}

	public function getPropertyGlAccountId() {
		return $this->m_intPropertyGlAccountId;
	}

	public function sqlPropertyGlAccountId() {
		return ( true == isset( $this->m_intPropertyGlAccountId ) ) ? ( string ) $this->m_intPropertyGlAccountId : 'NULL';
	}

	public function setVacantUnitGlAccountId( $intVacantUnitGlAccountId ) {
		$this->set( 'm_intVacantUnitGlAccountId', CStrings::strToIntDef( $intVacantUnitGlAccountId, NULL, false ) );
	}

	public function getVacantUnitGlAccountId() {
		return $this->m_intVacantUnitGlAccountId;
	}

	public function sqlVacantUnitGlAccountId() {
		return ( true == isset( $this->m_intVacantUnitGlAccountId ) ) ? ( string ) $this->m_intVacantUnitGlAccountId : 'NULL';
	}

	public function setUnitGlAccountId( $intUnitGlAccountId ) {
		$this->set( 'm_intUnitGlAccountId', CStrings::strToIntDef( $intUnitGlAccountId, NULL, false ) );
	}

	public function getUnitGlAccountId() {
		return $this->m_intUnitGlAccountId;
	}

	public function sqlUnitGlAccountId() {
		return ( true == isset( $this->m_intUnitGlAccountId ) ) ? ( string ) $this->m_intUnitGlAccountId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, commodity_id, property_utility_type_id, has_uem_service, uem_go_live_date, property_gl_account_id, vacant_unit_gl_account_id, unit_gl_account_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCommodityId() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlHasUemService() . ', ' .
						$this->sqlUemGoLiveDate() . ', ' .
						$this->sqlPropertyGlAccountId() . ', ' .
						$this->sqlVacantUnitGlAccountId() . ', ' .
						$this->sqlUnitGlAccountId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commodity_id = ' . $this->sqlCommodityId(). ',' ; } elseif( true == array_key_exists( 'CommodityId', $this->getChangedColumns() ) ) { $strSql .= ' commodity_id = ' . $this->sqlCommodityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_uem_service = ' . $this->sqlHasUemService(). ',' ; } elseif( true == array_key_exists( 'HasUemService', $this->getChangedColumns() ) ) { $strSql .= ' has_uem_service = ' . $this->sqlHasUemService() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uem_go_live_date = ' . $this->sqlUemGoLiveDate(). ',' ; } elseif( true == array_key_exists( 'UemGoLiveDate', $this->getChangedColumns() ) ) { $strSql .= ' uem_go_live_date = ' . $this->sqlUemGoLiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_gl_account_id = ' . $this->sqlPropertyGlAccountId(). ',' ; } elseif( true == array_key_exists( 'PropertyGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' property_gl_account_id = ' . $this->sqlPropertyGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacant_unit_gl_account_id = ' . $this->sqlVacantUnitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'VacantUnitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' vacant_unit_gl_account_id = ' . $this->sqlVacantUnitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_gl_account_id = ' . $this->sqlUnitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'UnitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' unit_gl_account_id = ' . $this->sqlUnitGlAccountId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'commodity_id' => $this->getCommodityId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'has_uem_service' => $this->getHasUemService(),
			'uem_go_live_date' => $this->getUemGoLiveDate(),
			'property_gl_account_id' => $this->getPropertyGlAccountId(),
			'vacant_unit_gl_account_id' => $this->getVacantUnitGlAccountId(),
			'unit_gl_account_id' => $this->getUnitGlAccountId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>