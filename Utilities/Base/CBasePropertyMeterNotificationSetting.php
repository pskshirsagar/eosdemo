<?php

class CBasePropertyMeterNotificationSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.property_meter_notification_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyMeteringSettingId;
	protected $m_intOccupantCount;
	protected $m_intHighVacantDailyLimit;
	protected $m_intHighVacantWeeklyLimit;
	protected $m_intHighConsumptionDailyLimit;
	protected $m_intHighConsumptionWeeklyLimit;
	protected $m_intZeroReadNotificationDays;
	protected $m_fltLeakPercentage;
	protected $m_boolIsHighConsumptionNotifications;
	protected $m_boolIsHighVacantConsumptionNotifications;
	protected $m_boolIsInactiveNotifications;
	protected $m_boolIsLowBatteryNotifications;
	protected $m_boolIsLeakDetectionNotifications;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMissingReadNotificationDays;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsHighConsumptionNotifications = false;
		$this->m_boolIsHighVacantConsumptionNotifications = false;
		$this->m_boolIsInactiveNotifications = false;
		$this->m_boolIsLowBatteryNotifications = false;
		$this->m_boolIsLeakDetectionNotifications = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_metering_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyMeteringSettingId', trim( $arrValues['property_metering_setting_id'] ) ); elseif( isset( $arrValues['property_metering_setting_id'] ) ) $this->setPropertyMeteringSettingId( $arrValues['property_metering_setting_id'] );
		if( isset( $arrValues['occupant_count'] ) && $boolDirectSet ) $this->set( 'm_intOccupantCount', trim( $arrValues['occupant_count'] ) ); elseif( isset( $arrValues['occupant_count'] ) ) $this->setOccupantCount( $arrValues['occupant_count'] );
		if( isset( $arrValues['high_vacant_daily_limit'] ) && $boolDirectSet ) $this->set( 'm_intHighVacantDailyLimit', trim( $arrValues['high_vacant_daily_limit'] ) ); elseif( isset( $arrValues['high_vacant_daily_limit'] ) ) $this->setHighVacantDailyLimit( $arrValues['high_vacant_daily_limit'] );
		if( isset( $arrValues['high_vacant_weekly_limit'] ) && $boolDirectSet ) $this->set( 'm_intHighVacantWeeklyLimit', trim( $arrValues['high_vacant_weekly_limit'] ) ); elseif( isset( $arrValues['high_vacant_weekly_limit'] ) ) $this->setHighVacantWeeklyLimit( $arrValues['high_vacant_weekly_limit'] );
		if( isset( $arrValues['high_consumption_daily_limit'] ) && $boolDirectSet ) $this->set( 'm_intHighConsumptionDailyLimit', trim( $arrValues['high_consumption_daily_limit'] ) ); elseif( isset( $arrValues['high_consumption_daily_limit'] ) ) $this->setHighConsumptionDailyLimit( $arrValues['high_consumption_daily_limit'] );
		if( isset( $arrValues['high_consumption_weekly_limit'] ) && $boolDirectSet ) $this->set( 'm_intHighConsumptionWeeklyLimit', trim( $arrValues['high_consumption_weekly_limit'] ) ); elseif( isset( $arrValues['high_consumption_weekly_limit'] ) ) $this->setHighConsumptionWeeklyLimit( $arrValues['high_consumption_weekly_limit'] );
		if( isset( $arrValues['zero_read_notification_days'] ) && $boolDirectSet ) $this->set( 'm_intZeroReadNotificationDays', trim( $arrValues['zero_read_notification_days'] ) ); elseif( isset( $arrValues['zero_read_notification_days'] ) ) $this->setZeroReadNotificationDays( $arrValues['zero_read_notification_days'] );
		if( isset( $arrValues['leak_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltLeakPercentage', trim( $arrValues['leak_percentage'] ) ); elseif( isset( $arrValues['leak_percentage'] ) ) $this->setLeakPercentage( $arrValues['leak_percentage'] );
		if( isset( $arrValues['is_high_consumption_notifications'] ) && $boolDirectSet ) $this->set( 'm_boolIsHighConsumptionNotifications', trim( stripcslashes( $arrValues['is_high_consumption_notifications'] ) ) ); elseif( isset( $arrValues['is_high_consumption_notifications'] ) ) $this->setIsHighConsumptionNotifications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_high_consumption_notifications'] ) : $arrValues['is_high_consumption_notifications'] );
		if( isset( $arrValues['is_high_vacant_consumption_notifications'] ) && $boolDirectSet ) $this->set( 'm_boolIsHighVacantConsumptionNotifications', trim( stripcslashes( $arrValues['is_high_vacant_consumption_notifications'] ) ) ); elseif( isset( $arrValues['is_high_vacant_consumption_notifications'] ) ) $this->setIsHighVacantConsumptionNotifications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_high_vacant_consumption_notifications'] ) : $arrValues['is_high_vacant_consumption_notifications'] );
		if( isset( $arrValues['is_inactive_notifications'] ) && $boolDirectSet ) $this->set( 'm_boolIsInactiveNotifications', trim( stripcslashes( $arrValues['is_inactive_notifications'] ) ) ); elseif( isset( $arrValues['is_inactive_notifications'] ) ) $this->setIsInactiveNotifications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_inactive_notifications'] ) : $arrValues['is_inactive_notifications'] );
		if( isset( $arrValues['is_low_battery_notifications'] ) && $boolDirectSet ) $this->set( 'm_boolIsLowBatteryNotifications', trim( stripcslashes( $arrValues['is_low_battery_notifications'] ) ) ); elseif( isset( $arrValues['is_low_battery_notifications'] ) ) $this->setIsLowBatteryNotifications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_low_battery_notifications'] ) : $arrValues['is_low_battery_notifications'] );
		if( isset( $arrValues['is_leak_detection_notifications'] ) && $boolDirectSet ) $this->set( 'm_boolIsLeakDetectionNotifications', trim( stripcslashes( $arrValues['is_leak_detection_notifications'] ) ) ); elseif( isset( $arrValues['is_leak_detection_notifications'] ) ) $this->setIsLeakDetectionNotifications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_leak_detection_notifications'] ) : $arrValues['is_leak_detection_notifications'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['missing_read_notification_days'] ) && $boolDirectSet ) $this->set( 'm_intMissingReadNotificationDays', trim( $arrValues['missing_read_notification_days'] ) ); elseif( isset( $arrValues['missing_read_notification_days'] ) ) $this->setMissingReadNotificationDays( $arrValues['missing_read_notification_days'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyMeteringSettingId( $intPropertyMeteringSettingId ) {
		$this->set( 'm_intPropertyMeteringSettingId', CStrings::strToIntDef( $intPropertyMeteringSettingId, NULL, false ) );
	}

	public function getPropertyMeteringSettingId() {
		return $this->m_intPropertyMeteringSettingId;
	}

	public function sqlPropertyMeteringSettingId() {
		return ( true == isset( $this->m_intPropertyMeteringSettingId ) ) ? ( string ) $this->m_intPropertyMeteringSettingId : 'NULL';
	}

	public function setOccupantCount( $intOccupantCount ) {
		$this->set( 'm_intOccupantCount', CStrings::strToIntDef( $intOccupantCount, NULL, false ) );
	}

	public function getOccupantCount() {
		return $this->m_intOccupantCount;
	}

	public function sqlOccupantCount() {
		return ( true == isset( $this->m_intOccupantCount ) ) ? ( string ) $this->m_intOccupantCount : 'NULL';
	}

	public function setHighVacantDailyLimit( $intHighVacantDailyLimit ) {
		$this->set( 'm_intHighVacantDailyLimit', CStrings::strToIntDef( $intHighVacantDailyLimit, NULL, false ) );
	}

	public function getHighVacantDailyLimit() {
		return $this->m_intHighVacantDailyLimit;
	}

	public function sqlHighVacantDailyLimit() {
		return ( true == isset( $this->m_intHighVacantDailyLimit ) ) ? ( string ) $this->m_intHighVacantDailyLimit : 'NULL';
	}

	public function setHighVacantWeeklyLimit( $intHighVacantWeeklyLimit ) {
		$this->set( 'm_intHighVacantWeeklyLimit', CStrings::strToIntDef( $intHighVacantWeeklyLimit, NULL, false ) );
	}

	public function getHighVacantWeeklyLimit() {
		return $this->m_intHighVacantWeeklyLimit;
	}

	public function sqlHighVacantWeeklyLimit() {
		return ( true == isset( $this->m_intHighVacantWeeklyLimit ) ) ? ( string ) $this->m_intHighVacantWeeklyLimit : 'NULL';
	}

	public function setHighConsumptionDailyLimit( $intHighConsumptionDailyLimit ) {
		$this->set( 'm_intHighConsumptionDailyLimit', CStrings::strToIntDef( $intHighConsumptionDailyLimit, NULL, false ) );
	}

	public function getHighConsumptionDailyLimit() {
		return $this->m_intHighConsumptionDailyLimit;
	}

	public function sqlHighConsumptionDailyLimit() {
		return ( true == isset( $this->m_intHighConsumptionDailyLimit ) ) ? ( string ) $this->m_intHighConsumptionDailyLimit : 'NULL';
	}

	public function setHighConsumptionWeeklyLimit( $intHighConsumptionWeeklyLimit ) {
		$this->set( 'm_intHighConsumptionWeeklyLimit', CStrings::strToIntDef( $intHighConsumptionWeeklyLimit, NULL, false ) );
	}

	public function getHighConsumptionWeeklyLimit() {
		return $this->m_intHighConsumptionWeeklyLimit;
	}

	public function sqlHighConsumptionWeeklyLimit() {
		return ( true == isset( $this->m_intHighConsumptionWeeklyLimit ) ) ? ( string ) $this->m_intHighConsumptionWeeklyLimit : 'NULL';
	}

	public function setZeroReadNotificationDays( $intZeroReadNotificationDays ) {
		$this->set( 'm_intZeroReadNotificationDays', CStrings::strToIntDef( $intZeroReadNotificationDays, NULL, false ) );
	}

	public function getZeroReadNotificationDays() {
		return $this->m_intZeroReadNotificationDays;
	}

	public function sqlZeroReadNotificationDays() {
		return ( true == isset( $this->m_intZeroReadNotificationDays ) ) ? ( string ) $this->m_intZeroReadNotificationDays : 'NULL';
	}

	public function setLeakPercentage( $fltLeakPercentage ) {
		$this->set( 'm_fltLeakPercentage', CStrings::strToFloatDef( $fltLeakPercentage, NULL, false, 2 ) );
	}

	public function getLeakPercentage() {
		return $this->m_fltLeakPercentage;
	}

	public function sqlLeakPercentage() {
		return ( true == isset( $this->m_fltLeakPercentage ) ) ? ( string ) $this->m_fltLeakPercentage : 'NULL';
	}

	public function setIsHighConsumptionNotifications( $boolIsHighConsumptionNotifications ) {
		$this->set( 'm_boolIsHighConsumptionNotifications', CStrings::strToBool( $boolIsHighConsumptionNotifications ) );
	}

	public function getIsHighConsumptionNotifications() {
		return $this->m_boolIsHighConsumptionNotifications;
	}

	public function sqlIsHighConsumptionNotifications() {
		return ( true == isset( $this->m_boolIsHighConsumptionNotifications ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHighConsumptionNotifications ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsHighVacantConsumptionNotifications( $boolIsHighVacantConsumptionNotifications ) {
		$this->set( 'm_boolIsHighVacantConsumptionNotifications', CStrings::strToBool( $boolIsHighVacantConsumptionNotifications ) );
	}

	public function getIsHighVacantConsumptionNotifications() {
		return $this->m_boolIsHighVacantConsumptionNotifications;
	}

	public function sqlIsHighVacantConsumptionNotifications() {
		return ( true == isset( $this->m_boolIsHighVacantConsumptionNotifications ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHighVacantConsumptionNotifications ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInactiveNotifications( $boolIsInactiveNotifications ) {
		$this->set( 'm_boolIsInactiveNotifications', CStrings::strToBool( $boolIsInactiveNotifications ) );
	}

	public function getIsInactiveNotifications() {
		return $this->m_boolIsInactiveNotifications;
	}

	public function sqlIsInactiveNotifications() {
		return ( true == isset( $this->m_boolIsInactiveNotifications ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInactiveNotifications ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLowBatteryNotifications( $boolIsLowBatteryNotifications ) {
		$this->set( 'm_boolIsLowBatteryNotifications', CStrings::strToBool( $boolIsLowBatteryNotifications ) );
	}

	public function getIsLowBatteryNotifications() {
		return $this->m_boolIsLowBatteryNotifications;
	}

	public function sqlIsLowBatteryNotifications() {
		return ( true == isset( $this->m_boolIsLowBatteryNotifications ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLowBatteryNotifications ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLeakDetectionNotifications( $boolIsLeakDetectionNotifications ) {
		$this->set( 'm_boolIsLeakDetectionNotifications', CStrings::strToBool( $boolIsLeakDetectionNotifications ) );
	}

	public function getIsLeakDetectionNotifications() {
		return $this->m_boolIsLeakDetectionNotifications;
	}

	public function sqlIsLeakDetectionNotifications() {
		return ( true == isset( $this->m_boolIsLeakDetectionNotifications ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLeakDetectionNotifications ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMissingReadNotificationDays( $intMissingReadNotificationDays ) {
		$this->set( 'm_intMissingReadNotificationDays', CStrings::strToIntDef( $intMissingReadNotificationDays, NULL, false ) );
	}

	public function getMissingReadNotificationDays() {
		return $this->m_intMissingReadNotificationDays;
	}

	public function sqlMissingReadNotificationDays() {
		return ( true == isset( $this->m_intMissingReadNotificationDays ) ) ? ( string ) $this->m_intMissingReadNotificationDays : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_metering_setting_id, occupant_count, high_vacant_daily_limit, high_vacant_weekly_limit, high_consumption_daily_limit, high_consumption_weekly_limit, zero_read_notification_days, leak_percentage, is_high_consumption_notifications, is_high_vacant_consumption_notifications, is_inactive_notifications, is_low_battery_notifications, is_leak_detection_notifications, updated_by, updated_on, created_by, created_on, missing_read_notification_days )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyMeteringSettingId() . ', ' .
						$this->sqlOccupantCount() . ', ' .
						$this->sqlHighVacantDailyLimit() . ', ' .
						$this->sqlHighVacantWeeklyLimit() . ', ' .
						$this->sqlHighConsumptionDailyLimit() . ', ' .
						$this->sqlHighConsumptionWeeklyLimit() . ', ' .
						$this->sqlZeroReadNotificationDays() . ', ' .
						$this->sqlLeakPercentage() . ', ' .
						$this->sqlIsHighConsumptionNotifications() . ', ' .
						$this->sqlIsHighVacantConsumptionNotifications() . ', ' .
						$this->sqlIsInactiveNotifications() . ', ' .
						$this->sqlIsLowBatteryNotifications() . ', ' .
						$this->sqlIsLeakDetectionNotifications() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlMissingReadNotificationDays() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_metering_setting_id = ' . $this->sqlPropertyMeteringSettingId(). ',' ; } elseif( true == array_key_exists( 'PropertyMeteringSettingId', $this->getChangedColumns() ) ) { $strSql .= ' property_metering_setting_id = ' . $this->sqlPropertyMeteringSettingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount(). ',' ; } elseif( true == array_key_exists( 'OccupantCount', $this->getChangedColumns() ) ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' high_vacant_daily_limit = ' . $this->sqlHighVacantDailyLimit(). ',' ; } elseif( true == array_key_exists( 'HighVacantDailyLimit', $this->getChangedColumns() ) ) { $strSql .= ' high_vacant_daily_limit = ' . $this->sqlHighVacantDailyLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' high_vacant_weekly_limit = ' . $this->sqlHighVacantWeeklyLimit(). ',' ; } elseif( true == array_key_exists( 'HighVacantWeeklyLimit', $this->getChangedColumns() ) ) { $strSql .= ' high_vacant_weekly_limit = ' . $this->sqlHighVacantWeeklyLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' high_consumption_daily_limit = ' . $this->sqlHighConsumptionDailyLimit(). ',' ; } elseif( true == array_key_exists( 'HighConsumptionDailyLimit', $this->getChangedColumns() ) ) { $strSql .= ' high_consumption_daily_limit = ' . $this->sqlHighConsumptionDailyLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' high_consumption_weekly_limit = ' . $this->sqlHighConsumptionWeeklyLimit(). ',' ; } elseif( true == array_key_exists( 'HighConsumptionWeeklyLimit', $this->getChangedColumns() ) ) { $strSql .= ' high_consumption_weekly_limit = ' . $this->sqlHighConsumptionWeeklyLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' zero_read_notification_days = ' . $this->sqlZeroReadNotificationDays(). ',' ; } elseif( true == array_key_exists( 'ZeroReadNotificationDays', $this->getChangedColumns() ) ) { $strSql .= ' zero_read_notification_days = ' . $this->sqlZeroReadNotificationDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leak_percentage = ' . $this->sqlLeakPercentage(). ',' ; } elseif( true == array_key_exists( 'LeakPercentage', $this->getChangedColumns() ) ) { $strSql .= ' leak_percentage = ' . $this->sqlLeakPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_high_consumption_notifications = ' . $this->sqlIsHighConsumptionNotifications(). ',' ; } elseif( true == array_key_exists( 'IsHighConsumptionNotifications', $this->getChangedColumns() ) ) { $strSql .= ' is_high_consumption_notifications = ' . $this->sqlIsHighConsumptionNotifications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_high_vacant_consumption_notifications = ' . $this->sqlIsHighVacantConsumptionNotifications(). ',' ; } elseif( true == array_key_exists( 'IsHighVacantConsumptionNotifications', $this->getChangedColumns() ) ) { $strSql .= ' is_high_vacant_consumption_notifications = ' . $this->sqlIsHighVacantConsumptionNotifications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_inactive_notifications = ' . $this->sqlIsInactiveNotifications(). ',' ; } elseif( true == array_key_exists( 'IsInactiveNotifications', $this->getChangedColumns() ) ) { $strSql .= ' is_inactive_notifications = ' . $this->sqlIsInactiveNotifications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_low_battery_notifications = ' . $this->sqlIsLowBatteryNotifications(). ',' ; } elseif( true == array_key_exists( 'IsLowBatteryNotifications', $this->getChangedColumns() ) ) { $strSql .= ' is_low_battery_notifications = ' . $this->sqlIsLowBatteryNotifications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_leak_detection_notifications = ' . $this->sqlIsLeakDetectionNotifications(). ',' ; } elseif( true == array_key_exists( 'IsLeakDetectionNotifications', $this->getChangedColumns() ) ) { $strSql .= ' is_leak_detection_notifications = ' . $this->sqlIsLeakDetectionNotifications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' missing_read_notification_days = ' . $this->sqlMissingReadNotificationDays(). ',' ; } elseif( true == array_key_exists( 'MissingReadNotificationDays', $this->getChangedColumns() ) ) { $strSql .= ' missing_read_notification_days = ' . $this->sqlMissingReadNotificationDays() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_metering_setting_id' => $this->getPropertyMeteringSettingId(),
			'occupant_count' => $this->getOccupantCount(),
			'high_vacant_daily_limit' => $this->getHighVacantDailyLimit(),
			'high_vacant_weekly_limit' => $this->getHighVacantWeeklyLimit(),
			'high_consumption_daily_limit' => $this->getHighConsumptionDailyLimit(),
			'high_consumption_weekly_limit' => $this->getHighConsumptionWeeklyLimit(),
			'zero_read_notification_days' => $this->getZeroReadNotificationDays(),
			'leak_percentage' => $this->getLeakPercentage(),
			'is_high_consumption_notifications' => $this->getIsHighConsumptionNotifications(),
			'is_high_vacant_consumption_notifications' => $this->getIsHighVacantConsumptionNotifications(),
			'is_inactive_notifications' => $this->getIsInactiveNotifications(),
			'is_low_battery_notifications' => $this->getIsLowBatteryNotifications(),
			'is_leak_detection_notifications' => $this->getIsLeakDetectionNotifications(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'missing_read_notification_days' => $this->getMissingReadNotificationDays()
		);
	}

}
?>