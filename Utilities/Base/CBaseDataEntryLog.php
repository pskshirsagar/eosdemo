<?php

class CBaseDataEntryLog extends CEosSingularBase {

	const TABLE_NAME = 'public.data_entry_logs';

	protected $m_intId;
	protected $m_intUtilityBillId;
	protected $m_intUserId;
	protected $m_intReferenceId;
	protected $m_strKey;
	protected $m_strValue;
	protected $m_strErrorDatetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intImplementationUtilityBillId;
	protected $m_intDataEntryAuditId;
	protected $m_strCorrectValue;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( $arrValues['key'] ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( $arrValues['key'] );
		if( isset( $arrValues['value'] ) && $boolDirectSet ) $this->set( 'm_strValue', trim( $arrValues['value'] ) ); elseif( isset( $arrValues['value'] ) ) $this->setValue( $arrValues['value'] );
		if( isset( $arrValues['error_datetime'] ) && $boolDirectSet ) $this->set( 'm_strErrorDatetime', trim( $arrValues['error_datetime'] ) ); elseif( isset( $arrValues['error_datetime'] ) ) $this->setErrorDatetime( $arrValues['error_datetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['implementation_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationUtilityBillId', trim( $arrValues['implementation_utility_bill_id'] ) ); elseif( isset( $arrValues['implementation_utility_bill_id'] ) ) $this->setImplementationUtilityBillId( $arrValues['implementation_utility_bill_id'] );
		if( isset( $arrValues['data_entry_audit_id'] ) && $boolDirectSet ) $this->set( 'm_intDataEntryAuditId', trim( $arrValues['data_entry_audit_id'] ) ); elseif( isset( $arrValues['data_entry_audit_id'] ) ) $this->setDataEntryAuditId( $arrValues['data_entry_audit_id'] );
		if( isset( $arrValues['correct_value'] ) && $boolDirectSet ) $this->set( 'm_strCorrectValue', trim( $arrValues['correct_value'] ) ); elseif( isset( $arrValues['correct_value'] ) ) $this->setCorrectValue( $arrValues['correct_value'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 80, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strKey ) : '\'' . addslashes( $this->m_strKey ) . '\'' ) : 'NULL';
	}

	public function setValue( $strValue ) {
		$this->set( 'm_strValue', CStrings::strTrimDef( $strValue, -1, NULL, true ) );
	}

	public function getValue() {
		return $this->m_strValue;
	}

	public function sqlValue() {
		return ( true == isset( $this->m_strValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strValue ) : '\'' . addslashes( $this->m_strValue ) . '\'' ) : 'NULL';
	}

	public function setErrorDatetime( $strErrorDatetime ) {
		$this->set( 'm_strErrorDatetime', CStrings::strTrimDef( $strErrorDatetime, -1, NULL, true ) );
	}

	public function getErrorDatetime() {
		return $this->m_strErrorDatetime;
	}

	public function sqlErrorDatetime() {
		return ( true == isset( $this->m_strErrorDatetime ) ) ? '\'' . $this->m_strErrorDatetime . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setImplementationUtilityBillId( $intImplementationUtilityBillId ) {
		$this->set( 'm_intImplementationUtilityBillId', CStrings::strToIntDef( $intImplementationUtilityBillId, NULL, false ) );
	}

	public function getImplementationUtilityBillId() {
		return $this->m_intImplementationUtilityBillId;
	}

	public function sqlImplementationUtilityBillId() {
		return ( true == isset( $this->m_intImplementationUtilityBillId ) ) ? ( string ) $this->m_intImplementationUtilityBillId : 'NULL';
	}

	public function setDataEntryAuditId( $intDataEntryAuditId ) {
		$this->set( 'm_intDataEntryAuditId', CStrings::strToIntDef( $intDataEntryAuditId, NULL, false ) );
	}

	public function getDataEntryAuditId() {
		return $this->m_intDataEntryAuditId;
	}

	public function sqlDataEntryAuditId() {
		return ( true == isset( $this->m_intDataEntryAuditId ) ) ? ( string ) $this->m_intDataEntryAuditId : 'NULL';
	}

	public function setCorrectValue( $strCorrectValue ) {
		$this->set( 'm_strCorrectValue', CStrings::strTrimDef( $strCorrectValue, -1, NULL, true ) );
	}

	public function getCorrectValue() {
		return $this->m_strCorrectValue;
	}

	public function sqlCorrectValue() {
		return ( true == isset( $this->m_strCorrectValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCorrectValue ) : '\'' . addslashes( $this->m_strCorrectValue ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_bill_id, user_id, reference_id, key, value, error_datetime, created_by, created_on, implementation_utility_bill_id, data_entry_audit_id, correct_value )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlUserId() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlKey() . ', ' .
						$this->sqlValue() . ', ' .
						$this->sqlErrorDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlImplementationUtilityBillId() . ', ' .
						$this->sqlDataEntryAuditId() . ', ' .
						$this->sqlCorrectValue() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId(). ',' ; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey(). ',' ; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value = ' . $this->sqlValue(). ',' ; } elseif( true == array_key_exists( 'Value', $this->getChangedColumns() ) ) { $strSql .= ' value = ' . $this->sqlValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_datetime = ' . $this->sqlErrorDatetime(). ',' ; } elseif( true == array_key_exists( 'ErrorDatetime', $this->getChangedColumns() ) ) { $strSql .= ' error_datetime = ' . $this->sqlErrorDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_utility_bill_id = ' . $this->sqlImplementationUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'ImplementationUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_utility_bill_id = ' . $this->sqlImplementationUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_entry_audit_id = ' . $this->sqlDataEntryAuditId(). ',' ; } elseif( true == array_key_exists( 'DataEntryAuditId', $this->getChangedColumns() ) ) { $strSql .= ' data_entry_audit_id = ' . $this->sqlDataEntryAuditId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' correct_value = ' . $this->sqlCorrectValue() ; } elseif( true == array_key_exists( 'CorrectValue', $this->getChangedColumns() ) ) { $strSql .= ' correct_value = ' . $this->sqlCorrectValue() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'user_id' => $this->getUserId(),
			'reference_id' => $this->getReferenceId(),
			'key' => $this->getKey(),
			'value' => $this->getValue(),
			'error_datetime' => $this->getErrorDatetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'implementation_utility_bill_id' => $this->getImplementationUtilityBillId(),
			'data_entry_audit_id' => $this->getDataEntryAuditId(),
			'correct_value' => $this->getCorrectValue()
		);
	}

}
?>