<?php

class CBaseDutyPermission extends CEosSingularBase {

	const TABLE_NAME = 'public.duty_permissions';

	protected $m_intId;
	protected $m_intDutyTypeId;
	protected $m_intControllerId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['duty_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDutyTypeId', trim( $arrValues['duty_type_id'] ) ); elseif( isset( $arrValues['duty_type_id'] ) ) $this->setDutyTypeId( $arrValues['duty_type_id'] );
		if( isset( $arrValues['controller_id'] ) && $boolDirectSet ) $this->set( 'm_intControllerId', trim( $arrValues['controller_id'] ) ); elseif( isset( $arrValues['controller_id'] ) ) $this->setControllerId( $arrValues['controller_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDutyTypeId( $intDutyTypeId ) {
		$this->set( 'm_intDutyTypeId', CStrings::strToIntDef( $intDutyTypeId, NULL, false ) );
	}

	public function getDutyTypeId() {
		return $this->m_intDutyTypeId;
	}

	public function sqlDutyTypeId() {
		return ( true == isset( $this->m_intDutyTypeId ) ) ? ( string ) $this->m_intDutyTypeId : 'NULL';
	}

	public function setControllerId( $intControllerId ) {
		$this->set( 'm_intControllerId', CStrings::strToIntDef( $intControllerId, NULL, false ) );
	}

	public function getControllerId() {
		return $this->m_intControllerId;
	}

	public function sqlControllerId() {
		return ( true == isset( $this->m_intControllerId ) ) ? ( string ) $this->m_intControllerId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, duty_type_id, controller_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDutyTypeId() . ', ' .
 						$this->sqlControllerId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duty_type_id = ' . $this->sqlDutyTypeId() . ','; } elseif( true == array_key_exists( 'DutyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' duty_type_id = ' . $this->sqlDutyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' controller_id = ' . $this->sqlControllerId() . ','; } elseif( true == array_key_exists( 'ControllerId', $this->getChangedColumns() ) ) { $strSql .= ' controller_id = ' . $this->sqlControllerId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'duty_type_id' => $this->getDutyTypeId(),
			'controller_id' => $this->getControllerId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>