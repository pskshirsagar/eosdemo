<?php

class CBaseEnergyStarMetric extends CEosSingularBase {

	const TABLE_NAME = 'public.energy_star_metrics';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intEnergyStarCustomerPropertyId;
	protected $m_fltScore;
	protected $m_fltSiteTotal;
	protected $m_fltSourceTotal;
	protected $m_fltSiteIntensity;
	protected $m_strDesignMetrics;
	protected $m_strDetailMetrics;
	protected $m_strNoScoreReason;
	protected $m_strLastSyncOn;
	protected $m_boolIsDisabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['energy_star_customer_property_id'] ) && $boolDirectSet ) $this->set( 'm_intEnergyStarCustomerPropertyId', trim( $arrValues['energy_star_customer_property_id'] ) ); elseif( isset( $arrValues['energy_star_customer_property_id'] ) ) $this->setEnergyStarCustomerPropertyId( $arrValues['energy_star_customer_property_id'] );
		if( isset( $arrValues['score'] ) && $boolDirectSet ) $this->set( 'm_fltScore', trim( $arrValues['score'] ) ); elseif( isset( $arrValues['score'] ) ) $this->setScore( $arrValues['score'] );
		if( isset( $arrValues['site_total'] ) && $boolDirectSet ) $this->set( 'm_fltSiteTotal', trim( $arrValues['site_total'] ) ); elseif( isset( $arrValues['site_total'] ) ) $this->setSiteTotal( $arrValues['site_total'] );
		if( isset( $arrValues['source_total'] ) && $boolDirectSet ) $this->set( 'm_fltSourceTotal', trim( $arrValues['source_total'] ) ); elseif( isset( $arrValues['source_total'] ) ) $this->setSourceTotal( $arrValues['source_total'] );
		if( isset( $arrValues['site_intensity'] ) && $boolDirectSet ) $this->set( 'm_fltSiteIntensity', trim( $arrValues['site_intensity'] ) ); elseif( isset( $arrValues['site_intensity'] ) ) $this->setSiteIntensity( $arrValues['site_intensity'] );
		if( isset( $arrValues['design_metrics'] ) && $boolDirectSet ) $this->set( 'm_strDesignMetrics', trim( stripcslashes( $arrValues['design_metrics'] ) ) ); elseif( isset( $arrValues['design_metrics'] ) ) $this->setDesignMetrics( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['design_metrics'] ) : $arrValues['design_metrics'] );
		if( isset( $arrValues['detail_metrics'] ) && $boolDirectSet ) $this->set( 'm_strDetailMetrics', trim( stripcslashes( $arrValues['detail_metrics'] ) ) ); elseif( isset( $arrValues['detail_metrics'] ) ) $this->setDetailMetrics( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['detail_metrics'] ) : $arrValues['detail_metrics'] );
		if( isset( $arrValues['no_score_reason'] ) && $boolDirectSet ) $this->set( 'm_strNoScoreReason', trim( stripcslashes( $arrValues['no_score_reason'] ) ) ); elseif( isset( $arrValues['no_score_reason'] ) ) $this->setNoScoreReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['no_score_reason'] ) : $arrValues['no_score_reason'] );
		if( isset( $arrValues['last_sync_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSyncOn', trim( $arrValues['last_sync_on'] ) ); elseif( isset( $arrValues['last_sync_on'] ) ) $this->setLastSyncOn( $arrValues['last_sync_on'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setEnergyStarCustomerPropertyId( $intEnergyStarCustomerPropertyId ) {
		$this->set( 'm_intEnergyStarCustomerPropertyId', CStrings::strToIntDef( $intEnergyStarCustomerPropertyId, NULL, false ) );
	}

	public function getEnergyStarCustomerPropertyId() {
		return $this->m_intEnergyStarCustomerPropertyId;
	}

	public function sqlEnergyStarCustomerPropertyId() {
		return ( true == isset( $this->m_intEnergyStarCustomerPropertyId ) ) ? ( string ) $this->m_intEnergyStarCustomerPropertyId : 'NULL';
	}

	public function setScore( $fltScore ) {
		$this->set( 'm_fltScore', CStrings::strToFloatDef( $fltScore, NULL, false, 2 ) );
	}

	public function getScore() {
		return $this->m_fltScore;
	}

	public function sqlScore() {
		return ( true == isset( $this->m_fltScore ) ) ? ( string ) $this->m_fltScore : 'NULL';
	}

	public function setSiteTotal( $fltSiteTotal ) {
		$this->set( 'm_fltSiteTotal', CStrings::strToFloatDef( $fltSiteTotal, NULL, false, 2 ) );
	}

	public function getSiteTotal() {
		return $this->m_fltSiteTotal;
	}

	public function sqlSiteTotal() {
		return ( true == isset( $this->m_fltSiteTotal ) ) ? ( string ) $this->m_fltSiteTotal : 'NULL';
	}

	public function setSourceTotal( $fltSourceTotal ) {
		$this->set( 'm_fltSourceTotal', CStrings::strToFloatDef( $fltSourceTotal, NULL, false, 2 ) );
	}

	public function getSourceTotal() {
		return $this->m_fltSourceTotal;
	}

	public function sqlSourceTotal() {
		return ( true == isset( $this->m_fltSourceTotal ) ) ? ( string ) $this->m_fltSourceTotal : 'NULL';
	}

	public function setSiteIntensity( $fltSiteIntensity ) {
		$this->set( 'm_fltSiteIntensity', CStrings::strToFloatDef( $fltSiteIntensity, NULL, false, 2 ) );
	}

	public function getSiteIntensity() {
		return $this->m_fltSiteIntensity;
	}

	public function sqlSiteIntensity() {
		return ( true == isset( $this->m_fltSiteIntensity ) ) ? ( string ) $this->m_fltSiteIntensity : 'NULL';
	}

	public function setDesignMetrics( $strDesignMetrics ) {
		$this->set( 'm_strDesignMetrics', CStrings::strTrimDef( $strDesignMetrics, -1, NULL, true ) );
	}

	public function getDesignMetrics() {
		return $this->m_strDesignMetrics;
	}

	public function sqlDesignMetrics() {
		return ( true == isset( $this->m_strDesignMetrics ) ) ? '\'' . addslashes( $this->m_strDesignMetrics ) . '\'' : 'NULL';
	}

	public function setDetailMetrics( $strDetailMetrics ) {
		$this->set( 'm_strDetailMetrics', CStrings::strTrimDef( $strDetailMetrics, -1, NULL, true ) );
	}

	public function getDetailMetrics() {
		return $this->m_strDetailMetrics;
	}

	public function sqlDetailMetrics() {
		return ( true == isset( $this->m_strDetailMetrics ) ) ? '\'' . addslashes( $this->m_strDetailMetrics ) . '\'' : 'NULL';
	}

	public function setNoScoreReason( $strNoScoreReason ) {
		$this->set( 'm_strNoScoreReason', CStrings::strTrimDef( $strNoScoreReason, -1, NULL, true ) );
	}

	public function getNoScoreReason() {
		return $this->m_strNoScoreReason;
	}

	public function sqlNoScoreReason() {
		return ( true == isset( $this->m_strNoScoreReason ) ) ? '\'' . addslashes( $this->m_strNoScoreReason ) . '\'' : 'NULL';
	}

	public function setLastSyncOn( $strLastSyncOn ) {
		$this->set( 'm_strLastSyncOn', CStrings::strTrimDef( $strLastSyncOn, -1, NULL, true ) );
	}

	public function getLastSyncOn() {
		return $this->m_strLastSyncOn;
	}

	public function sqlLastSyncOn() {
		return ( true == isset( $this->m_strLastSyncOn ) ) ? '\'' . $this->m_strLastSyncOn . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, energy_star_customer_property_id, score, site_total, source_total, site_intensity, design_metrics, detail_metrics, no_score_reason, last_sync_on, is_disabled, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlEnergyStarCustomerPropertyId() . ', ' .
 						$this->sqlScore() . ', ' .
 						$this->sqlSiteTotal() . ', ' .
 						$this->sqlSourceTotal() . ', ' .
 						$this->sqlSiteIntensity() . ', ' .
 						$this->sqlDesignMetrics() . ', ' .
 						$this->sqlDetailMetrics() . ', ' .
 						$this->sqlNoScoreReason() . ', ' .
 						$this->sqlLastSyncOn() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' energy_star_customer_property_id = ' . $this->sqlEnergyStarCustomerPropertyId() . ','; } elseif( true == array_key_exists( 'EnergyStarCustomerPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' energy_star_customer_property_id = ' . $this->sqlEnergyStarCustomerPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' score = ' . $this->sqlScore() . ','; } elseif( true == array_key_exists( 'Score', $this->getChangedColumns() ) ) { $strSql .= ' score = ' . $this->sqlScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' site_total = ' . $this->sqlSiteTotal() . ','; } elseif( true == array_key_exists( 'SiteTotal', $this->getChangedColumns() ) ) { $strSql .= ' site_total = ' . $this->sqlSiteTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_total = ' . $this->sqlSourceTotal() . ','; } elseif( true == array_key_exists( 'SourceTotal', $this->getChangedColumns() ) ) { $strSql .= ' source_total = ' . $this->sqlSourceTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' site_intensity = ' . $this->sqlSiteIntensity() . ','; } elseif( true == array_key_exists( 'SiteIntensity', $this->getChangedColumns() ) ) { $strSql .= ' site_intensity = ' . $this->sqlSiteIntensity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' design_metrics = ' . $this->sqlDesignMetrics() . ','; } elseif( true == array_key_exists( 'DesignMetrics', $this->getChangedColumns() ) ) { $strSql .= ' design_metrics = ' . $this->sqlDesignMetrics() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' detail_metrics = ' . $this->sqlDetailMetrics() . ','; } elseif( true == array_key_exists( 'DetailMetrics', $this->getChangedColumns() ) ) { $strSql .= ' detail_metrics = ' . $this->sqlDetailMetrics() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' no_score_reason = ' . $this->sqlNoScoreReason() . ','; } elseif( true == array_key_exists( 'NoScoreReason', $this->getChangedColumns() ) ) { $strSql .= ' no_score_reason = ' . $this->sqlNoScoreReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn() . ','; } elseif( true == array_key_exists( 'LastSyncOn', $this->getChangedColumns() ) ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'energy_star_customer_property_id' => $this->getEnergyStarCustomerPropertyId(),
			'score' => $this->getScore(),
			'site_total' => $this->getSiteTotal(),
			'source_total' => $this->getSourceTotal(),
			'site_intensity' => $this->getSiteIntensity(),
			'design_metrics' => $this->getDesignMetrics(),
			'detail_metrics' => $this->getDetailMetrics(),
			'no_score_reason' => $this->getNoScoreReason(),
			'last_sync_on' => $this->getLastSyncOn(),
			'is_disabled' => $this->getIsDisabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>