<?php

class CBaseWorker extends CEosSingularBase {

	const TABLE_NAME = 'public.workers';

	protected $m_intId;
	protected $m_intVendorId;
	protected $m_intEmployeeStatusTypeId;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strBirthDate;
	protected $m_strTaxNumberEncrypted;
	protected $m_strTitle;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_strEmailAddress;
	protected $m_intPhoneNumber1TypeId;
	protected $m_strPhoneNumber1;
	protected $m_strPhoneNumber1Extension;
	protected $m_intPhoneNumber2TypeId;
	protected $m_strPhoneNumber2;
	protected $m_strPhoneNumber2Extension;
	protected $m_strDateStarted;
	protected $m_strDateTerminated;
	protected $m_strPasswordEncrypted;
	protected $m_strLastLogin;
	protected $m_intLoginAttemptCount;
	protected $m_strLastLoginAttemptOn;
	protected $m_boolIsAdministrator;
	protected $m_boolDisableLogin;
	protected $m_boolIsEmployee;
	protected $m_boolIsOnSite;
	protected $m_boolIsClientContact;
	protected $m_boolIsReceiveActivityDigestEmail;
	protected $m_boolIsPrincipal;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intEmployeeStatusTypeId = '1';
		$this->m_boolIsAdministrator = false;
		$this->m_boolDisableLogin = false;
		$this->m_boolIsEmployee = true;
		$this->m_boolIsOnSite = false;
		$this->m_boolIsClientContact = false;
		$this->m_boolIsReceiveActivityDigestEmail = false;
		$this->m_boolIsPrincipal = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['employee_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeStatusTypeId', trim( $arrValues['employee_status_type_id'] ) ); elseif( isset( $arrValues['employee_status_type_id'] ) ) $this->setEmployeeStatusTypeId( $arrValues['employee_status_type_id'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( stripcslashes( $arrValues['name_first'] ) ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first'] ) : $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( stripcslashes( $arrValues['name_middle'] ) ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_middle'] ) : $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( stripcslashes( $arrValues['name_last'] ) ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last'] ) : $arrValues['name_last'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( $arrValues['birth_date'] ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( $arrValues['birth_date'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( stripcslashes( $arrValues['tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_encrypted'] ) : $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine2', trim( stripcslashes( $arrValues['street_line2'] ) ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
		if( isset( $arrValues['street_line3'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine3', trim( stripcslashes( $arrValues['street_line3'] ) ) ); elseif( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line3'] ) : $arrValues['street_line3'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( stripcslashes( $arrValues['province'] ) ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['phone_number1_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPhoneNumber1TypeId', trim( $arrValues['phone_number1_type_id'] ) ); elseif( isset( $arrValues['phone_number1_type_id'] ) ) $this->setPhoneNumber1TypeId( $arrValues['phone_number1_type_id'] );
		if( isset( $arrValues['phone_number1'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber1', trim( stripcslashes( $arrValues['phone_number1'] ) ) ); elseif( isset( $arrValues['phone_number1'] ) ) $this->setPhoneNumber1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number1'] ) : $arrValues['phone_number1'] );
		if( isset( $arrValues['phone_number1_extension'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber1Extension', trim( stripcslashes( $arrValues['phone_number1_extension'] ) ) ); elseif( isset( $arrValues['phone_number1_extension'] ) ) $this->setPhoneNumber1Extension( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number1_extension'] ) : $arrValues['phone_number1_extension'] );
		if( isset( $arrValues['phone_number2_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPhoneNumber2TypeId', trim( $arrValues['phone_number2_type_id'] ) ); elseif( isset( $arrValues['phone_number2_type_id'] ) ) $this->setPhoneNumber2TypeId( $arrValues['phone_number2_type_id'] );
		if( isset( $arrValues['phone_number2'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber2', trim( stripcslashes( $arrValues['phone_number2'] ) ) ); elseif( isset( $arrValues['phone_number2'] ) ) $this->setPhoneNumber2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number2'] ) : $arrValues['phone_number2'] );
		if( isset( $arrValues['phone_number2_extension'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber2Extension', trim( stripcslashes( $arrValues['phone_number2_extension'] ) ) ); elseif( isset( $arrValues['phone_number2_extension'] ) ) $this->setPhoneNumber2Extension( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number2_extension'] ) : $arrValues['phone_number2_extension'] );
		if( isset( $arrValues['date_started'] ) && $boolDirectSet ) $this->set( 'm_strDateStarted', trim( $arrValues['date_started'] ) ); elseif( isset( $arrValues['date_started'] ) ) $this->setDateStarted( $arrValues['date_started'] );
		if( isset( $arrValues['date_terminated'] ) && $boolDirectSet ) $this->set( 'm_strDateTerminated', trim( $arrValues['date_terminated'] ) ); elseif( isset( $arrValues['date_terminated'] ) ) $this->setDateTerminated( $arrValues['date_terminated'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['last_login'] ) && $boolDirectSet ) $this->set( 'm_strLastLogin', trim( $arrValues['last_login'] ) ); elseif( isset( $arrValues['last_login'] ) ) $this->setLastLogin( $arrValues['last_login'] );
		if( isset( $arrValues['login_attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intLoginAttemptCount', trim( $arrValues['login_attempt_count'] ) ); elseif( isset( $arrValues['login_attempt_count'] ) ) $this->setLoginAttemptCount( $arrValues['login_attempt_count'] );
		if( isset( $arrValues['last_login_attempt_on'] ) && $boolDirectSet ) $this->set( 'm_strLastLoginAttemptOn', trim( $arrValues['last_login_attempt_on'] ) ); elseif( isset( $arrValues['last_login_attempt_on'] ) ) $this->setLastLoginAttemptOn( $arrValues['last_login_attempt_on'] );
		if( isset( $arrValues['is_administrator'] ) && $boolDirectSet ) $this->set( 'm_boolIsAdministrator', trim( stripcslashes( $arrValues['is_administrator'] ) ) ); elseif( isset( $arrValues['is_administrator'] ) ) $this->setIsAdministrator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_administrator'] ) : $arrValues['is_administrator'] );
		if( isset( $arrValues['disable_login'] ) && $boolDirectSet ) $this->set( 'm_boolDisableLogin', trim( stripcslashes( $arrValues['disable_login'] ) ) ); elseif( isset( $arrValues['disable_login'] ) ) $this->setDisableLogin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['disable_login'] ) : $arrValues['disable_login'] );
		if( isset( $arrValues['is_employee'] ) && $boolDirectSet ) $this->set( 'm_boolIsEmployee', trim( stripcslashes( $arrValues['is_employee'] ) ) ); elseif( isset( $arrValues['is_employee'] ) ) $this->setIsEmployee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_employee'] ) : $arrValues['is_employee'] );
		if( isset( $arrValues['is_on_site'] ) && $boolDirectSet ) $this->set( 'm_boolIsOnSite', trim( stripcslashes( $arrValues['is_on_site'] ) ) ); elseif( isset( $arrValues['is_on_site'] ) ) $this->setIsOnSite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_on_site'] ) : $arrValues['is_on_site'] );
		if( isset( $arrValues['is_client_contact'] ) && $boolDirectSet ) $this->set( 'm_boolIsClientContact', trim( stripcslashes( $arrValues['is_client_contact'] ) ) ); elseif( isset( $arrValues['is_client_contact'] ) ) $this->setIsClientContact( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_client_contact'] ) : $arrValues['is_client_contact'] );
		if( isset( $arrValues['is_receive_activity_digest_email'] ) && $boolDirectSet ) $this->set( 'm_boolIsReceiveActivityDigestEmail', trim( stripcslashes( $arrValues['is_receive_activity_digest_email'] ) ) ); elseif( isset( $arrValues['is_receive_activity_digest_email'] ) ) $this->setIsReceiveActivityDigestEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_receive_activity_digest_email'] ) : $arrValues['is_receive_activity_digest_email'] );
		if( isset( $arrValues['is_principal'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrincipal', trim( stripcslashes( $arrValues['is_principal'] ) ) ); elseif( isset( $arrValues['is_principal'] ) ) $this->setIsPrincipal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_principal'] ) : $arrValues['is_principal'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setEmployeeStatusTypeId( $intEmployeeStatusTypeId ) {
		$this->set( 'm_intEmployeeStatusTypeId', CStrings::strToIntDef( $intEmployeeStatusTypeId, NULL, false ) );
	}

	public function getEmployeeStatusTypeId() {
		return $this->m_intEmployeeStatusTypeId;
	}

	public function sqlEmployeeStatusTypeId() {
		return ( true == isset( $this->m_intEmployeeStatusTypeId ) ) ? ( string ) $this->m_intEmployeeStatusTypeId : '1';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? '\'' . addslashes( $this->m_strNameFirst ) . '\'' : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? '\'' . addslashes( $this->m_strNameMiddle ) . '\'' : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? '\'' . addslashes( $this->m_strNameLast ) . '\'' : 'NULL';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, -1, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . $this->m_strBirthDate . '\'' : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->set( 'm_strStreetLine1', CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->set( 'm_strStreetLine2', CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) );
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->set( 'm_strStreetLine3', CStrings::strTrimDef( $strStreetLine3, 100, NULL, true ) );
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function sqlStreetLine3() {
		return ( true == isset( $this->m_strStreetLine3 ) ) ? '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setPhoneNumber1TypeId( $intPhoneNumber1TypeId ) {
		$this->set( 'm_intPhoneNumber1TypeId', CStrings::strToIntDef( $intPhoneNumber1TypeId, NULL, false ) );
	}

	public function getPhoneNumber1TypeId() {
		return $this->m_intPhoneNumber1TypeId;
	}

	public function sqlPhoneNumber1TypeId() {
		return ( true == isset( $this->m_intPhoneNumber1TypeId ) ) ? ( string ) $this->m_intPhoneNumber1TypeId : 'NULL';
	}

	public function setPhoneNumber1( $strPhoneNumber1 ) {
		$this->set( 'm_strPhoneNumber1', CStrings::strTrimDef( $strPhoneNumber1, 30, NULL, true ) );
	}

	public function getPhoneNumber1() {
		return $this->m_strPhoneNumber1;
	}

	public function sqlPhoneNumber1() {
		return ( true == isset( $this->m_strPhoneNumber1 ) ) ? '\'' . addslashes( $this->m_strPhoneNumber1 ) . '\'' : 'NULL';
	}

	public function setPhoneNumber1Extension( $strPhoneNumber1Extension ) {
		$this->set( 'm_strPhoneNumber1Extension', CStrings::strTrimDef( $strPhoneNumber1Extension, 20, NULL, true ) );
	}

	public function getPhoneNumber1Extension() {
		return $this->m_strPhoneNumber1Extension;
	}

	public function sqlPhoneNumber1Extension() {
		return ( true == isset( $this->m_strPhoneNumber1Extension ) ) ? '\'' . addslashes( $this->m_strPhoneNumber1Extension ) . '\'' : 'NULL';
	}

	public function setPhoneNumber2TypeId( $intPhoneNumber2TypeId ) {
		$this->set( 'm_intPhoneNumber2TypeId', CStrings::strToIntDef( $intPhoneNumber2TypeId, NULL, false ) );
	}

	public function getPhoneNumber2TypeId() {
		return $this->m_intPhoneNumber2TypeId;
	}

	public function sqlPhoneNumber2TypeId() {
		return ( true == isset( $this->m_intPhoneNumber2TypeId ) ) ? ( string ) $this->m_intPhoneNumber2TypeId : 'NULL';
	}

	public function setPhoneNumber2( $strPhoneNumber2 ) {
		$this->set( 'm_strPhoneNumber2', CStrings::strTrimDef( $strPhoneNumber2, 30, NULL, true ) );
	}

	public function getPhoneNumber2() {
		return $this->m_strPhoneNumber2;
	}

	public function sqlPhoneNumber2() {
		return ( true == isset( $this->m_strPhoneNumber2 ) ) ? '\'' . addslashes( $this->m_strPhoneNumber2 ) . '\'' : 'NULL';
	}

	public function setPhoneNumber2Extension( $strPhoneNumber2Extension ) {
		$this->set( 'm_strPhoneNumber2Extension', CStrings::strTrimDef( $strPhoneNumber2Extension, 20, NULL, true ) );
	}

	public function getPhoneNumber2Extension() {
		return $this->m_strPhoneNumber2Extension;
	}

	public function sqlPhoneNumber2Extension() {
		return ( true == isset( $this->m_strPhoneNumber2Extension ) ) ? '\'' . addslashes( $this->m_strPhoneNumber2Extension ) . '\'' : 'NULL';
	}

	public function setDateStarted( $strDateStarted ) {
		$this->set( 'm_strDateStarted', CStrings::strTrimDef( $strDateStarted, -1, NULL, true ) );
	}

	public function getDateStarted() {
		return $this->m_strDateStarted;
	}

	public function sqlDateStarted() {
		return ( true == isset( $this->m_strDateStarted ) ) ? '\'' . $this->m_strDateStarted . '\'' : 'NULL';
	}

	public function setDateTerminated( $strDateTerminated ) {
		$this->set( 'm_strDateTerminated', CStrings::strTrimDef( $strDateTerminated, -1, NULL, true ) );
	}

	public function getDateTerminated() {
		return $this->m_strDateTerminated;
	}

	public function sqlDateTerminated() {
		return ( true == isset( $this->m_strDateTerminated ) ) ? '\'' . $this->m_strDateTerminated . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setLastLogin( $strLastLogin ) {
		$this->set( 'm_strLastLogin', CStrings::strTrimDef( $strLastLogin, -1, NULL, true ) );
	}

	public function getLastLogin() {
		return $this->m_strLastLogin;
	}

	public function sqlLastLogin() {
		return ( true == isset( $this->m_strLastLogin ) ) ? '\'' . $this->m_strLastLogin . '\'' : 'NULL';
	}

	public function setLoginAttemptCount( $intLoginAttemptCount ) {
		$this->set( 'm_intLoginAttemptCount', CStrings::strToIntDef( $intLoginAttemptCount, NULL, false ) );
	}

	public function getLoginAttemptCount() {
		return $this->m_intLoginAttemptCount;
	}

	public function sqlLoginAttemptCount() {
		return ( true == isset( $this->m_intLoginAttemptCount ) ) ? ( string ) $this->m_intLoginAttemptCount : 'NULL';
	}

	public function setLastLoginAttemptOn( $strLastLoginAttemptOn ) {
		$this->set( 'm_strLastLoginAttemptOn', CStrings::strTrimDef( $strLastLoginAttemptOn, -1, NULL, true ) );
	}

	public function getLastLoginAttemptOn() {
		return $this->m_strLastLoginAttemptOn;
	}

	public function sqlLastLoginAttemptOn() {
		return ( true == isset( $this->m_strLastLoginAttemptOn ) ) ? '\'' . $this->m_strLastLoginAttemptOn . '\'' : 'NULL';
	}

	public function setIsAdministrator( $boolIsAdministrator ) {
		$this->set( 'm_boolIsAdministrator', CStrings::strToBool( $boolIsAdministrator ) );
	}

	public function getIsAdministrator() {
		return $this->m_boolIsAdministrator;
	}

	public function sqlIsAdministrator() {
		return ( true == isset( $this->m_boolIsAdministrator ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAdministrator ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDisableLogin( $boolDisableLogin ) {
		$this->set( 'm_boolDisableLogin', CStrings::strToBool( $boolDisableLogin ) );
	}

	public function getDisableLogin() {
		return $this->m_boolDisableLogin;
	}

	public function sqlDisableLogin() {
		return ( true == isset( $this->m_boolDisableLogin ) ) ? '\'' . ( true == ( bool ) $this->m_boolDisableLogin ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsEmployee( $boolIsEmployee ) {
		$this->set( 'm_boolIsEmployee', CStrings::strToBool( $boolIsEmployee ) );
	}

	public function getIsEmployee() {
		return $this->m_boolIsEmployee;
	}

	public function sqlIsEmployee() {
		return ( true == isset( $this->m_boolIsEmployee ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEmployee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOnSite( $boolIsOnSite ) {
		$this->set( 'm_boolIsOnSite', CStrings::strToBool( $boolIsOnSite ) );
	}

	public function getIsOnSite() {
		return $this->m_boolIsOnSite;
	}

	public function sqlIsOnSite() {
		return ( true == isset( $this->m_boolIsOnSite ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOnSite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsClientContact( $boolIsClientContact ) {
		$this->set( 'm_boolIsClientContact', CStrings::strToBool( $boolIsClientContact ) );
	}

	public function getIsClientContact() {
		return $this->m_boolIsClientContact;
	}

	public function sqlIsClientContact() {
		return ( true == isset( $this->m_boolIsClientContact ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClientContact ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReceiveActivityDigestEmail( $boolIsReceiveActivityDigestEmail ) {
		$this->set( 'm_boolIsReceiveActivityDigestEmail', CStrings::strToBool( $boolIsReceiveActivityDigestEmail ) );
	}

	public function getIsReceiveActivityDigestEmail() {
		return $this->m_boolIsReceiveActivityDigestEmail;
	}

	public function sqlIsReceiveActivityDigestEmail() {
		return ( true == isset( $this->m_boolIsReceiveActivityDigestEmail ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReceiveActivityDigestEmail ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPrincipal( $boolIsPrincipal ) {
		$this->set( 'm_boolIsPrincipal', CStrings::strToBool( $boolIsPrincipal ) );
	}

	public function getIsPrincipal() {
		return $this->m_boolIsPrincipal;
	}

	public function sqlIsPrincipal() {
		return ( true == isset( $this->m_boolIsPrincipal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrincipal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_id, employee_status_type_id, name_first, name_middle, name_last, birth_date, tax_number_encrypted, title, street_line1, street_line2, street_line3, city, state_code, province, postal_code, country_code, email_address, phone_number1_type_id, phone_number1, phone_number1_extension, phone_number2_type_id, phone_number2, phone_number2_extension, date_started, date_terminated, password_encrypted, last_login, login_attempt_count, last_login_attempt_on, is_administrator, disable_login, is_employee, is_on_site, is_client_contact, is_receive_activity_digest_email, is_principal, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlVendorId() . ', ' .
 						$this->sqlEmployeeStatusTypeId() . ', ' .
 						$this->sqlNameFirst() . ', ' .
 						$this->sqlNameMiddle() . ', ' .
 						$this->sqlNameLast() . ', ' .
 						$this->sqlBirthDate() . ', ' .
 						$this->sqlTaxNumberEncrypted() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlStreetLine1() . ', ' .
 						$this->sqlStreetLine2() . ', ' .
 						$this->sqlStreetLine3() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlProvince() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlCountryCode() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlPhoneNumber1TypeId() . ', ' .
 						$this->sqlPhoneNumber1() . ', ' .
 						$this->sqlPhoneNumber1Extension() . ', ' .
 						$this->sqlPhoneNumber2TypeId() . ', ' .
 						$this->sqlPhoneNumber2() . ', ' .
 						$this->sqlPhoneNumber2Extension() . ', ' .
 						$this->sqlDateStarted() . ', ' .
 						$this->sqlDateTerminated() . ', ' .
 						$this->sqlPasswordEncrypted() . ', ' .
 						$this->sqlLastLogin() . ', ' .
 						$this->sqlLoginAttemptCount() . ', ' .
 						$this->sqlLastLoginAttemptOn() . ', ' .
 						$this->sqlIsAdministrator() . ', ' .
 						$this->sqlDisableLogin() . ', ' .
 						$this->sqlIsEmployee() . ', ' .
 						$this->sqlIsOnSite() . ', ' .
 						$this->sqlIsClientContact() . ', ' .
 						$this->sqlIsReceiveActivityDigestEmail() . ', ' .
 						$this->sqlIsPrincipal() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_status_type_id = ' . $this->sqlEmployeeStatusTypeId() . ','; } elseif( true == array_key_exists( 'EmployeeStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_status_type_id = ' . $this->sqlEmployeeStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; } elseif( true == array_key_exists( 'BirthDate', $this->getChangedColumns() ) ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; } elseif( true == array_key_exists( 'StreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number1_type_id = ' . $this->sqlPhoneNumber1TypeId() . ','; } elseif( true == array_key_exists( 'PhoneNumber1TypeId', $this->getChangedColumns() ) ) { $strSql .= ' phone_number1_type_id = ' . $this->sqlPhoneNumber1TypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number1 = ' . $this->sqlPhoneNumber1() . ','; } elseif( true == array_key_exists( 'PhoneNumber1', $this->getChangedColumns() ) ) { $strSql .= ' phone_number1 = ' . $this->sqlPhoneNumber1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number1_extension = ' . $this->sqlPhoneNumber1Extension() . ','; } elseif( true == array_key_exists( 'PhoneNumber1Extension', $this->getChangedColumns() ) ) { $strSql .= ' phone_number1_extension = ' . $this->sqlPhoneNumber1Extension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number2_type_id = ' . $this->sqlPhoneNumber2TypeId() . ','; } elseif( true == array_key_exists( 'PhoneNumber2TypeId', $this->getChangedColumns() ) ) { $strSql .= ' phone_number2_type_id = ' . $this->sqlPhoneNumber2TypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number2 = ' . $this->sqlPhoneNumber2() . ','; } elseif( true == array_key_exists( 'PhoneNumber2', $this->getChangedColumns() ) ) { $strSql .= ' phone_number2 = ' . $this->sqlPhoneNumber2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number2_extension = ' . $this->sqlPhoneNumber2Extension() . ','; } elseif( true == array_key_exists( 'PhoneNumber2Extension', $this->getChangedColumns() ) ) { $strSql .= ' phone_number2_extension = ' . $this->sqlPhoneNumber2Extension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; } elseif( true == array_key_exists( 'DateStarted', $this->getChangedColumns() ) ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; } elseif( true == array_key_exists( 'DateTerminated', $this->getChangedColumns() ) ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login = ' . $this->sqlLastLogin() . ','; } elseif( true == array_key_exists( 'LastLogin', $this->getChangedColumns() ) ) { $strSql .= ' last_login = ' . $this->sqlLastLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount() . ','; } elseif( true == array_key_exists( 'LoginAttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn() . ','; } elseif( true == array_key_exists( 'LastLoginAttemptOn', $this->getChangedColumns() ) ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_administrator = ' . $this->sqlIsAdministrator() . ','; } elseif( true == array_key_exists( 'IsAdministrator', $this->getChangedColumns() ) ) { $strSql .= ' is_administrator = ' . $this->sqlIsAdministrator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disable_login = ' . $this->sqlDisableLogin() . ','; } elseif( true == array_key_exists( 'DisableLogin', $this->getChangedColumns() ) ) { $strSql .= ' disable_login = ' . $this->sqlDisableLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_employee = ' . $this->sqlIsEmployee() . ','; } elseif( true == array_key_exists( 'IsEmployee', $this->getChangedColumns() ) ) { $strSql .= ' is_employee = ' . $this->sqlIsEmployee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_on_site = ' . $this->sqlIsOnSite() . ','; } elseif( true == array_key_exists( 'IsOnSite', $this->getChangedColumns() ) ) { $strSql .= ' is_on_site = ' . $this->sqlIsOnSite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_contact = ' . $this->sqlIsClientContact() . ','; } elseif( true == array_key_exists( 'IsClientContact', $this->getChangedColumns() ) ) { $strSql .= ' is_client_contact = ' . $this->sqlIsClientContact() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_receive_activity_digest_email = ' . $this->sqlIsReceiveActivityDigestEmail() . ','; } elseif( true == array_key_exists( 'IsReceiveActivityDigestEmail', $this->getChangedColumns() ) ) { $strSql .= ' is_receive_activity_digest_email = ' . $this->sqlIsReceiveActivityDigestEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_principal = ' . $this->sqlIsPrincipal() . ','; } elseif( true == array_key_exists( 'IsPrincipal', $this->getChangedColumns() ) ) { $strSql .= ' is_principal = ' . $this->sqlIsPrincipal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_id' => $this->getVendorId(),
			'employee_status_type_id' => $this->getEmployeeStatusTypeId(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'birth_date' => $this->getBirthDate(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'title' => $this->getTitle(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'street_line3' => $this->getStreetLine3(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'province' => $this->getProvince(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'email_address' => $this->getEmailAddress(),
			'phone_number1_type_id' => $this->getPhoneNumber1TypeId(),
			'phone_number1' => $this->getPhoneNumber1(),
			'phone_number1_extension' => $this->getPhoneNumber1Extension(),
			'phone_number2_type_id' => $this->getPhoneNumber2TypeId(),
			'phone_number2' => $this->getPhoneNumber2(),
			'phone_number2_extension' => $this->getPhoneNumber2Extension(),
			'date_started' => $this->getDateStarted(),
			'date_terminated' => $this->getDateTerminated(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'last_login' => $this->getLastLogin(),
			'login_attempt_count' => $this->getLoginAttemptCount(),
			'last_login_attempt_on' => $this->getLastLoginAttemptOn(),
			'is_administrator' => $this->getIsAdministrator(),
			'disable_login' => $this->getDisableLogin(),
			'is_employee' => $this->getIsEmployee(),
			'is_on_site' => $this->getIsOnSite(),
			'is_client_contact' => $this->getIsClientContact(),
			'is_receive_activity_digest_email' => $this->getIsReceiveActivityDigestEmail(),
			'is_principal' => $this->getIsPrincipal(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>