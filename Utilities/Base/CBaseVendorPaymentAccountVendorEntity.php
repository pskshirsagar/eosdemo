<?php

class CBaseVendorPaymentAccountVendorEntity extends CEosSingularBase {

	const TABLE_NAME = 'public.vendor_payment_account_vendor_entities';

	protected $m_intId;
	protected $m_intVendorPaymentAccountId;
	protected $m_intVendorEntityId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_payment_account_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorPaymentAccountId', trim( $arrValues['vendor_payment_account_id'] ) ); elseif( isset( $arrValues['vendor_payment_account_id'] ) ) $this->setVendorPaymentAccountId( $arrValues['vendor_payment_account_id'] );
		if( isset( $arrValues['vendor_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorEntityId', trim( $arrValues['vendor_entity_id'] ) ); elseif( isset( $arrValues['vendor_entity_id'] ) ) $this->setVendorEntityId( $arrValues['vendor_entity_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorPaymentAccountId( $intVendorPaymentAccountId ) {
		$this->set( 'm_intVendorPaymentAccountId', CStrings::strToIntDef( $intVendorPaymentAccountId, NULL, false ) );
	}

	public function getVendorPaymentAccountId() {
		return $this->m_intVendorPaymentAccountId;
	}

	public function sqlVendorPaymentAccountId() {
		return ( true == isset( $this->m_intVendorPaymentAccountId ) ) ? ( string ) $this->m_intVendorPaymentAccountId : 'NULL';
	}

	public function setVendorEntityId( $intVendorEntityId ) {
		$this->set( 'm_intVendorEntityId', CStrings::strToIntDef( $intVendorEntityId, NULL, false ) );
	}

	public function getVendorEntityId() {
		return $this->m_intVendorEntityId;
	}

	public function sqlVendorEntityId() {
		return ( true == isset( $this->m_intVendorEntityId ) ) ? ( string ) $this->m_intVendorEntityId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_payment_account_id, vendor_entity_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlVendorPaymentAccountId() . ', ' .
 						$this->sqlVendorEntityId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_payment_account_id = ' . $this->sqlVendorPaymentAccountId() . ','; } elseif( true == array_key_exists( 'VendorPaymentAccountId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_payment_account_id = ' . $this->sqlVendorPaymentAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_entity_id = ' . $this->sqlVendorEntityId() . ','; } elseif( true == array_key_exists( 'VendorEntityId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_entity_id = ' . $this->sqlVendorEntityId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_payment_account_id' => $this->getVendorPaymentAccountId(),
			'vendor_entity_id' => $this->getVendorEntityId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>