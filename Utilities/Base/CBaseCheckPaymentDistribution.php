<?php

class CBaseCheckPaymentDistribution extends CEosSingularBase {

	const TABLE_NAME = 'public.check_payment_distributions';

	protected $m_intId;
	protected $m_intCheckPaymentDistributionStepId;
	protected $m_strPrintedOn;
	protected $m_strMailedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['check_payment_distribution_step_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckPaymentDistributionStepId', trim( $arrValues['check_payment_distribution_step_id'] ) ); elseif( isset( $arrValues['check_payment_distribution_step_id'] ) ) $this->setCheckPaymentDistributionStepId( $arrValues['check_payment_distribution_step_id'] );
		if( isset( $arrValues['printed_on'] ) && $boolDirectSet ) $this->set( 'm_strPrintedOn', trim( $arrValues['printed_on'] ) ); elseif( isset( $arrValues['printed_on'] ) ) $this->setPrintedOn( $arrValues['printed_on'] );
		if( isset( $arrValues['mailed_on'] ) && $boolDirectSet ) $this->set( 'm_strMailedOn', trim( $arrValues['mailed_on'] ) ); elseif( isset( $arrValues['mailed_on'] ) ) $this->setMailedOn( $arrValues['mailed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCheckPaymentDistributionStepId( $intCheckPaymentDistributionStepId ) {
		$this->set( 'm_intCheckPaymentDistributionStepId', CStrings::strToIntDef( $intCheckPaymentDistributionStepId, NULL, false ) );
	}

	public function getCheckPaymentDistributionStepId() {
		return $this->m_intCheckPaymentDistributionStepId;
	}

	public function sqlCheckPaymentDistributionStepId() {
		return ( true == isset( $this->m_intCheckPaymentDistributionStepId ) ) ? ( string ) $this->m_intCheckPaymentDistributionStepId : 'NULL';
	}

	public function setPrintedOn( $strPrintedOn ) {
		$this->set( 'm_strPrintedOn', CStrings::strTrimDef( $strPrintedOn, -1, NULL, true ) );
	}

	public function getPrintedOn() {
		return $this->m_strPrintedOn;
	}

	public function sqlPrintedOn() {
		return ( true == isset( $this->m_strPrintedOn ) ) ? '\'' . $this->m_strPrintedOn . '\'' : 'NULL';
	}

	public function setMailedOn( $strMailedOn ) {
		$this->set( 'm_strMailedOn', CStrings::strTrimDef( $strMailedOn, -1, NULL, true ) );
	}

	public function getMailedOn() {
		return $this->m_strMailedOn;
	}

	public function sqlMailedOn() {
		return ( true == isset( $this->m_strMailedOn ) ) ? '\'' . $this->m_strMailedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, check_payment_distribution_step_id, printed_on, mailed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCheckPaymentDistributionStepId() . ', ' .
 						$this->sqlPrintedOn() . ', ' .
 						$this->sqlMailedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_payment_distribution_step_id = ' . $this->sqlCheckPaymentDistributionStepId() . ','; } elseif( true == array_key_exists( 'CheckPaymentDistributionStepId', $this->getChangedColumns() ) ) { $strSql .= ' check_payment_distribution_step_id = ' . $this->sqlCheckPaymentDistributionStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' printed_on = ' . $this->sqlPrintedOn() . ','; } elseif( true == array_key_exists( 'PrintedOn', $this->getChangedColumns() ) ) { $strSql .= ' printed_on = ' . $this->sqlPrintedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailed_on = ' . $this->sqlMailedOn() . ','; } elseif( true == array_key_exists( 'MailedOn', $this->getChangedColumns() ) ) { $strSql .= ' mailed_on = ' . $this->sqlMailedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'check_payment_distribution_step_id' => $this->getCheckPaymentDistributionStepId(),
			'printed_on' => $this->getPrintedOn(),
			'mailed_on' => $this->getMailedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>