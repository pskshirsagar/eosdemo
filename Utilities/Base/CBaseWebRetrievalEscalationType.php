<?php

class CBaseWebRetrievalEscalationType extends CEosSingularBase {

	const TABLE_NAME = 'public.web_retrieval_escalation_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intOrderNum;
	protected $m_intParentWebRetrievalEscalationTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['parent_web_retrieval_escalation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intParentWebRetrievalEscalationTypeId', trim( $arrValues['parent_web_retrieval_escalation_type_id'] ) ); elseif( isset( $arrValues['parent_web_retrieval_escalation_type_id'] ) ) $this->setParentWebRetrievalEscalationTypeId( $arrValues['parent_web_retrieval_escalation_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setParentWebRetrievalEscalationTypeId( $intParentWebRetrievalEscalationTypeId ) {
		$this->set( 'm_intParentWebRetrievalEscalationTypeId', CStrings::strToIntDef( $intParentWebRetrievalEscalationTypeId, NULL, false ) );
	}

	public function getParentWebRetrievalEscalationTypeId() {
		return $this->m_intParentWebRetrievalEscalationTypeId;
	}

	public function sqlParentWebRetrievalEscalationTypeId() {
		return ( true == isset( $this->m_intParentWebRetrievalEscalationTypeId ) ) ? ( string ) $this->m_intParentWebRetrievalEscalationTypeId : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'order_num' => $this->getOrderNum(),
			'parent_web_retrieval_escalation_type_id' => $this->getParentWebRetrievalEscalationTypeId()
		);
	}

}
?>