<?php

class CBaseUtilityBillAccountMeter extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_account_meters';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillAccountId;
	protected $m_intUtilityConsumptionTypeId;
	protected $m_intUtilityBillAccountMeterId;
	protected $m_intExternalId;
	protected $m_intEnergyStarCustomerBuildingId;
	protected $m_strMeterNumber;
	protected $m_boolIsPropertyMeter;
	protected $m_boolIsDemandMeter;
	protected $m_boolIsTimeOfUseMeter;
	protected $m_strLastSyncOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolCaptureUsageTotalOnly;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPropertyMeter = false;
		$this->m_boolIsDemandMeter = false;
		$this->m_boolIsTimeOfUseMeter = false;
		$this->m_boolCaptureUsageTotalOnly = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['utility_consumption_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityConsumptionTypeId', trim( $arrValues['utility_consumption_type_id'] ) ); elseif( isset( $arrValues['utility_consumption_type_id'] ) ) $this->setUtilityConsumptionTypeId( $arrValues['utility_consumption_type_id'] );
		if( isset( $arrValues['utility_bill_account_meter_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountMeterId', trim( $arrValues['utility_bill_account_meter_id'] ) ); elseif( isset( $arrValues['utility_bill_account_meter_id'] ) ) $this->setUtilityBillAccountMeterId( $arrValues['utility_bill_account_meter_id'] );
		if( isset( $arrValues['external_id'] ) && $boolDirectSet ) $this->set( 'm_intExternalId', trim( $arrValues['external_id'] ) ); elseif( isset( $arrValues['external_id'] ) ) $this->setExternalId( $arrValues['external_id'] );
		if( isset( $arrValues['energy_star_customer_building_id'] ) && $boolDirectSet ) $this->set( 'm_intEnergyStarCustomerBuildingId', trim( $arrValues['energy_star_customer_building_id'] ) ); elseif( isset( $arrValues['energy_star_customer_building_id'] ) ) $this->setEnergyStarCustomerBuildingId( $arrValues['energy_star_customer_building_id'] );
		if( isset( $arrValues['meter_number'] ) && $boolDirectSet ) $this->set( 'm_strMeterNumber', trim( $arrValues['meter_number'] ) ); elseif( isset( $arrValues['meter_number'] ) ) $this->setMeterNumber( $arrValues['meter_number'] );
		if( isset( $arrValues['is_property_meter'] ) && $boolDirectSet ) $this->set( 'm_boolIsPropertyMeter', trim( stripcslashes( $arrValues['is_property_meter'] ) ) ); elseif( isset( $arrValues['is_property_meter'] ) ) $this->setIsPropertyMeter( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_property_meter'] ) : $arrValues['is_property_meter'] );
		if( isset( $arrValues['is_demand_meter'] ) && $boolDirectSet ) $this->set( 'm_boolIsDemandMeter', trim( stripcslashes( $arrValues['is_demand_meter'] ) ) ); elseif( isset( $arrValues['is_demand_meter'] ) ) $this->setIsDemandMeter( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_demand_meter'] ) : $arrValues['is_demand_meter'] );
		if( isset( $arrValues['is_time_of_use_meter'] ) && $boolDirectSet ) $this->set( 'm_boolIsTimeOfUseMeter', trim( stripcslashes( $arrValues['is_time_of_use_meter'] ) ) ); elseif( isset( $arrValues['is_time_of_use_meter'] ) ) $this->setIsTimeOfUseMeter( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_time_of_use_meter'] ) : $arrValues['is_time_of_use_meter'] );
		if( isset( $arrValues['last_sync_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSyncOn', trim( $arrValues['last_sync_on'] ) ); elseif( isset( $arrValues['last_sync_on'] ) ) $this->setLastSyncOn( $arrValues['last_sync_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['capture_usage_total_only'] ) && $boolDirectSet ) $this->set( 'm_boolCaptureUsageTotalOnly', trim( stripcslashes( $arrValues['capture_usage_total_only'] ) ) ); elseif( isset( $arrValues['capture_usage_total_only'] ) ) $this->setCaptureUsageTotalOnly( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['capture_usage_total_only'] ) : $arrValues['capture_usage_total_only'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setUtilityConsumptionTypeId( $intUtilityConsumptionTypeId ) {
		$this->set( 'm_intUtilityConsumptionTypeId', CStrings::strToIntDef( $intUtilityConsumptionTypeId, NULL, false ) );
	}

	public function getUtilityConsumptionTypeId() {
		return $this->m_intUtilityConsumptionTypeId;
	}

	public function sqlUtilityConsumptionTypeId() {
		return ( true == isset( $this->m_intUtilityConsumptionTypeId ) ) ? ( string ) $this->m_intUtilityConsumptionTypeId : 'NULL';
	}

	public function setUtilityBillAccountMeterId( $intUtilityBillAccountMeterId ) {
		$this->set( 'm_intUtilityBillAccountMeterId', CStrings::strToIntDef( $intUtilityBillAccountMeterId, NULL, false ) );
	}

	public function getUtilityBillAccountMeterId() {
		return $this->m_intUtilityBillAccountMeterId;
	}

	public function sqlUtilityBillAccountMeterId() {
		return ( true == isset( $this->m_intUtilityBillAccountMeterId ) ) ? ( string ) $this->m_intUtilityBillAccountMeterId : 'NULL';
	}

	public function setExternalId( $intExternalId ) {
		$this->set( 'm_intExternalId', CStrings::strToIntDef( $intExternalId, NULL, false ) );
	}

	public function getExternalId() {
		return $this->m_intExternalId;
	}

	public function sqlExternalId() {
		return ( true == isset( $this->m_intExternalId ) ) ? ( string ) $this->m_intExternalId : 'NULL';
	}

	public function setEnergyStarCustomerBuildingId( $intEnergyStarCustomerBuildingId ) {
		$this->set( 'm_intEnergyStarCustomerBuildingId', CStrings::strToIntDef( $intEnergyStarCustomerBuildingId, NULL, false ) );
	}

	public function getEnergyStarCustomerBuildingId() {
		return $this->m_intEnergyStarCustomerBuildingId;
	}

	public function sqlEnergyStarCustomerBuildingId() {
		return ( true == isset( $this->m_intEnergyStarCustomerBuildingId ) ) ? ( string ) $this->m_intEnergyStarCustomerBuildingId : 'NULL';
	}

	public function setMeterNumber( $strMeterNumber ) {
		$this->set( 'm_strMeterNumber', CStrings::strTrimDef( $strMeterNumber, 240, NULL, true ) );
	}

	public function getMeterNumber() {
		return $this->m_strMeterNumber;
	}

	public function sqlMeterNumber() {
		return ( true == isset( $this->m_strMeterNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMeterNumber ) : '\'' . addslashes( $this->m_strMeterNumber ) . '\'' ) : 'NULL';
	}

	public function setIsPropertyMeter( $boolIsPropertyMeter ) {
		$this->set( 'm_boolIsPropertyMeter', CStrings::strToBool( $boolIsPropertyMeter ) );
	}

	public function getIsPropertyMeter() {
		return $this->m_boolIsPropertyMeter;
	}

	public function sqlIsPropertyMeter() {
		return ( true == isset( $this->m_boolIsPropertyMeter ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPropertyMeter ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDemandMeter( $boolIsDemandMeter ) {
		$this->set( 'm_boolIsDemandMeter', CStrings::strToBool( $boolIsDemandMeter ) );
	}

	public function getIsDemandMeter() {
		return $this->m_boolIsDemandMeter;
	}

	public function sqlIsDemandMeter() {
		return ( true == isset( $this->m_boolIsDemandMeter ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDemandMeter ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTimeOfUseMeter( $boolIsTimeOfUseMeter ) {
		$this->set( 'm_boolIsTimeOfUseMeter', CStrings::strToBool( $boolIsTimeOfUseMeter ) );
	}

	public function getIsTimeOfUseMeter() {
		return $this->m_boolIsTimeOfUseMeter;
	}

	public function sqlIsTimeOfUseMeter() {
		return ( true == isset( $this->m_boolIsTimeOfUseMeter ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTimeOfUseMeter ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLastSyncOn( $strLastSyncOn ) {
		$this->set( 'm_strLastSyncOn', CStrings::strTrimDef( $strLastSyncOn, -1, NULL, true ) );
	}

	public function getLastSyncOn() {
		return $this->m_strLastSyncOn;
	}

	public function sqlLastSyncOn() {
		return ( true == isset( $this->m_strLastSyncOn ) ) ? '\'' . $this->m_strLastSyncOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCaptureUsageTotalOnly( $boolCaptureUsageTotalOnly ) {
		$this->set( 'm_boolCaptureUsageTotalOnly', CStrings::strToBool( $boolCaptureUsageTotalOnly ) );
	}

	public function getCaptureUsageTotalOnly() {
		return $this->m_boolCaptureUsageTotalOnly;
	}

	public function sqlCaptureUsageTotalOnly() {
		return ( true == isset( $this->m_boolCaptureUsageTotalOnly ) ) ? '\'' . ( true == ( bool ) $this->m_boolCaptureUsageTotalOnly ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_account_id, utility_consumption_type_id, utility_bill_account_meter_id, external_id, energy_star_customer_building_id, meter_number, is_property_meter, is_demand_meter, is_time_of_use_meter, last_sync_on, updated_by, updated_on, created_by, created_on, capture_usage_total_only )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityBillAccountId() . ', ' .
						$this->sqlUtilityConsumptionTypeId() . ', ' .
						$this->sqlUtilityBillAccountMeterId() . ', ' .
						$this->sqlExternalId() . ', ' .
						$this->sqlEnergyStarCustomerBuildingId() . ', ' .
						$this->sqlMeterNumber() . ', ' .
						$this->sqlIsPropertyMeter() . ', ' .
						$this->sqlIsDemandMeter() . ', ' .
						$this->sqlIsTimeOfUseMeter() . ', ' .
						$this->sqlLastSyncOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCaptureUsageTotalOnly() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_consumption_type_id = ' . $this->sqlUtilityConsumptionTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityConsumptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_consumption_type_id = ' . $this->sqlUtilityConsumptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_meter_id = ' . $this->sqlUtilityBillAccountMeterId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountMeterId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_meter_id = ' . $this->sqlUtilityBillAccountMeterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_id = ' . $this->sqlExternalId(). ',' ; } elseif( true == array_key_exists( 'ExternalId', $this->getChangedColumns() ) ) { $strSql .= ' external_id = ' . $this->sqlExternalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' energy_star_customer_building_id = ' . $this->sqlEnergyStarCustomerBuildingId(). ',' ; } elseif( true == array_key_exists( 'EnergyStarCustomerBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' energy_star_customer_building_id = ' . $this->sqlEnergyStarCustomerBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_number = ' . $this->sqlMeterNumber(). ',' ; } elseif( true == array_key_exists( 'MeterNumber', $this->getChangedColumns() ) ) { $strSql .= ' meter_number = ' . $this->sqlMeterNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_property_meter = ' . $this->sqlIsPropertyMeter(). ',' ; } elseif( true == array_key_exists( 'IsPropertyMeter', $this->getChangedColumns() ) ) { $strSql .= ' is_property_meter = ' . $this->sqlIsPropertyMeter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_demand_meter = ' . $this->sqlIsDemandMeter(). ',' ; } elseif( true == array_key_exists( 'IsDemandMeter', $this->getChangedColumns() ) ) { $strSql .= ' is_demand_meter = ' . $this->sqlIsDemandMeter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_time_of_use_meter = ' . $this->sqlIsTimeOfUseMeter(). ',' ; } elseif( true == array_key_exists( 'IsTimeOfUseMeter', $this->getChangedColumns() ) ) { $strSql .= ' is_time_of_use_meter = ' . $this->sqlIsTimeOfUseMeter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn(). ',' ; } elseif( true == array_key_exists( 'LastSyncOn', $this->getChangedColumns() ) ) { $strSql .= ' last_sync_on = ' . $this->sqlLastSyncOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' capture_usage_total_only = ' . $this->sqlCaptureUsageTotalOnly(). ',' ; } elseif( true == array_key_exists( 'CaptureUsageTotalOnly', $this->getChangedColumns() ) ) { $strSql .= ' capture_usage_total_only = ' . $this->sqlCaptureUsageTotalOnly() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'utility_consumption_type_id' => $this->getUtilityConsumptionTypeId(),
			'utility_bill_account_meter_id' => $this->getUtilityBillAccountMeterId(),
			'external_id' => $this->getExternalId(),
			'energy_star_customer_building_id' => $this->getEnergyStarCustomerBuildingId(),
			'meter_number' => $this->getMeterNumber(),
			'is_property_meter' => $this->getIsPropertyMeter(),
			'is_demand_meter' => $this->getIsDemandMeter(),
			'is_time_of_use_meter' => $this->getIsTimeOfUseMeter(),
			'last_sync_on' => $this->getLastSyncOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'capture_usage_total_only' => $this->getCaptureUsageTotalOnly()
		);
	}

}
?>