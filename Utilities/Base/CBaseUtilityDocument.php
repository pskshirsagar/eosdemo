<?php

class CBaseUtilityDocument extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_documents';

	protected $m_intId;
	protected $m_intFileExtensionId;
	protected $m_strDocumentDatetime;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_boolIsDated;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUtilityDocumentTypeId;
	protected $m_intPageCount;
	protected $m_intOriginalUtilityDocumentId;
	protected $m_intUtilityBillBatchId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDated = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['file_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intFileExtensionId', trim( $arrValues['file_extension_id'] ) ); elseif( isset( $arrValues['file_extension_id'] ) ) $this->setFileExtensionId( $arrValues['file_extension_id'] );
		if( isset( $arrValues['document_datetime'] ) && $boolDirectSet ) $this->set( 'm_strDocumentDatetime', trim( $arrValues['document_datetime'] ) ); elseif( isset( $arrValues['document_datetime'] ) ) $this->setDocumentDatetime( $arrValues['document_datetime'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( $arrValues['file_name'] ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( $arrValues['file_path'] ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( $arrValues['file_path'] );
		if( isset( $arrValues['is_dated'] ) && $boolDirectSet ) $this->set( 'm_boolIsDated', trim( stripcslashes( $arrValues['is_dated'] ) ) ); elseif( isset( $arrValues['is_dated'] ) ) $this->setIsDated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_dated'] ) : $arrValues['is_dated'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['utility_document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDocumentTypeId', trim( $arrValues['utility_document_type_id'] ) ); elseif( isset( $arrValues['utility_document_type_id'] ) ) $this->setUtilityDocumentTypeId( $arrValues['utility_document_type_id'] );
		if( isset( $arrValues['page_count'] ) && $boolDirectSet ) $this->set( 'm_intPageCount', trim( $arrValues['page_count'] ) ); elseif( isset( $arrValues['page_count'] ) ) $this->setPageCount( $arrValues['page_count'] );
		if( isset( $arrValues['original_utility_document_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalUtilityDocumentId', trim( $arrValues['original_utility_document_id'] ) ); elseif( isset( $arrValues['original_utility_document_id'] ) ) $this->setOriginalUtilityDocumentId( $arrValues['original_utility_document_id'] );
		if( isset( $arrValues['utility_bill_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillBatchId', trim( $arrValues['utility_bill_batch_id'] ) ); elseif( isset( $arrValues['utility_bill_batch_id'] ) ) $this->setUtilityBillBatchId( $arrValues['utility_bill_batch_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setFileExtensionId( $intFileExtensionId ) {
		$this->set( 'm_intFileExtensionId', CStrings::strToIntDef( $intFileExtensionId, NULL, false ) );
	}

	public function getFileExtensionId() {
		return $this->m_intFileExtensionId;
	}

	public function sqlFileExtensionId() {
		return ( true == isset( $this->m_intFileExtensionId ) ) ? ( string ) $this->m_intFileExtensionId : 'NULL';
	}

	public function setDocumentDatetime( $strDocumentDatetime ) {
		$this->set( 'm_strDocumentDatetime', CStrings::strTrimDef( $strDocumentDatetime, -1, NULL, true ) );
	}

	public function getDocumentDatetime() {
		return $this->m_strDocumentDatetime;
	}

	public function sqlDocumentDatetime() {
		return ( true == isset( $this->m_strDocumentDatetime ) ) ? '\'' . $this->m_strDocumentDatetime . '\'' : 'NOW()';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 300, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileName ) : '\'' . addslashes( $this->m_strFileName ) . '\'' ) : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 300, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFilePath ) : '\'' . addslashes( $this->m_strFilePath ) . '\'' ) : 'NULL';
	}

	public function setIsDated( $boolIsDated ) {
		$this->set( 'm_boolIsDated', CStrings::strToBool( $boolIsDated ) );
	}

	public function getIsDated() {
		return $this->m_boolIsDated;
	}

	public function sqlIsDated() {
		return ( true == isset( $this->m_boolIsDated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUtilityDocumentTypeId( $intUtilityDocumentTypeId ) {
		$this->set( 'm_intUtilityDocumentTypeId', CStrings::strToIntDef( $intUtilityDocumentTypeId, NULL, false ) );
	}

	public function getUtilityDocumentTypeId() {
		return $this->m_intUtilityDocumentTypeId;
	}

	public function sqlUtilityDocumentTypeId() {
		return ( true == isset( $this->m_intUtilityDocumentTypeId ) ) ? ( string ) $this->m_intUtilityDocumentTypeId : 'NULL';
	}

	public function setPageCount( $intPageCount ) {
		$this->set( 'm_intPageCount', CStrings::strToIntDef( $intPageCount, NULL, false ) );
	}

	public function getPageCount() {
		return $this->m_intPageCount;
	}

	public function sqlPageCount() {
		return ( true == isset( $this->m_intPageCount ) ) ? ( string ) $this->m_intPageCount : 'NULL';
	}

	public function setOriginalUtilityDocumentId( $intOriginalUtilityDocumentId ) {
		$this->set( 'm_intOriginalUtilityDocumentId', CStrings::strToIntDef( $intOriginalUtilityDocumentId, NULL, false ) );
	}

	public function getOriginalUtilityDocumentId() {
		return $this->m_intOriginalUtilityDocumentId;
	}

	public function sqlOriginalUtilityDocumentId() {
		return ( true == isset( $this->m_intOriginalUtilityDocumentId ) ) ? ( string ) $this->m_intOriginalUtilityDocumentId : 'NULL';
	}

	public function setUtilityBillBatchId( $intUtilityBillBatchId ) {
		$this->set( 'm_intUtilityBillBatchId', CStrings::strToIntDef( $intUtilityBillBatchId, NULL, false ) );
	}

	public function getUtilityBillBatchId() {
		return $this->m_intUtilityBillBatchId;
	}

	public function sqlUtilityBillBatchId() {
		return ( true == isset( $this->m_intUtilityBillBatchId ) ) ? ( string ) $this->m_intUtilityBillBatchId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, file_extension_id, document_datetime, file_name, file_path, is_dated, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, utility_document_type_id, page_count, original_utility_document_id, utility_bill_batch_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlFileExtensionId() . ', ' .
						$this->sqlDocumentDatetime() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlFilePath() . ', ' .
						$this->sqlIsDated() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUtilityDocumentTypeId() . ', ' .
						$this->sqlPageCount() . ', ' .
						$this->sqlOriginalUtilityDocumentId() . ', ' .
						$this->sqlUtilityBillBatchId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId(). ',' ; } elseif( true == array_key_exists( 'FileExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_datetime = ' . $this->sqlDocumentDatetime(). ',' ; } elseif( true == array_key_exists( 'DocumentDatetime', $this->getChangedColumns() ) ) { $strSql .= ' document_datetime = ' . $this->sqlDocumentDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath(). ',' ; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_dated = ' . $this->sqlIsDated(). ',' ; } elseif( true == array_key_exists( 'IsDated', $this->getChangedColumns() ) ) { $strSql .= ' is_dated = ' . $this->sqlIsDated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_document_type_id = ' . $this->sqlUtilityDocumentTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityDocumentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_document_type_id = ' . $this->sqlUtilityDocumentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' page_count = ' . $this->sqlPageCount(). ',' ; } elseif( true == array_key_exists( 'PageCount', $this->getChangedColumns() ) ) { $strSql .= ' page_count = ' . $this->sqlPageCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_utility_document_id = ' . $this->sqlOriginalUtilityDocumentId(). ',' ; } elseif( true == array_key_exists( 'OriginalUtilityDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' original_utility_document_id = ' . $this->sqlOriginalUtilityDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_batch_id = ' . $this->sqlUtilityBillBatchId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_batch_id = ' . $this->sqlUtilityBillBatchId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'file_extension_id' => $this->getFileExtensionId(),
			'document_datetime' => $this->getDocumentDatetime(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'is_dated' => $this->getIsDated(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'utility_document_type_id' => $this->getUtilityDocumentTypeId(),
			'page_count' => $this->getPageCount(),
			'original_utility_document_id' => $this->getOriginalUtilityDocumentId(),
			'utility_bill_batch_id' => $this->getUtilityBillBatchId()
		);
	}

}
?>