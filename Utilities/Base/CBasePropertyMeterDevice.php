<?php

class CBasePropertyMeterDevice extends CEosSingularBase {

	const TABLE_NAME = 'public.property_meter_devices';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityTypeId;
	protected $m_intPropertyUnitId;
	protected $m_intMeterManufacturerId;
	protected $m_intMeterPartTypeId;
	protected $m_intRadioManufacturerId;
	protected $m_intRadioPartTypeId;
	protected $m_intOldPropertyMeterDeviceId;
	protected $m_strDeviceNumber;
	protected $m_strUnitNumber;
	protected $m_fltCountFactor;
	protected $m_boolIsReadBackwards;
	protected $m_strInstalledOn;
	protected $m_strLastRepairedOn;
	protected $m_strDeactivatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strRemotePrimaryKey;
	protected $m_intHighConsumptionDays;
	protected $m_intMissingReadDays;
	protected $m_intZeroConsumptionDays;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsReadBackwards = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeId', trim( $arrValues['utility_type_id'] ) ); elseif( isset( $arrValues['utility_type_id'] ) ) $this->setUtilityTypeId( $arrValues['utility_type_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['meter_manufacturer_id'] ) && $boolDirectSet ) $this->set( 'm_intMeterManufacturerId', trim( $arrValues['meter_manufacturer_id'] ) ); elseif( isset( $arrValues['meter_manufacturer_id'] ) ) $this->setMeterManufacturerId( $arrValues['meter_manufacturer_id'] );
		if( isset( $arrValues['meter_part_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMeterPartTypeId', trim( $arrValues['meter_part_type_id'] ) ); elseif( isset( $arrValues['meter_part_type_id'] ) ) $this->setMeterPartTypeId( $arrValues['meter_part_type_id'] );
		if( isset( $arrValues['radio_manufacturer_id'] ) && $boolDirectSet ) $this->set( 'm_intRadioManufacturerId', trim( $arrValues['radio_manufacturer_id'] ) ); elseif( isset( $arrValues['radio_manufacturer_id'] ) ) $this->setRadioManufacturerId( $arrValues['radio_manufacturer_id'] );
		if( isset( $arrValues['radio_part_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRadioPartTypeId', trim( $arrValues['radio_part_type_id'] ) ); elseif( isset( $arrValues['radio_part_type_id'] ) ) $this->setRadioPartTypeId( $arrValues['radio_part_type_id'] );
		if( isset( $arrValues['old_property_meter_device_id'] ) && $boolDirectSet ) $this->set( 'm_intOldPropertyMeterDeviceId', trim( $arrValues['old_property_meter_device_id'] ) ); elseif( isset( $arrValues['old_property_meter_device_id'] ) ) $this->setOldPropertyMeterDeviceId( $arrValues['old_property_meter_device_id'] );
		if( isset( $arrValues['device_number'] ) && $boolDirectSet ) $this->set( 'm_strDeviceNumber', trim( $arrValues['device_number'] ) ); elseif( isset( $arrValues['device_number'] ) ) $this->setDeviceNumber( $arrValues['device_number'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( $arrValues['unit_number'] ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( $arrValues['unit_number'] );
		if( isset( $arrValues['count_factor'] ) && $boolDirectSet ) $this->set( 'm_fltCountFactor', trim( $arrValues['count_factor'] ) ); elseif( isset( $arrValues['count_factor'] ) ) $this->setCountFactor( $arrValues['count_factor'] );
		if( isset( $arrValues['is_read_backwards'] ) && $boolDirectSet ) $this->set( 'm_boolIsReadBackwards', trim( stripcslashes( $arrValues['is_read_backwards'] ) ) ); elseif( isset( $arrValues['is_read_backwards'] ) ) $this->setIsReadBackwards( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_read_backwards'] ) : $arrValues['is_read_backwards'] );
		if( isset( $arrValues['installed_on'] ) && $boolDirectSet ) $this->set( 'm_strInstalledOn', trim( $arrValues['installed_on'] ) ); elseif( isset( $arrValues['installed_on'] ) ) $this->setInstalledOn( $arrValues['installed_on'] );
		if( isset( $arrValues['last_repaired_on'] ) && $boolDirectSet ) $this->set( 'm_strLastRepairedOn', trim( $arrValues['last_repaired_on'] ) ); elseif( isset( $arrValues['last_repaired_on'] ) ) $this->setLastRepairedOn( $arrValues['last_repaired_on'] );
		if( isset( $arrValues['deactivated_on'] ) && $boolDirectSet ) $this->set( 'm_strDeactivatedOn', trim( $arrValues['deactivated_on'] ) ); elseif( isset( $arrValues['deactivated_on'] ) ) $this->setDeactivatedOn( $arrValues['deactivated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['high_consumption_days'] ) && $boolDirectSet ) $this->set( 'm_intHighConsumptionDays', trim( $arrValues['high_consumption_days'] ) ); elseif( isset( $arrValues['high_consumption_days'] ) ) $this->setHighConsumptionDays( $arrValues['high_consumption_days'] );
		if( isset( $arrValues['missing_read_days'] ) && $boolDirectSet ) $this->set( 'm_intMissingReadDays', trim( $arrValues['missing_read_days'] ) ); elseif( isset( $arrValues['missing_read_days'] ) ) $this->setMissingReadDays( $arrValues['missing_read_days'] );
		if( isset( $arrValues['zero_consumption_days'] ) && $boolDirectSet ) $this->set( 'm_intZeroConsumptionDays', trim( $arrValues['zero_consumption_days'] ) ); elseif( isset( $arrValues['zero_consumption_days'] ) ) $this->setZeroConsumptionDays( $arrValues['zero_consumption_days'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->set( 'm_intUtilityTypeId', CStrings::strToIntDef( $intUtilityTypeId, NULL, false ) );
	}

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function sqlUtilityTypeId() {
		return ( true == isset( $this->m_intUtilityTypeId ) ) ? ( string ) $this->m_intUtilityTypeId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setMeterManufacturerId( $intMeterManufacturerId ) {
		$this->set( 'm_intMeterManufacturerId', CStrings::strToIntDef( $intMeterManufacturerId, NULL, false ) );
	}

	public function getMeterManufacturerId() {
		return $this->m_intMeterManufacturerId;
	}

	public function sqlMeterManufacturerId() {
		return ( true == isset( $this->m_intMeterManufacturerId ) ) ? ( string ) $this->m_intMeterManufacturerId : 'NULL';
	}

	public function setMeterPartTypeId( $intMeterPartTypeId ) {
		$this->set( 'm_intMeterPartTypeId', CStrings::strToIntDef( $intMeterPartTypeId, NULL, false ) );
	}

	public function getMeterPartTypeId() {
		return $this->m_intMeterPartTypeId;
	}

	public function sqlMeterPartTypeId() {
		return ( true == isset( $this->m_intMeterPartTypeId ) ) ? ( string ) $this->m_intMeterPartTypeId : 'NULL';
	}

	public function setRadioManufacturerId( $intRadioManufacturerId ) {
		$this->set( 'm_intRadioManufacturerId', CStrings::strToIntDef( $intRadioManufacturerId, NULL, false ) );
	}

	public function getRadioManufacturerId() {
		return $this->m_intRadioManufacturerId;
	}

	public function sqlRadioManufacturerId() {
		return ( true == isset( $this->m_intRadioManufacturerId ) ) ? ( string ) $this->m_intRadioManufacturerId : 'NULL';
	}

	public function setRadioPartTypeId( $intRadioPartTypeId ) {
		$this->set( 'm_intRadioPartTypeId', CStrings::strToIntDef( $intRadioPartTypeId, NULL, false ) );
	}

	public function getRadioPartTypeId() {
		return $this->m_intRadioPartTypeId;
	}

	public function sqlRadioPartTypeId() {
		return ( true == isset( $this->m_intRadioPartTypeId ) ) ? ( string ) $this->m_intRadioPartTypeId : 'NULL';
	}

	public function setOldPropertyMeterDeviceId( $intOldPropertyMeterDeviceId ) {
		$this->set( 'm_intOldPropertyMeterDeviceId', CStrings::strToIntDef( $intOldPropertyMeterDeviceId, NULL, false ) );
	}

	public function getOldPropertyMeterDeviceId() {
		return $this->m_intOldPropertyMeterDeviceId;
	}

	public function sqlOldPropertyMeterDeviceId() {
		return ( true == isset( $this->m_intOldPropertyMeterDeviceId ) ) ? ( string ) $this->m_intOldPropertyMeterDeviceId : 'NULL';
	}

	public function setDeviceNumber( $strDeviceNumber ) {
		$this->set( 'm_strDeviceNumber', CStrings::strTrimDef( $strDeviceNumber, 240, NULL, true ) );
	}

	public function getDeviceNumber() {
		return $this->m_strDeviceNumber;
	}

	public function sqlDeviceNumber() {
		return ( true == isset( $this->m_strDeviceNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDeviceNumber ) : '\'' . addslashes( $this->m_strDeviceNumber ) . '\'' ) : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUnitNumber ) : '\'' . addslashes( $this->m_strUnitNumber ) . '\'' ) : 'NULL';
	}

	public function setCountFactor( $fltCountFactor ) {
		$this->set( 'm_fltCountFactor', CStrings::strToFloatDef( $fltCountFactor, NULL, false, 3 ) );
	}

	public function getCountFactor() {
		return $this->m_fltCountFactor;
	}

	public function sqlCountFactor() {
		return ( true == isset( $this->m_fltCountFactor ) ) ? ( string ) $this->m_fltCountFactor : 'NULL';
	}

	public function setIsReadBackwards( $boolIsReadBackwards ) {
		$this->set( 'm_boolIsReadBackwards', CStrings::strToBool( $boolIsReadBackwards ) );
	}

	public function getIsReadBackwards() {
		return $this->m_boolIsReadBackwards;
	}

	public function sqlIsReadBackwards() {
		return ( true == isset( $this->m_boolIsReadBackwards ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReadBackwards ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInstalledOn( $strInstalledOn ) {
		$this->set( 'm_strInstalledOn', CStrings::strTrimDef( $strInstalledOn, -1, NULL, true ) );
	}

	public function getInstalledOn() {
		return $this->m_strInstalledOn;
	}

	public function sqlInstalledOn() {
		return ( true == isset( $this->m_strInstalledOn ) ) ? '\'' . $this->m_strInstalledOn . '\'' : 'NULL';
	}

	public function setLastRepairedOn( $strLastRepairedOn ) {
		$this->set( 'm_strLastRepairedOn', CStrings::strTrimDef( $strLastRepairedOn, -1, NULL, true ) );
	}

	public function getLastRepairedOn() {
		return $this->m_strLastRepairedOn;
	}

	public function sqlLastRepairedOn() {
		return ( true == isset( $this->m_strLastRepairedOn ) ) ? '\'' . $this->m_strLastRepairedOn . '\'' : 'NULL';
	}

	public function setDeactivatedOn( $strDeactivatedOn ) {
		$this->set( 'm_strDeactivatedOn', CStrings::strTrimDef( $strDeactivatedOn, -1, NULL, true ) );
	}

	public function getDeactivatedOn() {
		return $this->m_strDeactivatedOn;
	}

	public function sqlDeactivatedOn() {
		return ( true == isset( $this->m_strDeactivatedOn ) ) ? '\'' . $this->m_strDeactivatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 240, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setHighConsumptionDays( $intHighConsumptionDays ) {
		$this->set( 'm_intHighConsumptionDays', CStrings::strToIntDef( $intHighConsumptionDays, NULL, false ) );
	}

	public function getHighConsumptionDays() {
		return $this->m_intHighConsumptionDays;
	}

	public function sqlHighConsumptionDays() {
		return ( true == isset( $this->m_intHighConsumptionDays ) ) ? ( string ) $this->m_intHighConsumptionDays : 'NULL';
	}

	public function setMissingReadDays( $intMissingReadDays ) {
		$this->set( 'm_intMissingReadDays', CStrings::strToIntDef( $intMissingReadDays, NULL, false ) );
	}

	public function getMissingReadDays() {
		return $this->m_intMissingReadDays;
	}

	public function sqlMissingReadDays() {
		return ( true == isset( $this->m_intMissingReadDays ) ) ? ( string ) $this->m_intMissingReadDays : 'NULL';
	}

	public function setZeroConsumptionDays( $intZeroConsumptionDays ) {
		$this->set( 'm_intZeroConsumptionDays', CStrings::strToIntDef( $intZeroConsumptionDays, NULL, false ) );
	}

	public function getZeroConsumptionDays() {
		return $this->m_intZeroConsumptionDays;
	}

	public function sqlZeroConsumptionDays() {
		return ( true == isset( $this->m_intZeroConsumptionDays ) ) ? ( string ) $this->m_intZeroConsumptionDays : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_type_id, property_unit_id, meter_manufacturer_id, meter_part_type_id, radio_manufacturer_id, radio_part_type_id, old_property_meter_device_id, device_number, unit_number, count_factor, is_read_backwards, installed_on, last_repaired_on, deactivated_on, updated_by, updated_on, created_by, created_on, remote_primary_key, high_consumption_days, missing_read_days, zero_consumption_days )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityTypeId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlMeterManufacturerId() . ', ' .
						$this->sqlMeterPartTypeId() . ', ' .
						$this->sqlRadioManufacturerId() . ', ' .
						$this->sqlRadioPartTypeId() . ', ' .
						$this->sqlOldPropertyMeterDeviceId() . ', ' .
						$this->sqlDeviceNumber() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlCountFactor() . ', ' .
						$this->sqlIsReadBackwards() . ', ' .
						$this->sqlInstalledOn() . ', ' .
						$this->sqlLastRepairedOn() . ', ' .
						$this->sqlDeactivatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlHighConsumptionDays() . ', ' .
						$this->sqlMissingReadDays() . ', ' .
						$this->sqlZeroConsumptionDays() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_manufacturer_id = ' . $this->sqlMeterManufacturerId(). ',' ; } elseif( true == array_key_exists( 'MeterManufacturerId', $this->getChangedColumns() ) ) { $strSql .= ' meter_manufacturer_id = ' . $this->sqlMeterManufacturerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_part_type_id = ' . $this->sqlMeterPartTypeId(). ',' ; } elseif( true == array_key_exists( 'MeterPartTypeId', $this->getChangedColumns() ) ) { $strSql .= ' meter_part_type_id = ' . $this->sqlMeterPartTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' radio_manufacturer_id = ' . $this->sqlRadioManufacturerId(). ',' ; } elseif( true == array_key_exists( 'RadioManufacturerId', $this->getChangedColumns() ) ) { $strSql .= ' radio_manufacturer_id = ' . $this->sqlRadioManufacturerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' radio_part_type_id = ' . $this->sqlRadioPartTypeId(). ',' ; } elseif( true == array_key_exists( 'RadioPartTypeId', $this->getChangedColumns() ) ) { $strSql .= ' radio_part_type_id = ' . $this->sqlRadioPartTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_property_meter_device_id = ' . $this->sqlOldPropertyMeterDeviceId(). ',' ; } elseif( true == array_key_exists( 'OldPropertyMeterDeviceId', $this->getChangedColumns() ) ) { $strSql .= ' old_property_meter_device_id = ' . $this->sqlOldPropertyMeterDeviceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' device_number = ' . $this->sqlDeviceNumber(). ',' ; } elseif( true == array_key_exists( 'DeviceNumber', $this->getChangedColumns() ) ) { $strSql .= ' device_number = ' . $this->sqlDeviceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber(). ',' ; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' count_factor = ' . $this->sqlCountFactor(). ',' ; } elseif( true == array_key_exists( 'CountFactor', $this->getChangedColumns() ) ) { $strSql .= ' count_factor = ' . $this->sqlCountFactor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_read_backwards = ' . $this->sqlIsReadBackwards(). ',' ; } elseif( true == array_key_exists( 'IsReadBackwards', $this->getChangedColumns() ) ) { $strSql .= ' is_read_backwards = ' . $this->sqlIsReadBackwards() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' installed_on = ' . $this->sqlInstalledOn(). ',' ; } elseif( true == array_key_exists( 'InstalledOn', $this->getChangedColumns() ) ) { $strSql .= ' installed_on = ' . $this->sqlInstalledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_repaired_on = ' . $this->sqlLastRepairedOn(). ',' ; } elseif( true == array_key_exists( 'LastRepairedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_repaired_on = ' . $this->sqlLastRepairedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deactivated_on = ' . $this->sqlDeactivatedOn(). ',' ; } elseif( true == array_key_exists( 'DeactivatedOn', $this->getChangedColumns() ) ) { $strSql .= ' deactivated_on = ' . $this->sqlDeactivatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' high_consumption_days = ' . $this->sqlHighConsumptionDays(). ',' ; } elseif( true == array_key_exists( 'HighConsumptionDays', $this->getChangedColumns() ) ) { $strSql .= ' high_consumption_days = ' . $this->sqlHighConsumptionDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' missing_read_days = ' . $this->sqlMissingReadDays(). ',' ; } elseif( true == array_key_exists( 'MissingReadDays', $this->getChangedColumns() ) ) { $strSql .= ' missing_read_days = ' . $this->sqlMissingReadDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' zero_consumption_days = ' . $this->sqlZeroConsumptionDays(). ',' ; } elseif( true == array_key_exists( 'ZeroConsumptionDays', $this->getChangedColumns() ) ) { $strSql .= ' zero_consumption_days = ' . $this->sqlZeroConsumptionDays() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_type_id' => $this->getUtilityTypeId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'meter_manufacturer_id' => $this->getMeterManufacturerId(),
			'meter_part_type_id' => $this->getMeterPartTypeId(),
			'radio_manufacturer_id' => $this->getRadioManufacturerId(),
			'radio_part_type_id' => $this->getRadioPartTypeId(),
			'old_property_meter_device_id' => $this->getOldPropertyMeterDeviceId(),
			'device_number' => $this->getDeviceNumber(),
			'unit_number' => $this->getUnitNumber(),
			'count_factor' => $this->getCountFactor(),
			'is_read_backwards' => $this->getIsReadBackwards(),
			'installed_on' => $this->getInstalledOn(),
			'last_repaired_on' => $this->getLastRepairedOn(),
			'deactivated_on' => $this->getDeactivatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'high_consumption_days' => $this->getHighConsumptionDays(),
			'missing_read_days' => $this->getMissingReadDays(),
			'zero_consumption_days' => $this->getZeroConsumptionDays()
		);
	}

}
?>