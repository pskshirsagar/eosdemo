<?php

class CBaseUtilityBillAnomaly extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_anomalies';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillAnomalyTypeId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intVcrUtilityBillId;
	protected $m_intUtilityBillAccountId;
	protected $m_intUtilityBillId;
	protected $m_intPropertyUnitId;
	protected $m_intCompanyMaintenanceRequestId;
	protected $m_strAnomalyDatetime;
	protected $m_strNotes;
	protected $m_strMaintenanceNotes;
	protected $m_boolIsInternal;
	protected $m_strAnomolyNotifiedOn;
	protected $m_intResolvedBy;
	protected $m_strResolvedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsInternal = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_anomaly_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAnomalyTypeId', trim( $arrValues['utility_bill_anomaly_type_id'] ) ); elseif( isset( $arrValues['utility_bill_anomaly_type_id'] ) ) $this->setUtilityBillAnomalyTypeId( $arrValues['utility_bill_anomaly_type_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['vcr_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intVcrUtilityBillId', trim( $arrValues['vcr_utility_bill_id'] ) ); elseif( isset( $arrValues['vcr_utility_bill_id'] ) ) $this->setVcrUtilityBillId( $arrValues['vcr_utility_bill_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['company_maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMaintenanceRequestId', trim( $arrValues['company_maintenance_request_id'] ) ); elseif( isset( $arrValues['company_maintenance_request_id'] ) ) $this->setCompanyMaintenanceRequestId( $arrValues['company_maintenance_request_id'] );
		if( isset( $arrValues['anomaly_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAnomalyDatetime', trim( $arrValues['anomaly_datetime'] ) ); elseif( isset( $arrValues['anomaly_datetime'] ) ) $this->setAnomalyDatetime( $arrValues['anomaly_datetime'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['maintenance_notes'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceNotes', trim( stripcslashes( $arrValues['maintenance_notes'] ) ) ); elseif( isset( $arrValues['maintenance_notes'] ) ) $this->setMaintenanceNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_notes'] ) : $arrValues['maintenance_notes'] );
		if( isset( $arrValues['is_internal'] ) && $boolDirectSet ) $this->set( 'm_boolIsInternal', trim( stripcslashes( $arrValues['is_internal'] ) ) ); elseif( isset( $arrValues['is_internal'] ) ) $this->setIsInternal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_internal'] ) : $arrValues['is_internal'] );
		if( isset( $arrValues['anomoly_notified_on'] ) && $boolDirectSet ) $this->set( 'm_strAnomolyNotifiedOn', trim( $arrValues['anomoly_notified_on'] ) ); elseif( isset( $arrValues['anomoly_notified_on'] ) ) $this->setAnomolyNotifiedOn( $arrValues['anomoly_notified_on'] );
		if( isset( $arrValues['resolved_by'] ) && $boolDirectSet ) $this->set( 'm_intResolvedBy', trim( $arrValues['resolved_by'] ) ); elseif( isset( $arrValues['resolved_by'] ) ) $this->setResolvedBy( $arrValues['resolved_by'] );
		if( isset( $arrValues['resolved_on'] ) && $boolDirectSet ) $this->set( 'm_strResolvedOn', trim( $arrValues['resolved_on'] ) ); elseif( isset( $arrValues['resolved_on'] ) ) $this->setResolvedOn( $arrValues['resolved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillAnomalyTypeId( $intUtilityBillAnomalyTypeId ) {
		$this->set( 'm_intUtilityBillAnomalyTypeId', CStrings::strToIntDef( $intUtilityBillAnomalyTypeId, NULL, false ) );
	}

	public function getUtilityBillAnomalyTypeId() {
		return $this->m_intUtilityBillAnomalyTypeId;
	}

	public function sqlUtilityBillAnomalyTypeId() {
		return ( true == isset( $this->m_intUtilityBillAnomalyTypeId ) ) ? ( string ) $this->m_intUtilityBillAnomalyTypeId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setVcrUtilityBillId( $intVcrUtilityBillId ) {
		$this->set( 'm_intVcrUtilityBillId', CStrings::strToIntDef( $intVcrUtilityBillId, NULL, false ) );
	}

	public function getVcrUtilityBillId() {
		return $this->m_intVcrUtilityBillId;
	}

	public function sqlVcrUtilityBillId() {
		return ( true == isset( $this->m_intVcrUtilityBillId ) ) ? ( string ) $this->m_intVcrUtilityBillId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setCompanyMaintenanceRequestId( $intCompanyMaintenanceRequestId ) {
		$this->set( 'm_intCompanyMaintenanceRequestId', CStrings::strToIntDef( $intCompanyMaintenanceRequestId, NULL, false ) );
	}

	public function getCompanyMaintenanceRequestId() {
		return $this->m_intCompanyMaintenanceRequestId;
	}

	public function sqlCompanyMaintenanceRequestId() {
		return ( true == isset( $this->m_intCompanyMaintenanceRequestId ) ) ? ( string ) $this->m_intCompanyMaintenanceRequestId : 'NULL';
	}

	public function setAnomalyDatetime( $strAnomalyDatetime ) {
		$this->set( 'm_strAnomalyDatetime', CStrings::strTrimDef( $strAnomalyDatetime, -1, NULL, true ) );
	}

	public function getAnomalyDatetime() {
		return $this->m_strAnomalyDatetime;
	}

	public function sqlAnomalyDatetime() {
		return ( true == isset( $this->m_strAnomalyDatetime ) ) ? '\'' . $this->m_strAnomalyDatetime . '\'' : 'NOW()';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setMaintenanceNotes( $strMaintenanceNotes ) {
		$this->set( 'm_strMaintenanceNotes', CStrings::strTrimDef( $strMaintenanceNotes, -1, NULL, true ) );
	}

	public function getMaintenanceNotes() {
		return $this->m_strMaintenanceNotes;
	}

	public function sqlMaintenanceNotes() {
		return ( true == isset( $this->m_strMaintenanceNotes ) ) ? '\'' . addslashes( $this->m_strMaintenanceNotes ) . '\'' : 'NULL';
	}

	public function setIsInternal( $boolIsInternal ) {
		$this->set( 'm_boolIsInternal', CStrings::strToBool( $boolIsInternal ) );
	}

	public function getIsInternal() {
		return $this->m_boolIsInternal;
	}

	public function sqlIsInternal() {
		return ( true == isset( $this->m_boolIsInternal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInternal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAnomolyNotifiedOn( $strAnomolyNotifiedOn ) {
		$this->set( 'm_strAnomolyNotifiedOn', CStrings::strTrimDef( $strAnomolyNotifiedOn, -1, NULL, true ) );
	}

	public function getAnomolyNotifiedOn() {
		return $this->m_strAnomolyNotifiedOn;
	}

	public function sqlAnomolyNotifiedOn() {
		return ( true == isset( $this->m_strAnomolyNotifiedOn ) ) ? '\'' . $this->m_strAnomolyNotifiedOn . '\'' : 'NULL';
	}

	public function setResolvedBy( $intResolvedBy ) {
		$this->set( 'm_intResolvedBy', CStrings::strToIntDef( $intResolvedBy, NULL, false ) );
	}

	public function getResolvedBy() {
		return $this->m_intResolvedBy;
	}

	public function sqlResolvedBy() {
		return ( true == isset( $this->m_intResolvedBy ) ) ? ( string ) $this->m_intResolvedBy : 'NULL';
	}

	public function setResolvedOn( $strResolvedOn ) {
		$this->set( 'm_strResolvedOn', CStrings::strTrimDef( $strResolvedOn, -1, NULL, true ) );
	}

	public function getResolvedOn() {
		return $this->m_strResolvedOn;
	}

	public function sqlResolvedOn() {
		return ( true == isset( $this->m_strResolvedOn ) ) ? '\'' . $this->m_strResolvedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_anomaly_type_id, property_utility_type_id, vcr_utility_bill_id, utility_bill_account_id, utility_bill_id, property_unit_id, company_maintenance_request_id, anomaly_datetime, notes, maintenance_notes, is_internal, anomoly_notified_on, resolved_by, resolved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUtilityBillAnomalyTypeId() . ', ' .
 						$this->sqlPropertyUtilityTypeId() . ', ' .
 						$this->sqlVcrUtilityBillId() . ', ' .
 						$this->sqlUtilityBillAccountId() . ', ' .
 						$this->sqlUtilityBillId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlCompanyMaintenanceRequestId() . ', ' .
 						$this->sqlAnomalyDatetime() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlMaintenanceNotes() . ', ' .
 						$this->sqlIsInternal() . ', ' .
 						$this->sqlAnomolyNotifiedOn() . ', ' .
 						$this->sqlResolvedBy() . ', ' .
 						$this->sqlResolvedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_anomaly_type_id = ' . $this->sqlUtilityBillAnomalyTypeId() . ','; } elseif( true == array_key_exists( 'UtilityBillAnomalyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_anomaly_type_id = ' . $this->sqlUtilityBillAnomalyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vcr_utility_bill_id = ' . $this->sqlVcrUtilityBillId() . ','; } elseif( true == array_key_exists( 'VcrUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' vcr_utility_bill_id = ' . $this->sqlVcrUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_maintenance_request_id = ' . $this->sqlCompanyMaintenanceRequestId() . ','; } elseif( true == array_key_exists( 'CompanyMaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' company_maintenance_request_id = ' . $this->sqlCompanyMaintenanceRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anomaly_datetime = ' . $this->sqlAnomalyDatetime() . ','; } elseif( true == array_key_exists( 'AnomalyDatetime', $this->getChangedColumns() ) ) { $strSql .= ' anomaly_datetime = ' . $this->sqlAnomalyDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_notes = ' . $this->sqlMaintenanceNotes() . ','; } elseif( true == array_key_exists( 'MaintenanceNotes', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_notes = ' . $this->sqlMaintenanceNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_internal = ' . $this->sqlIsInternal() . ','; } elseif( true == array_key_exists( 'IsInternal', $this->getChangedColumns() ) ) { $strSql .= ' is_internal = ' . $this->sqlIsInternal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anomoly_notified_on = ' . $this->sqlAnomolyNotifiedOn() . ','; } elseif( true == array_key_exists( 'AnomolyNotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' anomoly_notified_on = ' . $this->sqlAnomolyNotifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy() . ','; } elseif( true == array_key_exists( 'ResolvedBy', $this->getChangedColumns() ) ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; } elseif( true == array_key_exists( 'ResolvedOn', $this->getChangedColumns() ) ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_anomaly_type_id' => $this->getUtilityBillAnomalyTypeId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'vcr_utility_bill_id' => $this->getVcrUtilityBillId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'company_maintenance_request_id' => $this->getCompanyMaintenanceRequestId(),
			'anomaly_datetime' => $this->getAnomalyDatetime(),
			'notes' => $this->getNotes(),
			'maintenance_notes' => $this->getMaintenanceNotes(),
			'is_internal' => $this->getIsInternal(),
			'anomoly_notified_on' => $this->getAnomolyNotifiedOn(),
			'resolved_by' => $this->getResolvedBy(),
			'resolved_on' => $this->getResolvedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>