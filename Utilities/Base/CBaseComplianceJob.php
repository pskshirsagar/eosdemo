<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseComplianceJob extends CEosSingularBase {

	const TABLE_NAME = 'public.compliance_jobs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intVendorInvitationId;
	protected $m_intPropertyId;
	protected $m_intComplianceStatusId;
	protected $m_intComplianceRulesetId;
	protected $m_intApPayeeId;
	protected $m_intVendorId;
	protected $m_intApLegalEntityId;
	protected $m_intVendorEntityId;
	protected $m_intTaskId;
	protected $m_strLastRequestedOn;
	protected $m_strLastApprovedOn;
	protected $m_intDeclinedBy;
	protected $m_strDeclinedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intComplianceStatusId = '3';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['vendor_invitation_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorInvitationId', trim( $arrValues['vendor_invitation_id'] ) ); elseif( isset( $arrValues['vendor_invitation_id'] ) ) $this->setVendorInvitationId( $arrValues['vendor_invitation_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['compliance_status_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceStatusId', trim( $arrValues['compliance_status_id'] ) ); elseif( isset( $arrValues['compliance_status_id'] ) ) $this->setComplianceStatusId( $arrValues['compliance_status_id'] );
		if( isset( $arrValues['compliance_ruleset_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceRulesetId', trim( $arrValues['compliance_ruleset_id'] ) ); elseif( isset( $arrValues['compliance_ruleset_id'] ) ) $this->setComplianceRulesetId( $arrValues['compliance_ruleset_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['ap_legal_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intApLegalEntityId', trim( $arrValues['ap_legal_entity_id'] ) ); elseif( isset( $arrValues['ap_legal_entity_id'] ) ) $this->setApLegalEntityId( $arrValues['ap_legal_entity_id'] );
		if( isset( $arrValues['vendor_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorEntityId', trim( $arrValues['vendor_entity_id'] ) ); elseif( isset( $arrValues['vendor_entity_id'] ) ) $this->setVendorEntityId( $arrValues['vendor_entity_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['last_requested_on'] ) && $boolDirectSet ) $this->set( 'm_strLastRequestedOn', trim( $arrValues['last_requested_on'] ) ); elseif( isset( $arrValues['last_requested_on'] ) ) $this->setLastRequestedOn( $arrValues['last_requested_on'] );
		if( isset( $arrValues['last_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strLastApprovedOn', trim( $arrValues['last_approved_on'] ) ); elseif( isset( $arrValues['last_approved_on'] ) ) $this->setLastApprovedOn( $arrValues['last_approved_on'] );
		if( isset( $arrValues['declined_by'] ) && $boolDirectSet ) $this->set( 'm_intDeclinedBy', trim( $arrValues['declined_by'] ) ); elseif( isset( $arrValues['declined_by'] ) ) $this->setDeclinedBy( $arrValues['declined_by'] );
		if( isset( $arrValues['declined_on'] ) && $boolDirectSet ) $this->set( 'm_strDeclinedOn', trim( $arrValues['declined_on'] ) ); elseif( isset( $arrValues['declined_on'] ) ) $this->setDeclinedOn( $arrValues['declined_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setVendorInvitationId( $intVendorInvitationId ) {
		$this->set( 'm_intVendorInvitationId', CStrings::strToIntDef( $intVendorInvitationId, NULL, false ) );
	}

	public function getVendorInvitationId() {
		return $this->m_intVendorInvitationId;
	}

	public function sqlVendorInvitationId() {
		return ( true == isset( $this->m_intVendorInvitationId ) ) ? ( string ) $this->m_intVendorInvitationId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setComplianceStatusId( $intComplianceStatusId ) {
		$this->set( 'm_intComplianceStatusId', CStrings::strToIntDef( $intComplianceStatusId, NULL, false ) );
	}

	public function getComplianceStatusId() {
		return $this->m_intComplianceStatusId;
	}

	public function sqlComplianceStatusId() {
		return ( true == isset( $this->m_intComplianceStatusId ) ) ? ( string ) $this->m_intComplianceStatusId : '3';
	}

	public function setComplianceRulesetId( $intComplianceRulesetId ) {
		$this->set( 'm_intComplianceRulesetId', CStrings::strToIntDef( $intComplianceRulesetId, NULL, false ) );
	}

	public function getComplianceRulesetId() {
		return $this->m_intComplianceRulesetId;
	}

	public function sqlComplianceRulesetId() {
		return ( true == isset( $this->m_intComplianceRulesetId ) ) ? ( string ) $this->m_intComplianceRulesetId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setApLegalEntityId( $intApLegalEntityId ) {
		$this->set( 'm_intApLegalEntityId', CStrings::strToIntDef( $intApLegalEntityId, NULL, false ) );
	}

	public function getApLegalEntityId() {
		return $this->m_intApLegalEntityId;
	}

	public function sqlApLegalEntityId() {
		return ( true == isset( $this->m_intApLegalEntityId ) ) ? ( string ) $this->m_intApLegalEntityId : 'NULL';
	}

	public function setVendorEntityId( $intVendorEntityId ) {
		$this->set( 'm_intVendorEntityId', CStrings::strToIntDef( $intVendorEntityId, NULL, false ) );
	}

	public function getVendorEntityId() {
		return $this->m_intVendorEntityId;
	}

	public function sqlVendorEntityId() {
		return ( true == isset( $this->m_intVendorEntityId ) ) ? ( string ) $this->m_intVendorEntityId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setLastRequestedOn( $strLastRequestedOn ) {
		$this->set( 'm_strLastRequestedOn', CStrings::strTrimDef( $strLastRequestedOn, -1, NULL, true ) );
	}

	public function getLastRequestedOn() {
		return $this->m_strLastRequestedOn;
	}

	public function sqlLastRequestedOn() {
		return ( true == isset( $this->m_strLastRequestedOn ) ) ? '\'' . $this->m_strLastRequestedOn . '\'' : 'NOW()';
	}

	public function setLastApprovedOn( $strLastApprovedOn ) {
		$this->set( 'm_strLastApprovedOn', CStrings::strTrimDef( $strLastApprovedOn, -1, NULL, true ) );
	}

	public function getLastApprovedOn() {
		return $this->m_strLastApprovedOn;
	}

	public function sqlLastApprovedOn() {
		return ( true == isset( $this->m_strLastApprovedOn ) ) ? '\'' . $this->m_strLastApprovedOn . '\'' : 'NULL';
	}

	public function setDeclinedBy( $intDeclinedBy ) {
		$this->set( 'm_intDeclinedBy', CStrings::strToIntDef( $intDeclinedBy, NULL, false ) );
	}

	public function getDeclinedBy() {
		return $this->m_intDeclinedBy;
	}

	public function sqlDeclinedBy() {
		return ( true == isset( $this->m_intDeclinedBy ) ) ? ( string ) $this->m_intDeclinedBy : 'NULL';
	}

	public function setDeclinedOn( $strDeclinedOn ) {
		$this->set( 'm_strDeclinedOn', CStrings::strTrimDef( $strDeclinedOn, -1, NULL, true ) );
	}

	public function getDeclinedOn() {
		return $this->m_strDeclinedOn;
	}

	public function sqlDeclinedOn() {
		return ( true == isset( $this->m_strDeclinedOn ) ) ? '\'' . $this->m_strDeclinedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, vendor_invitation_id, property_id, compliance_status_id, compliance_ruleset_id, ap_payee_id, vendor_id, ap_legal_entity_id, vendor_entity_id, task_id, last_requested_on, last_approved_on, declined_by, declined_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlVendorInvitationId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlComplianceStatusId() . ', ' .
 						$this->sqlComplianceRulesetId() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlVendorId() . ', ' .
 						$this->sqlApLegalEntityId() . ', ' .
 						$this->sqlVendorEntityId() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlLastRequestedOn() . ', ' .
 						$this->sqlLastApprovedOn() . ', ' .
 						$this->sqlDeclinedBy() . ', ' .
 						$this->sqlDeclinedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_invitation_id = ' . $this->sqlVendorInvitationId() . ','; } elseif( true == array_key_exists( 'VendorInvitationId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_invitation_id = ' . $this->sqlVendorInvitationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_status_id = ' . $this->sqlComplianceStatusId() . ','; } elseif( true == array_key_exists( 'ComplianceStatusId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_status_id = ' . $this->sqlComplianceStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_ruleset_id = ' . $this->sqlComplianceRulesetId() . ','; } elseif( true == array_key_exists( 'ComplianceRulesetId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_ruleset_id = ' . $this->sqlComplianceRulesetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_legal_entity_id = ' . $this->sqlApLegalEntityId() . ','; } elseif( true == array_key_exists( 'ApLegalEntityId', $this->getChangedColumns() ) ) { $strSql .= ' ap_legal_entity_id = ' . $this->sqlApLegalEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_entity_id = ' . $this->sqlVendorEntityId() . ','; } elseif( true == array_key_exists( 'VendorEntityId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_entity_id = ' . $this->sqlVendorEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_requested_on = ' . $this->sqlLastRequestedOn() . ','; } elseif( true == array_key_exists( 'LastRequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_requested_on = ' . $this->sqlLastRequestedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_approved_on = ' . $this->sqlLastApprovedOn() . ','; } elseif( true == array_key_exists( 'LastApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_approved_on = ' . $this->sqlLastApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' declined_by = ' . $this->sqlDeclinedBy() . ','; } elseif( true == array_key_exists( 'DeclinedBy', $this->getChangedColumns() ) ) { $strSql .= ' declined_by = ' . $this->sqlDeclinedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' declined_on = ' . $this->sqlDeclinedOn() . ','; } elseif( true == array_key_exists( 'DeclinedOn', $this->getChangedColumns() ) ) { $strSql .= ' declined_on = ' . $this->sqlDeclinedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'vendor_invitation_id' => $this->getVendorInvitationId(),
			'property_id' => $this->getPropertyId(),
			'compliance_status_id' => $this->getComplianceStatusId(),
			'compliance_ruleset_id' => $this->getComplianceRulesetId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'vendor_id' => $this->getVendorId(),
			'ap_legal_entity_id' => $this->getApLegalEntityId(),
			'vendor_entity_id' => $this->getVendorEntityId(),
			'task_id' => $this->getTaskId(),
			'last_requested_on' => $this->getLastRequestedOn(),
			'last_approved_on' => $this->getLastApprovedOn(),
			'declined_by' => $this->getDeclinedBy(),
			'declined_on' => $this->getDeclinedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>