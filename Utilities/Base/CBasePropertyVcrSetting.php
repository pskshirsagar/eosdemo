<?php

class CBasePropertyVcrSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.property_vcr_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intUsageArCodeId;
	protected $m_intServiceFeeArCodeId;
	protected $m_intUtilityTemplateId;
	protected $m_intUsageExportGroupId;
	protected $m_intServiceFeeExportGroupId;
	protected $m_strUsageDescription;
	protected $m_strServiceFeeDescription;
	protected $m_fltVacantUnitBillAlertAmount;
	protected $m_strVcrInstructions;
	protected $m_intIsImmediatePost;
	protected $m_intDontSendVcrSummaryEmail;
	protected $m_intProviderContactInformation;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUtilityTaxRateId;
	protected $m_intTaxRateArCodeId;
	protected $m_strTaxRateDescription;
	protected $m_boolIsManuallyVerify;

	public function __construct() {
		parent::__construct();

		$this->m_intIsImmediatePost = '0';
		$this->m_intDontSendVcrSummaryEmail = '0';
		$this->m_intProviderContactInformation = '0';
		$this->m_boolIsManuallyVerify = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['usage_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intUsageArCodeId', trim( $arrValues['usage_ar_code_id'] ) ); elseif( isset( $arrValues['usage_ar_code_id'] ) ) $this->setUsageArCodeId( $arrValues['usage_ar_code_id'] );
		if( isset( $arrValues['service_fee_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceFeeArCodeId', trim( $arrValues['service_fee_ar_code_id'] ) ); elseif( isset( $arrValues['service_fee_ar_code_id'] ) ) $this->setServiceFeeArCodeId( $arrValues['service_fee_ar_code_id'] );
		if( isset( $arrValues['utility_template_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTemplateId', trim( $arrValues['utility_template_id'] ) ); elseif( isset( $arrValues['utility_template_id'] ) ) $this->setUtilityTemplateId( $arrValues['utility_template_id'] );
		if( isset( $arrValues['usage_export_group_id'] ) && $boolDirectSet ) $this->set( 'm_intUsageExportGroupId', trim( $arrValues['usage_export_group_id'] ) ); elseif( isset( $arrValues['usage_export_group_id'] ) ) $this->setUsageExportGroupId( $arrValues['usage_export_group_id'] );
		if( isset( $arrValues['service_fee_export_group_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceFeeExportGroupId', trim( $arrValues['service_fee_export_group_id'] ) ); elseif( isset( $arrValues['service_fee_export_group_id'] ) ) $this->setServiceFeeExportGroupId( $arrValues['service_fee_export_group_id'] );
		if( isset( $arrValues['usage_description'] ) && $boolDirectSet ) $this->set( 'm_strUsageDescription', trim( $arrValues['usage_description'] ) ); elseif( isset( $arrValues['usage_description'] ) ) $this->setUsageDescription( $arrValues['usage_description'] );
		if( isset( $arrValues['service_fee_description'] ) && $boolDirectSet ) $this->set( 'm_strServiceFeeDescription', trim( $arrValues['service_fee_description'] ) ); elseif( isset( $arrValues['service_fee_description'] ) ) $this->setServiceFeeDescription( $arrValues['service_fee_description'] );
		if( isset( $arrValues['vacant_unit_bill_alert_amount'] ) && $boolDirectSet ) $this->set( 'm_fltVacantUnitBillAlertAmount', trim( $arrValues['vacant_unit_bill_alert_amount'] ) ); elseif( isset( $arrValues['vacant_unit_bill_alert_amount'] ) ) $this->setVacantUnitBillAlertAmount( $arrValues['vacant_unit_bill_alert_amount'] );
		if( isset( $arrValues['vcr_instructions'] ) && $boolDirectSet ) $this->set( 'm_strVcrInstructions', trim( $arrValues['vcr_instructions'] ) ); elseif( isset( $arrValues['vcr_instructions'] ) ) $this->setVcrInstructions( $arrValues['vcr_instructions'] );
		if( isset( $arrValues['is_immediate_post'] ) && $boolDirectSet ) $this->set( 'm_intIsImmediatePost', trim( $arrValues['is_immediate_post'] ) ); elseif( isset( $arrValues['is_immediate_post'] ) ) $this->setIsImmediatePost( $arrValues['is_immediate_post'] );
		if( isset( $arrValues['dont_send_vcr_summary_email'] ) && $boolDirectSet ) $this->set( 'm_intDontSendVcrSummaryEmail', trim( $arrValues['dont_send_vcr_summary_email'] ) ); elseif( isset( $arrValues['dont_send_vcr_summary_email'] ) ) $this->setDontSendVcrSummaryEmail( $arrValues['dont_send_vcr_summary_email'] );
		if( isset( $arrValues['provider_contact_information'] ) && $boolDirectSet ) $this->set( 'm_intProviderContactInformation', trim( $arrValues['provider_contact_information'] ) ); elseif( isset( $arrValues['provider_contact_information'] ) ) $this->setProviderContactInformation( $arrValues['provider_contact_information'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['utility_tax_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTaxRateId', trim( $arrValues['utility_tax_rate_id'] ) ); elseif( isset( $arrValues['utility_tax_rate_id'] ) ) $this->setUtilityTaxRateId( $arrValues['utility_tax_rate_id'] );
		if( isset( $arrValues['tax_rate_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxRateArCodeId', trim( $arrValues['tax_rate_ar_code_id'] ) ); elseif( isset( $arrValues['tax_rate_ar_code_id'] ) ) $this->setTaxRateArCodeId( $arrValues['tax_rate_ar_code_id'] );
		if( isset( $arrValues['tax_rate_description'] ) && $boolDirectSet ) $this->set( 'm_strTaxRateDescription', trim( $arrValues['tax_rate_description'] ) ); elseif( isset( $arrValues['tax_rate_description'] ) ) $this->setTaxRateDescription( $arrValues['tax_rate_description'] );
		if( isset( $arrValues['is_manually_verify'] ) && $boolDirectSet ) $this->set( 'm_boolIsManuallyVerify', trim( stripcslashes( $arrValues['is_manually_verify'] ) ) ); elseif( isset( $arrValues['is_manually_verify'] ) ) $this->setIsManuallyVerify( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manually_verify'] ) : $arrValues['is_manually_verify'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setUsageArCodeId( $intUsageArCodeId ) {
		$this->set( 'm_intUsageArCodeId', CStrings::strToIntDef( $intUsageArCodeId, NULL, false ) );
	}

	public function getUsageArCodeId() {
		return $this->m_intUsageArCodeId;
	}

	public function sqlUsageArCodeId() {
		return ( true == isset( $this->m_intUsageArCodeId ) ) ? ( string ) $this->m_intUsageArCodeId : 'NULL';
	}

	public function setServiceFeeArCodeId( $intServiceFeeArCodeId ) {
		$this->set( 'm_intServiceFeeArCodeId', CStrings::strToIntDef( $intServiceFeeArCodeId, NULL, false ) );
	}

	public function getServiceFeeArCodeId() {
		return $this->m_intServiceFeeArCodeId;
	}

	public function sqlServiceFeeArCodeId() {
		return ( true == isset( $this->m_intServiceFeeArCodeId ) ) ? ( string ) $this->m_intServiceFeeArCodeId : 'NULL';
	}

	public function setUtilityTemplateId( $intUtilityTemplateId ) {
		$this->set( 'm_intUtilityTemplateId', CStrings::strToIntDef( $intUtilityTemplateId, NULL, false ) );
	}

	public function getUtilityTemplateId() {
		return $this->m_intUtilityTemplateId;
	}

	public function sqlUtilityTemplateId() {
		return ( true == isset( $this->m_intUtilityTemplateId ) ) ? ( string ) $this->m_intUtilityTemplateId : 'NULL';
	}

	public function setUsageExportGroupId( $intUsageExportGroupId ) {
		$this->set( 'm_intUsageExportGroupId', CStrings::strToIntDef( $intUsageExportGroupId, NULL, false ) );
	}

	public function getUsageExportGroupId() {
		return $this->m_intUsageExportGroupId;
	}

	public function sqlUsageExportGroupId() {
		return ( true == isset( $this->m_intUsageExportGroupId ) ) ? ( string ) $this->m_intUsageExportGroupId : 'NULL';
	}

	public function setServiceFeeExportGroupId( $intServiceFeeExportGroupId ) {
		$this->set( 'm_intServiceFeeExportGroupId', CStrings::strToIntDef( $intServiceFeeExportGroupId, NULL, false ) );
	}

	public function getServiceFeeExportGroupId() {
		return $this->m_intServiceFeeExportGroupId;
	}

	public function sqlServiceFeeExportGroupId() {
		return ( true == isset( $this->m_intServiceFeeExportGroupId ) ) ? ( string ) $this->m_intServiceFeeExportGroupId : 'NULL';
	}

	public function setUsageDescription( $strUsageDescription ) {
		$this->set( 'm_strUsageDescription', CStrings::strTrimDef( $strUsageDescription, 240, NULL, true ) );
	}

	public function getUsageDescription() {
		return $this->m_strUsageDescription;
	}

	public function sqlUsageDescription() {
		return ( true == isset( $this->m_strUsageDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUsageDescription ) : '\'' . addslashes( $this->m_strUsageDescription ) . '\'' ) : 'NULL';
	}

	public function setServiceFeeDescription( $strServiceFeeDescription ) {
		$this->set( 'm_strServiceFeeDescription', CStrings::strTrimDef( $strServiceFeeDescription, 240, NULL, true ) );
	}

	public function getServiceFeeDescription() {
		return $this->m_strServiceFeeDescription;
	}

	public function sqlServiceFeeDescription() {
		return ( true == isset( $this->m_strServiceFeeDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceFeeDescription ) : '\'' . addslashes( $this->m_strServiceFeeDescription ) . '\'' ) : 'NULL';
	}

	public function setVacantUnitBillAlertAmount( $fltVacantUnitBillAlertAmount ) {
		$this->set( 'm_fltVacantUnitBillAlertAmount', CStrings::strToFloatDef( $fltVacantUnitBillAlertAmount, NULL, false, 2 ) );
	}

	public function getVacantUnitBillAlertAmount() {
		return $this->m_fltVacantUnitBillAlertAmount;
	}

	public function sqlVacantUnitBillAlertAmount() {
		return ( true == isset( $this->m_fltVacantUnitBillAlertAmount ) ) ? ( string ) $this->m_fltVacantUnitBillAlertAmount : 'NULL';
	}

	public function setVcrInstructions( $strVcrInstructions ) {
		$this->set( 'm_strVcrInstructions', CStrings::strTrimDef( $strVcrInstructions, -1, NULL, true ) );
	}

	public function getVcrInstructions() {
		return $this->m_strVcrInstructions;
	}

	public function sqlVcrInstructions() {
		return ( true == isset( $this->m_strVcrInstructions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVcrInstructions ) : '\'' . addslashes( $this->m_strVcrInstructions ) . '\'' ) : 'NULL';
	}

	public function setIsImmediatePost( $intIsImmediatePost ) {
		$this->set( 'm_intIsImmediatePost', CStrings::strToIntDef( $intIsImmediatePost, NULL, false ) );
	}

	public function getIsImmediatePost() {
		return $this->m_intIsImmediatePost;
	}

	public function sqlIsImmediatePost() {
		return ( true == isset( $this->m_intIsImmediatePost ) ) ? ( string ) $this->m_intIsImmediatePost : '0';
	}

	public function setDontSendVcrSummaryEmail( $intDontSendVcrSummaryEmail ) {
		$this->set( 'm_intDontSendVcrSummaryEmail', CStrings::strToIntDef( $intDontSendVcrSummaryEmail, NULL, false ) );
	}

	public function getDontSendVcrSummaryEmail() {
		return $this->m_intDontSendVcrSummaryEmail;
	}

	public function sqlDontSendVcrSummaryEmail() {
		return ( true == isset( $this->m_intDontSendVcrSummaryEmail ) ) ? ( string ) $this->m_intDontSendVcrSummaryEmail : '0';
	}

	public function setProviderContactInformation( $intProviderContactInformation ) {
		$this->set( 'm_intProviderContactInformation', CStrings::strToIntDef( $intProviderContactInformation, NULL, false ) );
	}

	public function getProviderContactInformation() {
		return $this->m_intProviderContactInformation;
	}

	public function sqlProviderContactInformation() {
		return ( true == isset( $this->m_intProviderContactInformation ) ) ? ( string ) $this->m_intProviderContactInformation : '0';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUtilityTaxRateId( $intUtilityTaxRateId ) {
		$this->set( 'm_intUtilityTaxRateId', CStrings::strToIntDef( $intUtilityTaxRateId, NULL, false ) );
	}

	public function getUtilityTaxRateId() {
		return $this->m_intUtilityTaxRateId;
	}

	public function sqlUtilityTaxRateId() {
		return ( true == isset( $this->m_intUtilityTaxRateId ) ) ? ( string ) $this->m_intUtilityTaxRateId : 'NULL';
	}

	public function setTaxRateArCodeId( $intTaxRateArCodeId ) {
		$this->set( 'm_intTaxRateArCodeId', CStrings::strToIntDef( $intTaxRateArCodeId, NULL, false ) );
	}

	public function getTaxRateArCodeId() {
		return $this->m_intTaxRateArCodeId;
	}

	public function sqlTaxRateArCodeId() {
		return ( true == isset( $this->m_intTaxRateArCodeId ) ) ? ( string ) $this->m_intTaxRateArCodeId : 'NULL';
	}

	public function setTaxRateDescription( $strTaxRateDescription ) {
		$this->set( 'm_strTaxRateDescription', CStrings::strTrimDef( $strTaxRateDescription, 250, NULL, true ) );
	}

	public function getTaxRateDescription() {
		return $this->m_strTaxRateDescription;
	}

	public function sqlTaxRateDescription() {
		return ( true == isset( $this->m_strTaxRateDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxRateDescription ) : '\'' . addslashes( $this->m_strTaxRateDescription ) . '\'' ) : 'NULL';
	}

	public function setIsManuallyVerify( $boolIsManuallyVerify ) {
		$this->set( 'm_boolIsManuallyVerify', CStrings::strToBool( $boolIsManuallyVerify ) );
	}

	public function getIsManuallyVerify() {
		return $this->m_boolIsManuallyVerify;
	}

	public function sqlIsManuallyVerify() {
		return ( true == isset( $this->m_boolIsManuallyVerify ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManuallyVerify ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_type_id, usage_ar_code_id, service_fee_ar_code_id, utility_template_id, usage_export_group_id, service_fee_export_group_id, usage_description, service_fee_description, vacant_unit_bill_alert_amount, vcr_instructions, is_immediate_post, dont_send_vcr_summary_email, provider_contact_information, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on, utility_tax_rate_id, tax_rate_ar_code_id, tax_rate_description, is_manually_verify )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlUsageArCodeId() . ', ' .
						$this->sqlServiceFeeArCodeId() . ', ' .
						$this->sqlUtilityTemplateId() . ', ' .
						$this->sqlUsageExportGroupId() . ', ' .
						$this->sqlServiceFeeExportGroupId() . ', ' .
						$this->sqlUsageDescription() . ', ' .
						$this->sqlServiceFeeDescription() . ', ' .
						$this->sqlVacantUnitBillAlertAmount() . ', ' .
						$this->sqlVcrInstructions() . ', ' .
						$this->sqlIsImmediatePost() . ', ' .
						$this->sqlDontSendVcrSummaryEmail() . ', ' .
						$this->sqlProviderContactInformation() . ', ' .
						$this->sqlDisabledBy() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUtilityTaxRateId() . ', ' .
						$this->sqlTaxRateArCodeId() . ', ' .
						$this->sqlTaxRateDescription() . ', ' .
						$this->sqlIsManuallyVerify() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' usage_ar_code_id = ' . $this->sqlUsageArCodeId(). ',' ; } elseif( true == array_key_exists( 'UsageArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' usage_ar_code_id = ' . $this->sqlUsageArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_fee_ar_code_id = ' . $this->sqlServiceFeeArCodeId(). ',' ; } elseif( true == array_key_exists( 'ServiceFeeArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' service_fee_ar_code_id = ' . $this->sqlServiceFeeArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_template_id = ' . $this->sqlUtilityTemplateId(). ',' ; } elseif( true == array_key_exists( 'UtilityTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' utility_template_id = ' . $this->sqlUtilityTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' usage_export_group_id = ' . $this->sqlUsageExportGroupId(). ',' ; } elseif( true == array_key_exists( 'UsageExportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' usage_export_group_id = ' . $this->sqlUsageExportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_fee_export_group_id = ' . $this->sqlServiceFeeExportGroupId(). ',' ; } elseif( true == array_key_exists( 'ServiceFeeExportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' service_fee_export_group_id = ' . $this->sqlServiceFeeExportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' usage_description = ' . $this->sqlUsageDescription(). ',' ; } elseif( true == array_key_exists( 'UsageDescription', $this->getChangedColumns() ) ) { $strSql .= ' usage_description = ' . $this->sqlUsageDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_fee_description = ' . $this->sqlServiceFeeDescription(). ',' ; } elseif( true == array_key_exists( 'ServiceFeeDescription', $this->getChangedColumns() ) ) { $strSql .= ' service_fee_description = ' . $this->sqlServiceFeeDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacant_unit_bill_alert_amount = ' . $this->sqlVacantUnitBillAlertAmount(). ',' ; } elseif( true == array_key_exists( 'VacantUnitBillAlertAmount', $this->getChangedColumns() ) ) { $strSql .= ' vacant_unit_bill_alert_amount = ' . $this->sqlVacantUnitBillAlertAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vcr_instructions = ' . $this->sqlVcrInstructions(). ',' ; } elseif( true == array_key_exists( 'VcrInstructions', $this->getChangedColumns() ) ) { $strSql .= ' vcr_instructions = ' . $this->sqlVcrInstructions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_immediate_post = ' . $this->sqlIsImmediatePost(). ',' ; } elseif( true == array_key_exists( 'IsImmediatePost', $this->getChangedColumns() ) ) { $strSql .= ' is_immediate_post = ' . $this->sqlIsImmediatePost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_send_vcr_summary_email = ' . $this->sqlDontSendVcrSummaryEmail(). ',' ; } elseif( true == array_key_exists( 'DontSendVcrSummaryEmail', $this->getChangedColumns() ) ) { $strSql .= ' dont_send_vcr_summary_email = ' . $this->sqlDontSendVcrSummaryEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' provider_contact_information = ' . $this->sqlProviderContactInformation(). ',' ; } elseif( true == array_key_exists( 'ProviderContactInformation', $this->getChangedColumns() ) ) { $strSql .= ' provider_contact_information = ' . $this->sqlProviderContactInformation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy(). ',' ; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_tax_rate_id = ' . $this->sqlUtilityTaxRateId(). ',' ; } elseif( true == array_key_exists( 'UtilityTaxRateId', $this->getChangedColumns() ) ) { $strSql .= ' utility_tax_rate_id = ' . $this->sqlUtilityTaxRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_rate_ar_code_id = ' . $this->sqlTaxRateArCodeId(). ',' ; } elseif( true == array_key_exists( 'TaxRateArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' tax_rate_ar_code_id = ' . $this->sqlTaxRateArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_rate_description = ' . $this->sqlTaxRateDescription(). ',' ; } elseif( true == array_key_exists( 'TaxRateDescription', $this->getChangedColumns() ) ) { $strSql .= ' tax_rate_description = ' . $this->sqlTaxRateDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manually_verify = ' . $this->sqlIsManuallyVerify(). ',' ; } elseif( true == array_key_exists( 'IsManuallyVerify', $this->getChangedColumns() ) ) { $strSql .= ' is_manually_verify = ' . $this->sqlIsManuallyVerify() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'usage_ar_code_id' => $this->getUsageArCodeId(),
			'service_fee_ar_code_id' => $this->getServiceFeeArCodeId(),
			'utility_template_id' => $this->getUtilityTemplateId(),
			'usage_export_group_id' => $this->getUsageExportGroupId(),
			'service_fee_export_group_id' => $this->getServiceFeeExportGroupId(),
			'usage_description' => $this->getUsageDescription(),
			'service_fee_description' => $this->getServiceFeeDescription(),
			'vacant_unit_bill_alert_amount' => $this->getVacantUnitBillAlertAmount(),
			'vcr_instructions' => $this->getVcrInstructions(),
			'is_immediate_post' => $this->getIsImmediatePost(),
			'dont_send_vcr_summary_email' => $this->getDontSendVcrSummaryEmail(),
			'provider_contact_information' => $this->getProviderContactInformation(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'utility_tax_rate_id' => $this->getUtilityTaxRateId(),
			'tax_rate_ar_code_id' => $this->getTaxRateArCodeId(),
			'tax_rate_description' => $this->getTaxRateDescription(),
			'is_manually_verify' => $this->getIsManuallyVerify()
		);
	}

}
?>