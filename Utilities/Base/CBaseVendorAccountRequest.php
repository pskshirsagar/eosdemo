<?php

class CBaseVendorAccountRequest extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.vendor_account_requests';

	protected $m_intId;
	protected $m_strEntityName;
	protected $m_strEmailAddress;
	protected $m_strPasswordHashed;
	protected $m_strTaxNumberEncrypted;
	protected $m_strTaxNumberHash;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['entity_name'] ) && $boolDirectSet ) $this->set( 'm_strEntityName', trim( $arrValues['entity_name'] ) ); elseif( isset( $arrValues['entity_name'] ) ) $this->setEntityName( $arrValues['entity_name'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['password_hashed'] ) && $boolDirectSet ) $this->set( 'm_strPasswordHashed', trim( $arrValues['password_hashed'] ) ); elseif( isset( $arrValues['password_hashed'] ) ) $this->setPasswordHashed( $arrValues['password_hashed'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['tax_number_hash'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberHash', trim( $arrValues['tax_number_hash'] ) ); elseif( isset( $arrValues['tax_number_hash'] ) ) $this->setTaxNumberHash( $arrValues['tax_number_hash'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEntityName( $strEntityName ) {
		$this->set( 'm_strEntityName', CStrings::strTrimDef( $strEntityName, 240, NULL, true ) );
	}

	public function getEntityName() {
		return $this->m_strEntityName;
	}

	public function sqlEntityName() {
		return ( true == isset( $this->m_strEntityName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEntityName ) : '\'' . addslashes( $this->m_strEntityName ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setPasswordHashed( $strPasswordHashed ) {
		$this->set( 'm_strPasswordHashed', CStrings::strTrimDef( $strPasswordHashed, 240, NULL, true ) );
	}

	public function getPasswordHashed() {
		return $this->m_strPasswordHashed;
	}

	public function sqlPasswordHashed() {
		return ( true == isset( $this->m_strPasswordHashed ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPasswordHashed ) : '\'' . addslashes( $this->m_strPasswordHashed ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberHash( $strTaxNumberHash ) {
		$this->set( 'm_strTaxNumberHash', CStrings::strTrimDef( $strTaxNumberHash, 240, NULL, true ) );
	}

	public function getTaxNumberHash() {
		return $this->m_strTaxNumberHash;
	}

	public function sqlTaxNumberHash() {
		return ( true == isset( $this->m_strTaxNumberHash ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberHash ) : '\'' . addslashes( $this->m_strTaxNumberHash ) . '\'' ) : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, entity_name, email_address, password_hashed, tax_number_encrypted, tax_number_hash, details, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEntityName() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlPasswordHashed() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlTaxNumberHash() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_name = ' . $this->sqlEntityName(). ',' ; } elseif( true == array_key_exists( 'EntityName', $this->getChangedColumns() ) ) { $strSql .= ' entity_name = ' . $this->sqlEntityName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_hashed = ' . $this->sqlPasswordHashed(). ',' ; } elseif( true == array_key_exists( 'PasswordHashed', $this->getChangedColumns() ) ) { $strSql .= ' password_hashed = ' . $this->sqlPasswordHashed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_hash = ' . $this->sqlTaxNumberHash(). ',' ; } elseif( true == array_key_exists( 'TaxNumberHash', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_hash = ' . $this->sqlTaxNumberHash() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'entity_name' => $this->getEntityName(),
			'email_address' => $this->getEmailAddress(),
			'password_hashed' => $this->getPasswordHashed(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'tax_number_hash' => $this->getTaxNumberHash(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>