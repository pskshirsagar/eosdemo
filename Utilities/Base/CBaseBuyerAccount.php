<?php

class CBaseBuyerAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.buyer_accounts';

	protected $m_intId;
	protected $m_intVendorId;
	protected $m_intStoreId;
	protected $m_intBuyerId;
	protected $m_intBuyerLocationId;
	protected $m_intApPayeeAccountId;
	protected $m_intUtilityBillAccountId;
	protected $m_intReplacementVendorAccountId;
	protected $m_intAuditFrequencyId;
	protected $m_intSummaryBuyerAccountId;
	protected $m_intReplacementBuyerAccountId;
	protected $m_strName;
	protected $m_strAccountNumber;
	protected $m_strAccountDescription;
	protected $m_intAuditDay;
	protected $m_strLastInvoiceDate;
	protected $m_strAuditStartDate;
	protected $m_boolAuditInvoice;
	protected $m_boolIsDisabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsIgnored;
	protected $m_intDefaultBuyerLocationId;

	public function __construct() {
		parent::__construct();

		$this->m_intAuditFrequencyId = '1';
		$this->m_boolAuditInvoice = false;
		$this->m_boolIsDisabled = false;
		$this->m_boolIsIgnored = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['store_id'] ) && $boolDirectSet ) $this->set( 'm_intStoreId', trim( $arrValues['store_id'] ) ); elseif( isset( $arrValues['store_id'] ) ) $this->setStoreId( $arrValues['store_id'] );
		if( isset( $arrValues['buyer_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerId', trim( $arrValues['buyer_id'] ) ); elseif( isset( $arrValues['buyer_id'] ) ) $this->setBuyerId( $arrValues['buyer_id'] );
		if( isset( $arrValues['buyer_location_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerLocationId', trim( $arrValues['buyer_location_id'] ) ); elseif( isset( $arrValues['buyer_location_id'] ) ) $this->setBuyerLocationId( $arrValues['buyer_location_id'] );
		if( isset( $arrValues['ap_payee_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeAccountId', trim( $arrValues['ap_payee_account_id'] ) ); elseif( isset( $arrValues['ap_payee_account_id'] ) ) $this->setApPayeeAccountId( $arrValues['ap_payee_account_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['replacement_vendor_account_id'] ) && $boolDirectSet ) $this->set( 'm_intReplacementVendorAccountId', trim( $arrValues['replacement_vendor_account_id'] ) ); elseif( isset( $arrValues['replacement_vendor_account_id'] ) ) $this->setReplacementVendorAccountId( $arrValues['replacement_vendor_account_id'] );
		if( isset( $arrValues['audit_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intAuditFrequencyId', trim( $arrValues['audit_frequency_id'] ) ); elseif( isset( $arrValues['audit_frequency_id'] ) ) $this->setAuditFrequencyId( $arrValues['audit_frequency_id'] );
		if( isset( $arrValues['summary_buyer_account_id'] ) && $boolDirectSet ) $this->set( 'm_intSummaryBuyerAccountId', trim( $arrValues['summary_buyer_account_id'] ) ); elseif( isset( $arrValues['summary_buyer_account_id'] ) ) $this->setSummaryBuyerAccountId( $arrValues['summary_buyer_account_id'] );
		if( isset( $arrValues['replacement_buyer_account_id'] ) && $boolDirectSet ) $this->set( 'm_intReplacementBuyerAccountId', trim( $arrValues['replacement_buyer_account_id'] ) ); elseif( isset( $arrValues['replacement_buyer_account_id'] ) ) $this->setReplacementBuyerAccountId( $arrValues['replacement_buyer_account_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['account_number'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumber', trim( stripcslashes( $arrValues['account_number'] ) ) ); elseif( isset( $arrValues['account_number'] ) ) $this->setAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number'] ) : $arrValues['account_number'] );
		if( isset( $arrValues['account_description'] ) && $boolDirectSet ) $this->set( 'm_strAccountDescription', trim( stripcslashes( $arrValues['account_description'] ) ) ); elseif( isset( $arrValues['account_description'] ) ) $this->setAccountDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_description'] ) : $arrValues['account_description'] );
		if( isset( $arrValues['audit_day'] ) && $boolDirectSet ) $this->set( 'm_intAuditDay', trim( $arrValues['audit_day'] ) ); elseif( isset( $arrValues['audit_day'] ) ) $this->setAuditDay( $arrValues['audit_day'] );
		if( isset( $arrValues['last_invoice_date'] ) && $boolDirectSet ) $this->set( 'm_strLastInvoiceDate', trim( $arrValues['last_invoice_date'] ) ); elseif( isset( $arrValues['last_invoice_date'] ) ) $this->setLastInvoiceDate( $arrValues['last_invoice_date'] );
		if( isset( $arrValues['audit_start_date'] ) && $boolDirectSet ) $this->set( 'm_strAuditStartDate', trim( $arrValues['audit_start_date'] ) ); elseif( isset( $arrValues['audit_start_date'] ) ) $this->setAuditStartDate( $arrValues['audit_start_date'] );
		if( isset( $arrValues['audit_invoice'] ) && $boolDirectSet ) $this->set( 'm_boolAuditInvoice', trim( stripcslashes( $arrValues['audit_invoice'] ) ) ); elseif( isset( $arrValues['audit_invoice'] ) ) $this->setAuditInvoice( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['audit_invoice'] ) : $arrValues['audit_invoice'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsIgnored', trim( stripcslashes( $arrValues['is_ignored'] ) ) ); elseif( isset( $arrValues['is_ignored'] ) ) $this->setIsIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ignored'] ) : $arrValues['is_ignored'] );
		if( isset( $arrValues['default_buyer_location_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultBuyerLocationId', trim( $arrValues['default_buyer_location_id'] ) ); elseif( isset( $arrValues['default_buyer_location_id'] ) ) $this->setDefaultBuyerLocationId( $arrValues['default_buyer_location_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setStoreId( $intStoreId ) {
		$this->set( 'm_intStoreId', CStrings::strToIntDef( $intStoreId, NULL, false ) );
	}

	public function getStoreId() {
		return $this->m_intStoreId;
	}

	public function sqlStoreId() {
		return ( true == isset( $this->m_intStoreId ) ) ? ( string ) $this->m_intStoreId : 'NULL';
	}

	public function setBuyerId( $intBuyerId ) {
		$this->set( 'm_intBuyerId', CStrings::strToIntDef( $intBuyerId, NULL, false ) );
	}

	public function getBuyerId() {
		return $this->m_intBuyerId;
	}

	public function sqlBuyerId() {
		return ( true == isset( $this->m_intBuyerId ) ) ? ( string ) $this->m_intBuyerId : 'NULL';
	}

	public function setBuyerLocationId( $intBuyerLocationId ) {
		$this->set( 'm_intBuyerLocationId', CStrings::strToIntDef( $intBuyerLocationId, NULL, false ) );
	}

	public function getBuyerLocationId() {
		return $this->m_intBuyerLocationId;
	}

	public function sqlBuyerLocationId() {
		return ( true == isset( $this->m_intBuyerLocationId ) ) ? ( string ) $this->m_intBuyerLocationId : 'NULL';
	}

	public function setApPayeeAccountId( $intApPayeeAccountId ) {
		$this->set( 'm_intApPayeeAccountId', CStrings::strToIntDef( $intApPayeeAccountId, NULL, false ) );
	}

	public function getApPayeeAccountId() {
		return $this->m_intApPayeeAccountId;
	}

	public function sqlApPayeeAccountId() {
		return ( true == isset( $this->m_intApPayeeAccountId ) ) ? ( string ) $this->m_intApPayeeAccountId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setReplacementVendorAccountId( $intReplacementVendorAccountId ) {
		$this->set( 'm_intReplacementVendorAccountId', CStrings::strToIntDef( $intReplacementVendorAccountId, NULL, false ) );
	}

	public function getReplacementVendorAccountId() {
		return $this->m_intReplacementVendorAccountId;
	}

	public function sqlReplacementVendorAccountId() {
		return ( true == isset( $this->m_intReplacementVendorAccountId ) ) ? ( string ) $this->m_intReplacementVendorAccountId : 'NULL';
	}

	public function setAuditFrequencyId( $intAuditFrequencyId ) {
		$this->set( 'm_intAuditFrequencyId', CStrings::strToIntDef( $intAuditFrequencyId, NULL, false ) );
	}

	public function getAuditFrequencyId() {
		return $this->m_intAuditFrequencyId;
	}

	public function sqlAuditFrequencyId() {
		return ( true == isset( $this->m_intAuditFrequencyId ) ) ? ( string ) $this->m_intAuditFrequencyId : '1';
	}

	public function setSummaryBuyerAccountId( $intSummaryBuyerAccountId ) {
		$this->set( 'm_intSummaryBuyerAccountId', CStrings::strToIntDef( $intSummaryBuyerAccountId, NULL, false ) );
	}

	public function getSummaryBuyerAccountId() {
		return $this->m_intSummaryBuyerAccountId;
	}

	public function sqlSummaryBuyerAccountId() {
		return ( true == isset( $this->m_intSummaryBuyerAccountId ) ) ? ( string ) $this->m_intSummaryBuyerAccountId : 'NULL';
	}

	public function setReplacementBuyerAccountId( $intReplacementBuyerAccountId ) {
		$this->set( 'm_intReplacementBuyerAccountId', CStrings::strToIntDef( $intReplacementBuyerAccountId, NULL, false ) );
	}

	public function getReplacementBuyerAccountId() {
		return $this->m_intReplacementBuyerAccountId;
	}

	public function sqlReplacementBuyerAccountId() {
		return ( true == isset( $this->m_intReplacementBuyerAccountId ) ) ? ( string ) $this->m_intReplacementBuyerAccountId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 50, NULL, true ) );
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function sqlAccountNumber() {
		return ( true == isset( $this->m_strAccountNumber ) ) ? '\'' . addslashes( $this->m_strAccountNumber ) . '\'' : 'NULL';
	}

	public function setAccountDescription( $strAccountDescription ) {
		$this->set( 'm_strAccountDescription', CStrings::strTrimDef( $strAccountDescription, 50, NULL, true ) );
	}

	public function getAccountDescription() {
		return $this->m_strAccountDescription;
	}

	public function sqlAccountDescription() {
		return ( true == isset( $this->m_strAccountDescription ) ) ? '\'' . addslashes( $this->m_strAccountDescription ) . '\'' : 'NULL';
	}

	public function setAuditDay( $intAuditDay ) {
		$this->set( 'm_intAuditDay', CStrings::strToIntDef( $intAuditDay, NULL, false ) );
	}

	public function getAuditDay() {
		return $this->m_intAuditDay;
	}

	public function sqlAuditDay() {
		return ( true == isset( $this->m_intAuditDay ) ) ? ( string ) $this->m_intAuditDay : 'NULL';
	}

	public function setLastInvoiceDate( $strLastInvoiceDate ) {
		$this->set( 'm_strLastInvoiceDate', CStrings::strTrimDef( $strLastInvoiceDate, -1, NULL, true ) );
	}

	public function getLastInvoiceDate() {
		return $this->m_strLastInvoiceDate;
	}

	public function sqlLastInvoiceDate() {
		return ( true == isset( $this->m_strLastInvoiceDate ) ) ? '\'' . $this->m_strLastInvoiceDate . '\'' : 'NULL';
	}

	public function setAuditStartDate( $strAuditStartDate ) {
		$this->set( 'm_strAuditStartDate', CStrings::strTrimDef( $strAuditStartDate, -1, NULL, true ) );
	}

	public function getAuditStartDate() {
		return $this->m_strAuditStartDate;
	}

	public function sqlAuditStartDate() {
		return ( true == isset( $this->m_strAuditStartDate ) ) ? '\'' . $this->m_strAuditStartDate . '\'' : 'NULL';
	}

	public function setAuditInvoice( $boolAuditInvoice ) {
		$this->set( 'm_boolAuditInvoice', CStrings::strToBool( $boolAuditInvoice ) );
	}

	public function getAuditInvoice() {
		return $this->m_boolAuditInvoice;
	}

	public function sqlAuditInvoice() {
		return ( true == isset( $this->m_boolAuditInvoice ) ) ? '\'' . ( true == ( bool ) $this->m_boolAuditInvoice ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsIgnored( $boolIsIgnored ) {
		$this->set( 'm_boolIsIgnored', CStrings::strToBool( $boolIsIgnored ) );
	}

	public function getIsIgnored() {
		return $this->m_boolIsIgnored;
	}

	public function sqlIsIgnored() {
		return ( true == isset( $this->m_boolIsIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDefaultBuyerLocationId( $intDefaultBuyerLocationId ) {
		$this->set( 'm_intDefaultBuyerLocationId', CStrings::strToIntDef( $intDefaultBuyerLocationId, NULL, false ) );
	}

	public function getDefaultBuyerLocationId() {
		return $this->m_intDefaultBuyerLocationId;
	}

	public function sqlDefaultBuyerLocationId() {
		return ( true == isset( $this->m_intDefaultBuyerLocationId ) ) ? ( string ) $this->m_intDefaultBuyerLocationId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_id, store_id, buyer_id, buyer_location_id, ap_payee_account_id, utility_bill_account_id, replacement_vendor_account_id, audit_frequency_id, summary_buyer_account_id, replacement_buyer_account_id, name, account_number, account_description, audit_day, last_invoice_date, audit_start_date, audit_invoice, is_disabled, updated_by, updated_on, created_by, created_on, is_ignored, default_buyer_location_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlVendorId() . ', ' .
						$this->sqlStoreId() . ', ' .
						$this->sqlBuyerId() . ', ' .
						$this->sqlBuyerLocationId() . ', ' .
						$this->sqlApPayeeAccountId() . ', ' .
						$this->sqlUtilityBillAccountId() . ', ' .
						$this->sqlReplacementVendorAccountId() . ', ' .
						$this->sqlAuditFrequencyId() . ', ' .
						$this->sqlSummaryBuyerAccountId() . ', ' .
						$this->sqlReplacementBuyerAccountId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlAccountNumber() . ', ' .
						$this->sqlAccountDescription() . ', ' .
						$this->sqlAuditDay() . ', ' .
						$this->sqlLastInvoiceDate() . ', ' .
						$this->sqlAuditStartDate() . ', ' .
						$this->sqlAuditInvoice() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsIgnored() . ', ' .
						$this->sqlDefaultBuyerLocationId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId(). ',' ; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' store_id = ' . $this->sqlStoreId(). ',' ; } elseif( true == array_key_exists( 'StoreId', $this->getChangedColumns() ) ) { $strSql .= ' store_id = ' . $this->sqlStoreId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_id = ' . $this->sqlBuyerId(). ',' ; } elseif( true == array_key_exists( 'BuyerId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_id = ' . $this->sqlBuyerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_location_id = ' . $this->sqlBuyerLocationId(). ',' ; } elseif( true == array_key_exists( 'BuyerLocationId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_location_id = ' . $this->sqlBuyerLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' replacement_vendor_account_id = ' . $this->sqlReplacementVendorAccountId(). ',' ; } elseif( true == array_key_exists( 'ReplacementVendorAccountId', $this->getChangedColumns() ) ) { $strSql .= ' replacement_vendor_account_id = ' . $this->sqlReplacementVendorAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_frequency_id = ' . $this->sqlAuditFrequencyId(). ',' ; } elseif( true == array_key_exists( 'AuditFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' audit_frequency_id = ' . $this->sqlAuditFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary_buyer_account_id = ' . $this->sqlSummaryBuyerAccountId(). ',' ; } elseif( true == array_key_exists( 'SummaryBuyerAccountId', $this->getChangedColumns() ) ) { $strSql .= ' summary_buyer_account_id = ' . $this->sqlSummaryBuyerAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' replacement_buyer_account_id = ' . $this->sqlReplacementBuyerAccountId(). ',' ; } elseif( true == array_key_exists( 'ReplacementBuyerAccountId', $this->getChangedColumns() ) ) { $strSql .= ' replacement_buyer_account_id = ' . $this->sqlReplacementBuyerAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber(). ',' ; } elseif( true == array_key_exists( 'AccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_description = ' . $this->sqlAccountDescription(). ',' ; } elseif( true == array_key_exists( 'AccountDescription', $this->getChangedColumns() ) ) { $strSql .= ' account_description = ' . $this->sqlAccountDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_day = ' . $this->sqlAuditDay(). ',' ; } elseif( true == array_key_exists( 'AuditDay', $this->getChangedColumns() ) ) { $strSql .= ' audit_day = ' . $this->sqlAuditDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_invoice_date = ' . $this->sqlLastInvoiceDate(). ',' ; } elseif( true == array_key_exists( 'LastInvoiceDate', $this->getChangedColumns() ) ) { $strSql .= ' last_invoice_date = ' . $this->sqlLastInvoiceDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_start_date = ' . $this->sqlAuditStartDate(). ',' ; } elseif( true == array_key_exists( 'AuditStartDate', $this->getChangedColumns() ) ) { $strSql .= ' audit_start_date = ' . $this->sqlAuditStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_invoice = ' . $this->sqlAuditInvoice(). ',' ; } elseif( true == array_key_exists( 'AuditInvoice', $this->getChangedColumns() ) ) { $strSql .= ' audit_invoice = ' . $this->sqlAuditInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ignored = ' . $this->sqlIsIgnored(). ',' ; } elseif( true == array_key_exists( 'IsIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_ignored = ' . $this->sqlIsIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_buyer_location_id = ' . $this->sqlDefaultBuyerLocationId(). ',' ; } elseif( true == array_key_exists( 'DefaultBuyerLocationId', $this->getChangedColumns() ) ) { $strSql .= ' default_buyer_location_id = ' . $this->sqlDefaultBuyerLocationId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_id' => $this->getVendorId(),
			'store_id' => $this->getStoreId(),
			'buyer_id' => $this->getBuyerId(),
			'buyer_location_id' => $this->getBuyerLocationId(),
			'ap_payee_account_id' => $this->getApPayeeAccountId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'replacement_vendor_account_id' => $this->getReplacementVendorAccountId(),
			'audit_frequency_id' => $this->getAuditFrequencyId(),
			'summary_buyer_account_id' => $this->getSummaryBuyerAccountId(),
			'replacement_buyer_account_id' => $this->getReplacementBuyerAccountId(),
			'name' => $this->getName(),
			'account_number' => $this->getAccountNumber(),
			'account_description' => $this->getAccountDescription(),
			'audit_day' => $this->getAuditDay(),
			'last_invoice_date' => $this->getLastInvoiceDate(),
			'audit_start_date' => $this->getAuditStartDate(),
			'audit_invoice' => $this->getAuditInvoice(),
			'is_disabled' => $this->getIsDisabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_ignored' => $this->getIsIgnored(),
			'default_buyer_location_id' => $this->getDefaultBuyerLocationId()
		);
	}

}
?>