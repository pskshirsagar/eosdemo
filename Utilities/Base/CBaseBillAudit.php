<?php

class CBaseBillAudit extends CEosSingularBase {

	const TABLE_NAME = 'public.bill_audits';

	protected $m_intId;
	protected $m_intUtilityBillId;
	protected $m_intUtilityBillChargeId;
	protected $m_intBillAuditTypeId;
	protected $m_intUemBillStatusTypeId;
	protected $m_intReferenceId;
	protected $m_intResolutionAuditStepId;
	protected $m_intAuditUtilityBillId;
	protected $m_intBillAuditLiabilityTypeId;
	protected $m_fltBillAuditAmount;
	protected $m_fltSavingsAmount;
	protected $m_strBillAuditMemo;
	protected $m_strLiabilityMemo;
	protected $m_strResolutionNote;
	protected $m_strReminderDatetime;
	protected $m_intBillAuditCompletedBy;
	protected $m_strBillAuditCompletedOn;
	protected $m_boolIsBilling;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsBilling = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['utility_bill_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillChargeId', trim( $arrValues['utility_bill_charge_id'] ) ); elseif( isset( $arrValues['utility_bill_charge_id'] ) ) $this->setUtilityBillChargeId( $arrValues['utility_bill_charge_id'] );
		if( isset( $arrValues['bill_audit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillAuditTypeId', trim( $arrValues['bill_audit_type_id'] ) ); elseif( isset( $arrValues['bill_audit_type_id'] ) ) $this->setBillAuditTypeId( $arrValues['bill_audit_type_id'] );
		if( isset( $arrValues['uem_bill_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUemBillStatusTypeId', trim( $arrValues['uem_bill_status_type_id'] ) ); elseif( isset( $arrValues['uem_bill_status_type_id'] ) ) $this->setUemBillStatusTypeId( $arrValues['uem_bill_status_type_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['resolution_audit_step_id'] ) && $boolDirectSet ) $this->set( 'm_intResolutionAuditStepId', trim( $arrValues['resolution_audit_step_id'] ) ); elseif( isset( $arrValues['resolution_audit_step_id'] ) ) $this->setResolutionAuditStepId( $arrValues['resolution_audit_step_id'] );
		if( isset( $arrValues['audit_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intAuditUtilityBillId', trim( $arrValues['audit_utility_bill_id'] ) ); elseif( isset( $arrValues['audit_utility_bill_id'] ) ) $this->setAuditUtilityBillId( $arrValues['audit_utility_bill_id'] );
		if( isset( $arrValues['bill_audit_liability_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillAuditLiabilityTypeId', trim( $arrValues['bill_audit_liability_type_id'] ) ); elseif( isset( $arrValues['bill_audit_liability_type_id'] ) ) $this->setBillAuditLiabilityTypeId( $arrValues['bill_audit_liability_type_id'] );
		if( isset( $arrValues['bill_audit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltBillAuditAmount', trim( $arrValues['bill_audit_amount'] ) ); elseif( isset( $arrValues['bill_audit_amount'] ) ) $this->setBillAuditAmount( $arrValues['bill_audit_amount'] );
		if( isset( $arrValues['savings_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSavingsAmount', trim( $arrValues['savings_amount'] ) ); elseif( isset( $arrValues['savings_amount'] ) ) $this->setSavingsAmount( $arrValues['savings_amount'] );
		if( isset( $arrValues['bill_audit_memo'] ) && $boolDirectSet ) $this->set( 'm_strBillAuditMemo', trim( stripcslashes( $arrValues['bill_audit_memo'] ) ) ); elseif( isset( $arrValues['bill_audit_memo'] ) ) $this->setBillAuditMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bill_audit_memo'] ) : $arrValues['bill_audit_memo'] );
		if( isset( $arrValues['liability_memo'] ) && $boolDirectSet ) $this->set( 'm_strLiabilityMemo', trim( stripcslashes( $arrValues['liability_memo'] ) ) ); elseif( isset( $arrValues['liability_memo'] ) ) $this->setLiabilityMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['liability_memo'] ) : $arrValues['liability_memo'] );
		if( isset( $arrValues['resolution_note'] ) && $boolDirectSet ) $this->set( 'm_strResolutionNote', trim( stripcslashes( $arrValues['resolution_note'] ) ) ); elseif( isset( $arrValues['resolution_note'] ) ) $this->setResolutionNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['resolution_note'] ) : $arrValues['resolution_note'] );
		if( isset( $arrValues['reminder_datetime'] ) && $boolDirectSet ) $this->set( 'm_strReminderDatetime', trim( $arrValues['reminder_datetime'] ) ); elseif( isset( $arrValues['reminder_datetime'] ) ) $this->setReminderDatetime( $arrValues['reminder_datetime'] );
		if( isset( $arrValues['bill_audit_completed_by'] ) && $boolDirectSet ) $this->set( 'm_intBillAuditCompletedBy', trim( $arrValues['bill_audit_completed_by'] ) ); elseif( isset( $arrValues['bill_audit_completed_by'] ) ) $this->setBillAuditCompletedBy( $arrValues['bill_audit_completed_by'] );
		if( isset( $arrValues['bill_audit_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strBillAuditCompletedOn', trim( $arrValues['bill_audit_completed_on'] ) ); elseif( isset( $arrValues['bill_audit_completed_on'] ) ) $this->setBillAuditCompletedOn( $arrValues['bill_audit_completed_on'] );
		if( isset( $arrValues['is_billing'] ) && $boolDirectSet ) $this->set( 'm_boolIsBilling', trim( stripcslashes( $arrValues['is_billing'] ) ) ); elseif( isset( $arrValues['is_billing'] ) ) $this->setIsBilling( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_billing'] ) : $arrValues['is_billing'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setUtilityBillChargeId( $intUtilityBillChargeId ) {
		$this->set( 'm_intUtilityBillChargeId', CStrings::strToIntDef( $intUtilityBillChargeId, NULL, false ) );
	}

	public function getUtilityBillChargeId() {
		return $this->m_intUtilityBillChargeId;
	}

	public function sqlUtilityBillChargeId() {
		return ( true == isset( $this->m_intUtilityBillChargeId ) ) ? ( string ) $this->m_intUtilityBillChargeId : 'NULL';
	}

	public function setBillAuditTypeId( $intBillAuditTypeId ) {
		$this->set( 'm_intBillAuditTypeId', CStrings::strToIntDef( $intBillAuditTypeId, NULL, false ) );
	}

	public function getBillAuditTypeId() {
		return $this->m_intBillAuditTypeId;
	}

	public function sqlBillAuditTypeId() {
		return ( true == isset( $this->m_intBillAuditTypeId ) ) ? ( string ) $this->m_intBillAuditTypeId : 'NULL';
	}

	public function setUemBillStatusTypeId( $intUemBillStatusTypeId ) {
		$this->set( 'm_intUemBillStatusTypeId', CStrings::strToIntDef( $intUemBillStatusTypeId, NULL, false ) );
	}

	public function getUemBillStatusTypeId() {
		return $this->m_intUemBillStatusTypeId;
	}

	public function sqlUemBillStatusTypeId() {
		return ( true == isset( $this->m_intUemBillStatusTypeId ) ) ? ( string ) $this->m_intUemBillStatusTypeId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setResolutionAuditStepId( $intResolutionAuditStepId ) {
		$this->set( 'm_intResolutionAuditStepId', CStrings::strToIntDef( $intResolutionAuditStepId, NULL, false ) );
	}

	public function getResolutionAuditStepId() {
		return $this->m_intResolutionAuditStepId;
	}

	public function sqlResolutionAuditStepId() {
		return ( true == isset( $this->m_intResolutionAuditStepId ) ) ? ( string ) $this->m_intResolutionAuditStepId : 'NULL';
	}

	public function setAuditUtilityBillId( $intAuditUtilityBillId ) {
		$this->set( 'm_intAuditUtilityBillId', CStrings::strToIntDef( $intAuditUtilityBillId, NULL, false ) );
	}

	public function getAuditUtilityBillId() {
		return $this->m_intAuditUtilityBillId;
	}

	public function sqlAuditUtilityBillId() {
		return ( true == isset( $this->m_intAuditUtilityBillId ) ) ? ( string ) $this->m_intAuditUtilityBillId : 'NULL';
	}

	public function setBillAuditLiabilityTypeId( $intBillAuditLiabilityTypeId ) {
		$this->set( 'm_intBillAuditLiabilityTypeId', CStrings::strToIntDef( $intBillAuditLiabilityTypeId, NULL, false ) );
	}

	public function getBillAuditLiabilityTypeId() {
		return $this->m_intBillAuditLiabilityTypeId;
	}

	public function sqlBillAuditLiabilityTypeId() {
		return ( true == isset( $this->m_intBillAuditLiabilityTypeId ) ) ? ( string ) $this->m_intBillAuditLiabilityTypeId : 'NULL';
	}

	public function setBillAuditAmount( $fltBillAuditAmount ) {
		$this->set( 'm_fltBillAuditAmount', CStrings::strToFloatDef( $fltBillAuditAmount, NULL, false, 2 ) );
	}

	public function getBillAuditAmount() {
		return $this->m_fltBillAuditAmount;
	}

	public function sqlBillAuditAmount() {
		return ( true == isset( $this->m_fltBillAuditAmount ) ) ? ( string ) $this->m_fltBillAuditAmount : 'NULL';
	}

	public function setSavingsAmount( $fltSavingsAmount ) {
		$this->set( 'm_fltSavingsAmount', CStrings::strToFloatDef( $fltSavingsAmount, NULL, false, 2 ) );
	}

	public function getSavingsAmount() {
		return $this->m_fltSavingsAmount;
	}

	public function sqlSavingsAmount() {
		return ( true == isset( $this->m_fltSavingsAmount ) ) ? ( string ) $this->m_fltSavingsAmount : 'NULL';
	}

	public function setBillAuditMemo( $strBillAuditMemo ) {
		$this->set( 'm_strBillAuditMemo', CStrings::strTrimDef( $strBillAuditMemo, -1, NULL, true ) );
	}

	public function getBillAuditMemo() {
		return $this->m_strBillAuditMemo;
	}

	public function sqlBillAuditMemo() {
		return ( true == isset( $this->m_strBillAuditMemo ) ) ? '\'' . addslashes( $this->m_strBillAuditMemo ) . '\'' : 'NULL';
	}

	public function setLiabilityMemo( $strLiabilityMemo ) {
		$this->set( 'm_strLiabilityMemo', CStrings::strTrimDef( $strLiabilityMemo, -1, NULL, true ) );
	}

	public function getLiabilityMemo() {
		return $this->m_strLiabilityMemo;
	}

	public function sqlLiabilityMemo() {
		return ( true == isset( $this->m_strLiabilityMemo ) ) ? '\'' . addslashes( $this->m_strLiabilityMemo ) . '\'' : 'NULL';
	}

	public function setResolutionNote( $strResolutionNote ) {
		$this->set( 'm_strResolutionNote', CStrings::strTrimDef( $strResolutionNote, -1, NULL, true ) );
	}

	public function getResolutionNote() {
		return $this->m_strResolutionNote;
	}

	public function sqlResolutionNote() {
		return ( true == isset( $this->m_strResolutionNote ) ) ? '\'' . addslashes( $this->m_strResolutionNote ) . '\'' : 'NULL';
	}

	public function setReminderDatetime( $strReminderDatetime ) {
		$this->set( 'm_strReminderDatetime', CStrings::strTrimDef( $strReminderDatetime, -1, NULL, true ) );
	}

	public function getReminderDatetime() {
		return $this->m_strReminderDatetime;
	}

	public function sqlReminderDatetime() {
		return ( true == isset( $this->m_strReminderDatetime ) ) ? '\'' . $this->m_strReminderDatetime . '\'' : 'NULL';
	}

	public function setBillAuditCompletedBy( $intBillAuditCompletedBy ) {
		$this->set( 'm_intBillAuditCompletedBy', CStrings::strToIntDef( $intBillAuditCompletedBy, NULL, false ) );
	}

	public function getBillAuditCompletedBy() {
		return $this->m_intBillAuditCompletedBy;
	}

	public function sqlBillAuditCompletedBy() {
		return ( true == isset( $this->m_intBillAuditCompletedBy ) ) ? ( string ) $this->m_intBillAuditCompletedBy : 'NULL';
	}

	public function setBillAuditCompletedOn( $strBillAuditCompletedOn ) {
		$this->set( 'm_strBillAuditCompletedOn', CStrings::strTrimDef( $strBillAuditCompletedOn, -1, NULL, true ) );
	}

	public function getBillAuditCompletedOn() {
		return $this->m_strBillAuditCompletedOn;
	}

	public function sqlBillAuditCompletedOn() {
		return ( true == isset( $this->m_strBillAuditCompletedOn ) ) ? '\'' . $this->m_strBillAuditCompletedOn . '\'' : 'NULL';
	}

	public function setIsBilling( $boolIsBilling ) {
		$this->set( 'm_boolIsBilling', CStrings::strToBool( $boolIsBilling ) );
	}

	public function getIsBilling() {
		return $this->m_boolIsBilling;
	}

	public function sqlIsBilling() {
		return ( true == isset( $this->m_boolIsBilling ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBilling ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_bill_id, utility_bill_charge_id, bill_audit_type_id, uem_bill_status_type_id, reference_id, resolution_audit_step_id, audit_utility_bill_id, bill_audit_liability_type_id, bill_audit_amount, savings_amount, bill_audit_memo, liability_memo, resolution_note, reminder_datetime, bill_audit_completed_by, bill_audit_completed_on, is_billing, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUtilityBillId() . ', ' .
 						$this->sqlUtilityBillChargeId() . ', ' .
 						$this->sqlBillAuditTypeId() . ', ' .
 						$this->sqlUemBillStatusTypeId() . ', ' .
 						$this->sqlReferenceId() . ', ' .
 						$this->sqlResolutionAuditStepId() . ', ' .
 						$this->sqlAuditUtilityBillId() . ', ' .
 						$this->sqlBillAuditLiabilityTypeId() . ', ' .
 						$this->sqlBillAuditAmount() . ', ' .
 						$this->sqlSavingsAmount() . ', ' .
 						$this->sqlBillAuditMemo() . ', ' .
 						$this->sqlLiabilityMemo() . ', ' .
 						$this->sqlResolutionNote() . ', ' .
 						$this->sqlReminderDatetime() . ', ' .
 						$this->sqlBillAuditCompletedBy() . ', ' .
 						$this->sqlBillAuditCompletedOn() . ', ' .
 						$this->sqlIsBilling() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_charge_id = ' . $this->sqlUtilityBillChargeId() . ','; } elseif( true == array_key_exists( 'UtilityBillChargeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_charge_id = ' . $this->sqlUtilityBillChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_type_id = ' . $this->sqlBillAuditTypeId() . ','; } elseif( true == array_key_exists( 'BillAuditTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_type_id = ' . $this->sqlBillAuditTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uem_bill_status_type_id = ' . $this->sqlUemBillStatusTypeId() . ','; } elseif( true == array_key_exists( 'UemBillStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' uem_bill_status_type_id = ' . $this->sqlUemBillStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolution_audit_step_id = ' . $this->sqlResolutionAuditStepId() . ','; } elseif( true == array_key_exists( 'ResolutionAuditStepId', $this->getChangedColumns() ) ) { $strSql .= ' resolution_audit_step_id = ' . $this->sqlResolutionAuditStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_utility_bill_id = ' . $this->sqlAuditUtilityBillId() . ','; } elseif( true == array_key_exists( 'AuditUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' audit_utility_bill_id = ' . $this->sqlAuditUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_liability_type_id = ' . $this->sqlBillAuditLiabilityTypeId() . ','; } elseif( true == array_key_exists( 'BillAuditLiabilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_liability_type_id = ' . $this->sqlBillAuditLiabilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_amount = ' . $this->sqlBillAuditAmount() . ','; } elseif( true == array_key_exists( 'BillAuditAmount', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_amount = ' . $this->sqlBillAuditAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' savings_amount = ' . $this->sqlSavingsAmount() . ','; } elseif( true == array_key_exists( 'SavingsAmount', $this->getChangedColumns() ) ) { $strSql .= ' savings_amount = ' . $this->sqlSavingsAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_memo = ' . $this->sqlBillAuditMemo() . ','; } elseif( true == array_key_exists( 'BillAuditMemo', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_memo = ' . $this->sqlBillAuditMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' liability_memo = ' . $this->sqlLiabilityMemo() . ','; } elseif( true == array_key_exists( 'LiabilityMemo', $this->getChangedColumns() ) ) { $strSql .= ' liability_memo = ' . $this->sqlLiabilityMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolution_note = ' . $this->sqlResolutionNote() . ','; } elseif( true == array_key_exists( 'ResolutionNote', $this->getChangedColumns() ) ) { $strSql .= ' resolution_note = ' . $this->sqlResolutionNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reminder_datetime = ' . $this->sqlReminderDatetime() . ','; } elseif( true == array_key_exists( 'ReminderDatetime', $this->getChangedColumns() ) ) { $strSql .= ' reminder_datetime = ' . $this->sqlReminderDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_completed_by = ' . $this->sqlBillAuditCompletedBy() . ','; } elseif( true == array_key_exists( 'BillAuditCompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_completed_by = ' . $this->sqlBillAuditCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_completed_on = ' . $this->sqlBillAuditCompletedOn() . ','; } elseif( true == array_key_exists( 'BillAuditCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_completed_on = ' . $this->sqlBillAuditCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_billing = ' . $this->sqlIsBilling() . ','; } elseif( true == array_key_exists( 'IsBilling', $this->getChangedColumns() ) ) { $strSql .= ' is_billing = ' . $this->sqlIsBilling() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'utility_bill_charge_id' => $this->getUtilityBillChargeId(),
			'bill_audit_type_id' => $this->getBillAuditTypeId(),
			'uem_bill_status_type_id' => $this->getUemBillStatusTypeId(),
			'reference_id' => $this->getReferenceId(),
			'resolution_audit_step_id' => $this->getResolutionAuditStepId(),
			'audit_utility_bill_id' => $this->getAuditUtilityBillId(),
			'bill_audit_liability_type_id' => $this->getBillAuditLiabilityTypeId(),
			'bill_audit_amount' => $this->getBillAuditAmount(),
			'savings_amount' => $this->getSavingsAmount(),
			'bill_audit_memo' => $this->getBillAuditMemo(),
			'liability_memo' => $this->getLiabilityMemo(),
			'resolution_note' => $this->getResolutionNote(),
			'reminder_datetime' => $this->getReminderDatetime(),
			'bill_audit_completed_by' => $this->getBillAuditCompletedBy(),
			'bill_audit_completed_on' => $this->getBillAuditCompletedOn(),
			'is_billing' => $this->getIsBilling(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>