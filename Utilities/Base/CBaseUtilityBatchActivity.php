<?php

class CBaseUtilityBatchActivity extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_batch_activities';

	protected $m_intId;
	protected $m_intUtilityBatchActivityTypeId;
	protected $m_intUtilityBatchId;
	protected $m_intUtilityBatchStepId;
	protected $m_intEmployeeUserId;
	protected $m_intCompanyUserId;
	protected $m_strMemo;
	protected $m_strErrorNote;
	protected $m_boolIsFailed;
	protected $m_strUtilityBatchLogDatetime;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsFailed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_batch_activity_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchActivityTypeId', trim( $arrValues['utility_batch_activity_type_id'] ) ); elseif( isset( $arrValues['utility_batch_activity_type_id'] ) ) $this->setUtilityBatchActivityTypeId( $arrValues['utility_batch_activity_type_id'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['utility_batch_step_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchStepId', trim( $arrValues['utility_batch_step_id'] ) ); elseif( isset( $arrValues['utility_batch_step_id'] ) ) $this->setUtilityBatchStepId( $arrValues['utility_batch_step_id'] );
		if( isset( $arrValues['employee_user_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeUserId', trim( $arrValues['employee_user_id'] ) ); elseif( isset( $arrValues['employee_user_id'] ) ) $this->setEmployeeUserId( $arrValues['employee_user_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['error_note'] ) && $boolDirectSet ) $this->set( 'm_strErrorNote', trim( stripcslashes( $arrValues['error_note'] ) ) ); elseif( isset( $arrValues['error_note'] ) ) $this->setErrorNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_note'] ) : $arrValues['error_note'] );
		if( isset( $arrValues['is_failed'] ) && $boolDirectSet ) $this->set( 'm_boolIsFailed', trim( stripcslashes( $arrValues['is_failed'] ) ) ); elseif( isset( $arrValues['is_failed'] ) ) $this->setIsFailed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_failed'] ) : $arrValues['is_failed'] );
		if( isset( $arrValues['utility_batch_log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strUtilityBatchLogDatetime', trim( $arrValues['utility_batch_log_datetime'] ) ); elseif( isset( $arrValues['utility_batch_log_datetime'] ) ) $this->setUtilityBatchLogDatetime( $arrValues['utility_batch_log_datetime'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBatchActivityTypeId( $intUtilityBatchActivityTypeId ) {
		$this->set( 'm_intUtilityBatchActivityTypeId', CStrings::strToIntDef( $intUtilityBatchActivityTypeId, NULL, false ) );
	}

	public function getUtilityBatchActivityTypeId() {
		return $this->m_intUtilityBatchActivityTypeId;
	}

	public function sqlUtilityBatchActivityTypeId() {
		return ( true == isset( $this->m_intUtilityBatchActivityTypeId ) ) ? ( string ) $this->m_intUtilityBatchActivityTypeId : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setUtilityBatchStepId( $intUtilityBatchStepId ) {
		$this->set( 'm_intUtilityBatchStepId', CStrings::strToIntDef( $intUtilityBatchStepId, NULL, false ) );
	}

	public function getUtilityBatchStepId() {
		return $this->m_intUtilityBatchStepId;
	}

	public function sqlUtilityBatchStepId() {
		return ( true == isset( $this->m_intUtilityBatchStepId ) ) ? ( string ) $this->m_intUtilityBatchStepId : 'NULL';
	}

	public function setEmployeeUserId( $intEmployeeUserId ) {
		$this->set( 'm_intEmployeeUserId', CStrings::strToIntDef( $intEmployeeUserId, NULL, false ) );
	}

	public function getEmployeeUserId() {
		return $this->m_intEmployeeUserId;
	}

	public function sqlEmployeeUserId() {
		return ( true == isset( $this->m_intEmployeeUserId ) ) ? ( string ) $this->m_intEmployeeUserId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, -1, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setErrorNote( $strErrorNote ) {
		$this->set( 'm_strErrorNote', CStrings::strTrimDef( $strErrorNote, -1, NULL, true ) );
	}

	public function getErrorNote() {
		return $this->m_strErrorNote;
	}

	public function sqlErrorNote() {
		return ( true == isset( $this->m_strErrorNote ) ) ? '\'' . addslashes( $this->m_strErrorNote ) . '\'' : 'NULL';
	}

	public function setIsFailed( $boolIsFailed ) {
		$this->set( 'm_boolIsFailed', CStrings::strToBool( $boolIsFailed ) );
	}

	public function getIsFailed() {
		return $this->m_boolIsFailed;
	}

	public function sqlIsFailed() {
		return ( true == isset( $this->m_boolIsFailed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFailed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUtilityBatchLogDatetime( $strUtilityBatchLogDatetime ) {
		$this->set( 'm_strUtilityBatchLogDatetime', CStrings::strTrimDef( $strUtilityBatchLogDatetime, -1, NULL, true ) );
	}

	public function getUtilityBatchLogDatetime() {
		return $this->m_strUtilityBatchLogDatetime;
	}

	public function sqlUtilityBatchLogDatetime() {
		return ( true == isset( $this->m_strUtilityBatchLogDatetime ) ) ? '\'' . $this->m_strUtilityBatchLogDatetime . '\'' : 'NOW()';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_batch_activity_type_id, utility_batch_id, utility_batch_step_id, employee_user_id, company_user_id, memo, error_note, is_failed, utility_batch_log_datetime, approved_by, approved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUtilityBatchActivityTypeId() . ', ' .
 						$this->sqlUtilityBatchId() . ', ' .
 						$this->sqlUtilityBatchStepId() . ', ' .
 						$this->sqlEmployeeUserId() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlMemo() . ', ' .
 						$this->sqlErrorNote() . ', ' .
 						$this->sqlIsFailed() . ', ' .
 						$this->sqlUtilityBatchLogDatetime() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_activity_type_id = ' . $this->sqlUtilityBatchActivityTypeId() . ','; } elseif( true == array_key_exists( 'UtilityBatchActivityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_activity_type_id = ' . $this->sqlUtilityBatchActivityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_step_id = ' . $this->sqlUtilityBatchStepId() . ','; } elseif( true == array_key_exists( 'UtilityBatchStepId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_step_id = ' . $this->sqlUtilityBatchStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_user_id = ' . $this->sqlEmployeeUserId() . ','; } elseif( true == array_key_exists( 'EmployeeUserId', $this->getChangedColumns() ) ) { $strSql .= ' employee_user_id = ' . $this->sqlEmployeeUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_note = ' . $this->sqlErrorNote() . ','; } elseif( true == array_key_exists( 'ErrorNote', $this->getChangedColumns() ) ) { $strSql .= ' error_note = ' . $this->sqlErrorNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; } elseif( true == array_key_exists( 'IsFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_log_datetime = ' . $this->sqlUtilityBatchLogDatetime() . ','; } elseif( true == array_key_exists( 'UtilityBatchLogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_log_datetime = ' . $this->sqlUtilityBatchLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_batch_activity_type_id' => $this->getUtilityBatchActivityTypeId(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'utility_batch_step_id' => $this->getUtilityBatchStepId(),
			'employee_user_id' => $this->getEmployeeUserId(),
			'company_user_id' => $this->getCompanyUserId(),
			'memo' => $this->getMemo(),
			'error_note' => $this->getErrorNote(),
			'is_failed' => $this->getIsFailed(),
			'utility_batch_log_datetime' => $this->getUtilityBatchLogDatetime(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>