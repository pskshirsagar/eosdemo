<?php

class CBaseCheckRemittanceDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.check_remittance_details';

	protected $m_intId;
	protected $m_intCheckPaymentId;
	protected $m_intPropertyId;
	protected $m_strPropertyName;
	protected $m_strInvoiceDate;
	protected $m_strInvoiceNumber;
	protected $m_strAccountNumber;
	protected $m_fltAmountDue;
	protected $m_fltAmountPaid;
	protected $m_strDescription;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['check_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckPaymentId', trim( $arrValues['check_payment_id'] ) ); elseif( isset( $arrValues['check_payment_id'] ) ) $this->setCheckPaymentId( $arrValues['check_payment_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['invoice_date'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceDate', trim( $arrValues['invoice_date'] ) ); elseif( isset( $arrValues['invoice_date'] ) ) $this->setInvoiceDate( $arrValues['invoice_date'] );
		if( isset( $arrValues['invoice_number'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceNumber', trim( stripcslashes( $arrValues['invoice_number'] ) ) ); elseif( isset( $arrValues['invoice_number'] ) ) $this->setInvoiceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['invoice_number'] ) : $arrValues['invoice_number'] );
		if( isset( $arrValues['account_number'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumber', trim( stripcslashes( $arrValues['account_number'] ) ) ); elseif( isset( $arrValues['account_number'] ) ) $this->setAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number'] ) : $arrValues['account_number'] );
		if( isset( $arrValues['amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltAmountDue', trim( $arrValues['amount_due'] ) ); elseif( isset( $arrValues['amount_due'] ) ) $this->setAmountDue( $arrValues['amount_due'] );
		if( isset( $arrValues['amount_paid'] ) && $boolDirectSet ) $this->set( 'm_fltAmountPaid', trim( $arrValues['amount_paid'] ) ); elseif( isset( $arrValues['amount_paid'] ) ) $this->setAmountPaid( $arrValues['amount_paid'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCheckPaymentId( $intCheckPaymentId ) {
		$this->set( 'm_intCheckPaymentId', CStrings::strToIntDef( $intCheckPaymentId, NULL, false ) );
	}

	public function getCheckPaymentId() {
		return $this->m_intCheckPaymentId;
	}

	public function sqlCheckPaymentId() {
		return ( true == isset( $this->m_intCheckPaymentId ) ) ? ( string ) $this->m_intCheckPaymentId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 100, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setInvoiceDate( $strInvoiceDate ) {
		$this->set( 'm_strInvoiceDate', CStrings::strTrimDef( $strInvoiceDate, -1, NULL, true ) );
	}

	public function getInvoiceDate() {
		return $this->m_strInvoiceDate;
	}

	public function sqlInvoiceDate() {
		return ( true == isset( $this->m_strInvoiceDate ) ) ? '\'' . $this->m_strInvoiceDate . '\'' : 'NULL';
	}

	public function setInvoiceNumber( $strInvoiceNumber ) {
		$this->set( 'm_strInvoiceNumber', CStrings::strTrimDef( $strInvoiceNumber, 100, NULL, true ) );
	}

	public function getInvoiceNumber() {
		return $this->m_strInvoiceNumber;
	}

	public function sqlInvoiceNumber() {
		return ( true == isset( $this->m_strInvoiceNumber ) ) ? '\'' . addslashes( $this->m_strInvoiceNumber ) . '\'' : 'NULL';
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 100, NULL, true ) );
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function sqlAccountNumber() {
		return ( true == isset( $this->m_strAccountNumber ) ) ? '\'' . addslashes( $this->m_strAccountNumber ) . '\'' : 'NULL';
	}

	public function setAmountDue( $fltAmountDue ) {
		$this->set( 'm_fltAmountDue', CStrings::strToFloatDef( $fltAmountDue, NULL, false, 2 ) );
	}

	public function getAmountDue() {
		return $this->m_fltAmountDue;
	}

	public function sqlAmountDue() {
		return ( true == isset( $this->m_fltAmountDue ) ) ? ( string ) $this->m_fltAmountDue : 'NULL';
	}

	public function setAmountPaid( $fltAmountPaid ) {
		$this->set( 'm_fltAmountPaid', CStrings::strToFloatDef( $fltAmountPaid, NULL, false, 2 ) );
	}

	public function getAmountPaid() {
		return $this->m_fltAmountPaid;
	}

	public function sqlAmountPaid() {
		return ( true == isset( $this->m_fltAmountPaid ) ) ? ( string ) $this->m_fltAmountPaid : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 100, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, check_payment_id, property_id, property_name, invoice_date, invoice_number, account_number, amount_due, amount_paid, description, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCheckPaymentId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyName() . ', ' .
 						$this->sqlInvoiceDate() . ', ' .
 						$this->sqlInvoiceNumber() . ', ' .
 						$this->sqlAccountNumber() . ', ' .
 						$this->sqlAmountDue() . ', ' .
 						$this->sqlAmountPaid() . ', ' .
 						$this->sqlDescription() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_payment_id = ' . $this->sqlCheckPaymentId() . ','; } elseif( true == array_key_exists( 'CheckPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' check_payment_id = ' . $this->sqlCheckPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_date = ' . $this->sqlInvoiceDate() . ','; } elseif( true == array_key_exists( 'InvoiceDate', $this->getChangedColumns() ) ) { $strSql .= ' invoice_date = ' . $this->sqlInvoiceDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_number = ' . $this->sqlInvoiceNumber() . ','; } elseif( true == array_key_exists( 'InvoiceNumber', $this->getChangedColumns() ) ) { $strSql .= ' invoice_number = ' . $this->sqlInvoiceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; } elseif( true == array_key_exists( 'AccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount_due = ' . $this->sqlAmountDue() . ','; } elseif( true == array_key_exists( 'AmountDue', $this->getChangedColumns() ) ) { $strSql .= ' amount_due = ' . $this->sqlAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount_paid = ' . $this->sqlAmountPaid() . ','; } elseif( true == array_key_exists( 'AmountPaid', $this->getChangedColumns() ) ) { $strSql .= ' amount_paid = ' . $this->sqlAmountPaid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'check_payment_id' => $this->getCheckPaymentId(),
			'property_id' => $this->getPropertyId(),
			'property_name' => $this->getPropertyName(),
			'invoice_date' => $this->getInvoiceDate(),
			'invoice_number' => $this->getInvoiceNumber(),
			'account_number' => $this->getAccountNumber(),
			'amount_due' => $this->getAmountDue(),
			'amount_paid' => $this->getAmountPaid(),
			'description' => $this->getDescription(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>