<?php

class CBaseUtilityBillAuditResolution extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_audit_resolutions';

	protected $m_intId;
	protected $m_intUtilityBillAuditTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsClientFacing;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsClientFacing = false;
		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_bill_audit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAuditTypeId', trim( $arrValues['utility_bill_audit_type_id'] ) ); elseif( isset( $arrValues['utility_bill_audit_type_id'] ) ) $this->setUtilityBillAuditTypeId( $arrValues['utility_bill_audit_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_client_facing'] ) && $boolDirectSet ) $this->set( 'm_boolIsClientFacing', trim( stripcslashes( $arrValues['is_client_facing'] ) ) ); elseif( isset( $arrValues['is_client_facing'] ) ) $this->setIsClientFacing( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_client_facing'] ) : $arrValues['is_client_facing'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBillAuditTypeId( $intUtilityBillAuditTypeId ) {
		$this->set( 'm_intUtilityBillAuditTypeId', CStrings::strToIntDef( $intUtilityBillAuditTypeId, NULL, false ) );
	}

	public function getUtilityBillAuditTypeId() {
		return $this->m_intUtilityBillAuditTypeId;
	}

	public function sqlUtilityBillAuditTypeId() {
		return ( true == isset( $this->m_intUtilityBillAuditTypeId ) ) ? ( string ) $this->m_intUtilityBillAuditTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsClientFacing( $boolIsClientFacing ) {
		$this->set( 'm_boolIsClientFacing', CStrings::strToBool( $boolIsClientFacing ) );
	}

	public function getIsClientFacing() {
		return $this->m_boolIsClientFacing;
	}

	public function sqlIsClientFacing() {
		return ( true == isset( $this->m_boolIsClientFacing ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClientFacing ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_bill_audit_type_id' => $this->getUtilityBillAuditTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_client_facing' => $this->getIsClientFacing(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>