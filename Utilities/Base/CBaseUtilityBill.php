<?php

class CBaseUtilityBill extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bills';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillTemplateId;
	protected $m_intUtilityBillTypeId;
	protected $m_intUtilityBillReceiptTypeId;
	protected $m_intUtilityBillAccountId;
	protected $m_intSummaryUtilityBillId;
	protected $m_intEstimatedUtilityBillId;
	protected $m_intTransactionId;
	protected $m_intUtilityDocumentId;
	protected $m_intApPayeeId;
	protected $m_intApPayeeLocationId;
	protected $m_intApPayeeAccountId;
	protected $m_intApHeaderId;
	protected $m_intReviewUtilityBillId;
	protected $m_intPsProductId;
	protected $m_intApHeaderSubTypeId;
	protected $m_intApExceptionQueueItemId;
	protected $m_arrintPoApHeaderIds;
	protected $m_intSummaryUtilityBillPageNumber;
	protected $m_strBillDatetime;
	protected $m_strBillNumber;
	protected $m_fltCurrentAmount;
	protected $m_fltTotalAmount;
	protected $m_strBillDate;
	protected $m_strDueDate;
	protected $m_strBillingStartDate;
	protected $m_strBillingEndDate;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strNotes;
	protected $m_boolIsHistorical;
	protected $m_boolIsQueueIgnored;
	protected $m_boolIsIgnoreInvoiceTracking;
	protected $m_boolIsOcred;
	protected $m_boolIsNoBillAvailable;
	protected $m_strStartProcessDatetime;
	protected $m_strEndProcessDatetime;
	protected $m_intLastProcessedBy;
	protected $m_strStartAssociateDatetime;
	protected $m_strEndAssociateDatetime;
	protected $m_intLastAssociatedBy;
	protected $m_intProcessedBy;
	protected $m_strProcessedOn;
	protected $m_intProcessedAgainBy;
	protected $m_strProcessedAgainOn;
	protected $m_strProcessedOnTime;
	protected $m_strProcessedAgainOnTime;
	protected $m_strStartReviewDatetime;
	protected $m_strEndReviewDatetime;
	protected $m_strScheduledExportDate;
	protected $m_intLastReviewedBy;
	protected $m_intExportedBy;
	protected $m_strExportedOn;
	protected $m_intAssociatedBy;
	protected $m_strAssociatedOn;
	protected $m_intAssociatedAgainBy;
	protected $m_strAssociatedAgainOn;
	protected $m_intSplitBy;
	protected $m_strSplitOn;
	protected $m_intSplitAgainBy;
	protected $m_strSplitAgainOn;
	protected $m_intReviewedBy;
	protected $m_strReviewedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intExceptionQueueItemCanceledBy;
	protected $m_strExceptionQueueItemCanceledOn;
	protected $m_strExceptionQueueItemNote;
	protected $m_intOcrTemplateId;
	protected $m_strDeletionReason;
	protected $m_intCopyUtilityBillId;
	protected $m_intBillingAllocationId;
	protected $m_intImportedApHeaderId;
	protected $m_intUtilityBillPullId;
	protected $m_boolIgnoreThirdPartyDataEntry;
	protected $m_fltPriorBalance;
	protected $m_fltPayments;
	protected $m_fltAdjustments;
	protected $m_fltBalanceForward;
	protected $m_boolIsForceExport;
	protected $m_fltBudgetBalanceAmount;
	protected $m_boolIsDueUponReceipt;
	protected $m_boolIsDoNotPay;
	protected $m_boolNoDueDate;
	protected $m_boolNoBillDate;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsHistorical = false;
		$this->m_boolIsQueueIgnored = false;
		$this->m_boolIsIgnoreInvoiceTracking = false;
		$this->m_boolIsOcred = false;
		$this->m_boolIsNoBillAvailable = false;
		$this->m_boolIgnoreThirdPartyDataEntry = false;
		$this->m_boolIsForceExport = false;
		$this->m_boolIsDueUponReceipt = false;
		$this->m_boolIsDoNotPay = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_template_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillTemplateId', trim( $arrValues['utility_bill_template_id'] ) ); elseif( isset( $arrValues['utility_bill_template_id'] ) ) $this->setUtilityBillTemplateId( $arrValues['utility_bill_template_id'] );
		if( isset( $arrValues['utility_bill_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillTypeId', trim( $arrValues['utility_bill_type_id'] ) ); elseif( isset( $arrValues['utility_bill_type_id'] ) ) $this->setUtilityBillTypeId( $arrValues['utility_bill_type_id'] );
		if( isset( $arrValues['utility_bill_receipt_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillReceiptTypeId', trim( $arrValues['utility_bill_receipt_type_id'] ) ); elseif( isset( $arrValues['utility_bill_receipt_type_id'] ) ) $this->setUtilityBillReceiptTypeId( $arrValues['utility_bill_receipt_type_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['summary_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intSummaryUtilityBillId', trim( $arrValues['summary_utility_bill_id'] ) ); elseif( isset( $arrValues['summary_utility_bill_id'] ) ) $this->setSummaryUtilityBillId( $arrValues['summary_utility_bill_id'] );
		if( isset( $arrValues['estimated_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intEstimatedUtilityBillId', trim( $arrValues['estimated_utility_bill_id'] ) ); elseif( isset( $arrValues['estimated_utility_bill_id'] ) ) $this->setEstimatedUtilityBillId( $arrValues['estimated_utility_bill_id'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['utility_document_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDocumentId', trim( $arrValues['utility_document_id'] ) ); elseif( isset( $arrValues['utility_document_id'] ) ) $this->setUtilityDocumentId( $arrValues['utility_document_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['ap_payee_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeAccountId', trim( $arrValues['ap_payee_account_id'] ) ); elseif( isset( $arrValues['ap_payee_account_id'] ) ) $this->setApPayeeAccountId( $arrValues['ap_payee_account_id'] );
		if( isset( $arrValues['ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderId', trim( $arrValues['ap_header_id'] ) ); elseif( isset( $arrValues['ap_header_id'] ) ) $this->setApHeaderId( $arrValues['ap_header_id'] );
		if( isset( $arrValues['review_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewUtilityBillId', trim( $arrValues['review_utility_bill_id'] ) ); elseif( isset( $arrValues['review_utility_bill_id'] ) ) $this->setReviewUtilityBillId( $arrValues['review_utility_bill_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ap_header_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderSubTypeId', trim( $arrValues['ap_header_sub_type_id'] ) ); elseif( isset( $arrValues['ap_header_sub_type_id'] ) ) $this->setApHeaderSubTypeId( $arrValues['ap_header_sub_type_id'] );
		if( isset( $arrValues['ap_exception_queue_item_id'] ) && $boolDirectSet ) $this->set( 'm_intApExceptionQueueItemId', trim( $arrValues['ap_exception_queue_item_id'] ) ); elseif( isset( $arrValues['ap_exception_queue_item_id'] ) ) $this->setApExceptionQueueItemId( $arrValues['ap_exception_queue_item_id'] );
		if( isset( $arrValues['po_ap_header_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintPoApHeaderIds', trim( $arrValues['po_ap_header_ids'] ) ); elseif( isset( $arrValues['po_ap_header_ids'] ) ) $this->setPoApHeaderIds( $arrValues['po_ap_header_ids'] );
		if( isset( $arrValues['summary_utility_bill_page_number'] ) && $boolDirectSet ) $this->set( 'm_intSummaryUtilityBillPageNumber', trim( $arrValues['summary_utility_bill_page_number'] ) ); elseif( isset( $arrValues['summary_utility_bill_page_number'] ) ) $this->setSummaryUtilityBillPageNumber( $arrValues['summary_utility_bill_page_number'] );
		if( isset( $arrValues['bill_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBillDatetime', trim( $arrValues['bill_datetime'] ) ); elseif( isset( $arrValues['bill_datetime'] ) ) $this->setBillDatetime( $arrValues['bill_datetime'] );
		if( isset( $arrValues['bill_number'] ) && $boolDirectSet ) $this->set( 'm_strBillNumber', trim( $arrValues['bill_number'] ) ); elseif( isset( $arrValues['bill_number'] ) ) $this->setBillNumber( $arrValues['bill_number'] );
		if( isset( $arrValues['current_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCurrentAmount', trim( $arrValues['current_amount'] ) ); elseif( isset( $arrValues['current_amount'] ) ) $this->setCurrentAmount( $arrValues['current_amount'] );
		if( isset( $arrValues['total_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalAmount', trim( $arrValues['total_amount'] ) ); elseif( isset( $arrValues['total_amount'] ) ) $this->setTotalAmount( $arrValues['total_amount'] );
		if( isset( $arrValues['bill_date'] ) && $boolDirectSet ) $this->set( 'm_strBillDate', trim( $arrValues['bill_date'] ) ); elseif( isset( $arrValues['bill_date'] ) ) $this->setBillDate( $arrValues['bill_date'] );
		if( isset( $arrValues['due_date'] ) && $boolDirectSet ) $this->set( 'm_strDueDate', trim( $arrValues['due_date'] ) ); elseif( isset( $arrValues['due_date'] ) ) $this->setDueDate( $arrValues['due_date'] );
		if( isset( $arrValues['billing_start_date'] ) && $boolDirectSet ) $this->set( 'm_strBillingStartDate', trim( $arrValues['billing_start_date'] ) ); elseif( isset( $arrValues['billing_start_date'] ) ) $this->setBillingStartDate( $arrValues['billing_start_date'] );
		if( isset( $arrValues['billing_end_date'] ) && $boolDirectSet ) $this->set( 'm_strBillingEndDate', trim( $arrValues['billing_end_date'] ) ); elseif( isset( $arrValues['billing_end_date'] ) ) $this->setBillingEndDate( $arrValues['billing_end_date'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['is_historical'] ) && $boolDirectSet ) $this->set( 'm_boolIsHistorical', trim( stripcslashes( $arrValues['is_historical'] ) ) ); elseif( isset( $arrValues['is_historical'] ) ) $this->setIsHistorical( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_historical'] ) : $arrValues['is_historical'] );
		if( isset( $arrValues['is_queue_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsQueueIgnored', trim( stripcslashes( $arrValues['is_queue_ignored'] ) ) ); elseif( isset( $arrValues['is_queue_ignored'] ) ) $this->setIsQueueIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_queue_ignored'] ) : $arrValues['is_queue_ignored'] );
		if( isset( $arrValues['is_ignore_invoice_tracking'] ) && $boolDirectSet ) $this->set( 'm_boolIsIgnoreInvoiceTracking', trim( stripcslashes( $arrValues['is_ignore_invoice_tracking'] ) ) ); elseif( isset( $arrValues['is_ignore_invoice_tracking'] ) ) $this->setIsIgnoreInvoiceTracking( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ignore_invoice_tracking'] ) : $arrValues['is_ignore_invoice_tracking'] );
		if( isset( $arrValues['is_ocred'] ) && $boolDirectSet ) $this->set( 'm_boolIsOcred', trim( stripcslashes( $arrValues['is_ocred'] ) ) ); elseif( isset( $arrValues['is_ocred'] ) ) $this->setIsOcred( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ocred'] ) : $arrValues['is_ocred'] );
		if( isset( $arrValues['is_no_bill_available'] ) && $boolDirectSet ) $this->set( 'm_boolIsNoBillAvailable', trim( stripcslashes( $arrValues['is_no_bill_available'] ) ) ); elseif( isset( $arrValues['is_no_bill_available'] ) ) $this->setIsNoBillAvailable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_no_bill_available'] ) : $arrValues['is_no_bill_available'] );
		if( isset( $arrValues['start_process_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartProcessDatetime', trim( $arrValues['start_process_datetime'] ) ); elseif( isset( $arrValues['start_process_datetime'] ) ) $this->setStartProcessDatetime( $arrValues['start_process_datetime'] );
		if( isset( $arrValues['end_process_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndProcessDatetime', trim( $arrValues['end_process_datetime'] ) ); elseif( isset( $arrValues['end_process_datetime'] ) ) $this->setEndProcessDatetime( $arrValues['end_process_datetime'] );
		if( isset( $arrValues['last_processed_by'] ) && $boolDirectSet ) $this->set( 'm_intLastProcessedBy', trim( $arrValues['last_processed_by'] ) ); elseif( isset( $arrValues['last_processed_by'] ) ) $this->setLastProcessedBy( $arrValues['last_processed_by'] );
		if( isset( $arrValues['start_associate_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartAssociateDatetime', trim( $arrValues['start_associate_datetime'] ) ); elseif( isset( $arrValues['start_associate_datetime'] ) ) $this->setStartAssociateDatetime( $arrValues['start_associate_datetime'] );
		if( isset( $arrValues['end_associate_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndAssociateDatetime', trim( $arrValues['end_associate_datetime'] ) ); elseif( isset( $arrValues['end_associate_datetime'] ) ) $this->setEndAssociateDatetime( $arrValues['end_associate_datetime'] );
		if( isset( $arrValues['last_associated_by'] ) && $boolDirectSet ) $this->set( 'm_intLastAssociatedBy', trim( $arrValues['last_associated_by'] ) ); elseif( isset( $arrValues['last_associated_by'] ) ) $this->setLastAssociatedBy( $arrValues['last_associated_by'] );
		if( isset( $arrValues['processed_by'] ) && $boolDirectSet ) $this->set( 'm_intProcessedBy', trim( $arrValues['processed_by'] ) ); elseif( isset( $arrValues['processed_by'] ) ) $this->setProcessedBy( $arrValues['processed_by'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['processed_again_by'] ) && $boolDirectSet ) $this->set( 'm_intProcessedAgainBy', trim( $arrValues['processed_again_by'] ) ); elseif( isset( $arrValues['processed_again_by'] ) ) $this->setProcessedAgainBy( $arrValues['processed_again_by'] );
		if( isset( $arrValues['processed_again_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedAgainOn', trim( $arrValues['processed_again_on'] ) ); elseif( isset( $arrValues['processed_again_on'] ) ) $this->setProcessedAgainOn( $arrValues['processed_again_on'] );
		if( isset( $arrValues['processed_on_time'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOnTime', trim( $arrValues['processed_on_time'] ) ); elseif( isset( $arrValues['processed_on_time'] ) ) $this->setProcessedOnTime( $arrValues['processed_on_time'] );
		if( isset( $arrValues['processed_again_on_time'] ) && $boolDirectSet ) $this->set( 'm_strProcessedAgainOnTime', trim( $arrValues['processed_again_on_time'] ) ); elseif( isset( $arrValues['processed_again_on_time'] ) ) $this->setProcessedAgainOnTime( $arrValues['processed_again_on_time'] );
		if( isset( $arrValues['start_review_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartReviewDatetime', trim( $arrValues['start_review_datetime'] ) ); elseif( isset( $arrValues['start_review_datetime'] ) ) $this->setStartReviewDatetime( $arrValues['start_review_datetime'] );
		if( isset( $arrValues['end_review_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndReviewDatetime', trim( $arrValues['end_review_datetime'] ) ); elseif( isset( $arrValues['end_review_datetime'] ) ) $this->setEndReviewDatetime( $arrValues['end_review_datetime'] );
		if( isset( $arrValues['scheduled_export_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledExportDate', trim( $arrValues['scheduled_export_date'] ) ); elseif( isset( $arrValues['scheduled_export_date'] ) ) $this->setScheduledExportDate( $arrValues['scheduled_export_date'] );
		if( isset( $arrValues['last_reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intLastReviewedBy', trim( $arrValues['last_reviewed_by'] ) ); elseif( isset( $arrValues['last_reviewed_by'] ) ) $this->setLastReviewedBy( $arrValues['last_reviewed_by'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['associated_by'] ) && $boolDirectSet ) $this->set( 'm_intAssociatedBy', trim( $arrValues['associated_by'] ) ); elseif( isset( $arrValues['associated_by'] ) ) $this->setAssociatedBy( $arrValues['associated_by'] );
		if( isset( $arrValues['associated_on'] ) && $boolDirectSet ) $this->set( 'm_strAssociatedOn', trim( $arrValues['associated_on'] ) ); elseif( isset( $arrValues['associated_on'] ) ) $this->setAssociatedOn( $arrValues['associated_on'] );
		if( isset( $arrValues['associated_again_by'] ) && $boolDirectSet ) $this->set( 'm_intAssociatedAgainBy', trim( $arrValues['associated_again_by'] ) ); elseif( isset( $arrValues['associated_again_by'] ) ) $this->setAssociatedAgainBy( $arrValues['associated_again_by'] );
		if( isset( $arrValues['associated_again_on'] ) && $boolDirectSet ) $this->set( 'm_strAssociatedAgainOn', trim( $arrValues['associated_again_on'] ) ); elseif( isset( $arrValues['associated_again_on'] ) ) $this->setAssociatedAgainOn( $arrValues['associated_again_on'] );
		if( isset( $arrValues['split_by'] ) && $boolDirectSet ) $this->set( 'm_intSplitBy', trim( $arrValues['split_by'] ) ); elseif( isset( $arrValues['split_by'] ) ) $this->setSplitBy( $arrValues['split_by'] );
		if( isset( $arrValues['split_on'] ) && $boolDirectSet ) $this->set( 'm_strSplitOn', trim( $arrValues['split_on'] ) ); elseif( isset( $arrValues['split_on'] ) ) $this->setSplitOn( $arrValues['split_on'] );
		if( isset( $arrValues['split_again_by'] ) && $boolDirectSet ) $this->set( 'm_intSplitAgainBy', trim( $arrValues['split_again_by'] ) ); elseif( isset( $arrValues['split_again_by'] ) ) $this->setSplitAgainBy( $arrValues['split_again_by'] );
		if( isset( $arrValues['split_again_on'] ) && $boolDirectSet ) $this->set( 'm_strSplitAgainOn', trim( $arrValues['split_again_on'] ) ); elseif( isset( $arrValues['split_again_on'] ) ) $this->setSplitAgainOn( $arrValues['split_again_on'] );
		if( isset( $arrValues['reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intReviewedBy', trim( $arrValues['reviewed_by'] ) ); elseif( isset( $arrValues['reviewed_by'] ) ) $this->setReviewedBy( $arrValues['reviewed_by'] );
		if( isset( $arrValues['reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strReviewedOn', trim( $arrValues['reviewed_on'] ) ); elseif( isset( $arrValues['reviewed_on'] ) ) $this->setReviewedOn( $arrValues['reviewed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['exception_queue_item_canceled_by'] ) && $boolDirectSet ) $this->set( 'm_intExceptionQueueItemCanceledBy', trim( $arrValues['exception_queue_item_canceled_by'] ) ); elseif( isset( $arrValues['exception_queue_item_canceled_by'] ) ) $this->setExceptionQueueItemCanceledBy( $arrValues['exception_queue_item_canceled_by'] );
		if( isset( $arrValues['exception_queue_item_canceled_on'] ) && $boolDirectSet ) $this->set( 'm_strExceptionQueueItemCanceledOn', trim( $arrValues['exception_queue_item_canceled_on'] ) ); elseif( isset( $arrValues['exception_queue_item_canceled_on'] ) ) $this->setExceptionQueueItemCanceledOn( $arrValues['exception_queue_item_canceled_on'] );
		if( isset( $arrValues['exception_queue_item_note'] ) && $boolDirectSet ) $this->set( 'm_strExceptionQueueItemNote', trim( $arrValues['exception_queue_item_note'] ) ); elseif( isset( $arrValues['exception_queue_item_note'] ) ) $this->setExceptionQueueItemNote( $arrValues['exception_queue_item_note'] );
		if( isset( $arrValues['ocr_template_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrTemplateId', trim( $arrValues['ocr_template_id'] ) ); elseif( isset( $arrValues['ocr_template_id'] ) ) $this->setOcrTemplateId( $arrValues['ocr_template_id'] );
		if( isset( $arrValues['deletion_reason'] ) && $boolDirectSet ) $this->set( 'm_strDeletionReason', trim( $arrValues['deletion_reason'] ) ); elseif( isset( $arrValues['deletion_reason'] ) ) $this->setDeletionReason( $arrValues['deletion_reason'] );
		if( isset( $arrValues['copy_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intCopyUtilityBillId', trim( $arrValues['copy_utility_bill_id'] ) ); elseif( isset( $arrValues['copy_utility_bill_id'] ) ) $this->setCopyUtilityBillId( $arrValues['copy_utility_bill_id'] );
		if( isset( $arrValues['billing_allocation_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingAllocationId', trim( $arrValues['billing_allocation_id'] ) ); elseif( isset( $arrValues['billing_allocation_id'] ) ) $this->setBillingAllocationId( $arrValues['billing_allocation_id'] );
		if( isset( $arrValues['imported_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intImportedApHeaderId', trim( $arrValues['imported_ap_header_id'] ) ); elseif( isset( $arrValues['imported_ap_header_id'] ) ) $this->setImportedApHeaderId( $arrValues['imported_ap_header_id'] );
		if( isset( $arrValues['utility_bill_pull_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillPullId', trim( $arrValues['utility_bill_pull_id'] ) ); elseif( isset( $arrValues['utility_bill_pull_id'] ) ) $this->setUtilityBillPullId( $arrValues['utility_bill_pull_id'] );
		if( isset( $arrValues['ignore_third_party_data_entry'] ) && $boolDirectSet ) $this->set( 'm_boolIgnoreThirdPartyDataEntry', trim( stripcslashes( $arrValues['ignore_third_party_data_entry'] ) ) ); elseif( isset( $arrValues['ignore_third_party_data_entry'] ) ) $this->setIgnoreThirdPartyDataEntry( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ignore_third_party_data_entry'] ) : $arrValues['ignore_third_party_data_entry'] );
		if( isset( $arrValues['prior_balance'] ) && $boolDirectSet ) $this->set( 'm_fltPriorBalance', trim( $arrValues['prior_balance'] ) ); elseif( isset( $arrValues['prior_balance'] ) ) $this->setPriorBalance( $arrValues['prior_balance'] );
		if( isset( $arrValues['payments'] ) && $boolDirectSet ) $this->set( 'm_fltPayments', trim( $arrValues['payments'] ) ); elseif( isset( $arrValues['payments'] ) ) $this->setPayments( $arrValues['payments'] );
		if( isset( $arrValues['adjustments'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustments', trim( $arrValues['adjustments'] ) ); elseif( isset( $arrValues['adjustments'] ) ) $this->setAdjustments( $arrValues['adjustments'] );
		if( isset( $arrValues['balance_forward'] ) && $boolDirectSet ) $this->set( 'm_fltBalanceForward', trim( $arrValues['balance_forward'] ) ); elseif( isset( $arrValues['balance_forward'] ) ) $this->setBalanceForward( $arrValues['balance_forward'] );
		if( isset( $arrValues['is_force_export'] ) && $boolDirectSet ) $this->set( 'm_boolIsForceExport', trim( stripcslashes( $arrValues['is_force_export'] ) ) ); elseif( isset( $arrValues['is_force_export'] ) ) $this->setIsForceExport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_force_export'] ) : $arrValues['is_force_export'] );
		if( isset( $arrValues['budget_balance_amount'] ) && $boolDirectSet ) $this->set( 'm_fltBudgetBalanceAmount', trim( $arrValues['budget_balance_amount'] ) ); elseif( isset( $arrValues['budget_balance_amount'] ) ) $this->setBudgetBalanceAmount( $arrValues['budget_balance_amount'] );
		if( isset( $arrValues['is_due_upon_receipt'] ) && $boolDirectSet ) $this->set( 'm_boolIsDueUponReceipt', trim( stripcslashes( $arrValues['is_due_upon_receipt'] ) ) ); elseif( isset( $arrValues['is_due_upon_receipt'] ) ) $this->setIsDueUponReceipt( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_due_upon_receipt'] ) : $arrValues['is_due_upon_receipt'] );
		if( isset( $arrValues['is_do_not_pay'] ) && $boolDirectSet ) $this->set( 'm_boolIsDoNotPay', trim( stripcslashes( $arrValues['is_do_not_pay'] ) ) ); elseif( isset( $arrValues['is_do_not_pay'] ) ) $this->setIsDoNotPay( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_do_not_pay'] ) : $arrValues['is_do_not_pay'] );
		if( isset( $arrValues['no_due_date'] ) && $boolDirectSet ) $this->set( 'm_boolNoDueDate', trim( stripcslashes( $arrValues['no_due_date'] ) ) ); elseif( isset( $arrValues['no_due_date'] ) ) $this->setNoDueDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['no_due_date'] ) : $arrValues['no_due_date'] );
		if( isset( $arrValues['no_bill_date'] ) && $boolDirectSet ) $this->set( 'm_boolNoBillDate', trim( stripcslashes( $arrValues['no_bill_date'] ) ) ); elseif( isset( $arrValues['no_bill_date'] ) ) $this->setNoBillDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['no_bill_date'] ) : $arrValues['no_bill_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillTemplateId( $intUtilityBillTemplateId ) {
		$this->set( 'm_intUtilityBillTemplateId', CStrings::strToIntDef( $intUtilityBillTemplateId, NULL, false ) );
	}

	public function getUtilityBillTemplateId() {
		return $this->m_intUtilityBillTemplateId;
	}

	public function sqlUtilityBillTemplateId() {
		return ( true == isset( $this->m_intUtilityBillTemplateId ) ) ? ( string ) $this->m_intUtilityBillTemplateId : 'NULL';
	}

	public function setUtilityBillTypeId( $intUtilityBillTypeId ) {
		$this->set( 'm_intUtilityBillTypeId', CStrings::strToIntDef( $intUtilityBillTypeId, NULL, false ) );
	}

	public function getUtilityBillTypeId() {
		return $this->m_intUtilityBillTypeId;
	}

	public function sqlUtilityBillTypeId() {
		return ( true == isset( $this->m_intUtilityBillTypeId ) ) ? ( string ) $this->m_intUtilityBillTypeId : 'NULL';
	}

	public function setUtilityBillReceiptTypeId( $intUtilityBillReceiptTypeId ) {
		$this->set( 'm_intUtilityBillReceiptTypeId', CStrings::strToIntDef( $intUtilityBillReceiptTypeId, NULL, false ) );
	}

	public function getUtilityBillReceiptTypeId() {
		return $this->m_intUtilityBillReceiptTypeId;
	}

	public function sqlUtilityBillReceiptTypeId() {
		return ( true == isset( $this->m_intUtilityBillReceiptTypeId ) ) ? ( string ) $this->m_intUtilityBillReceiptTypeId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setSummaryUtilityBillId( $intSummaryUtilityBillId ) {
		$this->set( 'm_intSummaryUtilityBillId', CStrings::strToIntDef( $intSummaryUtilityBillId, NULL, false ) );
	}

	public function getSummaryUtilityBillId() {
		return $this->m_intSummaryUtilityBillId;
	}

	public function sqlSummaryUtilityBillId() {
		return ( true == isset( $this->m_intSummaryUtilityBillId ) ) ? ( string ) $this->m_intSummaryUtilityBillId : 'NULL';
	}

	public function setEstimatedUtilityBillId( $intEstimatedUtilityBillId ) {
		$this->set( 'm_intEstimatedUtilityBillId', CStrings::strToIntDef( $intEstimatedUtilityBillId, NULL, false ) );
	}

	public function getEstimatedUtilityBillId() {
		return $this->m_intEstimatedUtilityBillId;
	}

	public function sqlEstimatedUtilityBillId() {
		return ( true == isset( $this->m_intEstimatedUtilityBillId ) ) ? ( string ) $this->m_intEstimatedUtilityBillId : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setUtilityDocumentId( $intUtilityDocumentId ) {
		$this->set( 'm_intUtilityDocumentId', CStrings::strToIntDef( $intUtilityDocumentId, NULL, false ) );
	}

	public function getUtilityDocumentId() {
		return $this->m_intUtilityDocumentId;
	}

	public function sqlUtilityDocumentId() {
		return ( true == isset( $this->m_intUtilityDocumentId ) ) ? ( string ) $this->m_intUtilityDocumentId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setApPayeeAccountId( $intApPayeeAccountId ) {
		$this->set( 'm_intApPayeeAccountId', CStrings::strToIntDef( $intApPayeeAccountId, NULL, false ) );
	}

	public function getApPayeeAccountId() {
		return $this->m_intApPayeeAccountId;
	}

	public function sqlApPayeeAccountId() {
		return ( true == isset( $this->m_intApPayeeAccountId ) ) ? ( string ) $this->m_intApPayeeAccountId : 'NULL';
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->set( 'm_intApHeaderId', CStrings::strToIntDef( $intApHeaderId, NULL, false ) );
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function sqlApHeaderId() {
		return ( true == isset( $this->m_intApHeaderId ) ) ? ( string ) $this->m_intApHeaderId : 'NULL';
	}

	public function setReviewUtilityBillId( $intReviewUtilityBillId ) {
		$this->set( 'm_intReviewUtilityBillId', CStrings::strToIntDef( $intReviewUtilityBillId, NULL, false ) );
	}

	public function getReviewUtilityBillId() {
		return $this->m_intReviewUtilityBillId;
	}

	public function sqlReviewUtilityBillId() {
		return ( true == isset( $this->m_intReviewUtilityBillId ) ) ? ( string ) $this->m_intReviewUtilityBillId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setApHeaderSubTypeId( $intApHeaderSubTypeId ) {
		$this->set( 'm_intApHeaderSubTypeId', CStrings::strToIntDef( $intApHeaderSubTypeId, NULL, false ) );
	}

	public function getApHeaderSubTypeId() {
		return $this->m_intApHeaderSubTypeId;
	}

	public function sqlApHeaderSubTypeId() {
		return ( true == isset( $this->m_intApHeaderSubTypeId ) ) ? ( string ) $this->m_intApHeaderSubTypeId : 'NULL';
	}

	public function setApExceptionQueueItemId( $intApExceptionQueueItemId ) {
		$this->set( 'm_intApExceptionQueueItemId', CStrings::strToIntDef( $intApExceptionQueueItemId, NULL, false ) );
	}

	public function getApExceptionQueueItemId() {
		return $this->m_intApExceptionQueueItemId;
	}

	public function sqlApExceptionQueueItemId() {
		return ( true == isset( $this->m_intApExceptionQueueItemId ) ) ? ( string ) $this->m_intApExceptionQueueItemId : 'NULL';
	}

	public function setPoApHeaderIds( $arrintPoApHeaderIds ) {
		$this->set( 'm_arrintPoApHeaderIds', CStrings::strToArrIntDef( $arrintPoApHeaderIds, NULL ) );
	}

	public function getPoApHeaderIds() {
		return $this->m_arrintPoApHeaderIds;
	}

	public function sqlPoApHeaderIds() {
		return ( true == isset( $this->m_arrintPoApHeaderIds ) && true == valArr( $this->m_arrintPoApHeaderIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPoApHeaderIds, NULL ) . '\'' : 'NULL';
	}

	public function setSummaryUtilityBillPageNumber( $intSummaryUtilityBillPageNumber ) {
		$this->set( 'm_intSummaryUtilityBillPageNumber', CStrings::strToIntDef( $intSummaryUtilityBillPageNumber, NULL, false ) );
	}

	public function getSummaryUtilityBillPageNumber() {
		return $this->m_intSummaryUtilityBillPageNumber;
	}

	public function sqlSummaryUtilityBillPageNumber() {
		return ( true == isset( $this->m_intSummaryUtilityBillPageNumber ) ) ? ( string ) $this->m_intSummaryUtilityBillPageNumber : 'NULL';
	}

	public function setBillDatetime( $strBillDatetime ) {
		$this->set( 'm_strBillDatetime', CStrings::strTrimDef( $strBillDatetime, -1, NULL, true ) );
	}

	public function getBillDatetime() {
		return $this->m_strBillDatetime;
	}

	public function sqlBillDatetime() {
		return ( true == isset( $this->m_strBillDatetime ) ) ? '\'' . $this->m_strBillDatetime . '\'' : 'NOW()';
	}

	public function setBillNumber( $strBillNumber ) {
		$this->set( 'm_strBillNumber', CStrings::strTrimDef( $strBillNumber, 50, NULL, true ) );
	}

	public function getBillNumber() {
		return $this->m_strBillNumber;
	}

	public function sqlBillNumber() {
		return ( true == isset( $this->m_strBillNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBillNumber ) : '\'' . addslashes( $this->m_strBillNumber ) . '\'' ) : 'NULL';
	}

	public function setCurrentAmount( $fltCurrentAmount ) {
		$this->set( 'm_fltCurrentAmount', CStrings::strToFloatDef( $fltCurrentAmount, NULL, false, 4 ) );
	}

	public function getCurrentAmount() {
		return $this->m_fltCurrentAmount;
	}

	public function sqlCurrentAmount() {
		return ( true == isset( $this->m_fltCurrentAmount ) ) ? ( string ) $this->m_fltCurrentAmount : 'NULL';
	}

	public function setTotalAmount( $fltTotalAmount ) {
		$this->set( 'm_fltTotalAmount', CStrings::strToFloatDef( $fltTotalAmount, NULL, false, 4 ) );
	}

	public function getTotalAmount() {
		return $this->m_fltTotalAmount;
	}

	public function sqlTotalAmount() {
		return ( true == isset( $this->m_fltTotalAmount ) ) ? ( string ) $this->m_fltTotalAmount : 'NULL';
	}

	public function setBillDate( $strBillDate ) {
		$this->set( 'm_strBillDate', CStrings::strTrimDef( $strBillDate, -1, NULL, true ) );
	}

	public function getBillDate() {
		return $this->m_strBillDate;
	}

	public function sqlBillDate() {
		return ( true == isset( $this->m_strBillDate ) ) ? '\'' . $this->m_strBillDate . '\'' : 'NULL';
	}

	public function setDueDate( $strDueDate ) {
		$this->set( 'm_strDueDate', CStrings::strTrimDef( $strDueDate, -1, NULL, true ) );
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function sqlDueDate() {
		return ( true == isset( $this->m_strDueDate ) ) ? '\'' . $this->m_strDueDate . '\'' : 'NULL';
	}

	public function setBillingStartDate( $strBillingStartDate ) {
		$this->set( 'm_strBillingStartDate', CStrings::strTrimDef( $strBillingStartDate, -1, NULL, true ) );
	}

	public function getBillingStartDate() {
		return $this->m_strBillingStartDate;
	}

	public function sqlBillingStartDate() {
		return ( true == isset( $this->m_strBillingStartDate ) ) ? '\'' . $this->m_strBillingStartDate . '\'' : 'NULL';
	}

	public function setBillingEndDate( $strBillingEndDate ) {
		$this->set( 'm_strBillingEndDate', CStrings::strTrimDef( $strBillingEndDate, -1, NULL, true ) );
	}

	public function getBillingEndDate() {
		return $this->m_strBillingEndDate;
	}

	public function sqlBillingEndDate() {
		return ( true == isset( $this->m_strBillingEndDate ) ) ? '\'' . $this->m_strBillingEndDate . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setIsHistorical( $boolIsHistorical ) {
		$this->set( 'm_boolIsHistorical', CStrings::strToBool( $boolIsHistorical ) );
	}

	public function getIsHistorical() {
		return $this->m_boolIsHistorical;
	}

	public function sqlIsHistorical() {
		return ( true == isset( $this->m_boolIsHistorical ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHistorical ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsQueueIgnored( $boolIsQueueIgnored ) {
		$this->set( 'm_boolIsQueueIgnored', CStrings::strToBool( $boolIsQueueIgnored ) );
	}

	public function getIsQueueIgnored() {
		return $this->m_boolIsQueueIgnored;
	}

	public function sqlIsQueueIgnored() {
		return ( true == isset( $this->m_boolIsQueueIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsQueueIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsIgnoreInvoiceTracking( $boolIsIgnoreInvoiceTracking ) {
		$this->set( 'm_boolIsIgnoreInvoiceTracking', CStrings::strToBool( $boolIsIgnoreInvoiceTracking ) );
	}

	public function getIsIgnoreInvoiceTracking() {
		return $this->m_boolIsIgnoreInvoiceTracking;
	}

	public function sqlIsIgnoreInvoiceTracking() {
		return ( true == isset( $this->m_boolIsIgnoreInvoiceTracking ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIgnoreInvoiceTracking ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOcred( $boolIsOcred ) {
		$this->set( 'm_boolIsOcred', CStrings::strToBool( $boolIsOcred ) );
	}

	public function getIsOcred() {
		return $this->m_boolIsOcred;
	}

	public function sqlIsOcred() {
		return ( true == isset( $this->m_boolIsOcred ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOcred ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsNoBillAvailable( $boolIsNoBillAvailable ) {
		$this->set( 'm_boolIsNoBillAvailable', CStrings::strToBool( $boolIsNoBillAvailable ) );
	}

	public function getIsNoBillAvailable() {
		return $this->m_boolIsNoBillAvailable;
	}

	public function sqlIsNoBillAvailable() {
		return ( true == isset( $this->m_boolIsNoBillAvailable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNoBillAvailable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setStartProcessDatetime( $strStartProcessDatetime ) {
		$this->set( 'm_strStartProcessDatetime', CStrings::strTrimDef( $strStartProcessDatetime, -1, NULL, true ) );
	}

	public function getStartProcessDatetime() {
		return $this->m_strStartProcessDatetime;
	}

	public function sqlStartProcessDatetime() {
		return ( true == isset( $this->m_strStartProcessDatetime ) ) ? '\'' . $this->m_strStartProcessDatetime . '\'' : 'NULL';
	}

	public function setEndProcessDatetime( $strEndProcessDatetime ) {
		$this->set( 'm_strEndProcessDatetime', CStrings::strTrimDef( $strEndProcessDatetime, -1, NULL, true ) );
	}

	public function getEndProcessDatetime() {
		return $this->m_strEndProcessDatetime;
	}

	public function sqlEndProcessDatetime() {
		return ( true == isset( $this->m_strEndProcessDatetime ) ) ? '\'' . $this->m_strEndProcessDatetime . '\'' : 'NULL';
	}

	public function setLastProcessedBy( $intLastProcessedBy ) {
		$this->set( 'm_intLastProcessedBy', CStrings::strToIntDef( $intLastProcessedBy, NULL, false ) );
	}

	public function getLastProcessedBy() {
		return $this->m_intLastProcessedBy;
	}

	public function sqlLastProcessedBy() {
		return ( true == isset( $this->m_intLastProcessedBy ) ) ? ( string ) $this->m_intLastProcessedBy : 'NULL';
	}

	public function setStartAssociateDatetime( $strStartAssociateDatetime ) {
		$this->set( 'm_strStartAssociateDatetime', CStrings::strTrimDef( $strStartAssociateDatetime, -1, NULL, true ) );
	}

	public function getStartAssociateDatetime() {
		return $this->m_strStartAssociateDatetime;
	}

	public function sqlStartAssociateDatetime() {
		return ( true == isset( $this->m_strStartAssociateDatetime ) ) ? '\'' . $this->m_strStartAssociateDatetime . '\'' : 'NULL';
	}

	public function setEndAssociateDatetime( $strEndAssociateDatetime ) {
		$this->set( 'm_strEndAssociateDatetime', CStrings::strTrimDef( $strEndAssociateDatetime, -1, NULL, true ) );
	}

	public function getEndAssociateDatetime() {
		return $this->m_strEndAssociateDatetime;
	}

	public function sqlEndAssociateDatetime() {
		return ( true == isset( $this->m_strEndAssociateDatetime ) ) ? '\'' . $this->m_strEndAssociateDatetime . '\'' : 'NULL';
	}

	public function setLastAssociatedBy( $intLastAssociatedBy ) {
		$this->set( 'm_intLastAssociatedBy', CStrings::strToIntDef( $intLastAssociatedBy, NULL, false ) );
	}

	public function getLastAssociatedBy() {
		return $this->m_intLastAssociatedBy;
	}

	public function sqlLastAssociatedBy() {
		return ( true == isset( $this->m_intLastAssociatedBy ) ) ? ( string ) $this->m_intLastAssociatedBy : 'NULL';
	}

	public function setProcessedBy( $intProcessedBy ) {
		$this->set( 'm_intProcessedBy', CStrings::strToIntDef( $intProcessedBy, NULL, false ) );
	}

	public function getProcessedBy() {
		return $this->m_intProcessedBy;
	}

	public function sqlProcessedBy() {
		return ( true == isset( $this->m_intProcessedBy ) ) ? ( string ) $this->m_intProcessedBy : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setProcessedAgainBy( $intProcessedAgainBy ) {
		$this->set( 'm_intProcessedAgainBy', CStrings::strToIntDef( $intProcessedAgainBy, NULL, false ) );
	}

	public function getProcessedAgainBy() {
		return $this->m_intProcessedAgainBy;
	}

	public function sqlProcessedAgainBy() {
		return ( true == isset( $this->m_intProcessedAgainBy ) ) ? ( string ) $this->m_intProcessedAgainBy : 'NULL';
	}

	public function setProcessedAgainOn( $strProcessedAgainOn ) {
		$this->set( 'm_strProcessedAgainOn', CStrings::strTrimDef( $strProcessedAgainOn, -1, NULL, true ) );
	}

	public function getProcessedAgainOn() {
		return $this->m_strProcessedAgainOn;
	}

	public function sqlProcessedAgainOn() {
		return ( true == isset( $this->m_strProcessedAgainOn ) ) ? '\'' . $this->m_strProcessedAgainOn . '\'' : 'NULL';
	}

	public function setProcessedOnTime( $strProcessedOnTime ) {
		$this->set( 'm_strProcessedOnTime', CStrings::strTrimDef( $strProcessedOnTime, NULL, NULL, true ) );
	}

	public function getProcessedOnTime() {
		return $this->m_strProcessedOnTime;
	}

	public function sqlProcessedOnTime() {
		return ( true == isset( $this->m_strProcessedOnTime ) ) ? '\'' . $this->m_strProcessedOnTime . '\'' : 'NULL';
	}

	public function setProcessedAgainOnTime( $strProcessedAgainOnTime ) {
		$this->set( 'm_strProcessedAgainOnTime', CStrings::strTrimDef( $strProcessedAgainOnTime, NULL, NULL, true ) );
	}

	public function getProcessedAgainOnTime() {
		return $this->m_strProcessedAgainOnTime;
	}

	public function sqlProcessedAgainOnTime() {
		return ( true == isset( $this->m_strProcessedAgainOnTime ) ) ? '\'' . $this->m_strProcessedAgainOnTime . '\'' : 'NULL';
	}

	public function setStartReviewDatetime( $strStartReviewDatetime ) {
		$this->set( 'm_strStartReviewDatetime', CStrings::strTrimDef( $strStartReviewDatetime, -1, NULL, true ) );
	}

	public function getStartReviewDatetime() {
		return $this->m_strStartReviewDatetime;
	}

	public function sqlStartReviewDatetime() {
		return ( true == isset( $this->m_strStartReviewDatetime ) ) ? '\'' . $this->m_strStartReviewDatetime . '\'' : 'NULL';
	}

	public function setEndReviewDatetime( $strEndReviewDatetime ) {
		$this->set( 'm_strEndReviewDatetime', CStrings::strTrimDef( $strEndReviewDatetime, -1, NULL, true ) );
	}

	public function getEndReviewDatetime() {
		return $this->m_strEndReviewDatetime;
	}

	public function sqlEndReviewDatetime() {
		return ( true == isset( $this->m_strEndReviewDatetime ) ) ? '\'' . $this->m_strEndReviewDatetime . '\'' : 'NULL';
	}

	public function setScheduledExportDate( $strScheduledExportDate ) {
		$this->set( 'm_strScheduledExportDate', CStrings::strTrimDef( $strScheduledExportDate, -1, NULL, true ) );
	}

	public function getScheduledExportDate() {
		return $this->m_strScheduledExportDate;
	}

	public function sqlScheduledExportDate() {
		return ( true == isset( $this->m_strScheduledExportDate ) ) ? '\'' . $this->m_strScheduledExportDate . '\'' : 'NULL';
	}

	public function setLastReviewedBy( $intLastReviewedBy ) {
		$this->set( 'm_intLastReviewedBy', CStrings::strToIntDef( $intLastReviewedBy, NULL, false ) );
	}

	public function getLastReviewedBy() {
		return $this->m_intLastReviewedBy;
	}

	public function sqlLastReviewedBy() {
		return ( true == isset( $this->m_intLastReviewedBy ) ) ? ( string ) $this->m_intLastReviewedBy : 'NULL';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setAssociatedBy( $intAssociatedBy ) {
		$this->set( 'm_intAssociatedBy', CStrings::strToIntDef( $intAssociatedBy, NULL, false ) );
	}

	public function getAssociatedBy() {
		return $this->m_intAssociatedBy;
	}

	public function sqlAssociatedBy() {
		return ( true == isset( $this->m_intAssociatedBy ) ) ? ( string ) $this->m_intAssociatedBy : 'NULL';
	}

	public function setAssociatedOn( $strAssociatedOn ) {
		$this->set( 'm_strAssociatedOn', CStrings::strTrimDef( $strAssociatedOn, -1, NULL, true ) );
	}

	public function getAssociatedOn() {
		return $this->m_strAssociatedOn;
	}

	public function sqlAssociatedOn() {
		return ( true == isset( $this->m_strAssociatedOn ) ) ? '\'' . $this->m_strAssociatedOn . '\'' : 'NULL';
	}

	public function setAssociatedAgainBy( $intAssociatedAgainBy ) {
		$this->set( 'm_intAssociatedAgainBy', CStrings::strToIntDef( $intAssociatedAgainBy, NULL, false ) );
	}

	public function getAssociatedAgainBy() {
		return $this->m_intAssociatedAgainBy;
	}

	public function sqlAssociatedAgainBy() {
		return ( true == isset( $this->m_intAssociatedAgainBy ) ) ? ( string ) $this->m_intAssociatedAgainBy : 'NULL';
	}

	public function setAssociatedAgainOn( $strAssociatedAgainOn ) {
		$this->set( 'm_strAssociatedAgainOn', CStrings::strTrimDef( $strAssociatedAgainOn, -1, NULL, true ) );
	}

	public function getAssociatedAgainOn() {
		return $this->m_strAssociatedAgainOn;
	}

	public function sqlAssociatedAgainOn() {
		return ( true == isset( $this->m_strAssociatedAgainOn ) ) ? '\'' . $this->m_strAssociatedAgainOn . '\'' : 'NULL';
	}

	public function setSplitBy( $intSplitBy ) {
		$this->set( 'm_intSplitBy', CStrings::strToIntDef( $intSplitBy, NULL, false ) );
	}

	public function getSplitBy() {
		return $this->m_intSplitBy;
	}

	public function sqlSplitBy() {
		return ( true == isset( $this->m_intSplitBy ) ) ? ( string ) $this->m_intSplitBy : 'NULL';
	}

	public function setSplitOn( $strSplitOn ) {
		$this->set( 'm_strSplitOn', CStrings::strTrimDef( $strSplitOn, -1, NULL, true ) );
	}

	public function getSplitOn() {
		return $this->m_strSplitOn;
	}

	public function sqlSplitOn() {
		return ( true == isset( $this->m_strSplitOn ) ) ? '\'' . $this->m_strSplitOn . '\'' : 'NULL';
	}

	public function setSplitAgainBy( $intSplitAgainBy ) {
		$this->set( 'm_intSplitAgainBy', CStrings::strToIntDef( $intSplitAgainBy, NULL, false ) );
	}

	public function getSplitAgainBy() {
		return $this->m_intSplitAgainBy;
	}

	public function sqlSplitAgainBy() {
		return ( true == isset( $this->m_intSplitAgainBy ) ) ? ( string ) $this->m_intSplitAgainBy : 'NULL';
	}

	public function setSplitAgainOn( $strSplitAgainOn ) {
		$this->set( 'm_strSplitAgainOn', CStrings::strTrimDef( $strSplitAgainOn, -1, NULL, true ) );
	}

	public function getSplitAgainOn() {
		return $this->m_strSplitAgainOn;
	}

	public function sqlSplitAgainOn() {
		return ( true == isset( $this->m_strSplitAgainOn ) ) ? '\'' . $this->m_strSplitAgainOn . '\'' : 'NULL';
	}

	public function setReviewedBy( $intReviewedBy ) {
		$this->set( 'm_intReviewedBy', CStrings::strToIntDef( $intReviewedBy, NULL, false ) );
	}

	public function getReviewedBy() {
		return $this->m_intReviewedBy;
	}

	public function sqlReviewedBy() {
		return ( true == isset( $this->m_intReviewedBy ) ) ? ( string ) $this->m_intReviewedBy : 'NULL';
	}

	public function setReviewedOn( $strReviewedOn ) {
		$this->set( 'm_strReviewedOn', CStrings::strTrimDef( $strReviewedOn, -1, NULL, true ) );
	}

	public function getReviewedOn() {
		return $this->m_strReviewedOn;
	}

	public function sqlReviewedOn() {
		return ( true == isset( $this->m_strReviewedOn ) ) ? '\'' . $this->m_strReviewedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setExceptionQueueItemCanceledBy( $intExceptionQueueItemCanceledBy ) {
		$this->set( 'm_intExceptionQueueItemCanceledBy', CStrings::strToIntDef( $intExceptionQueueItemCanceledBy, NULL, false ) );
	}

	public function getExceptionQueueItemCanceledBy() {
		return $this->m_intExceptionQueueItemCanceledBy;
	}

	public function sqlExceptionQueueItemCanceledBy() {
		return ( true == isset( $this->m_intExceptionQueueItemCanceledBy ) ) ? ( string ) $this->m_intExceptionQueueItemCanceledBy : 'NULL';
	}

	public function setExceptionQueueItemCanceledOn( $strExceptionQueueItemCanceledOn ) {
		$this->set( 'm_strExceptionQueueItemCanceledOn', CStrings::strTrimDef( $strExceptionQueueItemCanceledOn, -1, NULL, true ) );
	}

	public function getExceptionQueueItemCanceledOn() {
		return $this->m_strExceptionQueueItemCanceledOn;
	}

	public function sqlExceptionQueueItemCanceledOn() {
		return ( true == isset( $this->m_strExceptionQueueItemCanceledOn ) ) ? '\'' . $this->m_strExceptionQueueItemCanceledOn . '\'' : 'NULL';
	}

	public function setExceptionQueueItemNote( $strExceptionQueueItemNote ) {
		$this->set( 'm_strExceptionQueueItemNote', CStrings::strTrimDef( $strExceptionQueueItemNote, -1, NULL, true ) );
	}

	public function getExceptionQueueItemNote() {
		return $this->m_strExceptionQueueItemNote;
	}

	public function sqlExceptionQueueItemNote() {
		return ( true == isset( $this->m_strExceptionQueueItemNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExceptionQueueItemNote ) : '\'' . addslashes( $this->m_strExceptionQueueItemNote ) . '\'' ) : 'NULL';
	}

	public function setOcrTemplateId( $intOcrTemplateId ) {
		$this->set( 'm_intOcrTemplateId', CStrings::strToIntDef( $intOcrTemplateId, NULL, false ) );
	}

	public function getOcrTemplateId() {
		return $this->m_intOcrTemplateId;
	}

	public function sqlOcrTemplateId() {
		return ( true == isset( $this->m_intOcrTemplateId ) ) ? ( string ) $this->m_intOcrTemplateId : 'NULL';
	}

	public function setDeletionReason( $strDeletionReason ) {
		$this->set( 'm_strDeletionReason', CStrings::strTrimDef( $strDeletionReason, 240, NULL, true ) );
	}

	public function getDeletionReason() {
		return $this->m_strDeletionReason;
	}

	public function sqlDeletionReason() {
		return ( true == isset( $this->m_strDeletionReason ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDeletionReason ) : '\'' . addslashes( $this->m_strDeletionReason ) . '\'' ) : 'NULL';
	}

	public function setCopyUtilityBillId( $intCopyUtilityBillId ) {
		$this->set( 'm_intCopyUtilityBillId', CStrings::strToIntDef( $intCopyUtilityBillId, NULL, false ) );
	}

	public function getCopyUtilityBillId() {
		return $this->m_intCopyUtilityBillId;
	}

	public function sqlCopyUtilityBillId() {
		return ( true == isset( $this->m_intCopyUtilityBillId ) ) ? ( string ) $this->m_intCopyUtilityBillId : 'NULL';
	}

	public function setBillingAllocationId( $intBillingAllocationId ) {
		$this->set( 'm_intBillingAllocationId', CStrings::strToIntDef( $intBillingAllocationId, NULL, false ) );
	}

	public function getBillingAllocationId() {
		return $this->m_intBillingAllocationId;
	}

	public function sqlBillingAllocationId() {
		return ( true == isset( $this->m_intBillingAllocationId ) ) ? ( string ) $this->m_intBillingAllocationId : 'NULL';
	}

	public function setImportedApHeaderId( $intImportedApHeaderId ) {
		$this->set( 'm_intImportedApHeaderId', CStrings::strToIntDef( $intImportedApHeaderId, NULL, false ) );
	}

	public function getImportedApHeaderId() {
		return $this->m_intImportedApHeaderId;
	}

	public function sqlImportedApHeaderId() {
		return ( true == isset( $this->m_intImportedApHeaderId ) ) ? ( string ) $this->m_intImportedApHeaderId : 'NULL';
	}

	public function setUtilityBillPullId( $intUtilityBillPullId ) {
		$this->set( 'm_intUtilityBillPullId', CStrings::strToIntDef( $intUtilityBillPullId, NULL, false ) );
	}

	public function getUtilityBillPullId() {
		return $this->m_intUtilityBillPullId;
	}

	public function sqlUtilityBillPullId() {
		return ( true == isset( $this->m_intUtilityBillPullId ) ) ? ( string ) $this->m_intUtilityBillPullId : 'NULL';
	}

	public function setIgnoreThirdPartyDataEntry( $boolIgnoreThirdPartyDataEntry ) {
		$this->set( 'm_boolIgnoreThirdPartyDataEntry', CStrings::strToBool( $boolIgnoreThirdPartyDataEntry ) );
	}

	public function getIgnoreThirdPartyDataEntry() {
		return $this->m_boolIgnoreThirdPartyDataEntry;
	}

	public function sqlIgnoreThirdPartyDataEntry() {
		return ( true == isset( $this->m_boolIgnoreThirdPartyDataEntry ) ) ? '\'' . ( true == ( bool ) $this->m_boolIgnoreThirdPartyDataEntry ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPriorBalance( $fltPriorBalance ) {
		$this->set( 'm_fltPriorBalance', CStrings::strToFloatDef( $fltPriorBalance, NULL, false, 4 ) );
	}

	public function getPriorBalance() {
		return $this->m_fltPriorBalance;
	}

	public function sqlPriorBalance() {
		return ( true == isset( $this->m_fltPriorBalance ) ) ? ( string ) $this->m_fltPriorBalance : 'NULL';
	}

	public function setPayments( $fltPayments ) {
		$this->set( 'm_fltPayments', CStrings::strToFloatDef( $fltPayments, NULL, false, 4 ) );
	}

	public function getPayments() {
		return $this->m_fltPayments;
	}

	public function sqlPayments() {
		return ( true == isset( $this->m_fltPayments ) ) ? ( string ) $this->m_fltPayments : 'NULL';
	}

	public function setAdjustments( $fltAdjustments ) {
		$this->set( 'm_fltAdjustments', CStrings::strToFloatDef( $fltAdjustments, NULL, false, 4 ) );
	}

	public function getAdjustments() {
		return $this->m_fltAdjustments;
	}

	public function sqlAdjustments() {
		return ( true == isset( $this->m_fltAdjustments ) ) ? ( string ) $this->m_fltAdjustments : 'NULL';
	}

	public function setBalanceForward( $fltBalanceForward ) {
		$this->set( 'm_fltBalanceForward', CStrings::strToFloatDef( $fltBalanceForward, NULL, false, 4 ) );
	}

	public function getBalanceForward() {
		return $this->m_fltBalanceForward;
	}

	public function sqlBalanceForward() {
		return ( true == isset( $this->m_fltBalanceForward ) ) ? ( string ) $this->m_fltBalanceForward : 'NULL';
	}

	public function setIsForceExport( $boolIsForceExport ) {
		$this->set( 'm_boolIsForceExport', CStrings::strToBool( $boolIsForceExport ) );
	}

	public function getIsForceExport() {
		return $this->m_boolIsForceExport;
	}

	public function sqlIsForceExport() {
		return ( true == isset( $this->m_boolIsForceExport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsForceExport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBudgetBalanceAmount( $fltBudgetBalanceAmount ) {
		$this->set( 'm_fltBudgetBalanceAmount', CStrings::strToFloatDef( $fltBudgetBalanceAmount, NULL, false, 4 ) );
	}

	public function getBudgetBalanceAmount() {
		return $this->m_fltBudgetBalanceAmount;
	}

	public function sqlBudgetBalanceAmount() {
		return ( true == isset( $this->m_fltBudgetBalanceAmount ) ) ? ( string ) $this->m_fltBudgetBalanceAmount : 'NULL';
	}

	public function setIsDueUponReceipt( $boolIsDueUponReceipt ) {
		$this->set( 'm_boolIsDueUponReceipt', CStrings::strToBool( $boolIsDueUponReceipt ) );
	}

	public function getIsDueUponReceipt() {
		return $this->m_boolIsDueUponReceipt;
	}

	public function sqlIsDueUponReceipt() {
		return ( true == isset( $this->m_boolIsDueUponReceipt ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDueUponReceipt ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDoNotPay( $boolIsDoNotPay ) {
		$this->set( 'm_boolIsDoNotPay', CStrings::strToBool( $boolIsDoNotPay ) );
	}

	public function getIsDoNotPay() {
		return $this->m_boolIsDoNotPay;
	}

	public function sqlIsDoNotPay() {
		return ( true == isset( $this->m_boolIsDoNotPay ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDoNotPay ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setNoDueDate( $boolNoDueDate ) {
		$this->set( 'm_boolNoDueDate', CStrings::strToBool( $boolNoDueDate ) );
	}

	public function getNoDueDate() {
		return $this->m_boolNoDueDate;
	}

	public function sqlNoDueDate() {
		return ( true == isset( $this->m_boolNoDueDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolNoDueDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setNoBillDate( $boolNoBillDate ) {
		$this->set( 'm_boolNoBillDate', CStrings::strToBool( $boolNoBillDate ) );
	}

	public function getNoBillDate() {
		return $this->m_boolNoBillDate;
	}

	public function sqlNoBillDate() {
		return ( true == isset( $this->m_boolNoBillDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolNoBillDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_template_id, utility_bill_type_id, utility_bill_receipt_type_id, utility_bill_account_id, summary_utility_bill_id, estimated_utility_bill_id, transaction_id, utility_document_id, ap_payee_id, ap_payee_location_id, ap_payee_account_id, ap_header_id, review_utility_bill_id, ps_product_id, ap_header_sub_type_id, ap_exception_queue_item_id, po_ap_header_ids, summary_utility_bill_page_number, bill_datetime, bill_number, current_amount, total_amount, bill_date, due_date, billing_start_date, billing_end_date, start_date, end_date, notes, is_historical, is_queue_ignored, is_ignore_invoice_tracking, is_ocred, is_no_bill_available, start_process_datetime, end_process_datetime, last_processed_by, start_associate_datetime, end_associate_datetime, last_associated_by, processed_by, processed_on, processed_again_by, processed_again_on, processed_on_time, processed_again_on_time, start_review_datetime, end_review_datetime, scheduled_export_date, last_reviewed_by, exported_by, exported_on, associated_by, associated_on, associated_again_by, associated_again_on, split_by, split_on, split_again_by, split_again_on, reviewed_by, reviewed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, exception_queue_item_canceled_by, exception_queue_item_canceled_on, exception_queue_item_note, ocr_template_id, deletion_reason, copy_utility_bill_id, billing_allocation_id, imported_ap_header_id, utility_bill_pull_id, ignore_third_party_data_entry, prior_balance, payments, adjustments, balance_forward, is_force_export, budget_balance_amount, is_due_upon_receipt, is_do_not_pay, no_due_date, no_bill_date )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityBillTemplateId() . ', ' .
						$this->sqlUtilityBillTypeId() . ', ' .
						$this->sqlUtilityBillReceiptTypeId() . ', ' .
						$this->sqlUtilityBillAccountId() . ', ' .
						$this->sqlSummaryUtilityBillId() . ', ' .
						$this->sqlEstimatedUtilityBillId() . ', ' .
						$this->sqlTransactionId() . ', ' .
						$this->sqlUtilityDocumentId() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlApPayeeLocationId() . ', ' .
						$this->sqlApPayeeAccountId() . ', ' .
						$this->sqlApHeaderId() . ', ' .
						$this->sqlReviewUtilityBillId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlApHeaderSubTypeId() . ', ' .
						$this->sqlApExceptionQueueItemId() . ', ' .
						$this->sqlPoApHeaderIds() . ', ' .
						$this->sqlSummaryUtilityBillPageNumber() . ', ' .
						$this->sqlBillDatetime() . ', ' .
						$this->sqlBillNumber() . ', ' .
						$this->sqlCurrentAmount() . ', ' .
						$this->sqlTotalAmount() . ', ' .
						$this->sqlBillDate() . ', ' .
						$this->sqlDueDate() . ', ' .
						$this->sqlBillingStartDate() . ', ' .
						$this->sqlBillingEndDate() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlIsHistorical() . ', ' .
						$this->sqlIsQueueIgnored() . ', ' .
						$this->sqlIsIgnoreInvoiceTracking() . ', ' .
						$this->sqlIsOcred() . ', ' .
						$this->sqlIsNoBillAvailable() . ', ' .
						$this->sqlStartProcessDatetime() . ', ' .
						$this->sqlEndProcessDatetime() . ', ' .
						$this->sqlLastProcessedBy() . ', ' .
						$this->sqlStartAssociateDatetime() . ', ' .
						$this->sqlEndAssociateDatetime() . ', ' .
						$this->sqlLastAssociatedBy() . ', ' .
						$this->sqlProcessedBy() . ', ' .
						$this->sqlProcessedOn() . ', ' .
						$this->sqlProcessedAgainBy() . ', ' .
						$this->sqlProcessedAgainOn() . ', ' .
						$this->sqlProcessedOnTime() . ', ' .
						$this->sqlProcessedAgainOnTime() . ', ' .
						$this->sqlStartReviewDatetime() . ', ' .
						$this->sqlEndReviewDatetime() . ', ' .
						$this->sqlScheduledExportDate() . ', ' .
						$this->sqlLastReviewedBy() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlAssociatedBy() . ', ' .
						$this->sqlAssociatedOn() . ', ' .
						$this->sqlAssociatedAgainBy() . ', ' .
						$this->sqlAssociatedAgainOn() . ', ' .
						$this->sqlSplitBy() . ', ' .
						$this->sqlSplitOn() . ', ' .
						$this->sqlSplitAgainBy() . ', ' .
						$this->sqlSplitAgainOn() . ', ' .
						$this->sqlReviewedBy() . ', ' .
						$this->sqlReviewedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlExceptionQueueItemCanceledBy() . ', ' .
						$this->sqlExceptionQueueItemCanceledOn() . ', ' .
						$this->sqlExceptionQueueItemNote() . ', ' .
						$this->sqlOcrTemplateId() . ', ' .
						$this->sqlDeletionReason() . ', ' .
						$this->sqlCopyUtilityBillId() . ', ' .
						$this->sqlBillingAllocationId() . ', ' .
						$this->sqlImportedApHeaderId() . ', ' .
						$this->sqlUtilityBillPullId() . ', ' .
						$this->sqlIgnoreThirdPartyDataEntry() . ', ' .
						$this->sqlPriorBalance() . ', ' .
						$this->sqlPayments() . ', ' .
						$this->sqlAdjustments() . ', ' .
						$this->sqlBalanceForward() . ', ' .
						$this->sqlIsForceExport() . ', ' .
						$this->sqlBudgetBalanceAmount() . ', ' .
						$this->sqlIsDueUponReceipt() . ', ' .
						$this->sqlIsDoNotPay() . ', ' .
						$this->sqlNoDueDate() . ', ' .
						$this->sqlNoBillDate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_template_id = ' . $this->sqlUtilityBillTemplateId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_template_id = ' . $this->sqlUtilityBillTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_type_id = ' . $this->sqlUtilityBillTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_type_id = ' . $this->sqlUtilityBillTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_receipt_type_id = ' . $this->sqlUtilityBillReceiptTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillReceiptTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_receipt_type_id = ' . $this->sqlUtilityBillReceiptTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary_utility_bill_id = ' . $this->sqlSummaryUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'SummaryUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' summary_utility_bill_id = ' . $this->sqlSummaryUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_utility_bill_id = ' . $this->sqlEstimatedUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'EstimatedUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' estimated_utility_bill_id = ' . $this->sqlEstimatedUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId(). ',' ; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId(). ',' ; } elseif( true == array_key_exists( 'UtilityDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_account_id = ' . $this->sqlApPayeeAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_utility_bill_id = ' . $this->sqlReviewUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'ReviewUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' review_utility_bill_id = ' . $this->sqlReviewUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_sub_type_id = ' . $this->sqlApHeaderSubTypeId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_sub_type_id = ' . $this->sqlApHeaderSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_exception_queue_item_id = ' . $this->sqlApExceptionQueueItemId(). ',' ; } elseif( true == array_key_exists( 'ApExceptionQueueItemId', $this->getChangedColumns() ) ) { $strSql .= ' ap_exception_queue_item_id = ' . $this->sqlApExceptionQueueItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' po_ap_header_ids = ' . $this->sqlPoApHeaderIds(). ',' ; } elseif( true == array_key_exists( 'PoApHeaderIds', $this->getChangedColumns() ) ) { $strSql .= ' po_ap_header_ids = ' . $this->sqlPoApHeaderIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary_utility_bill_page_number = ' . $this->sqlSummaryUtilityBillPageNumber(). ',' ; } elseif( true == array_key_exists( 'SummaryUtilityBillPageNumber', $this->getChangedColumns() ) ) { $strSql .= ' summary_utility_bill_page_number = ' . $this->sqlSummaryUtilityBillPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_datetime = ' . $this->sqlBillDatetime(). ',' ; } elseif( true == array_key_exists( 'BillDatetime', $this->getChangedColumns() ) ) { $strSql .= ' bill_datetime = ' . $this->sqlBillDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_number = ' . $this->sqlBillNumber(). ',' ; } elseif( true == array_key_exists( 'BillNumber', $this->getChangedColumns() ) ) { $strSql .= ' bill_number = ' . $this->sqlBillNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_amount = ' . $this->sqlCurrentAmount(). ',' ; } elseif( true == array_key_exists( 'CurrentAmount', $this->getChangedColumns() ) ) { $strSql .= ' current_amount = ' . $this->sqlCurrentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_amount = ' . $this->sqlTotalAmount(). ',' ; } elseif( true == array_key_exists( 'TotalAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_amount = ' . $this->sqlTotalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_date = ' . $this->sqlBillDate(). ',' ; } elseif( true == array_key_exists( 'BillDate', $this->getChangedColumns() ) ) { $strSql .= ' bill_date = ' . $this->sqlBillDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date = ' . $this->sqlDueDate(). ',' ; } elseif( true == array_key_exists( 'DueDate', $this->getChangedColumns() ) ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_start_date = ' . $this->sqlBillingStartDate(). ',' ; } elseif( true == array_key_exists( 'BillingStartDate', $this->getChangedColumns() ) ) { $strSql .= ' billing_start_date = ' . $this->sqlBillingStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_end_date = ' . $this->sqlBillingEndDate(). ',' ; } elseif( true == array_key_exists( 'BillingEndDate', $this->getChangedColumns() ) ) { $strSql .= ' billing_end_date = ' . $this->sqlBillingEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_historical = ' . $this->sqlIsHistorical(). ',' ; } elseif( true == array_key_exists( 'IsHistorical', $this->getChangedColumns() ) ) { $strSql .= ' is_historical = ' . $this->sqlIsHistorical() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_queue_ignored = ' . $this->sqlIsQueueIgnored(). ',' ; } elseif( true == array_key_exists( 'IsQueueIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_queue_ignored = ' . $this->sqlIsQueueIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ignore_invoice_tracking = ' . $this->sqlIsIgnoreInvoiceTracking(). ',' ; } elseif( true == array_key_exists( 'IsIgnoreInvoiceTracking', $this->getChangedColumns() ) ) { $strSql .= ' is_ignore_invoice_tracking = ' . $this->sqlIsIgnoreInvoiceTracking() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ocred = ' . $this->sqlIsOcred(). ',' ; } elseif( true == array_key_exists( 'IsOcred', $this->getChangedColumns() ) ) { $strSql .= ' is_ocred = ' . $this->sqlIsOcred() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_no_bill_available = ' . $this->sqlIsNoBillAvailable(). ',' ; } elseif( true == array_key_exists( 'IsNoBillAvailable', $this->getChangedColumns() ) ) { $strSql .= ' is_no_bill_available = ' . $this->sqlIsNoBillAvailable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_process_datetime = ' . $this->sqlStartProcessDatetime(). ',' ; } elseif( true == array_key_exists( 'StartProcessDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_process_datetime = ' . $this->sqlStartProcessDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_process_datetime = ' . $this->sqlEndProcessDatetime(). ',' ; } elseif( true == array_key_exists( 'EndProcessDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_process_datetime = ' . $this->sqlEndProcessDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_processed_by = ' . $this->sqlLastProcessedBy(). ',' ; } elseif( true == array_key_exists( 'LastProcessedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_processed_by = ' . $this->sqlLastProcessedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_associate_datetime = ' . $this->sqlStartAssociateDatetime(). ',' ; } elseif( true == array_key_exists( 'StartAssociateDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_associate_datetime = ' . $this->sqlStartAssociateDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_associate_datetime = ' . $this->sqlEndAssociateDatetime(). ',' ; } elseif( true == array_key_exists( 'EndAssociateDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_associate_datetime = ' . $this->sqlEndAssociateDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_associated_by = ' . $this->sqlLastAssociatedBy(). ',' ; } elseif( true == array_key_exists( 'LastAssociatedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_associated_by = ' . $this->sqlLastAssociatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy(). ',' ; } elseif( true == array_key_exists( 'ProcessedBy', $this->getChangedColumns() ) ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn(). ',' ; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_again_by = ' . $this->sqlProcessedAgainBy(). ',' ; } elseif( true == array_key_exists( 'ProcessedAgainBy', $this->getChangedColumns() ) ) { $strSql .= ' processed_again_by = ' . $this->sqlProcessedAgainBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_again_on = ' . $this->sqlProcessedAgainOn(). ',' ; } elseif( true == array_key_exists( 'ProcessedAgainOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_again_on = ' . $this->sqlProcessedAgainOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on_time = ' . $this->sqlProcessedOnTime(). ',' ; } elseif( true == array_key_exists( 'ProcessedOnTime', $this->getChangedColumns() ) ) { $strSql .= ' processed_on_time = ' . $this->sqlProcessedOnTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_again_on_time = ' . $this->sqlProcessedAgainOnTime(). ',' ; } elseif( true == array_key_exists( 'ProcessedAgainOnTime', $this->getChangedColumns() ) ) { $strSql .= ' processed_again_on_time = ' . $this->sqlProcessedAgainOnTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_review_datetime = ' . $this->sqlStartReviewDatetime(). ',' ; } elseif( true == array_key_exists( 'StartReviewDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_review_datetime = ' . $this->sqlStartReviewDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_review_datetime = ' . $this->sqlEndReviewDatetime(). ',' ; } elseif( true == array_key_exists( 'EndReviewDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_review_datetime = ' . $this->sqlEndReviewDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_export_date = ' . $this->sqlScheduledExportDate(). ',' ; } elseif( true == array_key_exists( 'ScheduledExportDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_export_date = ' . $this->sqlScheduledExportDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_reviewed_by = ' . $this->sqlLastReviewedBy(). ',' ; } elseif( true == array_key_exists( 'LastReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_reviewed_by = ' . $this->sqlLastReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy(). ',' ; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_by = ' . $this->sqlAssociatedBy(). ',' ; } elseif( true == array_key_exists( 'AssociatedBy', $this->getChangedColumns() ) ) { $strSql .= ' associated_by = ' . $this->sqlAssociatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_on = ' . $this->sqlAssociatedOn(). ',' ; } elseif( true == array_key_exists( 'AssociatedOn', $this->getChangedColumns() ) ) { $strSql .= ' associated_on = ' . $this->sqlAssociatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_again_by = ' . $this->sqlAssociatedAgainBy(). ',' ; } elseif( true == array_key_exists( 'AssociatedAgainBy', $this->getChangedColumns() ) ) { $strSql .= ' associated_again_by = ' . $this->sqlAssociatedAgainBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_again_on = ' . $this->sqlAssociatedAgainOn(). ',' ; } elseif( true == array_key_exists( 'AssociatedAgainOn', $this->getChangedColumns() ) ) { $strSql .= ' associated_again_on = ' . $this->sqlAssociatedAgainOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' split_by = ' . $this->sqlSplitBy(). ',' ; } elseif( true == array_key_exists( 'SplitBy', $this->getChangedColumns() ) ) { $strSql .= ' split_by = ' . $this->sqlSplitBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' split_on = ' . $this->sqlSplitOn(). ',' ; } elseif( true == array_key_exists( 'SplitOn', $this->getChangedColumns() ) ) { $strSql .= ' split_on = ' . $this->sqlSplitOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' split_again_by = ' . $this->sqlSplitAgainBy(). ',' ; } elseif( true == array_key_exists( 'SplitAgainBy', $this->getChangedColumns() ) ) { $strSql .= ' split_again_by = ' . $this->sqlSplitAgainBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' split_again_on = ' . $this->sqlSplitAgainOn(). ',' ; } elseif( true == array_key_exists( 'SplitAgainOn', $this->getChangedColumns() ) ) { $strSql .= ' split_again_on = ' . $this->sqlSplitAgainOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy(). ',' ; } elseif( true == array_key_exists( 'ReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn(). ',' ; } elseif( true == array_key_exists( 'ReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exception_queue_item_canceled_by = ' . $this->sqlExceptionQueueItemCanceledBy(). ',' ; } elseif( true == array_key_exists( 'ExceptionQueueItemCanceledBy', $this->getChangedColumns() ) ) { $strSql .= ' exception_queue_item_canceled_by = ' . $this->sqlExceptionQueueItemCanceledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exception_queue_item_canceled_on = ' . $this->sqlExceptionQueueItemCanceledOn(). ',' ; } elseif( true == array_key_exists( 'ExceptionQueueItemCanceledOn', $this->getChangedColumns() ) ) { $strSql .= ' exception_queue_item_canceled_on = ' . $this->sqlExceptionQueueItemCanceledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exception_queue_item_note = ' . $this->sqlExceptionQueueItemNote(). ',' ; } elseif( true == array_key_exists( 'ExceptionQueueItemNote', $this->getChangedColumns() ) ) { $strSql .= ' exception_queue_item_note = ' . $this->sqlExceptionQueueItemNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_template_id = ' . $this->sqlOcrTemplateId(). ',' ; } elseif( true == array_key_exists( 'OcrTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_template_id = ' . $this->sqlOcrTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deletion_reason = ' . $this->sqlDeletionReason(). ',' ; } elseif( true == array_key_exists( 'DeletionReason', $this->getChangedColumns() ) ) { $strSql .= ' deletion_reason = ' . $this->sqlDeletionReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' copy_utility_bill_id = ' . $this->sqlCopyUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'CopyUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' copy_utility_bill_id = ' . $this->sqlCopyUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_allocation_id = ' . $this->sqlBillingAllocationId(). ',' ; } elseif( true == array_key_exists( 'BillingAllocationId', $this->getChangedColumns() ) ) { $strSql .= ' billing_allocation_id = ' . $this->sqlBillingAllocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_ap_header_id = ' . $this->sqlImportedApHeaderId(). ',' ; } elseif( true == array_key_exists( 'ImportedApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' imported_ap_header_id = ' . $this->sqlImportedApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_pull_id = ' . $this->sqlUtilityBillPullId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillPullId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_pull_id = ' . $this->sqlUtilityBillPullId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignore_third_party_data_entry = ' . $this->sqlIgnoreThirdPartyDataEntry(). ',' ; } elseif( true == array_key_exists( 'IgnoreThirdPartyDataEntry', $this->getChangedColumns() ) ) { $strSql .= ' ignore_third_party_data_entry = ' . $this->sqlIgnoreThirdPartyDataEntry() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_balance = ' . $this->sqlPriorBalance(). ',' ; } elseif( true == array_key_exists( 'PriorBalance', $this->getChangedColumns() ) ) { $strSql .= ' prior_balance = ' . $this->sqlPriorBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payments = ' . $this->sqlPayments(). ',' ; } elseif( true == array_key_exists( 'Payments', $this->getChangedColumns() ) ) { $strSql .= ' payments = ' . $this->sqlPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustments = ' . $this->sqlAdjustments(). ',' ; } elseif( true == array_key_exists( 'Adjustments', $this->getChangedColumns() ) ) { $strSql .= ' adjustments = ' . $this->sqlAdjustments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' balance_forward = ' . $this->sqlBalanceForward(). ',' ; } elseif( true == array_key_exists( 'BalanceForward', $this->getChangedColumns() ) ) { $strSql .= ' balance_forward = ' . $this->sqlBalanceForward() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_force_export = ' . $this->sqlIsForceExport(). ',' ; } elseif( true == array_key_exists( 'IsForceExport', $this->getChangedColumns() ) ) { $strSql .= ' is_force_export = ' . $this->sqlIsForceExport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_balance_amount = ' . $this->sqlBudgetBalanceAmount(). ',' ; } elseif( true == array_key_exists( 'BudgetBalanceAmount', $this->getChangedColumns() ) ) { $strSql .= ' budget_balance_amount = ' . $this->sqlBudgetBalanceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_due_upon_receipt = ' . $this->sqlIsDueUponReceipt(). ',' ; } elseif( true == array_key_exists( 'IsDueUponReceipt', $this->getChangedColumns() ) ) { $strSql .= ' is_due_upon_receipt = ' . $this->sqlIsDueUponReceipt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_do_not_pay = ' . $this->sqlIsDoNotPay(). ',' ; } elseif( true == array_key_exists( 'IsDoNotPay', $this->getChangedColumns() ) ) { $strSql .= ' is_do_not_pay = ' . $this->sqlIsDoNotPay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' no_due_date = ' . $this->sqlNoDueDate(). ',' ; } elseif( true == array_key_exists( 'NoDueDate', $this->getChangedColumns() ) ) { $strSql .= ' no_due_date = ' . $this->sqlNoDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' no_bill_date = ' . $this->sqlNoBillDate(). ',' ; } elseif( true == array_key_exists( 'NoBillDate', $this->getChangedColumns() ) ) { $strSql .= ' no_bill_date = ' . $this->sqlNoBillDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_template_id' => $this->getUtilityBillTemplateId(),
			'utility_bill_type_id' => $this->getUtilityBillTypeId(),
			'utility_bill_receipt_type_id' => $this->getUtilityBillReceiptTypeId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'summary_utility_bill_id' => $this->getSummaryUtilityBillId(),
			'estimated_utility_bill_id' => $this->getEstimatedUtilityBillId(),
			'transaction_id' => $this->getTransactionId(),
			'utility_document_id' => $this->getUtilityDocumentId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'ap_payee_account_id' => $this->getApPayeeAccountId(),
			'ap_header_id' => $this->getApHeaderId(),
			'review_utility_bill_id' => $this->getReviewUtilityBillId(),
			'ps_product_id' => $this->getPsProductId(),
			'ap_header_sub_type_id' => $this->getApHeaderSubTypeId(),
			'ap_exception_queue_item_id' => $this->getApExceptionQueueItemId(),
			'po_ap_header_ids' => $this->getPoApHeaderIds(),
			'summary_utility_bill_page_number' => $this->getSummaryUtilityBillPageNumber(),
			'bill_datetime' => $this->getBillDatetime(),
			'bill_number' => $this->getBillNumber(),
			'current_amount' => $this->getCurrentAmount(),
			'total_amount' => $this->getTotalAmount(),
			'bill_date' => $this->getBillDate(),
			'due_date' => $this->getDueDate(),
			'billing_start_date' => $this->getBillingStartDate(),
			'billing_end_date' => $this->getBillingEndDate(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'notes' => $this->getNotes(),
			'is_historical' => $this->getIsHistorical(),
			'is_queue_ignored' => $this->getIsQueueIgnored(),
			'is_ignore_invoice_tracking' => $this->getIsIgnoreInvoiceTracking(),
			'is_ocred' => $this->getIsOcred(),
			'is_no_bill_available' => $this->getIsNoBillAvailable(),
			'start_process_datetime' => $this->getStartProcessDatetime(),
			'end_process_datetime' => $this->getEndProcessDatetime(),
			'last_processed_by' => $this->getLastProcessedBy(),
			'start_associate_datetime' => $this->getStartAssociateDatetime(),
			'end_associate_datetime' => $this->getEndAssociateDatetime(),
			'last_associated_by' => $this->getLastAssociatedBy(),
			'processed_by' => $this->getProcessedBy(),
			'processed_on' => $this->getProcessedOn(),
			'processed_again_by' => $this->getProcessedAgainBy(),
			'processed_again_on' => $this->getProcessedAgainOn(),
			'processed_on_time' => $this->getProcessedOnTime(),
			'processed_again_on_time' => $this->getProcessedAgainOnTime(),
			'start_review_datetime' => $this->getStartReviewDatetime(),
			'end_review_datetime' => $this->getEndReviewDatetime(),
			'scheduled_export_date' => $this->getScheduledExportDate(),
			'last_reviewed_by' => $this->getLastReviewedBy(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn(),
			'associated_by' => $this->getAssociatedBy(),
			'associated_on' => $this->getAssociatedOn(),
			'associated_again_by' => $this->getAssociatedAgainBy(),
			'associated_again_on' => $this->getAssociatedAgainOn(),
			'split_by' => $this->getSplitBy(),
			'split_on' => $this->getSplitOn(),
			'split_again_by' => $this->getSplitAgainBy(),
			'split_again_on' => $this->getSplitAgainOn(),
			'reviewed_by' => $this->getReviewedBy(),
			'reviewed_on' => $this->getReviewedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'exception_queue_item_canceled_by' => $this->getExceptionQueueItemCanceledBy(),
			'exception_queue_item_canceled_on' => $this->getExceptionQueueItemCanceledOn(),
			'exception_queue_item_note' => $this->getExceptionQueueItemNote(),
			'ocr_template_id' => $this->getOcrTemplateId(),
			'deletion_reason' => $this->getDeletionReason(),
			'copy_utility_bill_id' => $this->getCopyUtilityBillId(),
			'billing_allocation_id' => $this->getBillingAllocationId(),
			'imported_ap_header_id' => $this->getImportedApHeaderId(),
			'utility_bill_pull_id' => $this->getUtilityBillPullId(),
			'ignore_third_party_data_entry' => $this->getIgnoreThirdPartyDataEntry(),
			'prior_balance' => $this->getPriorBalance(),
			'payments' => $this->getPayments(),
			'adjustments' => $this->getAdjustments(),
			'balance_forward' => $this->getBalanceForward(),
			'is_force_export' => $this->getIsForceExport(),
			'budget_balance_amount' => $this->getBudgetBalanceAmount(),
			'is_due_upon_receipt' => $this->getIsDueUponReceipt(),
			'is_do_not_pay' => $this->getIsDoNotPay(),
			'no_due_date' => $this->getNoDueDate(),
			'no_bill_date' => $this->getNoBillDate()
		);
	}

}
?>