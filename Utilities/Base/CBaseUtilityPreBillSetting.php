<?php

class CBaseUtilityPreBillSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_pre_bill_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUtilitySettingId;
	protected $m_intWaivePrebillApprovalDays;
	protected $m_strBillingMonth;
	protected $m_strBillingInstructions;
	protected $m_strBillingSpecialistNote;
	protected $m_strMeterMaintenanceNotes;
	protected $m_boolIsConvergent;
	protected $m_boolDontApproveConvergentServicePeriod;
	protected $m_boolInvoiceIsDueUponReceipt;
	protected $m_boolApplyUsageFactor;
	protected $m_boolUnitSpaceUsageFactor;
	protected $m_boolIsSubsidyOnUnit;
	protected $m_boolIsManagerApprovalMode;
	protected $m_boolIsNextMonthPostMonth;
	protected $m_intMoveOutEstimationMonths;
	protected $m_boolLeaseStartDateAsMoveInDate;
	protected $m_boolDontSubsidyBill;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsShowUnitDetails;
	protected $m_strQcNotes;
	protected $m_boolIsNoBillingFee;
	protected $m_arrintBilledLedgerIds;
	protected $m_boolAutoApprovePreBill;
	protected $m_boolReallocateProratedExpense;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsConvergent = false;
		$this->m_boolDontApproveConvergentServicePeriod = false;
		$this->m_boolInvoiceIsDueUponReceipt = false;
		$this->m_boolApplyUsageFactor = false;
		$this->m_boolUnitSpaceUsageFactor = false;
		$this->m_boolIsSubsidyOnUnit = false;
		$this->m_boolIsManagerApprovalMode = false;
		$this->m_boolIsNextMonthPostMonth = true;
		$this->m_intMoveOutEstimationMonths = '3';
		$this->m_boolLeaseStartDateAsMoveInDate = false;
		$this->m_boolDontSubsidyBill = false;
		$this->m_boolIsShowUnitDetails = false;
		$this->m_boolIsNoBillingFee = false;
		$this->m_boolAutoApprovePreBill = true;
		$this->m_boolReallocateProratedExpense = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_utility_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilitySettingId', trim( $arrValues['property_utility_setting_id'] ) ); elseif( isset( $arrValues['property_utility_setting_id'] ) ) $this->setPropertyUtilitySettingId( $arrValues['property_utility_setting_id'] );
		if( isset( $arrValues['waive_prebill_approval_days'] ) && $boolDirectSet ) $this->set( 'm_intWaivePrebillApprovalDays', trim( $arrValues['waive_prebill_approval_days'] ) ); elseif( isset( $arrValues['waive_prebill_approval_days'] ) ) $this->setWaivePrebillApprovalDays( $arrValues['waive_prebill_approval_days'] );
		if( isset( $arrValues['billing_month'] ) && $boolDirectSet ) $this->set( 'm_strBillingMonth', trim( $arrValues['billing_month'] ) ); elseif( isset( $arrValues['billing_month'] ) ) $this->setBillingMonth( $arrValues['billing_month'] );
		if( isset( $arrValues['billing_instructions'] ) && $boolDirectSet ) $this->set( 'm_strBillingInstructions', trim( $arrValues['billing_instructions'] ) ); elseif( isset( $arrValues['billing_instructions'] ) ) $this->setBillingInstructions( $arrValues['billing_instructions'] );
		if( isset( $arrValues['billing_specialist_note'] ) && $boolDirectSet ) $this->set( 'm_strBillingSpecialistNote', trim( $arrValues['billing_specialist_note'] ) ); elseif( isset( $arrValues['billing_specialist_note'] ) ) $this->setBillingSpecialistNote( $arrValues['billing_specialist_note'] );
		if( isset( $arrValues['meter_maintenance_notes'] ) && $boolDirectSet ) $this->set( 'm_strMeterMaintenanceNotes', trim( $arrValues['meter_maintenance_notes'] ) ); elseif( isset( $arrValues['meter_maintenance_notes'] ) ) $this->setMeterMaintenanceNotes( $arrValues['meter_maintenance_notes'] );
		if( isset( $arrValues['is_convergent'] ) && $boolDirectSet ) $this->set( 'm_boolIsConvergent', trim( stripcslashes( $arrValues['is_convergent'] ) ) ); elseif( isset( $arrValues['is_convergent'] ) ) $this->setIsConvergent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_convergent'] ) : $arrValues['is_convergent'] );
		if( isset( $arrValues['dont_approve_convergent_service_period'] ) && $boolDirectSet ) $this->set( 'm_boolDontApproveConvergentServicePeriod', trim( stripcslashes( $arrValues['dont_approve_convergent_service_period'] ) ) ); elseif( isset( $arrValues['dont_approve_convergent_service_period'] ) ) $this->setDontApproveConvergentServicePeriod( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_approve_convergent_service_period'] ) : $arrValues['dont_approve_convergent_service_period'] );
		if( isset( $arrValues['invoice_is_due_upon_receipt'] ) && $boolDirectSet ) $this->set( 'm_boolInvoiceIsDueUponReceipt', trim( stripcslashes( $arrValues['invoice_is_due_upon_receipt'] ) ) ); elseif( isset( $arrValues['invoice_is_due_upon_receipt'] ) ) $this->setInvoiceIsDueUponReceipt( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['invoice_is_due_upon_receipt'] ) : $arrValues['invoice_is_due_upon_receipt'] );
		if( isset( $arrValues['apply_usage_factor'] ) && $boolDirectSet ) $this->set( 'm_boolApplyUsageFactor', trim( stripcslashes( $arrValues['apply_usage_factor'] ) ) ); elseif( isset( $arrValues['apply_usage_factor'] ) ) $this->setApplyUsageFactor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['apply_usage_factor'] ) : $arrValues['apply_usage_factor'] );
		if( isset( $arrValues['unit_space_usage_factor'] ) && $boolDirectSet ) $this->set( 'm_boolUnitSpaceUsageFactor', trim( stripcslashes( $arrValues['unit_space_usage_factor'] ) ) ); elseif( isset( $arrValues['unit_space_usage_factor'] ) ) $this->setUnitSpaceUsageFactor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_space_usage_factor'] ) : $arrValues['unit_space_usage_factor'] );
		if( isset( $arrValues['is_subsidy_on_unit'] ) && $boolDirectSet ) $this->set( 'm_boolIsSubsidyOnUnit', trim( stripcslashes( $arrValues['is_subsidy_on_unit'] ) ) ); elseif( isset( $arrValues['is_subsidy_on_unit'] ) ) $this->setIsSubsidyOnUnit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_subsidy_on_unit'] ) : $arrValues['is_subsidy_on_unit'] );
		if( isset( $arrValues['is_manager_approval_mode'] ) && $boolDirectSet ) $this->set( 'm_boolIsManagerApprovalMode', trim( stripcslashes( $arrValues['is_manager_approval_mode'] ) ) ); elseif( isset( $arrValues['is_manager_approval_mode'] ) ) $this->setIsManagerApprovalMode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manager_approval_mode'] ) : $arrValues['is_manager_approval_mode'] );
		if( isset( $arrValues['is_next_month_post_month'] ) && $boolDirectSet ) $this->set( 'm_boolIsNextMonthPostMonth', trim( stripcslashes( $arrValues['is_next_month_post_month'] ) ) ); elseif( isset( $arrValues['is_next_month_post_month'] ) ) $this->setIsNextMonthPostMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_next_month_post_month'] ) : $arrValues['is_next_month_post_month'] );
		if( isset( $arrValues['move_out_estimation_months'] ) && $boolDirectSet ) $this->set( 'm_intMoveOutEstimationMonths', trim( $arrValues['move_out_estimation_months'] ) ); elseif( isset( $arrValues['move_out_estimation_months'] ) ) $this->setMoveOutEstimationMonths( $arrValues['move_out_estimation_months'] );
		if( isset( $arrValues['lease_start_date_as_move_in_date'] ) && $boolDirectSet ) $this->set( 'm_boolLeaseStartDateAsMoveInDate', trim( stripcslashes( $arrValues['lease_start_date_as_move_in_date'] ) ) ); elseif( isset( $arrValues['lease_start_date_as_move_in_date'] ) ) $this->setLeaseStartDateAsMoveInDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lease_start_date_as_move_in_date'] ) : $arrValues['lease_start_date_as_move_in_date'] );
		if( isset( $arrValues['dont_subsidy_bill'] ) && $boolDirectSet ) $this->set( 'm_boolDontSubsidyBill', trim( stripcslashes( $arrValues['dont_subsidy_bill'] ) ) ); elseif( isset( $arrValues['dont_subsidy_bill'] ) ) $this->setDontSubsidyBill( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_subsidy_bill'] ) : $arrValues['dont_subsidy_bill'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_show_unit_details'] ) && $boolDirectSet ) $this->set( 'm_boolIsShowUnitDetails', trim( stripcslashes( $arrValues['is_show_unit_details'] ) ) ); elseif( isset( $arrValues['is_show_unit_details'] ) ) $this->setIsShowUnitDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_show_unit_details'] ) : $arrValues['is_show_unit_details'] );
		if( isset( $arrValues['qc_notes'] ) && $boolDirectSet ) $this->set( 'm_strQcNotes', trim( $arrValues['qc_notes'] ) ); elseif( isset( $arrValues['qc_notes'] ) ) $this->setQcNotes( $arrValues['qc_notes'] );
		if( isset( $arrValues['is_no_billing_fee'] ) && $boolDirectSet ) $this->set( 'm_boolIsNoBillingFee', trim( stripcslashes( $arrValues['is_no_billing_fee'] ) ) ); elseif( isset( $arrValues['is_no_billing_fee'] ) ) $this->setIsNoBillingFee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_no_billing_fee'] ) : $arrValues['is_no_billing_fee'] );
		if( isset( $arrValues['billed_ledger_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintBilledLedgerIds', trim( $arrValues['billed_ledger_ids'] ) ); elseif( isset( $arrValues['billed_ledger_ids'] ) ) $this->setBilledLedgerIds( $arrValues['billed_ledger_ids'] );
		if( isset( $arrValues['auto_approve_pre_bill'] ) && $boolDirectSet ) $this->set( 'm_boolAutoApprovePreBill', trim( stripcslashes( $arrValues['auto_approve_pre_bill'] ) ) ); elseif( isset( $arrValues['auto_approve_pre_bill'] ) ) $this->setAutoApprovePreBill( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_approve_pre_bill'] ) : $arrValues['auto_approve_pre_bill'] );
		if( isset( $arrValues['reallocate_prorated_expense'] ) && $boolDirectSet ) $this->set( 'm_boolReallocateProratedExpense', trim( stripcslashes( $arrValues['reallocate_prorated_expense'] ) ) ); elseif( isset( $arrValues['reallocate_prorated_expense'] ) ) $this->setReallocateProratedExpense( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reallocate_prorated_expense'] ) : $arrValues['reallocate_prorated_expense'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUtilitySettingId( $intPropertyUtilitySettingId ) {
		$this->set( 'm_intPropertyUtilitySettingId', CStrings::strToIntDef( $intPropertyUtilitySettingId, NULL, false ) );
	}

	public function getPropertyUtilitySettingId() {
		return $this->m_intPropertyUtilitySettingId;
	}

	public function sqlPropertyUtilitySettingId() {
		return ( true == isset( $this->m_intPropertyUtilitySettingId ) ) ? ( string ) $this->m_intPropertyUtilitySettingId : 'NULL';
	}

	public function setWaivePrebillApprovalDays( $intWaivePrebillApprovalDays ) {
		$this->set( 'm_intWaivePrebillApprovalDays', CStrings::strToIntDef( $intWaivePrebillApprovalDays, NULL, false ) );
	}

	public function getWaivePrebillApprovalDays() {
		return $this->m_intWaivePrebillApprovalDays;
	}

	public function sqlWaivePrebillApprovalDays() {
		return ( true == isset( $this->m_intWaivePrebillApprovalDays ) ) ? ( string ) $this->m_intWaivePrebillApprovalDays : 'NULL';
	}

	public function setBillingMonth( $strBillingMonth ) {
		$this->set( 'm_strBillingMonth', CStrings::strTrimDef( $strBillingMonth, -1, NULL, true ) );
	}

	public function getBillingMonth() {
		return $this->m_strBillingMonth;
	}

	public function sqlBillingMonth() {
		return ( true == isset( $this->m_strBillingMonth ) ) ? '\'' . $this->m_strBillingMonth . '\'' : 'NULL';
	}

	public function setBillingInstructions( $strBillingInstructions ) {
		$this->set( 'm_strBillingInstructions', CStrings::strTrimDef( $strBillingInstructions, -1, NULL, true ) );
	}

	public function getBillingInstructions() {
		return $this->m_strBillingInstructions;
	}

	public function sqlBillingInstructions() {
		return ( true == isset( $this->m_strBillingInstructions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBillingInstructions ) : '\'' . addslashes( $this->m_strBillingInstructions ) . '\'' ) : 'NULL';
	}

	public function setBillingSpecialistNote( $strBillingSpecialistNote ) {
		$this->set( 'm_strBillingSpecialistNote', CStrings::strTrimDef( $strBillingSpecialistNote, -1, NULL, true ) );
	}

	public function getBillingSpecialistNote() {
		return $this->m_strBillingSpecialistNote;
	}

	public function sqlBillingSpecialistNote() {
		return ( true == isset( $this->m_strBillingSpecialistNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBillingSpecialistNote ) : '\'' . addslashes( $this->m_strBillingSpecialistNote ) . '\'' ) : 'NULL';
	}

	public function setMeterMaintenanceNotes( $strMeterMaintenanceNotes ) {
		$this->set( 'm_strMeterMaintenanceNotes', CStrings::strTrimDef( $strMeterMaintenanceNotes, -1, NULL, true ) );
	}

	public function getMeterMaintenanceNotes() {
		return $this->m_strMeterMaintenanceNotes;
	}

	public function sqlMeterMaintenanceNotes() {
		return ( true == isset( $this->m_strMeterMaintenanceNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMeterMaintenanceNotes ) : '\'' . addslashes( $this->m_strMeterMaintenanceNotes ) . '\'' ) : 'NULL';
	}

	public function setIsConvergent( $boolIsConvergent ) {
		$this->set( 'm_boolIsConvergent', CStrings::strToBool( $boolIsConvergent ) );
	}

	public function getIsConvergent() {
		return $this->m_boolIsConvergent;
	}

	public function sqlIsConvergent() {
		return ( true == isset( $this->m_boolIsConvergent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConvergent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontApproveConvergentServicePeriod( $boolDontApproveConvergentServicePeriod ) {
		$this->set( 'm_boolDontApproveConvergentServicePeriod', CStrings::strToBool( $boolDontApproveConvergentServicePeriod ) );
	}

	public function getDontApproveConvergentServicePeriod() {
		return $this->m_boolDontApproveConvergentServicePeriod;
	}

	public function sqlDontApproveConvergentServicePeriod() {
		return ( true == isset( $this->m_boolDontApproveConvergentServicePeriod ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontApproveConvergentServicePeriod ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInvoiceIsDueUponReceipt( $boolInvoiceIsDueUponReceipt ) {
		$this->set( 'm_boolInvoiceIsDueUponReceipt', CStrings::strToBool( $boolInvoiceIsDueUponReceipt ) );
	}

	public function getInvoiceIsDueUponReceipt() {
		return $this->m_boolInvoiceIsDueUponReceipt;
	}

	public function sqlInvoiceIsDueUponReceipt() {
		return ( true == isset( $this->m_boolInvoiceIsDueUponReceipt ) ) ? '\'' . ( true == ( bool ) $this->m_boolInvoiceIsDueUponReceipt ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApplyUsageFactor( $boolApplyUsageFactor ) {
		$this->set( 'm_boolApplyUsageFactor', CStrings::strToBool( $boolApplyUsageFactor ) );
	}

	public function getApplyUsageFactor() {
		return $this->m_boolApplyUsageFactor;
	}

	public function sqlApplyUsageFactor() {
		return ( true == isset( $this->m_boolApplyUsageFactor ) ) ? '\'' . ( true == ( bool ) $this->m_boolApplyUsageFactor ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUnitSpaceUsageFactor( $boolUnitSpaceUsageFactor ) {
		$this->set( 'm_boolUnitSpaceUsageFactor', CStrings::strToBool( $boolUnitSpaceUsageFactor ) );
	}

	public function getUnitSpaceUsageFactor() {
		return $this->m_boolUnitSpaceUsageFactor;
	}

	public function sqlUnitSpaceUsageFactor() {
		return ( true == isset( $this->m_boolUnitSpaceUsageFactor ) ) ? '\'' . ( true == ( bool ) $this->m_boolUnitSpaceUsageFactor ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSubsidyOnUnit( $boolIsSubsidyOnUnit ) {
		$this->set( 'm_boolIsSubsidyOnUnit', CStrings::strToBool( $boolIsSubsidyOnUnit ) );
	}

	public function getIsSubsidyOnUnit() {
		return $this->m_boolIsSubsidyOnUnit;
	}

	public function sqlIsSubsidyOnUnit() {
		return ( true == isset( $this->m_boolIsSubsidyOnUnit ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSubsidyOnUnit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsManagerApprovalMode( $boolIsManagerApprovalMode ) {
		$this->set( 'm_boolIsManagerApprovalMode', CStrings::strToBool( $boolIsManagerApprovalMode ) );
	}

	public function getIsManagerApprovalMode() {
		return $this->m_boolIsManagerApprovalMode;
	}

	public function sqlIsManagerApprovalMode() {
		return ( true == isset( $this->m_boolIsManagerApprovalMode ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManagerApprovalMode ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsNextMonthPostMonth( $boolIsNextMonthPostMonth ) {
		$this->set( 'm_boolIsNextMonthPostMonth', CStrings::strToBool( $boolIsNextMonthPostMonth ) );
	}

	public function getIsNextMonthPostMonth() {
		return $this->m_boolIsNextMonthPostMonth;
	}

	public function sqlIsNextMonthPostMonth() {
		return ( true == isset( $this->m_boolIsNextMonthPostMonth ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNextMonthPostMonth ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMoveOutEstimationMonths( $intMoveOutEstimationMonths ) {
		$this->set( 'm_intMoveOutEstimationMonths', CStrings::strToIntDef( $intMoveOutEstimationMonths, NULL, false ) );
	}

	public function getMoveOutEstimationMonths() {
		return $this->m_intMoveOutEstimationMonths;
	}

	public function sqlMoveOutEstimationMonths() {
		return ( true == isset( $this->m_intMoveOutEstimationMonths ) ) ? ( string ) $this->m_intMoveOutEstimationMonths : '3';
	}

	public function setLeaseStartDateAsMoveInDate( $boolLeaseStartDateAsMoveInDate ) {
		$this->set( 'm_boolLeaseStartDateAsMoveInDate', CStrings::strToBool( $boolLeaseStartDateAsMoveInDate ) );
	}

	public function getLeaseStartDateAsMoveInDate() {
		return $this->m_boolLeaseStartDateAsMoveInDate;
	}

	public function sqlLeaseStartDateAsMoveInDate() {
		return ( true == isset( $this->m_boolLeaseStartDateAsMoveInDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolLeaseStartDateAsMoveInDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontSubsidyBill( $boolDontSubsidyBill ) {
		$this->set( 'm_boolDontSubsidyBill', CStrings::strToBool( $boolDontSubsidyBill ) );
	}

	public function getDontSubsidyBill() {
		return $this->m_boolDontSubsidyBill;
	}

	public function sqlDontSubsidyBill() {
		return ( true == isset( $this->m_boolDontSubsidyBill ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontSubsidyBill ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsShowUnitDetails( $boolIsShowUnitDetails ) {
		$this->set( 'm_boolIsShowUnitDetails', CStrings::strToBool( $boolIsShowUnitDetails ) );
	}

	public function getIsShowUnitDetails() {
		return $this->m_boolIsShowUnitDetails;
	}

	public function sqlIsShowUnitDetails() {
		return ( true == isset( $this->m_boolIsShowUnitDetails ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsShowUnitDetails ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setQcNotes( $strQcNotes ) {
		$this->set( 'm_strQcNotes', CStrings::strTrimDef( $strQcNotes, -1, NULL, true ) );
	}

	public function getQcNotes() {
		return $this->m_strQcNotes;
	}

	public function sqlQcNotes() {
		return ( true == isset( $this->m_strQcNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strQcNotes ) : '\'' . addslashes( $this->m_strQcNotes ) . '\'' ) : 'NULL';
	}

	public function setIsNoBillingFee( $boolIsNoBillingFee ) {
		$this->set( 'm_boolIsNoBillingFee', CStrings::strToBool( $boolIsNoBillingFee ) );
	}

	public function getIsNoBillingFee() {
		return $this->m_boolIsNoBillingFee;
	}

	public function sqlIsNoBillingFee() {
		return ( true == isset( $this->m_boolIsNoBillingFee ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNoBillingFee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBilledLedgerIds( $arrintBilledLedgerIds ) {
		$this->set( 'm_arrintBilledLedgerIds', CStrings::strToArrIntDef( $arrintBilledLedgerIds, NULL ) );
	}

	public function getBilledLedgerIds() {
		return $this->m_arrintBilledLedgerIds;
	}

	public function sqlBilledLedgerIds() {
		return ( true == isset( $this->m_arrintBilledLedgerIds ) && true == valArr( $this->m_arrintBilledLedgerIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintBilledLedgerIds, NULL ) . '\'' : 'NULL';
	}

	public function setAutoApprovePreBill( $boolAutoApprovePreBill ) {
		$this->set( 'm_boolAutoApprovePreBill', CStrings::strToBool( $boolAutoApprovePreBill ) );
	}

	public function getAutoApprovePreBill() {
		return $this->m_boolAutoApprovePreBill;
	}

	public function sqlAutoApprovePreBill() {
		return ( true == isset( $this->m_boolAutoApprovePreBill ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoApprovePreBill ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setReallocateProratedExpense( $boolReallocateProratedExpense ) {
		$this->set( 'm_boolReallocateProratedExpense', CStrings::strToBool( $boolReallocateProratedExpense ) );
	}

	public function getReallocateProratedExpense() {
		return $this->m_boolReallocateProratedExpense;
	}

	public function sqlReallocateProratedExpense() {
		return ( true == isset( $this->m_boolReallocateProratedExpense ) ) ? '\'' . ( true == ( bool ) $this->m_boolReallocateProratedExpense ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_setting_id, waive_prebill_approval_days, billing_month, billing_instructions, billing_specialist_note, meter_maintenance_notes, is_convergent, dont_approve_convergent_service_period, invoice_is_due_upon_receipt, apply_usage_factor, unit_space_usage_factor, is_subsidy_on_unit, is_manager_approval_mode, is_next_month_post_month, move_out_estimation_months, lease_start_date_as_move_in_date, dont_subsidy_bill, updated_by, updated_on, created_by, created_on, is_show_unit_details, qc_notes, is_no_billing_fee, billed_ledger_ids, auto_approve_pre_bill, reallocate_prorated_expense )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUtilitySettingId() . ', ' .
						$this->sqlWaivePrebillApprovalDays() . ', ' .
						$this->sqlBillingMonth() . ', ' .
						$this->sqlBillingInstructions() . ', ' .
						$this->sqlBillingSpecialistNote() . ', ' .
						$this->sqlMeterMaintenanceNotes() . ', ' .
						$this->sqlIsConvergent() . ', ' .
						$this->sqlDontApproveConvergentServicePeriod() . ', ' .
						$this->sqlInvoiceIsDueUponReceipt() . ', ' .
						$this->sqlApplyUsageFactor() . ', ' .
						$this->sqlUnitSpaceUsageFactor() . ', ' .
						$this->sqlIsSubsidyOnUnit() . ', ' .
						$this->sqlIsManagerApprovalMode() . ', ' .
						$this->sqlIsNextMonthPostMonth() . ', ' .
						$this->sqlMoveOutEstimationMonths() . ', ' .
						$this->sqlLeaseStartDateAsMoveInDate() . ', ' .
						$this->sqlDontSubsidyBill() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsShowUnitDetails() . ', ' .
						$this->sqlQcNotes() . ', ' .
						$this->sqlIsNoBillingFee() . ', ' .
						$this->sqlBilledLedgerIds() . ', ' .
						$this->sqlAutoApprovePreBill() . ', ' .
						$this->sqlReallocateProratedExpense() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilitySettingId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' waive_prebill_approval_days = ' . $this->sqlWaivePrebillApprovalDays(). ',' ; } elseif( true == array_key_exists( 'WaivePrebillApprovalDays', $this->getChangedColumns() ) ) { $strSql .= ' waive_prebill_approval_days = ' . $this->sqlWaivePrebillApprovalDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_month = ' . $this->sqlBillingMonth(). ',' ; } elseif( true == array_key_exists( 'BillingMonth', $this->getChangedColumns() ) ) { $strSql .= ' billing_month = ' . $this->sqlBillingMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_instructions = ' . $this->sqlBillingInstructions(). ',' ; } elseif( true == array_key_exists( 'BillingInstructions', $this->getChangedColumns() ) ) { $strSql .= ' billing_instructions = ' . $this->sqlBillingInstructions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_specialist_note = ' . $this->sqlBillingSpecialistNote(). ',' ; } elseif( true == array_key_exists( 'BillingSpecialistNote', $this->getChangedColumns() ) ) { $strSql .= ' billing_specialist_note = ' . $this->sqlBillingSpecialistNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_maintenance_notes = ' . $this->sqlMeterMaintenanceNotes(). ',' ; } elseif( true == array_key_exists( 'MeterMaintenanceNotes', $this->getChangedColumns() ) ) { $strSql .= ' meter_maintenance_notes = ' . $this->sqlMeterMaintenanceNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_convergent = ' . $this->sqlIsConvergent(). ',' ; } elseif( true == array_key_exists( 'IsConvergent', $this->getChangedColumns() ) ) { $strSql .= ' is_convergent = ' . $this->sqlIsConvergent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_approve_convergent_service_period = ' . $this->sqlDontApproveConvergentServicePeriod(). ',' ; } elseif( true == array_key_exists( 'DontApproveConvergentServicePeriod', $this->getChangedColumns() ) ) { $strSql .= ' dont_approve_convergent_service_period = ' . $this->sqlDontApproveConvergentServicePeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_is_due_upon_receipt = ' . $this->sqlInvoiceIsDueUponReceipt(). ',' ; } elseif( true == array_key_exists( 'InvoiceIsDueUponReceipt', $this->getChangedColumns() ) ) { $strSql .= ' invoice_is_due_upon_receipt = ' . $this->sqlInvoiceIsDueUponReceipt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_usage_factor = ' . $this->sqlApplyUsageFactor(). ',' ; } elseif( true == array_key_exists( 'ApplyUsageFactor', $this->getChangedColumns() ) ) { $strSql .= ' apply_usage_factor = ' . $this->sqlApplyUsageFactor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_usage_factor = ' . $this->sqlUnitSpaceUsageFactor(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceUsageFactor', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_usage_factor = ' . $this->sqlUnitSpaceUsageFactor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_subsidy_on_unit = ' . $this->sqlIsSubsidyOnUnit(). ',' ; } elseif( true == array_key_exists( 'IsSubsidyOnUnit', $this->getChangedColumns() ) ) { $strSql .= ' is_subsidy_on_unit = ' . $this->sqlIsSubsidyOnUnit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manager_approval_mode = ' . $this->sqlIsManagerApprovalMode(). ',' ; } elseif( true == array_key_exists( 'IsManagerApprovalMode', $this->getChangedColumns() ) ) { $strSql .= ' is_manager_approval_mode = ' . $this->sqlIsManagerApprovalMode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_next_month_post_month = ' . $this->sqlIsNextMonthPostMonth(). ',' ; } elseif( true == array_key_exists( 'IsNextMonthPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' is_next_month_post_month = ' . $this->sqlIsNextMonthPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_estimation_months = ' . $this->sqlMoveOutEstimationMonths(). ',' ; } elseif( true == array_key_exists( 'MoveOutEstimationMonths', $this->getChangedColumns() ) ) { $strSql .= ' move_out_estimation_months = ' . $this->sqlMoveOutEstimationMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date_as_move_in_date = ' . $this->sqlLeaseStartDateAsMoveInDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDateAsMoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date_as_move_in_date = ' . $this->sqlLeaseStartDateAsMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_subsidy_bill = ' . $this->sqlDontSubsidyBill(). ',' ; } elseif( true == array_key_exists( 'DontSubsidyBill', $this->getChangedColumns() ) ) { $strSql .= ' dont_subsidy_bill = ' . $this->sqlDontSubsidyBill() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_show_unit_details = ' . $this->sqlIsShowUnitDetails(). ',' ; } elseif( true == array_key_exists( 'IsShowUnitDetails', $this->getChangedColumns() ) ) { $strSql .= ' is_show_unit_details = ' . $this->sqlIsShowUnitDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_notes = ' . $this->sqlQcNotes(). ',' ; } elseif( true == array_key_exists( 'QcNotes', $this->getChangedColumns() ) ) { $strSql .= ' qc_notes = ' . $this->sqlQcNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_no_billing_fee = ' . $this->sqlIsNoBillingFee(). ',' ; } elseif( true == array_key_exists( 'IsNoBillingFee', $this->getChangedColumns() ) ) { $strSql .= ' is_no_billing_fee = ' . $this->sqlIsNoBillingFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billed_ledger_ids = ' . $this->sqlBilledLedgerIds(). ',' ; } elseif( true == array_key_exists( 'BilledLedgerIds', $this->getChangedColumns() ) ) { $strSql .= ' billed_ledger_ids = ' . $this->sqlBilledLedgerIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_approve_pre_bill = ' . $this->sqlAutoApprovePreBill(). ',' ; } elseif( true == array_key_exists( 'AutoApprovePreBill', $this->getChangedColumns() ) ) { $strSql .= ' auto_approve_pre_bill = ' . $this->sqlAutoApprovePreBill() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reallocate_prorated_expense = ' . $this->sqlReallocateProratedExpense(). ',' ; } elseif( true == array_key_exists( 'ReallocateProratedExpense', $this->getChangedColumns() ) ) { $strSql .= ' reallocate_prorated_expense = ' . $this->sqlReallocateProratedExpense() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_utility_setting_id' => $this->getPropertyUtilitySettingId(),
			'waive_prebill_approval_days' => $this->getWaivePrebillApprovalDays(),
			'billing_month' => $this->getBillingMonth(),
			'billing_instructions' => $this->getBillingInstructions(),
			'billing_specialist_note' => $this->getBillingSpecialistNote(),
			'meter_maintenance_notes' => $this->getMeterMaintenanceNotes(),
			'is_convergent' => $this->getIsConvergent(),
			'dont_approve_convergent_service_period' => $this->getDontApproveConvergentServicePeriod(),
			'invoice_is_due_upon_receipt' => $this->getInvoiceIsDueUponReceipt(),
			'apply_usage_factor' => $this->getApplyUsageFactor(),
			'unit_space_usage_factor' => $this->getUnitSpaceUsageFactor(),
			'is_subsidy_on_unit' => $this->getIsSubsidyOnUnit(),
			'is_manager_approval_mode' => $this->getIsManagerApprovalMode(),
			'is_next_month_post_month' => $this->getIsNextMonthPostMonth(),
			'move_out_estimation_months' => $this->getMoveOutEstimationMonths(),
			'lease_start_date_as_move_in_date' => $this->getLeaseStartDateAsMoveInDate(),
			'dont_subsidy_bill' => $this->getDontSubsidyBill(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_show_unit_details' => $this->getIsShowUnitDetails(),
			'qc_notes' => $this->getQcNotes(),
			'is_no_billing_fee' => $this->getIsNoBillingFee(),
			'billed_ledger_ids' => $this->getBilledLedgerIds(),
			'auto_approve_pre_bill' => $this->getAutoApprovePreBill(),
			'reallocate_prorated_expense' => $this->getReallocateProratedExpense()
		);
	}

}
?>