<?php

class CBaseUtilityIntegrationSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_integration_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUtilitySettingId;
	protected $m_intScheduledChargePostDay;
	protected $m_intChargeIntegrationDay;
	protected $m_intRemoveTempTransactionDay;
	protected $m_boolDontPostTempTransactions;
	protected $m_boolIsExportMoveOutTransactions;
	protected $m_boolIsExportBatchTransactions;
	protected $m_boolIsChargeIntegrationNextMonth;
	protected $m_strLastSyncedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUtilityPostDateTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_boolDontPostTempTransactions = false;
		$this->m_boolIsExportMoveOutTransactions = false;
		$this->m_boolIsExportBatchTransactions = false;
		$this->m_boolIsChargeIntegrationNextMonth = false;
		$this->m_intUtilityPostDateTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_utility_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilitySettingId', trim( $arrValues['property_utility_setting_id'] ) ); elseif( isset( $arrValues['property_utility_setting_id'] ) ) $this->setPropertyUtilitySettingId( $arrValues['property_utility_setting_id'] );
		if( isset( $arrValues['scheduled_charge_post_day'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargePostDay', trim( $arrValues['scheduled_charge_post_day'] ) ); elseif( isset( $arrValues['scheduled_charge_post_day'] ) ) $this->setScheduledChargePostDay( $arrValues['scheduled_charge_post_day'] );
		if( isset( $arrValues['charge_integration_day'] ) && $boolDirectSet ) $this->set( 'm_intChargeIntegrationDay', trim( $arrValues['charge_integration_day'] ) ); elseif( isset( $arrValues['charge_integration_day'] ) ) $this->setChargeIntegrationDay( $arrValues['charge_integration_day'] );
		if( isset( $arrValues['remove_temp_transaction_day'] ) && $boolDirectSet ) $this->set( 'm_intRemoveTempTransactionDay', trim( $arrValues['remove_temp_transaction_day'] ) ); elseif( isset( $arrValues['remove_temp_transaction_day'] ) ) $this->setRemoveTempTransactionDay( $arrValues['remove_temp_transaction_day'] );
		if( isset( $arrValues['dont_post_temp_transactions'] ) && $boolDirectSet ) $this->set( 'm_boolDontPostTempTransactions', trim( stripcslashes( $arrValues['dont_post_temp_transactions'] ) ) ); elseif( isset( $arrValues['dont_post_temp_transactions'] ) ) $this->setDontPostTempTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_post_temp_transactions'] ) : $arrValues['dont_post_temp_transactions'] );
		if( isset( $arrValues['is_export_move_out_transactions'] ) && $boolDirectSet ) $this->set( 'm_boolIsExportMoveOutTransactions', trim( stripcslashes( $arrValues['is_export_move_out_transactions'] ) ) ); elseif( isset( $arrValues['is_export_move_out_transactions'] ) ) $this->setIsExportMoveOutTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_export_move_out_transactions'] ) : $arrValues['is_export_move_out_transactions'] );
		if( isset( $arrValues['is_export_batch_transactions'] ) && $boolDirectSet ) $this->set( 'm_boolIsExportBatchTransactions', trim( stripcslashes( $arrValues['is_export_batch_transactions'] ) ) ); elseif( isset( $arrValues['is_export_batch_transactions'] ) ) $this->setIsExportBatchTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_export_batch_transactions'] ) : $arrValues['is_export_batch_transactions'] );
		if( isset( $arrValues['is_charge_integration_next_month'] ) && $boolDirectSet ) $this->set( 'm_boolIsChargeIntegrationNextMonth', trim( stripcslashes( $arrValues['is_charge_integration_next_month'] ) ) ); elseif( isset( $arrValues['is_charge_integration_next_month'] ) ) $this->setIsChargeIntegrationNextMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_charge_integration_next_month'] ) : $arrValues['is_charge_integration_next_month'] );
		if( isset( $arrValues['last_synced_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSyncedOn', trim( $arrValues['last_synced_on'] ) ); elseif( isset( $arrValues['last_synced_on'] ) ) $this->setLastSyncedOn( $arrValues['last_synced_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['utility_post_date_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityPostDateTypeId', trim( $arrValues['utility_post_date_type_id'] ) ); elseif( isset( $arrValues['utility_post_date_type_id'] ) ) $this->setUtilityPostDateTypeId( $arrValues['utility_post_date_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUtilitySettingId( $intPropertyUtilitySettingId ) {
		$this->set( 'm_intPropertyUtilitySettingId', CStrings::strToIntDef( $intPropertyUtilitySettingId, NULL, false ) );
	}

	public function getPropertyUtilitySettingId() {
		return $this->m_intPropertyUtilitySettingId;
	}

	public function sqlPropertyUtilitySettingId() {
		return ( true == isset( $this->m_intPropertyUtilitySettingId ) ) ? ( string ) $this->m_intPropertyUtilitySettingId : 'NULL';
	}

	public function setScheduledChargePostDay( $intScheduledChargePostDay ) {
		$this->set( 'm_intScheduledChargePostDay', CStrings::strToIntDef( $intScheduledChargePostDay, NULL, false ) );
	}

	public function getScheduledChargePostDay() {
		return $this->m_intScheduledChargePostDay;
	}

	public function sqlScheduledChargePostDay() {
		return ( true == isset( $this->m_intScheduledChargePostDay ) ) ? ( string ) $this->m_intScheduledChargePostDay : 'NULL';
	}

	public function setChargeIntegrationDay( $intChargeIntegrationDay ) {
		$this->set( 'm_intChargeIntegrationDay', CStrings::strToIntDef( $intChargeIntegrationDay, NULL, false ) );
	}

	public function getChargeIntegrationDay() {
		return $this->m_intChargeIntegrationDay;
	}

	public function sqlChargeIntegrationDay() {
		return ( true == isset( $this->m_intChargeIntegrationDay ) ) ? ( string ) $this->m_intChargeIntegrationDay : 'NULL';
	}

	public function setRemoveTempTransactionDay( $intRemoveTempTransactionDay ) {
		$this->set( 'm_intRemoveTempTransactionDay', CStrings::strToIntDef( $intRemoveTempTransactionDay, NULL, false ) );
	}

	public function getRemoveTempTransactionDay() {
		return $this->m_intRemoveTempTransactionDay;
	}

	public function sqlRemoveTempTransactionDay() {
		return ( true == isset( $this->m_intRemoveTempTransactionDay ) ) ? ( string ) $this->m_intRemoveTempTransactionDay : 'NULL';
	}

	public function setDontPostTempTransactions( $boolDontPostTempTransactions ) {
		$this->set( 'm_boolDontPostTempTransactions', CStrings::strToBool( $boolDontPostTempTransactions ) );
	}

	public function getDontPostTempTransactions() {
		return $this->m_boolDontPostTempTransactions;
	}

	public function sqlDontPostTempTransactions() {
		return ( true == isset( $this->m_boolDontPostTempTransactions ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontPostTempTransactions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsExportMoveOutTransactions( $boolIsExportMoveOutTransactions ) {
		$this->set( 'm_boolIsExportMoveOutTransactions', CStrings::strToBool( $boolIsExportMoveOutTransactions ) );
	}

	public function getIsExportMoveOutTransactions() {
		return $this->m_boolIsExportMoveOutTransactions;
	}

	public function sqlIsExportMoveOutTransactions() {
		return ( true == isset( $this->m_boolIsExportMoveOutTransactions ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsExportMoveOutTransactions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsExportBatchTransactions( $boolIsExportBatchTransactions ) {
		$this->set( 'm_boolIsExportBatchTransactions', CStrings::strToBool( $boolIsExportBatchTransactions ) );
	}

	public function getIsExportBatchTransactions() {
		return $this->m_boolIsExportBatchTransactions;
	}

	public function sqlIsExportBatchTransactions() {
		return ( true == isset( $this->m_boolIsExportBatchTransactions ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsExportBatchTransactions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsChargeIntegrationNextMonth( $boolIsChargeIntegrationNextMonth ) {
		$this->set( 'm_boolIsChargeIntegrationNextMonth', CStrings::strToBool( $boolIsChargeIntegrationNextMonth ) );
	}

	public function getIsChargeIntegrationNextMonth() {
		return $this->m_boolIsChargeIntegrationNextMonth;
	}

	public function sqlIsChargeIntegrationNextMonth() {
		return ( true == isset( $this->m_boolIsChargeIntegrationNextMonth ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsChargeIntegrationNextMonth ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLastSyncedOn( $strLastSyncedOn ) {
		$this->set( 'm_strLastSyncedOn', CStrings::strTrimDef( $strLastSyncedOn, -1, NULL, true ) );
	}

	public function getLastSyncedOn() {
		return $this->m_strLastSyncedOn;
	}

	public function sqlLastSyncedOn() {
		return ( true == isset( $this->m_strLastSyncedOn ) ) ? '\'' . $this->m_strLastSyncedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUtilityPostDateTypeId( $intUtilityPostDateTypeId ) {
		$this->set( 'm_intUtilityPostDateTypeId', CStrings::strToIntDef( $intUtilityPostDateTypeId, NULL, false ) );
	}

	public function getUtilityPostDateTypeId() {
		return $this->m_intUtilityPostDateTypeId;
	}

	public function sqlUtilityPostDateTypeId() {
		return ( true == isset( $this->m_intUtilityPostDateTypeId ) ) ? ( string ) $this->m_intUtilityPostDateTypeId : '1';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_setting_id, scheduled_charge_post_day, charge_integration_day, remove_temp_transaction_day, dont_post_temp_transactions, is_export_move_out_transactions, is_export_batch_transactions, is_charge_integration_next_month, last_synced_on, updated_by, updated_on, created_by, created_on, utility_post_date_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUtilitySettingId() . ', ' .
						$this->sqlScheduledChargePostDay() . ', ' .
						$this->sqlChargeIntegrationDay() . ', ' .
						$this->sqlRemoveTempTransactionDay() . ', ' .
						$this->sqlDontPostTempTransactions() . ', ' .
						$this->sqlIsExportMoveOutTransactions() . ', ' .
						$this->sqlIsExportBatchTransactions() . ', ' .
						$this->sqlIsChargeIntegrationNextMonth() . ', ' .
						$this->sqlLastSyncedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUtilityPostDateTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilitySettingId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_post_day = ' . $this->sqlScheduledChargePostDay(). ',' ; } elseif( true == array_key_exists( 'ScheduledChargePostDay', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_post_day = ' . $this->sqlScheduledChargePostDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_integration_day = ' . $this->sqlChargeIntegrationDay(). ',' ; } elseif( true == array_key_exists( 'ChargeIntegrationDay', $this->getChangedColumns() ) ) { $strSql .= ' charge_integration_day = ' . $this->sqlChargeIntegrationDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remove_temp_transaction_day = ' . $this->sqlRemoveTempTransactionDay(). ',' ; } elseif( true == array_key_exists( 'RemoveTempTransactionDay', $this->getChangedColumns() ) ) { $strSql .= ' remove_temp_transaction_day = ' . $this->sqlRemoveTempTransactionDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_post_temp_transactions = ' . $this->sqlDontPostTempTransactions(). ',' ; } elseif( true == array_key_exists( 'DontPostTempTransactions', $this->getChangedColumns() ) ) { $strSql .= ' dont_post_temp_transactions = ' . $this->sqlDontPostTempTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_export_move_out_transactions = ' . $this->sqlIsExportMoveOutTransactions(). ',' ; } elseif( true == array_key_exists( 'IsExportMoveOutTransactions', $this->getChangedColumns() ) ) { $strSql .= ' is_export_move_out_transactions = ' . $this->sqlIsExportMoveOutTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_export_batch_transactions = ' . $this->sqlIsExportBatchTransactions(). ',' ; } elseif( true == array_key_exists( 'IsExportBatchTransactions', $this->getChangedColumns() ) ) { $strSql .= ' is_export_batch_transactions = ' . $this->sqlIsExportBatchTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_charge_integration_next_month = ' . $this->sqlIsChargeIntegrationNextMonth(). ',' ; } elseif( true == array_key_exists( 'IsChargeIntegrationNextMonth', $this->getChangedColumns() ) ) { $strSql .= ' is_charge_integration_next_month = ' . $this->sqlIsChargeIntegrationNextMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_synced_on = ' . $this->sqlLastSyncedOn(). ',' ; } elseif( true == array_key_exists( 'LastSyncedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_synced_on = ' . $this->sqlLastSyncedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_post_date_type_id = ' . $this->sqlUtilityPostDateTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityPostDateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_post_date_type_id = ' . $this->sqlUtilityPostDateTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_utility_setting_id' => $this->getPropertyUtilitySettingId(),
			'scheduled_charge_post_day' => $this->getScheduledChargePostDay(),
			'charge_integration_day' => $this->getChargeIntegrationDay(),
			'remove_temp_transaction_day' => $this->getRemoveTempTransactionDay(),
			'dont_post_temp_transactions' => $this->getDontPostTempTransactions(),
			'is_export_move_out_transactions' => $this->getIsExportMoveOutTransactions(),
			'is_export_batch_transactions' => $this->getIsExportBatchTransactions(),
			'is_charge_integration_next_month' => $this->getIsChargeIntegrationNextMonth(),
			'last_synced_on' => $this->getLastSyncedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'utility_post_date_type_id' => $this->getUtilityPostDateTypeId()
		);
	}

}
?>