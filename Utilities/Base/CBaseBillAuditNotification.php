<?php

class CBaseBillAuditNotification extends CEosSingularBase {

	const TABLE_NAME = 'public.bill_audit_notifications';

	protected $m_intId;
	protected $m_intBillAuditId;
	protected $m_intCompanyUserId;
	protected $m_strEmailAddress;
	protected $m_strSubject;
	protected $m_strContent;
	protected $m_strEmailSentOn;
	protected $m_intEmailSentBy;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['bill_audit_id'] ) && $boolDirectSet ) $this->set( 'm_intBillAuditId', trim( $arrValues['bill_audit_id'] ) ); elseif( isset( $arrValues['bill_audit_id'] ) ) $this->setBillAuditId( $arrValues['bill_audit_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( stripcslashes( $arrValues['subject'] ) ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subject'] ) : $arrValues['subject'] );
		if( isset( $arrValues['content'] ) && $boolDirectSet ) $this->set( 'm_strContent', trim( stripcslashes( $arrValues['content'] ) ) ); elseif( isset( $arrValues['content'] ) ) $this->setContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['content'] ) : $arrValues['content'] );
		if( isset( $arrValues['email_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strEmailSentOn', trim( $arrValues['email_sent_on'] ) ); elseif( isset( $arrValues['email_sent_on'] ) ) $this->setEmailSentOn( $arrValues['email_sent_on'] );
		if( isset( $arrValues['email_sent_by'] ) && $boolDirectSet ) $this->set( 'm_intEmailSentBy', trim( $arrValues['email_sent_by'] ) ); elseif( isset( $arrValues['email_sent_by'] ) ) $this->setEmailSentBy( $arrValues['email_sent_by'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setBillAuditId( $intBillAuditId ) {
		$this->set( 'm_intBillAuditId', CStrings::strToIntDef( $intBillAuditId, NULL, false ) );
	}

	public function getBillAuditId() {
		return $this->m_intBillAuditId;
	}

	public function sqlBillAuditId() {
		return ( true == isset( $this->m_intBillAuditId ) ) ? ( string ) $this->m_intBillAuditId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 250, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, 250, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? '\'' . addslashes( $this->m_strSubject ) . '\'' : 'NULL';
	}

	public function setContent( $strContent ) {
		$this->set( 'm_strContent', CStrings::strTrimDef( $strContent, -1, NULL, true ) );
	}

	public function getContent() {
		return $this->m_strContent;
	}

	public function sqlContent() {
		return ( true == isset( $this->m_strContent ) ) ? '\'' . addslashes( $this->m_strContent ) . '\'' : 'NULL';
	}

	public function setEmailSentOn( $strEmailSentOn ) {
		$this->set( 'm_strEmailSentOn', CStrings::strTrimDef( $strEmailSentOn, -1, NULL, true ) );
	}

	public function getEmailSentOn() {
		return $this->m_strEmailSentOn;
	}

	public function sqlEmailSentOn() {
		return ( true == isset( $this->m_strEmailSentOn ) ) ? '\'' . $this->m_strEmailSentOn . '\'' : 'NULL';
	}

	public function setEmailSentBy( $intEmailSentBy ) {
		$this->set( 'm_intEmailSentBy', CStrings::strToIntDef( $intEmailSentBy, NULL, false ) );
	}

	public function getEmailSentBy() {
		return $this->m_intEmailSentBy;
	}

	public function sqlEmailSentBy() {
		return ( true == isset( $this->m_intEmailSentBy ) ) ? ( string ) $this->m_intEmailSentBy : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, bill_audit_id, company_user_id, email_address, subject, content, email_sent_on, email_sent_by, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlBillAuditId() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlSubject() . ', ' .
 						$this->sqlContent() . ', ' .
 						$this->sqlEmailSentOn() . ', ' .
 						$this->sqlEmailSentBy() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_audit_id = ' . $this->sqlBillAuditId() . ','; } elseif( true == array_key_exists( 'BillAuditId', $this->getChangedColumns() ) ) { $strSql .= ' bill_audit_id = ' . $this->sqlBillAuditId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content = ' . $this->sqlContent() . ','; } elseif( true == array_key_exists( 'Content', $this->getChangedColumns() ) ) { $strSql .= ' content = ' . $this->sqlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_sent_on = ' . $this->sqlEmailSentOn() . ','; } elseif( true == array_key_exists( 'EmailSentOn', $this->getChangedColumns() ) ) { $strSql .= ' email_sent_on = ' . $this->sqlEmailSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_sent_by = ' . $this->sqlEmailSentBy() . ','; } elseif( true == array_key_exists( 'EmailSentBy', $this->getChangedColumns() ) ) { $strSql .= ' email_sent_by = ' . $this->sqlEmailSentBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'bill_audit_id' => $this->getBillAuditId(),
			'company_user_id' => $this->getCompanyUserId(),
			'email_address' => $this->getEmailAddress(),
			'subject' => $this->getSubject(),
			'content' => $this->getContent(),
			'email_sent_on' => $this->getEmailSentOn(),
			'email_sent_by' => $this->getEmailSentBy(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>