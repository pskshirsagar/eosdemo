<?php

class CBaseUtilityBillsAwsDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bills_aws_details';

	protected $m_intId;
	protected $m_intUtilityBillId;
	protected $m_intImplementationUtilityBillId;
	protected $m_strAwsJobNumber;
	protected $m_strFileName;
	protected $m_boolIsJobCompleted;
	protected $m_strAwsTextractResponse;
	protected $m_jsonAwsTextractResponse;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsJobCompleted = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['implementation_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationUtilityBillId', trim( $arrValues['implementation_utility_bill_id'] ) ); elseif( isset( $arrValues['implementation_utility_bill_id'] ) ) $this->setImplementationUtilityBillId( $arrValues['implementation_utility_bill_id'] );
		if( isset( $arrValues['aws_job_number'] ) && $boolDirectSet ) $this->set( 'm_strAwsJobNumber', trim( stripcslashes( $arrValues['aws_job_number'] ) ) ); elseif( isset( $arrValues['aws_job_number'] ) ) $this->setAwsJobNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['aws_job_number'] ) : $arrValues['aws_job_number'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['is_job_completed'] ) && $boolDirectSet ) $this->set( 'm_boolIsJobCompleted', trim( stripcslashes( $arrValues['is_job_completed'] ) ) ); elseif( isset( $arrValues['is_job_completed'] ) ) $this->setIsJobCompleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_job_completed'] ) : $arrValues['is_job_completed'] );
		if( isset( $arrValues['aws_textract_response'] ) ) $this->set( 'm_strAwsTextractResponse', trim( $arrValues['aws_textract_response'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setImplementationUtilityBillId( $intImplementationUtilityBillId ) {
		$this->set( 'm_intImplementationUtilityBillId', CStrings::strToIntDef( $intImplementationUtilityBillId, NULL, false ) );
	}

	public function getImplementationUtilityBillId() {
		return $this->m_intImplementationUtilityBillId;
	}

	public function sqlImplementationUtilityBillId() {
		return ( true == isset( $this->m_intImplementationUtilityBillId ) ) ? ( string ) $this->m_intImplementationUtilityBillId : 'NULL';
	}

	public function setAwsJobNumber( $strAwsJobNumber ) {
		$this->set( 'm_strAwsJobNumber', CStrings::strTrimDef( $strAwsJobNumber, 300, NULL, true ) );
	}

	public function getAwsJobNumber() {
		return $this->m_strAwsJobNumber;
	}

	public function sqlAwsJobNumber() {
		return ( true == isset( $this->m_strAwsJobNumber ) ) ? '\'' . addslashes( $this->m_strAwsJobNumber ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 300, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setIsJobCompleted( $boolIsJobCompleted ) {
		$this->set( 'm_boolIsJobCompleted', CStrings::strToBool( $boolIsJobCompleted ) );
	}

	public function getIsJobCompleted() {
		return $this->m_boolIsJobCompleted;
	}

	public function sqlIsJobCompleted() {
		return ( true == isset( $this->m_boolIsJobCompleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsJobCompleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAwsTextractResponse( $jsonAwsTextractResponse ) {
		if( true == valObj( $jsonAwsTextractResponse, 'stdClass' ) ) {
			$this->set( 'm_jsonAwsTextractResponse', $jsonAwsTextractResponse );
		} elseif( true == valJsonString( $jsonAwsTextractResponse ) ) {
			$this->set( 'm_jsonAwsTextractResponse', CStrings::strToJson( $jsonAwsTextractResponse ) );
		} else {
			$this->set( 'm_jsonAwsTextractResponse', NULL ); 
		}
		unset( $this->m_strAwsTextractResponse );
	}

	public function getAwsTextractResponse() {
		if( true == isset( $this->m_strAwsTextractResponse ) ) {
			$this->m_jsonAwsTextractResponse = CStrings::strToJson( $this->m_strAwsTextractResponse );
			unset( $this->m_strAwsTextractResponse );
		}
		return $this->m_jsonAwsTextractResponse;
	}

	public function sqlAwsTextractResponse() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAwsTextractResponse() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getAwsTextractResponse() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_bill_id, implementation_utility_bill_id, aws_job_number, file_name, is_job_completed, aws_textract_response, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlImplementationUtilityBillId() . ', ' .
						$this->sqlAwsJobNumber() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlIsJobCompleted() . ', ' .
						$this->sqlAwsTextractResponse() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_utility_bill_id = ' . $this->sqlImplementationUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'ImplementationUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_utility_bill_id = ' . $this->sqlImplementationUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aws_job_number = ' . $this->sqlAwsJobNumber(). ',' ; } elseif( true == array_key_exists( 'AwsJobNumber', $this->getChangedColumns() ) ) { $strSql .= ' aws_job_number = ' . $this->sqlAwsJobNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_job_completed = ' . $this->sqlIsJobCompleted(). ',' ; } elseif( true == array_key_exists( 'IsJobCompleted', $this->getChangedColumns() ) ) { $strSql .= ' is_job_completed = ' . $this->sqlIsJobCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aws_textract_response = ' . $this->sqlAwsTextractResponse(). ',' ; } elseif( true == array_key_exists( 'AwsTextractResponse', $this->getChangedColumns() ) ) { $strSql .= ' aws_textract_response = ' . $this->sqlAwsTextractResponse() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'implementation_utility_bill_id' => $this->getImplementationUtilityBillId(),
			'aws_job_number' => $this->getAwsJobNumber(),
			'file_name' => $this->getFileName(),
			'is_job_completed' => $this->getIsJobCompleted(),
			'aws_textract_response' => $this->getAwsTextractResponse(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>