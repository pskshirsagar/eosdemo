<?php

class CBaseMissingBillEscalation extends CEosSingularBase {

	const TABLE_NAME = 'public.missing_bill_escalations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intCompanyUserId;
	protected $m_strEscalationDatetime;
	protected $m_intDaysLateTrigger;
	protected $m_boolIsGeneric;
	protected $m_boolIsGenericProperty;
	protected $m_boolIsGenericRegion;
	protected $m_boolIsGenericCompany;
	protected $m_boolIncludeVcrBills;
	protected $m_boolIncludeUtilityBills;
	protected $m_boolIncludeBillPayBills;
	protected $m_boolSendEmail;
	protected $m_boolSendSms;
	protected $m_boolSendCall;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsGeneric = false;
		$this->m_boolIsGenericProperty = false;
		$this->m_boolIsGenericRegion = false;
		$this->m_boolIsGenericCompany = false;
		$this->m_boolIncludeVcrBills = false;
		$this->m_boolIncludeUtilityBills = false;
		$this->m_boolIncludeBillPayBills = false;
		$this->m_boolSendEmail = false;
		$this->m_boolSendSms = false;
		$this->m_boolSendCall = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['escalation_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEscalationDatetime', trim( $arrValues['escalation_datetime'] ) ); elseif( isset( $arrValues['escalation_datetime'] ) ) $this->setEscalationDatetime( $arrValues['escalation_datetime'] );
		if( isset( $arrValues['days_late_trigger'] ) && $boolDirectSet ) $this->set( 'm_intDaysLateTrigger', trim( $arrValues['days_late_trigger'] ) ); elseif( isset( $arrValues['days_late_trigger'] ) ) $this->setDaysLateTrigger( $arrValues['days_late_trigger'] );
		if( isset( $arrValues['is_generic'] ) && $boolDirectSet ) $this->set( 'm_boolIsGeneric', trim( stripcslashes( $arrValues['is_generic'] ) ) ); elseif( isset( $arrValues['is_generic'] ) ) $this->setIsGeneric( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_generic'] ) : $arrValues['is_generic'] );
		if( isset( $arrValues['is_generic_property'] ) && $boolDirectSet ) $this->set( 'm_boolIsGenericProperty', trim( stripcslashes( $arrValues['is_generic_property'] ) ) ); elseif( isset( $arrValues['is_generic_property'] ) ) $this->setIsGenericProperty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_generic_property'] ) : $arrValues['is_generic_property'] );
		if( isset( $arrValues['is_generic_region'] ) && $boolDirectSet ) $this->set( 'm_boolIsGenericRegion', trim( stripcslashes( $arrValues['is_generic_region'] ) ) ); elseif( isset( $arrValues['is_generic_region'] ) ) $this->setIsGenericRegion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_generic_region'] ) : $arrValues['is_generic_region'] );
		if( isset( $arrValues['is_generic_company'] ) && $boolDirectSet ) $this->set( 'm_boolIsGenericCompany', trim( stripcslashes( $arrValues['is_generic_company'] ) ) ); elseif( isset( $arrValues['is_generic_company'] ) ) $this->setIsGenericCompany( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_generic_company'] ) : $arrValues['is_generic_company'] );
		if( isset( $arrValues['include_vcr_bills'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeVcrBills', trim( stripcslashes( $arrValues['include_vcr_bills'] ) ) ); elseif( isset( $arrValues['include_vcr_bills'] ) ) $this->setIncludeVcrBills( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_vcr_bills'] ) : $arrValues['include_vcr_bills'] );
		if( isset( $arrValues['include_utility_bills'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeUtilityBills', trim( stripcslashes( $arrValues['include_utility_bills'] ) ) ); elseif( isset( $arrValues['include_utility_bills'] ) ) $this->setIncludeUtilityBills( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_utility_bills'] ) : $arrValues['include_utility_bills'] );
		if( isset( $arrValues['include_bill_pay_bills'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeBillPayBills', trim( stripcslashes( $arrValues['include_bill_pay_bills'] ) ) ); elseif( isset( $arrValues['include_bill_pay_bills'] ) ) $this->setIncludeBillPayBills( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_bill_pay_bills'] ) : $arrValues['include_bill_pay_bills'] );
		if( isset( $arrValues['send_email'] ) && $boolDirectSet ) $this->set( 'm_boolSendEmail', trim( stripcslashes( $arrValues['send_email'] ) ) ); elseif( isset( $arrValues['send_email'] ) ) $this->setSendEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['send_email'] ) : $arrValues['send_email'] );
		if( isset( $arrValues['send_sms'] ) && $boolDirectSet ) $this->set( 'm_boolSendSms', trim( stripcslashes( $arrValues['send_sms'] ) ) ); elseif( isset( $arrValues['send_sms'] ) ) $this->setSendSms( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['send_sms'] ) : $arrValues['send_sms'] );
		if( isset( $arrValues['send_call'] ) && $boolDirectSet ) $this->set( 'm_boolSendCall', trim( stripcslashes( $arrValues['send_call'] ) ) ); elseif( isset( $arrValues['send_call'] ) ) $this->setSendCall( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['send_call'] ) : $arrValues['send_call'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setEscalationDatetime( $strEscalationDatetime ) {
		$this->set( 'm_strEscalationDatetime', CStrings::strTrimDef( $strEscalationDatetime, -1, NULL, true ) );
	}

	public function getEscalationDatetime() {
		return $this->m_strEscalationDatetime;
	}

	public function sqlEscalationDatetime() {
		return ( true == isset( $this->m_strEscalationDatetime ) ) ? '\'' . $this->m_strEscalationDatetime . '\'' : 'NOW()';
	}

	public function setDaysLateTrigger( $intDaysLateTrigger ) {
		$this->set( 'm_intDaysLateTrigger', CStrings::strToIntDef( $intDaysLateTrigger, NULL, false ) );
	}

	public function getDaysLateTrigger() {
		return $this->m_intDaysLateTrigger;
	}

	public function sqlDaysLateTrigger() {
		return ( true == isset( $this->m_intDaysLateTrigger ) ) ? ( string ) $this->m_intDaysLateTrigger : 'NULL';
	}

	public function setIsGeneric( $boolIsGeneric ) {
		$this->set( 'm_boolIsGeneric', CStrings::strToBool( $boolIsGeneric ) );
	}

	public function getIsGeneric() {
		return $this->m_boolIsGeneric;
	}

	public function sqlIsGeneric() {
		return ( true == isset( $this->m_boolIsGeneric ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGeneric ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsGenericProperty( $boolIsGenericProperty ) {
		$this->set( 'm_boolIsGenericProperty', CStrings::strToBool( $boolIsGenericProperty ) );
	}

	public function getIsGenericProperty() {
		return $this->m_boolIsGenericProperty;
	}

	public function sqlIsGenericProperty() {
		return ( true == isset( $this->m_boolIsGenericProperty ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGenericProperty ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsGenericRegion( $boolIsGenericRegion ) {
		$this->set( 'm_boolIsGenericRegion', CStrings::strToBool( $boolIsGenericRegion ) );
	}

	public function getIsGenericRegion() {
		return $this->m_boolIsGenericRegion;
	}

	public function sqlIsGenericRegion() {
		return ( true == isset( $this->m_boolIsGenericRegion ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGenericRegion ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsGenericCompany( $boolIsGenericCompany ) {
		$this->set( 'm_boolIsGenericCompany', CStrings::strToBool( $boolIsGenericCompany ) );
	}

	public function getIsGenericCompany() {
		return $this->m_boolIsGenericCompany;
	}

	public function sqlIsGenericCompany() {
		return ( true == isset( $this->m_boolIsGenericCompany ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGenericCompany ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeVcrBills( $boolIncludeVcrBills ) {
		$this->set( 'm_boolIncludeVcrBills', CStrings::strToBool( $boolIncludeVcrBills ) );
	}

	public function getIncludeVcrBills() {
		return $this->m_boolIncludeVcrBills;
	}

	public function sqlIncludeVcrBills() {
		return ( true == isset( $this->m_boolIncludeVcrBills ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeVcrBills ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeUtilityBills( $boolIncludeUtilityBills ) {
		$this->set( 'm_boolIncludeUtilityBills', CStrings::strToBool( $boolIncludeUtilityBills ) );
	}

	public function getIncludeUtilityBills() {
		return $this->m_boolIncludeUtilityBills;
	}

	public function sqlIncludeUtilityBills() {
		return ( true == isset( $this->m_boolIncludeUtilityBills ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeUtilityBills ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeBillPayBills( $boolIncludeBillPayBills ) {
		$this->set( 'm_boolIncludeBillPayBills', CStrings::strToBool( $boolIncludeBillPayBills ) );
	}

	public function getIncludeBillPayBills() {
		return $this->m_boolIncludeBillPayBills;
	}

	public function sqlIncludeBillPayBills() {
		return ( true == isset( $this->m_boolIncludeBillPayBills ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeBillPayBills ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSendEmail( $boolSendEmail ) {
		$this->set( 'm_boolSendEmail', CStrings::strToBool( $boolSendEmail ) );
	}

	public function getSendEmail() {
		return $this->m_boolSendEmail;
	}

	public function sqlSendEmail() {
		return ( true == isset( $this->m_boolSendEmail ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendEmail ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSendSms( $boolSendSms ) {
		$this->set( 'm_boolSendSms', CStrings::strToBool( $boolSendSms ) );
	}

	public function getSendSms() {
		return $this->m_boolSendSms;
	}

	public function sqlSendSms() {
		return ( true == isset( $this->m_boolSendSms ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendSms ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSendCall( $boolSendCall ) {
		$this->set( 'm_boolSendCall', CStrings::strToBool( $boolSendCall ) );
	}

	public function getSendCall() {
		return $this->m_boolSendCall;
	}

	public function sqlSendCall() {
		return ( true == isset( $this->m_boolSendCall ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendCall ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_type_id, company_user_id, escalation_datetime, days_late_trigger, is_generic, is_generic_property, is_generic_region, is_generic_company, include_vcr_bills, include_utility_bills, include_bill_pay_bills, send_email, send_sms, send_call, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyUtilityTypeId() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlEscalationDatetime() . ', ' .
 						$this->sqlDaysLateTrigger() . ', ' .
 						$this->sqlIsGeneric() . ', ' .
 						$this->sqlIsGenericProperty() . ', ' .
 						$this->sqlIsGenericRegion() . ', ' .
 						$this->sqlIsGenericCompany() . ', ' .
 						$this->sqlIncludeVcrBills() . ', ' .
 						$this->sqlIncludeUtilityBills() . ', ' .
 						$this->sqlIncludeBillPayBills() . ', ' .
 						$this->sqlSendEmail() . ', ' .
 						$this->sqlSendSms() . ', ' .
 						$this->sqlSendCall() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalation_datetime = ' . $this->sqlEscalationDatetime() . ','; } elseif( true == array_key_exists( 'EscalationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' escalation_datetime = ' . $this->sqlEscalationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_late_trigger = ' . $this->sqlDaysLateTrigger() . ','; } elseif( true == array_key_exists( 'DaysLateTrigger', $this->getChangedColumns() ) ) { $strSql .= ' days_late_trigger = ' . $this->sqlDaysLateTrigger() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_generic = ' . $this->sqlIsGeneric() . ','; } elseif( true == array_key_exists( 'IsGeneric', $this->getChangedColumns() ) ) { $strSql .= ' is_generic = ' . $this->sqlIsGeneric() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_generic_property = ' . $this->sqlIsGenericProperty() . ','; } elseif( true == array_key_exists( 'IsGenericProperty', $this->getChangedColumns() ) ) { $strSql .= ' is_generic_property = ' . $this->sqlIsGenericProperty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_generic_region = ' . $this->sqlIsGenericRegion() . ','; } elseif( true == array_key_exists( 'IsGenericRegion', $this->getChangedColumns() ) ) { $strSql .= ' is_generic_region = ' . $this->sqlIsGenericRegion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_generic_company = ' . $this->sqlIsGenericCompany() . ','; } elseif( true == array_key_exists( 'IsGenericCompany', $this->getChangedColumns() ) ) { $strSql .= ' is_generic_company = ' . $this->sqlIsGenericCompany() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_vcr_bills = ' . $this->sqlIncludeVcrBills() . ','; } elseif( true == array_key_exists( 'IncludeVcrBills', $this->getChangedColumns() ) ) { $strSql .= ' include_vcr_bills = ' . $this->sqlIncludeVcrBills() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_utility_bills = ' . $this->sqlIncludeUtilityBills() . ','; } elseif( true == array_key_exists( 'IncludeUtilityBills', $this->getChangedColumns() ) ) { $strSql .= ' include_utility_bills = ' . $this->sqlIncludeUtilityBills() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_bill_pay_bills = ' . $this->sqlIncludeBillPayBills() . ','; } elseif( true == array_key_exists( 'IncludeBillPayBills', $this->getChangedColumns() ) ) { $strSql .= ' include_bill_pay_bills = ' . $this->sqlIncludeBillPayBills() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_email = ' . $this->sqlSendEmail() . ','; } elseif( true == array_key_exists( 'SendEmail', $this->getChangedColumns() ) ) { $strSql .= ' send_email = ' . $this->sqlSendEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_sms = ' . $this->sqlSendSms() . ','; } elseif( true == array_key_exists( 'SendSms', $this->getChangedColumns() ) ) { $strSql .= ' send_sms = ' . $this->sqlSendSms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_call = ' . $this->sqlSendCall() . ','; } elseif( true == array_key_exists( 'SendCall', $this->getChangedColumns() ) ) { $strSql .= ' send_call = ' . $this->sqlSendCall() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'company_user_id' => $this->getCompanyUserId(),
			'escalation_datetime' => $this->getEscalationDatetime(),
			'days_late_trigger' => $this->getDaysLateTrigger(),
			'is_generic' => $this->getIsGeneric(),
			'is_generic_property' => $this->getIsGenericProperty(),
			'is_generic_region' => $this->getIsGenericRegion(),
			'is_generic_company' => $this->getIsGenericCompany(),
			'include_vcr_bills' => $this->getIncludeVcrBills(),
			'include_utility_bills' => $this->getIncludeUtilityBills(),
			'include_bill_pay_bills' => $this->getIncludeBillPayBills(),
			'send_email' => $this->getSendEmail(),
			'send_sms' => $this->getSendSms(),
			'send_call' => $this->getSendCall(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>