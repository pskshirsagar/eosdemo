<?php

class CBaseOcrFieldAnchorCriteria extends CEosSingularBase {

	const TABLE_NAME = 'public.ocr_field_anchor_criterias';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityProviderId;
	protected $m_intOcrFieldId;
	protected $m_intAnchorTypeId;
	protected $m_strLabels;
	protected $m_strLabelVertices;
	protected $m_jsonLabelVertices;
	protected $m_strAnchors;
	protected $m_jsonAnchors;
	protected $m_strAnchorVertices;
	protected $m_jsonAnchorVertices;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityProviderId', trim( $arrValues['utility_provider_id'] ) ); elseif( isset( $arrValues['utility_provider_id'] ) ) $this->setUtilityProviderId( $arrValues['utility_provider_id'] );
		if( isset( $arrValues['ocr_field_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrFieldId', trim( $arrValues['ocr_field_id'] ) ); elseif( isset( $arrValues['ocr_field_id'] ) ) $this->setOcrFieldId( $arrValues['ocr_field_id'] );
		if( isset( $arrValues['anchor_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAnchorTypeId', trim( $arrValues['anchor_type_id'] ) ); elseif( isset( $arrValues['anchor_type_id'] ) ) $this->setAnchorTypeId( $arrValues['anchor_type_id'] );
		if( isset( $arrValues['labels'] ) && $boolDirectSet ) $this->set( 'm_strLabels', trim( $arrValues['labels'] ) ); elseif( isset( $arrValues['labels'] ) ) $this->setLabels( $arrValues['labels'] );
		if( isset( $arrValues['label_vertices'] ) ) $this->set( 'm_strLabelVertices', trim( $arrValues['label_vertices'] ) );
		if( isset( $arrValues['anchors'] ) ) $this->set( 'm_strAnchors', trim( $arrValues['anchors'] ) );
		if( isset( $arrValues['anchor_vertices'] ) ) $this->set( 'm_strAnchorVertices', trim( $arrValues['anchor_vertices'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityProviderId( $intUtilityProviderId ) {
		$this->set( 'm_intUtilityProviderId', CStrings::strToIntDef( $intUtilityProviderId, NULL, false ) );
	}

	public function getUtilityProviderId() {
		return $this->m_intUtilityProviderId;
	}

	public function sqlUtilityProviderId() {
		return ( true == isset( $this->m_intUtilityProviderId ) ) ? ( string ) $this->m_intUtilityProviderId : 'NULL';
	}

	public function setOcrFieldId( $intOcrFieldId ) {
		$this->set( 'm_intOcrFieldId', CStrings::strToIntDef( $intOcrFieldId, NULL, false ) );
	}

	public function getOcrFieldId() {
		return $this->m_intOcrFieldId;
	}

	public function sqlOcrFieldId() {
		return ( true == isset( $this->m_intOcrFieldId ) ) ? ( string ) $this->m_intOcrFieldId : 'NULL';
	}

	public function setAnchorTypeId( $intAnchorTypeId ) {
		$this->set( 'm_intAnchorTypeId', CStrings::strToIntDef( $intAnchorTypeId, NULL, false ) );
	}

	public function getAnchorTypeId() {
		return $this->m_intAnchorTypeId;
	}

	public function sqlAnchorTypeId() {
		return ( true == isset( $this->m_intAnchorTypeId ) ) ? ( string ) $this->m_intAnchorTypeId : 'NULL';
	}

	public function setLabels( $strLabels ) {
		$this->set( 'm_strLabels', CStrings::strTrimDef( $strLabels, 250, NULL, true ) );
	}

	public function getLabels() {
		return $this->m_strLabels;
	}

	public function sqlLabels() {
		return ( true == isset( $this->m_strLabels ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLabels ) : '\'' . addslashes( $this->m_strLabels ) . '\'' ) : 'NULL';
	}

	public function setLabelVertices( $jsonLabelVertices ) {
		if( true == valObj( $jsonLabelVertices, 'stdClass' ) ) {
			$this->set( 'm_jsonLabelVertices', $jsonLabelVertices );
		} elseif( true == valJsonString( $jsonLabelVertices ) ) {
			$this->set( 'm_jsonLabelVertices', CStrings::strToJson( $jsonLabelVertices ) );
		} else {
			$this->set( 'm_jsonLabelVertices', NULL ); 
		}
		unset( $this->m_strLabelVertices );
	}

	public function getLabelVertices() {
		if( true == isset( $this->m_strLabelVertices ) ) {
			$this->m_jsonLabelVertices = CStrings::strToJson( $this->m_strLabelVertices );
			unset( $this->m_strLabelVertices );
		}
		return $this->m_jsonLabelVertices;
	}

	public function sqlLabelVertices() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getLabelVertices() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getLabelVertices() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getLabelVertices() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setAnchors( $jsonAnchors ) {
		if( true == valObj( $jsonAnchors, 'stdClass' ) ) {
			$this->set( 'm_jsonAnchors', $jsonAnchors );
		} elseif( true == valJsonString( $jsonAnchors ) ) {
			$this->set( 'm_jsonAnchors', CStrings::strToJson( $jsonAnchors ) );
		} else {
			$this->set( 'm_jsonAnchors', NULL ); 
		}
		unset( $this->m_strAnchors );
	}

	public function getAnchors() {
		if( true == isset( $this->m_strAnchors ) ) {
			$this->m_jsonAnchors = CStrings::strToJson( $this->m_strAnchors );
			unset( $this->m_strAnchors );
		}
		return $this->m_jsonAnchors;
	}

	public function sqlAnchors() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAnchors() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getAnchors() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getAnchors() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setAnchorVertices( $jsonAnchorVertices ) {
		if( true == valObj( $jsonAnchorVertices, 'stdClass' ) ) {
			$this->set( 'm_jsonAnchorVertices', $jsonAnchorVertices );
		} elseif( true == valJsonString( $jsonAnchorVertices ) ) {
			$this->set( 'm_jsonAnchorVertices', CStrings::strToJson( $jsonAnchorVertices ) );
		} else {
			$this->set( 'm_jsonAnchorVertices', NULL ); 
		}
		unset( $this->m_strAnchorVertices );
	}

	public function getAnchorVertices() {
		if( true == isset( $this->m_strAnchorVertices ) ) {
			$this->m_jsonAnchorVertices = CStrings::strToJson( $this->m_strAnchorVertices );
			unset( $this->m_strAnchorVertices );
		}
		return $this->m_jsonAnchorVertices;
	}

	public function sqlAnchorVertices() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAnchorVertices() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getAnchorVertices() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getAnchorVertices() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_provider_id, ocr_field_id, anchor_type_id, labels, label_vertices, anchors, anchor_vertices, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityProviderId() . ', ' .
						$this->sqlOcrFieldId() . ', ' .
						$this->sqlAnchorTypeId() . ', ' .
						$this->sqlLabels() . ', ' .
						$this->sqlLabelVertices() . ', ' .
						$this->sqlAnchors() . ', ' .
						$this->sqlAnchorVertices() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId(). ',' ; } elseif( true == array_key_exists( 'UtilityProviderId', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_field_id = ' . $this->sqlOcrFieldId(). ',' ; } elseif( true == array_key_exists( 'OcrFieldId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_field_id = ' . $this->sqlOcrFieldId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anchor_type_id = ' . $this->sqlAnchorTypeId(). ',' ; } elseif( true == array_key_exists( 'AnchorTypeId', $this->getChangedColumns() ) ) { $strSql .= ' anchor_type_id = ' . $this->sqlAnchorTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' labels = ' . $this->sqlLabels(). ',' ; } elseif( true == array_key_exists( 'Labels', $this->getChangedColumns() ) ) { $strSql .= ' labels = ' . $this->sqlLabels() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label_vertices = ' . $this->sqlLabelVertices(). ',' ; } elseif( true == array_key_exists( 'LabelVertices', $this->getChangedColumns() ) ) { $strSql .= ' label_vertices = ' . $this->sqlLabelVertices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anchors = ' . $this->sqlAnchors(). ',' ; } elseif( true == array_key_exists( 'Anchors', $this->getChangedColumns() ) ) { $strSql .= ' anchors = ' . $this->sqlAnchors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anchor_vertices = ' . $this->sqlAnchorVertices() ; } elseif( true == array_key_exists( 'AnchorVertices', $this->getChangedColumns() ) ) { $strSql .= ' anchor_vertices = ' . $this->sqlAnchorVertices() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_provider_id' => $this->getUtilityProviderId(),
			'ocr_field_id' => $this->getOcrFieldId(),
			'anchor_type_id' => $this->getAnchorTypeId(),
			'labels' => $this->getLabels(),
			'label_vertices' => $this->getLabelVertices(),
			'anchors' => $this->getAnchors(),
			'anchor_vertices' => $this->getAnchorVertices(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>