<?php

class CBaseOrderHeader extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.order_headers';

	protected $m_intId;
	protected $m_intVendorId;
	protected $m_intApHeaderTypeId;
	protected $m_intApHeaderSubTypeId;
	protected $m_intApPhysicalStatusTypeId;
	protected $m_intApFinancialStatusTypeId;
	protected $m_intStoreId;
	protected $m_intBuyerId;
	protected $m_intBulkBuyerLocationId;
	protected $m_intBuyerAccountId;
	protected $m_intCid;
	protected $m_intApHeaderId;
	protected $m_arrintOrderHeaderIds;
	protected $m_intAssignedWorkerId;
	protected $m_intApExceptionQueueItemId;
	protected $m_strTransactionDatetime;
	protected $m_strDueDate;
	protected $m_strHeaderNumber;
	protected $m_strExternalUrl;
	protected $m_jsonExternalUrl;
	protected $m_strHeaderMemo;
	protected $m_fltControlTotal;
	protected $m_fltTransactionAmount;
	protected $m_fltTransactionAmountDue;
	protected $m_fltTaxAmount;
	protected $m_fltDiscountAmount;
	protected $m_fltShippingAmount;
	protected $m_boolIsNew;
	protected $m_boolIsPosted;
	protected $m_intInvoicedBy;
	protected $m_strInvoicedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strPostDate;

	public function __construct() {
		parent::__construct();

		$this->m_intApHeaderTypeId = '5';
		$this->m_intApHeaderSubTypeId = '5';
		$this->m_fltControlTotal = '0';
		$this->m_fltTransactionAmount = '0';
		$this->m_fltTransactionAmountDue = '0';
		$this->m_fltTaxAmount = '0';
		$this->m_fltDiscountAmount = '0';
		$this->m_fltShippingAmount = '0';
		$this->m_boolIsNew = true;
		$this->m_boolIsPosted = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['ap_header_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderTypeId', trim( $arrValues['ap_header_type_id'] ) ); elseif( isset( $arrValues['ap_header_type_id'] ) ) $this->setApHeaderTypeId( $arrValues['ap_header_type_id'] );
		if( isset( $arrValues['ap_header_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderSubTypeId', trim( $arrValues['ap_header_sub_type_id'] ) ); elseif( isset( $arrValues['ap_header_sub_type_id'] ) ) $this->setApHeaderSubTypeId( $arrValues['ap_header_sub_type_id'] );
		if( isset( $arrValues['ap_physical_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApPhysicalStatusTypeId', trim( $arrValues['ap_physical_status_type_id'] ) ); elseif( isset( $arrValues['ap_physical_status_type_id'] ) ) $this->setApPhysicalStatusTypeId( $arrValues['ap_physical_status_type_id'] );
		if( isset( $arrValues['ap_financial_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApFinancialStatusTypeId', trim( $arrValues['ap_financial_status_type_id'] ) ); elseif( isset( $arrValues['ap_financial_status_type_id'] ) ) $this->setApFinancialStatusTypeId( $arrValues['ap_financial_status_type_id'] );
		if( isset( $arrValues['store_id'] ) && $boolDirectSet ) $this->set( 'm_intStoreId', trim( $arrValues['store_id'] ) ); elseif( isset( $arrValues['store_id'] ) ) $this->setStoreId( $arrValues['store_id'] );
		if( isset( $arrValues['buyer_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerId', trim( $arrValues['buyer_id'] ) ); elseif( isset( $arrValues['buyer_id'] ) ) $this->setBuyerId( $arrValues['buyer_id'] );
		if( isset( $arrValues['bulk_buyer_location_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkBuyerLocationId', trim( $arrValues['bulk_buyer_location_id'] ) ); elseif( isset( $arrValues['bulk_buyer_location_id'] ) ) $this->setBulkBuyerLocationId( $arrValues['bulk_buyer_location_id'] );
		if( isset( $arrValues['buyer_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerAccountId', trim( $arrValues['buyer_account_id'] ) ); elseif( isset( $arrValues['buyer_account_id'] ) ) $this->setBuyerAccountId( $arrValues['buyer_account_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderId', trim( $arrValues['ap_header_id'] ) ); elseif( isset( $arrValues['ap_header_id'] ) ) $this->setApHeaderId( $arrValues['ap_header_id'] );
		if( isset( $arrValues['order_header_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintOrderHeaderIds', trim( $arrValues['order_header_ids'] ) ); elseif( isset( $arrValues['order_header_ids'] ) ) $this->setOrderHeaderIds( $arrValues['order_header_ids'] );
		if( isset( $arrValues['assigned_worker_id'] ) && $boolDirectSet ) $this->set( 'm_intAssignedWorkerId', trim( $arrValues['assigned_worker_id'] ) ); elseif( isset( $arrValues['assigned_worker_id'] ) ) $this->setAssignedWorkerId( $arrValues['assigned_worker_id'] );
		if( isset( $arrValues['ap_exception_queue_item_id'] ) && $boolDirectSet ) $this->set( 'm_intApExceptionQueueItemId', trim( $arrValues['ap_exception_queue_item_id'] ) ); elseif( isset( $arrValues['ap_exception_queue_item_id'] ) ) $this->setApExceptionQueueItemId( $arrValues['ap_exception_queue_item_id'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['due_date'] ) && $boolDirectSet ) $this->set( 'm_strDueDate', trim( $arrValues['due_date'] ) ); elseif( isset( $arrValues['due_date'] ) ) $this->setDueDate( $arrValues['due_date'] );
		if( isset( $arrValues['header_number'] ) && $boolDirectSet ) $this->set( 'm_strHeaderNumber', trim( $arrValues['header_number'] ) ); elseif( isset( $arrValues['header_number'] ) ) $this->setHeaderNumber( $arrValues['header_number'] );
		if( isset( $arrValues['external_url'] ) ) $this->set( 'm_strExternalUrl', trim( $arrValues['external_url'] ) );
		if( isset( $arrValues['header_memo'] ) && $boolDirectSet ) $this->set( 'm_strHeaderMemo', trim( $arrValues['header_memo'] ) ); elseif( isset( $arrValues['header_memo'] ) ) $this->setHeaderMemo( $arrValues['header_memo'] );
		if( isset( $arrValues['control_total'] ) && $boolDirectSet ) $this->set( 'm_fltControlTotal', trim( $arrValues['control_total'] ) ); elseif( isset( $arrValues['control_total'] ) ) $this->setControlTotal( $arrValues['control_total'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['transaction_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmountDue', trim( $arrValues['transaction_amount_due'] ) ); elseif( isset( $arrValues['transaction_amount_due'] ) ) $this->setTransactionAmountDue( $arrValues['transaction_amount_due'] );
		if( isset( $arrValues['tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTaxAmount', trim( $arrValues['tax_amount'] ) ); elseif( isset( $arrValues['tax_amount'] ) ) $this->setTaxAmount( $arrValues['tax_amount'] );
		if( isset( $arrValues['discount_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDiscountAmount', trim( $arrValues['discount_amount'] ) ); elseif( isset( $arrValues['discount_amount'] ) ) $this->setDiscountAmount( $arrValues['discount_amount'] );
		if( isset( $arrValues['shipping_amount'] ) && $boolDirectSet ) $this->set( 'm_fltShippingAmount', trim( $arrValues['shipping_amount'] ) ); elseif( isset( $arrValues['shipping_amount'] ) ) $this->setShippingAmount( $arrValues['shipping_amount'] );
		if( isset( $arrValues['is_new'] ) && $boolDirectSet ) $this->set( 'm_boolIsNew', trim( stripcslashes( $arrValues['is_new'] ) ) ); elseif( isset( $arrValues['is_new'] ) ) $this->setIsNew( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_new'] ) : $arrValues['is_new'] );
		if( isset( $arrValues['is_posted'] ) && $boolDirectSet ) $this->set( 'm_boolIsPosted', trim( stripcslashes( $arrValues['is_posted'] ) ) ); elseif( isset( $arrValues['is_posted'] ) ) $this->setIsPosted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_posted'] ) : $arrValues['is_posted'] );
		if( isset( $arrValues['invoiced_by'] ) && $boolDirectSet ) $this->set( 'm_intInvoicedBy', trim( $arrValues['invoiced_by'] ) ); elseif( isset( $arrValues['invoiced_by'] ) ) $this->setInvoicedBy( $arrValues['invoiced_by'] );
		if( isset( $arrValues['invoiced_on'] ) && $boolDirectSet ) $this->set( 'm_strInvoicedOn', trim( $arrValues['invoiced_on'] ) ); elseif( isset( $arrValues['invoiced_on'] ) ) $this->setInvoicedOn( $arrValues['invoiced_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setApHeaderTypeId( $intApHeaderTypeId ) {
		$this->set( 'm_intApHeaderTypeId', CStrings::strToIntDef( $intApHeaderTypeId, NULL, false ) );
	}

	public function getApHeaderTypeId() {
		return $this->m_intApHeaderTypeId;
	}

	public function sqlApHeaderTypeId() {
		return ( true == isset( $this->m_intApHeaderTypeId ) ) ? ( string ) $this->m_intApHeaderTypeId : '5';
	}

	public function setApHeaderSubTypeId( $intApHeaderSubTypeId ) {
		$this->set( 'm_intApHeaderSubTypeId', CStrings::strToIntDef( $intApHeaderSubTypeId, NULL, false ) );
	}

	public function getApHeaderSubTypeId() {
		return $this->m_intApHeaderSubTypeId;
	}

	public function sqlApHeaderSubTypeId() {
		return ( true == isset( $this->m_intApHeaderSubTypeId ) ) ? ( string ) $this->m_intApHeaderSubTypeId : '5';
	}

	public function setApPhysicalStatusTypeId( $intApPhysicalStatusTypeId ) {
		$this->set( 'm_intApPhysicalStatusTypeId', CStrings::strToIntDef( $intApPhysicalStatusTypeId, NULL, false ) );
	}

	public function getApPhysicalStatusTypeId() {
		return $this->m_intApPhysicalStatusTypeId;
	}

	public function sqlApPhysicalStatusTypeId() {
		return ( true == isset( $this->m_intApPhysicalStatusTypeId ) ) ? ( string ) $this->m_intApPhysicalStatusTypeId : 'NULL';
	}

	public function setApFinancialStatusTypeId( $intApFinancialStatusTypeId ) {
		$this->set( 'm_intApFinancialStatusTypeId', CStrings::strToIntDef( $intApFinancialStatusTypeId, NULL, false ) );
	}

	public function getApFinancialStatusTypeId() {
		return $this->m_intApFinancialStatusTypeId;
	}

	public function sqlApFinancialStatusTypeId() {
		return ( true == isset( $this->m_intApFinancialStatusTypeId ) ) ? ( string ) $this->m_intApFinancialStatusTypeId : 'NULL';
	}

	public function setStoreId( $intStoreId ) {
		$this->set( 'm_intStoreId', CStrings::strToIntDef( $intStoreId, NULL, false ) );
	}

	public function getStoreId() {
		return $this->m_intStoreId;
	}

	public function sqlStoreId() {
		return ( true == isset( $this->m_intStoreId ) ) ? ( string ) $this->m_intStoreId : 'NULL';
	}

	public function setBuyerId( $intBuyerId ) {
		$this->set( 'm_intBuyerId', CStrings::strToIntDef( $intBuyerId, NULL, false ) );
	}

	public function getBuyerId() {
		return $this->m_intBuyerId;
	}

	public function sqlBuyerId() {
		return ( true == isset( $this->m_intBuyerId ) ) ? ( string ) $this->m_intBuyerId : 'NULL';
	}

	public function setBulkBuyerLocationId( $intBulkBuyerLocationId ) {
		$this->set( 'm_intBulkBuyerLocationId', CStrings::strToIntDef( $intBulkBuyerLocationId, NULL, false ) );
	}

	public function getBulkBuyerLocationId() {
		return $this->m_intBulkBuyerLocationId;
	}

	public function sqlBulkBuyerLocationId() {
		return ( true == isset( $this->m_intBulkBuyerLocationId ) ) ? ( string ) $this->m_intBulkBuyerLocationId : 'NULL';
	}

	public function setBuyerAccountId( $intBuyerAccountId ) {
		$this->set( 'm_intBuyerAccountId', CStrings::strToIntDef( $intBuyerAccountId, NULL, false ) );
	}

	public function getBuyerAccountId() {
		return $this->m_intBuyerAccountId;
	}

	public function sqlBuyerAccountId() {
		return ( true == isset( $this->m_intBuyerAccountId ) ) ? ( string ) $this->m_intBuyerAccountId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->set( 'm_intApHeaderId', CStrings::strToIntDef( $intApHeaderId, NULL, false ) );
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function sqlApHeaderId() {
		return ( true == isset( $this->m_intApHeaderId ) ) ? ( string ) $this->m_intApHeaderId : 'NULL';
	}

	public function setOrderHeaderIds( $arrintOrderHeaderIds ) {
		$this->set( 'm_arrintOrderHeaderIds', CStrings::strToArrIntDef( $arrintOrderHeaderIds, NULL ) );
	}

	public function getOrderHeaderIds() {
		return $this->m_arrintOrderHeaderIds;
	}

	public function sqlOrderHeaderIds() {
		return ( true == isset( $this->m_arrintOrderHeaderIds ) && true == valArr( $this->m_arrintOrderHeaderIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintOrderHeaderIds, NULL ) . '\'' : 'NULL';
	}

	public function setAssignedWorkerId( $intAssignedWorkerId ) {
		$this->set( 'm_intAssignedWorkerId', CStrings::strToIntDef( $intAssignedWorkerId, NULL, false ) );
	}

	public function getAssignedWorkerId() {
		return $this->m_intAssignedWorkerId;
	}

	public function sqlAssignedWorkerId() {
		return ( true == isset( $this->m_intAssignedWorkerId ) ) ? ( string ) $this->m_intAssignedWorkerId : 'NULL';
	}

	public function setApExceptionQueueItemId( $intApExceptionQueueItemId ) {
		$this->set( 'm_intApExceptionQueueItemId', CStrings::strToIntDef( $intApExceptionQueueItemId, NULL, false ) );
	}

	public function getApExceptionQueueItemId() {
		return $this->m_intApExceptionQueueItemId;
	}

	public function sqlApExceptionQueueItemId() {
		return ( true == isset( $this->m_intApExceptionQueueItemId ) ) ? ( string ) $this->m_intApExceptionQueueItemId : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setDueDate( $strDueDate ) {
		$this->set( 'm_strDueDate', CStrings::strTrimDef( $strDueDate, -1, NULL, true ) );
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function sqlDueDate() {
		return ( true == isset( $this->m_strDueDate ) ) ? '\'' . $this->m_strDueDate . '\'' : 'NOW()';
	}

	public function setHeaderNumber( $strHeaderNumber ) {
		$this->set( 'm_strHeaderNumber', CStrings::strTrimDef( $strHeaderNumber, 100, NULL, true ) );
	}

	public function getHeaderNumber() {
		return $this->m_strHeaderNumber;
	}

	public function sqlHeaderNumber() {
		return ( true == isset( $this->m_strHeaderNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHeaderNumber ) : '\'' . addslashes( $this->m_strHeaderNumber ) . '\'' ) : 'NULL';
	}

	public function setExternalUrl( $jsonExternalUrl ) {
		if( true == valObj( $jsonExternalUrl, 'stdClass' ) ) {
			$this->set( 'm_jsonExternalUrl', $jsonExternalUrl );
		} elseif( true == valJsonString( $jsonExternalUrl ) ) {
			$this->set( 'm_jsonExternalUrl', CStrings::strToJson( $jsonExternalUrl ) );
		} else {
			$this->set( 'm_jsonExternalUrl', NULL );
		}
		unset( $this->m_strExternalUrl );
	}

	public function getExternalUrl() {
		if( true == isset( $this->m_strExternalUrl ) ) {
			$this->m_jsonExternalUrl = CStrings::strToJson( $this->m_strExternalUrl );
			unset( $this->m_strExternalUrl );
		}
		return $this->m_jsonExternalUrl;
	}

	public function sqlExternalUrl() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getExternalUrl() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getExternalUrl() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getExternalUrl() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setHeaderMemo( $strHeaderMemo ) {
		$this->set( 'm_strHeaderMemo', CStrings::strTrimDef( $strHeaderMemo, 2000, NULL, true ) );
	}

	public function getHeaderMemo() {
		return $this->m_strHeaderMemo;
	}

	public function sqlHeaderMemo() {
		return ( true == isset( $this->m_strHeaderMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHeaderMemo ) : '\'' . addslashes( $this->m_strHeaderMemo ) . '\'' ) : 'NULL';
	}

	public function setControlTotal( $fltControlTotal ) {
		$this->set( 'm_fltControlTotal', CStrings::strToFloatDef( $fltControlTotal, NULL, false, 2 ) );
	}

	public function getControlTotal() {
		return $this->m_fltControlTotal;
	}

	public function sqlControlTotal() {
		return ( true == isset( $this->m_fltControlTotal ) ) ? ( string ) $this->m_fltControlTotal : '0';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : '0';
	}

	public function setTransactionAmountDue( $fltTransactionAmountDue ) {
		$this->set( 'm_fltTransactionAmountDue', CStrings::strToFloatDef( $fltTransactionAmountDue, NULL, false, 2 ) );
	}

	public function getTransactionAmountDue() {
		return $this->m_fltTransactionAmountDue;
	}

	public function sqlTransactionAmountDue() {
		return ( true == isset( $this->m_fltTransactionAmountDue ) ) ? ( string ) $this->m_fltTransactionAmountDue : '0';
	}

	public function setTaxAmount( $fltTaxAmount ) {
		$this->set( 'm_fltTaxAmount', CStrings::strToFloatDef( $fltTaxAmount, NULL, false, 2 ) );
	}

	public function getTaxAmount() {
		return $this->m_fltTaxAmount;
	}

	public function sqlTaxAmount() {
		return ( true == isset( $this->m_fltTaxAmount ) ) ? ( string ) $this->m_fltTaxAmount : '0';
	}

	public function setDiscountAmount( $fltDiscountAmount ) {
		$this->set( 'm_fltDiscountAmount', CStrings::strToFloatDef( $fltDiscountAmount, NULL, false, 2 ) );
	}

	public function getDiscountAmount() {
		return $this->m_fltDiscountAmount;
	}

	public function sqlDiscountAmount() {
		return ( true == isset( $this->m_fltDiscountAmount ) ) ? ( string ) $this->m_fltDiscountAmount : '0';
	}

	public function setShippingAmount( $fltShippingAmount ) {
		$this->set( 'm_fltShippingAmount', CStrings::strToFloatDef( $fltShippingAmount, NULL, false, 2 ) );
	}

	public function getShippingAmount() {
		return $this->m_fltShippingAmount;
	}

	public function sqlShippingAmount() {
		return ( true == isset( $this->m_fltShippingAmount ) ) ? ( string ) $this->m_fltShippingAmount : '0';
	}

	public function setIsNew( $boolIsNew ) {
		$this->set( 'm_boolIsNew', CStrings::strToBool( $boolIsNew ) );
	}

	public function getIsNew() {
		return $this->m_boolIsNew;
	}

	public function sqlIsNew() {
		return ( true == isset( $this->m_boolIsNew ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNew ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPosted( $boolIsPosted ) {
		$this->set( 'm_boolIsPosted', CStrings::strToBool( $boolIsPosted ) );
	}

	public function getIsPosted() {
		return $this->m_boolIsPosted;
	}

	public function sqlIsPosted() {
		return ( true == isset( $this->m_boolIsPosted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPosted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInvoicedBy( $intInvoicedBy ) {
		$this->set( 'm_intInvoicedBy', CStrings::strToIntDef( $intInvoicedBy, NULL, false ) );
	}

	public function getInvoicedBy() {
		return $this->m_intInvoicedBy;
	}

	public function sqlInvoicedBy() {
		return ( true == isset( $this->m_intInvoicedBy ) ) ? ( string ) $this->m_intInvoicedBy : 'NULL';
	}

	public function setInvoicedOn( $strInvoicedOn ) {
		$this->set( 'm_strInvoicedOn', CStrings::strTrimDef( $strInvoicedOn, -1, NULL, true ) );
	}

	public function getInvoicedOn() {
		return $this->m_strInvoicedOn;
	}

	public function sqlInvoicedOn() {
		return ( true == isset( $this->m_strInvoicedOn ) ) ? '\'' . $this->m_strInvoicedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_id, ap_header_type_id, ap_header_sub_type_id, ap_physical_status_type_id, ap_financial_status_type_id, store_id, buyer_id, bulk_buyer_location_id, buyer_account_id, cid, ap_header_id, order_header_ids, assigned_worker_id, ap_exception_queue_item_id, transaction_datetime, due_date, header_number, external_url, header_memo, control_total, transaction_amount, transaction_amount_due, tax_amount, discount_amount, shipping_amount, is_new, is_posted, invoiced_by, invoiced_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, post_date )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlVendorId() . ', ' .
		          $this->sqlApHeaderTypeId() . ', ' .
		          $this->sqlApHeaderSubTypeId() . ', ' .
		          $this->sqlApPhysicalStatusTypeId() . ', ' .
		          $this->sqlApFinancialStatusTypeId() . ', ' .
		          $this->sqlStoreId() . ', ' .
		          $this->sqlBuyerId() . ', ' .
		          $this->sqlBulkBuyerLocationId() . ', ' .
		          $this->sqlBuyerAccountId() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlApHeaderId() . ', ' .
		          $this->sqlOrderHeaderIds() . ', ' .
		          $this->sqlAssignedWorkerId() . ', ' .
		          $this->sqlApExceptionQueueItemId() . ', ' .
		          $this->sqlTransactionDatetime() . ', ' .
		          $this->sqlDueDate() . ', ' .
		          $this->sqlHeaderNumber() . ', ' .
		          $this->sqlExternalUrl() . ', ' .
		          $this->sqlHeaderMemo() . ', ' .
		          $this->sqlControlTotal() . ', ' .
		          $this->sqlTransactionAmount() . ', ' .
		          $this->sqlTransactionAmountDue() . ', ' .
		          $this->sqlTaxAmount() . ', ' .
		          $this->sqlDiscountAmount() . ', ' .
		          $this->sqlShippingAmount() . ', ' .
		          $this->sqlIsNew() . ', ' .
		          $this->sqlIsPosted() . ', ' .
		          $this->sqlInvoicedBy() . ', ' .
		          $this->sqlInvoicedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlPostDate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId(). ',' ; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_type_id = ' . $this->sqlApHeaderTypeId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_type_id = ' . $this->sqlApHeaderTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_sub_type_id = ' . $this->sqlApHeaderSubTypeId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_sub_type_id = ' . $this->sqlApHeaderSubTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_physical_status_type_id = ' . $this->sqlApPhysicalStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ApPhysicalStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_physical_status_type_id = ' . $this->sqlApPhysicalStatusTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_financial_status_type_id = ' . $this->sqlApFinancialStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ApFinancialStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_financial_status_type_id = ' . $this->sqlApFinancialStatusTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' store_id = ' . $this->sqlStoreId(). ',' ; } elseif( true == array_key_exists( 'StoreId', $this->getChangedColumns() ) ) { $strSql .= ' store_id = ' . $this->sqlStoreId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_id = ' . $this->sqlBuyerId(). ',' ; } elseif( true == array_key_exists( 'BuyerId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_id = ' . $this->sqlBuyerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_buyer_location_id = ' . $this->sqlBulkBuyerLocationId(). ',' ; } elseif( true == array_key_exists( 'BulkBuyerLocationId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_buyer_location_id = ' . $this->sqlBulkBuyerLocationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_account_id = ' . $this->sqlBuyerAccountId(). ',' ; } elseif( true == array_key_exists( 'BuyerAccountId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_account_id = ' . $this->sqlBuyerAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_header_ids = ' . $this->sqlOrderHeaderIds(). ',' ; } elseif( true == array_key_exists( 'OrderHeaderIds', $this->getChangedColumns() ) ) { $strSql .= ' order_header_ids = ' . $this->sqlOrderHeaderIds() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_worker_id = ' . $this->sqlAssignedWorkerId(). ',' ; } elseif( true == array_key_exists( 'AssignedWorkerId', $this->getChangedColumns() ) ) { $strSql .= ' assigned_worker_id = ' . $this->sqlAssignedWorkerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_exception_queue_item_id = ' . $this->sqlApExceptionQueueItemId(). ',' ; } elseif( true == array_key_exists( 'ApExceptionQueueItemId', $this->getChangedColumns() ) ) { $strSql .= ' ap_exception_queue_item_id = ' . $this->sqlApExceptionQueueItemId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime(). ',' ; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date = ' . $this->sqlDueDate(). ',' ; } elseif( true == array_key_exists( 'DueDate', $this->getChangedColumns() ) ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_number = ' . $this->sqlHeaderNumber(). ',' ; } elseif( true == array_key_exists( 'HeaderNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_number = ' . $this->sqlHeaderNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_url = ' . $this->sqlExternalUrl(). ',' ; } elseif( true == array_key_exists( 'ExternalUrl', $this->getChangedColumns() ) ) { $strSql .= ' external_url = ' . $this->sqlExternalUrl() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_memo = ' . $this->sqlHeaderMemo(). ',' ; } elseif( true == array_key_exists( 'HeaderMemo', $this->getChangedColumns() ) ) { $strSql .= ' header_memo = ' . $this->sqlHeaderMemo() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' control_total = ' . $this->sqlControlTotal(). ',' ; } elseif( true == array_key_exists( 'ControlTotal', $this->getChangedColumns() ) ) { $strSql .= ' control_total = ' . $this->sqlControlTotal() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount_due = ' . $this->sqlTransactionAmountDue(). ',' ; } elseif( true == array_key_exists( 'TransactionAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount_due = ' . $this->sqlTransactionAmountDue() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_amount = ' . $this->sqlTaxAmount(). ',' ; } elseif( true == array_key_exists( 'TaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' tax_amount = ' . $this->sqlTaxAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discount_amount = ' . $this->sqlDiscountAmount(). ',' ; } elseif( true == array_key_exists( 'DiscountAmount', $this->getChangedColumns() ) ) { $strSql .= ' discount_amount = ' . $this->sqlDiscountAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' shipping_amount = ' . $this->sqlShippingAmount(). ',' ; } elseif( true == array_key_exists( 'ShippingAmount', $this->getChangedColumns() ) ) { $strSql .= ' shipping_amount = ' . $this->sqlShippingAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_new = ' . $this->sqlIsNew(). ',' ; } elseif( true == array_key_exists( 'IsNew', $this->getChangedColumns() ) ) { $strSql .= ' is_new = ' . $this->sqlIsNew() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted(). ',' ; } elseif( true == array_key_exists( 'IsPosted', $this->getChangedColumns() ) ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoiced_by = ' . $this->sqlInvoicedBy(). ',' ; } elseif( true == array_key_exists( 'InvoicedBy', $this->getChangedColumns() ) ) { $strSql .= ' invoiced_by = ' . $this->sqlInvoicedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoiced_on = ' . $this->sqlInvoicedOn(). ',' ; } elseif( true == array_key_exists( 'InvoicedOn', $this->getChangedColumns() ) ) { $strSql .= ' invoiced_on = ' . $this->sqlInvoicedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_id' => $this->getVendorId(),
			'ap_header_type_id' => $this->getApHeaderTypeId(),
			'ap_header_sub_type_id' => $this->getApHeaderSubTypeId(),
			'ap_physical_status_type_id' => $this->getApPhysicalStatusTypeId(),
			'ap_financial_status_type_id' => $this->getApFinancialStatusTypeId(),
			'store_id' => $this->getStoreId(),
			'buyer_id' => $this->getBuyerId(),
			'bulk_buyer_location_id' => $this->getBulkBuyerLocationId(),
			'buyer_account_id' => $this->getBuyerAccountId(),
			'cid' => $this->getCid(),
			'ap_header_id' => $this->getApHeaderId(),
			'order_header_ids' => $this->getOrderHeaderIds(),
			'assigned_worker_id' => $this->getAssignedWorkerId(),
			'ap_exception_queue_item_id' => $this->getApExceptionQueueItemId(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'due_date' => $this->getDueDate(),
			'header_number' => $this->getHeaderNumber(),
			'external_url' => $this->getExternalUrl(),
			'header_memo' => $this->getHeaderMemo(),
			'control_total' => $this->getControlTotal(),
			'transaction_amount' => $this->getTransactionAmount(),
			'transaction_amount_due' => $this->getTransactionAmountDue(),
			'tax_amount' => $this->getTaxAmount(),
			'discount_amount' => $this->getDiscountAmount(),
			'shipping_amount' => $this->getShippingAmount(),
			'is_new' => $this->getIsNew(),
			'is_posted' => $this->getIsPosted(),
			'invoiced_by' => $this->getInvoicedBy(),
			'invoiced_on' => $this->getInvoicedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'post_date' => $this->getPostDate()
		);
	}

}
?>