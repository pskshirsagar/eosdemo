<?php

class CBaseUtilityBatchFactor extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_batch_factors';

	protected $m_intId;
	protected $m_intUtilityBatchId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intPropertyBuildingId;
	protected $m_intUtilityRubsFormulaId;
	protected $m_fltTotalOccupants;
	protected $m_fltTotalUnits;
	protected $m_fltTotalOccupiedUnits;
	protected $m_fltTotalSquareFeets;
	protected $m_fltTotalOccupiedSquareFeet;
	protected $m_fltTotalFactor;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltTotalSubsidyAmount;
	protected $m_strCalculationDetails;
	protected $m_jsonCalculationDetails;
	protected $m_strVacantExpense;
	protected $m_jsonVacantExpense;

	public function __construct() {
		parent::__construct();

		$this->m_intPropertyBuildingId = '0';
		$this->m_intUtilityRubsFormulaId = '0';
		$this->m_fltTotalOccupants = '0.0';
		$this->m_fltTotalUnits = '0.00';
		$this->m_fltTotalOccupiedUnits = '0.00';
		$this->m_fltTotalSquareFeets = '0.0';
		$this->m_fltTotalOccupiedSquareFeet = '0.0';
		$this->m_fltTotalFactor = '0.0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['utility_rubs_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityRubsFormulaId', trim( $arrValues['utility_rubs_formula_id'] ) ); elseif( isset( $arrValues['utility_rubs_formula_id'] ) ) $this->setUtilityRubsFormulaId( $arrValues['utility_rubs_formula_id'] );
		if( isset( $arrValues['total_occupants'] ) && $boolDirectSet ) $this->set( 'm_fltTotalOccupants', trim( $arrValues['total_occupants'] ) ); elseif( isset( $arrValues['total_occupants'] ) ) $this->setTotalOccupants( $arrValues['total_occupants'] );
		if( isset( $arrValues['total_units'] ) && $boolDirectSet ) $this->set( 'm_fltTotalUnits', trim( $arrValues['total_units'] ) ); elseif( isset( $arrValues['total_units'] ) ) $this->setTotalUnits( $arrValues['total_units'] );
		if( isset( $arrValues['total_occupied_units'] ) && $boolDirectSet ) $this->set( 'm_fltTotalOccupiedUnits', trim( $arrValues['total_occupied_units'] ) ); elseif( isset( $arrValues['total_occupied_units'] ) ) $this->setTotalOccupiedUnits( $arrValues['total_occupied_units'] );
		if( isset( $arrValues['total_square_feets'] ) && $boolDirectSet ) $this->set( 'm_fltTotalSquareFeets', trim( $arrValues['total_square_feets'] ) ); elseif( isset( $arrValues['total_square_feets'] ) ) $this->setTotalSquareFeets( $arrValues['total_square_feets'] );
		if( isset( $arrValues['total_occupied_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltTotalOccupiedSquareFeet', trim( $arrValues['total_occupied_square_feet'] ) ); elseif( isset( $arrValues['total_occupied_square_feet'] ) ) $this->setTotalOccupiedSquareFeet( $arrValues['total_occupied_square_feet'] );
		if( isset( $arrValues['total_factor'] ) && $boolDirectSet ) $this->set( 'm_fltTotalFactor', trim( $arrValues['total_factor'] ) ); elseif( isset( $arrValues['total_factor'] ) ) $this->setTotalFactor( $arrValues['total_factor'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['total_subsidy_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalSubsidyAmount', trim( $arrValues['total_subsidy_amount'] ) ); elseif( isset( $arrValues['total_subsidy_amount'] ) ) $this->setTotalSubsidyAmount( $arrValues['total_subsidy_amount'] );
		if( isset( $arrValues['calculation_details'] ) ) $this->set( 'm_strCalculationDetails', trim( $arrValues['calculation_details'] ) );
		if( isset( $arrValues['vacant_expense'] ) ) $this->set( 'm_strVacantExpense', trim( $arrValues['vacant_expense'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : '0';
	}

	public function setUtilityRubsFormulaId( $intUtilityRubsFormulaId ) {
		$this->set( 'm_intUtilityRubsFormulaId', CStrings::strToIntDef( $intUtilityRubsFormulaId, NULL, false ) );
	}

	public function getUtilityRubsFormulaId() {
		return $this->m_intUtilityRubsFormulaId;
	}

	public function sqlUtilityRubsFormulaId() {
		return ( true == isset( $this->m_intUtilityRubsFormulaId ) ) ? ( string ) $this->m_intUtilityRubsFormulaId : '0';
	}

	public function setTotalOccupants( $fltTotalOccupants ) {
		$this->set( 'm_fltTotalOccupants', CStrings::strToFloatDef( $fltTotalOccupants, NULL, false, 4 ) );
	}

	public function getTotalOccupants() {
		return $this->m_fltTotalOccupants;
	}

	public function sqlTotalOccupants() {
		return ( true == isset( $this->m_fltTotalOccupants ) ) ? ( string ) $this->m_fltTotalOccupants : '0.0';
	}

	public function setTotalUnits( $fltTotalUnits ) {
		$this->set( 'm_fltTotalUnits', CStrings::strToFloatDef( $fltTotalUnits, NULL, false, 4 ) );
	}

	public function getTotalUnits() {
		return $this->m_fltTotalUnits;
	}

	public function sqlTotalUnits() {
		return ( true == isset( $this->m_fltTotalUnits ) ) ? ( string ) $this->m_fltTotalUnits : '0.00';
	}

	public function setTotalOccupiedUnits( $fltTotalOccupiedUnits ) {
		$this->set( 'm_fltTotalOccupiedUnits', CStrings::strToFloatDef( $fltTotalOccupiedUnits, NULL, false, 4 ) );
	}

	public function getTotalOccupiedUnits() {
		return $this->m_fltTotalOccupiedUnits;
	}

	public function sqlTotalOccupiedUnits() {
		return ( true == isset( $this->m_fltTotalOccupiedUnits ) ) ? ( string ) $this->m_fltTotalOccupiedUnits : '0.00';
	}

	public function setTotalSquareFeets( $fltTotalSquareFeets ) {
		$this->set( 'm_fltTotalSquareFeets', CStrings::strToFloatDef( $fltTotalSquareFeets, NULL, false, 4 ) );
	}

	public function getTotalSquareFeets() {
		return $this->m_fltTotalSquareFeets;
	}

	public function sqlTotalSquareFeets() {
		return ( true == isset( $this->m_fltTotalSquareFeets ) ) ? ( string ) $this->m_fltTotalSquareFeets : '0.0';
	}

	public function setTotalOccupiedSquareFeet( $fltTotalOccupiedSquareFeet ) {
		$this->set( 'm_fltTotalOccupiedSquareFeet', CStrings::strToFloatDef( $fltTotalOccupiedSquareFeet, NULL, false, 4 ) );
	}

	public function getTotalOccupiedSquareFeet() {
		return $this->m_fltTotalOccupiedSquareFeet;
	}

	public function sqlTotalOccupiedSquareFeet() {
		return ( true == isset( $this->m_fltTotalOccupiedSquareFeet ) ) ? ( string ) $this->m_fltTotalOccupiedSquareFeet : '0.0';
	}

	public function setTotalFactor( $fltTotalFactor ) {
		$this->set( 'm_fltTotalFactor', CStrings::strToFloatDef( $fltTotalFactor, NULL, false, 4 ) );
	}

	public function getTotalFactor() {
		return $this->m_fltTotalFactor;
	}

	public function sqlTotalFactor() {
		return ( true == isset( $this->m_fltTotalFactor ) ) ? ( string ) $this->m_fltTotalFactor : '0.0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTotalSubsidyAmount( $fltTotalSubsidyAmount ) {
		$this->set( 'm_fltTotalSubsidyAmount', CStrings::strToFloatDef( $fltTotalSubsidyAmount, NULL, false, 2 ) );
	}

	public function getTotalSubsidyAmount() {
		return $this->m_fltTotalSubsidyAmount;
	}

	public function sqlTotalSubsidyAmount() {
		return ( true == isset( $this->m_fltTotalSubsidyAmount ) ) ? ( string ) $this->m_fltTotalSubsidyAmount : 'NULL';
	}

	public function setCalculationDetails( $jsonCalculationDetails ) {
		if( true == valObj( $jsonCalculationDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonCalculationDetails', $jsonCalculationDetails );
		} elseif( true == valJsonString( $jsonCalculationDetails ) ) {
			$this->set( 'm_jsonCalculationDetails', CStrings::strToJson( $jsonCalculationDetails ) );
		} else {
			$this->set( 'm_jsonCalculationDetails', NULL ); 
		}
		unset( $this->m_strCalculationDetails );
	}

	public function getCalculationDetails() {
		if( true == isset( $this->m_strCalculationDetails ) ) {
			$this->m_jsonCalculationDetails = CStrings::strToJson( $this->m_strCalculationDetails );
			unset( $this->m_strCalculationDetails );
		}
		return $this->m_jsonCalculationDetails;
	}

	public function sqlCalculationDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getCalculationDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getCalculationDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setVacantExpense( $jsonVacantExpense ) {
		if( true == valObj( $jsonVacantExpense, 'stdClass' ) ) {
			$this->set( 'm_jsonVacantExpense', $jsonVacantExpense );
		} elseif( true == valJsonString( $jsonVacantExpense ) ) {
			$this->set( 'm_jsonVacantExpense', CStrings::strToJson( $jsonVacantExpense ) );
		} else {
			$this->set( 'm_jsonVacantExpense', NULL ); 
		}
		unset( $this->m_strVacantExpense );
	}

	public function getVacantExpense() {
		if( true == isset( $this->m_strVacantExpense ) ) {
			$this->m_jsonVacantExpense = CStrings::strToJson( $this->m_strVacantExpense );
			unset( $this->m_strVacantExpense );
		}
		return $this->m_jsonVacantExpense;
	}

	public function sqlVacantExpense() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getVacantExpense() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getVacantExpense() ) ) . '\'';
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_batch_id, property_utility_type_id, property_building_id, utility_rubs_formula_id, total_occupants, total_units, total_occupied_units, total_square_feets, total_occupied_square_feet, total_factor, updated_by, updated_on, created_by, created_on, total_subsidy_amount, calculation_details, vacant_expense )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityBatchId() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlUtilityRubsFormulaId() . ', ' .
						$this->sqlTotalOccupants() . ', ' .
						$this->sqlTotalUnits() . ', ' .
						$this->sqlTotalOccupiedUnits() . ', ' .
						$this->sqlTotalSquareFeets() . ', ' .
						$this->sqlTotalOccupiedSquareFeet() . ', ' .
						$this->sqlTotalFactor() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlTotalSubsidyAmount() . ', ' .
						$this->sqlCalculationDetails() . ', ' .
						$this->sqlVacantExpense() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId(). ',' ; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_rubs_formula_id = ' . $this->sqlUtilityRubsFormulaId(). ',' ; } elseif( true == array_key_exists( 'UtilityRubsFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' utility_rubs_formula_id = ' . $this->sqlUtilityRubsFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_occupants = ' . $this->sqlTotalOccupants(). ',' ; } elseif( true == array_key_exists( 'TotalOccupants', $this->getChangedColumns() ) ) { $strSql .= ' total_occupants = ' . $this->sqlTotalOccupants() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_units = ' . $this->sqlTotalUnits(). ',' ; } elseif( true == array_key_exists( 'TotalUnits', $this->getChangedColumns() ) ) { $strSql .= ' total_units = ' . $this->sqlTotalUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_occupied_units = ' . $this->sqlTotalOccupiedUnits(). ',' ; } elseif( true == array_key_exists( 'TotalOccupiedUnits', $this->getChangedColumns() ) ) { $strSql .= ' total_occupied_units = ' . $this->sqlTotalOccupiedUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_square_feets = ' . $this->sqlTotalSquareFeets(). ',' ; } elseif( true == array_key_exists( 'TotalSquareFeets', $this->getChangedColumns() ) ) { $strSql .= ' total_square_feets = ' . $this->sqlTotalSquareFeets() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_occupied_square_feet = ' . $this->sqlTotalOccupiedSquareFeet(). ',' ; } elseif( true == array_key_exists( 'TotalOccupiedSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' total_occupied_square_feet = ' . $this->sqlTotalOccupiedSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_factor = ' . $this->sqlTotalFactor(). ',' ; } elseif( true == array_key_exists( 'TotalFactor', $this->getChangedColumns() ) ) { $strSql .= ' total_factor = ' . $this->sqlTotalFactor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_subsidy_amount = ' . $this->sqlTotalSubsidyAmount(). ',' ; } elseif( true == array_key_exists( 'TotalSubsidyAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_subsidy_amount = ' . $this->sqlTotalSubsidyAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calculation_details = ' . $this->sqlCalculationDetails(). ',' ; } elseif( true == array_key_exists( 'CalculationDetails', $this->getChangedColumns() ) ) { $strSql .= ' calculation_details = ' . $this->sqlCalculationDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacant_expense = ' . $this->sqlVacantExpense(). ',' ; } elseif( true == array_key_exists( 'VacantExpense', $this->getChangedColumns() ) ) { $strSql .= ' vacant_expense = ' . $this->sqlVacantExpense() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'utility_rubs_formula_id' => $this->getUtilityRubsFormulaId(),
			'total_occupants' => $this->getTotalOccupants(),
			'total_units' => $this->getTotalUnits(),
			'total_occupied_units' => $this->getTotalOccupiedUnits(),
			'total_square_feets' => $this->getTotalSquareFeets(),
			'total_occupied_square_feet' => $this->getTotalOccupiedSquareFeet(),
			'total_factor' => $this->getTotalFactor(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'total_subsidy_amount' => $this->getTotalSubsidyAmount(),
			'calculation_details' => $this->getCalculationDetails(),
			'vacant_expense' => $this->getVacantExpense()
		);
	}

}
?>