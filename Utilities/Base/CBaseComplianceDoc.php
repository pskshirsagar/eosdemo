<?php

class CBaseComplianceDoc extends CEosSingularBase {

	const TABLE_NAME = 'public.compliance_docs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intComplianceDocId;
	protected $m_intVendorId;
	protected $m_intApPayeeId;
	protected $m_intComplianceLevelId;
	protected $m_intComplianceItemId;
	protected $m_intRequestComplianceJobItemId;
	protected $m_intVendorReferenceId;
	protected $m_intEntrataReferenceId;
	protected $m_intScreeningId;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_intDocId;
	protected $m_strCoverageName;
	protected $m_strDocDatetime;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_fltAmount;
	protected $m_strExpirationDate;
	protected $m_boolIsVendorDoc;
	protected $m_boolIsArchived;
	protected $m_boolIsAdditionalInterest;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsVendorDoc = false;
		$this->m_boolIsArchived = false;
		$this->m_boolIsAdditionalInterest = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['compliance_doc_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceDocId', trim( $arrValues['compliance_doc_id'] ) ); elseif( isset( $arrValues['compliance_doc_id'] ) ) $this->setComplianceDocId( $arrValues['compliance_doc_id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['compliance_level_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceLevelId', trim( $arrValues['compliance_level_id'] ) ); elseif( isset( $arrValues['compliance_level_id'] ) ) $this->setComplianceLevelId( $arrValues['compliance_level_id'] );
		if( isset( $arrValues['compliance_item_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceItemId', trim( $arrValues['compliance_item_id'] ) ); elseif( isset( $arrValues['compliance_item_id'] ) ) $this->setComplianceItemId( $arrValues['compliance_item_id'] );
		if( isset( $arrValues['request_compliance_job_item_id'] ) && $boolDirectSet ) $this->set( 'm_intRequestComplianceJobItemId', trim( $arrValues['request_compliance_job_item_id'] ) ); elseif( isset( $arrValues['request_compliance_job_item_id'] ) ) $this->setRequestComplianceJobItemId( $arrValues['request_compliance_job_item_id'] );
		if( isset( $arrValues['vendor_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorReferenceId', trim( $arrValues['vendor_reference_id'] ) ); elseif( isset( $arrValues['vendor_reference_id'] ) ) $this->setVendorReferenceId( $arrValues['vendor_reference_id'] );
		if( isset( $arrValues['entrata_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intEntrataReferenceId', trim( $arrValues['entrata_reference_id'] ) ); elseif( isset( $arrValues['entrata_reference_id'] ) ) $this->setEntrataReferenceId( $arrValues['entrata_reference_id'] );
		if( isset( $arrValues['screening_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningId', trim( $arrValues['screening_id'] ) ); elseif( isset( $arrValues['screening_id'] ) ) $this->setScreeningId( $arrValues['screening_id'] );
		if( isset( $arrValues['screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningRecommendationTypeId', trim( $arrValues['screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['screening_recommendation_type_id'] ) ) $this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		if( isset( $arrValues['doc_id'] ) && $boolDirectSet ) $this->set( 'm_intDocId', trim( $arrValues['doc_id'] ) ); elseif( isset( $arrValues['doc_id'] ) ) $this->setDocId( $arrValues['doc_id'] );
		if( isset( $arrValues['coverage_name'] ) && $boolDirectSet ) $this->set( 'm_strCoverageName', trim( stripcslashes( $arrValues['coverage_name'] ) ) ); elseif( isset( $arrValues['coverage_name'] ) ) $this->setCoverageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['coverage_name'] ) : $arrValues['coverage_name'] );
		if( isset( $arrValues['doc_datetime'] ) && $boolDirectSet ) $this->set( 'm_strDocDatetime', trim( $arrValues['doc_datetime'] ) ); elseif( isset( $arrValues['doc_datetime'] ) ) $this->setDocDatetime( $arrValues['doc_datetime'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['expiration_date'] ) && $boolDirectSet ) $this->set( 'm_strExpirationDate', trim( $arrValues['expiration_date'] ) ); elseif( isset( $arrValues['expiration_date'] ) ) $this->setExpirationDate( $arrValues['expiration_date'] );
		if( isset( $arrValues['is_vendor_doc'] ) && $boolDirectSet ) $this->set( 'm_boolIsVendorDoc', trim( stripcslashes( $arrValues['is_vendor_doc'] ) ) ); elseif( isset( $arrValues['is_vendor_doc'] ) ) $this->setIsVendorDoc( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_vendor_doc'] ) : $arrValues['is_vendor_doc'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_boolIsArchived', trim( stripcslashes( $arrValues['is_archived'] ) ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_archived'] ) : $arrValues['is_archived'] );
		if( isset( $arrValues['is_additional_interest'] ) && $boolDirectSet ) $this->set( 'm_boolIsAdditionalInterest', trim( stripcslashes( $arrValues['is_additional_interest'] ) ) ); elseif( isset( $arrValues['is_additional_interest'] ) ) $this->setIsAdditionalInterest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_additional_interest'] ) : $arrValues['is_additional_interest'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setComplianceDocId( $intComplianceDocId ) {
		$this->set( 'm_intComplianceDocId', CStrings::strToIntDef( $intComplianceDocId, NULL, false ) );
	}

	public function getComplianceDocId() {
		return $this->m_intComplianceDocId;
	}

	public function sqlComplianceDocId() {
		return ( true == isset( $this->m_intComplianceDocId ) ) ? ( string ) $this->m_intComplianceDocId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setComplianceLevelId( $intComplianceLevelId ) {
		$this->set( 'm_intComplianceLevelId', CStrings::strToIntDef( $intComplianceLevelId, NULL, false ) );
	}

	public function getComplianceLevelId() {
		return $this->m_intComplianceLevelId;
	}

	public function sqlComplianceLevelId() {
		return ( true == isset( $this->m_intComplianceLevelId ) ) ? ( string ) $this->m_intComplianceLevelId : 'NULL';
	}

	public function setComplianceItemId( $intComplianceItemId ) {
		$this->set( 'm_intComplianceItemId', CStrings::strToIntDef( $intComplianceItemId, NULL, false ) );
	}

	public function getComplianceItemId() {
		return $this->m_intComplianceItemId;
	}

	public function sqlComplianceItemId() {
		return ( true == isset( $this->m_intComplianceItemId ) ) ? ( string ) $this->m_intComplianceItemId : 'NULL';
	}

	public function setRequestComplianceJobItemId( $intRequestComplianceJobItemId ) {
		$this->set( 'm_intRequestComplianceJobItemId', CStrings::strToIntDef( $intRequestComplianceJobItemId, NULL, false ) );
	}

	public function getRequestComplianceJobItemId() {
		return $this->m_intRequestComplianceJobItemId;
	}

	public function sqlRequestComplianceJobItemId() {
		return ( true == isset( $this->m_intRequestComplianceJobItemId ) ) ? ( string ) $this->m_intRequestComplianceJobItemId : 'NULL';
	}

	public function setVendorReferenceId( $intVendorReferenceId ) {
		$this->set( 'm_intVendorReferenceId', CStrings::strToIntDef( $intVendorReferenceId, NULL, false ) );
	}

	public function getVendorReferenceId() {
		return $this->m_intVendorReferenceId;
	}

	public function sqlVendorReferenceId() {
		return ( true == isset( $this->m_intVendorReferenceId ) ) ? ( string ) $this->m_intVendorReferenceId : 'NULL';
	}

	public function setEntrataReferenceId( $intEntrataReferenceId ) {
		$this->set( 'm_intEntrataReferenceId', CStrings::strToIntDef( $intEntrataReferenceId, NULL, false ) );
	}

	public function getEntrataReferenceId() {
		return $this->m_intEntrataReferenceId;
	}

	public function sqlEntrataReferenceId() {
		return ( true == isset( $this->m_intEntrataReferenceId ) ) ? ( string ) $this->m_intEntrataReferenceId : 'NULL';
	}

	public function setScreeningId( $intScreeningId ) {
		$this->set( 'm_intScreeningId', CStrings::strToIntDef( $intScreeningId, NULL, false ) );
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function sqlScreeningId() {
		return ( true == isset( $this->m_intScreeningId ) ) ? ( string ) $this->m_intScreeningId : 'NULL';
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->set( 'm_intScreeningRecommendationTypeId', CStrings::strToIntDef( $intScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function sqlScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intScreeningRecommendationTypeId : 'NULL';
	}

	public function setDocId( $intDocId ) {
		$this->set( 'm_intDocId', CStrings::strToIntDef( $intDocId, NULL, false ) );
	}

	public function getDocId() {
		return $this->m_intDocId;
	}

	public function sqlDocId() {
		return ( true == isset( $this->m_intDocId ) ) ? ( string ) $this->m_intDocId : 'NULL';
	}

	public function setCoverageName( $strCoverageName ) {
		$this->set( 'm_strCoverageName', CStrings::strTrimDef( $strCoverageName, 240, NULL, true ) );
	}

	public function getCoverageName() {
		return $this->m_strCoverageName;
	}

	public function sqlCoverageName() {
		return ( true == isset( $this->m_strCoverageName ) ) ? '\'' . addslashes( $this->m_strCoverageName ) . '\'' : 'NULL';
	}

	public function setDocDatetime( $strDocDatetime ) {
		$this->set( 'm_strDocDatetime', CStrings::strTrimDef( $strDocDatetime, -1, NULL, true ) );
	}

	public function getDocDatetime() {
		return $this->m_strDocDatetime;
	}

	public function sqlDocDatetime() {
		return ( true == isset( $this->m_strDocDatetime ) ) ? '\'' . $this->m_strDocDatetime . '\'' : 'NOW()';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 510, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 2 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : 'NULL';
	}

	public function setExpirationDate( $strExpirationDate ) {
		$this->set( 'm_strExpirationDate', CStrings::strTrimDef( $strExpirationDate, -1, NULL, true ) );
	}

	public function getExpirationDate() {
		return $this->m_strExpirationDate;
	}

	public function sqlExpirationDate() {
		return ( true == isset( $this->m_strExpirationDate ) ) ? '\'' . $this->m_strExpirationDate . '\'' : 'NULL';
	}

	public function setIsVendorDoc( $boolIsVendorDoc ) {
		$this->set( 'm_boolIsVendorDoc', CStrings::strToBool( $boolIsVendorDoc ) );
	}

	public function getIsVendorDoc() {
		return $this->m_boolIsVendorDoc;
	}

	public function sqlIsVendorDoc() {
		return ( true == isset( $this->m_boolIsVendorDoc ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVendorDoc ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsArchived( $boolIsArchived ) {
		$this->set( 'm_boolIsArchived', CStrings::strToBool( $boolIsArchived ) );
	}

	public function getIsArchived() {
		return $this->m_boolIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_boolIsArchived ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsArchived ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAdditionalInterest( $boolIsAdditionalInterest ) {
		$this->set( 'm_boolIsAdditionalInterest', CStrings::strToBool( $boolIsAdditionalInterest ) );
	}

	public function getIsAdditionalInterest() {
		return $this->m_boolIsAdditionalInterest;
	}

	public function sqlIsAdditionalInterest() {
		return ( true == isset( $this->m_boolIsAdditionalInterest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAdditionalInterest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, compliance_doc_id, vendor_id, ap_payee_id, compliance_level_id, compliance_item_id, request_compliance_job_item_id, vendor_reference_id, entrata_reference_id, screening_id, screening_recommendation_type_id, doc_id, coverage_name, doc_datetime, title, description, amount, expiration_date, is_vendor_doc, is_archived, is_additional_interest, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlComplianceDocId() . ', ' .
 						$this->sqlVendorId() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlComplianceLevelId() . ', ' .
 						$this->sqlComplianceItemId() . ', ' .
 						$this->sqlRequestComplianceJobItemId() . ', ' .
 						$this->sqlVendorReferenceId() . ', ' .
 						$this->sqlEntrataReferenceId() . ', ' .
 						$this->sqlScreeningId() . ', ' .
 						$this->sqlScreeningRecommendationTypeId() . ', ' .
 						$this->sqlDocId() . ', ' .
 						$this->sqlCoverageName() . ', ' .
 						$this->sqlDocDatetime() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlAmount() . ', ' .
 						$this->sqlExpirationDate() . ', ' .
 						$this->sqlIsVendorDoc() . ', ' .
 						$this->sqlIsArchived() . ', ' .
 						$this->sqlIsAdditionalInterest() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_doc_id = ' . $this->sqlComplianceDocId() . ','; } elseif( true == array_key_exists( 'ComplianceDocId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_doc_id = ' . $this->sqlComplianceDocId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_level_id = ' . $this->sqlComplianceLevelId() . ','; } elseif( true == array_key_exists( 'ComplianceLevelId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_level_id = ' . $this->sqlComplianceLevelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_item_id = ' . $this->sqlComplianceItemId() . ','; } elseif( true == array_key_exists( 'ComplianceItemId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_item_id = ' . $this->sqlComplianceItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_compliance_job_item_id = ' . $this->sqlRequestComplianceJobItemId() . ','; } elseif( true == array_key_exists( 'RequestComplianceJobItemId', $this->getChangedColumns() ) ) { $strSql .= ' request_compliance_job_item_id = ' . $this->sqlRequestComplianceJobItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_reference_id = ' . $this->sqlVendorReferenceId() . ','; } elseif( true == array_key_exists( 'VendorReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_reference_id = ' . $this->sqlVendorReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_reference_id = ' . $this->sqlEntrataReferenceId() . ','; } elseif( true == array_key_exists( 'EntrataReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' entrata_reference_id = ' . $this->sqlEntrataReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; } elseif( true == array_key_exists( 'ScreeningId', $this->getChangedColumns() ) ) { $strSql .= ' screening_id = ' . $this->sqlScreeningId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' doc_id = ' . $this->sqlDocId() . ','; } elseif( true == array_key_exists( 'DocId', $this->getChangedColumns() ) ) { $strSql .= ' doc_id = ' . $this->sqlDocId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' coverage_name = ' . $this->sqlCoverageName() . ','; } elseif( true == array_key_exists( 'CoverageName', $this->getChangedColumns() ) ) { $strSql .= ' coverage_name = ' . $this->sqlCoverageName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' doc_datetime = ' . $this->sqlDocDatetime() . ','; } elseif( true == array_key_exists( 'DocDatetime', $this->getChangedColumns() ) ) { $strSql .= ' doc_datetime = ' . $this->sqlDocDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiration_date = ' . $this->sqlExpirationDate() . ','; } elseif( true == array_key_exists( 'ExpirationDate', $this->getChangedColumns() ) ) { $strSql .= ' expiration_date = ' . $this->sqlExpirationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_vendor_doc = ' . $this->sqlIsVendorDoc() . ','; } elseif( true == array_key_exists( 'IsVendorDoc', $this->getChangedColumns() ) ) { $strSql .= ' is_vendor_doc = ' . $this->sqlIsVendorDoc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_additional_interest = ' . $this->sqlIsAdditionalInterest() . ','; } elseif( true == array_key_exists( 'IsAdditionalInterest', $this->getChangedColumns() ) ) { $strSql .= ' is_additional_interest = ' . $this->sqlIsAdditionalInterest() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'compliance_doc_id' => $this->getComplianceDocId(),
			'vendor_id' => $this->getVendorId(),
			'ap_payee_id' => $this->getApPayeeId(),
			'compliance_level_id' => $this->getComplianceLevelId(),
			'compliance_item_id' => $this->getComplianceItemId(),
			'request_compliance_job_item_id' => $this->getRequestComplianceJobItemId(),
			'vendor_reference_id' => $this->getVendorReferenceId(),
			'entrata_reference_id' => $this->getEntrataReferenceId(),
			'screening_id' => $this->getScreeningId(),
			'screening_recommendation_type_id' => $this->getScreeningRecommendationTypeId(),
			'doc_id' => $this->getDocId(),
			'coverage_name' => $this->getCoverageName(),
			'doc_datetime' => $this->getDocDatetime(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'amount' => $this->getAmount(),
			'expiration_date' => $this->getExpirationDate(),
			'is_vendor_doc' => $this->getIsVendorDoc(),
			'is_archived' => $this->getIsArchived(),
			'is_additional_interest' => $this->getIsAdditionalInterest(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>