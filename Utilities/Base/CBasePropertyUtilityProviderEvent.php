<?php

class CBasePropertyUtilityProviderEvent extends CEosSingularBase {

	const TABLE_NAME = 'public.property_utility_provider_events';

	protected $m_intId;
	protected $m_intPropertyId;
	protected $m_intUtilityProviderId;
	protected $m_intUtilityContactTypeId;
	protected $m_intSystemEmailId;
	protected $m_intUtilityDocumentId;
	protected $m_strEventDatetime;
	protected $m_strEmailToAddress;
	protected $m_strEmailSubject;
	protected $m_strEmailContent;
	protected $m_strFaxNumber;
	protected $m_strMailTo;
	protected $m_strMailStreetLineOne;
	protected $m_strMailStreetLineTwo;
	protected $m_strMailCity;
	protected $m_strMailState;
	protected $m_strMailPostalCode;
	protected $m_strPhoneNumber;
	protected $m_strPhoneContact;
	protected $m_boolPhoneIsInProgress;
	protected $m_strPhoneNotes;
	protected $m_strIsSystemDocument;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolPhoneIsInProgress = false;
		$this->m_strIsSystemDocument = '\'False\'::text';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityProviderId', trim( $arrValues['utility_provider_id'] ) ); elseif( isset( $arrValues['utility_provider_id'] ) ) $this->setUtilityProviderId( $arrValues['utility_provider_id'] );
		if( isset( $arrValues['utility_contact_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityContactTypeId', trim( $arrValues['utility_contact_type_id'] ) ); elseif( isset( $arrValues['utility_contact_type_id'] ) ) $this->setUtilityContactTypeId( $arrValues['utility_contact_type_id'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['utility_document_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityDocumentId', trim( $arrValues['utility_document_id'] ) ); elseif( isset( $arrValues['utility_document_id'] ) ) $this->setUtilityDocumentId( $arrValues['utility_document_id'] );
		if( isset( $arrValues['event_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEventDatetime', trim( $arrValues['event_datetime'] ) ); elseif( isset( $arrValues['event_datetime'] ) ) $this->setEventDatetime( $arrValues['event_datetime'] );
		if( isset( $arrValues['email_to_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailToAddress', trim( $arrValues['email_to_address'] ) ); elseif( isset( $arrValues['email_to_address'] ) ) $this->setEmailToAddress( $arrValues['email_to_address'] );
		if( isset( $arrValues['email_subject'] ) && $boolDirectSet ) $this->set( 'm_strEmailSubject', trim( $arrValues['email_subject'] ) ); elseif( isset( $arrValues['email_subject'] ) ) $this->setEmailSubject( $arrValues['email_subject'] );
		if( isset( $arrValues['email_content'] ) && $boolDirectSet ) $this->set( 'm_strEmailContent', trim( $arrValues['email_content'] ) ); elseif( isset( $arrValues['email_content'] ) ) $this->setEmailContent( $arrValues['email_content'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( $arrValues['fax_number'] ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( $arrValues['fax_number'] );
		if( isset( $arrValues['mail_to'] ) && $boolDirectSet ) $this->set( 'm_strMailTo', trim( $arrValues['mail_to'] ) ); elseif( isset( $arrValues['mail_to'] ) ) $this->setMailTo( $arrValues['mail_to'] );
		if( isset( $arrValues['mail_street_line_one'] ) && $boolDirectSet ) $this->set( 'm_strMailStreetLineOne', trim( $arrValues['mail_street_line_one'] ) ); elseif( isset( $arrValues['mail_street_line_one'] ) ) $this->setMailStreetLineOne( $arrValues['mail_street_line_one'] );
		if( isset( $arrValues['mail_street_line_two'] ) && $boolDirectSet ) $this->set( 'm_strMailStreetLineTwo', trim( $arrValues['mail_street_line_two'] ) ); elseif( isset( $arrValues['mail_street_line_two'] ) ) $this->setMailStreetLineTwo( $arrValues['mail_street_line_two'] );
		if( isset( $arrValues['mail_city'] ) && $boolDirectSet ) $this->set( 'm_strMailCity', trim( $arrValues['mail_city'] ) ); elseif( isset( $arrValues['mail_city'] ) ) $this->setMailCity( $arrValues['mail_city'] );
		if( isset( $arrValues['mail_state'] ) && $boolDirectSet ) $this->set( 'm_strMailState', trim( $arrValues['mail_state'] ) ); elseif( isset( $arrValues['mail_state'] ) ) $this->setMailState( $arrValues['mail_state'] );
		if( isset( $arrValues['mail_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strMailPostalCode', trim( $arrValues['mail_postal_code'] ) ); elseif( isset( $arrValues['mail_postal_code'] ) ) $this->setMailPostalCode( $arrValues['mail_postal_code'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['phone_contact'] ) && $boolDirectSet ) $this->set( 'm_strPhoneContact', trim( $arrValues['phone_contact'] ) ); elseif( isset( $arrValues['phone_contact'] ) ) $this->setPhoneContact( $arrValues['phone_contact'] );
		if( isset( $arrValues['phone_is_in_progress'] ) && $boolDirectSet ) $this->set( 'm_boolPhoneIsInProgress', trim( stripcslashes( $arrValues['phone_is_in_progress'] ) ) ); elseif( isset( $arrValues['phone_is_in_progress'] ) ) $this->setPhoneIsInProgress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_is_in_progress'] ) : $arrValues['phone_is_in_progress'] );
		if( isset( $arrValues['phone_notes'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNotes', trim( $arrValues['phone_notes'] ) ); elseif( isset( $arrValues['phone_notes'] ) ) $this->setPhoneNotes( $arrValues['phone_notes'] );
		if( isset( $arrValues['is_system_document'] ) && $boolDirectSet ) $this->set( 'm_strIsSystemDocument', trim( $arrValues['is_system_document'] ) ); elseif( isset( $arrValues['is_system_document'] ) ) $this->setIsSystemDocument( $arrValues['is_system_document'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityProviderId( $intUtilityProviderId ) {
		$this->set( 'm_intUtilityProviderId', CStrings::strToIntDef( $intUtilityProviderId, NULL, false ) );
	}

	public function getUtilityProviderId() {
		return $this->m_intUtilityProviderId;
	}

	public function sqlUtilityProviderId() {
		return ( true == isset( $this->m_intUtilityProviderId ) ) ? ( string ) $this->m_intUtilityProviderId : 'NULL';
	}

	public function setUtilityContactTypeId( $intUtilityContactTypeId ) {
		$this->set( 'm_intUtilityContactTypeId', CStrings::strToIntDef( $intUtilityContactTypeId, NULL, false ) );
	}

	public function getUtilityContactTypeId() {
		return $this->m_intUtilityContactTypeId;
	}

	public function sqlUtilityContactTypeId() {
		return ( true == isset( $this->m_intUtilityContactTypeId ) ) ? ( string ) $this->m_intUtilityContactTypeId : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setUtilityDocumentId( $intUtilityDocumentId ) {
		$this->set( 'm_intUtilityDocumentId', CStrings::strToIntDef( $intUtilityDocumentId, NULL, false ) );
	}

	public function getUtilityDocumentId() {
		return $this->m_intUtilityDocumentId;
	}

	public function sqlUtilityDocumentId() {
		return ( true == isset( $this->m_intUtilityDocumentId ) ) ? ( string ) $this->m_intUtilityDocumentId : 'NULL';
	}

	public function setEventDatetime( $strEventDatetime ) {
		$this->set( 'm_strEventDatetime', CStrings::strTrimDef( $strEventDatetime, -1, NULL, true ) );
	}

	public function getEventDatetime() {
		return $this->m_strEventDatetime;
	}

	public function sqlEventDatetime() {
		return ( true == isset( $this->m_strEventDatetime ) ) ? '\'' . $this->m_strEventDatetime . '\'' : 'NULL';
	}

	public function setEmailToAddress( $strEmailToAddress ) {
		$this->set( 'm_strEmailToAddress', CStrings::strTrimDef( $strEmailToAddress, -1, NULL, true ) );
	}

	public function getEmailToAddress() {
		return $this->m_strEmailToAddress;
	}

	public function sqlEmailToAddress() {
		return ( true == isset( $this->m_strEmailToAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailToAddress ) : '\'' . addslashes( $this->m_strEmailToAddress ) . '\'' ) : 'NULL';
	}

	public function setEmailSubject( $strEmailSubject ) {
		$this->set( 'm_strEmailSubject', CStrings::strTrimDef( $strEmailSubject, -1, NULL, true ) );
	}

	public function getEmailSubject() {
		return $this->m_strEmailSubject;
	}

	public function sqlEmailSubject() {
		return ( true == isset( $this->m_strEmailSubject ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailSubject ) : '\'' . addslashes( $this->m_strEmailSubject ) . '\'' ) : 'NULL';
	}

	public function setEmailContent( $strEmailContent ) {
		$this->set( 'm_strEmailContent', CStrings::strTrimDef( $strEmailContent, -1, NULL, true ) );
	}

	public function getEmailContent() {
		return $this->m_strEmailContent;
	}

	public function sqlEmailContent() {
		return ( true == isset( $this->m_strEmailContent ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailContent ) : '\'' . addslashes( $this->m_strEmailContent ) . '\'' ) : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, -1, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFaxNumber ) : '\'' . addslashes( $this->m_strFaxNumber ) . '\'' ) : 'NULL';
	}

	public function setMailTo( $strMailTo ) {
		$this->set( 'm_strMailTo', CStrings::strTrimDef( $strMailTo, -1, NULL, true ) );
	}

	public function getMailTo() {
		return $this->m_strMailTo;
	}

	public function sqlMailTo() {
		return ( true == isset( $this->m_strMailTo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailTo ) : '\'' . addslashes( $this->m_strMailTo ) . '\'' ) : 'NULL';
	}

	public function setMailStreetLineOne( $strMailStreetLineOne ) {
		$this->set( 'm_strMailStreetLineOne', CStrings::strTrimDef( $strMailStreetLineOne, -1, NULL, true ) );
	}

	public function getMailStreetLineOne() {
		return $this->m_strMailStreetLineOne;
	}

	public function sqlMailStreetLineOne() {
		return ( true == isset( $this->m_strMailStreetLineOne ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailStreetLineOne ) : '\'' . addslashes( $this->m_strMailStreetLineOne ) . '\'' ) : 'NULL';
	}

	public function setMailStreetLineTwo( $strMailStreetLineTwo ) {
		$this->set( 'm_strMailStreetLineTwo', CStrings::strTrimDef( $strMailStreetLineTwo, -1, NULL, true ) );
	}

	public function getMailStreetLineTwo() {
		return $this->m_strMailStreetLineTwo;
	}

	public function sqlMailStreetLineTwo() {
		return ( true == isset( $this->m_strMailStreetLineTwo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailStreetLineTwo ) : '\'' . addslashes( $this->m_strMailStreetLineTwo ) . '\'' ) : 'NULL';
	}

	public function setMailCity( $strMailCity ) {
		$this->set( 'm_strMailCity', CStrings::strTrimDef( $strMailCity, -1, NULL, true ) );
	}

	public function getMailCity() {
		return $this->m_strMailCity;
	}

	public function sqlMailCity() {
		return ( true == isset( $this->m_strMailCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailCity ) : '\'' . addslashes( $this->m_strMailCity ) . '\'' ) : 'NULL';
	}

	public function setMailState( $strMailState ) {
		$this->set( 'm_strMailState', CStrings::strTrimDef( $strMailState, -1, NULL, true ) );
	}

	public function getMailState() {
		return $this->m_strMailState;
	}

	public function sqlMailState() {
		return ( true == isset( $this->m_strMailState ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailState ) : '\'' . addslashes( $this->m_strMailState ) . '\'' ) : 'NULL';
	}

	public function setMailPostalCode( $strMailPostalCode ) {
		$this->set( 'm_strMailPostalCode', CStrings::strTrimDef( $strMailPostalCode, -1, NULL, true ) );
	}

	public function getMailPostalCode() {
		return $this->m_strMailPostalCode;
	}

	public function sqlMailPostalCode() {
		return ( true == isset( $this->m_strMailPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMailPostalCode ) : '\'' . addslashes( $this->m_strMailPostalCode ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, -1, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setPhoneContact( $strPhoneContact ) {
		$this->set( 'm_strPhoneContact', CStrings::strTrimDef( $strPhoneContact, -1, NULL, true ) );
	}

	public function getPhoneContact() {
		return $this->m_strPhoneContact;
	}

	public function sqlPhoneContact() {
		return ( true == isset( $this->m_strPhoneContact ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneContact ) : '\'' . addslashes( $this->m_strPhoneContact ) . '\'' ) : 'NULL';
	}

	public function setPhoneIsInProgress( $boolPhoneIsInProgress ) {
		$this->set( 'm_boolPhoneIsInProgress', CStrings::strToBool( $boolPhoneIsInProgress ) );
	}

	public function getPhoneIsInProgress() {
		return $this->m_boolPhoneIsInProgress;
	}

	public function sqlPhoneIsInProgress() {
		return ( true == isset( $this->m_boolPhoneIsInProgress ) ) ? '\'' . ( true == ( bool ) $this->m_boolPhoneIsInProgress ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPhoneNotes( $strPhoneNotes ) {
		$this->set( 'm_strPhoneNotes', CStrings::strTrimDef( $strPhoneNotes, -1, NULL, true ) );
	}

	public function getPhoneNotes() {
		return $this->m_strPhoneNotes;
	}

	public function sqlPhoneNotes() {
		return ( true == isset( $this->m_strPhoneNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNotes ) : '\'' . addslashes( $this->m_strPhoneNotes ) . '\'' ) : 'NULL';
	}

	public function setIsSystemDocument( $strIsSystemDocument ) {
		$this->set( 'm_strIsSystemDocument', CStrings::strTrimDef( $strIsSystemDocument, -1, NULL, true ) );
	}

	public function getIsSystemDocument() {
		return $this->m_strIsSystemDocument;
	}

	public function sqlIsSystemDocument() {
		return ( true == isset( $this->m_strIsSystemDocument ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIsSystemDocument ) : '\'' . addslashes( $this->m_strIsSystemDocument ) . '\'' ) : '\'False\'';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, property_id, utility_provider_id, utility_contact_type_id, system_email_id, utility_document_id, event_datetime, email_to_address, email_subject, email_content, fax_number, mail_to, mail_street_line_one, mail_street_line_two, mail_city, mail_state, mail_postal_code, phone_number, phone_contact, phone_is_in_progress, phone_notes, is_system_document, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlUtilityProviderId() . ', ' .
		          $this->sqlUtilityContactTypeId() . ', ' .
		          $this->sqlSystemEmailId() . ', ' .
		          $this->sqlUtilityDocumentId() . ', ' .
		          $this->sqlEventDatetime() . ', ' .
		          $this->sqlEmailToAddress() . ', ' .
		          $this->sqlEmailSubject() . ', ' .
		          $this->sqlEmailContent() . ', ' .
		          $this->sqlFaxNumber() . ', ' .
		          $this->sqlMailTo() . ', ' .
		          $this->sqlMailStreetLineOne() . ', ' .
		          $this->sqlMailStreetLineTwo() . ', ' .
		          $this->sqlMailCity() . ', ' .
		          $this->sqlMailState() . ', ' .
		          $this->sqlMailPostalCode() . ', ' .
		          $this->sqlPhoneNumber() . ', ' .
		          $this->sqlPhoneContact() . ', ' .
		          $this->sqlPhoneIsInProgress() . ', ' .
		          $this->sqlPhoneNotes() . ', ' .
		          $this->sqlIsSystemDocument() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId(). ',' ; } elseif( true == array_key_exists( 'UtilityProviderId', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_contact_type_id = ' . $this->sqlUtilityContactTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityContactTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_contact_type_id = ' . $this->sqlUtilityContactTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId(). ',' ; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId(). ',' ; } elseif( true == array_key_exists( 'UtilityDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' utility_document_id = ' . $this->sqlUtilityDocumentId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_datetime = ' . $this->sqlEventDatetime(). ',' ; } elseif( true == array_key_exists( 'EventDatetime', $this->getChangedColumns() ) ) { $strSql .= ' event_datetime = ' . $this->sqlEventDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_to_address = ' . $this->sqlEmailToAddress(). ',' ; } elseif( true == array_key_exists( 'EmailToAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_to_address = ' . $this->sqlEmailToAddress() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_subject = ' . $this->sqlEmailSubject(). ',' ; } elseif( true == array_key_exists( 'EmailSubject', $this->getChangedColumns() ) ) { $strSql .= ' email_subject = ' . $this->sqlEmailSubject() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_content = ' . $this->sqlEmailContent(). ',' ; } elseif( true == array_key_exists( 'EmailContent', $this->getChangedColumns() ) ) { $strSql .= ' email_content = ' . $this->sqlEmailContent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber(). ',' ; } elseif( true == array_key_exists( 'FaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_to = ' . $this->sqlMailTo(). ',' ; } elseif( true == array_key_exists( 'MailTo', $this->getChangedColumns() ) ) { $strSql .= ' mail_to = ' . $this->sqlMailTo() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_street_line_one = ' . $this->sqlMailStreetLineOne(). ',' ; } elseif( true == array_key_exists( 'MailStreetLineOne', $this->getChangedColumns() ) ) { $strSql .= ' mail_street_line_one = ' . $this->sqlMailStreetLineOne() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_street_line_two = ' . $this->sqlMailStreetLineTwo(). ',' ; } elseif( true == array_key_exists( 'MailStreetLineTwo', $this->getChangedColumns() ) ) { $strSql .= ' mail_street_line_two = ' . $this->sqlMailStreetLineTwo() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_city = ' . $this->sqlMailCity(). ',' ; } elseif( true == array_key_exists( 'MailCity', $this->getChangedColumns() ) ) { $strSql .= ' mail_city = ' . $this->sqlMailCity() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_state = ' . $this->sqlMailState(). ',' ; } elseif( true == array_key_exists( 'MailState', $this->getChangedColumns() ) ) { $strSql .= ' mail_state = ' . $this->sqlMailState() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mail_postal_code = ' . $this->sqlMailPostalCode(). ',' ; } elseif( true == array_key_exists( 'MailPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' mail_postal_code = ' . $this->sqlMailPostalCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_contact = ' . $this->sqlPhoneContact(). ',' ; } elseif( true == array_key_exists( 'PhoneContact', $this->getChangedColumns() ) ) { $strSql .= ' phone_contact = ' . $this->sqlPhoneContact() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_is_in_progress = ' . $this->sqlPhoneIsInProgress(). ',' ; } elseif( true == array_key_exists( 'PhoneIsInProgress', $this->getChangedColumns() ) ) { $strSql .= ' phone_is_in_progress = ' . $this->sqlPhoneIsInProgress() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_notes = ' . $this->sqlPhoneNotes(). ',' ; } elseif( true == array_key_exists( 'PhoneNotes', $this->getChangedColumns() ) ) { $strSql .= ' phone_notes = ' . $this->sqlPhoneNotes() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system_document = ' . $this->sqlIsSystemDocument(). ',' ; } elseif( true == array_key_exists( 'IsSystemDocument', $this->getChangedColumns() ) ) { $strSql .= ' is_system_document = ' . $this->sqlIsSystemDocument() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'property_id' => $this->getPropertyId(),
			'utility_provider_id' => $this->getUtilityProviderId(),
			'utility_contact_type_id' => $this->getUtilityContactTypeId(),
			'system_email_id' => $this->getSystemEmailId(),
			'utility_document_id' => $this->getUtilityDocumentId(),
			'event_datetime' => $this->getEventDatetime(),
			'email_to_address' => $this->getEmailToAddress(),
			'email_subject' => $this->getEmailSubject(),
			'email_content' => $this->getEmailContent(),
			'fax_number' => $this->getFaxNumber(),
			'mail_to' => $this->getMailTo(),
			'mail_street_line_one' => $this->getMailStreetLineOne(),
			'mail_street_line_two' => $this->getMailStreetLineTwo(),
			'mail_city' => $this->getMailCity(),
			'mail_state' => $this->getMailState(),
			'mail_postal_code' => $this->getMailPostalCode(),
			'phone_number' => $this->getPhoneNumber(),
			'phone_contact' => $this->getPhoneContact(),
			'phone_is_in_progress' => $this->getPhoneIsInProgress(),
			'phone_notes' => $this->getPhoneNotes(),
			'is_system_document' => $this->getIsSystemDocument(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>