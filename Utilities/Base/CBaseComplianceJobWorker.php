<?php

class CBaseComplianceJobWorker extends CEosSingularBase {

	const TABLE_NAME = 'public.compliance_job_workers';

	protected $m_intId;
	protected $m_intVendorId;
	protected $m_intVendorInvitationId;
	protected $m_intComplianceJobId;
	protected $m_intWorkerId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['vendor_invitation_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorInvitationId', trim( $arrValues['vendor_invitation_id'] ) ); elseif( isset( $arrValues['vendor_invitation_id'] ) ) $this->setVendorInvitationId( $arrValues['vendor_invitation_id'] );
		if( isset( $arrValues['compliance_job_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceJobId', trim( $arrValues['compliance_job_id'] ) ); elseif( isset( $arrValues['compliance_job_id'] ) ) $this->setComplianceJobId( $arrValues['compliance_job_id'] );
		if( isset( $arrValues['worker_id'] ) && $boolDirectSet ) $this->set( 'm_intWorkerId', trim( $arrValues['worker_id'] ) ); elseif( isset( $arrValues['worker_id'] ) ) $this->setWorkerId( $arrValues['worker_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setVendorInvitationId( $intVendorInvitationId ) {
		$this->set( 'm_intVendorInvitationId', CStrings::strToIntDef( $intVendorInvitationId, NULL, false ) );
	}

	public function getVendorInvitationId() {
		return $this->m_intVendorInvitationId;
	}

	public function sqlVendorInvitationId() {
		return ( true == isset( $this->m_intVendorInvitationId ) ) ? ( string ) $this->m_intVendorInvitationId : 'NULL';
	}

	public function setComplianceJobId( $intComplianceJobId ) {
		$this->set( 'm_intComplianceJobId', CStrings::strToIntDef( $intComplianceJobId, NULL, false ) );
	}

	public function getComplianceJobId() {
		return $this->m_intComplianceJobId;
	}

	public function sqlComplianceJobId() {
		return ( true == isset( $this->m_intComplianceJobId ) ) ? ( string ) $this->m_intComplianceJobId : 'NULL';
	}

	public function setWorkerId( $intWorkerId ) {
		$this->set( 'm_intWorkerId', CStrings::strToIntDef( $intWorkerId, NULL, false ) );
	}

	public function getWorkerId() {
		return $this->m_intWorkerId;
	}

	public function sqlWorkerId() {
		return ( true == isset( $this->m_intWorkerId ) ) ? ( string ) $this->m_intWorkerId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_id, vendor_invitation_id, compliance_job_id, worker_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlVendorId() . ', ' .
 						$this->sqlVendorInvitationId() . ', ' .
 						$this->sqlComplianceJobId() . ', ' .
 						$this->sqlWorkerId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_invitation_id = ' . $this->sqlVendorInvitationId() . ','; } elseif( true == array_key_exists( 'VendorInvitationId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_invitation_id = ' . $this->sqlVendorInvitationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_job_id = ' . $this->sqlComplianceJobId() . ','; } elseif( true == array_key_exists( 'ComplianceJobId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_job_id = ' . $this->sqlComplianceJobId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' worker_id = ' . $this->sqlWorkerId() . ','; } elseif( true == array_key_exists( 'WorkerId', $this->getChangedColumns() ) ) { $strSql .= ' worker_id = ' . $this->sqlWorkerId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_id' => $this->getVendorId(),
			'vendor_invitation_id' => $this->getVendorInvitationId(),
			'compliance_job_id' => $this->getComplianceJobId(),
			'worker_id' => $this->getWorkerId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>