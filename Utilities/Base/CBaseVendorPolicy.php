<?php

class CBaseVendorPolicy extends CEosSingularBase {

	const TABLE_NAME = 'public.vendor_policies';

	protected $m_intId;
	protected $m_intVendorId;
	protected $m_intBrokerId;
	protected $m_intComplianceItemId;
	protected $m_strPolicyDescription;
	protected $m_strPolicyNumber;
	protected $m_strExpirationDate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intVendorEntityId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['broker_id'] ) && $boolDirectSet ) $this->set( 'm_intBrokerId', trim( $arrValues['broker_id'] ) ); elseif( isset( $arrValues['broker_id'] ) ) $this->setBrokerId( $arrValues['broker_id'] );
		if( isset( $arrValues['compliance_item_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceItemId', trim( $arrValues['compliance_item_id'] ) ); elseif( isset( $arrValues['compliance_item_id'] ) ) $this->setComplianceItemId( $arrValues['compliance_item_id'] );
		if( isset( $arrValues['policy_description'] ) && $boolDirectSet ) $this->set( 'm_strPolicyDescription', trim( stripcslashes( $arrValues['policy_description'] ) ) ); elseif( isset( $arrValues['policy_description'] ) ) $this->setPolicyDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['policy_description'] ) : $arrValues['policy_description'] );
		if( isset( $arrValues['policy_number'] ) && $boolDirectSet ) $this->set( 'm_strPolicyNumber', trim( stripcslashes( $arrValues['policy_number'] ) ) ); elseif( isset( $arrValues['policy_number'] ) ) $this->setPolicyNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['policy_number'] ) : $arrValues['policy_number'] );
		if( isset( $arrValues['expiration_date'] ) && $boolDirectSet ) $this->set( 'm_strExpirationDate', trim( $arrValues['expiration_date'] ) ); elseif( isset( $arrValues['expiration_date'] ) ) $this->setExpirationDate( $arrValues['expiration_date'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['vendor_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorEntityId', trim( $arrValues['vendor_entity_id'] ) ); elseif( isset( $arrValues['vendor_entity_id'] ) ) $this->setVendorEntityId( $arrValues['vendor_entity_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setBrokerId( $intBrokerId ) {
		$this->set( 'm_intBrokerId', CStrings::strToIntDef( $intBrokerId, NULL, false ) );
	}

	public function getBrokerId() {
		return $this->m_intBrokerId;
	}

	public function sqlBrokerId() {
		return ( true == isset( $this->m_intBrokerId ) ) ? ( string ) $this->m_intBrokerId : 'NULL';
	}

	public function setComplianceItemId( $intComplianceItemId ) {
		$this->set( 'm_intComplianceItemId', CStrings::strToIntDef( $intComplianceItemId, NULL, false ) );
	}

	public function getComplianceItemId() {
		return $this->m_intComplianceItemId;
	}

	public function sqlComplianceItemId() {
		return ( true == isset( $this->m_intComplianceItemId ) ) ? ( string ) $this->m_intComplianceItemId : 'NULL';
	}

	public function setPolicyDescription( $strPolicyDescription ) {
		$this->set( 'm_strPolicyDescription', CStrings::strTrimDef( $strPolicyDescription, 240, NULL, true ) );
	}

	public function getPolicyDescription() {
		return $this->m_strPolicyDescription;
	}

	public function sqlPolicyDescription() {
		return ( true == isset( $this->m_strPolicyDescription ) ) ? '\'' . addslashes( $this->m_strPolicyDescription ) . '\'' : 'NULL';
	}

	public function setPolicyNumber( $strPolicyNumber ) {
		$this->set( 'm_strPolicyNumber', CStrings::strTrimDef( $strPolicyNumber, 240, NULL, true ) );
	}

	public function getPolicyNumber() {
		return $this->m_strPolicyNumber;
	}

	public function sqlPolicyNumber() {
		return ( true == isset( $this->m_strPolicyNumber ) ) ? '\'' . addslashes( $this->m_strPolicyNumber ) . '\'' : 'NULL';
	}

	public function setExpirationDate( $strExpirationDate ) {
		$this->set( 'm_strExpirationDate', CStrings::strTrimDef( $strExpirationDate, -1, NULL, true ) );
	}

	public function getExpirationDate() {
		return $this->m_strExpirationDate;
	}

	public function sqlExpirationDate() {
		return ( true == isset( $this->m_strExpirationDate ) ) ? '\'' . $this->m_strExpirationDate . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setVendorEntityId( $intVendorEntityId ) {
		$this->set( 'm_intVendorEntityId', CStrings::strToIntDef( $intVendorEntityId, NULL, false ) );
	}

	public function getVendorEntityId() {
		return $this->m_intVendorEntityId;
	}

	public function sqlVendorEntityId() {
		return ( true == isset( $this->m_intVendorEntityId ) ) ? ( string ) $this->m_intVendorEntityId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_id, broker_id, compliance_item_id, policy_description, policy_number, expiration_date, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, vendor_entity_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlVendorId() . ', ' .
 						$this->sqlBrokerId() . ', ' .
 						$this->sqlComplianceItemId() . ', ' .
 						$this->sqlPolicyDescription() . ', ' .
 						$this->sqlPolicyNumber() . ', ' .
 						$this->sqlExpirationDate() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlVendorEntityId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' broker_id = ' . $this->sqlBrokerId() . ','; } elseif( true == array_key_exists( 'BrokerId', $this->getChangedColumns() ) ) { $strSql .= ' broker_id = ' . $this->sqlBrokerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_item_id = ' . $this->sqlComplianceItemId() . ','; } elseif( true == array_key_exists( 'ComplianceItemId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_item_id = ' . $this->sqlComplianceItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_description = ' . $this->sqlPolicyDescription() . ','; } elseif( true == array_key_exists( 'PolicyDescription', $this->getChangedColumns() ) ) { $strSql .= ' policy_description = ' . $this->sqlPolicyDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_number = ' . $this->sqlPolicyNumber() . ','; } elseif( true == array_key_exists( 'PolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' policy_number = ' . $this->sqlPolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiration_date = ' . $this->sqlExpirationDate() . ','; } elseif( true == array_key_exists( 'ExpirationDate', $this->getChangedColumns() ) ) { $strSql .= ' expiration_date = ' . $this->sqlExpirationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_entity_id = ' . $this->sqlVendorEntityId() . ','; } elseif( true == array_key_exists( 'VendorEntityId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_entity_id = ' . $this->sqlVendorEntityId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_id' => $this->getVendorId(),
			'broker_id' => $this->getBrokerId(),
			'compliance_item_id' => $this->getComplianceItemId(),
			'policy_description' => $this->getPolicyDescription(),
			'policy_number' => $this->getPolicyNumber(),
			'expiration_date' => $this->getExpirationDate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'vendor_entity_id' => $this->getVendorEntityId()
		);
	}

}
?>