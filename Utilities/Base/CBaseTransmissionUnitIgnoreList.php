<?php

class CBaseTransmissionUnitIgnoreList extends CEosSingularBase {

	const TABLE_NAME = 'public.transmission_unit_ignore_lists';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intUtilityTransmissionId;
	protected $m_intUtilityTransmissionDetailId;
	protected $m_strUnitNumber;
	protected $m_strIgnoredDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['utility_transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTransmissionId', trim( $arrValues['utility_transmission_id'] ) ); elseif( isset( $arrValues['utility_transmission_id'] ) ) $this->setUtilityTransmissionId( $arrValues['utility_transmission_id'] );
		if( isset( $arrValues['utility_transmission_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTransmissionDetailId', trim( $arrValues['utility_transmission_detail_id'] ) ); elseif( isset( $arrValues['utility_transmission_detail_id'] ) ) $this->setUtilityTransmissionDetailId( $arrValues['utility_transmission_detail_id'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['ignored_datetime'] ) && $boolDirectSet ) $this->set( 'm_strIgnoredDatetime', trim( $arrValues['ignored_datetime'] ) ); elseif( isset( $arrValues['ignored_datetime'] ) ) $this->setIgnoredDatetime( $arrValues['ignored_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUtilityTransmissionId( $intUtilityTransmissionId ) {
		$this->set( 'm_intUtilityTransmissionId', CStrings::strToIntDef( $intUtilityTransmissionId, NULL, false ) );
	}

	public function getUtilityTransmissionId() {
		return $this->m_intUtilityTransmissionId;
	}

	public function sqlUtilityTransmissionId() {
		return ( true == isset( $this->m_intUtilityTransmissionId ) ) ? ( string ) $this->m_intUtilityTransmissionId : 'NULL';
	}

	public function setUtilityTransmissionDetailId( $intUtilityTransmissionDetailId ) {
		$this->set( 'm_intUtilityTransmissionDetailId', CStrings::strToIntDef( $intUtilityTransmissionDetailId, NULL, false ) );
	}

	public function getUtilityTransmissionDetailId() {
		return $this->m_intUtilityTransmissionDetailId;
	}

	public function sqlUtilityTransmissionDetailId() {
		return ( true == isset( $this->m_intUtilityTransmissionDetailId ) ) ? ( string ) $this->m_intUtilityTransmissionDetailId : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setIgnoredDatetime( $strIgnoredDatetime ) {
		$this->set( 'm_strIgnoredDatetime', CStrings::strTrimDef( $strIgnoredDatetime, -1, NULL, true ) );
	}

	public function getIgnoredDatetime() {
		return $this->m_strIgnoredDatetime;
	}

	public function sqlIgnoredDatetime() {
		return ( true == isset( $this->m_strIgnoredDatetime ) ) ? '\'' . $this->m_strIgnoredDatetime . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, utility_transmission_id, utility_transmission_detail_id, unit_number, ignored_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlUtilityTransmissionId() . ', ' .
 						$this->sqlUtilityTransmissionDetailId() . ', ' .
 						$this->sqlUnitNumber() . ', ' .
 						$this->sqlIgnoredDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_transmission_id = ' . $this->sqlUtilityTransmissionId() . ','; } elseif( true == array_key_exists( 'UtilityTransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_transmission_id = ' . $this->sqlUtilityTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_transmission_detail_id = ' . $this->sqlUtilityTransmissionDetailId() . ','; } elseif( true == array_key_exists( 'UtilityTransmissionDetailId', $this->getChangedColumns() ) ) { $strSql .= ' utility_transmission_detail_id = ' . $this->sqlUtilityTransmissionDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignored_datetime = ' . $this->sqlIgnoredDatetime() . ','; } elseif( true == array_key_exists( 'IgnoredDatetime', $this->getChangedColumns() ) ) { $strSql .= ' ignored_datetime = ' . $this->sqlIgnoredDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'utility_transmission_id' => $this->getUtilityTransmissionId(),
			'utility_transmission_detail_id' => $this->getUtilityTransmissionDetailId(),
			'unit_number' => $this->getUnitNumber(),
			'ignored_datetime' => $this->getIgnoredDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>