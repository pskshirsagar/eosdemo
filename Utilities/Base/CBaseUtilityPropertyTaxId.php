<?php

class CBaseUtilityPropertyTaxId extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_property_tax_ids';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strTaxIdEncrypted;
	protected $m_strNotes;
	protected $m_boolIsFromMerchantAccount;
	protected $m_boolIsFromEntrata;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsPrimary;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';
		$this->m_boolIsPrimary = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['tax_id_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxIdEncrypted', trim( $arrValues['tax_id_encrypted'] ) ); elseif( isset( $arrValues['tax_id_encrypted'] ) ) $this->setTaxIdEncrypted( $arrValues['tax_id_encrypted'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['is_from_merchant_account'] ) && $boolDirectSet ) $this->set( 'm_boolIsFromMerchantAccount', trim( stripcslashes( $arrValues['is_from_merchant_account'] ) ) ); elseif( isset( $arrValues['is_from_merchant_account'] ) ) $this->setIsFromMerchantAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_from_merchant_account'] ) : $arrValues['is_from_merchant_account'] );
		if( isset( $arrValues['is_from_entrata'] ) && $boolDirectSet ) $this->set( 'm_boolIsFromEntrata', trim( stripcslashes( $arrValues['is_from_entrata'] ) ) ); elseif( isset( $arrValues['is_from_entrata'] ) ) $this->setIsFromEntrata( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_from_entrata'] ) : $arrValues['is_from_entrata'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_primary'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrimary', trim( stripcslashes( $arrValues['is_primary'] ) ) ); elseif( isset( $arrValues['is_primary'] ) ) $this->setIsPrimary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_primary'] ) : $arrValues['is_primary'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setTaxIdEncrypted( $strTaxIdEncrypted ) {
		$this->set( 'm_strTaxIdEncrypted', CStrings::strTrimDef( $strTaxIdEncrypted, -1, NULL, true ) );
	}

	public function getTaxIdEncrypted() {
		return $this->m_strTaxIdEncrypted;
	}

	public function sqlTaxIdEncrypted() {
		return ( true == isset( $this->m_strTaxIdEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxIdEncrypted ) : '\'' . addslashes( $this->m_strTaxIdEncrypted ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setIsFromMerchantAccount( $boolIsFromMerchantAccount ) {
		$this->set( 'm_boolIsFromMerchantAccount', CStrings::strToBool( $boolIsFromMerchantAccount ) );
	}

	public function getIsFromMerchantAccount() {
		return $this->m_boolIsFromMerchantAccount;
	}

	public function sqlIsFromMerchantAccount() {
		return ( true == isset( $this->m_boolIsFromMerchantAccount ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFromMerchantAccount ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFromEntrata( $boolIsFromEntrata ) {
		$this->set( 'm_boolIsFromEntrata', CStrings::strToBool( $boolIsFromEntrata ) );
	}

	public function getIsFromEntrata() {
		return $this->m_boolIsFromEntrata;
	}

	public function sqlIsFromEntrata() {
		return ( true == isset( $this->m_boolIsFromEntrata ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFromEntrata ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsPrimary( $boolIsPrimary ) {
		$this->set( 'm_boolIsPrimary', CStrings::strToBool( $boolIsPrimary ) );
	}

	public function getIsPrimary() {
		return $this->m_boolIsPrimary;
	}

	public function sqlIsPrimary() {
		return ( true == isset( $this->m_boolIsPrimary ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrimary ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, tax_id_encrypted, notes, is_from_merchant_account, is_from_entrata, updated_by, updated_on, created_by, created_on, is_primary )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlTaxIdEncrypted() . ', ' .
		          $this->sqlNotes() . ', ' .
		          $this->sqlIsFromMerchantAccount() . ', ' .
		          $this->sqlIsFromEntrata() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlIsPrimary() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_id_encrypted = ' . $this->sqlTaxIdEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxIdEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_id_encrypted = ' . $this->sqlTaxIdEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_from_merchant_account = ' . $this->sqlIsFromMerchantAccount(). ',' ; } elseif( true == array_key_exists( 'IsFromMerchantAccount', $this->getChangedColumns() ) ) { $strSql .= ' is_from_merchant_account = ' . $this->sqlIsFromMerchantAccount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_from_entrata = ' . $this->sqlIsFromEntrata(). ',' ; } elseif( true == array_key_exists( 'IsFromEntrata', $this->getChangedColumns() ) ) { $strSql .= ' is_from_entrata = ' . $this->sqlIsFromEntrata() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary(). ',' ; } elseif( true == array_key_exists( 'IsPrimary', $this->getChangedColumns() ) ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'tax_id_encrypted' => $this->getTaxIdEncrypted(),
			'notes' => $this->getNotes(),
			'is_from_merchant_account' => $this->getIsFromMerchantAccount(),
			'is_from_entrata' => $this->getIsFromEntrata(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_primary' => $this->getIsPrimary()
		);
	}

}
?>