<?php

class CBaseUtilityTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_transactions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityTransactionTypeId;
	protected $m_intUtilityAccountId;
	protected $m_intOriginalUtilityTransactionId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intPropertyUtilityTypeRateId;
	protected $m_intPropertyUtilityTypeRateMethodId;
	protected $m_intUtilityTypeId;
	protected $m_intUtilityBatchId;
	protected $m_intUtilityTypeBatchId;
	protected $m_intUtilityBillChargeId;
	protected $m_intArCodeId;
	protected $m_intArPaymentId;
	protected $m_intUtilityInvoiceId;
	protected $m_intUtilityRubsFormulaId;
	protected $m_intTransactionId;
	protected $m_intArTransactionId;
	protected $m_intReconUtilityTransactionId;
	protected $m_intTaxUtilityTransactionId;
	protected $m_intInvoiceTransactionId;
	protected $m_intUtilityConsumptionTypeId;
	protected $m_intScheduledChargeId;
	protected $m_intCustomResidentChargeId;
	protected $m_strRemotePrimaryKey;
	protected $m_fltUnitsConsumed;
	protected $m_strChargeName;
	protected $m_strChargeDescription;
	protected $m_strTransactionDatetime;
	protected $m_strScheduledExportDate;
	protected $m_fltOriginalAmount;
	protected $m_fltActualAmount;
	protected $m_fltCurrentAmount;
	protected $m_fltUnadjustedAmount;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_fltMeterStartRead;
	protected $m_fltMeterEndRead;
	protected $m_strMemo;
	protected $m_fltUsageFactor;
	protected $m_boolIsOriginal;
	protected $m_boolIsManual;
	protected $m_boolIsMoveOut;
	protected $m_boolIsTax;
	protected $m_boolIsCorrected;
	protected $m_fltBtuFactor;
	protected $m_intBaselineAllowance;
	protected $m_strPostMonth;
	protected $m_strAdjustedOn;
	protected $m_intRejectedBy;
	protected $m_strRejectedOn;
	protected $m_intExportedBy;
	protected $m_strExportedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltTotalSubsidyAmount;
	protected $m_strUtilityProviderName;
	protected $m_strUtilityCalculationType;
	protected $m_intPropertyVcrTypeRateId;
	protected $m_intVcrUtilityBillId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsOriginal = false;
		$this->m_boolIsManual = false;
		$this->m_boolIsMoveOut = false;
		$this->m_boolIsTax = false;
		$this->m_boolIsCorrected = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTransactionTypeId', trim( $arrValues['utility_transaction_type_id'] ) ); elseif( isset( $arrValues['utility_transaction_type_id'] ) ) $this->setUtilityTransactionTypeId( $arrValues['utility_transaction_type_id'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['original_utility_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalUtilityTransactionId', trim( $arrValues['original_utility_transaction_id'] ) ); elseif( isset( $arrValues['original_utility_transaction_id'] ) ) $this->setOriginalUtilityTransactionId( $arrValues['original_utility_transaction_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['property_utility_type_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeRateId', trim( $arrValues['property_utility_type_rate_id'] ) ); elseif( isset( $arrValues['property_utility_type_rate_id'] ) ) $this->setPropertyUtilityTypeRateId( $arrValues['property_utility_type_rate_id'] );
		if( isset( $arrValues['property_utility_type_rate_method_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeRateMethodId', trim( $arrValues['property_utility_type_rate_method_id'] ) ); elseif( isset( $arrValues['property_utility_type_rate_method_id'] ) ) $this->setPropertyUtilityTypeRateMethodId( $arrValues['property_utility_type_rate_method_id'] );
		if( isset( $arrValues['utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeId', trim( $arrValues['utility_type_id'] ) ); elseif( isset( $arrValues['utility_type_id'] ) ) $this->setUtilityTypeId( $arrValues['utility_type_id'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['utility_type_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeBatchId', trim( $arrValues['utility_type_batch_id'] ) ); elseif( isset( $arrValues['utility_type_batch_id'] ) ) $this->setUtilityTypeBatchId( $arrValues['utility_type_batch_id'] );
		if( isset( $arrValues['utility_bill_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillChargeId', trim( $arrValues['utility_bill_charge_id'] ) ); elseif( isset( $arrValues['utility_bill_charge_id'] ) ) $this->setUtilityBillChargeId( $arrValues['utility_bill_charge_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['utility_invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityInvoiceId', trim( $arrValues['utility_invoice_id'] ) ); elseif( isset( $arrValues['utility_invoice_id'] ) ) $this->setUtilityInvoiceId( $arrValues['utility_invoice_id'] );
		if( isset( $arrValues['utility_rubs_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityRubsFormulaId', trim( $arrValues['utility_rubs_formula_id'] ) ); elseif( isset( $arrValues['utility_rubs_formula_id'] ) ) $this->setUtilityRubsFormulaId( $arrValues['utility_rubs_formula_id'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['recon_utility_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intReconUtilityTransactionId', trim( $arrValues['recon_utility_transaction_id'] ) ); elseif( isset( $arrValues['recon_utility_transaction_id'] ) ) $this->setReconUtilityTransactionId( $arrValues['recon_utility_transaction_id'] );
		if( isset( $arrValues['tax_utility_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxUtilityTransactionId', trim( $arrValues['tax_utility_transaction_id'] ) ); elseif( isset( $arrValues['tax_utility_transaction_id'] ) ) $this->setTaxUtilityTransactionId( $arrValues['tax_utility_transaction_id'] );
		if( isset( $arrValues['invoice_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceTransactionId', trim( $arrValues['invoice_transaction_id'] ) ); elseif( isset( $arrValues['invoice_transaction_id'] ) ) $this->setInvoiceTransactionId( $arrValues['invoice_transaction_id'] );
		if( isset( $arrValues['utility_consumption_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityConsumptionTypeId', trim( $arrValues['utility_consumption_type_id'] ) ); elseif( isset( $arrValues['utility_consumption_type_id'] ) ) $this->setUtilityConsumptionTypeId( $arrValues['utility_consumption_type_id'] );
		if( isset( $arrValues['scheduled_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeId', trim( $arrValues['scheduled_charge_id'] ) ); elseif( isset( $arrValues['scheduled_charge_id'] ) ) $this->setScheduledChargeId( $arrValues['scheduled_charge_id'] );
		if( isset( $arrValues['custom_resident_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomResidentChargeId', trim( $arrValues['custom_resident_charge_id'] ) ); elseif( isset( $arrValues['custom_resident_charge_id'] ) ) $this->setCustomResidentChargeId( $arrValues['custom_resident_charge_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['units_consumed'] ) && $boolDirectSet ) $this->set( 'm_fltUnitsConsumed', trim( $arrValues['units_consumed'] ) ); elseif( isset( $arrValues['units_consumed'] ) ) $this->setUnitsConsumed( $arrValues['units_consumed'] );
		if( isset( $arrValues['charge_name'] ) && $boolDirectSet ) $this->set( 'm_strChargeName', trim( stripcslashes( $arrValues['charge_name'] ) ) ); elseif( isset( $arrValues['charge_name'] ) ) $this->setChargeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_name'] ) : $arrValues['charge_name'] );
		if( isset( $arrValues['charge_description'] ) && $boolDirectSet ) $this->set( 'm_strChargeDescription', trim( stripcslashes( $arrValues['charge_description'] ) ) ); elseif( isset( $arrValues['charge_description'] ) ) $this->setChargeDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_description'] ) : $arrValues['charge_description'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['scheduled_export_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledExportDate', trim( $arrValues['scheduled_export_date'] ) ); elseif( isset( $arrValues['scheduled_export_date'] ) ) $this->setScheduledExportDate( $arrValues['scheduled_export_date'] );
		if( isset( $arrValues['original_amount'] ) && $boolDirectSet ) $this->set( 'm_fltOriginalAmount', trim( $arrValues['original_amount'] ) ); elseif( isset( $arrValues['original_amount'] ) ) $this->setOriginalAmount( $arrValues['original_amount'] );
		if( isset( $arrValues['actual_amount'] ) && $boolDirectSet ) $this->set( 'm_fltActualAmount', trim( $arrValues['actual_amount'] ) ); elseif( isset( $arrValues['actual_amount'] ) ) $this->setActualAmount( $arrValues['actual_amount'] );
		if( isset( $arrValues['current_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCurrentAmount', trim( $arrValues['current_amount'] ) ); elseif( isset( $arrValues['current_amount'] ) ) $this->setCurrentAmount( $arrValues['current_amount'] );
		if( isset( $arrValues['unadjusted_amount'] ) && $boolDirectSet ) $this->set( 'm_fltUnadjustedAmount', trim( $arrValues['unadjusted_amount'] ) ); elseif( isset( $arrValues['unadjusted_amount'] ) ) $this->setUnadjustedAmount( $arrValues['unadjusted_amount'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['meter_start_read'] ) && $boolDirectSet ) $this->set( 'm_fltMeterStartRead', trim( $arrValues['meter_start_read'] ) ); elseif( isset( $arrValues['meter_start_read'] ) ) $this->setMeterStartRead( $arrValues['meter_start_read'] );
		if( isset( $arrValues['meter_end_read'] ) && $boolDirectSet ) $this->set( 'm_fltMeterEndRead', trim( $arrValues['meter_end_read'] ) ); elseif( isset( $arrValues['meter_end_read'] ) ) $this->setMeterEndRead( $arrValues['meter_end_read'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['usage_factor'] ) && $boolDirectSet ) $this->set( 'm_fltUsageFactor', trim( $arrValues['usage_factor'] ) ); elseif( isset( $arrValues['usage_factor'] ) ) $this->setUsageFactor( $arrValues['usage_factor'] );
		if( isset( $arrValues['is_original'] ) && $boolDirectSet ) $this->set( 'm_boolIsOriginal', trim( stripcslashes( $arrValues['is_original'] ) ) ); elseif( isset( $arrValues['is_original'] ) ) $this->setIsOriginal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_original'] ) : $arrValues['is_original'] );
		if( isset( $arrValues['is_manual'] ) && $boolDirectSet ) $this->set( 'm_boolIsManual', trim( stripcslashes( $arrValues['is_manual'] ) ) ); elseif( isset( $arrValues['is_manual'] ) ) $this->setIsManual( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manual'] ) : $arrValues['is_manual'] );
		if( isset( $arrValues['is_move_out'] ) && $boolDirectSet ) $this->set( 'm_boolIsMoveOut', trim( stripcslashes( $arrValues['is_move_out'] ) ) ); elseif( isset( $arrValues['is_move_out'] ) ) $this->setIsMoveOut( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_move_out'] ) : $arrValues['is_move_out'] );
		if( isset( $arrValues['is_tax'] ) && $boolDirectSet ) $this->set( 'm_boolIsTax', trim( stripcslashes( $arrValues['is_tax'] ) ) ); elseif( isset( $arrValues['is_tax'] ) ) $this->setIsTax( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_tax'] ) : $arrValues['is_tax'] );
		if( isset( $arrValues['is_corrected'] ) && $boolDirectSet ) $this->set( 'm_boolIsCorrected', trim( stripcslashes( $arrValues['is_corrected'] ) ) ); elseif( isset( $arrValues['is_corrected'] ) ) $this->setIsCorrected( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_corrected'] ) : $arrValues['is_corrected'] );
		if( isset( $arrValues['btu_factor'] ) && $boolDirectSet ) $this->set( 'm_fltBtuFactor', trim( $arrValues['btu_factor'] ) ); elseif( isset( $arrValues['btu_factor'] ) ) $this->setBtuFactor( $arrValues['btu_factor'] );
		if( isset( $arrValues['baseline_allowance'] ) && $boolDirectSet ) $this->set( 'm_intBaselineAllowance', trim( $arrValues['baseline_allowance'] ) ); elseif( isset( $arrValues['baseline_allowance'] ) ) $this->setBaselineAllowance( $arrValues['baseline_allowance'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['adjusted_on'] ) && $boolDirectSet ) $this->set( 'm_strAdjustedOn', trim( $arrValues['adjusted_on'] ) ); elseif( isset( $arrValues['adjusted_on'] ) ) $this->setAdjustedOn( $arrValues['adjusted_on'] );
		if( isset( $arrValues['rejected_by'] ) && $boolDirectSet ) $this->set( 'm_intRejectedBy', trim( $arrValues['rejected_by'] ) ); elseif( isset( $arrValues['rejected_by'] ) ) $this->setRejectedBy( $arrValues['rejected_by'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['total_subsidy_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalSubsidyAmount', trim( $arrValues['total_subsidy_amount'] ) ); elseif( isset( $arrValues['total_subsidy_amount'] ) ) $this->setTotalSubsidyAmount( $arrValues['total_subsidy_amount'] );
		if( isset( $arrValues['utility_provider_name'] ) && $boolDirectSet ) $this->set( 'm_strUtilityProviderName', trim( stripcslashes( $arrValues['utility_provider_name'] ) ) ); elseif( isset( $arrValues['utility_provider_name'] ) ) $this->setUtilityProviderName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['utility_provider_name'] ) : $arrValues['utility_provider_name'] );
		if( isset( $arrValues['utility_calculation_type'] ) && $boolDirectSet ) $this->set( 'm_strUtilityCalculationType', trim( stripcslashes( $arrValues['utility_calculation_type'] ) ) ); elseif( isset( $arrValues['utility_calculation_type'] ) ) $this->setUtilityCalculationType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['utility_calculation_type'] ) : $arrValues['utility_calculation_type'] );
		if( isset( $arrValues['property_vcr_type_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyVcrTypeRateId', trim( $arrValues['property_vcr_type_rate_id'] ) ); elseif( isset( $arrValues['property_vcr_type_rate_id'] ) ) $this->setPropertyVcrTypeRateId( $arrValues['property_vcr_type_rate_id'] );
		if( isset( $arrValues['vcr_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intVcrUtilityBillId', trim( $arrValues['vcr_utility_bill_id'] ) ); elseif( isset( $arrValues['vcr_utility_bill_id'] ) ) $this->setVcrUtilityBillId( $arrValues['vcr_utility_bill_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityTransactionTypeId( $intUtilityTransactionTypeId ) {
		$this->set( 'm_intUtilityTransactionTypeId', CStrings::strToIntDef( $intUtilityTransactionTypeId, NULL, false ) );
	}

	public function getUtilityTransactionTypeId() {
		return $this->m_intUtilityTransactionTypeId;
	}

	public function sqlUtilityTransactionTypeId() {
		return ( true == isset( $this->m_intUtilityTransactionTypeId ) ) ? ( string ) $this->m_intUtilityTransactionTypeId : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setOriginalUtilityTransactionId( $intOriginalUtilityTransactionId ) {
		$this->set( 'm_intOriginalUtilityTransactionId', CStrings::strToIntDef( $intOriginalUtilityTransactionId, NULL, false ) );
	}

	public function getOriginalUtilityTransactionId() {
		return $this->m_intOriginalUtilityTransactionId;
	}

	public function sqlOriginalUtilityTransactionId() {
		return ( true == isset( $this->m_intOriginalUtilityTransactionId ) ) ? ( string ) $this->m_intOriginalUtilityTransactionId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setPropertyUtilityTypeRateId( $intPropertyUtilityTypeRateId ) {
		$this->set( 'm_intPropertyUtilityTypeRateId', CStrings::strToIntDef( $intPropertyUtilityTypeRateId, NULL, false ) );
	}

	public function getPropertyUtilityTypeRateId() {
		return $this->m_intPropertyUtilityTypeRateId;
	}

	public function sqlPropertyUtilityTypeRateId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeRateId ) ) ? ( string ) $this->m_intPropertyUtilityTypeRateId : 'NULL';
	}

	public function setPropertyUtilityTypeRateMethodId( $intPropertyUtilityTypeRateMethodId ) {
		$this->set( 'm_intPropertyUtilityTypeRateMethodId', CStrings::strToIntDef( $intPropertyUtilityTypeRateMethodId, NULL, false ) );
	}

	public function getPropertyUtilityTypeRateMethodId() {
		return $this->m_intPropertyUtilityTypeRateMethodId;
	}

	public function sqlPropertyUtilityTypeRateMethodId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeRateMethodId ) ) ? ( string ) $this->m_intPropertyUtilityTypeRateMethodId : 'NULL';
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->set( 'm_intUtilityTypeId', CStrings::strToIntDef( $intUtilityTypeId, NULL, false ) );
	}

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function sqlUtilityTypeId() {
		return ( true == isset( $this->m_intUtilityTypeId ) ) ? ( string ) $this->m_intUtilityTypeId : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setUtilityTypeBatchId( $intUtilityTypeBatchId ) {
		$this->set( 'm_intUtilityTypeBatchId', CStrings::strToIntDef( $intUtilityTypeBatchId, NULL, false ) );
	}

	public function getUtilityTypeBatchId() {
		return $this->m_intUtilityTypeBatchId;
	}

	public function sqlUtilityTypeBatchId() {
		return ( true == isset( $this->m_intUtilityTypeBatchId ) ) ? ( string ) $this->m_intUtilityTypeBatchId : 'NULL';
	}

	public function setUtilityBillChargeId( $intUtilityBillChargeId ) {
		$this->set( 'm_intUtilityBillChargeId', CStrings::strToIntDef( $intUtilityBillChargeId, NULL, false ) );
	}

	public function getUtilityBillChargeId() {
		return $this->m_intUtilityBillChargeId;
	}

	public function sqlUtilityBillChargeId() {
		return ( true == isset( $this->m_intUtilityBillChargeId ) ) ? ( string ) $this->m_intUtilityBillChargeId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setUtilityInvoiceId( $intUtilityInvoiceId ) {
		$this->set( 'm_intUtilityInvoiceId', CStrings::strToIntDef( $intUtilityInvoiceId, NULL, false ) );
	}

	public function getUtilityInvoiceId() {
		return $this->m_intUtilityInvoiceId;
	}

	public function sqlUtilityInvoiceId() {
		return ( true == isset( $this->m_intUtilityInvoiceId ) ) ? ( string ) $this->m_intUtilityInvoiceId : 'NULL';
	}

	public function setUtilityRubsFormulaId( $intUtilityRubsFormulaId ) {
		$this->set( 'm_intUtilityRubsFormulaId', CStrings::strToIntDef( $intUtilityRubsFormulaId, NULL, false ) );
	}

	public function getUtilityRubsFormulaId() {
		return $this->m_intUtilityRubsFormulaId;
	}

	public function sqlUtilityRubsFormulaId() {
		return ( true == isset( $this->m_intUtilityRubsFormulaId ) ) ? ( string ) $this->m_intUtilityRubsFormulaId : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setReconUtilityTransactionId( $intReconUtilityTransactionId ) {
		$this->set( 'm_intReconUtilityTransactionId', CStrings::strToIntDef( $intReconUtilityTransactionId, NULL, false ) );
	}

	public function getReconUtilityTransactionId() {
		return $this->m_intReconUtilityTransactionId;
	}

	public function sqlReconUtilityTransactionId() {
		return ( true == isset( $this->m_intReconUtilityTransactionId ) ) ? ( string ) $this->m_intReconUtilityTransactionId : 'NULL';
	}

	public function setTaxUtilityTransactionId( $intTaxUtilityTransactionId ) {
		$this->set( 'm_intTaxUtilityTransactionId', CStrings::strToIntDef( $intTaxUtilityTransactionId, NULL, false ) );
	}

	public function getTaxUtilityTransactionId() {
		return $this->m_intTaxUtilityTransactionId;
	}

	public function sqlTaxUtilityTransactionId() {
		return ( true == isset( $this->m_intTaxUtilityTransactionId ) ) ? ( string ) $this->m_intTaxUtilityTransactionId : 'NULL';
	}

	public function setInvoiceTransactionId( $intInvoiceTransactionId ) {
		$this->set( 'm_intInvoiceTransactionId', CStrings::strToIntDef( $intInvoiceTransactionId, NULL, false ) );
	}

	public function getInvoiceTransactionId() {
		return $this->m_intInvoiceTransactionId;
	}

	public function sqlInvoiceTransactionId() {
		return ( true == isset( $this->m_intInvoiceTransactionId ) ) ? ( string ) $this->m_intInvoiceTransactionId : 'NULL';
	}

	public function setUtilityConsumptionTypeId( $intUtilityConsumptionTypeId ) {
		$this->set( 'm_intUtilityConsumptionTypeId', CStrings::strToIntDef( $intUtilityConsumptionTypeId, NULL, false ) );
	}

	public function getUtilityConsumptionTypeId() {
		return $this->m_intUtilityConsumptionTypeId;
	}

	public function sqlUtilityConsumptionTypeId() {
		return ( true == isset( $this->m_intUtilityConsumptionTypeId ) ) ? ( string ) $this->m_intUtilityConsumptionTypeId : 'NULL';
	}

	public function setScheduledChargeId( $intScheduledChargeId ) {
		$this->set( 'm_intScheduledChargeId', CStrings::strToIntDef( $intScheduledChargeId, NULL, false ) );
	}

	public function getScheduledChargeId() {
		return $this->m_intScheduledChargeId;
	}

	public function sqlScheduledChargeId() {
		return ( true == isset( $this->m_intScheduledChargeId ) ) ? ( string ) $this->m_intScheduledChargeId : 'NULL';
	}

	public function setCustomResidentChargeId( $intCustomResidentChargeId ) {
		$this->set( 'm_intCustomResidentChargeId', CStrings::strToIntDef( $intCustomResidentChargeId, NULL, false ) );
	}

	public function getCustomResidentChargeId() {
		return $this->m_intCustomResidentChargeId;
	}

	public function sqlCustomResidentChargeId() {
		return ( true == isset( $this->m_intCustomResidentChargeId ) ) ? ( string ) $this->m_intCustomResidentChargeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setUnitsConsumed( $fltUnitsConsumed ) {
		$this->set( 'm_fltUnitsConsumed', CStrings::strToFloatDef( $fltUnitsConsumed, NULL, false, 2 ) );
	}

	public function getUnitsConsumed() {
		return $this->m_fltUnitsConsumed;
	}

	public function sqlUnitsConsumed() {
		return ( true == isset( $this->m_fltUnitsConsumed ) ) ? ( string ) $this->m_fltUnitsConsumed : 'NULL';
	}

	public function setChargeName( $strChargeName ) {
		$this->set( 'm_strChargeName', CStrings::strTrimDef( $strChargeName, 50, NULL, true ) );
	}

	public function getChargeName() {
		return $this->m_strChargeName;
	}

	public function sqlChargeName() {
		return ( true == isset( $this->m_strChargeName ) ) ? '\'' . addslashes( $this->m_strChargeName ) . '\'' : 'NULL';
	}

	public function setChargeDescription( $strChargeDescription ) {
		$this->set( 'm_strChargeDescription', CStrings::strTrimDef( $strChargeDescription, 2000, NULL, true ) );
	}

	public function getChargeDescription() {
		return $this->m_strChargeDescription;
	}

	public function sqlChargeDescription() {
		return ( true == isset( $this->m_strChargeDescription ) ) ? '\'' . addslashes( $this->m_strChargeDescription ) . '\'' : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setScheduledExportDate( $strScheduledExportDate ) {
		$this->set( 'm_strScheduledExportDate', CStrings::strTrimDef( $strScheduledExportDate, -1, NULL, true ) );
	}

	public function getScheduledExportDate() {
		return $this->m_strScheduledExportDate;
	}

	public function sqlScheduledExportDate() {
		return ( true == isset( $this->m_strScheduledExportDate ) ) ? '\'' . $this->m_strScheduledExportDate . '\'' : 'NULL';
	}

	public function setOriginalAmount( $fltOriginalAmount ) {
		$this->set( 'm_fltOriginalAmount', CStrings::strToFloatDef( $fltOriginalAmount, NULL, false, 2 ) );
	}

	public function getOriginalAmount() {
		return $this->m_fltOriginalAmount;
	}

	public function sqlOriginalAmount() {
		return ( true == isset( $this->m_fltOriginalAmount ) ) ? ( string ) $this->m_fltOriginalAmount : 'NULL';
	}

	public function setActualAmount( $fltActualAmount ) {
		$this->set( 'm_fltActualAmount', CStrings::strToFloatDef( $fltActualAmount, NULL, false, 2 ) );
	}

	public function getActualAmount() {
		return $this->m_fltActualAmount;
	}

	public function sqlActualAmount() {
		return ( true == isset( $this->m_fltActualAmount ) ) ? ( string ) $this->m_fltActualAmount : 'NULL';
	}

	public function setCurrentAmount( $fltCurrentAmount ) {
		$this->set( 'm_fltCurrentAmount', CStrings::strToFloatDef( $fltCurrentAmount, NULL, false, 2 ) );
	}

	public function getCurrentAmount() {
		return $this->m_fltCurrentAmount;
	}

	public function sqlCurrentAmount() {
		return ( true == isset( $this->m_fltCurrentAmount ) ) ? ( string ) $this->m_fltCurrentAmount : 'NULL';
	}

	public function setUnadjustedAmount( $fltUnadjustedAmount ) {
		$this->set( 'm_fltUnadjustedAmount', CStrings::strToFloatDef( $fltUnadjustedAmount, NULL, false, 2 ) );
	}

	public function getUnadjustedAmount() {
		return $this->m_fltUnadjustedAmount;
	}

	public function sqlUnadjustedAmount() {
		return ( true == isset( $this->m_fltUnadjustedAmount ) ) ? ( string ) $this->m_fltUnadjustedAmount : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setMeterStartRead( $fltMeterStartRead ) {
		$this->set( 'm_fltMeterStartRead', CStrings::strToFloatDef( $fltMeterStartRead, NULL, false, 2 ) );
	}

	public function getMeterStartRead() {
		return $this->m_fltMeterStartRead;
	}

	public function sqlMeterStartRead() {
		return ( true == isset( $this->m_fltMeterStartRead ) ) ? ( string ) $this->m_fltMeterStartRead : 'NULL';
	}

	public function setMeterEndRead( $fltMeterEndRead ) {
		$this->set( 'm_fltMeterEndRead', CStrings::strToFloatDef( $fltMeterEndRead, NULL, false, 2 ) );
	}

	public function getMeterEndRead() {
		return $this->m_fltMeterEndRead;
	}

	public function sqlMeterEndRead() {
		return ( true == isset( $this->m_fltMeterEndRead ) ) ? ( string ) $this->m_fltMeterEndRead : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, -1, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setUsageFactor( $fltUsageFactor ) {
		$this->set( 'm_fltUsageFactor', CStrings::strToFloatDef( $fltUsageFactor, NULL, false, 2 ) );
	}

	public function getUsageFactor() {
		return $this->m_fltUsageFactor;
	}

	public function sqlUsageFactor() {
		return ( true == isset( $this->m_fltUsageFactor ) ) ? ( string ) $this->m_fltUsageFactor : 'NULL';
	}

	public function setIsOriginal( $boolIsOriginal ) {
		$this->set( 'm_boolIsOriginal', CStrings::strToBool( $boolIsOriginal ) );
	}

	public function getIsOriginal() {
		return $this->m_boolIsOriginal;
	}

	public function sqlIsOriginal() {
		return ( true == isset( $this->m_boolIsOriginal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOriginal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsManual( $boolIsManual ) {
		$this->set( 'm_boolIsManual', CStrings::strToBool( $boolIsManual ) );
	}

	public function getIsManual() {
		return $this->m_boolIsManual;
	}

	public function sqlIsManual() {
		return ( true == isset( $this->m_boolIsManual ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManual ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsMoveOut( $boolIsMoveOut ) {
		$this->set( 'm_boolIsMoveOut', CStrings::strToBool( $boolIsMoveOut ) );
	}

	public function getIsMoveOut() {
		return $this->m_boolIsMoveOut;
	}

	public function sqlIsMoveOut() {
		return ( true == isset( $this->m_boolIsMoveOut ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMoveOut ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTax( $boolIsTax ) {
		$this->set( 'm_boolIsTax', CStrings::strToBool( $boolIsTax ) );
	}

	public function getIsTax() {
		return $this->m_boolIsTax;
	}

	public function sqlIsTax() {
		return ( true == isset( $this->m_boolIsTax ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTax ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCorrected( $boolIsCorrected ) {
		$this->set( 'm_boolIsCorrected', CStrings::strToBool( $boolIsCorrected ) );
	}

	public function getIsCorrected() {
		return $this->m_boolIsCorrected;
	}

	public function sqlIsCorrected() {
		return ( true == isset( $this->m_boolIsCorrected ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCorrected ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBtuFactor( $fltBtuFactor ) {
		$this->set( 'm_fltBtuFactor', CStrings::strToFloatDef( $fltBtuFactor, NULL, false, 5 ) );
	}

	public function getBtuFactor() {
		return $this->m_fltBtuFactor;
	}

	public function sqlBtuFactor() {
		return ( true == isset( $this->m_fltBtuFactor ) ) ? ( string ) $this->m_fltBtuFactor : 'NULL';
	}

	public function setBaselineAllowance( $intBaselineAllowance ) {
		$this->set( 'm_intBaselineAllowance', CStrings::strToIntDef( $intBaselineAllowance, NULL, false ) );
	}

	public function getBaselineAllowance() {
		return $this->m_intBaselineAllowance;
	}

	public function sqlBaselineAllowance() {
		return ( true == isset( $this->m_intBaselineAllowance ) ) ? ( string ) $this->m_intBaselineAllowance : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NULL';
	}

	public function setAdjustedOn( $strAdjustedOn ) {
		$this->set( 'm_strAdjustedOn', CStrings::strTrimDef( $strAdjustedOn, -1, NULL, true ) );
	}

	public function getAdjustedOn() {
		return $this->m_strAdjustedOn;
	}

	public function sqlAdjustedOn() {
		return ( true == isset( $this->m_strAdjustedOn ) ) ? '\'' . $this->m_strAdjustedOn . '\'' : 'NULL';
	}

	public function setRejectedBy( $intRejectedBy ) {
		$this->set( 'm_intRejectedBy', CStrings::strToIntDef( $intRejectedBy, NULL, false ) );
	}

	public function getRejectedBy() {
		return $this->m_intRejectedBy;
	}

	public function sqlRejectedBy() {
		return ( true == isset( $this->m_intRejectedBy ) ) ? ( string ) $this->m_intRejectedBy : 'NULL';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTotalSubsidyAmount( $fltTotalSubsidyAmount ) {
		$this->set( 'm_fltTotalSubsidyAmount', CStrings::strToFloatDef( $fltTotalSubsidyAmount, NULL, false, 2 ) );
	}

	public function getTotalSubsidyAmount() {
		return $this->m_fltTotalSubsidyAmount;
	}

	public function sqlTotalSubsidyAmount() {
		return ( true == isset( $this->m_fltTotalSubsidyAmount ) ) ? ( string ) $this->m_fltTotalSubsidyAmount : 'NULL';
	}

	public function setUtilityProviderName( $strUtilityProviderName ) {
		$this->set( 'm_strUtilityProviderName', CStrings::strTrimDef( $strUtilityProviderName, 64, NULL, true ) );
	}

	public function getUtilityProviderName() {
		return $this->m_strUtilityProviderName;
	}

	public function sqlUtilityProviderName() {
		return ( true == isset( $this->m_strUtilityProviderName ) ) ? '\'' . addslashes( $this->m_strUtilityProviderName ) . '\'' : 'NULL';
	}

	public function setUtilityCalculationType( $strUtilityCalculationType ) {
		$this->set( 'm_strUtilityCalculationType', CStrings::strTrimDef( $strUtilityCalculationType, 5, NULL, true ) );
	}

	public function getUtilityCalculationType() {
		return $this->m_strUtilityCalculationType;
	}

	public function sqlUtilityCalculationType() {
		return ( true == isset( $this->m_strUtilityCalculationType ) ) ? '\'' . addslashes( $this->m_strUtilityCalculationType ) . '\'' : 'NULL';
	}

	public function setPropertyVcrTypeRateId( $intPropertyVcrTypeRateId ) {
		$this->set( 'm_intPropertyVcrTypeRateId', CStrings::strToIntDef( $intPropertyVcrTypeRateId, NULL, false ) );
	}

	public function getPropertyVcrTypeRateId() {
		return $this->m_intPropertyVcrTypeRateId;
	}

	public function sqlPropertyVcrTypeRateId() {
		return ( true == isset( $this->m_intPropertyVcrTypeRateId ) ) ? ( string ) $this->m_intPropertyVcrTypeRateId : 'NULL';
	}

	public function setVcrUtilityBillId( $intVcrUtilityBillId ) {
		$this->set( 'm_intVcrUtilityBillId', CStrings::strToIntDef( $intVcrUtilityBillId, NULL, false ) );
	}

	public function getVcrUtilityBillId() {
		return $this->m_intVcrUtilityBillId;
	}

	public function sqlVcrUtilityBillId() {
		return ( true == isset( $this->m_intVcrUtilityBillId ) ) ? ( string ) $this->m_intVcrUtilityBillId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_transaction_type_id, utility_account_id, original_utility_transaction_id, property_utility_type_id, property_utility_type_rate_id, property_utility_type_rate_method_id, utility_type_id, utility_batch_id, utility_type_batch_id, utility_bill_charge_id, ar_code_id, ar_payment_id, utility_invoice_id, utility_rubs_formula_id, transaction_id, ar_transaction_id, recon_utility_transaction_id, tax_utility_transaction_id, invoice_transaction_id, utility_consumption_type_id, scheduled_charge_id, custom_resident_charge_id, remote_primary_key, units_consumed, charge_name, charge_description, transaction_datetime, scheduled_export_date, original_amount, actual_amount, current_amount, unadjusted_amount, start_date, end_date, meter_start_read, meter_end_read, memo, usage_factor, is_original, is_manual, is_move_out, is_tax, is_corrected, btu_factor, baseline_allowance, post_month, adjusted_on, rejected_by, rejected_on, exported_by, exported_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, total_subsidy_amount, utility_provider_name, utility_calculation_type, property_vcr_type_rate_id, vcr_utility_bill_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityTransactionTypeId() . ', ' .
						$this->sqlUtilityAccountId() . ', ' .
						$this->sqlOriginalUtilityTransactionId() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlPropertyUtilityTypeRateId() . ', ' .
						$this->sqlPropertyUtilityTypeRateMethodId() . ', ' .
						$this->sqlUtilityTypeId() . ', ' .
						$this->sqlUtilityBatchId() . ', ' .
						$this->sqlUtilityTypeBatchId() . ', ' .
						$this->sqlUtilityBillChargeId() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlArPaymentId() . ', ' .
						$this->sqlUtilityInvoiceId() . ', ' .
						$this->sqlUtilityRubsFormulaId() . ', ' .
						$this->sqlTransactionId() . ', ' .
						$this->sqlArTransactionId() . ', ' .
						$this->sqlReconUtilityTransactionId() . ', ' .
						$this->sqlTaxUtilityTransactionId() . ', ' .
						$this->sqlInvoiceTransactionId() . ', ' .
						$this->sqlUtilityConsumptionTypeId() . ', ' .
						$this->sqlScheduledChargeId() . ', ' .
						$this->sqlCustomResidentChargeId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlUnitsConsumed() . ', ' .
						$this->sqlChargeName() . ', ' .
						$this->sqlChargeDescription() . ', ' .
						$this->sqlTransactionDatetime() . ', ' .
						$this->sqlScheduledExportDate() . ', ' .
						$this->sqlOriginalAmount() . ', ' .
						$this->sqlActualAmount() . ', ' .
						$this->sqlCurrentAmount() . ', ' .
						$this->sqlUnadjustedAmount() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlMeterStartRead() . ', ' .
						$this->sqlMeterEndRead() . ', ' .
						$this->sqlMemo() . ', ' .
						$this->sqlUsageFactor() . ', ' .
						$this->sqlIsOriginal() . ', ' .
						$this->sqlIsManual() . ', ' .
						$this->sqlIsMoveOut() . ', ' .
						$this->sqlIsTax() . ', ' .
						$this->sqlIsCorrected() . ', ' .
						$this->sqlBtuFactor() . ', ' .
						$this->sqlBaselineAllowance() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlAdjustedOn() . ', ' .
						$this->sqlRejectedBy() . ', ' .
						$this->sqlRejectedOn() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlTotalSubsidyAmount() . ', ' .
						$this->sqlUtilityProviderName() . ', ' .
						$this->sqlUtilityCalculationType() . ', ' .
						$this->sqlPropertyVcrTypeRateId() . ', ' .
						$this->sqlVcrUtilityBillId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_transaction_type_id = ' . $this->sqlUtilityTransactionTypeId() . ','; } elseif( true == array_key_exists( 'UtilityTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_transaction_type_id = ' . $this->sqlUtilityTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_utility_transaction_id = ' . $this->sqlOriginalUtilityTransactionId() . ','; } elseif( true == array_key_exists( 'OriginalUtilityTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' original_utility_transaction_id = ' . $this->sqlOriginalUtilityTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_rate_id = ' . $this->sqlPropertyUtilityTypeRateId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeRateId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_rate_id = ' . $this->sqlPropertyUtilityTypeRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_rate_method_id = ' . $this->sqlPropertyUtilityTypeRateMethodId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeRateMethodId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_rate_method_id = ' . $this->sqlPropertyUtilityTypeRateMethodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; } elseif( true == array_key_exists( 'UtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_batch_id = ' . $this->sqlUtilityTypeBatchId() . ','; } elseif( true == array_key_exists( 'UtilityTypeBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_batch_id = ' . $this->sqlUtilityTypeBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_charge_id = ' . $this->sqlUtilityBillChargeId() . ','; } elseif( true == array_key_exists( 'UtilityBillChargeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_charge_id = ' . $this->sqlUtilityBillChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_invoice_id = ' . $this->sqlUtilityInvoiceId() . ','; } elseif( true == array_key_exists( 'UtilityInvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' utility_invoice_id = ' . $this->sqlUtilityInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_rubs_formula_id = ' . $this->sqlUtilityRubsFormulaId() . ','; } elseif( true == array_key_exists( 'UtilityRubsFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' utility_rubs_formula_id = ' . $this->sqlUtilityRubsFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recon_utility_transaction_id = ' . $this->sqlReconUtilityTransactionId() . ','; } elseif( true == array_key_exists( 'ReconUtilityTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' recon_utility_transaction_id = ' . $this->sqlReconUtilityTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_utility_transaction_id = ' . $this->sqlTaxUtilityTransactionId() . ','; } elseif( true == array_key_exists( 'TaxUtilityTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' tax_utility_transaction_id = ' . $this->sqlTaxUtilityTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_transaction_id = ' . $this->sqlInvoiceTransactionId() . ','; } elseif( true == array_key_exists( 'InvoiceTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_transaction_id = ' . $this->sqlInvoiceTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_consumption_type_id = ' . $this->sqlUtilityConsumptionTypeId() . ','; } elseif( true == array_key_exists( 'UtilityConsumptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_consumption_type_id = ' . $this->sqlUtilityConsumptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_id = ' . $this->sqlScheduledChargeId() . ','; } elseif( true == array_key_exists( 'ScheduledChargeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_id = ' . $this->sqlScheduledChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_resident_charge_id = ' . $this->sqlCustomResidentChargeId() . ','; } elseif( true == array_key_exists( 'CustomResidentChargeId', $this->getChangedColumns() ) ) { $strSql .= ' custom_resident_charge_id = ' . $this->sqlCustomResidentChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' units_consumed = ' . $this->sqlUnitsConsumed() . ','; } elseif( true == array_key_exists( 'UnitsConsumed', $this->getChangedColumns() ) ) { $strSql .= ' units_consumed = ' . $this->sqlUnitsConsumed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_name = ' . $this->sqlChargeName() . ','; } elseif( true == array_key_exists( 'ChargeName', $this->getChangedColumns() ) ) { $strSql .= ' charge_name = ' . $this->sqlChargeName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_description = ' . $this->sqlChargeDescription() . ','; } elseif( true == array_key_exists( 'ChargeDescription', $this->getChangedColumns() ) ) { $strSql .= ' charge_description = ' . $this->sqlChargeDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_export_date = ' . $this->sqlScheduledExportDate() . ','; } elseif( true == array_key_exists( 'ScheduledExportDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_export_date = ' . $this->sqlScheduledExportDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_amount = ' . $this->sqlOriginalAmount() . ','; } elseif( true == array_key_exists( 'OriginalAmount', $this->getChangedColumns() ) ) { $strSql .= ' original_amount = ' . $this->sqlOriginalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_amount = ' . $this->sqlActualAmount() . ','; } elseif( true == array_key_exists( 'ActualAmount', $this->getChangedColumns() ) ) { $strSql .= ' actual_amount = ' . $this->sqlActualAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_amount = ' . $this->sqlCurrentAmount() . ','; } elseif( true == array_key_exists( 'CurrentAmount', $this->getChangedColumns() ) ) { $strSql .= ' current_amount = ' . $this->sqlCurrentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unadjusted_amount = ' . $this->sqlUnadjustedAmount() . ','; } elseif( true == array_key_exists( 'UnadjustedAmount', $this->getChangedColumns() ) ) { $strSql .= ' unadjusted_amount = ' . $this->sqlUnadjustedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_start_read = ' . $this->sqlMeterStartRead() . ','; } elseif( true == array_key_exists( 'MeterStartRead', $this->getChangedColumns() ) ) { $strSql .= ' meter_start_read = ' . $this->sqlMeterStartRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_end_read = ' . $this->sqlMeterEndRead() . ','; } elseif( true == array_key_exists( 'MeterEndRead', $this->getChangedColumns() ) ) { $strSql .= ' meter_end_read = ' . $this->sqlMeterEndRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' usage_factor = ' . $this->sqlUsageFactor() . ','; } elseif( true == array_key_exists( 'UsageFactor', $this->getChangedColumns() ) ) { $strSql .= ' usage_factor = ' . $this->sqlUsageFactor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_original = ' . $this->sqlIsOriginal() . ','; } elseif( true == array_key_exists( 'IsOriginal', $this->getChangedColumns() ) ) { $strSql .= ' is_original = ' . $this->sqlIsOriginal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual = ' . $this->sqlIsManual() . ','; } elseif( true == array_key_exists( 'IsManual', $this->getChangedColumns() ) ) { $strSql .= ' is_manual = ' . $this->sqlIsManual() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_move_out = ' . $this->sqlIsMoveOut() . ','; } elseif( true == array_key_exists( 'IsMoveOut', $this->getChangedColumns() ) ) { $strSql .= ' is_move_out = ' . $this->sqlIsMoveOut() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_tax = ' . $this->sqlIsTax() . ','; } elseif( true == array_key_exists( 'IsTax', $this->getChangedColumns() ) ) { $strSql .= ' is_tax = ' . $this->sqlIsTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_corrected = ' . $this->sqlIsCorrected() . ','; } elseif( true == array_key_exists( 'IsCorrected', $this->getChangedColumns() ) ) { $strSql .= ' is_corrected = ' . $this->sqlIsCorrected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' btu_factor = ' . $this->sqlBtuFactor() . ','; } elseif( true == array_key_exists( 'BtuFactor', $this->getChangedColumns() ) ) { $strSql .= ' btu_factor = ' . $this->sqlBtuFactor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' baseline_allowance = ' . $this->sqlBaselineAllowance() . ','; } elseif( true == array_key_exists( 'BaselineAllowance', $this->getChangedColumns() ) ) { $strSql .= ' baseline_allowance = ' . $this->sqlBaselineAllowance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjusted_on = ' . $this->sqlAdjustedOn() . ','; } elseif( true == array_key_exists( 'AdjustedOn', $this->getChangedColumns() ) ) { $strSql .= ' adjusted_on = ' . $this->sqlAdjustedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; } elseif( true == array_key_exists( 'RejectedBy', $this->getChangedColumns() ) ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_subsidy_amount = ' . $this->sqlTotalSubsidyAmount() . ','; } elseif( true == array_key_exists( 'TotalSubsidyAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_subsidy_amount = ' . $this->sqlTotalSubsidyAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_name = ' . $this->sqlUtilityProviderName() . ','; } elseif( true == array_key_exists( 'UtilityProviderName', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_name = ' . $this->sqlUtilityProviderName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_calculation_type = ' . $this->sqlUtilityCalculationType() . ','; } elseif( true == array_key_exists( 'UtilityCalculationType', $this->getChangedColumns() ) ) { $strSql .= ' utility_calculation_type = ' . $this->sqlUtilityCalculationType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_vcr_type_rate_id = ' . $this->sqlPropertyVcrTypeRateId() . ','; } elseif( true == array_key_exists( 'PropertyVcrTypeRateId', $this->getChangedColumns() ) ) { $strSql .= ' property_vcr_type_rate_id = ' . $this->sqlPropertyVcrTypeRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vcr_utility_bill_id = ' . $this->sqlVcrUtilityBillId() . ','; } elseif( true == array_key_exists( 'VcrUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' vcr_utility_bill_id = ' . $this->sqlVcrUtilityBillId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_transaction_type_id' => $this->getUtilityTransactionTypeId(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'original_utility_transaction_id' => $this->getOriginalUtilityTransactionId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'property_utility_type_rate_id' => $this->getPropertyUtilityTypeRateId(),
			'property_utility_type_rate_method_id' => $this->getPropertyUtilityTypeRateMethodId(),
			'utility_type_id' => $this->getUtilityTypeId(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'utility_type_batch_id' => $this->getUtilityTypeBatchId(),
			'utility_bill_charge_id' => $this->getUtilityBillChargeId(),
			'ar_code_id' => $this->getArCodeId(),
			'ar_payment_id' => $this->getArPaymentId(),
			'utility_invoice_id' => $this->getUtilityInvoiceId(),
			'utility_rubs_formula_id' => $this->getUtilityRubsFormulaId(),
			'transaction_id' => $this->getTransactionId(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'recon_utility_transaction_id' => $this->getReconUtilityTransactionId(),
			'tax_utility_transaction_id' => $this->getTaxUtilityTransactionId(),
			'invoice_transaction_id' => $this->getInvoiceTransactionId(),
			'utility_consumption_type_id' => $this->getUtilityConsumptionTypeId(),
			'scheduled_charge_id' => $this->getScheduledChargeId(),
			'custom_resident_charge_id' => $this->getCustomResidentChargeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'units_consumed' => $this->getUnitsConsumed(),
			'charge_name' => $this->getChargeName(),
			'charge_description' => $this->getChargeDescription(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'scheduled_export_date' => $this->getScheduledExportDate(),
			'original_amount' => $this->getOriginalAmount(),
			'actual_amount' => $this->getActualAmount(),
			'current_amount' => $this->getCurrentAmount(),
			'unadjusted_amount' => $this->getUnadjustedAmount(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'meter_start_read' => $this->getMeterStartRead(),
			'meter_end_read' => $this->getMeterEndRead(),
			'memo' => $this->getMemo(),
			'usage_factor' => $this->getUsageFactor(),
			'is_original' => $this->getIsOriginal(),
			'is_manual' => $this->getIsManual(),
			'is_move_out' => $this->getIsMoveOut(),
			'is_tax' => $this->getIsTax(),
			'is_corrected' => $this->getIsCorrected(),
			'btu_factor' => $this->getBtuFactor(),
			'baseline_allowance' => $this->getBaselineAllowance(),
			'post_month' => $this->getPostMonth(),
			'adjusted_on' => $this->getAdjustedOn(),
			'rejected_by' => $this->getRejectedBy(),
			'rejected_on' => $this->getRejectedOn(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'total_subsidy_amount' => $this->getTotalSubsidyAmount(),
			'utility_provider_name' => $this->getUtilityProviderName(),
			'utility_calculation_type' => $this->getUtilityCalculationType(),
			'property_vcr_type_rate_id' => $this->getPropertyVcrTypeRateId(),
			'vcr_utility_bill_id' => $this->getVcrUtilityBillId()
		);
	}

}
?>