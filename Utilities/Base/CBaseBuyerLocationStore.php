<?php

class CBaseBuyerLocationStore extends CEosSingularBase {

	const TABLE_NAME = 'public.buyer_location_stores';

	protected $m_intId;
	protected $m_intBuyerLocationId;
	protected $m_intStoreId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['buyer_location_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerLocationId', trim( $arrValues['buyer_location_id'] ) ); elseif( isset( $arrValues['buyer_location_id'] ) ) $this->setBuyerLocationId( $arrValues['buyer_location_id'] );
		if( isset( $arrValues['store_id'] ) && $boolDirectSet ) $this->set( 'm_intStoreId', trim( $arrValues['store_id'] ) ); elseif( isset( $arrValues['store_id'] ) ) $this->setStoreId( $arrValues['store_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setBuyerLocationId( $intBuyerLocationId ) {
		$this->set( 'm_intBuyerLocationId', CStrings::strToIntDef( $intBuyerLocationId, NULL, false ) );
	}

	public function getBuyerLocationId() {
		return $this->m_intBuyerLocationId;
	}

	public function sqlBuyerLocationId() {
		return ( true == isset( $this->m_intBuyerLocationId ) ) ? ( string ) $this->m_intBuyerLocationId : 'NULL';
	}

	public function setStoreId( $intStoreId ) {
		$this->set( 'm_intStoreId', CStrings::strToIntDef( $intStoreId, NULL, false ) );
	}

	public function getStoreId() {
		return $this->m_intStoreId;
	}

	public function sqlStoreId() {
		return ( true == isset( $this->m_intStoreId ) ) ? ( string ) $this->m_intStoreId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, buyer_location_id, store_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlBuyerLocationId() . ', ' .
 						$this->sqlStoreId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_location_id = ' . $this->sqlBuyerLocationId() . ','; } elseif( true == array_key_exists( 'BuyerLocationId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_location_id = ' . $this->sqlBuyerLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' store_id = ' . $this->sqlStoreId() . ','; } elseif( true == array_key_exists( 'StoreId', $this->getChangedColumns() ) ) { $strSql .= ' store_id = ' . $this->sqlStoreId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'buyer_location_id' => $this->getBuyerLocationId(),
			'store_id' => $this->getStoreId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>