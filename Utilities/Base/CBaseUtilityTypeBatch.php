<?php

class CBaseUtilityTypeBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_type_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUtilityBatchId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intUtilityConsumptionTypeId;
	protected $m_intNumberOfEstimatedUnits;
	protected $m_intNumberOfBills;
	protected $m_intUnitsWithFaultyEquipment;
	protected $m_fltTotalRecoveryAmount;
	protected $m_strPeriodStartDate;
	protected $m_strPeriodEndDate;
	protected $m_strDueDate;
	protected $m_fltBillConsumptionUnits;
	protected $m_fltBillTotalPrice;
	protected $m_strNotes;
	protected $m_strReconciledOn;
	protected $m_intRequiresReconciliation;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intNumberOfEstimatedUnits = '0';
		$this->m_intNumberOfBills = '0';
		$this->m_intUnitsWithFaultyEquipment = '0';
		$this->m_fltTotalRecoveryAmount = '0';
		$this->m_fltBillConsumptionUnits = '0';
		$this->m_fltBillTotalPrice = '0';
		$this->m_intRequiresReconciliation = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['utility_consumption_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityConsumptionTypeId', trim( $arrValues['utility_consumption_type_id'] ) ); elseif( isset( $arrValues['utility_consumption_type_id'] ) ) $this->setUtilityConsumptionTypeId( $arrValues['utility_consumption_type_id'] );
		if( isset( $arrValues['number_of_estimated_units'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfEstimatedUnits', trim( $arrValues['number_of_estimated_units'] ) ); elseif( isset( $arrValues['number_of_estimated_units'] ) ) $this->setNumberOfEstimatedUnits( $arrValues['number_of_estimated_units'] );
		if( isset( $arrValues['number_of_bills'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfBills', trim( $arrValues['number_of_bills'] ) ); elseif( isset( $arrValues['number_of_bills'] ) ) $this->setNumberOfBills( $arrValues['number_of_bills'] );
		if( isset( $arrValues['units_with_faulty_equipment'] ) && $boolDirectSet ) $this->set( 'm_intUnitsWithFaultyEquipment', trim( $arrValues['units_with_faulty_equipment'] ) ); elseif( isset( $arrValues['units_with_faulty_equipment'] ) ) $this->setUnitsWithFaultyEquipment( $arrValues['units_with_faulty_equipment'] );
		if( isset( $arrValues['total_recovery_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalRecoveryAmount', trim( $arrValues['total_recovery_amount'] ) ); elseif( isset( $arrValues['total_recovery_amount'] ) ) $this->setTotalRecoveryAmount( $arrValues['total_recovery_amount'] );
		if( isset( $arrValues['period_start_date'] ) && $boolDirectSet ) $this->set( 'm_strPeriodStartDate', trim( $arrValues['period_start_date'] ) ); elseif( isset( $arrValues['period_start_date'] ) ) $this->setPeriodStartDate( $arrValues['period_start_date'] );
		if( isset( $arrValues['period_end_date'] ) && $boolDirectSet ) $this->set( 'm_strPeriodEndDate', trim( $arrValues['period_end_date'] ) ); elseif( isset( $arrValues['period_end_date'] ) ) $this->setPeriodEndDate( $arrValues['period_end_date'] );
		if( isset( $arrValues['due_date'] ) && $boolDirectSet ) $this->set( 'm_strDueDate', trim( $arrValues['due_date'] ) ); elseif( isset( $arrValues['due_date'] ) ) $this->setDueDate( $arrValues['due_date'] );
		if( isset( $arrValues['bill_consumption_units'] ) && $boolDirectSet ) $this->set( 'm_fltBillConsumptionUnits', trim( $arrValues['bill_consumption_units'] ) ); elseif( isset( $arrValues['bill_consumption_units'] ) ) $this->setBillConsumptionUnits( $arrValues['bill_consumption_units'] );
		if( isset( $arrValues['bill_total_price'] ) && $boolDirectSet ) $this->set( 'm_fltBillTotalPrice', trim( $arrValues['bill_total_price'] ) ); elseif( isset( $arrValues['bill_total_price'] ) ) $this->setBillTotalPrice( $arrValues['bill_total_price'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['reconciled_on'] ) && $boolDirectSet ) $this->set( 'm_strReconciledOn', trim( $arrValues['reconciled_on'] ) ); elseif( isset( $arrValues['reconciled_on'] ) ) $this->setReconciledOn( $arrValues['reconciled_on'] );
		if( isset( $arrValues['requires_reconciliation'] ) && $boolDirectSet ) $this->set( 'm_intRequiresReconciliation', trim( $arrValues['requires_reconciliation'] ) ); elseif( isset( $arrValues['requires_reconciliation'] ) ) $this->setRequiresReconciliation( $arrValues['requires_reconciliation'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setUtilityConsumptionTypeId( $intUtilityConsumptionTypeId ) {
		$this->set( 'm_intUtilityConsumptionTypeId', CStrings::strToIntDef( $intUtilityConsumptionTypeId, NULL, false ) );
	}

	public function getUtilityConsumptionTypeId() {
		return $this->m_intUtilityConsumptionTypeId;
	}

	public function sqlUtilityConsumptionTypeId() {
		return ( true == isset( $this->m_intUtilityConsumptionTypeId ) ) ? ( string ) $this->m_intUtilityConsumptionTypeId : 'NULL';
	}

	public function setNumberOfEstimatedUnits( $intNumberOfEstimatedUnits ) {
		$this->set( 'm_intNumberOfEstimatedUnits', CStrings::strToIntDef( $intNumberOfEstimatedUnits, NULL, false ) );
	}

	public function getNumberOfEstimatedUnits() {
		return $this->m_intNumberOfEstimatedUnits;
	}

	public function sqlNumberOfEstimatedUnits() {
		return ( true == isset( $this->m_intNumberOfEstimatedUnits ) ) ? ( string ) $this->m_intNumberOfEstimatedUnits : '0';
	}

	public function setNumberOfBills( $intNumberOfBills ) {
		$this->set( 'm_intNumberOfBills', CStrings::strToIntDef( $intNumberOfBills, NULL, false ) );
	}

	public function getNumberOfBills() {
		return $this->m_intNumberOfBills;
	}

	public function sqlNumberOfBills() {
		return ( true == isset( $this->m_intNumberOfBills ) ) ? ( string ) $this->m_intNumberOfBills : '0';
	}

	public function setUnitsWithFaultyEquipment( $intUnitsWithFaultyEquipment ) {
		$this->set( 'm_intUnitsWithFaultyEquipment', CStrings::strToIntDef( $intUnitsWithFaultyEquipment, NULL, false ) );
	}

	public function getUnitsWithFaultyEquipment() {
		return $this->m_intUnitsWithFaultyEquipment;
	}

	public function sqlUnitsWithFaultyEquipment() {
		return ( true == isset( $this->m_intUnitsWithFaultyEquipment ) ) ? ( string ) $this->m_intUnitsWithFaultyEquipment : '0';
	}

	public function setTotalRecoveryAmount( $fltTotalRecoveryAmount ) {
		$this->set( 'm_fltTotalRecoveryAmount', CStrings::strToFloatDef( $fltTotalRecoveryAmount, NULL, false, 2 ) );
	}

	public function getTotalRecoveryAmount() {
		return $this->m_fltTotalRecoveryAmount;
	}

	public function sqlTotalRecoveryAmount() {
		return ( true == isset( $this->m_fltTotalRecoveryAmount ) ) ? ( string ) $this->m_fltTotalRecoveryAmount : '0';
	}

	public function setPeriodStartDate( $strPeriodStartDate ) {
		$this->set( 'm_strPeriodStartDate', CStrings::strTrimDef( $strPeriodStartDate, -1, NULL, true ) );
	}

	public function getPeriodStartDate() {
		return $this->m_strPeriodStartDate;
	}

	public function sqlPeriodStartDate() {
		return ( true == isset( $this->m_strPeriodStartDate ) ) ? '\'' . $this->m_strPeriodStartDate . '\'' : 'NOW()';
	}

	public function setPeriodEndDate( $strPeriodEndDate ) {
		$this->set( 'm_strPeriodEndDate', CStrings::strTrimDef( $strPeriodEndDate, -1, NULL, true ) );
	}

	public function getPeriodEndDate() {
		return $this->m_strPeriodEndDate;
	}

	public function sqlPeriodEndDate() {
		return ( true == isset( $this->m_strPeriodEndDate ) ) ? '\'' . $this->m_strPeriodEndDate . '\'' : 'NOW()';
	}

	public function setDueDate( $strDueDate ) {
		$this->set( 'm_strDueDate', CStrings::strTrimDef( $strDueDate, -1, NULL, true ) );
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function sqlDueDate() {
		return ( true == isset( $this->m_strDueDate ) ) ? '\'' . $this->m_strDueDate . '\'' : 'NOW()';
	}

	public function setBillConsumptionUnits( $fltBillConsumptionUnits ) {
		$this->set( 'm_fltBillConsumptionUnits', CStrings::strToFloatDef( $fltBillConsumptionUnits, NULL, false, 4 ) );
	}

	public function getBillConsumptionUnits() {
		return $this->m_fltBillConsumptionUnits;
	}

	public function sqlBillConsumptionUnits() {
		return ( true == isset( $this->m_fltBillConsumptionUnits ) ) ? ( string ) $this->m_fltBillConsumptionUnits : '0';
	}

	public function setBillTotalPrice( $fltBillTotalPrice ) {
		$this->set( 'm_fltBillTotalPrice', CStrings::strToFloatDef( $fltBillTotalPrice, NULL, false, 4 ) );
	}

	public function getBillTotalPrice() {
		return $this->m_fltBillTotalPrice;
	}

	public function sqlBillTotalPrice() {
		return ( true == isset( $this->m_fltBillTotalPrice ) ) ? ( string ) $this->m_fltBillTotalPrice : '0';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setReconciledOn( $strReconciledOn ) {
		$this->set( 'm_strReconciledOn', CStrings::strTrimDef( $strReconciledOn, -1, NULL, true ) );
	}

	public function getReconciledOn() {
		return $this->m_strReconciledOn;
	}

	public function sqlReconciledOn() {
		return ( true == isset( $this->m_strReconciledOn ) ) ? '\'' . $this->m_strReconciledOn . '\'' : 'NULL';
	}

	public function setRequiresReconciliation( $intRequiresReconciliation ) {
		$this->set( 'm_intRequiresReconciliation', CStrings::strToIntDef( $intRequiresReconciliation, NULL, false ) );
	}

	public function getRequiresReconciliation() {
		return $this->m_intRequiresReconciliation;
	}

	public function sqlRequiresReconciliation() {
		return ( true == isset( $this->m_intRequiresReconciliation ) ) ? ( string ) $this->m_intRequiresReconciliation : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, utility_batch_id, property_utility_type_id, utility_consumption_type_id, number_of_estimated_units, number_of_bills, units_with_faulty_equipment, total_recovery_amount, period_start_date, period_end_date, due_date, bill_consumption_units, bill_total_price, notes, reconciled_on, requires_reconciliation, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlUtilityBatchId() . ', ' .
 						$this->sqlPropertyUtilityTypeId() . ', ' .
 						$this->sqlUtilityConsumptionTypeId() . ', ' .
 						$this->sqlNumberOfEstimatedUnits() . ', ' .
 						$this->sqlNumberOfBills() . ', ' .
 						$this->sqlUnitsWithFaultyEquipment() . ', ' .
 						$this->sqlTotalRecoveryAmount() . ', ' .
 						$this->sqlPeriodStartDate() . ', ' .
 						$this->sqlPeriodEndDate() . ', ' .
 						$this->sqlDueDate() . ', ' .
 						$this->sqlBillConsumptionUnits() . ', ' .
 						$this->sqlBillTotalPrice() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlReconciledOn() . ', ' .
 						$this->sqlRequiresReconciliation() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_consumption_type_id = ' . $this->sqlUtilityConsumptionTypeId() . ','; } elseif( true == array_key_exists( 'UtilityConsumptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_consumption_type_id = ' . $this->sqlUtilityConsumptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_estimated_units = ' . $this->sqlNumberOfEstimatedUnits() . ','; } elseif( true == array_key_exists( 'NumberOfEstimatedUnits', $this->getChangedColumns() ) ) { $strSql .= ' number_of_estimated_units = ' . $this->sqlNumberOfEstimatedUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bills = ' . $this->sqlNumberOfBills() . ','; } elseif( true == array_key_exists( 'NumberOfBills', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bills = ' . $this->sqlNumberOfBills() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' units_with_faulty_equipment = ' . $this->sqlUnitsWithFaultyEquipment() . ','; } elseif( true == array_key_exists( 'UnitsWithFaultyEquipment', $this->getChangedColumns() ) ) { $strSql .= ' units_with_faulty_equipment = ' . $this->sqlUnitsWithFaultyEquipment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_recovery_amount = ' . $this->sqlTotalRecoveryAmount() . ','; } elseif( true == array_key_exists( 'TotalRecoveryAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_recovery_amount = ' . $this->sqlTotalRecoveryAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_start_date = ' . $this->sqlPeriodStartDate() . ','; } elseif( true == array_key_exists( 'PeriodStartDate', $this->getChangedColumns() ) ) { $strSql .= ' period_start_date = ' . $this->sqlPeriodStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_end_date = ' . $this->sqlPeriodEndDate() . ','; } elseif( true == array_key_exists( 'PeriodEndDate', $this->getChangedColumns() ) ) { $strSql .= ' period_end_date = ' . $this->sqlPeriodEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; } elseif( true == array_key_exists( 'DueDate', $this->getChangedColumns() ) ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_consumption_units = ' . $this->sqlBillConsumptionUnits() . ','; } elseif( true == array_key_exists( 'BillConsumptionUnits', $this->getChangedColumns() ) ) { $strSql .= ' bill_consumption_units = ' . $this->sqlBillConsumptionUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_total_price = ' . $this->sqlBillTotalPrice() . ','; } elseif( true == array_key_exists( 'BillTotalPrice', $this->getChangedColumns() ) ) { $strSql .= ' bill_total_price = ' . $this->sqlBillTotalPrice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciled_on = ' . $this->sqlReconciledOn() . ','; } elseif( true == array_key_exists( 'ReconciledOn', $this->getChangedColumns() ) ) { $strSql .= ' reconciled_on = ' . $this->sqlReconciledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_reconciliation = ' . $this->sqlRequiresReconciliation() . ','; } elseif( true == array_key_exists( 'RequiresReconciliation', $this->getChangedColumns() ) ) { $strSql .= ' requires_reconciliation = ' . $this->sqlRequiresReconciliation() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'utility_consumption_type_id' => $this->getUtilityConsumptionTypeId(),
			'number_of_estimated_units' => $this->getNumberOfEstimatedUnits(),
			'number_of_bills' => $this->getNumberOfBills(),
			'units_with_faulty_equipment' => $this->getUnitsWithFaultyEquipment(),
			'total_recovery_amount' => $this->getTotalRecoveryAmount(),
			'period_start_date' => $this->getPeriodStartDate(),
			'period_end_date' => $this->getPeriodEndDate(),
			'due_date' => $this->getDueDate(),
			'bill_consumption_units' => $this->getBillConsumptionUnits(),
			'bill_total_price' => $this->getBillTotalPrice(),
			'notes' => $this->getNotes(),
			'reconciled_on' => $this->getReconciledOn(),
			'requires_reconciliation' => $this->getRequiresReconciliation(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>