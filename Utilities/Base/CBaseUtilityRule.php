<?php

class CBaseUtilityRule extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_rules';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityRuleTypeId;
	protected $m_intUtilityRuleInputOptionId;
	protected $m_strStringValue;
	protected $m_intMinNumericValue;
	protected $m_intMaxNumericValue;
	protected $m_fltMinPercentageValue;
	protected $m_fltMaxPercentageValue;
	protected $m_boolBooleanValue;
	protected $m_strReferenceDescription;
	protected $m_strSummary;
	protected $m_intOrderNum;
	protected $m_strReferenceUrl;
	protected $m_strErrorMessage;
	protected $m_boolIsBatchRule;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolBooleanValue = false;
		$this->m_intOrderNum = '0';
		$this->m_boolIsBatchRule = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_rule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityRuleTypeId', trim( $arrValues['utility_rule_type_id'] ) ); elseif( isset( $arrValues['utility_rule_type_id'] ) ) $this->setUtilityRuleTypeId( $arrValues['utility_rule_type_id'] );
		if( isset( $arrValues['utility_rule_input_option_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityRuleInputOptionId', trim( $arrValues['utility_rule_input_option_id'] ) ); elseif( isset( $arrValues['utility_rule_input_option_id'] ) ) $this->setUtilityRuleInputOptionId( $arrValues['utility_rule_input_option_id'] );
		if( isset( $arrValues['string_value'] ) && $boolDirectSet ) $this->set( 'm_strStringValue', trim( stripcslashes( $arrValues['string_value'] ) ) ); elseif( isset( $arrValues['string_value'] ) ) $this->setStringValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['string_value'] ) : $arrValues['string_value'] );
		if( isset( $arrValues['min_numeric_value'] ) && $boolDirectSet ) $this->set( 'm_intMinNumericValue', trim( $arrValues['min_numeric_value'] ) ); elseif( isset( $arrValues['min_numeric_value'] ) ) $this->setMinNumericValue( $arrValues['min_numeric_value'] );
		if( isset( $arrValues['max_numeric_value'] ) && $boolDirectSet ) $this->set( 'm_intMaxNumericValue', trim( $arrValues['max_numeric_value'] ) ); elseif( isset( $arrValues['max_numeric_value'] ) ) $this->setMaxNumericValue( $arrValues['max_numeric_value'] );
		if( isset( $arrValues['min_percentage_value'] ) && $boolDirectSet ) $this->set( 'm_fltMinPercentageValue', trim( $arrValues['min_percentage_value'] ) ); elseif( isset( $arrValues['min_percentage_value'] ) ) $this->setMinPercentageValue( $arrValues['min_percentage_value'] );
		if( isset( $arrValues['max_percentage_value'] ) && $boolDirectSet ) $this->set( 'm_fltMaxPercentageValue', trim( $arrValues['max_percentage_value'] ) ); elseif( isset( $arrValues['max_percentage_value'] ) ) $this->setMaxPercentageValue( $arrValues['max_percentage_value'] );
		if( isset( $arrValues['boolean_value'] ) && $boolDirectSet ) $this->set( 'm_boolBooleanValue', trim( stripcslashes( $arrValues['boolean_value'] ) ) ); elseif( isset( $arrValues['boolean_value'] ) ) $this->setBooleanValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['boolean_value'] ) : $arrValues['boolean_value'] );
		if( isset( $arrValues['reference_description'] ) && $boolDirectSet ) $this->set( 'm_strReferenceDescription', trim( stripcslashes( $arrValues['reference_description'] ) ) ); elseif( isset( $arrValues['reference_description'] ) ) $this->setReferenceDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reference_description'] ) : $arrValues['reference_description'] );
		if( isset( $arrValues['summary'] ) && $boolDirectSet ) $this->set( 'm_strSummary', trim( stripcslashes( $arrValues['summary'] ) ) ); elseif( isset( $arrValues['summary'] ) ) $this->setSummary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['summary'] ) : $arrValues['summary'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['reference_url'] ) && $boolDirectSet ) $this->set( 'm_strReferenceUrl', trim( stripcslashes( $arrValues['reference_url'] ) ) ); elseif( isset( $arrValues['reference_url'] ) ) $this->setReferenceUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reference_url'] ) : $arrValues['reference_url'] );
		if( isset( $arrValues['error_message'] ) && $boolDirectSet ) $this->set( 'm_strErrorMessage', trim( stripcslashes( $arrValues['error_message'] ) ) ); elseif( isset( $arrValues['error_message'] ) ) $this->setErrorMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_message'] ) : $arrValues['error_message'] );
		if( isset( $arrValues['is_batch_rule'] ) && $boolDirectSet ) $this->set( 'm_boolIsBatchRule', trim( stripcslashes( $arrValues['is_batch_rule'] ) ) ); elseif( isset( $arrValues['is_batch_rule'] ) ) $this->setIsBatchRule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_batch_rule'] ) : $arrValues['is_batch_rule'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityRuleTypeId( $intUtilityRuleTypeId ) {
		$this->set( 'm_intUtilityRuleTypeId', CStrings::strToIntDef( $intUtilityRuleTypeId, NULL, false ) );
	}

	public function getUtilityRuleTypeId() {
		return $this->m_intUtilityRuleTypeId;
	}

	public function sqlUtilityRuleTypeId() {
		return ( true == isset( $this->m_intUtilityRuleTypeId ) ) ? ( string ) $this->m_intUtilityRuleTypeId : 'NULL';
	}

	public function setUtilityRuleInputOptionId( $intUtilityRuleInputOptionId ) {
		$this->set( 'm_intUtilityRuleInputOptionId', CStrings::strToIntDef( $intUtilityRuleInputOptionId, NULL, false ) );
	}

	public function getUtilityRuleInputOptionId() {
		return $this->m_intUtilityRuleInputOptionId;
	}

	public function sqlUtilityRuleInputOptionId() {
		return ( true == isset( $this->m_intUtilityRuleInputOptionId ) ) ? ( string ) $this->m_intUtilityRuleInputOptionId : 'NULL';
	}

	public function setStringValue( $strStringValue ) {
		$this->set( 'm_strStringValue', CStrings::strTrimDef( $strStringValue, 240, NULL, true ) );
	}

	public function getStringValue() {
		return $this->m_strStringValue;
	}

	public function sqlStringValue() {
		return ( true == isset( $this->m_strStringValue ) ) ? '\'' . addslashes( $this->m_strStringValue ) . '\'' : 'NULL';
	}

	public function setMinNumericValue( $intMinNumericValue ) {
		$this->set( 'm_intMinNumericValue', CStrings::strToIntDef( $intMinNumericValue, NULL, false ) );
	}

	public function getMinNumericValue() {
		return $this->m_intMinNumericValue;
	}

	public function sqlMinNumericValue() {
		return ( true == isset( $this->m_intMinNumericValue ) ) ? ( string ) $this->m_intMinNumericValue : 'NULL';
	}

	public function setMaxNumericValue( $intMaxNumericValue ) {
		$this->set( 'm_intMaxNumericValue', CStrings::strToIntDef( $intMaxNumericValue, NULL, false ) );
	}

	public function getMaxNumericValue() {
		return $this->m_intMaxNumericValue;
	}

	public function sqlMaxNumericValue() {
		return ( true == isset( $this->m_intMaxNumericValue ) ) ? ( string ) $this->m_intMaxNumericValue : 'NULL';
	}

	public function setMinPercentageValue( $fltMinPercentageValue ) {
		$this->set( 'm_fltMinPercentageValue', CStrings::strToFloatDef( $fltMinPercentageValue, NULL, false, 6 ) );
	}

	public function getMinPercentageValue() {
		return $this->m_fltMinPercentageValue;
	}

	public function sqlMinPercentageValue() {
		return ( true == isset( $this->m_fltMinPercentageValue ) ) ? ( string ) $this->m_fltMinPercentageValue : 'NULL';
	}

	public function setMaxPercentageValue( $fltMaxPercentageValue ) {
		$this->set( 'm_fltMaxPercentageValue', CStrings::strToFloatDef( $fltMaxPercentageValue, NULL, false, 6 ) );
	}

	public function getMaxPercentageValue() {
		return $this->m_fltMaxPercentageValue;
	}

	public function sqlMaxPercentageValue() {
		return ( true == isset( $this->m_fltMaxPercentageValue ) ) ? ( string ) $this->m_fltMaxPercentageValue : 'NULL';
	}

	public function setBooleanValue( $boolBooleanValue ) {
		$this->set( 'm_boolBooleanValue', CStrings::strToBool( $boolBooleanValue ) );
	}

	public function getBooleanValue() {
		return $this->m_boolBooleanValue;
	}

	public function sqlBooleanValue() {
		return ( true == isset( $this->m_boolBooleanValue ) ) ? '\'' . ( true == ( bool ) $this->m_boolBooleanValue ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setReferenceDescription( $strReferenceDescription ) {
		$this->set( 'm_strReferenceDescription', CStrings::strTrimDef( $strReferenceDescription, -1, NULL, true ) );
	}

	public function getReferenceDescription() {
		return $this->m_strReferenceDescription;
	}

	public function sqlReferenceDescription() {
		return ( true == isset( $this->m_strReferenceDescription ) ) ? '\'' . addslashes( $this->m_strReferenceDescription ) . '\'' : 'NULL';
	}

	public function setSummary( $strSummary ) {
		$this->set( 'm_strSummary', CStrings::strTrimDef( $strSummary, -1, NULL, true ) );
	}

	public function getSummary() {
		return $this->m_strSummary;
	}

	public function sqlSummary() {
		return ( true == isset( $this->m_strSummary ) ) ? '\'' . addslashes( $this->m_strSummary ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setReferenceUrl( $strReferenceUrl ) {
		$this->set( 'm_strReferenceUrl', CStrings::strTrimDef( $strReferenceUrl, 1000, NULL, true ) );
	}

	public function getReferenceUrl() {
		return $this->m_strReferenceUrl;
	}

	public function sqlReferenceUrl() {
		return ( true == isset( $this->m_strReferenceUrl ) ) ? '\'' . addslashes( $this->m_strReferenceUrl ) . '\'' : 'NULL';
	}

	public function setErrorMessage( $strErrorMessage ) {
		$this->set( 'm_strErrorMessage', CStrings::strTrimDef( $strErrorMessage, 500, NULL, true ) );
	}

	public function getErrorMessage() {
		return $this->m_strErrorMessage;
	}

	public function sqlErrorMessage() {
		return ( true == isset( $this->m_strErrorMessage ) ) ? '\'' . addslashes( $this->m_strErrorMessage ) . '\'' : 'NULL';
	}

	public function setIsBatchRule( $boolIsBatchRule ) {
		$this->set( 'm_boolIsBatchRule', CStrings::strToBool( $boolIsBatchRule ) );
	}

	public function getIsBatchRule() {
		return $this->m_boolIsBatchRule;
	}

	public function sqlIsBatchRule() {
		return ( true == isset( $this->m_boolIsBatchRule ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBatchRule ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_rule_type_id, utility_rule_input_option_id, string_value, min_numeric_value, max_numeric_value, min_percentage_value, max_percentage_value, boolean_value, reference_description, summary, order_num, reference_url, error_message, is_batch_rule, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUtilityRuleTypeId() . ', ' .
 						$this->sqlUtilityRuleInputOptionId() . ', ' .
 						$this->sqlStringValue() . ', ' .
 						$this->sqlMinNumericValue() . ', ' .
 						$this->sqlMaxNumericValue() . ', ' .
 						$this->sqlMinPercentageValue() . ', ' .
 						$this->sqlMaxPercentageValue() . ', ' .
 						$this->sqlBooleanValue() . ', ' .
 						$this->sqlReferenceDescription() . ', ' .
 						$this->sqlSummary() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlReferenceUrl() . ', ' .
 						$this->sqlErrorMessage() . ', ' .
 						$this->sqlIsBatchRule() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_rule_type_id = ' . $this->sqlUtilityRuleTypeId() . ','; } elseif( true == array_key_exists( 'UtilityRuleTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_rule_type_id = ' . $this->sqlUtilityRuleTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_rule_input_option_id = ' . $this->sqlUtilityRuleInputOptionId() . ','; } elseif( true == array_key_exists( 'UtilityRuleInputOptionId', $this->getChangedColumns() ) ) { $strSql .= ' utility_rule_input_option_id = ' . $this->sqlUtilityRuleInputOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' string_value = ' . $this->sqlStringValue() . ','; } elseif( true == array_key_exists( 'StringValue', $this->getChangedColumns() ) ) { $strSql .= ' string_value = ' . $this->sqlStringValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_numeric_value = ' . $this->sqlMinNumericValue() . ','; } elseif( true == array_key_exists( 'MinNumericValue', $this->getChangedColumns() ) ) { $strSql .= ' min_numeric_value = ' . $this->sqlMinNumericValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_numeric_value = ' . $this->sqlMaxNumericValue() . ','; } elseif( true == array_key_exists( 'MaxNumericValue', $this->getChangedColumns() ) ) { $strSql .= ' max_numeric_value = ' . $this->sqlMaxNumericValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_percentage_value = ' . $this->sqlMinPercentageValue() . ','; } elseif( true == array_key_exists( 'MinPercentageValue', $this->getChangedColumns() ) ) { $strSql .= ' min_percentage_value = ' . $this->sqlMinPercentageValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_percentage_value = ' . $this->sqlMaxPercentageValue() . ','; } elseif( true == array_key_exists( 'MaxPercentageValue', $this->getChangedColumns() ) ) { $strSql .= ' max_percentage_value = ' . $this->sqlMaxPercentageValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' boolean_value = ' . $this->sqlBooleanValue() . ','; } elseif( true == array_key_exists( 'BooleanValue', $this->getChangedColumns() ) ) { $strSql .= ' boolean_value = ' . $this->sqlBooleanValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_description = ' . $this->sqlReferenceDescription() . ','; } elseif( true == array_key_exists( 'ReferenceDescription', $this->getChangedColumns() ) ) { $strSql .= ' reference_description = ' . $this->sqlReferenceDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' summary = ' . $this->sqlSummary() . ','; } elseif( true == array_key_exists( 'Summary', $this->getChangedColumns() ) ) { $strSql .= ' summary = ' . $this->sqlSummary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_url = ' . $this->sqlReferenceUrl() . ','; } elseif( true == array_key_exists( 'ReferenceUrl', $this->getChangedColumns() ) ) { $strSql .= ' reference_url = ' . $this->sqlReferenceUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_message = ' . $this->sqlErrorMessage() . ','; } elseif( true == array_key_exists( 'ErrorMessage', $this->getChangedColumns() ) ) { $strSql .= ' error_message = ' . $this->sqlErrorMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_batch_rule = ' . $this->sqlIsBatchRule() . ','; } elseif( true == array_key_exists( 'IsBatchRule', $this->getChangedColumns() ) ) { $strSql .= ' is_batch_rule = ' . $this->sqlIsBatchRule() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_rule_type_id' => $this->getUtilityRuleTypeId(),
			'utility_rule_input_option_id' => $this->getUtilityRuleInputOptionId(),
			'string_value' => $this->getStringValue(),
			'min_numeric_value' => $this->getMinNumericValue(),
			'max_numeric_value' => $this->getMaxNumericValue(),
			'min_percentage_value' => $this->getMinPercentageValue(),
			'max_percentage_value' => $this->getMaxPercentageValue(),
			'boolean_value' => $this->getBooleanValue(),
			'reference_description' => $this->getReferenceDescription(),
			'summary' => $this->getSummary(),
			'order_num' => $this->getOrderNum(),
			'reference_url' => $this->getReferenceUrl(),
			'error_message' => $this->getErrorMessage(),
			'is_batch_rule' => $this->getIsBatchRule(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>