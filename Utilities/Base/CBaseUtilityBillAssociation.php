<?php

class CBaseUtilityBillAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_associations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBillId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strStartReviewDatetime;
	protected $m_strEndReviewDatetime;
	protected $m_intLastReviewedBy;
	protected $m_strAssociationJson;
	protected $m_jsonAssociationJson;
	protected $m_strAssociationAgainJson;
	protected $m_jsonAssociationAgainJson;
	protected $m_strAssociationReviewCreatedOn;
	protected $m_strAssociationReviewCompletedOn;
	protected $m_intAssociationReviewCompletedBy;
	protected $m_strReviewAssociationJson;
	protected $m_jsonReviewAssociationJson;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['start_review_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartReviewDatetime', trim( $arrValues['start_review_datetime'] ) ); elseif( isset( $arrValues['start_review_datetime'] ) ) $this->setStartReviewDatetime( $arrValues['start_review_datetime'] );
		if( isset( $arrValues['end_review_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndReviewDatetime', trim( $arrValues['end_review_datetime'] ) ); elseif( isset( $arrValues['end_review_datetime'] ) ) $this->setEndReviewDatetime( $arrValues['end_review_datetime'] );
		if( isset( $arrValues['last_reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intLastReviewedBy', trim( $arrValues['last_reviewed_by'] ) ); elseif( isset( $arrValues['last_reviewed_by'] ) ) $this->setLastReviewedBy( $arrValues['last_reviewed_by'] );
		if( isset( $arrValues['association_json'] ) ) $this->set( 'm_strAssociationJson', trim( $arrValues['association_json'] ) );
		if( isset( $arrValues['association_again_json'] ) ) $this->set( 'm_strAssociationAgainJson', trim( $arrValues['association_again_json'] ) );
		if( isset( $arrValues['association_review_created_on'] ) && $boolDirectSet ) $this->set( 'm_strAssociationReviewCreatedOn', trim( $arrValues['association_review_created_on'] ) ); elseif( isset( $arrValues['association_review_created_on'] ) ) $this->setAssociationReviewCreatedOn( $arrValues['association_review_created_on'] );
		if( isset( $arrValues['association_review_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strAssociationReviewCompletedOn', trim( $arrValues['association_review_completed_on'] ) ); elseif( isset( $arrValues['association_review_completed_on'] ) ) $this->setAssociationReviewCompletedOn( $arrValues['association_review_completed_on'] );
		if( isset( $arrValues['association_review_completed_by'] ) && $boolDirectSet ) $this->set( 'm_intAssociationReviewCompletedBy', trim( $arrValues['association_review_completed_by'] ) ); elseif( isset( $arrValues['association_review_completed_by'] ) ) $this->setAssociationReviewCompletedBy( $arrValues['association_review_completed_by'] );
		if( isset( $arrValues['review_association_json'] ) ) $this->set( 'm_strReviewAssociationJson', trim( $arrValues['review_association_json'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setStartReviewDatetime( $strStartReviewDatetime ) {
		$this->set( 'm_strStartReviewDatetime', CStrings::strTrimDef( $strStartReviewDatetime, -1, NULL, true ) );
	}

	public function getStartReviewDatetime() {
		return $this->m_strStartReviewDatetime;
	}

	public function sqlStartReviewDatetime() {
		return ( true == isset( $this->m_strStartReviewDatetime ) ) ? '\'' . $this->m_strStartReviewDatetime . '\'' : 'NULL';
	}

	public function setEndReviewDatetime( $strEndReviewDatetime ) {
		$this->set( 'm_strEndReviewDatetime', CStrings::strTrimDef( $strEndReviewDatetime, -1, NULL, true ) );
	}

	public function getEndReviewDatetime() {
		return $this->m_strEndReviewDatetime;
	}

	public function sqlEndReviewDatetime() {
		return ( true == isset( $this->m_strEndReviewDatetime ) ) ? '\'' . $this->m_strEndReviewDatetime . '\'' : 'NULL';
	}

	public function setLastReviewedBy( $intLastReviewedBy ) {
		$this->set( 'm_intLastReviewedBy', CStrings::strToIntDef( $intLastReviewedBy, NULL, false ) );
	}

	public function getLastReviewedBy() {
		return $this->m_intLastReviewedBy;
	}

	public function sqlLastReviewedBy() {
		return ( true == isset( $this->m_intLastReviewedBy ) ) ? ( string ) $this->m_intLastReviewedBy : 'NULL';
	}

	public function setAssociationJson( $jsonAssociationJson ) {
		if( true == valObj( $jsonAssociationJson, 'stdClass' ) ) {
			$this->set( 'm_jsonAssociationJson', $jsonAssociationJson );
		} elseif( true == valJsonString( $jsonAssociationJson ) ) {
			$this->set( 'm_jsonAssociationJson', CStrings::strToJson( $jsonAssociationJson ) );
		} else {
			$this->set( 'm_jsonAssociationJson', NULL ); 
		}
		unset( $this->m_strAssociationJson );
	}

	public function getAssociationJson() {
		if( true == isset( $this->m_strAssociationJson ) ) {
			$this->m_jsonAssociationJson = CStrings::strToJson( $this->m_strAssociationJson );
			unset( $this->m_strAssociationJson );
		}
		return $this->m_jsonAssociationJson;
	}

	public function sqlAssociationJson() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAssociationJson() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getAssociationJson() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getAssociationJson() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setAssociationAgainJson( $jsonAssociationAgainJson ) {
		if( true == valObj( $jsonAssociationAgainJson, 'stdClass' ) ) {
			$this->set( 'm_jsonAssociationAgainJson', $jsonAssociationAgainJson );
		} elseif( true == valJsonString( $jsonAssociationAgainJson ) ) {
			$this->set( 'm_jsonAssociationAgainJson', CStrings::strToJson( $jsonAssociationAgainJson ) );
		} else {
			$this->set( 'm_jsonAssociationAgainJson', NULL ); 
		}
		unset( $this->m_strAssociationAgainJson );
	}

	public function getAssociationAgainJson() {
		if( true == isset( $this->m_strAssociationAgainJson ) ) {
			$this->m_jsonAssociationAgainJson = CStrings::strToJson( $this->m_strAssociationAgainJson );
			unset( $this->m_strAssociationAgainJson );
		}
		return $this->m_jsonAssociationAgainJson;
	}

	public function sqlAssociationAgainJson() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAssociationAgainJson() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getAssociationAgainJson() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getAssociationAgainJson() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setAssociationReviewCreatedOn( $strAssociationReviewCreatedOn ) {
		$this->set( 'm_strAssociationReviewCreatedOn', CStrings::strTrimDef( $strAssociationReviewCreatedOn, -1, NULL, true ) );
	}

	public function getAssociationReviewCreatedOn() {
		return $this->m_strAssociationReviewCreatedOn;
	}

	public function sqlAssociationReviewCreatedOn() {
		return ( true == isset( $this->m_strAssociationReviewCreatedOn ) ) ? '\'' . $this->m_strAssociationReviewCreatedOn . '\'' : 'NULL';
	}

	public function setAssociationReviewCompletedOn( $strAssociationReviewCompletedOn ) {
		$this->set( 'm_strAssociationReviewCompletedOn', CStrings::strTrimDef( $strAssociationReviewCompletedOn, -1, NULL, true ) );
	}

	public function getAssociationReviewCompletedOn() {
		return $this->m_strAssociationReviewCompletedOn;
	}

	public function sqlAssociationReviewCompletedOn() {
		return ( true == isset( $this->m_strAssociationReviewCompletedOn ) ) ? '\'' . $this->m_strAssociationReviewCompletedOn . '\'' : 'NULL';
	}

	public function setAssociationReviewCompletedBy( $intAssociationReviewCompletedBy ) {
		$this->set( 'm_intAssociationReviewCompletedBy', CStrings::strToIntDef( $intAssociationReviewCompletedBy, NULL, false ) );
	}

	public function getAssociationReviewCompletedBy() {
		return $this->m_intAssociationReviewCompletedBy;
	}

	public function sqlAssociationReviewCompletedBy() {
		return ( true == isset( $this->m_intAssociationReviewCompletedBy ) ) ? ( string ) $this->m_intAssociationReviewCompletedBy : 'NULL';
	}

	public function setReviewAssociationJson( $jsonReviewAssociationJson ) {
		if( true == valObj( $jsonReviewAssociationJson, 'stdClass' ) ) {
			$this->set( 'm_jsonReviewAssociationJson', $jsonReviewAssociationJson );
		} elseif( true == valJsonString( $jsonReviewAssociationJson ) ) {
			$this->set( 'm_jsonReviewAssociationJson', CStrings::strToJson( $jsonReviewAssociationJson ) );
		} else {
			$this->set( 'm_jsonReviewAssociationJson', NULL ); 
		}
		unset( $this->m_strReviewAssociationJson );
	}

	public function getReviewAssociationJson() {
		if( true == isset( $this->m_strReviewAssociationJson ) ) {
			$this->m_jsonReviewAssociationJson = CStrings::strToJson( $this->m_strReviewAssociationJson );
			unset( $this->m_strReviewAssociationJson );
		}
		return $this->m_jsonReviewAssociationJson;
	}

	public function sqlReviewAssociationJson() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getReviewAssociationJson() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getReviewAssociationJson() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getReviewAssociationJson() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_bill_id, updated_by, updated_on, created_by, created_on, start_review_datetime, end_review_datetime, last_reviewed_by, association_json, association_again_json, association_review_created_on, association_review_completed_on, association_review_completed_by, review_association_json )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlStartReviewDatetime() . ', ' .
						$this->sqlEndReviewDatetime() . ', ' .
						$this->sqlLastReviewedBy() . ', ' .
						$this->sqlAssociationJson() . ', ' .
						$this->sqlAssociationAgainJson() . ', ' .
						$this->sqlAssociationReviewCreatedOn() . ', ' .
						$this->sqlAssociationReviewCompletedOn() . ', ' .
						$this->sqlAssociationReviewCompletedBy() . ', ' .
						$this->sqlReviewAssociationJson() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_review_datetime = ' . $this->sqlStartReviewDatetime(). ',' ; } elseif( true == array_key_exists( 'StartReviewDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_review_datetime = ' . $this->sqlStartReviewDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_review_datetime = ' . $this->sqlEndReviewDatetime(). ',' ; } elseif( true == array_key_exists( 'EndReviewDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_review_datetime = ' . $this->sqlEndReviewDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_reviewed_by = ' . $this->sqlLastReviewedBy(). ',' ; } elseif( true == array_key_exists( 'LastReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_reviewed_by = ' . $this->sqlLastReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' association_json = ' . $this->sqlAssociationJson(). ',' ; } elseif( true == array_key_exists( 'AssociationJson', $this->getChangedColumns() ) ) { $strSql .= ' association_json = ' . $this->sqlAssociationJson() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' association_again_json = ' . $this->sqlAssociationAgainJson(). ',' ; } elseif( true == array_key_exists( 'AssociationAgainJson', $this->getChangedColumns() ) ) { $strSql .= ' association_again_json = ' . $this->sqlAssociationAgainJson() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' association_review_created_on = ' . $this->sqlAssociationReviewCreatedOn(). ',' ; } elseif( true == array_key_exists( 'AssociationReviewCreatedOn', $this->getChangedColumns() ) ) { $strSql .= ' association_review_created_on = ' . $this->sqlAssociationReviewCreatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' association_review_completed_on = ' . $this->sqlAssociationReviewCompletedOn(). ',' ; } elseif( true == array_key_exists( 'AssociationReviewCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' association_review_completed_on = ' . $this->sqlAssociationReviewCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' association_review_completed_by = ' . $this->sqlAssociationReviewCompletedBy(). ',' ; } elseif( true == array_key_exists( 'AssociationReviewCompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' association_review_completed_by = ' . $this->sqlAssociationReviewCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_association_json = ' . $this->sqlReviewAssociationJson(). ',' ; } elseif( true == array_key_exists( 'ReviewAssociationJson', $this->getChangedColumns() ) ) { $strSql .= ' review_association_json = ' . $this->sqlReviewAssociationJson() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'start_review_datetime' => $this->getStartReviewDatetime(),
			'end_review_datetime' => $this->getEndReviewDatetime(),
			'last_reviewed_by' => $this->getLastReviewedBy(),
			'association_json' => $this->getAssociationJson(),
			'association_again_json' => $this->getAssociationAgainJson(),
			'association_review_created_on' => $this->getAssociationReviewCreatedOn(),
			'association_review_completed_on' => $this->getAssociationReviewCompletedOn(),
			'association_review_completed_by' => $this->getAssociationReviewCompletedBy(),
			'review_association_json' => $this->getReviewAssociationJson()
		);
	}

}
?>