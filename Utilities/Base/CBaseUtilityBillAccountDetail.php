<?php

class CBaseUtilityBillAccountDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_account_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityProviderId;
	protected $m_intUtilityBillReceiptTypeId;
	protected $m_strWebRetrievalUrl;
	protected $m_strWebRetrievalUsername;
	protected $m_strWebRetrievalPassword;
	protected $m_strWebRetrievalInstructions;
	protected $m_strWebRetrievalQuestionAnswers;
	protected $m_strEntrataAddressOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strExpectedBillDate;
	protected $m_strExpectedDueDate;
	protected $m_fltBillDayStandardDeviation;
	protected $m_fltDueDayStandardDeviation;
	protected $m_intAverageDifferenceBillDays;
	protected $m_intAverageDifferenceDueDays;
	protected $m_intUtilityCredentialId;
	protected $m_boolHasEntrataAddress;
	protected $m_boolHasEntrataCid;

	public function __construct() {
		parent::__construct();

		$this->m_fltBillDayStandardDeviation = '0';
		$this->m_fltDueDayStandardDeviation = '0';
		$this->m_intAverageDifferenceBillDays = '0';
		$this->m_intAverageDifferenceDueDays = '0';
		$this->m_boolHasEntrataAddress = false;
		$this->m_boolHasEntrataCid = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityProviderId', trim( $arrValues['utility_provider_id'] ) ); elseif( isset( $arrValues['utility_provider_id'] ) ) $this->setUtilityProviderId( $arrValues['utility_provider_id'] );
		if( isset( $arrValues['utility_bill_receipt_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillReceiptTypeId', trim( $arrValues['utility_bill_receipt_type_id'] ) ); elseif( isset( $arrValues['utility_bill_receipt_type_id'] ) ) $this->setUtilityBillReceiptTypeId( $arrValues['utility_bill_receipt_type_id'] );
		if( isset( $arrValues['web_retrieval_url'] ) && $boolDirectSet ) $this->set( 'm_strWebRetrievalUrl', trim( $arrValues['web_retrieval_url'] ) ); elseif( isset( $arrValues['web_retrieval_url'] ) ) $this->setWebRetrievalUrl( $arrValues['web_retrieval_url'] );
		if( isset( $arrValues['web_retrieval_username'] ) && $boolDirectSet ) $this->set( 'm_strWebRetrievalUsername', trim( $arrValues['web_retrieval_username'] ) ); elseif( isset( $arrValues['web_retrieval_username'] ) ) $this->setWebRetrievalUsername( $arrValues['web_retrieval_username'] );
		if( isset( $arrValues['web_retrieval_password'] ) && $boolDirectSet ) $this->set( 'm_strWebRetrievalPassword', trim( $arrValues['web_retrieval_password'] ) ); elseif( isset( $arrValues['web_retrieval_password'] ) ) $this->setWebRetrievalPassword( $arrValues['web_retrieval_password'] );
		if( isset( $arrValues['web_retrieval_instructions'] ) && $boolDirectSet ) $this->set( 'm_strWebRetrievalInstructions', trim( $arrValues['web_retrieval_instructions'] ) ); elseif( isset( $arrValues['web_retrieval_instructions'] ) ) $this->setWebRetrievalInstructions( $arrValues['web_retrieval_instructions'] );
		if( isset( $arrValues['web_retrieval_question_answers'] ) && $boolDirectSet ) $this->set( 'm_strWebRetrievalQuestionAnswers', trim( $arrValues['web_retrieval_question_answers'] ) ); elseif( isset( $arrValues['web_retrieval_question_answers'] ) ) $this->setWebRetrievalQuestionAnswers( $arrValues['web_retrieval_question_answers'] );
		if( isset( $arrValues['entrata_address_on'] ) && $boolDirectSet ) $this->set( 'm_strEntrataAddressOn', trim( $arrValues['entrata_address_on'] ) ); elseif( isset( $arrValues['entrata_address_on'] ) ) $this->setEntrataAddressOn( $arrValues['entrata_address_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['expected_bill_date'] ) && $boolDirectSet ) $this->set( 'm_strExpectedBillDate', trim( $arrValues['expected_bill_date'] ) ); elseif( isset( $arrValues['expected_bill_date'] ) ) $this->setExpectedBillDate( $arrValues['expected_bill_date'] );
		if( isset( $arrValues['expected_due_date'] ) && $boolDirectSet ) $this->set( 'm_strExpectedDueDate', trim( $arrValues['expected_due_date'] ) ); elseif( isset( $arrValues['expected_due_date'] ) ) $this->setExpectedDueDate( $arrValues['expected_due_date'] );
		if( isset( $arrValues['bill_day_standard_deviation'] ) && $boolDirectSet ) $this->set( 'm_fltBillDayStandardDeviation', trim( $arrValues['bill_day_standard_deviation'] ) ); elseif( isset( $arrValues['bill_day_standard_deviation'] ) ) $this->setBillDayStandardDeviation( $arrValues['bill_day_standard_deviation'] );
		if( isset( $arrValues['due_day_standard_deviation'] ) && $boolDirectSet ) $this->set( 'm_fltDueDayStandardDeviation', trim( $arrValues['due_day_standard_deviation'] ) ); elseif( isset( $arrValues['due_day_standard_deviation'] ) ) $this->setDueDayStandardDeviation( $arrValues['due_day_standard_deviation'] );
		if( isset( $arrValues['average_difference_bill_days'] ) && $boolDirectSet ) $this->set( 'm_intAverageDifferenceBillDays', trim( $arrValues['average_difference_bill_days'] ) ); elseif( isset( $arrValues['average_difference_bill_days'] ) ) $this->setAverageDifferenceBillDays( $arrValues['average_difference_bill_days'] );
		if( isset( $arrValues['average_difference_due_days'] ) && $boolDirectSet ) $this->set( 'm_intAverageDifferenceDueDays', trim( $arrValues['average_difference_due_days'] ) ); elseif( isset( $arrValues['average_difference_due_days'] ) ) $this->setAverageDifferenceDueDays( $arrValues['average_difference_due_days'] );
		if( isset( $arrValues['utility_credential_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityCredentialId', trim( $arrValues['utility_credential_id'] ) ); elseif( isset( $arrValues['utility_credential_id'] ) ) $this->setUtilityCredentialId( $arrValues['utility_credential_id'] );
		if( isset( $arrValues['has_entrata_address'] ) && $boolDirectSet ) $this->set( 'm_boolHasEntrataAddress', trim( stripcslashes( $arrValues['has_entrata_address'] ) ) ); elseif( isset( $arrValues['has_entrata_address'] ) ) $this->setHasEntrataAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_entrata_address'] ) : $arrValues['has_entrata_address'] );
		if( isset( $arrValues['has_entrata_cid'] ) && $boolDirectSet ) $this->set( 'm_boolHasEntrataCid', trim( stripcslashes( $arrValues['has_entrata_cid'] ) ) ); elseif( isset( $arrValues['has_entrata_cid'] ) ) $this->setHasEntrataCid( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_entrata_cid'] ) : $arrValues['has_entrata_cid'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityProviderId( $intUtilityProviderId ) {
		$this->set( 'm_intUtilityProviderId', CStrings::strToIntDef( $intUtilityProviderId, NULL, false ) );
	}

	public function getUtilityProviderId() {
		return $this->m_intUtilityProviderId;
	}

	public function sqlUtilityProviderId() {
		return ( true == isset( $this->m_intUtilityProviderId ) ) ? ( string ) $this->m_intUtilityProviderId : 'NULL';
	}

	public function setUtilityBillReceiptTypeId( $intUtilityBillReceiptTypeId ) {
		$this->set( 'm_intUtilityBillReceiptTypeId', CStrings::strToIntDef( $intUtilityBillReceiptTypeId, NULL, false ) );
	}

	public function getUtilityBillReceiptTypeId() {
		return $this->m_intUtilityBillReceiptTypeId;
	}

	public function sqlUtilityBillReceiptTypeId() {
		return ( true == isset( $this->m_intUtilityBillReceiptTypeId ) ) ? ( string ) $this->m_intUtilityBillReceiptTypeId : 'NULL';
	}

	public function setWebRetrievalUrl( $strWebRetrievalUrl ) {
		$this->set( 'm_strWebRetrievalUrl', CStrings::strTrimDef( $strWebRetrievalUrl, 300, NULL, true ) );
	}

	public function getWebRetrievalUrl() {
		return $this->m_strWebRetrievalUrl;
	}

	public function sqlWebRetrievalUrl() {
		return ( true == isset( $this->m_strWebRetrievalUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebRetrievalUrl ) : '\'' . addslashes( $this->m_strWebRetrievalUrl ) . '\'' ) : 'NULL';
	}

	public function setWebRetrievalUsername( $strWebRetrievalUsername ) {
		$this->set( 'm_strWebRetrievalUsername', CStrings::strTrimDef( $strWebRetrievalUsername, 240, NULL, true ) );
	}

	public function getWebRetrievalUsername() {
		return $this->m_strWebRetrievalUsername;
	}

	public function sqlWebRetrievalUsername() {
		return ( true == isset( $this->m_strWebRetrievalUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebRetrievalUsername ) : '\'' . addslashes( $this->m_strWebRetrievalUsername ) . '\'' ) : 'NULL';
	}

	public function setWebRetrievalPassword( $strWebRetrievalPassword ) {
		$this->set( 'm_strWebRetrievalPassword', CStrings::strTrimDef( $strWebRetrievalPassword, -1, NULL, true ) );
	}

	public function getWebRetrievalPassword() {
		return $this->m_strWebRetrievalPassword;
	}

	public function sqlWebRetrievalPassword() {
		return ( true == isset( $this->m_strWebRetrievalPassword ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebRetrievalPassword ) : '\'' . addslashes( $this->m_strWebRetrievalPassword ) . '\'' ) : 'NULL';
	}

	public function setWebRetrievalInstructions( $strWebRetrievalInstructions ) {
		$this->set( 'm_strWebRetrievalInstructions', CStrings::strTrimDef( $strWebRetrievalInstructions, -1, NULL, true ) );
	}

	public function getWebRetrievalInstructions() {
		return $this->m_strWebRetrievalInstructions;
	}

	public function sqlWebRetrievalInstructions() {
		return ( true == isset( $this->m_strWebRetrievalInstructions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebRetrievalInstructions ) : '\'' . addslashes( $this->m_strWebRetrievalInstructions ) . '\'' ) : 'NULL';
	}

	public function setWebRetrievalQuestionAnswers( $strWebRetrievalQuestionAnswers ) {
		$this->set( 'm_strWebRetrievalQuestionAnswers', CStrings::strTrimDef( $strWebRetrievalQuestionAnswers, -1, NULL, true ) );
	}

	public function getWebRetrievalQuestionAnswers() {
		return $this->m_strWebRetrievalQuestionAnswers;
	}

	public function sqlWebRetrievalQuestionAnswers() {
		return ( true == isset( $this->m_strWebRetrievalQuestionAnswers ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebRetrievalQuestionAnswers ) : '\'' . addslashes( $this->m_strWebRetrievalQuestionAnswers ) . '\'' ) : 'NULL';
	}

	public function setEntrataAddressOn( $strEntrataAddressOn ) {
		$this->set( 'm_strEntrataAddressOn', CStrings::strTrimDef( $strEntrataAddressOn, -1, NULL, true ) );
	}

	public function getEntrataAddressOn() {
		return $this->m_strEntrataAddressOn;
	}

	public function sqlEntrataAddressOn() {
		return ( true == isset( $this->m_strEntrataAddressOn ) ) ? '\'' . $this->m_strEntrataAddressOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setExpectedBillDate( $strExpectedBillDate ) {
		$this->set( 'm_strExpectedBillDate', CStrings::strTrimDef( $strExpectedBillDate, -1, NULL, true ) );
	}

	public function getExpectedBillDate() {
		return $this->m_strExpectedBillDate;
	}

	public function sqlExpectedBillDate() {
		return ( true == isset( $this->m_strExpectedBillDate ) ) ? '\'' . $this->m_strExpectedBillDate . '\'' : 'NULL';
	}

	public function setExpectedDueDate( $strExpectedDueDate ) {
		$this->set( 'm_strExpectedDueDate', CStrings::strTrimDef( $strExpectedDueDate, -1, NULL, true ) );
	}

	public function getExpectedDueDate() {
		return $this->m_strExpectedDueDate;
	}

	public function sqlExpectedDueDate() {
		return ( true == isset( $this->m_strExpectedDueDate ) ) ? '\'' . $this->m_strExpectedDueDate . '\'' : 'NULL';
	}

	public function setBillDayStandardDeviation( $fltBillDayStandardDeviation ) {
		$this->set( 'm_fltBillDayStandardDeviation', CStrings::strToFloatDef( $fltBillDayStandardDeviation, NULL, false, 4 ) );
	}

	public function getBillDayStandardDeviation() {
		return $this->m_fltBillDayStandardDeviation;
	}

	public function sqlBillDayStandardDeviation() {
		return ( true == isset( $this->m_fltBillDayStandardDeviation ) ) ? ( string ) $this->m_fltBillDayStandardDeviation : '0';
	}

	public function setDueDayStandardDeviation( $fltDueDayStandardDeviation ) {
		$this->set( 'm_fltDueDayStandardDeviation', CStrings::strToFloatDef( $fltDueDayStandardDeviation, NULL, false, 4 ) );
	}

	public function getDueDayStandardDeviation() {
		return $this->m_fltDueDayStandardDeviation;
	}

	public function sqlDueDayStandardDeviation() {
		return ( true == isset( $this->m_fltDueDayStandardDeviation ) ) ? ( string ) $this->m_fltDueDayStandardDeviation : '0';
	}

	public function setAverageDifferenceBillDays( $intAverageDifferenceBillDays ) {
		$this->set( 'm_intAverageDifferenceBillDays', CStrings::strToIntDef( $intAverageDifferenceBillDays, NULL, false ) );
	}

	public function getAverageDifferenceBillDays() {
		return $this->m_intAverageDifferenceBillDays;
	}

	public function sqlAverageDifferenceBillDays() {
		return ( true == isset( $this->m_intAverageDifferenceBillDays ) ) ? ( string ) $this->m_intAverageDifferenceBillDays : '0';
	}

	public function setAverageDifferenceDueDays( $intAverageDifferenceDueDays ) {
		$this->set( 'm_intAverageDifferenceDueDays', CStrings::strToIntDef( $intAverageDifferenceDueDays, NULL, false ) );
	}

	public function getAverageDifferenceDueDays() {
		return $this->m_intAverageDifferenceDueDays;
	}

	public function sqlAverageDifferenceDueDays() {
		return ( true == isset( $this->m_intAverageDifferenceDueDays ) ) ? ( string ) $this->m_intAverageDifferenceDueDays : '0';
	}

	public function setUtilityCredentialId( $intUtilityCredentialId ) {
		$this->set( 'm_intUtilityCredentialId', CStrings::strToIntDef( $intUtilityCredentialId, NULL, false ) );
	}

	public function getUtilityCredentialId() {
		return $this->m_intUtilityCredentialId;
	}

	public function sqlUtilityCredentialId() {
		return ( true == isset( $this->m_intUtilityCredentialId ) ) ? ( string ) $this->m_intUtilityCredentialId : 'NULL';
	}

	public function setHasEntrataAddress( $boolHasEntrataAddress ) {
		$this->set( 'm_boolHasEntrataAddress', CStrings::strToBool( $boolHasEntrataAddress ) );
	}

	public function getHasEntrataAddress() {
		return $this->m_boolHasEntrataAddress;
	}

	public function sqlHasEntrataAddress() {
		return ( true == isset( $this->m_boolHasEntrataAddress ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasEntrataAddress ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasEntrataCid( $boolHasEntrataCid ) {
		$this->set( 'm_boolHasEntrataCid', CStrings::strToBool( $boolHasEntrataCid ) );
	}

	public function getHasEntrataCid() {
		return $this->m_boolHasEntrataCid;
	}

	public function sqlHasEntrataCid() {
		return ( true == isset( $this->m_boolHasEntrataCid ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasEntrataCid ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_provider_id, utility_bill_receipt_type_id, web_retrieval_url, web_retrieval_username, web_retrieval_password, web_retrieval_instructions, web_retrieval_question_answers, entrata_address_on, updated_by, updated_on, created_by, created_on, expected_bill_date, expected_due_date, bill_day_standard_deviation, due_day_standard_deviation, average_difference_bill_days, average_difference_due_days, utility_credential_id, has_entrata_address, has_entrata_cid )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlUtilityProviderId() . ', ' .
		          $this->sqlUtilityBillReceiptTypeId() . ', ' .
		          $this->sqlWebRetrievalUrl() . ', ' .
		          $this->sqlWebRetrievalUsername() . ', ' .
		          $this->sqlWebRetrievalPassword() . ', ' .
		          $this->sqlWebRetrievalInstructions() . ', ' .
		          $this->sqlWebRetrievalQuestionAnswers() . ', ' .
		          $this->sqlEntrataAddressOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlExpectedBillDate() . ', ' .
		          $this->sqlExpectedDueDate() . ', ' .
		          $this->sqlBillDayStandardDeviation() . ', ' .
		          $this->sqlDueDayStandardDeviation() . ', ' .
		          $this->sqlAverageDifferenceBillDays() . ', ' .
		          $this->sqlAverageDifferenceDueDays() . ', ' .
		          $this->sqlUtilityCredentialId() . ', ' .
		          $this->sqlHasEntrataAddress() . ', ' .
		          $this->sqlHasEntrataCid() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId(). ',' ; } elseif( true == array_key_exists( 'UtilityProviderId', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_receipt_type_id = ' . $this->sqlUtilityBillReceiptTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillReceiptTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_receipt_type_id = ' . $this->sqlUtilityBillReceiptTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_retrieval_url = ' . $this->sqlWebRetrievalUrl(). ',' ; } elseif( true == array_key_exists( 'WebRetrievalUrl', $this->getChangedColumns() ) ) { $strSql .= ' web_retrieval_url = ' . $this->sqlWebRetrievalUrl() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_retrieval_username = ' . $this->sqlWebRetrievalUsername(). ',' ; } elseif( true == array_key_exists( 'WebRetrievalUsername', $this->getChangedColumns() ) ) { $strSql .= ' web_retrieval_username = ' . $this->sqlWebRetrievalUsername() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_retrieval_password = ' . $this->sqlWebRetrievalPassword(). ',' ; } elseif( true == array_key_exists( 'WebRetrievalPassword', $this->getChangedColumns() ) ) { $strSql .= ' web_retrieval_password = ' . $this->sqlWebRetrievalPassword() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_retrieval_instructions = ' . $this->sqlWebRetrievalInstructions(). ',' ; } elseif( true == array_key_exists( 'WebRetrievalInstructions', $this->getChangedColumns() ) ) { $strSql .= ' web_retrieval_instructions = ' . $this->sqlWebRetrievalInstructions() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_retrieval_question_answers = ' . $this->sqlWebRetrievalQuestionAnswers(). ',' ; } elseif( true == array_key_exists( 'WebRetrievalQuestionAnswers', $this->getChangedColumns() ) ) { $strSql .= ' web_retrieval_question_answers = ' . $this->sqlWebRetrievalQuestionAnswers() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_address_on = ' . $this->sqlEntrataAddressOn(). ',' ; } elseif( true == array_key_exists( 'EntrataAddressOn', $this->getChangedColumns() ) ) { $strSql .= ' entrata_address_on = ' . $this->sqlEntrataAddressOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_bill_date = ' . $this->sqlExpectedBillDate(). ',' ; } elseif( true == array_key_exists( 'ExpectedBillDate', $this->getChangedColumns() ) ) { $strSql .= ' expected_bill_date = ' . $this->sqlExpectedBillDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_due_date = ' . $this->sqlExpectedDueDate(). ',' ; } elseif( true == array_key_exists( 'ExpectedDueDate', $this->getChangedColumns() ) ) { $strSql .= ' expected_due_date = ' . $this->sqlExpectedDueDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_day_standard_deviation = ' . $this->sqlBillDayStandardDeviation(). ',' ; } elseif( true == array_key_exists( 'BillDayStandardDeviation', $this->getChangedColumns() ) ) { $strSql .= ' bill_day_standard_deviation = ' . $this->sqlBillDayStandardDeviation() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_day_standard_deviation = ' . $this->sqlDueDayStandardDeviation(). ',' ; } elseif( true == array_key_exists( 'DueDayStandardDeviation', $this->getChangedColumns() ) ) { $strSql .= ' due_day_standard_deviation = ' . $this->sqlDueDayStandardDeviation() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_difference_bill_days = ' . $this->sqlAverageDifferenceBillDays(). ',' ; } elseif( true == array_key_exists( 'AverageDifferenceBillDays', $this->getChangedColumns() ) ) { $strSql .= ' average_difference_bill_days = ' . $this->sqlAverageDifferenceBillDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_difference_due_days = ' . $this->sqlAverageDifferenceDueDays(). ',' ; } elseif( true == array_key_exists( 'AverageDifferenceDueDays', $this->getChangedColumns() ) ) { $strSql .= ' average_difference_due_days = ' . $this->sqlAverageDifferenceDueDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_credential_id = ' . $this->sqlUtilityCredentialId(). ',' ; } elseif( true == array_key_exists( 'UtilityCredentialId', $this->getChangedColumns() ) ) { $strSql .= ' utility_credential_id = ' . $this->sqlUtilityCredentialId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_entrata_address = ' . $this->sqlHasEntrataAddress(). ',' ; } elseif( true == array_key_exists( 'HasEntrataAddress', $this->getChangedColumns() ) ) { $strSql .= ' has_entrata_address = ' . $this->sqlHasEntrataAddress() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_entrata_cid = ' . $this->sqlHasEntrataCid(). ',' ; } elseif( true == array_key_exists( 'HasEntrataCid', $this->getChangedColumns() ) ) { $strSql .= ' has_entrata_cid = ' . $this->sqlHasEntrataCid() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_provider_id' => $this->getUtilityProviderId(),
			'utility_bill_receipt_type_id' => $this->getUtilityBillReceiptTypeId(),
			'web_retrieval_url' => $this->getWebRetrievalUrl(),
			'web_retrieval_username' => $this->getWebRetrievalUsername(),
			'web_retrieval_password' => $this->getWebRetrievalPassword(),
			'web_retrieval_instructions' => $this->getWebRetrievalInstructions(),
			'web_retrieval_question_answers' => $this->getWebRetrievalQuestionAnswers(),
			'entrata_address_on' => $this->getEntrataAddressOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'expected_bill_date' => $this->getExpectedBillDate(),
			'expected_due_date' => $this->getExpectedDueDate(),
			'bill_day_standard_deviation' => $this->getBillDayStandardDeviation(),
			'due_day_standard_deviation' => $this->getDueDayStandardDeviation(),
			'average_difference_bill_days' => $this->getAverageDifferenceBillDays(),
			'average_difference_due_days' => $this->getAverageDifferenceDueDays(),
			'utility_credential_id' => $this->getUtilityCredentialId(),
			'has_entrata_address' => $this->getHasEntrataAddress(),
			'has_entrata_cid' => $this->getHasEntrataCid()
		);
	}

}
?>