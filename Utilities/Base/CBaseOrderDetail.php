<?php

class CBaseOrderDetail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.order_details';

	protected $m_intId;
	protected $m_intVendorId;
	protected $m_intBuyerLocationId;
	protected $m_intOrderHeaderId;
	protected $m_intCid;
	protected $m_intApDetailId;
	protected $m_intOrderDetailId;
	protected $m_strTransactionDatetime;
	protected $m_fltQuantityOrdered;
	protected $m_fltRate;
	protected $m_fltSubtotalAmount;
	protected $m_fltTaxAmount;
	protected $m_fltDiscountAmount;
	protected $m_fltShippingAmount;
	protected $m_fltPreApprovalAmount;
	protected $m_fltTransactionAmount;
	protected $m_fltTransactionAmountDue;
	protected $m_strDescription;
	protected $m_intInvoicedBy;
	protected $m_strInvoicedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strVendorSku;
	protected $m_strCatalogItemName;
	protected $m_strPurchasingUnitOfMeasure;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_fltQuantityOrdered = '1';
		$this->m_fltRate = '0';
		$this->m_fltSubtotalAmount = ( 0 );
		$this->m_fltTaxAmount = ( 0 );
		$this->m_fltDiscountAmount = ( 0 );
		$this->m_fltShippingAmount = ( 0 );
		$this->m_fltPreApprovalAmount = '0';
		$this->m_fltTransactionAmount = '0';
		$this->m_fltTransactionAmountDue = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['buyer_location_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerLocationId', trim( $arrValues['buyer_location_id'] ) ); elseif( isset( $arrValues['buyer_location_id'] ) ) $this->setBuyerLocationId( $arrValues['buyer_location_id'] );
		if( isset( $arrValues['order_header_id'] ) && $boolDirectSet ) $this->set( 'm_intOrderHeaderId', trim( $arrValues['order_header_id'] ) ); elseif( isset( $arrValues['order_header_id'] ) ) $this->setOrderHeaderId( $arrValues['order_header_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intApDetailId', trim( $arrValues['ap_detail_id'] ) ); elseif( isset( $arrValues['ap_detail_id'] ) ) $this->setApDetailId( $arrValues['ap_detail_id'] );
		if( isset( $arrValues['order_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intOrderDetailId', trim( $arrValues['order_detail_id'] ) ); elseif( isset( $arrValues['order_detail_id'] ) ) $this->setOrderDetailId( $arrValues['order_detail_id'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['quantity_ordered'] ) && $boolDirectSet ) $this->set( 'm_fltQuantityOrdered', trim( $arrValues['quantity_ordered'] ) ); elseif( isset( $arrValues['quantity_ordered'] ) ) $this->setQuantityOrdered( $arrValues['quantity_ordered'] );
		if( isset( $arrValues['rate'] ) && $boolDirectSet ) $this->set( 'm_fltRate', trim( $arrValues['rate'] ) ); elseif( isset( $arrValues['rate'] ) ) $this->setRate( $arrValues['rate'] );
		if( isset( $arrValues['subtotal_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSubtotalAmount', trim( $arrValues['subtotal_amount'] ) ); elseif( isset( $arrValues['subtotal_amount'] ) ) $this->setSubtotalAmount( $arrValues['subtotal_amount'] );
		if( isset( $arrValues['tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTaxAmount', trim( $arrValues['tax_amount'] ) ); elseif( isset( $arrValues['tax_amount'] ) ) $this->setTaxAmount( $arrValues['tax_amount'] );
		if( isset( $arrValues['discount_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDiscountAmount', trim( $arrValues['discount_amount'] ) ); elseif( isset( $arrValues['discount_amount'] ) ) $this->setDiscountAmount( $arrValues['discount_amount'] );
		if( isset( $arrValues['shipping_amount'] ) && $boolDirectSet ) $this->set( 'm_fltShippingAmount', trim( $arrValues['shipping_amount'] ) ); elseif( isset( $arrValues['shipping_amount'] ) ) $this->setShippingAmount( $arrValues['shipping_amount'] );
		if( isset( $arrValues['pre_approval_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPreApprovalAmount', trim( $arrValues['pre_approval_amount'] ) ); elseif( isset( $arrValues['pre_approval_amount'] ) ) $this->setPreApprovalAmount( $arrValues['pre_approval_amount'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['transaction_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmountDue', trim( $arrValues['transaction_amount_due'] ) ); elseif( isset( $arrValues['transaction_amount_due'] ) ) $this->setTransactionAmountDue( $arrValues['transaction_amount_due'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['invoiced_by'] ) && $boolDirectSet ) $this->set( 'm_intInvoicedBy', trim( $arrValues['invoiced_by'] ) ); elseif( isset( $arrValues['invoiced_by'] ) ) $this->setInvoicedBy( $arrValues['invoiced_by'] );
		if( isset( $arrValues['invoiced_on'] ) && $boolDirectSet ) $this->set( 'm_strInvoicedOn', trim( $arrValues['invoiced_on'] ) ); elseif( isset( $arrValues['invoiced_on'] ) ) $this->setInvoicedOn( $arrValues['invoiced_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['vendor_sku'] ) && $boolDirectSet ) $this->set( 'm_strVendorSku', trim( $arrValues['vendor_sku'] ) ); elseif( isset( $arrValues['vendor_sku'] ) ) $this->setVendorSku( $arrValues['vendor_sku'] );
		if( isset( $arrValues['catalog_item_name'] ) && $boolDirectSet ) $this->set( 'm_strCatalogItemName', trim( $arrValues['catalog_item_name'] ) ); elseif( isset( $arrValues['catalog_item_name'] ) ) $this->setCatalogItemName( $arrValues['catalog_item_name'] );
		if( isset( $arrValues['purchasing_unit_of_measure'] ) && $boolDirectSet ) $this->set( 'm_strPurchasingUnitOfMeasure', trim( $arrValues['purchasing_unit_of_measure'] ) ); elseif( isset( $arrValues['purchasing_unit_of_measure'] ) ) $this->setPurchasingUnitOfMeasure( $arrValues['purchasing_unit_of_measure'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setBuyerLocationId( $intBuyerLocationId ) {
		$this->set( 'm_intBuyerLocationId', CStrings::strToIntDef( $intBuyerLocationId, NULL, false ) );
	}

	public function getBuyerLocationId() {
		return $this->m_intBuyerLocationId;
	}

	public function sqlBuyerLocationId() {
		return ( true == isset( $this->m_intBuyerLocationId ) ) ? ( string ) $this->m_intBuyerLocationId : 'NULL';
	}

	public function setOrderHeaderId( $intOrderHeaderId ) {
		$this->set( 'm_intOrderHeaderId', CStrings::strToIntDef( $intOrderHeaderId, NULL, false ) );
	}

	public function getOrderHeaderId() {
		return $this->m_intOrderHeaderId;
	}

	public function sqlOrderHeaderId() {
		return ( true == isset( $this->m_intOrderHeaderId ) ) ? ( string ) $this->m_intOrderHeaderId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApDetailId( $intApDetailId ) {
		$this->set( 'm_intApDetailId', CStrings::strToIntDef( $intApDetailId, NULL, false ) );
	}

	public function getApDetailId() {
		return $this->m_intApDetailId;
	}

	public function sqlApDetailId() {
		return ( true == isset( $this->m_intApDetailId ) ) ? ( string ) $this->m_intApDetailId : 'NULL';
	}

	public function setOrderDetailId( $intOrderDetailId ) {
		$this->set( 'm_intOrderDetailId', CStrings::strToIntDef( $intOrderDetailId, NULL, false ) );
	}

	public function getOrderDetailId() {
		return $this->m_intOrderDetailId;
	}

	public function sqlOrderDetailId() {
		return ( true == isset( $this->m_intOrderDetailId ) ) ? ( string ) $this->m_intOrderDetailId : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setQuantityOrdered( $fltQuantityOrdered ) {
		$this->set( 'm_fltQuantityOrdered', CStrings::strToFloatDef( $fltQuantityOrdered, NULL, false, 3 ) );
	}

	public function getQuantityOrdered() {
		return $this->m_fltQuantityOrdered;
	}

	public function sqlQuantityOrdered() {
		return ( true == isset( $this->m_fltQuantityOrdered ) ) ? ( string ) $this->m_fltQuantityOrdered : '1';
	}

	public function setRate( $fltRate ) {
		$this->set( 'm_fltRate', CStrings::strToFloatDef( $fltRate, NULL, false, 3 ) );
	}

	public function getRate() {
		return $this->m_fltRate;
	}

	public function sqlRate() {
		return ( true == isset( $this->m_fltRate ) ) ? ( string ) $this->m_fltRate : '0';
	}

	public function setSubtotalAmount( $fltSubtotalAmount ) {
		$this->set( 'm_fltSubtotalAmount', CStrings::strToFloatDef( $fltSubtotalAmount, NULL, false, 2 ) );
	}

	public function getSubtotalAmount() {
		return $this->m_fltSubtotalAmount;
	}

	public function sqlSubtotalAmount() {
		return ( true == isset( $this->m_fltSubtotalAmount ) ) ? ( string ) $this->m_fltSubtotalAmount : '( 0 )::numeric';
	}

	public function setTaxAmount( $fltTaxAmount ) {
		$this->set( 'm_fltTaxAmount', CStrings::strToFloatDef( $fltTaxAmount, NULL, false, 2 ) );
	}

	public function getTaxAmount() {
		return $this->m_fltTaxAmount;
	}

	public function sqlTaxAmount() {
		return ( true == isset( $this->m_fltTaxAmount ) ) ? ( string ) $this->m_fltTaxAmount : '( 0 )::numeric';
	}

	public function setDiscountAmount( $fltDiscountAmount ) {
		$this->set( 'm_fltDiscountAmount', CStrings::strToFloatDef( $fltDiscountAmount, NULL, false, 2 ) );
	}

	public function getDiscountAmount() {
		return $this->m_fltDiscountAmount;
	}

	public function sqlDiscountAmount() {
		return ( true == isset( $this->m_fltDiscountAmount ) ) ? ( string ) $this->m_fltDiscountAmount : '( 0 )::numeric';
	}

	public function setShippingAmount( $fltShippingAmount ) {
		$this->set( 'm_fltShippingAmount', CStrings::strToFloatDef( $fltShippingAmount, NULL, false, 2 ) );
	}

	public function getShippingAmount() {
		return $this->m_fltShippingAmount;
	}

	public function sqlShippingAmount() {
		return ( true == isset( $this->m_fltShippingAmount ) ) ? ( string ) $this->m_fltShippingAmount : '( 0 )::numeric';
	}

	public function setPreApprovalAmount( $fltPreApprovalAmount ) {
		$this->set( 'm_fltPreApprovalAmount', CStrings::strToFloatDef( $fltPreApprovalAmount, NULL, false, 2 ) );
	}

	public function getPreApprovalAmount() {
		return $this->m_fltPreApprovalAmount;
	}

	public function sqlPreApprovalAmount() {
		return ( true == isset( $this->m_fltPreApprovalAmount ) ) ? ( string ) $this->m_fltPreApprovalAmount : '0';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : '0';
	}

	public function setTransactionAmountDue( $fltTransactionAmountDue ) {
		$this->set( 'm_fltTransactionAmountDue', CStrings::strToFloatDef( $fltTransactionAmountDue, NULL, false, 2 ) );
	}

	public function getTransactionAmountDue() {
		return $this->m_fltTransactionAmountDue;
	}

	public function sqlTransactionAmountDue() {
		return ( true == isset( $this->m_fltTransactionAmountDue ) ) ? ( string ) $this->m_fltTransactionAmountDue : '0';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setInvoicedBy( $intInvoicedBy ) {
		$this->set( 'm_intInvoicedBy', CStrings::strToIntDef( $intInvoicedBy, NULL, false ) );
	}

	public function getInvoicedBy() {
		return $this->m_intInvoicedBy;
	}

	public function sqlInvoicedBy() {
		return ( true == isset( $this->m_intInvoicedBy ) ) ? ( string ) $this->m_intInvoicedBy : 'NULL';
	}

	public function setInvoicedOn( $strInvoicedOn ) {
		$this->set( 'm_strInvoicedOn', CStrings::strTrimDef( $strInvoicedOn, -1, NULL, true ) );
	}

	public function getInvoicedOn() {
		return $this->m_strInvoicedOn;
	}

	public function sqlInvoicedOn() {
		return ( true == isset( $this->m_strInvoicedOn ) ) ? '\'' . $this->m_strInvoicedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setVendorSku( $strVendorSku ) {
		$this->set( 'm_strVendorSku', CStrings::strTrimDef( $strVendorSku, 50, NULL, true ) );
	}

	public function getVendorSku() {
		return $this->m_strVendorSku;
	}

	public function sqlVendorSku() {
		return ( true == isset( $this->m_strVendorSku ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVendorSku ) : '\'' . addslashes( $this->m_strVendorSku ) . '\'' ) : 'NULL';
	}

	public function setCatalogItemName( $strCatalogItemName ) {
		$this->set( 'm_strCatalogItemName', CStrings::strTrimDef( $strCatalogItemName, 240, NULL, true ) );
	}

	public function getCatalogItemName() {
		return $this->m_strCatalogItemName;
	}

	public function sqlCatalogItemName() {
		return ( true == isset( $this->m_strCatalogItemName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCatalogItemName ) : '\'' . addslashes( $this->m_strCatalogItemName ) . '\'' ) : 'NULL';
	}

	public function setPurchasingUnitOfMeasure( $strPurchasingUnitOfMeasure ) {
		$this->set( 'm_strPurchasingUnitOfMeasure', CStrings::strTrimDef( $strPurchasingUnitOfMeasure, 100, NULL, true ) );
	}

	public function getPurchasingUnitOfMeasure() {
		return $this->m_strPurchasingUnitOfMeasure;
	}

	public function sqlPurchasingUnitOfMeasure() {
		return ( true == isset( $this->m_strPurchasingUnitOfMeasure ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPurchasingUnitOfMeasure ) : '\'' . addslashes( $this->m_strPurchasingUnitOfMeasure ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_id, buyer_location_id, order_header_id, cid, ap_detail_id, order_detail_id, transaction_datetime, quantity_ordered, rate, subtotal_amount, tax_amount, discount_amount, shipping_amount, pre_approval_amount, transaction_amount, transaction_amount_due, description, invoiced_by, invoiced_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, vendor_sku, catalog_item_name, purchasing_unit_of_measure, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlVendorId() . ', ' .
						$this->sqlBuyerLocationId() . ', ' .
						$this->sqlOrderHeaderId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApDetailId() . ', ' .
						$this->sqlOrderDetailId() . ', ' .
						$this->sqlTransactionDatetime() . ', ' .
						$this->sqlQuantityOrdered() . ', ' .
						$this->sqlRate() . ', ' .
						$this->sqlSubtotalAmount() . ', ' .
						$this->sqlTaxAmount() . ', ' .
						$this->sqlDiscountAmount() . ', ' .
						$this->sqlShippingAmount() . ', ' .
						$this->sqlPreApprovalAmount() . ', ' .
						$this->sqlTransactionAmount() . ', ' .
						$this->sqlTransactionAmountDue() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlInvoicedBy() . ', ' .
						$this->sqlInvoicedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlVendorSku() . ', ' .
						$this->sqlCatalogItemName() . ', ' .
						$this->sqlPurchasingUnitOfMeasure() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId(). ',' ; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_location_id = ' . $this->sqlBuyerLocationId(). ',' ; } elseif( true == array_key_exists( 'BuyerLocationId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_location_id = ' . $this->sqlBuyerLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_header_id = ' . $this->sqlOrderHeaderId(). ',' ; } elseif( true == array_key_exists( 'OrderHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' order_header_id = ' . $this->sqlOrderHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId(). ',' ; } elseif( true == array_key_exists( 'ApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_detail_id = ' . $this->sqlOrderDetailId(). ',' ; } elseif( true == array_key_exists( 'OrderDetailId', $this->getChangedColumns() ) ) { $strSql .= ' order_detail_id = ' . $this->sqlOrderDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime(). ',' ; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quantity_ordered = ' . $this->sqlQuantityOrdered(). ',' ; } elseif( true == array_key_exists( 'QuantityOrdered', $this->getChangedColumns() ) ) { $strSql .= ' quantity_ordered = ' . $this->sqlQuantityOrdered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate = ' . $this->sqlRate(). ',' ; } elseif( true == array_key_exists( 'Rate', $this->getChangedColumns() ) ) { $strSql .= ' rate = ' . $this->sqlRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subtotal_amount = ' . $this->sqlSubtotalAmount(). ',' ; } elseif( true == array_key_exists( 'SubtotalAmount', $this->getChangedColumns() ) ) { $strSql .= ' subtotal_amount = ' . $this->sqlSubtotalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_amount = ' . $this->sqlTaxAmount(). ',' ; } elseif( true == array_key_exists( 'TaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' tax_amount = ' . $this->sqlTaxAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discount_amount = ' . $this->sqlDiscountAmount(). ',' ; } elseif( true == array_key_exists( 'DiscountAmount', $this->getChangedColumns() ) ) { $strSql .= ' discount_amount = ' . $this->sqlDiscountAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' shipping_amount = ' . $this->sqlShippingAmount(). ',' ; } elseif( true == array_key_exists( 'ShippingAmount', $this->getChangedColumns() ) ) { $strSql .= ' shipping_amount = ' . $this->sqlShippingAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_approval_amount = ' . $this->sqlPreApprovalAmount(). ',' ; } elseif( true == array_key_exists( 'PreApprovalAmount', $this->getChangedColumns() ) ) { $strSql .= ' pre_approval_amount = ' . $this->sqlPreApprovalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount_due = ' . $this->sqlTransactionAmountDue(). ',' ; } elseif( true == array_key_exists( 'TransactionAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount_due = ' . $this->sqlTransactionAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoiced_by = ' . $this->sqlInvoicedBy(). ',' ; } elseif( true == array_key_exists( 'InvoicedBy', $this->getChangedColumns() ) ) { $strSql .= ' invoiced_by = ' . $this->sqlInvoicedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoiced_on = ' . $this->sqlInvoicedOn(). ',' ; } elseif( true == array_key_exists( 'InvoicedOn', $this->getChangedColumns() ) ) { $strSql .= ' invoiced_on = ' . $this->sqlInvoicedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_sku = ' . $this->sqlVendorSku(). ',' ; } elseif( true == array_key_exists( 'VendorSku', $this->getChangedColumns() ) ) { $strSql .= ' vendor_sku = ' . $this->sqlVendorSku() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' catalog_item_name = ' . $this->sqlCatalogItemName(). ',' ; } elseif( true == array_key_exists( 'CatalogItemName', $this->getChangedColumns() ) ) { $strSql .= ' catalog_item_name = ' . $this->sqlCatalogItemName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchasing_unit_of_measure = ' . $this->sqlPurchasingUnitOfMeasure(). ',' ; } elseif( true == array_key_exists( 'PurchasingUnitOfMeasure', $this->getChangedColumns() ) ) { $strSql .= ' purchasing_unit_of_measure = ' . $this->sqlPurchasingUnitOfMeasure() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_id' => $this->getVendorId(),
			'buyer_location_id' => $this->getBuyerLocationId(),
			'order_header_id' => $this->getOrderHeaderId(),
			'cid' => $this->getCid(),
			'ap_detail_id' => $this->getApDetailId(),
			'order_detail_id' => $this->getOrderDetailId(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'quantity_ordered' => $this->getQuantityOrdered(),
			'rate' => $this->getRate(),
			'subtotal_amount' => $this->getSubtotalAmount(),
			'tax_amount' => $this->getTaxAmount(),
			'discount_amount' => $this->getDiscountAmount(),
			'shipping_amount' => $this->getShippingAmount(),
			'pre_approval_amount' => $this->getPreApprovalAmount(),
			'transaction_amount' => $this->getTransactionAmount(),
			'transaction_amount_due' => $this->getTransactionAmountDue(),
			'description' => $this->getDescription(),
			'invoiced_by' => $this->getInvoicedBy(),
			'invoiced_on' => $this->getInvoicedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'vendor_sku' => $this->getVendorSku(),
			'catalog_item_name' => $this->getCatalogItemName(),
			'purchasing_unit_of_measure' => $this->getPurchasingUnitOfMeasure(),
			'details' => $this->getDetails()
		);
	}

}
?>