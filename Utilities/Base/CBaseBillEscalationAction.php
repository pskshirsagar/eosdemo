<?php

class CBaseBillEscalationAction extends CEosSingularBase {

	const TABLE_NAME = 'public.bill_escalation_actions';

	protected $m_intId;
	protected $m_intBillEscalationId;
	protected $m_intBillEscalateStepId;
	protected $m_strMemo;
	protected $m_strBillEscalationActionDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['bill_escalation_id'] ) && $boolDirectSet ) $this->set( 'm_intBillEscalationId', trim( $arrValues['bill_escalation_id'] ) ); elseif( isset( $arrValues['bill_escalation_id'] ) ) $this->setBillEscalationId( $arrValues['bill_escalation_id'] );
		if( isset( $arrValues['bill_escalate_step_id'] ) && $boolDirectSet ) $this->set( 'm_intBillEscalateStepId', trim( $arrValues['bill_escalate_step_id'] ) ); elseif( isset( $arrValues['bill_escalate_step_id'] ) ) $this->setBillEscalateStepId( $arrValues['bill_escalate_step_id'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['bill_escalation_action_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBillEscalationActionDatetime', trim( $arrValues['bill_escalation_action_datetime'] ) ); elseif( isset( $arrValues['bill_escalation_action_datetime'] ) ) $this->setBillEscalationActionDatetime( $arrValues['bill_escalation_action_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setBillEscalationId( $intBillEscalationId ) {
		$this->set( 'm_intBillEscalationId', CStrings::strToIntDef( $intBillEscalationId, NULL, false ) );
	}

	public function getBillEscalationId() {
		return $this->m_intBillEscalationId;
	}

	public function sqlBillEscalationId() {
		return ( true == isset( $this->m_intBillEscalationId ) ) ? ( string ) $this->m_intBillEscalationId : 'NULL';
	}

	public function setBillEscalateStepId( $intBillEscalateStepId ) {
		$this->set( 'm_intBillEscalateStepId', CStrings::strToIntDef( $intBillEscalateStepId, NULL, false ) );
	}

	public function getBillEscalateStepId() {
		return $this->m_intBillEscalateStepId;
	}

	public function sqlBillEscalateStepId() {
		return ( true == isset( $this->m_intBillEscalateStepId ) ) ? ( string ) $this->m_intBillEscalateStepId : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, 200, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setBillEscalationActionDatetime( $strBillEscalationActionDatetime ) {
		$this->set( 'm_strBillEscalationActionDatetime', CStrings::strTrimDef( $strBillEscalationActionDatetime, -1, NULL, true ) );
	}

	public function getBillEscalationActionDatetime() {
		return $this->m_strBillEscalationActionDatetime;
	}

	public function sqlBillEscalationActionDatetime() {
		return ( true == isset( $this->m_strBillEscalationActionDatetime ) ) ? '\'' . $this->m_strBillEscalationActionDatetime . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, bill_escalation_id, bill_escalate_step_id, memo, bill_escalation_action_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlBillEscalationId() . ', ' .
 						$this->sqlBillEscalateStepId() . ', ' .
 						$this->sqlMemo() . ', ' .
 						$this->sqlBillEscalationActionDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_escalation_id = ' . $this->sqlBillEscalationId() . ','; } elseif( true == array_key_exists( 'BillEscalationId', $this->getChangedColumns() ) ) { $strSql .= ' bill_escalation_id = ' . $this->sqlBillEscalationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_escalate_step_id = ' . $this->sqlBillEscalateStepId() . ','; } elseif( true == array_key_exists( 'BillEscalateStepId', $this->getChangedColumns() ) ) { $strSql .= ' bill_escalate_step_id = ' . $this->sqlBillEscalateStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_escalation_action_datetime = ' . $this->sqlBillEscalationActionDatetime() . ','; } elseif( true == array_key_exists( 'BillEscalationActionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' bill_escalation_action_datetime = ' . $this->sqlBillEscalationActionDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'bill_escalation_id' => $this->getBillEscalationId(),
			'bill_escalate_step_id' => $this->getBillEscalateStepId(),
			'memo' => $this->getMemo(),
			'bill_escalation_action_datetime' => $this->getBillEscalationActionDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>