<?php

class CBaseBillEvent extends CEosSingularBase {

	const TABLE_NAME = 'public.bill_events';

	protected $m_intId;
	protected $m_intUtilityBillId;
	protected $m_intBillEventTypeId;
	protected $m_intBillEventSubTypeId;
	protected $m_intBillEventReferenceId;
	protected $m_intCompanyUserId;
	protected $m_strNewData;
	protected $m_jsonNewData;
	protected $m_strOldData;
	protected $m_jsonOldData;
	protected $m_strEventDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['bill_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillEventTypeId', trim( $arrValues['bill_event_type_id'] ) ); elseif( isset( $arrValues['bill_event_type_id'] ) ) $this->setBillEventTypeId( $arrValues['bill_event_type_id'] );
		if( isset( $arrValues['bill_event_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillEventSubTypeId', trim( $arrValues['bill_event_sub_type_id'] ) ); elseif( isset( $arrValues['bill_event_sub_type_id'] ) ) $this->setBillEventSubTypeId( $arrValues['bill_event_sub_type_id'] );
		if( isset( $arrValues['bill_event_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intBillEventReferenceId', trim( $arrValues['bill_event_reference_id'] ) ); elseif( isset( $arrValues['bill_event_reference_id'] ) ) $this->setBillEventReferenceId( $arrValues['bill_event_reference_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['new_data'] ) ) $this->set( 'm_strNewData', trim( $arrValues['new_data'] ) );
		if( isset( $arrValues['old_data'] ) ) $this->set( 'm_strOldData', trim( $arrValues['old_data'] ) );
		if( isset( $arrValues['event_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEventDatetime', trim( $arrValues['event_datetime'] ) ); elseif( isset( $arrValues['event_datetime'] ) ) $this->setEventDatetime( $arrValues['event_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setBillEventTypeId( $intBillEventTypeId ) {
		$this->set( 'm_intBillEventTypeId', CStrings::strToIntDef( $intBillEventTypeId, NULL, false ) );
	}

	public function getBillEventTypeId() {
		return $this->m_intBillEventTypeId;
	}

	public function sqlBillEventTypeId() {
		return ( true == isset( $this->m_intBillEventTypeId ) ) ? ( string ) $this->m_intBillEventTypeId : 'NULL';
	}

	public function setBillEventSubTypeId( $intBillEventSubTypeId ) {
		$this->set( 'm_intBillEventSubTypeId', CStrings::strToIntDef( $intBillEventSubTypeId, NULL, false ) );
	}

	public function getBillEventSubTypeId() {
		return $this->m_intBillEventSubTypeId;
	}

	public function sqlBillEventSubTypeId() {
		return ( true == isset( $this->m_intBillEventSubTypeId ) ) ? ( string ) $this->m_intBillEventSubTypeId : 'NULL';
	}

	public function setBillEventReferenceId( $intBillEventReferenceId ) {
		$this->set( 'm_intBillEventReferenceId', CStrings::strToIntDef( $intBillEventReferenceId, NULL, false ) );
	}

	public function getBillEventReferenceId() {
		return $this->m_intBillEventReferenceId;
	}

	public function sqlBillEventReferenceId() {
		return ( true == isset( $this->m_intBillEventReferenceId ) ) ? ( string ) $this->m_intBillEventReferenceId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setNewData( $jsonNewData ) {
		if( true == valObj( $jsonNewData, 'stdClass' ) ) {
			$this->set( 'm_jsonNewData', $jsonNewData );
		} elseif( true == valJsonString( $jsonNewData ) ) {
			$this->set( 'm_jsonNewData', CStrings::strToJson( $jsonNewData ) );
		} else {
			$this->set( 'm_jsonNewData', NULL ); 
		}
		unset( $this->m_strNewData );
	}

	public function getNewData() {
		if( true == isset( $this->m_strNewData ) ) {
			$this->m_jsonNewData = CStrings::strToJson( $this->m_strNewData );
			unset( $this->m_strNewData );
		}
		return $this->m_jsonNewData;
	}

	public function sqlNewData() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getNewData() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getNewData() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getNewData() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setOldData( $jsonOldData ) {
		if( true == valObj( $jsonOldData, 'stdClass' ) ) {
			$this->set( 'm_jsonOldData', $jsonOldData );
		} elseif( true == valJsonString( $jsonOldData ) ) {
			$this->set( 'm_jsonOldData', CStrings::strToJson( $jsonOldData ) );
		} else {
			$this->set( 'm_jsonOldData', NULL ); 
		}
		unset( $this->m_strOldData );
	}

	public function getOldData() {
		if( true == isset( $this->m_strOldData ) ) {
			$this->m_jsonOldData = CStrings::strToJson( $this->m_strOldData );
			unset( $this->m_strOldData );
		}
		return $this->m_jsonOldData;
	}

	public function sqlOldData() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getOldData() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getOldData() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getOldData() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setEventDatetime( $strEventDatetime ) {
		$this->set( 'm_strEventDatetime', CStrings::strTrimDef( $strEventDatetime, -1, NULL, true ) );
	}

	public function getEventDatetime() {
		return $this->m_strEventDatetime;
	}

	public function sqlEventDatetime() {
		return ( true == isset( $this->m_strEventDatetime ) ) ? '\'' . $this->m_strEventDatetime . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_bill_id, bill_event_type_id, bill_event_sub_type_id, bill_event_reference_id, company_user_id, new_data, old_data, event_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlBillEventTypeId() . ', ' .
						$this->sqlBillEventSubTypeId() . ', ' .
						$this->sqlBillEventReferenceId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlNewData() . ', ' .
						$this->sqlOldData() . ', ' .
						$this->sqlEventDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_event_type_id = ' . $this->sqlBillEventTypeId(). ',' ; } elseif( true == array_key_exists( 'BillEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bill_event_type_id = ' . $this->sqlBillEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_event_sub_type_id = ' . $this->sqlBillEventSubTypeId(). ',' ; } elseif( true == array_key_exists( 'BillEventSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bill_event_sub_type_id = ' . $this->sqlBillEventSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_event_reference_id = ' . $this->sqlBillEventReferenceId(). ',' ; } elseif( true == array_key_exists( 'BillEventReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' bill_event_reference_id = ' . $this->sqlBillEventReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_data = ' . $this->sqlNewData(). ',' ; } elseif( true == array_key_exists( 'NewData', $this->getChangedColumns() ) ) { $strSql .= ' new_data = ' . $this->sqlNewData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_data = ' . $this->sqlOldData(). ',' ; } elseif( true == array_key_exists( 'OldData', $this->getChangedColumns() ) ) { $strSql .= ' old_data = ' . $this->sqlOldData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_datetime = ' . $this->sqlEventDatetime(). ',' ; } elseif( true == array_key_exists( 'EventDatetime', $this->getChangedColumns() ) ) { $strSql .= ' event_datetime = ' . $this->sqlEventDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'bill_event_type_id' => $this->getBillEventTypeId(),
			'bill_event_sub_type_id' => $this->getBillEventSubTypeId(),
			'bill_event_reference_id' => $this->getBillEventReferenceId(),
			'company_user_id' => $this->getCompanyUserId(),
			'new_data' => $this->getNewData(),
			'old_data' => $this->getOldData(),
			'event_datetime' => $this->getEventDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>