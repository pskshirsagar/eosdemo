<?php

class CBaseUtilityPropertyUnit extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_property_units';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityAccountId;
	protected $m_intUtilityAccountAddressTypeId;
	protected $m_intUnitSpaceId;
	protected $m_intPropertyUnitId;
	protected $m_intPropertyBuildingId;
	protected $m_intTimeZoneId;
	protected $m_strSpaceNumber;
	protected $m_strUnitNumber;
	protected $m_strBuildingName;
	protected $m_fltSquareFeet;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_intOrderNum;
	protected $m_intReturnedInvoiceCount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';
		$this->m_intReturnedInvoiceCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['utility_account_address_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountAddressTypeId', trim( $arrValues['utility_account_address_type_id'] ) ); elseif( isset( $arrValues['utility_account_address_type_id'] ) ) $this->setUtilityAccountAddressTypeId( $arrValues['utility_account_address_type_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['time_zone_id'] ) && $boolDirectSet ) $this->set( 'm_intTimeZoneId', trim( $arrValues['time_zone_id'] ) ); elseif( isset( $arrValues['time_zone_id'] ) ) $this->setTimeZoneId( $arrValues['time_zone_id'] );
		if( isset( $arrValues['space_number'] ) && $boolDirectSet ) $this->set( 'm_strSpaceNumber', trim( stripcslashes( $arrValues['space_number'] ) ) ); elseif( isset( $arrValues['space_number'] ) ) $this->setSpaceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['space_number'] ) : $arrValues['space_number'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['building_name'] ) && $boolDirectSet ) $this->set( 'm_strBuildingName', trim( stripcslashes( $arrValues['building_name'] ) ) ); elseif( isset( $arrValues['building_name'] ) ) $this->setBuildingName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['building_name'] ) : $arrValues['building_name'] );
		if( isset( $arrValues['square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltSquareFeet', trim( $arrValues['square_feet'] ) ); elseif( isset( $arrValues['square_feet'] ) ) $this->setSquareFeet( $arrValues['square_feet'] );
		if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine2', trim( stripcslashes( $arrValues['street_line2'] ) ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
		if( isset( $arrValues['street_line3'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine3', trim( stripcslashes( $arrValues['street_line3'] ) ) ); elseif( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line3'] ) : $arrValues['street_line3'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( stripcslashes( $arrValues['province'] ) ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['returned_invoice_count'] ) && $boolDirectSet ) $this->set( 'm_intReturnedInvoiceCount', trim( $arrValues['returned_invoice_count'] ) ); elseif( isset( $arrValues['returned_invoice_count'] ) ) $this->setReturnedInvoiceCount( $arrValues['returned_invoice_count'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setUtilityAccountAddressTypeId( $intUtilityAccountAddressTypeId ) {
		$this->set( 'm_intUtilityAccountAddressTypeId', CStrings::strToIntDef( $intUtilityAccountAddressTypeId, NULL, false ) );
	}

	public function getUtilityAccountAddressTypeId() {
		return $this->m_intUtilityAccountAddressTypeId;
	}

	public function sqlUtilityAccountAddressTypeId() {
		return ( true == isset( $this->m_intUtilityAccountAddressTypeId ) ) ? ( string ) $this->m_intUtilityAccountAddressTypeId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setTimeZoneId( $intTimeZoneId ) {
		$this->set( 'm_intTimeZoneId', CStrings::strToIntDef( $intTimeZoneId, NULL, false ) );
	}

	public function getTimeZoneId() {
		return $this->m_intTimeZoneId;
	}

	public function sqlTimeZoneId() {
		return ( true == isset( $this->m_intTimeZoneId ) ) ? ( string ) $this->m_intTimeZoneId : 'NULL';
	}

	public function setSpaceNumber( $strSpaceNumber ) {
		$this->set( 'm_strSpaceNumber', CStrings::strTrimDef( $strSpaceNumber, 50, NULL, true ) );
	}

	public function getSpaceNumber() {
		return $this->m_strSpaceNumber;
	}

	public function sqlSpaceNumber() {
		return ( true == isset( $this->m_strSpaceNumber ) ) ? '\'' . addslashes( $this->m_strSpaceNumber ) . '\'' : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setBuildingName( $strBuildingName ) {
		$this->set( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ) );
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function sqlBuildingName() {
		return ( true == isset( $this->m_strBuildingName ) ) ? '\'' . addslashes( $this->m_strBuildingName ) . '\'' : 'NULL';
	}

	public function setSquareFeet( $fltSquareFeet ) {
		$this->set( 'm_fltSquareFeet', CStrings::strToFloatDef( $fltSquareFeet, NULL, false, 4 ) );
	}

	public function getSquareFeet() {
		return $this->m_fltSquareFeet;
	}

	public function sqlSquareFeet() {
		return ( true == isset( $this->m_fltSquareFeet ) ) ? ( string ) $this->m_fltSquareFeet : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->set( 'm_strStreetLine1', CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->set( 'm_strStreetLine2', CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) );
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->set( 'm_strStreetLine3', CStrings::strTrimDef( $strStreetLine3, 100, NULL, true ) );
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function sqlStreetLine3() {
		return ( true == isset( $this->m_strStreetLine3 ) ) ? '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setReturnedInvoiceCount( $intReturnedInvoiceCount ) {
		$this->set( 'm_intReturnedInvoiceCount', CStrings::strToIntDef( $intReturnedInvoiceCount, NULL, false ) );
	}

	public function getReturnedInvoiceCount() {
		return $this->m_intReturnedInvoiceCount;
	}

	public function sqlReturnedInvoiceCount() {
		return ( true == isset( $this->m_intReturnedInvoiceCount ) ) ? ( string ) $this->m_intReturnedInvoiceCount : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_account_id, utility_account_address_type_id, unit_space_id, property_unit_id, property_building_id, time_zone_id, space_number, unit_number, building_name, square_feet, street_line1, street_line2, street_line3, city, state_code, province, postal_code, country_code, order_num, returned_invoice_count, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUtilityAccountId() . ', ' .
 						$this->sqlUtilityAccountAddressTypeId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlPropertyBuildingId() . ', ' .
 						$this->sqlTimeZoneId() . ', ' .
 						$this->sqlSpaceNumber() . ', ' .
 						$this->sqlUnitNumber() . ', ' .
 						$this->sqlBuildingName() . ', ' .
 						$this->sqlSquareFeet() . ', ' .
 						$this->sqlStreetLine1() . ', ' .
 						$this->sqlStreetLine2() . ', ' .
 						$this->sqlStreetLine3() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlProvince() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlCountryCode() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlReturnedInvoiceCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_address_type_id = ' . $this->sqlUtilityAccountAddressTypeId() . ','; } elseif( true == array_key_exists( 'UtilityAccountAddressTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_address_type_id = ' . $this->sqlUtilityAccountAddressTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; } elseif( true == array_key_exists( 'TimeZoneId', $this->getChangedColumns() ) ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' space_number = ' . $this->sqlSpaceNumber() . ','; } elseif( true == array_key_exists( 'SpaceNumber', $this->getChangedColumns() ) ) { $strSql .= ' space_number = ' . $this->sqlSpaceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_name = ' . $this->sqlBuildingName() . ','; } elseif( true == array_key_exists( 'BuildingName', $this->getChangedColumns() ) ) { $strSql .= ' building_name = ' . $this->sqlBuildingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet() . ','; } elseif( true == array_key_exists( 'SquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; } elseif( true == array_key_exists( 'StreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_invoice_count = ' . $this->sqlReturnedInvoiceCount() . ','; } elseif( true == array_key_exists( 'ReturnedInvoiceCount', $this->getChangedColumns() ) ) { $strSql .= ' returned_invoice_count = ' . $this->sqlReturnedInvoiceCount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'utility_account_address_type_id' => $this->getUtilityAccountAddressTypeId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'time_zone_id' => $this->getTimeZoneId(),
			'space_number' => $this->getSpaceNumber(),
			'unit_number' => $this->getUnitNumber(),
			'building_name' => $this->getBuildingName(),
			'square_feet' => $this->getSquareFeet(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'street_line3' => $this->getStreetLine3(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'province' => $this->getProvince(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'order_num' => $this->getOrderNum(),
			'returned_invoice_count' => $this->getReturnedInvoiceCount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>