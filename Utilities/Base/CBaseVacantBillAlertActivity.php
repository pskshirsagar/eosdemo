<?php

class CBaseVacantBillAlertActivity extends CEosSingularBase {

	const TABLE_NAME = 'public.vacant_bill_alert_activities';

	protected $m_intId;
	protected $m_intVacantBillAlertId;
	protected $m_intVacantBillAlertEventId;
	protected $m_strMemo;
	protected $m_strActivityDatetime;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vacant_bill_alert_id'] ) && $boolDirectSet ) $this->set( 'm_intVacantBillAlertId', trim( $arrValues['vacant_bill_alert_id'] ) ); elseif( isset( $arrValues['vacant_bill_alert_id'] ) ) $this->setVacantBillAlertId( $arrValues['vacant_bill_alert_id'] );
		if( isset( $arrValues['vacant_bill_alert_event_id'] ) && $boolDirectSet ) $this->set( 'm_intVacantBillAlertEventId', trim( $arrValues['vacant_bill_alert_event_id'] ) ); elseif( isset( $arrValues['vacant_bill_alert_event_id'] ) ) $this->setVacantBillAlertEventId( $arrValues['vacant_bill_alert_event_id'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( $arrValues['memo'] ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( $arrValues['memo'] );
		if( isset( $arrValues['activity_datetime'] ) && $boolDirectSet ) $this->set( 'm_strActivityDatetime', trim( $arrValues['activity_datetime'] ) ); elseif( isset( $arrValues['activity_datetime'] ) ) $this->setActivityDatetime( $arrValues['activity_datetime'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVacantBillAlertId( $intVacantBillAlertId ) {
		$this->set( 'm_intVacantBillAlertId', CStrings::strToIntDef( $intVacantBillAlertId, NULL, false ) );
	}

	public function getVacantBillAlertId() {
		return $this->m_intVacantBillAlertId;
	}

	public function sqlVacantBillAlertId() {
		return ( true == isset( $this->m_intVacantBillAlertId ) ) ? ( string ) $this->m_intVacantBillAlertId : 'NULL';
	}

	public function setVacantBillAlertEventId( $intVacantBillAlertEventId ) {
		$this->set( 'm_intVacantBillAlertEventId', CStrings::strToIntDef( $intVacantBillAlertEventId, NULL, false ) );
	}

	public function getVacantBillAlertEventId() {
		return $this->m_intVacantBillAlertEventId;
	}

	public function sqlVacantBillAlertEventId() {
		return ( true == isset( $this->m_intVacantBillAlertEventId ) ) ? ( string ) $this->m_intVacantBillAlertEventId : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, -1, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMemo ) : '\'' . addslashes( $this->m_strMemo ) . '\'' ) : 'NULL';
	}

	public function setActivityDatetime( $strActivityDatetime ) {
		$this->set( 'm_strActivityDatetime', CStrings::strTrimDef( $strActivityDatetime, -1, NULL, true ) );
	}

	public function getActivityDatetime() {
		return $this->m_strActivityDatetime;
	}

	public function sqlActivityDatetime() {
		return ( true == isset( $this->m_strActivityDatetime ) ) ? '\'' . $this->m_strActivityDatetime . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vacant_bill_alert_id, vacant_bill_alert_event_id, memo, activity_datetime, updated_on, updated_by, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlVacantBillAlertId() . ', ' .
						$this->sqlVacantBillAlertEventId() . ', ' .
						$this->sqlMemo() . ', ' .
						$this->sqlActivityDatetime() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacant_bill_alert_id = ' . $this->sqlVacantBillAlertId(). ',' ; } elseif( true == array_key_exists( 'VacantBillAlertId', $this->getChangedColumns() ) ) { $strSql .= ' vacant_bill_alert_id = ' . $this->sqlVacantBillAlertId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacant_bill_alert_event_id = ' . $this->sqlVacantBillAlertEventId(). ',' ; } elseif( true == array_key_exists( 'VacantBillAlertEventId', $this->getChangedColumns() ) ) { $strSql .= ' vacant_bill_alert_event_id = ' . $this->sqlVacantBillAlertEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo(). ',' ; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activity_datetime = ' . $this->sqlActivityDatetime(). ',' ; } elseif( true == array_key_exists( 'ActivityDatetime', $this->getChangedColumns() ) ) { $strSql .= ' activity_datetime = ' . $this->sqlActivityDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vacant_bill_alert_id' => $this->getVacantBillAlertId(),
			'vacant_bill_alert_event_id' => $this->getVacantBillAlertEventId(),
			'memo' => $this->getMemo(),
			'activity_datetime' => $this->getActivityDatetime(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>