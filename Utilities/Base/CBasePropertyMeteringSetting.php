<?php

class CBasePropertyMeteringSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.property_metering_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intExpensePropertyUtilityTypeId;
	protected $m_intUtilityTransmissionTypeId;
	protected $m_intMeterRateTypeId;
	protected $m_intMeterCareRateTypeId;
	protected $m_intPropertyMeterEstimateTypeId;
	protected $m_strSettingsDatetime;
	protected $m_strInstallationCompanyName;
	protected $m_strDescription;
	protected $m_fltTamperFee;
	protected $m_strRdlDccPhoneNumber;
	protected $m_strInstallationNotes;
	protected $m_strRolloutProgressNotes;
	protected $m_strMeterWorkRepairNotes;
	protected $m_strRemotePrimaryKey;
	protected $m_intTransmissionExpectedDay;
	protected $m_intTransmissionDeadlineDay;
	protected $m_intTransmissionEmergencyDay;
	protected $m_strLastTransmissionDatetime;
	protected $m_strLastMeterAuditOn;
	protected $m_strExpectedInstallCompletion;
	protected $m_boolIsPartialCaptureSystem;
	protected $m_boolEstimateMissingMeterReads;
	protected $m_boolEstimateZeroConsumption;
	protected $m_boolEstimateLowConsumption;
	protected $m_boolEstimateHighConsumption;
	protected $m_intLowUsageThreshhold;
	protected $m_intHighUsageThreshhold;
	protected $m_fltBilledConsumptionThreshold;
	protected $m_fltLowConsumptionPercent;
	protected $m_fltHighConsumptionPercent;
	protected $m_fltTrueUpPercent;
	protected $m_fltPercentOfEstimate;
	protected $m_fltContractedSuccessfulMeterReportingRate;
	protected $m_fltVacantUnitConsumptionCap;
	protected $m_boolAllowsEstimates;
	protected $m_boolUsePercentEstimate;
	protected $m_boolIsOccupancyAverage;
	protected $m_boolIncludeHighLowConsumptionInAverages;
	protected $m_fltBtuFactor;
	protected $m_fltDailyAllowance;
	protected $m_strMeterRateChangeDate;
	protected $m_strMeterCareRateChangeDate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMaxEstimationMonths;

	public function __construct() {
		parent::__construct();

		$this->m_fltTamperFee = '0';
		$this->m_boolIsPartialCaptureSystem = false;
		$this->m_boolEstimateMissingMeterReads = true;
		$this->m_boolEstimateZeroConsumption = false;
		$this->m_boolEstimateLowConsumption = true;
		$this->m_boolEstimateHighConsumption = true;
		$this->m_fltBilledConsumptionThreshold = '20';
		$this->m_fltLowConsumptionPercent = '0.9';
		$this->m_fltHighConsumptionPercent = '3';
		$this->m_fltContractedSuccessfulMeterReportingRate = '0';
		$this->m_fltVacantUnitConsumptionCap = '30';
		$this->m_boolAllowsEstimates = true;
		$this->m_boolUsePercentEstimate = false;
		$this->m_boolIsOccupancyAverage = false;
		$this->m_boolIncludeHighLowConsumptionInAverages = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['expense_property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intExpensePropertyUtilityTypeId', trim( $arrValues['expense_property_utility_type_id'] ) ); elseif( isset( $arrValues['expense_property_utility_type_id'] ) ) $this->setExpensePropertyUtilityTypeId( $arrValues['expense_property_utility_type_id'] );
		if( isset( $arrValues['utility_transmission_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTransmissionTypeId', trim( $arrValues['utility_transmission_type_id'] ) ); elseif( isset( $arrValues['utility_transmission_type_id'] ) ) $this->setUtilityTransmissionTypeId( $arrValues['utility_transmission_type_id'] );
		if( isset( $arrValues['meter_rate_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMeterRateTypeId', trim( $arrValues['meter_rate_type_id'] ) ); elseif( isset( $arrValues['meter_rate_type_id'] ) ) $this->setMeterRateTypeId( $arrValues['meter_rate_type_id'] );
		if( isset( $arrValues['meter_care_rate_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMeterCareRateTypeId', trim( $arrValues['meter_care_rate_type_id'] ) ); elseif( isset( $arrValues['meter_care_rate_type_id'] ) ) $this->setMeterCareRateTypeId( $arrValues['meter_care_rate_type_id'] );
		if( isset( $arrValues['property_meter_estimate_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyMeterEstimateTypeId', trim( $arrValues['property_meter_estimate_type_id'] ) ); elseif( isset( $arrValues['property_meter_estimate_type_id'] ) ) $this->setPropertyMeterEstimateTypeId( $arrValues['property_meter_estimate_type_id'] );
		if( isset( $arrValues['settings_datetime'] ) && $boolDirectSet ) $this->set( 'm_strSettingsDatetime', trim( $arrValues['settings_datetime'] ) ); elseif( isset( $arrValues['settings_datetime'] ) ) $this->setSettingsDatetime( $arrValues['settings_datetime'] );
		if( isset( $arrValues['installation_company_name'] ) && $boolDirectSet ) $this->set( 'm_strInstallationCompanyName', trim( stripcslashes( $arrValues['installation_company_name'] ) ) ); elseif( isset( $arrValues['installation_company_name'] ) ) $this->setInstallationCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['installation_company_name'] ) : $arrValues['installation_company_name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['tamper_fee'] ) && $boolDirectSet ) $this->set( 'm_fltTamperFee', trim( $arrValues['tamper_fee'] ) ); elseif( isset( $arrValues['tamper_fee'] ) ) $this->setTamperFee( $arrValues['tamper_fee'] );
		if( isset( $arrValues['rdl_dcc_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strRdlDccPhoneNumber', trim( stripcslashes( $arrValues['rdl_dcc_phone_number'] ) ) ); elseif( isset( $arrValues['rdl_dcc_phone_number'] ) ) $this->setRdlDccPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rdl_dcc_phone_number'] ) : $arrValues['rdl_dcc_phone_number'] );
		if( isset( $arrValues['installation_notes'] ) && $boolDirectSet ) $this->set( 'm_strInstallationNotes', trim( stripcslashes( $arrValues['installation_notes'] ) ) ); elseif( isset( $arrValues['installation_notes'] ) ) $this->setInstallationNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['installation_notes'] ) : $arrValues['installation_notes'] );
		if( isset( $arrValues['rollout_progress_notes'] ) && $boolDirectSet ) $this->set( 'm_strRolloutProgressNotes', trim( stripcslashes( $arrValues['rollout_progress_notes'] ) ) ); elseif( isset( $arrValues['rollout_progress_notes'] ) ) $this->setRolloutProgressNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rollout_progress_notes'] ) : $arrValues['rollout_progress_notes'] );
		if( isset( $arrValues['meter_work_repair_notes'] ) && $boolDirectSet ) $this->set( 'm_strMeterWorkRepairNotes', trim( stripcslashes( $arrValues['meter_work_repair_notes'] ) ) ); elseif( isset( $arrValues['meter_work_repair_notes'] ) ) $this->setMeterWorkRepairNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['meter_work_repair_notes'] ) : $arrValues['meter_work_repair_notes'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['transmission_expected_day'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionExpectedDay', trim( $arrValues['transmission_expected_day'] ) ); elseif( isset( $arrValues['transmission_expected_day'] ) ) $this->setTransmissionExpectedDay( $arrValues['transmission_expected_day'] );
		if( isset( $arrValues['transmission_deadline_day'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionDeadlineDay', trim( $arrValues['transmission_deadline_day'] ) ); elseif( isset( $arrValues['transmission_deadline_day'] ) ) $this->setTransmissionDeadlineDay( $arrValues['transmission_deadline_day'] );
		if( isset( $arrValues['transmission_emergency_day'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionEmergencyDay', trim( $arrValues['transmission_emergency_day'] ) ); elseif( isset( $arrValues['transmission_emergency_day'] ) ) $this->setTransmissionEmergencyDay( $arrValues['transmission_emergency_day'] );
		if( isset( $arrValues['last_transmission_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLastTransmissionDatetime', trim( $arrValues['last_transmission_datetime'] ) ); elseif( isset( $arrValues['last_transmission_datetime'] ) ) $this->setLastTransmissionDatetime( $arrValues['last_transmission_datetime'] );
		if( isset( $arrValues['last_meter_audit_on'] ) && $boolDirectSet ) $this->set( 'm_strLastMeterAuditOn', trim( $arrValues['last_meter_audit_on'] ) ); elseif( isset( $arrValues['last_meter_audit_on'] ) ) $this->setLastMeterAuditOn( $arrValues['last_meter_audit_on'] );
		if( isset( $arrValues['expected_install_completion'] ) && $boolDirectSet ) $this->set( 'm_strExpectedInstallCompletion', trim( $arrValues['expected_install_completion'] ) ); elseif( isset( $arrValues['expected_install_completion'] ) ) $this->setExpectedInstallCompletion( $arrValues['expected_install_completion'] );
		if( isset( $arrValues['is_partial_capture_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsPartialCaptureSystem', trim( stripcslashes( $arrValues['is_partial_capture_system'] ) ) ); elseif( isset( $arrValues['is_partial_capture_system'] ) ) $this->setIsPartialCaptureSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_partial_capture_system'] ) : $arrValues['is_partial_capture_system'] );
		if( isset( $arrValues['estimate_missing_meter_reads'] ) && $boolDirectSet ) $this->set( 'm_boolEstimateMissingMeterReads', trim( stripcslashes( $arrValues['estimate_missing_meter_reads'] ) ) ); elseif( isset( $arrValues['estimate_missing_meter_reads'] ) ) $this->setEstimateMissingMeterReads( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['estimate_missing_meter_reads'] ) : $arrValues['estimate_missing_meter_reads'] );
		if( isset( $arrValues['estimate_zero_consumption'] ) && $boolDirectSet ) $this->set( 'm_boolEstimateZeroConsumption', trim( stripcslashes( $arrValues['estimate_zero_consumption'] ) ) ); elseif( isset( $arrValues['estimate_zero_consumption'] ) ) $this->setEstimateZeroConsumption( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['estimate_zero_consumption'] ) : $arrValues['estimate_zero_consumption'] );
		if( isset( $arrValues['estimate_low_consumption'] ) && $boolDirectSet ) $this->set( 'm_boolEstimateLowConsumption', trim( stripcslashes( $arrValues['estimate_low_consumption'] ) ) ); elseif( isset( $arrValues['estimate_low_consumption'] ) ) $this->setEstimateLowConsumption( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['estimate_low_consumption'] ) : $arrValues['estimate_low_consumption'] );
		if( isset( $arrValues['estimate_high_consumption'] ) && $boolDirectSet ) $this->set( 'm_boolEstimateHighConsumption', trim( stripcslashes( $arrValues['estimate_high_consumption'] ) ) ); elseif( isset( $arrValues['estimate_high_consumption'] ) ) $this->setEstimateHighConsumption( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['estimate_high_consumption'] ) : $arrValues['estimate_high_consumption'] );
		if( isset( $arrValues['low_usage_threshhold'] ) && $boolDirectSet ) $this->set( 'm_intLowUsageThreshhold', trim( $arrValues['low_usage_threshhold'] ) ); elseif( isset( $arrValues['low_usage_threshhold'] ) ) $this->setLowUsageThreshhold( $arrValues['low_usage_threshhold'] );
		if( isset( $arrValues['high_usage_threshhold'] ) && $boolDirectSet ) $this->set( 'm_intHighUsageThreshhold', trim( $arrValues['high_usage_threshhold'] ) ); elseif( isset( $arrValues['high_usage_threshhold'] ) ) $this->setHighUsageThreshhold( $arrValues['high_usage_threshhold'] );
		if( isset( $arrValues['billed_consumption_threshold'] ) && $boolDirectSet ) $this->set( 'm_fltBilledConsumptionThreshold', trim( $arrValues['billed_consumption_threshold'] ) ); elseif( isset( $arrValues['billed_consumption_threshold'] ) ) $this->setBilledConsumptionThreshold( $arrValues['billed_consumption_threshold'] );
		if( isset( $arrValues['low_consumption_percent'] ) && $boolDirectSet ) $this->set( 'm_fltLowConsumptionPercent', trim( $arrValues['low_consumption_percent'] ) ); elseif( isset( $arrValues['low_consumption_percent'] ) ) $this->setLowConsumptionPercent( $arrValues['low_consumption_percent'] );
		if( isset( $arrValues['high_consumption_percent'] ) && $boolDirectSet ) $this->set( 'm_fltHighConsumptionPercent', trim( $arrValues['high_consumption_percent'] ) ); elseif( isset( $arrValues['high_consumption_percent'] ) ) $this->setHighConsumptionPercent( $arrValues['high_consumption_percent'] );
		if( isset( $arrValues['true_up_percent'] ) && $boolDirectSet ) $this->set( 'm_fltTrueUpPercent', trim( $arrValues['true_up_percent'] ) ); elseif( isset( $arrValues['true_up_percent'] ) ) $this->setTrueUpPercent( $arrValues['true_up_percent'] );
		if( isset( $arrValues['percent_of_estimate'] ) && $boolDirectSet ) $this->set( 'm_fltPercentOfEstimate', trim( $arrValues['percent_of_estimate'] ) ); elseif( isset( $arrValues['percent_of_estimate'] ) ) $this->setPercentOfEstimate( $arrValues['percent_of_estimate'] );
		if( isset( $arrValues['contracted_successful_meter_reporting_rate'] ) && $boolDirectSet ) $this->set( 'm_fltContractedSuccessfulMeterReportingRate', trim( $arrValues['contracted_successful_meter_reporting_rate'] ) ); elseif( isset( $arrValues['contracted_successful_meter_reporting_rate'] ) ) $this->setContractedSuccessfulMeterReportingRate( $arrValues['contracted_successful_meter_reporting_rate'] );
		if( isset( $arrValues['vacant_unit_consumption_cap'] ) && $boolDirectSet ) $this->set( 'm_fltVacantUnitConsumptionCap', trim( $arrValues['vacant_unit_consumption_cap'] ) ); elseif( isset( $arrValues['vacant_unit_consumption_cap'] ) ) $this->setVacantUnitConsumptionCap( $arrValues['vacant_unit_consumption_cap'] );
		if( isset( $arrValues['allows_estimates'] ) && $boolDirectSet ) $this->set( 'm_boolAllowsEstimates', trim( stripcslashes( $arrValues['allows_estimates'] ) ) ); elseif( isset( $arrValues['allows_estimates'] ) ) $this->setAllowsEstimates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allows_estimates'] ) : $arrValues['allows_estimates'] );
		if( isset( $arrValues['use_percent_estimate'] ) && $boolDirectSet ) $this->set( 'm_boolUsePercentEstimate', trim( stripcslashes( $arrValues['use_percent_estimate'] ) ) ); elseif( isset( $arrValues['use_percent_estimate'] ) ) $this->setUsePercentEstimate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_percent_estimate'] ) : $arrValues['use_percent_estimate'] );
		if( isset( $arrValues['is_occupancy_average'] ) && $boolDirectSet ) $this->set( 'm_boolIsOccupancyAverage', trim( stripcslashes( $arrValues['is_occupancy_average'] ) ) ); elseif( isset( $arrValues['is_occupancy_average'] ) ) $this->setIsOccupancyAverage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_occupancy_average'] ) : $arrValues['is_occupancy_average'] );
		if( isset( $arrValues['include_high_low_consumption_in_averages'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeHighLowConsumptionInAverages', trim( stripcslashes( $arrValues['include_high_low_consumption_in_averages'] ) ) ); elseif( isset( $arrValues['include_high_low_consumption_in_averages'] ) ) $this->setIncludeHighLowConsumptionInAverages( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_high_low_consumption_in_averages'] ) : $arrValues['include_high_low_consumption_in_averages'] );
		if( isset( $arrValues['btu_factor'] ) && $boolDirectSet ) $this->set( 'm_fltBtuFactor', trim( $arrValues['btu_factor'] ) ); elseif( isset( $arrValues['btu_factor'] ) ) $this->setBtuFactor( $arrValues['btu_factor'] );
		if( isset( $arrValues['daily_allowance'] ) && $boolDirectSet ) $this->set( 'm_fltDailyAllowance', trim( $arrValues['daily_allowance'] ) ); elseif( isset( $arrValues['daily_allowance'] ) ) $this->setDailyAllowance( $arrValues['daily_allowance'] );
		if( isset( $arrValues['meter_rate_change_date'] ) && $boolDirectSet ) $this->set( 'm_strMeterRateChangeDate', trim( $arrValues['meter_rate_change_date'] ) ); elseif( isset( $arrValues['meter_rate_change_date'] ) ) $this->setMeterRateChangeDate( $arrValues['meter_rate_change_date'] );
		if( isset( $arrValues['meter_care_rate_change_date'] ) && $boolDirectSet ) $this->set( 'm_strMeterCareRateChangeDate', trim( $arrValues['meter_care_rate_change_date'] ) ); elseif( isset( $arrValues['meter_care_rate_change_date'] ) ) $this->setMeterCareRateChangeDate( $arrValues['meter_care_rate_change_date'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['max_estimation_months'] ) && $boolDirectSet ) $this->set( 'm_intMaxEstimationMonths', trim( $arrValues['max_estimation_months'] ) ); elseif( isset( $arrValues['max_estimation_months'] ) ) $this->setMaxEstimationMonths( $arrValues['max_estimation_months'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setExpensePropertyUtilityTypeId( $intExpensePropertyUtilityTypeId ) {
		$this->set( 'm_intExpensePropertyUtilityTypeId', CStrings::strToIntDef( $intExpensePropertyUtilityTypeId, NULL, false ) );
	}

	public function getExpensePropertyUtilityTypeId() {
		return $this->m_intExpensePropertyUtilityTypeId;
	}

	public function sqlExpensePropertyUtilityTypeId() {
		return ( true == isset( $this->m_intExpensePropertyUtilityTypeId ) ) ? ( string ) $this->m_intExpensePropertyUtilityTypeId : 'NULL';
	}

	public function setUtilityTransmissionTypeId( $intUtilityTransmissionTypeId ) {
		$this->set( 'm_intUtilityTransmissionTypeId', CStrings::strToIntDef( $intUtilityTransmissionTypeId, NULL, false ) );
	}

	public function getUtilityTransmissionTypeId() {
		return $this->m_intUtilityTransmissionTypeId;
	}

	public function sqlUtilityTransmissionTypeId() {
		return ( true == isset( $this->m_intUtilityTransmissionTypeId ) ) ? ( string ) $this->m_intUtilityTransmissionTypeId : 'NULL';
	}

	public function setMeterRateTypeId( $intMeterRateTypeId ) {
		$this->set( 'm_intMeterRateTypeId', CStrings::strToIntDef( $intMeterRateTypeId, NULL, false ) );
	}

	public function getMeterRateTypeId() {
		return $this->m_intMeterRateTypeId;
	}

	public function sqlMeterRateTypeId() {
		return ( true == isset( $this->m_intMeterRateTypeId ) ) ? ( string ) $this->m_intMeterRateTypeId : 'NULL';
	}

	public function setMeterCareRateTypeId( $intMeterCareRateTypeId ) {
		$this->set( 'm_intMeterCareRateTypeId', CStrings::strToIntDef( $intMeterCareRateTypeId, NULL, false ) );
	}

	public function getMeterCareRateTypeId() {
		return $this->m_intMeterCareRateTypeId;
	}

	public function sqlMeterCareRateTypeId() {
		return ( true == isset( $this->m_intMeterCareRateTypeId ) ) ? ( string ) $this->m_intMeterCareRateTypeId : 'NULL';
	}

	public function setPropertyMeterEstimateTypeId( $intPropertyMeterEstimateTypeId ) {
		$this->set( 'm_intPropertyMeterEstimateTypeId', CStrings::strToIntDef( $intPropertyMeterEstimateTypeId, NULL, false ) );
	}

	public function getPropertyMeterEstimateTypeId() {
		return $this->m_intPropertyMeterEstimateTypeId;
	}

	public function sqlPropertyMeterEstimateTypeId() {
		return ( true == isset( $this->m_intPropertyMeterEstimateTypeId ) ) ? ( string ) $this->m_intPropertyMeterEstimateTypeId : 'NULL';
	}

	public function setSettingsDatetime( $strSettingsDatetime ) {
		$this->set( 'm_strSettingsDatetime', CStrings::strTrimDef( $strSettingsDatetime, -1, NULL, true ) );
	}

	public function getSettingsDatetime() {
		return $this->m_strSettingsDatetime;
	}

	public function sqlSettingsDatetime() {
		return ( true == isset( $this->m_strSettingsDatetime ) ) ? '\'' . $this->m_strSettingsDatetime . '\'' : 'NOW()';
	}

	public function setInstallationCompanyName( $strInstallationCompanyName ) {
		$this->set( 'm_strInstallationCompanyName', CStrings::strTrimDef( $strInstallationCompanyName, 50, NULL, true ) );
	}

	public function getInstallationCompanyName() {
		return $this->m_strInstallationCompanyName;
	}

	public function sqlInstallationCompanyName() {
		return ( true == isset( $this->m_strInstallationCompanyName ) ) ? '\'' . addslashes( $this->m_strInstallationCompanyName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setTamperFee( $fltTamperFee ) {
		$this->set( 'm_fltTamperFee', CStrings::strToFloatDef( $fltTamperFee, NULL, false, 2 ) );
	}

	public function getTamperFee() {
		return $this->m_fltTamperFee;
	}

	public function sqlTamperFee() {
		return ( true == isset( $this->m_fltTamperFee ) ) ? ( string ) $this->m_fltTamperFee : '0';
	}

	public function setRdlDccPhoneNumber( $strRdlDccPhoneNumber ) {
		$this->set( 'm_strRdlDccPhoneNumber', CStrings::strTrimDef( $strRdlDccPhoneNumber, 30, NULL, true ) );
	}

	public function getRdlDccPhoneNumber() {
		return $this->m_strRdlDccPhoneNumber;
	}

	public function sqlRdlDccPhoneNumber() {
		return ( true == isset( $this->m_strRdlDccPhoneNumber ) ) ? '\'' . addslashes( $this->m_strRdlDccPhoneNumber ) . '\'' : 'NULL';
	}

	public function setInstallationNotes( $strInstallationNotes ) {
		$this->set( 'm_strInstallationNotes', CStrings::strTrimDef( $strInstallationNotes, -1, NULL, true ) );
	}

	public function getInstallationNotes() {
		return $this->m_strInstallationNotes;
	}

	public function sqlInstallationNotes() {
		return ( true == isset( $this->m_strInstallationNotes ) ) ? '\'' . addslashes( $this->m_strInstallationNotes ) . '\'' : 'NULL';
	}

	public function setRolloutProgressNotes( $strRolloutProgressNotes ) {
		$this->set( 'm_strRolloutProgressNotes', CStrings::strTrimDef( $strRolloutProgressNotes, -1, NULL, true ) );
	}

	public function getRolloutProgressNotes() {
		return $this->m_strRolloutProgressNotes;
	}

	public function sqlRolloutProgressNotes() {
		return ( true == isset( $this->m_strRolloutProgressNotes ) ) ? '\'' . addslashes( $this->m_strRolloutProgressNotes ) . '\'' : 'NULL';
	}

	public function setMeterWorkRepairNotes( $strMeterWorkRepairNotes ) {
		$this->set( 'm_strMeterWorkRepairNotes', CStrings::strTrimDef( $strMeterWorkRepairNotes, -1, NULL, true ) );
	}

	public function getMeterWorkRepairNotes() {
		return $this->m_strMeterWorkRepairNotes;
	}

	public function sqlMeterWorkRepairNotes() {
		return ( true == isset( $this->m_strMeterWorkRepairNotes ) ) ? '\'' . addslashes( $this->m_strMeterWorkRepairNotes ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 240, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setTransmissionExpectedDay( $intTransmissionExpectedDay ) {
		$this->set( 'm_intTransmissionExpectedDay', CStrings::strToIntDef( $intTransmissionExpectedDay, NULL, false ) );
	}

	public function getTransmissionExpectedDay() {
		return $this->m_intTransmissionExpectedDay;
	}

	public function sqlTransmissionExpectedDay() {
		return ( true == isset( $this->m_intTransmissionExpectedDay ) ) ? ( string ) $this->m_intTransmissionExpectedDay : 'NULL';
	}

	public function setTransmissionDeadlineDay( $intTransmissionDeadlineDay ) {
		$this->set( 'm_intTransmissionDeadlineDay', CStrings::strToIntDef( $intTransmissionDeadlineDay, NULL, false ) );
	}

	public function getTransmissionDeadlineDay() {
		return $this->m_intTransmissionDeadlineDay;
	}

	public function sqlTransmissionDeadlineDay() {
		return ( true == isset( $this->m_intTransmissionDeadlineDay ) ) ? ( string ) $this->m_intTransmissionDeadlineDay : 'NULL';
	}

	public function setTransmissionEmergencyDay( $intTransmissionEmergencyDay ) {
		$this->set( 'm_intTransmissionEmergencyDay', CStrings::strToIntDef( $intTransmissionEmergencyDay, NULL, false ) );
	}

	public function getTransmissionEmergencyDay() {
		return $this->m_intTransmissionEmergencyDay;
	}

	public function sqlTransmissionEmergencyDay() {
		return ( true == isset( $this->m_intTransmissionEmergencyDay ) ) ? ( string ) $this->m_intTransmissionEmergencyDay : 'NULL';
	}

	public function setLastTransmissionDatetime( $strLastTransmissionDatetime ) {
		$this->set( 'm_strLastTransmissionDatetime', CStrings::strTrimDef( $strLastTransmissionDatetime, -1, NULL, true ) );
	}

	public function getLastTransmissionDatetime() {
		return $this->m_strLastTransmissionDatetime;
	}

	public function sqlLastTransmissionDatetime() {
		return ( true == isset( $this->m_strLastTransmissionDatetime ) ) ? '\'' . $this->m_strLastTransmissionDatetime . '\'' : 'NULL';
	}

	public function setLastMeterAuditOn( $strLastMeterAuditOn ) {
		$this->set( 'm_strLastMeterAuditOn', CStrings::strTrimDef( $strLastMeterAuditOn, -1, NULL, true ) );
	}

	public function getLastMeterAuditOn() {
		return $this->m_strLastMeterAuditOn;
	}

	public function sqlLastMeterAuditOn() {
		return ( true == isset( $this->m_strLastMeterAuditOn ) ) ? '\'' . $this->m_strLastMeterAuditOn . '\'' : 'NULL';
	}

	public function setExpectedInstallCompletion( $strExpectedInstallCompletion ) {
		$this->set( 'm_strExpectedInstallCompletion', CStrings::strTrimDef( $strExpectedInstallCompletion, -1, NULL, true ) );
	}

	public function getExpectedInstallCompletion() {
		return $this->m_strExpectedInstallCompletion;
	}

	public function sqlExpectedInstallCompletion() {
		return ( true == isset( $this->m_strExpectedInstallCompletion ) ) ? '\'' . $this->m_strExpectedInstallCompletion . '\'' : 'NULL';
	}

	public function setIsPartialCaptureSystem( $boolIsPartialCaptureSystem ) {
		$this->set( 'm_boolIsPartialCaptureSystem', CStrings::strToBool( $boolIsPartialCaptureSystem ) );
	}

	public function getIsPartialCaptureSystem() {
		return $this->m_boolIsPartialCaptureSystem;
	}

	public function sqlIsPartialCaptureSystem() {
		return ( true == isset( $this->m_boolIsPartialCaptureSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPartialCaptureSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEstimateMissingMeterReads( $boolEstimateMissingMeterReads ) {
		$this->set( 'm_boolEstimateMissingMeterReads', CStrings::strToBool( $boolEstimateMissingMeterReads ) );
	}

	public function getEstimateMissingMeterReads() {
		return $this->m_boolEstimateMissingMeterReads;
	}

	public function sqlEstimateMissingMeterReads() {
		return ( true == isset( $this->m_boolEstimateMissingMeterReads ) ) ? '\'' . ( true == ( bool ) $this->m_boolEstimateMissingMeterReads ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEstimateZeroConsumption( $boolEstimateZeroConsumption ) {
		$this->set( 'm_boolEstimateZeroConsumption', CStrings::strToBool( $boolEstimateZeroConsumption ) );
	}

	public function getEstimateZeroConsumption() {
		return $this->m_boolEstimateZeroConsumption;
	}

	public function sqlEstimateZeroConsumption() {
		return ( true == isset( $this->m_boolEstimateZeroConsumption ) ) ? '\'' . ( true == ( bool ) $this->m_boolEstimateZeroConsumption ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEstimateLowConsumption( $boolEstimateLowConsumption ) {
		$this->set( 'm_boolEstimateLowConsumption', CStrings::strToBool( $boolEstimateLowConsumption ) );
	}

	public function getEstimateLowConsumption() {
		return $this->m_boolEstimateLowConsumption;
	}

	public function sqlEstimateLowConsumption() {
		return ( true == isset( $this->m_boolEstimateLowConsumption ) ) ? '\'' . ( true == ( bool ) $this->m_boolEstimateLowConsumption ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEstimateHighConsumption( $boolEstimateHighConsumption ) {
		$this->set( 'm_boolEstimateHighConsumption', CStrings::strToBool( $boolEstimateHighConsumption ) );
	}

	public function getEstimateHighConsumption() {
		return $this->m_boolEstimateHighConsumption;
	}

	public function sqlEstimateHighConsumption() {
		return ( true == isset( $this->m_boolEstimateHighConsumption ) ) ? '\'' . ( true == ( bool ) $this->m_boolEstimateHighConsumption ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLowUsageThreshhold( $intLowUsageThreshhold ) {
		$this->set( 'm_intLowUsageThreshhold', CStrings::strToIntDef( $intLowUsageThreshhold, NULL, false ) );
	}

	public function getLowUsageThreshhold() {
		return $this->m_intLowUsageThreshhold;
	}

	public function sqlLowUsageThreshhold() {
		return ( true == isset( $this->m_intLowUsageThreshhold ) ) ? ( string ) $this->m_intLowUsageThreshhold : 'NULL';
	}

	public function setHighUsageThreshhold( $intHighUsageThreshhold ) {
		$this->set( 'm_intHighUsageThreshhold', CStrings::strToIntDef( $intHighUsageThreshhold, NULL, false ) );
	}

	public function getHighUsageThreshhold() {
		return $this->m_intHighUsageThreshhold;
	}

	public function sqlHighUsageThreshhold() {
		return ( true == isset( $this->m_intHighUsageThreshhold ) ) ? ( string ) $this->m_intHighUsageThreshhold : 'NULL';
	}

	public function setBilledConsumptionThreshold( $fltBilledConsumptionThreshold ) {
		$this->set( 'm_fltBilledConsumptionThreshold', CStrings::strToFloatDef( $fltBilledConsumptionThreshold, NULL, false, 2 ) );
	}

	public function getBilledConsumptionThreshold() {
		return $this->m_fltBilledConsumptionThreshold;
	}

	public function sqlBilledConsumptionThreshold() {
		return ( true == isset( $this->m_fltBilledConsumptionThreshold ) ) ? ( string ) $this->m_fltBilledConsumptionThreshold : '20';
	}

	public function setLowConsumptionPercent( $fltLowConsumptionPercent ) {
		$this->set( 'm_fltLowConsumptionPercent', CStrings::strToFloatDef( $fltLowConsumptionPercent, NULL, false, 2 ) );
	}

	public function getLowConsumptionPercent() {
		return $this->m_fltLowConsumptionPercent;
	}

	public function sqlLowConsumptionPercent() {
		return ( true == isset( $this->m_fltLowConsumptionPercent ) ) ? ( string ) $this->m_fltLowConsumptionPercent : '0.9';
	}

	public function setHighConsumptionPercent( $fltHighConsumptionPercent ) {
		$this->set( 'm_fltHighConsumptionPercent', CStrings::strToFloatDef( $fltHighConsumptionPercent, NULL, false, 2 ) );
	}

	public function getHighConsumptionPercent() {
		return $this->m_fltHighConsumptionPercent;
	}

	public function sqlHighConsumptionPercent() {
		return ( true == isset( $this->m_fltHighConsumptionPercent ) ) ? ( string ) $this->m_fltHighConsumptionPercent : '3';
	}

	public function setTrueUpPercent( $fltTrueUpPercent ) {
		$this->set( 'm_fltTrueUpPercent', CStrings::strToFloatDef( $fltTrueUpPercent, NULL, false, 2 ) );
	}

	public function getTrueUpPercent() {
		return $this->m_fltTrueUpPercent;
	}

	public function sqlTrueUpPercent() {
		return ( true == isset( $this->m_fltTrueUpPercent ) ) ? ( string ) $this->m_fltTrueUpPercent : 'NULL';
	}

	public function setPercentOfEstimate( $fltPercentOfEstimate ) {
		$this->set( 'm_fltPercentOfEstimate', CStrings::strToFloatDef( $fltPercentOfEstimate, NULL, false, 2 ) );
	}

	public function getPercentOfEstimate() {
		return $this->m_fltPercentOfEstimate;
	}

	public function sqlPercentOfEstimate() {
		return ( true == isset( $this->m_fltPercentOfEstimate ) ) ? ( string ) $this->m_fltPercentOfEstimate : 'NULL';
	}

	public function setContractedSuccessfulMeterReportingRate( $fltContractedSuccessfulMeterReportingRate ) {
		$this->set( 'm_fltContractedSuccessfulMeterReportingRate', CStrings::strToFloatDef( $fltContractedSuccessfulMeterReportingRate, NULL, false, 2 ) );
	}

	public function getContractedSuccessfulMeterReportingRate() {
		return $this->m_fltContractedSuccessfulMeterReportingRate;
	}

	public function sqlContractedSuccessfulMeterReportingRate() {
		return ( true == isset( $this->m_fltContractedSuccessfulMeterReportingRate ) ) ? ( string ) $this->m_fltContractedSuccessfulMeterReportingRate : '0';
	}

	public function setVacantUnitConsumptionCap( $fltVacantUnitConsumptionCap ) {
		$this->set( 'm_fltVacantUnitConsumptionCap', CStrings::strToFloatDef( $fltVacantUnitConsumptionCap, NULL, false, 2 ) );
	}

	public function getVacantUnitConsumptionCap() {
		return $this->m_fltVacantUnitConsumptionCap;
	}

	public function sqlVacantUnitConsumptionCap() {
		return ( true == isset( $this->m_fltVacantUnitConsumptionCap ) ) ? ( string ) $this->m_fltVacantUnitConsumptionCap : '30';
	}

	public function setAllowsEstimates( $boolAllowsEstimates ) {
		$this->set( 'm_boolAllowsEstimates', CStrings::strToBool( $boolAllowsEstimates ) );
	}

	public function getAllowsEstimates() {
		return $this->m_boolAllowsEstimates;
	}

	public function sqlAllowsEstimates() {
		return ( true == isset( $this->m_boolAllowsEstimates ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowsEstimates ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUsePercentEstimate( $boolUsePercentEstimate ) {
		$this->set( 'm_boolUsePercentEstimate', CStrings::strToBool( $boolUsePercentEstimate ) );
	}

	public function getUsePercentEstimate() {
		return $this->m_boolUsePercentEstimate;
	}

	public function sqlUsePercentEstimate() {
		return ( true == isset( $this->m_boolUsePercentEstimate ) ) ? '\'' . ( true == ( bool ) $this->m_boolUsePercentEstimate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOccupancyAverage( $boolIsOccupancyAverage ) {
		$this->set( 'm_boolIsOccupancyAverage', CStrings::strToBool( $boolIsOccupancyAverage ) );
	}

	public function getIsOccupancyAverage() {
		return $this->m_boolIsOccupancyAverage;
	}

	public function sqlIsOccupancyAverage() {
		return ( true == isset( $this->m_boolIsOccupancyAverage ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOccupancyAverage ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeHighLowConsumptionInAverages( $boolIncludeHighLowConsumptionInAverages ) {
		$this->set( 'm_boolIncludeHighLowConsumptionInAverages', CStrings::strToBool( $boolIncludeHighLowConsumptionInAverages ) );
	}

	public function getIncludeHighLowConsumptionInAverages() {
		return $this->m_boolIncludeHighLowConsumptionInAverages;
	}

	public function sqlIncludeHighLowConsumptionInAverages() {
		return ( true == isset( $this->m_boolIncludeHighLowConsumptionInAverages ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeHighLowConsumptionInAverages ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBtuFactor( $fltBtuFactor ) {
		$this->set( 'm_fltBtuFactor', CStrings::strToFloatDef( $fltBtuFactor, NULL, false, 5 ) );
	}

	public function getBtuFactor() {
		return $this->m_fltBtuFactor;
	}

	public function sqlBtuFactor() {
		return ( true == isset( $this->m_fltBtuFactor ) ) ? ( string ) $this->m_fltBtuFactor : 'NULL';
	}

	public function setDailyAllowance( $fltDailyAllowance ) {
		$this->set( 'm_fltDailyAllowance', CStrings::strToFloatDef( $fltDailyAllowance, NULL, false, 5 ) );
	}

	public function getDailyAllowance() {
		return $this->m_fltDailyAllowance;
	}

	public function sqlDailyAllowance() {
		return ( true == isset( $this->m_fltDailyAllowance ) ) ? ( string ) $this->m_fltDailyAllowance : 'NULL';
	}

	public function setMeterRateChangeDate( $strMeterRateChangeDate ) {
		$this->set( 'm_strMeterRateChangeDate', CStrings::strTrimDef( $strMeterRateChangeDate, -1, NULL, true ) );
	}

	public function getMeterRateChangeDate() {
		return $this->m_strMeterRateChangeDate;
	}

	public function sqlMeterRateChangeDate() {
		return ( true == isset( $this->m_strMeterRateChangeDate ) ) ? '\'' . $this->m_strMeterRateChangeDate . '\'' : 'NULL';
	}

	public function setMeterCareRateChangeDate( $strMeterCareRateChangeDate ) {
		$this->set( 'm_strMeterCareRateChangeDate', CStrings::strTrimDef( $strMeterCareRateChangeDate, -1, NULL, true ) );
	}

	public function getMeterCareRateChangeDate() {
		return $this->m_strMeterCareRateChangeDate;
	}

	public function sqlMeterCareRateChangeDate() {
		return ( true == isset( $this->m_strMeterCareRateChangeDate ) ) ? '\'' . $this->m_strMeterCareRateChangeDate . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMaxEstimationMonths( $intMaxEstimationMonths ) {
		$this->set( 'm_intMaxEstimationMonths', CStrings::strToIntDef( $intMaxEstimationMonths, NULL, false ) );
	}

	public function getMaxEstimationMonths() {
		return $this->m_intMaxEstimationMonths;
	}

	public function sqlMaxEstimationMonths() {
		return ( true == isset( $this->m_intMaxEstimationMonths ) ) ? ( string ) $this->m_intMaxEstimationMonths : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_utility_type_id, expense_property_utility_type_id, utility_transmission_type_id, meter_rate_type_id, meter_care_rate_type_id, property_meter_estimate_type_id, settings_datetime, installation_company_name, description, tamper_fee, rdl_dcc_phone_number, installation_notes, rollout_progress_notes, meter_work_repair_notes, remote_primary_key, transmission_expected_day, transmission_deadline_day, transmission_emergency_day, last_transmission_datetime, last_meter_audit_on, expected_install_completion, is_partial_capture_system, estimate_missing_meter_reads, estimate_zero_consumption, estimate_low_consumption, estimate_high_consumption, low_usage_threshhold, high_usage_threshhold, billed_consumption_threshold, low_consumption_percent, high_consumption_percent, true_up_percent, percent_of_estimate, contracted_successful_meter_reporting_rate, vacant_unit_consumption_cap, allows_estimates, use_percent_estimate, is_occupancy_average, include_high_low_consumption_in_averages, btu_factor, daily_allowance, meter_rate_change_date, meter_care_rate_change_date, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, max_estimation_months )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlExpensePropertyUtilityTypeId() . ', ' .
						$this->sqlUtilityTransmissionTypeId() . ', ' .
						$this->sqlMeterRateTypeId() . ', ' .
						$this->sqlMeterCareRateTypeId() . ', ' .
						$this->sqlPropertyMeterEstimateTypeId() . ', ' .
						$this->sqlSettingsDatetime() . ', ' .
						$this->sqlInstallationCompanyName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlTamperFee() . ', ' .
						$this->sqlRdlDccPhoneNumber() . ', ' .
						$this->sqlInstallationNotes() . ', ' .
						$this->sqlRolloutProgressNotes() . ', ' .
						$this->sqlMeterWorkRepairNotes() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlTransmissionExpectedDay() . ', ' .
						$this->sqlTransmissionDeadlineDay() . ', ' .
						$this->sqlTransmissionEmergencyDay() . ', ' .
						$this->sqlLastTransmissionDatetime() . ', ' .
						$this->sqlLastMeterAuditOn() . ', ' .
						$this->sqlExpectedInstallCompletion() . ', ' .
						$this->sqlIsPartialCaptureSystem() . ', ' .
						$this->sqlEstimateMissingMeterReads() . ', ' .
						$this->sqlEstimateZeroConsumption() . ', ' .
						$this->sqlEstimateLowConsumption() . ', ' .
						$this->sqlEstimateHighConsumption() . ', ' .
						$this->sqlLowUsageThreshhold() . ', ' .
						$this->sqlHighUsageThreshhold() . ', ' .
						$this->sqlBilledConsumptionThreshold() . ', ' .
						$this->sqlLowConsumptionPercent() . ', ' .
						$this->sqlHighConsumptionPercent() . ', ' .
						$this->sqlTrueUpPercent() . ', ' .
						$this->sqlPercentOfEstimate() . ', ' .
						$this->sqlContractedSuccessfulMeterReportingRate() . ', ' .
						$this->sqlVacantUnitConsumptionCap() . ', ' .
						$this->sqlAllowsEstimates() . ', ' .
						$this->sqlUsePercentEstimate() . ', ' .
						$this->sqlIsOccupancyAverage() . ', ' .
						$this->sqlIncludeHighLowConsumptionInAverages() . ', ' .
						$this->sqlBtuFactor() . ', ' .
						$this->sqlDailyAllowance() . ', ' .
						$this->sqlMeterRateChangeDate() . ', ' .
						$this->sqlMeterCareRateChangeDate() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlMaxEstimationMonths() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expense_property_utility_type_id = ' . $this->sqlExpensePropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'ExpensePropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' expense_property_utility_type_id = ' . $this->sqlExpensePropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_transmission_type_id = ' . $this->sqlUtilityTransmissionTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityTransmissionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_transmission_type_id = ' . $this->sqlUtilityTransmissionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_rate_type_id = ' . $this->sqlMeterRateTypeId(). ',' ; } elseif( true == array_key_exists( 'MeterRateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' meter_rate_type_id = ' . $this->sqlMeterRateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_care_rate_type_id = ' . $this->sqlMeterCareRateTypeId(). ',' ; } elseif( true == array_key_exists( 'MeterCareRateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' meter_care_rate_type_id = ' . $this->sqlMeterCareRateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_meter_estimate_type_id = ' . $this->sqlPropertyMeterEstimateTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyMeterEstimateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_meter_estimate_type_id = ' . $this->sqlPropertyMeterEstimateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settings_datetime = ' . $this->sqlSettingsDatetime(). ',' ; } elseif( true == array_key_exists( 'SettingsDatetime', $this->getChangedColumns() ) ) { $strSql .= ' settings_datetime = ' . $this->sqlSettingsDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' installation_company_name = ' . $this->sqlInstallationCompanyName(). ',' ; } elseif( true == array_key_exists( 'InstallationCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' installation_company_name = ' . $this->sqlInstallationCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tamper_fee = ' . $this->sqlTamperFee(). ',' ; } elseif( true == array_key_exists( 'TamperFee', $this->getChangedColumns() ) ) { $strSql .= ' tamper_fee = ' . $this->sqlTamperFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rdl_dcc_phone_number = ' . $this->sqlRdlDccPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'RdlDccPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' rdl_dcc_phone_number = ' . $this->sqlRdlDccPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' installation_notes = ' . $this->sqlInstallationNotes(). ',' ; } elseif( true == array_key_exists( 'InstallationNotes', $this->getChangedColumns() ) ) { $strSql .= ' installation_notes = ' . $this->sqlInstallationNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rollout_progress_notes = ' . $this->sqlRolloutProgressNotes(). ',' ; } elseif( true == array_key_exists( 'RolloutProgressNotes', $this->getChangedColumns() ) ) { $strSql .= ' rollout_progress_notes = ' . $this->sqlRolloutProgressNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_work_repair_notes = ' . $this->sqlMeterWorkRepairNotes(). ',' ; } elseif( true == array_key_exists( 'MeterWorkRepairNotes', $this->getChangedColumns() ) ) { $strSql .= ' meter_work_repair_notes = ' . $this->sqlMeterWorkRepairNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_expected_day = ' . $this->sqlTransmissionExpectedDay(). ',' ; } elseif( true == array_key_exists( 'TransmissionExpectedDay', $this->getChangedColumns() ) ) { $strSql .= ' transmission_expected_day = ' . $this->sqlTransmissionExpectedDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_deadline_day = ' . $this->sqlTransmissionDeadlineDay(). ',' ; } elseif( true == array_key_exists( 'TransmissionDeadlineDay', $this->getChangedColumns() ) ) { $strSql .= ' transmission_deadline_day = ' . $this->sqlTransmissionDeadlineDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_emergency_day = ' . $this->sqlTransmissionEmergencyDay(). ',' ; } elseif( true == array_key_exists( 'TransmissionEmergencyDay', $this->getChangedColumns() ) ) { $strSql .= ' transmission_emergency_day = ' . $this->sqlTransmissionEmergencyDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_transmission_datetime = ' . $this->sqlLastTransmissionDatetime(). ',' ; } elseif( true == array_key_exists( 'LastTransmissionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' last_transmission_datetime = ' . $this->sqlLastTransmissionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_meter_audit_on = ' . $this->sqlLastMeterAuditOn(). ',' ; } elseif( true == array_key_exists( 'LastMeterAuditOn', $this->getChangedColumns() ) ) { $strSql .= ' last_meter_audit_on = ' . $this->sqlLastMeterAuditOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_install_completion = ' . $this->sqlExpectedInstallCompletion(). ',' ; } elseif( true == array_key_exists( 'ExpectedInstallCompletion', $this->getChangedColumns() ) ) { $strSql .= ' expected_install_completion = ' . $this->sqlExpectedInstallCompletion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_partial_capture_system = ' . $this->sqlIsPartialCaptureSystem(). ',' ; } elseif( true == array_key_exists( 'IsPartialCaptureSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_partial_capture_system = ' . $this->sqlIsPartialCaptureSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimate_missing_meter_reads = ' . $this->sqlEstimateMissingMeterReads(). ',' ; } elseif( true == array_key_exists( 'EstimateMissingMeterReads', $this->getChangedColumns() ) ) { $strSql .= ' estimate_missing_meter_reads = ' . $this->sqlEstimateMissingMeterReads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimate_zero_consumption = ' . $this->sqlEstimateZeroConsumption(). ',' ; } elseif( true == array_key_exists( 'EstimateZeroConsumption', $this->getChangedColumns() ) ) { $strSql .= ' estimate_zero_consumption = ' . $this->sqlEstimateZeroConsumption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimate_low_consumption = ' . $this->sqlEstimateLowConsumption(). ',' ; } elseif( true == array_key_exists( 'EstimateLowConsumption', $this->getChangedColumns() ) ) { $strSql .= ' estimate_low_consumption = ' . $this->sqlEstimateLowConsumption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimate_high_consumption = ' . $this->sqlEstimateHighConsumption(). ',' ; } elseif( true == array_key_exists( 'EstimateHighConsumption', $this->getChangedColumns() ) ) { $strSql .= ' estimate_high_consumption = ' . $this->sqlEstimateHighConsumption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' low_usage_threshhold = ' . $this->sqlLowUsageThreshhold(). ',' ; } elseif( true == array_key_exists( 'LowUsageThreshhold', $this->getChangedColumns() ) ) { $strSql .= ' low_usage_threshhold = ' . $this->sqlLowUsageThreshhold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' high_usage_threshhold = ' . $this->sqlHighUsageThreshhold(). ',' ; } elseif( true == array_key_exists( 'HighUsageThreshhold', $this->getChangedColumns() ) ) { $strSql .= ' high_usage_threshhold = ' . $this->sqlHighUsageThreshhold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billed_consumption_threshold = ' . $this->sqlBilledConsumptionThreshold(). ',' ; } elseif( true == array_key_exists( 'BilledConsumptionThreshold', $this->getChangedColumns() ) ) { $strSql .= ' billed_consumption_threshold = ' . $this->sqlBilledConsumptionThreshold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' low_consumption_percent = ' . $this->sqlLowConsumptionPercent(). ',' ; } elseif( true == array_key_exists( 'LowConsumptionPercent', $this->getChangedColumns() ) ) { $strSql .= ' low_consumption_percent = ' . $this->sqlLowConsumptionPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' high_consumption_percent = ' . $this->sqlHighConsumptionPercent(). ',' ; } elseif( true == array_key_exists( 'HighConsumptionPercent', $this->getChangedColumns() ) ) { $strSql .= ' high_consumption_percent = ' . $this->sqlHighConsumptionPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' true_up_percent = ' . $this->sqlTrueUpPercent(). ',' ; } elseif( true == array_key_exists( 'TrueUpPercent', $this->getChangedColumns() ) ) { $strSql .= ' true_up_percent = ' . $this->sqlTrueUpPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent_of_estimate = ' . $this->sqlPercentOfEstimate(). ',' ; } elseif( true == array_key_exists( 'PercentOfEstimate', $this->getChangedColumns() ) ) { $strSql .= ' percent_of_estimate = ' . $this->sqlPercentOfEstimate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contracted_successful_meter_reporting_rate = ' . $this->sqlContractedSuccessfulMeterReportingRate(). ',' ; } elseif( true == array_key_exists( 'ContractedSuccessfulMeterReportingRate', $this->getChangedColumns() ) ) { $strSql .= ' contracted_successful_meter_reporting_rate = ' . $this->sqlContractedSuccessfulMeterReportingRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacant_unit_consumption_cap = ' . $this->sqlVacantUnitConsumptionCap(). ',' ; } elseif( true == array_key_exists( 'VacantUnitConsumptionCap', $this->getChangedColumns() ) ) { $strSql .= ' vacant_unit_consumption_cap = ' . $this->sqlVacantUnitConsumptionCap() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allows_estimates = ' . $this->sqlAllowsEstimates(). ',' ; } elseif( true == array_key_exists( 'AllowsEstimates', $this->getChangedColumns() ) ) { $strSql .= ' allows_estimates = ' . $this->sqlAllowsEstimates() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_percent_estimate = ' . $this->sqlUsePercentEstimate(). ',' ; } elseif( true == array_key_exists( 'UsePercentEstimate', $this->getChangedColumns() ) ) { $strSql .= ' use_percent_estimate = ' . $this->sqlUsePercentEstimate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_occupancy_average = ' . $this->sqlIsOccupancyAverage(). ',' ; } elseif( true == array_key_exists( 'IsOccupancyAverage', $this->getChangedColumns() ) ) { $strSql .= ' is_occupancy_average = ' . $this->sqlIsOccupancyAverage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_high_low_consumption_in_averages = ' . $this->sqlIncludeHighLowConsumptionInAverages(). ',' ; } elseif( true == array_key_exists( 'IncludeHighLowConsumptionInAverages', $this->getChangedColumns() ) ) { $strSql .= ' include_high_low_consumption_in_averages = ' . $this->sqlIncludeHighLowConsumptionInAverages() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' btu_factor = ' . $this->sqlBtuFactor(). ',' ; } elseif( true == array_key_exists( 'BtuFactor', $this->getChangedColumns() ) ) { $strSql .= ' btu_factor = ' . $this->sqlBtuFactor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' daily_allowance = ' . $this->sqlDailyAllowance(). ',' ; } elseif( true == array_key_exists( 'DailyAllowance', $this->getChangedColumns() ) ) { $strSql .= ' daily_allowance = ' . $this->sqlDailyAllowance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_rate_change_date = ' . $this->sqlMeterRateChangeDate(). ',' ; } elseif( true == array_key_exists( 'MeterRateChangeDate', $this->getChangedColumns() ) ) { $strSql .= ' meter_rate_change_date = ' . $this->sqlMeterRateChangeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_care_rate_change_date = ' . $this->sqlMeterCareRateChangeDate(). ',' ; } elseif( true == array_key_exists( 'MeterCareRateChangeDate', $this->getChangedColumns() ) ) { $strSql .= ' meter_care_rate_change_date = ' . $this->sqlMeterCareRateChangeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_estimation_months = ' . $this->sqlMaxEstimationMonths(). ',' ; } elseif( true == array_key_exists( 'MaxEstimationMonths', $this->getChangedColumns() ) ) { $strSql .= ' max_estimation_months = ' . $this->sqlMaxEstimationMonths() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'expense_property_utility_type_id' => $this->getExpensePropertyUtilityTypeId(),
			'utility_transmission_type_id' => $this->getUtilityTransmissionTypeId(),
			'meter_rate_type_id' => $this->getMeterRateTypeId(),
			'meter_care_rate_type_id' => $this->getMeterCareRateTypeId(),
			'property_meter_estimate_type_id' => $this->getPropertyMeterEstimateTypeId(),
			'settings_datetime' => $this->getSettingsDatetime(),
			'installation_company_name' => $this->getInstallationCompanyName(),
			'description' => $this->getDescription(),
			'tamper_fee' => $this->getTamperFee(),
			'rdl_dcc_phone_number' => $this->getRdlDccPhoneNumber(),
			'installation_notes' => $this->getInstallationNotes(),
			'rollout_progress_notes' => $this->getRolloutProgressNotes(),
			'meter_work_repair_notes' => $this->getMeterWorkRepairNotes(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'transmission_expected_day' => $this->getTransmissionExpectedDay(),
			'transmission_deadline_day' => $this->getTransmissionDeadlineDay(),
			'transmission_emergency_day' => $this->getTransmissionEmergencyDay(),
			'last_transmission_datetime' => $this->getLastTransmissionDatetime(),
			'last_meter_audit_on' => $this->getLastMeterAuditOn(),
			'expected_install_completion' => $this->getExpectedInstallCompletion(),
			'is_partial_capture_system' => $this->getIsPartialCaptureSystem(),
			'estimate_missing_meter_reads' => $this->getEstimateMissingMeterReads(),
			'estimate_zero_consumption' => $this->getEstimateZeroConsumption(),
			'estimate_low_consumption' => $this->getEstimateLowConsumption(),
			'estimate_high_consumption' => $this->getEstimateHighConsumption(),
			'low_usage_threshhold' => $this->getLowUsageThreshhold(),
			'high_usage_threshhold' => $this->getHighUsageThreshhold(),
			'billed_consumption_threshold' => $this->getBilledConsumptionThreshold(),
			'low_consumption_percent' => $this->getLowConsumptionPercent(),
			'high_consumption_percent' => $this->getHighConsumptionPercent(),
			'true_up_percent' => $this->getTrueUpPercent(),
			'percent_of_estimate' => $this->getPercentOfEstimate(),
			'contracted_successful_meter_reporting_rate' => $this->getContractedSuccessfulMeterReportingRate(),
			'vacant_unit_consumption_cap' => $this->getVacantUnitConsumptionCap(),
			'allows_estimates' => $this->getAllowsEstimates(),
			'use_percent_estimate' => $this->getUsePercentEstimate(),
			'is_occupancy_average' => $this->getIsOccupancyAverage(),
			'include_high_low_consumption_in_averages' => $this->getIncludeHighLowConsumptionInAverages(),
			'btu_factor' => $this->getBtuFactor(),
			'daily_allowance' => $this->getDailyAllowance(),
			'meter_rate_change_date' => $this->getMeterRateChangeDate(),
			'meter_care_rate_change_date' => $this->getMeterCareRateChangeDate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'max_estimation_months' => $this->getMaxEstimationMonths()
		);
	}

}
?>