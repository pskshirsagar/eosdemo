<?php

class CBaseOrderHeaderDoc extends CEosSingularBase {

	const TABLE_NAME = 'public.order_header_docs';

	protected $m_intId;
	protected $m_intOrderHeaderId;
	protected $m_intDocId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['order_header_id'] ) && $boolDirectSet ) $this->set( 'm_intOrderHeaderId', trim( $arrValues['order_header_id'] ) ); elseif( isset( $arrValues['order_header_id'] ) ) $this->setOrderHeaderId( $arrValues['order_header_id'] );
		if( isset( $arrValues['doc_id'] ) && $boolDirectSet ) $this->set( 'm_intDocId', trim( $arrValues['doc_id'] ) ); elseif( isset( $arrValues['doc_id'] ) ) $this->setDocId( $arrValues['doc_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOrderHeaderId( $intOrderHeaderId ) {
		$this->set( 'm_intOrderHeaderId', CStrings::strToIntDef( $intOrderHeaderId, NULL, false ) );
	}

	public function getOrderHeaderId() {
		return $this->m_intOrderHeaderId;
	}

	public function sqlOrderHeaderId() {
		return ( true == isset( $this->m_intOrderHeaderId ) ) ? ( string ) $this->m_intOrderHeaderId : 'NULL';
	}

	public function setDocId( $intDocId ) {
		$this->set( 'm_intDocId', CStrings::strToIntDef( $intDocId, NULL, false ) );
	}

	public function getDocId() {
		return $this->m_intDocId;
	}

	public function sqlDocId() {
		return ( true == isset( $this->m_intDocId ) ) ? ( string ) $this->m_intDocId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, order_header_id, doc_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlOrderHeaderId() . ', ' .
 						$this->sqlDocId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_header_id = ' . $this->sqlOrderHeaderId() . ','; } elseif( true == array_key_exists( 'OrderHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' order_header_id = ' . $this->sqlOrderHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' doc_id = ' . $this->sqlDocId() . ','; } elseif( true == array_key_exists( 'DocId', $this->getChangedColumns() ) ) { $strSql .= ' doc_id = ' . $this->sqlDocId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'order_header_id' => $this->getOrderHeaderId(),
			'doc_id' => $this->getDocId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>