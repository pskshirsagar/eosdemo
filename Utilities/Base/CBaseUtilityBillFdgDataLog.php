<?php

class CBaseUtilityBillFdgDataLog extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_fdg_data_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intFdgDataLogTypeId;
	protected $m_intUtilityBillId;
	protected $m_intImplementationUtilityBillId;
	protected $m_arrintChildBillIds;
	protected $m_strRemotePrimaryKey;
	protected $m_strLogDatetime;
	protected $m_strJsonData;
	protected $m_strError;
	protected $m_boolIsFailed;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsFailed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['fdg_data_log_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFdgDataLogTypeId', trim( $arrValues['fdg_data_log_type_id'] ) ); elseif( isset( $arrValues['fdg_data_log_type_id'] ) ) $this->setFdgDataLogTypeId( $arrValues['fdg_data_log_type_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['implementation_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationUtilityBillId', trim( $arrValues['implementation_utility_bill_id'] ) ); elseif( isset( $arrValues['implementation_utility_bill_id'] ) ) $this->setImplementationUtilityBillId( $arrValues['implementation_utility_bill_id'] );
		if( isset( $arrValues['child_bill_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintChildBillIds', trim( $arrValues['child_bill_ids'] ) ); elseif( isset( $arrValues['child_bill_ids'] ) ) $this->setChildBillIds( $arrValues['child_bill_ids'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['json_data'] ) && $boolDirectSet ) $this->set( 'm_strJsonData', trim( stripcslashes( $arrValues['json_data'] ) ) ); elseif( isset( $arrValues['json_data'] ) ) $this->setJsonData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['json_data'] ) : $arrValues['json_data'] );
		if( isset( $arrValues['error'] ) && $boolDirectSet ) $this->set( 'm_strError', trim( $arrValues['error'] ) ); elseif( isset( $arrValues['error'] ) ) $this->setError( $arrValues['error'] );
		if( isset( $arrValues['is_failed'] ) && $boolDirectSet ) $this->set( 'm_boolIsFailed', trim( stripcslashes( $arrValues['is_failed'] ) ) ); elseif( isset( $arrValues['is_failed'] ) ) $this->setIsFailed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_failed'] ) : $arrValues['is_failed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setFdgDataLogTypeId( $intFdgDataLogTypeId ) {
		$this->set( 'm_intFdgDataLogTypeId', CStrings::strToIntDef( $intFdgDataLogTypeId, NULL, false ) );
	}

	public function getFdgDataLogTypeId() {
		return $this->m_intFdgDataLogTypeId;
	}

	public function sqlFdgDataLogTypeId() {
		return ( true == isset( $this->m_intFdgDataLogTypeId ) ) ? ( string ) $this->m_intFdgDataLogTypeId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setImplementationUtilityBillId( $intImplementationUtilityBillId ) {
		$this->set( 'm_intImplementationUtilityBillId', CStrings::strToIntDef( $intImplementationUtilityBillId, NULL, false ) );
	}

	public function getImplementationUtilityBillId() {
		return $this->m_intImplementationUtilityBillId;
	}

	public function sqlImplementationUtilityBillId() {
		return ( true == isset( $this->m_intImplementationUtilityBillId ) ) ? ( string ) $this->m_intImplementationUtilityBillId : 'NULL';
	}

	public function setChildBillIds( $arrintChildBillIds ) {
		$this->set( 'm_arrintChildBillIds', CStrings::strToArrIntDef( $arrintChildBillIds, NULL ) );
	}

	public function getChildBillIds() {
		return $this->m_arrintChildBillIds;
	}

	public function sqlChildBillIds() {
		return ( true == isset( $this->m_arrintChildBillIds ) && true == valArr( $this->m_arrintChildBillIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintChildBillIds, NULL ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 80, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setJsonData( $strJsonData ) {
		$this->set( 'm_strJsonData', CStrings::strTrimDef( $strJsonData, -1, NULL, true ) );
	}

	public function getJsonData() {
		return $this->m_strJsonData;
	}

	public function sqlJsonData() {
		return ( true == isset( $this->m_strJsonData ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strJsonData ) : '\'' . addslashes( $this->m_strJsonData ) . '\'' ) : 'NULL';
	}

	public function setError( $strError ) {
		$this->set( 'm_strError', CStrings::strTrimDef( $strError, -1, NULL, true ) );
	}

	public function getError() {
		return $this->m_strError;
	}

	public function sqlError() {
		return ( true == isset( $this->m_strError ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strError ) : '\'' . addslashes( $this->m_strError ) . '\'' ) : 'NULL';
	}

	public function setIsFailed( $boolIsFailed ) {
		$this->set( 'm_boolIsFailed', CStrings::strToBool( $boolIsFailed ) );
	}

	public function getIsFailed() {
		return $this->m_boolIsFailed;
	}

	public function sqlIsFailed() {
		return ( true == isset( $this->m_boolIsFailed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFailed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, fdg_data_log_type_id, utility_bill_id, implementation_utility_bill_id, child_bill_ids, remote_primary_key, log_datetime, json_data, error, is_failed, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlFdgDataLogTypeId() . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlImplementationUtilityBillId() . ', ' .
						$this->sqlChildBillIds() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlJsonData() . ', ' .
						$this->sqlError() . ', ' .
						$this->sqlIsFailed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fdg_data_log_type_id = ' . $this->sqlFdgDataLogTypeId(). ',' ; } elseif( true == array_key_exists( 'FdgDataLogTypeId', $this->getChangedColumns() ) ) { $strSql .= ' fdg_data_log_type_id = ' . $this->sqlFdgDataLogTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_utility_bill_id = ' . $this->sqlImplementationUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'ImplementationUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_utility_bill_id = ' . $this->sqlImplementationUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' child_bill_ids = ' . $this->sqlChildBillIds(). ',' ; } elseif( true == array_key_exists( 'ChildBillIds', $this->getChangedColumns() ) ) { $strSql .= ' child_bill_ids = ' . $this->sqlChildBillIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' json_data = ' . $this->sqlJsonData(). ',' ; } elseif( true == array_key_exists( 'JsonData', $this->getChangedColumns() ) ) { $strSql .= ' json_data = ' . $this->sqlJsonData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error = ' . $this->sqlError(). ',' ; } elseif( true == array_key_exists( 'Error', $this->getChangedColumns() ) ) { $strSql .= ' error = ' . $this->sqlError() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed(). ',' ; } elseif( true == array_key_exists( 'IsFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'fdg_data_log_type_id' => $this->getFdgDataLogTypeId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'implementation_utility_bill_id' => $this->getImplementationUtilityBillId(),
			'child_bill_ids' => $this->getChildBillIds(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'log_datetime' => $this->getLogDatetime(),
			'json_data' => $this->getJsonData(),
			'error' => $this->getError(),
			'is_failed' => $this->getIsFailed(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>