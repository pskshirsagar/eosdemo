<?php

class CBaseEnergyStarMetrics extends CEosPluralBase {

	/**
	 * @return CEnergyStarMetric[]
	 */
	public static function fetchEnergyStarMetrics( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEnergyStarMetric::class, $objDatabase );
	}

	/**
	 * @return CEnergyStarMetric
	 */
	public static function fetchEnergyStarMetric( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEnergyStarMetric::class, $objDatabase );
	}

	public static function fetchEnergyStarMetricCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'energy_star_metrics', $objDatabase );
	}

	public static function fetchEnergyStarMetricById( $intId, $objDatabase ) {
		return self::fetchEnergyStarMetric( sprintf( 'SELECT * FROM energy_star_metrics WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEnergyStarMetricsByCid( $intCid, $objDatabase ) {
		return self::fetchEnergyStarMetrics( sprintf( 'SELECT * FROM energy_star_metrics WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEnergyStarMetricsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchEnergyStarMetrics( sprintf( 'SELECT * FROM energy_star_metrics WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchEnergyStarMetricsByEnergyStarCustomerPropertyId( $intEnergyStarCustomerPropertyId, $objDatabase ) {
		return self::fetchEnergyStarMetrics( sprintf( 'SELECT * FROM energy_star_metrics WHERE energy_star_customer_property_id = %d', ( int ) $intEnergyStarCustomerPropertyId ), $objDatabase );
	}

}
?>