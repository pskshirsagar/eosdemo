<?php

class CBasePropertyUtilitySetting extends CEosSingularBase {

	const TABLE_NAME = 'public.property_utility_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intOldPropertyId;
	protected $m_intBillingFeeAccountId;
	protected $m_intCompanyEmployeeId;
	protected $m_intRegionalCompanyEmployeeId;
	protected $m_intAnalystEmployeeId;
	protected $m_intBillingImplementationEmployeeId;
	protected $m_intFaxCallPhoneNumberId;
	protected $m_intSupportCallPhoneNumberId;
	protected $m_intUtilityCompetitorId;
	protected $m_arrintMissingBillCompanyEmployeeIds;
	protected $m_fltPsLateFeePercent;
	protected $m_intPreBatchDay;
	protected $m_intDistributionDay;
	protected $m_intFirstMissingBillWarningDays;
	protected $m_intSecondMissingBillWarningDays;
	protected $m_strUtilityRuleNotes;
	protected $m_strCodeVersion;
	protected $m_strInitiationDate;
	protected $m_strTerminationDate;
	protected $m_boolIsInImplementation;
	protected $m_boolIsExportUtilityBills;
	protected $m_boolIsTestDistribution;
	protected $m_boolIsDataEntryInIndia;
	protected $m_strLastBilledOn;
	protected $m_strValidatedOn;
	protected $m_intLegalApprovedBy;
	protected $m_strLegalApprovedOn;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_arrintDecisionMakerCompanyEmployeeIds;
	protected $m_intVerifyInvoiceDay;
	protected $m_intUemImplementationEmployeeId;
	protected $m_strBillingImplementationAssignedOn;
	protected $m_strUemImplementationAssignedOn;
	protected $m_strBillingImplementationScheduledOn;
	protected $m_strUemImplementationScheduledOn;
	protected $m_strBillingImplementationCompletedOn;
	protected $m_strUemImplementationCompletedOn;
	protected $m_boolExportRuBillsAsIp;

	public function __construct() {
		parent::__construct();

		$this->m_fltPsLateFeePercent = '0';
		$this->m_intPreBatchDay = '1';
		$this->m_intFirstMissingBillWarningDays = '5';
		$this->m_intSecondMissingBillWarningDays = '1';
		$this->m_boolIsInImplementation = true;
		$this->m_boolIsExportUtilityBills = false;
		$this->m_boolIsTestDistribution = false;
		$this->m_boolIsDataEntryInIndia = true;
		$this->m_intVerifyInvoiceDay = '0';
		$this->m_boolExportRuBillsAsIp = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['old_property_id'] ) && $boolDirectSet ) $this->set( 'm_intOldPropertyId', trim( $arrValues['old_property_id'] ) ); elseif( isset( $arrValues['old_property_id'] ) ) $this->setOldPropertyId( $arrValues['old_property_id'] );
		if( isset( $arrValues['billing_fee_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingFeeAccountId', trim( $arrValues['billing_fee_account_id'] ) ); elseif( isset( $arrValues['billing_fee_account_id'] ) ) $this->setBillingFeeAccountId( $arrValues['billing_fee_account_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['regional_company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intRegionalCompanyEmployeeId', trim( $arrValues['regional_company_employee_id'] ) ); elseif( isset( $arrValues['regional_company_employee_id'] ) ) $this->setRegionalCompanyEmployeeId( $arrValues['regional_company_employee_id'] );
		if( isset( $arrValues['analyst_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intAnalystEmployeeId', trim( $arrValues['analyst_employee_id'] ) ); elseif( isset( $arrValues['analyst_employee_id'] ) ) $this->setAnalystEmployeeId( $arrValues['analyst_employee_id'] );
		if( isset( $arrValues['billing_implementation_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingImplementationEmployeeId', trim( $arrValues['billing_implementation_employee_id'] ) ); elseif( isset( $arrValues['billing_implementation_employee_id'] ) ) $this->setBillingImplementationEmployeeId( $arrValues['billing_implementation_employee_id'] );
		if( isset( $arrValues['fax_call_phone_number_id'] ) && $boolDirectSet ) $this->set( 'm_intFaxCallPhoneNumberId', trim( $arrValues['fax_call_phone_number_id'] ) ); elseif( isset( $arrValues['fax_call_phone_number_id'] ) ) $this->setFaxCallPhoneNumberId( $arrValues['fax_call_phone_number_id'] );
		if( isset( $arrValues['support_call_phone_number_id'] ) && $boolDirectSet ) $this->set( 'm_intSupportCallPhoneNumberId', trim( $arrValues['support_call_phone_number_id'] ) ); elseif( isset( $arrValues['support_call_phone_number_id'] ) ) $this->setSupportCallPhoneNumberId( $arrValues['support_call_phone_number_id'] );
		if( isset( $arrValues['utility_competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityCompetitorId', trim( $arrValues['utility_competitor_id'] ) ); elseif( isset( $arrValues['utility_competitor_id'] ) ) $this->setUtilityCompetitorId( $arrValues['utility_competitor_id'] );
		if( isset( $arrValues['missing_bill_company_employee_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintMissingBillCompanyEmployeeIds', trim( $arrValues['missing_bill_company_employee_ids'] ) ); elseif( isset( $arrValues['missing_bill_company_employee_ids'] ) ) $this->setMissingBillCompanyEmployeeIds( $arrValues['missing_bill_company_employee_ids'] );
		if( isset( $arrValues['ps_late_fee_percent'] ) && $boolDirectSet ) $this->set( 'm_fltPsLateFeePercent', trim( $arrValues['ps_late_fee_percent'] ) ); elseif( isset( $arrValues['ps_late_fee_percent'] ) ) $this->setPsLateFeePercent( $arrValues['ps_late_fee_percent'] );
		if( isset( $arrValues['pre_batch_day'] ) && $boolDirectSet ) $this->set( 'm_intPreBatchDay', trim( $arrValues['pre_batch_day'] ) ); elseif( isset( $arrValues['pre_batch_day'] ) ) $this->setPreBatchDay( $arrValues['pre_batch_day'] );
		if( isset( $arrValues['distribution_day'] ) && $boolDirectSet ) $this->set( 'm_intDistributionDay', trim( $arrValues['distribution_day'] ) ); elseif( isset( $arrValues['distribution_day'] ) ) $this->setDistributionDay( $arrValues['distribution_day'] );
		if( isset( $arrValues['first_missing_bill_warning_days'] ) && $boolDirectSet ) $this->set( 'm_intFirstMissingBillWarningDays', trim( $arrValues['first_missing_bill_warning_days'] ) ); elseif( isset( $arrValues['first_missing_bill_warning_days'] ) ) $this->setFirstMissingBillWarningDays( $arrValues['first_missing_bill_warning_days'] );
		if( isset( $arrValues['second_missing_bill_warning_days'] ) && $boolDirectSet ) $this->set( 'm_intSecondMissingBillWarningDays', trim( $arrValues['second_missing_bill_warning_days'] ) ); elseif( isset( $arrValues['second_missing_bill_warning_days'] ) ) $this->setSecondMissingBillWarningDays( $arrValues['second_missing_bill_warning_days'] );
		if( isset( $arrValues['utility_rule_notes'] ) && $boolDirectSet ) $this->set( 'm_strUtilityRuleNotes', trim( $arrValues['utility_rule_notes'] ) ); elseif( isset( $arrValues['utility_rule_notes'] ) ) $this->setUtilityRuleNotes( $arrValues['utility_rule_notes'] );
		if( isset( $arrValues['code_version'] ) && $boolDirectSet ) $this->set( 'm_strCodeVersion', trim( $arrValues['code_version'] ) ); elseif( isset( $arrValues['code_version'] ) ) $this->setCodeVersion( $arrValues['code_version'] );
		if( isset( $arrValues['initiation_date'] ) && $boolDirectSet ) $this->set( 'm_strInitiationDate', trim( $arrValues['initiation_date'] ) ); elseif( isset( $arrValues['initiation_date'] ) ) $this->setInitiationDate( $arrValues['initiation_date'] );
		if( isset( $arrValues['termination_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationDate', trim( $arrValues['termination_date'] ) ); elseif( isset( $arrValues['termination_date'] ) ) $this->setTerminationDate( $arrValues['termination_date'] );
		if( isset( $arrValues['is_in_implementation'] ) && $boolDirectSet ) $this->set( 'm_boolIsInImplementation', trim( stripcslashes( $arrValues['is_in_implementation'] ) ) ); elseif( isset( $arrValues['is_in_implementation'] ) ) $this->setIsInImplementation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_in_implementation'] ) : $arrValues['is_in_implementation'] );
		if( isset( $arrValues['is_export_utility_bills'] ) && $boolDirectSet ) $this->set( 'm_boolIsExportUtilityBills', trim( stripcslashes( $arrValues['is_export_utility_bills'] ) ) ); elseif( isset( $arrValues['is_export_utility_bills'] ) ) $this->setIsExportUtilityBills( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_export_utility_bills'] ) : $arrValues['is_export_utility_bills'] );
		if( isset( $arrValues['is_test_distribution'] ) && $boolDirectSet ) $this->set( 'm_boolIsTestDistribution', trim( stripcslashes( $arrValues['is_test_distribution'] ) ) ); elseif( isset( $arrValues['is_test_distribution'] ) ) $this->setIsTestDistribution( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_test_distribution'] ) : $arrValues['is_test_distribution'] );
		if( isset( $arrValues['is_data_entry_in_india'] ) && $boolDirectSet ) $this->set( 'm_boolIsDataEntryInIndia', trim( stripcslashes( $arrValues['is_data_entry_in_india'] ) ) ); elseif( isset( $arrValues['is_data_entry_in_india'] ) ) $this->setIsDataEntryInIndia( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_data_entry_in_india'] ) : $arrValues['is_data_entry_in_india'] );
		if( isset( $arrValues['last_billed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastBilledOn', trim( $arrValues['last_billed_on'] ) ); elseif( isset( $arrValues['last_billed_on'] ) ) $this->setLastBilledOn( $arrValues['last_billed_on'] );
		if( isset( $arrValues['validated_on'] ) && $boolDirectSet ) $this->set( 'm_strValidatedOn', trim( $arrValues['validated_on'] ) ); elseif( isset( $arrValues['validated_on'] ) ) $this->setValidatedOn( $arrValues['validated_on'] );
		if( isset( $arrValues['legal_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intLegalApprovedBy', trim( $arrValues['legal_approved_by'] ) ); elseif( isset( $arrValues['legal_approved_by'] ) ) $this->setLegalApprovedBy( $arrValues['legal_approved_by'] );
		if( isset( $arrValues['legal_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strLegalApprovedOn', trim( $arrValues['legal_approved_on'] ) ); elseif( isset( $arrValues['legal_approved_on'] ) ) $this->setLegalApprovedOn( $arrValues['legal_approved_on'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['decision_maker_company_employee_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintDecisionMakerCompanyEmployeeIds', trim( $arrValues['decision_maker_company_employee_ids'] ) ); elseif( isset( $arrValues['decision_maker_company_employee_ids'] ) ) $this->setDecisionMakerCompanyEmployeeIds( $arrValues['decision_maker_company_employee_ids'] );
		if( isset( $arrValues['verify_invoice_day'] ) && $boolDirectSet ) $this->set( 'm_intVerifyInvoiceDay', trim( $arrValues['verify_invoice_day'] ) ); elseif( isset( $arrValues['verify_invoice_day'] ) ) $this->setVerifyInvoiceDay( $arrValues['verify_invoice_day'] );
		if( isset( $arrValues['uem_implementation_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intUemImplementationEmployeeId', trim( $arrValues['uem_implementation_employee_id'] ) ); elseif( isset( $arrValues['uem_implementation_employee_id'] ) ) $this->setUemImplementationEmployeeId( $arrValues['uem_implementation_employee_id'] );
		if( isset( $arrValues['billing_implementation_assigned_on'] ) && $boolDirectSet ) $this->set( 'm_strBillingImplementationAssignedOn', trim( $arrValues['billing_implementation_assigned_on'] ) ); elseif( isset( $arrValues['billing_implementation_assigned_on'] ) ) $this->setBillingImplementationAssignedOn( $arrValues['billing_implementation_assigned_on'] );
		if( isset( $arrValues['uem_implementation_assigned_on'] ) && $boolDirectSet ) $this->set( 'm_strUemImplementationAssignedOn', trim( $arrValues['uem_implementation_assigned_on'] ) ); elseif( isset( $arrValues['uem_implementation_assigned_on'] ) ) $this->setUemImplementationAssignedOn( $arrValues['uem_implementation_assigned_on'] );
		if( isset( $arrValues['billing_implementation_scheduled_on'] ) && $boolDirectSet ) $this->set( 'm_strBillingImplementationScheduledOn', trim( $arrValues['billing_implementation_scheduled_on'] ) ); elseif( isset( $arrValues['billing_implementation_scheduled_on'] ) ) $this->setBillingImplementationScheduledOn( $arrValues['billing_implementation_scheduled_on'] );
		if( isset( $arrValues['uem_implementation_scheduled_on'] ) && $boolDirectSet ) $this->set( 'm_strUemImplementationScheduledOn', trim( $arrValues['uem_implementation_scheduled_on'] ) ); elseif( isset( $arrValues['uem_implementation_scheduled_on'] ) ) $this->setUemImplementationScheduledOn( $arrValues['uem_implementation_scheduled_on'] );
		if( isset( $arrValues['billing_implementation_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strBillingImplementationCompletedOn', trim( $arrValues['billing_implementation_completed_on'] ) ); elseif( isset( $arrValues['billing_implementation_completed_on'] ) ) $this->setBillingImplementationCompletedOn( $arrValues['billing_implementation_completed_on'] );
		if( isset( $arrValues['uem_implementation_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strUemImplementationCompletedOn', trim( $arrValues['uem_implementation_completed_on'] ) ); elseif( isset( $arrValues['uem_implementation_completed_on'] ) ) $this->setUemImplementationCompletedOn( $arrValues['uem_implementation_completed_on'] );
		if( isset( $arrValues['export_ru_bills_as_ip'] ) && $boolDirectSet ) $this->set( 'm_boolExportRuBillsAsIp', trim( stripcslashes( $arrValues['export_ru_bills_as_ip'] ) ) ); elseif( isset( $arrValues['export_ru_bills_as_ip'] ) ) $this->setExportRuBillsAsIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['export_ru_bills_as_ip'] ) : $arrValues['export_ru_bills_as_ip'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setOldPropertyId( $intOldPropertyId ) {
		$this->set( 'm_intOldPropertyId', CStrings::strToIntDef( $intOldPropertyId, NULL, false ) );
	}

	public function getOldPropertyId() {
		return $this->m_intOldPropertyId;
	}

	public function sqlOldPropertyId() {
		return ( true == isset( $this->m_intOldPropertyId ) ) ? ( string ) $this->m_intOldPropertyId : 'NULL';
	}

	public function setBillingFeeAccountId( $intBillingFeeAccountId ) {
		$this->set( 'm_intBillingFeeAccountId', CStrings::strToIntDef( $intBillingFeeAccountId, NULL, false ) );
	}

	public function getBillingFeeAccountId() {
		return $this->m_intBillingFeeAccountId;
	}

	public function sqlBillingFeeAccountId() {
		return ( true == isset( $this->m_intBillingFeeAccountId ) ) ? ( string ) $this->m_intBillingFeeAccountId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setRegionalCompanyEmployeeId( $intRegionalCompanyEmployeeId ) {
		$this->set( 'm_intRegionalCompanyEmployeeId', CStrings::strToIntDef( $intRegionalCompanyEmployeeId, NULL, false ) );
	}

	public function getRegionalCompanyEmployeeId() {
		return $this->m_intRegionalCompanyEmployeeId;
	}

	public function sqlRegionalCompanyEmployeeId() {
		return ( true == isset( $this->m_intRegionalCompanyEmployeeId ) ) ? ( string ) $this->m_intRegionalCompanyEmployeeId : 'NULL';
	}

	public function setAnalystEmployeeId( $intAnalystEmployeeId ) {
		$this->set( 'm_intAnalystEmployeeId', CStrings::strToIntDef( $intAnalystEmployeeId, NULL, false ) );
	}

	public function getAnalystEmployeeId() {
		return $this->m_intAnalystEmployeeId;
	}

	public function sqlAnalystEmployeeId() {
		return ( true == isset( $this->m_intAnalystEmployeeId ) ) ? ( string ) $this->m_intAnalystEmployeeId : 'NULL';
	}

	public function setBillingImplementationEmployeeId( $intBillingImplementationEmployeeId ) {
		$this->set( 'm_intBillingImplementationEmployeeId', CStrings::strToIntDef( $intBillingImplementationEmployeeId, NULL, false ) );
	}

	public function getBillingImplementationEmployeeId() {
		return $this->m_intBillingImplementationEmployeeId;
	}

	public function sqlBillingImplementationEmployeeId() {
		return ( true == isset( $this->m_intBillingImplementationEmployeeId ) ) ? ( string ) $this->m_intBillingImplementationEmployeeId : 'NULL';
	}

	public function setFaxCallPhoneNumberId( $intFaxCallPhoneNumberId ) {
		$this->set( 'm_intFaxCallPhoneNumberId', CStrings::strToIntDef( $intFaxCallPhoneNumberId, NULL, false ) );
	}

	public function getFaxCallPhoneNumberId() {
		return $this->m_intFaxCallPhoneNumberId;
	}

	public function sqlFaxCallPhoneNumberId() {
		return ( true == isset( $this->m_intFaxCallPhoneNumberId ) ) ? ( string ) $this->m_intFaxCallPhoneNumberId : 'NULL';
	}

	public function setSupportCallPhoneNumberId( $intSupportCallPhoneNumberId ) {
		$this->set( 'm_intSupportCallPhoneNumberId', CStrings::strToIntDef( $intSupportCallPhoneNumberId, NULL, false ) );
	}

	public function getSupportCallPhoneNumberId() {
		return $this->m_intSupportCallPhoneNumberId;
	}

	public function sqlSupportCallPhoneNumberId() {
		return ( true == isset( $this->m_intSupportCallPhoneNumberId ) ) ? ( string ) $this->m_intSupportCallPhoneNumberId : 'NULL';
	}

	public function setUtilityCompetitorId( $intUtilityCompetitorId ) {
		$this->set( 'm_intUtilityCompetitorId', CStrings::strToIntDef( $intUtilityCompetitorId, NULL, false ) );
	}

	public function getUtilityCompetitorId() {
		return $this->m_intUtilityCompetitorId;
	}

	public function sqlUtilityCompetitorId() {
		return ( true == isset( $this->m_intUtilityCompetitorId ) ) ? ( string ) $this->m_intUtilityCompetitorId : 'NULL';
	}

	public function setMissingBillCompanyEmployeeIds( $arrintMissingBillCompanyEmployeeIds ) {
		$this->set( 'm_arrintMissingBillCompanyEmployeeIds', CStrings::strToArrIntDef( $arrintMissingBillCompanyEmployeeIds, NULL ) );
	}

	public function getMissingBillCompanyEmployeeIds() {
		return $this->m_arrintMissingBillCompanyEmployeeIds;
	}

	public function sqlMissingBillCompanyEmployeeIds() {
		return ( true == isset( $this->m_arrintMissingBillCompanyEmployeeIds ) && true == valArr( $this->m_arrintMissingBillCompanyEmployeeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintMissingBillCompanyEmployeeIds, NULL ) . '\'' : 'NULL';
	}

	public function setPsLateFeePercent( $fltPsLateFeePercent ) {
		$this->set( 'm_fltPsLateFeePercent', CStrings::strToFloatDef( $fltPsLateFeePercent, NULL, false, 6 ) );
	}

	public function getPsLateFeePercent() {
		return $this->m_fltPsLateFeePercent;
	}

	public function sqlPsLateFeePercent() {
		return ( true == isset( $this->m_fltPsLateFeePercent ) ) ? ( string ) $this->m_fltPsLateFeePercent : '0';
	}

	public function setPreBatchDay( $intPreBatchDay ) {
		$this->set( 'm_intPreBatchDay', CStrings::strToIntDef( $intPreBatchDay, NULL, false ) );
	}

	public function getPreBatchDay() {
		return $this->m_intPreBatchDay;
	}

	public function sqlPreBatchDay() {
		return ( true == isset( $this->m_intPreBatchDay ) ) ? ( string ) $this->m_intPreBatchDay : '1';
	}

	public function setDistributionDay( $intDistributionDay ) {
		$this->set( 'm_intDistributionDay', CStrings::strToIntDef( $intDistributionDay, NULL, false ) );
	}

	public function getDistributionDay() {
		return $this->m_intDistributionDay;
	}

	public function sqlDistributionDay() {
		return ( true == isset( $this->m_intDistributionDay ) ) ? ( string ) $this->m_intDistributionDay : 'NULL';
	}

	public function setFirstMissingBillWarningDays( $intFirstMissingBillWarningDays ) {
		$this->set( 'm_intFirstMissingBillWarningDays', CStrings::strToIntDef( $intFirstMissingBillWarningDays, NULL, false ) );
	}

	public function getFirstMissingBillWarningDays() {
		return $this->m_intFirstMissingBillWarningDays;
	}

	public function sqlFirstMissingBillWarningDays() {
		return ( true == isset( $this->m_intFirstMissingBillWarningDays ) ) ? ( string ) $this->m_intFirstMissingBillWarningDays : '5';
	}

	public function setSecondMissingBillWarningDays( $intSecondMissingBillWarningDays ) {
		$this->set( 'm_intSecondMissingBillWarningDays', CStrings::strToIntDef( $intSecondMissingBillWarningDays, NULL, false ) );
	}

	public function getSecondMissingBillWarningDays() {
		return $this->m_intSecondMissingBillWarningDays;
	}

	public function sqlSecondMissingBillWarningDays() {
		return ( true == isset( $this->m_intSecondMissingBillWarningDays ) ) ? ( string ) $this->m_intSecondMissingBillWarningDays : '1';
	}

	public function setUtilityRuleNotes( $strUtilityRuleNotes ) {
		$this->set( 'm_strUtilityRuleNotes', CStrings::strTrimDef( $strUtilityRuleNotes, -1, NULL, true ) );
	}

	public function getUtilityRuleNotes() {
		return $this->m_strUtilityRuleNotes;
	}

	public function sqlUtilityRuleNotes() {
		return ( true == isset( $this->m_strUtilityRuleNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUtilityRuleNotes ) : '\'' . addslashes( $this->m_strUtilityRuleNotes ) . '\'' ) : 'NULL';
	}

	public function setCodeVersion( $strCodeVersion ) {
		$this->set( 'm_strCodeVersion', CStrings::strTrimDef( $strCodeVersion, 10, NULL, true ) );
	}

	public function getCodeVersion() {
		return $this->m_strCodeVersion;
	}

	public function sqlCodeVersion() {
		return ( true == isset( $this->m_strCodeVersion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCodeVersion ) : '\'' . addslashes( $this->m_strCodeVersion ) . '\'' ) : 'NULL';
	}

	public function setInitiationDate( $strInitiationDate ) {
		$this->set( 'm_strInitiationDate', CStrings::strTrimDef( $strInitiationDate, -1, NULL, true ) );
	}

	public function getInitiationDate() {
		return $this->m_strInitiationDate;
	}

	public function sqlInitiationDate() {
		return ( true == isset( $this->m_strInitiationDate ) ) ? '\'' . $this->m_strInitiationDate . '\'' : 'NOW()';
	}

	public function setTerminationDate( $strTerminationDate ) {
		$this->set( 'm_strTerminationDate', CStrings::strTrimDef( $strTerminationDate, -1, NULL, true ) );
	}

	public function getTerminationDate() {
		return $this->m_strTerminationDate;
	}

	public function sqlTerminationDate() {
		return ( true == isset( $this->m_strTerminationDate ) ) ? '\'' . $this->m_strTerminationDate . '\'' : 'NULL';
	}

	public function setIsInImplementation( $boolIsInImplementation ) {
		$this->set( 'm_boolIsInImplementation', CStrings::strToBool( $boolIsInImplementation ) );
	}

	public function getIsInImplementation() {
		return $this->m_boolIsInImplementation;
	}

	public function sqlIsInImplementation() {
		return ( true == isset( $this->m_boolIsInImplementation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInImplementation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsExportUtilityBills( $boolIsExportUtilityBills ) {
		$this->set( 'm_boolIsExportUtilityBills', CStrings::strToBool( $boolIsExportUtilityBills ) );
	}

	public function getIsExportUtilityBills() {
		return $this->m_boolIsExportUtilityBills;
	}

	public function sqlIsExportUtilityBills() {
		return ( true == isset( $this->m_boolIsExportUtilityBills ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsExportUtilityBills ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTestDistribution( $boolIsTestDistribution ) {
		$this->set( 'm_boolIsTestDistribution', CStrings::strToBool( $boolIsTestDistribution ) );
	}

	public function getIsTestDistribution() {
		return $this->m_boolIsTestDistribution;
	}

	public function sqlIsTestDistribution() {
		return ( true == isset( $this->m_boolIsTestDistribution ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTestDistribution ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDataEntryInIndia( $boolIsDataEntryInIndia ) {
		$this->set( 'm_boolIsDataEntryInIndia', CStrings::strToBool( $boolIsDataEntryInIndia ) );
	}

	public function getIsDataEntryInIndia() {
		return $this->m_boolIsDataEntryInIndia;
	}

	public function sqlIsDataEntryInIndia() {
		return ( true == isset( $this->m_boolIsDataEntryInIndia ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDataEntryInIndia ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLastBilledOn( $strLastBilledOn ) {
		$this->set( 'm_strLastBilledOn', CStrings::strTrimDef( $strLastBilledOn, -1, NULL, true ) );
	}

	public function getLastBilledOn() {
		return $this->m_strLastBilledOn;
	}

	public function sqlLastBilledOn() {
		return ( true == isset( $this->m_strLastBilledOn ) ) ? '\'' . $this->m_strLastBilledOn . '\'' : 'NULL';
	}

	public function setValidatedOn( $strValidatedOn ) {
		$this->set( 'm_strValidatedOn', CStrings::strTrimDef( $strValidatedOn, -1, NULL, true ) );
	}

	public function getValidatedOn() {
		return $this->m_strValidatedOn;
	}

	public function sqlValidatedOn() {
		return ( true == isset( $this->m_strValidatedOn ) ) ? '\'' . $this->m_strValidatedOn . '\'' : 'NULL';
	}

	public function setLegalApprovedBy( $intLegalApprovedBy ) {
		$this->set( 'm_intLegalApprovedBy', CStrings::strToIntDef( $intLegalApprovedBy, NULL, false ) );
	}

	public function getLegalApprovedBy() {
		return $this->m_intLegalApprovedBy;
	}

	public function sqlLegalApprovedBy() {
		return ( true == isset( $this->m_intLegalApprovedBy ) ) ? ( string ) $this->m_intLegalApprovedBy : 'NULL';
	}

	public function setLegalApprovedOn( $strLegalApprovedOn ) {
		$this->set( 'm_strLegalApprovedOn', CStrings::strTrimDef( $strLegalApprovedOn, -1, NULL, true ) );
	}

	public function getLegalApprovedOn() {
		return $this->m_strLegalApprovedOn;
	}

	public function sqlLegalApprovedOn() {
		return ( true == isset( $this->m_strLegalApprovedOn ) ) ? '\'' . $this->m_strLegalApprovedOn . '\'' : 'NULL';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDecisionMakerCompanyEmployeeIds( $arrintDecisionMakerCompanyEmployeeIds ) {
		$this->set( 'm_arrintDecisionMakerCompanyEmployeeIds', CStrings::strToArrIntDef( $arrintDecisionMakerCompanyEmployeeIds, NULL ) );
	}

	public function getDecisionMakerCompanyEmployeeIds() {
		return $this->m_arrintDecisionMakerCompanyEmployeeIds;
	}

	public function sqlDecisionMakerCompanyEmployeeIds() {
		return ( true == isset( $this->m_arrintDecisionMakerCompanyEmployeeIds ) && true == valArr( $this->m_arrintDecisionMakerCompanyEmployeeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintDecisionMakerCompanyEmployeeIds, NULL ) . '\'' : 'NULL';
	}

	public function setVerifyInvoiceDay( $intVerifyInvoiceDay ) {
		$this->set( 'm_intVerifyInvoiceDay', CStrings::strToIntDef( $intVerifyInvoiceDay, NULL, false ) );
	}

	public function getVerifyInvoiceDay() {
		return $this->m_intVerifyInvoiceDay;
	}

	public function sqlVerifyInvoiceDay() {
		return ( true == isset( $this->m_intVerifyInvoiceDay ) ) ? ( string ) $this->m_intVerifyInvoiceDay : '0';
	}

	public function setUemImplementationEmployeeId( $intUemImplementationEmployeeId ) {
		$this->set( 'm_intUemImplementationEmployeeId', CStrings::strToIntDef( $intUemImplementationEmployeeId, NULL, false ) );
	}

	public function getUemImplementationEmployeeId() {
		return $this->m_intUemImplementationEmployeeId;
	}

	public function sqlUemImplementationEmployeeId() {
		return ( true == isset( $this->m_intUemImplementationEmployeeId ) ) ? ( string ) $this->m_intUemImplementationEmployeeId : 'NULL';
	}

	public function setBillingImplementationAssignedOn( $strBillingImplementationAssignedOn ) {
		$this->set( 'm_strBillingImplementationAssignedOn', CStrings::strTrimDef( $strBillingImplementationAssignedOn, -1, NULL, true ) );
	}

	public function getBillingImplementationAssignedOn() {
		return $this->m_strBillingImplementationAssignedOn;
	}

	public function sqlBillingImplementationAssignedOn() {
		return ( true == isset( $this->m_strBillingImplementationAssignedOn ) ) ? '\'' . $this->m_strBillingImplementationAssignedOn . '\'' : 'NULL';
	}

	public function setUemImplementationAssignedOn( $strUemImplementationAssignedOn ) {
		$this->set( 'm_strUemImplementationAssignedOn', CStrings::strTrimDef( $strUemImplementationAssignedOn, -1, NULL, true ) );
	}

	public function getUemImplementationAssignedOn() {
		return $this->m_strUemImplementationAssignedOn;
	}

	public function sqlUemImplementationAssignedOn() {
		return ( true == isset( $this->m_strUemImplementationAssignedOn ) ) ? '\'' . $this->m_strUemImplementationAssignedOn . '\'' : 'NULL';
	}

	public function setBillingImplementationScheduledOn( $strBillingImplementationScheduledOn ) {
		$this->set( 'm_strBillingImplementationScheduledOn', CStrings::strTrimDef( $strBillingImplementationScheduledOn, -1, NULL, true ) );
	}

	public function getBillingImplementationScheduledOn() {
		return $this->m_strBillingImplementationScheduledOn;
	}

	public function sqlBillingImplementationScheduledOn() {
		return ( true == isset( $this->m_strBillingImplementationScheduledOn ) ) ? '\'' . $this->m_strBillingImplementationScheduledOn . '\'' : 'NULL';
	}

	public function setUemImplementationScheduledOn( $strUemImplementationScheduledOn ) {
		$this->set( 'm_strUemImplementationScheduledOn', CStrings::strTrimDef( $strUemImplementationScheduledOn, -1, NULL, true ) );
	}

	public function getUemImplementationScheduledOn() {
		return $this->m_strUemImplementationScheduledOn;
	}

	public function sqlUemImplementationScheduledOn() {
		return ( true == isset( $this->m_strUemImplementationScheduledOn ) ) ? '\'' . $this->m_strUemImplementationScheduledOn . '\'' : 'NULL';
	}

	public function setBillingImplementationCompletedOn( $strBillingImplementationCompletedOn ) {
		$this->set( 'm_strBillingImplementationCompletedOn', CStrings::strTrimDef( $strBillingImplementationCompletedOn, -1, NULL, true ) );
	}

	public function getBillingImplementationCompletedOn() {
		return $this->m_strBillingImplementationCompletedOn;
	}

	public function sqlBillingImplementationCompletedOn() {
		return ( true == isset( $this->m_strBillingImplementationCompletedOn ) ) ? '\'' . $this->m_strBillingImplementationCompletedOn . '\'' : 'NULL';
	}

	public function setUemImplementationCompletedOn( $strUemImplementationCompletedOn ) {
		$this->set( 'm_strUemImplementationCompletedOn', CStrings::strTrimDef( $strUemImplementationCompletedOn, -1, NULL, true ) );
	}

	public function getUemImplementationCompletedOn() {
		return $this->m_strUemImplementationCompletedOn;
	}

	public function sqlUemImplementationCompletedOn() {
		return ( true == isset( $this->m_strUemImplementationCompletedOn ) ) ? '\'' . $this->m_strUemImplementationCompletedOn . '\'' : 'NULL';
	}

	public function setExportRuBillsAsIp( $boolExportRuBillsAsIp ) {
		$this->set( 'm_boolExportRuBillsAsIp', CStrings::strToBool( $boolExportRuBillsAsIp ) );
	}

	public function getExportRuBillsAsIp() {
		return $this->m_boolExportRuBillsAsIp;
	}

	public function sqlExportRuBillsAsIp() {
		return ( true == isset( $this->m_boolExportRuBillsAsIp ) ) ? '\'' . ( true == ( bool ) $this->m_boolExportRuBillsAsIp ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, old_property_id, billing_fee_account_id, company_employee_id, regional_company_employee_id, analyst_employee_id, billing_implementation_employee_id, fax_call_phone_number_id, support_call_phone_number_id, utility_competitor_id, missing_bill_company_employee_ids, ps_late_fee_percent, pre_batch_day, distribution_day, first_missing_bill_warning_days, second_missing_bill_warning_days, utility_rule_notes, code_version, initiation_date, termination_date, is_in_implementation, is_export_utility_bills, is_test_distribution, is_data_entry_in_india, last_billed_on, validated_on, legal_approved_by, legal_approved_on, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on, decision_maker_company_employee_ids, verify_invoice_day, uem_implementation_employee_id, billing_implementation_assigned_on, uem_implementation_assigned_on, billing_implementation_scheduled_on, uem_implementation_scheduled_on, billing_implementation_completed_on, uem_implementation_completed_on, export_ru_bills_as_ip )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlOldPropertyId() . ', ' .
						$this->sqlBillingFeeAccountId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlRegionalCompanyEmployeeId() . ', ' .
						$this->sqlAnalystEmployeeId() . ', ' .
						$this->sqlBillingImplementationEmployeeId() . ', ' .
						$this->sqlFaxCallPhoneNumberId() . ', ' .
						$this->sqlSupportCallPhoneNumberId() . ', ' .
						$this->sqlUtilityCompetitorId() . ', ' .
						$this->sqlMissingBillCompanyEmployeeIds() . ', ' .
						$this->sqlPsLateFeePercent() . ', ' .
						$this->sqlPreBatchDay() . ', ' .
						$this->sqlDistributionDay() . ', ' .
						$this->sqlFirstMissingBillWarningDays() . ', ' .
						$this->sqlSecondMissingBillWarningDays() . ', ' .
						$this->sqlUtilityRuleNotes() . ', ' .
						$this->sqlCodeVersion() . ', ' .
						$this->sqlInitiationDate() . ', ' .
						$this->sqlTerminationDate() . ', ' .
						$this->sqlIsInImplementation() . ', ' .
						$this->sqlIsExportUtilityBills() . ', ' .
						$this->sqlIsTestDistribution() . ', ' .
						$this->sqlIsDataEntryInIndia() . ', ' .
						$this->sqlLastBilledOn() . ', ' .
						$this->sqlValidatedOn() . ', ' .
						$this->sqlLegalApprovedBy() . ', ' .
						$this->sqlLegalApprovedOn() . ', ' .
						$this->sqlDisabledBy() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDecisionMakerCompanyEmployeeIds() . ', ' .
						$this->sqlVerifyInvoiceDay() . ', ' .
						$this->sqlUemImplementationEmployeeId() . ', ' .
						$this->sqlBillingImplementationAssignedOn() . ', ' .
						$this->sqlUemImplementationAssignedOn() . ', ' .
						$this->sqlBillingImplementationScheduledOn() . ', ' .
						$this->sqlUemImplementationScheduledOn() . ', ' .
						$this->sqlBillingImplementationCompletedOn() . ', ' .
						$this->sqlUemImplementationCompletedOn() . ', ' .
						$this->sqlExportRuBillsAsIp() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_property_id = ' . $this->sqlOldPropertyId(). ',' ; } elseif( true == array_key_exists( 'OldPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' old_property_id = ' . $this->sqlOldPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_fee_account_id = ' . $this->sqlBillingFeeAccountId(). ',' ; } elseif( true == array_key_exists( 'BillingFeeAccountId', $this->getChangedColumns() ) ) { $strSql .= ' billing_fee_account_id = ' . $this->sqlBillingFeeAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' regional_company_employee_id = ' . $this->sqlRegionalCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'RegionalCompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' regional_company_employee_id = ' . $this->sqlRegionalCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' analyst_employee_id = ' . $this->sqlAnalystEmployeeId(). ',' ; } elseif( true == array_key_exists( 'AnalystEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' analyst_employee_id = ' . $this->sqlAnalystEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_implementation_employee_id = ' . $this->sqlBillingImplementationEmployeeId(). ',' ; } elseif( true == array_key_exists( 'BillingImplementationEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' billing_implementation_employee_id = ' . $this->sqlBillingImplementationEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_call_phone_number_id = ' . $this->sqlFaxCallPhoneNumberId(). ',' ; } elseif( true == array_key_exists( 'FaxCallPhoneNumberId', $this->getChangedColumns() ) ) { $strSql .= ' fax_call_phone_number_id = ' . $this->sqlFaxCallPhoneNumberId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' support_call_phone_number_id = ' . $this->sqlSupportCallPhoneNumberId(). ',' ; } elseif( true == array_key_exists( 'SupportCallPhoneNumberId', $this->getChangedColumns() ) ) { $strSql .= ' support_call_phone_number_id = ' . $this->sqlSupportCallPhoneNumberId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_competitor_id = ' . $this->sqlUtilityCompetitorId(). ',' ; } elseif( true == array_key_exists( 'UtilityCompetitorId', $this->getChangedColumns() ) ) { $strSql .= ' utility_competitor_id = ' . $this->sqlUtilityCompetitorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' missing_bill_company_employee_ids = ' . $this->sqlMissingBillCompanyEmployeeIds(). ',' ; } elseif( true == array_key_exists( 'MissingBillCompanyEmployeeIds', $this->getChangedColumns() ) ) { $strSql .= ' missing_bill_company_employee_ids = ' . $this->sqlMissingBillCompanyEmployeeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_late_fee_percent = ' . $this->sqlPsLateFeePercent(). ',' ; } elseif( true == array_key_exists( 'PsLateFeePercent', $this->getChangedColumns() ) ) { $strSql .= ' ps_late_fee_percent = ' . $this->sqlPsLateFeePercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_batch_day = ' . $this->sqlPreBatchDay(). ',' ; } elseif( true == array_key_exists( 'PreBatchDay', $this->getChangedColumns() ) ) { $strSql .= ' pre_batch_day = ' . $this->sqlPreBatchDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_day = ' . $this->sqlDistributionDay(). ',' ; } elseif( true == array_key_exists( 'DistributionDay', $this->getChangedColumns() ) ) { $strSql .= ' distribution_day = ' . $this->sqlDistributionDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_missing_bill_warning_days = ' . $this->sqlFirstMissingBillWarningDays(). ',' ; } elseif( true == array_key_exists( 'FirstMissingBillWarningDays', $this->getChangedColumns() ) ) { $strSql .= ' first_missing_bill_warning_days = ' . $this->sqlFirstMissingBillWarningDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' second_missing_bill_warning_days = ' . $this->sqlSecondMissingBillWarningDays(). ',' ; } elseif( true == array_key_exists( 'SecondMissingBillWarningDays', $this->getChangedColumns() ) ) { $strSql .= ' second_missing_bill_warning_days = ' . $this->sqlSecondMissingBillWarningDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_rule_notes = ' . $this->sqlUtilityRuleNotes(). ',' ; } elseif( true == array_key_exists( 'UtilityRuleNotes', $this->getChangedColumns() ) ) { $strSql .= ' utility_rule_notes = ' . $this->sqlUtilityRuleNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' code_version = ' . $this->sqlCodeVersion(). ',' ; } elseif( true == array_key_exists( 'CodeVersion', $this->getChangedColumns() ) ) { $strSql .= ' code_version = ' . $this->sqlCodeVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initiation_date = ' . $this->sqlInitiationDate(). ',' ; } elseif( true == array_key_exists( 'InitiationDate', $this->getChangedColumns() ) ) { $strSql .= ' initiation_date = ' . $this->sqlInitiationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate(). ',' ; } elseif( true == array_key_exists( 'TerminationDate', $this->getChangedColumns() ) ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_in_implementation = ' . $this->sqlIsInImplementation(). ',' ; } elseif( true == array_key_exists( 'IsInImplementation', $this->getChangedColumns() ) ) { $strSql .= ' is_in_implementation = ' . $this->sqlIsInImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_export_utility_bills = ' . $this->sqlIsExportUtilityBills(). ',' ; } elseif( true == array_key_exists( 'IsExportUtilityBills', $this->getChangedColumns() ) ) { $strSql .= ' is_export_utility_bills = ' . $this->sqlIsExportUtilityBills() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_test_distribution = ' . $this->sqlIsTestDistribution(). ',' ; } elseif( true == array_key_exists( 'IsTestDistribution', $this->getChangedColumns() ) ) { $strSql .= ' is_test_distribution = ' . $this->sqlIsTestDistribution() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_data_entry_in_india = ' . $this->sqlIsDataEntryInIndia(). ',' ; } elseif( true == array_key_exists( 'IsDataEntryInIndia', $this->getChangedColumns() ) ) { $strSql .= ' is_data_entry_in_india = ' . $this->sqlIsDataEntryInIndia() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_billed_on = ' . $this->sqlLastBilledOn(). ',' ; } elseif( true == array_key_exists( 'LastBilledOn', $this->getChangedColumns() ) ) { $strSql .= ' last_billed_on = ' . $this->sqlLastBilledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' validated_on = ' . $this->sqlValidatedOn(). ',' ; } elseif( true == array_key_exists( 'ValidatedOn', $this->getChangedColumns() ) ) { $strSql .= ' validated_on = ' . $this->sqlValidatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' legal_approved_by = ' . $this->sqlLegalApprovedBy(). ',' ; } elseif( true == array_key_exists( 'LegalApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' legal_approved_by = ' . $this->sqlLegalApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' legal_approved_on = ' . $this->sqlLegalApprovedOn(). ',' ; } elseif( true == array_key_exists( 'LegalApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' legal_approved_on = ' . $this->sqlLegalApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy(). ',' ; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' decision_maker_company_employee_ids = ' . $this->sqlDecisionMakerCompanyEmployeeIds(). ',' ; } elseif( true == array_key_exists( 'DecisionMakerCompanyEmployeeIds', $this->getChangedColumns() ) ) { $strSql .= ' decision_maker_company_employee_ids = ' . $this->sqlDecisionMakerCompanyEmployeeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verify_invoice_day = ' . $this->sqlVerifyInvoiceDay(). ',' ; } elseif( true == array_key_exists( 'VerifyInvoiceDay', $this->getChangedColumns() ) ) { $strSql .= ' verify_invoice_day = ' . $this->sqlVerifyInvoiceDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uem_implementation_employee_id = ' . $this->sqlUemImplementationEmployeeId(). ',' ; } elseif( true == array_key_exists( 'UemImplementationEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' uem_implementation_employee_id = ' . $this->sqlUemImplementationEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_implementation_assigned_on = ' . $this->sqlBillingImplementationAssignedOn(). ',' ; } elseif( true == array_key_exists( 'BillingImplementationAssignedOn', $this->getChangedColumns() ) ) { $strSql .= ' billing_implementation_assigned_on = ' . $this->sqlBillingImplementationAssignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uem_implementation_assigned_on = ' . $this->sqlUemImplementationAssignedOn(). ',' ; } elseif( true == array_key_exists( 'UemImplementationAssignedOn', $this->getChangedColumns() ) ) { $strSql .= ' uem_implementation_assigned_on = ' . $this->sqlUemImplementationAssignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_implementation_scheduled_on = ' . $this->sqlBillingImplementationScheduledOn(). ',' ; } elseif( true == array_key_exists( 'BillingImplementationScheduledOn', $this->getChangedColumns() ) ) { $strSql .= ' billing_implementation_scheduled_on = ' . $this->sqlBillingImplementationScheduledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uem_implementation_scheduled_on = ' . $this->sqlUemImplementationScheduledOn(). ',' ; } elseif( true == array_key_exists( 'UemImplementationScheduledOn', $this->getChangedColumns() ) ) { $strSql .= ' uem_implementation_scheduled_on = ' . $this->sqlUemImplementationScheduledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_implementation_completed_on = ' . $this->sqlBillingImplementationCompletedOn(). ',' ; } elseif( true == array_key_exists( 'BillingImplementationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' billing_implementation_completed_on = ' . $this->sqlBillingImplementationCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uem_implementation_completed_on = ' . $this->sqlUemImplementationCompletedOn(). ',' ; } elseif( true == array_key_exists( 'UemImplementationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' uem_implementation_completed_on = ' . $this->sqlUemImplementationCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_ru_bills_as_ip = ' . $this->sqlExportRuBillsAsIp(). ',' ; } elseif( true == array_key_exists( 'ExportRuBillsAsIp', $this->getChangedColumns() ) ) { $strSql .= ' export_ru_bills_as_ip = ' . $this->sqlExportRuBillsAsIp() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'old_property_id' => $this->getOldPropertyId(),
			'billing_fee_account_id' => $this->getBillingFeeAccountId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'regional_company_employee_id' => $this->getRegionalCompanyEmployeeId(),
			'analyst_employee_id' => $this->getAnalystEmployeeId(),
			'billing_implementation_employee_id' => $this->getBillingImplementationEmployeeId(),
			'fax_call_phone_number_id' => $this->getFaxCallPhoneNumberId(),
			'support_call_phone_number_id' => $this->getSupportCallPhoneNumberId(),
			'utility_competitor_id' => $this->getUtilityCompetitorId(),
			'missing_bill_company_employee_ids' => $this->getMissingBillCompanyEmployeeIds(),
			'ps_late_fee_percent' => $this->getPsLateFeePercent(),
			'pre_batch_day' => $this->getPreBatchDay(),
			'distribution_day' => $this->getDistributionDay(),
			'first_missing_bill_warning_days' => $this->getFirstMissingBillWarningDays(),
			'second_missing_bill_warning_days' => $this->getSecondMissingBillWarningDays(),
			'utility_rule_notes' => $this->getUtilityRuleNotes(),
			'code_version' => $this->getCodeVersion(),
			'initiation_date' => $this->getInitiationDate(),
			'termination_date' => $this->getTerminationDate(),
			'is_in_implementation' => $this->getIsInImplementation(),
			'is_export_utility_bills' => $this->getIsExportUtilityBills(),
			'is_test_distribution' => $this->getIsTestDistribution(),
			'is_data_entry_in_india' => $this->getIsDataEntryInIndia(),
			'last_billed_on' => $this->getLastBilledOn(),
			'validated_on' => $this->getValidatedOn(),
			'legal_approved_by' => $this->getLegalApprovedBy(),
			'legal_approved_on' => $this->getLegalApprovedOn(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'decision_maker_company_employee_ids' => $this->getDecisionMakerCompanyEmployeeIds(),
			'verify_invoice_day' => $this->getVerifyInvoiceDay(),
			'uem_implementation_employee_id' => $this->getUemImplementationEmployeeId(),
			'billing_implementation_assigned_on' => $this->getBillingImplementationAssignedOn(),
			'uem_implementation_assigned_on' => $this->getUemImplementationAssignedOn(),
			'billing_implementation_scheduled_on' => $this->getBillingImplementationScheduledOn(),
			'uem_implementation_scheduled_on' => $this->getUemImplementationScheduledOn(),
			'billing_implementation_completed_on' => $this->getBillingImplementationCompletedOn(),
			'uem_implementation_completed_on' => $this->getUemImplementationCompletedOn(),
			'export_ru_bills_as_ip' => $this->getExportRuBillsAsIp()
		);
	}

}
?>