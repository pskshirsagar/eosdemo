<?php

class CBaseVendor extends CEosSingularBase {

	const TABLE_NAME = 'public.vendors';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultVendorRemittanceId;
	protected $m_intDefaultVendorEntityId;
	protected $m_strRemotePrimaryKey;
	protected $m_strCompanyName;
	protected $m_strSubdomain;
	protected $m_strCompanyLogo;
	protected $m_strWebsiteUrl;
	protected $m_strStartDate;
	protected $m_strServiceDescriptions;
	protected $m_boolIsOnSite;
	protected $m_boolIsInSetup;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsOnSite = false;
		$this->m_boolIsInSetup = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_vendor_remittance_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultVendorRemittanceId', trim( $arrValues['default_vendor_remittance_id'] ) ); elseif( isset( $arrValues['default_vendor_remittance_id'] ) ) $this->setDefaultVendorRemittanceId( $arrValues['default_vendor_remittance_id'] );
		if( isset( $arrValues['default_vendor_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultVendorEntityId', trim( $arrValues['default_vendor_entity_id'] ) ); elseif( isset( $arrValues['default_vendor_entity_id'] ) ) $this->setDefaultVendorEntityId( $arrValues['default_vendor_entity_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['subdomain'] ) && $boolDirectSet ) $this->set( 'm_strSubdomain', trim( $arrValues['subdomain'] ) ); elseif( isset( $arrValues['subdomain'] ) ) $this->setSubdomain( $arrValues['subdomain'] );
		if( isset( $arrValues['company_logo'] ) && $boolDirectSet ) $this->set( 'm_strCompanyLogo', trim( $arrValues['company_logo'] ) ); elseif( isset( $arrValues['company_logo'] ) ) $this->setCompanyLogo( $arrValues['company_logo'] );
		if( isset( $arrValues['website_url'] ) && $boolDirectSet ) $this->set( 'm_strWebsiteUrl', trim( $arrValues['website_url'] ) ); elseif( isset( $arrValues['website_url'] ) ) $this->setWebsiteUrl( $arrValues['website_url'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['service_descriptions'] ) && $boolDirectSet ) $this->set( 'm_strServiceDescriptions', trim( $arrValues['service_descriptions'] ) ); elseif( isset( $arrValues['service_descriptions'] ) ) $this->setServiceDescriptions( $arrValues['service_descriptions'] );
		if( isset( $arrValues['is_on_site'] ) && $boolDirectSet ) $this->set( 'm_boolIsOnSite', trim( stripcslashes( $arrValues['is_on_site'] ) ) ); elseif( isset( $arrValues['is_on_site'] ) ) $this->setIsOnSite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_on_site'] ) : $arrValues['is_on_site'] );
		if( isset( $arrValues['is_in_setup'] ) && $boolDirectSet ) $this->set( 'm_boolIsInSetup', trim( stripcslashes( $arrValues['is_in_setup'] ) ) ); elseif( isset( $arrValues['is_in_setup'] ) ) $this->setIsInSetup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_in_setup'] ) : $arrValues['is_in_setup'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultVendorRemittanceId( $intDefaultVendorRemittanceId ) {
		$this->set( 'm_intDefaultVendorRemittanceId', CStrings::strToIntDef( $intDefaultVendorRemittanceId, NULL, false ) );
	}

	public function getDefaultVendorRemittanceId() {
		return $this->m_intDefaultVendorRemittanceId;
	}

	public function sqlDefaultVendorRemittanceId() {
		return ( true == isset( $this->m_intDefaultVendorRemittanceId ) ) ? ( string ) $this->m_intDefaultVendorRemittanceId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jun 17 2020.
	 */
	public function setDefaultVendorEntityId( $intDefaultVendorEntityId ) {
		$this->set( 'm_intDefaultVendorEntityId', CStrings::strToIntDef( $intDefaultVendorEntityId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jun 17 2020.
	 */
	public function getDefaultVendorEntityId() {
		return $this->m_intDefaultVendorEntityId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jun 17 2020.
	 */
	public function sqlDefaultVendorEntityId() {
		return ( true == isset( $this->m_intDefaultVendorEntityId ) ) ? ( string ) $this->m_intDefaultVendorEntityId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 40, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setSubdomain( $strSubdomain ) {
		$this->set( 'm_strSubdomain', CStrings::strTrimDef( $strSubdomain, 240, NULL, true ) );
	}

	public function getSubdomain() {
		return $this->m_strSubdomain;
	}

	public function sqlSubdomain() {
		return ( true == isset( $this->m_strSubdomain ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSubdomain ) : '\'' . addslashes( $this->m_strSubdomain ) . '\'' ) : 'NULL';
	}

	public function setCompanyLogo( $strCompanyLogo ) {
		$this->set( 'm_strCompanyLogo', CStrings::strTrimDef( $strCompanyLogo, -1, NULL, true ) );
	}

	public function getCompanyLogo() {
		return $this->m_strCompanyLogo;
	}

	public function sqlCompanyLogo() {
		return ( true == isset( $this->m_strCompanyLogo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyLogo ) : '\'' . addslashes( $this->m_strCompanyLogo ) . '\'' ) : 'NULL';
	}

	public function setWebsiteUrl( $strWebsiteUrl ) {
		$this->set( 'm_strWebsiteUrl', CStrings::strTrimDef( $strWebsiteUrl, 150, NULL, true ) );
	}

	public function getWebsiteUrl() {
		return $this->m_strWebsiteUrl;
	}

	public function sqlWebsiteUrl() {
		return ( true == isset( $this->m_strWebsiteUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebsiteUrl ) : '\'' . addslashes( $this->m_strWebsiteUrl ) . '\'' ) : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, 4, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStartDate ) : '\'' . addslashes( $this->m_strStartDate ) . '\'' ) : 'NULL';
	}

	public function setServiceDescriptions( $strServiceDescriptions ) {
		$this->set( 'm_strServiceDescriptions', CStrings::strTrimDef( $strServiceDescriptions, -1, NULL, true ) );
	}

	public function getServiceDescriptions() {
		return $this->m_strServiceDescriptions;
	}

	public function sqlServiceDescriptions() {
		return ( true == isset( $this->m_strServiceDescriptions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceDescriptions ) : '\'' . addslashes( $this->m_strServiceDescriptions ) . '\'' ) : 'NULL';
	}

	public function setIsOnSite( $boolIsOnSite ) {
		$this->set( 'm_boolIsOnSite', CStrings::strToBool( $boolIsOnSite ) );
	}

	public function getIsOnSite() {
		return $this->m_boolIsOnSite;
	}

	public function sqlIsOnSite() {
		return ( true == isset( $this->m_boolIsOnSite ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOnSite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInSetup( $boolIsInSetup ) {
		$this->set( 'm_boolIsInSetup', CStrings::strToBool( $boolIsInSetup ) );
	}

	public function getIsInSetup() {
		return $this->m_boolIsInSetup;
	}

	public function sqlIsInSetup() {
		return ( true == isset( $this->m_boolIsInSetup ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInSetup ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_vendor_remittance_id, default_vendor_entity_id, remote_primary_key, company_name, subdomain, company_logo, website_url, start_date, service_descriptions, is_on_site, is_in_setup, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultVendorRemittanceId() . ', ' .
						$this->sqlDefaultVendorEntityId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlSubdomain() . ', ' .
						$this->sqlCompanyLogo() . ', ' .
						$this->sqlWebsiteUrl() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlServiceDescriptions() . ', ' .
						$this->sqlIsOnSite() . ', ' .
						$this->sqlIsInSetup() . ', ' .
						$this->sqlDisabledBy() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_vendor_remittance_id = ' . $this->sqlDefaultVendorRemittanceId(). ',' ; } elseif( true == array_key_exists( 'DefaultVendorRemittanceId', $this->getChangedColumns() ) ) { $strSql .= ' default_vendor_remittance_id = ' . $this->sqlDefaultVendorRemittanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_vendor_entity_id = ' . $this->sqlDefaultVendorEntityId(). ',' ; } elseif( true == array_key_exists( 'DefaultVendorEntityId', $this->getChangedColumns() ) ) { $strSql .= ' default_vendor_entity_id = ' . $this->sqlDefaultVendorEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subdomain = ' . $this->sqlSubdomain(). ',' ; } elseif( true == array_key_exists( 'Subdomain', $this->getChangedColumns() ) ) { $strSql .= ' subdomain = ' . $this->sqlSubdomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_logo = ' . $this->sqlCompanyLogo(). ',' ; } elseif( true == array_key_exists( 'CompanyLogo', $this->getChangedColumns() ) ) { $strSql .= ' company_logo = ' . $this->sqlCompanyLogo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl(). ',' ; } elseif( true == array_key_exists( 'WebsiteUrl', $this->getChangedColumns() ) ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_descriptions = ' . $this->sqlServiceDescriptions(). ',' ; } elseif( true == array_key_exists( 'ServiceDescriptions', $this->getChangedColumns() ) ) { $strSql .= ' service_descriptions = ' . $this->sqlServiceDescriptions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_on_site = ' . $this->sqlIsOnSite(). ',' ; } elseif( true == array_key_exists( 'IsOnSite', $this->getChangedColumns() ) ) { $strSql .= ' is_on_site = ' . $this->sqlIsOnSite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_in_setup = ' . $this->sqlIsInSetup(). ',' ; } elseif( true == array_key_exists( 'IsInSetup', $this->getChangedColumns() ) ) { $strSql .= ' is_in_setup = ' . $this->sqlIsInSetup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy(). ',' ; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_vendor_remittance_id' => $this->getDefaultVendorRemittanceId(),
			'default_vendor_entity_id' => $this->getDefaultVendorEntityId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'company_name' => $this->getCompanyName(),
			'subdomain' => $this->getSubdomain(),
			'company_logo' => $this->getCompanyLogo(),
			'website_url' => $this->getWebsiteUrl(),
			'start_date' => $this->getStartDate(),
			'service_descriptions' => $this->getServiceDescriptions(),
			'is_on_site' => $this->getIsOnSite(),
			'is_in_setup' => $this->getIsInSetup(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>