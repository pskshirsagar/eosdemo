<?php

class CBaseUtilityBatchQcEscalation extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_batch_qc_escalations';

	protected $m_intId;
	protected $m_intUtilityBatchId;
	protected $m_intUtilityBatchActivityId;
	protected $m_intUtilityBatchQcFlagTypeId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intUtilityAccountId;
	protected $m_intUtilityBillAccountId;
	protected $m_intUtilityBillChargeId;
	protected $m_strQcMemo;
	protected $m_strBillingAnalystNote;
	protected $m_intQcApprovedBy;
	protected $m_strQcApprovedOn;
	protected $m_intQcDeclinedBy;
	protected $m_strQcDeclinedOn;
	protected $m_strQcSpecialistNote;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strClientFacingNote;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['utility_batch_activity_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchActivityId', trim( $arrValues['utility_batch_activity_id'] ) ); elseif( isset( $arrValues['utility_batch_activity_id'] ) ) $this->setUtilityBatchActivityId( $arrValues['utility_batch_activity_id'] );
		if( isset( $arrValues['utility_batch_qc_flag_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchQcFlagTypeId', trim( $arrValues['utility_batch_qc_flag_type_id'] ) ); elseif( isset( $arrValues['utility_batch_qc_flag_type_id'] ) ) $this->setUtilityBatchQcFlagTypeId( $arrValues['utility_batch_qc_flag_type_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['utility_bill_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillChargeId', trim( $arrValues['utility_bill_charge_id'] ) ); elseif( isset( $arrValues['utility_bill_charge_id'] ) ) $this->setUtilityBillChargeId( $arrValues['utility_bill_charge_id'] );
		if( isset( $arrValues['qc_memo'] ) && $boolDirectSet ) $this->set( 'm_strQcMemo', trim( stripcslashes( $arrValues['qc_memo'] ) ) ); elseif( isset( $arrValues['qc_memo'] ) ) $this->setQcMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['qc_memo'] ) : $arrValues['qc_memo'] );
		if( isset( $arrValues['billing_analyst_note'] ) && $boolDirectSet ) $this->set( 'm_strBillingAnalystNote', trim( stripcslashes( $arrValues['billing_analyst_note'] ) ) ); elseif( isset( $arrValues['billing_analyst_note'] ) ) $this->setBillingAnalystNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billing_analyst_note'] ) : $arrValues['billing_analyst_note'] );
		if( isset( $arrValues['qc_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intQcApprovedBy', trim( $arrValues['qc_approved_by'] ) ); elseif( isset( $arrValues['qc_approved_by'] ) ) $this->setQcApprovedBy( $arrValues['qc_approved_by'] );
		if( isset( $arrValues['qc_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strQcApprovedOn', trim( $arrValues['qc_approved_on'] ) ); elseif( isset( $arrValues['qc_approved_on'] ) ) $this->setQcApprovedOn( $arrValues['qc_approved_on'] );
		if( isset( $arrValues['qc_declined_by'] ) && $boolDirectSet ) $this->set( 'm_intQcDeclinedBy', trim( $arrValues['qc_declined_by'] ) ); elseif( isset( $arrValues['qc_declined_by'] ) ) $this->setQcDeclinedBy( $arrValues['qc_declined_by'] );
		if( isset( $arrValues['qc_declined_on'] ) && $boolDirectSet ) $this->set( 'm_strQcDeclinedOn', trim( $arrValues['qc_declined_on'] ) ); elseif( isset( $arrValues['qc_declined_on'] ) ) $this->setQcDeclinedOn( $arrValues['qc_declined_on'] );
		if( isset( $arrValues['qc_specialist_note'] ) && $boolDirectSet ) $this->set( 'm_strQcSpecialistNote', trim( stripcslashes( $arrValues['qc_specialist_note'] ) ) ); elseif( isset( $arrValues['qc_specialist_note'] ) ) $this->setQcSpecialistNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['qc_specialist_note'] ) : $arrValues['qc_specialist_note'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['client_facing_note'] ) && $boolDirectSet ) $this->set( 'm_strClientFacingNote', trim( stripcslashes( $arrValues['client_facing_note'] ) ) ); elseif( isset( $arrValues['client_facing_note'] ) ) $this->setClientFacingNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_facing_note'] ) : $arrValues['client_facing_note'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setUtilityBatchActivityId( $intUtilityBatchActivityId ) {
		$this->set( 'm_intUtilityBatchActivityId', CStrings::strToIntDef( $intUtilityBatchActivityId, NULL, false ) );
	}

	public function getUtilityBatchActivityId() {
		return $this->m_intUtilityBatchActivityId;
	}

	public function sqlUtilityBatchActivityId() {
		return ( true == isset( $this->m_intUtilityBatchActivityId ) ) ? ( string ) $this->m_intUtilityBatchActivityId : 'NULL';
	}

	public function setUtilityBatchQcFlagTypeId( $intUtilityBatchQcFlagTypeId ) {
		$this->set( 'm_intUtilityBatchQcFlagTypeId', CStrings::strToIntDef( $intUtilityBatchQcFlagTypeId, NULL, false ) );
	}

	public function getUtilityBatchQcFlagTypeId() {
		return $this->m_intUtilityBatchQcFlagTypeId;
	}

	public function sqlUtilityBatchQcFlagTypeId() {
		return ( true == isset( $this->m_intUtilityBatchQcFlagTypeId ) ) ? ( string ) $this->m_intUtilityBatchQcFlagTypeId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setUtilityBillChargeId( $intUtilityBillChargeId ) {
		$this->set( 'm_intUtilityBillChargeId', CStrings::strToIntDef( $intUtilityBillChargeId, NULL, false ) );
	}

	public function getUtilityBillChargeId() {
		return $this->m_intUtilityBillChargeId;
	}

	public function sqlUtilityBillChargeId() {
		return ( true == isset( $this->m_intUtilityBillChargeId ) ) ? ( string ) $this->m_intUtilityBillChargeId : 'NULL';
	}

	public function setQcMemo( $strQcMemo ) {
		$this->set( 'm_strQcMemo', CStrings::strTrimDef( $strQcMemo, -1, NULL, true ) );
	}

	public function getQcMemo() {
		return $this->m_strQcMemo;
	}

	public function sqlQcMemo() {
		return ( true == isset( $this->m_strQcMemo ) ) ? '\'' . addslashes( $this->m_strQcMemo ) . '\'' : 'NULL';
	}

	public function setBillingAnalystNote( $strBillingAnalystNote ) {
		$this->set( 'm_strBillingAnalystNote', CStrings::strTrimDef( $strBillingAnalystNote, -1, NULL, true ) );
	}

	public function getBillingAnalystNote() {
		return $this->m_strBillingAnalystNote;
	}

	public function sqlBillingAnalystNote() {
		return ( true == isset( $this->m_strBillingAnalystNote ) ) ? '\'' . addslashes( $this->m_strBillingAnalystNote ) . '\'' : 'NULL';
	}

	public function setQcApprovedBy( $intQcApprovedBy ) {
		$this->set( 'm_intQcApprovedBy', CStrings::strToIntDef( $intQcApprovedBy, NULL, false ) );
	}

	public function getQcApprovedBy() {
		return $this->m_intQcApprovedBy;
	}

	public function sqlQcApprovedBy() {
		return ( true == isset( $this->m_intQcApprovedBy ) ) ? ( string ) $this->m_intQcApprovedBy : 'NULL';
	}

	public function setQcApprovedOn( $strQcApprovedOn ) {
		$this->set( 'm_strQcApprovedOn', CStrings::strTrimDef( $strQcApprovedOn, -1, NULL, true ) );
	}

	public function getQcApprovedOn() {
		return $this->m_strQcApprovedOn;
	}

	public function sqlQcApprovedOn() {
		return ( true == isset( $this->m_strQcApprovedOn ) ) ? '\'' . $this->m_strQcApprovedOn . '\'' : 'NULL';
	}

	public function setQcDeclinedBy( $intQcDeclinedBy ) {
		$this->set( 'm_intQcDeclinedBy', CStrings::strToIntDef( $intQcDeclinedBy, NULL, false ) );
	}

	public function getQcDeclinedBy() {
		return $this->m_intQcDeclinedBy;
	}

	public function sqlQcDeclinedBy() {
		return ( true == isset( $this->m_intQcDeclinedBy ) ) ? ( string ) $this->m_intQcDeclinedBy : 'NULL';
	}

	public function setQcDeclinedOn( $strQcDeclinedOn ) {
		$this->set( 'm_strQcDeclinedOn', CStrings::strTrimDef( $strQcDeclinedOn, -1, NULL, true ) );
	}

	public function getQcDeclinedOn() {
		return $this->m_strQcDeclinedOn;
	}

	public function sqlQcDeclinedOn() {
		return ( true == isset( $this->m_strQcDeclinedOn ) ) ? '\'' . $this->m_strQcDeclinedOn . '\'' : 'NULL';
	}

	public function setQcSpecialistNote( $strQcSpecialistNote ) {
		$this->set( 'm_strQcSpecialistNote', CStrings::strTrimDef( $strQcSpecialistNote, -1, NULL, true ) );
	}

	public function getQcSpecialistNote() {
		return $this->m_strQcSpecialistNote;
	}

	public function sqlQcSpecialistNote() {
		return ( true == isset( $this->m_strQcSpecialistNote ) ) ? '\'' . addslashes( $this->m_strQcSpecialistNote ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setClientFacingNote( $strClientFacingNote ) {
		$this->set( 'm_strClientFacingNote', CStrings::strTrimDef( $strClientFacingNote, -1, NULL, true ) );
	}

	public function getClientFacingNote() {
		return $this->m_strClientFacingNote;
	}

	public function sqlClientFacingNote() {
		return ( true == isset( $this->m_strClientFacingNote ) ) ? '\'' . addslashes( $this->m_strClientFacingNote ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_batch_id, utility_batch_activity_id, utility_batch_qc_flag_type_id, property_utility_type_id, utility_account_id, utility_bill_account_id, utility_bill_charge_id, qc_memo, billing_analyst_note, qc_approved_by, qc_approved_on, qc_declined_by, qc_declined_on, qc_specialist_note, updated_by, updated_on, created_by, created_on, client_facing_note )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityBatchId() . ', ' .
						$this->sqlUtilityBatchActivityId() . ', ' .
						$this->sqlUtilityBatchQcFlagTypeId() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlUtilityAccountId() . ', ' .
						$this->sqlUtilityBillAccountId() . ', ' .
						$this->sqlUtilityBillChargeId() . ', ' .
						$this->sqlQcMemo() . ', ' .
						$this->sqlBillingAnalystNote() . ', ' .
						$this->sqlQcApprovedBy() . ', ' .
						$this->sqlQcApprovedOn() . ', ' .
						$this->sqlQcDeclinedBy() . ', ' .
						$this->sqlQcDeclinedOn() . ', ' .
						$this->sqlQcSpecialistNote() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlClientFacingNote() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId(). ',' ; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_activity_id = ' . $this->sqlUtilityBatchActivityId(). ',' ; } elseif( true == array_key_exists( 'UtilityBatchActivityId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_activity_id = ' . $this->sqlUtilityBatchActivityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_qc_flag_type_id = ' . $this->sqlUtilityBatchQcFlagTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBatchQcFlagTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_qc_flag_type_id = ' . $this->sqlUtilityBatchQcFlagTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_charge_id = ' . $this->sqlUtilityBillChargeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillChargeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_charge_id = ' . $this->sqlUtilityBillChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_memo = ' . $this->sqlQcMemo(). ',' ; } elseif( true == array_key_exists( 'QcMemo', $this->getChangedColumns() ) ) { $strSql .= ' qc_memo = ' . $this->sqlQcMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_analyst_note = ' . $this->sqlBillingAnalystNote(). ',' ; } elseif( true == array_key_exists( 'BillingAnalystNote', $this->getChangedColumns() ) ) { $strSql .= ' billing_analyst_note = ' . $this->sqlBillingAnalystNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_approved_by = ' . $this->sqlQcApprovedBy(). ',' ; } elseif( true == array_key_exists( 'QcApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' qc_approved_by = ' . $this->sqlQcApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_approved_on = ' . $this->sqlQcApprovedOn(). ',' ; } elseif( true == array_key_exists( 'QcApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' qc_approved_on = ' . $this->sqlQcApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_declined_by = ' . $this->sqlQcDeclinedBy(). ',' ; } elseif( true == array_key_exists( 'QcDeclinedBy', $this->getChangedColumns() ) ) { $strSql .= ' qc_declined_by = ' . $this->sqlQcDeclinedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_declined_on = ' . $this->sqlQcDeclinedOn(). ',' ; } elseif( true == array_key_exists( 'QcDeclinedOn', $this->getChangedColumns() ) ) { $strSql .= ' qc_declined_on = ' . $this->sqlQcDeclinedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qc_specialist_note = ' . $this->sqlQcSpecialistNote(). ',' ; } elseif( true == array_key_exists( 'QcSpecialistNote', $this->getChangedColumns() ) ) { $strSql .= ' qc_specialist_note = ' . $this->sqlQcSpecialistNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_facing_note = ' . $this->sqlClientFacingNote(). ',' ; } elseif( true == array_key_exists( 'ClientFacingNote', $this->getChangedColumns() ) ) { $strSql .= ' client_facing_note = ' . $this->sqlClientFacingNote() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'utility_batch_activity_id' => $this->getUtilityBatchActivityId(),
			'utility_batch_qc_flag_type_id' => $this->getUtilityBatchQcFlagTypeId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'utility_bill_charge_id' => $this->getUtilityBillChargeId(),
			'qc_memo' => $this->getQcMemo(),
			'billing_analyst_note' => $this->getBillingAnalystNote(),
			'qc_approved_by' => $this->getQcApprovedBy(),
			'qc_approved_on' => $this->getQcApprovedOn(),
			'qc_declined_by' => $this->getQcDeclinedBy(),
			'qc_declined_on' => $this->getQcDeclinedOn(),
			'qc_specialist_note' => $this->getQcSpecialistNote(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'client_facing_note' => $this->getClientFacingNote()
		);
	}

}
?>