<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUtilityArCodeGroup extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.utility_ar_code_groups';

	protected $m_intWorksId;
	protected $m_intCid;
	protected $m_intArCodeGroupTypeId;
	protected $m_intArCodeSummaryTypeId;
	protected $m_intDefaultArCodeGroupId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['works_id'] ) && $boolDirectSet ) $this->set( 'm_intWorksId', trim( $arrValues['works_id'] ) ); elseif( isset( $arrValues['works_id'] ) ) $this->setWorksId( $arrValues['works_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ar_code_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeGroupTypeId', trim( $arrValues['ar_code_group_type_id'] ) ); elseif( isset( $arrValues['ar_code_group_type_id'] ) ) $this->setArCodeGroupTypeId( $arrValues['ar_code_group_type_id'] );
		if( isset( $arrValues['ar_code_summary_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeSummaryTypeId', trim( $arrValues['ar_code_summary_type_id'] ) ); elseif( isset( $arrValues['ar_code_summary_type_id'] ) ) $this->setArCodeSummaryTypeId( $arrValues['ar_code_summary_type_id'] );
		if( isset( $arrValues['default_ar_code_group_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultArCodeGroupId', trim( $arrValues['default_ar_code_group_id'] ) ); elseif( isset( $arrValues['default_ar_code_group_id'] ) ) $this->setDefaultArCodeGroupId( $arrValues['default_ar_code_group_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setWorksId( $intWorksId ) {
		$this->set( 'm_intWorksId', CStrings::strToIntDef( $intWorksId, NULL, false ) );
	}

	public function getWorksId() {
		return $this->m_intWorksId;
	}

	public function sqlWorksId() {
		return ( true == isset( $this->m_intWorksId ) ) ? ( string ) $this->m_intWorksId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setArCodeGroupTypeId( $intArCodeGroupTypeId ) {
		$this->set( 'm_intArCodeGroupTypeId', CStrings::strToIntDef( $intArCodeGroupTypeId, NULL, false ) );
	}

	public function getArCodeGroupTypeId() {
		return $this->m_intArCodeGroupTypeId;
	}

	public function sqlArCodeGroupTypeId() {
		return ( true == isset( $this->m_intArCodeGroupTypeId ) ) ? ( string ) $this->m_intArCodeGroupTypeId : 'NULL';
	}

	public function setArCodeSummaryTypeId( $intArCodeSummaryTypeId ) {
		$this->set( 'm_intArCodeSummaryTypeId', CStrings::strToIntDef( $intArCodeSummaryTypeId, NULL, false ) );
	}

	public function getArCodeSummaryTypeId() {
		return $this->m_intArCodeSummaryTypeId;
	}

	public function sqlArCodeSummaryTypeId() {
		return ( true == isset( $this->m_intArCodeSummaryTypeId ) ) ? ( string ) $this->m_intArCodeSummaryTypeId : 'NULL';
	}

	public function setDefaultArCodeGroupId( $intDefaultArCodeGroupId ) {
		$this->set( 'm_intDefaultArCodeGroupId', CStrings::strToIntDef( $intDefaultArCodeGroupId, NULL, false ) );
	}

	public function getDefaultArCodeGroupId() {
		return $this->m_intDefaultArCodeGroupId;
	}

	public function sqlDefaultArCodeGroupId() {
		return ( true == isset( $this->m_intDefaultArCodeGroupId ) ) ? ( string ) $this->m_intDefaultArCodeGroupId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function toArray() {
		return array(
			'works_id' => $this->getWorksId(),
			'cid' => $this->getCid(),
			'ar_code_group_type_id' => $this->getArCodeGroupTypeId(),
			'ar_code_summary_type_id' => $this->getArCodeSummaryTypeId(),
			'default_ar_code_group_id' => $this->getDefaultArCodeGroupId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>