<?php

class CBaseVendorPaymentAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.vendor_payment_accounts';

	protected $m_intId;
	protected $m_intVendorId;
	protected $m_intPaymentTypeId;
	protected $m_intPsProductId;
	protected $m_strBilltoAcountNumber;
	protected $m_strBilltoCompanyName;
	protected $m_strBilltoNameFirst;
	protected $m_strBilltoNameLast;
	protected $m_strBilltoStreetLine1;
	protected $m_strBilltoStreetLine2;
	protected $m_strBilltoStreetLine3;
	protected $m_strBilltoUnitNumber;
	protected $m_strBilltoCity;
	protected $m_strBilltoStateCode;
	protected $m_strBilltoProvince;
	protected $m_strBilltoPostalCode;
	protected $m_strBilltoCountryCode;
	protected $m_strBilltoPhoneNumber;
	protected $m_strBilltoEmailAddress;
	protected $m_strSecureReferenceNumber;
	protected $m_strCcCardNumberEncrypted;
	protected $m_strCcExpDateMonth;
	protected $m_strCcExpDateYear;
	protected $m_strCcNameOnCard;
	protected $m_strCcBinNumber;
	protected $m_boolIsDebitCard;
	protected $m_strCheckBankName;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckNameOnAccount;
	protected $m_strAccountNickname;
	protected $m_boolIsInternational;
	protected $m_boolIsDefault;
	protected $m_strLastUsedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDebitCard = false;
		$this->m_boolIsInternational = false;
		$this->m_boolIsDefault = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['billto_acount_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoAcountNumber', trim( $arrValues['billto_acount_number'] ) ); elseif( isset( $arrValues['billto_acount_number'] ) ) $this->setBilltoAcountNumber( $arrValues['billto_acount_number'] );
		if( isset( $arrValues['billto_company_name'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCompanyName', trim( $arrValues['billto_company_name'] ) ); elseif( isset( $arrValues['billto_company_name'] ) ) $this->setBilltoCompanyName( $arrValues['billto_company_name'] );
		if( isset( $arrValues['billto_name_first'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameFirst', trim( $arrValues['billto_name_first'] ) ); elseif( isset( $arrValues['billto_name_first'] ) ) $this->setBilltoNameFirst( $arrValues['billto_name_first'] );
		if( isset( $arrValues['billto_name_last'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameLast', trim( $arrValues['billto_name_last'] ) ); elseif( isset( $arrValues['billto_name_last'] ) ) $this->setBilltoNameLast( $arrValues['billto_name_last'] );
		if( isset( $arrValues['billto_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine1', trim( $arrValues['billto_street_line1'] ) ); elseif( isset( $arrValues['billto_street_line1'] ) ) $this->setBilltoStreetLine1( $arrValues['billto_street_line1'] );
		if( isset( $arrValues['billto_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine2', trim( $arrValues['billto_street_line2'] ) ); elseif( isset( $arrValues['billto_street_line2'] ) ) $this->setBilltoStreetLine2( $arrValues['billto_street_line2'] );
		if( isset( $arrValues['billto_street_line3'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine3', trim( $arrValues['billto_street_line3'] ) ); elseif( isset( $arrValues['billto_street_line3'] ) ) $this->setBilltoStreetLine3( $arrValues['billto_street_line3'] );
		if( isset( $arrValues['billto_unit_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoUnitNumber', trim( $arrValues['billto_unit_number'] ) ); elseif( isset( $arrValues['billto_unit_number'] ) ) $this->setBilltoUnitNumber( $arrValues['billto_unit_number'] );
		if( isset( $arrValues['billto_city'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCity', trim( $arrValues['billto_city'] ) ); elseif( isset( $arrValues['billto_city'] ) ) $this->setBilltoCity( $arrValues['billto_city'] );
		if( isset( $arrValues['billto_state_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStateCode', trim( $arrValues['billto_state_code'] ) ); elseif( isset( $arrValues['billto_state_code'] ) ) $this->setBilltoStateCode( $arrValues['billto_state_code'] );
		if( isset( $arrValues['billto_province'] ) && $boolDirectSet ) $this->set( 'm_strBilltoProvince', trim( $arrValues['billto_province'] ) ); elseif( isset( $arrValues['billto_province'] ) ) $this->setBilltoProvince( $arrValues['billto_province'] );
		if( isset( $arrValues['billto_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPostalCode', trim( $arrValues['billto_postal_code'] ) ); elseif( isset( $arrValues['billto_postal_code'] ) ) $this->setBilltoPostalCode( $arrValues['billto_postal_code'] );
		if( isset( $arrValues['billto_country_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCountryCode', trim( $arrValues['billto_country_code'] ) ); elseif( isset( $arrValues['billto_country_code'] ) ) $this->setBilltoCountryCode( $arrValues['billto_country_code'] );
		if( isset( $arrValues['billto_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPhoneNumber', trim( $arrValues['billto_phone_number'] ) ); elseif( isset( $arrValues['billto_phone_number'] ) ) $this->setBilltoPhoneNumber( $arrValues['billto_phone_number'] );
		if( isset( $arrValues['billto_email_address'] ) && $boolDirectSet ) $this->set( 'm_strBilltoEmailAddress', trim( $arrValues['billto_email_address'] ) ); elseif( isset( $arrValues['billto_email_address'] ) ) $this->setBilltoEmailAddress( $arrValues['billto_email_address'] );
		if( isset( $arrValues['secure_reference_number'] ) && $boolDirectSet ) $this->set( 'm_strSecureReferenceNumber', trim( $arrValues['secure_reference_number'] ) ); elseif( isset( $arrValues['secure_reference_number'] ) ) $this->setSecureReferenceNumber( $arrValues['secure_reference_number'] );
		if( isset( $arrValues['cc_card_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCcCardNumberEncrypted', trim( $arrValues['cc_card_number_encrypted'] ) ); elseif( isset( $arrValues['cc_card_number_encrypted'] ) ) $this->setCcCardNumberEncrypted( $arrValues['cc_card_number_encrypted'] );
		if( isset( $arrValues['cc_exp_date_month'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateMonth', trim( $arrValues['cc_exp_date_month'] ) ); elseif( isset( $arrValues['cc_exp_date_month'] ) ) $this->setCcExpDateMonth( $arrValues['cc_exp_date_month'] );
		if( isset( $arrValues['cc_exp_date_year'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateYear', trim( $arrValues['cc_exp_date_year'] ) ); elseif( isset( $arrValues['cc_exp_date_year'] ) ) $this->setCcExpDateYear( $arrValues['cc_exp_date_year'] );
		if( isset( $arrValues['cc_name_on_card'] ) && $boolDirectSet ) $this->set( 'm_strCcNameOnCard', trim( $arrValues['cc_name_on_card'] ) ); elseif( isset( $arrValues['cc_name_on_card'] ) ) $this->setCcNameOnCard( $arrValues['cc_name_on_card'] );
		if( isset( $arrValues['cc_bin_number'] ) && $boolDirectSet ) $this->set( 'm_strCcBinNumber', trim( $arrValues['cc_bin_number'] ) ); elseif( isset( $arrValues['cc_bin_number'] ) ) $this->setCcBinNumber( $arrValues['cc_bin_number'] );
		if( isset( $arrValues['is_debit_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsDebitCard', trim( stripcslashes( $arrValues['is_debit_card'] ) ) ); elseif( isset( $arrValues['is_debit_card'] ) ) $this->setIsDebitCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_debit_card'] ) : $arrValues['is_debit_card'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckBankName', trim( $arrValues['check_bank_name'] ) ); elseif( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( $arrValues['check_bank_name'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( $arrValues['check_account_number_encrypted'] ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( $arrValues['check_routing_number'] ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strCheckNameOnAccount', trim( $arrValues['check_name_on_account'] ) ); elseif( isset( $arrValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( $arrValues['check_name_on_account'] );
		if( isset( $arrValues['account_nickname'] ) && $boolDirectSet ) $this->set( 'm_strAccountNickname', trim( $arrValues['account_nickname'] ) ); elseif( isset( $arrValues['account_nickname'] ) ) $this->setAccountNickname( $arrValues['account_nickname'] );
		if( isset( $arrValues['is_international'] ) && $boolDirectSet ) $this->set( 'm_boolIsInternational', trim( stripcslashes( $arrValues['is_international'] ) ) ); elseif( isset( $arrValues['is_international'] ) ) $this->setIsInternational( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_international'] ) : $arrValues['is_international'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['last_used_on'] ) && $boolDirectSet ) $this->set( 'm_strLastUsedOn', trim( $arrValues['last_used_on'] ) ); elseif( isset( $arrValues['last_used_on'] ) ) $this->setLastUsedOn( $arrValues['last_used_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setBilltoAcountNumber( $strBilltoAcountNumber ) {
		$this->set( 'm_strBilltoAcountNumber', CStrings::strTrimDef( $strBilltoAcountNumber, 50, NULL, true ) );
	}

	public function getBilltoAcountNumber() {
		return $this->m_strBilltoAcountNumber;
	}

	public function sqlBilltoAcountNumber() {
		return ( true == isset( $this->m_strBilltoAcountNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoAcountNumber ) : '\'' . addslashes( $this->m_strBilltoAcountNumber ) . '\'' ) : 'NULL';
	}

	public function setBilltoCompanyName( $strBilltoCompanyName ) {
		$this->set( 'm_strBilltoCompanyName', CStrings::strTrimDef( $strBilltoCompanyName, 100, NULL, true ) );
	}

	public function getBilltoCompanyName() {
		return $this->m_strBilltoCompanyName;
	}

	public function sqlBilltoCompanyName() {
		return ( true == isset( $this->m_strBilltoCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoCompanyName ) : '\'' . addslashes( $this->m_strBilltoCompanyName ) . '\'' ) : 'NULL';
	}

	public function setBilltoNameFirst( $strBilltoNameFirst ) {
		$this->set( 'm_strBilltoNameFirst', CStrings::strTrimDef( $strBilltoNameFirst, 50, NULL, true ) );
	}

	public function getBilltoNameFirst() {
		return $this->m_strBilltoNameFirst;
	}

	public function sqlBilltoNameFirst() {
		return ( true == isset( $this->m_strBilltoNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoNameFirst ) : '\'' . addslashes( $this->m_strBilltoNameFirst ) . '\'' ) : 'NULL';
	}

	public function setBilltoNameLast( $strBilltoNameLast ) {
		$this->set( 'm_strBilltoNameLast', CStrings::strTrimDef( $strBilltoNameLast, 50, NULL, true ) );
	}

	public function getBilltoNameLast() {
		return $this->m_strBilltoNameLast;
	}

	public function sqlBilltoNameLast() {
		return ( true == isset( $this->m_strBilltoNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoNameLast ) : '\'' . addslashes( $this->m_strBilltoNameLast ) . '\'' ) : 'NULL';
	}

	public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
		$this->set( 'm_strBilltoStreetLine1', CStrings::strTrimDef( $strBilltoStreetLine1, 100, NULL, true ) );
	}

	public function getBilltoStreetLine1() {
		return $this->m_strBilltoStreetLine1;
	}

	public function sqlBilltoStreetLine1() {
		return ( true == isset( $this->m_strBilltoStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStreetLine1 ) : '\'' . addslashes( $this->m_strBilltoStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setBilltoStreetLine2( $strBilltoStreetLine2 ) {
		$this->set( 'm_strBilltoStreetLine2', CStrings::strTrimDef( $strBilltoStreetLine2, 100, NULL, true ) );
	}

	public function getBilltoStreetLine2() {
		return $this->m_strBilltoStreetLine2;
	}

	public function sqlBilltoStreetLine2() {
		return ( true == isset( $this->m_strBilltoStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStreetLine2 ) : '\'' . addslashes( $this->m_strBilltoStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setBilltoStreetLine3( $strBilltoStreetLine3 ) {
		$this->set( 'm_strBilltoStreetLine3', CStrings::strTrimDef( $strBilltoStreetLine3, 100, NULL, true ) );
	}

	public function getBilltoStreetLine3() {
		return $this->m_strBilltoStreetLine3;
	}

	public function sqlBilltoStreetLine3() {
		return ( true == isset( $this->m_strBilltoStreetLine3 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStreetLine3 ) : '\'' . addslashes( $this->m_strBilltoStreetLine3 ) . '\'' ) : 'NULL';
	}

	public function setBilltoUnitNumber( $strBilltoUnitNumber ) {
		$this->set( 'm_strBilltoUnitNumber', CStrings::strTrimDef( $strBilltoUnitNumber, 50, NULL, true ) );
	}

	public function getBilltoUnitNumber() {
		return $this->m_strBilltoUnitNumber;
	}

	public function sqlBilltoUnitNumber() {
		return ( true == isset( $this->m_strBilltoUnitNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoUnitNumber ) : '\'' . addslashes( $this->m_strBilltoUnitNumber ) . '\'' ) : 'NULL';
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->set( 'm_strBilltoCity', CStrings::strTrimDef( $strBilltoCity, 50, NULL, true ) );
	}

	public function getBilltoCity() {
		return $this->m_strBilltoCity;
	}

	public function sqlBilltoCity() {
		return ( true == isset( $this->m_strBilltoCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoCity ) : '\'' . addslashes( $this->m_strBilltoCity ) . '\'' ) : 'NULL';
	}

	public function setBilltoStateCode( $strBilltoStateCode ) {
		$this->set( 'm_strBilltoStateCode', CStrings::strTrimDef( $strBilltoStateCode, 2, NULL, true ) );
	}

	public function getBilltoStateCode() {
		return $this->m_strBilltoStateCode;
	}

	public function sqlBilltoStateCode() {
		return ( true == isset( $this->m_strBilltoStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStateCode ) : '\'' . addslashes( $this->m_strBilltoStateCode ) . '\'' ) : 'NULL';
	}

	public function setBilltoProvince( $strBilltoProvince ) {
		$this->set( 'm_strBilltoProvince', CStrings::strTrimDef( $strBilltoProvince, 50, NULL, true ) );
	}

	public function getBilltoProvince() {
		return $this->m_strBilltoProvince;
	}

	public function sqlBilltoProvince() {
		return ( true == isset( $this->m_strBilltoProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoProvince ) : '\'' . addslashes( $this->m_strBilltoProvince ) . '\'' ) : 'NULL';
	}

	public function setBilltoPostalCode( $strBilltoPostalCode ) {
		$this->set( 'm_strBilltoPostalCode', CStrings::strTrimDef( $strBilltoPostalCode, 6, NULL, true ) );
	}

	public function getBilltoPostalCode() {
		return $this->m_strBilltoPostalCode;
	}

	public function sqlBilltoPostalCode() {
		return ( true == isset( $this->m_strBilltoPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoPostalCode ) : '\'' . addslashes( $this->m_strBilltoPostalCode ) . '\'' ) : 'NULL';
	}

	public function setBilltoCountryCode( $strBilltoCountryCode ) {
		$this->set( 'm_strBilltoCountryCode', CStrings::strTrimDef( $strBilltoCountryCode, 2, NULL, true ) );
	}

	public function getBilltoCountryCode() {
		return $this->m_strBilltoCountryCode;
	}

	public function sqlBilltoCountryCode() {
		return ( true == isset( $this->m_strBilltoCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoCountryCode ) : '\'' . addslashes( $this->m_strBilltoCountryCode ) . '\'' ) : 'NULL';
	}

	public function setBilltoPhoneNumber( $strBilltoPhoneNumber ) {
		$this->set( 'm_strBilltoPhoneNumber', CStrings::strTrimDef( $strBilltoPhoneNumber, 15, NULL, true ) );
	}

	public function getBilltoPhoneNumber() {
		return $this->m_strBilltoPhoneNumber;
	}

	public function sqlBilltoPhoneNumber() {
		return ( true == isset( $this->m_strBilltoPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoPhoneNumber ) : '\'' . addslashes( $this->m_strBilltoPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setBilltoEmailAddress( $strBilltoEmailAddress ) {
		$this->set( 'm_strBilltoEmailAddress', CStrings::strTrimDef( $strBilltoEmailAddress, 50, NULL, true ) );
	}

	public function getBilltoEmailAddress() {
		return $this->m_strBilltoEmailAddress;
	}

	public function sqlBilltoEmailAddress() {
		return ( true == isset( $this->m_strBilltoEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoEmailAddress ) : '\'' . addslashes( $this->m_strBilltoEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setSecureReferenceNumber( $strSecureReferenceNumber ) {
		$this->set( 'm_strSecureReferenceNumber', CStrings::strTrimDef( $strSecureReferenceNumber, 30, NULL, true ) );
	}

	public function getSecureReferenceNumber() {
		return $this->m_strSecureReferenceNumber;
	}

	public function sqlSecureReferenceNumber() {
		return ( true == isset( $this->m_strSecureReferenceNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSecureReferenceNumber ) : '\'' . addslashes( $this->m_strSecureReferenceNumber ) . '\'' ) : 'NULL';
	}

	public function setCcCardNumberEncrypted( $strCcCardNumberEncrypted ) {
		$this->set( 'm_strCcCardNumberEncrypted', CStrings::strTrimDef( $strCcCardNumberEncrypted, -1, NULL, true ) );
	}

	public function getCcCardNumberEncrypted() {
		return $this->m_strCcCardNumberEncrypted;
	}

	public function sqlCcCardNumberEncrypted() {
		return ( true == isset( $this->m_strCcCardNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcCardNumberEncrypted ) : '\'' . addslashes( $this->m_strCcCardNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setCcExpDateMonth( $strCcExpDateMonth ) {
		$this->set( 'm_strCcExpDateMonth', CStrings::strTrimDef( $strCcExpDateMonth, 2, NULL, true ) );
	}

	public function getCcExpDateMonth() {
		return $this->m_strCcExpDateMonth;
	}

	public function sqlCcExpDateMonth() {
		return ( true == isset( $this->m_strCcExpDateMonth ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcExpDateMonth ) : '\'' . addslashes( $this->m_strCcExpDateMonth ) . '\'' ) : 'NULL';
	}

	public function setCcExpDateYear( $strCcExpDateYear ) {
		$this->set( 'm_strCcExpDateYear', CStrings::strTrimDef( $strCcExpDateYear, 4, NULL, true ) );
	}

	public function getCcExpDateYear() {
		return $this->m_strCcExpDateYear;
	}

	public function sqlCcExpDateYear() {
		return ( true == isset( $this->m_strCcExpDateYear ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcExpDateYear ) : '\'' . addslashes( $this->m_strCcExpDateYear ) . '\'' ) : 'NULL';
	}

	public function setCcNameOnCard( $strCcNameOnCard ) {
		$this->set( 'm_strCcNameOnCard', CStrings::strTrimDef( $strCcNameOnCard, 50, NULL, true ) );
	}

	public function getCcNameOnCard() {
		return $this->m_strCcNameOnCard;
	}

	public function sqlCcNameOnCard() {
		return ( true == isset( $this->m_strCcNameOnCard ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcNameOnCard ) : '\'' . addslashes( $this->m_strCcNameOnCard ) . '\'' ) : 'NULL';
	}

	public function setCcBinNumber( $strCcBinNumber ) {
		$this->set( 'm_strCcBinNumber', CStrings::strTrimDef( $strCcBinNumber, 10, NULL, true ) );
	}

	public function getCcBinNumber() {
		return $this->m_strCcBinNumber;
	}

	public function sqlCcBinNumber() {
		return ( true == isset( $this->m_strCcBinNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcBinNumber ) : '\'' . addslashes( $this->m_strCcBinNumber ) . '\'' ) : 'NULL';
	}

	public function setIsDebitCard( $boolIsDebitCard ) {
		$this->set( 'm_boolIsDebitCard', CStrings::strToBool( $boolIsDebitCard ) );
	}

	public function getIsDebitCard() {
		return $this->m_boolIsDebitCard;
	}

	public function sqlIsDebitCard() {
		return ( true == isset( $this->m_boolIsDebitCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDebitCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->set( 'm_strCheckBankName', CStrings::strTrimDef( $strCheckBankName, 50, NULL, true ) );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckBankName ) : '\'' . addslashes( $this->m_strCheckBankName ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, -1, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 9, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckRoutingNumber ) : '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->set( 'm_strCheckNameOnAccount', CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true ) );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckNameOnAccount ) : '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' ) : 'NULL';
	}

	public function setAccountNickname( $strAccountNickname ) {
		$this->set( 'm_strAccountNickname', CStrings::strTrimDef( $strAccountNickname, 30, NULL, true ) );
	}

	public function getAccountNickname() {
		return $this->m_strAccountNickname;
	}

	public function sqlAccountNickname() {
		return ( true == isset( $this->m_strAccountNickname ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountNickname ) : '\'' . addslashes( $this->m_strAccountNickname ) . '\'' ) : 'NULL';
	}

	public function setIsInternational( $boolIsInternational ) {
		$this->set( 'm_boolIsInternational', CStrings::strToBool( $boolIsInternational ) );
	}

	public function getIsInternational() {
		return $this->m_boolIsInternational;
	}

	public function sqlIsInternational() {
		return ( true == isset( $this->m_boolIsInternational ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInternational ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLastUsedOn( $strLastUsedOn ) {
		$this->set( 'm_strLastUsedOn', CStrings::strTrimDef( $strLastUsedOn, -1, NULL, true ) );
	}

	public function getLastUsedOn() {
		return $this->m_strLastUsedOn;
	}

	public function sqlLastUsedOn() {
		return ( true == isset( $this->m_strLastUsedOn ) ) ? '\'' . $this->m_strLastUsedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_id, payment_type_id, ps_product_id, billto_acount_number, billto_company_name, billto_name_first, billto_name_last, billto_street_line1, billto_street_line2, billto_street_line3, billto_unit_number, billto_city, billto_state_code, billto_province, billto_postal_code, billto_country_code, billto_phone_number, billto_email_address, secure_reference_number, cc_card_number_encrypted, cc_exp_date_month, cc_exp_date_year, cc_name_on_card, cc_bin_number, is_debit_card, check_bank_name, check_account_type_id, check_account_number_encrypted, check_routing_number, check_name_on_account, account_nickname, is_international, is_default, last_used_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlVendorId() . ', ' .
		          $this->sqlPaymentTypeId() . ', ' .
		          $this->sqlPsProductId() . ', ' .
		          $this->sqlBilltoAcountNumber() . ', ' .
		          $this->sqlBilltoCompanyName() . ', ' .
		          $this->sqlBilltoNameFirst() . ', ' .
		          $this->sqlBilltoNameLast() . ', ' .
		          $this->sqlBilltoStreetLine1() . ', ' .
		          $this->sqlBilltoStreetLine2() . ', ' .
		          $this->sqlBilltoStreetLine3() . ', ' .
		          $this->sqlBilltoUnitNumber() . ', ' .
		          $this->sqlBilltoCity() . ', ' .
		          $this->sqlBilltoStateCode() . ', ' .
		          $this->sqlBilltoProvince() . ', ' .
		          $this->sqlBilltoPostalCode() . ', ' .
		          $this->sqlBilltoCountryCode() . ', ' .
		          $this->sqlBilltoPhoneNumber() . ', ' .
		          $this->sqlBilltoEmailAddress() . ', ' .
		          $this->sqlSecureReferenceNumber() . ', ' .
		          $this->sqlCcCardNumberEncrypted() . ', ' .
		          $this->sqlCcExpDateMonth() . ', ' .
		          $this->sqlCcExpDateYear() . ', ' .
		          $this->sqlCcNameOnCard() . ', ' .
		          $this->sqlCcBinNumber() . ', ' .
		          $this->sqlIsDebitCard() . ', ' .
		          $this->sqlCheckBankName() . ', ' .
		          $this->sqlCheckAccountTypeId() . ', ' .
		          $this->sqlCheckAccountNumberEncrypted() . ', ' .
		          $this->sqlCheckRoutingNumber() . ', ' .
		          $this->sqlCheckNameOnAccount() . ', ' .
		          $this->sqlAccountNickname() . ', ' .
		          $this->sqlIsInternational() . ', ' .
		          $this->sqlIsDefault() . ', ' .
		          $this->sqlLastUsedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId(). ',' ; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_acount_number = ' . $this->sqlBilltoAcountNumber(). ',' ; } elseif( true == array_key_exists( 'BilltoAcountNumber', $this->getChangedColumns() ) ) { $strSql .= ' billto_acount_number = ' . $this->sqlBilltoAcountNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName(). ',' ; } elseif( true == array_key_exists( 'BilltoCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_first = ' . $this->sqlBilltoNameFirst(). ',' ; } elseif( true == array_key_exists( 'BilltoNameFirst', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_first = ' . $this->sqlBilltoNameFirst() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_name_last = ' . $this->sqlBilltoNameLast(). ',' ; } elseif( true == array_key_exists( 'BilltoNameLast', $this->getChangedColumns() ) ) { $strSql .= ' billto_name_last = ' . $this->sqlBilltoNameLast() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line3 = ' . $this->sqlBilltoStreetLine3(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line3 = ' . $this->sqlBilltoStreetLine3() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_unit_number = ' . $this->sqlBilltoUnitNumber(). ',' ; } elseif( true == array_key_exists( 'BilltoUnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' billto_unit_number = ' . $this->sqlBilltoUnitNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity(). ',' ; } elseif( true == array_key_exists( 'BilltoCity', $this->getChangedColumns() ) ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode(). ',' ; } elseif( true == array_key_exists( 'BilltoStateCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince(). ',' ; } elseif( true == array_key_exists( 'BilltoProvince', $this->getChangedColumns() ) ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode(). ',' ; } elseif( true == array_key_exists( 'BilltoPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode(). ',' ; } elseif( true == array_key_exists( 'BilltoCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_phone_number = ' . $this->sqlBilltoPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'BilltoPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' billto_phone_number = ' . $this->sqlBilltoPhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_email_address = ' . $this->sqlBilltoEmailAddress(). ',' ; } elseif( true == array_key_exists( 'BilltoEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' billto_email_address = ' . $this->sqlBilltoEmailAddress() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'SecureReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_card_number_encrypted = ' . $this->sqlCcCardNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CcCardNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' cc_card_number_encrypted = ' . $this->sqlCcCardNumberEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_exp_date_month = ' . $this->sqlCcExpDateMonth(). ',' ; } elseif( true == array_key_exists( 'CcExpDateMonth', $this->getChangedColumns() ) ) { $strSql .= ' cc_exp_date_month = ' . $this->sqlCcExpDateMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_exp_date_year = ' . $this->sqlCcExpDateYear(). ',' ; } elseif( true == array_key_exists( 'CcExpDateYear', $this->getChangedColumns() ) ) { $strSql .= ' cc_exp_date_year = ' . $this->sqlCcExpDateYear() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_name_on_card = ' . $this->sqlCcNameOnCard(). ',' ; } elseif( true == array_key_exists( 'CcNameOnCard', $this->getChangedColumns() ) ) { $strSql .= ' cc_name_on_card = ' . $this->sqlCcNameOnCard() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_bin_number = ' . $this->sqlCcBinNumber(). ',' ; } elseif( true == array_key_exists( 'CcBinNumber', $this->getChangedColumns() ) ) { $strSql .= ' cc_bin_number = ' . $this->sqlCcBinNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_debit_card = ' . $this->sqlIsDebitCard(). ',' ; } elseif( true == array_key_exists( 'IsDebitCard', $this->getChangedColumns() ) ) { $strSql .= ' is_debit_card = ' . $this->sqlIsDebitCard() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName(). ',' ; } elseif( true == array_key_exists( 'CheckBankName', $this->getChangedColumns() ) ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'CheckNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_nickname = ' . $this->sqlAccountNickname(). ',' ; } elseif( true == array_key_exists( 'AccountNickname', $this->getChangedColumns() ) ) { $strSql .= ' account_nickname = ' . $this->sqlAccountNickname() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_international = ' . $this->sqlIsInternational(). ',' ; } elseif( true == array_key_exists( 'IsInternational', $this->getChangedColumns() ) ) { $strSql .= ' is_international = ' . $this->sqlIsInternational() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_used_on = ' . $this->sqlLastUsedOn(). ',' ; } elseif( true == array_key_exists( 'LastUsedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_used_on = ' . $this->sqlLastUsedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_id' => $this->getVendorId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'ps_product_id' => $this->getPsProductId(),
			'billto_acount_number' => $this->getBilltoAcountNumber(),
			'billto_company_name' => $this->getBilltoCompanyName(),
			'billto_name_first' => $this->getBilltoNameFirst(),
			'billto_name_last' => $this->getBilltoNameLast(),
			'billto_street_line1' => $this->getBilltoStreetLine1(),
			'billto_street_line2' => $this->getBilltoStreetLine2(),
			'billto_street_line3' => $this->getBilltoStreetLine3(),
			'billto_unit_number' => $this->getBilltoUnitNumber(),
			'billto_city' => $this->getBilltoCity(),
			'billto_state_code' => $this->getBilltoStateCode(),
			'billto_province' => $this->getBilltoProvince(),
			'billto_postal_code' => $this->getBilltoPostalCode(),
			'billto_country_code' => $this->getBilltoCountryCode(),
			'billto_phone_number' => $this->getBilltoPhoneNumber(),
			'billto_email_address' => $this->getBilltoEmailAddress(),
			'secure_reference_number' => $this->getSecureReferenceNumber(),
			'cc_card_number_encrypted' => $this->getCcCardNumberEncrypted(),
			'cc_exp_date_month' => $this->getCcExpDateMonth(),
			'cc_exp_date_year' => $this->getCcExpDateYear(),
			'cc_name_on_card' => $this->getCcNameOnCard(),
			'cc_bin_number' => $this->getCcBinNumber(),
			'is_debit_card' => $this->getIsDebitCard(),
			'check_bank_name' => $this->getCheckBankName(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_name_on_account' => $this->getCheckNameOnAccount(),
			'account_nickname' => $this->getAccountNickname(),
			'is_international' => $this->getIsInternational(),
			'is_default' => $this->getIsDefault(),
			'last_used_on' => $this->getLastUsedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>