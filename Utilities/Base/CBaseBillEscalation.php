<?php

class CBaseBillEscalation extends CEosSingularBase {

	const TABLE_NAME = 'public.bill_escalations';

	protected $m_intId;
	protected $m_intBillEscalateTypeId;
	protected $m_intUtilityBillId;
	protected $m_intUemBillStatusTypeId;
	protected $m_intResolutionEscalateStepId;
	protected $m_strEscalationDatetime;
	protected $m_strEscalatedEmailAddress;
	protected $m_strResolutionNote;
	protected $m_strEscalationNote;
	protected $m_strReminderDatetime;
	protected $m_boolIsBillProcessEscalation;
	protected $m_intEscalationCompletedBy;
	protected $m_strEscalationCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intUemBillStatusTypeId = '1';
		$this->m_boolIsBillProcessEscalation = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['bill_escalate_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillEscalateTypeId', trim( $arrValues['bill_escalate_type_id'] ) ); elseif( isset( $arrValues['bill_escalate_type_id'] ) ) $this->setBillEscalateTypeId( $arrValues['bill_escalate_type_id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['uem_bill_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUemBillStatusTypeId', trim( $arrValues['uem_bill_status_type_id'] ) ); elseif( isset( $arrValues['uem_bill_status_type_id'] ) ) $this->setUemBillStatusTypeId( $arrValues['uem_bill_status_type_id'] );
		if( isset( $arrValues['resolution_escalate_step_id'] ) && $boolDirectSet ) $this->set( 'm_intResolutionEscalateStepId', trim( $arrValues['resolution_escalate_step_id'] ) ); elseif( isset( $arrValues['resolution_escalate_step_id'] ) ) $this->setResolutionEscalateStepId( $arrValues['resolution_escalate_step_id'] );
		if( isset( $arrValues['escalation_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEscalationDatetime', trim( $arrValues['escalation_datetime'] ) ); elseif( isset( $arrValues['escalation_datetime'] ) ) $this->setEscalationDatetime( $arrValues['escalation_datetime'] );
		if( isset( $arrValues['escalated_email_address'] ) && $boolDirectSet ) $this->set( 'm_strEscalatedEmailAddress', trim( stripcslashes( $arrValues['escalated_email_address'] ) ) ); elseif( isset( $arrValues['escalated_email_address'] ) ) $this->setEscalatedEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['escalated_email_address'] ) : $arrValues['escalated_email_address'] );
		if( isset( $arrValues['resolution_note'] ) && $boolDirectSet ) $this->set( 'm_strResolutionNote', trim( stripcslashes( $arrValues['resolution_note'] ) ) ); elseif( isset( $arrValues['resolution_note'] ) ) $this->setResolutionNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['resolution_note'] ) : $arrValues['resolution_note'] );
		if( isset( $arrValues['escalation_note'] ) && $boolDirectSet ) $this->set( 'm_strEscalationNote', trim( stripcslashes( $arrValues['escalation_note'] ) ) ); elseif( isset( $arrValues['escalation_note'] ) ) $this->setEscalationNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['escalation_note'] ) : $arrValues['escalation_note'] );
		if( isset( $arrValues['reminder_datetime'] ) && $boolDirectSet ) $this->set( 'm_strReminderDatetime', trim( $arrValues['reminder_datetime'] ) ); elseif( isset( $arrValues['reminder_datetime'] ) ) $this->setReminderDatetime( $arrValues['reminder_datetime'] );
		if( isset( $arrValues['is_bill_process_escalation'] ) && $boolDirectSet ) $this->set( 'm_boolIsBillProcessEscalation', trim( stripcslashes( $arrValues['is_bill_process_escalation'] ) ) ); elseif( isset( $arrValues['is_bill_process_escalation'] ) ) $this->setIsBillProcessEscalation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_bill_process_escalation'] ) : $arrValues['is_bill_process_escalation'] );
		if( isset( $arrValues['escalation_completed_by'] ) && $boolDirectSet ) $this->set( 'm_intEscalationCompletedBy', trim( $arrValues['escalation_completed_by'] ) ); elseif( isset( $arrValues['escalation_completed_by'] ) ) $this->setEscalationCompletedBy( $arrValues['escalation_completed_by'] );
		if( isset( $arrValues['escalation_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strEscalationCompletedOn', trim( $arrValues['escalation_completed_on'] ) ); elseif( isset( $arrValues['escalation_completed_on'] ) ) $this->setEscalationCompletedOn( $arrValues['escalation_completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setBillEscalateTypeId( $intBillEscalateTypeId ) {
		$this->set( 'm_intBillEscalateTypeId', CStrings::strToIntDef( $intBillEscalateTypeId, NULL, false ) );
	}

	public function getBillEscalateTypeId() {
		return $this->m_intBillEscalateTypeId;
	}

	public function sqlBillEscalateTypeId() {
		return ( true == isset( $this->m_intBillEscalateTypeId ) ) ? ( string ) $this->m_intBillEscalateTypeId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setUemBillStatusTypeId( $intUemBillStatusTypeId ) {
		$this->set( 'm_intUemBillStatusTypeId', CStrings::strToIntDef( $intUemBillStatusTypeId, NULL, false ) );
	}

	public function getUemBillStatusTypeId() {
		return $this->m_intUemBillStatusTypeId;
	}

	public function sqlUemBillStatusTypeId() {
		return ( true == isset( $this->m_intUemBillStatusTypeId ) ) ? ( string ) $this->m_intUemBillStatusTypeId : '1';
	}

	public function setResolutionEscalateStepId( $intResolutionEscalateStepId ) {
		$this->set( 'm_intResolutionEscalateStepId', CStrings::strToIntDef( $intResolutionEscalateStepId, NULL, false ) );
	}

	public function getResolutionEscalateStepId() {
		return $this->m_intResolutionEscalateStepId;
	}

	public function sqlResolutionEscalateStepId() {
		return ( true == isset( $this->m_intResolutionEscalateStepId ) ) ? ( string ) $this->m_intResolutionEscalateStepId : 'NULL';
	}

	public function setEscalationDatetime( $strEscalationDatetime ) {
		$this->set( 'm_strEscalationDatetime', CStrings::strTrimDef( $strEscalationDatetime, -1, NULL, true ) );
	}

	public function getEscalationDatetime() {
		return $this->m_strEscalationDatetime;
	}

	public function sqlEscalationDatetime() {
		return ( true == isset( $this->m_strEscalationDatetime ) ) ? '\'' . $this->m_strEscalationDatetime . '\'' : 'NULL';
	}

	public function setEscalatedEmailAddress( $strEscalatedEmailAddress ) {
		$this->set( 'm_strEscalatedEmailAddress', CStrings::strTrimDef( $strEscalatedEmailAddress, 200, NULL, true ) );
	}

	public function getEscalatedEmailAddress() {
		return $this->m_strEscalatedEmailAddress;
	}

	public function sqlEscalatedEmailAddress() {
		return ( true == isset( $this->m_strEscalatedEmailAddress ) ) ? '\'' . addslashes( $this->m_strEscalatedEmailAddress ) . '\'' : 'NULL';
	}

	public function setResolutionNote( $strResolutionNote ) {
		$this->set( 'm_strResolutionNote', CStrings::strTrimDef( $strResolutionNote, -1, NULL, true ) );
	}

	public function getResolutionNote() {
		return $this->m_strResolutionNote;
	}

	public function sqlResolutionNote() {
		return ( true == isset( $this->m_strResolutionNote ) ) ? '\'' . addslashes( $this->m_strResolutionNote ) . '\'' : 'NULL';
	}

	public function setEscalationNote( $strEscalationNote ) {
		$this->set( 'm_strEscalationNote', CStrings::strTrimDef( $strEscalationNote, -1, NULL, true ) );
	}

	public function getEscalationNote() {
		return $this->m_strEscalationNote;
	}

	public function sqlEscalationNote() {
		return ( true == isset( $this->m_strEscalationNote ) ) ? '\'' . addslashes( $this->m_strEscalationNote ) . '\'' : 'NULL';
	}

	public function setReminderDatetime( $strReminderDatetime ) {
		$this->set( 'm_strReminderDatetime', CStrings::strTrimDef( $strReminderDatetime, -1, NULL, true ) );
	}

	public function getReminderDatetime() {
		return $this->m_strReminderDatetime;
	}

	public function sqlReminderDatetime() {
		return ( true == isset( $this->m_strReminderDatetime ) ) ? '\'' . $this->m_strReminderDatetime . '\'' : 'NULL';
	}

	public function setIsBillProcessEscalation( $boolIsBillProcessEscalation ) {
		$this->set( 'm_boolIsBillProcessEscalation', CStrings::strToBool( $boolIsBillProcessEscalation ) );
	}

	public function getIsBillProcessEscalation() {
		return $this->m_boolIsBillProcessEscalation;
	}

	public function sqlIsBillProcessEscalation() {
		return ( true == isset( $this->m_boolIsBillProcessEscalation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBillProcessEscalation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEscalationCompletedBy( $intEscalationCompletedBy ) {
		$this->set( 'm_intEscalationCompletedBy', CStrings::strToIntDef( $intEscalationCompletedBy, NULL, false ) );
	}

	public function getEscalationCompletedBy() {
		return $this->m_intEscalationCompletedBy;
	}

	public function sqlEscalationCompletedBy() {
		return ( true == isset( $this->m_intEscalationCompletedBy ) ) ? ( string ) $this->m_intEscalationCompletedBy : 'NULL';
	}

	public function setEscalationCompletedOn( $strEscalationCompletedOn ) {
		$this->set( 'm_strEscalationCompletedOn', CStrings::strTrimDef( $strEscalationCompletedOn, -1, NULL, true ) );
	}

	public function getEscalationCompletedOn() {
		return $this->m_strEscalationCompletedOn;
	}

	public function sqlEscalationCompletedOn() {
		return ( true == isset( $this->m_strEscalationCompletedOn ) ) ? '\'' . $this->m_strEscalationCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, bill_escalate_type_id, utility_bill_id, uem_bill_status_type_id, resolution_escalate_step_id, escalation_datetime, escalated_email_address, resolution_note, escalation_note, reminder_datetime, is_bill_process_escalation, escalation_completed_by, escalation_completed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlBillEscalateTypeId() . ', ' .
 						$this->sqlUtilityBillId() . ', ' .
 						$this->sqlUemBillStatusTypeId() . ', ' .
 						$this->sqlResolutionEscalateStepId() . ', ' .
 						$this->sqlEscalationDatetime() . ', ' .
 						$this->sqlEscalatedEmailAddress() . ', ' .
 						$this->sqlResolutionNote() . ', ' .
 						$this->sqlEscalationNote() . ', ' .
 						$this->sqlReminderDatetime() . ', ' .
 						$this->sqlIsBillProcessEscalation() . ', ' .
 						$this->sqlEscalationCompletedBy() . ', ' .
 						$this->sqlEscalationCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_escalate_type_id = ' . $this->sqlBillEscalateTypeId() . ','; } elseif( true == array_key_exists( 'BillEscalateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bill_escalate_type_id = ' . $this->sqlBillEscalateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uem_bill_status_type_id = ' . $this->sqlUemBillStatusTypeId() . ','; } elseif( true == array_key_exists( 'UemBillStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' uem_bill_status_type_id = ' . $this->sqlUemBillStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolution_escalate_step_id = ' . $this->sqlResolutionEscalateStepId() . ','; } elseif( true == array_key_exists( 'ResolutionEscalateStepId', $this->getChangedColumns() ) ) { $strSql .= ' resolution_escalate_step_id = ' . $this->sqlResolutionEscalateStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalation_datetime = ' . $this->sqlEscalationDatetime() . ','; } elseif( true == array_key_exists( 'EscalationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' escalation_datetime = ' . $this->sqlEscalationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_email_address = ' . $this->sqlEscalatedEmailAddress() . ','; } elseif( true == array_key_exists( 'EscalatedEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' escalated_email_address = ' . $this->sqlEscalatedEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolution_note = ' . $this->sqlResolutionNote() . ','; } elseif( true == array_key_exists( 'ResolutionNote', $this->getChangedColumns() ) ) { $strSql .= ' resolution_note = ' . $this->sqlResolutionNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalation_note = ' . $this->sqlEscalationNote() . ','; } elseif( true == array_key_exists( 'EscalationNote', $this->getChangedColumns() ) ) { $strSql .= ' escalation_note = ' . $this->sqlEscalationNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reminder_datetime = ' . $this->sqlReminderDatetime() . ','; } elseif( true == array_key_exists( 'ReminderDatetime', $this->getChangedColumns() ) ) { $strSql .= ' reminder_datetime = ' . $this->sqlReminderDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_bill_process_escalation = ' . $this->sqlIsBillProcessEscalation() . ','; } elseif( true == array_key_exists( 'IsBillProcessEscalation', $this->getChangedColumns() ) ) { $strSql .= ' is_bill_process_escalation = ' . $this->sqlIsBillProcessEscalation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalation_completed_by = ' . $this->sqlEscalationCompletedBy() . ','; } elseif( true == array_key_exists( 'EscalationCompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' escalation_completed_by = ' . $this->sqlEscalationCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalation_completed_on = ' . $this->sqlEscalationCompletedOn() . ','; } elseif( true == array_key_exists( 'EscalationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' escalation_completed_on = ' . $this->sqlEscalationCompletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'bill_escalate_type_id' => $this->getBillEscalateTypeId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'uem_bill_status_type_id' => $this->getUemBillStatusTypeId(),
			'resolution_escalate_step_id' => $this->getResolutionEscalateStepId(),
			'escalation_datetime' => $this->getEscalationDatetime(),
			'escalated_email_address' => $this->getEscalatedEmailAddress(),
			'resolution_note' => $this->getResolutionNote(),
			'escalation_note' => $this->getEscalationNote(),
			'reminder_datetime' => $this->getReminderDatetime(),
			'is_bill_process_escalation' => $this->getIsBillProcessEscalation(),
			'escalation_completed_by' => $this->getEscalationCompletedBy(),
			'escalation_completed_on' => $this->getEscalationCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>