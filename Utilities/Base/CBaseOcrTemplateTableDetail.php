<?php

class CBaseOcrTemplateTableDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.ocr_template_table_details';

	protected $m_intId;
	protected $m_intOcrTemplateId;
	protected $m_intOcrTemplateLabelId;
	protected $m_intTableNumber;
	protected $m_intColumnNumber;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ocr_template_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrTemplateId', trim( $arrValues['ocr_template_id'] ) ); elseif( isset( $arrValues['ocr_template_id'] ) ) $this->setOcrTemplateId( $arrValues['ocr_template_id'] );
		if( isset( $arrValues['ocr_template_label_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrTemplateLabelId', trim( $arrValues['ocr_template_label_id'] ) ); elseif( isset( $arrValues['ocr_template_label_id'] ) ) $this->setOcrTemplateLabelId( $arrValues['ocr_template_label_id'] );
		if( isset( $arrValues['table_number'] ) && $boolDirectSet ) $this->set( 'm_intTableNumber', trim( $arrValues['table_number'] ) ); elseif( isset( $arrValues['table_number'] ) ) $this->setTableNumber( $arrValues['table_number'] );
		if( isset( $arrValues['column_number'] ) && $boolDirectSet ) $this->set( 'm_intColumnNumber', trim( $arrValues['column_number'] ) ); elseif( isset( $arrValues['column_number'] ) ) $this->setColumnNumber( $arrValues['column_number'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOcrTemplateId( $intOcrTemplateId ) {
		$this->set( 'm_intOcrTemplateId', CStrings::strToIntDef( $intOcrTemplateId, NULL, false ) );
	}

	public function getOcrTemplateId() {
		return $this->m_intOcrTemplateId;
	}

	public function sqlOcrTemplateId() {
		return ( true == isset( $this->m_intOcrTemplateId ) ) ? ( string ) $this->m_intOcrTemplateId : 'NULL';
	}

	public function setOcrTemplateLabelId( $intOcrTemplateLabelId ) {
		$this->set( 'm_intOcrTemplateLabelId', CStrings::strToIntDef( $intOcrTemplateLabelId, NULL, false ) );
	}

	public function getOcrTemplateLabelId() {
		return $this->m_intOcrTemplateLabelId;
	}

	public function sqlOcrTemplateLabelId() {
		return ( true == isset( $this->m_intOcrTemplateLabelId ) ) ? ( string ) $this->m_intOcrTemplateLabelId : 'NULL';
	}

	public function setTableNumber( $intTableNumber ) {
		$this->set( 'm_intTableNumber', CStrings::strToIntDef( $intTableNumber, NULL, false ) );
	}

	public function getTableNumber() {
		return $this->m_intTableNumber;
	}

	public function sqlTableNumber() {
		return ( true == isset( $this->m_intTableNumber ) ) ? ( string ) $this->m_intTableNumber : 'NULL';
	}

	public function setColumnNumber( $intColumnNumber ) {
		$this->set( 'm_intColumnNumber', CStrings::strToIntDef( $intColumnNumber, NULL, false ) );
	}

	public function getColumnNumber() {
		return $this->m_intColumnNumber;
	}

	public function sqlColumnNumber() {
		return ( true == isset( $this->m_intColumnNumber ) ) ? ( string ) $this->m_intColumnNumber : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ocr_template_id, ocr_template_label_id, table_number, column_number, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlOcrTemplateId() . ', ' .
						$this->sqlOcrTemplateLabelId() . ', ' .
						$this->sqlTableNumber() . ', ' .
						$this->sqlColumnNumber() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_template_id = ' . $this->sqlOcrTemplateId(). ',' ; } elseif( true == array_key_exists( 'OcrTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_template_id = ' . $this->sqlOcrTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_template_label_id = ' . $this->sqlOcrTemplateLabelId(). ',' ; } elseif( true == array_key_exists( 'OcrTemplateLabelId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_template_label_id = ' . $this->sqlOcrTemplateLabelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_number = ' . $this->sqlTableNumber(). ',' ; } elseif( true == array_key_exists( 'TableNumber', $this->getChangedColumns() ) ) { $strSql .= ' table_number = ' . $this->sqlTableNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' column_number = ' . $this->sqlColumnNumber(). ',' ; } elseif( true == array_key_exists( 'ColumnNumber', $this->getChangedColumns() ) ) { $strSql .= ' column_number = ' . $this->sqlColumnNumber() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ocr_template_id' => $this->getOcrTemplateId(),
			'ocr_template_label_id' => $this->getOcrTemplateLabelId(),
			'table_number' => $this->getTableNumber(),
			'column_number' => $this->getColumnNumber(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>