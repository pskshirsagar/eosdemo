<?php

class CBaseVendorInvitation extends CEosSingularBase {

	const TABLE_NAME = 'public.vendor_invitations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intApLegalEntityId;
	protected $m_intCompanyEmployeeId;
	protected $m_intApPayeeContactId;
	protected $m_intVendorInvitationId;
	protected $m_strRecipientName;
	protected $m_strRecipientEmailAddress;
	protected $m_strTaxNumberEncrypted;
	protected $m_strInvitationDatetime;
	protected $m_boolIsSynced;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strTaxNumberHash;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsSynced = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_legal_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intApLegalEntityId', trim( $arrValues['ap_legal_entity_id'] ) ); elseif( isset( $arrValues['ap_legal_entity_id'] ) ) $this->setApLegalEntityId( $arrValues['ap_legal_entity_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['ap_payee_contact_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeContactId', trim( $arrValues['ap_payee_contact_id'] ) ); elseif( isset( $arrValues['ap_payee_contact_id'] ) ) $this->setApPayeeContactId( $arrValues['ap_payee_contact_id'] );
		if( isset( $arrValues['vendor_invitation_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorInvitationId', trim( $arrValues['vendor_invitation_id'] ) ); elseif( isset( $arrValues['vendor_invitation_id'] ) ) $this->setVendorInvitationId( $arrValues['vendor_invitation_id'] );
		if( isset( $arrValues['recipient_name'] ) && $boolDirectSet ) $this->set( 'm_strRecipientName', trim( stripcslashes( $arrValues['recipient_name'] ) ) ); elseif( isset( $arrValues['recipient_name'] ) ) $this->setRecipientName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['recipient_name'] ) : $arrValues['recipient_name'] );
		if( isset( $arrValues['recipient_email_address'] ) && $boolDirectSet ) $this->set( 'm_strRecipientEmailAddress', trim( stripcslashes( $arrValues['recipient_email_address'] ) ) ); elseif( isset( $arrValues['recipient_email_address'] ) ) $this->setRecipientEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['recipient_email_address'] ) : $arrValues['recipient_email_address'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( stripcslashes( $arrValues['tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_encrypted'] ) : $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['invitation_datetime'] ) && $boolDirectSet ) $this->set( 'm_strInvitationDatetime', trim( $arrValues['invitation_datetime'] ) ); elseif( isset( $arrValues['invitation_datetime'] ) ) $this->setInvitationDatetime( $arrValues['invitation_datetime'] );
		if( isset( $arrValues['is_synced'] ) && $boolDirectSet ) $this->set( 'm_boolIsSynced', trim( stripcslashes( $arrValues['is_synced'] ) ) ); elseif( isset( $arrValues['is_synced'] ) ) $this->setIsSynced( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_synced'] ) : $arrValues['is_synced'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['tax_number_hash'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberHash', trim( stripcslashes( $arrValues['tax_number_hash'] ) ) ); elseif( isset( $arrValues['tax_number_hash'] ) ) $this->setTaxNumberHash( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_hash'] ) : $arrValues['tax_number_hash'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApLegalEntityId( $intApLegalEntityId ) {
		$this->set( 'm_intApLegalEntityId', CStrings::strToIntDef( $intApLegalEntityId, NULL, false ) );
	}

	public function getApLegalEntityId() {
		return $this->m_intApLegalEntityId;
	}

	public function sqlApLegalEntityId() {
		return ( true == isset( $this->m_intApLegalEntityId ) ) ? ( string ) $this->m_intApLegalEntityId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setApPayeeContactId( $intApPayeeContactId ) {
		$this->set( 'm_intApPayeeContactId', CStrings::strToIntDef( $intApPayeeContactId, NULL, false ) );
	}

	public function getApPayeeContactId() {
		return $this->m_intApPayeeContactId;
	}

	public function sqlApPayeeContactId() {
		return ( true == isset( $this->m_intApPayeeContactId ) ) ? ( string ) $this->m_intApPayeeContactId : 'NULL';
	}

	public function setVendorInvitationId( $intVendorInvitationId ) {
		$this->set( 'm_intVendorInvitationId', CStrings::strToIntDef( $intVendorInvitationId, NULL, false ) );
	}

	public function getVendorInvitationId() {
		return $this->m_intVendorInvitationId;
	}

	public function sqlVendorInvitationId() {
		return ( true == isset( $this->m_intVendorInvitationId ) ) ? ( string ) $this->m_intVendorInvitationId : 'NULL';
	}

	public function setRecipientName( $strRecipientName ) {
		$this->set( 'm_strRecipientName', CStrings::strTrimDef( $strRecipientName, 240, NULL, true ) );
	}

	public function getRecipientName() {
		return $this->m_strRecipientName;
	}

	public function sqlRecipientName() {
		return ( true == isset( $this->m_strRecipientName ) ) ? '\'' . addslashes( $this->m_strRecipientName ) . '\'' : 'NULL';
	}

	public function setRecipientEmailAddress( $strRecipientEmailAddress ) {
		$this->set( 'm_strRecipientEmailAddress', CStrings::strTrimDef( $strRecipientEmailAddress, 240, NULL, true ) );
	}

	public function getRecipientEmailAddress() {
		return $this->m_strRecipientEmailAddress;
	}

	public function sqlRecipientEmailAddress() {
		return ( true == isset( $this->m_strRecipientEmailAddress ) ) ? '\'' . addslashes( $this->m_strRecipientEmailAddress ) . '\'' : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setInvitationDatetime( $strInvitationDatetime ) {
		$this->set( 'm_strInvitationDatetime', CStrings::strTrimDef( $strInvitationDatetime, -1, NULL, true ) );
	}

	public function getInvitationDatetime() {
		return $this->m_strInvitationDatetime;
	}

	public function sqlInvitationDatetime() {
		return ( true == isset( $this->m_strInvitationDatetime ) ) ? '\'' . $this->m_strInvitationDatetime . '\'' : 'NOW()';
	}

	public function setIsSynced( $boolIsSynced ) {
		$this->set( 'm_boolIsSynced', CStrings::strToBool( $boolIsSynced ) );
	}

	public function getIsSynced() {
		return $this->m_boolIsSynced;
	}

	public function sqlIsSynced() {
		return ( true == isset( $this->m_boolIsSynced ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSynced ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTaxNumberHash( $strTaxNumberHash ) {
		$this->set( 'm_strTaxNumberHash', CStrings::strTrimDef( $strTaxNumberHash, 240, NULL, true ) );
	}

	public function getTaxNumberHash() {
		return $this->m_strTaxNumberHash;
	}

	public function sqlTaxNumberHash() {
		return ( true == isset( $this->m_strTaxNumberHash ) ) ? '\'' . addslashes( $this->m_strTaxNumberHash ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, ap_legal_entity_id, company_employee_id, ap_payee_contact_id, vendor_invitation_id, recipient_name, recipient_email_address, tax_number_encrypted, invitation_datetime, is_synced, updated_by, updated_on, created_by, created_on, tax_number_hash )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlApLegalEntityId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlApPayeeContactId() . ', ' .
						$this->sqlVendorInvitationId() . ', ' .
						$this->sqlRecipientName() . ', ' .
						$this->sqlRecipientEmailAddress() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlInvitationDatetime() . ', ' .
						$this->sqlIsSynced() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlTaxNumberHash() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_legal_entity_id = ' . $this->sqlApLegalEntityId(). ',' ; } elseif( true == array_key_exists( 'ApLegalEntityId', $this->getChangedColumns() ) ) { $strSql .= ' ap_legal_entity_id = ' . $this->sqlApLegalEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_contact_id = ' . $this->sqlApPayeeContactId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeContactId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_contact_id = ' . $this->sqlApPayeeContactId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_invitation_id = ' . $this->sqlVendorInvitationId(). ',' ; } elseif( true == array_key_exists( 'VendorInvitationId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_invitation_id = ' . $this->sqlVendorInvitationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recipient_name = ' . $this->sqlRecipientName(). ',' ; } elseif( true == array_key_exists( 'RecipientName', $this->getChangedColumns() ) ) { $strSql .= ' recipient_name = ' . $this->sqlRecipientName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recipient_email_address = ' . $this->sqlRecipientEmailAddress(). ',' ; } elseif( true == array_key_exists( 'RecipientEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' recipient_email_address = ' . $this->sqlRecipientEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invitation_datetime = ' . $this->sqlInvitationDatetime(). ',' ; } elseif( true == array_key_exists( 'InvitationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' invitation_datetime = ' . $this->sqlInvitationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_synced = ' . $this->sqlIsSynced(). ',' ; } elseif( true == array_key_exists( 'IsSynced', $this->getChangedColumns() ) ) { $strSql .= ' is_synced = ' . $this->sqlIsSynced() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_hash = ' . $this->sqlTaxNumberHash(). ',' ; } elseif( true == array_key_exists( 'TaxNumberHash', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_hash = ' . $this->sqlTaxNumberHash() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_legal_entity_id' => $this->getApLegalEntityId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'ap_payee_contact_id' => $this->getApPayeeContactId(),
			'vendor_invitation_id' => $this->getVendorInvitationId(),
			'recipient_name' => $this->getRecipientName(),
			'recipient_email_address' => $this->getRecipientEmailAddress(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'invitation_datetime' => $this->getInvitationDatetime(),
			'is_synced' => $this->getIsSynced(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'tax_number_hash' => $this->getTaxNumberHash()
		);
	}

}
?>