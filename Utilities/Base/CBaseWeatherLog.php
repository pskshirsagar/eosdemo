<?php

class CBaseWeatherLog extends CEosSingularBase {

	const TABLE_NAME = 'public.weather_logs';

	protected $m_intId;
	protected $m_intPostalCode;
	protected $m_strTempDatetime;
	protected $m_strSunriseTime;
	protected $m_strSunsetTime;
	protected $m_fltTemp;
	protected $m_fltTempMin;
	protected $m_fltTempMax;
	protected $m_fltHumidity;
	protected $m_fltPressure;
	protected $m_fltWindSpeed;
	protected $m_strWindName;
	protected $m_strWindDirection;
	protected $m_strClouds;
	protected $m_fltPrecipitation;
	protected $m_strWeatherDescription;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_intPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['temp_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTempDatetime', trim( $arrValues['temp_datetime'] ) ); elseif( isset( $arrValues['temp_datetime'] ) ) $this->setTempDatetime( $arrValues['temp_datetime'] );
		if( isset( $arrValues['sunrise_time'] ) && $boolDirectSet ) $this->set( 'm_strSunriseTime', trim( $arrValues['sunrise_time'] ) ); elseif( isset( $arrValues['sunrise_time'] ) ) $this->setSunriseTime( $arrValues['sunrise_time'] );
		if( isset( $arrValues['sunset_time'] ) && $boolDirectSet ) $this->set( 'm_strSunsetTime', trim( $arrValues['sunset_time'] ) ); elseif( isset( $arrValues['sunset_time'] ) ) $this->setSunsetTime( $arrValues['sunset_time'] );
		if( isset( $arrValues['temp'] ) && $boolDirectSet ) $this->set( 'm_fltTemp', trim( $arrValues['temp'] ) ); elseif( isset( $arrValues['temp'] ) ) $this->setTemp( $arrValues['temp'] );
		if( isset( $arrValues['temp_min'] ) && $boolDirectSet ) $this->set( 'm_fltTempMin', trim( $arrValues['temp_min'] ) ); elseif( isset( $arrValues['temp_min'] ) ) $this->setTempMin( $arrValues['temp_min'] );
		if( isset( $arrValues['temp_max'] ) && $boolDirectSet ) $this->set( 'm_fltTempMax', trim( $arrValues['temp_max'] ) ); elseif( isset( $arrValues['temp_max'] ) ) $this->setTempMax( $arrValues['temp_max'] );
		if( isset( $arrValues['humidity'] ) && $boolDirectSet ) $this->set( 'm_fltHumidity', trim( $arrValues['humidity'] ) ); elseif( isset( $arrValues['humidity'] ) ) $this->setHumidity( $arrValues['humidity'] );
		if( isset( $arrValues['pressure'] ) && $boolDirectSet ) $this->set( 'm_fltPressure', trim( $arrValues['pressure'] ) ); elseif( isset( $arrValues['pressure'] ) ) $this->setPressure( $arrValues['pressure'] );
		if( isset( $arrValues['wind_speed'] ) && $boolDirectSet ) $this->set( 'm_fltWindSpeed', trim( $arrValues['wind_speed'] ) ); elseif( isset( $arrValues['wind_speed'] ) ) $this->setWindSpeed( $arrValues['wind_speed'] );
		if( isset( $arrValues['wind_name'] ) && $boolDirectSet ) $this->set( 'm_strWindName', trim( stripcslashes( $arrValues['wind_name'] ) ) ); elseif( isset( $arrValues['wind_name'] ) ) $this->setWindName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['wind_name'] ) : $arrValues['wind_name'] );
		if( isset( $arrValues['wind_direction'] ) && $boolDirectSet ) $this->set( 'm_strWindDirection', trim( stripcslashes( $arrValues['wind_direction'] ) ) ); elseif( isset( $arrValues['wind_direction'] ) ) $this->setWindDirection( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['wind_direction'] ) : $arrValues['wind_direction'] );
		if( isset( $arrValues['clouds'] ) && $boolDirectSet ) $this->set( 'm_strClouds', trim( stripcslashes( $arrValues['clouds'] ) ) ); elseif( isset( $arrValues['clouds'] ) ) $this->setClouds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['clouds'] ) : $arrValues['clouds'] );
		if( isset( $arrValues['precipitation'] ) && $boolDirectSet ) $this->set( 'm_fltPrecipitation', trim( $arrValues['precipitation'] ) ); elseif( isset( $arrValues['precipitation'] ) ) $this->setPrecipitation( $arrValues['precipitation'] );
		if( isset( $arrValues['weather_description'] ) && $boolDirectSet ) $this->set( 'm_strWeatherDescription', trim( stripcslashes( $arrValues['weather_description'] ) ) ); elseif( isset( $arrValues['weather_description'] ) ) $this->setWeatherDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['weather_description'] ) : $arrValues['weather_description'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPostalCode( $intPostalCode ) {
		$this->set( 'm_intPostalCode', CStrings::strToIntDef( $intPostalCode, NULL, false ) );
	}

	public function getPostalCode() {
		return $this->m_intPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_intPostalCode ) ) ? ( string ) $this->m_intPostalCode : 'NULL';
	}

	public function setTempDatetime( $strTempDatetime ) {
		$this->set( 'm_strTempDatetime', CStrings::strTrimDef( $strTempDatetime, -1, NULL, true ) );
	}

	public function getTempDatetime() {
		return $this->m_strTempDatetime;
	}

	public function sqlTempDatetime() {
		return ( true == isset( $this->m_strTempDatetime ) ) ? '\'' . $this->m_strTempDatetime . '\'' : 'NOW()';
	}

	public function setSunriseTime( $strSunriseTime ) {
		$this->set( 'm_strSunriseTime', CStrings::strTrimDef( $strSunriseTime, -1, NULL, true ) );
	}

	public function getSunriseTime() {
		return $this->m_strSunriseTime;
	}

	public function sqlSunriseTime() {
		return ( true == isset( $this->m_strSunriseTime ) ) ? '\'' . $this->m_strSunriseTime . '\'' : 'NULL';
	}

	public function setSunsetTime( $strSunsetTime ) {
		$this->set( 'm_strSunsetTime', CStrings::strTrimDef( $strSunsetTime, -1, NULL, true ) );
	}

	public function getSunsetTime() {
		return $this->m_strSunsetTime;
	}

	public function sqlSunsetTime() {
		return ( true == isset( $this->m_strSunsetTime ) ) ? '\'' . $this->m_strSunsetTime . '\'' : 'NULL';
	}

	public function setTemp( $fltTemp ) {
		$this->set( 'm_fltTemp', CStrings::strToFloatDef( $fltTemp, NULL, false, 5 ) );
	}

	public function getTemp() {
		return $this->m_fltTemp;
	}

	public function sqlTemp() {
		return ( true == isset( $this->m_fltTemp ) ) ? ( string ) $this->m_fltTemp : 'NULL';
	}

	public function setTempMin( $fltTempMin ) {
		$this->set( 'm_fltTempMin', CStrings::strToFloatDef( $fltTempMin, NULL, false, 5 ) );
	}

	public function getTempMin() {
		return $this->m_fltTempMin;
	}

	public function sqlTempMin() {
		return ( true == isset( $this->m_fltTempMin ) ) ? ( string ) $this->m_fltTempMin : 'NULL';
	}

	public function setTempMax( $fltTempMax ) {
		$this->set( 'm_fltTempMax', CStrings::strToFloatDef( $fltTempMax, NULL, false, 5 ) );
	}

	public function getTempMax() {
		return $this->m_fltTempMax;
	}

	public function sqlTempMax() {
		return ( true == isset( $this->m_fltTempMax ) ) ? ( string ) $this->m_fltTempMax : 'NULL';
	}

	public function setHumidity( $fltHumidity ) {
		$this->set( 'm_fltHumidity', CStrings::strToFloatDef( $fltHumidity, NULL, false, 5 ) );
	}

	public function getHumidity() {
		return $this->m_fltHumidity;
	}

	public function sqlHumidity() {
		return ( true == isset( $this->m_fltHumidity ) ) ? ( string ) $this->m_fltHumidity : 'NULL';
	}

	public function setPressure( $fltPressure ) {
		$this->set( 'm_fltPressure', CStrings::strToFloatDef( $fltPressure, NULL, false, 5 ) );
	}

	public function getPressure() {
		return $this->m_fltPressure;
	}

	public function sqlPressure() {
		return ( true == isset( $this->m_fltPressure ) ) ? ( string ) $this->m_fltPressure : 'NULL';
	}

	public function setWindSpeed( $fltWindSpeed ) {
		$this->set( 'm_fltWindSpeed', CStrings::strToFloatDef( $fltWindSpeed, NULL, false, 5 ) );
	}

	public function getWindSpeed() {
		return $this->m_fltWindSpeed;
	}

	public function sqlWindSpeed() {
		return ( true == isset( $this->m_fltWindSpeed ) ) ? ( string ) $this->m_fltWindSpeed : 'NULL';
	}

	public function setWindName( $strWindName ) {
		$this->set( 'm_strWindName', CStrings::strTrimDef( $strWindName, 100, NULL, true ) );
	}

	public function getWindName() {
		return $this->m_strWindName;
	}

	public function sqlWindName() {
		return ( true == isset( $this->m_strWindName ) ) ? '\'' . addslashes( $this->m_strWindName ) . '\'' : 'NULL';
	}

	public function setWindDirection( $strWindDirection ) {
		$this->set( 'm_strWindDirection', CStrings::strTrimDef( $strWindDirection, 100, NULL, true ) );
	}

	public function getWindDirection() {
		return $this->m_strWindDirection;
	}

	public function sqlWindDirection() {
		return ( true == isset( $this->m_strWindDirection ) ) ? '\'' . addslashes( $this->m_strWindDirection ) . '\'' : 'NULL';
	}

	public function setClouds( $strClouds ) {
		$this->set( 'm_strClouds', CStrings::strTrimDef( $strClouds, 100, NULL, true ) );
	}

	public function getClouds() {
		return $this->m_strClouds;
	}

	public function sqlClouds() {
		return ( true == isset( $this->m_strClouds ) ) ? '\'' . addslashes( $this->m_strClouds ) . '\'' : 'NULL';
	}

	public function setPrecipitation( $fltPrecipitation ) {
		$this->set( 'm_fltPrecipitation', CStrings::strToFloatDef( $fltPrecipitation, NULL, false, 5 ) );
	}

	public function getPrecipitation() {
		return $this->m_fltPrecipitation;
	}

	public function sqlPrecipitation() {
		return ( true == isset( $this->m_fltPrecipitation ) ) ? ( string ) $this->m_fltPrecipitation : 'NULL';
	}

	public function setWeatherDescription( $strWeatherDescription ) {
		$this->set( 'm_strWeatherDescription', CStrings::strTrimDef( $strWeatherDescription, -1, NULL, true ) );
	}

	public function getWeatherDescription() {
		return $this->m_strWeatherDescription;
	}

	public function sqlWeatherDescription() {
		return ( true == isset( $this->m_strWeatherDescription ) ) ? '\'' . addslashes( $this->m_strWeatherDescription ) . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, postal_code, temp_datetime, sunrise_time, sunset_time, temp, temp_min, temp_max, humidity, pressure, wind_speed, wind_name, wind_direction, clouds, precipitation, weather_description, updated_on, updated_by, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlTempDatetime() . ', ' .
 						$this->sqlSunriseTime() . ', ' .
 						$this->sqlSunsetTime() . ', ' .
 						$this->sqlTemp() . ', ' .
 						$this->sqlTempMin() . ', ' .
 						$this->sqlTempMax() . ', ' .
 						$this->sqlHumidity() . ', ' .
 						$this->sqlPressure() . ', ' .
 						$this->sqlWindSpeed() . ', ' .
 						$this->sqlWindName() . ', ' .
 						$this->sqlWindDirection() . ', ' .
 						$this->sqlClouds() . ', ' .
 						$this->sqlPrecipitation() . ', ' .
 						$this->sqlWeatherDescription() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' temp_datetime = ' . $this->sqlTempDatetime() . ','; } elseif( true == array_key_exists( 'TempDatetime', $this->getChangedColumns() ) ) { $strSql .= ' temp_datetime = ' . $this->sqlTempDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sunrise_time = ' . $this->sqlSunriseTime() . ','; } elseif( true == array_key_exists( 'SunriseTime', $this->getChangedColumns() ) ) { $strSql .= ' sunrise_time = ' . $this->sqlSunriseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sunset_time = ' . $this->sqlSunsetTime() . ','; } elseif( true == array_key_exists( 'SunsetTime', $this->getChangedColumns() ) ) { $strSql .= ' sunset_time = ' . $this->sqlSunsetTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' temp = ' . $this->sqlTemp() . ','; } elseif( true == array_key_exists( 'Temp', $this->getChangedColumns() ) ) { $strSql .= ' temp = ' . $this->sqlTemp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' temp_min = ' . $this->sqlTempMin() . ','; } elseif( true == array_key_exists( 'TempMin', $this->getChangedColumns() ) ) { $strSql .= ' temp_min = ' . $this->sqlTempMin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' temp_max = ' . $this->sqlTempMax() . ','; } elseif( true == array_key_exists( 'TempMax', $this->getChangedColumns() ) ) { $strSql .= ' temp_max = ' . $this->sqlTempMax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' humidity = ' . $this->sqlHumidity() . ','; } elseif( true == array_key_exists( 'Humidity', $this->getChangedColumns() ) ) { $strSql .= ' humidity = ' . $this->sqlHumidity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pressure = ' . $this->sqlPressure() . ','; } elseif( true == array_key_exists( 'Pressure', $this->getChangedColumns() ) ) { $strSql .= ' pressure = ' . $this->sqlPressure() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wind_speed = ' . $this->sqlWindSpeed() . ','; } elseif( true == array_key_exists( 'WindSpeed', $this->getChangedColumns() ) ) { $strSql .= ' wind_speed = ' . $this->sqlWindSpeed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wind_name = ' . $this->sqlWindName() . ','; } elseif( true == array_key_exists( 'WindName', $this->getChangedColumns() ) ) { $strSql .= ' wind_name = ' . $this->sqlWindName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wind_direction = ' . $this->sqlWindDirection() . ','; } elseif( true == array_key_exists( 'WindDirection', $this->getChangedColumns() ) ) { $strSql .= ' wind_direction = ' . $this->sqlWindDirection() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clouds = ' . $this->sqlClouds() . ','; } elseif( true == array_key_exists( 'Clouds', $this->getChangedColumns() ) ) { $strSql .= ' clouds = ' . $this->sqlClouds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' precipitation = ' . $this->sqlPrecipitation() . ','; } elseif( true == array_key_exists( 'Precipitation', $this->getChangedColumns() ) ) { $strSql .= ' precipitation = ' . $this->sqlPrecipitation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' weather_description = ' . $this->sqlWeatherDescription() . ','; } elseif( true == array_key_exists( 'WeatherDescription', $this->getChangedColumns() ) ) { $strSql .= ' weather_description = ' . $this->sqlWeatherDescription() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'postal_code' => $this->getPostalCode(),
			'temp_datetime' => $this->getTempDatetime(),
			'sunrise_time' => $this->getSunriseTime(),
			'sunset_time' => $this->getSunsetTime(),
			'temp' => $this->getTemp(),
			'temp_min' => $this->getTempMin(),
			'temp_max' => $this->getTempMax(),
			'humidity' => $this->getHumidity(),
			'pressure' => $this->getPressure(),
			'wind_speed' => $this->getWindSpeed(),
			'wind_name' => $this->getWindName(),
			'wind_direction' => $this->getWindDirection(),
			'clouds' => $this->getClouds(),
			'precipitation' => $this->getPrecipitation(),
			'weather_description' => $this->getWeatherDescription(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>