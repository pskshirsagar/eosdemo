<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUtilityArCode extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_ar_codes';

	protected $m_intWorksId;
	protected $m_intCid;
	protected $m_intGlAccountTypeId;
	protected $m_intModeTypeId;
	protected $m_intArCodeTypeId;
	protected $m_intArOriginId;
	protected $m_intArTriggerTypeId;
	protected $m_intArTriggerId;
	protected $m_intCategorizationTypeId;
	protected $m_intDebitGlAccountId;
	protected $m_intCreditGlAccountId;
	protected $m_intModifiedDebitGlAccountId;
	protected $m_intWriteOffArCodeId;
	protected $m_intDefaultArCodeId;
	protected $m_intArCodeGroupId;
	protected $m_intArCodeSummaryTypeId;
	protected $m_intIntegrationDatabaseId;
	protected $m_intLedgerFilterId;
	protected $m_intRoundTypeId;
	protected $m_intRecoveryArCodeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltDefaultAmount;
	protected $m_intCaptureDelayDays;
	protected $m_boolCaptureOnApplicationApproval;
	protected $m_boolHideCharges;
	protected $m_boolHideCredits;
	protected $m_boolWaiveLateFees;
	protected $m_boolPostToCash;
	protected $m_boolShowMoveOutReminder;
	protected $m_boolIntegratedOnly;
	protected $m_boolDontExport;
	protected $m_boolProrateCharges;
	protected $m_boolIsCategorizationQueued;
	protected $m_boolIsPaymentInKind;
	protected $m_boolIsReserved;
	protected $m_boolIsSystem;
	protected $m_boolIsDisabled;
	protected $m_boolDefaultAmountIsEditable;
	protected $m_boolRequireNote;
	protected $m_intOrderNum;
	protected $m_intPriorityNum;
	protected $m_intCategorizedBy;
	protected $m_strCategorizedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolApplyToFirstMonth;
	protected $m_boolApplyToLastMonth;

	public function __construct() {
		parent::__construct();

		$this->m_intModeTypeId = '1';
		$this->m_intCategorizationTypeId = '3';
		$this->m_fltDefaultAmount = '0';
		$this->m_boolCaptureOnApplicationApproval = false;
		$this->m_boolHideCharges = false;
		$this->m_boolHideCredits = false;
		$this->m_boolWaiveLateFees = false;
		$this->m_boolPostToCash = false;
		$this->m_boolShowMoveOutReminder = false;
		$this->m_boolIntegratedOnly = false;
		$this->m_boolDontExport = false;
		$this->m_boolIsCategorizationQueued = false;
		$this->m_boolIsPaymentInKind = false;
		$this->m_boolIsReserved = false;
		$this->m_boolIsSystem = false;
		$this->m_boolIsDisabled = false;
		$this->m_boolDefaultAmountIsEditable = true;
		$this->m_boolRequireNote = false;
		$this->m_intOrderNum = '0';
		$this->m_intPriorityNum = '0';
		$this->m_boolApplyToFirstMonth = false;
		$this->m_boolApplyToLastMonth = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['works_id'] ) && $boolDirectSet ) $this->set( 'm_intWorksId', trim( $arrValues['works_id'] ) ); elseif( isset( $arrValues['works_id'] ) ) $this->setWorksId( $arrValues['works_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountTypeId', trim( $arrValues['gl_account_type_id'] ) ); elseif( isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrValues['gl_account_type_id'] );
		if( isset( $arrValues['mode_type_id'] ) && $boolDirectSet ) $this->set( 'm_intModeTypeId', trim( $arrValues['mode_type_id'] ) ); elseif( isset( $arrValues['mode_type_id'] ) ) $this->setModeTypeId( $arrValues['mode_type_id'] );
		if( isset( $arrValues['ar_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeTypeId', trim( $arrValues['ar_code_type_id'] ) ); elseif( isset( $arrValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrValues['ar_code_type_id'] );
		if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginId', trim( $arrValues['ar_origin_id'] ) ); elseif( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
		if( isset( $arrValues['ar_trigger_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerTypeId', trim( $arrValues['ar_trigger_type_id'] ) ); elseif( isset( $arrValues['ar_trigger_type_id'] ) ) $this->setArTriggerTypeId( $arrValues['ar_trigger_type_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['categorization_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCategorizationTypeId', trim( $arrValues['categorization_type_id'] ) ); elseif( isset( $arrValues['categorization_type_id'] ) ) $this->setCategorizationTypeId( $arrValues['categorization_type_id'] );
		if( isset( $arrValues['debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDebitGlAccountId', trim( $arrValues['debit_gl_account_id'] ) ); elseif( isset( $arrValues['debit_gl_account_id'] ) ) $this->setDebitGlAccountId( $arrValues['debit_gl_account_id'] );
		if( isset( $arrValues['credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditGlAccountId', trim( $arrValues['credit_gl_account_id'] ) ); elseif( isset( $arrValues['credit_gl_account_id'] ) ) $this->setCreditGlAccountId( $arrValues['credit_gl_account_id'] );
		if( isset( $arrValues['modified_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intModifiedDebitGlAccountId', trim( $arrValues['modified_debit_gl_account_id'] ) ); elseif( isset( $arrValues['modified_debit_gl_account_id'] ) ) $this->setModifiedDebitGlAccountId( $arrValues['modified_debit_gl_account_id'] );
		if( isset( $arrValues['write_off_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intWriteOffArCodeId', trim( $arrValues['write_off_ar_code_id'] ) ); elseif( isset( $arrValues['write_off_ar_code_id'] ) ) $this->setWriteOffArCodeId( $arrValues['write_off_ar_code_id'] );
		if( isset( $arrValues['default_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultArCodeId', trim( $arrValues['default_ar_code_id'] ) ); elseif( isset( $arrValues['default_ar_code_id'] ) ) $this->setDefaultArCodeId( $arrValues['default_ar_code_id'] );
		if( isset( $arrValues['ar_code_group_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeGroupId', trim( $arrValues['ar_code_group_id'] ) ); elseif( isset( $arrValues['ar_code_group_id'] ) ) $this->setArCodeGroupId( $arrValues['ar_code_group_id'] );
		if( isset( $arrValues['ar_code_summary_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeSummaryTypeId', trim( $arrValues['ar_code_summary_type_id'] ) ); elseif( isset( $arrValues['ar_code_summary_type_id'] ) ) $this->setArCodeSummaryTypeId( $arrValues['ar_code_summary_type_id'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['ledger_filter_id'] ) && $boolDirectSet ) $this->set( 'm_intLedgerFilterId', trim( $arrValues['ledger_filter_id'] ) ); elseif( isset( $arrValues['ledger_filter_id'] ) ) $this->setLedgerFilterId( $arrValues['ledger_filter_id'] );
		if( isset( $arrValues['round_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRoundTypeId', trim( $arrValues['round_type_id'] ) ); elseif( isset( $arrValues['round_type_id'] ) ) $this->setRoundTypeId( $arrValues['round_type_id'] );
		if( isset( $arrValues['recovery_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intRecoveryArCodeId', trim( $arrValues['recovery_ar_code_id'] ) ); elseif( isset( $arrValues['recovery_ar_code_id'] ) ) $this->setRecoveryArCodeId( $arrValues['recovery_ar_code_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['default_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDefaultAmount', trim( $arrValues['default_amount'] ) ); elseif( isset( $arrValues['default_amount'] ) ) $this->setDefaultAmount( $arrValues['default_amount'] );
		if( isset( $arrValues['capture_delay_days'] ) && $boolDirectSet ) $this->set( 'm_intCaptureDelayDays', trim( $arrValues['capture_delay_days'] ) ); elseif( isset( $arrValues['capture_delay_days'] ) ) $this->setCaptureDelayDays( $arrValues['capture_delay_days'] );
		if( isset( $arrValues['capture_on_application_approval'] ) && $boolDirectSet ) $this->set( 'm_boolCaptureOnApplicationApproval', trim( stripcslashes( $arrValues['capture_on_application_approval'] ) ) ); elseif( isset( $arrValues['capture_on_application_approval'] ) ) $this->setCaptureOnApplicationApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['capture_on_application_approval'] ) : $arrValues['capture_on_application_approval'] );
		if( isset( $arrValues['hide_charges'] ) && $boolDirectSet ) $this->set( 'm_boolHideCharges', trim( stripcslashes( $arrValues['hide_charges'] ) ) ); elseif( isset( $arrValues['hide_charges'] ) ) $this->setHideCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_charges'] ) : $arrValues['hide_charges'] );
		if( isset( $arrValues['hide_credits'] ) && $boolDirectSet ) $this->set( 'm_boolHideCredits', trim( stripcslashes( $arrValues['hide_credits'] ) ) ); elseif( isset( $arrValues['hide_credits'] ) ) $this->setHideCredits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_credits'] ) : $arrValues['hide_credits'] );
		if( isset( $arrValues['waive_late_fees'] ) && $boolDirectSet ) $this->set( 'm_boolWaiveLateFees', trim( stripcslashes( $arrValues['waive_late_fees'] ) ) ); elseif( isset( $arrValues['waive_late_fees'] ) ) $this->setWaiveLateFees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['waive_late_fees'] ) : $arrValues['waive_late_fees'] );
		if( isset( $arrValues['post_to_cash'] ) && $boolDirectSet ) $this->set( 'm_boolPostToCash', trim( stripcslashes( $arrValues['post_to_cash'] ) ) ); elseif( isset( $arrValues['post_to_cash'] ) ) $this->setPostToCash( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_to_cash'] ) : $arrValues['post_to_cash'] );
		if( isset( $arrValues['show_move_out_reminder'] ) && $boolDirectSet ) $this->set( 'm_boolShowMoveOutReminder', trim( stripcslashes( $arrValues['show_move_out_reminder'] ) ) ); elseif( isset( $arrValues['show_move_out_reminder'] ) ) $this->setShowMoveOutReminder( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_move_out_reminder'] ) : $arrValues['show_move_out_reminder'] );
		if( isset( $arrValues['integrated_only'] ) && $boolDirectSet ) $this->set( 'm_boolIntegratedOnly', trim( stripcslashes( $arrValues['integrated_only'] ) ) ); elseif( isset( $arrValues['integrated_only'] ) ) $this->setIntegratedOnly( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['integrated_only'] ) : $arrValues['integrated_only'] );
		if( isset( $arrValues['dont_export'] ) && $boolDirectSet ) $this->set( 'm_boolDontExport', trim( stripcslashes( $arrValues['dont_export'] ) ) ); elseif( isset( $arrValues['dont_export'] ) ) $this->setDontExport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_export'] ) : $arrValues['dont_export'] );
		if( isset( $arrValues['prorate_charges'] ) && $boolDirectSet ) $this->set( 'm_boolProrateCharges', trim( stripcslashes( $arrValues['prorate_charges'] ) ) ); elseif( isset( $arrValues['prorate_charges'] ) ) $this->setProrateCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['prorate_charges'] ) : $arrValues['prorate_charges'] );
		if( isset( $arrValues['is_categorization_queued'] ) && $boolDirectSet ) $this->set( 'm_boolIsCategorizationQueued', trim( stripcslashes( $arrValues['is_categorization_queued'] ) ) ); elseif( isset( $arrValues['is_categorization_queued'] ) ) $this->setIsCategorizationQueued( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_categorization_queued'] ) : $arrValues['is_categorization_queued'] );
		if( isset( $arrValues['is_payment_in_kind'] ) && $boolDirectSet ) $this->set( 'm_boolIsPaymentInKind', trim( stripcslashes( $arrValues['is_payment_in_kind'] ) ) ); elseif( isset( $arrValues['is_payment_in_kind'] ) ) $this->setIsPaymentInKind( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_payment_in_kind'] ) : $arrValues['is_payment_in_kind'] );
		if( isset( $arrValues['is_reserved'] ) && $boolDirectSet ) $this->set( 'm_boolIsReserved', trim( stripcslashes( $arrValues['is_reserved'] ) ) ); elseif( isset( $arrValues['is_reserved'] ) ) $this->setIsReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reserved'] ) : $arrValues['is_reserved'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['default_amount_is_editable'] ) && $boolDirectSet ) $this->set( 'm_boolDefaultAmountIsEditable', trim( stripcslashes( $arrValues['default_amount_is_editable'] ) ) ); elseif( isset( $arrValues['default_amount_is_editable'] ) ) $this->setDefaultAmountIsEditable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['default_amount_is_editable'] ) : $arrValues['default_amount_is_editable'] );
		if( isset( $arrValues['require_note'] ) && $boolDirectSet ) $this->set( 'm_boolRequireNote', trim( stripcslashes( $arrValues['require_note'] ) ) ); elseif( isset( $arrValues['require_note'] ) ) $this->setRequireNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_note'] ) : $arrValues['require_note'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['priority_num'] ) && $boolDirectSet ) $this->set( 'm_intPriorityNum', trim( $arrValues['priority_num'] ) ); elseif( isset( $arrValues['priority_num'] ) ) $this->setPriorityNum( $arrValues['priority_num'] );
		if( isset( $arrValues['categorized_by'] ) && $boolDirectSet ) $this->set( 'm_intCategorizedBy', trim( $arrValues['categorized_by'] ) ); elseif( isset( $arrValues['categorized_by'] ) ) $this->setCategorizedBy( $arrValues['categorized_by'] );
		if( isset( $arrValues['categorized_on'] ) && $boolDirectSet ) $this->set( 'm_strCategorizedOn', trim( $arrValues['categorized_on'] ) ); elseif( isset( $arrValues['categorized_on'] ) ) $this->setCategorizedOn( $arrValues['categorized_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['apply_to_first_month'] ) && $boolDirectSet ) $this->set( 'm_boolApplyToFirstMonth', trim( stripcslashes( $arrValues['apply_to_first_month'] ) ) ); elseif( isset( $arrValues['apply_to_first_month'] ) ) $this->setApplyToFirstMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['apply_to_first_month'] ) : $arrValues['apply_to_first_month'] );
		if( isset( $arrValues['apply_to_last_month'] ) && $boolDirectSet ) $this->set( 'm_boolApplyToLastMonth', trim( stripcslashes( $arrValues['apply_to_last_month'] ) ) ); elseif( isset( $arrValues['apply_to_last_month'] ) ) $this->setApplyToLastMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['apply_to_last_month'] ) : $arrValues['apply_to_last_month'] );
		$this->m_boolInitialized = true;
	}

	public function setWorksId( $intWorksId ) {
		$this->set( 'm_intWorksId', CStrings::strToIntDef( $intWorksId, NULL, false ) );
	}

	public function getWorksId() {
		return $this->m_intWorksId;
	}

	public function sqlWorksId() {
		return ( true == isset( $this->m_intWorksId ) ) ? ( string ) $this->m_intWorksId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->set( 'm_intGlAccountTypeId', CStrings::strToIntDef( $intGlAccountTypeId, NULL, false ) );
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function sqlGlAccountTypeId() {
		return ( true == isset( $this->m_intGlAccountTypeId ) ) ? ( string ) $this->m_intGlAccountTypeId : 'NULL';
	}

	public function setModeTypeId( $intModeTypeId ) {
		$this->set( 'm_intModeTypeId', CStrings::strToIntDef( $intModeTypeId, NULL, false ) );
	}

	public function getModeTypeId() {
		return $this->m_intModeTypeId;
	}

	public function sqlModeTypeId() {
		return ( true == isset( $this->m_intModeTypeId ) ) ? ( string ) $this->m_intModeTypeId : '1';
	}

	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->set( 'm_intArCodeTypeId', CStrings::strToIntDef( $intArCodeTypeId, NULL, false ) );
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function sqlArCodeTypeId() {
		return ( true == isset( $this->m_intArCodeTypeId ) ) ? ( string ) $this->m_intArCodeTypeId : 'NULL';
	}

	public function setArOriginId( $intArOriginId ) {
		$this->set( 'm_intArOriginId', CStrings::strToIntDef( $intArOriginId, NULL, false ) );
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function sqlArOriginId() {
		return ( true == isset( $this->m_intArOriginId ) ) ? ( string ) $this->m_intArOriginId : 'NULL';
	}

	public function setArTriggerTypeId( $intArTriggerTypeId ) {
		$this->set( 'm_intArTriggerTypeId', CStrings::strToIntDef( $intArTriggerTypeId, NULL, false ) );
	}

	public function getArTriggerTypeId() {
		return $this->m_intArTriggerTypeId;
	}

	public function sqlArTriggerTypeId() {
		return ( true == isset( $this->m_intArTriggerTypeId ) ) ? ( string ) $this->m_intArTriggerTypeId : 'NULL';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setCategorizationTypeId( $intCategorizationTypeId ) {
		$this->set( 'm_intCategorizationTypeId', CStrings::strToIntDef( $intCategorizationTypeId, NULL, false ) );
	}

	public function getCategorizationTypeId() {
		return $this->m_intCategorizationTypeId;
	}

	public function sqlCategorizationTypeId() {
		return ( true == isset( $this->m_intCategorizationTypeId ) ) ? ( string ) $this->m_intCategorizationTypeId : '3';
	}

	public function setDebitGlAccountId( $intDebitGlAccountId ) {
		$this->set( 'm_intDebitGlAccountId', CStrings::strToIntDef( $intDebitGlAccountId, NULL, false ) );
	}

	public function getDebitGlAccountId() {
		return $this->m_intDebitGlAccountId;
	}

	public function sqlDebitGlAccountId() {
		return ( true == isset( $this->m_intDebitGlAccountId ) ) ? ( string ) $this->m_intDebitGlAccountId : 'NULL';
	}

	public function setCreditGlAccountId( $intCreditGlAccountId ) {
		$this->set( 'm_intCreditGlAccountId', CStrings::strToIntDef( $intCreditGlAccountId, NULL, false ) );
	}

	public function getCreditGlAccountId() {
		return $this->m_intCreditGlAccountId;
	}

	public function sqlCreditGlAccountId() {
		return ( true == isset( $this->m_intCreditGlAccountId ) ) ? ( string ) $this->m_intCreditGlAccountId : 'NULL';
	}

	public function setModifiedDebitGlAccountId( $intModifiedDebitGlAccountId ) {
		$this->set( 'm_intModifiedDebitGlAccountId', CStrings::strToIntDef( $intModifiedDebitGlAccountId, NULL, false ) );
	}

	public function getModifiedDebitGlAccountId() {
		return $this->m_intModifiedDebitGlAccountId;
	}

	public function sqlModifiedDebitGlAccountId() {
		return ( true == isset( $this->m_intModifiedDebitGlAccountId ) ) ? ( string ) $this->m_intModifiedDebitGlAccountId : 'NULL';
	}

	public function setWriteOffArCodeId( $intWriteOffArCodeId ) {
		$this->set( 'm_intWriteOffArCodeId', CStrings::strToIntDef( $intWriteOffArCodeId, NULL, false ) );
	}

	public function getWriteOffArCodeId() {
		return $this->m_intWriteOffArCodeId;
	}

	public function sqlWriteOffArCodeId() {
		return ( true == isset( $this->m_intWriteOffArCodeId ) ) ? ( string ) $this->m_intWriteOffArCodeId : 'NULL';
	}

	public function setDefaultArCodeId( $intDefaultArCodeId ) {
		$this->set( 'm_intDefaultArCodeId', CStrings::strToIntDef( $intDefaultArCodeId, NULL, false ) );
	}

	public function getDefaultArCodeId() {
		return $this->m_intDefaultArCodeId;
	}

	public function sqlDefaultArCodeId() {
		return ( true == isset( $this->m_intDefaultArCodeId ) ) ? ( string ) $this->m_intDefaultArCodeId : 'NULL';
	}

	public function setArCodeGroupId( $intArCodeGroupId ) {
		$this->set( 'm_intArCodeGroupId', CStrings::strToIntDef( $intArCodeGroupId, NULL, false ) );
	}

	public function getArCodeGroupId() {
		return $this->m_intArCodeGroupId;
	}

	public function sqlArCodeGroupId() {
		return ( true == isset( $this->m_intArCodeGroupId ) ) ? ( string ) $this->m_intArCodeGroupId : 'NULL';
	}

	public function setArCodeSummaryTypeId( $intArCodeSummaryTypeId ) {
		$this->set( 'm_intArCodeSummaryTypeId', CStrings::strToIntDef( $intArCodeSummaryTypeId, NULL, false ) );
	}

	public function getArCodeSummaryTypeId() {
		return $this->m_intArCodeSummaryTypeId;
	}

	public function sqlArCodeSummaryTypeId() {
		return ( true == isset( $this->m_intArCodeSummaryTypeId ) ) ? ( string ) $this->m_intArCodeSummaryTypeId : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setLedgerFilterId( $intLedgerFilterId ) {
		$this->set( 'm_intLedgerFilterId', CStrings::strToIntDef( $intLedgerFilterId, NULL, false ) );
	}

	public function getLedgerFilterId() {
		return $this->m_intLedgerFilterId;
	}

	public function sqlLedgerFilterId() {
		return ( true == isset( $this->m_intLedgerFilterId ) ) ? ( string ) $this->m_intLedgerFilterId : 'NULL';
	}

	public function setRoundTypeId( $intRoundTypeId ) {
		$this->set( 'm_intRoundTypeId', CStrings::strToIntDef( $intRoundTypeId, NULL, false ) );
	}

	public function getRoundTypeId() {
		return $this->m_intRoundTypeId;
	}

	public function sqlRoundTypeId() {
		return ( true == isset( $this->m_intRoundTypeId ) ) ? ( string ) $this->m_intRoundTypeId : 'NULL';
	}

	public function setRecoveryArCodeId( $intRecoveryArCodeId ) {
		$this->set( 'm_intRecoveryArCodeId', CStrings::strToIntDef( $intRecoveryArCodeId, NULL, false ) );
	}

	public function getRecoveryArCodeId() {
		return $this->m_intRecoveryArCodeId;
	}

	public function sqlRecoveryArCodeId() {
		return ( true == isset( $this->m_intRecoveryArCodeId ) ) ? ( string ) $this->m_intRecoveryArCodeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDefaultAmount( $fltDefaultAmount ) {
		$this->set( 'm_fltDefaultAmount', CStrings::strToFloatDef( $fltDefaultAmount, NULL, false, 4 ) );
	}

	public function getDefaultAmount() {
		return $this->m_fltDefaultAmount;
	}

	public function sqlDefaultAmount() {
		return ( true == isset( $this->m_fltDefaultAmount ) ) ? ( string ) $this->m_fltDefaultAmount : '0';
	}

	public function setCaptureDelayDays( $intCaptureDelayDays ) {
		$this->set( 'm_intCaptureDelayDays', CStrings::strToIntDef( $intCaptureDelayDays, NULL, false ) );
	}

	public function getCaptureDelayDays() {
		return $this->m_intCaptureDelayDays;
	}

	public function sqlCaptureDelayDays() {
		return ( true == isset( $this->m_intCaptureDelayDays ) ) ? ( string ) $this->m_intCaptureDelayDays : 'NULL';
	}

	public function setCaptureOnApplicationApproval( $boolCaptureOnApplicationApproval ) {
		$this->set( 'm_boolCaptureOnApplicationApproval', CStrings::strToBool( $boolCaptureOnApplicationApproval ) );
	}

	public function getCaptureOnApplicationApproval() {
		return $this->m_boolCaptureOnApplicationApproval;
	}

	public function sqlCaptureOnApplicationApproval() {
		return ( true == isset( $this->m_boolCaptureOnApplicationApproval ) ) ? '\'' . ( true == ( bool ) $this->m_boolCaptureOnApplicationApproval ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideCharges( $boolHideCharges ) {
		$this->set( 'm_boolHideCharges', CStrings::strToBool( $boolHideCharges ) );
	}

	public function getHideCharges() {
		return $this->m_boolHideCharges;
	}

	public function sqlHideCharges() {
		return ( true == isset( $this->m_boolHideCharges ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideCharges ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideCredits( $boolHideCredits ) {
		$this->set( 'm_boolHideCredits', CStrings::strToBool( $boolHideCredits ) );
	}

	public function getHideCredits() {
		return $this->m_boolHideCredits;
	}

	public function sqlHideCredits() {
		return ( true == isset( $this->m_boolHideCredits ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideCredits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setWaiveLateFees( $boolWaiveLateFees ) {
		$this->set( 'm_boolWaiveLateFees', CStrings::strToBool( $boolWaiveLateFees ) );
	}

	public function getWaiveLateFees() {
		return $this->m_boolWaiveLateFees;
	}

	public function sqlWaiveLateFees() {
		return ( true == isset( $this->m_boolWaiveLateFees ) ) ? '\'' . ( true == ( bool ) $this->m_boolWaiveLateFees ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPostToCash( $boolPostToCash ) {
		$this->set( 'm_boolPostToCash', CStrings::strToBool( $boolPostToCash ) );
	}

	public function getPostToCash() {
		return $this->m_boolPostToCash;
	}

	public function sqlPostToCash() {
		return ( true == isset( $this->m_boolPostToCash ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostToCash ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowMoveOutReminder( $boolShowMoveOutReminder ) {
		$this->set( 'm_boolShowMoveOutReminder', CStrings::strToBool( $boolShowMoveOutReminder ) );
	}

	public function getShowMoveOutReminder() {
		return $this->m_boolShowMoveOutReminder;
	}

	public function sqlShowMoveOutReminder() {
		return ( true == isset( $this->m_boolShowMoveOutReminder ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowMoveOutReminder ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIntegratedOnly( $boolIntegratedOnly ) {
		$this->set( 'm_boolIntegratedOnly', CStrings::strToBool( $boolIntegratedOnly ) );
	}

	public function getIntegratedOnly() {
		return $this->m_boolIntegratedOnly;
	}

	public function sqlIntegratedOnly() {
		return ( true == isset( $this->m_boolIntegratedOnly ) ) ? '\'' . ( true == ( bool ) $this->m_boolIntegratedOnly ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontExport( $boolDontExport ) {
		$this->set( 'm_boolDontExport', CStrings::strToBool( $boolDontExport ) );
	}

	public function getDontExport() {
		return $this->m_boolDontExport;
	}

	public function sqlDontExport() {
		return ( true == isset( $this->m_boolDontExport ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontExport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setProrateCharges( $boolProrateCharges ) {
		$this->set( 'm_boolProrateCharges', CStrings::strToBool( $boolProrateCharges ) );
	}

	public function getProrateCharges() {
		return $this->m_boolProrateCharges;
	}

	public function sqlProrateCharges() {
		return ( true == isset( $this->m_boolProrateCharges ) ) ? '\'' . ( true == ( bool ) $this->m_boolProrateCharges ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCategorizationQueued( $boolIsCategorizationQueued ) {
		$this->set( 'm_boolIsCategorizationQueued', CStrings::strToBool( $boolIsCategorizationQueued ) );
	}

	public function getIsCategorizationQueued() {
		return $this->m_boolIsCategorizationQueued;
	}

	public function sqlIsCategorizationQueued() {
		return ( true == isset( $this->m_boolIsCategorizationQueued ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCategorizationQueued ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPaymentInKind( $boolIsPaymentInKind ) {
		$this->set( 'm_boolIsPaymentInKind', CStrings::strToBool( $boolIsPaymentInKind ) );
	}

	public function getIsPaymentInKind() {
		return $this->m_boolIsPaymentInKind;
	}

	public function sqlIsPaymentInKind() {
		return ( true == isset( $this->m_boolIsPaymentInKind ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPaymentInKind ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReserved( $boolIsReserved ) {
		$this->set( 'm_boolIsReserved', CStrings::strToBool( $boolIsReserved ) );
	}

	public function getIsReserved() {
		return $this->m_boolIsReserved;
	}

	public function sqlIsReserved() {
		return ( true == isset( $this->m_boolIsReserved ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReserved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDefaultAmountIsEditable( $boolDefaultAmountIsEditable ) {
		$this->set( 'm_boolDefaultAmountIsEditable', CStrings::strToBool( $boolDefaultAmountIsEditable ) );
	}

	public function getDefaultAmountIsEditable() {
		return $this->m_boolDefaultAmountIsEditable;
	}

	public function sqlDefaultAmountIsEditable() {
		return ( true == isset( $this->m_boolDefaultAmountIsEditable ) ) ? '\'' . ( true == ( bool ) $this->m_boolDefaultAmountIsEditable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireNote( $boolRequireNote ) {
		$this->set( 'm_boolRequireNote', CStrings::strToBool( $boolRequireNote ) );
	}

	public function getRequireNote() {
		return $this->m_boolRequireNote;
	}

	public function sqlRequireNote() {
		return ( true == isset( $this->m_boolRequireNote ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireNote ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setPriorityNum( $intPriorityNum ) {
		$this->set( 'm_intPriorityNum', CStrings::strToIntDef( $intPriorityNum, NULL, false ) );
	}

	public function getPriorityNum() {
		return $this->m_intPriorityNum;
	}

	public function sqlPriorityNum() {
		return ( true == isset( $this->m_intPriorityNum ) ) ? ( string ) $this->m_intPriorityNum : '0';
	}

	public function setCategorizedBy( $intCategorizedBy ) {
		$this->set( 'm_intCategorizedBy', CStrings::strToIntDef( $intCategorizedBy, NULL, false ) );
	}

	public function getCategorizedBy() {
		return $this->m_intCategorizedBy;
	}

	public function sqlCategorizedBy() {
		return ( true == isset( $this->m_intCategorizedBy ) ) ? ( string ) $this->m_intCategorizedBy : 'NULL';
	}

	public function setCategorizedOn( $strCategorizedOn ) {
		$this->set( 'm_strCategorizedOn', CStrings::strTrimDef( $strCategorizedOn, -1, NULL, true ) );
	}

	public function getCategorizedOn() {
		return $this->m_strCategorizedOn;
	}

	public function sqlCategorizedOn() {
		return ( true == isset( $this->m_strCategorizedOn ) ) ? '\'' . $this->m_strCategorizedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setApplyToFirstMonth( $boolApplyToFirstMonth ) {
		$this->set( 'm_boolApplyToFirstMonth', CStrings::strToBool( $boolApplyToFirstMonth ) );
	}

	public function getApplyToFirstMonth() {
		return $this->m_boolApplyToFirstMonth;
	}

	public function sqlApplyToFirstMonth() {
		return ( true == isset( $this->m_boolApplyToFirstMonth ) ) ? '\'' . ( true == ( bool ) $this->m_boolApplyToFirstMonth ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApplyToLastMonth( $boolApplyToLastMonth ) {
		$this->set( 'm_boolApplyToLastMonth', CStrings::strToBool( $boolApplyToLastMonth ) );
	}

	public function getApplyToLastMonth() {
		return $this->m_boolApplyToLastMonth;
	}

	public function sqlApplyToLastMonth() {
		return ( true == isset( $this->m_boolApplyToLastMonth ) ) ? '\'' . ( true == ( bool ) $this->m_boolApplyToLastMonth ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'works_id' => $this->getWorksId(),
			'cid' => $this->getCid(),
			'gl_account_type_id' => $this->getGlAccountTypeId(),
			'mode_type_id' => $this->getModeTypeId(),
			'ar_code_type_id' => $this->getArCodeTypeId(),
			'ar_origin_id' => $this->getArOriginId(),
			'ar_trigger_type_id' => $this->getArTriggerTypeId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'categorization_type_id' => $this->getCategorizationTypeId(),
			'debit_gl_account_id' => $this->getDebitGlAccountId(),
			'credit_gl_account_id' => $this->getCreditGlAccountId(),
			'modified_debit_gl_account_id' => $this->getModifiedDebitGlAccountId(),
			'write_off_ar_code_id' => $this->getWriteOffArCodeId(),
			'default_ar_code_id' => $this->getDefaultArCodeId(),
			'ar_code_group_id' => $this->getArCodeGroupId(),
			'ar_code_summary_type_id' => $this->getArCodeSummaryTypeId(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'ledger_filter_id' => $this->getLedgerFilterId(),
			'round_type_id' => $this->getRoundTypeId(),
			'recovery_ar_code_id' => $this->getRecoveryArCodeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'default_amount' => $this->getDefaultAmount(),
			'capture_delay_days' => $this->getCaptureDelayDays(),
			'capture_on_application_approval' => $this->getCaptureOnApplicationApproval(),
			'hide_charges' => $this->getHideCharges(),
			'hide_credits' => $this->getHideCredits(),
			'waive_late_fees' => $this->getWaiveLateFees(),
			'post_to_cash' => $this->getPostToCash(),
			'show_move_out_reminder' => $this->getShowMoveOutReminder(),
			'integrated_only' => $this->getIntegratedOnly(),
			'dont_export' => $this->getDontExport(),
			'prorate_charges' => $this->getProrateCharges(),
			'is_categorization_queued' => $this->getIsCategorizationQueued(),
			'is_payment_in_kind' => $this->getIsPaymentInKind(),
			'is_reserved' => $this->getIsReserved(),
			'is_system' => $this->getIsSystem(),
			'is_disabled' => $this->getIsDisabled(),
			'default_amount_is_editable' => $this->getDefaultAmountIsEditable(),
			'require_note' => $this->getRequireNote(),
			'order_num' => $this->getOrderNum(),
			'priority_num' => $this->getPriorityNum(),
			'categorized_by' => $this->getCategorizedBy(),
			'categorized_on' => $this->getCategorizedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'apply_to_first_month' => $this->getApplyToFirstMonth(),
			'apply_to_last_month' => $this->getApplyToLastMonth()
		);
	}

}
?>