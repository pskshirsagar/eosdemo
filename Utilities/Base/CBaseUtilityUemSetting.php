<?php

class CBaseUtilityUemSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_uem_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUtilitySettingId;
	protected $m_intUemEmployeeId;
	protected $m_arrintAuditCompanyContactIds;
	protected $m_boolIsUemImplementation;
	protected $m_boolIsAuditEnabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUemPostMonthOptionId;
	protected $m_boolIsOverrideBillNumber;
	protected $m_strUemGoLiveDate;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsUemImplementation = true;
		$this->m_boolIsAuditEnabled = true;
		$this->m_intUemPostMonthOptionId = '1';
		$this->m_boolIsOverrideBillNumber = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_utility_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilitySettingId', trim( $arrValues['property_utility_setting_id'] ) ); elseif( isset( $arrValues['property_utility_setting_id'] ) ) $this->setPropertyUtilitySettingId( $arrValues['property_utility_setting_id'] );
		if( isset( $arrValues['uem_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intUemEmployeeId', trim( $arrValues['uem_employee_id'] ) ); elseif( isset( $arrValues['uem_employee_id'] ) ) $this->setUemEmployeeId( $arrValues['uem_employee_id'] );
		if( isset( $arrValues['audit_company_contact_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintAuditCompanyContactIds', trim( $arrValues['audit_company_contact_ids'] ) ); elseif( isset( $arrValues['audit_company_contact_ids'] ) ) $this->setAuditCompanyContactIds( $arrValues['audit_company_contact_ids'] );
		if( isset( $arrValues['is_uem_implementation'] ) && $boolDirectSet ) $this->set( 'm_boolIsUemImplementation', trim( stripcslashes( $arrValues['is_uem_implementation'] ) ) ); elseif( isset( $arrValues['is_uem_implementation'] ) ) $this->setIsUemImplementation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_uem_implementation'] ) : $arrValues['is_uem_implementation'] );
		if( isset( $arrValues['is_audit_enabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsAuditEnabled', trim( stripcslashes( $arrValues['is_audit_enabled'] ) ) ); elseif( isset( $arrValues['is_audit_enabled'] ) ) $this->setIsAuditEnabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_audit_enabled'] ) : $arrValues['is_audit_enabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['uem_post_month_option_id'] ) && $boolDirectSet ) $this->set( 'm_intUemPostMonthOptionId', trim( $arrValues['uem_post_month_option_id'] ) ); elseif( isset( $arrValues['uem_post_month_option_id'] ) ) $this->setUemPostMonthOptionId( $arrValues['uem_post_month_option_id'] );
		if( isset( $arrValues['is_override_bill_number'] ) && $boolDirectSet ) $this->set( 'm_boolIsOverrideBillNumber', trim( stripcslashes( $arrValues['is_override_bill_number'] ) ) ); elseif( isset( $arrValues['is_override_bill_number'] ) ) $this->setIsOverrideBillNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_override_bill_number'] ) : $arrValues['is_override_bill_number'] );
		if( isset( $arrValues['uem_go_live_date'] ) && $boolDirectSet ) $this->set( 'm_strUemGoLiveDate', trim( $arrValues['uem_go_live_date'] ) ); elseif( isset( $arrValues['uem_go_live_date'] ) ) $this->setUemGoLiveDate( $arrValues['uem_go_live_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUtilitySettingId( $intPropertyUtilitySettingId ) {
		$this->set( 'm_intPropertyUtilitySettingId', CStrings::strToIntDef( $intPropertyUtilitySettingId, NULL, false ) );
	}

	public function getPropertyUtilitySettingId() {
		return $this->m_intPropertyUtilitySettingId;
	}

	public function sqlPropertyUtilitySettingId() {
		return ( true == isset( $this->m_intPropertyUtilitySettingId ) ) ? ( string ) $this->m_intPropertyUtilitySettingId : 'NULL';
	}

	public function setUemEmployeeId( $intUemEmployeeId ) {
		$this->set( 'm_intUemEmployeeId', CStrings::strToIntDef( $intUemEmployeeId, NULL, false ) );
	}

	public function getUemEmployeeId() {
		return $this->m_intUemEmployeeId;
	}

	public function sqlUemEmployeeId() {
		return ( true == isset( $this->m_intUemEmployeeId ) ) ? ( string ) $this->m_intUemEmployeeId : 'NULL';
	}

	public function setAuditCompanyContactIds( $arrintAuditCompanyContactIds ) {
		$this->set( 'm_arrintAuditCompanyContactIds', CStrings::strToArrIntDef( $arrintAuditCompanyContactIds, NULL ) );
	}

	public function getAuditCompanyContactIds() {
		return $this->m_arrintAuditCompanyContactIds;
	}

	public function sqlAuditCompanyContactIds() {
		return ( true == isset( $this->m_arrintAuditCompanyContactIds ) && true == valArr( $this->m_arrintAuditCompanyContactIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintAuditCompanyContactIds, NULL ) . '\'' : 'NULL';
	}

	public function setIsUemImplementation( $boolIsUemImplementation ) {
		$this->set( 'm_boolIsUemImplementation', CStrings::strToBool( $boolIsUemImplementation ) );
	}

	public function getIsUemImplementation() {
		return $this->m_boolIsUemImplementation;
	}

	public function sqlIsUemImplementation() {
		return ( true == isset( $this->m_boolIsUemImplementation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUemImplementation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAuditEnabled( $boolIsAuditEnabled ) {
		$this->set( 'm_boolIsAuditEnabled', CStrings::strToBool( $boolIsAuditEnabled ) );
	}

	public function getIsAuditEnabled() {
		return $this->m_boolIsAuditEnabled;
	}

	public function sqlIsAuditEnabled() {
		return ( true == isset( $this->m_boolIsAuditEnabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAuditEnabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUemPostMonthOptionId( $intUemPostMonthOptionId ) {
		$this->set( 'm_intUemPostMonthOptionId', CStrings::strToIntDef( $intUemPostMonthOptionId, NULL, false ) );
	}

	public function getUemPostMonthOptionId() {
		return $this->m_intUemPostMonthOptionId;
	}

	public function sqlUemPostMonthOptionId() {
		return ( true == isset( $this->m_intUemPostMonthOptionId ) ) ? ( string ) $this->m_intUemPostMonthOptionId : '1';
	}

	public function setIsOverrideBillNumber( $boolIsOverrideBillNumber ) {
		$this->set( 'm_boolIsOverrideBillNumber', CStrings::strToBool( $boolIsOverrideBillNumber ) );
	}

	public function getIsOverrideBillNumber() {
		return $this->m_boolIsOverrideBillNumber;
	}

	public function sqlIsOverrideBillNumber() {
		return ( true == isset( $this->m_boolIsOverrideBillNumber ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOverrideBillNumber ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Oct 07 2020.
	 */
	public function setUemGoLiveDate( $strUemGoLiveDate ) {
		$this->set( 'm_strUemGoLiveDate', CStrings::strTrimDef( $strUemGoLiveDate, -1, NULL, true ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Oct 07 2020.
	 */
	public function getUemGoLiveDate() {
		return $this->m_strUemGoLiveDate;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Oct 07 2020.
	 */
	public function sqlUemGoLiveDate() {
		return ( true == isset( $this->m_strUemGoLiveDate ) ) ? '\'' . $this->m_strUemGoLiveDate . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_setting_id, uem_employee_id, audit_company_contact_ids, is_uem_implementation, is_audit_enabled, updated_by, updated_on, created_by, created_on, uem_post_month_option_id, is_override_bill_number, uem_go_live_date )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlPropertyUtilitySettingId() . ', ' .
		          $this->sqlUemEmployeeId() . ', ' .
		          $this->sqlAuditCompanyContactIds() . ', ' .
		          $this->sqlIsUemImplementation() . ', ' .
		          $this->sqlIsAuditEnabled() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlUemPostMonthOptionId() . ', ' .
		          $this->sqlIsOverrideBillNumber() . ', ' .
		          $this->sqlUemGoLiveDate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilitySettingId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_setting_id = ' . $this->sqlPropertyUtilitySettingId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uem_employee_id = ' . $this->sqlUemEmployeeId(). ',' ; } elseif( true == array_key_exists( 'UemEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' uem_employee_id = ' . $this->sqlUemEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_company_contact_ids = ' . $this->sqlAuditCompanyContactIds(). ',' ; } elseif( true == array_key_exists( 'AuditCompanyContactIds', $this->getChangedColumns() ) ) { $strSql .= ' audit_company_contact_ids = ' . $this->sqlAuditCompanyContactIds() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_uem_implementation = ' . $this->sqlIsUemImplementation(). ',' ; } elseif( true == array_key_exists( 'IsUemImplementation', $this->getChangedColumns() ) ) { $strSql .= ' is_uem_implementation = ' . $this->sqlIsUemImplementation() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_audit_enabled = ' . $this->sqlIsAuditEnabled(). ',' ; } elseif( true == array_key_exists( 'IsAuditEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_audit_enabled = ' . $this->sqlIsAuditEnabled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uem_post_month_option_id = ' . $this->sqlUemPostMonthOptionId(). ',' ; } elseif( true == array_key_exists( 'UemPostMonthOptionId', $this->getChangedColumns() ) ) { $strSql .= ' uem_post_month_option_id = ' . $this->sqlUemPostMonthOptionId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_override_bill_number = ' . $this->sqlIsOverrideBillNumber(). ',' ; } elseif( true == array_key_exists( 'IsOverrideBillNumber', $this->getChangedColumns() ) ) { $strSql .= ' is_override_bill_number = ' . $this->sqlIsOverrideBillNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uem_go_live_date = ' . $this->sqlUemGoLiveDate(). ',' ; } elseif( true == array_key_exists( 'UemGoLiveDate', $this->getChangedColumns() ) ) { $strSql .= ' uem_go_live_date = ' . $this->sqlUemGoLiveDate() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_utility_setting_id' => $this->getPropertyUtilitySettingId(),
			'uem_employee_id' => $this->getUemEmployeeId(),
			'audit_company_contact_ids' => $this->getAuditCompanyContactIds(),
			'is_uem_implementation' => $this->getIsUemImplementation(),
			'is_audit_enabled' => $this->getIsAuditEnabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'uem_post_month_option_id' => $this->getUemPostMonthOptionId(),
			'is_override_bill_number' => $this->getIsOverrideBillNumber(),
			'uem_go_live_date' => $this->getUemGoLiveDate()
		);
	}

}
?>