<?php

class CBaseRuPricing extends CEosSingularBase {

	const TABLE_NAME = 'public.ru_pricings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_fltConvergentBillingFeeAmount;
	protected $m_fltSubsidizedConvergentBillingFeeAmount;
	protected $m_fltMoveInFeeAmount;
	protected $m_fltMoveOutFeeAmount;
	protected $m_fltVcrServiceFeeAmount;
	protected $m_fltVcmAmount;
	protected $m_boolChargeForPostage;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltSubmeteredBillingFeeAmount;

	public function __construct() {
		parent::__construct();

		$this->m_boolChargeForPostage = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['convergent_billing_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltConvergentBillingFeeAmount', trim( $arrValues['convergent_billing_fee_amount'] ) ); elseif( isset( $arrValues['convergent_billing_fee_amount'] ) ) $this->setConvergentBillingFeeAmount( $arrValues['convergent_billing_fee_amount'] );
		if( isset( $arrValues['subsidized_convergent_billing_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSubsidizedConvergentBillingFeeAmount', trim( $arrValues['subsidized_convergent_billing_fee_amount'] ) ); elseif( isset( $arrValues['subsidized_convergent_billing_fee_amount'] ) ) $this->setSubsidizedConvergentBillingFeeAmount( $arrValues['subsidized_convergent_billing_fee_amount'] );
		if( isset( $arrValues['move_in_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMoveInFeeAmount', trim( $arrValues['move_in_fee_amount'] ) ); elseif( isset( $arrValues['move_in_fee_amount'] ) ) $this->setMoveInFeeAmount( $arrValues['move_in_fee_amount'] );
		if( isset( $arrValues['move_out_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMoveOutFeeAmount', trim( $arrValues['move_out_fee_amount'] ) ); elseif( isset( $arrValues['move_out_fee_amount'] ) ) $this->setMoveOutFeeAmount( $arrValues['move_out_fee_amount'] );
		if( isset( $arrValues['vcr_service_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltVcrServiceFeeAmount', trim( $arrValues['vcr_service_fee_amount'] ) ); elseif( isset( $arrValues['vcr_service_fee_amount'] ) ) $this->setVcrServiceFeeAmount( $arrValues['vcr_service_fee_amount'] );
		if( isset( $arrValues['vcm_amount'] ) && $boolDirectSet ) $this->set( 'm_fltVcmAmount', trim( $arrValues['vcm_amount'] ) ); elseif( isset( $arrValues['vcm_amount'] ) ) $this->setVcmAmount( $arrValues['vcm_amount'] );
		if( isset( $arrValues['charge_for_postage'] ) && $boolDirectSet ) $this->set( 'm_boolChargeForPostage', trim( stripcslashes( $arrValues['charge_for_postage'] ) ) ); elseif( isset( $arrValues['charge_for_postage'] ) ) $this->setChargeForPostage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_for_postage'] ) : $arrValues['charge_for_postage'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['submetered_billing_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSubmeteredBillingFeeAmount', trim( $arrValues['submetered_billing_fee_amount'] ) ); elseif( isset( $arrValues['submetered_billing_fee_amount'] ) ) $this->setSubmeteredBillingFeeAmount( $arrValues['submetered_billing_fee_amount'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setConvergentBillingFeeAmount( $fltConvergentBillingFeeAmount ) {
		$this->set( 'm_fltConvergentBillingFeeAmount', CStrings::strToFloatDef( $fltConvergentBillingFeeAmount, NULL, false, 2 ) );
	}

	public function getConvergentBillingFeeAmount() {
		return $this->m_fltConvergentBillingFeeAmount;
	}

	public function sqlConvergentBillingFeeAmount() {
		return ( true == isset( $this->m_fltConvergentBillingFeeAmount ) ) ? ( string ) $this->m_fltConvergentBillingFeeAmount : 'NULL';
	}

	public function setSubsidizedConvergentBillingFeeAmount( $fltSubsidizedConvergentBillingFeeAmount ) {
		$this->set( 'm_fltSubsidizedConvergentBillingFeeAmount', CStrings::strToFloatDef( $fltSubsidizedConvergentBillingFeeAmount, NULL, false, 2 ) );
	}

	public function getSubsidizedConvergentBillingFeeAmount() {
		return $this->m_fltSubsidizedConvergentBillingFeeAmount;
	}

	public function sqlSubsidizedConvergentBillingFeeAmount() {
		return ( true == isset( $this->m_fltSubsidizedConvergentBillingFeeAmount ) ) ? ( string ) $this->m_fltSubsidizedConvergentBillingFeeAmount : 'NULL';
	}

	public function setMoveInFeeAmount( $fltMoveInFeeAmount ) {
		$this->set( 'm_fltMoveInFeeAmount', CStrings::strToFloatDef( $fltMoveInFeeAmount, NULL, false, 2 ) );
	}

	public function getMoveInFeeAmount() {
		return $this->m_fltMoveInFeeAmount;
	}

	public function sqlMoveInFeeAmount() {
		return ( true == isset( $this->m_fltMoveInFeeAmount ) ) ? ( string ) $this->m_fltMoveInFeeAmount : 'NULL';
	}

	public function setMoveOutFeeAmount( $fltMoveOutFeeAmount ) {
		$this->set( 'm_fltMoveOutFeeAmount', CStrings::strToFloatDef( $fltMoveOutFeeAmount, NULL, false, 2 ) );
	}

	public function getMoveOutFeeAmount() {
		return $this->m_fltMoveOutFeeAmount;
	}

	public function sqlMoveOutFeeAmount() {
		return ( true == isset( $this->m_fltMoveOutFeeAmount ) ) ? ( string ) $this->m_fltMoveOutFeeAmount : 'NULL';
	}

	public function setVcrServiceFeeAmount( $fltVcrServiceFeeAmount ) {
		$this->set( 'm_fltVcrServiceFeeAmount', CStrings::strToFloatDef( $fltVcrServiceFeeAmount, NULL, false, 2 ) );
	}

	public function getVcrServiceFeeAmount() {
		return $this->m_fltVcrServiceFeeAmount;
	}

	public function sqlVcrServiceFeeAmount() {
		return ( true == isset( $this->m_fltVcrServiceFeeAmount ) ) ? ( string ) $this->m_fltVcrServiceFeeAmount : 'NULL';
	}

	public function setVcmAmount( $fltVcmAmount ) {
		$this->set( 'm_fltVcmAmount', CStrings::strToFloatDef( $fltVcmAmount, NULL, false, 2 ) );
	}

	public function getVcmAmount() {
		return $this->m_fltVcmAmount;
	}

	public function sqlVcmAmount() {
		return ( true == isset( $this->m_fltVcmAmount ) ) ? ( string ) $this->m_fltVcmAmount : 'NULL';
	}

	public function setChargeForPostage( $boolChargeForPostage ) {
		$this->set( 'm_boolChargeForPostage', CStrings::strToBool( $boolChargeForPostage ) );
	}

	public function getChargeForPostage() {
		return $this->m_boolChargeForPostage;
	}

	public function sqlChargeForPostage() {
		return ( true == isset( $this->m_boolChargeForPostage ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargeForPostage ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSubmeteredBillingFeeAmount( $fltSubmeteredBillingFeeAmount ) {
		$this->set( 'm_fltSubmeteredBillingFeeAmount', CStrings::strToFloatDef( $fltSubmeteredBillingFeeAmount, NULL, false, 2 ) );
	}

	public function getSubmeteredBillingFeeAmount() {
		return $this->m_fltSubmeteredBillingFeeAmount;
	}

	public function sqlSubmeteredBillingFeeAmount() {
		return ( true == isset( $this->m_fltSubmeteredBillingFeeAmount ) ) ? ( string ) $this->m_fltSubmeteredBillingFeeAmount : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, convergent_billing_fee_amount, subsidized_convergent_billing_fee_amount, move_in_fee_amount, move_out_fee_amount, vcr_service_fee_amount, vcm_amount, charge_for_postage, updated_by, updated_on, created_by, created_on, submetered_billing_fee_amount )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlConvergentBillingFeeAmount() . ', ' .
						$this->sqlSubsidizedConvergentBillingFeeAmount() . ', ' .
						$this->sqlMoveInFeeAmount() . ', ' .
						$this->sqlMoveOutFeeAmount() . ', ' .
						$this->sqlVcrServiceFeeAmount() . ', ' .
						$this->sqlVcmAmount() . ', ' .
						$this->sqlChargeForPostage() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlSubmeteredBillingFeeAmount() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' convergent_billing_fee_amount = ' . $this->sqlConvergentBillingFeeAmount(). ',' ; } elseif( true == array_key_exists( 'ConvergentBillingFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' convergent_billing_fee_amount = ' . $this->sqlConvergentBillingFeeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidized_convergent_billing_fee_amount = ' . $this->sqlSubsidizedConvergentBillingFeeAmount(). ',' ; } elseif( true == array_key_exists( 'SubsidizedConvergentBillingFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' subsidized_convergent_billing_fee_amount = ' . $this->sqlSubsidizedConvergentBillingFeeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_fee_amount = ' . $this->sqlMoveInFeeAmount(). ',' ; } elseif( true == array_key_exists( 'MoveInFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' move_in_fee_amount = ' . $this->sqlMoveInFeeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_fee_amount = ' . $this->sqlMoveOutFeeAmount(). ',' ; } elseif( true == array_key_exists( 'MoveOutFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' move_out_fee_amount = ' . $this->sqlMoveOutFeeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vcr_service_fee_amount = ' . $this->sqlVcrServiceFeeAmount(). ',' ; } elseif( true == array_key_exists( 'VcrServiceFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' vcr_service_fee_amount = ' . $this->sqlVcrServiceFeeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vcm_amount = ' . $this->sqlVcmAmount(). ',' ; } elseif( true == array_key_exists( 'VcmAmount', $this->getChangedColumns() ) ) { $strSql .= ' vcm_amount = ' . $this->sqlVcmAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_for_postage = ' . $this->sqlChargeForPostage(). ',' ; } elseif( true == array_key_exists( 'ChargeForPostage', $this->getChangedColumns() ) ) { $strSql .= ' charge_for_postage = ' . $this->sqlChargeForPostage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submetered_billing_fee_amount = ' . $this->sqlSubmeteredBillingFeeAmount(). ',' ; } elseif( true == array_key_exists( 'SubmeteredBillingFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' submetered_billing_fee_amount = ' . $this->sqlSubmeteredBillingFeeAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'convergent_billing_fee_amount' => $this->getConvergentBillingFeeAmount(),
			'subsidized_convergent_billing_fee_amount' => $this->getSubsidizedConvergentBillingFeeAmount(),
			'move_in_fee_amount' => $this->getMoveInFeeAmount(),
			'move_out_fee_amount' => $this->getMoveOutFeeAmount(),
			'vcr_service_fee_amount' => $this->getVcrServiceFeeAmount(),
			'vcm_amount' => $this->getVcmAmount(),
			'charge_for_postage' => $this->getChargeForPostage(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'submetered_billing_fee_amount' => $this->getSubmeteredBillingFeeAmount()
		);
	}

}
?>