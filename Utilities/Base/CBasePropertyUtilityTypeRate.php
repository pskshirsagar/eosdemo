<?php

class CBasePropertyUtilityTypeRate extends CEosSingularBase {

	const TABLE_NAME = 'public.property_utility_type_rates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intUtilityRubsFormulaId;
	protected $m_intSubsidyArCodeId;
	protected $m_intNewPropertyUtilityTypeRateId;
	protected $m_strLeaseStartDateCriteria;
	protected $m_strLeaseEndDateCriteria;
	protected $m_fltCommonAreaDeduction;
	protected $m_fltMoveInFee;
	protected $m_fltTerminationFee;
	protected $m_fltFlatFeeAmount;
	protected $m_fltBillingFeeAmount;
	protected $m_fltBillingFeePercent;
	protected $m_fltBudgetBillingAmount;
	protected $m_fltRecoveryPercentage;
	protected $m_boolAllocateToVacantUnits;
	protected $m_boolIgnoreRenewals;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltMoveInFee = '0';
		$this->m_fltTerminationFee = '0';
		$this->m_fltBillingFeePercent = '0';
		$this->m_boolAllocateToVacantUnits = false;
		$this->m_boolIgnoreRenewals = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['utility_rubs_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityRubsFormulaId', trim( $arrValues['utility_rubs_formula_id'] ) ); elseif( isset( $arrValues['utility_rubs_formula_id'] ) ) $this->setUtilityRubsFormulaId( $arrValues['utility_rubs_formula_id'] );
		if( isset( $arrValues['subsidy_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyArCodeId', trim( $arrValues['subsidy_ar_code_id'] ) ); elseif( isset( $arrValues['subsidy_ar_code_id'] ) ) $this->setSubsidyArCodeId( $arrValues['subsidy_ar_code_id'] );
		if( isset( $arrValues['new_property_utility_type_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertyUtilityTypeRateId', trim( $arrValues['new_property_utility_type_rate_id'] ) ); elseif( isset( $arrValues['new_property_utility_type_rate_id'] ) ) $this->setNewPropertyUtilityTypeRateId( $arrValues['new_property_utility_type_rate_id'] );
		if( isset( $arrValues['lease_start_date_criteria'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDateCriteria', trim( $arrValues['lease_start_date_criteria'] ) ); elseif( isset( $arrValues['lease_start_date_criteria'] ) ) $this->setLeaseStartDateCriteria( $arrValues['lease_start_date_criteria'] );
		if( isset( $arrValues['lease_end_date_criteria'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDateCriteria', trim( $arrValues['lease_end_date_criteria'] ) ); elseif( isset( $arrValues['lease_end_date_criteria'] ) ) $this->setLeaseEndDateCriteria( $arrValues['lease_end_date_criteria'] );
		if( isset( $arrValues['common_area_deduction'] ) && $boolDirectSet ) $this->set( 'm_fltCommonAreaDeduction', trim( $arrValues['common_area_deduction'] ) ); elseif( isset( $arrValues['common_area_deduction'] ) ) $this->setCommonAreaDeduction( $arrValues['common_area_deduction'] );
		if( isset( $arrValues['move_in_fee'] ) && $boolDirectSet ) $this->set( 'm_fltMoveInFee', trim( $arrValues['move_in_fee'] ) ); elseif( isset( $arrValues['move_in_fee'] ) ) $this->setMoveInFee( $arrValues['move_in_fee'] );
		if( isset( $arrValues['termination_fee'] ) && $boolDirectSet ) $this->set( 'm_fltTerminationFee', trim( $arrValues['termination_fee'] ) ); elseif( isset( $arrValues['termination_fee'] ) ) $this->setTerminationFee( $arrValues['termination_fee'] );
		if( isset( $arrValues['flat_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltFlatFeeAmount', trim( $arrValues['flat_fee_amount'] ) ); elseif( isset( $arrValues['flat_fee_amount'] ) ) $this->setFlatFeeAmount( $arrValues['flat_fee_amount'] );
		if( isset( $arrValues['billing_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltBillingFeeAmount', trim( $arrValues['billing_fee_amount'] ) ); elseif( isset( $arrValues['billing_fee_amount'] ) ) $this->setBillingFeeAmount( $arrValues['billing_fee_amount'] );
		if( isset( $arrValues['billing_fee_percent'] ) && $boolDirectSet ) $this->set( 'm_fltBillingFeePercent', trim( $arrValues['billing_fee_percent'] ) ); elseif( isset( $arrValues['billing_fee_percent'] ) ) $this->setBillingFeePercent( $arrValues['billing_fee_percent'] );
		if( isset( $arrValues['budget_billing_amount'] ) && $boolDirectSet ) $this->set( 'm_fltBudgetBillingAmount', trim( $arrValues['budget_billing_amount'] ) ); elseif( isset( $arrValues['budget_billing_amount'] ) ) $this->setBudgetBillingAmount( $arrValues['budget_billing_amount'] );
		if( isset( $arrValues['recovery_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltRecoveryPercentage', trim( $arrValues['recovery_percentage'] ) ); elseif( isset( $arrValues['recovery_percentage'] ) ) $this->setRecoveryPercentage( $arrValues['recovery_percentage'] );
		if( isset( $arrValues['allocate_to_vacant_units'] ) && $boolDirectSet ) $this->set( 'm_boolAllocateToVacantUnits', trim( stripcslashes( $arrValues['allocate_to_vacant_units'] ) ) ); elseif( isset( $arrValues['allocate_to_vacant_units'] ) ) $this->setAllocateToVacantUnits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allocate_to_vacant_units'] ) : $arrValues['allocate_to_vacant_units'] );
		if( isset( $arrValues['ignore_renewals'] ) && $boolDirectSet ) $this->set( 'm_boolIgnoreRenewals', trim( stripcslashes( $arrValues['ignore_renewals'] ) ) ); elseif( isset( $arrValues['ignore_renewals'] ) ) $this->setIgnoreRenewals( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ignore_renewals'] ) : $arrValues['ignore_renewals'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setUtilityRubsFormulaId( $intUtilityRubsFormulaId ) {
		$this->set( 'm_intUtilityRubsFormulaId', CStrings::strToIntDef( $intUtilityRubsFormulaId, NULL, false ) );
	}

	public function getUtilityRubsFormulaId() {
		return $this->m_intUtilityRubsFormulaId;
	}

	public function sqlUtilityRubsFormulaId() {
		return ( true == isset( $this->m_intUtilityRubsFormulaId ) ) ? ( string ) $this->m_intUtilityRubsFormulaId : 'NULL';
	}

	public function setSubsidyArCodeId( $intSubsidyArCodeId ) {
		$this->set( 'm_intSubsidyArCodeId', CStrings::strToIntDef( $intSubsidyArCodeId, NULL, false ) );
	}

	public function getSubsidyArCodeId() {
		return $this->m_intSubsidyArCodeId;
	}

	public function sqlSubsidyArCodeId() {
		return ( true == isset( $this->m_intSubsidyArCodeId ) ) ? ( string ) $this->m_intSubsidyArCodeId : 'NULL';
	}

	public function setNewPropertyUtilityTypeRateId( $intNewPropertyUtilityTypeRateId ) {
		$this->set( 'm_intNewPropertyUtilityTypeRateId', CStrings::strToIntDef( $intNewPropertyUtilityTypeRateId, NULL, false ) );
	}

	public function getNewPropertyUtilityTypeRateId() {
		return $this->m_intNewPropertyUtilityTypeRateId;
	}

	public function sqlNewPropertyUtilityTypeRateId() {
		return ( true == isset( $this->m_intNewPropertyUtilityTypeRateId ) ) ? ( string ) $this->m_intNewPropertyUtilityTypeRateId : 'NULL';
	}

	public function setLeaseStartDateCriteria( $strLeaseStartDateCriteria ) {
		$this->set( 'm_strLeaseStartDateCriteria', CStrings::strTrimDef( $strLeaseStartDateCriteria, -1, NULL, true ) );
	}

	public function getLeaseStartDateCriteria() {
		return $this->m_strLeaseStartDateCriteria;
	}

	public function sqlLeaseStartDateCriteria() {
		return ( true == isset( $this->m_strLeaseStartDateCriteria ) ) ? '\'' . $this->m_strLeaseStartDateCriteria . '\'' : 'NULL';
	}

	public function setLeaseEndDateCriteria( $strLeaseEndDateCriteria ) {
		$this->set( 'm_strLeaseEndDateCriteria', CStrings::strTrimDef( $strLeaseEndDateCriteria, -1, NULL, true ) );
	}

	public function getLeaseEndDateCriteria() {
		return $this->m_strLeaseEndDateCriteria;
	}

	public function sqlLeaseEndDateCriteria() {
		return ( true == isset( $this->m_strLeaseEndDateCriteria ) ) ? '\'' . $this->m_strLeaseEndDateCriteria . '\'' : 'NULL';
	}

	public function setCommonAreaDeduction( $fltCommonAreaDeduction ) {
		$this->set( 'm_fltCommonAreaDeduction', CStrings::strToFloatDef( $fltCommonAreaDeduction, NULL, false, 6 ) );
	}

	public function getCommonAreaDeduction() {
		return $this->m_fltCommonAreaDeduction;
	}

	public function sqlCommonAreaDeduction() {
		return ( true == isset( $this->m_fltCommonAreaDeduction ) ) ? ( string ) $this->m_fltCommonAreaDeduction : 'NULL';
	}

	public function setMoveInFee( $fltMoveInFee ) {
		$this->set( 'm_fltMoveInFee', CStrings::strToFloatDef( $fltMoveInFee, NULL, false, 2 ) );
	}

	public function getMoveInFee() {
		return $this->m_fltMoveInFee;
	}

	public function sqlMoveInFee() {
		return ( true == isset( $this->m_fltMoveInFee ) ) ? ( string ) $this->m_fltMoveInFee : '0';
	}

	public function setTerminationFee( $fltTerminationFee ) {
		$this->set( 'm_fltTerminationFee', CStrings::strToFloatDef( $fltTerminationFee, NULL, false, 2 ) );
	}

	public function getTerminationFee() {
		return $this->m_fltTerminationFee;
	}

	public function sqlTerminationFee() {
		return ( true == isset( $this->m_fltTerminationFee ) ) ? ( string ) $this->m_fltTerminationFee : '0';
	}

	public function setFlatFeeAmount( $fltFlatFeeAmount ) {
		$this->set( 'm_fltFlatFeeAmount', CStrings::strToFloatDef( $fltFlatFeeAmount, NULL, false, 2 ) );
	}

	public function getFlatFeeAmount() {
		return $this->m_fltFlatFeeAmount;
	}

	public function sqlFlatFeeAmount() {
		return ( true == isset( $this->m_fltFlatFeeAmount ) ) ? ( string ) $this->m_fltFlatFeeAmount : 'NULL';
	}

	public function setBillingFeeAmount( $fltBillingFeeAmount ) {
		$this->set( 'm_fltBillingFeeAmount', CStrings::strToFloatDef( $fltBillingFeeAmount, NULL, false, 2 ) );
	}

	public function getBillingFeeAmount() {
		return $this->m_fltBillingFeeAmount;
	}

	public function sqlBillingFeeAmount() {
		return ( true == isset( $this->m_fltBillingFeeAmount ) ) ? ( string ) $this->m_fltBillingFeeAmount : 'NULL';
	}

	public function setBillingFeePercent( $fltBillingFeePercent ) {
		$this->set( 'm_fltBillingFeePercent', CStrings::strToFloatDef( $fltBillingFeePercent, NULL, false, 6 ) );
	}

	public function getBillingFeePercent() {
		return $this->m_fltBillingFeePercent;
	}

	public function sqlBillingFeePercent() {
		return ( true == isset( $this->m_fltBillingFeePercent ) ) ? ( string ) $this->m_fltBillingFeePercent : '0';
	}

	public function setBudgetBillingAmount( $fltBudgetBillingAmount ) {
		$this->set( 'm_fltBudgetBillingAmount', CStrings::strToFloatDef( $fltBudgetBillingAmount, NULL, false, 2 ) );
	}

	public function getBudgetBillingAmount() {
		return $this->m_fltBudgetBillingAmount;
	}

	public function sqlBudgetBillingAmount() {
		return ( true == isset( $this->m_fltBudgetBillingAmount ) ) ? ( string ) $this->m_fltBudgetBillingAmount : 'NULL';
	}

	public function setRecoveryPercentage( $fltRecoveryPercentage ) {
		$this->set( 'm_fltRecoveryPercentage', CStrings::strToFloatDef( $fltRecoveryPercentage, NULL, false, 2 ) );
	}

	public function getRecoveryPercentage() {
		return $this->m_fltRecoveryPercentage;
	}

	public function sqlRecoveryPercentage() {
		return ( true == isset( $this->m_fltRecoveryPercentage ) ) ? ( string ) $this->m_fltRecoveryPercentage : 'NULL';
	}

	public function setAllocateToVacantUnits( $boolAllocateToVacantUnits ) {
		$this->set( 'm_boolAllocateToVacantUnits', CStrings::strToBool( $boolAllocateToVacantUnits ) );
	}

	public function getAllocateToVacantUnits() {
		return $this->m_boolAllocateToVacantUnits;
	}

	public function sqlAllocateToVacantUnits() {
		return ( true == isset( $this->m_boolAllocateToVacantUnits ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllocateToVacantUnits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIgnoreRenewals( $boolIgnoreRenewals ) {
		$this->set( 'm_boolIgnoreRenewals', CStrings::strToBool( $boolIgnoreRenewals ) );
	}

	public function getIgnoreRenewals() {
		return $this->m_boolIgnoreRenewals;
	}

	public function sqlIgnoreRenewals() {
		return ( true == isset( $this->m_boolIgnoreRenewals ) ) ? '\'' . ( true == ( bool ) $this->m_boolIgnoreRenewals ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_utility_type_id, utility_rubs_formula_id, subsidy_ar_code_id, new_property_utility_type_rate_id, lease_start_date_criteria, lease_end_date_criteria, common_area_deduction, move_in_fee, termination_fee, flat_fee_amount, billing_fee_amount, billing_fee_percent, budget_billing_amount, recovery_percentage, allocate_to_vacant_units, ignore_renewals, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyUtilityTypeId() . ', ' .
						$this->sqlUtilityRubsFormulaId() . ', ' .
						$this->sqlSubsidyArCodeId() . ', ' .
						$this->sqlNewPropertyUtilityTypeRateId() . ', ' .
						$this->sqlLeaseStartDateCriteria() . ', ' .
						$this->sqlLeaseEndDateCriteria() . ', ' .
						$this->sqlCommonAreaDeduction() . ', ' .
						$this->sqlMoveInFee() . ', ' .
						$this->sqlTerminationFee() . ', ' .
						$this->sqlFlatFeeAmount() . ', ' .
						$this->sqlBillingFeeAmount() . ', ' .
						$this->sqlBillingFeePercent() . ', ' .
						$this->sqlBudgetBillingAmount() . ', ' .
						$this->sqlRecoveryPercentage() . ', ' .
						$this->sqlAllocateToVacantUnits() . ', ' .
						$this->sqlIgnoreRenewals() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_rubs_formula_id = ' . $this->sqlUtilityRubsFormulaId(). ',' ; } elseif( true == array_key_exists( 'UtilityRubsFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' utility_rubs_formula_id = ' . $this->sqlUtilityRubsFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_ar_code_id = ' . $this->sqlSubsidyArCodeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_ar_code_id = ' . $this->sqlSubsidyArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_utility_type_rate_id = ' . $this->sqlNewPropertyUtilityTypeRateId(). ',' ; } elseif( true == array_key_exists( 'NewPropertyUtilityTypeRateId', $this->getChangedColumns() ) ) { $strSql .= ' new_property_utility_type_rate_id = ' . $this->sqlNewPropertyUtilityTypeRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date_criteria = ' . $this->sqlLeaseStartDateCriteria(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDateCriteria', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date_criteria = ' . $this->sqlLeaseStartDateCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_date_criteria = ' . $this->sqlLeaseEndDateCriteria(). ',' ; } elseif( true == array_key_exists( 'LeaseEndDateCriteria', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_date_criteria = ' . $this->sqlLeaseEndDateCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' common_area_deduction = ' . $this->sqlCommonAreaDeduction(). ',' ; } elseif( true == array_key_exists( 'CommonAreaDeduction', $this->getChangedColumns() ) ) { $strSql .= ' common_area_deduction = ' . $this->sqlCommonAreaDeduction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_fee = ' . $this->sqlMoveInFee(). ',' ; } elseif( true == array_key_exists( 'MoveInFee', $this->getChangedColumns() ) ) { $strSql .= ' move_in_fee = ' . $this->sqlMoveInFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_fee = ' . $this->sqlTerminationFee(). ',' ; } elseif( true == array_key_exists( 'TerminationFee', $this->getChangedColumns() ) ) { $strSql .= ' termination_fee = ' . $this->sqlTerminationFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' flat_fee_amount = ' . $this->sqlFlatFeeAmount(). ',' ; } elseif( true == array_key_exists( 'FlatFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' flat_fee_amount = ' . $this->sqlFlatFeeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_fee_amount = ' . $this->sqlBillingFeeAmount(). ',' ; } elseif( true == array_key_exists( 'BillingFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' billing_fee_amount = ' . $this->sqlBillingFeeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_fee_percent = ' . $this->sqlBillingFeePercent(). ',' ; } elseif( true == array_key_exists( 'BillingFeePercent', $this->getChangedColumns() ) ) { $strSql .= ' billing_fee_percent = ' . $this->sqlBillingFeePercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_billing_amount = ' . $this->sqlBudgetBillingAmount(). ',' ; } elseif( true == array_key_exists( 'BudgetBillingAmount', $this->getChangedColumns() ) ) { $strSql .= ' budget_billing_amount = ' . $this->sqlBudgetBillingAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recovery_percentage = ' . $this->sqlRecoveryPercentage(). ',' ; } elseif( true == array_key_exists( 'RecoveryPercentage', $this->getChangedColumns() ) ) { $strSql .= ' recovery_percentage = ' . $this->sqlRecoveryPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocate_to_vacant_units = ' . $this->sqlAllocateToVacantUnits(). ',' ; } elseif( true == array_key_exists( 'AllocateToVacantUnits', $this->getChangedColumns() ) ) { $strSql .= ' allocate_to_vacant_units = ' . $this->sqlAllocateToVacantUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignore_renewals = ' . $this->sqlIgnoreRenewals(). ',' ; } elseif( true == array_key_exists( 'IgnoreRenewals', $this->getChangedColumns() ) ) { $strSql .= ' ignore_renewals = ' . $this->sqlIgnoreRenewals() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'utility_rubs_formula_id' => $this->getUtilityRubsFormulaId(),
			'subsidy_ar_code_id' => $this->getSubsidyArCodeId(),
			'new_property_utility_type_rate_id' => $this->getNewPropertyUtilityTypeRateId(),
			'lease_start_date_criteria' => $this->getLeaseStartDateCriteria(),
			'lease_end_date_criteria' => $this->getLeaseEndDateCriteria(),
			'common_area_deduction' => $this->getCommonAreaDeduction(),
			'move_in_fee' => $this->getMoveInFee(),
			'termination_fee' => $this->getTerminationFee(),
			'flat_fee_amount' => $this->getFlatFeeAmount(),
			'billing_fee_amount' => $this->getBillingFeeAmount(),
			'billing_fee_percent' => $this->getBillingFeePercent(),
			'budget_billing_amount' => $this->getBudgetBillingAmount(),
			'recovery_percentage' => $this->getRecoveryPercentage(),
			'allocate_to_vacant_units' => $this->getAllocateToVacantUnits(),
			'ignore_renewals' => $this->getIgnoreRenewals(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>