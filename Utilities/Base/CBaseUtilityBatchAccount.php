<?php

class CBaseUtilityBatchAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_batch_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUtilityBatchId;
	protected $m_intUtilityAccountId;
	protected $m_intUnitTypeId;
	protected $m_intPropertyUnitId;
	protected $m_arrintUnitSpaceIds;
	protected $m_fltSquareFeet;
	protected $m_intBedroomCount;
	protected $m_strOriginalLeaseStartDate;
	protected $m_strLeaseStartDate;
	protected $m_strOriginalMoveInDate;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_intOccupantCount;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strExceptionDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityAccountId', trim( $arrValues['utility_account_id'] ) ); elseif( isset( $arrValues['utility_account_id'] ) ) $this->setUtilityAccountId( $arrValues['utility_account_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintUnitSpaceIds', trim( $arrValues['unit_space_ids'] ) ); elseif( isset( $arrValues['unit_space_ids'] ) ) $this->setUnitSpaceIds( $arrValues['unit_space_ids'] );
		if( isset( $arrValues['square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltSquareFeet', trim( $arrValues['square_feet'] ) ); elseif( isset( $arrValues['square_feet'] ) ) $this->setSquareFeet( $arrValues['square_feet'] );
		if( isset( $arrValues['bedroom_count'] ) && $boolDirectSet ) $this->set( 'm_intBedroomCount', trim( $arrValues['bedroom_count'] ) ); elseif( isset( $arrValues['bedroom_count'] ) ) $this->setBedroomCount( $arrValues['bedroom_count'] );
		if( isset( $arrValues['original_lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalLeaseStartDate', trim( $arrValues['original_lease_start_date'] ) ); elseif( isset( $arrValues['original_lease_start_date'] ) ) $this->setOriginalLeaseStartDate( $arrValues['original_lease_start_date'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['original_move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalMoveInDate', trim( $arrValues['original_move_in_date'] ) ); elseif( isset( $arrValues['original_move_in_date'] ) ) $this->setOriginalMoveInDate( $arrValues['original_move_in_date'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['occupant_count'] ) && $boolDirectSet ) $this->set( 'm_intOccupantCount', trim( $arrValues['occupant_count'] ) ); elseif( isset( $arrValues['occupant_count'] ) ) $this->setOccupantCount( $arrValues['occupant_count'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['exception_details'] ) && $boolDirectSet ) $this->set( 'm_strExceptionDetails', trim( stripcslashes( $arrValues['exception_details'] ) ) ); elseif( isset( $arrValues['exception_details'] ) ) $this->setExceptionDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exception_details'] ) : $arrValues['exception_details'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setUtilityAccountId( $intUtilityAccountId ) {
		$this->set( 'm_intUtilityAccountId', CStrings::strToIntDef( $intUtilityAccountId, NULL, false ) );
	}

	public function getUtilityAccountId() {
		return $this->m_intUtilityAccountId;
	}

	public function sqlUtilityAccountId() {
		return ( true == isset( $this->m_intUtilityAccountId ) ) ? ( string ) $this->m_intUtilityAccountId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceIds( $arrintUnitSpaceIds ) {
		$this->set( 'm_arrintUnitSpaceIds', CStrings::strToArrIntDef( $arrintUnitSpaceIds, NULL ) );
	}

	public function getUnitSpaceIds() {
		return $this->m_arrintUnitSpaceIds;
	}

	public function sqlUnitSpaceIds() {
		return ( true == isset( $this->m_arrintUnitSpaceIds ) && true == valArr( $this->m_arrintUnitSpaceIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintUnitSpaceIds, NULL ) . '\'' : 'NULL';
	}

	public function setSquareFeet( $fltSquareFeet ) {
		$this->set( 'm_fltSquareFeet', CStrings::strToFloatDef( $fltSquareFeet, NULL, false, 4 ) );
	}

	public function getSquareFeet() {
		return $this->m_fltSquareFeet;
	}

	public function sqlSquareFeet() {
		return ( true == isset( $this->m_fltSquareFeet ) ) ? ( string ) $this->m_fltSquareFeet : 'NULL';
	}

	public function setBedroomCount( $intBedroomCount ) {
		$this->set( 'm_intBedroomCount', CStrings::strToIntDef( $intBedroomCount, NULL, false ) );
	}

	public function getBedroomCount() {
		return $this->m_intBedroomCount;
	}

	public function sqlBedroomCount() {
		return ( true == isset( $this->m_intBedroomCount ) ) ? ( string ) $this->m_intBedroomCount : 'NULL';
	}

	public function setOriginalLeaseStartDate( $strOriginalLeaseStartDate ) {
		$this->set( 'm_strOriginalLeaseStartDate', CStrings::strTrimDef( $strOriginalLeaseStartDate, -1, NULL, true ) );
	}

	public function getOriginalLeaseStartDate() {
		return $this->m_strOriginalLeaseStartDate;
	}

	public function sqlOriginalLeaseStartDate() {
		return ( true == isset( $this->m_strOriginalLeaseStartDate ) ) ? '\'' . $this->m_strOriginalLeaseStartDate . '\'' : 'NULL';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function setOriginalMoveInDate( $strOriginalMoveInDate ) {
		$this->set( 'm_strOriginalMoveInDate', CStrings::strTrimDef( $strOriginalMoveInDate, -1, NULL, true ) );
	}

	public function getOriginalMoveInDate() {
		return $this->m_strOriginalMoveInDate;
	}

	public function sqlOriginalMoveInDate() {
		return ( true == isset( $this->m_strOriginalMoveInDate ) ) ? '\'' . $this->m_strOriginalMoveInDate . '\'' : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setOccupantCount( $intOccupantCount ) {
		$this->set( 'm_intOccupantCount', CStrings::strToIntDef( $intOccupantCount, NULL, false ) );
	}

	public function getOccupantCount() {
		return $this->m_intOccupantCount;
	}

	public function sqlOccupantCount() {
		return ( true == isset( $this->m_intOccupantCount ) ) ? ( string ) $this->m_intOccupantCount : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setExceptionDetails( $strExceptionDetails ) {
		$this->set( 'm_strExceptionDetails', CStrings::strTrimDef( $strExceptionDetails, -1, NULL, true ) );
	}

	public function getExceptionDetails() {
		return $this->m_strExceptionDetails;
	}

	public function sqlExceptionDetails() {
		return ( true == isset( $this->m_strExceptionDetails ) ) ? '\'' . addslashes( $this->m_strExceptionDetails ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, utility_batch_id, utility_account_id, unit_type_id, property_unit_id, unit_space_ids, square_feet, bedroom_count, original_lease_start_date, lease_start_date, original_move_in_date, move_in_date, move_out_date, occupant_count, created_on, created_by, updated_on, updated_by, exception_details )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUtilityBatchId() . ', ' .
 						$this->sqlUtilityAccountId() . ', ' .
 						$this->sqlUnitTypeId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlUnitSpaceIds() . ', ' .
 						$this->sqlSquareFeet() . ', ' .
 						$this->sqlBedroomCount() . ', ' .
 						$this->sqlOriginalLeaseStartDate() . ', ' .
 						$this->sqlLeaseStartDate() . ', ' .
 						$this->sqlOriginalMoveInDate() . ', ' .
 						$this->sqlMoveInDate() . ', ' .
 						$this->sqlMoveOutDate() . ', ' .
 						$this->sqlOccupantCount() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlExceptionDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; } elseif( true == array_key_exists( 'UtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_account_id = ' . $this->sqlUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_ids = ' . $this->sqlUnitSpaceIds() . ','; } elseif( true == array_key_exists( 'UnitSpaceIds', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_ids = ' . $this->sqlUnitSpaceIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet() . ','; } elseif( true == array_key_exists( 'SquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bedroom_count = ' . $this->sqlBedroomCount() . ','; } elseif( true == array_key_exists( 'BedroomCount', $this->getChangedColumns() ) ) { $strSql .= ' bedroom_count = ' . $this->sqlBedroomCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_lease_start_date = ' . $this->sqlOriginalLeaseStartDate() . ','; } elseif( true == array_key_exists( 'OriginalLeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' original_lease_start_date = ' . $this->sqlOriginalLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_move_in_date = ' . $this->sqlOriginalMoveInDate() . ','; } elseif( true == array_key_exists( 'OriginalMoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' original_move_in_date = ' . $this->sqlOriginalMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount() . ','; } elseif( true == array_key_exists( 'OccupantCount', $this->getChangedColumns() ) ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exception_details = ' . $this->sqlExceptionDetails() . ','; } elseif( true == array_key_exists( 'ExceptionDetails', $this->getChangedColumns() ) ) { $strSql .= ' exception_details = ' . $this->sqlExceptionDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'utility_account_id' => $this->getUtilityAccountId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_ids' => $this->getUnitSpaceIds(),
			'square_feet' => $this->getSquareFeet(),
			'bedroom_count' => $this->getBedroomCount(),
			'original_lease_start_date' => $this->getOriginalLeaseStartDate(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'original_move_in_date' => $this->getOriginalMoveInDate(),
			'move_in_date' => $this->getMoveInDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'occupant_count' => $this->getOccupantCount(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'exception_details' => $this->getExceptionDetails()
		);
	}

}
?>