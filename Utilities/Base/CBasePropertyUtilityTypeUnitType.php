<?php

class CBasePropertyUtilityTypeUnitType extends CEosSingularBase {

	const TABLE_NAME = 'public.property_utility_type_unit_types';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUtilityRubsFormulaId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intPropertyUtilityTypeRateId;
	protected $m_intPropertyUtilityTypeRateMethodId;
	protected $m_intPropertyUtilityTypeFactorRuleId;
	protected $m_intUnitTypeId;
	protected $m_fltFactor;
	protected $m_fltFixedAmount;
	protected $m_boolIsFixedAmount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltFactor = '0';
		$this->m_fltFixedAmount = '0';
		$this->m_boolIsFixedAmount = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['utility_rubs_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityRubsFormulaId', trim( $arrValues['utility_rubs_formula_id'] ) ); elseif( isset( $arrValues['utility_rubs_formula_id'] ) ) $this->setUtilityRubsFormulaId( $arrValues['utility_rubs_formula_id'] );
		if( isset( $arrValues['property_utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeId', trim( $arrValues['property_utility_type_id'] ) ); elseif( isset( $arrValues['property_utility_type_id'] ) ) $this->setPropertyUtilityTypeId( $arrValues['property_utility_type_id'] );
		if( isset( $arrValues['property_utility_type_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeRateId', trim( $arrValues['property_utility_type_rate_id'] ) ); elseif( isset( $arrValues['property_utility_type_rate_id'] ) ) $this->setPropertyUtilityTypeRateId( $arrValues['property_utility_type_rate_id'] );
		if( isset( $arrValues['property_utility_type_rate_method_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeRateMethodId', trim( $arrValues['property_utility_type_rate_method_id'] ) ); elseif( isset( $arrValues['property_utility_type_rate_method_id'] ) ) $this->setPropertyUtilityTypeRateMethodId( $arrValues['property_utility_type_rate_method_id'] );
		if( isset( $arrValues['property_utility_type_factor_rule_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeFactorRuleId', trim( $arrValues['property_utility_type_factor_rule_id'] ) ); elseif( isset( $arrValues['property_utility_type_factor_rule_id'] ) ) $this->setPropertyUtilityTypeFactorRuleId( $arrValues['property_utility_type_factor_rule_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['factor'] ) && $boolDirectSet ) $this->set( 'm_fltFactor', trim( $arrValues['factor'] ) ); elseif( isset( $arrValues['factor'] ) ) $this->setFactor( $arrValues['factor'] );
		if( isset( $arrValues['fixed_amount'] ) && $boolDirectSet ) $this->set( 'm_fltFixedAmount', trim( $arrValues['fixed_amount'] ) ); elseif( isset( $arrValues['fixed_amount'] ) ) $this->setFixedAmount( $arrValues['fixed_amount'] );
		if( isset( $arrValues['is_fixed_amount'] ) && $boolDirectSet ) $this->set( 'm_boolIsFixedAmount', trim( stripcslashes( $arrValues['is_fixed_amount'] ) ) ); elseif( isset( $arrValues['is_fixed_amount'] ) ) $this->setIsFixedAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_fixed_amount'] ) : $arrValues['is_fixed_amount'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUtilityRubsFormulaId( $intUtilityRubsFormulaId ) {
		$this->set( 'm_intUtilityRubsFormulaId', CStrings::strToIntDef( $intUtilityRubsFormulaId, NULL, false ) );
	}

	public function getUtilityRubsFormulaId() {
		return $this->m_intUtilityRubsFormulaId;
	}

	public function sqlUtilityRubsFormulaId() {
		return ( true == isset( $this->m_intUtilityRubsFormulaId ) ) ? ( string ) $this->m_intUtilityRubsFormulaId : 'NULL';
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->set( 'm_intPropertyUtilityTypeId', CStrings::strToIntDef( $intPropertyUtilityTypeId, NULL, false ) );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function sqlPropertyUtilityTypeId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeId ) ) ? ( string ) $this->m_intPropertyUtilityTypeId : 'NULL';
	}

	public function setPropertyUtilityTypeRateId( $intPropertyUtilityTypeRateId ) {
		$this->set( 'm_intPropertyUtilityTypeRateId', CStrings::strToIntDef( $intPropertyUtilityTypeRateId, NULL, false ) );
	}

	public function getPropertyUtilityTypeRateId() {
		return $this->m_intPropertyUtilityTypeRateId;
	}

	public function sqlPropertyUtilityTypeRateId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeRateId ) ) ? ( string ) $this->m_intPropertyUtilityTypeRateId : 'NULL';
	}

	public function setPropertyUtilityTypeRateMethodId( $intPropertyUtilityTypeRateMethodId ) {
		$this->set( 'm_intPropertyUtilityTypeRateMethodId', CStrings::strToIntDef( $intPropertyUtilityTypeRateMethodId, NULL, false ) );
	}

	public function getPropertyUtilityTypeRateMethodId() {
		return $this->m_intPropertyUtilityTypeRateMethodId;
	}

	public function sqlPropertyUtilityTypeRateMethodId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeRateMethodId ) ) ? ( string ) $this->m_intPropertyUtilityTypeRateMethodId : 'NULL';
	}

	public function setPropertyUtilityTypeFactorRuleId( $intPropertyUtilityTypeFactorRuleId ) {
		$this->set( 'm_intPropertyUtilityTypeFactorRuleId', CStrings::strToIntDef( $intPropertyUtilityTypeFactorRuleId, NULL, false ) );
	}

	public function getPropertyUtilityTypeFactorRuleId() {
		return $this->m_intPropertyUtilityTypeFactorRuleId;
	}

	public function sqlPropertyUtilityTypeFactorRuleId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeFactorRuleId ) ) ? ( string ) $this->m_intPropertyUtilityTypeFactorRuleId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setFactor( $fltFactor ) {
		$this->set( 'm_fltFactor', CStrings::strToFloatDef( $fltFactor, NULL, false, 4 ) );
	}

	public function getFactor() {
		return $this->m_fltFactor;
	}

	public function sqlFactor() {
		return ( true == isset( $this->m_fltFactor ) ) ? ( string ) $this->m_fltFactor : '0';
	}

	public function setFixedAmount( $fltFixedAmount ) {
		$this->set( 'm_fltFixedAmount', CStrings::strToFloatDef( $fltFixedAmount, NULL, false, 2 ) );
	}

	public function getFixedAmount() {
		return $this->m_fltFixedAmount;
	}

	public function sqlFixedAmount() {
		return ( true == isset( $this->m_fltFixedAmount ) ) ? ( string ) $this->m_fltFixedAmount : '0';
	}

	public function setIsFixedAmount( $boolIsFixedAmount ) {
		$this->set( 'm_boolIsFixedAmount', CStrings::strToBool( $boolIsFixedAmount ) );
	}

	public function getIsFixedAmount() {
		return $this->m_boolIsFixedAmount;
	}

	public function sqlIsFixedAmount() {
		return ( true == isset( $this->m_boolIsFixedAmount ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFixedAmount ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, utility_rubs_formula_id, property_utility_type_id, property_utility_type_rate_id, property_utility_type_rate_method_id, property_utility_type_factor_rule_id, unit_type_id, factor, fixed_amount, is_fixed_amount, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlUtilityRubsFormulaId() . ', ' .
 						$this->sqlPropertyUtilityTypeId() . ', ' .
 						$this->sqlPropertyUtilityTypeRateId() . ', ' .
 						$this->sqlPropertyUtilityTypeRateMethodId() . ', ' .
 						$this->sqlPropertyUtilityTypeFactorRuleId() . ', ' .
 						$this->sqlUnitTypeId() . ', ' .
 						$this->sqlFactor() . ', ' .
 						$this->sqlFixedAmount() . ', ' .
 						$this->sqlIsFixedAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_rubs_formula_id = ' . $this->sqlUtilityRubsFormulaId() . ','; } elseif( true == array_key_exists( 'UtilityRubsFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' utility_rubs_formula_id = ' . $this->sqlUtilityRubsFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_id = ' . $this->sqlPropertyUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_rate_id = ' . $this->sqlPropertyUtilityTypeRateId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeRateId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_rate_id = ' . $this->sqlPropertyUtilityTypeRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_rate_method_id = ' . $this->sqlPropertyUtilityTypeRateMethodId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeRateMethodId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_rate_method_id = ' . $this->sqlPropertyUtilityTypeRateMethodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_factor_rule_id = ' . $this->sqlPropertyUtilityTypeFactorRuleId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeFactorRuleId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_factor_rule_id = ' . $this->sqlPropertyUtilityTypeFactorRuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' factor = ' . $this->sqlFactor() . ','; } elseif( true == array_key_exists( 'Factor', $this->getChangedColumns() ) ) { $strSql .= ' factor = ' . $this->sqlFactor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fixed_amount = ' . $this->sqlFixedAmount() . ','; } elseif( true == array_key_exists( 'FixedAmount', $this->getChangedColumns() ) ) { $strSql .= ' fixed_amount = ' . $this->sqlFixedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_fixed_amount = ' . $this->sqlIsFixedAmount() . ','; } elseif( true == array_key_exists( 'IsFixedAmount', $this->getChangedColumns() ) ) { $strSql .= ' is_fixed_amount = ' . $this->sqlIsFixedAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'utility_rubs_formula_id' => $this->getUtilityRubsFormulaId(),
			'property_utility_type_id' => $this->getPropertyUtilityTypeId(),
			'property_utility_type_rate_id' => $this->getPropertyUtilityTypeRateId(),
			'property_utility_type_rate_method_id' => $this->getPropertyUtilityTypeRateMethodId(),
			'property_utility_type_factor_rule_id' => $this->getPropertyUtilityTypeFactorRuleId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'factor' => $this->getFactor(),
			'fixed_amount' => $this->getFixedAmount(),
			'is_fixed_amount' => $this->getIsFixedAmount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>