<?php

class CBasePropertyVcrTypeRate extends CEosSingularBase {

	const TABLE_NAME = 'public.property_vcr_type_rates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyVcrSettingId;
	protected $m_intFirstVcrServiceFeeTypeId;
	protected $m_intRapidVcrServiceFeeTypeId;
	protected $m_intGraceDays;
	protected $m_intWaiveUsageDuringGraceDays;
	protected $m_intFirstBillVcrSeparately;
	protected $m_intBillConsecutiveVcrViolations;
	protected $m_intRapidLeaseDefinitionDays;
	protected $m_intMoveOutEstimatedChargesDays;
	protected $m_fltUsageWaiverAmount;
	protected $m_fltNoFeeThreshhold;
	protected $m_fltFirstFixedServiceFee;
	protected $m_fltFirstDailyServiceFee;
	protected $m_fltFirstDailyServiceCap;
	protected $m_fltFirstDay1ServiceFee;
	protected $m_fltFirstDay2ServiceFee;
	protected $m_fltFirstDay3ServiceFee;
	protected $m_fltFirstDay4ServiceFee;
	protected $m_fltFirstDay5ServiceFee;
	protected $m_fltFirstDay6ServiceFee;
	protected $m_fltFirstDay7ServiceFee;
	protected $m_fltRapidFixedServiceFee;
	protected $m_fltRapidDailyServiceFee;
	protected $m_fltRapidDailyServiceCap;
	protected $m_fltRapidDay1ServiceFee;
	protected $m_fltRapidDay2ServiceFee;
	protected $m_fltRapidDay3ServiceFee;
	protected $m_fltRapidDay4ServiceFee;
	protected $m_fltRapidDay5ServiceFee;
	protected $m_fltRapidDay6ServiceFee;
	protected $m_fltRapidDay7ServiceFee;
	protected $m_boolIsIgnoreRenewals;
	protected $m_boolIsHideMoveOutEstimatedCharges;
	protected $m_boolWaiveFirstVcrFee;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strVcrNoticeText;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intNewPropertyVcrTypeRateId;

	public function __construct() {
		parent::__construct();

		$this->m_intGraceDays = '1';
		$this->m_boolIsIgnoreRenewals = false;
		$this->m_boolIsHideMoveOutEstimatedCharges = false;
		$this->m_boolWaiveFirstVcrFee = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_vcr_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyVcrSettingId', trim( $arrValues['property_vcr_setting_id'] ) ); elseif( isset( $arrValues['property_vcr_setting_id'] ) ) $this->setPropertyVcrSettingId( $arrValues['property_vcr_setting_id'] );
		if( isset( $arrValues['first_vcr_service_fee_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstVcrServiceFeeTypeId', trim( $arrValues['first_vcr_service_fee_type_id'] ) ); elseif( isset( $arrValues['first_vcr_service_fee_type_id'] ) ) $this->setFirstVcrServiceFeeTypeId( $arrValues['first_vcr_service_fee_type_id'] );
		if( isset( $arrValues['rapid_vcr_service_fee_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRapidVcrServiceFeeTypeId', trim( $arrValues['rapid_vcr_service_fee_type_id'] ) ); elseif( isset( $arrValues['rapid_vcr_service_fee_type_id'] ) ) $this->setRapidVcrServiceFeeTypeId( $arrValues['rapid_vcr_service_fee_type_id'] );
		if( isset( $arrValues['grace_days'] ) && $boolDirectSet ) $this->set( 'm_intGraceDays', trim( $arrValues['grace_days'] ) ); elseif( isset( $arrValues['grace_days'] ) ) $this->setGraceDays( $arrValues['grace_days'] );
		if( isset( $arrValues['waive_usage_during_grace_days'] ) && $boolDirectSet ) $this->set( 'm_intWaiveUsageDuringGraceDays', trim( $arrValues['waive_usage_during_grace_days'] ) ); elseif( isset( $arrValues['waive_usage_during_grace_days'] ) ) $this->setWaiveUsageDuringGraceDays( $arrValues['waive_usage_during_grace_days'] );
		if( isset( $arrValues['first_bill_vcr_separately'] ) && $boolDirectSet ) $this->set( 'm_intFirstBillVcrSeparately', trim( $arrValues['first_bill_vcr_separately'] ) ); elseif( isset( $arrValues['first_bill_vcr_separately'] ) ) $this->setFirstBillVcrSeparately( $arrValues['first_bill_vcr_separately'] );
		if( isset( $arrValues['bill_consecutive_vcr_violations'] ) && $boolDirectSet ) $this->set( 'm_intBillConsecutiveVcrViolations', trim( $arrValues['bill_consecutive_vcr_violations'] ) ); elseif( isset( $arrValues['bill_consecutive_vcr_violations'] ) ) $this->setBillConsecutiveVcrViolations( $arrValues['bill_consecutive_vcr_violations'] );
		if( isset( $arrValues['rapid_lease_definition_days'] ) && $boolDirectSet ) $this->set( 'm_intRapidLeaseDefinitionDays', trim( $arrValues['rapid_lease_definition_days'] ) ); elseif( isset( $arrValues['rapid_lease_definition_days'] ) ) $this->setRapidLeaseDefinitionDays( $arrValues['rapid_lease_definition_days'] );
		if( isset( $arrValues['move_out_estimated_charges_days'] ) && $boolDirectSet ) $this->set( 'm_intMoveOutEstimatedChargesDays', trim( $arrValues['move_out_estimated_charges_days'] ) ); elseif( isset( $arrValues['move_out_estimated_charges_days'] ) ) $this->setMoveOutEstimatedChargesDays( $arrValues['move_out_estimated_charges_days'] );
		if( isset( $arrValues['usage_waiver_amount'] ) && $boolDirectSet ) $this->set( 'm_fltUsageWaiverAmount', trim( $arrValues['usage_waiver_amount'] ) ); elseif( isset( $arrValues['usage_waiver_amount'] ) ) $this->setUsageWaiverAmount( $arrValues['usage_waiver_amount'] );
		if( isset( $arrValues['no_fee_threshhold'] ) && $boolDirectSet ) $this->set( 'm_fltNoFeeThreshhold', trim( $arrValues['no_fee_threshhold'] ) ); elseif( isset( $arrValues['no_fee_threshhold'] ) ) $this->setNoFeeThreshhold( $arrValues['no_fee_threshhold'] );
		if( isset( $arrValues['first_fixed_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFirstFixedServiceFee', trim( $arrValues['first_fixed_service_fee'] ) ); elseif( isset( $arrValues['first_fixed_service_fee'] ) ) $this->setFirstFixedServiceFee( $arrValues['first_fixed_service_fee'] );
		if( isset( $arrValues['first_daily_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFirstDailyServiceFee', trim( $arrValues['first_daily_service_fee'] ) ); elseif( isset( $arrValues['first_daily_service_fee'] ) ) $this->setFirstDailyServiceFee( $arrValues['first_daily_service_fee'] );
		if( isset( $arrValues['first_daily_service_cap'] ) && $boolDirectSet ) $this->set( 'm_fltFirstDailyServiceCap', trim( $arrValues['first_daily_service_cap'] ) ); elseif( isset( $arrValues['first_daily_service_cap'] ) ) $this->setFirstDailyServiceCap( $arrValues['first_daily_service_cap'] );
		if( isset( $arrValues['first_day1_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFirstDay1ServiceFee', trim( $arrValues['first_day1_service_fee'] ) ); elseif( isset( $arrValues['first_day1_service_fee'] ) ) $this->setFirstDay1ServiceFee( $arrValues['first_day1_service_fee'] );
		if( isset( $arrValues['first_day2_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFirstDay2ServiceFee', trim( $arrValues['first_day2_service_fee'] ) ); elseif( isset( $arrValues['first_day2_service_fee'] ) ) $this->setFirstDay2ServiceFee( $arrValues['first_day2_service_fee'] );
		if( isset( $arrValues['first_day3_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFirstDay3ServiceFee', trim( $arrValues['first_day3_service_fee'] ) ); elseif( isset( $arrValues['first_day3_service_fee'] ) ) $this->setFirstDay3ServiceFee( $arrValues['first_day3_service_fee'] );
		if( isset( $arrValues['first_day4_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFirstDay4ServiceFee', trim( $arrValues['first_day4_service_fee'] ) ); elseif( isset( $arrValues['first_day4_service_fee'] ) ) $this->setFirstDay4ServiceFee( $arrValues['first_day4_service_fee'] );
		if( isset( $arrValues['first_day5_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFirstDay5ServiceFee', trim( $arrValues['first_day5_service_fee'] ) ); elseif( isset( $arrValues['first_day5_service_fee'] ) ) $this->setFirstDay5ServiceFee( $arrValues['first_day5_service_fee'] );
		if( isset( $arrValues['first_day6_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFirstDay6ServiceFee', trim( $arrValues['first_day6_service_fee'] ) ); elseif( isset( $arrValues['first_day6_service_fee'] ) ) $this->setFirstDay6ServiceFee( $arrValues['first_day6_service_fee'] );
		if( isset( $arrValues['first_day7_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFirstDay7ServiceFee', trim( $arrValues['first_day7_service_fee'] ) ); elseif( isset( $arrValues['first_day7_service_fee'] ) ) $this->setFirstDay7ServiceFee( $arrValues['first_day7_service_fee'] );
		if( isset( $arrValues['rapid_fixed_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltRapidFixedServiceFee', trim( $arrValues['rapid_fixed_service_fee'] ) ); elseif( isset( $arrValues['rapid_fixed_service_fee'] ) ) $this->setRapidFixedServiceFee( $arrValues['rapid_fixed_service_fee'] );
		if( isset( $arrValues['rapid_daily_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltRapidDailyServiceFee', trim( $arrValues['rapid_daily_service_fee'] ) ); elseif( isset( $arrValues['rapid_daily_service_fee'] ) ) $this->setRapidDailyServiceFee( $arrValues['rapid_daily_service_fee'] );
		if( isset( $arrValues['rapid_daily_service_cap'] ) && $boolDirectSet ) $this->set( 'm_fltRapidDailyServiceCap', trim( $arrValues['rapid_daily_service_cap'] ) ); elseif( isset( $arrValues['rapid_daily_service_cap'] ) ) $this->setRapidDailyServiceCap( $arrValues['rapid_daily_service_cap'] );
		if( isset( $arrValues['rapid_day1_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltRapidDay1ServiceFee', trim( $arrValues['rapid_day1_service_fee'] ) ); elseif( isset( $arrValues['rapid_day1_service_fee'] ) ) $this->setRapidDay1ServiceFee( $arrValues['rapid_day1_service_fee'] );
		if( isset( $arrValues['rapid_day2_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltRapidDay2ServiceFee', trim( $arrValues['rapid_day2_service_fee'] ) ); elseif( isset( $arrValues['rapid_day2_service_fee'] ) ) $this->setRapidDay2ServiceFee( $arrValues['rapid_day2_service_fee'] );
		if( isset( $arrValues['rapid_day3_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltRapidDay3ServiceFee', trim( $arrValues['rapid_day3_service_fee'] ) ); elseif( isset( $arrValues['rapid_day3_service_fee'] ) ) $this->setRapidDay3ServiceFee( $arrValues['rapid_day3_service_fee'] );
		if( isset( $arrValues['rapid_day4_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltRapidDay4ServiceFee', trim( $arrValues['rapid_day4_service_fee'] ) ); elseif( isset( $arrValues['rapid_day4_service_fee'] ) ) $this->setRapidDay4ServiceFee( $arrValues['rapid_day4_service_fee'] );
		if( isset( $arrValues['rapid_day5_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltRapidDay5ServiceFee', trim( $arrValues['rapid_day5_service_fee'] ) ); elseif( isset( $arrValues['rapid_day5_service_fee'] ) ) $this->setRapidDay5ServiceFee( $arrValues['rapid_day5_service_fee'] );
		if( isset( $arrValues['rapid_day6_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltRapidDay6ServiceFee', trim( $arrValues['rapid_day6_service_fee'] ) ); elseif( isset( $arrValues['rapid_day6_service_fee'] ) ) $this->setRapidDay6ServiceFee( $arrValues['rapid_day6_service_fee'] );
		if( isset( $arrValues['rapid_day7_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltRapidDay7ServiceFee', trim( $arrValues['rapid_day7_service_fee'] ) ); elseif( isset( $arrValues['rapid_day7_service_fee'] ) ) $this->setRapidDay7ServiceFee( $arrValues['rapid_day7_service_fee'] );
		if( isset( $arrValues['is_ignore_renewals'] ) && $boolDirectSet ) $this->set( 'm_boolIsIgnoreRenewals', trim( stripcslashes( $arrValues['is_ignore_renewals'] ) ) ); elseif( isset( $arrValues['is_ignore_renewals'] ) ) $this->setIsIgnoreRenewals( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ignore_renewals'] ) : $arrValues['is_ignore_renewals'] );
		if( isset( $arrValues['is_hide_move_out_estimated_charges'] ) && $boolDirectSet ) $this->set( 'm_boolIsHideMoveOutEstimatedCharges', trim( stripcslashes( $arrValues['is_hide_move_out_estimated_charges'] ) ) ); elseif( isset( $arrValues['is_hide_move_out_estimated_charges'] ) ) $this->setIsHideMoveOutEstimatedCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hide_move_out_estimated_charges'] ) : $arrValues['is_hide_move_out_estimated_charges'] );
		if( isset( $arrValues['waive_first_vcr_fee'] ) && $boolDirectSet ) $this->set( 'm_boolWaiveFirstVcrFee', trim( stripcslashes( $arrValues['waive_first_vcr_fee'] ) ) ); elseif( isset( $arrValues['waive_first_vcr_fee'] ) ) $this->setWaiveFirstVcrFee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['waive_first_vcr_fee'] ) : $arrValues['waive_first_vcr_fee'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['vcr_notice_text'] ) && $boolDirectSet ) $this->set( 'm_strVcrNoticeText', trim( stripcslashes( $arrValues['vcr_notice_text'] ) ) ); elseif( isset( $arrValues['vcr_notice_text'] ) ) $this->setVcrNoticeText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vcr_notice_text'] ) : $arrValues['vcr_notice_text'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['new_property_vcr_type_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertyVcrTypeRateId', trim( $arrValues['new_property_vcr_type_rate_id'] ) ); elseif( isset( $arrValues['new_property_vcr_type_rate_id'] ) ) $this->setNewPropertyVcrTypeRateId( $arrValues['new_property_vcr_type_rate_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyVcrSettingId( $intPropertyVcrSettingId ) {
		$this->set( 'm_intPropertyVcrSettingId', CStrings::strToIntDef( $intPropertyVcrSettingId, NULL, false ) );
	}

	public function getPropertyVcrSettingId() {
		return $this->m_intPropertyVcrSettingId;
	}

	public function sqlPropertyVcrSettingId() {
		return ( true == isset( $this->m_intPropertyVcrSettingId ) ) ? ( string ) $this->m_intPropertyVcrSettingId : 'NULL';
	}

	public function setFirstVcrServiceFeeTypeId( $intFirstVcrServiceFeeTypeId ) {
		$this->set( 'm_intFirstVcrServiceFeeTypeId', CStrings::strToIntDef( $intFirstVcrServiceFeeTypeId, NULL, false ) );
	}

	public function getFirstVcrServiceFeeTypeId() {
		return $this->m_intFirstVcrServiceFeeTypeId;
	}

	public function sqlFirstVcrServiceFeeTypeId() {
		return ( true == isset( $this->m_intFirstVcrServiceFeeTypeId ) ) ? ( string ) $this->m_intFirstVcrServiceFeeTypeId : 'NULL';
	}

	public function setRapidVcrServiceFeeTypeId( $intRapidVcrServiceFeeTypeId ) {
		$this->set( 'm_intRapidVcrServiceFeeTypeId', CStrings::strToIntDef( $intRapidVcrServiceFeeTypeId, NULL, false ) );
	}

	public function getRapidVcrServiceFeeTypeId() {
		return $this->m_intRapidVcrServiceFeeTypeId;
	}

	public function sqlRapidVcrServiceFeeTypeId() {
		return ( true == isset( $this->m_intRapidVcrServiceFeeTypeId ) ) ? ( string ) $this->m_intRapidVcrServiceFeeTypeId : 'NULL';
	}

	public function setGraceDays( $intGraceDays ) {
		$this->set( 'm_intGraceDays', CStrings::strToIntDef( $intGraceDays, NULL, false ) );
	}

	public function getGraceDays() {
		return $this->m_intGraceDays;
	}

	public function sqlGraceDays() {
		return ( true == isset( $this->m_intGraceDays ) ) ? ( string ) $this->m_intGraceDays : '1';
	}

	public function setWaiveUsageDuringGraceDays( $intWaiveUsageDuringGraceDays ) {
		$this->set( 'm_intWaiveUsageDuringGraceDays', CStrings::strToIntDef( $intWaiveUsageDuringGraceDays, NULL, false ) );
	}

	public function getWaiveUsageDuringGraceDays() {
		return $this->m_intWaiveUsageDuringGraceDays;
	}

	public function sqlWaiveUsageDuringGraceDays() {
		return ( true == isset( $this->m_intWaiveUsageDuringGraceDays ) ) ? ( string ) $this->m_intWaiveUsageDuringGraceDays : 'NULL';
	}

	public function setFirstBillVcrSeparately( $intFirstBillVcrSeparately ) {
		$this->set( 'm_intFirstBillVcrSeparately', CStrings::strToIntDef( $intFirstBillVcrSeparately, NULL, false ) );
	}

	public function getFirstBillVcrSeparately() {
		return $this->m_intFirstBillVcrSeparately;
	}

	public function sqlFirstBillVcrSeparately() {
		return ( true == isset( $this->m_intFirstBillVcrSeparately ) ) ? ( string ) $this->m_intFirstBillVcrSeparately : 'NULL';
	}

	public function setBillConsecutiveVcrViolations( $intBillConsecutiveVcrViolations ) {
		$this->set( 'm_intBillConsecutiveVcrViolations', CStrings::strToIntDef( $intBillConsecutiveVcrViolations, NULL, false ) );
	}

	public function getBillConsecutiveVcrViolations() {
		return $this->m_intBillConsecutiveVcrViolations;
	}

	public function sqlBillConsecutiveVcrViolations() {
		return ( true == isset( $this->m_intBillConsecutiveVcrViolations ) ) ? ( string ) $this->m_intBillConsecutiveVcrViolations : 'NULL';
	}

	public function setRapidLeaseDefinitionDays( $intRapidLeaseDefinitionDays ) {
		$this->set( 'm_intRapidLeaseDefinitionDays', CStrings::strToIntDef( $intRapidLeaseDefinitionDays, NULL, false ) );
	}

	public function getRapidLeaseDefinitionDays() {
		return $this->m_intRapidLeaseDefinitionDays;
	}

	public function sqlRapidLeaseDefinitionDays() {
		return ( true == isset( $this->m_intRapidLeaseDefinitionDays ) ) ? ( string ) $this->m_intRapidLeaseDefinitionDays : 'NULL';
	}

	public function setMoveOutEstimatedChargesDays( $intMoveOutEstimatedChargesDays ) {
		$this->set( 'm_intMoveOutEstimatedChargesDays', CStrings::strToIntDef( $intMoveOutEstimatedChargesDays, NULL, false ) );
	}

	public function getMoveOutEstimatedChargesDays() {
		return $this->m_intMoveOutEstimatedChargesDays;
	}

	public function sqlMoveOutEstimatedChargesDays() {
		return ( true == isset( $this->m_intMoveOutEstimatedChargesDays ) ) ? ( string ) $this->m_intMoveOutEstimatedChargesDays : 'NULL';
	}

	public function setUsageWaiverAmount( $fltUsageWaiverAmount ) {
		$this->set( 'm_fltUsageWaiverAmount', CStrings::strToFloatDef( $fltUsageWaiverAmount, NULL, false, 2 ) );
	}

	public function getUsageWaiverAmount() {
		return $this->m_fltUsageWaiverAmount;
	}

	public function sqlUsageWaiverAmount() {
		return ( true == isset( $this->m_fltUsageWaiverAmount ) ) ? ( string ) $this->m_fltUsageWaiverAmount : 'NULL';
	}

	public function setNoFeeThreshhold( $fltNoFeeThreshhold ) {
		$this->set( 'm_fltNoFeeThreshhold', CStrings::strToFloatDef( $fltNoFeeThreshhold, NULL, false, 2 ) );
	}

	public function getNoFeeThreshhold() {
		return $this->m_fltNoFeeThreshhold;
	}

	public function sqlNoFeeThreshhold() {
		return ( true == isset( $this->m_fltNoFeeThreshhold ) ) ? ( string ) $this->m_fltNoFeeThreshhold : 'NULL';
	}

	public function setFirstFixedServiceFee( $fltFirstFixedServiceFee ) {
		$this->set( 'm_fltFirstFixedServiceFee', CStrings::strToFloatDef( $fltFirstFixedServiceFee, NULL, false, 2 ) );
	}

	public function getFirstFixedServiceFee() {
		return $this->m_fltFirstFixedServiceFee;
	}

	public function sqlFirstFixedServiceFee() {
		return ( true == isset( $this->m_fltFirstFixedServiceFee ) ) ? ( string ) $this->m_fltFirstFixedServiceFee : 'NULL';
	}

	public function setFirstDailyServiceFee( $fltFirstDailyServiceFee ) {
		$this->set( 'm_fltFirstDailyServiceFee', CStrings::strToFloatDef( $fltFirstDailyServiceFee, NULL, false, 2 ) );
	}

	public function getFirstDailyServiceFee() {
		return $this->m_fltFirstDailyServiceFee;
	}

	public function sqlFirstDailyServiceFee() {
		return ( true == isset( $this->m_fltFirstDailyServiceFee ) ) ? ( string ) $this->m_fltFirstDailyServiceFee : 'NULL';
	}

	public function setFirstDailyServiceCap( $fltFirstDailyServiceCap ) {
		$this->set( 'm_fltFirstDailyServiceCap', CStrings::strToFloatDef( $fltFirstDailyServiceCap, NULL, false, 2 ) );
	}

	public function getFirstDailyServiceCap() {
		return $this->m_fltFirstDailyServiceCap;
	}

	public function sqlFirstDailyServiceCap() {
		return ( true == isset( $this->m_fltFirstDailyServiceCap ) ) ? ( string ) $this->m_fltFirstDailyServiceCap : 'NULL';
	}

	public function setFirstDay1ServiceFee( $fltFirstDay1ServiceFee ) {
		$this->set( 'm_fltFirstDay1ServiceFee', CStrings::strToFloatDef( $fltFirstDay1ServiceFee, NULL, false, 2 ) );
	}

	public function getFirstDay1ServiceFee() {
		return $this->m_fltFirstDay1ServiceFee;
	}

	public function sqlFirstDay1ServiceFee() {
		return ( true == isset( $this->m_fltFirstDay1ServiceFee ) ) ? ( string ) $this->m_fltFirstDay1ServiceFee : 'NULL';
	}

	public function setFirstDay2ServiceFee( $fltFirstDay2ServiceFee ) {
		$this->set( 'm_fltFirstDay2ServiceFee', CStrings::strToFloatDef( $fltFirstDay2ServiceFee, NULL, false, 2 ) );
	}

	public function getFirstDay2ServiceFee() {
		return $this->m_fltFirstDay2ServiceFee;
	}

	public function sqlFirstDay2ServiceFee() {
		return ( true == isset( $this->m_fltFirstDay2ServiceFee ) ) ? ( string ) $this->m_fltFirstDay2ServiceFee : 'NULL';
	}

	public function setFirstDay3ServiceFee( $fltFirstDay3ServiceFee ) {
		$this->set( 'm_fltFirstDay3ServiceFee', CStrings::strToFloatDef( $fltFirstDay3ServiceFee, NULL, false, 2 ) );
	}

	public function getFirstDay3ServiceFee() {
		return $this->m_fltFirstDay3ServiceFee;
	}

	public function sqlFirstDay3ServiceFee() {
		return ( true == isset( $this->m_fltFirstDay3ServiceFee ) ) ? ( string ) $this->m_fltFirstDay3ServiceFee : 'NULL';
	}

	public function setFirstDay4ServiceFee( $fltFirstDay4ServiceFee ) {
		$this->set( 'm_fltFirstDay4ServiceFee', CStrings::strToFloatDef( $fltFirstDay4ServiceFee, NULL, false, 2 ) );
	}

	public function getFirstDay4ServiceFee() {
		return $this->m_fltFirstDay4ServiceFee;
	}

	public function sqlFirstDay4ServiceFee() {
		return ( true == isset( $this->m_fltFirstDay4ServiceFee ) ) ? ( string ) $this->m_fltFirstDay4ServiceFee : 'NULL';
	}

	public function setFirstDay5ServiceFee( $fltFirstDay5ServiceFee ) {
		$this->set( 'm_fltFirstDay5ServiceFee', CStrings::strToFloatDef( $fltFirstDay5ServiceFee, NULL, false, 2 ) );
	}

	public function getFirstDay5ServiceFee() {
		return $this->m_fltFirstDay5ServiceFee;
	}

	public function sqlFirstDay5ServiceFee() {
		return ( true == isset( $this->m_fltFirstDay5ServiceFee ) ) ? ( string ) $this->m_fltFirstDay5ServiceFee : 'NULL';
	}

	public function setFirstDay6ServiceFee( $fltFirstDay6ServiceFee ) {
		$this->set( 'm_fltFirstDay6ServiceFee', CStrings::strToFloatDef( $fltFirstDay6ServiceFee, NULL, false, 2 ) );
	}

	public function getFirstDay6ServiceFee() {
		return $this->m_fltFirstDay6ServiceFee;
	}

	public function sqlFirstDay6ServiceFee() {
		return ( true == isset( $this->m_fltFirstDay6ServiceFee ) ) ? ( string ) $this->m_fltFirstDay6ServiceFee : 'NULL';
	}

	public function setFirstDay7ServiceFee( $fltFirstDay7ServiceFee ) {
		$this->set( 'm_fltFirstDay7ServiceFee', CStrings::strToFloatDef( $fltFirstDay7ServiceFee, NULL, false, 2 ) );
	}

	public function getFirstDay7ServiceFee() {
		return $this->m_fltFirstDay7ServiceFee;
	}

	public function sqlFirstDay7ServiceFee() {
		return ( true == isset( $this->m_fltFirstDay7ServiceFee ) ) ? ( string ) $this->m_fltFirstDay7ServiceFee : 'NULL';
	}

	public function setRapidFixedServiceFee( $fltRapidFixedServiceFee ) {
		$this->set( 'm_fltRapidFixedServiceFee', CStrings::strToFloatDef( $fltRapidFixedServiceFee, NULL, false, 2 ) );
	}

	public function getRapidFixedServiceFee() {
		return $this->m_fltRapidFixedServiceFee;
	}

	public function sqlRapidFixedServiceFee() {
		return ( true == isset( $this->m_fltRapidFixedServiceFee ) ) ? ( string ) $this->m_fltRapidFixedServiceFee : 'NULL';
	}

	public function setRapidDailyServiceFee( $fltRapidDailyServiceFee ) {
		$this->set( 'm_fltRapidDailyServiceFee', CStrings::strToFloatDef( $fltRapidDailyServiceFee, NULL, false, 2 ) );
	}

	public function getRapidDailyServiceFee() {
		return $this->m_fltRapidDailyServiceFee;
	}

	public function sqlRapidDailyServiceFee() {
		return ( true == isset( $this->m_fltRapidDailyServiceFee ) ) ? ( string ) $this->m_fltRapidDailyServiceFee : 'NULL';
	}

	public function setRapidDailyServiceCap( $fltRapidDailyServiceCap ) {
		$this->set( 'm_fltRapidDailyServiceCap', CStrings::strToFloatDef( $fltRapidDailyServiceCap, NULL, false, 2 ) );
	}

	public function getRapidDailyServiceCap() {
		return $this->m_fltRapidDailyServiceCap;
	}

	public function sqlRapidDailyServiceCap() {
		return ( true == isset( $this->m_fltRapidDailyServiceCap ) ) ? ( string ) $this->m_fltRapidDailyServiceCap : 'NULL';
	}

	public function setRapidDay1ServiceFee( $fltRapidDay1ServiceFee ) {
		$this->set( 'm_fltRapidDay1ServiceFee', CStrings::strToFloatDef( $fltRapidDay1ServiceFee, NULL, false, 2 ) );
	}

	public function getRapidDay1ServiceFee() {
		return $this->m_fltRapidDay1ServiceFee;
	}

	public function sqlRapidDay1ServiceFee() {
		return ( true == isset( $this->m_fltRapidDay1ServiceFee ) ) ? ( string ) $this->m_fltRapidDay1ServiceFee : 'NULL';
	}

	public function setRapidDay2ServiceFee( $fltRapidDay2ServiceFee ) {
		$this->set( 'm_fltRapidDay2ServiceFee', CStrings::strToFloatDef( $fltRapidDay2ServiceFee, NULL, false, 2 ) );
	}

	public function getRapidDay2ServiceFee() {
		return $this->m_fltRapidDay2ServiceFee;
	}

	public function sqlRapidDay2ServiceFee() {
		return ( true == isset( $this->m_fltRapidDay2ServiceFee ) ) ? ( string ) $this->m_fltRapidDay2ServiceFee : 'NULL';
	}

	public function setRapidDay3ServiceFee( $fltRapidDay3ServiceFee ) {
		$this->set( 'm_fltRapidDay3ServiceFee', CStrings::strToFloatDef( $fltRapidDay3ServiceFee, NULL, false, 2 ) );
	}

	public function getRapidDay3ServiceFee() {
		return $this->m_fltRapidDay3ServiceFee;
	}

	public function sqlRapidDay3ServiceFee() {
		return ( true == isset( $this->m_fltRapidDay3ServiceFee ) ) ? ( string ) $this->m_fltRapidDay3ServiceFee : 'NULL';
	}

	public function setRapidDay4ServiceFee( $fltRapidDay4ServiceFee ) {
		$this->set( 'm_fltRapidDay4ServiceFee', CStrings::strToFloatDef( $fltRapidDay4ServiceFee, NULL, false, 2 ) );
	}

	public function getRapidDay4ServiceFee() {
		return $this->m_fltRapidDay4ServiceFee;
	}

	public function sqlRapidDay4ServiceFee() {
		return ( true == isset( $this->m_fltRapidDay4ServiceFee ) ) ? ( string ) $this->m_fltRapidDay4ServiceFee : 'NULL';
	}

	public function setRapidDay5ServiceFee( $fltRapidDay5ServiceFee ) {
		$this->set( 'm_fltRapidDay5ServiceFee', CStrings::strToFloatDef( $fltRapidDay5ServiceFee, NULL, false, 2 ) );
	}

	public function getRapidDay5ServiceFee() {
		return $this->m_fltRapidDay5ServiceFee;
	}

	public function sqlRapidDay5ServiceFee() {
		return ( true == isset( $this->m_fltRapidDay5ServiceFee ) ) ? ( string ) $this->m_fltRapidDay5ServiceFee : 'NULL';
	}

	public function setRapidDay6ServiceFee( $fltRapidDay6ServiceFee ) {
		$this->set( 'm_fltRapidDay6ServiceFee', CStrings::strToFloatDef( $fltRapidDay6ServiceFee, NULL, false, 2 ) );
	}

	public function getRapidDay6ServiceFee() {
		return $this->m_fltRapidDay6ServiceFee;
	}

	public function sqlRapidDay6ServiceFee() {
		return ( true == isset( $this->m_fltRapidDay6ServiceFee ) ) ? ( string ) $this->m_fltRapidDay6ServiceFee : 'NULL';
	}

	public function setRapidDay7ServiceFee( $fltRapidDay7ServiceFee ) {
		$this->set( 'm_fltRapidDay7ServiceFee', CStrings::strToFloatDef( $fltRapidDay7ServiceFee, NULL, false, 2 ) );
	}

	public function getRapidDay7ServiceFee() {
		return $this->m_fltRapidDay7ServiceFee;
	}

	public function sqlRapidDay7ServiceFee() {
		return ( true == isset( $this->m_fltRapidDay7ServiceFee ) ) ? ( string ) $this->m_fltRapidDay7ServiceFee : 'NULL';
	}

	public function setIsIgnoreRenewals( $boolIsIgnoreRenewals ) {
		$this->set( 'm_boolIsIgnoreRenewals', CStrings::strToBool( $boolIsIgnoreRenewals ) );
	}

	public function getIsIgnoreRenewals() {
		return $this->m_boolIsIgnoreRenewals;
	}

	public function sqlIsIgnoreRenewals() {
		return ( true == isset( $this->m_boolIsIgnoreRenewals ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIgnoreRenewals ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsHideMoveOutEstimatedCharges( $boolIsHideMoveOutEstimatedCharges ) {
		$this->set( 'm_boolIsHideMoveOutEstimatedCharges', CStrings::strToBool( $boolIsHideMoveOutEstimatedCharges ) );
	}

	public function getIsHideMoveOutEstimatedCharges() {
		return $this->m_boolIsHideMoveOutEstimatedCharges;
	}

	public function sqlIsHideMoveOutEstimatedCharges() {
		return ( true == isset( $this->m_boolIsHideMoveOutEstimatedCharges ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHideMoveOutEstimatedCharges ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setWaiveFirstVcrFee( $boolWaiveFirstVcrFee ) {
		$this->set( 'm_boolWaiveFirstVcrFee', CStrings::strToBool( $boolWaiveFirstVcrFee ) );
	}

	public function getWaiveFirstVcrFee() {
		return $this->m_boolWaiveFirstVcrFee;
	}

	public function sqlWaiveFirstVcrFee() {
		return ( true == isset( $this->m_boolWaiveFirstVcrFee ) ) ? '\'' . ( true == ( bool ) $this->m_boolWaiveFirstVcrFee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setVcrNoticeText( $strVcrNoticeText ) {
		$this->set( 'm_strVcrNoticeText', CStrings::strTrimDef( $strVcrNoticeText, -1, NULL, true ) );
	}

	public function getVcrNoticeText() {
		return $this->m_strVcrNoticeText;
	}

	public function sqlVcrNoticeText() {
		return ( true == isset( $this->m_strVcrNoticeText ) ) ? '\'' . addslashes( $this->m_strVcrNoticeText ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setNewPropertyVcrTypeRateId( $intNewPropertyVcrTypeRateId ) {
		$this->set( 'm_intNewPropertyVcrTypeRateId', CStrings::strToIntDef( $intNewPropertyVcrTypeRateId, NULL, false ) );
	}

	public function getNewPropertyVcrTypeRateId() {
		return $this->m_intNewPropertyVcrTypeRateId;
	}

	public function sqlNewPropertyVcrTypeRateId() {
		return ( true == isset( $this->m_intNewPropertyVcrTypeRateId ) ) ? ( string ) $this->m_intNewPropertyVcrTypeRateId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_vcr_setting_id, first_vcr_service_fee_type_id, rapid_vcr_service_fee_type_id, grace_days, waive_usage_during_grace_days, first_bill_vcr_separately, bill_consecutive_vcr_violations, rapid_lease_definition_days, move_out_estimated_charges_days, usage_waiver_amount, no_fee_threshhold, first_fixed_service_fee, first_daily_service_fee, first_daily_service_cap, first_day1_service_fee, first_day2_service_fee, first_day3_service_fee, first_day4_service_fee, first_day5_service_fee, first_day6_service_fee, first_day7_service_fee, rapid_fixed_service_fee, rapid_daily_service_fee, rapid_daily_service_cap, rapid_day1_service_fee, rapid_day2_service_fee, rapid_day3_service_fee, rapid_day4_service_fee, rapid_day5_service_fee, rapid_day6_service_fee, rapid_day7_service_fee, is_ignore_renewals, is_hide_move_out_estimated_charges, waive_first_vcr_fee, start_date, end_date, vcr_notice_text, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, new_property_vcr_type_rate_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyVcrSettingId() . ', ' .
						$this->sqlFirstVcrServiceFeeTypeId() . ', ' .
						$this->sqlRapidVcrServiceFeeTypeId() . ', ' .
						$this->sqlGraceDays() . ', ' .
						$this->sqlWaiveUsageDuringGraceDays() . ', ' .
						$this->sqlFirstBillVcrSeparately() . ', ' .
						$this->sqlBillConsecutiveVcrViolations() . ', ' .
						$this->sqlRapidLeaseDefinitionDays() . ', ' .
						$this->sqlMoveOutEstimatedChargesDays() . ', ' .
						$this->sqlUsageWaiverAmount() . ', ' .
						$this->sqlNoFeeThreshhold() . ', ' .
						$this->sqlFirstFixedServiceFee() . ', ' .
						$this->sqlFirstDailyServiceFee() . ', ' .
						$this->sqlFirstDailyServiceCap() . ', ' .
						$this->sqlFirstDay1ServiceFee() . ', ' .
						$this->sqlFirstDay2ServiceFee() . ', ' .
						$this->sqlFirstDay3ServiceFee() . ', ' .
						$this->sqlFirstDay4ServiceFee() . ', ' .
						$this->sqlFirstDay5ServiceFee() . ', ' .
						$this->sqlFirstDay6ServiceFee() . ', ' .
						$this->sqlFirstDay7ServiceFee() . ', ' .
						$this->sqlRapidFixedServiceFee() . ', ' .
						$this->sqlRapidDailyServiceFee() . ', ' .
						$this->sqlRapidDailyServiceCap() . ', ' .
						$this->sqlRapidDay1ServiceFee() . ', ' .
						$this->sqlRapidDay2ServiceFee() . ', ' .
						$this->sqlRapidDay3ServiceFee() . ', ' .
						$this->sqlRapidDay4ServiceFee() . ', ' .
						$this->sqlRapidDay5ServiceFee() . ', ' .
						$this->sqlRapidDay6ServiceFee() . ', ' .
						$this->sqlRapidDay7ServiceFee() . ', ' .
						$this->sqlIsIgnoreRenewals() . ', ' .
						$this->sqlIsHideMoveOutEstimatedCharges() . ', ' .
						$this->sqlWaiveFirstVcrFee() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlVcrNoticeText() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlNewPropertyVcrTypeRateId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_vcr_setting_id = ' . $this->sqlPropertyVcrSettingId(). ',' ; } elseif( true == array_key_exists( 'PropertyVcrSettingId', $this->getChangedColumns() ) ) { $strSql .= ' property_vcr_setting_id = ' . $this->sqlPropertyVcrSettingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_vcr_service_fee_type_id = ' . $this->sqlFirstVcrServiceFeeTypeId(). ',' ; } elseif( true == array_key_exists( 'FirstVcrServiceFeeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' first_vcr_service_fee_type_id = ' . $this->sqlFirstVcrServiceFeeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_vcr_service_fee_type_id = ' . $this->sqlRapidVcrServiceFeeTypeId(). ',' ; } elseif( true == array_key_exists( 'RapidVcrServiceFeeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' rapid_vcr_service_fee_type_id = ' . $this->sqlRapidVcrServiceFeeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' grace_days = ' . $this->sqlGraceDays(). ',' ; } elseif( true == array_key_exists( 'GraceDays', $this->getChangedColumns() ) ) { $strSql .= ' grace_days = ' . $this->sqlGraceDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' waive_usage_during_grace_days = ' . $this->sqlWaiveUsageDuringGraceDays(). ',' ; } elseif( true == array_key_exists( 'WaiveUsageDuringGraceDays', $this->getChangedColumns() ) ) { $strSql .= ' waive_usage_during_grace_days = ' . $this->sqlWaiveUsageDuringGraceDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_bill_vcr_separately = ' . $this->sqlFirstBillVcrSeparately(). ',' ; } elseif( true == array_key_exists( 'FirstBillVcrSeparately', $this->getChangedColumns() ) ) { $strSql .= ' first_bill_vcr_separately = ' . $this->sqlFirstBillVcrSeparately() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_consecutive_vcr_violations = ' . $this->sqlBillConsecutiveVcrViolations(). ',' ; } elseif( true == array_key_exists( 'BillConsecutiveVcrViolations', $this->getChangedColumns() ) ) { $strSql .= ' bill_consecutive_vcr_violations = ' . $this->sqlBillConsecutiveVcrViolations() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_lease_definition_days = ' . $this->sqlRapidLeaseDefinitionDays(). ',' ; } elseif( true == array_key_exists( 'RapidLeaseDefinitionDays', $this->getChangedColumns() ) ) { $strSql .= ' rapid_lease_definition_days = ' . $this->sqlRapidLeaseDefinitionDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_estimated_charges_days = ' . $this->sqlMoveOutEstimatedChargesDays(). ',' ; } elseif( true == array_key_exists( 'MoveOutEstimatedChargesDays', $this->getChangedColumns() ) ) { $strSql .= ' move_out_estimated_charges_days = ' . $this->sqlMoveOutEstimatedChargesDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' usage_waiver_amount = ' . $this->sqlUsageWaiverAmount(). ',' ; } elseif( true == array_key_exists( 'UsageWaiverAmount', $this->getChangedColumns() ) ) { $strSql .= ' usage_waiver_amount = ' . $this->sqlUsageWaiverAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' no_fee_threshhold = ' . $this->sqlNoFeeThreshhold(). ',' ; } elseif( true == array_key_exists( 'NoFeeThreshhold', $this->getChangedColumns() ) ) { $strSql .= ' no_fee_threshhold = ' . $this->sqlNoFeeThreshhold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_fixed_service_fee = ' . $this->sqlFirstFixedServiceFee(). ',' ; } elseif( true == array_key_exists( 'FirstFixedServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' first_fixed_service_fee = ' . $this->sqlFirstFixedServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_daily_service_fee = ' . $this->sqlFirstDailyServiceFee(). ',' ; } elseif( true == array_key_exists( 'FirstDailyServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' first_daily_service_fee = ' . $this->sqlFirstDailyServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_daily_service_cap = ' . $this->sqlFirstDailyServiceCap(). ',' ; } elseif( true == array_key_exists( 'FirstDailyServiceCap', $this->getChangedColumns() ) ) { $strSql .= ' first_daily_service_cap = ' . $this->sqlFirstDailyServiceCap() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day1_service_fee = ' . $this->sqlFirstDay1ServiceFee(). ',' ; } elseif( true == array_key_exists( 'FirstDay1ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' first_day1_service_fee = ' . $this->sqlFirstDay1ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day2_service_fee = ' . $this->sqlFirstDay2ServiceFee(). ',' ; } elseif( true == array_key_exists( 'FirstDay2ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' first_day2_service_fee = ' . $this->sqlFirstDay2ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day3_service_fee = ' . $this->sqlFirstDay3ServiceFee(). ',' ; } elseif( true == array_key_exists( 'FirstDay3ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' first_day3_service_fee = ' . $this->sqlFirstDay3ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day4_service_fee = ' . $this->sqlFirstDay4ServiceFee(). ',' ; } elseif( true == array_key_exists( 'FirstDay4ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' first_day4_service_fee = ' . $this->sqlFirstDay4ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day5_service_fee = ' . $this->sqlFirstDay5ServiceFee(). ',' ; } elseif( true == array_key_exists( 'FirstDay5ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' first_day5_service_fee = ' . $this->sqlFirstDay5ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day6_service_fee = ' . $this->sqlFirstDay6ServiceFee(). ',' ; } elseif( true == array_key_exists( 'FirstDay6ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' first_day6_service_fee = ' . $this->sqlFirstDay6ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day7_service_fee = ' . $this->sqlFirstDay7ServiceFee(). ',' ; } elseif( true == array_key_exists( 'FirstDay7ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' first_day7_service_fee = ' . $this->sqlFirstDay7ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_fixed_service_fee = ' . $this->sqlRapidFixedServiceFee(). ',' ; } elseif( true == array_key_exists( 'RapidFixedServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' rapid_fixed_service_fee = ' . $this->sqlRapidFixedServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_daily_service_fee = ' . $this->sqlRapidDailyServiceFee(). ',' ; } elseif( true == array_key_exists( 'RapidDailyServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' rapid_daily_service_fee = ' . $this->sqlRapidDailyServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_daily_service_cap = ' . $this->sqlRapidDailyServiceCap(). ',' ; } elseif( true == array_key_exists( 'RapidDailyServiceCap', $this->getChangedColumns() ) ) { $strSql .= ' rapid_daily_service_cap = ' . $this->sqlRapidDailyServiceCap() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_day1_service_fee = ' . $this->sqlRapidDay1ServiceFee(). ',' ; } elseif( true == array_key_exists( 'RapidDay1ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' rapid_day1_service_fee = ' . $this->sqlRapidDay1ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_day2_service_fee = ' . $this->sqlRapidDay2ServiceFee(). ',' ; } elseif( true == array_key_exists( 'RapidDay2ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' rapid_day2_service_fee = ' . $this->sqlRapidDay2ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_day3_service_fee = ' . $this->sqlRapidDay3ServiceFee(). ',' ; } elseif( true == array_key_exists( 'RapidDay3ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' rapid_day3_service_fee = ' . $this->sqlRapidDay3ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_day4_service_fee = ' . $this->sqlRapidDay4ServiceFee(). ',' ; } elseif( true == array_key_exists( 'RapidDay4ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' rapid_day4_service_fee = ' . $this->sqlRapidDay4ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_day5_service_fee = ' . $this->sqlRapidDay5ServiceFee(). ',' ; } elseif( true == array_key_exists( 'RapidDay5ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' rapid_day5_service_fee = ' . $this->sqlRapidDay5ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_day6_service_fee = ' . $this->sqlRapidDay6ServiceFee(). ',' ; } elseif( true == array_key_exists( 'RapidDay6ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' rapid_day6_service_fee = ' . $this->sqlRapidDay6ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rapid_day7_service_fee = ' . $this->sqlRapidDay7ServiceFee(). ',' ; } elseif( true == array_key_exists( 'RapidDay7ServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' rapid_day7_service_fee = ' . $this->sqlRapidDay7ServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ignore_renewals = ' . $this->sqlIsIgnoreRenewals(). ',' ; } elseif( true == array_key_exists( 'IsIgnoreRenewals', $this->getChangedColumns() ) ) { $strSql .= ' is_ignore_renewals = ' . $this->sqlIsIgnoreRenewals() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hide_move_out_estimated_charges = ' . $this->sqlIsHideMoveOutEstimatedCharges(). ',' ; } elseif( true == array_key_exists( 'IsHideMoveOutEstimatedCharges', $this->getChangedColumns() ) ) { $strSql .= ' is_hide_move_out_estimated_charges = ' . $this->sqlIsHideMoveOutEstimatedCharges() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' waive_first_vcr_fee = ' . $this->sqlWaiveFirstVcrFee(). ',' ; } elseif( true == array_key_exists( 'WaiveFirstVcrFee', $this->getChangedColumns() ) ) { $strSql .= ' waive_first_vcr_fee = ' . $this->sqlWaiveFirstVcrFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vcr_notice_text = ' . $this->sqlVcrNoticeText(). ',' ; } elseif( true == array_key_exists( 'VcrNoticeText', $this->getChangedColumns() ) ) { $strSql .= ' vcr_notice_text = ' . $this->sqlVcrNoticeText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_vcr_type_rate_id = ' . $this->sqlNewPropertyVcrTypeRateId(). ',' ; } elseif( true == array_key_exists( 'NewPropertyVcrTypeRateId', $this->getChangedColumns() ) ) { $strSql .= ' new_property_vcr_type_rate_id = ' . $this->sqlNewPropertyVcrTypeRateId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_vcr_setting_id' => $this->getPropertyVcrSettingId(),
			'first_vcr_service_fee_type_id' => $this->getFirstVcrServiceFeeTypeId(),
			'rapid_vcr_service_fee_type_id' => $this->getRapidVcrServiceFeeTypeId(),
			'grace_days' => $this->getGraceDays(),
			'waive_usage_during_grace_days' => $this->getWaiveUsageDuringGraceDays(),
			'first_bill_vcr_separately' => $this->getFirstBillVcrSeparately(),
			'bill_consecutive_vcr_violations' => $this->getBillConsecutiveVcrViolations(),
			'rapid_lease_definition_days' => $this->getRapidLeaseDefinitionDays(),
			'move_out_estimated_charges_days' => $this->getMoveOutEstimatedChargesDays(),
			'usage_waiver_amount' => $this->getUsageWaiverAmount(),
			'no_fee_threshhold' => $this->getNoFeeThreshhold(),
			'first_fixed_service_fee' => $this->getFirstFixedServiceFee(),
			'first_daily_service_fee' => $this->getFirstDailyServiceFee(),
			'first_daily_service_cap' => $this->getFirstDailyServiceCap(),
			'first_day1_service_fee' => $this->getFirstDay1ServiceFee(),
			'first_day2_service_fee' => $this->getFirstDay2ServiceFee(),
			'first_day3_service_fee' => $this->getFirstDay3ServiceFee(),
			'first_day4_service_fee' => $this->getFirstDay4ServiceFee(),
			'first_day5_service_fee' => $this->getFirstDay5ServiceFee(),
			'first_day6_service_fee' => $this->getFirstDay6ServiceFee(),
			'first_day7_service_fee' => $this->getFirstDay7ServiceFee(),
			'rapid_fixed_service_fee' => $this->getRapidFixedServiceFee(),
			'rapid_daily_service_fee' => $this->getRapidDailyServiceFee(),
			'rapid_daily_service_cap' => $this->getRapidDailyServiceCap(),
			'rapid_day1_service_fee' => $this->getRapidDay1ServiceFee(),
			'rapid_day2_service_fee' => $this->getRapidDay2ServiceFee(),
			'rapid_day3_service_fee' => $this->getRapidDay3ServiceFee(),
			'rapid_day4_service_fee' => $this->getRapidDay4ServiceFee(),
			'rapid_day5_service_fee' => $this->getRapidDay5ServiceFee(),
			'rapid_day6_service_fee' => $this->getRapidDay6ServiceFee(),
			'rapid_day7_service_fee' => $this->getRapidDay7ServiceFee(),
			'is_ignore_renewals' => $this->getIsIgnoreRenewals(),
			'is_hide_move_out_estimated_charges' => $this->getIsHideMoveOutEstimatedCharges(),
			'waive_first_vcr_fee' => $this->getWaiveFirstVcrFee(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'vcr_notice_text' => $this->getVcrNoticeText(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'new_property_vcr_type_rate_id' => $this->getNewPropertyVcrTypeRateId()
		);
	}

}
?>