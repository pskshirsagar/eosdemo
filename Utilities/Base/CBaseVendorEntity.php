<?php

class CBaseVendorEntity extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.vendor_entities';

	protected $m_intId;
	protected $m_intVendorId;
	protected $m_intDefaultOwnerTypeId;
	protected $m_intAccountId;
	protected $m_strTaxNumberEncrypted;
	protected $m_intForm1099TypeId;
	protected $m_intForm1099BoxTypeId;
	protected $m_strEntityName;
	protected $m_boolReceives1099;
	protected $m_strDeniedReason;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeniedBy;
	protected $m_strDeniedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strTaxNumberHash;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intForm1099TypeId = '1';
		$this->m_intForm1099BoxTypeId = '1';
		$this->m_boolReceives1099 = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorId', trim( $arrValues['vendor_id'] ) ); elseif( isset( $arrValues['vendor_id'] ) ) $this->setVendorId( $arrValues['vendor_id'] );
		if( isset( $arrValues['default_owner_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultOwnerTypeId', trim( $arrValues['default_owner_type_id'] ) ); elseif( isset( $arrValues['default_owner_type_id'] ) ) $this->setDefaultOwnerTypeId( $arrValues['default_owner_type_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['form_1099_type_id'] ) && $boolDirectSet ) $this->set( 'm_intForm1099TypeId', trim( $arrValues['form_1099_type_id'] ) ); elseif( isset( $arrValues['form_1099_type_id'] ) ) $this->setForm1099TypeId( $arrValues['form_1099_type_id'] );
		if( isset( $arrValues['form_1099_box_type_id'] ) && $boolDirectSet ) $this->set( 'm_intForm1099BoxTypeId', trim( $arrValues['form_1099_box_type_id'] ) ); elseif( isset( $arrValues['form_1099_box_type_id'] ) ) $this->setForm1099BoxTypeId( $arrValues['form_1099_box_type_id'] );
		if( isset( $arrValues['entity_name'] ) && $boolDirectSet ) $this->set( 'm_strEntityName', trim( $arrValues['entity_name'] ) ); elseif( isset( $arrValues['entity_name'] ) ) $this->setEntityName( $arrValues['entity_name'] );
		if( isset( $arrValues['receives_1099'] ) && $boolDirectSet ) $this->set( 'm_boolReceives1099', trim( stripcslashes( $arrValues['receives_1099'] ) ) ); elseif( isset( $arrValues['receives_1099'] ) ) $this->setReceives1099( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['receives_1099'] ) : $arrValues['receives_1099'] );
		if( isset( $arrValues['denied_reason'] ) && $boolDirectSet ) $this->set( 'm_strDeniedReason', trim( $arrValues['denied_reason'] ) ); elseif( isset( $arrValues['denied_reason'] ) ) $this->setDeniedReason( $arrValues['denied_reason'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['denied_by'] ) && $boolDirectSet ) $this->set( 'm_intDeniedBy', trim( $arrValues['denied_by'] ) ); elseif( isset( $arrValues['denied_by'] ) ) $this->setDeniedBy( $arrValues['denied_by'] );
		if( isset( $arrValues['denied_on'] ) && $boolDirectSet ) $this->set( 'm_strDeniedOn', trim( $arrValues['denied_on'] ) ); elseif( isset( $arrValues['denied_on'] ) ) $this->setDeniedOn( $arrValues['denied_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['tax_number_hash'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberHash', trim( $arrValues['tax_number_hash'] ) ); elseif( isset( $arrValues['tax_number_hash'] ) ) $this->setTaxNumberHash( $arrValues['tax_number_hash'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVendorId( $intVendorId ) {
		$this->set( 'm_intVendorId', CStrings::strToIntDef( $intVendorId, NULL, false ) );
	}

	public function getVendorId() {
		return $this->m_intVendorId;
	}

	public function sqlVendorId() {
		return ( true == isset( $this->m_intVendorId ) ) ? ( string ) $this->m_intVendorId : 'NULL';
	}

	public function setDefaultOwnerTypeId( $intDefaultOwnerTypeId ) {
		$this->set( 'm_intDefaultOwnerTypeId', CStrings::strToIntDef( $intDefaultOwnerTypeId, NULL, false ) );
	}

	public function getDefaultOwnerTypeId() {
		return $this->m_intDefaultOwnerTypeId;
	}

	public function sqlDefaultOwnerTypeId() {
		return ( true == isset( $this->m_intDefaultOwnerTypeId ) ) ? ( string ) $this->m_intDefaultOwnerTypeId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 200, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setForm1099TypeId( $intForm1099TypeId ) {
		$this->set( 'm_intForm1099TypeId', CStrings::strToIntDef( $intForm1099TypeId, NULL, false ) );
	}

	public function getForm1099TypeId() {
		return $this->m_intForm1099TypeId;
	}

	public function sqlForm1099TypeId() {
		return ( true == isset( $this->m_intForm1099TypeId ) ) ? ( string ) $this->m_intForm1099TypeId : '1';
	}

	public function setForm1099BoxTypeId( $intForm1099BoxTypeId ) {
		$this->set( 'm_intForm1099BoxTypeId', CStrings::strToIntDef( $intForm1099BoxTypeId, NULL, false ) );
	}

	public function getForm1099BoxTypeId() {
		return $this->m_intForm1099BoxTypeId;
	}

	public function sqlForm1099BoxTypeId() {
		return ( true == isset( $this->m_intForm1099BoxTypeId ) ) ? ( string ) $this->m_intForm1099BoxTypeId : '1';
	}

	public function setEntityName( $strEntityName ) {
		$this->set( 'm_strEntityName', CStrings::strTrimDef( $strEntityName, 200, NULL, true ) );
	}

	public function getEntityName() {
		return $this->m_strEntityName;
	}

	public function sqlEntityName() {
		return ( true == isset( $this->m_strEntityName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEntityName ) : '\'' . addslashes( $this->m_strEntityName ) . '\'' ) : 'NULL';
	}

	public function setReceives1099( $boolReceives1099 ) {
		$this->set( 'm_boolReceives1099', CStrings::strToBool( $boolReceives1099 ) );
	}

	public function getReceives1099() {
		return $this->m_boolReceives1099;
	}

	public function sqlReceives1099() {
		return ( true == isset( $this->m_boolReceives1099 ) ) ? '\'' . ( true == ( bool ) $this->m_boolReceives1099 ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeniedReason( $strDeniedReason ) {
		$this->set( 'm_strDeniedReason', CStrings::strTrimDef( $strDeniedReason, 240, NULL, true ) );
	}

	public function getDeniedReason() {
		return $this->m_strDeniedReason;
	}

	public function sqlDeniedReason() {
		return ( true == isset( $this->m_strDeniedReason ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDeniedReason ) : '\'' . addslashes( $this->m_strDeniedReason ) . '\'' ) : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeniedBy( $intDeniedBy ) {
		$this->set( 'm_intDeniedBy', CStrings::strToIntDef( $intDeniedBy, NULL, false ) );
	}

	public function getDeniedBy() {
		return $this->m_intDeniedBy;
	}

	public function sqlDeniedBy() {
		return ( true == isset( $this->m_intDeniedBy ) ) ? ( string ) $this->m_intDeniedBy : 'NULL';
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->set( 'm_strDeniedOn', CStrings::strTrimDef( $strDeniedOn, -1, NULL, true ) );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function sqlDeniedOn() {
		return ( true == isset( $this->m_strDeniedOn ) ) ? '\'' . $this->m_strDeniedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTaxNumberHash( $strTaxNumberHash ) {
		$this->set( 'm_strTaxNumberHash', CStrings::strTrimDef( $strTaxNumberHash, 240, NULL, true ) );
	}

	public function getTaxNumberHash() {
		return $this->m_strTaxNumberHash;
	}

	public function sqlTaxNumberHash() {
		return ( true == isset( $this->m_strTaxNumberHash ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberHash ) : '\'' . addslashes( $this->m_strTaxNumberHash ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vendor_id, default_owner_type_id, account_id, tax_number_encrypted, form_1099_type_id, form_1099_box_type_id, entity_name, receives_1099, denied_reason, approved_by, approved_on, denied_by, denied_on, updated_by, updated_on, created_by, created_on, tax_number_hash, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlVendorId() . ', ' .
						$this->sqlDefaultOwnerTypeId() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlForm1099TypeId() . ', ' .
						$this->sqlForm1099BoxTypeId() . ', ' .
						$this->sqlEntityName() . ', ' .
						$this->sqlReceives1099() . ', ' .
						$this->sqlDeniedReason() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeniedBy() . ', ' .
						$this->sqlDeniedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlTaxNumberHash() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId(). ',' ; } elseif( true == array_key_exists( 'VendorId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_id = ' . $this->sqlVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_owner_type_id = ' . $this->sqlDefaultOwnerTypeId(). ',' ; } elseif( true == array_key_exists( 'DefaultOwnerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' default_owner_type_id = ' . $this->sqlDefaultOwnerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' form_1099_type_id = ' . $this->sqlForm1099TypeId(). ',' ; } elseif( true == array_key_exists( 'Form1099TypeId', $this->getChangedColumns() ) ) { $strSql .= ' form_1099_type_id = ' . $this->sqlForm1099TypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' form_1099_box_type_id = ' . $this->sqlForm1099BoxTypeId(). ',' ; } elseif( true == array_key_exists( 'Form1099BoxTypeId', $this->getChangedColumns() ) ) { $strSql .= ' form_1099_box_type_id = ' . $this->sqlForm1099BoxTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_name = ' . $this->sqlEntityName(). ',' ; } elseif( true == array_key_exists( 'EntityName', $this->getChangedColumns() ) ) { $strSql .= ' entity_name = ' . $this->sqlEntityName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receives_1099 = ' . $this->sqlReceives1099(). ',' ; } elseif( true == array_key_exists( 'Receives1099', $this->getChangedColumns() ) ) { $strSql .= ' receives_1099 = ' . $this->sqlReceives1099() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_reason = ' . $this->sqlDeniedReason(). ',' ; } elseif( true == array_key_exists( 'DeniedReason', $this->getChangedColumns() ) ) { $strSql .= ' denied_reason = ' . $this->sqlDeniedReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy(). ',' ; } elseif( true == array_key_exists( 'DeniedBy', $this->getChangedColumns() ) ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn(). ',' ; } elseif( true == array_key_exists( 'DeniedOn', $this->getChangedColumns() ) ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_hash = ' . $this->sqlTaxNumberHash(). ',' ; } elseif( true == array_key_exists( 'TaxNumberHash', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_hash = ' . $this->sqlTaxNumberHash() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vendor_id' => $this->getVendorId(),
			'default_owner_type_id' => $this->getDefaultOwnerTypeId(),
			'account_id' => $this->getAccountId(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'form_1099_type_id' => $this->getForm1099TypeId(),
			'form_1099_box_type_id' => $this->getForm1099BoxTypeId(),
			'entity_name' => $this->getEntityName(),
			'receives_1099' => $this->getReceives1099(),
			'denied_reason' => $this->getDeniedReason(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'denied_by' => $this->getDeniedBy(),
			'denied_on' => $this->getDeniedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'tax_number_hash' => $this->getTaxNumberHash(),
			'details' => $this->getDetails()
		);
	}

}
?>