<?php

class CBaseUtilityBillTemplate extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_templates';

	protected $m_intId;
	protected $m_intUtilityProviderId;
	protected $m_intSampleUtilityDocumentId;
	protected $m_strName;
	protected $m_strTemplateDatetime;
	protected $m_strDescription;
	protected $m_intBeginXServiceStart;
	protected $m_intBeginYServiceStart;
	protected $m_intServiceStartWidth;
	protected $m_intServiceStartHeight;
	protected $m_intBeginXServiceEnd;
	protected $m_intBeginYServiceEnd;
	protected $m_intServiceEndWidth;
	protected $m_intServiceEndHeight;
	protected $m_intBeginXBillDate;
	protected $m_intBeginYBillDate;
	protected $m_intBillDateWidth;
	protected $m_intBillDateHeight;
	protected $m_intBeginXDueDate;
	protected $m_intBeginYDueDate;
	protected $m_intDueDateWidth;
	protected $m_intDueDateHeight;
	protected $m_intBeginXAccountNumber;
	protected $m_intBeginYAccountNumber;
	protected $m_intAccountNumberWidth;
	protected $m_intAccountNumberHeight;
	protected $m_intBeginXBillNumber;
	protected $m_intBeginYBillNumber;
	protected $m_intBillNumberWidth;
	protected $m_intBillNumberHeight;
	protected $m_intBeginXMeterStartRead;
	protected $m_intBeginYMeterStartRead;
	protected $m_intMeterStartReadWidth;
	protected $m_intMeterStartReadHeight;
	protected $m_intBeginXMeterEndRead;
	protected $m_intBeginYMeterEndRead;
	protected $m_intMeterEndReadWidth;
	protected $m_intMeterEndReadHeight;
	protected $m_intBeginXBillAmount;
	protected $m_intBeginYBillAmount;
	protected $m_intBillAmountWidth;
	protected $m_intBillAmountHeight;
	protected $m_intBeginXRemittanceStub;
	protected $m_intBeginYRemittanceStub;
	protected $m_intRemittanceStubHeight;
	protected $m_intRemittanceStubWidth;
	protected $m_intServiceStartPageNumber;
	protected $m_intServiceEndPageNumber;
	protected $m_intBillDatePageNumber;
	protected $m_boolShowServiceEndDate;
	protected $m_boolShowServiceStartDate;
	protected $m_intDueDatePageNumber;
	protected $m_intAccountNumberPageNumber;
	protected $m_intBillNumberPageNumber;
	protected $m_intMeterStartReadPageNumber;
	protected $m_intMeterEndReadPageNumber;
	protected $m_intBillAmountPageNumber;
	protected $m_intRemittanceStubPageNumber;
	protected $m_boolShowBillDate;
	protected $m_boolShowDueDate;
	protected $m_boolShowBillNumber;
	protected $m_boolShowMeterStartRead;
	protected $m_boolShowMeterEndRead;
	protected $m_boolShowBillAmount;
	protected $m_boolIsPublished;
	protected $m_boolAttemptOcr;
	protected $m_intOrderNum;
	protected $m_boolRequireMultiplier;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intServiceStartPageNumber = '0';
		$this->m_intServiceEndPageNumber = '0';
		$this->m_intBillDatePageNumber = '0';
		$this->m_boolShowServiceEndDate = true;
		$this->m_boolShowServiceStartDate = true;
		$this->m_intDueDatePageNumber = '0';
		$this->m_intAccountNumberPageNumber = '0';
		$this->m_intBillNumberPageNumber = '0';
		$this->m_intMeterStartReadPageNumber = '0';
		$this->m_intMeterEndReadPageNumber = '0';
		$this->m_intBillAmountPageNumber = '0';
		$this->m_boolShowBillDate = true;
		$this->m_boolShowDueDate = true;
		$this->m_boolShowBillNumber = true;
		$this->m_boolShowMeterStartRead = true;
		$this->m_boolShowMeterEndRead = true;
		$this->m_boolShowBillAmount = true;
		$this->m_boolIsPublished = true;
		$this->m_boolAttemptOcr = false;
		$this->m_intOrderNum = '0';
		$this->m_boolRequireMultiplier = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityProviderId', trim( $arrValues['utility_provider_id'] ) ); elseif( isset( $arrValues['utility_provider_id'] ) ) $this->setUtilityProviderId( $arrValues['utility_provider_id'] );
		if( isset( $arrValues['sample_utility_document_id'] ) && $boolDirectSet ) $this->set( 'm_intSampleUtilityDocumentId', trim( $arrValues['sample_utility_document_id'] ) ); elseif( isset( $arrValues['sample_utility_document_id'] ) ) $this->setSampleUtilityDocumentId( $arrValues['sample_utility_document_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['template_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTemplateDatetime', trim( $arrValues['template_datetime'] ) ); elseif( isset( $arrValues['template_datetime'] ) ) $this->setTemplateDatetime( $arrValues['template_datetime'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['begin_x_service_start'] ) && $boolDirectSet ) $this->set( 'm_intBeginXServiceStart', trim( $arrValues['begin_x_service_start'] ) ); elseif( isset( $arrValues['begin_x_service_start'] ) ) $this->setBeginXServiceStart( $arrValues['begin_x_service_start'] );
		if( isset( $arrValues['begin_y_service_start'] ) && $boolDirectSet ) $this->set( 'm_intBeginYServiceStart', trim( $arrValues['begin_y_service_start'] ) ); elseif( isset( $arrValues['begin_y_service_start'] ) ) $this->setBeginYServiceStart( $arrValues['begin_y_service_start'] );
		if( isset( $arrValues['service_start_width'] ) && $boolDirectSet ) $this->set( 'm_intServiceStartWidth', trim( $arrValues['service_start_width'] ) ); elseif( isset( $arrValues['service_start_width'] ) ) $this->setServiceStartWidth( $arrValues['service_start_width'] );
		if( isset( $arrValues['service_start_height'] ) && $boolDirectSet ) $this->set( 'm_intServiceStartHeight', trim( $arrValues['service_start_height'] ) ); elseif( isset( $arrValues['service_start_height'] ) ) $this->setServiceStartHeight( $arrValues['service_start_height'] );
		if( isset( $arrValues['begin_x_service_end'] ) && $boolDirectSet ) $this->set( 'm_intBeginXServiceEnd', trim( $arrValues['begin_x_service_end'] ) ); elseif( isset( $arrValues['begin_x_service_end'] ) ) $this->setBeginXServiceEnd( $arrValues['begin_x_service_end'] );
		if( isset( $arrValues['begin_y_service_end'] ) && $boolDirectSet ) $this->set( 'm_intBeginYServiceEnd', trim( $arrValues['begin_y_service_end'] ) ); elseif( isset( $arrValues['begin_y_service_end'] ) ) $this->setBeginYServiceEnd( $arrValues['begin_y_service_end'] );
		if( isset( $arrValues['service_end_width'] ) && $boolDirectSet ) $this->set( 'm_intServiceEndWidth', trim( $arrValues['service_end_width'] ) ); elseif( isset( $arrValues['service_end_width'] ) ) $this->setServiceEndWidth( $arrValues['service_end_width'] );
		if( isset( $arrValues['service_end_height'] ) && $boolDirectSet ) $this->set( 'm_intServiceEndHeight', trim( $arrValues['service_end_height'] ) ); elseif( isset( $arrValues['service_end_height'] ) ) $this->setServiceEndHeight( $arrValues['service_end_height'] );
		if( isset( $arrValues['begin_x_bill_date'] ) && $boolDirectSet ) $this->set( 'm_intBeginXBillDate', trim( $arrValues['begin_x_bill_date'] ) ); elseif( isset( $arrValues['begin_x_bill_date'] ) ) $this->setBeginXBillDate( $arrValues['begin_x_bill_date'] );
		if( isset( $arrValues['begin_y_bill_date'] ) && $boolDirectSet ) $this->set( 'm_intBeginYBillDate', trim( $arrValues['begin_y_bill_date'] ) ); elseif( isset( $arrValues['begin_y_bill_date'] ) ) $this->setBeginYBillDate( $arrValues['begin_y_bill_date'] );
		if( isset( $arrValues['bill_date_width'] ) && $boolDirectSet ) $this->set( 'm_intBillDateWidth', trim( $arrValues['bill_date_width'] ) ); elseif( isset( $arrValues['bill_date_width'] ) ) $this->setBillDateWidth( $arrValues['bill_date_width'] );
		if( isset( $arrValues['bill_date_height'] ) && $boolDirectSet ) $this->set( 'm_intBillDateHeight', trim( $arrValues['bill_date_height'] ) ); elseif( isset( $arrValues['bill_date_height'] ) ) $this->setBillDateHeight( $arrValues['bill_date_height'] );
		if( isset( $arrValues['begin_x_due_date'] ) && $boolDirectSet ) $this->set( 'm_intBeginXDueDate', trim( $arrValues['begin_x_due_date'] ) ); elseif( isset( $arrValues['begin_x_due_date'] ) ) $this->setBeginXDueDate( $arrValues['begin_x_due_date'] );
		if( isset( $arrValues['begin_y_due_date'] ) && $boolDirectSet ) $this->set( 'm_intBeginYDueDate', trim( $arrValues['begin_y_due_date'] ) ); elseif( isset( $arrValues['begin_y_due_date'] ) ) $this->setBeginYDueDate( $arrValues['begin_y_due_date'] );
		if( isset( $arrValues['due_date_width'] ) && $boolDirectSet ) $this->set( 'm_intDueDateWidth', trim( $arrValues['due_date_width'] ) ); elseif( isset( $arrValues['due_date_width'] ) ) $this->setDueDateWidth( $arrValues['due_date_width'] );
		if( isset( $arrValues['due_date_height'] ) && $boolDirectSet ) $this->set( 'm_intDueDateHeight', trim( $arrValues['due_date_height'] ) ); elseif( isset( $arrValues['due_date_height'] ) ) $this->setDueDateHeight( $arrValues['due_date_height'] );
		if( isset( $arrValues['begin_x_account_number'] ) && $boolDirectSet ) $this->set( 'm_intBeginXAccountNumber', trim( $arrValues['begin_x_account_number'] ) ); elseif( isset( $arrValues['begin_x_account_number'] ) ) $this->setBeginXAccountNumber( $arrValues['begin_x_account_number'] );
		if( isset( $arrValues['begin_y_account_number'] ) && $boolDirectSet ) $this->set( 'm_intBeginYAccountNumber', trim( $arrValues['begin_y_account_number'] ) ); elseif( isset( $arrValues['begin_y_account_number'] ) ) $this->setBeginYAccountNumber( $arrValues['begin_y_account_number'] );
		if( isset( $arrValues['account_number_width'] ) && $boolDirectSet ) $this->set( 'm_intAccountNumberWidth', trim( $arrValues['account_number_width'] ) ); elseif( isset( $arrValues['account_number_width'] ) ) $this->setAccountNumberWidth( $arrValues['account_number_width'] );
		if( isset( $arrValues['account_number_height'] ) && $boolDirectSet ) $this->set( 'm_intAccountNumberHeight', trim( $arrValues['account_number_height'] ) ); elseif( isset( $arrValues['account_number_height'] ) ) $this->setAccountNumberHeight( $arrValues['account_number_height'] );
		if( isset( $arrValues['begin_x_bill_number'] ) && $boolDirectSet ) $this->set( 'm_intBeginXBillNumber', trim( $arrValues['begin_x_bill_number'] ) ); elseif( isset( $arrValues['begin_x_bill_number'] ) ) $this->setBeginXBillNumber( $arrValues['begin_x_bill_number'] );
		if( isset( $arrValues['begin_y_bill_number'] ) && $boolDirectSet ) $this->set( 'm_intBeginYBillNumber', trim( $arrValues['begin_y_bill_number'] ) ); elseif( isset( $arrValues['begin_y_bill_number'] ) ) $this->setBeginYBillNumber( $arrValues['begin_y_bill_number'] );
		if( isset( $arrValues['bill_number_width'] ) && $boolDirectSet ) $this->set( 'm_intBillNumberWidth', trim( $arrValues['bill_number_width'] ) ); elseif( isset( $arrValues['bill_number_width'] ) ) $this->setBillNumberWidth( $arrValues['bill_number_width'] );
		if( isset( $arrValues['bill_number_height'] ) && $boolDirectSet ) $this->set( 'm_intBillNumberHeight', trim( $arrValues['bill_number_height'] ) ); elseif( isset( $arrValues['bill_number_height'] ) ) $this->setBillNumberHeight( $arrValues['bill_number_height'] );
		if( isset( $arrValues['begin_x_meter_start_read'] ) && $boolDirectSet ) $this->set( 'm_intBeginXMeterStartRead', trim( $arrValues['begin_x_meter_start_read'] ) ); elseif( isset( $arrValues['begin_x_meter_start_read'] ) ) $this->setBeginXMeterStartRead( $arrValues['begin_x_meter_start_read'] );
		if( isset( $arrValues['begin_y_meter_start_read'] ) && $boolDirectSet ) $this->set( 'm_intBeginYMeterStartRead', trim( $arrValues['begin_y_meter_start_read'] ) ); elseif( isset( $arrValues['begin_y_meter_start_read'] ) ) $this->setBeginYMeterStartRead( $arrValues['begin_y_meter_start_read'] );
		if( isset( $arrValues['meter_start_read_width'] ) && $boolDirectSet ) $this->set( 'm_intMeterStartReadWidth', trim( $arrValues['meter_start_read_width'] ) ); elseif( isset( $arrValues['meter_start_read_width'] ) ) $this->setMeterStartReadWidth( $arrValues['meter_start_read_width'] );
		if( isset( $arrValues['meter_start_read_height'] ) && $boolDirectSet ) $this->set( 'm_intMeterStartReadHeight', trim( $arrValues['meter_start_read_height'] ) ); elseif( isset( $arrValues['meter_start_read_height'] ) ) $this->setMeterStartReadHeight( $arrValues['meter_start_read_height'] );
		if( isset( $arrValues['begin_x_meter_end_read'] ) && $boolDirectSet ) $this->set( 'm_intBeginXMeterEndRead', trim( $arrValues['begin_x_meter_end_read'] ) ); elseif( isset( $arrValues['begin_x_meter_end_read'] ) ) $this->setBeginXMeterEndRead( $arrValues['begin_x_meter_end_read'] );
		if( isset( $arrValues['begin_y_meter_end_read'] ) && $boolDirectSet ) $this->set( 'm_intBeginYMeterEndRead', trim( $arrValues['begin_y_meter_end_read'] ) ); elseif( isset( $arrValues['begin_y_meter_end_read'] ) ) $this->setBeginYMeterEndRead( $arrValues['begin_y_meter_end_read'] );
		if( isset( $arrValues['meter_end_read_width'] ) && $boolDirectSet ) $this->set( 'm_intMeterEndReadWidth', trim( $arrValues['meter_end_read_width'] ) ); elseif( isset( $arrValues['meter_end_read_width'] ) ) $this->setMeterEndReadWidth( $arrValues['meter_end_read_width'] );
		if( isset( $arrValues['meter_end_read_height'] ) && $boolDirectSet ) $this->set( 'm_intMeterEndReadHeight', trim( $arrValues['meter_end_read_height'] ) ); elseif( isset( $arrValues['meter_end_read_height'] ) ) $this->setMeterEndReadHeight( $arrValues['meter_end_read_height'] );
		if( isset( $arrValues['begin_x_bill_amount'] ) && $boolDirectSet ) $this->set( 'm_intBeginXBillAmount', trim( $arrValues['begin_x_bill_amount'] ) ); elseif( isset( $arrValues['begin_x_bill_amount'] ) ) $this->setBeginXBillAmount( $arrValues['begin_x_bill_amount'] );
		if( isset( $arrValues['begin_y_bill_amount'] ) && $boolDirectSet ) $this->set( 'm_intBeginYBillAmount', trim( $arrValues['begin_y_bill_amount'] ) ); elseif( isset( $arrValues['begin_y_bill_amount'] ) ) $this->setBeginYBillAmount( $arrValues['begin_y_bill_amount'] );
		if( isset( $arrValues['bill_amount_width'] ) && $boolDirectSet ) $this->set( 'm_intBillAmountWidth', trim( $arrValues['bill_amount_width'] ) ); elseif( isset( $arrValues['bill_amount_width'] ) ) $this->setBillAmountWidth( $arrValues['bill_amount_width'] );
		if( isset( $arrValues['bill_amount_height'] ) && $boolDirectSet ) $this->set( 'm_intBillAmountHeight', trim( $arrValues['bill_amount_height'] ) ); elseif( isset( $arrValues['bill_amount_height'] ) ) $this->setBillAmountHeight( $arrValues['bill_amount_height'] );
		if( isset( $arrValues['begin_x_remittance_stub'] ) && $boolDirectSet ) $this->set( 'm_intBeginXRemittanceStub', trim( $arrValues['begin_x_remittance_stub'] ) ); elseif( isset( $arrValues['begin_x_remittance_stub'] ) ) $this->setBeginXRemittanceStub( $arrValues['begin_x_remittance_stub'] );
		if( isset( $arrValues['begin_y_remittance_stub'] ) && $boolDirectSet ) $this->set( 'm_intBeginYRemittanceStub', trim( $arrValues['begin_y_remittance_stub'] ) ); elseif( isset( $arrValues['begin_y_remittance_stub'] ) ) $this->setBeginYRemittanceStub( $arrValues['begin_y_remittance_stub'] );
		if( isset( $arrValues['remittance_stub_height'] ) && $boolDirectSet ) $this->set( 'm_intRemittanceStubHeight', trim( $arrValues['remittance_stub_height'] ) ); elseif( isset( $arrValues['remittance_stub_height'] ) ) $this->setRemittanceStubHeight( $arrValues['remittance_stub_height'] );
		if( isset( $arrValues['remittance_stub_width'] ) && $boolDirectSet ) $this->set( 'm_intRemittanceStubWidth', trim( $arrValues['remittance_stub_width'] ) ); elseif( isset( $arrValues['remittance_stub_width'] ) ) $this->setRemittanceStubWidth( $arrValues['remittance_stub_width'] );
		if( isset( $arrValues['service_start_page_number'] ) && $boolDirectSet ) $this->set( 'm_intServiceStartPageNumber', trim( $arrValues['service_start_page_number'] ) ); elseif( isset( $arrValues['service_start_page_number'] ) ) $this->setServiceStartPageNumber( $arrValues['service_start_page_number'] );
		if( isset( $arrValues['service_end_page_number'] ) && $boolDirectSet ) $this->set( 'm_intServiceEndPageNumber', trim( $arrValues['service_end_page_number'] ) ); elseif( isset( $arrValues['service_end_page_number'] ) ) $this->setServiceEndPageNumber( $arrValues['service_end_page_number'] );
		if( isset( $arrValues['bill_date_page_number'] ) && $boolDirectSet ) $this->set( 'm_intBillDatePageNumber', trim( $arrValues['bill_date_page_number'] ) ); elseif( isset( $arrValues['bill_date_page_number'] ) ) $this->setBillDatePageNumber( $arrValues['bill_date_page_number'] );
		if( isset( $arrValues['show_service_end_date'] ) && $boolDirectSet ) $this->set( 'm_boolShowServiceEndDate', trim( stripcslashes( $arrValues['show_service_end_date'] ) ) ); elseif( isset( $arrValues['show_service_end_date'] ) ) $this->setShowServiceEndDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_service_end_date'] ) : $arrValues['show_service_end_date'] );
		if( isset( $arrValues['show_service_start_date'] ) && $boolDirectSet ) $this->set( 'm_boolShowServiceStartDate', trim( stripcslashes( $arrValues['show_service_start_date'] ) ) ); elseif( isset( $arrValues['show_service_start_date'] ) ) $this->setShowServiceStartDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_service_start_date'] ) : $arrValues['show_service_start_date'] );
		if( isset( $arrValues['due_date_page_number'] ) && $boolDirectSet ) $this->set( 'm_intDueDatePageNumber', trim( $arrValues['due_date_page_number'] ) ); elseif( isset( $arrValues['due_date_page_number'] ) ) $this->setDueDatePageNumber( $arrValues['due_date_page_number'] );
		if( isset( $arrValues['account_number_page_number'] ) && $boolDirectSet ) $this->set( 'm_intAccountNumberPageNumber', trim( $arrValues['account_number_page_number'] ) ); elseif( isset( $arrValues['account_number_page_number'] ) ) $this->setAccountNumberPageNumber( $arrValues['account_number_page_number'] );
		if( isset( $arrValues['bill_number_page_number'] ) && $boolDirectSet ) $this->set( 'm_intBillNumberPageNumber', trim( $arrValues['bill_number_page_number'] ) ); elseif( isset( $arrValues['bill_number_page_number'] ) ) $this->setBillNumberPageNumber( $arrValues['bill_number_page_number'] );
		if( isset( $arrValues['meter_start_read_page_number'] ) && $boolDirectSet ) $this->set( 'm_intMeterStartReadPageNumber', trim( $arrValues['meter_start_read_page_number'] ) ); elseif( isset( $arrValues['meter_start_read_page_number'] ) ) $this->setMeterStartReadPageNumber( $arrValues['meter_start_read_page_number'] );
		if( isset( $arrValues['meter_end_read_page_number'] ) && $boolDirectSet ) $this->set( 'm_intMeterEndReadPageNumber', trim( $arrValues['meter_end_read_page_number'] ) ); elseif( isset( $arrValues['meter_end_read_page_number'] ) ) $this->setMeterEndReadPageNumber( $arrValues['meter_end_read_page_number'] );
		if( isset( $arrValues['bill_amount_page_number'] ) && $boolDirectSet ) $this->set( 'm_intBillAmountPageNumber', trim( $arrValues['bill_amount_page_number'] ) ); elseif( isset( $arrValues['bill_amount_page_number'] ) ) $this->setBillAmountPageNumber( $arrValues['bill_amount_page_number'] );
		if( isset( $arrValues['remittance_stub_page_number'] ) && $boolDirectSet ) $this->set( 'm_intRemittanceStubPageNumber', trim( $arrValues['remittance_stub_page_number'] ) ); elseif( isset( $arrValues['remittance_stub_page_number'] ) ) $this->setRemittanceStubPageNumber( $arrValues['remittance_stub_page_number'] );
		if( isset( $arrValues['show_bill_date'] ) && $boolDirectSet ) $this->set( 'm_boolShowBillDate', trim( stripcslashes( $arrValues['show_bill_date'] ) ) ); elseif( isset( $arrValues['show_bill_date'] ) ) $this->setShowBillDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_bill_date'] ) : $arrValues['show_bill_date'] );
		if( isset( $arrValues['show_due_date'] ) && $boolDirectSet ) $this->set( 'm_boolShowDueDate', trim( stripcslashes( $arrValues['show_due_date'] ) ) ); elseif( isset( $arrValues['show_due_date'] ) ) $this->setShowDueDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_due_date'] ) : $arrValues['show_due_date'] );
		if( isset( $arrValues['show_bill_number'] ) && $boolDirectSet ) $this->set( 'm_boolShowBillNumber', trim( stripcslashes( $arrValues['show_bill_number'] ) ) ); elseif( isset( $arrValues['show_bill_number'] ) ) $this->setShowBillNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_bill_number'] ) : $arrValues['show_bill_number'] );
		if( isset( $arrValues['show_meter_start_read'] ) && $boolDirectSet ) $this->set( 'm_boolShowMeterStartRead', trim( stripcslashes( $arrValues['show_meter_start_read'] ) ) ); elseif( isset( $arrValues['show_meter_start_read'] ) ) $this->setShowMeterStartRead( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_meter_start_read'] ) : $arrValues['show_meter_start_read'] );
		if( isset( $arrValues['show_meter_end_read'] ) && $boolDirectSet ) $this->set( 'm_boolShowMeterEndRead', trim( stripcslashes( $arrValues['show_meter_end_read'] ) ) ); elseif( isset( $arrValues['show_meter_end_read'] ) ) $this->setShowMeterEndRead( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_meter_end_read'] ) : $arrValues['show_meter_end_read'] );
		if( isset( $arrValues['show_bill_amount'] ) && $boolDirectSet ) $this->set( 'm_boolShowBillAmount', trim( stripcslashes( $arrValues['show_bill_amount'] ) ) ); elseif( isset( $arrValues['show_bill_amount'] ) ) $this->setShowBillAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_bill_amount'] ) : $arrValues['show_bill_amount'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['attempt_ocr'] ) && $boolDirectSet ) $this->set( 'm_boolAttemptOcr', trim( stripcslashes( $arrValues['attempt_ocr'] ) ) ); elseif( isset( $arrValues['attempt_ocr'] ) ) $this->setAttemptOcr( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['attempt_ocr'] ) : $arrValues['attempt_ocr'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['require_multiplier'] ) && $boolDirectSet ) $this->set( 'm_boolRequireMultiplier', trim( stripcslashes( $arrValues['require_multiplier'] ) ) ); elseif( isset( $arrValues['require_multiplier'] ) ) $this->setRequireMultiplier( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_multiplier'] ) : $arrValues['require_multiplier'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityProviderId( $intUtilityProviderId ) {
		$this->set( 'm_intUtilityProviderId', CStrings::strToIntDef( $intUtilityProviderId, NULL, false ) );
	}

	public function getUtilityProviderId() {
		return $this->m_intUtilityProviderId;
	}

	public function sqlUtilityProviderId() {
		return ( true == isset( $this->m_intUtilityProviderId ) ) ? ( string ) $this->m_intUtilityProviderId : 'NULL';
	}

	public function setSampleUtilityDocumentId( $intSampleUtilityDocumentId ) {
		$this->set( 'm_intSampleUtilityDocumentId', CStrings::strToIntDef( $intSampleUtilityDocumentId, NULL, false ) );
	}

	public function getSampleUtilityDocumentId() {
		return $this->m_intSampleUtilityDocumentId;
	}

	public function sqlSampleUtilityDocumentId() {
		return ( true == isset( $this->m_intSampleUtilityDocumentId ) ) ? ( string ) $this->m_intSampleUtilityDocumentId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setTemplateDatetime( $strTemplateDatetime ) {
		$this->set( 'm_strTemplateDatetime', CStrings::strTrimDef( $strTemplateDatetime, -1, NULL, true ) );
	}

	public function getTemplateDatetime() {
		return $this->m_strTemplateDatetime;
	}

	public function sqlTemplateDatetime() {
		return ( true == isset( $this->m_strTemplateDatetime ) ) ? '\'' . $this->m_strTemplateDatetime . '\'' : 'NOW()';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setBeginXServiceStart( $intBeginXServiceStart ) {
		$this->set( 'm_intBeginXServiceStart', CStrings::strToIntDef( $intBeginXServiceStart, NULL, false ) );
	}

	public function getBeginXServiceStart() {
		return $this->m_intBeginXServiceStart;
	}

	public function sqlBeginXServiceStart() {
		return ( true == isset( $this->m_intBeginXServiceStart ) ) ? ( string ) $this->m_intBeginXServiceStart : 'NULL';
	}

	public function setBeginYServiceStart( $intBeginYServiceStart ) {
		$this->set( 'm_intBeginYServiceStart', CStrings::strToIntDef( $intBeginYServiceStart, NULL, false ) );
	}

	public function getBeginYServiceStart() {
		return $this->m_intBeginYServiceStart;
	}

	public function sqlBeginYServiceStart() {
		return ( true == isset( $this->m_intBeginYServiceStart ) ) ? ( string ) $this->m_intBeginYServiceStart : 'NULL';
	}

	public function setServiceStartWidth( $intServiceStartWidth ) {
		$this->set( 'm_intServiceStartWidth', CStrings::strToIntDef( $intServiceStartWidth, NULL, false ) );
	}

	public function getServiceStartWidth() {
		return $this->m_intServiceStartWidth;
	}

	public function sqlServiceStartWidth() {
		return ( true == isset( $this->m_intServiceStartWidth ) ) ? ( string ) $this->m_intServiceStartWidth : 'NULL';
	}

	public function setServiceStartHeight( $intServiceStartHeight ) {
		$this->set( 'm_intServiceStartHeight', CStrings::strToIntDef( $intServiceStartHeight, NULL, false ) );
	}

	public function getServiceStartHeight() {
		return $this->m_intServiceStartHeight;
	}

	public function sqlServiceStartHeight() {
		return ( true == isset( $this->m_intServiceStartHeight ) ) ? ( string ) $this->m_intServiceStartHeight : 'NULL';
	}

	public function setBeginXServiceEnd( $intBeginXServiceEnd ) {
		$this->set( 'm_intBeginXServiceEnd', CStrings::strToIntDef( $intBeginXServiceEnd, NULL, false ) );
	}

	public function getBeginXServiceEnd() {
		return $this->m_intBeginXServiceEnd;
	}

	public function sqlBeginXServiceEnd() {
		return ( true == isset( $this->m_intBeginXServiceEnd ) ) ? ( string ) $this->m_intBeginXServiceEnd : 'NULL';
	}

	public function setBeginYServiceEnd( $intBeginYServiceEnd ) {
		$this->set( 'm_intBeginYServiceEnd', CStrings::strToIntDef( $intBeginYServiceEnd, NULL, false ) );
	}

	public function getBeginYServiceEnd() {
		return $this->m_intBeginYServiceEnd;
	}

	public function sqlBeginYServiceEnd() {
		return ( true == isset( $this->m_intBeginYServiceEnd ) ) ? ( string ) $this->m_intBeginYServiceEnd : 'NULL';
	}

	public function setServiceEndWidth( $intServiceEndWidth ) {
		$this->set( 'm_intServiceEndWidth', CStrings::strToIntDef( $intServiceEndWidth, NULL, false ) );
	}

	public function getServiceEndWidth() {
		return $this->m_intServiceEndWidth;
	}

	public function sqlServiceEndWidth() {
		return ( true == isset( $this->m_intServiceEndWidth ) ) ? ( string ) $this->m_intServiceEndWidth : 'NULL';
	}

	public function setServiceEndHeight( $intServiceEndHeight ) {
		$this->set( 'm_intServiceEndHeight', CStrings::strToIntDef( $intServiceEndHeight, NULL, false ) );
	}

	public function getServiceEndHeight() {
		return $this->m_intServiceEndHeight;
	}

	public function sqlServiceEndHeight() {
		return ( true == isset( $this->m_intServiceEndHeight ) ) ? ( string ) $this->m_intServiceEndHeight : 'NULL';
	}

	public function setBeginXBillDate( $intBeginXBillDate ) {
		$this->set( 'm_intBeginXBillDate', CStrings::strToIntDef( $intBeginXBillDate, NULL, false ) );
	}

	public function getBeginXBillDate() {
		return $this->m_intBeginXBillDate;
	}

	public function sqlBeginXBillDate() {
		return ( true == isset( $this->m_intBeginXBillDate ) ) ? ( string ) $this->m_intBeginXBillDate : 'NULL';
	}

	public function setBeginYBillDate( $intBeginYBillDate ) {
		$this->set( 'm_intBeginYBillDate', CStrings::strToIntDef( $intBeginYBillDate, NULL, false ) );
	}

	public function getBeginYBillDate() {
		return $this->m_intBeginYBillDate;
	}

	public function sqlBeginYBillDate() {
		return ( true == isset( $this->m_intBeginYBillDate ) ) ? ( string ) $this->m_intBeginYBillDate : 'NULL';
	}

	public function setBillDateWidth( $intBillDateWidth ) {
		$this->set( 'm_intBillDateWidth', CStrings::strToIntDef( $intBillDateWidth, NULL, false ) );
	}

	public function getBillDateWidth() {
		return $this->m_intBillDateWidth;
	}

	public function sqlBillDateWidth() {
		return ( true == isset( $this->m_intBillDateWidth ) ) ? ( string ) $this->m_intBillDateWidth : 'NULL';
	}

	public function setBillDateHeight( $intBillDateHeight ) {
		$this->set( 'm_intBillDateHeight', CStrings::strToIntDef( $intBillDateHeight, NULL, false ) );
	}

	public function getBillDateHeight() {
		return $this->m_intBillDateHeight;
	}

	public function sqlBillDateHeight() {
		return ( true == isset( $this->m_intBillDateHeight ) ) ? ( string ) $this->m_intBillDateHeight : 'NULL';
	}

	public function setBeginXDueDate( $intBeginXDueDate ) {
		$this->set( 'm_intBeginXDueDate', CStrings::strToIntDef( $intBeginXDueDate, NULL, false ) );
	}

	public function getBeginXDueDate() {
		return $this->m_intBeginXDueDate;
	}

	public function sqlBeginXDueDate() {
		return ( true == isset( $this->m_intBeginXDueDate ) ) ? ( string ) $this->m_intBeginXDueDate : 'NULL';
	}

	public function setBeginYDueDate( $intBeginYDueDate ) {
		$this->set( 'm_intBeginYDueDate', CStrings::strToIntDef( $intBeginYDueDate, NULL, false ) );
	}

	public function getBeginYDueDate() {
		return $this->m_intBeginYDueDate;
	}

	public function sqlBeginYDueDate() {
		return ( true == isset( $this->m_intBeginYDueDate ) ) ? ( string ) $this->m_intBeginYDueDate : 'NULL';
	}

	public function setDueDateWidth( $intDueDateWidth ) {
		$this->set( 'm_intDueDateWidth', CStrings::strToIntDef( $intDueDateWidth, NULL, false ) );
	}

	public function getDueDateWidth() {
		return $this->m_intDueDateWidth;
	}

	public function sqlDueDateWidth() {
		return ( true == isset( $this->m_intDueDateWidth ) ) ? ( string ) $this->m_intDueDateWidth : 'NULL';
	}

	public function setDueDateHeight( $intDueDateHeight ) {
		$this->set( 'm_intDueDateHeight', CStrings::strToIntDef( $intDueDateHeight, NULL, false ) );
	}

	public function getDueDateHeight() {
		return $this->m_intDueDateHeight;
	}

	public function sqlDueDateHeight() {
		return ( true == isset( $this->m_intDueDateHeight ) ) ? ( string ) $this->m_intDueDateHeight : 'NULL';
	}

	public function setBeginXAccountNumber( $intBeginXAccountNumber ) {
		$this->set( 'm_intBeginXAccountNumber', CStrings::strToIntDef( $intBeginXAccountNumber, NULL, false ) );
	}

	public function getBeginXAccountNumber() {
		return $this->m_intBeginXAccountNumber;
	}

	public function sqlBeginXAccountNumber() {
		return ( true == isset( $this->m_intBeginXAccountNumber ) ) ? ( string ) $this->m_intBeginXAccountNumber : 'NULL';
	}

	public function setBeginYAccountNumber( $intBeginYAccountNumber ) {
		$this->set( 'm_intBeginYAccountNumber', CStrings::strToIntDef( $intBeginYAccountNumber, NULL, false ) );
	}

	public function getBeginYAccountNumber() {
		return $this->m_intBeginYAccountNumber;
	}

	public function sqlBeginYAccountNumber() {
		return ( true == isset( $this->m_intBeginYAccountNumber ) ) ? ( string ) $this->m_intBeginYAccountNumber : 'NULL';
	}

	public function setAccountNumberWidth( $intAccountNumberWidth ) {
		$this->set( 'm_intAccountNumberWidth', CStrings::strToIntDef( $intAccountNumberWidth, NULL, false ) );
	}

	public function getAccountNumberWidth() {
		return $this->m_intAccountNumberWidth;
	}

	public function sqlAccountNumberWidth() {
		return ( true == isset( $this->m_intAccountNumberWidth ) ) ? ( string ) $this->m_intAccountNumberWidth : 'NULL';
	}

	public function setAccountNumberHeight( $intAccountNumberHeight ) {
		$this->set( 'm_intAccountNumberHeight', CStrings::strToIntDef( $intAccountNumberHeight, NULL, false ) );
	}

	public function getAccountNumberHeight() {
		return $this->m_intAccountNumberHeight;
	}

	public function sqlAccountNumberHeight() {
		return ( true == isset( $this->m_intAccountNumberHeight ) ) ? ( string ) $this->m_intAccountNumberHeight : 'NULL';
	}

	public function setBeginXBillNumber( $intBeginXBillNumber ) {
		$this->set( 'm_intBeginXBillNumber', CStrings::strToIntDef( $intBeginXBillNumber, NULL, false ) );
	}

	public function getBeginXBillNumber() {
		return $this->m_intBeginXBillNumber;
	}

	public function sqlBeginXBillNumber() {
		return ( true == isset( $this->m_intBeginXBillNumber ) ) ? ( string ) $this->m_intBeginXBillNumber : 'NULL';
	}

	public function setBeginYBillNumber( $intBeginYBillNumber ) {
		$this->set( 'm_intBeginYBillNumber', CStrings::strToIntDef( $intBeginYBillNumber, NULL, false ) );
	}

	public function getBeginYBillNumber() {
		return $this->m_intBeginYBillNumber;
	}

	public function sqlBeginYBillNumber() {
		return ( true == isset( $this->m_intBeginYBillNumber ) ) ? ( string ) $this->m_intBeginYBillNumber : 'NULL';
	}

	public function setBillNumberWidth( $intBillNumberWidth ) {
		$this->set( 'm_intBillNumberWidth', CStrings::strToIntDef( $intBillNumberWidth, NULL, false ) );
	}

	public function getBillNumberWidth() {
		return $this->m_intBillNumberWidth;
	}

	public function sqlBillNumberWidth() {
		return ( true == isset( $this->m_intBillNumberWidth ) ) ? ( string ) $this->m_intBillNumberWidth : 'NULL';
	}

	public function setBillNumberHeight( $intBillNumberHeight ) {
		$this->set( 'm_intBillNumberHeight', CStrings::strToIntDef( $intBillNumberHeight, NULL, false ) );
	}

	public function getBillNumberHeight() {
		return $this->m_intBillNumberHeight;
	}

	public function sqlBillNumberHeight() {
		return ( true == isset( $this->m_intBillNumberHeight ) ) ? ( string ) $this->m_intBillNumberHeight : 'NULL';
	}

	public function setBeginXMeterStartRead( $intBeginXMeterStartRead ) {
		$this->set( 'm_intBeginXMeterStartRead', CStrings::strToIntDef( $intBeginXMeterStartRead, NULL, false ) );
	}

	public function getBeginXMeterStartRead() {
		return $this->m_intBeginXMeterStartRead;
	}

	public function sqlBeginXMeterStartRead() {
		return ( true == isset( $this->m_intBeginXMeterStartRead ) ) ? ( string ) $this->m_intBeginXMeterStartRead : 'NULL';
	}

	public function setBeginYMeterStartRead( $intBeginYMeterStartRead ) {
		$this->set( 'm_intBeginYMeterStartRead', CStrings::strToIntDef( $intBeginYMeterStartRead, NULL, false ) );
	}

	public function getBeginYMeterStartRead() {
		return $this->m_intBeginYMeterStartRead;
	}

	public function sqlBeginYMeterStartRead() {
		return ( true == isset( $this->m_intBeginYMeterStartRead ) ) ? ( string ) $this->m_intBeginYMeterStartRead : 'NULL';
	}

	public function setMeterStartReadWidth( $intMeterStartReadWidth ) {
		$this->set( 'm_intMeterStartReadWidth', CStrings::strToIntDef( $intMeterStartReadWidth, NULL, false ) );
	}

	public function getMeterStartReadWidth() {
		return $this->m_intMeterStartReadWidth;
	}

	public function sqlMeterStartReadWidth() {
		return ( true == isset( $this->m_intMeterStartReadWidth ) ) ? ( string ) $this->m_intMeterStartReadWidth : 'NULL';
	}

	public function setMeterStartReadHeight( $intMeterStartReadHeight ) {
		$this->set( 'm_intMeterStartReadHeight', CStrings::strToIntDef( $intMeterStartReadHeight, NULL, false ) );
	}

	public function getMeterStartReadHeight() {
		return $this->m_intMeterStartReadHeight;
	}

	public function sqlMeterStartReadHeight() {
		return ( true == isset( $this->m_intMeterStartReadHeight ) ) ? ( string ) $this->m_intMeterStartReadHeight : 'NULL';
	}

	public function setBeginXMeterEndRead( $intBeginXMeterEndRead ) {
		$this->set( 'm_intBeginXMeterEndRead', CStrings::strToIntDef( $intBeginXMeterEndRead, NULL, false ) );
	}

	public function getBeginXMeterEndRead() {
		return $this->m_intBeginXMeterEndRead;
	}

	public function sqlBeginXMeterEndRead() {
		return ( true == isset( $this->m_intBeginXMeterEndRead ) ) ? ( string ) $this->m_intBeginXMeterEndRead : 'NULL';
	}

	public function setBeginYMeterEndRead( $intBeginYMeterEndRead ) {
		$this->set( 'm_intBeginYMeterEndRead', CStrings::strToIntDef( $intBeginYMeterEndRead, NULL, false ) );
	}

	public function getBeginYMeterEndRead() {
		return $this->m_intBeginYMeterEndRead;
	}

	public function sqlBeginYMeterEndRead() {
		return ( true == isset( $this->m_intBeginYMeterEndRead ) ) ? ( string ) $this->m_intBeginYMeterEndRead : 'NULL';
	}

	public function setMeterEndReadWidth( $intMeterEndReadWidth ) {
		$this->set( 'm_intMeterEndReadWidth', CStrings::strToIntDef( $intMeterEndReadWidth, NULL, false ) );
	}

	public function getMeterEndReadWidth() {
		return $this->m_intMeterEndReadWidth;
	}

	public function sqlMeterEndReadWidth() {
		return ( true == isset( $this->m_intMeterEndReadWidth ) ) ? ( string ) $this->m_intMeterEndReadWidth : 'NULL';
	}

	public function setMeterEndReadHeight( $intMeterEndReadHeight ) {
		$this->set( 'm_intMeterEndReadHeight', CStrings::strToIntDef( $intMeterEndReadHeight, NULL, false ) );
	}

	public function getMeterEndReadHeight() {
		return $this->m_intMeterEndReadHeight;
	}

	public function sqlMeterEndReadHeight() {
		return ( true == isset( $this->m_intMeterEndReadHeight ) ) ? ( string ) $this->m_intMeterEndReadHeight : 'NULL';
	}

	public function setBeginXBillAmount( $intBeginXBillAmount ) {
		$this->set( 'm_intBeginXBillAmount', CStrings::strToIntDef( $intBeginXBillAmount, NULL, false ) );
	}

	public function getBeginXBillAmount() {
		return $this->m_intBeginXBillAmount;
	}

	public function sqlBeginXBillAmount() {
		return ( true == isset( $this->m_intBeginXBillAmount ) ) ? ( string ) $this->m_intBeginXBillAmount : 'NULL';
	}

	public function setBeginYBillAmount( $intBeginYBillAmount ) {
		$this->set( 'm_intBeginYBillAmount', CStrings::strToIntDef( $intBeginYBillAmount, NULL, false ) );
	}

	public function getBeginYBillAmount() {
		return $this->m_intBeginYBillAmount;
	}

	public function sqlBeginYBillAmount() {
		return ( true == isset( $this->m_intBeginYBillAmount ) ) ? ( string ) $this->m_intBeginYBillAmount : 'NULL';
	}

	public function setBillAmountWidth( $intBillAmountWidth ) {
		$this->set( 'm_intBillAmountWidth', CStrings::strToIntDef( $intBillAmountWidth, NULL, false ) );
	}

	public function getBillAmountWidth() {
		return $this->m_intBillAmountWidth;
	}

	public function sqlBillAmountWidth() {
		return ( true == isset( $this->m_intBillAmountWidth ) ) ? ( string ) $this->m_intBillAmountWidth : 'NULL';
	}

	public function setBillAmountHeight( $intBillAmountHeight ) {
		$this->set( 'm_intBillAmountHeight', CStrings::strToIntDef( $intBillAmountHeight, NULL, false ) );
	}

	public function getBillAmountHeight() {
		return $this->m_intBillAmountHeight;
	}

	public function sqlBillAmountHeight() {
		return ( true == isset( $this->m_intBillAmountHeight ) ) ? ( string ) $this->m_intBillAmountHeight : 'NULL';
	}

	public function setBeginXRemittanceStub( $intBeginXRemittanceStub ) {
		$this->set( 'm_intBeginXRemittanceStub', CStrings::strToIntDef( $intBeginXRemittanceStub, NULL, false ) );
	}

	public function getBeginXRemittanceStub() {
		return $this->m_intBeginXRemittanceStub;
	}

	public function sqlBeginXRemittanceStub() {
		return ( true == isset( $this->m_intBeginXRemittanceStub ) ) ? ( string ) $this->m_intBeginXRemittanceStub : 'NULL';
	}

	public function setBeginYRemittanceStub( $intBeginYRemittanceStub ) {
		$this->set( 'm_intBeginYRemittanceStub', CStrings::strToIntDef( $intBeginYRemittanceStub, NULL, false ) );
	}

	public function getBeginYRemittanceStub() {
		return $this->m_intBeginYRemittanceStub;
	}

	public function sqlBeginYRemittanceStub() {
		return ( true == isset( $this->m_intBeginYRemittanceStub ) ) ? ( string ) $this->m_intBeginYRemittanceStub : 'NULL';
	}

	public function setRemittanceStubHeight( $intRemittanceStubHeight ) {
		$this->set( 'm_intRemittanceStubHeight', CStrings::strToIntDef( $intRemittanceStubHeight, NULL, false ) );
	}

	public function getRemittanceStubHeight() {
		return $this->m_intRemittanceStubHeight;
	}

	public function sqlRemittanceStubHeight() {
		return ( true == isset( $this->m_intRemittanceStubHeight ) ) ? ( string ) $this->m_intRemittanceStubHeight : 'NULL';
	}

	public function setRemittanceStubWidth( $intRemittanceStubWidth ) {
		$this->set( 'm_intRemittanceStubWidth', CStrings::strToIntDef( $intRemittanceStubWidth, NULL, false ) );
	}

	public function getRemittanceStubWidth() {
		return $this->m_intRemittanceStubWidth;
	}

	public function sqlRemittanceStubWidth() {
		return ( true == isset( $this->m_intRemittanceStubWidth ) ) ? ( string ) $this->m_intRemittanceStubWidth : 'NULL';
	}

	public function setServiceStartPageNumber( $intServiceStartPageNumber ) {
		$this->set( 'm_intServiceStartPageNumber', CStrings::strToIntDef( $intServiceStartPageNumber, NULL, false ) );
	}

	public function getServiceStartPageNumber() {
		return $this->m_intServiceStartPageNumber;
	}

	public function sqlServiceStartPageNumber() {
		return ( true == isset( $this->m_intServiceStartPageNumber ) ) ? ( string ) $this->m_intServiceStartPageNumber : '0';
	}

	public function setServiceEndPageNumber( $intServiceEndPageNumber ) {
		$this->set( 'm_intServiceEndPageNumber', CStrings::strToIntDef( $intServiceEndPageNumber, NULL, false ) );
	}

	public function getServiceEndPageNumber() {
		return $this->m_intServiceEndPageNumber;
	}

	public function sqlServiceEndPageNumber() {
		return ( true == isset( $this->m_intServiceEndPageNumber ) ) ? ( string ) $this->m_intServiceEndPageNumber : '0';
	}

	public function setBillDatePageNumber( $intBillDatePageNumber ) {
		$this->set( 'm_intBillDatePageNumber', CStrings::strToIntDef( $intBillDatePageNumber, NULL, false ) );
	}

	public function getBillDatePageNumber() {
		return $this->m_intBillDatePageNumber;
	}

	public function sqlBillDatePageNumber() {
		return ( true == isset( $this->m_intBillDatePageNumber ) ) ? ( string ) $this->m_intBillDatePageNumber : '0';
	}

	public function setShowServiceEndDate( $boolShowServiceEndDate ) {
		$this->set( 'm_boolShowServiceEndDate', CStrings::strToBool( $boolShowServiceEndDate ) );
	}

	public function getShowServiceEndDate() {
		return $this->m_boolShowServiceEndDate;
	}

	public function sqlShowServiceEndDate() {
		return ( true == isset( $this->m_boolShowServiceEndDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowServiceEndDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowServiceStartDate( $boolShowServiceStartDate ) {
		$this->set( 'm_boolShowServiceStartDate', CStrings::strToBool( $boolShowServiceStartDate ) );
	}

	public function getShowServiceStartDate() {
		return $this->m_boolShowServiceStartDate;
	}

	public function sqlShowServiceStartDate() {
		return ( true == isset( $this->m_boolShowServiceStartDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowServiceStartDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDueDatePageNumber( $intDueDatePageNumber ) {
		$this->set( 'm_intDueDatePageNumber', CStrings::strToIntDef( $intDueDatePageNumber, NULL, false ) );
	}

	public function getDueDatePageNumber() {
		return $this->m_intDueDatePageNumber;
	}

	public function sqlDueDatePageNumber() {
		return ( true == isset( $this->m_intDueDatePageNumber ) ) ? ( string ) $this->m_intDueDatePageNumber : '0';
	}

	public function setAccountNumberPageNumber( $intAccountNumberPageNumber ) {
		$this->set( 'm_intAccountNumberPageNumber', CStrings::strToIntDef( $intAccountNumberPageNumber, NULL, false ) );
	}

	public function getAccountNumberPageNumber() {
		return $this->m_intAccountNumberPageNumber;
	}

	public function sqlAccountNumberPageNumber() {
		return ( true == isset( $this->m_intAccountNumberPageNumber ) ) ? ( string ) $this->m_intAccountNumberPageNumber : '0';
	}

	public function setBillNumberPageNumber( $intBillNumberPageNumber ) {
		$this->set( 'm_intBillNumberPageNumber', CStrings::strToIntDef( $intBillNumberPageNumber, NULL, false ) );
	}

	public function getBillNumberPageNumber() {
		return $this->m_intBillNumberPageNumber;
	}

	public function sqlBillNumberPageNumber() {
		return ( true == isset( $this->m_intBillNumberPageNumber ) ) ? ( string ) $this->m_intBillNumberPageNumber : '0';
	}

	public function setMeterStartReadPageNumber( $intMeterStartReadPageNumber ) {
		$this->set( 'm_intMeterStartReadPageNumber', CStrings::strToIntDef( $intMeterStartReadPageNumber, NULL, false ) );
	}

	public function getMeterStartReadPageNumber() {
		return $this->m_intMeterStartReadPageNumber;
	}

	public function sqlMeterStartReadPageNumber() {
		return ( true == isset( $this->m_intMeterStartReadPageNumber ) ) ? ( string ) $this->m_intMeterStartReadPageNumber : '0';
	}

	public function setMeterEndReadPageNumber( $intMeterEndReadPageNumber ) {
		$this->set( 'm_intMeterEndReadPageNumber', CStrings::strToIntDef( $intMeterEndReadPageNumber, NULL, false ) );
	}

	public function getMeterEndReadPageNumber() {
		return $this->m_intMeterEndReadPageNumber;
	}

	public function sqlMeterEndReadPageNumber() {
		return ( true == isset( $this->m_intMeterEndReadPageNumber ) ) ? ( string ) $this->m_intMeterEndReadPageNumber : '0';
	}

	public function setBillAmountPageNumber( $intBillAmountPageNumber ) {
		$this->set( 'm_intBillAmountPageNumber', CStrings::strToIntDef( $intBillAmountPageNumber, NULL, false ) );
	}

	public function getBillAmountPageNumber() {
		return $this->m_intBillAmountPageNumber;
	}

	public function sqlBillAmountPageNumber() {
		return ( true == isset( $this->m_intBillAmountPageNumber ) ) ? ( string ) $this->m_intBillAmountPageNumber : '0';
	}

	public function setRemittanceStubPageNumber( $intRemittanceStubPageNumber ) {
		$this->set( 'm_intRemittanceStubPageNumber', CStrings::strToIntDef( $intRemittanceStubPageNumber, NULL, false ) );
	}

	public function getRemittanceStubPageNumber() {
		return $this->m_intRemittanceStubPageNumber;
	}

	public function sqlRemittanceStubPageNumber() {
		return ( true == isset( $this->m_intRemittanceStubPageNumber ) ) ? ( string ) $this->m_intRemittanceStubPageNumber : 'NULL';
	}

	public function setShowBillDate( $boolShowBillDate ) {
		$this->set( 'm_boolShowBillDate', CStrings::strToBool( $boolShowBillDate ) );
	}

	public function getShowBillDate() {
		return $this->m_boolShowBillDate;
	}

	public function sqlShowBillDate() {
		return ( true == isset( $this->m_boolShowBillDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowBillDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowDueDate( $boolShowDueDate ) {
		$this->set( 'm_boolShowDueDate', CStrings::strToBool( $boolShowDueDate ) );
	}

	public function getShowDueDate() {
		return $this->m_boolShowDueDate;
	}

	public function sqlShowDueDate() {
		return ( true == isset( $this->m_boolShowDueDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowDueDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowBillNumber( $boolShowBillNumber ) {
		$this->set( 'm_boolShowBillNumber', CStrings::strToBool( $boolShowBillNumber ) );
	}

	public function getShowBillNumber() {
		return $this->m_boolShowBillNumber;
	}

	public function sqlShowBillNumber() {
		return ( true == isset( $this->m_boolShowBillNumber ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowBillNumber ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowMeterStartRead( $boolShowMeterStartRead ) {
		$this->set( 'm_boolShowMeterStartRead', CStrings::strToBool( $boolShowMeterStartRead ) );
	}

	public function getShowMeterStartRead() {
		return $this->m_boolShowMeterStartRead;
	}

	public function sqlShowMeterStartRead() {
		return ( true == isset( $this->m_boolShowMeterStartRead ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowMeterStartRead ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowMeterEndRead( $boolShowMeterEndRead ) {
		$this->set( 'm_boolShowMeterEndRead', CStrings::strToBool( $boolShowMeterEndRead ) );
	}

	public function getShowMeterEndRead() {
		return $this->m_boolShowMeterEndRead;
	}

	public function sqlShowMeterEndRead() {
		return ( true == isset( $this->m_boolShowMeterEndRead ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowMeterEndRead ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowBillAmount( $boolShowBillAmount ) {
		$this->set( 'm_boolShowBillAmount', CStrings::strToBool( $boolShowBillAmount ) );
	}

	public function getShowBillAmount() {
		return $this->m_boolShowBillAmount;
	}

	public function sqlShowBillAmount() {
		return ( true == isset( $this->m_boolShowBillAmount ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowBillAmount ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAttemptOcr( $boolAttemptOcr ) {
		$this->set( 'm_boolAttemptOcr', CStrings::strToBool( $boolAttemptOcr ) );
	}

	public function getAttemptOcr() {
		return $this->m_boolAttemptOcr;
	}

	public function sqlAttemptOcr() {
		return ( true == isset( $this->m_boolAttemptOcr ) ) ? '\'' . ( true == ( bool ) $this->m_boolAttemptOcr ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setRequireMultiplier( $boolRequireMultiplier ) {
		$this->set( 'm_boolRequireMultiplier', CStrings::strToBool( $boolRequireMultiplier ) );
	}

	public function getRequireMultiplier() {
		return $this->m_boolRequireMultiplier;
	}

	public function sqlRequireMultiplier() {
		return ( true == isset( $this->m_boolRequireMultiplier ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireMultiplier ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_provider_id, sample_utility_document_id, name, template_datetime, description, begin_x_service_start, begin_y_service_start, service_start_width, service_start_height, begin_x_service_end, begin_y_service_end, service_end_width, service_end_height, begin_x_bill_date, begin_y_bill_date, bill_date_width, bill_date_height, begin_x_due_date, begin_y_due_date, due_date_width, due_date_height, begin_x_account_number, begin_y_account_number, account_number_width, account_number_height, begin_x_bill_number, begin_y_bill_number, bill_number_width, bill_number_height, begin_x_meter_start_read, begin_y_meter_start_read, meter_start_read_width, meter_start_read_height, begin_x_meter_end_read, begin_y_meter_end_read, meter_end_read_width, meter_end_read_height, begin_x_bill_amount, begin_y_bill_amount, bill_amount_width, bill_amount_height, begin_x_remittance_stub, begin_y_remittance_stub, remittance_stub_height, remittance_stub_width, service_start_page_number, service_end_page_number, bill_date_page_number, show_service_end_date, show_service_start_date, due_date_page_number, account_number_page_number, bill_number_page_number, meter_start_read_page_number, meter_end_read_page_number, bill_amount_page_number, remittance_stub_page_number, show_bill_date, show_due_date, show_bill_number, show_meter_start_read, show_meter_end_read, show_bill_amount, is_published, attempt_ocr, order_num, require_multiplier, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUtilityProviderId() . ', ' .
 						$this->sqlSampleUtilityDocumentId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlTemplateDatetime() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlBeginXServiceStart() . ', ' .
 						$this->sqlBeginYServiceStart() . ', ' .
 						$this->sqlServiceStartWidth() . ', ' .
 						$this->sqlServiceStartHeight() . ', ' .
 						$this->sqlBeginXServiceEnd() . ', ' .
 						$this->sqlBeginYServiceEnd() . ', ' .
 						$this->sqlServiceEndWidth() . ', ' .
 						$this->sqlServiceEndHeight() . ', ' .
 						$this->sqlBeginXBillDate() . ', ' .
 						$this->sqlBeginYBillDate() . ', ' .
 						$this->sqlBillDateWidth() . ', ' .
 						$this->sqlBillDateHeight() . ', ' .
 						$this->sqlBeginXDueDate() . ', ' .
 						$this->sqlBeginYDueDate() . ', ' .
 						$this->sqlDueDateWidth() . ', ' .
 						$this->sqlDueDateHeight() . ', ' .
 						$this->sqlBeginXAccountNumber() . ', ' .
 						$this->sqlBeginYAccountNumber() . ', ' .
 						$this->sqlAccountNumberWidth() . ', ' .
 						$this->sqlAccountNumberHeight() . ', ' .
 						$this->sqlBeginXBillNumber() . ', ' .
 						$this->sqlBeginYBillNumber() . ', ' .
 						$this->sqlBillNumberWidth() . ', ' .
 						$this->sqlBillNumberHeight() . ', ' .
 						$this->sqlBeginXMeterStartRead() . ', ' .
 						$this->sqlBeginYMeterStartRead() . ', ' .
 						$this->sqlMeterStartReadWidth() . ', ' .
 						$this->sqlMeterStartReadHeight() . ', ' .
 						$this->sqlBeginXMeterEndRead() . ', ' .
 						$this->sqlBeginYMeterEndRead() . ', ' .
 						$this->sqlMeterEndReadWidth() . ', ' .
 						$this->sqlMeterEndReadHeight() . ', ' .
 						$this->sqlBeginXBillAmount() . ', ' .
 						$this->sqlBeginYBillAmount() . ', ' .
 						$this->sqlBillAmountWidth() . ', ' .
 						$this->sqlBillAmountHeight() . ', ' .
 						$this->sqlBeginXRemittanceStub() . ', ' .
 						$this->sqlBeginYRemittanceStub() . ', ' .
 						$this->sqlRemittanceStubHeight() . ', ' .
 						$this->sqlRemittanceStubWidth() . ', ' .
 						$this->sqlServiceStartPageNumber() . ', ' .
 						$this->sqlServiceEndPageNumber() . ', ' .
 						$this->sqlBillDatePageNumber() . ', ' .
 						$this->sqlShowServiceEndDate() . ', ' .
 						$this->sqlShowServiceStartDate() . ', ' .
 						$this->sqlDueDatePageNumber() . ', ' .
 						$this->sqlAccountNumberPageNumber() . ', ' .
 						$this->sqlBillNumberPageNumber() . ', ' .
 						$this->sqlMeterStartReadPageNumber() . ', ' .
 						$this->sqlMeterEndReadPageNumber() . ', ' .
 						$this->sqlBillAmountPageNumber() . ', ' .
 						$this->sqlRemittanceStubPageNumber() . ', ' .
 						$this->sqlShowBillDate() . ', ' .
 						$this->sqlShowDueDate() . ', ' .
 						$this->sqlShowBillNumber() . ', ' .
 						$this->sqlShowMeterStartRead() . ', ' .
 						$this->sqlShowMeterEndRead() . ', ' .
 						$this->sqlShowBillAmount() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlAttemptOcr() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlRequireMultiplier() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId() . ','; } elseif( true == array_key_exists( 'UtilityProviderId', $this->getChangedColumns() ) ) { $strSql .= ' utility_provider_id = ' . $this->sqlUtilityProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sample_utility_document_id = ' . $this->sqlSampleUtilityDocumentId() . ','; } elseif( true == array_key_exists( 'SampleUtilityDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' sample_utility_document_id = ' . $this->sqlSampleUtilityDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_datetime = ' . $this->sqlTemplateDatetime() . ','; } elseif( true == array_key_exists( 'TemplateDatetime', $this->getChangedColumns() ) ) { $strSql .= ' template_datetime = ' . $this->sqlTemplateDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_x_service_start = ' . $this->sqlBeginXServiceStart() . ','; } elseif( true == array_key_exists( 'BeginXServiceStart', $this->getChangedColumns() ) ) { $strSql .= ' begin_x_service_start = ' . $this->sqlBeginXServiceStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_y_service_start = ' . $this->sqlBeginYServiceStart() . ','; } elseif( true == array_key_exists( 'BeginYServiceStart', $this->getChangedColumns() ) ) { $strSql .= ' begin_y_service_start = ' . $this->sqlBeginYServiceStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_start_width = ' . $this->sqlServiceStartWidth() . ','; } elseif( true == array_key_exists( 'ServiceStartWidth', $this->getChangedColumns() ) ) { $strSql .= ' service_start_width = ' . $this->sqlServiceStartWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_start_height = ' . $this->sqlServiceStartHeight() . ','; } elseif( true == array_key_exists( 'ServiceStartHeight', $this->getChangedColumns() ) ) { $strSql .= ' service_start_height = ' . $this->sqlServiceStartHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_x_service_end = ' . $this->sqlBeginXServiceEnd() . ','; } elseif( true == array_key_exists( 'BeginXServiceEnd', $this->getChangedColumns() ) ) { $strSql .= ' begin_x_service_end = ' . $this->sqlBeginXServiceEnd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_y_service_end = ' . $this->sqlBeginYServiceEnd() . ','; } elseif( true == array_key_exists( 'BeginYServiceEnd', $this->getChangedColumns() ) ) { $strSql .= ' begin_y_service_end = ' . $this->sqlBeginYServiceEnd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_end_width = ' . $this->sqlServiceEndWidth() . ','; } elseif( true == array_key_exists( 'ServiceEndWidth', $this->getChangedColumns() ) ) { $strSql .= ' service_end_width = ' . $this->sqlServiceEndWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_end_height = ' . $this->sqlServiceEndHeight() . ','; } elseif( true == array_key_exists( 'ServiceEndHeight', $this->getChangedColumns() ) ) { $strSql .= ' service_end_height = ' . $this->sqlServiceEndHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_x_bill_date = ' . $this->sqlBeginXBillDate() . ','; } elseif( true == array_key_exists( 'BeginXBillDate', $this->getChangedColumns() ) ) { $strSql .= ' begin_x_bill_date = ' . $this->sqlBeginXBillDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_y_bill_date = ' . $this->sqlBeginYBillDate() . ','; } elseif( true == array_key_exists( 'BeginYBillDate', $this->getChangedColumns() ) ) { $strSql .= ' begin_y_bill_date = ' . $this->sqlBeginYBillDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_date_width = ' . $this->sqlBillDateWidth() . ','; } elseif( true == array_key_exists( 'BillDateWidth', $this->getChangedColumns() ) ) { $strSql .= ' bill_date_width = ' . $this->sqlBillDateWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_date_height = ' . $this->sqlBillDateHeight() . ','; } elseif( true == array_key_exists( 'BillDateHeight', $this->getChangedColumns() ) ) { $strSql .= ' bill_date_height = ' . $this->sqlBillDateHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_x_due_date = ' . $this->sqlBeginXDueDate() . ','; } elseif( true == array_key_exists( 'BeginXDueDate', $this->getChangedColumns() ) ) { $strSql .= ' begin_x_due_date = ' . $this->sqlBeginXDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_y_due_date = ' . $this->sqlBeginYDueDate() . ','; } elseif( true == array_key_exists( 'BeginYDueDate', $this->getChangedColumns() ) ) { $strSql .= ' begin_y_due_date = ' . $this->sqlBeginYDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date_width = ' . $this->sqlDueDateWidth() . ','; } elseif( true == array_key_exists( 'DueDateWidth', $this->getChangedColumns() ) ) { $strSql .= ' due_date_width = ' . $this->sqlDueDateWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date_height = ' . $this->sqlDueDateHeight() . ','; } elseif( true == array_key_exists( 'DueDateHeight', $this->getChangedColumns() ) ) { $strSql .= ' due_date_height = ' . $this->sqlDueDateHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_x_account_number = ' . $this->sqlBeginXAccountNumber() . ','; } elseif( true == array_key_exists( 'BeginXAccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' begin_x_account_number = ' . $this->sqlBeginXAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_y_account_number = ' . $this->sqlBeginYAccountNumber() . ','; } elseif( true == array_key_exists( 'BeginYAccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' begin_y_account_number = ' . $this->sqlBeginYAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_width = ' . $this->sqlAccountNumberWidth() . ','; } elseif( true == array_key_exists( 'AccountNumberWidth', $this->getChangedColumns() ) ) { $strSql .= ' account_number_width = ' . $this->sqlAccountNumberWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_height = ' . $this->sqlAccountNumberHeight() . ','; } elseif( true == array_key_exists( 'AccountNumberHeight', $this->getChangedColumns() ) ) { $strSql .= ' account_number_height = ' . $this->sqlAccountNumberHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_x_bill_number = ' . $this->sqlBeginXBillNumber() . ','; } elseif( true == array_key_exists( 'BeginXBillNumber', $this->getChangedColumns() ) ) { $strSql .= ' begin_x_bill_number = ' . $this->sqlBeginXBillNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_y_bill_number = ' . $this->sqlBeginYBillNumber() . ','; } elseif( true == array_key_exists( 'BeginYBillNumber', $this->getChangedColumns() ) ) { $strSql .= ' begin_y_bill_number = ' . $this->sqlBeginYBillNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_number_width = ' . $this->sqlBillNumberWidth() . ','; } elseif( true == array_key_exists( 'BillNumberWidth', $this->getChangedColumns() ) ) { $strSql .= ' bill_number_width = ' . $this->sqlBillNumberWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_number_height = ' . $this->sqlBillNumberHeight() . ','; } elseif( true == array_key_exists( 'BillNumberHeight', $this->getChangedColumns() ) ) { $strSql .= ' bill_number_height = ' . $this->sqlBillNumberHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_x_meter_start_read = ' . $this->sqlBeginXMeterStartRead() . ','; } elseif( true == array_key_exists( 'BeginXMeterStartRead', $this->getChangedColumns() ) ) { $strSql .= ' begin_x_meter_start_read = ' . $this->sqlBeginXMeterStartRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_y_meter_start_read = ' . $this->sqlBeginYMeterStartRead() . ','; } elseif( true == array_key_exists( 'BeginYMeterStartRead', $this->getChangedColumns() ) ) { $strSql .= ' begin_y_meter_start_read = ' . $this->sqlBeginYMeterStartRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_start_read_width = ' . $this->sqlMeterStartReadWidth() . ','; } elseif( true == array_key_exists( 'MeterStartReadWidth', $this->getChangedColumns() ) ) { $strSql .= ' meter_start_read_width = ' . $this->sqlMeterStartReadWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_start_read_height = ' . $this->sqlMeterStartReadHeight() . ','; } elseif( true == array_key_exists( 'MeterStartReadHeight', $this->getChangedColumns() ) ) { $strSql .= ' meter_start_read_height = ' . $this->sqlMeterStartReadHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_x_meter_end_read = ' . $this->sqlBeginXMeterEndRead() . ','; } elseif( true == array_key_exists( 'BeginXMeterEndRead', $this->getChangedColumns() ) ) { $strSql .= ' begin_x_meter_end_read = ' . $this->sqlBeginXMeterEndRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_y_meter_end_read = ' . $this->sqlBeginYMeterEndRead() . ','; } elseif( true == array_key_exists( 'BeginYMeterEndRead', $this->getChangedColumns() ) ) { $strSql .= ' begin_y_meter_end_read = ' . $this->sqlBeginYMeterEndRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_end_read_width = ' . $this->sqlMeterEndReadWidth() . ','; } elseif( true == array_key_exists( 'MeterEndReadWidth', $this->getChangedColumns() ) ) { $strSql .= ' meter_end_read_width = ' . $this->sqlMeterEndReadWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_end_read_height = ' . $this->sqlMeterEndReadHeight() . ','; } elseif( true == array_key_exists( 'MeterEndReadHeight', $this->getChangedColumns() ) ) { $strSql .= ' meter_end_read_height = ' . $this->sqlMeterEndReadHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_x_bill_amount = ' . $this->sqlBeginXBillAmount() . ','; } elseif( true == array_key_exists( 'BeginXBillAmount', $this->getChangedColumns() ) ) { $strSql .= ' begin_x_bill_amount = ' . $this->sqlBeginXBillAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_y_bill_amount = ' . $this->sqlBeginYBillAmount() . ','; } elseif( true == array_key_exists( 'BeginYBillAmount', $this->getChangedColumns() ) ) { $strSql .= ' begin_y_bill_amount = ' . $this->sqlBeginYBillAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_amount_width = ' . $this->sqlBillAmountWidth() . ','; } elseif( true == array_key_exists( 'BillAmountWidth', $this->getChangedColumns() ) ) { $strSql .= ' bill_amount_width = ' . $this->sqlBillAmountWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_amount_height = ' . $this->sqlBillAmountHeight() . ','; } elseif( true == array_key_exists( 'BillAmountHeight', $this->getChangedColumns() ) ) { $strSql .= ' bill_amount_height = ' . $this->sqlBillAmountHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_x_remittance_stub = ' . $this->sqlBeginXRemittanceStub() . ','; } elseif( true == array_key_exists( 'BeginXRemittanceStub', $this->getChangedColumns() ) ) { $strSql .= ' begin_x_remittance_stub = ' . $this->sqlBeginXRemittanceStub() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_y_remittance_stub = ' . $this->sqlBeginYRemittanceStub() . ','; } elseif( true == array_key_exists( 'BeginYRemittanceStub', $this->getChangedColumns() ) ) { $strSql .= ' begin_y_remittance_stub = ' . $this->sqlBeginYRemittanceStub() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remittance_stub_height = ' . $this->sqlRemittanceStubHeight() . ','; } elseif( true == array_key_exists( 'RemittanceStubHeight', $this->getChangedColumns() ) ) { $strSql .= ' remittance_stub_height = ' . $this->sqlRemittanceStubHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remittance_stub_width = ' . $this->sqlRemittanceStubWidth() . ','; } elseif( true == array_key_exists( 'RemittanceStubWidth', $this->getChangedColumns() ) ) { $strSql .= ' remittance_stub_width = ' . $this->sqlRemittanceStubWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_start_page_number = ' . $this->sqlServiceStartPageNumber() . ','; } elseif( true == array_key_exists( 'ServiceStartPageNumber', $this->getChangedColumns() ) ) { $strSql .= ' service_start_page_number = ' . $this->sqlServiceStartPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_end_page_number = ' . $this->sqlServiceEndPageNumber() . ','; } elseif( true == array_key_exists( 'ServiceEndPageNumber', $this->getChangedColumns() ) ) { $strSql .= ' service_end_page_number = ' . $this->sqlServiceEndPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_date_page_number = ' . $this->sqlBillDatePageNumber() . ','; } elseif( true == array_key_exists( 'BillDatePageNumber', $this->getChangedColumns() ) ) { $strSql .= ' bill_date_page_number = ' . $this->sqlBillDatePageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_service_end_date = ' . $this->sqlShowServiceEndDate() . ','; } elseif( true == array_key_exists( 'ShowServiceEndDate', $this->getChangedColumns() ) ) { $strSql .= ' show_service_end_date = ' . $this->sqlShowServiceEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_service_start_date = ' . $this->sqlShowServiceStartDate() . ','; } elseif( true == array_key_exists( 'ShowServiceStartDate', $this->getChangedColumns() ) ) { $strSql .= ' show_service_start_date = ' . $this->sqlShowServiceStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date_page_number = ' . $this->sqlDueDatePageNumber() . ','; } elseif( true == array_key_exists( 'DueDatePageNumber', $this->getChangedColumns() ) ) { $strSql .= ' due_date_page_number = ' . $this->sqlDueDatePageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number_page_number = ' . $this->sqlAccountNumberPageNumber() . ','; } elseif( true == array_key_exists( 'AccountNumberPageNumber', $this->getChangedColumns() ) ) { $strSql .= ' account_number_page_number = ' . $this->sqlAccountNumberPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_number_page_number = ' . $this->sqlBillNumberPageNumber() . ','; } elseif( true == array_key_exists( 'BillNumberPageNumber', $this->getChangedColumns() ) ) { $strSql .= ' bill_number_page_number = ' . $this->sqlBillNumberPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_start_read_page_number = ' . $this->sqlMeterStartReadPageNumber() . ','; } elseif( true == array_key_exists( 'MeterStartReadPageNumber', $this->getChangedColumns() ) ) { $strSql .= ' meter_start_read_page_number = ' . $this->sqlMeterStartReadPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_end_read_page_number = ' . $this->sqlMeterEndReadPageNumber() . ','; } elseif( true == array_key_exists( 'MeterEndReadPageNumber', $this->getChangedColumns() ) ) { $strSql .= ' meter_end_read_page_number = ' . $this->sqlMeterEndReadPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_amount_page_number = ' . $this->sqlBillAmountPageNumber() . ','; } elseif( true == array_key_exists( 'BillAmountPageNumber', $this->getChangedColumns() ) ) { $strSql .= ' bill_amount_page_number = ' . $this->sqlBillAmountPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remittance_stub_page_number = ' . $this->sqlRemittanceStubPageNumber() . ','; } elseif( true == array_key_exists( 'RemittanceStubPageNumber', $this->getChangedColumns() ) ) { $strSql .= ' remittance_stub_page_number = ' . $this->sqlRemittanceStubPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_bill_date = ' . $this->sqlShowBillDate() . ','; } elseif( true == array_key_exists( 'ShowBillDate', $this->getChangedColumns() ) ) { $strSql .= ' show_bill_date = ' . $this->sqlShowBillDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_due_date = ' . $this->sqlShowDueDate() . ','; } elseif( true == array_key_exists( 'ShowDueDate', $this->getChangedColumns() ) ) { $strSql .= ' show_due_date = ' . $this->sqlShowDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_bill_number = ' . $this->sqlShowBillNumber() . ','; } elseif( true == array_key_exists( 'ShowBillNumber', $this->getChangedColumns() ) ) { $strSql .= ' show_bill_number = ' . $this->sqlShowBillNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_meter_start_read = ' . $this->sqlShowMeterStartRead() . ','; } elseif( true == array_key_exists( 'ShowMeterStartRead', $this->getChangedColumns() ) ) { $strSql .= ' show_meter_start_read = ' . $this->sqlShowMeterStartRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_meter_end_read = ' . $this->sqlShowMeterEndRead() . ','; } elseif( true == array_key_exists( 'ShowMeterEndRead', $this->getChangedColumns() ) ) { $strSql .= ' show_meter_end_read = ' . $this->sqlShowMeterEndRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_bill_amount = ' . $this->sqlShowBillAmount() . ','; } elseif( true == array_key_exists( 'ShowBillAmount', $this->getChangedColumns() ) ) { $strSql .= ' show_bill_amount = ' . $this->sqlShowBillAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attempt_ocr = ' . $this->sqlAttemptOcr() . ','; } elseif( true == array_key_exists( 'AttemptOcr', $this->getChangedColumns() ) ) { $strSql .= ' attempt_ocr = ' . $this->sqlAttemptOcr() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_multiplier = ' . $this->sqlRequireMultiplier() . ','; } elseif( true == array_key_exists( 'RequireMultiplier', $this->getChangedColumns() ) ) { $strSql .= ' require_multiplier = ' . $this->sqlRequireMultiplier() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_provider_id' => $this->getUtilityProviderId(),
			'sample_utility_document_id' => $this->getSampleUtilityDocumentId(),
			'name' => $this->getName(),
			'template_datetime' => $this->getTemplateDatetime(),
			'description' => $this->getDescription(),
			'begin_x_service_start' => $this->getBeginXServiceStart(),
			'begin_y_service_start' => $this->getBeginYServiceStart(),
			'service_start_width' => $this->getServiceStartWidth(),
			'service_start_height' => $this->getServiceStartHeight(),
			'begin_x_service_end' => $this->getBeginXServiceEnd(),
			'begin_y_service_end' => $this->getBeginYServiceEnd(),
			'service_end_width' => $this->getServiceEndWidth(),
			'service_end_height' => $this->getServiceEndHeight(),
			'begin_x_bill_date' => $this->getBeginXBillDate(),
			'begin_y_bill_date' => $this->getBeginYBillDate(),
			'bill_date_width' => $this->getBillDateWidth(),
			'bill_date_height' => $this->getBillDateHeight(),
			'begin_x_due_date' => $this->getBeginXDueDate(),
			'begin_y_due_date' => $this->getBeginYDueDate(),
			'due_date_width' => $this->getDueDateWidth(),
			'due_date_height' => $this->getDueDateHeight(),
			'begin_x_account_number' => $this->getBeginXAccountNumber(),
			'begin_y_account_number' => $this->getBeginYAccountNumber(),
			'account_number_width' => $this->getAccountNumberWidth(),
			'account_number_height' => $this->getAccountNumberHeight(),
			'begin_x_bill_number' => $this->getBeginXBillNumber(),
			'begin_y_bill_number' => $this->getBeginYBillNumber(),
			'bill_number_width' => $this->getBillNumberWidth(),
			'bill_number_height' => $this->getBillNumberHeight(),
			'begin_x_meter_start_read' => $this->getBeginXMeterStartRead(),
			'begin_y_meter_start_read' => $this->getBeginYMeterStartRead(),
			'meter_start_read_width' => $this->getMeterStartReadWidth(),
			'meter_start_read_height' => $this->getMeterStartReadHeight(),
			'begin_x_meter_end_read' => $this->getBeginXMeterEndRead(),
			'begin_y_meter_end_read' => $this->getBeginYMeterEndRead(),
			'meter_end_read_width' => $this->getMeterEndReadWidth(),
			'meter_end_read_height' => $this->getMeterEndReadHeight(),
			'begin_x_bill_amount' => $this->getBeginXBillAmount(),
			'begin_y_bill_amount' => $this->getBeginYBillAmount(),
			'bill_amount_width' => $this->getBillAmountWidth(),
			'bill_amount_height' => $this->getBillAmountHeight(),
			'begin_x_remittance_stub' => $this->getBeginXRemittanceStub(),
			'begin_y_remittance_stub' => $this->getBeginYRemittanceStub(),
			'remittance_stub_height' => $this->getRemittanceStubHeight(),
			'remittance_stub_width' => $this->getRemittanceStubWidth(),
			'service_start_page_number' => $this->getServiceStartPageNumber(),
			'service_end_page_number' => $this->getServiceEndPageNumber(),
			'bill_date_page_number' => $this->getBillDatePageNumber(),
			'show_service_end_date' => $this->getShowServiceEndDate(),
			'show_service_start_date' => $this->getShowServiceStartDate(),
			'due_date_page_number' => $this->getDueDatePageNumber(),
			'account_number_page_number' => $this->getAccountNumberPageNumber(),
			'bill_number_page_number' => $this->getBillNumberPageNumber(),
			'meter_start_read_page_number' => $this->getMeterStartReadPageNumber(),
			'meter_end_read_page_number' => $this->getMeterEndReadPageNumber(),
			'bill_amount_page_number' => $this->getBillAmountPageNumber(),
			'remittance_stub_page_number' => $this->getRemittanceStubPageNumber(),
			'show_bill_date' => $this->getShowBillDate(),
			'show_due_date' => $this->getShowDueDate(),
			'show_bill_number' => $this->getShowBillNumber(),
			'show_meter_start_read' => $this->getShowMeterStartRead(),
			'show_meter_end_read' => $this->getShowMeterEndRead(),
			'show_bill_amount' => $this->getShowBillAmount(),
			'is_published' => $this->getIsPublished(),
			'attempt_ocr' => $this->getAttemptOcr(),
			'order_num' => $this->getOrderNum(),
			'require_multiplier' => $this->getRequireMultiplier(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>