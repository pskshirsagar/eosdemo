<?php

class CBaseVacantBillAlert extends CEosSingularBase {

	const TABLE_NAME = 'public.vacant_bill_alerts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intUtilityBillAccountTemplateId;
	protected $m_strAlertDatetime;
	protected $m_strReminderDatetime;
	protected $m_strMoveOutDate;
	protected $m_intMoveOutUtilityAccountId;
	protected $m_strMoveInDate;
	protected $m_intMoveInUtilityAccountId;
	protected $m_intResolvedUtilityBillAccountId;
	protected $m_intResolvedUtilityBillId;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['utility_bill_account_template_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountTemplateId', trim( $arrValues['utility_bill_account_template_id'] ) ); elseif( isset( $arrValues['utility_bill_account_template_id'] ) ) $this->setUtilityBillAccountTemplateId( $arrValues['utility_bill_account_template_id'] );
		if( isset( $arrValues['alert_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAlertDatetime', trim( $arrValues['alert_datetime'] ) ); elseif( isset( $arrValues['alert_datetime'] ) ) $this->setAlertDatetime( $arrValues['alert_datetime'] );
		if( isset( $arrValues['reminder_datetime'] ) && $boolDirectSet ) $this->set( 'm_strReminderDatetime', trim( $arrValues['reminder_datetime'] ) ); elseif( isset( $arrValues['reminder_datetime'] ) ) $this->setReminderDatetime( $arrValues['reminder_datetime'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['move_out_utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveOutUtilityAccountId', trim( $arrValues['move_out_utility_account_id'] ) ); elseif( isset( $arrValues['move_out_utility_account_id'] ) ) $this->setMoveOutUtilityAccountId( $arrValues['move_out_utility_account_id'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['move_in_utility_account_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveInUtilityAccountId', trim( $arrValues['move_in_utility_account_id'] ) ); elseif( isset( $arrValues['move_in_utility_account_id'] ) ) $this->setMoveInUtilityAccountId( $arrValues['move_in_utility_account_id'] );
		if( isset( $arrValues['resolved_utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intResolvedUtilityBillAccountId', trim( $arrValues['resolved_utility_bill_account_id'] ) ); elseif( isset( $arrValues['resolved_utility_bill_account_id'] ) ) $this->setResolvedUtilityBillAccountId( $arrValues['resolved_utility_bill_account_id'] );
		if( isset( $arrValues['resolved_utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intResolvedUtilityBillId', trim( $arrValues['resolved_utility_bill_id'] ) ); elseif( isset( $arrValues['resolved_utility_bill_id'] ) ) $this->setResolvedUtilityBillId( $arrValues['resolved_utility_bill_id'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUtilityBillAccountTemplateId( $intUtilityBillAccountTemplateId ) {
		$this->set( 'm_intUtilityBillAccountTemplateId', CStrings::strToIntDef( $intUtilityBillAccountTemplateId, NULL, false ) );
	}

	public function getUtilityBillAccountTemplateId() {
		return $this->m_intUtilityBillAccountTemplateId;
	}

	public function sqlUtilityBillAccountTemplateId() {
		return ( true == isset( $this->m_intUtilityBillAccountTemplateId ) ) ? ( string ) $this->m_intUtilityBillAccountTemplateId : 'NULL';
	}

	public function setAlertDatetime( $strAlertDatetime ) {
		$this->set( 'm_strAlertDatetime', CStrings::strTrimDef( $strAlertDatetime, -1, NULL, true ) );
	}

	public function getAlertDatetime() {
		return $this->m_strAlertDatetime;
	}

	public function sqlAlertDatetime() {
		return ( true == isset( $this->m_strAlertDatetime ) ) ? '\'' . $this->m_strAlertDatetime . '\'' : 'NULL';
	}

	public function setReminderDatetime( $strReminderDatetime ) {
		$this->set( 'm_strReminderDatetime', CStrings::strTrimDef( $strReminderDatetime, -1, NULL, true ) );
	}

	public function getReminderDatetime() {
		return $this->m_strReminderDatetime;
	}

	public function sqlReminderDatetime() {
		return ( true == isset( $this->m_strReminderDatetime ) ) ? '\'' . $this->m_strReminderDatetime . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setMoveOutUtilityAccountId( $intMoveOutUtilityAccountId ) {
		$this->set( 'm_intMoveOutUtilityAccountId', CStrings::strToIntDef( $intMoveOutUtilityAccountId, NULL, false ) );
	}

	public function getMoveOutUtilityAccountId() {
		return $this->m_intMoveOutUtilityAccountId;
	}

	public function sqlMoveOutUtilityAccountId() {
		return ( true == isset( $this->m_intMoveOutUtilityAccountId ) ) ? ( string ) $this->m_intMoveOutUtilityAccountId : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setMoveInUtilityAccountId( $intMoveInUtilityAccountId ) {
		$this->set( 'm_intMoveInUtilityAccountId', CStrings::strToIntDef( $intMoveInUtilityAccountId, NULL, false ) );
	}

	public function getMoveInUtilityAccountId() {
		return $this->m_intMoveInUtilityAccountId;
	}

	public function sqlMoveInUtilityAccountId() {
		return ( true == isset( $this->m_intMoveInUtilityAccountId ) ) ? ( string ) $this->m_intMoveInUtilityAccountId : 'NULL';
	}

	public function setResolvedUtilityBillAccountId( $intResolvedUtilityBillAccountId ) {
		$this->set( 'm_intResolvedUtilityBillAccountId', CStrings::strToIntDef( $intResolvedUtilityBillAccountId, NULL, false ) );
	}

	public function getResolvedUtilityBillAccountId() {
		return $this->m_intResolvedUtilityBillAccountId;
	}

	public function sqlResolvedUtilityBillAccountId() {
		return ( true == isset( $this->m_intResolvedUtilityBillAccountId ) ) ? ( string ) $this->m_intResolvedUtilityBillAccountId : 'NULL';
	}

	public function setResolvedUtilityBillId( $intResolvedUtilityBillId ) {
		$this->set( 'm_intResolvedUtilityBillId', CStrings::strToIntDef( $intResolvedUtilityBillId, NULL, false ) );
	}

	public function getResolvedUtilityBillId() {
		return $this->m_intResolvedUtilityBillId;
	}

	public function sqlResolvedUtilityBillId() {
		return ( true == isset( $this->m_intResolvedUtilityBillId ) ) ? ( string ) $this->m_intResolvedUtilityBillId : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, utility_bill_account_template_id, alert_datetime, reminder_datetime, move_out_date, move_out_utility_account_id, move_in_date, move_in_utility_account_id, resolved_utility_bill_account_id, resolved_utility_bill_id, updated_on, updated_by, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUtilityBillAccountTemplateId() . ', ' .
						$this->sqlAlertDatetime() . ', ' .
						$this->sqlReminderDatetime() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlMoveOutUtilityAccountId() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlMoveInUtilityAccountId() . ', ' .
						$this->sqlResolvedUtilityBillAccountId() . ', ' .
						$this->sqlResolvedUtilityBillId() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_template_id = ' . $this->sqlUtilityBillAccountTemplateId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_template_id = ' . $this->sqlUtilityBillAccountTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alert_datetime = ' . $this->sqlAlertDatetime(). ',' ; } elseif( true == array_key_exists( 'AlertDatetime', $this->getChangedColumns() ) ) { $strSql .= ' alert_datetime = ' . $this->sqlAlertDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reminder_datetime = ' . $this->sqlReminderDatetime(). ',' ; } elseif( true == array_key_exists( 'ReminderDatetime', $this->getChangedColumns() ) ) { $strSql .= ' reminder_datetime = ' . $this->sqlReminderDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_utility_account_id = ' . $this->sqlMoveOutUtilityAccountId(). ',' ; } elseif( true == array_key_exists( 'MoveOutUtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' move_out_utility_account_id = ' . $this->sqlMoveOutUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate(). ',' ; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_utility_account_id = ' . $this->sqlMoveInUtilityAccountId(). ',' ; } elseif( true == array_key_exists( 'MoveInUtilityAccountId', $this->getChangedColumns() ) ) { $strSql .= ' move_in_utility_account_id = ' . $this->sqlMoveInUtilityAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_utility_bill_account_id = ' . $this->sqlResolvedUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'ResolvedUtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' resolved_utility_bill_account_id = ' . $this->sqlResolvedUtilityBillAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_utility_bill_id = ' . $this->sqlResolvedUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'ResolvedUtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' resolved_utility_bill_id = ' . $this->sqlResolvedUtilityBillId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'utility_bill_account_template_id' => $this->getUtilityBillAccountTemplateId(),
			'alert_datetime' => $this->getAlertDatetime(),
			'reminder_datetime' => $this->getReminderDatetime(),
			'move_out_date' => $this->getMoveOutDate(),
			'move_out_utility_account_id' => $this->getMoveOutUtilityAccountId(),
			'move_in_date' => $this->getMoveInDate(),
			'move_in_utility_account_id' => $this->getMoveInUtilityAccountId(),
			'resolved_utility_bill_account_id' => $this->getResolvedUtilityBillAccountId(),
			'resolved_utility_bill_id' => $this->getResolvedUtilityBillId(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>