<?php

class CBasePropertyUtilityTypeRateMethod extends CEosSingularBase {

	const TABLE_NAME = 'public.property_utility_type_rate_methods';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUtilityTypeRateId;
	protected $m_intNewPropertyUtilityTypeRateMethodId;
	protected $m_intUtilityRubsFormulaId;
	protected $m_intMeterRateTypeId;
	protected $m_strPercentage;
	protected $m_intChargeCodeId;
	protected $m_strDescription;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_utility_type_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUtilityTypeRateId', trim( $arrValues['property_utility_type_rate_id'] ) ); elseif( isset( $arrValues['property_utility_type_rate_id'] ) ) $this->setPropertyUtilityTypeRateId( $arrValues['property_utility_type_rate_id'] );
		if( isset( $arrValues['new_property_utility_type_rate_method_id'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertyUtilityTypeRateMethodId', trim( $arrValues['new_property_utility_type_rate_method_id'] ) ); elseif( isset( $arrValues['new_property_utility_type_rate_method_id'] ) ) $this->setNewPropertyUtilityTypeRateMethodId( $arrValues['new_property_utility_type_rate_method_id'] );
		if( isset( $arrValues['utility_rubs_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityRubsFormulaId', trim( $arrValues['utility_rubs_formula_id'] ) ); elseif( isset( $arrValues['utility_rubs_formula_id'] ) ) $this->setUtilityRubsFormulaId( $arrValues['utility_rubs_formula_id'] );
		if( isset( $arrValues['meter_rate_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMeterRateTypeId', trim( $arrValues['meter_rate_type_id'] ) ); elseif( isset( $arrValues['meter_rate_type_id'] ) ) $this->setMeterRateTypeId( $arrValues['meter_rate_type_id'] );
		if( isset( $arrValues['percentage'] ) && $boolDirectSet ) $this->set( 'm_strPercentage', trim( $arrValues['percentage'] ) ); elseif( isset( $arrValues['percentage'] ) ) $this->setPercentage( $arrValues['percentage'] );
		if( isset( $arrValues['charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeCodeId', trim( $arrValues['charge_code_id'] ) ); elseif( isset( $arrValues['charge_code_id'] ) ) $this->setChargeCodeId( $arrValues['charge_code_id'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUtilityTypeRateId( $intPropertyUtilityTypeRateId ) {
		$this->set( 'm_intPropertyUtilityTypeRateId', CStrings::strToIntDef( $intPropertyUtilityTypeRateId, NULL, false ) );
	}

	public function getPropertyUtilityTypeRateId() {
		return $this->m_intPropertyUtilityTypeRateId;
	}

	public function sqlPropertyUtilityTypeRateId() {
		return ( true == isset( $this->m_intPropertyUtilityTypeRateId ) ) ? ( string ) $this->m_intPropertyUtilityTypeRateId : 'NULL';
	}

	public function setNewPropertyUtilityTypeRateMethodId( $intNewPropertyUtilityTypeRateMethodId ) {
		$this->set( 'm_intNewPropertyUtilityTypeRateMethodId', CStrings::strToIntDef( $intNewPropertyUtilityTypeRateMethodId, NULL, false ) );
	}

	public function getNewPropertyUtilityTypeRateMethodId() {
		return $this->m_intNewPropertyUtilityTypeRateMethodId;
	}

	public function sqlNewPropertyUtilityTypeRateMethodId() {
		return ( true == isset( $this->m_intNewPropertyUtilityTypeRateMethodId ) ) ? ( string ) $this->m_intNewPropertyUtilityTypeRateMethodId : 'NULL';
	}

	public function setUtilityRubsFormulaId( $intUtilityRubsFormulaId ) {
		$this->set( 'm_intUtilityRubsFormulaId', CStrings::strToIntDef( $intUtilityRubsFormulaId, NULL, false ) );
	}

	public function getUtilityRubsFormulaId() {
		return $this->m_intUtilityRubsFormulaId;
	}

	public function sqlUtilityRubsFormulaId() {
		return ( true == isset( $this->m_intUtilityRubsFormulaId ) ) ? ( string ) $this->m_intUtilityRubsFormulaId : 'NULL';
	}

	public function setMeterRateTypeId( $intMeterRateTypeId ) {
		$this->set( 'm_intMeterRateTypeId', CStrings::strToIntDef( $intMeterRateTypeId, NULL, false ) );
	}

	public function getMeterRateTypeId() {
		return $this->m_intMeterRateTypeId;
	}

	public function sqlMeterRateTypeId() {
		return ( true == isset( $this->m_intMeterRateTypeId ) ) ? ( string ) $this->m_intMeterRateTypeId : 'NULL';
	}

	public function setPercentage( $strPercentage ) {
		$this->set( 'm_strPercentage', CStrings::strTrimDef( $strPercentage, NULL, NULL, true ) );
	}

	public function getPercentage() {
		return $this->m_strPercentage;
	}

	public function sqlPercentage() {
		return ( true == isset( $this->m_strPercentage ) ) ? '\'' . addslashes( $this->m_strPercentage ) . '\'' : 'NULL';
	}

	public function setChargeCodeId( $intChargeCodeId ) {
		$this->set( 'm_intChargeCodeId', CStrings::strToIntDef( $intChargeCodeId, NULL, false ) );
	}

	public function getChargeCodeId() {
		return $this->m_intChargeCodeId;
	}

	public function sqlChargeCodeId() {
		return ( true == isset( $this->m_intChargeCodeId ) ) ? ( string ) $this->m_intChargeCodeId : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 250, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_utility_type_rate_id, new_property_utility_type_rate_method_id, utility_rubs_formula_id, meter_rate_type_id, percentage, charge_code_id, description, updated_on, updated_by, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyUtilityTypeRateId() . ', ' .
 						$this->sqlNewPropertyUtilityTypeRateMethodId() . ', ' .
 						$this->sqlUtilityRubsFormulaId() . ', ' .
 						$this->sqlMeterRateTypeId() . ', ' .
 						$this->sqlPercentage() . ', ' .
 						$this->sqlChargeCodeId() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_utility_type_rate_id = ' . $this->sqlPropertyUtilityTypeRateId() . ','; } elseif( true == array_key_exists( 'PropertyUtilityTypeRateId', $this->getChangedColumns() ) ) { $strSql .= ' property_utility_type_rate_id = ' . $this->sqlPropertyUtilityTypeRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_utility_type_rate_method_id = ' . $this->sqlNewPropertyUtilityTypeRateMethodId() . ','; } elseif( true == array_key_exists( 'NewPropertyUtilityTypeRateMethodId', $this->getChangedColumns() ) ) { $strSql .= ' new_property_utility_type_rate_method_id = ' . $this->sqlNewPropertyUtilityTypeRateMethodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_rubs_formula_id = ' . $this->sqlUtilityRubsFormulaId() . ','; } elseif( true == array_key_exists( 'UtilityRubsFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' utility_rubs_formula_id = ' . $this->sqlUtilityRubsFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meter_rate_type_id = ' . $this->sqlMeterRateTypeId() . ','; } elseif( true == array_key_exists( 'MeterRateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' meter_rate_type_id = ' . $this->sqlMeterRateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percentage = ' . $this->sqlPercentage() . ','; } elseif( true == array_key_exists( 'Percentage', $this->getChangedColumns() ) ) { $strSql .= ' percentage = ' . $this->sqlPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; } elseif( true == array_key_exists( 'ChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_utility_type_rate_id' => $this->getPropertyUtilityTypeRateId(),
			'new_property_utility_type_rate_method_id' => $this->getNewPropertyUtilityTypeRateMethodId(),
			'utility_rubs_formula_id' => $this->getUtilityRubsFormulaId(),
			'meter_rate_type_id' => $this->getMeterRateTypeId(),
			'percentage' => $this->getPercentage(),
			'charge_code_id' => $this->getChargeCodeId(),
			'description' => $this->getDescription(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>