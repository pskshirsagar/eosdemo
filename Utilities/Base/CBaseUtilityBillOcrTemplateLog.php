<?php

class CBaseUtilityBillOcrTemplateLog extends CEosSingularBase {

	const TABLE_NAME = 'public.utility_bill_ocr_template_logs';

	protected $m_intId;
	protected $m_intUtilityBillId;
	protected $m_intOcrTemplateId;
	protected $m_intOcrTemplateLabelId;
	protected $m_strSystemLabel;
	protected $m_strParsedLabel;
	protected $m_strParsedValue;
	protected $m_fltLabelScore;
	protected $m_fltValueScore;
	protected $m_strNote;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['utility_bill_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillId', trim( $arrValues['utility_bill_id'] ) ); elseif( isset( $arrValues['utility_bill_id'] ) ) $this->setUtilityBillId( $arrValues['utility_bill_id'] );
		if( isset( $arrValues['ocr_template_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrTemplateId', trim( $arrValues['ocr_template_id'] ) ); elseif( isset( $arrValues['ocr_template_id'] ) ) $this->setOcrTemplateId( $arrValues['ocr_template_id'] );
		if( isset( $arrValues['ocr_template_label_id'] ) && $boolDirectSet ) $this->set( 'm_intOcrTemplateLabelId', trim( $arrValues['ocr_template_label_id'] ) ); elseif( isset( $arrValues['ocr_template_label_id'] ) ) $this->setOcrTemplateLabelId( $arrValues['ocr_template_label_id'] );
		if( isset( $arrValues['system_label'] ) && $boolDirectSet ) $this->set( 'm_strSystemLabel', trim( stripcslashes( $arrValues['system_label'] ) ) ); elseif( isset( $arrValues['system_label'] ) ) $this->setSystemLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_label'] ) : $arrValues['system_label'] );
		if( isset( $arrValues['parsed_label'] ) && $boolDirectSet ) $this->set( 'm_strParsedLabel', trim( stripcslashes( $arrValues['parsed_label'] ) ) ); elseif( isset( $arrValues['parsed_label'] ) ) $this->setParsedLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['parsed_label'] ) : $arrValues['parsed_label'] );
		if( isset( $arrValues['parsed_value'] ) && $boolDirectSet ) $this->set( 'm_strParsedValue', trim( stripcslashes( $arrValues['parsed_value'] ) ) ); elseif( isset( $arrValues['parsed_value'] ) ) $this->setParsedValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['parsed_value'] ) : $arrValues['parsed_value'] );
		if( isset( $arrValues['label_score'] ) && $boolDirectSet ) $this->set( 'm_fltLabelScore', trim( $arrValues['label_score'] ) ); elseif( isset( $arrValues['label_score'] ) ) $this->setLabelScore( $arrValues['label_score'] );
		if( isset( $arrValues['value_score'] ) && $boolDirectSet ) $this->set( 'm_fltValueScore', trim( $arrValues['value_score'] ) ); elseif( isset( $arrValues['value_score'] ) ) $this->setValueScore( $arrValues['value_score'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->set( 'm_intUtilityBillId', CStrings::strToIntDef( $intUtilityBillId, NULL, false ) );
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function sqlUtilityBillId() {
		return ( true == isset( $this->m_intUtilityBillId ) ) ? ( string ) $this->m_intUtilityBillId : 'NULL';
	}

	public function setOcrTemplateId( $intOcrTemplateId ) {
		$this->set( 'm_intOcrTemplateId', CStrings::strToIntDef( $intOcrTemplateId, NULL, false ) );
	}

	public function getOcrTemplateId() {
		return $this->m_intOcrTemplateId;
	}

	public function sqlOcrTemplateId() {
		return ( true == isset( $this->m_intOcrTemplateId ) ) ? ( string ) $this->m_intOcrTemplateId : 'NULL';
	}

	public function setOcrTemplateLabelId( $intOcrTemplateLabelId ) {
		$this->set( 'm_intOcrTemplateLabelId', CStrings::strToIntDef( $intOcrTemplateLabelId, NULL, false ) );
	}

	public function getOcrTemplateLabelId() {
		return $this->m_intOcrTemplateLabelId;
	}

	public function sqlOcrTemplateLabelId() {
		return ( true == isset( $this->m_intOcrTemplateLabelId ) ) ? ( string ) $this->m_intOcrTemplateLabelId : 'NULL';
	}

	public function setSystemLabel( $strSystemLabel ) {
		$this->set( 'm_strSystemLabel', CStrings::strTrimDef( $strSystemLabel, 200, NULL, true ) );
	}

	public function getSystemLabel() {
		return $this->m_strSystemLabel;
	}

	public function sqlSystemLabel() {
		return ( true == isset( $this->m_strSystemLabel ) ) ? '\'' . addslashes( $this->m_strSystemLabel ) . '\'' : 'NULL';
	}

	public function setParsedLabel( $strParsedLabel ) {
		$this->set( 'm_strParsedLabel', CStrings::strTrimDef( $strParsedLabel, 200, NULL, true ) );
	}

	public function getParsedLabel() {
		return $this->m_strParsedLabel;
	}

	public function sqlParsedLabel() {
		return ( true == isset( $this->m_strParsedLabel ) ) ? '\'' . addslashes( $this->m_strParsedLabel ) . '\'' : 'NULL';
	}

	public function setParsedValue( $strParsedValue ) {
		$this->set( 'm_strParsedValue', CStrings::strTrimDef( $strParsedValue, 200, NULL, true ) );
	}

	public function getParsedValue() {
		return $this->m_strParsedValue;
	}

	public function sqlParsedValue() {
		return ( true == isset( $this->m_strParsedValue ) ) ? '\'' . addslashes( $this->m_strParsedValue ) . '\'' : 'NULL';
	}

	public function setLabelScore( $fltLabelScore ) {
		$this->set( 'm_fltLabelScore', CStrings::strToFloatDef( $fltLabelScore, NULL, false, 2 ) );
	}

	public function getLabelScore() {
		return $this->m_fltLabelScore;
	}

	public function sqlLabelScore() {
		return ( true == isset( $this->m_fltLabelScore ) ) ? ( string ) $this->m_fltLabelScore : 'NULL';
	}

	public function setValueScore( $fltValueScore ) {
		$this->set( 'm_fltValueScore', CStrings::strToFloatDef( $fltValueScore, NULL, false, 2 ) );
	}

	public function getValueScore() {
		return $this->m_fltValueScore;
	}

	public function sqlValueScore() {
		return ( true == isset( $this->m_fltValueScore ) ) ? ( string ) $this->m_fltValueScore : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, utility_bill_id, ocr_template_id, ocr_template_label_id, system_label, parsed_label, parsed_value, label_score, value_score, note, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlUtilityBillId() . ', ' .
						$this->sqlOcrTemplateId() . ', ' .
						$this->sqlOcrTemplateLabelId() . ', ' .
						$this->sqlSystemLabel() . ', ' .
						$this->sqlParsedLabel() . ', ' .
						$this->sqlParsedValue() . ', ' .
						$this->sqlLabelScore() . ', ' .
						$this->sqlValueScore() . ', ' .
						$this->sqlNote() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_id = ' . $this->sqlUtilityBillId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_template_id = ' . $this->sqlOcrTemplateId(). ',' ; } elseif( true == array_key_exists( 'OcrTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_template_id = ' . $this->sqlOcrTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ocr_template_label_id = ' . $this->sqlOcrTemplateLabelId(). ',' ; } elseif( true == array_key_exists( 'OcrTemplateLabelId', $this->getChangedColumns() ) ) { $strSql .= ' ocr_template_label_id = ' . $this->sqlOcrTemplateLabelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_label = ' . $this->sqlSystemLabel(). ',' ; } elseif( true == array_key_exists( 'SystemLabel', $this->getChangedColumns() ) ) { $strSql .= ' system_label = ' . $this->sqlSystemLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parsed_label = ' . $this->sqlParsedLabel(). ',' ; } elseif( true == array_key_exists( 'ParsedLabel', $this->getChangedColumns() ) ) { $strSql .= ' parsed_label = ' . $this->sqlParsedLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parsed_value = ' . $this->sqlParsedValue(). ',' ; } elseif( true == array_key_exists( 'ParsedValue', $this->getChangedColumns() ) ) { $strSql .= ' parsed_value = ' . $this->sqlParsedValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label_score = ' . $this->sqlLabelScore(). ',' ; } elseif( true == array_key_exists( 'LabelScore', $this->getChangedColumns() ) ) { $strSql .= ' label_score = ' . $this->sqlLabelScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value_score = ' . $this->sqlValueScore(). ',' ; } elseif( true == array_key_exists( 'ValueScore', $this->getChangedColumns() ) ) { $strSql .= ' value_score = ' . $this->sqlValueScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'utility_bill_id' => $this->getUtilityBillId(),
			'ocr_template_id' => $this->getOcrTemplateId(),
			'ocr_template_label_id' => $this->getOcrTemplateLabelId(),
			'system_label' => $this->getSystemLabel(),
			'parsed_label' => $this->getParsedLabel(),
			'parsed_value' => $this->getParsedValue(),
			'label_score' => $this->getLabelScore(),
			'value_score' => $this->getValueScore(),
			'note' => $this->getNote(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>