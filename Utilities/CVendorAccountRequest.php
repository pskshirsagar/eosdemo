<?php

class CVendorAccountRequest extends CBaseVendorAccountRequest {

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valEntityName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valEmailAddress( $objDatabase ) {

		if( false == valStr( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is required.' ) ) );
		} elseif( true == isset( $this->m_strEmail ) && $this->m_strEmailAddress != filter_var( $this->m_strEmailAddress, FILTER_VALIDATE_EMAIL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address does not appear to be valid.' ) ) );
		} elseif( 0 < strlen( $this->m_strEmailAddress ) && true == valObj( $objDatabase, 'CDatabase' ) && 0 < \Psi\Eos\Utilities\CVendorAccountRequests::createService()->fetchVendorAccountRequestCount( ' WHERE lower( email_address ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getEmailAddress() ) ) . '\'', $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is already being used.' ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valPasswordHashed() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function getTaxNumber() {
		if( false == valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER );
	}

	public function setTaxNumber( $strPlainTaxNumber ) {

		if( false == valStr( $strPlainTaxNumber ) || true == \Psi\CStringService::singleton()->stristr( $strPlainTaxNumber, '*' ) ) {
			return;
		}
		if( false == valStr( $strPlainTaxNumber ) ) {
			return;
		}
		$this->setTaxNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );

		$this->setTaxNumberHash( $strPlainTaxNumber );
	}

	public function setTaxNumberHash( $strPlainTaxNumber ) {
		$intPlainTaxNumber = Psi\Libraries\UtilStrings\CStrings::createService()->strTrimDef( str_replace( '-', '', $strPlainTaxNumber ) );

		if( true == is_numeric( $intPlainTaxNumber ) ) {
			parent::setTaxNumberHash( \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $intPlainTaxNumber, CONFIG_SODIUM_KEY_BLIND_INDEX ) );
		} else {
			parent::setTaxNumberHash( $intPlainTaxNumber );
		}
	}

	public function setPassword( $strPassword ) {

		if( true == valStr( $strPassword ) ) {
			parent::setPasswordHashed( \Psi\Libraries\Cryptography\CCrypto::createService()->hash( trim( $strPassword ) ) );
		}
	}

	public function valTaxNumberEncrypted( $objDatabase ) {
		$boolIsValid  = true;
		$strTaxNumber = $this->getTaxNumber();

		if( CVendorEntity::TAX_ID_NUMBER_FORMAT_ONE == $strTaxNumber || CVendorEntity::TAX_ID_NUMBER_FORMAT_TWO == $strTaxNumber ) {
			return $boolIsValid;
		}

		if( true == valStr( $strTaxNumber ) ) {
			if( false == preg_match( '/^[\d\-]+$/', $strTaxNumber ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax number should be in XX-XXXXXXX or XXX-XX-XXXX and should be numeric.' ) ) );
			} elseif( false == preg_match( '/^([\d]{2}-[\d]{7})$|^([\d]{3}-[\d]{2}-[\d]{4})$/', $strTaxNumber ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax number should be in XX-XXXXXXX or XXX-XX-XXXX.' ) ) );
			} elseif( 0 < \Psi\Eos\Utilities\CVendorAccountRequests::createService()->fetchVendorAccountRequestCount( ' WHERE tax_number_hash = \'' . $this->getTaxNumberHash() . '\'', $objDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax number is already being used.' ) ) );
			}
		} else {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax number is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTaxNumberHash() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valEmailAddress( $objDatabase );
				$boolIsValid &= $this->valTaxNumberEncrypted( $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>