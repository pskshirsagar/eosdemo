<?php

class CUtilityBatchStep extends CBaseUtilityBatchStep {

	const VALIDATE 					= 1;
	const BILL_SELECTION			= 2;
	const BILLING_PERIOD			= 3;
	const RESYNC_RESIDENT_INFO 		= 4;
	const APPROVE_CHARGES			= 5;
	const TRANSMISSIONS_APPROVAL	= 6;
	const PRE_BATCH_READY       	= 7;
	const CALCULATE_BILLING_DATA 	= 8;
	const QC_READY                  = 9;
	const QC_APPROVED               = 10;
	const ANALYST_APPROVED 			= 11;
	const PENDING_MANAGER_APPROVAL 	= 12;
	const MANAGER_APPROVED 			= 13;
	const NOTICES 					= 14;
	const GENERATE_INVOICES 		= 15;
	const VERIFY_INVOICES 			= 16;
	const POST_BATCH 				= 17;
	const BATCH_POSTED 				= 18;
	const BATCH_DISTRIBUTED 		= 19;
	const BATCH_INTEGRATED 			= 20;
	const COMPLETE 					= 21;

	public static function getNameByUtilityBatchStepId( $intUtilityBatchStepId ) {

		$strUtilityBatchStep = '';

		switch( $intUtilityBatchStepId ) {
			case CUtilityBatchStep::VALIDATE:
				$strUtilityBatchStep = 'Validating';
				break;

			case CUtilityBatchStep::BILL_SELECTION:
				$strUtilityBatchStep = 'Bill Selection';
				break;

			case CUtilityBatchStep::BILLING_PERIOD:
				$strUtilityBatchStep = 'Service Period';
				break;

			case CUtilityBatchStep::RESYNC_RESIDENT_INFO:
				$strUtilityBatchStep = 'Syncing Resident Info';
				break;

			case CUtilityBatchStep::APPROVE_CHARGES:
				$strUtilityBatchStep = 'Approve Charges';
				break;

			case CUtilityBatchStep::TRANSMISSIONS_APPROVAL:
				$strUtilityBatchStep = 'Transmission Approval';
				break;

			case CUtilityBatchStep::PRE_BATCH_READY:
				$strUtilityBatchStep = 'Pre Batch Ready';
				break;

			case CUtilityBatchStep::CALCULATE_BILLING_DATA:
				$strUtilityBatchStep = 'Calculate Billing Data';
				break;

			case CUtilityBatchStep::QC_READY:
				$strUtilityBatchStep = 'QC Ready';
				break;

			case CUtilityBatchStep::QC_APPROVED:
				$strUtilityBatchStep = 'QC Approved';
				break;

			case CUtilityBatchStep::ANALYST_APPROVED:
				$strUtilityBatchStep = 'Analyst Approved';
				break;

			case CUtilityBatchStep::PENDING_MANAGER_APPROVAL:
				$strUtilityBatchStep = 'Pending Manager Approval';
				break;

			case CUtilityBatchStep::MANAGER_APPROVED:
				$strUtilityBatchStep = 'Manager Approved';
				break;

			case CUtilityBatchStep::NOTICES:
				$strUtilityBatchStep = 'Notices';
				break;

			case CUtilityBatchStep::GENERATE_INVOICES:
				$strUtilityBatchStep = 'Generate Invoices';
				break;

			case CUtilityBatchStep::VERIFY_INVOICES:
				$strUtilityBatchStep = 'Verify Invoices';
				break;

			case CUtilityBatchStep::POST_BATCH:
				$strUtilityBatchStep = 'Post Batch';
				break;

			case CUtilityBatchStep::BATCH_POSTED:
				$strUtilityBatchStep = 'Batch Posted';
				break;

			case CUtilityBatchStep::BATCH_DISTRIBUTED:
				$strUtilityBatchStep = 'Batch Distributed';
				break;

			case CUtilityBatchStep::BATCH_INTEGRATED:
				$strUtilityBatchStep = 'Batch Integrated';
				break;

			case CUtilityBatchStep::COMPLETE:
				$strUtilityBatchStep = 'Complete';
				break;

			default:
				// default case
				break;
		}

		return $strUtilityBatchStep;
	}

	public static function assignUtilityBatchStepConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['validate']					= self::VALIDATE;
		$arrmixTemplateParameters['bill_selection']				= self::BILL_SELECTION;
		$arrmixTemplateParameters['billing_period']				= self::BILLING_PERIOD;
		$arrmixTemplateParameters['resync_resident_info']		= self::RESYNC_RESIDENT_INFO;
		$arrmixTemplateParameters['approve_charges']			= self::APPROVE_CHARGES;
		$arrmixTemplateParameters['transmissions_approval']		= self::TRANSMISSIONS_APPROVAL;
		$arrmixTemplateParameters['pre_batch_ready']        	= self::PRE_BATCH_READY;
		$arrmixTemplateParameters['calculate_billing_data']		= self::CALCULATE_BILLING_DATA;
		$arrmixTemplateParameters['qc_ready']					= self::QC_READY;
		$arrmixTemplateParameters['qc_approved']				= self::QC_APPROVED;
		$arrmixTemplateParameters['analyst_approved']			= self::ANALYST_APPROVED;
		$arrmixTemplateParameters['pending_manager_approval']	= self::PENDING_MANAGER_APPROVAL;
		$arrmixTemplateParameters['manager_approved']			= self::MANAGER_APPROVED;
		$arrmixTemplateParameters['notices']					= self::NOTICES;
		$arrmixTemplateParameters['generate_invoices']			= self::GENERATE_INVOICES;
		$arrmixTemplateParameters['verify_invoices']			= self::VERIFY_INVOICES;
		$arrmixTemplateParameters['post_batch']					= self::POST_BATCH;
		$arrmixTemplateParameters['batch_posted']				= self::BATCH_POSTED;
		$arrmixTemplateParameters['batch_distributed']			= self::BATCH_DISTRIBUTED;
		$arrmixTemplateParameters['batch_integrated']			= self::BATCH_INTEGRATED;
		$arrmixTemplateParameters['complete']					= self::COMPLETE;

		return $arrmixTemplateParameters;
	}

}
?>