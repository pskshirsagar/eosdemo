<?php

class CMissingBillAlert extends CBaseMissingBillAlert {

	protected $m_intRecentEventId;

    public function valReminderDatetime() {
        $boolIsValid = true;
        if( true == is_null( $this->getReminderDatetime() ) ) {
            $boolIsValid &= false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reminder_datetime', 'Please set reminder date' ) );
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valReminderDatetime();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues,$boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['recent_event_id'] ) ) {
        	$this->setRecentEventId( $arrmixValues['recent_event_id'] );
        }
    }

    public function setRecentEventId( $intRecentEventId ) {
    	$this->m_intRecentEventId = CStrings::strToIntDef( $intRecentEventId );
    }

    public function getRecentEventId() {
    	return $this->m_intRecentEventId;
    }

}
?>