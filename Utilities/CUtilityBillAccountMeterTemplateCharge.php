<?php

class CUtilityBillAccountMeterTemplateCharge extends CBaseUtilityBillAccountMeterTemplateCharge {

	protected $m_intUtilityBillTemplateChargeId;
	protected $m_intUtilityTypeId;
	protected $m_intCommodityId;
	protected $m_intUtilityConsumptionTypeId;
	protected $m_strMeterNumber;
	protected $m_boolIsIgnored;
	protected $m_boolIsDemandMeter;
	protected $m_boolIsTimeOfUseMeter;
	protected $m_boolCaptureUsageTotalOnly;
	Protected $m_strReplacementMeter;

	protected $m_strDatabaseTransactionType;

	public function __construct() {
		parent::__construct();
		$this->m_strDatabaseTransactionType = 'insert';
	}

	public function getUtilityTypeId() {
    	return $this->m_intUtilityTypeId;
	}

    public function getUtilityBillTemplateChargeId() {
    	return $this->m_intUtilityBillTemplateChargeId;
    }

    public function getUtilityConsumptionTypeId() {
    	return $this->m_intUtilityConsumptionTypeId;
    }

    public function getMeterNumber() {
    	return $this->m_strMeterNumber;
    }

    public function getIsIgnored() {
    	return $this->m_boolIsIgnored;
    }

	public function getIsDemandMeter() {
		return $this->m_boolIsDemandMeter;
	}

	public function getIsTimeOfUseMeter() {
		return $this->m_boolIsTimeOfUseMeter;
	}

	public function getCaptureUsageTotalOnly() {
		return $this->m_boolCaptureUsageTotalOnly;
	}

    public function getReplacementMeter() {
    	return $this->m_strReplacementMeter;
    }

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getDatabaseTransactionType() {
		return $this->m_strDatabaseTransactionType;
	}

	public function getCommodityId() {
		return $this->m_intCommodityId;
	}

	public function setDatabaseTransactionType( $strDatabaseTransactionType ) {
		$this->m_strDatabaseTransactionType = $strDatabaseTransactionType;
	}

    public function setUtilityTypeId( $intUtilityTypeId ) {
    	$this->m_intUtilityTypeId = $intUtilityTypeId;
    }

    public function setUtilityBillTemplateChargeId( $intUtilityBillTemplateChargeId ) {
    	$this->m_intUtilityBillTemplateChargeId = $intUtilityBillTemplateChargeId;
    }

    public function setUtilityConsumptionTypeId( $intUtilityConsumptionTypeId ) {
    	$this->m_intUtilityConsumptionTypeId = $intUtilityConsumptionTypeId;
    }

    public function setMeterNumber( $strMeterNumber ) {
    	$this->m_strMeterNumber = $strMeterNumber;
    }

    public function setIsIgnored( $boolIsIgnored ) {
    	$this->m_boolIsIgnored = CStrings::strToBool( $boolIsIgnored );
    }

	public function setIsDemandMeter( $boolIsDemandMeter ) {
		$this->m_boolIsDemandMeter = CStrings::strToBool( $boolIsDemandMeter );
	}

	public function setIsTimeOfUseMeter( $boolIsTimeOfUseMeter ) {
		$this->m_boolIsTimeOfUseMeter = CStrings::strToBool( $boolIsTimeOfUseMeter );
	}

	public function setCaptureUsageTotalOnly( $boolCaptureUsageTotalOnly ) {
		$this->m_boolCaptureUsageTotalOnly = CStrings::strToBool( $boolCaptureUsageTotalOnly );
	}

    public function setReplacementMeter( $strReplacementMeter ) {
    	$this->m_strReplacementMeter = $strReplacementMeter;
    }

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = $strAccountNumber;
	}

	public function setCommodityId( $intCommodityId ) {
		$this->m_intCommodityId = $intCommodityId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_type_id'] ) ) {
			$this->setUtilityTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_type_id'] ) : $arrmixValues['utility_type_id'] );
		}
		if( true == isset( $arrmixValues['utility_bill_template_charge_id'] ) ) {
			$this->setUtilityBillTemplateChargeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_template_charge_id'] ) : $arrmixValues['utility_bill_template_charge_id'] );
		}
		if( true == isset( $arrmixValues['utility_consumption_type_id'] ) ) {
			$this->setUtilityConsumptionTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_consumption_type_id'] ) : $arrmixValues['utility_consumption_type_id'] );
		}
		if( true == isset( $arrmixValues['meter_number'] ) ) {
			$this->setMeterNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['meter_number'] ) : $arrmixValues['meter_number'] );
		}
		if( true == isset( $arrmixValues['is_ignored'] ) ) {
			$this->setIsIgnored( $arrmixValues['is_ignored'] );
		}
		if( true == isset( $arrmixValues['replacement_meter'] ) ) {
			$this->setReplacementMeter( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['replacement_meter'] ) : $arrmixValues['replacement_meter'] );
		}

		if( true == isset( $arrmixValues['is_demand_meter'] ) ) {
			$this->setIsDemandMeter( $arrmixValues['is_demand_meter'] );
		}

		if( true == isset( $arrmixValues['is_time_of_use_meter'] ) ) {
			$this->setIsTimeOfUseMeter( $arrmixValues['is_time_of_use_meter'] );
		}

		if( true == isset( $arrmixValues['capture_usage_total_only'] ) ) {
			$this->setCaptureUsageTotalOnly( $arrmixValues['capture_usage_total_only'] );
		}

		if( true == isset( $arrmixValues['commodity_id'] ) ) {
			$this->setCommodityId( $arrmixValues['commodity_id'] );
		}

		return true;
	}

}
?>