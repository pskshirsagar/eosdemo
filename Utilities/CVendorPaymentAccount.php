<?php

class CVendorPaymentAccount extends CBaseVendorPaymentAccount {

	protected $m_intVendorEntityId;
	protected $m_strAuthToken;
	protected $m_strCurrentCcCardNumberEncrypted;
	protected $m_arrintVendorEntityIds;

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valPaymentTypeId() {

		$boolIsValid = true;

		if( true == is_null( $this->m_intPaymentTypeId ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type_id', 'Payment type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {
		return true;
	}

	public function valBilltoAcountNumber() {
		return true;
	}

	public function valBilltoCompanyName() {
		return true;
	}

	public function valBilltoNameFirst() {
		return true;
	}

	public function valBilltoNameLast() {
		return true;
	}

	public function valBilltoStreetLine1() {

		$boolIsValid = true;

		if( true == is_null( $this->m_strBilltoStreetLine1 ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_street_line1', 'Street line 1 is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoStreetLine2() {
		return true;
	}

	public function valBilltoStreetLine3() {
		return true;
	}

	public function valBilltoUnitNumber() {
		return true;
	}

	public function valBilltoCity() {

		$boolIsValid = true;

		if( true == is_null( $this->m_strBilltoCity ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_city', 'City is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoStateCode() {

		$boolIsValid = true;

		if( true == is_null( $this->m_strBilltoStateCode ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_state_code', 'State is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoProvince() {
		return true;
	}

	public function valBilltoPostalCode() {

		if( false == isset( $this->m_strBilltoPostalCode ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code', 'Zip code is required.' ) );
		} elseif( false == CValidation::validatePostalCode( $this->m_strBilltoPostalCode, CCountry::CODE_USA ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code',  'Valid zip code is required.' ) );
		} elseif( false == CValidation::validateMilitaryPostalCode( $this->m_strBilltoPostalCode, $this->m_strBilltoStateCode ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code',  'Billing military state zip code must start with 0 or 9.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valBilltoCountryCode() {

		$boolIsValid = true;

		if( true == is_null( $this->m_strBilltoCountryCode ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_country_code', 'Country is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBilltoPhoneNumber() {
		return true;
	}

	public function valBilltoEmailAddress() {
		return true;
	}

	public function valSecureReferenceNumber() {

		$boolIsValid = true;
		if( true == is_null( $this->m_strSecureReferenceNumber ) && false == valStr( $this->m_strSecureReferenceNumber ) && true == valStr( $this->m_strCcCardNumberEncrypted ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', 'Error in communicating processing gateway.', 621 ) );
		}

		return $boolIsValid;
	}

	public function valCcCardNumberEncrypted( $intVendorId = NULL, $objDatabase = NULL ) {

		$boolIsValid = true;

		$strCardNumber = $this->getCcCardNumberEncrypted();
		$strWhere = ' WHERE cc_card_number_encrypted = \'' . $this->m_strCcCardNumberEncrypted . '\' AND id != ' . ( int ) $this->getId() . ' AND vendor_id = ' . ( int ) $intVendorId . ' AND deleted_by IS NULL AND deleted_on IS NULL';

		if( true == is_null( $strCardNumber ) || 0 == strlen( $strCardNumber ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number_encrypted', 'Card number is required.' ) );
			return false;
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) && 0 < \Psi\Eos\Utilities\CVendorPaymentAccounts::createService()->fetchVendorPaymentAccountCount( $strWhere, $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number_encrypted', 'Card number is already exist.' ) );
		}

		return $boolIsValid;
	}

	public function valCcExpDateMonth() {

		$boolIsValid = true;

		if( false == isset( $this->m_strCcExpDateMonth ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_month', 'Expiration month is required.' ) );
		}

		if( true == isset( $this->m_strCcExpDateMonth ) && true == isset( $this->m_strCcExpDateYear ) && $this->m_strCcExpDateYear == date( 'Y' ) && $this->m_strCcExpDateMonth < date( 'm' ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_month', 'Expiration date is invalid or expired.' ) );
		}

		return $boolIsValid;
	}

	public function valCcExpDateYear() {

		$boolIsValid = true;

		if( false == isset( $this->m_strCcExpDateYear ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_year', 'Expiration year is required.' ) );
		}

		if( true == isset( $this->m_strCcExpDateYear ) && $this->m_strCcExpDateYear < date( 'Y' ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_year', 'Expiration year is invalid or expired.' ) );
		}

		return $boolIsValid;
	}

	public function valCcNameOnCard() {

		$boolIsValid = true;

		if( false == isset( $this->m_strCcNameOnCard ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_name_on_card', 'Name on card is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCcBinNumber() {
		return true;
	}

	public function valIsDebitCard() {
		return true;
	}

	public function valIsDefault() {
		return true;
	}

	public function valCheckBankName() {

		if( false == isset( $this->m_strCheckBankName ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', 'Bank name is required.' ) );
		} elseif( true == valStr( $this->m_strCheckBankName ) && false == preg_match( "/^[a-z0-9\s\-.',&\/\\\]+$/i", $this->m_strCheckBankName ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', 'Bank name is not valid.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valCheckAccountTypeId() {

		$boolIsValid = true;

		if( false == is_numeric( $this->m_intCheckAccountTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_type_id', 'Account type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckAccountNumberEncrypted( $intVendorId = NULL, $objDatabase = NULL ) {

		$boolIsValid = true;

		$strWhere = ' WHERE check_account_number_encrypted = \'' . $this->m_strCheckAccountNumberEncrypted . '\' AND id != ' . ( int ) $this->getId() . ' AND vendor_id = ' . ( int ) $intVendorId . ' AND deleted_by IS NULL AND deleted_on IS NULL';
		if( true == is_null( $this->getCheckAccountNumber() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', 'Account number is required.' ) );
		}

		if( 17 < strlen( $this->getCheckAccountNumber() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', 'Account number should not be more than 17 digits long.' ) );
		} elseif( true == valObj( $objDatabase, 'CDatabase' ) && 0 < \Psi\Eos\Utilities\CVendorPaymentAccounts::createService()->fetchVendorPaymentAccountCount( $strWhere, $objDatabase ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', 'Account number is already exist.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckRoutingNumber() {

		$boolIsValid = true;

		if( true == is_null( $this->getCheckRoutingNumber() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number is required.' ) );
			return false;
		}

		if( 0 !== preg_match( '/[^0-9]/', $this->getCheckRoutingNumber() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be a number.' ) );
			return false;
		}

		if( 9 != strlen( $this->getCheckRoutingNumber() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must contain only 9 digits.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valCheckNameOnAccount() {

		if( false == isset( $this->m_strCheckNameOnAccount ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', 'Account holder&#39;s name is required.' ) );
			$boolIsValid = false;
		} elseif( true == valStr( $this->m_strCheckNameOnAccount ) && false == preg_match( "/^[a-z0-9\s\-.',&\/\\\]+$/i", $this->m_strCheckNameOnAccount ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', 'Account holder&#39;s name not valid.' ) );
			$boolIsValid = false;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valAccountNickname() {

		$boolIsValid = true;

		if( true == is_null( $this->m_strAccountNickname ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_nickname', 'Account nickname is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsInternational() {
		return true;
	}

	public function valLastUsedOn() {
		return true;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function setVendorEntityId( $intVendorEntityId ) {
		$this->m_intVendorEntityId = $intVendorEntityId;
	}

	public function getVendorEntityId() {
		return $this->m_intVendorEntityId;
	}

	public function setAuthToken( $strAuthToken ) {
		$this->m_strAuthToken = $strAuthToken;
	}

	public function getAuthToken() {
		return $this->m_strAuthToken;
	}

	public function setCurrentCcCardNumberEncrypted( $strCurrentCcCardNumberEncrypted ) {
		$this->m_strCurrentCcCardNumberEncrypted = $strCurrentCcCardNumberEncrypted;
	}

	public function getCurrentCcCardNumberEncrypted() {
		return $this->m_strCurrentCcCardNumberEncrypted;
	}

	public function valVendorEntityIds() {

		$boolIsValid = true;

		if( false == valArr( $this->getVendorEntityIds() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_entity_id', 'Entity is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intPaymentTypeId, $intVendorId = NULL, $objDatabase = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valVendorEntityIds();
				$boolIsValid &= $this->valAccountNickname();
				$boolIsValid &= $this->valBilltoStreetLine1();
				$boolIsValid &= $this->valBilltoCity();
				$boolIsValid &= $this->valBilltoStateCode();
				$boolIsValid &= $this->valBilltoPostalCode();
				$boolIsValid &= $this->valBilltoCountryCode();

				if( $intPaymentTypeId == CPaymentType::ACH ) {
					$boolIsValid &= $this->valCheckBankName();
					$boolIsValid &= $this->valCheckNameOnAccount();
					$boolIsValid &= $this->valCheckAccountTypeId();
					$boolIsValid &= $this->valCheckRoutingNumber();
					$boolIsValid &= $this->valCheckAccountNumberEncrypted( $intVendorId, $objDatabase );
				}

				if( $intPaymentTypeId == CPaymentType::VISA || $intPaymentTypeId == CPaymentType::MASTERCARD || $intPaymentTypeId == CPaymentType::DISCOVER || $intPaymentTypeId == CPaymentType::AMEX ) {
					$boolIsValid &= $this->valCcNameOnCard();
					$boolIsValid &= $this->valCcCardNumberEncrypted( $intVendorId, $objDatabase );
					$boolIsValid &= $this->valCcExpDateYear();
					$boolIsValid &= $this->valCcExpDateMonth();
					$boolIsValid &= $this->valSecureReferenceNumber();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {

			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

	/**
	 * Set Functions
	 *
	 */

	public function setVendorEntityIds( $arrintVendorEntityIds ) {
		$this->m_arrintVendorEntityIds = $arrintVendorEntityIds;
	}

	public function setCcCardNumber( $strCcCardNumber ) {
		if( false == valStr( $strCcCardNumber ) ) {
			return;
		}
		$strCcCardNumber = CStrings::strTrimDef( $strCcCardNumber, 200, NULL, true );
		$this->setCcCardNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCcCardNumber, CONFIG_SODIUM_KEY_CC_CARD_NUMBER ) );
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {

		if( false == valStr( $strCheckAccountNumber ) || true == \Psi\CStringService::singleton()->stristr( $strCheckAccountNumber, 'X' ) ) {
			return;
		}

		$strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 200, NULL, true );
		$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

	/**
	 * Get Functions
	 *
	 */

	public function getVendorEntityIds() {
		return $this->m_arrintVendorEntityIds;
	}

	public function getCcCardNumberEncrypted() {
		return( true == isset( $this->m_strCcCardNumberEncrypted ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCcCardNumberEncrypted, CONFIG_SODIUM_KEY_CC_CARD_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CC_CARD_NUMBER ] ) : NULL;
	}

	public function getCheckAccountNumber() {
		return( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] ) : NULL;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['vendor_entity_ids'] ) && $boolDirectSet ) {
			$this->m_arrintVendorEntityIds = $arrmixValues['vendor_entity_ids'];
		} elseif( isset( $arrmixValues['vendor_entity_ids'] ) ) {
			$this->setVendorEntityIds( $arrmixValues['vendor_entity_ids'] );
		}

		if( true == isset( $arrmixValues['cc_card_number'] ) ) {
			$this->setCcCardNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_card_number'] ) : $arrmixValues['cc_card_number'] );
		}

		if( true == isset( $arrmixValues['auth_token'] ) ) {
			$this->setAuthToken( $arrmixValues['auth_token'] );
		}

		if( true == isset( $arrmixValues['check_account_number'] ) ) {
			$this->setCheckAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['check_account_number'] ) : $arrmixValues['check_account_number'] );
		}

		if( true == isset( $arrmixValues['vendor_entity_id'] ) ) {
			$this->setVendorEntityId( $arrmixValues['vendor_entity_id'] );
		}

	}

}
?>