<?php

class CPropertyUtilityTypeRateMethod extends CBasePropertyUtilityTypeRateMethod {

	protected $m_arrobjPropertyUtilityTypeFactorRules;
	protected $m_arrobjPropertyUtilityTypeUnitTypes;
	protected $m_arrobjOldPropertyUtilityTypeFactorRules;

	protected $m_intNewMeterRateTypeId;
	protected $m_intOldPropertyUtilityTypeRateMethodId;

	protected $m_arrintCustomRubsFormulaIds;

	protected $m_strTotalPercentage;

	const VALIDATE_DUPLICATE_RUBS_FORMULA		= 'validate_duplicate_rubs_formula';
	const VALIDATE_DUPLICATE_METER_RATE_TYPES	= 'validate_duplicate_meter_rate_types';
	const VALIDATE_SUM_OF_PERCENTAGE			= 'validate_sum_of_percentage';

    /**
     * Get Functions
     */

    public function getPropertyUtilityTypeFactorRules() {
    	return $this->m_arrobjPropertyUtilityTypeFactorRules;
    }

    public function getPropertyUtilityTypeUnitTypes() {
    	return $this->m_arrobjPropertyUtilityTypeUnitTypes;
    }

    public function getOldPropertyUtilityTypeFactorRules() {
    	return $this->m_arrobjOldPropertyUtilityTypeFactorRules;
    }

    public function getOldPropertyUtilityTypeRateMethodId() {
    	return $this->m_intOldPropertyUtilityTypeRateMethodId;
    }

	public function getCustomRubsFormulaIds() {
		return $this->m_arrintCustomRubsFormulaIds;
	}

	public function getNewMeterRateTypeId() {
		return $this->m_intNewMeterRateTypeId;
	}

	public function getTotalPercentage() {
		return $this->m_strTotalPercentage;
	}

    /**
     * Add Functions
     */

    public function setOldPropertyUtilityTypeRateMethodId( $intOldPropertyUtilityTypeRateMethodId ) {
    	$this->m_intOldPropertyUtilityTypeRateMethodId = $intOldPropertyUtilityTypeRateMethodId;
    }

    public function addPropertyUtilityTypeFactorRules( $objPropertyUtilityTypeFactorRule ) {

    	if( CUtilityRubsFormula::SQUARE_FOOTAGE_WEIGHTED == $objPropertyUtilityTypeFactorRule->getUtilityRubsFormulaId() ) {
    		$this->m_arrobjPropertyUtilityTypeFactorRules[$objPropertyUtilityTypeFactorRule->getSquareFeet()] = $objPropertyUtilityTypeFactorRule;
    	} else {
    		$this->m_arrobjPropertyUtilityTypeFactorRules[$objPropertyUtilityTypeFactorRule->getCount()] = $objPropertyUtilityTypeFactorRule;
    	}
    }

    public function addOldPropertyUtilityTypeFactorRules( $objPropertyUtilityTypeFactorRule ) {

    	if( CUtilityRubsFormula::SQUARE_FOOTAGE_WEIGHTED == $objPropertyUtilityTypeFactorRule->getUtilityRubsFormulaId() ) {
    		$this->m_arrobjOldPropertyUtilityTypeFactorRules[$objPropertyUtilityTypeFactorRule->getSquareFeet()] = $objPropertyUtilityTypeFactorRule;
    	} else {
    		$this->m_arrobjOldPropertyUtilityTypeFactorRules[$objPropertyUtilityTypeFactorRule->getCount()] = $objPropertyUtilityTypeFactorRule;
    	}
    }

    public function addPropertyUtilityTypeUnitTypes( $objPropertyUtilityTypeUnitType ) {
    	$this->m_arrobjPropertyUtilityTypeUnitTypes[$objPropertyUtilityTypeUnitType->getUnitTypeId()] = $objPropertyUtilityTypeUnitType;
    }

	/**
	 * Set Functions
	 */

	public function setCustomRubsFormulaIds( $arrintCustomRubsFormulaIds ) {
		$this->m_arrintCustomRubsFormulaIds = $arrintCustomRubsFormulaIds;
	}

	public function setNewMeterRateTypeId( $intNewMeterRateTypeId ) {
		$this->m_intNewMeterRateTypeId = CStrings::strToIntDef( $intNewMeterRateTypeId, NULL, false );
	}

	public function setTotalPercentage( $strTotalPercentage ) {
		$this->m_strTotalPercentage = CStrings::strTrimDef( $strTotalPercentage, NULL, NULL, true );
	}

    /**
     * Validate Functions
     */

    public function valUtilityRubsFormulaId() {
        $boolIsValid = true;
        if( true == is_null( $this->getUtilityRubsFormulaId() ) && true == is_null( $this->getMeterRateTypeId() ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_rubs_formula_id', 'Please select rubs formula or meter rate type.' ) );
        }
        return $boolIsValid;
    }

    public function valMeterRateTypeId() {
        $boolIsValid = true;
        if( true == is_null( $this->getMeterRateTypeId() ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_rate_type_id', 'Please select meter rate.' ) );
        }
        return $boolIsValid;
    }

    public function valPercentage() {
        $boolIsValid = true;

        if( true == is_null( $this->getPercentage() ) || false == is_numeric( $this->getPercentage() ) || 0 >= $this->getPercentage() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percentage', 'Invalid percentage data.' ) );
        }
        return $boolIsValid;
    }

    public function valChargeCodeId() {
        $boolIsValid = true;
        if( true == is_null( $this->getChargeCodeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_id', 'Please select charge code.' ) );
        }
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        if( true == is_null( $this->getDescription() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Invalid description.' ) );
        }
        return $boolIsValid;
    }

	public function valDuplicateRubsFormula() {
		$boolIsValid = true;

		if( false == is_null( $this->getUtilityRubsFormulaId() ) && true == in_array( $this->getUtilityRubsFormulaId(), $this->getCustomRubsFormulaIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_rubs_formula_id', 'You can not repeat the formula more than once.' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateMeterRateTypes() {
		$boolIsValid = true;

		if( false == is_null( $this->getMeterRateTypeId() ) && $this->getNewMeterRateTypeId() == $this->getMeterRateTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_rate_type_id', 'You can not repeat the meter rate type more than once.' ) );
		}

		return $boolIsValid;
	}

	public function valSumOfPercentage() {

		$intTotalPercentage = 100;
		$boolIsValid = true;

		if( ( $this->getTotalPercentage() * 100 ) != $intTotalPercentage ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percentage', 'Sum of all the percentage must be equal to 100.' ) );
		}

		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valUtilityRubsFormulaId();
        		$boolIsValid &= $this->valPercentage();
        		break;

        	case VALIDATE_DELETE:
        		break;

			case self::VALIDATE_DUPLICATE_RUBS_FORMULA:
				$boolIsValid &= $this->valDuplicateRubsFormula();
				break;

			case self::VALIDATE_DUPLICATE_METER_RATE_TYPES:
				$boolIsValid &= $this->valDuplicateMeterRateTypes();
				break;

			case self::VALIDATE_SUM_OF_PERCENTAGE:
				$boolIsValid &= $this->valSumOfPercentage();
				break;

        	default:
        	    $boolIsValid = false;
        }

        return $boolIsValid;
    }

    /**
     * Fetch Functions
     */

    public function fetchPropertyUtilityTypeFactorRules( $objUtilitiesDatabase ) {

    	return \Psi\Eos\Utilities\CPropertyUtilityTypeFactorRules::createService()->fetchPropertyUtilityTypeFactorRulesByPropertyUtilityTypeRateIdByPropertyUtilityTypeRateMethodId( $this->getPropertyUtilityTypeRateId(), $this->getId(), $objUtilitiesDatabase );
    }

    public function fetchPropertyUtilityTypeUnitTypes( $objUtilitiesDatabase ) {

    	return \Psi\Eos\Utilities\CPropertyUtilityTypeUnitTypes::createService()->fetchPropertyUtilityTypeUnitTypesByPropertyUtilityTypeRateMethodId( $this->getId(), $objUtilitiesDatabase );
    }

    /**
     * Other Functions
     */

    public function createPropertyUtilityTypeFactorRule( $intPropertyUtilityTypeId ) {

    	$objPropertyUtilityTypeFactorRule = new CPropertyUtilityTypeFactorRule;

    	$objPropertyUtilityTypeFactorRule->setCid( $this->getCid() );
    	$objPropertyUtilityTypeFactorRule->setUtilityRubsFormulaId( $this->getUtilityRubsFormulaId() );
    	$objPropertyUtilityTypeFactorRule->setPropertyUtilityTypeId( $intPropertyUtilityTypeId );
    	$objPropertyUtilityTypeFactorRule->setPropertyUtilityTypeRateId( $this->getId() );

    	return $objPropertyUtilityTypeFactorRule;
    }

	public function createPropertyUtilityTypeUnitType( $intPropertyUtilityTypeId ) {

		$objPropertyUtilityTypeUnitType = new CPropertyUtilityTypeUnitType;

		$objPropertyUtilityTypeUnitType->setCid( $this->getCid() );
		$objPropertyUtilityTypeUnitType->setUtilityRubsFormulaId( $this->getUtilityRubsFormulaId() );
		$objPropertyUtilityTypeUnitType->setPropertyUtilityTypeId( $intPropertyUtilityTypeId );
		$objPropertyUtilityTypeUnitType->setPropertyUtilityTypeRateId( $this->getPropertyUtilityTypeRateId() );
		$objPropertyUtilityTypeUnitType->setPropertyUtilityTypeRateMethodId( $this->getId() );

		return $objPropertyUtilityTypeUnitType;
	}

}
?>