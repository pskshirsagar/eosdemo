<?php

class CUtilityBillAccountDetail extends CBaseUtilityBillAccountDetail {

	protected $m_arrobjUtilityBillAccounts;

	protected $m_strUtilityTypeName;
	protected $m_strUtilityProviderName;
	protected $m_strUtilityConsumptionTypeName;
	protected $m_strUtilityBillReceiptTypeName;
	protected $m_strUtilityBillPaymentTypeName;
	protected $m_strAccountName;
	protected $m_strAccountNumber;
	protected $m_strServiceAddress;
	protected $m_strUtilityBillTypeName;

	protected $m_intFrequencyId;
	protected $m_intUtilityBillTypeId;
	protected $m_intUtilityBillAccountId;
	protected $m_intRequiredBillsInPeriod;
	protected $m_intAllocationCount;
	protected $m_intSummaryUtilityBillAccountId;

	protected $m_boolIsTemplate;
	protected $m_boolIsInactive;
	protected $m_boolIsNeverEstimate;
	protected $m_boolIsFromUserInterface;

	const VALIDATE_SMART_BILL_ACCOUNT				= 'validate_smart_bill_account';
	const VALIDATE_UBAD_RECEIPT_SETTINGS_TAB		= 'receipt_settings';
	const VALIDATE_UBAD_UEM_RU_SETTINGS_TAB			= 'uem_and_ru_settings';
	const VALIDATE_UBAD_INFORMATION_SETTINGS_TAB	= 'information';

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_type_name'] ) ) {
			$this->setUtilityTypeName( $arrmixValues['utility_type_name'] );
		}
		if( true == isset( $arrmixValues['utility_provider_name'] ) ) {
			$this->setUtilityProviderName( $arrmixValues['utility_provider_name'] );
		}
		if( true == isset( $arrmixValues['utility_consumption_type_name'] ) ) {
			$this->setUtilityConsumptionTypeName( $arrmixValues['utility_consumption_type_name'] );
		}
		if( true == isset( $arrmixValues['utility_bill_receipt_type_name'] ) ) {
			$this->setUtilityBillReceiptTypeName( $arrmixValues['utility_bill_receipt_type_name'] );
		}
		if( true == isset( $arrmixValues['account_name'] ) ) {
			$this->setAccountName( $arrmixValues['account_name'] );
		}
		if( true == isset( $arrmixValues['utility_bill_account_id'] ) ) {
			$this->setUtilityBillAccountId( $arrmixValues['utility_bill_account_id'] );
		}
		if( true == isset( $arrmixValues['account_number'] ) ) {
			$this->setAccountNumber( $arrmixValues['account_number'] );
		}
		if( true == isset( $arrmixValues['allocation_count'] ) ) {
			$this->setAllocationCount( $arrmixValues['allocation_count'] );
		}
		if( true == isset( $arrmixValues['required_bills_in_period'] ) ) {
			$this->setRequiredBillsInPeriod( $arrmixValues['required_bills_in_period'] );
		}
		if( true == isset( $arrmixValues['is_template'] ) ) {
			$this->setIsTemplate( $arrmixValues['is_template'] );
		}
		if( true == isset( $arrmixValues['is_inactive'] ) ) {
			$this->setIsInactive( $arrmixValues['is_inactive'] );
		}
		if( true == isset( $arrmixValues['service_address'] ) ) {
			$this->setServiceAddress( $arrmixValues['service_address'] );
		}
		if( true == isset( $arrmixValues['is_never_estimate'] ) ) $this->setIsNeverEstimate( $arrmixValues['is_never_estimate'] );
		if( true == isset( $arrmixValues['utility_bill_type_id'] ) ) $this->setUtilityBillTypeId( $arrmixValues['utility_bill_type_id'] );

		if( true == isset( $arrmixValues['utility_bill_type_name'] ) ) {
			$this->setUtilityBillTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_type_name'] ) : $arrmixValues['utility_bill_type_name'] );
		}

		if( isset( $arrmixValues['ap_payee_id'] ) ) {
			$this->setApPayeeId( trim( $arrmixValues['ap_payee_id'] ) );
		}

		if( isset( $arrmixValues['ap_payee_location_id'] ) ) {
			$this->setApPayeeLocationId( trim( $arrmixValues['ap_payee_location_id'] ) );
		}

		if( isset( $arrmixValues['summary_utility_bill_account_id'] ) ) {
			$this->setSummaryUtilityBillAccountId( trim( $arrmixValues['summary_utility_bill_account_id'] ) );
		}

		if( isset( $arrmixValues['frequency_id'] ) ) {
			$this->setFrequencyId( trim( $arrmixValues['frequency_id'] ) );
		}

	}

    public function valUtilityTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_type_id', 'Utility type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valUtilityProviderId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityProviderId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_provider_id', 'Utility Provider is required.' ) );
        }
        return $boolIsValid;
    }

	public function valUtilityBillReceiptTypeId( $boolIsFromBillProcessing = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityBillReceiptTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_receipt_type_id', 'Bill receipt type is required.' ) );

		} elseif( false == $boolIsFromBillProcessing && CUtilityBillReceiptType::WEB_RETRIEVAL == $this->getUtilityBillReceiptTypeId() ) {
			$boolIsValid &= $this->valUtilityCredentialId();
		}

		return $boolIsValid;

	}

	public function valUtilityCredentialId() {

		$boolIsValid = true;
		if( true == is_null( $this->getUtilityCredentialId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_credential_id', 'Utility credential is required.' ) );

		}

		return $boolIsValid;
	}

    public function validate( $strAction, $boolIsFromBillProcessing = false ) {

        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUtilityProviderId();
				$boolIsValid &= $this->valUtilityBillReceiptTypeId( $boolIsFromBillProcessing );
				break;

			case self::VALIDATE_UBAD_UEM_RU_SETTINGS_TAB:
			case self::VALIDATE_UBAD_INFORMATION_SETTINGS_TAB:
			case self::VALIDATE_SMART_BILL_ACCOUNT:
				$boolIsValid &= $this->valUtilityProviderId();
				break;

			case self::VALIDATE_UBAD_RECEIPT_SETTINGS_TAB:
				$boolIsValid &= $this->valUtilityBillReceiptTypeId( $boolIsFromBillProcessing );
				break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function getUtilityTypeName() {
		return $this->m_strUtilityTypeName;
	}

	public function setUtilityTypeName( $strUtilityTypeName ) {
		$this->m_strUtilityTypeName = $strUtilityTypeName;
	}

	public function getUtilityProviderName() {
		return $this->m_strUtilityProviderName;
	}

	public function setUtilityProviderName( $strUtilityProviderName ) {
		$this->m_strUtilityProviderName = $strUtilityProviderName;
	}

	public function getUtilityConsumptionTypeName() {
		return $this->m_strUtilityConsumptionTypeName;
	}

	public function setUtilityConsumptionTypeName( $strUtilityConsumptionTypeName ) {
		$this->m_strUtilityConsumptionTypeName = $strUtilityConsumptionTypeName;
	}

	public function getUtilityBillReceiptTypeName() {
		return $this->m_strUtilityBillReceiptTypeName;
	}

	public function setUtilityBillReceiptTypeName( $strUtilityBillReceiptTypeName ) {
		$this->m_strUtilityBillReceiptTypeName = $strUtilityBillReceiptTypeName;
	}

	public function setServiceAddress( $strServiceAddress ) {
		$this->m_strServiceAddress = $strServiceAddress;
	}

	public function setUtilityBillTypeId( $intUtilityBillTypeId ) {
		$this->m_intUtilityBillTypeId = $intUtilityBillTypeId;
	}

	public function setIsNeverEstimate( $boolIsNeverEstimate ) {
		$this->m_boolIsNeverEstimate = CStrings::strToBool( $boolIsNeverEstimate );
	}

	public function addUtilityBillAccount( $objUtilityBillAccount ) {
		$this->m_arrobjUtilityBillAccounts[$objUtilityBillAccount->getId()] = $objUtilityBillAccount;
	}

	public function getUtilityBillAccounts() {
		return $this->m_arrobjUtilityBillAccounts;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = $strAccountNumber;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->m_intUtilityBillAccountId = $intUtilityBillAccountId;
	}

	public function getRequiredBillsInPeriod() {
		return $this->m_intRequiredBillsInPeriod;
	}

	public function setRequiredBillsInPeriod( $intRequiredBillsInPeriod ) {
		$this->m_intRequiredBillsInPeriod = $intRequiredBillsInPeriod;
	}

	public function setAllocationCount( $intAllocationCount ) {
		$this->set( 'm_intAllocationCount', CStrings::strToIntDef( $intAllocationCount ) );
	}

	public function getAllocationCount() {
		return $this->m_intAllocationCount;
	}

	public function setIsTemplate( $boolIsTemplate ) {
		$this->m_boolIsTemplate = CStrings::strToBool( $boolIsTemplate );
	}

	public function getIsTemplate() {
		return $this->m_boolIsTemplate;
	}

	public function setIsInactive( $boolIsInactive ) {
		$this->m_boolIsInactive = CStrings::strToBool( $boolIsInactive );
	}

	public function getIsInactive() {
		return $this->m_boolIsInactive;
	}

	public function getServiceAddress() {
		return $this->m_strServiceAddress;
	}

	public function getUtilityBillTypeId() {
		return $this->m_intUtilityBillTypeId;
	}

	public function getIsNeverEstimate() {
		return $this->m_boolIsNeverEstimate;
	}

	public function getUtilityBillTypeName() {
		return $this->m_strUtilityBillTypeName;
	}

	public function setUtilityBillTypeName( $strUtilityBillTypeName ) {
		$this->m_strUtilityBillTypeName = $strUtilityBillTypeName;
	}

	public function getIsFromUserInterface() {
		return $this->m_boolIsFromUserInterface;
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->m_intApPayeeId = CStrings::strToIntDef( $intApPayeeId, NULL, false );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->m_intApPayeeLocationId = CStrings::strToIntDef( $intApPayeeLocationId, NULL, false );
	}

	public function setIsFromUserInterface( $boolIsFromUserInterface ) {
		$this->m_boolIsFromUserInterface = CStrings::strToBool( $boolIsFromUserInterface );
	}

	public function getSummaryUtilityBillAccountId() {
		return $this->m_intSummaryUtilityBillAccountId;
	}

	public function setSummaryUtilityBillAccountId( $intSummaryUtilityBillAccountId ) {
		$this->m_intSummaryUtilityBillAccountId = $intSummaryUtilityBillAccountId;
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->m_intFrequencyId = $intFrequencyId;
	}

}
?>