<?php

class COcrTemplateLabel extends CBaseOcrTemplateLabel {

	const ACCOUNT_NUMBER = 1;
	const HOPPER = 10;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLabelNames() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>