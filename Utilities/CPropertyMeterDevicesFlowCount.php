<?php

class CPropertyMeterDevicesFlowCount extends CBasePropertyMeterDevicesFlowCount {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyMeterDeviceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFlowCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFlowCountDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsVacant() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>