<?php

class CUtilityBillAudit extends CBaseUtilityBillAudit {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAuditTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUtilityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAuditStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAuditResolutionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAuditUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillChargeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAuditMemo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAuditProgress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSavings() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>