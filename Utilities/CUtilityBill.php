<?php

class CUtilityBill extends CBaseUtilityBill {

	const UTILITY_VCR_RECOVERY_FEE_COST_AMOUNT 	= 5.00;

	const QUEUE_RU_BILLS_IN_REVIEW						= 'ru_bills_in_review';
	const QUEUE_IP_BILLS_IN_REVIEW						= 'ip_bills_in_review';
	const QUEUE_UEM_BILLS_IN_REVIEW						= 'uem_bills_in_review';
	const QUEUE_UNPROCESSED_RU_BILLS					= 'unprocessed_ru_bills';
	const QUEUE_UNPROCESSED_IP_BILLS					= 'unprocessed_ip_bills';
	const QUEUE_UNPROCESSED_UEM_BILLS					= 'unprocessed_uem_bills';
	const QUEUE_UNASSOCIATED_IP_BILLS					= 'unassociated_ip_bills';
	const QUEUE_UNASSOCIATED_UTILITY_BILLS				= 'unassociated_utility_bills';
	const QUEUE_UNASSOCIATED_IP_BILLS_IN_REVIEW			= 'unassociated_ip_bills_in_review';
	const QUEUE_UNASSOCIATED_UTILITY_BILLS_IN_REVIEW	= 'unassociated_utility_bills_in_review';

	const VALIDATE_AUTO_PROCESSING_DETAILS = 'validate_auto_processing';

	const DO_NOT_PAY       = 'is_do_not_pay';
	const DUE_UPON_RECEIPT = 'is_due_upon_receipt';

	protected $m_strPropertyName;
	protected $m_strUtilityTypeName;
	protected $m_strUtilityBillAccountName;
	protected $m_strPropertyUtilityTypeName;
	protected $m_strUtilityDocumentFilePath;
	protected $m_strUtilityDocumentFileName;
	protected $m_strUtilityBillReceiptTypeName;
	protected $m_strUtilityBillPaymentTypeName;
	protected $m_strUtilityConsumptionTypeName;

	// Objects assigned for document processing.
	protected $m_objUtilityDocument;
    protected $m_objUtilityBillDocument;
	protected $m_objAdminDatabase;
	protected $m_objClientDatabase;
	protected $m_objEmailDatabase;

	protected $m_arrobjUtilityBillUnits;
	protected $m_arrobjOldUtilityBillUnits;
	protected $m_arrobjUtilityBillCharges;
	protected $m_arrobjUtilityBillMeterReads;
	protected $m_arrobjUtilityBillAllocations;

	protected $m_intUserId;
	protected $m_intUnitNumber;
	protected $m_intHasHighUsage;
	protected $m_intMeterEndRead;
	protected $m_intUtilityTypeId;
	protected $m_intBillAuditCount;
	protected $m_intPropertyUnitId;
	protected $m_intUtilityBatchId;
	protected $m_intMeterStartRead;
	protected $m_intAllocationCount;
	protected $m_intUtilityAccountId;
	protected $m_intVcrUtilityBillId;
	protected $m_intConsumptionUnits;
	protected $m_intDocumentPageCount;
	protected $m_intUtilityBillAmount;
	protected $m_intUtilityBatchStepId;
	protected $m_intBillEscalationCount;
	protected $m_intUemBillStatusTypeId;
	protected $m_intUtilityDistributionId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intMissingUtilityBillCount;
	protected $m_intUtilityConsumptionTypeId;
	protected $m_intAverageConsumptionDayDifferenece;
	protected $m_intFdgDataLogTypeId;

	protected $m_boolIsUemProperty;
	protected $m_boolIsSplitUtilityBill;
	protected $m_boolIsAssociatedWithBatch;
	protected $m_boolIsVcrVerified;

	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strMoveOutDate;
	protected $m_strBatchDateTime;
	protected $m_strEscalationNote;
	protected $m_strRequestDatetime;

	protected $m_fltVcmAmount;
	protected $m_fltUpdatedBillAmount;
	protected $m_fltUnitConsumptionAmount;
	protected $m_fltBillAuditSavingAmount;
	protected $m_fltVacantUnitBillAlertAmount;

	protected $m_arrintCommodityIds;
	protected $m_arrintPropertyBuildingIds;
	protected $m_arrintPropertyUtilityTypeIds;

    /**
     *Set Methods
     */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_name'] ) ) {
			$this->setPropertyUtilityTypeName( $arrmixValues['property_utility_type_name'] );
		}
		if( true == isset( $arrmixValues['utility_bill_receipt_type_name'] ) ) {
			$this->setUtilityBillReceiptTypeName( $arrmixValues['utility_bill_receipt_type_name'] );
		}
		if( true == isset( $arrmixValues['utility_bill_account_name'] ) ) {
			$this->setUtilityBillAccountName( $arrmixValues['utility_bill_account_name'] );
		}
		if( true == isset( $arrmixValues['utility_consumption_type_name'] ) ) {
			$this->setUtilityConsumptionTypeName( $arrmixValues['utility_consumption_type_name'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_id'] ) ) {
			$this->setPropertyUtilityTypeId( $arrmixValues['property_utility_type_id'] );
		}
		if( true == isset( $arrmixValues['missing_utility_bill_count'] ) ) {
			$this->setMissingUtilityBillCount( $arrmixValues['missing_utility_bill_count'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_ids'] ) ) {
			$this->setPropertyUtilityTypeIds( $arrmixValues['property_utility_type_ids'] );
		}
		if( true == isset( $arrmixValues['utility_bill_amount'] ) ) {
			$this->setUtilityBillAmount( $arrmixValues['utility_bill_amount'] );
		}
		if( true == isset( $arrmixValues['utility_document_file_path'] ) ) {
			$this->setUtilityDocumentFilePath( $arrmixValues['utility_document_file_path'] );
		}
		if( true == isset( $arrmixValues['utility_document_file_name'] ) ) {
			$this->setUtilityDocumentFileName( $arrmixValues['utility_document_file_name'] );
		}
		if( true == isset( $arrmixValues['utility_batch_step_id'] ) ) {
			$this->setUtilityBatchStepId( $arrmixValues['utility_batch_step_id'] );
		}
		if( true == isset( $arrmixValues['batch_datetime'] ) ) {
			$this->setBatchDateTime( $arrmixValues['batch_datetime'] );
		}
		if( true == isset( $arrmixValues['request_datetime'] ) ) {
			$this->setRequestDatetime( $arrmixValues['request_datetime'] );
		}
		if( true == isset( $arrmixValues['vcr_utility_bill_id'] ) ) {
			$this->setVcrUtilityBillId( $arrmixValues['vcr_utility_bill_id'] );
		}
		if( true == isset( $arrmixValues['vacant_unit_bill_alert_amount'] ) ) {
			$this->setVacantUnitBillAlertAmount( $arrmixValues['vacant_unit_bill_alert_amount'] );
		}
		if( true == isset( $arrmixValues['utility_account_id'] ) ) {
			$this->setUtilityAccountId( $arrmixValues['utility_account_id'] );
		}
		if( true == isset( $arrmixValues['allocation_count'] ) ) {
			$this->setAllocationCount( $arrmixValues['allocation_count'] );
		}
		if( true == isset( $arrmixValues['utility_batch_id'] ) ) {
			$this->setUtilityBatchId( $arrmixValues['utility_batch_id'] );
		}
		if( true == isset( $arrmixValues['utility_distribution_id'] ) ) {
			$this->setUtilityDistributionId( $arrmixValues['utility_distribution_id'] );
		}
		if( true == isset( $arrmixValues['utility_type_id'] ) ) {
			$this->setUtilityTypeId( $arrmixValues['utility_type_id'] );
		}
		if( true == isset( $arrmixValues['utility_consumption_type_id'] ) ) {
			$this->setUtilityConsumptionTypeId( $arrmixValues['utility_consumption_type_id'] );
		}
		if( true == isset( $arrmixValues['has_high_usage'] ) ) {
			$this->setHasHighUsage( $arrmixValues['has_high_usage'] );
		}
		if( true == isset( $arrmixValues['is_associated_with_batch'] ) ) {
			$this->setIsAssociatedWithBatch( $arrmixValues['is_associated_with_batch'] );
		}
		if( true == isset( $arrmixValues['bill_audit_count'] ) ) {
			$this->setBillAuditCount( $arrmixValues['bill_audit_count'] );
		}
		if( true == isset( $arrmixValues['saving_amount'] ) ) {
			$this->setBillAuditSavingAmount( $arrmixValues['saving_amount'] );
		}
		if( true == isset( $arrmixValues['uem_bill_status_type_id'] ) ) {
			$this->setUemBillStatusTypeId( $arrmixValues['uem_bill_status_type_id'] );
		}
		if( true == isset( $arrmixValues['bill_escalation_count'] ) ) {
			$this->setBillEscalationCount( $arrmixValues['bill_escalation_count'] );
		}
		if( true == isset( $arrmixValues['escalation_note'] ) ) {
			$this->setEscalationNote( $arrmixValues['escalation_note'] );
		}

		if( true == isset( $arrmixValues['meter_start_read'] ) ) {
			$this->setMeterStartRead( $arrmixValues['meter_start_read'] );
		}
		if( true == isset( $arrmixValues['meter_end_read'] ) ) {
			$this->setMeterEndRead( $arrmixValues['meter_end_read'] );
		}
		if( true == isset( $arrmixValues['consumption_units'] ) ) {
			$this->setConsumptionUnits( $arrmixValues['consumption_units'] );
		}
		if( true == isset( $arrmixValues['consumption_amount'] ) ) {
			$this->setUnitConsumptionAmount( $arrmixValues['consumption_amount'] );
		}
		if( true == isset( $arrmixValues['move_out_date'] ) ) {
			$this->setMoveOutDate( $arrmixValues['move_out_date'] );
		}
		if( true == isset( $arrmixValues['property_unit_id'] ) ) {
			$this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		}
		if( true == isset( $arrmixValues['vcm_amount'] ) ) {
			$this->setVcmAmount( $arrmixValues['vcm_amount'] );
		}

		if( true == isset( $arrmixValues['is_vcr_verified'] ) ) {
			$this->setIsVcrVerified( $arrmixValues['is_vcr_verified'] );
		}

		if( true == isset( $arrmixValues['property_building_ids'] ) ) {
			$this->setPropertyBuildingIds( $arrmixValues['property_building_ids'] );
		}

		if( true == isset( $arrmixValues['fdg_data_log_type_id'] ) ) {
			$this->setFdgDataLogTypeId( $arrmixValues['fdg_data_log_type_id'] );
		}

		if( true == isset( $arrmixValues['commodity_ids'] ) ) {
			$this->setCommodityIds( $arrmixValues['commodity_ids'] );
		}
	}

	public function setPropertyName( $strPropertyName ) {
    	$this->m_strPropertyName = $strPropertyName;
    }

    public function setUtilityTypeName( $strUtilityTypeName ) {
    	$this->m_strUtilityTypeName = $strUtilityTypeName;
    }

    public function setPropertyUtilityTypeName( $strPropertyUtilityTypeName ) {
    	$this->m_strPropertyUtilityTypeName = $strPropertyUtilityTypeName;
    }

    public function setUtilityBillReceiptTypeName( $strUtilityBillReceiptTypeName ) {
    	$this->m_strUtilityBillReceiptTypeName = $strUtilityBillReceiptTypeName;
    }

    public function setUtilityBillPaymentTypeName( $strUtilityBillPaymentTypeName ) {
    	$this->m_strUtilityBillPaymentTypeName = $strUtilityBillPaymentTypeName;
    }

    public function setUtilityBillAccountName( $strUtilityBillAccountName ) {
    	$this->m_strUtilityBillAccountName = $strUtilityBillAccountName;
    }

    public function setUtilityConsumptionTypeName( $strUtilityConsumptionTypeName ) {
    	$this->m_strUtilityConsumptionTypeName = $strUtilityConsumptionTypeName;
    }

    public function setUtilityDocument( $objUtilityDocument ) {
    	$this->m_objUtilityDocument = $objUtilityDocument;
    }

   	public function setUtilityBillDocument( $objUtilityBillDocument ) {
    	$this->m_objUtilityBillDocument = $objUtilityBillDocument;
    }

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->m_intPropertyUtilityTypeId = $intPropertyUtilityTypeId;
	}

	public function setMissingUtilityBillCount( $intMissingUtilityBillCount ) {
		$this->m_intMissingUtilityBillCount = $intMissingUtilityBillCount;
	}

	public function setPropertyUtilityTypeIds( $arrintPropertyUtilityTypeIds ) {
		$this->m_arrintPropertyUtilityTypeIds = $arrintPropertyUtilityTypeIds;
	}

	public function setUtilityBillAmount( $intUtilityBillAmount ) {
		$this->m_intUtilityBillAmount = $intUtilityBillAmount;
	}

	public function setUtilityDocumentFilePath( $strUtilityDocumentFilePath ) {
		if( false == is_null( $this->getCid() ) ) {
			$this->m_strUtilityDocumentFilePath = getMountsPath( $this->getCid(), PATH_MOUNTS_UTILITY_DOCUMENTS ) . $strUtilityDocumentFilePath;
		} else {
			$this->m_strUtilityDocumentFilePath = PATH_MOUNTS_GLOBAL_UTILITY_DOCUMENTS . $strUtilityDocumentFilePath;
		}
	}

	public function setUtilityDocumentFileName( $strUtilityDocumentFileName ) {
		$this->m_strUtilityDocumentFileName = $strUtilityDocumentFileName;
	}

 	public function setUtilityBatchStepId( $intUtilityBatchStepId ) {
        $this->m_intUtilityBatchStepId = $intUtilityBatchStepId;
    }

 	public function setBatchDateTime( $strBatchDateTime ) {
        $this->m_strBatchDateTime = $strBatchDateTime;
    }

	public function setRequestDatetime( $strRequestDatetime ) {
        $this->m_strRequestDatetime = CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true );
    }

	public function setVcrUtilityBillId( $intVcrUtilityBillId ) {
    	 $this->m_intVcrUtilityBillId = $intVcrUtilityBillId;
    }

    public function setVacantUnitBillAlertAmount( $fltVacantUnitBillAlertAmount ) {
    	 $this->m_fltVacantUnitBillAlertAmount = $fltVacantUnitBillAlertAmount;
    }

    public function setUtilityAccountId( $intUtilityAccountId ) {
    	$this->m_intUtilityAccountId = $intUtilityAccountId;
    }

    public function setAllocationCount( $intAllocationCount ) {
    	$this->m_intAllocationCount = $intAllocationCount;
    }

    public function setUtilityBatchId( $intUtilityBatchId ) {
    	$this->m_intUtilityBatchId = $intUtilityBatchId;
    }

    public function setUtilityDistributionId( $intUtilityDistributionId ) {
    	$this->m_intUtilityDistributionId = $intUtilityDistributionId;
    }

	public function setStateCode( $strStateCode ) {
        $this->m_strStateCode = $strStateCode;
    }

	public function setPostalCode( $strPostalCode ) {
        $this->m_strPostalCode = $strPostalCode;
    }

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->m_intUtilityTypeId = $intUtilityTypeId;
	}

	public function setUtilityConsumptionTypeId( $intUtilityConsumptionTypeId ) {
		$this->m_intUtilityConsumptionTypeId = $intUtilityConsumptionTypeId;
	}

	public function setHasHighUsage( $intHasHighUsage ) {
    	$this->m_intHasHighUsage = CStrings::strToBool( $intHasHighUsage );
    }

    public function setUtilityBillAllocations( $arrobjUtilityBillAloocations ) {
    	$this->m_arrobjUtilityBillAllocations = $arrobjUtilityBillAloocations;
    }

    public function setUtilityBillCharges( $arrobjUtilityBillCharges ) {
    	$this->m_arrobjUtilityBillCharges = $arrobjUtilityBillCharges;
    }

	public function setIsAssociatedWithBatch( $boolIsAssociatedWithBatch ) {
		$this->m_boolIsAssociatedWithBatch = CStrings::strToBool( $boolIsAssociatedWithBatch );
	}

	public function setIsVcrVerified( $boolIsAssociatedWithBatch ) {
		$this->m_boolIsVcrVerified = CStrings::strToBool( $boolIsAssociatedWithBatch );
	}

	public function setUpdatedBillAmount( $fltUpdatedBillAmount ) {
		$this->m_fltUpdatedBillAmount = $fltUpdatedBillAmount;
	}

	public function setDocumentPageCount( $intDocumentPageCount ) {
		$this->m_intDocumentPageCount = $intDocumentPageCount;
	}

	public function setIsSplitUtilityBill( $boolIsSplitUtilityBill ) {
		$this->m_boolIsSplitUtilityBill = $boolIsSplitUtilityBill;
	}

	public function setBillAuditCount( $intBillAuditCount ) {
		$this->m_intBillAuditCount = $intBillAuditCount;
	}

	public function setBillAuditSavingAmount( $fltSavingAmount ) {
		$this->m_fltBillAuditSavingAmount = $fltSavingAmount;
	}

	public function setUemBillStatusTypeId( $intUemBillStatusTypeId ) {
		$this->m_intUemBillStatusTypeId = $intUemBillStatusTypeId;
	}

	public function setBillEscalationCount( $intBillEscalationCount ) {
		$this->m_intBillEscalationCount = $intBillEscalationCount;
	}

	public function setEscalationNote( $strEscalationNote ) {
		$this->m_strEscalationNote = $strEscalationNote;
	}

	public function setUnitNumber( $intUnitNumber ) {
		$this->m_intUnitNumber = $intUnitNumber;
	}

	public function setUtilityBillUnits( $arrobjUtilityBillUnits ) {
		$this->m_arrobjUtilityBillUnits = $arrobjUtilityBillUnits;
	}

	public function setOldUtilityBillUnits( $arrobjOldUtilityBillUnits ) {
		$this->m_arrobjOldUtilityBillUnits = $arrobjOldUtilityBillUnits;
	}

	public function setUtilityBillMeterReads( $arrobjUtilityBillMeterReads ) {
		$this->m_arrobjUtilityBillMeterReads = $arrobjUtilityBillMeterReads;
	}

	public function setMeterStartRead( $intMeterStartRead ) {
		$this->m_intMeterStartRead = CStrings::strToIntDef( $intMeterStartRead );
	}

	public function setMeterEndRead( $intMeterEndRead ) {
		$this->m_intMeterEndRead = CStrings::strToIntDef( $intMeterEndRead );
	}

	public function setUnitConsumptionAmount( $fltUnitConsumptionAmount ) {
		$this->m_fltUnitConsumptionAmount = CStrings::strToIntDef( $fltUnitConsumptionAmount );
	}

	public function setConsumptionUnits( $intConsumptionUnits ) {
		$this->m_intConsumptionUnits = CStrings::strToIntDef( $intConsumptionUnits );
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->m_strMoveOutDate = CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true );
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = CStrings::strToIntDef( $intPropertyUnitId );
	}

	public function setVcmAmount( $fltVcmAmount ) {
		$this->m_fltVcmAmount = CStrings::strToFloatDef( $fltVcmAmount, NULL, false, 2 );
	}

	public function setIsUemProperty( $boolIsUemProperty ) {
		$this->m_boolIsUemProperty = CStrings::strToBool( $boolIsUemProperty );
	}

	public function setAdminDatabase( $objAdminDatabase ) {
		$this->m_objAdminDatabase = $objAdminDatabase;
	}

	public function setClientDatabase( $objClientDatabase ) {
		$this->m_objClientDatabase = $objClientDatabase;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setEmailDatabase( $objEmailDatabase ) {
		$this->m_objEmailDatabase = $objEmailDatabase;
	}

	public function setPropertyBuildingIds( $arrintPropertyBuildingIds ) {
		$this->m_arrintPropertyBuildingIds = $arrintPropertyBuildingIds;
	}

	public function setFdgDataLogTypeId( $intFdgDataLogTypeId ) {
		$this->m_intFdgDataLogTypeId = $intFdgDataLogTypeId;
	}

	public function setSummaryImplementationUtilityBillId( $intSummaryImplementationUtilityBillId ) {
		$this->set( 'm_intSummaryImplementationUtilityBillId', CStrings::strToIntDef( $intSummaryImplementationUtilityBillId, NULL, false ) );
	}

	public function setCommodityIds( $arrintCommodityIds ) {
		$this->m_arrintCommodityIds = $arrintCommodityIds;
	}

	/**
	 *Get Methods
	 */

    public function getPropertyName() {
    	return $this->m_strPropertyName;
    }

    public function getUtilityTypeName() {
    	return $this->m_strUtilityTypeName;
    }

	public function getPropertyUtilityTypeName() {
		return $this->m_strPropertyUtilityTypeName;
	}

    public function getUtilityBillReceiptTypeName() {
    	return $this->m_strUtilityBillReceiptTypeName;
    }

    public function getUtilityBillPaymentTypeName() {
    	return $this->m_strUtilityBillPaymentTypeName;
    }

    public function getUtilityBillAccountName() {
    	return $this->m_strUtilityBillAccountName;
    }

    public function getUtilityConsumptionTypeName() {
    	return $this->m_strUtilityConsumptionTypeName;
    }

	public function getUtilityDocument() {
    	return $this->m_objUtilityDocument;
    }

    public function getUtilityBillDocument() {
    	return $this->m_objUtilityBillDocument;
    }

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function getMissingUtilityBillCount() {
		return $this->m_intMissingUtilityBillCount;
	}

	public function getPropertyUtilityTypeIds() {
		return $this->m_arrintPropertyUtilityTypeIds;
	}

	public function getUtilityBillAmount() {
		return $this->m_intUtilityBillAmount;
	}

	public function getUtilityDocumentFilePath() {
		return $this->m_strUtilityDocumentFilePath;
	}

	public function getUtilityDocumentFileName() {
		return $this->m_strUtilityDocumentFileName;
	}

	public function getUtilityBatchStepId() {
        return $this->m_intUtilityBatchStepId;
    }

	public function getBatchDateTime() {
        return $this->m_strBatchDateTime;
    }

	public function getRequestDatetime() {
        return $this->m_strRequestDatetime;
    }

    public function getVcrUtilityBillId() {
    	return $this->m_intVcrUtilityBillId;
    }

	public function getVacantUnitBillAlertAmount() {
    	return $this->m_fltVacantUnitBillAlertAmount;
    }

	public function getUtilityAccountId() {
    	return $this->m_intUtilityAccountId;
    }

	public function getAllocationCount() {
    	return $this->m_intAllocationCount;
    }

	public function getUtilityBatchId() {
    	return	$this->m_intUtilityBatchId;
    }

	public function getUtilityDistributionId() {
    	return $this->m_intUtilityDistributionId;
    }

	public function getStateCode() {
        return $this->m_strStateCode;
    }

	public function getPostalCode() {
        return $this->m_strPostalCode;
    }

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function getUtilityConsumptionTypeId() {
		return $this->m_intUtilityConsumptionTypeId;
	}

    public function getHasHighUsage() {
    	return $this->m_intHasHighUsage;
    }

    public function getUtilityBillAllocations() {
    	return $this->m_arrobjUtilityBillAllocations;
    }

    public function getUtilityBillCharges() {
    	return $this->m_arrobjUtilityBillCharges;
    }

	public function getIsAssociatedWithBatch() {
		return $this->m_boolIsAssociatedWithBatch;
	}

	public function getIsVcrVerified() {
		return $this->m_boolIsVcrVerified;
	}

	public function getUpdatedBillAmount() {
		return $this->m_fltUpdatedBillAmount;
	}

	public function getDocumentPageCount() {
		return $this->m_intDocumentPageCount;
	}

	public function getIsSplitUtilityBill() {
		return $this->m_boolIsSplitUtilityBill;
	}

	public function getBillAuditCount() {
		return $this->m_intBillAuditCount;
	}

	public function getBillAuditSavingAmount() {
		return $this->m_fltBillAuditSavingAmount;
	}

	public function getUemBillStatusTypeId() {
		return $this->m_intUemBillStatusTypeId;
	}

	public function getBillEscalationCount() {
		return $this->m_intBillEscalationCount;
	}

	public function getEscalationNote() {
		return $this->m_strEscalationNote;
	}

	public function getUnitNumber() {
		return $this->m_intUnitNumber;
	}

	public function getUtilityBillUnits() {
		return $this->m_arrobjUtilityBillUnits;
	}

	public function getOldUtilityBillUnits() {
		return $this->m_arrobjOldUtilityBillUnits;
	}

	public function getUtilityBillMeterReads() {
		return $this->m_arrobjUtilityBillMeterReads;
	}

	public function getMeterStartRead() {
		return $this->m_intMeterStartRead;
	}

	public function getMeterEndRead() {
		return $this->m_intMeterEndRead;
	}

	public function getUnitConsumptionAmount() {
		return $this->m_fltUnitConsumptionAmount;
	}

	public function getConsumptionUnits() {
		return $this->m_intConsumptionUnits;
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getVcmAmount() {
		return $this->m_fltVcmAmount;
	}

	public function getIsUemProperty() {
		return $this->m_boolIsUemProperty;
	}

	public function getAdminDatabase() {
		return $this->m_objAdminDatabase;
	}

	public function getClientDatabase() {
		return $this->m_objClientDatabase;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getEmailDatabase() {
    	return $this->m_objEmailDatabase;
	}

	public function getPropertyBuildingIds() {
		return $this->m_arrintPropertyBuildingIds;
	}

	public function getFdgDataLogTypeId() {
		return $this->m_intFdgDataLogTypeId;
	}

	public function getSummaryImplementationUtilityBillId() {
		return $this->m_intSummaryImplementationUtilityBillId;
	}

	public function setBillProcessingDetails( $jsonBillProcessingDetails ) {
		if( true == valObj( $jsonBillProcessingDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonBillProcessingDetails', $jsonBillProcessingDetails );
		} elseif( true == valJsonString( $jsonBillProcessingDetails ) ) {
			$this->set( 'm_jsonBillProcessingDetails', CStrings::strToJson( $jsonBillProcessingDetails ) );
		} else {
			$this->set( 'm_jsonBillProcessingDetails', NULL );
		}
		unset( $this->m_strBillProcessingDetails );
	}

	public function getBillProcessingDetails() {
		if( true == isset( $this->m_strBillProcessingDetails ) ) {
			$this->m_jsonBillProcessingDetails = CStrings::strToJson( $this->m_strBillProcessingDetails );
			unset( $this->m_strBillProcessingDetails );
		}
		return $this->m_jsonBillProcessingDetails;
	}

	public function getCommodityIds() {
		return $this->m_arrintCommodityIds;
	}

	/**
	 * Validate Methods
	 */

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property name is required.' ) );
        }
        return $boolIsValid;
    }

    public function valPropertyBuildingId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyBuildingId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_building_id', 'Property building is required.' ) );
        }
        return $boolIsValid;
    }

    public function valUtilityBillAccountId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityBillAccountId() ) && CPsProduct::INVOICE_PROCESSING != $this->getPsProductId() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_account_id', 'Utility bill account is required.' ) );
        }
        return $boolIsValid;
    }

    public function valUtilityConsumptionTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityConsumptionTypeId() ) && CPsProduct::INVOICE_PROCESSING != $this->getPsProductId() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_consumption_type_id', 'Utility consumption type is required.' ) );
        }
        return $boolIsValid;
    }

    public function valEstimatedUtilityBillId() {
        $boolIsValid = true;

        if( true == is_null( $this->getEstimatedUtilityBillId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_utility_bill_id', 'Estimated utility bill not choosen.' ) );
        }
        return $boolIsValid;
    }

    public function valTransactionId() {
        $boolIsValid = true;

 	   	if( true == is_null( $this->getTransactionId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_id', 'Transaction is required.' ) );
        }
        return $boolIsValid;
    }

    public function valBillAmount( $boolIsFromFdg = false, $fltCurrentAmount = NULL ) {
        $boolIsValid = true;

        if( true == $boolIsFromFdg ) {
	        $fltBillCurrentAmount = round( ( float ) $this->getCurrentAmount(), 2 );
			if( true == is_null( $this->getSummaryUtilityBillId() ) && $fltBillCurrentAmount != $fltCurrentAmount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_amount', 'Invalid bill amount.' ) );
			}

			return $boolIsValid;
        }

        if( false == is_null( $this->getCurrentAmount() ) && 0 == $this->getCurrentAmount() && CUtilityBillType::NO_CHARGE_BILL != $this->getUtilityBillTypeId() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_amount', 'Invalid bill amount.' ) );
        }

        return $boolIsValid;
    }

    public function valBillNumber( $objDatabase, $strBillNumber = NULL ) {
    	$boolIsValid = true;

	    if( true == valStr( $strBillNumber ) && true == is_null( $this->getBillNumber() ) ) {
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_date', 'Failed to set the bill number.' ) );
		    return false;
	    }

    	if( false == is_null( $this->getBillNumber() ) ) {

    		$intUtilityBillsCount = \Psi\Eos\Utilities\CUtilityBills::createService()->fetchUtilityBillCount( ' WHERE id <> ' . ( int ) $this->getId() . ' AND property_id = ' . ( int ) $this->getPropertyId() . ' AND utility_bill_account_id = ' . ( int ) $this->getUtilityBillAccountId() . ' AND lower( bill_number ) LIKE \'%' . \Psi\CStringService::singleton()->strtolower( $this->getBillNumber() ) . '%\' AND deleted_on IS NULL and deleted_by IS NULL ', $objDatabase );
    		if( 0 < $intUtilityBillsCount ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_number', 'You have already processed bill that contains this bill number. ' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valBillDate( $strBillDate = NULL ) {
        $boolIsValid = true;

        if( false == is_null( $this->getBillDate() ) && 1 != strlen( CValidation::checkDate( $this->getBillDate() ) ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_date', 'Invoice date is invalid and must be in mm/dd/yyyy format.' ) );
        }

        if( true == valStr( $strBillDate ) && true == is_null( $this->getBillDate() ) ) {
	        $boolIsValid &= false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_date', 'Failed to set the bill date.' ) );
        }

        return $boolIsValid;
    }

    public function valDueDate( $strDueDate = NULL ) {
        $boolIsValid = true;

        if( false == is_null( $this->getDueDate() ) && 1 != strlen( CValidation::checkDate( $this->getDueDate() ) ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', 'Due date is invalid and must be in mm/dd/yyyy format.' ) );
        }

	    if( true == valStr( $strDueDate ) && true == is_null( $this->getDueDate() ) ) {
		    $boolIsValid &= false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', 'Failed to set the due date.' ) );
	    }

        return $boolIsValid;
    }

    public function valBillDueDate() {
	    $boolIsValid = true;

	    if( true == is_null( $this->getDueDate() ) && true == is_null( $this->getBillDate() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', 'Bill Date or Due Date is required.' ) );
	    }

	    return $boolIsValid;
    }

    public function valBillingDates( $boolReturnOnly = false ) {

    	$boolIsValid = true;

    	if( true == is_null( $this->getBillingStartDate() ) || true == is_null( $this->getBillingEndDate() ) ) {
    		$boolIsValid &= false;
    		if( false == $boolReturnOnly ) {
   				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Billing dates are invalid and must be in mm/dd/yyyy format.' ) );
    		}
   		}

    	if( true == $boolIsValid && ( 1 != strlen( CValidation::checkDate( $this->getBillingStartDate() ) ) || 1 != strlen( CValidation::checkDate( $this->getBillingEndDate() ) ) ) ) {
    		$boolIsValid &= false;
    		if( false == $boolReturnOnly ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Invalid date format for billing start date or billing end date.' ) );
    		}
    	}

    	if( true == $boolIsValid && 0 > $this->daysDifference( $this->getBillingEndDate(), $this->getBillingStartDate() ) ) {
    			$boolIsValid &= false;
    			if( false == $boolReturnOnly ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Invalid billing start date or billing end date.' ) );
    			}
    	}

    	return $boolIsValid;

    }

    public function valStartDate( $boolReturnOnly = false ) {
        $boolIsValid = true;

        if( true == is_null( $this->getStartDate() ) || 0 >= strtotime( $this->getStartDate() ) || 1 != strlen( CValidation::checkDate( $this->getStartDate() ) ) ) {
            $boolIsValid = false;
            if( false == $boolReturnOnly ) {
	            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Service start date is invalid and must be in mm/dd/yyyy format.' ) );
            }
        }

        return $boolIsValid;
    }

    public function valEndDate( $boolReturnOnly = false ) {
        $boolIsValid = true;

        if( true == is_null( $this->getEndDate() ) || 0 >= strtotime( $this->getEndDate() ) || 1 != strlen( CValidation::checkDate( $this->getEndDate() ) ) ) {
            $boolIsValid = false;
            if( false == $boolReturnOnly ) {
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Service end date is invalid and must be in mm/dd/yyyy format.' ) );
            }
        }

        return $boolIsValid;
    }

    public function valMeterStartRead() {
        $boolIsValid = true;

        if( false == is_null( $this->getMeterStartRead() ) && false == preg_match( '/^[0-9]*$/', $this->getMeterStartRead() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_start_read', 'Meter start reading is invalid.' ) );
        }

        return $boolIsValid;
    }

    public function valTotalAmount( $boolIsFromFdg = false, $fltTotalAmount = NULL ) {
    	$boolIsValid = true;

	    if( true == $boolIsFromFdg ) {

		    if( true == is_null( $this->getSummaryUtilityBillId() ) && ( float ) $this->getTotalAmount() != ( float ) $fltTotalAmount ) {
			    $boolIsValid = false;
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_amount', 'Invalid total amount.' ) );
		    }

		    return $boolIsValid;
	    }

    	if( true == is_null( $this->getTotalAmount() ) ) {
    		$boolIsValid &= false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Bill_amount_required', 'Bill total amount required.' ) );

    	}
    	return $boolIsValid;

    }

	public function valUtilityBillByPropertyIdByUtilityTypeIdByBillDate( $objAdminDatabase ) {

    	$boolIsValid = true;

    	if( false == is_null( $this->getPropertyId() ) && false == is_null( $this->getPropertyUtilityTypeId() ) && false == is_null( $this->getBillDate() ) ) {
			$intUtilityBillCount = \Psi\Eos\Utilities\CUtilityBills::createService()->fetchUtilityBillsCountByPropertyIdByPropertyUtilityTypeIdByBillDate( $this->getPropertyId(), $this->getPropertyUtilityTypeId(), $this->getBillDate(), $objAdminDatabase );
			if( 0 < $intUtilityBillCount ) {
				$boolIsValid = false;
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_utility_type_id', 'Utility bill already exist for same property utility type and invoice month.' ) );
			}
   		}

    	return $boolIsValid;
	}

	public function valMeterReading() {
   		$boolIsValid = true;

   		if( false == is_null( $this->getMeterStartRead() ) && false == is_null( $this->getMeterEndRead() ) ) {
   			if( $this->getMeterEndRead() < $this->getMeterStartRead() ) {
   				$boolIsValid = false;
   				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_start_read', 'Start meter read or End meter read is invalid.' ) );
   			}
   		}
   		return $boolIsValid;
	}

    public function valStartDateAndEndDate( $boolReturnOnly = false ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getStartDate() ) && false == is_null( $this->getEndDate() ) ) {

    		if( 1 != strlen( CValidation::checkDate( $this->getStartDate() ) ) || 1 != strlen( CValidation::checkDate( $this->getEndDate() ) ) ) {
    			$boolIsValid &= false;
    		}

    		if( true == $boolIsValid && 0 > $this->daysDifference( $this->getEndDate(), $this->getStartDate() ) ) {
    			$boolIsValid = false;
    			if( false == $boolReturnOnly ) {
	    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Invalid service start date or service end date.' ) );
    			}
    		}
    	}
    	return $boolIsValid;
    }

    public function valBillDateAndDueDate() {
    	$boolIsValid = true;

    	if( ( false == is_null( $this->getBillDate() ) && 1 != strlen( CValidation::checkDate( $this->getBillDate() ) ) ) || ( false == is_null( $this->getDueDate() ) && 1 != strlen( CValidation::checkDate( $this->getDueDate() ) ) ) ) {
    		$boolIsValid &= false;
    	}

    	if( true == $boolIsValid && false == is_null( $this->getBillDate() ) && false == is_null( $this->getDueDate() ) ) {
    		if( 0 > $this->daysDifference( $this->getDueDate(), $this->getBillDate() ) ) {
    			$boolIsValid &= false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', 'Invalid bill date or due date.' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valDependencies( $objDatabase ) {

    	$boolIsValid = true;

    	if( false == is_null( \Psi\Eos\Utilities\CUtilityBills::createService()->fetchBatchedUtilityBillsById( $this->getId(), $objDatabase ) ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '0', 'Utility bill ' . $this->getId() . ' is associated with the batch so cannot delete.' ) );
    	}
    	return $boolIsValid;
    }

    public function valPropertyUtilityTypeId() {

    	$boolIsValid = true;
    	if( false == is_numeric( $this->getPropertyUtilityTypeId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_utility_type_id', 'Property utility type is required.' ) );
    	}
    	return $boolIsValid;

    }

	public function valPropertyUnitId() {

    	$boolIsValid = true;
    	if( false == is_numeric( $this->getPropertyUnitId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', 'Property unit is required.' ) );
    	}
    	return $boolIsValid;

    }

    public function valPreviousBalance( $fltPreviousBalance = NULL ) {

	    $boolIsValid = true;

    	if( false == is_null( $fltPreviousBalance ) && true == is_null( $this->getBalanceForward() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_balance_amount', 'failed to set the Balance Forward.' ) );
	    }

	    return $boolIsValid;
    }

	/**
	 * Other Methods
	 */

    public function validate( $strAction, $objDatabase, $boolSkipValidation = false, $boolHasNonZeroUtilityBillCharges = false, $arrmixReceivedData = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:

            	if( false == $boolSkipValidation || CUtilityBillType::VACANT_UNIT == $this->getUtilityBillTypeId() ) {
            		$boolIsValid &= $this->valUtilityBillAccountId();
	            }

            	if( VALIDATE_UPDATE == $strAction ) {
            		$boolIsValid &= $this->valUtilityBillAccountId();

		            if( CUtilityBillType::ESTIMATE <> $this->getUtilityBillTypeId() ) {
			            $boolIsValid &= $this->valBillDueDate();
		            }

		            if( false == valArr( $this->m_arrintPropertyUtilityTypeIds ) ) {
			            $boolIsValid &= $this->valBillAmount();
		            }
	            }

            	if( false == $boolSkipValidation && ( CUtilityBillType::ESTIMATE != $this->getUtilityBillTypeId() && CUtilityBillType::DIRECT_METERED != $this->getUtilityBillTypeId() && CUtilityBillType::ESTIMATED_DIRECT_METERED != $this->getUtilityBillTypeId() ) ) {
            		$boolIsValid &= $this->valUtilityConsumptionTypeId();
            	}

            	if( false == $boolHasNonZeroUtilityBillCharges ) {
            		$boolIsValid &= $this->valBillAmount();
            	}

            	$boolIsValid &= $this->valBillDate();
            	$boolIsValid &= $this->valDueDate();
            	$boolIsValid &= $this->valStartDateAndEndDate();
            	$boolIsValid &= $this->valBillDateAndDueDate();
           		break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valDependencies( $objDatabase );
                break;

	        case self::VALIDATE_AUTO_PROCESSING_DETAILS:
	        	if( true == is_null( $this->getProcessedAgainBy() ) && true == is_null( $this->getProcessedAgainOn() ) ) {
	        		return true;
		        }
	        	$objFdgLibrary = new CFDGLibrary();
	            $fltCurrentAmount = $objFdgLibrary->calculateParentBillCurrentCharge( $arrmixReceivedData );
	        	$boolIsValid &= $this->valBillAmount( true, $fltCurrentAmount );
	        	$boolIsValid &= $this->valTotalAmount( true, getArrayElementByKey( 'TotalDue', $arrmixReceivedData ) );
		        $boolIsValid &= $this->valBillDate( getArrayElementByKey( 'InvoiceDate', $arrmixReceivedData ) );
		        $boolIsValid &= $this->valDueDate( getArrayElementByKey( 'DueDate', $arrmixReceivedData ) );

		        if( true == is_null( $this->getSummaryUtilityBillId() ) ) {
			        $boolIsValid &= $this->valPreviousBalance( getArrayElementByKey( 'BalanceForward', $arrmixReceivedData ) );
		        }
		        break;

            default:
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
	    // Set OFF to is_ignore_queue flag on utility bill if all escalation are completed for bill and the flag is ON.
	    if( true == $this->getIsQueueIgnored() ) {
			$intUnresolvedBillEscalationCount = \Psi\Eos\Utilities\CBillEscalations::createService()->fetchCustomUnresolvedBillEscalationCountByUtilityBillId( $this->getId(), $objDatabase );

			if( 0 == $intUnresolvedBillEscalationCount ) {
				$this->setIsQueueIgnored( false );
			}
		}

		// Add event if bill is marked as deleted.
		if( false == is_null( $this->getDeletedBy() ) && false == is_null( $this->getDeletedOn() ) ) {
			$intBillEventReferenceId = CBillEventReference::PROCESSING;

			if( true == is_null( $this->getProcessedBy() ) && true == is_null( $this->getProcessedOn() ) ) {
				$intBillEventReferenceId = CBillEventReference::ASSOCIATION;
			}

			$objBillEventLibrary = new CBillEventLibrary();

			$objBillEvent = $objBillEventLibrary->createBillEvent( CBillEventType::DELETED, CBillEventSubType::DELETED, $intBillEventReferenceId );
			$objBillEvent->setUtilityBillId( $this->getId() );
			$objBillEvent->setNewData( json_encode( [ 'Reason' => $this->getDeletionReason() ] ) );

			if( false === $objBillEventLibrary->insertBillEvent( $objBillEvent, $intCurrentUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $objBillEventLibrary->getErrorMsgs() );
				return false;
			}
		}

	    return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ); // TODO: Change the autogenerated stub
    }

	/**
     * Other Methods
     */

    public function daysDifference( $intEndDate, $intBeginDate ) {
	    $intBeginDate = date( 'm/d/Y', strtotime( $intBeginDate ) );
	    $intEndDate = date( 'm/d/Y', strtotime( $intEndDate ) );

		$arrintDateParts1 = explode( '/', $intBeginDate );
		$arrintDateParts2 = explode( '/', $intEndDate );
		$intStartDate  = gregoriantojd( $arrintDateParts1[0], $arrintDateParts1[1], $arrintDateParts1[2] );
		$intEndDate	  = gregoriantojd( $arrintDateParts2[0], $arrintDateParts2[1], $arrintDateParts2[2] );

		return $intEndDate - $intStartDate;
	}

	public function fetchUtilityBillAccount( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchUtilityBillAccountsByPropertyId( $this->getPropertyId(), $objDatabase );
	}

	public function fetchPropertyUtilityType( $objDatabase ) {
		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchNoBudgetPropertyUtilityTypesByPropertyId( $this->getPropertyId(), $objDatabase, $this->getIsUemProperty() );
	}

    public function fetchUtilityBillCharges( $objDatabase ) {
    	return \Psi\Eos\Utilities\CUtilityBillCharges::createService()->fetchUtilityBillChargesByUtilityBillId( $this->getId(), $objDatabase );
    }

	public function fetchPropertyUnit( $objClientDatabase ) {
    	return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objClientDatabase );
    }

    public function fetchUtilityBillAllocations( $objUtilitiesDatabase ) {
    	return \Psi\Eos\Utilities\CUtilityBillAllocations::createService()->fetchUtilityBillAllocationsByUtilityBillId( $this->getId(), $objUtilitiesDatabase );
    }

	public function fetchBatchedAllocation( $objUtilitiesDatabase ) {
    	return \Psi\Eos\Utilities\CUtilityBillAllocations::createService()->fetchBatchedUtilityBillAllocationByUtilityBillId( $this->getId(), $objUtilitiesDatabase );
    }

    public function fetchUtilityInvoice( $objUtilityDatabase ) {
    	return \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchUtilityInvoiceByUtilityBillId( $this->getId(), $objUtilityDatabase );
    }

    public function fetchBillAudits( $objUtilityDatabase ) {
    	return \Psi\Eos\Utilities\CBillAudits::createService()->fetchBillAuditsByUtilityBillId( $this->getId(), $objUtilityDatabase );
    }

    public function fetchBillAuditByBillAuditTypeId( $intBillAuditTypeId, $objUtilityDatabase ) {
    	return \Psi\Eos\Utilities\CBillAudits::createService()->fetchBillAuditByUtilityBillIdByBillAuditTypeId( $this->getId(), $intBillAuditTypeId, $objUtilityDatabase );
    }

    public function fetchBillAuditByBillAuditTypeIdByUtilityBillChargeId( $intBillAuditTypeId, $intUtilityBillChargeId, $objUtilityDatabase ) {
	    return \Psi\Eos\Utilities\CBillAudits::createService()->fetchBillAuditByUtilityBillIdByBillAuditTypeIdByUtilityBillChargeId( $this->getId(), $intBillAuditTypeId, $intUtilityBillChargeId, $objUtilityDatabase );
    }

	public function fetchBillAuditByBillAuditTypeIdByBillAuditStatusTypeId( $intBillAuditTypeId, $intBillAuditStatusTypeId, $objDatabase ) {
		return \Psi\Eos\Utilities\CBillAudits::createService()->fetchBillAuditByUtilityBillIdByBillAuditTypeIdByBillAuditStatusTypeId( $this->getId(), $intBillAuditTypeId, $intBillAuditStatusTypeId, $objDatabase );
	}

	public function fetchUtilityBillAuditsByUtilityBillAuditTypeIdByUtilityBillAuditStatusTypeId( $intUtilityBillAuditTypeId, $intUtilityBillAuditStatusTypeId, $objDatabase ) {
		return ( array ) \Psi\Eos\Utilities\CUtilityBillAudits::createService()->fetchUtilityBillAuditByUtilityBillIdByUtilityBillAuditTypeIdByUtilityBillAuditStatusTypeId( $this->getId(), $intUtilityBillAuditTypeId, $intUtilityBillAuditStatusTypeId, $objDatabase );
	}

	public function fetchUtilityDocument( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityDocuments::createService()->fetchUtilityDocumentById( $this->getUtilityDocumentId(), $objDatabase );
	}

	public function fetchUtilityBillUnits( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBillUnits::createService()->fetchUtilityBillUnitsByUtilityBillId( $this->getId(), $objDatabase );
	}

	/**
	 * Create Functions
	 */

	public function createUtilityDocument() {

		$objUtilityDocument = new CUtilityDocument();

		$objUtilityDocument->setFileExtensionId( CFileExtension::APPLICATION_PDF );
		$objUtilityDocument->setFilePath( $this->getPropertyId(), CUtilityDocument::TYPE_BILL );
		$objUtilityDocument->setDocumentDatetime( 'NOW()' );
		$objUtilityDocument->setUtilityDocumentTypeId( CUtilityDocumentType::DOCUMENT_TYPE_BILL );

		return $objUtilityDocument;
	}

	public function createVcrUtilityBill() {

		$objVcrUtilityBill = new CVcrUtilityBill();
		$objVcrUtilityBill->setCid( $this->getCid() );
		$objVcrUtilityBill->setPropertyId( $this->getPropertyId() );
		$objVcrUtilityBill->setUtilityBillId( $this->getId() );

		return $objVcrUtilityBill;

	}

	public function createUtilityBillAllocation() {

		$objUtilityBillAllocation = new CUtilityBillAllocation();
		$objUtilityBillAllocation->setCid( $this->getCid() );
		$objUtilityBillAllocation->setPropertyId( $this->getPropertyId() );
		$objUtilityBillAllocation->setUtilityBillId( $this->getId() );
		return $objUtilityBillAllocation;
	}

	public function createBillAudit( $intBillAuditTypeId, $objDatabase ) {

		$objBillAudit = new CBillAudit();
		$objBillAudit->setUtilityBillId( $this->getId() );
		$objBillAudit->setBillAuditTypeId( $intBillAuditTypeId );
		$objBillAudit->setUemBillStatusTypeId( CUemBillStatusType::UNUSED );

		// set last processed bill id as default audit_utility_bill_id
		$objPreviousUtilityBill = \Psi\Eos\Utilities\CUtilityBills::createService()->fetchLastProcessedUtilityBillByUtilityBillIdByUtilityBillAccountIdByPropertyId( $this->getId(), $this->getUtilityBillAccountId(), $this->getPropertyId(), $objDatabase );
		if( true == valObj( $objPreviousUtilityBill, 'CUtilityBill' ) ) {
			$objBillAudit->setAuditUtilityBillId( $objPreviousUtilityBill->getId() );
		}
		return $objBillAudit;

	}

	public function createBillEscalation( $intBillEscalateTypeId, $strEscalationNote ) {

		$objBillEscalation = new CBillEscalation();

		$objBillEscalation->setUtilityBillId( $this->getId() );
		$objBillEscalation->setBillEscalateTypeId( $intBillEscalateTypeId );
		$objBillEscalation->setEscalationDatetime( 'now()' );
		$objBillEscalation->setEscalatedEmailAddress( 'jjones@entrata.com' );

		if( true == valStr( $strEscalationNote ) ) {
			$objBillEscalation->setEscalationNote( $strEscalationNote );
		}

		$this->setIsQueueIgnored( true );

		return $objBillEscalation;
	}

	public function unsetAllErrorMsgs() {
		$this->m_arrobjErrorMsgs = NULL;
	}

	public function fetchApPayeeAccount( $objDatabase ) {
		return CApPayeeAccounts::fetchApPayeeAccountByIdByCid( $this->getApPayeeAccountId(), $this->getCid(), $objDatabase );
	}

	public function createUtilityBillUnit() {

		$objUtilityBillUnit = new CUtilityBillUnit();
		$objUtilityBillUnit->setCid( $this->getCid() );
		$objUtilityBillUnit->setPropertyId( $this->getPropertyId() );
		$objUtilityBillUnit->setUtilityBillId( $this->getId() );

		return $objUtilityBillUnit;

	}

	public function fetchCustomUtilityBillMeterChargeCount( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBills::createService()->fetchCustomUtilityBillMeterChargeCountByUtilityBillAccountId( $this->getUtilityBillAccountId(), $objDatabase );
	}

	public function fetchCustomUtilityBillAccountTemplateMeterDetails( $objDatabse ) {
		return \Psi\Eos\Utilities\CUtilityBills::createService()->fetchCustomUtilityBillAccountTemplateMeterDetailsByUtilityBillAccountId( $this->getUtilityBillAccountId(), $objDatabse );
	}

	/**
	 * Other functions
	 */

	public function loadStandardPoNumbers( $arrobjCompanyPreferences, $objClientDatabase ) {

		$arrstrCompanyPreferences = $arrintStandardApHeaders = [];

		if( true == array_key_exists( CCompanyPreference::ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS, $arrobjCompanyPreferences ) ) {
			$arrstrCompanyPreferences[$arrobjCompanyPreferences[CCompanyPreference::ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS]->getKey()] = $arrobjCompanyPreferences[CCompanyPreference::ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS]->getValue();
		}

		$objApTransactionsFilter = new CApTransactionsFilter();
		$objApTransactionsFilter->setApHeaderSubTypeId( CApHeaderSubType::STANDARD_INVOICE );

		$arrmixStandardPONumbers = ( array ) CApHeaders::fetchAllPoApHeadersByFilterByCid( $objApTransactionsFilter, $this->getCid(), $arrstrCompanyPreferences, $objClientDatabase );

		if( true == valArr( $arrmixStandardPONumbers ) ) {
			foreach( $arrmixStandardPONumbers as $arrmixPONumber ) {

				$arrintStandardApHeaders[$arrmixPONumber['ap_header_id']]['po_ap_header_id']	= $arrmixPONumber['ap_header_id'];
				$arrintStandardApHeaders[$arrmixPONumber['ap_header_id']]['po_number']	= $arrmixPONumber['number'];
				$arrintStandardApHeaders[$arrmixPONumber['ap_header_id']]['ap_payee_account_id']	= $arrmixPONumber['ap_payee_account_id'];
				$arrintStandardApHeaders[$arrmixPONumber['ap_header_id']]['ap_payee_id']	= $arrmixPONumber['ap_payee_id'];
				$arrintStandardApHeaders[$arrmixPONumber['ap_header_id']]['bulk_property_id']	= $arrmixPONumber['bulk_property_id'];
				$arrintStandardApHeaders[$arrmixPONumber['ap_header_id']]['ap_payee_location_id']	= $arrmixPONumber['ap_payee_location_id'];
				$arrintStandardApHeaders[$arrmixPONumber['ap_header_id']]['ap_header_sub_type_id']	= $arrmixPONumber['ap_header_sub_type_id'];
			}
		}
		return $arrintStandardApHeaders;
	}

	public function loadCatalogPoNumbers( $objClientDatabase ) {

		$arrstrCompanyPreferences = $arrintCatalogApHeaders = [];

		$objApTransactionsFilter = new CApTransactionsFilter();
		$objApTransactionsFilter->setApHeaderSubTypeId( CApHeaderSubType::CATALOG_INVOICE );

		$arrmixCatalogPONumbers = ( array ) CApHeaders::fetchAllPoApHeadersByFilterByCid( $objApTransactionsFilter, $this->getCid(), $arrstrCompanyPreferences, $objClientDatabase );

		if( true == valArr( $arrmixCatalogPONumbers ) ) {
			foreach( $arrmixCatalogPONumbers as $arrmixPONumber ) {

				$arrintCatalogApHeaders[$arrmixPONumber['ap_header_id']]['po_ap_header_id']	= $arrmixPONumber['ap_header_id'];
				$arrintCatalogApHeaders[$arrmixPONumber['ap_header_id']]['po_number']	= $arrmixPONumber['number'];
				$arrintCatalogApHeaders[$arrmixPONumber['ap_header_id']]['ap_payee_account_id']	= $arrmixPONumber['ap_payee_account_id'];
				$arrintCatalogApHeaders[$arrmixPONumber['ap_header_id']]['ap_payee_id']	= $arrmixPONumber['ap_payee_id'];
				$arrintCatalogApHeaders[$arrmixPONumber['ap_header_id']]['bulk_property_id']	= $arrmixPONumber['bulk_property_id'];
				$arrintCatalogApHeaders[$arrmixPONumber['ap_header_id']]['ap_payee_location_id']	= $arrmixPONumber['ap_payee_location_id'];
				$arrintCatalogApHeaders[$arrmixPONumber['ap_header_id']]['ap_header_sub_type_id']	= $arrmixPONumber['ap_header_sub_type_id'];
			}
		}

		return $arrintCatalogApHeaders;
	}

	public function loadJobContractPoNumbers( $arrobjCompanyPreferences, $objClientDatabase ) {

		$arrstrCompanyPreferences = $arrintJobApHeaders = [];

		$objApTransactionsFilter = new CApTransactionsFilter();
		$objApTransactionsFilter->setApHeaderSubTypeId( CApHeaderSubType::STANDARD_JOB_INVOICE );

		if( true == array_key_exists( CCompanyPreference::ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS, $arrobjCompanyPreferences ) ) {
			$arrstrCompanyPreferences[$arrobjCompanyPreferences[CCompanyPreference::ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS]->getKey()] = $arrobjCompanyPreferences[CCompanyPreference::ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS]->getValue();
		}

		$arrmixJobPONumbers = ( array ) CApHeaders::fetchAllPoApHeadersByFilterByCid( $objApTransactionsFilter, $this->getCid(), $arrstrCompanyPreferences, $objClientDatabase );

		if( true == valArr( $arrmixJobPONumbers ) ) {
			foreach( $arrmixJobPONumbers as $arrmixPONumber ) {

				$arrintJobApHeaders[$arrmixPONumber['ap_header_id']]['po_ap_header_id']	= $arrmixPONumber['ap_header_id'];
				$arrintJobApHeaders[$arrmixPONumber['ap_header_id']]['po_number']	= $arrmixPONumber['number'];
				$arrintJobApHeaders[$arrmixPONumber['ap_header_id']]['ap_payee_account_id']	= $arrmixPONumber['ap_payee_account_id'];
				$arrintJobApHeaders[$arrmixPONumber['ap_header_id']]['ap_payee_id']	= $arrmixPONumber['ap_payee_id'];
				$arrintJobApHeaders[$arrmixPONumber['ap_header_id']]['bulk_property_id']	= $arrmixPONumber['bulk_property_id'];
				$arrintJobApHeaders[$arrmixPONumber['ap_header_id']]['ap_payee_location_id']	= $arrmixPONumber['ap_payee_location_id'];
				$arrintJobApHeaders[$arrmixPONumber['ap_header_id']]['ap_header_sub_type_id']	= $arrmixPONumber['ap_header_sub_type_id'];
			}
		}

		return $arrintJobApHeaders;
	}

	public function reverseVcrUtilityBills() : bool {

		if( false == $this->reversalRU() ) {
			return false;
		}

		if( false == $this->reversalUEM() ) {
			return false;
		}

		return true;
	}

	public function reversalRU() : bool {

		$arrintUtilityInvoiceIds = $arrintUtilityTransactionIds = $arrintArTransactionIds = $arrintVcrBillIds = $arrobjCreditUtilityTransacionts = $arrintLeaseIds = $arrintTransactionIds = $arrobjServiceFeeUtilityTransaction = [];

		$arrobjVcrUtilityBills = ( array ) \Psi\Eos\Utilities\CVcrUtilityBills::createService()->fetchVcrUtilityBillsByUtilityBillId( $this->getId(), $this->getDatabase() );
		$arrobjClonedUtilityTransactions = ( array ) \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchCustomUtilityTransactionsByVcrBillIds( array_keys( $arrobjVcrUtilityBills ), $this->getDatabase() );

		foreach( $arrobjClonedUtilityTransactions as $objUtilityTransaction ) {
			$arrintUtilityInvoiceIds[]		= $objUtilityTransaction->getUtilityInvoiceId();
			$arrintUtilityTransactionIds[]	= $objUtilityTransaction->getOriginalUtilityTransactionId();
			$arrintTransactionIds[] = $objUtilityTransaction->getTransactionId();

			if( CUtilityTransactionType::VCR_SERVICE_FEE == $objUtilityTransaction->getUtilityTransactionTypeId() ) {
				$arrobjServiceFeeUtilityTransaction[$objUtilityTransaction->getId()] = $objUtilityTransaction;
			}
		}

		$arrobjUtilityInvoices				= ( array ) \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchUtilityInvoicesByIds( array_filter( $arrintUtilityInvoiceIds ), $this->getDatabase() );
		$arrobjOriginalUtilityTransactions	= ( array ) \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUtilityTransactionsByIds( array_filter( $arrintUtilityTransactionIds ), $this->getDatabase() );

		foreach( $arrobjOriginalUtilityTransactions as $objUtilityTransaction ) {
			$arrintArTransactionIds[] = $objUtilityTransaction->getArTransactionId();
			$arrintTransactionIds[] = $objUtilityTransaction->getTransactionId();
		}

		$objProperty = \Psi\Eos\Admin\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $this->getClientDatabase() );
		$arrobjArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByIdsByCid( array_filter( $arrintArTransactionIds ), $this->getCid(), $this->getClientDatabase(), $boolExcludeDeleted = true );

		foreach( $arrobjArTransactions as $objArTransaction ) {
			$arrintLeaseIds[$objArTransaction->getLeaseId()] = $objArTransaction->getLeaseId();
		}

		switch( NULL ) {
			default:

				$boolIsValid = true;
				$intCreditAmount = 0;

				foreach( $arrobjClonedUtilityTransactions as $objUtilityTransaction ) {
					if( false == $objUtilityTransaction->delete( $this->getUserId(), $this->getDatabase() ) ) {
						$boolIsValid = false;
						break 2;
					}
				}

				foreach( $arrobjOriginalUtilityTransactions as $objUtilityTransaction ) {

					if( true == valId( $objUtilityTransaction->getUtilityBatchId() ) ) {
						$intCreditAmount += $objUtilityTransaction->getOriginalAmount();
					}

					if( false == $objUtilityTransaction->delete( $this->getUserId(), $this->getDatabase() ) ) {
						$boolIsValid = false;
						break 2;
					}
				}

				foreach( $arrobjVcrUtilityBills as $objVcrUtilityBill ) {
					if( false == $objVcrUtilityBill->delete( $this->getUserId(), $this->getDatabase() ) ) {
						$boolIsValid = false;
						break 2;
					}
				}

				$boolIsVCRInvoiceDistributed = false;
				foreach( $arrobjUtilityInvoices as $objUtilityInvoice ) {

					$strFunction = 'delete';

					if( true == valId( $objUtilityInvoice->getUtilityBatchId() ) ) {
						$strFunction = 'update';
						$objUtilityInvoice->setInvoiceAmount( $objUtilityInvoice->getInvoiceAmount() - $intCreditAmount );
					}

					$arrintUtilityBillAccountIds[$objUtilityInvoice->getUtilityAccountId()] = $objUtilityInvoice->getUtilityAccountId();

					if( false == $objUtilityInvoice->$strFunction( $this->getUserId(), $this->getDatabase() ) ) {
						$boolIsValid = false;
						break 2;
					}

					if( false == $boolIsVCRInvoiceDistributed && true == valId( $objUtilityInvoice->getUtilityDistributionId() ) ) {
						$boolIsVCRInvoiceDistributed = true;
					}
				}

				if( true == valObj( $objProperty, 'CProperty' ) ) {
					if( true == $objProperty->isIntegrated( $this->getClientDatabase() ) ) {

						foreach( $arrobjOriginalUtilityTransactions as $objUtilityTransaction ) {

							if( true == empty( $objUtilityTransaction->getArTransactionId() ) || true == empty( $objUtilityTransaction->getexportedBy() ) || true == empty( $objUtilityTransaction->getexportedon() ) ) {
								continue;
							}

							$objNewUtilityTransaction = clone $objUtilityTransaction;

							$objNewUtilityTransaction->setId( $objNewUtilityTransaction->fetchNextId( $this->getDatabase() ) );
							$objNewUtilityTransaction->setCurrentAmount( -1 * $objNewUtilityTransaction->getOriginalAmount() );
							$objNewUtilityTransaction->setOriginalAmount( $objNewUtilityTransaction->getCurrentAmount() );
							$objNewUtilityTransaction->setUnadjustedAmount( $objNewUtilityTransaction->getCurrentAmount() );
							$objNewUtilityTransaction->setexportedBy( NULL );
							$objNewUtilityTransaction->setexportedon( NULL );
							$objNewUtilityTransaction->setRemotePrimaryKey( NULL );
							$objNewUtilityTransaction->setArTransactionId( $objNewUtilityTransaction->getId() );
							$arrobjCreditUtilityTransacionts[$objNewUtilityTransaction->getId()] = $objNewUtilityTransaction;
						}

						foreach( $arrobjCreditUtilityTransacionts as $objUtilityTransaction ) {
							if( false == $objUtilityTransaction->insert( $this->getUserId(), $this->getDatabase() ) ) {
								$boolIsValid = false;
								break 2;
							}
						}
					} else {

						foreach( $arrobjArTransactions as $objArTransaction ) {

							if( false == $objArTransaction->delete( $this->getUserId(), $this->getClientDatabase() ) ) {
								$boolIsValid = false;
								break 2;
							}
						}
					}
				}
		}

		if( true == $boolIsValid ) {
			$boolIsValid = $this->addCreditForVcrServiceFee( array_filter( $arrintTransactionIds ), $arrobjServiceFeeUtilityTransaction, $objProperty );
		}

		if( true == $boolIsValid && true == $boolIsVCRInvoiceDistributed && true == valArr( $arrobjUtilityInvoices ) && true == valArr( array_diff_key( $this->getUtilityBillUnits(), $this->getOldUtilityBillUnits() ) ) ) {
			$boolIsValid = $this->sendDisregardEmailsToResidents( $arrintUtilityBillAccountIds, $arrobjUtilityInvoices );
		}

		return $boolIsValid;
	}

	public function reversalUEM() : bool {

		$boolIsValid = true;
		$boolResetUtilityBill = false;
		$boolIsSendDisregardEmail = false;

		if( false == is_null( $this->getApHeaderId() ) ) {
			$objApHeader			= \Psi\Eos\Entrata\CApHeaders::createService()->fetchApHeaderByIdByCid( $this->getApHeaderId(), $this->getCid(), $this->getClientDatabase() );
			$objPropertyGlSetting	= \Psi\Eos\Entrata\CPropertyGlSettings::createService()->fetchPropertyGlSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $this->getClientDatabase() );

			if( true == valObj( $objApHeader, 'CApHeader' ) && true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {

				$arrobjApDetails = ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchApDetailsByApHeaderIdByCid( $objApHeader->getId(), $objApHeader->getCid(), $this->getClientDatabase() );
				$arrobjGlDetails = ( array ) \Psi\Eos\Entrata\CGlDetails::createService()->fetchGlDetailsByApHeaderIdsByCid( $objApHeader->getId(), $objApHeader->getCid(), $this->getClientDatabase() );
				$objUtilityBillUnit = current( $this->getUtilityBillUnits() );

				if( true == $objApHeader->getIsPosted() ) {

					if( false == empty( $objUtilityBillUnit->getPropertyUnitId() ) ) {

						foreach( $arrobjApDetails as $objApDetail ) {
							$objApDetail->setPropertyUnitId( $objUtilityBillUnit->getPropertyUnitId() );

							if( false == $objApDetail->update( $this->getUserId(), $this->getClientDatabase() ) ) {
								$boolIsValid = false;
								break;
							}
						}

						foreach( $arrobjGlDetails as $objGlDetail ) {
							$objGlDetail->setPropertyUnitId( $objUtilityBillUnit->getPropertyUnitId() );

							if( false == $objGlDetail->update( $this->getUserId(), $this->getClientDatabase() ) ) {
								$boolIsValid = false;
								break;
							}
						}

						$boolIsSendDisregardEmail = true;
					}

				} else {
					$boolResetUtilityBill = true;
					if( false == $objApHeader->delete( $this->getUserId(), $this->getClientDatabase() ) ) {
						$boolIsValid = false;
					}
				}
			}
 		} elseif( false == is_null( $this->getApExceptionQueueItemId() ) ) {

			$boolResetUtilityBill = true;

			$objApExceptionQueueItem = \Psi\Eos\Entrata\CApExceptionQueueItems::createService()->fetchApExceptionQueueItemByIdByCid( $this->getApExceptionQueueItemId(), $this->getCid(), $this->getClientDatabase() );

			if( true == valObj( $objApExceptionQueueItem, 'CApExceptionQueueItem' ) && false == $objApExceptionQueueItem->delete( $this->getUserId(), $this->getClientDatabase() ) ) {
				$boolIsValid = false;
			}
		}

		if( true == $boolResetUtilityBill && true == $boolIsValid ) {
			$this->setExportedOn( NULL );
			$this->setExportedBy( NULL );
			$this->setApHeaderId( NULL );
			$this->setApExceptionQueueItemId( NULL );
			$this->setExceptionQueueItemCanceledBy( NULL );
			$this->setExceptionQueueItemCanceledOn( NULL );

			if( false == $this->update( $this->getUserId(), $this->getDatabase() ) ) {
				$boolIsValid = false;
			}
		}

		if( true == $boolIsValid && true == $boolIsSendDisregardEmail && true == valArr( array_diff_key( $this->getUtilityBillUnits(), $this->getOldUtilityBillUnits() ) ) ) {
			$boolIsValid = $this->sendDisregardEmailsToProperty( $objApHeader );
		}

		return $boolIsValid;
	}

	public function addCreditForVcrServiceFee( $arrintTransactionId, $arrobjServiceFeeUtilityTransaction, $objProperty ) : bool {

		if( false == valArr( $arrintTransactionId ) ) {
			return true;
		}

		$objPropertyUtilitySetting = \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingByPropertyId( $this->getPropertyId(), $this->getDatabase() );

		$arrmixContractProperties = ( array ) \Psi\Eos\Admin\CContractProperties::createService()->fetchActiveContractPropertiesDetailsByPsProductIdsByCidsByPropertyIds( [ CPsProduct::RESIDENT_UTILITY ], $this->m_objAdminDatabase, [ $objPropertyUtilitySetting->getCid() ], [ $objPropertyUtilitySetting->getPropertyId() ] );

		$objUtilityDistributionLibrary = new CUtilityDistributionLibrary();
		$objUtilityDistributionLibrary->setAdminDatabase( $this->m_objAdminDatabase );
		$objUtilityDistributionLibrary->setContractedProperties( $arrmixContractProperties );
		$objUtilityDistributionLibrary->setPropertyId( $objPropertyUtilitySetting->getPropertyId() );
		$intAccountId = $objUtilityDistributionLibrary->loadRecurringAccountId();

		if( false == valId( $intAccountId ) ) {
			return true;
		}

		$objBillingFeeAccount = \Psi\Eos\Admin\CAccounts::createService()->fetchAccountById( $intAccountId, $this->getAdminDatabase() );

		if( false == valObj( $objBillingFeeAccount, 'CAccount' ) ) {
			return true;
		}

		$objRuPricing = \Psi\Eos\Utilities\CRuPricings::createService()->fetchRuUemPricingsByPropertyId( $this->getPropertyId(), $this->getDatabase() );

		if( false == valObj( $objRuPricing, 'CRuPricing' ) ) {
			return true;
		}

		$intItemCount = \Psi\Libraries\UtilFunctions\count( $arrobjServiceFeeUtilityTransaction );
		$arrobjTransactions = ( array ) \Psi\Eos\Admin\CTransactions::createService()->fetchTransactionsByIds( $arrintTransactionId, $this->getAdminDatabase() );
		$intTransactionAmount = ( -1 ) * $intItemCount * ( float ) $objRuPricing->getVcrServiceFeeAmount();

		if( true == valArr( $arrobjTransactions ) && 0 < $intItemCount && 0 > $intTransactionAmount ) {

			$objCreditServiceFee = $objBillingFeeAccount->createTransaction();

			$objCreditServiceFee->setReferenceNumber( $this->getId() );
			$objCreditServiceFee->setItemCount( $intItemCount );
			$objCreditServiceFee->setChargeCodeId( CChargeCode::UTILITY_VCR_RECOVERY_FEES );
			$objCreditServiceFee->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
			$objCreditServiceFee->setTransactionAmount( $intTransactionAmount );
			$objCreditServiceFee->setCostAmount( $intItemCount * self::UTILITY_VCR_RECOVERY_FEE_COST_AMOUNT );
			$objCreditServiceFee->setMemo( 'Reversed VCR Recovery Fees for ' . $objProperty->getPropertyName() );
			$objCreditServiceFee->setPropertyId( $objProperty->getId() );

			if( false == $objCreditServiceFee->postCharge( $this->getUserId(), $this->getAdminDatabase() ) ) {
				$this->addErrorMsgs( $objCreditServiceFee->getErrorMsgs() );
				return false;
			}
		}

		return true;
	}

	public function sendDisregardEmailsToResidents( $arrintUtilityAccountIds, $arrobjUtilityInvoices ) {

		if( false == valArr( $arrintUtilityAccountIds ) ) {
			return true;
		}

		$arrobjUtilityInvoices = rekeyObjects( 'UtilityAccountId', $arrobjUtilityInvoices );
		$arrobjUtilityAccounts = rekeyObjects( 'CustomerId', \Psi\Eos\Utilities\CUtilityAccounts::createService()->fetchUtilityAccountsByIds( $arrintUtilityAccountIds, $this->getDatabase() ) );

		$arrintCustomerIds = $arrintGuarantorCustomerIds = [];

		foreach( $arrobjUtilityAccounts as $objUtilityAccount ) {
			$arrintCustomerIds[$objUtilityAccount->getCustomerId()] = $objUtilityAccount->getCustomerId();

			if( true == CStrings::strToBool( $objUtilityAccount->getIsSendInvoiceToGuarantor() ) ) {
				$arrintGuarantorCustomerIds[$objUtilityAccount->getCustomerId()] = $objUtilityAccount->getCustomerId();
			}
		}

		$arrobjCustomers = \Psi\Eos\Entrata\CCustomers::createService()->fetchViewCustomersByIdsByCids( $arrintCustomerIds, [ $this->getCid() ], $this->getClientDatabase() );
		$arrmixGuarantorCustomersData = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchCustomGuarantorCustomerDetailsByCustomerIdsByCids( $arrintGuarantorCustomerIds, [ $this->getCid() ], $this->getClientDatabase() );
		$arrobjCustomerSettings = rekeyObjects( 'CustomerId', ( array ) \Psi\Eos\Entrata\CCustomerSettings::createService()->fetchCustomCustomerSettingsByCustomerIdsByKeysByCids( $arrintCustomerIds, [ 'is_block_email_invoice' ], [ $this->getCid() ], $this->getClientDatabase() ) );

		$arrobjCustomers = array_diff_key( $arrobjCustomers, $arrobjCustomerSettings );

		if( false == valArr( $arrobjCustomers ) ) {
			return true;
		}

		if( true == valArr( $arrmixGuarantorCustomersData ) ) {

			$arrmixRekeyGuarantorCustomers = nestArrayByKeys( $arrmixGuarantorCustomersData, 'primary_customer_id', 'guarantor_customer_id', 'guarantor_email_address' );

			foreach( $arrmixRekeyGuarantorCustomers as $intPrimaryCustomerId => $arrmixGuarantorCustomers ) {

				$objCustomer = getArrayElementByKey( $intPrimaryCustomerId, $arrobjCustomers );
				$objUtilityAccount = getArrayElementByKey( $intPrimaryCustomerId, $arrobjUtilityAccounts );

				if( true == valObj( $objUtilityAccount, 'CUtilityAccount' ) && true == valArr( $objUtilityAccount->getAdditionalEmailAddresses() ) ) {
					$arrmixGuarantorCustomers = array_merge( $arrmixGuarantorCustomers, $objUtilityAccount->getAdditionalEmailAddresses() );
				}

				if( true == valObj( $objCustomer, 'CCustomer' ) ) {
					$objCustomer->setEmailAddress( $objCustomer->getEmailAddress() . ', ' . implode( ',', $arrmixGuarantorCustomers ) );
				}
			}
		}

		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'base_uri',					CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'image_url',					CONFIG_COMMON_PATH );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX',	'entrata_' );

		foreach( $arrobjCustomers as $objCustomer ) {

			$objUtilityAccount = getArrayElementByKey( $objCustomer->getId(), $arrobjUtilityAccounts );

			if( false == valObj( $objUtilityAccount, 'CUtilityAccount' ) ) {
				continue;
			}

			$objUtilityInvoice = getArrayElementByKey( $objUtilityAccount->getId(), $arrobjUtilityInvoices );

			if( false == valObj( $objUtilityInvoice, 'CUtilityInvoice' ) ) {
				continue;
			}

			if( true == empty( $objCustomer->getEmailAddress() ) ) {
				continue;
			}

			$objSmarty->assign( 'customer', $objCustomer );
			$objSmarty->assign( 'utility_invoice', $objUtilityInvoice );

			$objSystemEmail = new CSystemEmail();
			$objSystemEmail->setFromEmailAddress( CSystemEmail::UTILITYSUPPORT_EMAIL_ADDRESS );
			$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::RESIDENT_UTILITY_INVOICE );
			$objSystemEmail->setSubject( 'Disregard VCR Notice' );
			$objSystemEmail->setCid( $this->getCid() );
			$objSystemEmail->setPropertyId( $this->getPropertyId() );
			$objSystemEmail->setCustomerId( $objCustomer->getId() );
			$objSystemEmail->setHtmlContent( $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'utilities/utility_bills/vcr_utility_bills/vcr_disregard_email_content.tpl', PATH_INTERFACES_ADMIN . 'common/layouts/utility_email.tpl' ) );
			$objSystemEmail->setToEmailAddress( $objCustomer->getEmailAddress() );

			if( false == $objSystemEmail->insert( $this->getUserId(), $this->getEmailDatabase() ) ) {
				$this->addErrorMsgs( $objSystemEmail->getErrorMsgs() );
				return false;
			}
		}

		return true;
	}

	public function sendDisregardEmailsToProperty( $objApHeader ) {

		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByIdByCid( $this->getPropertyId(), $this->getCid(), $this->getClientDatabase() );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return true;
		}

		$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'VACANT_USAGE_EMAIL', $this->getPropertyId(), $this->getCid(), $this->getClientDatabase() );

		if( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) || true == empty( $objPropertyPreference->getValue() ) ) {
			return true;
		}

		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$objNewUtilityBillUnit = current( $this->m_arrobjUtilityBillUnits );
		$objOldUtilityBillUnit = current( $this->m_arrobjOldUtilityBillUnits );

		$strOldUnitNumber = $strNewUnitNumber = '';

		if( true == valObj( $objOldUtilityBillUnit, 'CUtilityBillUnit' ) && true == valObj( $objNewUtilityBillUnit, 'CUtilityBillUnit' ) ) {

			$arrobjUtilityPropertyUnit = rekeyObjects( 'PropertyUnitId', ( array ) \Psi\Eos\Utilities\CUtilityPropertyUnits::createService()->fetchUtilityPropertyUnitsByPropertyUnitIdsByPropertyIds( [ $objOldUtilityBillUnit->getPropertyUnitId(), $objNewUtilityBillUnit->getPropertyUnitId() ], [ $this->getPropertyId() ], $this->getDatabase() ) );

			if( false == valarr( $arrobjUtilityPropertyUnit ) ) {
				return true;
			}

			$objOldUtilityPropertyUnit = getArrayElementByKey( $objOldUtilityBillUnit->getPropertyUnitId(), $arrobjUtilityPropertyUnit );
			$objNewUtilityPropertyUnit = getArrayElementByKey( $objNewUtilityBillUnit->getPropertyUnitId(), $arrobjUtilityPropertyUnit );

			if( true == valObj( $objOldUtilityPropertyUnit, 'CUtilityPropertyUnit' ) ) {
				$strOldUnitNumber = $objOldUtilityPropertyUnit->getUnitNumber();
			}

			if( true == valObj( $objNewUtilityPropertyUnit, 'CUtilityPropertyUnit' ) ) {
				$strNewUnitNumber = $objNewUtilityPropertyUnit->getUnitNumber();
			}
		}

		if( false == valStr( $strOldUnitNumber ) || false == valStr( $strNewUnitNumber ) ) {
			return true;
		}

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'base_uri',					CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'image_url',					CONFIG_COMMON_PATH );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX',	'entrata_' );
		$objSmarty->assign( 'property',					$objProperty );
		$objSmarty->assign( 'ap_header',					$objApHeader );
		$objSmarty->assign( 'old_unit_number',			$strOldUnitNumber );
		$objSmarty->assign( 'new_unit_number',			$strNewUnitNumber );

		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setPropertyId( $this->getPropertyId() );

		$objSystemEmail->setToEmailAddress( $objPropertyPreference->getValue() );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::UTILITYSUPPORT_EMAIL_ADDRESS );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::RESIDENT_UTILITY_INVOICE );

		$objSystemEmail->setSubject( 'Disregard VCR Notice' );
		$objSystemEmail->setHtmlContent( $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'utilities/utility_bills/vcr_utility_bills/vcr_disregard_email_property_content.tpl', PATH_INTERFACES_ADMIN . 'common/layouts/utility_email.tpl' ) );

		if( false == $objSystemEmail->insert( $this->getUserId(), $this->getEmailDatabase() ) ) {
			$this->addErrorMsgs( $objSystemEmail->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function findNonRecoveredCommodities() {

		$arrmixNonRecoveredCommodities = [];

		if( true == valArr( $this->m_arrobjUtilityBillCharges ) ) {
			foreach( $this->m_arrobjUtilityBillCharges as $intKey => $objUtilityBillCharge ) {
				if( true == valObj( $objUtilityBillCharge, 'CUtilityBillCharge' ) ) {
					$arrmixUtilityBillCharge = $objUtilityBillCharge->toArray();
				} else {
					$arrmixUtilityBillCharge = $objUtilityBillCharge;
				}

				if( false == array_key_exists( $arrmixUtilityBillCharge['commodity_id'], $arrmixNonRecoveredCommodities ) ) {
					$arrmixNonRecoveredCommodities[$arrmixUtilityBillCharge['commodity_id']] = false;
				}

				if( true == CStrings::strToBool( $arrmixUtilityBillCharge['is_recovered'] ) ) {
					$arrmixNonRecoveredCommodities[$arrmixUtilityBillCharge['commodity_id']] = true;
				}
			}
		}

		return $arrmixNonRecoveredCommodities;
	}

	public function checkIsTestBatchBill( $objDatabase ) {
		$arrobjTestUtilityBills = \Psi\Eos\Utilities\CTestUtilityBills::createService()->fetchBatchedTestUtilityBillsByTestUtilityBillId( $this->getId(), $objDatabase );

		if( true == \valArr( $arrobjTestUtilityBills ) ) {
			return true;
		}

		return false;
	}

	public function setModifiedDueDate( $strModifiedField ) {
		if( false == valStr( $strModifiedField ) ) {
			return false;
		}

		if( false == is_null( $this->getBillDate() ) ) {
			$this->setDueDate( $this->getBillDate() );
		} else {
			$this->setDueDate( date( 'Y/m/d', strtotime( $this->getBillDatetime() ) ) );
		}

		if( self::DUE_UPON_RECEIPT == $strModifiedField ) {
			$this->setIsDueUponReceipt( true );
		} elseif( self::DO_NOT_PAY == $strModifiedField ) {
			$this->setIsDoNotPay( true );
		}
	}

}
?>