<?php

class CUtilityIntegrationSetting extends CBaseUtilityIntegrationSetting {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		// utility integration setting
		$arrstrTempUtilityIntegrationSettingOriginalValues = $arrstrUtilityIntegrationSettingOriginalValueChanges = $arrstrUtilityIntegrationSettingOriginalValues = [];
		$boolUtilityIntegrationSettingChangeFlag = false;

		$arrmixUtilityIntegrationSettingOriginalValues = fetchData( 'SELECT * FROM utility_integration_settings WHERE property_utility_setting_id = ' . ( int ) $this->getPropertyUtilitySettingId(), $objDatabase );

		$arrstrTempUtilityIntegrationSettingOriginalValues = $arrmixUtilityIntegrationSettingOriginalValues[0];

		foreach( $arrstrTempUtilityIntegrationSettingOriginalValues as $strKey => $mixUtilityIntegrationSettingOriginalValue ) {

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strFunctionName    = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			if( ( false == CStrings::strToBool( $mixUtilityIntegrationSettingOriginalValue ) && '' == $this->$strFunctionName() ) || ( true == CStrings::strToBool( $mixUtilityIntegrationSettingOriginalValue ) && '1' == $this->$strFunctionName() ) ) {
				continue;
			}

			if( CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $mixUtilityIntegrationSettingOriginalValue ) {

				$arrstrUtilityIntegrationSettingOriginalValueChanges[$strKey] = $this->$strFunctionName();
				$arrstrUtilityIntegrationSettingOriginalValues[$strKey]       = $mixUtilityIntegrationSettingOriginalValue;
				$boolUtilityIntegrationSettingChangeFlag                      = true;
			}
		}

		$boolUtilityIntegrationSettingUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUtilityIntegrationSettingUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolUtilityIntegrationSettingChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempUtilityIntegrationSettingOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempUtilityIntegrationSettingOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'utility_integration_settings' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrUtilityIntegrationSettingOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrUtilityIntegrationSettingOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		if( true == $boolUtilityIntegrationSettingUpdateStatus ) {
			return true;
		} else {
			return false;
		}
	}

}
?>