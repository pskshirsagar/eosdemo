<?php

class CUtilityTemplateType extends CBaseUtilityTemplateType {

	const FULL_LETTER_BILLING_INVOICE 	= 1;
	const POST_CARD_BILLING_INVOICE		= 2;
	const STAND_ALONE_UTILITY_NOTICE 	= 3;

}
?>