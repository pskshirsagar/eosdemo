<?php

class CMeterPartType extends CBaseMeterPartType {

	protected $m_strMeterManufacturerName;
	protected $m_strUtilityConsumptionTypeName;

	/**
	 * Get Functions
	 */

	public function getMeterManufacturerName() {
		return $this->m_strMeterManufacturerName;
	}

	public function getUtilityConsumptionTypeName() {
		return $this->m_strUtilityConsumptionTypeName;
	}

	/**
	 * Set Functions
	 */

	public function setMeterManufacturerName( $strMeterManufacturerName ) {
		$this->m_strMeterManufacturerName = $strMeterManufacturerName;
	}

	public function setUtilityConsumptionTypeName( $strUtilityConsumptionTypeName ) {
		$this->m_strUtilityConsumptionTypeName = $strUtilityConsumptionTypeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['meter_manufacturer_name'] ) ) {
			$this->setMeterManufacturerName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['meter_manufacturer_name'] ) : $arrmixValues['meter_manufacturer_name'] );
		}
		if( true == isset( $arrmixValues['utility_consumption_type_name'] ) ) {
			$this->setUtilityConsumptionTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_consumption_type_name'] ) : $arrmixValues['utility_consumption_type_name'] );
		}
	}

	/**
	 * Validate Functions
	 */

	public function valMeterManufacturerId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMeterManufacturerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_manufacturer_id', ' Meter manufacturer is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		if( false == is_null( $this->getName() ) && ( false == preg_match( '/^[0-9a-zA-Z ]+$/', $this->getName() ) || true == is_numeric( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Invalid name.' ) );
		}
		return $boolIsValid;
	}

	public function valCountFactor() {
		$boolIsValid = true;

		if( false == is_null( $this->getCountFactor() ) ) {
			if( 0 == $this->getCountFactor() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'count_factor', 'Invalid count factor.' ) );
			} elseif( 8 < strlen( $this->getCountFactor() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'count_factor', 'Count factor should not be greater than 8 digits.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valUniqueName( $objDatabase ) {

		$boolIsValid = true;

		$strWhereCondition = ' WHERE name = \'' . addslashes( $this->getName() ) . '\'';

		if( false == is_null( $this->getId() ) ) {
			$strWhereCondition .= ' AND id != ' . ( int ) $this->getId();
		}

		$intCount = \Psi\Eos\Utilities\CMeterPartTypes::createService()->fetchMeterPartTypeCount( $strWhereCondition, $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  ' Entered name is already used.' ) );
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		$intPropertyMeterDeviceCount = \Psi\Eos\Utilities\CPropertyMeterDevices::createService()->fetchPropertyMeterDeviceCount( 'WHERE meter_part_type_id = ' . ( int ) $this->getId(), $objDatabase );

		if( 0 < $intPropertyMeterDeviceCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  $this->m_strName . ' Meter part type cannot be removed because property meter device(s) depends on it.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase=NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMeterManufacturerId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valUniqueName( $objDatabase );
				$boolIsValid &= $this->valCountFactor();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPropertyMeterDevices( $objDatabase ) {
		return \Psi\Eos\Utilities\CPropertyMeterDevices::createService()->fetchPropertyMeterDevicesByMeterPartTypeId( $this->getId(), $objDatabase );
	}

}
?>