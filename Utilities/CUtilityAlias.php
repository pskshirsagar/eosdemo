<?php

class CUtilityAlias extends CBaseUtilityAlias {

	const TAB_PROPERTY = 'Property';
	const TAB_VENDOR   = 'Vendor';

	protected $m_strSelectedTab;

	public function getSelectedTab() {
		return $this->m_strSelectedTab;
	}

	public function setSelectedTab( $strSelectedTab ) {
		$this->m_strSelectedTab = $strSelectedTab;
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {

		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client name is required.' ) );
		}

		return $boolIsValid;

	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property name is required.' ) );
		}

		return $boolIsValid;

	}

	public function valApPayeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApPayeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Vendor name is required.' ) );
		}

		return $boolIsValid;

	}

	public function valAlias( $objDatabase ) {
		$boolIsValid = true;

		if( false == \valStr( $this->getAlias() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alias', 'Alias is required.' ) );
		} else {
			$strWhere    = 'WHERE lower( alias ) = \'' . strtolower( $this->getAlias() ) . '\'';
			$strSubWhere = NULL;
			if( self::TAB_PROPERTY == $this->m_strSelectedTab && true == $this->valPropertyId() ) {
				$strSubWhere .= ' AND property_id = ' . $this->getPropertyId();
			}

			if( self::TAB_VENDOR == $this->m_strSelectedTab && true == $this->valApPayeeId() ) {
				$strSubWhere .= ' AND ap_payee_id = ' . $this->getApPayeeId();
			}

			if( true == \valId( $this->getId() ) ) {
				$strSubWhere .= ' AND id <> ' . $this->getId();
			}

			if( false == is_null( $strSubWhere ) ) {
				$intCount = \Psi\Eos\Utilities\CUtilityAliases::createService()->fetchUtilityAliasCount( $strWhere . $strSubWhere, $objDatabase );

				if( 0 < $intCount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alias', 'Alias already exists for same ' . $this->m_strSelectedTab . '.' ) );
					$boolIsValid = false;
				}

			}

		}

		return $boolIsValid;

	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAlias( $objDatabase );

				if( self::TAB_PROPERTY == $this->m_strSelectedTab ) {
					$boolIsValid &= $this->valPropertyId();
				}
				if( self::TAB_VENDOR == $this->m_strSelectedTab ) {
					$boolIsValid &= $this->valApPayeeId();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>