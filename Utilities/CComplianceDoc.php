<?php

class CComplianceDoc extends CBaseComplianceDoc {

	protected $m_intPropertyId;
	protected $m_intComplianceTypeId;
	protected $m_intComplianceJobItemId;

	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strPolicyType;
	protected $m_strCompanyName;
	protected $m_strComplianceItemName;

	protected $m_boolIsIncludeAllProperties;
	protected $m_boolIsValidateExpirationDate;

	public function valId() {
		return true;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Customer is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valVendorId() {
		return true;
	}

	public function valApPayeeId() {
		return true;
	}

	public function valComplianceItemId( $boolIsDocFromEntrata = false ) {
		$boolIsValid = true;

		if( 0 == ( int ) $this->getComplianceItemId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'compliance_item_id', __( 'Type is required.' ) ) );
			$boolIsValid = false;
		}

		if( true == $boolIsDocFromEntrata && true == array_key_exists( $this->getComplianceItemId(), CComplianceItem::$c_arrintCustomerSpecificSharigComplianceItemIds ) && false == valId( $this->getCid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Customer is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valRequestComplianceJobItemId() {
		return true;
	}

	public function valScreeningId() {
		return true;
	}

	public function valScreeningRecommendationTypeId() {
		return true;
	}

	public function valDocId() {
		return true;
	}

	public function valDocDatetime() {
		return true;
	}

	public function valIsVendorDoc() {
		return true;
	}

	public function valIsArchived() {
		return true;
	}

	public function valCoverageName() {
		$boolIsValid = true;

		if( true == is_null( $this->getCoverageName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'coverage_name', 'Coverage Name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function validateComplianceDoc( $arrmixValidateComplianceDocs ) {

		$boolIsValid = true;
		$intVendorEntityId = $arrmixValidateComplianceDocs['vendor_entity_id'];
		$arrintPropertyIds = $arrmixValidateComplianceDocs['property_ids'];

		$objDoc              = getArrayElementByKey( 'old_doc', $arrmixValidateComplianceDocs );
		$boolIsUpdateValue   = getArrayElementByKey( 'is_update_value', $arrmixValidateComplianceDocs );
		$objOldComplianceDoc = getArrayElementByKey( 'old_compliance_doc', $arrmixValidateComplianceDocs );

		if( false == valId( $intVendorEntityId ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Select at least one legal entity.' ), NULL ) );
		}

		if( true == array_key_exists( $this->getComplianceItemId(), CComplianceItem::$c_arrintCustomerSpecificSharigComplianceItemIds ) ) {

			if( true == valId( $intVendorEntityId ) && false == valId( $this->getCid() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Customer is required.' ) ) );
				$boolIsValid &= false;
			}

			if( false == valArr( $arrintPropertyIds ) && false == $this->getIsIncludeAllProperties() && true == valId( $intVendorEntityId ) && true == valId( $this->getCid() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Select at least one property.' ) ) );
				$boolIsValid &= false;
			}
		}

		if( true == $this->getIsValidateExpirationDate() ) {
			if( true == is_null( $this->getExpirationDate() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( NULL, 'expiration_date', __( 'Expiration date is required.' ) ) );
			} elseif( false == CValidation::validateDate( $this->getExpirationDate() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( NULL, 'expiration_date', __( 'A valid expiration date is required.' ) ) );
			}
		}

		if( ( false == is_null( $this->getId() ) && true == $boolIsUpdateValue ) || ( true == valObj( $objOldComplianceDoc, CComplianceDoc::class )
		                                                                              && strtotime( $this->getExpirationDate() ) != strtotime( $objOldComplianceDoc->getExpirationDate() )
		                                                                              && false == valObj( $objDoc, CDoc::class ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please re-upload this document, as one or more fields have been updated.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixValidateComplianceDocs = [] ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();

				if( true == $this->getIsVendorDoc() ) {
					$boolIsValid &= $this->valCoverageName();
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_company_document':
				$boolIsValid &= $this->valComplianceItemId( true );
				break;

			case 'company_document':
				$boolIsValid &= $this->valComplianceItemId();
				$boolIsValid &= $this->validateComplianceDoc( $arrmixValidateComplianceDocs );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function setPolicyType( $strPolicyType ) {
		$this->m_strPolicyType = $strPolicyType;
	}

	public function getPolicyType() {
		return $this->m_strPolicyType;
	}

	public function setComplianceJobItemId( $intComplianceJobItemId ) {
		$this->m_intComplianceJobItemId = $intComplianceJobItemId;
	}

	public function getComplianceJobItemId() {
		return $this->m_intComplianceJobItemId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = CStrings::strTrimDef( $strFileName, 240, NULL, true );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = CStrings::strTrimDef( $strFilePath, 240, NULL, true );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function setComplianceItemName( $strComplianceItemName ) {
		$this->m_strComplianceItemName = CStrings::strTrimDef( $strComplianceItemName, 240, NULL, true );
	}

	public function getComplianceItemName() {
		return $this->m_strComplianceItemName;
	}

	public function setEntrataReferenceId( $intEntrataReferenceId ) {
		$this->m_intEntrataReferenceId = $intEntrataReferenceId;
	}

	public function setComplianceLevelId( $intComplianceLevelId ) {
		$this->m_intComplianceLevelId = $intComplianceLevelId;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = CStrings::strTrimDef( $strCompanyName, 40, NULL, true );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function setComplianceTypeId( $intComplianceTypeId ) {
		$this->m_intComplianceTypeId = $intComplianceTypeId;
	}

	public function getComplianceTypeId() {
		return $this->m_intComplianceTypeId;
	}

	public function setIsIncludeAllProperties( $boolIsIncludeAllProperties ) {
		$this->set( 'm_boolIsIncludeAllProperties', CStrings::strToBool( $boolIsIncludeAllProperties ) );
	}

	public function getIsIncludeAllProperties() {
		return $this->m_boolIsIncludeAllProperties;
	}

	public function setIsValidateExpirationDate( $boolIsValidateExpirationDate ) {
		$this->set( 'm_boolIsValidateExpirationDate', CStrings::strToBool( $boolIsValidateExpirationDate ) );
	}

	public function getIsValidateExpirationDate() {
		return $this->m_boolIsValidateExpirationDate;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['file_name'] ) && $boolDirectSet ) {
			$this->m_strFileName = trim( stripcslashes( $arrmixValues['file_name'] ) );
		} elseif( isset( $arrmixValues['file_name'] ) ) {
			$this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['file_name'] ) : $arrmixValues['file_name'] );
		}

		if( isset( $arrmixValues['file_path'] ) && $boolDirectSet ) {
			$this->m_strFilePath = trim( stripcslashes( $arrmixValues['file_path'] ) );
		} elseif( isset( $arrmixValues['file_path'] ) ) {
			$this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['file_path'] ) : $arrmixValues['file_path'] );
		}

		if( isset( $arrmixValues['compliance_item_name'] ) && $boolDirectSet ) {
			$this->m_strComplianceItemName = trim( stripcslashes( $arrmixValues['compliance_item_name'] ) );
		} elseif( isset( $arrmixValues['compliance_item_name'] ) ) {
			$this->setComplianceItemName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['compliance_item_name'] ) : $arrmixValues['compliance_item_name'] );
		}

		if( isset( $arrmixValues['compliance_job_item_id'] ) && $boolDirectSet ) {
			$this->m_intComplianceJobItemId = trim( $arrmixValues['compliance_job_item_id'] );
		} elseif( isset( $arrmixValues['compliance_job_item_id'] ) ) {
			$this->setComplianceJobItemId( $arrmixValues['compliance_job_item_id'] );
		}

		if( true == isset( $arrmixValues['policy_type'] ) ) {
			$this->setPolicyType( $arrmixValues['policy_type'] );
		}

		if( isset( $arrmixValues['property_id'] ) && $boolDirectSet ) {
			$this->m_intPropertyId = trim( $arrmixValues['property_id'] );
		} elseif( isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}

		if( isset( $arrmixValues['entrata_reference_id'] ) && $boolDirectSet ) {
			$this->m_intEntrataReferenceId = trim( $arrmixValues['entrata_reference_id'] );
		} elseif( isset( $arrmixValues['entrata_reference_id'] ) ) {
			$this->setEntrataReferenceId( $arrmixValues['entrata_reference_id'] );
		}

		if( isset( $arrmixValues['compliance_level_id'] ) && $boolDirectSet ) {
			$this->m_intComplianceLevelId = trim( $arrmixValues['compliance_level_id'] );
		} elseif( isset( $arrmixValues['compliance_level_id'] ) ) {
			$this->setComplianceLevelId( $arrmixValues['compliance_level_id'] );
		}

		if( isset( $arrmixValues['company_name'] ) && $boolDirectSet ) {
			$this->m_strCompanyName = trim( stripcslashes( $arrmixValues['company_name'] ) );
		} elseif( isset( $arrmixValues['company_name'] ) ) {
			$this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['company_name'] ) : $arrmixValues['company_name'] );
		}

		if( isset( $arrmixValues['compliance_type_id'] ) && $boolDirectSet ) {
			$this->m_intPropertyId = trim( $arrmixValues['compliance_type_id'] );
		} elseif( isset( $arrmixValues['compliance_type_id'] ) ) {
			$this->setPropertyId( $arrmixValues['compliance_type_id'] );
		}

		if( isset( $arrmixValues['is_include_all_properties'] ) && $boolDirectSet ) {
			$this->set( 'm_boolIsIncludeAllProperties', trim( stripcslashes( $arrmixValues['is_include_all_properties'] ) ) );
		} elseif( isset( $arrmixValues['is_include_all_properties'] ) ) {
			$this->setIsIncludeAllProperties( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_include_all_properties'] ) : $arrmixValues['is_include_all_properties'] );
		}

		if( isset( $arrmixValues['is_validate_expiration_date'] ) && $boolDirectSet ) {
			$this->set( 'm_boolIsValidateExpirationDate', trim( stripcslashes( $arrmixValues['is_validate_expiration_date'] ) ) );
		} elseif( isset( $arrmixValues['is_validate_expiration_date'] ) ) {
			$this->setIsValidateExpirationDate( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_validate_expiration_date'] ) : $arrmixValues['is_validate_expiration_date'] );
		}

	}

	/**
	 * Create Functions
	 */

	public function createComplianceDocPropertyGroup() {
		$objComplianceDocPropertyGroup = new CComplianceDocPropertyGroup();

		$objComplianceDocPropertyGroup->setCid( $this->getCid() );
		$objComplianceDocPropertyGroup->setComplianceDocId( $this->getId() );
		$objComplianceDocPropertyGroup->setApPayeeId( $this->getApPayeeId() );
		$objComplianceDocPropertyGroup->setVendorId( $this->getVendorId() );

		return $objComplianceDocPropertyGroup;
	}

	public function createComplianceDocAssociation() {
		$objComplianceDocAssociation = new CComplianceDocAssociation();

		$objComplianceDocAssociation->setCid( $this->getCid() );
		$objComplianceDocAssociation->setComplianceDocId( $this->getId() );

		return $objComplianceDocAssociation;
	}

	public function createComplianceDocPolicy() {
		$objComplianceDocPolicy = new CComplianceDocPolicy();

		$objComplianceDocPolicy->setCid( $this->getCid() );
		$objComplianceDocPolicy->setComplianceDocId( $this->getId() );

		return $objComplianceDocPolicy;
	}

}
?>