<?php

class CMeterTransmissionBatch extends CBaseMeterTransmissionBatch {

	protected $m_intUtilityTypeId;
	protected $m_intCountFactor;
	protected $m_intPropertyId;

	protected $m_strUnitNumber;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_type_id'] ) ) {
			$this->setUtilityTypeId( $arrmixValues['utility_type_id'] );
		}
		if( true == isset( $arrmixValues['count_factor'] ) ) {
			$this->setCountFactor( $arrmixValues['count_factor'] );
		}
		if( true == isset( $arrmixValues['unit_number'] ) ) {
			$this->setUnitNumber( $arrmixValues['unit_number'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}

	}

    public function setUtilityTypeId( $intUtilityTypeId ) {
    	$this->m_intUtilityTypeId = $intUtilityTypeId;
    }

    public function setCountFactor( $intCountFactor ) {
    	$this->m_intCountFactor = $intCountFactor;
    }

    public function setUnitNumber( $strUnitNumber ) {
    	$this->m_strUnitNumber = $strUnitNumber;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function getUtilityTypeId() {
    	return $this->m_intUtilityTypeId;
    }

    public function getCountFactor() {
    	return $this->m_intCountFactor;
    }

    public function getUnitNumber() {
    	return $this->m_strUnitNumber;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>