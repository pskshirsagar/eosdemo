<?php

class CCheckPaymentDistributionStep extends CBaseCheckPaymentDistributionStep {

	const GENERATED			= 1;
	const APPROVED			= 2;
	const MAILED			= 3;
	const COMPLETED			= 4;

}
?>