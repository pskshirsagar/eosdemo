<?php

class CUtilityAccountStatusType extends CBaseUtilityAccountStatusType {

	const FUTURE 			= 1;
	const PENDING_MOVE_IN 	= 2;
	const CURRENT 			= 3;
	const PENDING_MOVE_OUT 	= 4;
	const MOVE_OUT_APPROVED	= 5;
	const PAST 				= 6;
	const PAST_UNPAID		= 7;
	const EVICTING			= 8;

	public static function getUtilityAccountStatusTypeIdByLeaseStatusTypeIdByBalanceDue( $objUtilityAccount, $intLeaseStatusTypeId, $fltBalanceDue = NULL, $boolIsEntrata = false ) {

		if( false == $boolIsEntrata && true == $objUtilityAccount->getIsMovedOut() && true == in_array( $objUtilityAccount->getUtilityAccountStatusTypeId(), [ self::PAST, self::PAST_UNPAID ] ) ) {
			return $objUtilityAccount->getUtilityAccountStatusTypeId();
		}

     	$intUtilityAccountStatusTypeId = NULL;

    	switch( $intLeaseStatusTypeId ) {

    		case CLeaseStatusType::CURRENT:
				$intUtilityAccountStatusTypeId = self::CURRENT;
				break;

			case CLeaseStatusType::NOTICE:
				$intUtilityAccountStatusTypeId = self::PENDING_MOVE_OUT;
				break;

    		case CLeaseStatusType::PAST:
				if( false == is_numeric( $objUtilityAccount->getId() ) ) {
					if( 0 < $fltBalanceDue ) {
						$intUtilityAccountStatusTypeId = self::PAST_UNPAID;
					}
				} else {
					$intUtilityAccountStatusTypeId = self::PAST;
				}
				break;

			case CLeaseStatusType::CANCELLED:
				$intUtilityAccountStatusTypeId = self::PAST;
				break;

			case CLeaseStatusType::FUTURE:
				if( true == is_null( $objUtilityAccount->getId() ) ) {

					$objLease 		= $objUtilityAccount->getLease();
					$objDateTime 	= new DateTime( date( 'Y-m-01' ) );
					$objDateTime->modify( 'next month' );

					if( true == valObj( $objLease, 'CLease' ) && true == valStr( $objLease->getLeaseStartDate() ) && ( date( 'm/Y', strtotime( $objLease->getLeaseStartDate() ) ) == date( 'm/Y' ) || date( 'm/Y', strtotime( $objLease->getLeaseStartDate() ) ) == $objDateTime->format( 'm/Y' ) ) ) {
						$intUtilityAccountStatusTypeId = self::PENDING_MOVE_IN;
					} else {
						$intUtilityAccountStatusTypeId = self::FUTURE;
					}

				} else {
					$intUtilityAccountStatusTypeId = ( self::CURRENT == $objUtilityAccount->getUtilityAccountStatusTypeId() ) ? $objUtilityAccount->getUtilityAccountStatusTypeId() : self::PENDING_MOVE_IN;
				}
				break;

    		default:
    			if( true == is_numeric( $objUtilityAccount->getId() ) ) {
    				$intUtilityAccountStatusTypeId = self::PAST;
    			}
    			break;
    	}
    	return $intUtilityAccountStatusTypeId;
    }

    public static function determineIsPast( $intUtilityAccountStatusTypeId ) {

    	$arrintPastUtilityAccountStatusTypeIds = [ self::MOVE_OUT_APPROVED, self::PAST, self::PAST_UNPAID ];

	    return true == in_array( $intUtilityAccountStatusTypeId, $arrintPastUtilityAccountStatusTypeIds );

    }

    // If we return false, we should not update or insert the record to be safe.

	public function getUtilityAccountStatusTypeIdByLeaseStatusTypeIdByBalanceDue2( $objUtilityAccount, $intLeaseStatusTypeId, $fltBalanceDue = NULL ) {

		$intUtilityAccountStatusTypeId = NULL;
		$boolIsLeaseStatusCurrent = CLeaseStatusType::determineIsCurrent( $intLeaseStatusTypeId );

		// If utility account is NOT already in the system.
		if( ( false == is_numeric( $objUtilityAccount->getId() ) && true == $boolIsLeaseStatusCurrent ) || ( true == is_numeric( $objUtilityAccount->getId() ) && true == $boolIsLeaseStatusCurrent && self::FUTURE == $objUtilityAccount->getUtilityAccountStatusTypeId() ) ) {
			$intUtilityAccountStatusTypeId = self::PENDING_MOVE_IN;

		} elseif( false == is_numeric( $objUtilityAccount->getId() ) && $intLeaseStatusTypeId == CLeaseStatusType::PAST && 0 < ( int ) $fltBalanceDue ) {
			$intUtilityAccountStatusTypeId = self::PAST_UNPAID;

		// If utility account is already in the system.
		} elseif( true == is_numeric( $objUtilityAccount->getId() ) && true == CLeaseStatusType::determineIsPast( $intLeaseStatusTypeId ) && false == CUtilityAccountStatusType::determineIsPast( $objUtilityAccount->getUtilityAccountStatusTypeId() ) ) {
			$intUtilityAccountStatusTypeId = self::PENDING_MOVE_OUT;

		} elseif( true == is_numeric( $objUtilityAccount->getId() ) ) {
			$intUtilityAccountStatusTypeId = $objUtilityAccount->getUtilityAccountStatusTypeId();
		}

    	return $intUtilityAccountStatusTypeId;
    }

    public static function getUtilityAccountStatusTypeNameByUtilityAccountStatusTypeId( $intAccountStatusTypeId ) {
    	$strStatusTypeName = NULL;

    	switch( $intAccountStatusTypeId ) {

    		case CUtilityAccountStatusType::FUTURE:
    			$strStatusTypeName = 'Future';
    			break;

    		case CUtilityAccountStatusType::PENDING_MOVE_IN:
    			$strStatusTypeName = 'Pending Move-In';
    			break;

    		case CUtilityAccountStatusType::CURRENT:
    			$strStatusTypeName = 'Current';
    			break;

    		case CUtilityAccountStatusType::PENDING_MOVE_OUT:
    			$strStatusTypeName = 'Pending Move-Out';
    			break;

    		case CUtilityAccountStatusType::MOVE_OUT_APPROVED:
    			$strStatusTypeName = 'Move-Out Approved';
    			break;

    		case CUtilityAccountStatusType::PAST:
    			$strStatusTypeName = 'Past';
    			break;

    		case CUtilityAccountStatusType::PAST_UNPAID:
    			$strStatusTypeName = 'Past Unpaid';
    			break;

    		case CUtilityAccountStatusType::EVICTING:
    			$strStatusTypeName = 'Evicting';
    			break;

    		default:
    			// default case
    			break;
    	}

    	return $strStatusTypeName;
    }

}
?>