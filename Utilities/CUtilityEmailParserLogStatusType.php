<?php

class CUtilityEmailParserLogStatusType extends CBaseUtilityEmailParserLogStatusType {

	const PROCESSED		= 1;
	const UNPROCESSED	= 2;
	const FAILED		= 3;

	public static function getUtilityEmailParserLogStatusTypes() {
		return [
			self::PROCESSED		=> 'Processed',
			self::UNPROCESSED	=> 'Unprocessed',
			self::FAILED		=> 'Failed'
		];
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>