<?php

class CBillAuditType extends CBaseBillAuditType {

	const MISSING_LINE_ITEM                     	= 1;
	const BILL_END_DATE_BEYOND_BILL_DATE         	= 2;
	const MULTIPLE_BILLS_IN_SAME_BILLING_PERIOD		= 4;
	const BILLING_CYCLE_DAY_DIFFERENCE            	= 5;
	const NEW_LINE_ITEM            				  	= 6;
	const LATE_FEE            				  	  	= 7;
	const BALANCE_FORWARD				  			= 8;
	const CREDIT 						  	  		= 9;
	const ABNORMAL_DUE_DATE							= 10;
	const CYCLE_OVERLAP                           	= 11;
	const CONSUMPTION_GAP                          	= 12;
	const ESTIMATED_BILL                          	= 13;
	const ZERO_CONSUMPTION                          = 14;
	const RATE_CHANGE								= 15;
	const USAGE_MISREAD								= 16;
	const NEW_METER_AUDIT							= 17;
	const DEMAND_AUDIT								= 18;
	const CORRECTED_BILL							= 19;
	const SUMMARY_INVOICE							= 20;
	const PAST_DUE_DATE								= 21;
	const CREDIT_TOTAL								= 22;

	public static function getBillAuditType( $intBillAuditType ) {

		$strBillAuditType = NULL;

		switch( $intBillAuditType ) {

			case self::MISSING_LINE_ITEM:
				$strBillAuditType = 'Missing Line Item';
                break;

			case self::BILL_END_DATE_BEYOND_BILL_DATE:
				$strBillAuditType = 'Bill End Date Beyond Bill Date';
				break;

			case self::MULTIPLE_BILLS_IN_SAME_BILLING_PERIOD:
				$strBillAuditType = 'Multiple Bills In Same Billing Period';
				break;

			case self::BILLING_CYCLE_DAY_DIFFERENCE:
				$strBillAuditType = 'Billing Cycle Day Difference';
				break;

			case self::NEW_LINE_ITEM:
				$strBillAuditType = 'New Line Item';
				break;

			case self::LATE_FEE:
				$strBillAuditType = 'Late Fee';
				break;

			case self::BALANCE_FORWARD:
				$strBillAuditType = 'Balance Forward';
				break;

			case self::CREDIT:
				$strBillAuditType = 'Credit';
				break;

			case self::ABNORMAL_DUE_DATE:
				$strBillAuditType = 'Abnormal due date';
				break;

			case self::CYCLE_OVERLAP:
				$strBillAuditType = 'Bill Cycle Overlap';
				break;

			case self::CONSUMPTION_GAP:
				$strBillAuditType = 'Consumption Gap';
				break;

			case self::ESTIMATED_BILL:
				$strBillAuditType = 'Estimated Bill';
				break;

			case self::ZERO_CONSUMPTION:
				$strBillAuditType = 'Zero Consumption';
				break;

			case self::RATE_CHANGE:
				$strBillAuditType = 'Rate Change';
				break;

			case self::USAGE_MISREAD:
				$strBillAuditType = 'Usage Misread';
				break;

			case self::NEW_METER_AUDIT:
				$strBillAuditType = 'New Meter Audit';
				break;

			case self::DEMAND_AUDIT:
				$strBillAuditType = 'Demand Audit';
				break;

			case self::CORRECTED_BILL:
				$strBillAuditType = 'Corrected Bill';
				break;

			case self::PAST_DUE_DATE:
				$strBillAuditType = 'Past Due Date';
				break;

			case self::SUMMARY_INVOICE:
				$strBillAuditType = 'Summary Invoice';
				break;

			case self::CREDIT_TOTAL:
				$strBillAuditType = 'Credit Total';
				break;

			default:
				// default case
				break;
		}

		return $strBillAuditType;
	}

}
?>