<?php

class CPool extends CBasePool {

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valName( $intVendorId, $objDatabase ) {

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Group name is required.' ) ) );
		} elseif( 0 < strlen( $this->getName() ) && true == valObj( $objDatabase, 'CDatabase' )
					&& 0 < \Psi\Eos\Utilities\CPools::createService()->fetchPoolCount( ' WHERE lower( name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\' AND id !=' . ( int ) $this->getId() . ' AND vendor_id = ' . ( int ) $intVendorId, $objDatabase ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Group name is already being used.' ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valDescription() {
		return true;
	}

	public function validate( $strAction, $intVendorId, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $intVendorId, $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>