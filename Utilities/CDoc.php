<?php

class CDoc extends CBaseDoc {

	protected $m_boolIsVendorDoc;

	protected $m_strTempFileName;

	protected $m_intCid;
	protected $m_intOrderHeaderId;
	protected $m_intVendorReferenceId;

	public function valId() {
		return true;
	}

	public function valFileName( $boolIsDocRequired, $arrstrFileExtensions = [] ) {

		if( false == valArr( $arrstrFileExtensions ) ) {
			$arrstrFileExtensions = [ 'pdf' ];
		}

		$intFileContentSize = ( int ) ( ( $_SERVER['CONTENT_LENGTH'] / 1024 ) / 1024 );
		$intMaxContentSize  = ini_get( 'upload_max_filesize' );
		$intMaxContentSize  = \Psi\CStringService::singleton()->substr( $intMaxContentSize, 0, - 1 );

		if( true == $boolIsDocRequired && true == is_null( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Please select a file to upload.' ) ) );

		} elseif( 0 != $intFileContentSize && $intFileContentSize > $intMaxContentSize ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Please select valid file to upload upto size ' ) . ini_get( 'upload_max_filesize' ) ) );

		} elseif( false == is_null( $this->getFileName() ) && false == in_array( \Psi\CStringService::singleton()->strtolower( pathinfo( $this->getFileName(), PATHINFO_EXTENSION ) ), $arrstrFileExtensions, true ) ) {

			$arrstrValidFiles = implode( ', ', $arrstrFileExtensions );
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Invalid file selected, must select a valid {%s, 0} file to upload.', [ $arrstrValidFiles ] ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valFilePath() {
		return true;
	}

	public function validate( $strAction, $boolIsDocRequired = true, $arrstrFileExtensions = [] ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFileName( $boolIsDocRequired, $arrstrFileExtensions );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function setOrderHeaderId( $intOrderHeaderId ) {
		$this->m_intOrderHeaderId = CStrings::strToIntDef( $intOrderHeaderId );
	}

	public function getOrderHeaderId() {
		return $this->m_intOrderHeaderId;
	}

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function getIsVendorDoc() {
		return $this->m_boolIsVendorDoc;
	}

	public function setIsVendorDoc( $boolIsVendorDoc ) {
		$this->m_boolIsVendorDoc = CStrings::strToBool( $boolIsVendorDoc );
	}

	public function setFileName( $strFileName ) {
		parent::setFileName( $strFileName );
		// replace spaces with underscore.
		$this->m_strFileName = str_replace( ' ', '_', $this->getFileName() );
		// replace all special characters by blank.
		$this->m_strFileName = preg_replace( '/[^A-Za-z0-9\-_.]/', '', $this->getFileName() );
	}

	public function setVendorReferenceId( $intVendorReferenceId ) {
		$this->m_intVendorReferenceId = $intVendorReferenceId;
	}

	public function getVendorReferenceId() {
		return $this->m_intVendorReferenceId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['cid'] ) && $boolDirectSet ) {
			$this->m_intCid = trim( stripcslashes( $arrmixValues['cid'] ) );
		} elseif( isset( $arrmixValues['cid'] ) ) {
			$this->setCid( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cid'] ) : $arrmixValues['cid'] );
		}

		if( isset( $arrmixValues['is_vendor_doc'] ) && $boolDirectSet ) {
			$this->m_boolIsVendorDoc = trim( stripcslashes( $arrmixValues['is_vendor_doc'] ) );
		} elseif( isset( $arrmixValues['is_vendor_doc'] ) ) {
			$this->setIsVendorDoc( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_vendor_doc'] ) : $arrmixValues['is_vendor_doc'] );
		}

		if( isset( $arrmixValues['vendor_reference_id'] ) && $boolDirectSet ) {
			$this->m_intVendorReferenceId = trim( stripcslashes( $arrmixValues['vendor_reference_id'] ) );
		} elseif( isset( $arrmixValues['vendor_reference_id'] ) ) {
			$this->setVendorReferenceId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['vendor_reference_id'] ) : $arrmixValues['vendor_reference_id'] );
		}

		if( isset( $arrmixValues['order_header_id'] ) && $boolDirectSet ) {
			$this->m_intOrderHeaderId = trim( $arrmixValues['order_header_id'] );
		} elseif( isset( $arrmixValues['order_header_id'] ) ) {
			$this->setOrderHeaderId( $arrmixValues['order_header_id'] );
		}
	}

	public function buildDocumentPath( $intVendorId = NULL ) {

		$strPath = $intVendorId . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/';

		$this->setFilePath( $strPath );
	}

	public function uploadDocument( $strUploadPath = PATH_MOUNTS_FILES ) {

		$boolIsValid = true;

		if( false == is_dir( $strUploadPath ) && false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
			$boolIsValid = false;
		}

		if( $boolIsValid == true && false == CFileIo::moveFile( $this->getTempFileName(), $strUploadPath . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function downloadFile( $objDatabase ) {

		$strFullPath = $this->getFilePath() . $this->getFileName();

		if( false == CFileIo::fileExists( $strFullPath ) ) {
			trigger_error( __( 'File is not present or has been deleted.' ), E_USER_ERROR );
			exit;
		}

		$arrmixFileParts = pathinfo( $this->getFileName() );

		if( true == valArr( $arrmixFileParts ) && false == is_null( $arrmixFileParts['extension'] ) ) {
			$objFileExtension = \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrmixFileParts['extension'], $objDatabase );
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: public' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Length:' . CFileIo::getFileSize( $strFullPath ) );
		header( 'Content-Type:' . $strMimeType );
		header( 'Content-Disposition: attachment; filename= "' . $this->getFileName() . '"' );
		header( 'Content-Transfer-Encoding:binary' );

		if( 'text/html' == $strMimeType ) {
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
		}

		echo CFileIo::fileGetContents( $strFullPath );
		exit;
	}

	public function copyDocument( $objCopyDocument, $strPath ) {

		$boolIsValid = true;

		$strSourceFilePath  = $strPath . $objCopyDocument->getFilePath() . $objCopyDocument->getFileName();
		$strDestinationPath = $strPath . $this->getFilePath();

		if( false == file_exists( $strSourceFilePath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No source file exists.' ) ) );
			$boolIsValid &= false;
		}

		if( false == is_dir( $strDestinationPath ) && false == CFileIo::recursiveMakeDir( $strDestinationPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
			$boolIsValid &= false;
		}

		if( false == CFileIo::copyFile( $strSourceFilePath, $strDestinationPath . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File could not copied.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public static function loadEntrataDocumentPath( $intCid ) {
		return getMountsPath( $intCid, PATH_MOUNTS_FILES );
	}

	public function deleteDocument( $strFolderPath = NULL ) {
    	if( false == is_null( $this->getFilePath() )
		    && false == is_null( $this->getFileName() )
		    && file_exists( $strFolderPath . $this->getFilePath() . $this->getFileName() ) ) {

			unlink( $strFolderPath . $this->getFilePath() . $this->getFileName() );

		}
	}

}

?>