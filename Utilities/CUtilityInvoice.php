<?php

class CUtilityInvoice extends CBaseUtilityInvoice {

	const FILE_PATH = 'documents/utility_billing/utility_invoices/';
	const VALIDATE_INSERT_FROM_BILL_PROCESS = 'insert_from_bill_process';

	protected $m_boolIsSubmetered;
	protected $m_boolIsDisableInvoiceSetting;
	protected $m_boolHideResidentInvoiceSpanish;
	protected $m_boolHideResidentInvoiceBillingDisputes;

	protected $m_intGraceDays;
	protected $m_intCustomerId;
	protected $m_intOccupantCount;
	protected $m_intMaxConsumption;
	protected $m_intMinConsumption;
	protected $m_intAvgConsumption;
	protected $m_intCallPhoneNumber;
	protected $m_intUtilityRubsFormulaId;
	protected $m_intPropertyUtilityTypeId;

	protected $m_fltMinAmount;
	protected $m_fltMaxAmount;
	protected $m_fltAvgAmount;

	protected $m_strPropertyName;
	protected $m_strLeaseEndDate;
	protected $m_strLeaseStartDate;
	protected $m_strUtilityTypeName;
	protected $m_strCustomerEmailAddress;
	protected $m_strUtilityAccountNameLast;
	protected $m_strUtilityAccountNameFirst;
	protected $m_strMoveOutDate;

	protected $m_objUtilityNotice;
	protected $m_objPrimaryAddress;
	protected $m_objPaymentAddress;
	protected $m_objUtilityAccount;
	protected $m_objPropertyWebsite;
	protected $m_objCompanyMediaData;
	protected $m_objPropertyPhoneNumber;
	protected $m_objPropertyEmailAddress;
	protected $m_objUtilityInvoiceMessage;
	protected $m_objUtilityPropertyUnitAddres;
	protected $m_objUtilityServicePropertyUnitAddress;

	protected $m_arrintUtilityBillIds;
	protected $m_arrintUtilityTransactionIds;

	protected $m_arrmixHeaderFooterData;

	protected $m_arrobjUtilityTransactions;

	/**
	 * Get Functions
	 */

	public function getUtilityTransactions() {
		return $this->m_arrobjUtilityTransactions;
	}

	public function getPrimaryAddress() {
		return $this->m_objPrimaryAddress;
	}

	public function getPaymentAddress() {
		return $this->m_objPaymentAddress;
	}

	public function getCompanyMediaData() {
		return $this->m_objCompanyMediaData;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getCallPhoneNumber() {
		return $this->m_intCallPhoneNumber;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPropertyWebsite() {
		return $this->m_objPropertyWebsite;
	}

	public function getUtilityAccountNameFirst() {

		$strNameFirst = $this->m_strUtilityAccountNameFirst;

		if( 'no name' == \Psi\CStringService::singleton()->strtolower( $strNameFirst ) ) {
			$strNameFirst = '';
		}

		return $strNameFirst;
	}

	public function getUtilityAccountNameLast() {

		$strNameLast = $this->m_strUtilityAccountNameLast;

		if( 'no name' == \Psi\CStringService::singleton()->strtolower( $strNameLast ) ) {
			$strNameLast = '';
		}

		return $strNameLast;
	}

	public function getCustomerEmailAddress() {
		return $this->m_strCustomerEmailAddress;
	}

	public function getUtilityTransactionIds() {
		return $this->m_arrintUtilityTransactionIds;
	}

	public function getHeaderFooterData() {
		return $this->m_arrmixHeaderFooterData;
	}

	public function getUtilityNotice() {
		return $this->m_objUtilityNotice;
	}

	public function getUtilityRubsFormulaId() {
		return $this->m_intUtilityRubsFormulaId;
	}

	public function getUtilityTypeName() {
		return $this->m_strUtilityTypeName;
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function getMinAmount() {
		return $this->m_fltMinAmount;
	}

	public function getMaxAmount() {
		return $this->m_fltMaxAmount;
	}

	public function getAvgAmount() {
		return $this->m_fltAvgAmount;
	}

	public function getOccupantCount() {
		return $this->m_intOccupantCount;
	}

	public function getUtilityAccount() {
		return $this->m_objUtilityAccount;
	}

	public function getGraceDays() {
		return $this->m_intGraceDays;
	}

	public function getHideResidentInvoiceSpanish() {
		return $this->m_boolHideResidentInvoiceSpanish;
	}

	public function getHideResidentInvoiceBillingDisputes() {
		return $this->m_boolHideResidentInvoiceBillingDisputes;
	}

	public function getPropertyPhoneNumber() {
		return $this->m_objPropertyPhoneNumber;
	}

	public function getPropertyEmailAddress() {
		return $this->m_objPropertyEmailAddress;
	}

	public function getUtilityPropertyUnitAddress() {
		return $this->m_objUtilityPropertyUnitAddres;
	}

    public function getUtilityServicePropertyUnitAddress() {
        return $this->m_objUtilityServicePropertyUnitAddress;
    }

	public function getIsSubmetered() {
		return $this->m_boolIsSubmetered;
	}

	public function getIsDisableInvoiceSetting() {
		return $this->m_boolIsDisableInvoiceSetting;
	}

	public function getMaxConsumption() {
		return $this->m_intMaxConsumption;
	}

	public function getMinConsumption() {
		return $this->m_intMinConsumption;
	}

	public function getAvgConsumption() {
		return $this->m_intAvgConsumption;
	}

	public function getUtilityInvoiceMessage() {
		return $this->m_objUtilityInvoiceMessage;
	}

	public function getUtilityBillIds() {
		return $this->m_arrintUtilityBillIds;
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_account_name_first'] ) ) {
			$this->setUtilityAccountNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_account_name_first'] ) : $arrmixValues['utility_account_name_first'] );
		}
		if( true == isset( $arrmixValues['utility_account_name_last'] ) ) {
			$this->setUtilityAccountNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_account_name_last'] ) : $arrmixValues['utility_account_name_last'] );
		}
		if( true == isset( $arrmixValues['customer_id'] ) ) {
			$this->setCustomerId( $arrmixValues['customer_id'] );
		}
		if( true == isset( $arrmixValues['utility_rubs_formula_id'] ) ) {
			$this->setUtilityRubsFormulaId( $arrmixValues['utility_rubs_formula_id'] );
		}
		if( true == isset( $arrmixValues['utility_type_name'] ) ) {
			$this->setUtilityTypeName( $arrmixValues['utility_type_name'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_id'] ) ) {
			$this->setPropertyUtilityTypeId( $arrmixValues['property_utility_type_id'] );
		}
		if( true == isset( $arrmixValues['property_unit_id'] ) ) {
			$this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		}
		if( true == isset( $arrmixValues['lease_start_date'] ) ) {
			$this->setLeaseStartDate( $arrmixValues['lease_start_date'] );
		}
		if( true == isset( $arrmixValues['lease_end_date'] ) ) {
			$this->setLeaseEndDate( $arrmixValues['lease_end_date'] );
		}
		if( true == isset( $arrmixValues['min_amount'] ) ) {
			$this->setMinAmount( $arrmixValues['min_amount'] );
		}
		if( true == isset( $arrmixValues['max_amount'] ) ) {
			$this->setMaxAmount( $arrmixValues['max_amount'] );
		}
		if( true == isset( $arrmixValues['avg_amount'] ) ) {
			$this->setAvgAmount( $arrmixValues['avg_amount'] );
		}
		if( true == isset( $arrmixValues['occupant_count'] ) ) {
			$this->setOccupantCount( $arrmixValues['occupant_count'] );
		}
		if( true == isset( $arrmixValues['is_submetered'] ) ) {
			$this->setIsSubmetered( $arrmixValues['is_submetered'] );
		}
		if( true == isset( $arrmixValues['min_consumption'] ) ) {
			$this->setMinConsumption( $arrmixValues['min_consumption'] );
		}
		if( true == isset( $arrmixValues['max_consumption'] ) ) {
			$this->setMaxConsumption( $arrmixValues['max_consumption'] );
		}
		if( true == isset( $arrmixValues['avg_consumption'] ) ) {
			$this->setAvgConsumption( $arrmixValues['avg_consumption'] );
		}
		if( true == isset( $arrmixValues['move_out_date'] ) ) {
			$this->setMoveOutDate( $arrmixValues['move_out_date'] );
		}
	}

	public function setUtilityTransactions( $arrobjUtilityTransactions ) {
		$this->m_arrobjUtilityTransactions = $arrobjUtilityTransactions;
	}

	public function setPrimaryAddress( $objPrimaryAddress ) {
		$this->m_objPrimaryAddress = $objPrimaryAddress;
	}

	public function setPaymentAddress( $objPaymentAddress ) {
		$this->m_objPaymentAddress = $objPaymentAddress;
	}

	public function setCompanyMediaData( $objCompanyMediaData ) {
		$this->m_objCompanyMediaData = $objCompanyMediaData;
	}

 	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = CStrings::strToIntDef( $intCustomerId );
	}

	public function setCallPhoneNumber( $intCallPhoneNumber ) {
		$this->m_intCallPhoneNumber = $intCallPhoneNumber;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setPropertyWebsite( $objPropertyWebsite ) {
		$this->m_objPropertyWebsite = $objPropertyWebsite;
	}

	public function setUtilityAccountNameFirst( $strUtilityAccountNameFirst ) {
		$this->m_strUtilityAccountNameFirst = CStrings::strTrimDef( $strUtilityAccountNameFirst, 50, NULL, true );
	}

	public function setUtilityAccountNameLast( $strUtilityAccountNameLast ) {
		$this->m_strUtilityAccountNameLast = CStrings::strTrimDef( $strUtilityAccountNameLast, 50, NULL, true );
	}

	public function setCustomerEmailAddress( $strCustomerEmailAddress ) {
		$this->m_strCustomerEmailAddress = CStrings::strTrimDef( $strCustomerEmailAddress, 50, NULL, true );
	}

	public function setUtilityTransactionIds( $arrintUtilityTransactionIds ) {
		$this->m_arrintUtilityTransactionIds = $arrintUtilityTransactionIds;
	}

	public function setHeaderFooterData( $arrmixHeaderFooterData ) {
		$this->m_arrmixHeaderFooterData = $arrmixHeaderFooterData;
	}

	public function setUtilityNotice( $objUtilityNotice ) {
		$this->m_objUtilityNotice = $objUtilityNotice;
	}

	public function setUtilityRubsFormulaId( $intUtilityRubsFormulaId ) {
		$this->m_intUtilityRubsFormulaId = $intUtilityRubsFormulaId;
	}

	public function setUtilityTypeName( $strUtilityTypeName ) {
		$this->m_strUtilityTypeName = $strUtilityTypeName;
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->m_intPropertyUtilityTypeId = CStrings::strToIntDef( $intPropertyUtilityTypeId );
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->m_strLeaseStartDate = $strLeaseStartDate;
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->m_strLeaseEndDate = $strLeaseEndDate;
	}

	public function setMinAmount( $fltMinAmount ) {
		$this->m_fltMinAmount = $fltMinAmount;
	}

	public function setMaxAmount( $fltMaxAmount ) {
		$this->m_fltMaxAmount = $fltMaxAmount;
	}

	public function setAvgAmount( $fltAvgAmount ) {
		$this->m_fltAvgAmount = $fltAvgAmount;
	}

	public function setOccupantCount( $intOccupantCount ) {
		$this->m_intOccupantCount = $intOccupantCount;
	}

	public function setUtilityAccount( $objUtilityAccount ) {
		$this->m_objUtilityAccount = $objUtilityAccount;
	}

 	public function setGraceDays( $intGraceDays ) {
		$this->m_intGraceDays = CStrings::strToIntDef( $intGraceDays );
	}

	public function setHideResidentInvoiceSpanish( $boolHideResidentInvoiceSpanish ) {
		$this->m_boolHideResidentInvoiceSpanish = $boolHideResidentInvoiceSpanish;
	}

	public function setHideResidentInvoiceBillingDisputes( $boolHideResidentInvoiceBillingDisputes ) {
		$this->m_boolHideResidentInvoiceBillingDisputes = $boolHideResidentInvoiceBillingDisputes;
	}

	public function setPropertyPhoneNumber( $objPropertyPhoneNumber ) {
		$this->m_objPropertyPhoneNumber = $objPropertyPhoneNumber;
	}

	public function setPropertyEmailAddress( $objPropertyEmailAddress ) {
		$this->m_objPropertyEmailAddress = $objPropertyEmailAddress;
	}

	public function setIsConvergent( $boolIsConvergent ) {
		$this->m_boolIsConvergent = CStrings::strToBool( $boolIsConvergent );
	}

	public function setUtilityPropertyUnitAddress( $objUtilityPropertyUnitAddress ) {
		$this->m_objUtilityPropertyUnitAddres = $objUtilityPropertyUnitAddress;
	}

    public function setUtilityServicePropertyUnitAddress( $objUtilityServicePropertyUnitAddress ) {
        $this->m_objUtilityServicePropertyUnitAddress = $objUtilityServicePropertyUnitAddress;
    }

	public function setIsSubmetered( $boolIsSubmetered ) {
		$this->m_boolIsSubmetered = CStrings::strToBool( $boolIsSubmetered );
	}

	public function setIsDisableInvoiceSetting( $boolIsDisableInvoiceSetting ) {
		$this->m_boolIsDisableInvoiceSetting = CStrings::strToBool( $boolIsDisableInvoiceSetting );
	}

	public function setMaxConsumption( $intMaxConsumption ) {
		$this->m_intMaxConsumption = $intMaxConsumption;
	}

	public function setMinConsumption( $intMinConsumption ) {
		$this->m_intMinConsumption = $intMinConsumption;
	}

	public function setAvgConsumption( $intAvgConsumption ) {
		$this->m_intAvgConsumption = $intAvgConsumption;
	}

	public function setUtilityInvoiceMessage( $objUtilityInvoiceMessage ) {
		$this->m_objUtilityInvoiceMessage = $objUtilityInvoiceMessage;
	}

	public function setUtilityBillIds( $arrintUtilityBillIds ) {
		$this->m_arrintUtilityBillIds = $arrintUtilityBillIds;
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->m_strMoveOutDate = $strMoveOutDate;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Please set client id.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Please set property id.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_account_id', 'Please set utility account id.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityBatchId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityBatchId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_batch_id', 'Please set batch id.' ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_date', 'Please set Invoice date.' ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceDueDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceDueDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_due_date', 'Please set invoice due date.' ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_amount', 'Please set Invoice Amount.' ) );
		}

		return $boolIsValid;
	}

	public function valUnitNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_number', 'Please set unit number for utility account - ' . $this->getUtilityAccountId() . '.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valUtilityAccountId();
				$boolIsValid &= $this->valInvoiceDate();
				$boolIsValid &= $this->valInvoiceAmount();
				$boolIsValid &= $this->valUnitNumber();
				$boolIsValid &= $this->valUtilityBatchId();
				break;

			case self::VALIDATE_INSERT_FROM_BILL_PROCESS:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valUtilityAccountId();
				$boolIsValid &= $this->valInvoiceDate();
				$boolIsValid &= $this->valInvoiceAmount();
				$boolIsValid &= $this->valUnitNumber();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function cloneUtilityTransations( $intCurrentUserId, $objDatabase ) {

		$arrobjCloneUtilityTransactions = $arrobjOriginalUtilityTransactions = [];

		$arrintUtilityTransactionTypes = [
			CUtilityTransactionType::VCR_USAGE_FEE,
 			CUtilityTransactionType::VCR_SERVICE_FEE
		];

		$arrobjUtilityTransactions = $this->getUtilityTransactions();

		$arrobjRekeyedUtilityTransactions = rekeyObjects( 'OriginalUtilityTransactionId', $arrobjUtilityTransactions );

		if( false == valArr( $arrobjUtilityTransactions ) ) {
			return false;
		}

		foreach( $arrobjUtilityTransactions as $objUtilityTransaction ) {

			// changes for cloning transaction
			// 1.Make the Clone of the Main transactions only for both normal and VCR transactions as well.
			// 2. if already transaction is cloned then set the main transactions is_original = 0 and it's 1st clone will be is_original = 1

			// to make the copy of Main transactions and add new transaction's row

			// if already clone transaction then skip it dont consider
			if( false == is_null( $objUtilityTransaction->getOriginalUtilityTransactionId() ) || false == is_null( $objUtilityTransaction->getUtilityInvoiceId() ) ) {
				continue;
			}

			// Dont create cloned trasanctions for move out invoices and disabled covergent billing and previous balance
			if( false == is_null( $this->getUtilityBatchId() ) && ( ( false == $this->getIsConvergent() && $objUtilityTransaction->getUtilityBatchId() != $this->getUtilityBatchId() ) || ( CUtilityInvoiceType::MOVE_OUT == $this->getUtilityInvoiceTypeId() && $objUtilityTransaction->getUtilityBatchId() != $this->getUtilityBatchId() ) ) ) {
				continue;
			}

			if( true == in_array( $objUtilityTransaction->getUtilityTransactionTypeId(), $arrintUtilityTransactionTypes ) && false == is_null( $objUtilityTransaction->getScheduledExportDate() ) ) {
				continue;
			}

			// make clone of the Original transactions only.
			$objCloneUtilityTransaction = clone $objUtilityTransaction;

			// Set the original_transaction_id of the copied transaction
			$objCloneUtilityTransaction->setOriginalUtilityTransactionId( $objUtilityTransaction->getId() );
			$objCloneUtilityTransaction->setUtilityInvoiceId( $this->getId() );
			$objCloneUtilityTransaction->setId( NULL );
			$objCloneUtilityTransaction->setIsOriginal( false );
			$objCloneUtilityTransaction->setScheduledExportDate( NULL );

			// set is_original = false for original transaction
			$objUtilityTransaction->setIsOriginal( false );

			$objKeyedUtilityTransaction = getArrayElementByKey( $objUtilityTransaction->getId(), $arrobjRekeyedUtilityTransactions );

			// if it first clone then set is_original = 1 else it will always 0 for other clones
			if( false == valObj( $objKeyedUtilityTransaction, 'CUtilityTransaction' ) ) {
				$objCloneUtilityTransaction->setIsOriginal( true );
			}

			$arrobjCloneUtilityTransactions[] 		= $objCloneUtilityTransaction;
			$arrobjOriginalUtilityTransactions[] 	= $objUtilityTransaction;
		}

		switch( NULL ) {
			default:

				if( false == valArr( $arrobjCloneUtilityTransactions ) || false == valArr( $arrobjOriginalUtilityTransactions ) ) {
					break;
				}

				$boolIsValid = true;

				foreach( $arrobjCloneUtilityTransactions as $objUtilityTransaction ) {
					if( false == $objUtilityTransaction->validate( VALIDATE_INSERT, $objDatabase ) ) {
						foreach( $objUtilityTransaction->getErrorMsgs() as $objErrorMsg ) {
							$this->addErrorMsg( $objErrorMsg );
						}
						$boolIsValid &= false;
					}
				}

				foreach( $arrobjOriginalUtilityTransactions as $objUtilityTransaction ) {
					if( false == $objUtilityTransaction->validate( VALIDATE_UPDATE, $objDatabase ) ) {
						foreach( $objUtilityTransaction->getErrorMsgs() as $objErrorMsg ) {
							$this->addErrorMsg( $objErrorMsg );
						}
						$boolIsValid &= false;
					}
				}

				if( false == $boolIsValid ) {
					break;
				}

				// inserting the cloned transactions
				foreach( $arrobjCloneUtilityTransactions as $objUtilityTransaction ) {
					if( false == $objUtilityTransaction->insert( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsg( $objUtilityTransaction->getErrorMsgs() );
						break 2;
					}
				}

				foreach( $arrobjOriginalUtilityTransactions as $objUtilityTransaction ) {
					if( false == $objUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsg( $objUtilityTransaction->getErrorMsgs() );
						break 2;
					}
				}

				return true;
		}

		return false;
	}

}
?>