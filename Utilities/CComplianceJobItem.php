<?php

class CComplianceJobItem extends CBaseComplianceJobItem {

	protected $m_intApPayeeId;
	protected $m_intComplianceLevelId;
	protected $m_intComplianceTypeId;
	protected $m_intScreeningPackageId;

	protected $m_boolIsTestVendor;
	protected $m_boolValidateExpiration;

	protected $m_strWorkerNameLast;
	protected $m_strWorkerNameFirst;
	protected $m_strComplianceItemName;
	protected $m_strComplianceStatusName;

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valPropertyId() {
		return true;
	}

	public function valVendorInvitationId() {
		return true;
	}

	public function valComplianceJobId() {
		return true;
	}

	public function valComplianceRuleId() {
		return true;
	}

	public function valComplianceStatusId() {
		return true;
	}

	public function valEntrataReferenceId() {
		return true;
	}

	public function valVendorReferenceId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valInviteSystemEmailId() {
		return true;
	}

	public function valFailureDescription() {
		return true;
	}

	public function valIsActive() {
		return true;
	}

	public function valLastRequestedOn() {
		return true;
	}

	public function valLastApprovedOn() {
		return true;
	}

	public function valInviteEmailSentOn() {
		return true;
	}

	public function valFailedBy() {
		return true;
	}

	public function valFailedOn() {
		return true;
	}

	public function valApprovedBy() {
		return true;
	}

	public function valApprovedOn() {
		return true;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function valComplianceItemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setComplianceLevelId( $intComplianceLevelId ) {
		$this->m_intComplianceLevelId = $intComplianceLevelId;
	}

	public function setComplianceItemName( $strComplianceItemName ) {
		$this->m_strComplianceItemName = $strComplianceItemName;
	}

	public function setComplianceTypeId( $intComplianceTypeId ) {
		$this->m_intComplianceTypeId = $intComplianceTypeId;
	}

	public function setValidateExpiration( $boolValidateExpiration ) {
		$this->m_boolValidateExpiration = CStrings::strToBool( $boolValidateExpiration );
	}

	public function setComplianceStatusName( $strComplianceStatusName ) {
		$this->m_strComplianceStatusName = $strComplianceStatusName;
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->m_intApPayeeId = $intApPayeeId;
	}

	public function setScreeningPackageId( $intScreeningPackageId ) {
		$this->m_intScreeningPackageId = $intScreeningPackageId;
	}

	public function setWorkerNameFirst( $strWorkerNameFirst ) {
		$this->m_strWorkerNameFirst = CStrings::strTrimDef( $strWorkerNameFirst );
	}

	public function setWorkerNameLast( $strWorkerNameLast ) {
		$this->m_strWorkerNameLast = CStrings::strTrimDef( $strWorkerNameLast );
	}

	public function setIsTestVendor( $boolIsTestVendor ) {
		$this->m_boolIsTestVendor = CStrings::strToBool( $boolIsTestVendor );
	}

	/**
	 * Get Functions
	 *
	 */

	public function getComplianceLevelId() {
		return $this->m_intComplianceLevelId;
	}

	public function getComplianceItemName() {
		return $this->m_strComplianceItemName;
	}

	public function getComplianceTypeId() {
		return $this->m_intComplianceTypeId;
	}

	public function getValidateExpiration() {
		return $this->m_boolValidateExpiration;
	}

	public function getComplianceStatusName() {
		return $this->m_strComplianceStatusName;
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function getScreeningPackageId() {
		return $this->m_intScreeningPackageId;
	}

	public function getWorkerNameFirst() {
		return $this->m_strWorkerNameFirst;
	}

	public function getWorkerNameLast() {
		return $this->m_strWorkerNameLast;
	}

	public function getFullName() {
		return $this->m_strWorkerNameFirst . ' ' . $this->m_strWorkerNameLast;
	}

	public function getIsTestVendor() {
		return $this->m_boolIsTestVendor;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['compliance_level_id'] ) && $boolDirectSet ) {
			$this->m_intComplianceLevelId = trim( $arrmixValues['compliance_level_id'] );
		} elseif( isset( $arrmixValues['compliance_level_id'] ) ) {
			$this->setComplianceLevelId( $arrmixValues['compliance_level_id'] );
		}

		if( isset( $arrmixValues['compliance_item_name'] ) && $boolDirectSet ) {
			$this->m_strComplianceItemName = trim( $arrmixValues['compliance_item_name'] );
		} elseif( isset( $arrmixValues['compliance_item_name'] ) ) {
			$this->setComplianceItemName( $arrmixValues['compliance_item_name'] );
		}

		if( isset( $arrmixValues['compliance_type_id'] ) && $boolDirectSet ) {
			$this->m_intComplianceTypeId = trim( $arrmixValues['compliance_type_id'] );
		} elseif( isset( $arrmixValues['compliance_type_id'] ) ) {
			$this->setComplianceTypeId( $arrmixValues['compliance_type_id'] );
		}

		if( isset( $arrmixValues['validate_expiration'] ) && $boolDirectSet ) {
			$this->m_boolValidateExpiration = trim( $arrmixValues['validate_expiration'] );
		} elseif( isset( $arrmixValues['validate_expiration'] ) ) {
			$this->setValidateExpiration( $arrmixValues['validate_expiration'] );
		}

		if( isset( $arrmixValues['compliance_status_name'] ) && $boolDirectSet ) {
			$this->m_strComplianceStatusName = trim( $arrmixValues['compliance_status_name'] );
		} elseif( isset( $arrmixValues['compliance_status_name'] ) ) {
			$this->setComplianceStatusName( $arrmixValues['compliance_status_name'] );
		}

		if( isset( $arrmixValues['ap_payee_id'] ) && $boolDirectSet ) {
			$this->m_intApPayeeId = trim( $arrmixValues['ap_payee_id'] );
		} elseif( isset( $arrmixValues['ap_payee_id'] ) ) {
			$this->setApPayeeId( $arrmixValues['ap_payee_id'] );
		}

		if( isset( $arrmixValues['screening_package_id'] ) && $boolDirectSet ) {
			$this->m_intScreeningPackageId = trim( $arrmixValues['screening_package_id'] );
		} elseif( isset( $arrmixValues['screening_package_id'] ) ) {
			$this->setScreeningPackageId( $arrmixValues['screening_package_id'] );
		}

		if( isset( $arrmixValues['worker_name_first'] ) && $boolDirectSet ) {
			$this->m_strWorkerNameFirst = trim( $arrmixValues['worker_name_first'] );
		} elseif( isset( $arrmixValues['worker_name_first'] ) ) {
			$this->setWorkerNameFirst( $arrmixValues['worker_name_first'] );
		}

		if( isset( $arrmixValues['worker_name_last'] ) && $boolDirectSet ) {
			$this->m_strWorkerNameLast = trim( $arrmixValues['worker_name_last'] );
		} elseif( isset( $arrmixValues['worker_name_last'] ) ) {
			$this->setWorkerNameLast( $arrmixValues['worker_name_last'] );
		}

		if( isset( $arrmixValues['is_test_vendor'] ) && $boolDirectSet ) {
			$this->m_boolIsTestVendor = trim( $arrmixValues['is_test_vendor'] );
		} elseif( isset( $arrmixValues['is_test_vendor'] ) ) {
			$this->setIsTestVendor( $arrmixValues['is_test_vendor'] );
		}
	}

	public function delete( $intUserId, $objDatabase, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase );
		}

    	return $this->update( $intUserId, $objDatabase );
    }

    public function customInsert( $intCurrentUserId, &$strJobItemsSqlString ) {

    	$strId = ( true == is_null( $this->getId() ) ) ? 'nextval( \'public.compliance_job_items_id_seq\' )' : $this->sqlId();

    	$strComplianceJobId = $this->getComplianceJobId();

    	if( false == valId( $strComplianceJobId ) ) {
			$strComplianceJobId = 'currval( \'public.compliance_jobs_id_seq\' )';
	    }

		if( '' == $strJobItemsSqlString ) {

			$strJobItemsSqlString = 'INSERT INTO
			public.compliance_job_items
				VALUES ( ' .
    				$strId . ', ' .
    				$this->sqlCid() . ', ' .
    				$this->sqlPropertyId() . ', ' .
    				$this->sqlVendorInvitationId() . ', ' .
    				$strComplianceJobId . ', ' .
    				$this->sqlComplianceRuleId() . ', ' .
    				$this->sqlComplianceStatusId() . ', ' .
					$this->sqlEntrataReferenceId() . ', ' .
					$this->sqlVendorReferenceId() . ', ' .
					$this->sqlVendorId() . ', ' .
					$this->sqlSystemEmailId() . ', ' .
					$this->sqlFailureDescription() . ', ' .
					$this->sqlIsActive() . ', ' .
					$this->sqlLastRequestedOn() . ', ' .
					$this->sqlLastApprovedOn() . ', ' .
					$this->sqlFailedBy() . ', ' .
					$this->sqlFailedOn() . ', ' .
					$this->sqlApprovedBy() . ', ' .
					$this->sqlApprovedOn() . ', ' .
					$this->sqlDeletedBy() . ', ' .
					$this->sqlDeletedOn() . ', ' .
					( int ) $intCurrentUserId . ', ' .
					$this->sqlUpdatedOn() . ', ' .
					( int ) $intCurrentUserId . ', ' .
			        $this->sqlCreatedOn() . ', ' .
			        $this->sqlComplianceItemId() . ' );';
		} else {

			$strJobItemsSqlString = preg_replace( '/;$/', ',', $strJobItemsSqlString );

			$strJobItemsSqlString .= ' ( ' .
    				$strId . ', ' .
    				$this->sqlCid() . ', ' .
    				$this->sqlPropertyId() . ', ' .
    				$this->sqlVendorInvitationId() . ', ' .
    				$strComplianceJobId . ', ' .
    				$this->sqlComplianceRuleId() . ', ' .
    				$this->sqlComplianceStatusId() . ', ' .
				$this->sqlEntrataReferenceId() . ', ' .
				$this->sqlVendorReferenceId() . ', ' .
				$this->sqlVendorId() . ', ' .
				$this->sqlSystemEmailId() . ', ' .
				$this->sqlFailureDescription() . ', ' .
				$this->sqlIsActive() . ', ' .
				$this->sqlLastRequestedOn() . ', ' .
				$this->sqlLastApprovedOn() . ', ' .
				$this->sqlFailedBy() . ', ' .
				$this->sqlFailedOn() . ', ' .
				$this->sqlApprovedBy() . ', ' .
				$this->sqlApprovedOn() . ', ' .
				$this->sqlDeletedBy() . ', ' .
				$this->sqlDeletedOn() . ', ' .
				( int ) $intCurrentUserId . ', ' .
				$this->sqlUpdatedOn() . ', ' .
				( int ) $intCurrentUserId . ', ' .
			    $this->sqlCreatedOn() . ', ' .
				$this->sqlComplianceItemId() . ' );';
		}
	}

	public function createComplianceDoc( $boolIsVendorDoc = false ) {

		$objComplianceDoc = new CComplianceDoc();

		if( false == $boolIsVendorDoc ) {
			$objComplianceDoc->setCid( $this->getCid() );
			$objComplianceDoc->setApPayeeId( $this->getApPayeeId() );
		}

		$objComplianceDoc->setVendorId( $this->getVendorId() );
		$objComplianceDoc->setComplianceItemId( $this->getComplianceItemId() );
		$objComplianceDoc->setDocDatetime( 'NOW()' );
		$objComplianceDoc->setIsVendorDoc( $boolIsVendorDoc );

		return $objComplianceDoc;
	}

	public function createComplianceDocAssociation( $boolIsVendorDoc = false ) {

		$objComplianceDocAssociation = new CComplianceDocAssociation();

		if( false == $boolIsVendorDoc ) {
			$objComplianceDocAssociation->setCid( $this->getCid() );
			$objComplianceDocAssociation->setEntrataReferenceId( $this->getEntrataReferenceId() );
		}

		$objComplianceDocAssociation->setComplianceLevelId( $this->getComplianceLevelId() );
		$objComplianceDocAssociation->setVendorReferenceId( $this->getVendorReferenceId() );

		return $objComplianceDocAssociation;
	}

}
?>