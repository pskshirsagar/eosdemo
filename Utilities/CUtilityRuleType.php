<?php

class CUtilityRuleType extends CBaseUtilityRuleType {

	const DONT_POST_TEMPORARY_CHARGES = 39;

	protected $m_intAssociateValuesCount;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
    	if( true == isset( $arrmixValues['associate_values_count'] ) ) {
    		$this->setAssociateValuesCount( $arrmixValues['associate_values_count'] );
	    }
    }

    public function valName( $strAction, $objDatabase ) {

        $intRuleTypeCount = 0;
    	$boolIsValid	  = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Rule type name is required.' ) );
        } elseif( false == is_null( $this->getName() ) && true == preg_match( '/[\@|\#|\$|\%|\^|\&|\*|\(|\)]/', $this->getName() ) ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Invalid rule type name.' ) );
        }

        if( VALIDATE_UPDATE == $strAction ) {
        	$intRuleTypeCount = 1;
        }

        if( $intRuleTypeCount < \Psi\Eos\Utilities\CUtilityRuleTypes::createService()->fetchUtilityRuleTypeCount( 'WHERE name=\'' . addslashes( $this->getName() ) . '\'', $objDatabase ) ) {
        	$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Rule type name already in used.' ) );
        }

        return $boolIsValid;
    }

	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		$arrobjUtilityRules = \Psi\Eos\Utilities\CUtilityRules::createService()->fetchUtilityRulesByUtilityRuleTypeId( $this->getId(), $objDatabase );

		if( true == valArr( $arrobjUtilityRules ) ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_rules', 'Failed to delete the utility rule type, it is being associated with utility rule(s).' ) );
		}

		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $strAction, $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valDependantInformation( $objDatabase );
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

 	public function getAssociateValuesCount() {
   		return $this->m_intAssociateValuesCount;
    }

	public function setAssociateValuesCount( $intAssociateValuesCount ) {
   		return $this->m_intAssociateValuesCount = $intAssociateValuesCount;
    }

}

?>