<?php

class CWebRetrievalEscalationType extends CBaseWebRetrievalEscalationType {

	const URL_INCORRECT 							= 1;
	const CREDENTIALS_INCORRECT						= 2;
	const SECRET_QUESTION							= 3;
	const THREE_ATTEMPTS							= 4;
	const OTHER										= 5;
	const WE_ALREADY_HAVE_THE_BILL_IN_THE_SYSTEM	= 6;
	const TWO_FACTOR_AUTHENTICATION					= 7;
	const USERNAME_PASSWORD_INCORRECT				= 8;
	const ACCOUNT_ACCESS_LOCKED						= 9;
	const ACCOUNT_NOT_FOUND							= 10;
	const FALSE_TRIGGER								= 11;
	const WEBSITE_ISSUE_ERROR						= 12;
	const UNABLE_TO_DOWNLOAD_BILL					= 13;
	const ACCOUNT_CLOSED							= 14;
	const LAST_BILL_WAS_FINAL_BILL					= 15;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static $c_arrintUtilityCredentialEscalationTypes = [
		self::URL_INCORRECT,
		self::CREDENTIALS_INCORRECT,
		self::SECRET_QUESTION,
		self::TWO_FACTOR_AUTHENTICATION
	];

}
?>