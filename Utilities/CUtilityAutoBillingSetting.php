<?php

class CUtilityAutoBillingSetting extends CBaseUtilityAutoBillingSetting {

	const TABLE_NAME_UTILITY_AUTO_BILLING_SETTINGS = 'utility_auto_billing_settings';

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		// utility Auto Billing setting
		$arrstrTempUtilityAutoBillingSettingOriginalValues = $arrstrUtilityAutoBillingSettingOriginalValueChanges = $arrstrUtilityAutoBillingSettingOriginalValues = [];
		$boolUtilityAutoBillingSettingChangeFlag = false;

		$objUtilityAutoBillingSetting = new CUtilityAutoBillingSetting();
		$arrmixUtilityAutoBillingSettingOriginalValues = \Psi\Eos\Utilities\CUtilityAutoBillingSettings::createService()->fetchUtilityAutoBillingSettingsById( $this->getPropertyUtilitySettingId(), $objDatabase );

		$arrstrTempUtilityAutoBillingSettingOriginalValues = $arrmixUtilityAutoBillingSettingOriginalValues[0];

		foreach( $arrstrTempUtilityAutoBillingSettingOriginalValues as $strKey => $mixUtilityAutoBillingSettingOriginalValue ) {

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strGetFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strSetFunctionName = 'set' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			$objUtilityAutoBillingSetting->$strSetFunctionName( $mixUtilityAutoBillingSettingOriginalValue );

			if( $this->$strGetFunctionName() != $objUtilityAutoBillingSetting->$strGetFunctionName() ) {
				$arrstrUtilityAutoBillingSettingOriginalValueChanges[$strKey] 	= $this->$strSqlFunctionName();
				$arrstrUtilityAutoBillingSettingOriginalValues[$strKey] 		= $objUtilityAutoBillingSetting->$strSqlFunctionName();
				$boolUtilityAutoBillingSettingChangeFlag 						= true;
			}
		}

		$boolUtilityAutoBillingSettingUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUtilityAutoBillingSettingUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolUtilityAutoBillingSettingChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempUtilityAutoBillingSettingOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempUtilityAutoBillingSettingOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( self::TABLE_NAME_UTILITY_AUTO_BILLING_SETTINGS );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrUtilityAutoBillingSettingOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrUtilityAutoBillingSettingOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		return $boolUtilityAutoBillingSettingUpdateStatus;
	}

}
?>