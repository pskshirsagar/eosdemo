<?php

use Psi\Eos\Entrata\CCompanyEmployees;
use Psi\Eos\Entrata\CUnitSpaces;

class CPropertyUtilitySetting extends CBasePropertyUtilitySetting {

	const MAIL							= 2;
	const EMAIL							= 1;
	const REMOTE_FOREIGN_KEY_DELIMITER	= '~..~';

	const VALIDATE_UPDATE_PROPERTY_TAB = 'validate_update_property_tab';
	const VALIDATE_UPDATE_BILLING_TAB = 'validate_update_billing_tab';
	const VALIDATE_UPDATE_PROPERTY_UTILITY_INFO = 'validate_update_property_utility_info';
	const PUT_UBA_TOTAL_VALIDATION_COUNT = 2;

	protected $m_boolIsDisabled;
	protected $m_boolIsRuProperty;
	protected $m_boolIsIpProperty;
	protected $m_boolIsUemProperty;
	protected $m_boolUseInvoiceCount;
	protected $m_boolIsShowUnitDetails;
	protected $m_boolAutoApprovePreBill;
	protected $m_boolIsSkipUnitCountValidation;
	protected $m_boolReallocateProratedExpense;

	protected $m_intPropertyTypeId;
	protected $m_intUtilityRuleCount;
	protected $m_intUtilityBillAccountId;
	protected $m_intOcrFailedAnomalyCount;
	protected $m_intUnprocessedBillsCount;
	protected $m_intLastOpenUtilityBatchId;
	protected $m_intAdminCompanyEmployeeId;
	protected $m_intExcessUsageAnomalyCount;
	protected $m_intLastCompleteUtilityBatchId;
	protected $m_intMissingVcrBillAnomalyCount;
	protected $m_intMissingBillEscalationsCount;
	protected $m_intUnprocessedVcrUtilityBillCount;
	protected $m_intMissingBillingBillAnomalyCount;
	protected $m_intMissingBillPayBillAnomalyCount;
	protected $m_intBillPayUtilityBillAccountsCount;
	protected $m_intApprovedUnprocessedChangeRequestCount;
	protected $m_intUnapprovedUnprocessedChangeRequestCount;

	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strNextBatchPostDate;
	protected $m_strUtilityBillAdCampaignName;
	protected $m_strUtilityInvoiceTemplateName;

	// File details of uploading document.
	protected $m_arrstrFileDetails;
	protected $m_arrstrPropertyCommodityWarnings;
	protected $m_arrstrUtilityBillAccountSetupErrors;
	protected $m_arrstrUtilityProviderChangeOfAddressWarnings;

	protected $m_arrmixUtilityBillAccounts;
	protected $m_arrmixPropertyUtilityTypeDeatils;

	protected $m_objProperty;
	protected $m_objAdminDatabase;

	protected $m_arrobjPropertyUtilityTypes;
	protected $m_arrobjSystemEmails;

	// utility_property_settings
	protected $m_intPropertyUtilitySettingId;
	protected $m_intUtilityRolloutTypeId;
	protected $m_fltUtilityBillPerUnitFee;
	protected $m_fltWeightedValue;
	protected $m_boolHideBuilding;
	protected $m_boolIsVcrOnly;
	protected $m_boolIsDisplayDirectMeteredInvoiceInRp;
	protected $m_intCompanyUserId;


	// utility_integration_settings
	protected $m_intScheduledChargePostDay;
	protected $m_intChargeIntegrationDay;
	protected $m_intRemoveTempTransactionDay;
	protected $m_boolDontPostTempTransactions;
	protected $m_boolIsExportMoveOutTransactions;
	protected $m_boolIsExportBatchTransactions;
	protected $m_boolIsChargeIntegrationNextMonth;
	protected $m_strLastSyncedOn;
	protected $m_intUtilityPostDateTypeId;

	// utility_pre_bill_settings
	protected $m_intWaivePrebillApprovalDays;
	protected $m_intPreBillDueDay;
	protected $m_intDistributionDueDay;
	protected $m_fltHighConsumptionPercent;
	protected $m_fltLowConsumptionPercent;
	protected $m_fltVacantUnitConsumptionCap;
	protected $m_strBillingMonth;
	protected $m_strBillingInstructions;
	protected $m_strBillingSpecialistNote;
	protected $m_boolIsConvergent;
	protected $m_boolDontApproveConvergentServicePeriod;
	protected $m_boolRequirePrebillApproval;
	protected $m_boolInvoiceIsDueUponReceipt;
	protected $m_boolApplyUsageFactor;
	protected $m_boolUnitSpaceUsageFactor;
	protected $m_boolIsSubsidyOnUnit;
	protected $m_boolIsManagerApprovalMode;
	protected $m_boolDontSubsidyBill;
	protected $m_boolIsNextMonthPostMonth;
	protected $m_fltContractedSuccessfulMeterReportingRate;
	protected $m_strMeterMaintenanceNotes;
	protected $m_fltPsSubsidyBillingFee;
	protected $m_intMoveOutEstimationMonths;
	protected $m_boolLeaseStartDateAsMoveInDate;
	protected $m_boolIsNoBillingFee;
	protected $m_arrintBilledLedgerIds;

	// utility_uem_settings
	protected $m_intUemEmployeeId;
	protected $m_boolIsUemImplementation;
	protected $m_boolIsAuditEnabled;
	protected $m_boolIsOverrideBillNumber;
	protected $m_arrintAuditCompanyContactIds;
	protected $m_intUemPostMonthOptionId;
	protected $m_strUemGoLiveDate;

	// utility_invoice_settings
	protected $m_intUtilityInvoiceDeliveryTypeId;
	protected $m_strNonResidentPortalUrl;
	protected $m_strBackPageFooter;
	protected $m_intInvoiceDueDay;
	protected $m_intInvoiceMinimumDueDays;
	protected $m_boolHideOnlinePayment;
	protected $m_boolDontSendUndeliveredInvoicesNotification;
	protected $m_boolMergeBillingFee;
	protected $m_boolIsBillingFeeItemize;
	protected $m_boolExcludePaymentAddress;
	protected $m_boolHideResidentInvoiceSpanish;
	protected $m_boolHideResidentInvoiceBillingDisputes;
	protected $m_boolIsHideEcheck;
	protected $m_boolIsHideMasterCard;
	protected $m_boolIsAllowMoneyGram;
	protected $m_boolIsHideAmericanExpress;
	protected $m_boolIsHideDiscover;
	protected $m_boolIsHideVisa;
	protected $m_boolIsExcludeWeekends;
	protected $m_boolIsExcludeHolidays;
	protected $m_boolIsHideUsageHistory;
	protected $m_boolDisplayLeaseId;

	// utility_auto_billing_settings
	protected $m_boolRequirePreBillAnalystApproval;
	protected $m_boolRequireBillPeriodApproval;
	protected $m_boolRequireTransmissionApproval;
	protected $m_boolRequireBillSelectionApproval;
	protected $m_boolRequireVerifyInvoiceApproval;
	protected $m_boolRequirePreBillCalculationApproval;
	protected $m_boolRequireQcApproval;

	protected $m_objUtilityIntegrationSetting;
	protected $m_objUtilityPreBillSetting;
	protected $m_objUtilityUemSetting;
	protected $m_objUtilityPropertySetting;
	protected $m_objUtilityInvoiceSetting;
	protected $m_objUtilityAutoBillingSetting;

	const VCR_ONLY				= 0.25;
	const DIRECT_METERED		= 0.5;
	const UNITS_ABOVE_250		= 0.25;
	const STUDENT_HOUSING		= 0.5;
	const CONVERGENT_BILLING	= 0.25;
	const SUBMETERED_UTILITY	= 0.5;
	const EACH_UTILITY_BILLED	= 0.25;
	const VCR_AND_LEASE_CHARGES = 0.5;

	const PROPERTY_ACTIVE				= 1;
	const PROPERTY_IN_IMPLEMENTATION	= 2;
	const PROPERTY_TERMINATED			= 3;

	const MERGE_UNIT_CID = 17011;

	public static function getUtilitySettingPropertyTypes() {

		return [
			self::PROPERTY_ACTIVE				=> 'Active',
			self::PROPERTY_IN_IMPLEMENTATION	=> 'In Implementation',
			self::PROPERTY_TERMINATED			=> 'Terminated'
		];
	}

	public function __construct() {
		parent::__construct();
		$this->m_boolIsSkipUnitCountValidation = false;

		$this->setUtilityBillAccounts( [] );
		$this->setPropertyUtilityTypeDeatils( [] );
		$this->setUtilityBillAccountSetupErrors( [] );
	}

	/**
	* Get Functions
	*/

	public function getIsShowUnitDetails() {
		return $this->m_boolIsShowUnitDetails;
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function getIsSkipUnitCountValidation() {
		return $this->m_boolIsSkipUnitCountValidation;
	}

	public function getReallocateProratedExpense() {
		return $this->m_boolReallocateProratedExpense;
	}

	public function getUtilityRuleCount() {
		return $this->m_intUtilityRuleCount;
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function getOcrFailedAnomalyCount() {
		return $this->m_intOcrFailedAnomalyCount;
	}

	public function getUnprocessedBillsCount() {
		return $this->m_intUnprocessedBillsCount;
	}

	public function getLastOpenUtilityBatchId() {
		return $this->m_intLastOpenUtilityBatchId;
	}

	public function getAdminCompanyEmployeeId() {
		return $this->m_intAdminCompanyEmployeeId;
	}

	public function getExcessUsageAnomalyCount() {
		return $this->m_intExcessUsageAnomalyCount;
	}

	public function getLastCompleteUtilityBatchId() {
		return $this->m_intLastCompleteUtilityBatchId;
	}

	public function getMissingVcrBillAnomalyCount() {
		return $this->m_intMissingVcrBillAnomalyCount;
	}

	public function getMissingBillEscalationsCount() {
		return $this->m_intMissingBillEscalationsCount;
	}

	public function getUnprocessedVcrUtilityBillCount() {
		return $this->m_intUnprocessedVcrUtilityBillCount;
	}

	public function getMissingBillingBillAnomalyCount() {
		return $this->m_intMissingBillingBillAnomalyCount;
	}

	public function getMissingBillPayBillAnomalyCount() {
		return $this->m_intMissingBillPayBillAnomalyCount;
	}

	public function getBillPayUtilityBillAccountsCount() {
		return $this->m_intBillPayUtilityBillAccountsCount;
	}

	public function getApprovedUnprocessedChangeRequestCount() {
		return $this->m_intApprovedUnprocessedChangeRequestCount;
	}

	public function getUnapprovedUnprocessedChangeRequestCount() {
		return $this->m_intUnapprovedUnprocessedChangeRequestCount;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getNextBatchPostDate() {
		return $this->m_strNextBatchPostDate;
	}

	public function getUtilityBillAdCampaignName() {
		return $this->m_strUtilityBillAdCampaignName;
	}

	public function getUtilityInvoiceTemplateName() {
		return $this->m_strUtilityInvoiceTemplateName;
	}

	public function getFileDetails() {
		return $this->m_arrstrFileDetails;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getPropertyUtilityTypes() {
		return $this->m_arrobjPropertyUtilityTypes;
	}

	public function getUtilityBillAccounts() {
		return $this->m_arrmixUtilityBillAccounts;
	}

	public function getPropertyUtilityTypeDeatils() {
		return $this->m_arrmixPropertyUtilityTypeDeatils;
	}

	public function getUtilityBillEmailAddress() {
		return 'property_bill_' . $this->getPropertyId() . '@residentutility.com';
	}

	public function getIsUemProperty() {
		return $this->m_boolIsUemProperty;
	}

	public function getIsIpProperty() {
		return $this->m_boolIsIpProperty;
	}

	public function getIsRuProperty() {
		return $this->m_boolIsRuProperty;
	}

	// Get methods of utility_property_settings table

	public function getUtilityRolloutTypeId() {
		return $this->m_intUtilityRolloutTypeId;
	}

	public function getWeightedValue() {
		return $this->m_fltWeightedValue;
	}

	public function getHideBuilding() {
		return $this->m_boolHideBuilding;
	}

	public function getIsVcrOnly() {
		return $this->m_boolIsVcrOnly;
	}

	public function getIsDisplayDirectMeteredInvoiceInRp() {
		return $this->m_boolIsDisplayDirectMeteredInvoiceInRp;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	// Get methods of utility_invoice_settings table

	public function getUtilityInvoiceDeliveryTypeId() {
		return $this->m_intUtilityInvoiceDeliveryTypeId;
	}

	public function getNonResidentPortalUrl() {
		return $this->m_strNonResidentPortalUrl;
	}

	public function getInvoiceDueDay() {
		return $this->m_intInvoiceDueDay;
	}

	public function getInvoiceMinimumDueDays() {
		return $this->m_intInvoiceMinimumDueDays;
	}

	public function getHideOnlinePayment() {
		return $this->m_boolHideOnlinePayment;
	}

	public function getDontSendUndeliveredInvoicesNotification() {
		return $this->m_boolDontSendUndeliveredInvoicesNotification;
	}

	public function getMergeBillingFee() {
		return $this->m_boolMergeBillingFee;
	}

	public function getIsBillingFeeItemize() {
		return $this->m_boolIsBillingFeeItemize;
	}

	public function getExcludePaymentAddress() {
		return $this->m_boolExcludePaymentAddress;
	}

	public function getHideResidentInvoiceSpanish() {
		return $this->m_boolHideResidentInvoiceSpanish;
	}

	public function getHideResidentInvoiceBillingDisputes() {
		return $this->m_boolHideResidentInvoiceBillingDisputes;
	}

	public function getIsHideEcheck() {
		return $this->m_boolIsHideEcheck;
	}

	public function getIsHideMasterCard() {
		return $this->m_boolIsHideMasterCard;
	}

	public function getIsHideVisa() {
		return $this->m_boolIsHideVisa;
	}

	public function getIsHideDiscover() {
		return $this->m_boolIsHideDiscover;
	}

	public function getIsHideAmericanExpress() {
		return $this->m_boolIsHideAmericanExpress;
	}

	public function getIsAllowMoneyGram() {
		return $this->m_boolIsAllowMoneyGram;
	}

	public function getIsExcludeWeekends() {
		return $this->m_boolIsExcludeWeekends;
	}

	public function getIsExcludeHolidays() {
		return $this->m_boolIsExcludeHolidays;
	}

	public function getIsHideUsageHistory() {
		return $this->m_boolIsHideUsageHistory;
	}

	public function getBackPageFooter() {
		return $this->m_strBackPageFooter;
	}

	public function getDisplayLeaseId() {
		return $this->m_boolDisplayLeaseId;
	}

	// Get methods of utility_pre_bill_settings table

	public function getWaivePrebillApprovalDays() {
		return $this->m_intWaivePrebillApprovalDays;
	}

	public function getHighConsumptionPercent() {
		return $this->m_fltHighConsumptionPercent;
	}

	public function getLowConsumptionPercent() {
		return $this->m_fltLowConsumptionPercent;
	}

	public function getVacantUnitConsumptionCap() {
		return $this->m_fltVacantUnitConsumptionCap;
	}

	public function getBillingMonth() {
		return $this->m_strBillingMonth;
	}

	public function getBillingInstructions() {
		return $this->m_strBillingInstructions;
	}

	public function getBillingSpecialistNote() {
		return $this->m_strBillingSpecialistNote;
	}

	public function getIsConvergent() {
		return $this->m_boolIsConvergent;
	}

	public function getDontApproveConvergentServicePeriod() {
		return $this->m_boolDontApproveConvergentServicePeriod;
	}

	public function getInvoiceIsDueUponReceipt() {
		return $this->m_boolInvoiceIsDueUponReceipt;
	}

	public function getApplyUsageFactor() {
		return $this->m_boolApplyUsageFactor;
	}

	public function getUnitSpaceUsageFactor() {
		return $this->m_boolUnitSpaceUsageFactor;
	}

	public function getIsSubsidyOnUnit() {
		return $this->m_boolIsSubsidyOnUnit;
	}

	public function getIsManagerApprovalMode() {
		return $this->m_boolIsManagerApprovalMode;
	}

	public function getDontSubsidyBill() {
		return $this->m_boolDontSubsidyBill;
	}

	public function getIsNextMonthPostMonth() {
		return $this->m_boolIsNextMonthPostMonth;
	}

	public function getUtilityPostDateTypeId() {
		return $this->m_intUtilityPostDateTypeId;
	}

	public function getContractedSuccessfulMeterReportingRate() {
		return $this->m_fltContractedSuccessfulMeterReportingRate;
	}

	public function getMeterMaintenanceNotes() {
		return $this->m_strMeterMaintenanceNotes;
	}

	public function getMoveOutEstimationMonths() {
		return $this->m_intMoveOutEstimationMonths;
	}

	public function getLeaseStartDateAsMoveInDate() {
		return $this->m_boolLeaseStartDateAsMoveInDate;
	}

	public function getQcNotes() {
		return $this->m_strQcNotes;
	}

	public function getIsNoBillingFee() {
		return $this->m_boolIsNoBillingFee;
	}

	public function getBilledLedgerIds() {
		return $this->m_arrintBilledLedgerIds;
	}

	// Get methods of utility_uem_settings table

	public function getUemEmployeeId() {
		return $this->m_intUemEmployeeId;
	}

	public function getIsUemImplementation() {
		return $this->m_boolIsUemImplementation;
	}

	public function getIsAuditEnabled() {
		return $this->m_boolIsAuditEnabled;
	}

	public function getIsOverrideBillNumber() {
		return $this->m_boolIsOverrideBillNumber;
	}

	public function getAuditCompanyContactIds() {
		return $this->m_arrintAuditCompanyContactIds;
	}

	public function getUemPostMonthOptionId() {
		return $this->m_intUemPostMonthOptionId;
	}

	public function getUemGoLiveDate() {
		return $this->m_strUemGoLiveDate;
	}

	// Get methods of utility_integration_settings table

	public function getScheduledChargePostDay() {
		return $this->m_intScheduledChargePostDay;
	}

	public function getChargeIntegrationDay() {
		return $this->m_intChargeIntegrationDay;
	}

	public function getRemoveTempTransactionDay() {
		return $this->m_intRemoveTempTransactionDay;
	}

	public function getDontPostTempTransactions() {
		return $this->m_boolDontPostTempTransactions;
	}

	public function getIsExportMoveOutTransactions() {
		return $this->m_boolIsExportMoveOutTransactions;
	}

	public function getIsExportBatchTransactions() {
		return $this->m_boolIsExportBatchTransactions;
	}

	public function getIsChargeIntegrationNextMonth() {
		return $this->m_boolIsChargeIntegrationNextMonth;
	}

	public function getLastSyncedOn() {
		return $this->m_strLastSyncedOn;
	}

	// Get methods of utility_auto_billing_settings table

	public function getRequirePreBillAnalystApproval() {
		return $this->m_boolRequirePreBillAnalystApproval;
	}

	public function getRequireBillPeriodApproval() {
		return $this->m_boolRequireBillPeriodApproval;
	}

	public function getRequireTransmissionApproval() {
		return $this->m_boolRequireTransmissionApproval;
	}

	public function getRequireBillSelectionApproval() {
		return $this->m_boolRequireBillSelectionApproval;
	}

	public function getRequireVerifyInvoiceApproval() {
		return $this->m_boolRequireVerifyInvoiceApproval;
	}

	public function getRequirePreBillCalculationApproval() {
		return $this->m_boolRequirePreBillCalculationApproval;
	}

	public function getRequireQcApproval() {
		return $this->m_boolRequireQcApproval;
	}

	// Get methods of settings

	public function getUtilityIntegrationSetting() {
		return $this->m_objUtilityIntegrationSetting;
	}

	public function getUtilityPreBillSetting() {
		return $this->m_objUtilityPreBillSetting;
	}

	public function getUtilityUemSetting() {
		return $this->m_objUtilityUemSetting;
	}

	public function getUtilityPropertySetting() {
		return $this->m_objUtilityPropertySetting;
	}

	public function getUtilityInvoiceSetting() {
		return $this->m_objUtilityInvoiceSetting;
	}

	public function getUtilityAutoBillingSetting() {
		return $this->m_objUtilityAutoBillingSetting;
	}

	public function getPropertyTypeId() {
		return $this->m_intPropertyTypeId;
	}

	public function getAdminDatabase() {
		return $this->m_objAdminDatabase;
	}

	public function getAutoApprovePreBill() {
		return $this->m_boolAutoApprovePreBill;
	}

	public function getUtilityBillAccountSetupErrors() {
		return $this->m_arrstrUtilityBillAccountSetupErrors;
	}

	public function getUtilityBillAccountSetupErrorsByKey( $strKey, $boolReturnMergedErrors = false ) {
		if( false == valArr( $this->m_arrstrUtilityBillAccountSetupErrors ) || false == array_key_exists( $strKey, $this->m_arrstrUtilityBillAccountSetupErrors ) ) {
			return [];
		}

		$arrstrUtilityBillAccountSetupErrors = getArrayElementByKey( $strKey, $this->m_arrstrUtilityBillAccountSetupErrors );

		if( true == $boolReturnMergedErrors ) {
			$arrstrTempErrors = [];

			foreach( $arrstrUtilityBillAccountSetupErrors as $arrstrUtilityTypeErrors ) {
				foreach( $arrstrUtilityTypeErrors as $strUtilityTypeError ) {
					$arrstrTempErrors[] = $strUtilityTypeError;
				}
			}

			$arrstrUtilityBillAccountSetupErrors = $arrstrTempErrors;
		}

		return $arrstrUtilityBillAccountSetupErrors;
	}

	public function getUtilityProviderChangeOfAddressWarnings() {
		return $this->m_arrstrUtilityProviderChangeOfAddressWarnings;
	}

	public function getProviderChangeOfAddressSetupErrorsByKey( $strKey, $boolReturnMergedErrors = false ) {
		if( false == valArr( $this->m_arrstrUtilityProviderChangeOfAddressWarnings ) || false == array_key_exists( $strKey, $this->m_arrstrUtilityProviderChangeOfAddressWarnings ) ) {
			return [];
		}

		$arrstrUtilityProviderChangeOfAddressWarnings = getArrayElementByKey( $strKey, $this->m_arrstrUtilityProviderChangeOfAddressWarnings );

		if( true == $boolReturnMergedErrors ) {
			$arrstrTempErrors = [];

			foreach( $arrstrUtilityProviderChangeOfAddressWarnings as $arrstrUtilityProviderChangeOfAddressWarnings ) {
				foreach( $arrstrUtilityProviderChangeOfAddressWarnings as $strUtilityProviderChangeOfAddressWarning ) {
					$arrstrTempErrors[] = $strUtilityProviderChangeOfAddressWarning;
				}
			}

			$arrstrUtilityProviderChangeOfAddressWarnings = $arrstrTempErrors;
		}

		return $arrstrUtilityProviderChangeOfAddressWarnings;
	}

	/**
	* Add Functions
	*/

	public function addPropertyUtilityType( $objPropertyUtilityType ) {
		$this->m_arrobjPropertyUtilityTypes[$objPropertyUtilityType->getId()] = $objPropertyUtilityType;
	}

	/**
	* Set Functions
	*/

	public function setDefaults() {

		$this->setUtilityBillAccountId( NULL );

		$this->setPsLateFeePercent( 0 );
		$this->setScheduledChargePostDay( 20 );
		$this->setInvoiceDueDay( 5 );
		$this->setIsConvergent( true );
		$this->setInvoiceIsDueUponReceipt( false );
		$this->setIsInImplementation( true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_invoice_template_name'] ) ) {
			$this->setUtilityInvoiceTemplateName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_invoice_template_name'] ) : $arrmixValues['utility_invoice_template_name'] );
		}
		if( true == isset( $arrmixValues['utility_bill_ad_campaign_name'] ) ) {
			$this->setUtilityBillAdCampaignName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_ad_campaign_name'] ) : $arrmixValues['utility_bill_ad_campaign_name'] );
		}
		if( true == isset( $arrmixValues['next_batch_post_date'] ) ) {
			$this->setNextBatchPostDate( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['next_batch_post_date'] ) : $arrmixValues['next_batch_post_date'] );
		}
		if( true == isset( $arrmixValues['utility_rule_count'] ) ) {
			$this->setUtilityRuleCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_rule_count'] ) : $arrmixValues['utility_rule_count'] );
		}
		if( true == isset( $arrmixValues['missing_bill_escalations_count'] ) ) {
			$this->setMissingBillEscalationsCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['missing_bill_escalations_count'] ) : $arrmixValues['missing_bill_escalations_count'] );
		}
		if( true == isset( $arrmixValues['approved_unprocessed_change_request_count'] ) ) {
			$this->setApprovedUnprocessedChangeRequestCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['approved_unprocessed_change_request_count'] ) : $arrmixValues['approved_unprocessed_change_request_count'] );
		}
		if( true == isset( $arrmixValues['unapproved_unprocessed_change_request_count'] ) ) {
			$this->setUnapprovedUnprocessedChangeRequestCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unapproved_unprocessed_change_request_count'] ) : $arrmixValues['unapproved_unprocessed_change_request_count'] );
		}
		if( true == isset( $arrmixValues['unprocessed_vcr_utility_bill_count'] ) ) {
			$this->setUnprocessedVcrUtilityBillCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unprocessed_vcr_utility_bill_count'] ) : $arrmixValues['unprocessed_vcr_utility_bill_count'] );
		}
		if( true == isset( $arrmixValues['missing_vcr_bill_anomaly_count'] ) ) {
			$this->setMissingVcrBillAnomalyCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['missing_vcr_bill_anomaly_count'] ) : $arrmixValues['missing_vcr_bill_anomaly_count'] );
		}
		if( true == isset( $arrmixValues['missing_billing_bill_anomaly_count'] ) ) {
			$this->setMissingBillingBillAnomalyCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['missing_billing_bill_anomaly_count'] ) : $arrmixValues['missing_billing_bill_anomaly_count'] );
		}
		if( true == isset( $arrmixValues['missing_bill_pay_bill_anomaly_count'] ) ) {
			$this->setMissingBillPayBillAnomalyCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['missing_bill_pay_bill_anomaly_count'] ) : $arrmixValues['missing_bill_pay_bill_anomaly_count'] );
		}
		if( true == isset( $arrmixValues['excess_usage_anomaly_count'] ) ) {
			$this->setExcessUsageAnomalyCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['excess_usage_anomaly_count'] ) : $arrmixValues['excess_usage_anomaly_count'] );
		}
		if( true == isset( $arrmixValues['ocr_failed_anomaly_count'] ) ) {
			$this->setOcrFailedAnomalyCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['ocr_failed_anomaly_count'] ) : $arrmixValues['ocr_failed_anomaly_count'] );
		}
		if( true == isset( $arrmixValues['bill_pay_utility_bill_accounts_count'] ) ) {
			$this->setBillPayUtilityBillAccountsCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bill_pay_utility_bill_accounts_count'] ) : $arrmixValues['bill_pay_utility_bill_accounts_count'] );
		}
		if( true == isset( $arrmixValues['unprocessed_bills_count'] ) ) {
			$this->setUnprocessedBillsCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unprocessed_bills_count'] ) : $arrmixValues['unprocessed_bills_count'] );
		}
		if( true == isset( $arrmixValues['admin_company_employee_id'] ) ) {
			$this->setAdminCompanyEmployeeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['admin_company_employee_id'] ) : $arrmixValues['admin_company_employee_id'] );
		}
		if( true == isset( $arrmixValues['last_open_utility_batch_id'] ) ) {
			$this->setLastOpenUtilityBatchId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['last_open_utility_batch_id'] ) : $arrmixValues['last_open_utility_batch_id'] );
		}
		if( true == isset( $arrmixValues['last_complete_utility_batch_id'] ) ) {
			$this->setLastCompleteUtilityBatchId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['last_complete_utility_batch_id'] ) : $arrmixValues['last_complete_utility_batch_id'] );
		}
		if( true == isset( $arrmixValues['utility_bill_account_id'] ) ) {
			$this->setUtilityBillAccountId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_account_id'] ) : $arrmixValues['utility_bill_account_id'] );
		}
		if( true == isset( $arrmixValues['state_code'] ) ) {
			$this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['state_code'] ) : $arrmixValues['state_code'] );
		}

		// set values for table
		if( true == isset( $arrmixValues['utility_uem_settings'] ) ) {
			$this->setUtilityUemSetting( $arrmixValues['utility_uem_settings'] );
		}

		// set values of utility_property_settings table
		if( true == isset( $arrmixValues['utility_rollout_type_id'] ) ) {
			$this->setUtilityRolloutTypeId( $arrmixValues['utility_rollout_type_id'] );
		}

		if( true == isset( $arrmixValues['weighted_value'] ) ) {
			$this->setWeightedValue( $arrmixValues['weighted_value'] );
		}
		if( true == isset( $arrmixValues['hide_building'] ) ) {
			$this->setHideBuilding( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['hide_building'] ) : $arrmixValues['hide_building'] );
		}
		if( true == isset( $arrmixValues['is_vcr_only'] ) ) {
			$this->setIsVcrOnly( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_vcr_only'] ) : $arrmixValues['is_vcr_only'] );
		}

		if( true == isset( $arrmixValues['is_display_direct_metered_invoice_in_rp'] ) ) {
			$this->setIsDisplayDirectMeteredInvoiceInRp( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_display_direct_metered_invoice_in_rp'] ) : $arrmixValues['is_display_direct_metered_invoice_in_rp'] );
		}

		// set values of utility_invoice_settings table
		if( true == isset( $arrmixValues['utility_invoice_delivery_type_id'] ) ) {
			$this->setUtilityInvoiceDeliveryTypeId( $arrmixValues['utility_invoice_delivery_type_id'] );
		}
		if( true == isset( $arrmixValues['non_resident_portal_url'] ) ) {
			$this->setNonResidentPortalUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['non_resident_portal_url'] ) : $arrmixValues['non_resident_portal_url'] );
		}
		if( true == isset( $arrmixValues['invoice_due_day'] ) ) {
			$this->setInvoiceDueDay( $arrmixValues['invoice_due_day'] );
		}
		if( true == isset( $arrmixValues['invoice_minimum_due_days'] ) ) {
			$this->setInvoiceMinimumDueDays( $arrmixValues['invoice_minimum_due_days'] );
		}
		if( true == isset( $arrmixValues['hide_online_payment'] ) ) {
			$this->setHideOnlinePayment( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['hide_online_payment'] ) : $arrmixValues['hide_online_payment'] );
		}
		if( true == isset( $arrmixValues['dont_send_undelivered_invoices_notification'] ) ) {
			$this->setDontSendUndeliveredInvoicesNotification( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['dont_send_undelivered_invoices_notification'] ) : $arrmixValues['dont_send_undelivered_invoices_notification'] );
		}
		if( true == isset( $arrmixValues['merge_billing_fee'] ) ) {
			$this->setMergeBillingFee( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['merge_billing_fee'] ) : $arrmixValues['merge_billing_fee'] );
		}
		if( true == isset( $arrmixValues['is_billing_fee_itemize'] ) ) {
			$this->setIsBillingFeeItemize( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_billing_fee_itemize'] ) : $arrmixValues['is_billing_fee_itemize'] );
		}
		if( true == isset( $arrmixValues['exclude_payment_address'] ) ) {
			$this->setExcludePaymentAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['exclude_payment_address'] ) : $arrmixValues['exclude_payment_address'] );
		}
		if( true == isset( $arrmixValues['hide_resident_invoice_spanish'] ) ) {
			$this->setHideResidentInvoiceSpanish( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['hide_resident_invoice_spanish'] ) : $arrmixValues['hide_resident_invoice_spanish'] );
		}
		if( true == isset( $arrmixValues['hide_resident_invoice_billing_disputes'] ) ) {
			$this->setHideResidentInvoiceBillingDisputes( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['hide_resident_invoice_billing_disputes'] ) : $arrmixValues['hide_resident_invoice_billing_disputes'] );
		}
		if( true == isset( $arrmixValues['is_hide_echeck'] ) ) {
			$this->setIsHideEcheck( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_hide_echeck'] ) : $arrmixValues['is_hide_echeck'] );
		}
		if( true == isset( $arrmixValues['is_hide_master_card'] ) ) {
			$this->setIsHideMasterCard( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_hide_master_card'] ) : $arrmixValues['is_hide_master_card'] );
		}
		if( true == isset( $arrmixValues['is_hide_visa'] ) ) {
			$this->setIsHideVisa( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_hide_visa'] ) : $arrmixValues['is_hide_visa'] );
		}
		if( true == isset( $arrmixValues['is_hide_discover'] ) ) {
			$this->setIsHideDiscover( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_hide_discover'] ) : $arrmixValues['is_hide_discover'] );
		}
		if( true == isset( $arrmixValues['is_hide_american_express'] ) ) {
			$this->setIsHideAmericanExpress( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_hide_american_express'] ) : $arrmixValues['is_hide_american_express'] );
		}
		if( true == isset( $arrmixValues['is_allow_money_gram'] ) ) {
			$this->setIsAllowMoneyGram( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_allow_money_gram'] ) : $arrmixValues['is_allow_money_gram'] );
		}

		if( true == isset( $arrmixValues['is_exclude_weekends'] ) ) {
			$this->setIsExcludeWeekends( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_exclude_weekends'] ) : $arrmixValues['is_exclude_weekends'] );
		}

		if( true == isset( $arrmixValues['is_exclude_holidays'] ) ) {
			$this->setIsExcludeHolidays( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_exclude_holidays'] ) : $arrmixValues['is_exclude_holidays'] );
		}

		if( true == isset( $arrmixValues['back_page_footer'] ) ) {
			$this->setBackPageFooter( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['back_page_footer'] ) : $arrmixValues['back_page_footer'] );
		}

		if( true == isset( $arrmixValues['is_hide_usage_history'] ) ) {
			$this->setIsHideUsageHistory( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_hide_usage_history'] ) : $arrmixValues['is_hide_usage_history'] );
		}

		if( true == isset( $arrmixValues['display_lease_id'] ) ) {
			$this->setDisplayLeaseId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['display_lease_id'] ) : $arrmixValues['display_lease_id'] );
		}

		// set values of utility_pre_bill_settings table
		if( true == isset( $arrmixValues['waive_prebill_approval_days'] ) ) {
			$this->setWaivePrebillApprovalDays( $arrmixValues['waive_prebill_approval_days'] );
		}
		if( true == isset( $arrmixValues['high_consumption_percent'] ) ) {
			$this->setHighConsumptionPercent( $arrmixValues['high_consumption_percent'] );
		}
		if( true == isset( $arrmixValues['low_consumption_percent'] ) ) {
			$this->setLowConsumptionPercent( $arrmixValues['low_consumption_percent'] );
		}
		if( true == isset( $arrmixValues['vacant_unit_consumption_cap'] ) ) {
			$this->setVacantUnitConsumptionCap( $arrmixValues['vacant_unit_consumption_cap'] );
		}
		if( true == isset( $arrmixValues['billing_month'] ) ) {
			$this->setBillingMonth( $arrmixValues['billing_month'] );
		}
		if( true == isset( $arrmixValues['billing_instructions'] ) ) {
			$this->setBillingInstructions( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billing_instructions'] ) : $arrmixValues['billing_instructions'] );
		}
		if( true == isset( $arrmixValues['billing_specialist_note'] ) ) {
			$this->setBillingSpecialistNote( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billing_specialist_note'] ) : $arrmixValues['billing_specialist_note'] );
		}
		if( true == isset( $arrmixValues['is_convergent'] ) ) {
			$this->setIsConvergent( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_convergent'] ) : $arrmixValues['is_convergent'] );
		}
		if( true == isset( $arrmixValues['dont_approve_convergent_service_period'] ) ) {
			$this->setDontApproveConvergentServicePeriod( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['dont_approve_convergent_service_period'] ) : $arrmixValues['dont_approve_convergent_service_period'] );
		}
		if( true == isset( $arrmixValues['invoice_is_due_upon_receipt'] ) ) {
			$this->setInvoiceIsDueUponReceipt( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['invoice_is_due_upon_receipt'] ) : $arrmixValues['invoice_is_due_upon_receipt'] );
		}
		if( true == isset( $arrmixValues['apply_usage_factor'] ) ) {
			$this->setApplyUsageFactor( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['apply_usage_factor'] ) : $arrmixValues['apply_usage_factor'] );
		}
		if( true == isset( $arrmixValues['unit_space_usage_factor'] ) ) {
			$this->setUnitSpaceUsageFactor( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unit_space_usage_factor'] ) : $arrmixValues['unit_space_usage_factor'] );
		}
		if( true == isset( $arrmixValues['is_subsidy_on_unit'] ) ) {
			$this->setIsSubsidyOnUnit( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_subsidy_on_unit'] ) : $arrmixValues['is_subsidy_on_unit'] );
		}
		if( true == isset( $arrmixValues['is_manager_approval_mode'] ) ) {
			$this->setIsManagerApprovalMode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_manager_approval_mode'] ) : $arrmixValues['is_manager_approval_mode'] );
		}
		if( true == isset( $arrmixValues['dont_subsidy_bill'] ) ) {
			$this->setDontSubsidyBill( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['dont_subsidy_bill'] ) : $arrmixValues['dont_subsidy_bill'] );
		}
		if( true == isset( $arrmixValues['is_next_month_post_month'] ) ) {
			$this->setIsNextMonthPostMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_next_month_post_month'] ) : $arrmixValues['is_next_month_post_month'] );
		}

		if( true == isset( $arrmixValues['utility_post_date_type_id'] ) ) {
			$this->setUtilityPostDateTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_post_date_type_id'] ) : $arrmixValues['utility_post_date_type_id'] );
		}

		if( true == isset( $arrmixValues['contracted_successful_meter_reporting_rate'] ) ) {
			$this->setContractedSuccessfulMeterReportingRate( $arrmixValues['contracted_successful_meter_reporting_rate'] );
		}
		if( true == isset( $arrmixValues['meter_maintenance_notes'] ) ) {
			$this->setMeterMaintenanceNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['meter_maintenance_notes'] ) : $arrmixValues['meter_maintenance_notes'] );
		}

		if( true == isset( $arrmixValues['move_out_estimation_months'] ) ) {
			$this->setMoveOutEstimationMonths( $arrmixValues['move_out_estimation_months'] );
		}

		if( true == isset( $arrmixValues['lease_start_date_as_move_in_date'] ) ) {
			$this->setLeaseStartDateAsMoveInDate( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['lease_start_date_as_move_in_date'] ) : $arrmixValues['lease_start_date_as_move_in_date'] );
		}

		if( true == isset( $arrmixValues['is_show_unit_details'] ) ) {
			$this->setIsShowUnitDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_show_unit_details'] ) : $arrmixValues['is_show_unit_details'] );
		}

		if( true == isset( $arrmixValues['is_no_billing_fee'] ) ) {
			$this->setIsNoBillingFee( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_no_billing_fee'] ) : $arrmixValues['is_no_billing_fee'] );
		}

		if( true == isset( $arrmixValues['billed_ledger_ids'] ) ) {
			$this->setBilledLedgerIds( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billed_ledger_ids'] ) : $arrmixValues['billed_ledger_ids'] );
		}

		// set values of utility_uem_settings table
		if( true == isset( $arrmixValues['uem_employee_id'] ) ) {
			$this->setUemEmployeeId( $arrmixValues['uem_employee_id'] );
		}
		if( true == isset( $arrmixValues['is_uem_implementation'] ) ) {
			$this->setIsUemImplementation( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_uem_implementation'] ) : $arrmixValues['is_uem_implementation'] );
		}
		if( true == isset( $arrmixValues['is_audit_enabled'] ) ) {
			$this->setIsAuditEnabled( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_audit_enabled'] ) : $arrmixValues['is_audit_enabled'] );
		}
		if( true == isset( $arrmixValues['audit_company_contact_ids'] ) ) {
			$this->setAuditCompanyContactIds( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['audit_company_contact_ids'] ) : $arrmixValues['audit_company_contact_ids'] );
		}
		if( true == isset( $arrmixValues['uem_post_month_option_id'] ) ) {
			$this->setUemPostMonthOptionId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['uem_post_month_option_id'] ) : $arrmixValues['uem_post_month_option_id'] );
		}
		if( true == isset( $arrmixValues['is_override_bill_number'] ) ) {
			$this->setIsOverrideBillNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_override_bill_number'] ) : $arrmixValues['is_override_bill_number'] );
		}
		if( true == isset( $arrmixValues['uem_go_live_date'] ) ) {
			$this->setUemGoLiveDate( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['uem_go_live_date'] ) : $arrmixValues['uem_go_live_date'] );
		}

		// set values of utility_integration_settings table
		if( true == isset( $arrmixValues['scheduled_charge_post_day'] ) ) {
			$this->setScheduledChargePostDay( $arrmixValues['scheduled_charge_post_day'] );
		}
		if( true == isset( $arrmixValues['charge_integration_day'] ) ) {
			$this->setChargeIntegrationDay( $arrmixValues['charge_integration_day'] );
		}
		if( true == isset( $arrmixValues['remove_temp_transaction_day'] ) ) {
			$this->setRemoveTempTransactionDay( $arrmixValues['remove_temp_transaction_day'] );
		}
		if( true == isset( $arrmixValues['dont_post_temp_transactions'] ) ) {
			$this->setDontPostTempTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['dont_post_temp_transactions'] ) : $arrmixValues['dont_post_temp_transactions'] );
		}

		if( true == isset( $arrmixValues['is_export_move_out_transactions'] ) ) {
			$this->setIsExportMoveOutTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_export_move_out_transactions'] ) : $arrmixValues['is_export_move_out_transactions'] );
		}
		if( true == isset( $arrmixValues['is_export_batch_transactions'] ) ) {
			$this->setIsExportBatchTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_export_batch_transactions'] ) : $arrmixValues['is_export_batch_transactions'] );
		}
		if( true == isset( $arrmixValues['is_charge_integration_next_month'] ) ) {
			$this->setIsChargeIntegrationNextMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_charge_integration_next_month'] ) : $arrmixValues['is_charge_integration_next_month'] );
		}
		if( true == isset( $arrmixValues['last_synced_on'] ) ) {
			$this->setLastSyncedOn( $arrmixValues['last_synced_on'] );
		}

		// set values of utility_auto_billing_settings table
		if( true == isset( $arrmixValues['require_pre_bill_analyst_approval'] ) ) {
			$this->setRequirePreBillAnalystApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['require_pre_bill_analyst_approval'] ) : $arrmixValues['require_pre_bill_analyst_approval'] );
		}
		if( true == isset( $arrmixValues['require_bill_period_approval'] ) ) {
			$this->setRequireBillPeriodApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['require_bill_period_approval'] ) : $arrmixValues['require_bill_period_approval'] );
		}
		if( true == isset( $arrmixValues['require_transmission_approval'] ) ) {
			$this->setRequireTransmissionApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['require_transmission_approval'] ) : $arrmixValues['require_transmission_approval'] );
		}
		if( true == isset( $arrmixValues['require_bill_selection_approval'] ) ) {
			$this->setRequireBillSelectionApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['require_bill_selection_approval'] ) : $arrmixValues['require_bill_selection_approval'] );
		}
		if( true == isset( $arrmixValues['require_verify_invoice_approval'] ) ) {
			$this->setRequireVerifyInvoiceApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['require_verify_invoice_approval'] ) : $arrmixValues['require_verify_invoice_approval'] );
		}
		if( true == isset( $arrmixValues['require_pre_bill_calculation_approval'] ) ) {
			$this->setRequirePreBillCalculationApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['require_pre_bill_calculation_approval'] ) : $arrmixValues['require_pre_bill_calculation_approval'] );
		}
		if( true == isset( $arrmixValues['require_qc_approval'] ) ) {
			$this->setRequireQcApproval( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['require_qc_approval'] ) : $arrmixValues['require_qc_approval'] );
		}

		if( true == isset( $arrmixValues['qc_notes'] ) ) {
			$this->setQcNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['qc_notes'] ) : $arrmixValues['qc_notes'] );
		}

		if( true == isset( $arrmixValues['auto_approve_pre_bill'] ) ) {
			$this->setAutoApprovePreBill( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['auto_approve_pre_bill'] ) : $arrmixValues['auto_approve_pre_bill'] );
		}

		if( true == isset( $arrmixValues['reallocate_prorated_expense'] ) ) {
			$this->setReallocateProratedExpense( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['reallocate_prorated_expense'] ) : $arrmixValues['reallocate_prorated_expense'] );
		}

	}

	public function setPropertyTypeId( $intPropertyTypeId ) {
		return $this->m_intPropertyTypeId = $intPropertyTypeId;
	}

	public function setIsShowUnitDetails( $boolIsShowUnitDetails ) {
		$this->m_boolIsShowUnitDetails = CStrings::strToBool( $boolIsShowUnitDetails );
	}

	public function setIsSkipUnitCountValidation( $boolValue ) {
		$this->m_boolIsSkipUnitCountValidation = $boolValue;
	}

	public function setReallocateProratedExpense( $boolReallocateProratedExpense ) {
		$this->set( 'm_boolReallocateProratedExpense', CStrings::strToBool( $boolReallocateProratedExpense ) );
	}

	public function setUtilityRuleCount( $intUtilityRuleCount ) {
		$this->m_intUtilityRuleCount = $intUtilityRuleCount;
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->m_intUtilityBillAccountId = $intUtilityBillAccountId;
	}

	public function setOcrFailedAnomalyCount( $intOcrFailedAnomalyCount ) {
		$this->m_intOcrFailedAnomalyCount = $intOcrFailedAnomalyCount;
	}

	public function setUnprocessedBillsCount( $intUnprocessedBillsCount ) {
		$this->m_intUnprocessedBillsCount = $intUnprocessedBillsCount;
	}

	public function setLastOpenUtilityBatchId( $intLastOpenUtilityBatchId ) {
		$this->m_intLastOpenUtilityBatchId = $intLastOpenUtilityBatchId;
	}

	public function setAdminCompanyEmployeeId( $intAdminCompanyEmployeeId ) {
		$this->m_intAdminCompanyEmployeeId = $intAdminCompanyEmployeeId;
	}

	public function setExcessUsageAnomalyCount( $intExcessUsageAnomalyCount ) {
		$this->m_intExcessUsageAnomalyCount = $intExcessUsageAnomalyCount;
	}

	public function setLastCompleteUtilityBatchId( $intLastCompleteUtilityBatchId ) {
		$this->m_intLastCompleteUtilityBatchId = $intLastCompleteUtilityBatchId;
	}

	public function setMissingVcrBillAnomalyCount( $intMissingVcrBillAnomalyCount ) {
		$this->m_intMissingVcrBillAnomalyCount = $intMissingVcrBillAnomalyCount;
	}

	public function setMissingBillEscalationsCount( $intMissingBillEscalationsCount ) {
		$this->m_intMissingBillEscalationsCount = $intMissingBillEscalationsCount;
	}

	public function setUnprocessedVcrUtilityBillCount( $intUnprocessedVcrUtilityBillCount ) {
		$this->m_intUnprocessedVcrUtilityBillCount = $intUnprocessedVcrUtilityBillCount;
	}

	public function setMissingBillingBillAnomalyCount( $intMissingBillingBillAnomalyCount ) {
		$this->m_intMissingBillingBillAnomalyCount = $intMissingBillingBillAnomalyCount;
	}

	public function setMissingBillPayBillAnomalyCount( $intMissingBillPayBillAnomalyCount ) {
		$this->m_intMissingBillPayBillAnomalyCount = $intMissingBillPayBillAnomalyCount;
	}

	public function setBillPayUtilityBillAccountsCount( $intBillPayUtilityBillAccountsCount ) {
		$this->m_intBillPayUtilityBillAccountsCount = $intBillPayUtilityBillAccountsCount;
	}

	public function setApprovedUnprocessedChangeRequestCount( $intApprovedUnprocessedChangeRequestCount ) {
		$this->m_intApprovedUnprocessedChangeRequestCount = $intApprovedUnprocessedChangeRequestCount;
	}

	public function setUnapprovedUnprocessedChangeRequestCount( $intUnapprovedUnprocessedChangeRequestCount ) {
		$this->m_intUnapprovedUnprocessedChangeRequestCount = $intUnapprovedUnprocessedChangeRequestCount;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function setNextBatchPostDate( $strNextBatchPostDate ) {
		$this->m_strNextBatchPostDate = $strNextBatchPostDate;
	}

	public function setUtilityBillAdCampaignName( $strUtilityBillAdCampaignName ) {
		$this->m_strUtilityBillAdCampaignName = $strUtilityBillAdCampaignName;
	}

	public function setUtilityInvoiceTemplateName( $strUtilityInvoiceTemplateName ) {
		$this->m_strUtilityInvoiceTemplateName = $strUtilityInvoiceTemplateName;
	}

	public function setFileDetails( $arrmixFileDetails ) {
		$this->m_arrstrFileDetails = $arrmixFileDetails;
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setPropertyUtilityTypes( $arrobjPropertyUtilityTypes ) {
		$this->m_arrobjPropertyUtilityTypes = $arrobjPropertyUtilityTypes;
	}

	public function setUtilityBillAccounts( $arrobjUtilityBillAccounts ) {
		$this->m_arrmixUtilityBillAccounts = $arrobjUtilityBillAccounts;
	}

	public function setPropertyUtilityTypeDeatils( $arrmixPropertyUtilityTypeDeatils ) {
		$this->m_arrmixPropertyUtilityTypeDeatils = $arrmixPropertyUtilityTypeDeatils;
	}

	public function setIsUemProperty( $boolIsUemProperty ) {
		$this->m_boolIsUemProperty = CStrings::strToBool( $boolIsUemProperty );
	}

	public function setIsIpProperty( $boolIsIpProperty ) {
		$this->m_boolIsIpProperty = CStrings::strToBool( $boolIsIpProperty );
	}

	public function setIsRuProperty( $boolIsRuProperty ) {
		$this->m_boolIsRuProperty = CStrings::strToBool( $boolIsRuProperty );
	}

	// Set methods of utility_property_settings table

	public function setUtilityRolloutTypeId( $intUtilityRolloutTypeId ) {
		$this->m_intUtilityRolloutTypeId = CStrings::strToIntDef( $intUtilityRolloutTypeId );
	}

	public function setWeightedValue( $fltWeightedValue ) {
		$this->m_fltWeightedValue = CStrings::strToFloatDef( $fltWeightedValue, NULL, false, 2 );
	}

	public function setHideBuilding( $boolHideBuilding ) {
		$this->m_boolHideBuilding = CStrings::strToBool( $boolHideBuilding );
	}

	public function setIsVcrOnly( $boolIsVcrOnly ) {
		$this->m_boolIsVcrOnly = CStrings::strToBool( $boolIsVcrOnly );
	}

	public function setIsDisplayDirectMeteredInvoiceInRp( $boolIsDisplayDirectMeteredInvoiceInRp ) {
		$this->m_boolIsDisplayDirectMeteredInvoiceInRp = CStrings::strToBool( $boolIsDisplayDirectMeteredInvoiceInRp );
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = $intCompanyUserId;
	}

	// Set methods of utility_invoice_settings table

	public function setUtilityInvoiceDeliveryTypeId( $intUtilityInvoiceDeliveryTypeId ) {
		$this->m_intUtilityInvoiceDeliveryTypeId = CStrings::strToIntDef( $intUtilityInvoiceDeliveryTypeId );
	}

	public function setNonResidentPortalUrl( $strNonResidentPortalUrl ) {
		$this->m_strNonResidentPortalUrl = CStrings::strTrimDef( $strNonResidentPortalUrl, 350, NULL, true );
	}

	public function setInvoiceDueDay( $intInvoiceDueDay ) {
		$this->m_intInvoiceDueDay = CStrings::strToIntDef( $intInvoiceDueDay );
	}

	public function setInvoiceMinimumDueDays( $intInvoiceMinimumDueDays ) {
		$this->m_intInvoiceMinimumDueDays = CStrings::strToIntDef( $intInvoiceMinimumDueDays );
	}

	public function setHideOnlinePayment( $boolHideOnlinePayment ) {
		$this->m_boolHideOnlinePayment = CStrings::strToBool( $boolHideOnlinePayment );
	}

	public function setDontSendUndeliveredInvoicesNotification( $boolDontSendUndeliveredInvoicesNotification ) {
		$this->m_boolDontSendUndeliveredInvoicesNotification = CStrings::strToBool( $boolDontSendUndeliveredInvoicesNotification );
	}

	public function setMergeBillingFee( $boolMergeBillingFee ) {
		$this->m_boolMergeBillingFee = CStrings::strToBool( $boolMergeBillingFee );
	}

	public function setIsBillingFeeItemize( $boolIsBillingFeeItemize ) {
		$this->m_boolIsBillingFeeItemize = CStrings::strToBool( $boolIsBillingFeeItemize );
	}

	public function setExcludePaymentAddress( $boolExcludePaymentAddress ) {
		$this->m_boolExcludePaymentAddress = CStrings::strToBool( $boolExcludePaymentAddress );
	}

	public function setHideResidentInvoiceSpanish( $boolHideResidentInvoiceSpanish ) {
		$this->m_boolHideResidentInvoiceSpanish = CStrings::strToBool( $boolHideResidentInvoiceSpanish );
	}

	public function setHideResidentInvoiceBillingDisputes( $boolHideResidentInvoiceBillingDisputes ) {
		$this->m_boolHideResidentInvoiceBillingDisputes = CStrings::strToBool( $boolHideResidentInvoiceBillingDisputes );
	}

	public function setIsHideEcheck( $boolIsHideEcheck ) {
		$this->m_boolIsHideEcheck = CStrings::strToBool( $boolIsHideEcheck );
	}

	public function setIsHideMasterCard( $boolIsHideMasterCard ) {
		$this->m_boolIsHideMasterCard = CStrings::strToBool( $boolIsHideMasterCard );
	}

	public function setIsHideVisa( $boolIsHideVisa ) {
		$this->m_boolIsHideVisa = CStrings::strToBool( $boolIsHideVisa );
	}

	public function setIsHideDiscover( $boolIsHideDiscover ) {
		$this->m_boolIsHideDiscover = CStrings::strToBool( $boolIsHideDiscover );
	}

	public function setIsHideAmericanExpress( $boolIsHideAmericanExpress ) {
		$this->m_boolIsHideAmericanExpress = CStrings::strToBool( $boolIsHideAmericanExpress );
	}

	public function setIsAllowMoneyGram( $boolIsAllowMoneyGram ) {
		$this->m_boolIsAllowMoneyGram = CStrings::strToBool( $boolIsAllowMoneyGram );
	}

	public function setIsExcludeWeekends( $boolIsExcludeWeekends ) {
		$this->m_boolIsExcludeWeekends = CStrings::strToBool( $boolIsExcludeWeekends );
	}

	public function setIsExcludeHolidays( $boolIsExcludeHolidays ) {
		$this->m_boolIsExcludeHolidays = CStrings::strToBool( $boolIsExcludeHolidays );
	}

	public function setIsHideUsageHistory( $boolIsHideUsageHistory ) {
		$this->m_boolIsHideUsageHistory = CStrings::strToBool( $boolIsHideUsageHistory );
	}

	public function setBackPageFooter( $strBackPageFooter ) {
		$this->set( 'm_strBackPageFooter', CStrings::strTrimDef( $strBackPageFooter, -1, NULL, true ) );
	}

	public function setDisplayLeaseId( $boolDisplayLeaseId ) {
		$this->m_boolDisplayLeaseId = CStrings::strToBool( $boolDisplayLeaseId );
	}

	// Set methods of utility_pre_bill_settings table

	public function setWaivePrebillApprovalDays( $intWaivePrebillApprovalDays ) {
		$this->m_intWaivePrebillApprovalDays = CStrings::strToIntDef( $intWaivePrebillApprovalDays );
	}

	public function setHighConsumptionPercent( $fltHighConsumptionPercent ) {
		$this->m_fltHighConsumptionPercent = CStrings::strToFloatDef( $fltHighConsumptionPercent, NULL, false, 2 );
	}

	public function setLowConsumptionPercent( $fltLowConsumptionPercent ) {
		$this->m_fltLowConsumptionPercent = CStrings::strToFloatDef( $fltLowConsumptionPercent, NULL, false, 2 );
	}

	public function setVacantUnitConsumptionCap( $fltVacantUnitConsumptionCap ) {
		$this->m_fltVacantUnitConsumptionCap = CStrings::strToFloatDef( $fltVacantUnitConsumptionCap, NULL, false, 2 );
	}

	public function setBillingMonth( $strBillingMonth ) {
		$this->set( 'm_strBillingMonth', CStrings::strTrimDef( $strBillingMonth, -1, NULL, true ) );
	}

	public function setBillingInstructions( $strBillingInstructions ) {
		$this->m_strBillingInstructions = CStrings::strTrimDef( $strBillingInstructions, -1, NULL, true );
	}

	public function setBillingSpecialistNote( $strBillingSpecialistNote ) {
		$this->m_strBillingSpecialistNote = CStrings::strTrimDef( $strBillingSpecialistNote, -1, NULL, true );
	}

	public function setIsConvergent( $boolIsConvergent ) {
		$this->m_boolIsConvergent = CStrings::strToBool( $boolIsConvergent );
	}

	public function setDontApproveConvergentServicePeriod( $boolDontApproveConvergentServicePeriod ) {
		$this->m_boolDontApproveConvergentServicePeriod = CStrings::strToBool( $boolDontApproveConvergentServicePeriod );
	}

	public function setInvoiceIsDueUponReceipt( $boolInvoiceIsDueUponReceipt ) {
		$this->m_boolInvoiceIsDueUponReceipt = CStrings::strToBool( $boolInvoiceIsDueUponReceipt );
	}

	public function setApplyUsageFactor( $boolApplyUsageFactor ) {
		$this->m_boolApplyUsageFactor = CStrings::strToBool( $boolApplyUsageFactor );
	}

	public function setUnitSpaceUsageFactor( $boolUnitSpaceUsageFactor ) {
		$this->m_boolUnitSpaceUsageFactor = CStrings::strToBool( $boolUnitSpaceUsageFactor );
	}

	public function setIsSubsidyOnUnit( $boolIsSubsidyOnUnit ) {
		$this->m_boolIsSubsidyOnUnit = CStrings::strToBool( $boolIsSubsidyOnUnit );
	}

	public function setIsManagerApprovalMode( $boolIsManagerApprovalMode ) {
		$this->m_boolIsManagerApprovalMode = CStrings::strToBool( $boolIsManagerApprovalMode );
	}

	public function setDontSubsidyBill( $boolDontSubsidyBill ) {
		$this->m_boolDontSubsidyBill = CStrings::strToBool( $boolDontSubsidyBill );
	}

	public function setIsNextMonthPostMonth( $boolIsNextMonthPostMonth ) {
		$this->m_boolIsNextMonthPostMonth = CStrings::strToBool( $boolIsNextMonthPostMonth );
	}

	public function setUtilityPostDateTypeId( $intUtilityPostDateTypeId ) {
		$this->m_intUtilityPostDateTypeId = $intUtilityPostDateTypeId;
	}

	public function setContractedSuccessfulMeterReportingRate( $fltContractedSuccessfulMeterReportingRate ) {
		$this->m_fltContractedSuccessfulMeterReportingRate = CStrings::strToFloatDef( $fltContractedSuccessfulMeterReportingRate, NULL, false, 2 );
	}

	public function setMeterMaintenanceNotes( $strMeterMaintenanceNotes ) {
		$this->m_strMeterMaintenanceNotes = CStrings::strTrimDef( $strMeterMaintenanceNotes, -1, NULL, true );
	}

	public function setMoveOutEstimationMonths( $intMoveOutEstimationMonths ) {
		$this->m_intMoveOutEstimationMonths = CStrings::strToIntDef( $intMoveOutEstimationMonths, NULL, false );
	}

	public function setLeaseStartDateAsMoveInDate( $boolLeaseStartDateAsMoveInDate ) {
		$this->set( 'm_boolLeaseStartDateAsMoveInDate', CStrings::strToBool( $boolLeaseStartDateAsMoveInDate ) );
	}

	public function setQcNotes( $strQcNotes ) {
		$this->set( 'm_strQcNotes', CStrings::strTrimDef( $strQcNotes, -1, NULL, true ) );
	}

	public function setIsNoBillingFee( $boolIsNoBillingFee ) {
		$this->set( 'm_boolIsNoBillingFee', CStrings::strToBool( $boolIsNoBillingFee ) );
	}

	public function setBilledLedgerIds( $arrintBilledLedgerIds ) {
		$this->m_arrintBilledLedgerIds = CStrings::strToArrIntDef( $arrintBilledLedgerIds, NULL );
	}

	// Set methods of utility_uem_settings table

	public function setUemEmployeeId( $intUemEmployeeId ) {
		$this->m_intUemEmployeeId = CStrings::strToIntDef( $intUemEmployeeId );
	}

	public function setIsUemImplementation( $boolIsUemImplementation ) {
		$this->m_boolIsUemImplementation = CStrings::strToBool( $boolIsUemImplementation );
	}

	public function setIsAuditEnabled( $boolIsAuditEnabled ) {
		$this->m_boolIsAuditEnabled = CStrings::strToBool( $boolIsAuditEnabled );
	}

	public function setIsOverrideBillNumber( $boolIsOverrideBillNumber ) {
		$this->m_boolIsOverrideBillNumber = CStrings::strToBool( $boolIsOverrideBillNumber );
	}

	public function setAuditCompanyContactIds( $arrintAuditCompanyContactIds ) {
		$this->m_arrintAuditCompanyContactIds = CStrings::strToArrIntDef( $arrintAuditCompanyContactIds );
	}

	public function setUemPostMonthOptionId( $intUemPostMonthOptionId ) {
		$this->set( 'm_intUemPostMonthOptionId', CStrings::strToIntDef( $intUemPostMonthOptionId, NULL, false ) );
	}

	public function setUemGoLiveDate( $strUemGoLiveDate ) {
		$this->m_strUemGoLiveDate = CStrings::strTrimDef( $strUemGoLiveDate, -1, NULL, true );
	}

	// Set methods of utility_integration_settings table

	public function setScheduledChargePostDay( $intScheduledChargePostDay ) {
		$this->m_intScheduledChargePostDay = CStrings::strToIntDef( $intScheduledChargePostDay );
	}

	public function setChargeIntegrationDay( $intChargeIntegrationDay ) {
		$this->m_intChargeIntegrationDay = CStrings::strToIntDef( $intChargeIntegrationDay );
	}

	public function setRemoveTempTransactionDay( $intRemoveTempTransactionDay ) {
		$this->m_intRemoveTempTransactionDay = CStrings::strToIntDef( $intRemoveTempTransactionDay );
	}

	public function setDontPostTempTransactions( $boolDontPostTempTransactions ) {
		$this->m_boolDontPostTempTransactions = CStrings::strToBool( $boolDontPostTempTransactions );
	}

	public function setIsExportMoveOutTransactions( $boolIsExportMoveOutTransactions ) {
		$this->m_boolIsExportMoveOutTransactions = CStrings::strToBool( $boolIsExportMoveOutTransactions );
	}

	public function setIsExportBatchTransactions( $boolIsExportBatchTransactions ) {
		$this->m_boolIsExportBatchTransactions = CStrings::strToBool( $boolIsExportBatchTransactions );
	}

	public function setIsChargeIntegrationNextMonth( $boolIsChargeIntegrationNextMonth ) {
		$this->m_boolIsChargeIntegrationNextMonth = CStrings::strToBool( $boolIsChargeIntegrationNextMonth );
	}

	public function setLastSyncedOn( $strLastSyncedOn ) {
		$this->m_strLastSyncedOn = CStrings::strTrimDef( $strLastSyncedOn, -1, NULL, true );
	}

	// Set methods of utility_auto_billing_settings table

	public function setRequirePreBillAnalystApproval( $boolRequirePreBillAnalystApproval ) {
		$this->m_boolRequirePreBillAnalystApproval = CStrings::strToBool( $boolRequirePreBillAnalystApproval );
	}

	public function setRequireBillPeriodApproval( $boolRequireBillPeriodApproval ) {
		$this->m_boolRequireBillPeriodApproval = CStrings::strToBool( $boolRequireBillPeriodApproval );
	}

	public function setRequireTransmissionApproval( $boolRequireTransmissionApproval ) {
		$this->m_boolRequireTransmissionApproval = CStrings::strToBool( $boolRequireTransmissionApproval );
	}

	public function setRequireBillSelectionApproval( $boolRequireBillSelectionApproval ) {
		$this->m_boolRequireBillSelectionApproval = CStrings::strToBool( $boolRequireBillSelectionApproval );
	}

	public function setRequireVerifyInvoiceApproval( $boolRequireVerifyInvoiceApproval ) {
		$this->m_boolRequireVerifyInvoiceApproval = CStrings::strToBool( $boolRequireVerifyInvoiceApproval );
	}

	public function setRequirePreBillCalculationApproval( $boolRequirePreBillCalculationApproval ) {
		$this->m_boolRequirePreBillCalculationApproval = CStrings::strToBool( $boolRequirePreBillCalculationApproval );
	}

	public function setRequireQcApproval( $boolRequireQcApproval ) {
		$this->m_boolRequireQcApproval = CStrings::strToBool( $boolRequireQcApproval );
	}

	public function setAdminDatabase( $objDatabase ) {
		$this->m_objAdminDatabase = $objDatabase;
	}

	public function setAutoApprovePreBill( $boolAutoApprovePreBill ) {
		$this->set( 'm_boolAutoApprovePreBill', CStrings::strToBool( $boolAutoApprovePreBill ) );
	}

	public function setUtilityBillAccountSetupErrors( $arrstrUtilityBillAccountSetupErrors ) {
		$this->m_arrstrUtilityBillAccountSetupErrors = $arrstrUtilityBillAccountSetupErrors;
	}

	public function setUtilityProviderChangeOfAddressWarnings( $arrmixAllUtilityProviderChangeOfAddressWarnings ) {
		$this->m_arrstrUtilityProviderChangeOfAddressWarnings = $arrmixAllUtilityProviderChangeOfAddressWarnings;
	}

	public function setPropertyCommodityWarnings( $arrmixAllPropertyCommodityWarnings ) {
		$this->m_arrstrPropertyCommodityWarnings = $arrmixAllPropertyCommodityWarnings;
	}

	/**
	 * Validate Functions
	 */

	public function valAnalystEmployeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAnalystEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'analyst_employee_id', 'Billing analyst is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {

		$boolIsValid = true;

		if( true == is_null( $this->getCompanyEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_id', 'Primary contact is required.' ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceDueDay() {

		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceDueDay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_due_day', 'Invoice Due Day is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsLateFeePercent() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsLateFeePercent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_late_fee_percent', 'Ps late fee percent is required.' ) );
		}

		if( false == is_null( $this->getPsLateFeePercent() ) && ( false == preg_match( '/^[0-9].*$/', $this->getPsLateFeePercent() ) || 100 < $this->getPsLateFeePercent() * 100 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_late_fee_percent', 'Invalid ps late fee percent.' ) );
		}

		return $boolIsValid;
	}

	public function valNonResidentPortalUrl() {

		$boolIsValid = true;

		if( false == is_null( $this->getNonResidentPortalUrl() ) && false == filter_var( $this->getNonResidentPortalUrl(), FILTER_VALIDATE_URL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_square_feet', 'Non resident portal url in not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valInitiationDate( $objUtilitiesDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getInitiationDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'initiation_date', 'Initiation date is required.' ) );
		} elseif( false == CValidation::validateDate( $this->getInitiationDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'initiation_date', 'Initiation date is not valid.' ) );
		} elseif( true == is_null( $this->getId() ) && ( strtotime( $this->getInitiationDate() ) < strtotime( date( 'm/d/Y', time() ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'initiation_date', 'Initiation date should not be less than today.' ) );
		}

		if( false == is_null( $this->getId() ) ) {
			$objPropertyUtilitySetting = \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingById( $this->getId(), $objUtilitiesDatabase );
			if( true == valObj( $objPropertyUtilitySetting, 'CPropertyUtilitySetting' ) && strtotime( $objPropertyUtilitySetting->getInitiationDate() ) != strtotime( $this->getInitiationDate() ) && ( strtotime( $this->getInitiationDate() ) < strtotime( date( 'm/d/Y', time() ) ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'initiation_date', 'Initiation date should not be less than today.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valTerminationDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getTerminationDate() ) && ( strtotime( $this->getInitiationDate() ) >= strtotime( $this->getTerminationDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_date', 'Termination date must be greater than Initiation date.' ) );
		}

		return $boolIsValid;
	}

	public function valDaysBetweenBills() {

		$boolIsValid = true;

		if( false == preg_match( '/^[0-9]*$/', $this->getDaysBetweenBills() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_between_bills', 'Invalid days between bills.' ) );
		}

		return $boolIsValid;
	}

	public function valWeightedValue() {
		$boolIsValid = true;

		if( false == is_null( $this->getWeightedValue() ) ) {
			if( true == 5 < strlen( floor( $this->getWeightedValue() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'weighted_value', 'Weighted value should not be greater than five digits before decimal.' ) );
			} elseif( true == preg_match( '/^\d+\.\d+$/', $this->getWeightedValue() ) && true == 2 < strcspn( \Psi\CStringService::singleton()->strrev( $this->getWeightedValue() ), '.' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'weighted_value', 'Weighted value should have maximum two decimal places.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valRolloutTypeid() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityRolloutTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_rollout_type_id', '! Utility Rollout Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsVcrOnly( $objUtilitiesDatabase ) {
		$boolIsValid = true;

		$objCurrentActiveBatch		= \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchActiveUtilityBatchByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );

		if( true == valObj( $objCurrentActiveBatch, 'CUtilityBatch' ) && true == $this->getIsVcrOnly() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_vcr_only', 'Property has active batch.' ) );
		}

		return $boolIsValid;
	}

	public function valIsInImplementationWithCameFrom( $boolIsUtilityProduct ) {

		$boolIsValid = true;

		if( true == $boolIsUtilityProduct && true == CStrings::strToBool( $this->getIsInImplementation() ) && true == is_null( $this->getUtilityCompetitorId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'initiation_date', 'Came from is required if the Implementation setting is checked.' ) );

		}

		return $boolIsValid;
	}

	public function valIsInImplementationWithBillingMonth( $boolIsUtilityProduct ) {

		$boolIsValid = true;

		if( true == $boolIsUtilityProduct && false == CStrings::strToBool( $this->getIsInImplementation() ) && true == is_null( $this->getBillingMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_in_implementation', 'The billing month must be set before you can take the property out of implementation mode.' ) );

		}

		if( true == $boolIsUtilityProduct && false == CStrings::strToBool( $this->getIsInImplementation() ) && true == is_null( $this->getChargeIntegrationDay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_in_implementation', 'Integration Day must be set before you can take the property out of implementation.' ) );

		}

		return $boolIsValid;
	}

	public function valOldPropertyId( $objUtilitiesDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getOldPropertyId() ) ) {
			$objOldPropertyUtilitySettings = \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingByPropertyId( $this->getOldPropertyId(), $objUtilitiesDatabase );

			if( $this->getOldPropertyId() == $this->getPropertyId() ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'old_property_id', 'Old Property ID can not be same as current Property ID.' ) );
			} else {
				if( false == valObj( $objOldPropertyUtilitySettings, 'CPropertyUtilitySetting' ) && false == is_null( $this->getOldPropertyId() ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'old_property_id', 'Please enter valid Old Property ID.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objUtilitiesDatabase = NULL, $boolIsUtilityProduct = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTerminationDate();
				$boolIsValid &= $this->valRolloutTypeid();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valInitiationDate( $objUtilitiesDatabase );
				$boolIsValid &= $this->valTerminationDate();
				$boolIsValid &= $this->valAnalystEmployeeId();
				$boolIsValid &= $this->valNonResidentPortalUrl();
				$boolIsValid &= $this->valWeightedValue();
				$boolIsValid &= $this->valRolloutTypeid();
				$boolIsValid &= $this->valInvoiceDueDay();
				$boolIsValid &= $this->valPsLateFeePercent();
				$boolIsValid &= $this->valIsVcrOnly( $objUtilitiesDatabase );
				break;

			case 'validate_property_utility_setting_in_settings':
				$boolIsValid &= $this->valInitiationDate( $objUtilitiesDatabase );
				$boolIsValid &= $this->valTerminationDate();
				$boolIsValid &= $this->valAnalystEmployeeId();
				$boolIsValid &= $this->valPsLateFeePercent();
				$boolIsValid &= $this->valIsInImplementationWithCameFrom( $boolIsUtilityProduct );
				$boolIsValid &= $this->valOldPropertyId( $objUtilitiesDatabase );
				$boolIsValid &= $this->valIsInImplementationWithBillingMonth( $boolIsUtilityProduct );
				break;

			case self::VALIDATE_UPDATE_PROPERTY_TAB:
				if( false == CStrings::strToBool( $this->getIsInImplementation() ) ) {
					$boolIsValid &= $this->valAnalystEmployeeId();
				}

				$boolIsValid &= $this->valIsInImplementationWithCameFrom( $boolIsUtilityProduct );
				break;

			case self::VALIDATE_UPDATE_BILLING_TAB:
				$boolIsValid &= $this->valInitiationDate( $objUtilitiesDatabase );
				$boolIsValid &= $this->valTerminationDate();
				$boolIsValid &= $this->valOldPropertyId( $objUtilitiesDatabase );
				$boolIsValid &= $this->valIsInImplementationWithBillingMonth( $boolIsUtilityProduct );
				break;

			case self::VALIDATE_UPDATE_PROPERTY_UTILITY_INFO:
				$boolIsValid &= $this->valAnalystEmployeeId();

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createUtilityBatch() {

		$objUtilityBatch = new CUtilityBatch();

		$objUtilityBatch->setCid( $this->getCid() );
		$objUtilityBatch->setPropertyId( $this->getPropertyId() );
		$objUtilityBatch->setUtilityBatchStepId( CUtilityBatchStep::VALIDATE );
		$objUtilityBatch->setBatchDatetime( 'NOW()' );
		$objUtilityBatch->setBillingMonth( $this->getBillingMonth() );

		if( true == $this->getAutoApprovePreBill() && false == $this->getIsFirstUtilityBatch() ) {
			$objUtilityBatch->setPreBillDeadline( $this->getPreBillDeadLine() );
		}

		if( false == is_null( $this->getDistributionDay() ) ) {
			$objUtilityBatch->setDistributionDeadline( $this->getDateFromBillingMonthByDay( $this->getDistributionDay() ) );
		}

		return $objUtilityBatch;
	}

	public function createPropertyUtilityType() {

		$objPropertyUtilityType = new CPropertyUtilityType();
		$objPropertyUtilityType->setDefaults();
		$objPropertyUtilityType->setCid( $this->getCid() );
		$objPropertyUtilityType->setPropertyId( $this->getPropertyId() );
		return $objPropertyUtilityType;
	}

	public function createUtilityBill( $objDatabase, $boolIsWithDocument = false ) {

		$objUtilityBill = new CUtilityBill();

		$objUtilityBill->setCid( $this->getCid() );
		$objUtilityBill->setPropertyId( $this->getPropertyId() );
		$objUtilityBill->setUtilityBillTypeId( CUtilityBillType::ACTUAL );
		$objUtilityBill->setUtilityBillPaymentTypeId( CUtilityBillPaymentType::BILL_PAYMENT_TYPE_PROPERTY_PAID );
		$objUtilityBill->setUtilityBillAccountId( $this->getUtilityBillAccountId() );
		$objUtilityBill->setBillDatetime( 'NOW()' );

		if( true == $boolIsWithDocument ) {
			$objUtilityDocument = $objUtilityBill->createUtilityDocument();

			// Set details of uploaded file.
			$arrmixFileDetails = $this->getFileDetails();
			$objUtilityDocument->setFileName( $arrmixFileDetails['file_name'] );
			$objUtilityDocument->setFileExtensionId( $arrmixFileDetails['file_extension_id'] );
			$objUtilityBillDocument = $objUtilityDocument->createUtilityBillDocument( $objDatabase );
			$objUtilityBillDocument->setCid( $this->getCid() );
			$objUtilityBillDocument->setPropertyId( $this->getPropertyId() );

			$objUtilityBill->setUtilityDocument( $objUtilityDocument );
			$objUtilityBill->setUtilityBillDocument( $objUtilityBillDocument );
		}

		return $objUtilityBill;
	}

	public function createUtilityBillAccount() {
		$objUtilityBillAccount = new CUtilityBillAccount();
		$objUtilityBillAccount->setCid( $this->getCid() );
		$objUtilityBillAccount->setPropertyId( $this->getPropertyId() );
		$objUtilityBillAccount->setAccountDatetime( 'NOW()' );
		return $objUtilityBillAccount;
	}

	public function createUtilityBillAccountDetail() {

		$objUtilityBillAccountDetail = new CUtilityBillAccountDetail();
		$objUtilityBillAccountDetail->setCid( $this->getCid() );
		$objUtilityBillAccountDetail->setPropertyId( $this->getPropertyId() );

		return $objUtilityBillAccountDetail;
	}

	public function createUtilityBillAccountType() {
		$objUtilityBillAccountType = new CUtilityBillAccountType();
		$objUtilityBillAccountType->setCid( $this->getCid() );
		$objUtilityBillAccountType->setPropertyId( $this->getPropertyId() );
		return $objUtilityBillAccountType;
	}

	public function createPropertyMeterDevice() {
		$objPropertyMeterDevice = new CPropertyMeterDevice();
		$objPropertyMeterDevice->setCid( $this->getCid() );
		$objPropertyMeterDevice->setPropertyId( $this->getPropertyId() );
		return $objPropertyMeterDevice;
	}

	public function createUtilityTransmissionDetail() {
		$objUtilityTransmissionDetail = new CUtilityTransmissionDetail();
		$objUtilityTransmissionDetail->setPropertyId( $this->getPropertyId() );
		$objUtilityTransmissionDetail->setCid( $this->getCid() );
		return $objUtilityTransmissionDetail;
	}

	public function createUtilityTransmission() {
		$objUtilityTransmission = new CUtilityTransmission();
		$objUtilityTransmission->setPropertyId( $this->getPropertyId() );
		$objUtilityTransmission->setCid( $this->getCid() );
		return $objUtilityTransmission;
	}

	public function createUtilityBillAccountMeterUtilityType() {
		$objUtilityBillAccountMeterUtilityType = new CUtilityBillAccountMeterUtilityType();
		$objUtilityBillAccountMeterUtilityType->setPropertyId( $this->getPropertyId() );
		$objUtilityBillAccountMeterUtilityType->setCid( $this->getCid() );
		return $objUtilityBillAccountMeterUtilityType;
	}

	public function createUtilityBillAccountMeterCommodity() {
		$objUtilityBillAccountMeterCommodity = new CUtilityBillAccountMeterCommodity();
		$objUtilityBillAccountMeterCommodity->setPropertyId( $this->getPropertyId() );
		$objUtilityBillAccountMeterCommodity->setCid( $this->getCid() );
		return $objUtilityBillAccountMeterCommodity;
	}

	public function createUtilityBillAccountUnit() {

		$objUtilityBillAccountUnit = new CUtilityBillAccountUnit();
		$objUtilityBillAccountUnit->setCid( $this->getCid() );
		$objUtilityBillAccountUnit->setPropertyId( $this->getPropertyId() );

		return $objUtilityBillAccountUnit;
	}

	public function createUtilityBillAccountMeter() {

		$objUtilityBillAccountMeter = new CUtilityBillAccountMeter();
		$objUtilityBillAccountMeter->setCid( $this->getCid() );
		$objUtilityBillAccountMeter->setPropertyId( $this->getPropertyId() );

		return $objUtilityBillAccountMeter;
	}

	public function createUtilityBillAccountTemplateCharge() {

		$objUtilityBillAccountTemplateCharge = new CUtilityBillAccountTemplateCharge();
		$objUtilityBillAccountTemplateCharge->setCid( $this->getCid() );
		$objUtilityBillAccountTemplateCharge->setPropertyId( $this->getPropertyId() );

		return $objUtilityBillAccountTemplateCharge;
	}

	public function createUtilityBillAccountMeterTemplateCharge() {

		$objUtilityBillAccountMeterTemplateCharge = new CUtilityBillAccountMeterTemplateCharge();
		$objUtilityBillAccountMeterTemplateCharge->setCid( $this->getCid() );
		$objUtilityBillAccountMeterTemplateCharge->setPropertyId( $this->getPropertyId() );

		return $objUtilityBillAccountMeterTemplateCharge;
	}

	public function createUtilityBillAccountCommodity() {
		$objUtilityBillAccountCommodity = new CUtilityBillAccountCommodity();
		$objUtilityBillAccountCommodity->setCid( $this->getCid() );
		$objUtilityBillAccountCommodity->setPropertyId( $this->getPropertyId() );
		return $objUtilityBillAccountCommodity;
	}

	public function createPropertyCommodity() {
		$objPropertyCommodity = new CPropertyCommodity();
		$objPropertyCommodity->setCid( $this->getCid() );
		$objPropertyCommodity->setPropertyId( $this->getPropertyId() );
		return $objPropertyCommodity;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCompanyEmployee( $objClientDatabase ) {

		return CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $this->getCompanyEmployeeId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchRegionalCompanyEmployee( $objClientDatabase ) {

		return CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $this->getRegionalCompanyEmployeeId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchComplexPropertyUtilityTypes( $objUtilitiesDatabase ) {

		$this->m_arrobjPropertyUtilityTypes = \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchComplexPropertyUtilityTypesByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );

		return $this->m_arrobjPropertyUtilityTypes;
	}

	public function fetchActivePropertyUtilityTypes( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchActivePropertyUtilityTypesByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase, $this->getIsUemProperty() );

	}

	public function fetchPropertyCommodities( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyCommodities::createService()->fetchCustomPropertyCommoditiesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objUtilitiesDatabase );
	}

	public function fetchProperty( $objAdminDatabase ) {

		return \Psi\Eos\Admin\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objAdminDatabase );
	}

	public function fetchUtilityAccounts( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CUtilityAccounts::createService()->fetchUtilityAccountsByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchUtilityBatches( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchUtilityBatchesByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchPropertyUtilityTypes( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchCustomPropertyUtilityTypesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objUtilitiesDatabase );
	}

	public function fetchPropertyUtilityTypesByHasVcrService( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchPropertyUtilityTypesByPropertyIdByHasVcrService( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchAllPropertyUtilityTypes( $objUtilitiesDatabase, $arrintPropertyUtilityTypeIds = [] ) {

		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchAllPropertyUtilityTypesByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase, $arrintPropertyUtilityTypeIds );
	}

	public function fetchPropertyAddress( $objClientDatabase ) {

		return \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getPropertyId(), CAddressType::PRIMARY, $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyVcrSettings( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CPropertyVcrSettings::createService()->fetchPropertyVcrSettingsByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchAccounts( $objAdminDatabase ) {
		return \Psi\Eos\Admin\CAccounts::createService()->fetchAccountsByCidOrderByAccountName( $this->getCid(), $objAdminDatabase );
	}

	public function fetchAssociatedCompanyEmployeesWithProperties( $objDatabase ) {

		$arrobjCompanyEmployees 		= ( array ) CCompanyEmployees::createService()->fetchAssociatedCompanyEmployeesByCid( $this->getCid(), $objDatabase );
		$arrmixCompanyUserProperties 	= \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchComplexCompanyUserPropertiesByCid( $this->getCid(), $objDatabase );

		if( true == valArr( $arrmixCompanyUserProperties ) ) {
			foreach( $arrmixCompanyUserProperties as $arrmixTempCompanyUserProperties ) {
				if( true == array_key_exists( $arrmixTempCompanyUserProperties['company_employee_id'], $arrobjCompanyEmployees ) ) {
					$objCompanyEmployee = $arrobjCompanyEmployees[$arrmixTempCompanyUserProperties['company_employee_id']];
					$objCompanyEmployee->addPropertyId( $arrmixTempCompanyUserProperties['property_id'] );
				}
			}
		}

		return $arrobjCompanyEmployees;
	}

	public function fetchCompanyDepartments( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyDepartments::createService()->fetchCompanyDepartmentsByCid( $this->getCid(), $objDatabase );
	}

	public function fetchUtilityBillAccountDetailsByPropertyUtilityTypeId( $intPropertyUtilityTypeId, $objUtilitiesDatabase, $boolFetchInactiveAccounts = false, $boolShowDontRequireBills = true ) {
		return \Psi\Eos\Utilities\CUtilityBillAccountDetails::createService()->fetchUtilityBillAccountDetailsByPropertyIdByPropertyUtilityTypeId( $this->getPropertyId(), $intPropertyUtilityTypeId, $objUtilitiesDatabase, $boolFetchInactiveAccounts, false, $this->getIsUemProperty(), $boolShowDontRequireBills );
	}

	public function fetchSubmeteredPropertyUtilityTypes( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchSubmeteredPropertyUtilityTypesByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchLeaseCustomersByLeaseStatusTypeIdsByCustomerTypeIds( $arrintCustomerTypes, $arrintLeaseStatusTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByLeaseStatusTypeIdsByCustomerTypeIdsByPropertyIdByCid( $arrintCustomerTypes, $arrintLeaseStatusTypeIds, $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUtilityTypeRates( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyUtilityTypeRates::createService()->fetchPropertyUtilityTypeRatesByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchOldUtilityBillAccounts( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchOldUtilityBillAccountsByPropertyId( $this->getPropertyId(), $objDatabase );
	}

	public function fetchUnitTypes( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CUnitTypes::createService()->fetchCustomUnitTypesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchCompanyEmployees( $objUtilitiesDatabase ) {
		return CCompanyEmployees::createService()->fetchEnabledCompanyEmployeesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objUtilitiesDatabase );
	}

	public function fetchUtilityAccountsByIds( $arrintUtilityAccountIds, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityAccounts::createService()->fetchUtilityAccountsByPropertyIdByIds( $this->getPropertyId(), $arrintUtilityAccountIds, $objUtilitiesDatabase );
	}

	public function fetchUtilityAccountsByUtilityBatchIdByIds( $intUtilityBatchId, $arrintUtilityAccountIds, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityAccounts::createService()->fetchUtilityAccountsByUtilityBatchIdByIds( $intUtilityBatchId, $arrintUtilityAccountIds, $objUtilitiesDatabase );
	}

	public function fetchActiveUtilityBatch( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchActiveUtilityBatchByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchPropertyUnits( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchSimplePropertyUnitsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyUnitsByIds( $arrintPropertyUnitIds, $objClientDatabase ) {
		return CPropertyUnits::fetchPropertyUnitByPropertyIdByIds( $this->getPropertyId(), $arrintPropertyUnitIds, $objClientDatabase );
	}

	public function fetchPropertyUnitsByUnitTypeIds( $arrintPropertyUnitTypeIds, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitsByPropertyIdByUnitTypeIdsByCid( $this->getPropertyId(), $arrintPropertyUnitTypeIds, $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyUtilityTypeByIds( $arrintPropertyUtilityTypeIds, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchPropertyUtilityTypesByPropertyIdByIds( $this->getPropertyId(), $arrintPropertyUtilityTypeIds, $objUtilitiesDatabase );
	}

	public function fetchUnApprovedUtilityChangeRequests( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityChangeRequests::createService()->fetchUnApprovedUtilityChangeRequestsByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchProcessedUtilityBillsByUtilityBatchIdByPropertyUtilityTypeIds( $intUtilityBatchId, $arrintPropertyUtilityTypeIds, $objUtilitiesDatabase, $boolIsPreviousBatchData = false ) {
		return \Psi\Eos\Utilities\CUtilityBills::createService()->fetchProcessedUtilityBillsByPropertyIdByUtilityBatchIdByPropertyUtilityTypeIds( $this, $intUtilityBatchId, $arrintPropertyUtilityTypeIds, $objUtilitiesDatabase, $boolIsPreviousBatchData );
	}

	public function fetchPropertyMeterDeviceCount( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyMeterDevices::createService()->fetchPropertyMeterDeviceCountByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchUtilityTransmissionByPropertyUtilityTypeIdByTransmissionDateTimeRange( $intPropertyUtilityTypeId, $strTransmissionDate, $strSortType, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransmissions::createService()->fetchUtilityTransmissionByPropertyIdByPropertyUtilityTypeIdByTransmissionDateTimeRange( $this->getPropertyId(), $intPropertyUtilityTypeId, $strTransmissionDate, $strSortType, $objUtilitiesDatabase );
	}

	public function fetchUtilityTransmissionDetailsByUtilityTransmissionId( $intUtilityTransmissionId, $objUtilitiesDatabase, $strOrderBy = NULL ) {
		return \Psi\Eos\Utilities\CUtilityTransmissionDetails::createService()->fetchUtilityTransmissionDetailsByPropertyIdByUtilityTransmissionId( $this->getPropertyId(), $intUtilityTransmissionId, $objUtilitiesDatabase, $strOrderBy );
	}

	public function fetchPropertyMeterDevicesByPropertyUnitIds( $arrintPropertyUnitIds, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyMeterDevices::createService()->fetchPropertyMeterDevicesByPropertyIdByPropertyUnitIds( $this->getPropertyId(), $arrintPropertyUnitIds, $objUtilitiesDatabase );
	}

	public function fetchPropertyBuildings( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchPropertyBuildingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchUnapprovedUtilityTransactionsByUtilityTransactionTypeIdsByUtilityAccountStatusTypeIds( $arrintUtilityTransactionTypeIds, $arrintUtilityAccountStatusTypeIds, $objUtilitiesDatabase ) {
		if( false == valArr( $arrintUtilityTransactionTypeIds ) || false == valArr( $arrintUtilityAccountStatusTypeIds ) ) {
			return NULL;
		}

		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUnapprovedUtilityTransactionsByCidByPropertyIdByUtilityTransactionTypeIdsByUtilityAccountStatusTypeIds( $this->getCid(), $this->getPropertyId(), $arrintUtilityTransactionTypeIds, $arrintUtilityAccountStatusTypeIds, $objUtilitiesDatabase );
	}

	public function fetchProcessedUtilityBillsByPropertyUtilityTypeIdsByUtilityBatchIdOrderByPropertyUtilityTypeId( $arrintPropertyUtilityTypeIds, $intUtilityBatchId, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBills::createService()->fetchProcessedUtilityBillsByPropertyUtilityTypeIdsByPropertyIdByUtilityBatchId( $arrintPropertyUtilityTypeIds, $this->getPropertyId(), $intUtilityBatchId, $objUtilitiesDatabase );
	}

	public function fetchPreviousUtilityBatchId( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchPreviousUtilityBatchIdBypropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchCompletedPreviousUtilityBatchIdsByUtilityBillAccountId( $strStartDate, $strEndDate, $intUtilityBillAccountId, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchCompletedPreviousUtilityBatchesByPropertyIdByBatchDatetimeRangeByUtilityBillAccountId( $this->getPropertyId(), $strStartDate, $strEndDate, $intUtilityBillAccountId, $objUtilitiesDatabase );
	}

	public function fetchUtilityAccountsCount( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityAccounts::createService()->fetchUtilityAccountsCountByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchPreviousUtilityBatchByUtilityBatchStepId( $intUtilityBatchStepId, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchPreviousUtilityBatchByPropertyIdByUtilityBatchStepId( $this->getPropertyId(), $intUtilityBatchStepId, $objUtilitiesDatabase );
	}

	public function fetchMissingUtilityBillsCount( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBills::createService()->fetchMissingUtilityBillsCountByCidByPropertyIds( $this->getCid(), [ $this->getPropertyId() ], $objDatabase );
	}

	public function fetchAnalystEmployee( $objClientDatabase ) {
		return CEmployees::fetchEmployeeById( $this->getAnalystEmployeeId(), $objClientDatabase );
	}

	public function fetchNonBillingPropertyUtilityTypeIds( $objDatabase ) {
		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchNonBillingPropertyUtilityTypeIdsByPropertyId( $this->getPropertyId(), $objDatabase );
	}

	public function fetchClient( $objAdminDatabase ) {
		return CClients::fetchClientById( $this->getCid(), $objAdminDatabase );
	}

	public function fetchUtilityTransmissionsByDateRange( $strStartDate, $strEndDate, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransmissions::createService()->fetchUtilityTransmissionsByPropertyIdByDateRange( $this->getPropertyId(), $strStartDate, $strEndDate, $objUtilitiesDatabase );
	}

	public function fetchUtilityPropertyUnitsByPropertyUnitId( $intPropertyUnitId, $objDatabase, $intPropertyUnitSpaceId = NULL ) {
		return \Psi\Eos\Utilities\CUtilityPropertyUnits::createService()->fetchUtilityPropertyUnitsByPropertyIdByPropertyUnitId( $this->getPropertyId(), $intPropertyUnitId, $objDatabase, $intPropertyUnitSpaceId );
	}

	public function fetchReadyBillCount( $objUtilitiesDatabase, $boolRequireBillAccount = false ) {
		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchReadyBillsCountByPropertyIds( ( array ) $this->getPropertyId(), $objUtilitiesDatabase, $boolRequireBillAccount );
	}

	public function fetchOutstandingBillCount( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchOutstandingBillsCountByPropertyIds( ( array ) $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function fetchCustomPropertyMeteringSettings( $objDatabase ) {
		return \Psi\Eos\Utilities\CPropertyMeteringSettings::createService()->fetchCustomPropertyMeteringSettingsByPropertyId( $this->getPropertyId(), $objDatabase );
	}

	public function fetchUtilityTransmissionDetailByPropertyUtilityTypeIdByPropertyUnitIdByTransmissionDateTimeRange( $intPropertyUtilityTypeId, $intPropertyUnitId, $strTransmissionDate, $strSortType, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransmissionDetails::createService()->fetchUtilityTransmissionDetailByPropertyIdByPropertyUtilityTypeIdByPropertyUnitIdByTransmissionDateTimeRange( $this->getPropertyId(), $intPropertyUtilityTypeId, $intPropertyUnitId, $strTransmissionDate, $strSortType, $objUtilitiesDatabase );
	}

	public function fetchUtilityBillAccountDetailsByPropertyUtilityTypeIdByCid( $intCid, $intPropertyUtilityTypeId, $intCommodityId, $objUtilitiesDatabase, $strBillAccountSubTab = \CUtilityBillAccount::STR_ACTIVE_TAB_TXT, $boolShowDontRequireBills = true ) {
		return ( array ) \Psi\Eos\Utilities\CUtilityBillAccountDetails::createService()->fetchUtilityBillAccountDetailsByPropertyIdByPropertyUtilityTypeIdByCid( $this->getPropertyId(), $intCid, $intPropertyUtilityTypeId, $intCommodityId, $objUtilitiesDatabase, $strBillAccountSubTab, false, $this->getIsUemProperty(), $boolShowDontRequireBills );
	}

	public function fetchUemGoLiveDate( $intCid, $objUtilitiesDatabase ) {

		$arrobjPropertyCommodities = reKeyObjects( 'CommodityId', \Psi\Eos\Utilities\CPropertyCommodities::createService()->fetchPropertyCommoditiesByPropertyIds( [ $this->getPropertyId() ], $objUtilitiesDatabase ) );

		$arrstrUemGoLiveDate = [];
		if( true == valArr( $arrobjPropertyCommodities ) ) {
			foreach( $arrobjPropertyCommodities as $objPropertyCommodity ) {

				if( true == valObj( $objPropertyCommodity, 'CPropertyCommodity' ) ) {
					$strUemGoLiveDate = $this->createFormattedDate( $objPropertyCommodity->getUemGoLiveDate() );
					$arrstrUemGoLiveDate[] = strtotime( $strUemGoLiveDate );
				}
			}
		}

		$arrstrAllUemGoLiveDates = array_unique( $arrstrUemGoLiveDate );

		$strUemGoLiveDate = NULL;

		if( true == valArr( $arrstrAllUemGoLiveDates ) && 1 == \Psi\Libraries\UtilFunctions\count( $arrstrAllUemGoLiveDates ) ) {
			$strUemGoLiveDate = date( 'm/d/Y', $arrstrAllUemGoLiveDates[0] );
		} elseif( true == valArr( $arrstrAllUemGoLiveDates ) ) {
			$strUemGoLiveDate = date( 'm/d/Y', min( $arrstrAllUemGoLiveDates ) );
		}

		return $strUemGoLiveDate;
	}

	public function createFormattedDate( $strDateTime ) {

		if( false == valStr( $strDateTime ) ) {
			return NULL;
		}

		$strNewlyCreatedDate = new DateTime( $strDateTime );

		$strFormattedDate = $strNewlyCreatedDate->format( 'm/d/Y' );

		return $strFormattedDate;
	}

	/**
	 * Other Functions
	 */

	public function importUtilityCustomers( $objUser, $objUtilitiesDatabase, $objClientDatabase, $boolUpdateLeaseCharges = true ) {
		set_time_limit( 1000 );
        $strSqlSetMaxTime = 'SET STATEMENT_TIMEOUT = \'1200s\';';

        executeSql( $strSqlSetMaxTime, $objClientDatabase );

		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			trigger_error( 'Application Error : Failed to load property object.', E_USER_ERROR );
		}

		$objClient = $objProperty->fetchClient( $objClientDatabase );

		$boolIsProcessed = true;

		if( false == $objProperty->isIntegrated( $objClientDatabase ) ) {
			$boolIsProcessed = $this->importUtilityPropertyAccountsWithTransactions( $objUser->getId(), $objProperty, $objUtilitiesDatabase, $objClientDatabase, $boolUpdateLeaseCharges );
		} else {
			require_once PATH_APPLICATION_SERVICES . 'Library/CServicesTokenManager.class.php';

			$strUrl = 'https://' . $objClient->getRwxDomain() . '.' . CONFIG_COMPANY_BASE_DOMAIN . '/api/internal/integration';
			$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objClientDatabase );

			$objServiceTokenManager = CServicesTokenManager::getInstance();
			$strToken = $objServiceTokenManager->getToken( CServicesTokenManager::CLIENT_ADMIN_SERVICES );
			$strHeaderToken = base64_encode( CServicesTokenManager::CLIENT_ADMIN_SERVICES . ':' . $strToken );

			$strBody = '<request>
							<auth>
								<type>internal</type>
							</auth>
							<requestId>' . $objUser->getId() . '</requestId>
							<consumer>internal</consumer>
							<method>
							<name>getIntegrationCallResult</name>
							<params>
							<integrationServiceId>' . CIntegrationService::RETRIEVE_CUSTOMERS . '</integrationServiceId>
							<integrationDatabaseId>' . $objIntegrationDatabase->getId() . '</integrationDatabaseId>
							<propertyIds>' . $objProperty->getId() . '</propertyIds>
							</params>
							</method>
						</request>';

			$arrmixParameters = [
				'url'               => $strUrl,
				'request_method'    => 'POST',
				'request_data'      => $strBody,
				'header_info'       => [ 'Content-type: APPLICATION/XML; charset=UTF-8', 'Authorization: Basic ' . $strHeaderToken ],
				'ssl_verify_peer'   => false,
				'execution_timeout' => 3000
			];

			$objExternalRequest = new CExternalRequest();
			$objExternalRequest->setParameters( $arrmixParameters );

			if( false === $objExternalRequest->execute( $boolGetResponse = true ) ) {
				$boolIsProcessed = false;
			}

			if( CIntegrationClientType::AMSI != $objIntegrationDatabase->getIntegrationClientTypeId() ) {

				$strBody = ' <request>
									<auth>
										<type>internal</type>
									</auth>
									<requestId>' . $objUser->getId() . '</requestId>
									<consumer>internal</consumer>
									<method>
									<name>getIntegrationCallResult</name>
									<params>
									<integrationServiceId>' . CIntegrationService::RETRIEVE_CUSTOMERS_WITH_BALANCE_DUE . '</integrationServiceId>
									<integrationDatabaseId>' . $objIntegrationDatabase->getId() . '</integrationDatabaseId>
									<propertyIds>' . $objProperty->getId() . '</propertyIds>
									</params>
									</method>
								</request>';

				$objExternalRequest->setRequestData( $strBody );

				if( false === $objExternalRequest->execute( $boolGetResponse = true ) ) {
					$boolIsProcessed = false;
				}
			}

			unset( $objExternalRequest );

			$arrintLeaseStatusTypeIds = [
				CLeaseStatusType::FUTURE,
				CLeaseStatusType::NOTICE,
				CLeaseStatusType::PAST,
				CLeaseStatusType::CURRENT
			];

			// Adding Real page integration client type for cid - 14246
			$arrintIntegrationClientTypeIds = [
				CIntegrationClientType::YARDI,
				CIntegrationClientType::MRI,
				CIntegrationClientType::REAL_PAGE,
				CIntegrationClientType::AMSI,
				CIntegrationClientType::YARDI_RPORTAL
			];

			if( true == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), $arrintIntegrationClientTypeIds ) ) {

				$arrintCustomerTypeIds = [ CCustomerType::PRIMARY ];

				// @TODO Test it thoroughly in second phase refactoring
				$arrmixLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByPropertyIdsByLeaseStatusTypeIdsByCustomerTypeIdsByCid( [ $objProperty->getId() ], $arrintLeaseStatusTypeIds, $arrintCustomerTypeIds, $this->getCid(), $objClientDatabase );

				if( false == valArr( $arrmixLeaseCustomers ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_resident', 'No resident found.' ) );
					return true;
				}

				$arrintLeaseIds = $arrmixData = $arrmixCustomerLeaseCustomers = $arrmixFutureCustomerLeaseCustomers = [];

				foreach( $arrmixLeaseCustomers as $arrmixLeaseCustomer ) {

					$intLeaseId = $arrmixLeaseCustomer['lease_id'];

					if( true == getArrayElementByKey( $intLeaseId, $arrmixData ) ) {
						continue;
					}

					$arrintLeaseIds[$intLeaseId] = $intLeaseId;

					// taking only current & Future status
					if( $arrmixLeaseCustomer['lease_status_type_id'] == CLeaseStatusType::CURRENT ) {
						$arrmixCustomerLeaseCustomers[$arrmixLeaseCustomer['customer_id']][$intLeaseId] = $arrmixLeaseCustomer;
					}

					if( $arrmixLeaseCustomer['lease_status_type_id'] == CLeaseStatusType::FUTURE ) {
						$arrmixFutureCustomerLeaseCustomers[$arrmixLeaseCustomer['customer_id']][$intLeaseId] = $arrmixLeaseCustomer;
					}
				}

				$arrobjRekeyLeaseIntervals = [];

				$arrobjLeaseIntervals = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchActiveLeaseIntervalsByLeaseIdsByCid( $arrintLeaseIds, $this->getCid(), $objClientDatabase );

				if( true == valArr( $arrobjLeaseIntervals ) ) {
					$arrobjRekeyLeaseIntervals = rekeyObjects( 'LeaseId', $arrobjLeaseIntervals );
				}

				$arrobjScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchFutureScheduledChargesByLeaseIdsByCid( $arrintLeaseIds, $this->getCid(), $objClientDatabase );

				// To add the pre-convergent charges
				if( true == valArr( $arrobjScheduledCharges ) && true == $boolUpdateLeaseCharges ) {

					$strNextPostedOn = ( 1 == date( 'd' ) ) ? date( 'm/01/Y' ) : date( 'm/t/Y', strtotime( date( 'm/d/Y', strtotime( '+1 month', strtotime( date( 'm/01/y' ) ) ) ) ) );

					$objClient = $objProperty->fetchClient( $objClientDatabase );

					$strSql = 'UPDATE scheduled_charges SET lease_association_id = NULL WHERE property_id = ' . ( int ) $objProperty->getId() . '  AND cid = ' . ( int ) $objClient->getId() . ' AND lease_association_id = 0 AND deleted_on IS NULL';
					fetchData( $strSql, $objClientDatabase );

					$objUtilityBatch = $this->fetchActiveUtilityBatch( $objUtilitiesDatabase );

					$strSubSql = '';

					if( true == valObj( $objUtilityBatch, 'CUtilityBatch' ) ) {
						$strSubSql = ' OR utility_batch_id = ' . ( int ) $objUtilityBatch->getId();
					}

					// Removing the scheduled utility transactions first to avoid duplication in case of integration failure.

					$strSql = 'DELETE
								FROM
									utility_transactions
								WHERE
									( ( scheduled_charge_id IN( ' . implode( ',', array_keys( $arrobjScheduledCharges ) ) . ' )
									AND utility_transaction_type_id = ' . CUtilityTransactionType::PRE_POSTED_CONVERGENT . ' ) OR utility_transaction_type_id = ' . CUtilityTransactionType::TAX . ' )
									AND ( utility_batch_id IS NULL ' . $strSubSql . ' )
									AND original_utility_transaction_id IS NULL
									AND cid = ' . ( int ) $objClient->getId() . '
									AND property_id = ' . ( int ) $objProperty->getId();

					fetchData( $strSql, $objUtilitiesDatabase );

					$objClientDatabase->begin();

					$strSql = 'UPDATE property_charge_settings SET accelerated_rent_type_id = ' . CAcceleratedRentType::NEVER_POST . ' WHERE property_id = ' . ( int ) $objProperty->getId() . '  AND cid = ' . ( int ) $objClient->getId();
					fetchData( $strSql, $objClientDatabase );

					$objPostScheduledChargesLibrary 	= CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::UTILITY_TRANSACTIONS, $objClientDatabase );
					$objPostScheduledChargesCriteria	= $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $objClient->getId(), [ $objProperty->getId() ], $objUser->getId() );

					$objPostScheduledChargesCriteria->setDataReturnTypeId( CPostScheduledChargesCriteria::DATA_RETURN_TYPE_AR_TRANSACTIONS )
													->setIsDryRun( true )
													->setEndScheduledChargesForcefully( true )
													->setPostWindowEndDateOverride( $strNextPostedOn )
													->setScheduledChargeIds( array_keys( $arrobjScheduledCharges ) )
													->setLeaseIds( array_keys( rekeyObjects( 'LeaseId', $arrobjScheduledCharges ) ) )
													->setRegenerateHistoricalPeriod( true );

					$objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria );
					// Tax transactions are generated in above call, Will remove the call after testing from Utility team
					// $objPostScheduledChargesLibrary->createTaxArTransaction( $objClient->getId(), $objProperty->getId(), $objClientDatabase );
					$arrobjArTransactions = $objPostScheduledChargesLibrary->getArTransactions();
					$objClientDatabase->rollback();

					if( false == valArr( $arrobjArTransactions ) ) {
						return true;
					}

					// remove all ar transactions whose having post month less than or equal to current month
					foreach( $arrobjArTransactions as $intKey => $objArTransaction ) {
						if( date( 'm', strtotime( $objArTransaction->getPostMonth() ) ) <= date( 'm' ) && date( 'Y', strtotime( $objArTransaction->getPostMonth() ) ) <= date( 'Y' ) ) {
							unset( $arrobjArTransactions[$intKey] );
						}
					}

					$arrobjArCodes		   = ( array ) $objProperty->fetchArCodes( $objClientDatabase );
					$arrobjUtilityAccounts = ( array ) $this->fetchUtilityAccounts( $objUtilitiesDatabase );

					$arrobjReKeyUtilityAccounts = [];

					foreach( $arrobjUtilityAccounts as $objUtilityAccount ) {
						$arrobjReKeyUtilityAccounts[$objUtilityAccount->getLeaseId()] = $objUtilityAccount;
					}

					// Delete duplicate convergent utility transactions block
					$arrobjConvergentUtilityTransactions = [];
					if( true == valArr( $arrobjUtilityAccounts ) ) {
						$strSql = 'SELECT
										*
									FROM
										utility_transactions
									WHERE
										utility_account_id IN ( ' . implode( ',', array_keys( $arrobjUtilityAccounts ) ) . ' )
										AND utility_transaction_type_id = ' . CUtilityTransactionType::CONVERGENT . '
										AND cid = ' . ( int ) $objClient->getId() . '
										AND property_id = ' . ( int ) $objProperty->getId() . '
										AND original_utility_transaction_id IS NULL
										AND ar_transaction_id IS NOT NULL
										AND deleted_by IS NULL
										AND deleted_on IS NULL';

						$arrobjConvergentUtilityTransactions = ( array ) \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUtilityTransactions( $strSql, $objUtilitiesDatabase );
					}

					$arrintScheduledChargeIds = array_keys( $arrobjScheduledCharges );

					$arrobjTempArTransactions = [];
					if( true == valArr( $arrobjConvergentUtilityTransactions ) ) {

						$strSql = 'SELECT
									*
									FROM
										ar_transactions
									WHERE
										id IN (' . implode( ',', array_keys( rekeyObjects( 'ArTransactionId', $arrobjConvergentUtilityTransactions ) ) ) . ' )
										AND scheduled_charge_id IN (' . implode( ',', $arrintScheduledChargeIds ) . ' )
										AND cid = ' . ( int ) $objClient->getId();

						$arrobjTempArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactions( $strSql, $objClientDatabase );
					}

					foreach( $arrobjConvergentUtilityTransactions as $objConvergentUtilityTransaction ) {

						if( true == is_null( $objConvergentUtilityTransaction ->getArTransactionId() ) ) {
							continue;
						}

						$objArTransaction = getArrayElementByKey( $objConvergentUtilityTransaction->getArTransactionId(), $arrobjTempArTransactions );

						if( false == valObj( $objArTransaction, 'CArTransaction' ) ) {
							continue;
						}

						$objScheduledCharge = getArrayElementByKey( $objArTransaction->getScheduledChargeId(), $arrobjScheduledCharges );

						if( true == valObj( $objScheduledCharge, 'CScheduledCharge' ) && date( 'm', strtotime( $objScheduledCharge->getLastPostedOn() ) ) <= date( 'm' ) && date( 'Y', strtotime( $objScheduledCharge->getLastPostedOn() ) ) <= date( 'Y' ) ) {
							continue;
						}

						if( false == $objConvergentUtilityTransaction->delete( $objUser->getId(), $objUtilitiesDatabase ) ) {
							trigger_error( 'Utility transaction id ' . $objConvergentUtilityTransaction->getId() . ' :: could not be deleted. .', E_USER_WARNING );
						}
					}
					// block ends

					foreach( $arrobjArTransactions as $objArTransaction ) {

						if( false == is_null( $objArTransaction->getScheduledChargeId() ) ) {

							$objScheduledCharge = getArrayElementByKey( $objArTransaction->getScheduledChargeId(), $arrobjScheduledCharges );

							if( true == valObj( $objScheduledCharge, 'CScheduledCharge' ) && CArTrigger::YEARLY == $objScheduledCharge->getArTriggerId() && false == is_null( $objScheduledCharge->getLastPostedOn() ) && false == is_null( $objScheduledCharge->getPostedThroughDate() ) ) {

								$objStartDate = new DateTime( $objScheduledCharge->getLastPostedOn() );
								$objEndDate   = new DateTime( $objScheduledCharge->getPostedThroughDate() );

								$intYear = $objStartDate->diff( $objEndDate )->format( '%y' );

								if( 0 < $intYear ) {
									continue;
								}
							}

						}

						$objUtilityAccount 	= getArrayElementByKey( $objArTransaction->getLeaseId(), $arrobjReKeyUtilityAccounts );
						$objArCode 			= getArrayElementByKey( $objArTransaction->getArCodeId(), $arrobjArCodes );

						if( false == valObj( $objUtilityAccount, 'CUtilityAccount' ) || false == valObj( $objArCode, 'CArCode' ) ) {
							continue;
						}

						// To remove charge after move out date
						if( false == is_null( $objUtilityAccount->getMoveOutDate() ) && strtotime( $objArTransaction->getPostMonth() ) > strtotime( $objUtilityAccount->getMoveOutDate() ) ) {
							continue;
						}

						// here looking for current lease customer
						$arrmixTempLeaseCustomer = getArrayElementByKey( $objUtilityAccount->getCustomerId(), $arrmixCustomerLeaseCustomers );
						$arrmixLeaseCustomer 	 = getArrayElementByKey( $objUtilityAccount->getCustomerId(), $arrmixFutureCustomerLeaseCustomers );

						$arrmixCurrentLeaseCustomer = getArrayElementByKey( $objArTransaction->getLeaseId(), $arrmixTempLeaseCustomer );
						$arrmixFutureLeaseCustomer = getArrayElementByKey( $objArTransaction->getLeaseId(), $arrmixLeaseCustomer );

						if( true == valArr( $arrmixCurrentLeaseCustomer ) && true == valArr( $arrmixFutureLeaseCustomer ) && CLeaseStatusType::FUTURE == $arrmixFutureLeaseCustomer['lease_status_type_id'] ) {

							$objCurrentLeaseInterval = getArrayElementByKey( $arrmixCurrentLeaseCustomer['lease_id'], $arrobjRekeyLeaseIntervals );
							$objFutureLeaseInterval  = getArrayElementByKey( $arrmixFutureLeaseCustomer['lease_id'], $arrobjRekeyLeaseIntervals );
							$arrstrFutureDate 		 = explode( '/', date( 'm/d/Y', strtotime( '+1 month', strtotime( date( 'm/01/y' ) ) ) ) );

							if( strtotime( $objFutureLeaseInterval->getLeaseStartDate() ) > strtotime( $objCurrentLeaseInterval->getLeaseEndDate() ) ) {
								if( ( date( 'm' ) == date( 'm', strtotime( $objCurrentLeaseInterval->getLeaseEndDate() ) ) ) && ( date( 'Y' ) == date( 'Y', strtotime( $objCurrentLeaseInterval->getLeaseEndDate() ) ) ) ) {
									if( ( $arrstrFutureDate[0] == date( 'm', strtotime( $objFutureLeaseInterval->getLeaseStartDate() ) ) ) && ( $arrstrFutureDate[2] == date( 'Y', strtotime( $objFutureLeaseInterval->getLeaseStartDate() ) ) ) ) {
										$objUtilityAccount 	= getArrayElementByKey( $objCurrentLeaseInterval->getLeaseId(), $arrobjReKeyUtilityAccounts );
									}
								}
							}
						}

						$objScheduledCharge = getArrayElementByKey( $objArTransaction->getScheduledChargeId(), $arrobjScheduledCharges );

						if( false == valObj( $objScheduledCharge, 'CScheduledCharge' ) && CArTrigger::TAX != $objArTransaction->getArTriggerId() ) {
							continue;
						}

						if( false == valObj( $objUtilityAccount, 'CUtilityAccount' ) ) {
							continue;
						}

						$objUtilityTransaction = $objUtilityAccount->createUtilityTransaction( $objArCode );

						$objUtilityTransaction->setOriginalAmount( $objArTransaction->getTransactionAmount() );
						$objUtilityTransaction->setCurrentAmount( $objArTransaction->getTransactionAmount() );
						$objUtilityTransaction->setUnadjustedAmount( $objArTransaction->getTransactionAmount() );
						$objUtilityTransaction->setUtilityAccountId( $objUtilityAccount->getId() );
						$objUtilityTransaction->setScheduledChargeId( $objArTransaction->getScheduledChargeId() );
						$objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::PRE_POSTED_CONVERGENT );

						if( true == valObj( $objScheduledCharge, 'CScheduledCharge' ) ) {
							$objUtilityTransaction->setRemotePrimaryKey( 'RC~..~' . $objScheduledCharge->getRemotePrimaryKey() );
						}

						$objUtilityTransaction->setTransactionDatetime( $objArTransaction->getPostDate() );
						$objUtilityTransaction->setMemo( $objArTransaction->getMemo() );

						if( CArTrigger::TAX == $objArTransaction->getArTriggerId() ) {
							$objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::TAX );
						}

						if( false == $objUtilityTransaction->insert( $objUser->getId(), $objUtilitiesDatabase ) ) {
							trigger_error( 'Lease charge id ' . $objArTransaction->getScheduledChargeId() . ' :: transaction could not be inserted. .', E_USER_WARNING );
						}
					}
				}
			}
		}

		if( true == $boolIsProcessed ) {
			$this->setLastSyncedOn( date( 'Y-m-d H:m:s' ) );
			$this->update( $objUser->getId(), $objUtilitiesDatabase );
		}

		$this->syncUtilityPropertyUnits( $objUser, $objUtilitiesDatabase, $objClientDatabase );

		// Updaying the property unit as per the configured on utility_property_units
		if( self::MERGE_UNIT_CID == $this->getCid() ) {
			$this->mergeUtilityPropertyUnit( $objUser, $objUtilitiesDatabase );
		}

		return $boolIsProcessed;
	}

	public function importUtilityPropertyAccountsWithTransactions( $intCompanyUserId, $objProperty, $objUtilitiesDatabase, $objClientDatabase, $boolUpdateLeaseCharges ) {
		set_time_limit( 600 );

		$arrintNotConsideredInRenevalLeaseStatusTypeIds = [
			CLeaseStatusType::PAST,
			CLeaseStatusType::CANCELLED,
			CLeaseStatusType::APPLICANT
		];

		$arrintLeaseStatusTypeIds = [
			CLeaseStatusType::CURRENT,
			CLeaseStatusType::NOTICE,
			CLeaseStatusType::FUTURE,
			CLeaseStatusType::PAST
		];

		$arrintCustomerTypeIds = [
			CCustomerType::RESPONSIBLE,
			CCustomerType::NOT_RESPONSIBLE,
			CCustomerType::PRIMARY,
			CCustomerType::NON_LEASING_OCCUPANT
		];

		$arrintPhoneNumberTypeIds = [
			CPhoneNumberType::PRIMARY,
			CPhoneNumberType::OFFICE,
			CPhoneNumberType::MOBILE,
			CPhoneNumberType::FAX
		];

		// @TODO Test it thoroughly in second phase refactoring
		$arrmixLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByPropertyIdsByLeaseStatusTypeIdsByCustomerTypeIdsByCid( [ $objProperty->getId() ], $arrintLeaseStatusTypeIds, $arrintCustomerTypeIds, $this->getCid(), $objClientDatabase );

		if( false == valArr( $arrmixLeaseCustomers ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_resident', 'No resident found.' ) );
			return true;
		}

		$arrintCustomerIds = $arrintLeaseIds = $arrobjRekeyUtilityAccountPhoneNumbers = $arrobjUtilityTransactions = $arrintProcessedUtilityAccountIds = [];

		$arrmixData = $arrstrCustomerInfo = $arrobjLeaseCustomers = $arrobjKeyedLeaseIntervals = $arrintSubsidyArCodeIds = $arrintActiveLeaseUnitSpaceIds = [];

		foreach( $arrmixLeaseCustomers as $arrmixLeaseCustomer ) {

			$intLeaseId = $arrmixLeaseCustomer['lease_id'];

			if( true == getArrayElementByKey( $arrmixLeaseCustomer['customer_id'], $arrmixData ) && true == getArrayElementByKey( $intLeaseId, $arrmixData[$arrmixLeaseCustomer['customer_id']] ) ) {
				continue;
			}

			$arrmixData[$arrmixLeaseCustomer['customer_id']][$intLeaseId]['primary_customer_count'] = 1;
			$arrmixData[$arrmixLeaseCustomer['customer_id']][$intLeaseId]['lease_id']               = $intLeaseId;
			$arrmixData[$arrmixLeaseCustomer['customer_id']][$intLeaseId]['primary_customer_id']    = $arrmixLeaseCustomer['customer_id'];
			$arrmixData[$arrmixLeaseCustomer['customer_id']][$intLeaseId]['occupant_count']         = $arrmixLeaseCustomer['occupant_count'];
			$arrmixData[$arrmixLeaseCustomer['customer_id']][$intLeaseId]['lease_status_type_id']   = $arrmixLeaseCustomer['lease_status_type_id'];

			$arrintLeaseIds[$intLeaseId] 							= $intLeaseId;
			$arrintCustomerIds[$arrmixLeaseCustomer['customer_id']]	= $arrmixLeaseCustomer['customer_id'];
		}

		$objArTransactionFilter = $this->loadArTransactionsFilter();

		$arrintActiveLeaseStatusTypeIds = [
			CLeaseStatusType::CURRENT,
			CLeaseStatusType::PAST,
			CLeaseStatusType::NOTICE,
			CLeaseStatusType::FUTURE
		];

		$arrobjLeases		= ( array ) \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesWithBalanceByIdsByCid( $arrintLeaseIds, $objProperty->getCid(), $objClientDatabase, $objArTransactionFilter );
		$arrobjCustomers	= ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomersByIdsByCid( $arrintCustomerIds, $objProperty->getCid(), $objClientDatabase );

		$strSql = 'SELECT
						*
					FROM
						lease_intervals
					WHERE
						cid = ' . ( int ) $objProperty->getCId() . '
						AND property_id = ' . ( int ) $objProperty->getId() . '
						AND lease_id IN( ' . implode( ',', array_keys( $arrobjLeases ) ) . ' )
						AND lease_status_type_id = ' . CLeaseStatusType::PAST . '
						AND lease_interval_type_id =' . CLeaseIntervalType::RENEWAL . '
					ORDER BY
						lease_id,id';

		$arrobjLeaseIntervals = ( array ) \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervals( $strSql, $objClientDatabase );

		foreach( $arrobjLeaseIntervals as $objLeaseInterval ) {
			$arrobjKeyedLeaseIntervals[$objLeaseInterval->getLeaseId()][$objLeaseInterval->getId()] = $objLeaseInterval;
		}

		$strSql = 'SELECT
						*
					FROM
						lease_customers
					WHERE
						cid = ' . ( int ) $objProperty->getCid() . '
						AND property_id = ' . ( int ) $objProperty->getId() . '
						AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND lease_status_type_id IN ( ' . implode( ',', $arrintActiveLeaseStatusTypeIds ) . ' )
						AND customer_type_id = ' . CCustomerType::PRIMARY;

		$arrobjAllLeaseCustomers = ( array ) \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomers( $strSql, $objClientDatabase );

		foreach( $arrobjAllLeaseCustomers as $objLeaseCustomer ) {

			if( CLeaseStatusType::PAST == $objLeaseCustomer->getLeaseStatusTypeId() ) {
				continue;
			}

			$arrobjLeaseCustomers[$objLeaseCustomer->getLeaseId()] = $objLeaseCustomer;
		}

		$objArTransactionFilter->setShowOutstandingOnly( true );
		$objArTransactionFilter->setHideTemporary( true );

		// Get all billed ledger filter set on property utility pre bill setting.
		$arrintDefaultLedgerFilterIds = $this->getBilledLedgerIds();

		// Default ledger.
		$arrintDefaultLedgerFilterIds[] = CDefaultLedgerFilter::RESIDENT;

		$arrobjLedgerFilters = \Psi\Eos\Entrata\CLedgerFilters::createService()->fetchLedgerFiltersByDefaultLedgerFilterIdsByCid( array_filter( $arrintDefaultLedgerFilterIds ), $objProperty->getCid(), $objClientDatabase );

		$arrintResidentArCodeIds = [];

		if( true == valArr( $arrobjLedgerFilters ) ) {
			foreach( $arrobjLedgerFilters as $objLedgerFilter ) {

				$objArCodesFilter = new CArCodesFilter();
				$objArCodesFilter->setLedgerFilterId( $objLedgerFilter->getId() );

				$arrobjResidentArCodes		= ( array ) $objProperty->fetchArCodes( $objClientDatabase, $objArCodesFilter );
				$arrintResidentArCodeIds	= array_merge( $arrintResidentArCodeIds, array_keys( $arrobjResidentArCodes ) );
			}
		}

		$arrobjArTransactions		= ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByLeaseIdsByCid( $arrintLeaseIds, $objArTransactionFilter, $objProperty->getCid(), $objClientDatabase );
		$arrobjScheduledCharges		= ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchFutureScheduledChargesByLeaseIdsByCid( $arrintLeaseIds, $this->getCid(), $objClientDatabase );
		$arrobjArCodes				= ( array ) \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodes( 'SELECT * FROM view_ar_codes WHERE cid = ' . $this->getCid() . ' AND property_id = ' . $objProperty->getId(), $objClientDatabase );
		$arrobjUtilityAccounts		= ( array ) $this->fetchUtilityAccounts( $objUtilitiesDatabase );

		$arrintActiveUnitSpaceDetails	= ( array ) CLeaseUnitSpaces::fetchActiveUnitSpaceIdsByLeaseIdsByPropertyIdByCid( $arrintLeaseIds, $objProperty->getId(), $this->getCid(), $objClientDatabase );

		foreach( $arrintActiveUnitSpaceDetails as $arrintActiveUnitSpaceDetail ) {
			$arrintActiveLeaseUnitSpaceIds[$arrintActiveUnitSpaceDetail['lease_id']][$arrintActiveUnitSpaceDetail['unit_space_id']] = $arrintActiveUnitSpaceDetail['unit_space_id'];
		}

		$arrobjRekeyedUtilityTransactions = $arrobjUtilityAccountsReKeyedByCustomerByLease = $arrobjReKeyUtilityAccounts = [];
		$arrintUtilityAccountIds          = array_keys( $arrobjUtilityAccounts );

		$arrobjUtilityAccountPhoneNumbers 	= ( array ) \Psi\Eos\Utilities\CUtilityAccountPhoneNumbers::createService()->fetchUtilityAccountPhoneNumbersByUtilityAccountIdsByPhoneNumberTypeIds( $arrintUtilityAccountIds, $arrintPhoneNumberTypeIds, $objUtilitiesDatabase );
		$arrobjUtilityTransactions 			= ( array ) \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchActiveUtilityTransactionsByUtilityAccountIdsByPropertyIdByCid( $arrintUtilityAccountIds, $this->getPropertyId(), $this->getCid(), $objUtilitiesDatabase );

		foreach( $arrobjUtilityTransactions as $objUtilityTransactions ) {

			$arrobjRekeyedUtilityTransactions[$objUtilityTransactions->getUtilityAccountId()][$objUtilityTransactions->getId()] = $objUtilityTransactions;

			if( CUtilityTransactionType::PRE_POSTED_CONVERGENT != $objUtilityTransactions->getUtilityTransactionTypeId() ) {
				unset( $arrobjUtilityTransactions[$objUtilityTransactions->getId()] );
			}

		}

		foreach( $arrobjUtilityAccounts as $objUtilityAccount ) {
			$arrobjUtilityAccountsReKeyedByCustomerByLease[$objUtilityAccount->getCustomerId() . self::REMOTE_FOREIGN_KEY_DELIMITER . $objUtilityAccount->getLeaseId()] = $objUtilityAccount;
			$arrobjReKeyUtilityAccounts[$objUtilityAccount->getLeaseId()][$objUtilityAccount->getCustomerId()] = $objUtilityAccount;
		}

		foreach( $arrobjUtilityAccountPhoneNumbers as $objUtilityAccountPhoneNumber ) {
			$arrobjRekeyUtilityAccountPhoneNumbers[$objUtilityAccountPhoneNumber->getUtilityAccountId()][$objUtilityAccountPhoneNumber->getPhoneNumberTypeId()] = $objUtilityAccountPhoneNumber;
		}

		if( true == valArr( $arrmixData ) ) {

			foreach( $arrmixData as $arrmixTempLeaseCustomerData ) {

				foreach( $arrmixTempLeaseCustomerData as $intLeaseIdKey => $arrmixLeaseCustomerData ) {

					if( 0 == ( int ) $arrmixLeaseCustomerData['primary_customer_count'] || 1 < ( int ) $arrmixLeaseCustomerData['primary_customer_count'] ) {
						continue;
					}

					$objLease		= getArrayElementByKey( $intLeaseIdKey, $arrobjLeases );
					$objCustomer	= getArrayElementByKey( $arrmixLeaseCustomerData['primary_customer_id'], $arrobjCustomers );

					if( false == valObj( $objLease, 'CLease' ) || true == is_null( $objLease->getUnitSpaceId() ) || false == valObj( $objCustomer, 'CCustomer' ) ) {
						continue;
					}

					$arrstrCustomerInfo[$objCustomer->getId()][$objLease->getUnitSpaceId()][$objLease->getId()] = $objLease;

				}
			}

			foreach( $arrmixData as $arrmixTempLeaseCustomerData ) {
				foreach( $arrmixTempLeaseCustomerData as $intLeaseId => $arrmixLeaseCustomerData ) {

					if( 0 == ( int ) $arrmixLeaseCustomerData['primary_customer_count'] || 1 < ( int ) $arrmixLeaseCustomerData['primary_customer_count'] ) {
						continue;
					}

					$intUtilityAccountStatusTypeId = NULL;
					$intLeaseId					   = $arrmixLeaseCustomerData['lease_id'];

					$objLease 			= getArrayElementByKey( $arrmixLeaseCustomerData['lease_id'], $arrobjLeases );
					$objCustomer 		= getArrayElementByKey( $arrmixLeaseCustomerData['primary_customer_id'], $arrobjCustomers );

					if( false == valObj( $objLease, 'CLease' ) || true == is_null( $objLease->getUnitSpaceId() ) || false == valObj( $objCustomer, 'CCustomer' ) || true == is_null( $objLease->getPropertyUnitId() ) ) {
						continue;
					}

					$arrobjLeaseIntervals = getArrayElementByKey( $objLease->getId(), $arrobjKeyedLeaseIntervals );

					$intOccupantCount	= getArrayElementByKey( 'occupant_count', $arrmixLeaseCustomerData );

					$strSearchKey 		= $objCustomer->getId() . self::REMOTE_FOREIGN_KEY_DELIMITER . $objLease->getId();
					$objUtilityAccount	= getArrayElementByKey( $strSearchKey, $arrobjUtilityAccountsReKeyedByCustomerByLease );

					if( false == valObj( $objUtilityAccount, 'CUtilityAccount' ) && CLeaseStatusType::PAST != $arrmixLeaseCustomerData['lease_status_type_id'] ) {
						$objUtilityAccount = $objCustomer->createUtilityAccount( $objLease );
					}

					if( false == valObj( $objUtilityAccount, 'CUtilityAccount' ) || true == is_null( $objUtilityAccount->getPropertyUnitId() ) || ( CLeaseStatusType::APPLICANT == $arrmixLeaseCustomerData['lease_status_type_id'] && true == is_null( $objUtilityAccount->getId() ) ) ) {
						continue;
					}

					$objLease = getArrayElementByKey( $intLeaseId, $arrobjLeases );

					$intOriginalLeaseId = $objUtilityAccount->getLeaseId();

					$objUtilityAccount->setLease( $objLease );
					$objUtilityAccount->setLeaseId( $objLease->getId() );

					$intUtilityAccountStatusTypeId = CUtilityAccountStatusType::getUtilityAccountStatusTypeIdByLeaseStatusTypeIdByBalanceDue( $objUtilityAccount, $arrmixLeaseCustomerData['lease_status_type_id'], ( float ) $objLease->getBalance(), $boolIsEntrata = true );

					$boolIsRenewal = false;

					$intLeaseCount = 1;

					if( true == is_null( $objUtilityAccount->getId() ) && ( true == array_key_exists( $objCustomer->getId(), $arrstrCustomerInfo ) && true == array_key_exists( $objLease->getUnitSpaceId(), $arrstrCustomerInfo[$objCustomer->getId()] ) ) ) {

						$arrintLeaseIds = array_keys( $arrstrCustomerInfo[$objCustomer->getId()][$objLease->getUnitSpaceId()] );

						if( 2 <= \Psi\Libraries\UtilFunctions\count( $arrintLeaseIds ) && true == is_null( $objUtilityAccount->getId() ) ) {

							$intLeaseCount = \Psi\Libraries\UtilFunctions\count( $arrintLeaseIds );

							$objNewLease 	= getArrayElementByKey( $objUtilityAccount->getLeaseId(), $arrobjLeases );
							$intOldLeaseId 	= ( $objUtilityAccount->getLeaseId() == $arrintLeaseIds[0] ? $arrintLeaseIds[1] : $arrintLeaseIds[0] );

							$objOldLease 	= getArrayElementByKey( $intOldLeaseId, $arrobjLeases );

							if( false == valObj( $objNewLease, 'CLease' ) || false == valObj( $objOldLease, 'CLease' ) ) {
								continue;
							}

							$objNewLeaseCustomer = getArrayElementByKey( $objNewLease->getId(), $arrobjLeaseCustomers );
							$objOldLeaseCustomer = getArrayElementByKey( $objOldLease->getId(), $arrobjLeaseCustomers );

							if( true == valObj( $objOldLeaseCustomer, 'CLeaseCustomer' ) && true == valObj( $objNewLeaseCustomer, 'CLeaseCustomer' ) && false == in_array( $objOldLeaseCustomer->getLeaseStatusTypeId(), $arrintNotConsideredInRenevalLeaseStatusTypeIds ) && false == in_array( $objNewLeaseCustomer->getLeaseStatusTypeId(), $arrintNotConsideredInRenevalLeaseStatusTypeIds ) ) {

								if( strtotime( $objOldLease->getLeaseStartDate() ) > strtotime( $objNewLease->getLeaseStartDate() ) ) {
									continue;
								} else {

									$strSearchKey 		= $objCustomer->getId() . self::REMOTE_FOREIGN_KEY_DELIMITER . $objOldLease->getId();
									$objUtilityAccount 	= getArrayElementByKey( $strSearchKey, $arrobjUtilityAccountsReKeyedByCustomerByLease );

									if( false == valObj( $objUtilityAccount, 'CUtilityAccount' ) ) {
										continue;
									}

									$intOriginalLeaseId = $objUtilityAccount->getLeaseId();

									$objUtilityAccount->setLease( $objOldLease );
									$objUtilityAccount->setLeaseId( $objOldLease->getId() );

									$intUtilityAccountStatusTypeId = $objUtilityAccount->getUtilityAccountStatusTypeId();
									$boolIsRenewal = true;
								}
							} elseif( false == valObj( $objOldLeaseCustomer, 'CLeaseCustomer' ) && true == valObj( $objNewLeaseCustomer, 'CLeaseCustomer' ) ) {

								$intUtilityAccountStatusTypeId = CUtilityAccountStatusType::getUtilityAccountStatusTypeIdByLeaseStatusTypeIdByBalanceDue( $objUtilityAccount, $arrmixLeaseCustomerData['lease_status_type_id'], ( float ) $objLease->getBalance(), $boolIsEntrata = true );
								$strSearchKey 		= $objCustomer->getId() . self::REMOTE_FOREIGN_KEY_DELIMITER . $objOldLease->getId();
								$objUtilityAccount 	= getArrayElementByKey( $strSearchKey, $arrobjUtilityAccountsReKeyedByCustomerByLease );

								// Checking if old lease start date is less then create current lease account

								if( true == is_null( $objOldLease->getLeaseStartDate() ) || strtotime( $objOldLease->getLeaseStartDate() ) <= strtotime( $objNewLease->getLeaseStartDate() ) ) {

									if( false == valObj( $objUtilityAccount, 'CUtilityAccount' ) ) {
										$objUtilityAccount = $objCustomer->createUtilityAccount( $objNewLease );
									}

									$intOriginalLeaseId = $objUtilityAccount->getLeaseId();

									$objUtilityAccount->setLease( $objNewLease );
									$objUtilityAccount->setLeaseId( $objNewLease->getId() );
									$objUtilityAccount->setUtilityAccountStatusTypeId( $intUtilityAccountStatusTypeId );
									$objUtilityAccount->setIsMovedOut( false );
									unset( $arrobjLeases[$objOldLease->getId()] );
									$boolIsRenewal = true;

								} else {
									// Checking if old lease start date is less then create current lease account

									if( false == valObj( $objUtilityAccount, 'CUtilityAccount' ) ) {
										continue;
									}

									if( true == $objUtilityAccount->getIsMovedOut() ) {
										continue;
									}

									$objUtilityAccount->setLease( $objNewLease );
									$objUtilityAccount->setLeaseId( $objNewLease->getId() );
									$objUtilityAccount->setUtilityAccountStatusTypeId( $intUtilityAccountStatusTypeId );

									// removing past lease as new current lease is set to old account
									unset( $arrobjLeases[$objOldLease->getId()] );
									$boolIsRenewal = true;
								}

							} else {
								continue;
							}

						} elseif( false == in_array( $intUtilityAccountStatusTypeId, [ CUtilityAccountStatusType::CURRENT, CUtilityAccountStatusType::PENDING_MOVE_OUT, CUtilityAccountStatusType::EVICTING ] ) ) {
							continue;
						}
					}

					if( 1 < $intLeaseCount && false == $boolIsRenewal && false == is_null( $objUtilityAccount->getId() ) && CUtilityAccountStatusType::PENDING_MOVE_IN == $intUtilityAccountStatusTypeId ) {
						$intUtilityAccountStatusTypeId = CUtilityAccountStatusType::PAST;
					}

					$arrobjUtilityAccountPhoneNumbers = getArrayElementByKey( $objUtilityAccount->getId(), $arrobjRekeyUtilityAccountPhoneNumbers );

					$arrobjTempArTransactions = getArrayElementByKey( $objLease->getId(), $arrobjArTransactions );

					if( true == valArr( $arrobjTempArTransactions ) ) {
						foreach( $arrobjTempArTransactions as $intKey => $objArTransaction ) {
							if( false == in_array( $objArTransaction->getArCodeId(), $arrintResidentArCodeIds ) ) {
								unset( $arrobjTempArTransactions[$intKey] );
							}
						}
					}

					$objLease->setArTransactions( $arrobjTempArTransactions );

					$objUtilityAccount->setUtilityAccountPhoneNumbers( $arrobjUtilityAccountPhoneNumbers );

					$objUtilityAccount->setProperty( $objProperty );

					$objUtilityAccount->setCustomer( $objCustomer );

					$objUtilityAccount->setLeaseIntervals( $arrobjLeaseIntervals );

					$boolRequiresUpdate = false;

					if( true == $objUtilityAccount->getIsMovedOut() && $objUtilityAccount->getUtilityAccountStatusTypeId() == CUtilityAccountStatusType::CURRENT ) {
						$objUtilityAccount->setIsMovedOut( false );
						$boolRequiresUpdate = true;
					}

					if( $intOriginalLeaseId <> $objUtilityAccount->getLeaseId() ) {
						$objUtilityAccount->setIsLeaseIdChange( true );
					}

					if( true == $objUtilityAccount->getIsMovedOut() ) {
						$intUtilityAccountStatusTypeId = $objUtilityAccount->getUtilityAccountStatusTypeId();
					}

					$objUtilityAccount->setUtilityTransactions( getArrayElementByKey( $objUtilityAccount->getId(), $arrobjRekeyedUtilityTransactions ) );

                    $objTempLease = $objUtilityAccount->getLease();

                    if( true == valObj( $objTempLease, 'CLease' ) ) {
                        $arrintLeaseSpaceIds = ( array ) getArrayElementByKey( $objTempLease->getId(), $arrintActiveLeaseUnitSpaceIds );

                        $objTempLease->setLeaseUnitSpaceIds( $arrintLeaseSpaceIds );
                        $objUtilityAccount->setLease( $objTempLease );
                    }

                    $objUtilityAccount->insertOrUpdate( $intCompanyUserId, $objUtilitiesDatabase, $intUtilityAccountStatusTypeId, $intOccupantCount, $boolRequiresUpdate );

                    if( true == $boolUpdateLeaseCharges ) {
                        $objUtilityAccount->updateUtilityTransactions( $intCompanyUserId, $objUtilitiesDatabase, $objClientDatabase, $arrobjArCodes );
                    }

					if( false == is_null( $objUtilityAccount->getId() ) ) {
						$arrintProcessedUtilityAccountIds[$objUtilityAccount->getId()] = $objUtilityAccount->getId();
					}

					$arrobjReKeyUtilityAccounts[$objUtilityAccount->getLeaseId()][$objUtilityAccount->getCustomerId()] = $objUtilityAccount;
				}
			}
		}

		// To add the pre-convergent charges
		if( true == valArr( $arrobjScheduledCharges ) && true == $boolUpdateLeaseCharges ) {

			$objClient = $objProperty->fetchClient( $objClientDatabase );

			// Removing the scheduled utility transactions first to avoid duplication in case of integration failure.
			$strSql = 'DELETE
						FROM
							utility_transactions
						WHERE
							scheduled_charge_id IN( ' . implode( ',', array_keys( $arrobjScheduledCharges ) ) . ' )
							AND utility_transaction_type_id = ' . CUtilityTransactionType::PRE_POSTED_CONVERGENT . '
							AND utility_batch_id IS NULL
							AND original_utility_transaction_id IS NULL
							AND cid = ' . ( int ) $objClient->getId() . '
							AND property_id = ' . ( int ) $objProperty->getId();

			fetchData( $strSql, $objUtilitiesDatabase );

			$strNextPostedOn = ( 1 == date( 'd' ) ) ? date( 'm/01/Y' ) : date( 'm/t/Y', strtotime( date( 'm/d/Y', strtotime( '+1 month', strtotime( date( 'm/01/y' ) ) ) ) ) );

			$objClientDatabase->begin();

			$objPostScheduledChargesLibrary 	= CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::UTILITY_TRANSACTIONS, $objClientDatabase );
			$objPostScheduledChargesCriteria	= $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $objClient->getId(), [ $objProperty->getId() ], CUser::ID_CLIENT_ADMIN );

			$objPostScheduledChargesCriteria->setDataReturnTypeId( CPostScheduledChargesCriteria::DATA_RETURN_TYPE_PROPERTIES )
											->setIsDryRun( true )
											->setPostWindowEndDateOverride( $strNextPostedOn )
											->setLeaseIds( array_keys( rekeyObjects( 'LeaseId', $arrobjScheduledCharges ) ) )
											->setArTriggerIds( CPostScheduledChargesCriteria::$c_arrintRecurringAndOneTimeArTriggerIds );

			$objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria );
			$arrobjArTransactions = $objPostScheduledChargesLibrary->getArTransactions();

			$objClientDatabase->rollback();

			// remove all ar transactions whose having post month less than or equal to current month & ar transactions from Resident Ledger
			if( true == valArr( $arrobjArTransactions ) ) {
				foreach( $arrobjArTransactions as $intKey => $objArTransaction ) {
					if( ( date( 'm', strtotime( $objArTransaction->getPostMonth() ) ) <= date( 'm' ) && date( 'Y', strtotime( $objArTransaction->getPostMonth() ) ) <= date( 'Y' ) )
					    || false == in_array( $objArTransaction->getArCodeId(), $arrintResidentArCodeIds ) ) {
						unset( $arrobjArTransactions[$intKey] );
					}
				}
			}

			// Delete all transactions

			if( true == valArr( $arrobjUtilityTransactions ) ) {
				foreach( $arrobjUtilityTransactions as $objUtilityTransaction ) {

					if( CUtilityTransactionType::PRE_POSTED_CONVERGENT != $objUtilityTransaction->getUtilityTransactionTypeId() ) {
						continue;
					}

					if( false == $objUtilityTransaction->delete( $intCompanyUserId, $objUtilitiesDatabase ) ) {
						trigger_error( 'Lease charge id ' . $objUtilityTransaction->getId() . ' :: transaction could not be deleted. .', E_USER_WARNING );
					}
				}
			}

			// Delete duplicate convergent utility transactions block

			if( true == valArr( $arrintUtilityAccountIds ) ) {

				$strSql = 'SELECT
								*
							FROM
								utility_transactions
							WHERE
								utility_account_id IN ( ' . implode( ',', $arrintUtilityAccountIds ) . ' )
								AND utility_transaction_type_id = ' . CUtilityTransactionType::CONVERGENT . '
								AND cid = ' . ( int ) $objClient->getId() . '
								AND property_id = ' . ( int ) $objProperty->getId() . '
								AND original_utility_transaction_id IS NULL
								AND ar_transaction_id IS NOT NULL
								AND deleted_by IS NULL
								AND deleted_on IS NULL';

				$arrobjConvergentUtilityTransactions = \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUtilityTransactions( $strSql, $objUtilitiesDatabase );

				$arrintScheduledChargeIds = array_keys( $arrobjScheduledCharges );

				if( true == valArr( $arrobjConvergentUtilityTransactions ) ) {

					$strSql = 'SELECT
									*
								FROM
									ar_transactions
								WHERE
									id IN (' . implode( ',', array_filter( array_keys( rekeyObjects( 'ArTransactionId', $arrobjConvergentUtilityTransactions ) ) ) ) . ' )
									AND scheduled_charge_id IN (' . implode( ',', $arrintScheduledChargeIds ) . ' )
									AND cid = ' . ( int ) $objClient->getId();

					$arrobjTempArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactions( $strSql, $objClientDatabase );

					foreach( $arrobjConvergentUtilityTransactions as $objConvergentUtilityTransaction ) {

						if( true == is_null( $objConvergentUtilityTransaction ->getArTransactionId() ) ) {
							continue;
						}

						$objArTransaction = getArrayElementByKey( $objConvergentUtilityTransaction->getArTransactionId(), $arrobjTempArTransactions );

						if( false == valObj( $objArTransaction, 'CArTransaction' ) ) {
							continue;
						}

						$objScheduledCharge = getArrayElementByKey( $objArTransaction->getScheduledChargeId(), $arrobjScheduledCharges );

						if( true == valObj( $objScheduledCharge, 'CScheduledCharge' )
						 && ( ( date( 'm', strtotime( $objScheduledCharge->getLastPostedOn() ) ) <= date( 'm' ) && date( 'Y', strtotime( $objScheduledCharge->getLastPostedOn() ) ) <= date( 'Y' ) )
						  || ( date( 'm', strtotime( $objScheduledCharge->getLastPostedOn() ) ) != date( 'm', strtotime( $objArTransaction->getPostDate() ) ) && date( 'Y', strtotime( $objScheduledCharge->getLastPostedOn() ) ) >= date( 'Y', strtotime( $objArTransaction->getPostDate() ) ) ) ) ) {
							continue;
						}

						if( $objArTransaction->getTransactionAmountDue() == $objArTransaction->getTransactionAmount() && false == $objConvergentUtilityTransaction->delete( $intCompanyUserId, $objUtilitiesDatabase ) ) {
							trigger_error( 'Utility transaction id ' . $objConvergentUtilityTransaction->getId() . ' :: could not be deleted. .', E_USER_WARNING );
						}
					}
				}
			}
			// block ends

			if( false == valArr( $arrobjArTransactions ) ) {
				return true;
			}

			$objCloneUtilityTransaction = new CUtilityTransaction();
			$objCloneUtilityTransaction->setCid( $objClient->getId() );
			$objCloneUtilityTransaction->setPropertyId( $objProperty->getId() );

			foreach( $arrobjArTransactions as $objArTransaction ) {

				$objLeaseCustomer  = getArrayElementByKey( $objArTransaction->getLeaseId(), $arrobjLeaseCustomers );

				if( false == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) ) {
					continue;
				}

				$arrobjTempUtilityAccounts 	= getArrayElementByKey( $objArTransaction->getLeaseId(), $arrobjReKeyUtilityAccounts );
				$objUtilityAccount 			= getArrayElementByKey( $objLeaseCustomer->getCustomerId(), $arrobjTempUtilityAccounts );
				$objArCode 					= getArrayElementByKey( $objArTransaction->getArCodeId(), $arrobjArCodes );

				if( false == valObj( $objUtilityAccount, 'CUtilityAccount' ) || false == valObj( $objArCode, 'CArCode' ) ) {
					continue;
				}

				$objUtilityTransaction = clone $objCloneUtilityTransaction;
				$objUtilityTransaction->setArCodeId( $objArCode->getId() );
				$objUtilityTransaction->setChargeName( $objArCode->getName() );
				$objUtilityTransaction->setChargeDescription( $objArCode->getDescription() );
				$objUtilityTransaction->setOriginalAmount( $objArTransaction->getTransactionAmount() );
				$objUtilityTransaction->setCurrentAmount( $objArTransaction->getTransactionAmount() );
				$objUtilityTransaction->setUnadjustedAmount( $objArTransaction->getTransactionAmount() );
				$objUtilityTransaction->setUtilityAccountId( $objUtilityAccount->getId() );
				$objUtilityTransaction->setScheduledChargeId( $objArTransaction->getScheduledChargeId() );

				$objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::PRE_POSTED_CONVERGENT );

				if( CArTrigger::TAX == $objArTransaction->getArTriggerId() ) {
					$objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::TAX );
				}

				$objUtilityTransaction->setTransactionDatetime( $objArTransaction->getPostDate() );

				if( false == $objUtilityTransaction->insert( $intCompanyUserId, $objUtilitiesDatabase ) ) {
					trigger_error( 'Lease charge id ' . $objArTransaction->getScheduledChargeId() . ' :: transaction could not be inserted. .', E_USER_WARNING );
				}
			}
		}

		// update the unprocessed utility accounts to past status
		if( true == valArr( $arrintProcessedUtilityAccountIds ) && true == valArr( $arrobjUtilityAccounts ) ) {
			$arrobjUnprocessedUtilityAccounts = array_diff_key( $arrobjUtilityAccounts, $arrintProcessedUtilityAccountIds );
			if( true == valArr( $arrobjUnprocessedUtilityAccounts ) ) {
				foreach( $arrobjUnprocessedUtilityAccounts as $objUtilityAccount ) {
					$objUtilityAccount->setUtilityAccountStatusTypeId( CUtilityAccountStatusType::PAST );
					$objUtilityAccount->update( $intCompanyUserId, $objUtilitiesDatabase );
				}
			}
		}

		return true;
	}

	public function importUtilityPropertyUnits( $intCompanyUserId, $objUtilitiesDatabase, $objClientDatabase ) {

		$arrobjUtilityPropertyUnits = [];

		$arrobjUnitAddresses            = ( array ) \Psi\Eos\Entrata\CUnitAddresses::createService()->fetchUnitPrimaryAddressesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		$arrobjTempUtilityPropertyUnits = ( array ) \Psi\Eos\Utilities\CUtilityPropertyUnits::createService()->fetchUtilityPropertyUnitsByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );
		$arrobjPropertyUnits            = ( array ) \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitsByPropertyIdOrderByUnitNumberByCid( $this->getPropertyId(), NULL, $this->getCid(), $objClientDatabase );
		$arrobjEntrataUnitSpaces        = ( array ) CUnitSpaces::createService()->fetchUnitSpacesByPropertyUnitIdsByCid( array_keys( $arrobjPropertyUnits ), $this->getCid(), $objClientDatabase );
		$arrobjPropertyBuildings        = ( array ) \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchPropertyBuildingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );

		$arrobjRekeyedUnitSpaces           = rekeyObjects( 'PropertyUnitId', $arrobjEntrataUnitSpaces, true );
		$arrobjRekeyedUtilityPropertyUnits = rekeyObjects( 'PropertyUnitId', $arrobjTempUtilityPropertyUnits, true );

		$arrintPropertyUnitIds = array_keys( $arrobjRekeyedUnitSpaces );

		$arrobjUnitSpaces                      = $arrobjNoUnitSpaceUnits = [];
		$arrobjTempUtilityPropertyUnitBySpaces = rekeyObjects( 'UnitSpaceId', $arrobjTempUtilityPropertyUnits );

		$arrintUsedUtilityUnitSpaceIds = array_keys( array_filter( $arrobjTempUtilityPropertyUnitBySpaces ) );

		foreach( $arrobjTempUtilityPropertyUnits as $objUtilityPropertyUnit ) {
			if( false == is_null( $objUtilityPropertyUnit->getUnitSpaceId() ) ) {
				continue;
			}
			$arrobjNoUnitSpaceUnits[] = $objUtilityPropertyUnit;
		}

		$arrobjNoUnitSpaceUnits = rekeyObjects( 'PropertyUnitId', $arrobjNoUnitSpaceUnits, true );

		$intNumberOfUnitSpaces = 1;

		if( true == valArr( $arrintPropertyUnitIds ) ) {

			$arrobjUnitAddresses = rekeyObjects( 'PropertyUnitId', $arrobjUnitAddresses );

			foreach( $arrintPropertyUnitIds as $intPropertyUnitId ) {

				$objUnitAddress = getArrayElementByKey( $intPropertyUnitId, $arrobjUnitAddresses );

				$arrobjNoUtilityUnitSpacesUnits = getArrayElementByKey( $intPropertyUnitId, $arrobjNoUnitSpaceUnits );
				$arrobjUnitSpaces               = getArrayElementByKey( $intPropertyUnitId, $arrobjRekeyedUnitSpaces );

				if( false == valArr( $arrobjUnitSpaces ) ) {
					continue;
				}

				$intNumberOfUnitSpaces = \Psi\Libraries\UtilFunctions\count( $arrobjUnitSpaces );

				$objPropertyUnit     = getArrayElementByKey( $intPropertyUnitId, $arrobjPropertyUnits );
				$objPropertyBuilding = getArrayElementByKey( $objPropertyUnit->getPropertyBuildingId(), $arrobjPropertyBuildings );

				$intSquareFeet = round( $objPropertyUnit->getSquareFeet() / $intNumberOfUnitSpaces, 2 );

				foreach( $arrobjUnitSpaces as $objUnitSpace ) {
					$objUtilityPropertyUnit = getArrayElementByKey( $objUnitSpace->getId(), $arrobjTempUtilityPropertyUnitBySpaces );

					if( true == valObj( $objUtilityPropertyUnit, 'CUtilityPropertyUnit' ) ) {
						$objUtilityPropertyUnit->setSquareFeet( $intSquareFeet );
						$this->updateUtilityPropertyUnits( $objUtilityPropertyUnit, $objUnitSpace, $objPropertyUnit, $objUnitAddress, $objPropertyBuilding );
					} elseif( true == valArr( $arrobjNoUtilityUnitSpacesUnits ) && false == valObj( $objUtilityPropertyUnit, 'CUtilityPropertyUnit' ) && false == in_array( $objUnitSpace->getId(), $arrintUsedUtilityUnitSpaceIds ) ) {

							$objUtilityPropertyUnit = array_shift( $arrobjNoUtilityUnitSpacesUnits );
							$objUtilityPropertyUnit->setUnitSpaceId( $objUnitSpace->getId() );
							$objUtilityPropertyUnit->setSquareFeet( $intSquareFeet );

							$this->updateUtilityPropertyUnits( $objUtilityPropertyUnit, $objUnitSpace, $objPropertyUnit, $objUnitAddress, $objPropertyBuilding );

							$arrintUsedUtilityUnitSpaceIds[] = $objUnitSpace->getId();

					} else {
							$objUtilityPropertyUnit = new CUtilityPropertyUnit();
							$objUtilityPropertyUnit->setCid( $objUnitSpace->getCid() );
							$objUtilityPropertyUnit->setPropertyId( $objUnitSpace->getPropertyId() );
							$objUtilityPropertyUnit->setUnitSpaceId( $objUnitSpace->getId() );
							$objUtilityPropertyUnit->setPropertyUnitId( $intPropertyUnitId );

							$objUtilityPropertyUnit->setSquareFeet( $intSquareFeet );

							$this->updateUtilityPropertyUnits( $objUtilityPropertyUnit, $objUnitSpace, $objPropertyUnit, $objUnitAddress, $objPropertyBuilding );

					}

					$arrobjUtilityPropertyUnits[] = $objUtilityPropertyUnit;
					$arrintUtilityPropertyUnitIds[$objUtilityPropertyUnit->getPropertyUnitId()] = $objUtilityPropertyUnit->getPropertyUnitId();
				}
			}

				$arrintUtilityPropertyUnitIds[$objUtilityPropertyUnit->getPropertyUnitId()] = $objUtilityPropertyUnit->getPropertyUnitId();
		}

		$arrintDiffPropertyUnitIds = $arrintNonEntrataUtilityPropertyUnitIds = [];

		if( true == valArr( $arrobjUtilityPropertyUnits ) ) {
			$arrintDiffPropertyUnitIds              = array_diff( array_keys( $arrobjPropertyUnits ), array_keys( $arrintUtilityPropertyUnitIds ) );
			$arrintNonEntrataUtilityPropertyUnitIds = array_diff( array_keys( $arrobjRekeyedUtilityPropertyUnits ), array_keys( $arrobjPropertyUnits ) );
		}

		if( true == valArr( $arrintDiffPropertyUnitIds ) ) {
			$arrintUsedUtilityUnitSpaceIds = [];
			foreach( $arrintDiffPropertyUnitIds as $intPropertyUnitId ) {

				$objPropertyUnit = getArrayElementByKey( $intPropertyUnitId, $arrobjPropertyUnits );

				$arrobjNoUtilityUnitSpacesUnits = getArrayElementByKey( $intPropertyUnitId, $arrobjNoUnitSpaceUnits );

				if( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) {

					$arrobjUnitSpaces = ( array ) getArrayElementByKey( $objPropertyUnit->getId(), $arrobjRekeyedUnitSpaces );

					if( false == valArr( $arrobjUnitSpaces ) ) {
						continue;
					}

					$intSquareFeet = round( $objPropertyUnit->getSquareFeet() / \Psi\Libraries\UtilFunctions\count( $arrobjUnitSpaces ), 2 );

					foreach( $arrobjUnitSpaces as $objUnitSpace ) {

						$objUtilityPropertyUnit = getArrayElementByKey( $objUnitSpace->getId(), $arrobjTempUtilityPropertyUnitBySpaces );

						if( true == valObj( $objUtilityPropertyUnit, 'CUtilityPropertyUnit' ) ) {
							$objUtilityPropertyUnit->setSquareFeet( $intSquareFeet );
						} elseif( true == valArr( $arrobjNoUtilityUnitSpacesUnits ) && false == valObj( $objUtilityPropertyUnit, 'CUtilityPropertyUnit' ) && false == in_array( $objUnitSpace->getId(), $arrintUsedUtilityUnitSpaceIds ) ) {
							$objUtilityPropertyUnit = array_shift( $arrobjNoUtilityUnitSpacesUnits );
							$objUtilityPropertyUnit->setUnitSpaceId( $objUnitSpace->getId() );
							$objUtilityPropertyUnit->setSquareFeet( $intSquareFeet );

							$arrintUsedUtilityUnitSpaceIds[] = $objUnitSpace->getId();
						} else {

							$objUtilityPropertyUnit = new CUtilityPropertyUnit();
							$objUtilityPropertyUnit->setCid( $this->getCid() );
							$objUtilityPropertyUnit->setPropertyId( $this->getPropertyId() );
							$objUtilityPropertyUnit->setPropertyUnitId( $objPropertyUnit->getId() );
							$objUtilityPropertyUnit->setUnitSpaceId( $objUnitSpace->getId() );
							$objUtilityPropertyUnit->setSquareFeet( $intSquareFeet );
						}
						$arrobjUtilityPropertyUnits[] = $objUtilityPropertyUnit;
					}

				}
			}
		}

			if( true == valIntArr( $arrintNonEntrataUtilityPropertyUnitIds ) ) {
				$strSql = 'DELETE FROM utility_property_units WHERE property_unit_id IN (' . implode( ',', array_filter( $arrintNonEntrataUtilityPropertyUnitIds ) ) . ')';
				fetchData( $strSql, $objUtilitiesDatabase );
			}

			switch( NULL ) {
				default:
					if( true == valArr( $arrobjUtilityPropertyUnits ) ) {

						$objUtilitiesDatabase->begin();

						foreach( $arrobjUtilityPropertyUnits as $objUtilityPropertyUnit ) {
							if( false == $objUtilityPropertyUnit->insertOrUpdate( $intCompanyUserId, $objUtilitiesDatabase ) ) {
								$objUtilitiesDatabase->rollback();
								break;
							}
						}

						$objUtilitiesDatabase->commit();

						return true;

					}
			}

			return false;
	}

	public function loadArTransactionsFilter() {

		$objArTransactionsFilter = new CArTransactionsFilter();
		$objArTransactionsFilter->setDefaults();

		return $objArTransactionsFilter;
	}

	public function determineDaysUntilNextBill() {
		return number_format( ( strtotime( $this->getNextBatchPostDate() ) - time() ) / 84600 );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false, $objClientDatabaase = NULL ) {

		$arrstrTempOriginalValues 	= $arrstrOriginalValues = $arrstrOriginalValueChanges = [];
		$boolChangeFlag				= false;

		$objPropertyUtilitySetting = new CPropertyUtilitySetting();
		$arrmixOriginalValues = fetchData( 'SELECT * FROM property_utility_settings WHERE id = ' . ( int ) $this->getId(), $objDatabase );

		$arrstrTempOriginalValues = $arrmixOriginalValues[0];

		foreach( $arrstrTempOriginalValues as $strKey => $mixOriginalValue ) {

			if( 'charge_for_postage' == $strKey ) {
				continue;
			}

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strGetFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strSetFunctionName = 'set' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			$objPropertyUtilitySetting->$strSetFunctionName( $mixOriginalValue );

			if( $this->$strGetFunctionName() != $objPropertyUtilitySetting->$strGetFunctionName() ) {
				$arrstrOriginalValueChanges[$strKey] 	= $this->$strSqlFunctionName();
				$arrstrOriginalValues[$strKey] 			= $objPropertyUtilitySetting->$strSqlFunctionName();
				$boolChangeFlag 						= true;
			}
		}

		$boolUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'property_utility_settings' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		if( false != $boolUpdateStatus ) {
			if( $arrstrTempOriginalValues['company_employee_id'] <> $this->getCompanyEmployeeId() ) {
				$boolUpdateStatus = $this->updatePropertyPreference( $this->getCompanyUserId(), $arrstrTempOriginalValues['company_employee_id'], $objClientDatabaase );
			}
		}

		return $boolUpdateStatus;

	}

	public function syncUtilityPropertyUnits( $objUser, $objUtilitiesDatabase, $objClientDatabase ) {
		$boolIsSuccess = true;

		$arrobjUtilityPropertyUnits = ( array ) \Psi\Eos\Utilities\CUtilityPropertyUnits::createService()->fetchUtilityPropertyUnitsByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );
		$arrobjPropertyUnits        = \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchSimplePropertyUnitsByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objClientDatabase );

		$objUtilitiesDatabase->begin();

		if( true == valArr( $arrobjPropertyUnits ) ) {
			foreach( $arrobjUtilityPropertyUnits as $objUtilityPropertyUnit ) {

				if( true == array_key_exists( $objUtilityPropertyUnit->getPropertyUnitId(), $arrobjPropertyUnits ) ) {
					continue;
				}

				if( false == $objUtilityPropertyUnit->delete( $objUser->getId(), $objUtilitiesDatabase ) ) {

					$this->addErrorMsg( 'Utility property unit could not be removed.' );
					$boolIsSuccess = false;
					$objUtilitiesDatabase->rollback();
					break;
				}
			}
		}

		if( true == $boolIsSuccess ) {
			$objUtilitiesDatabase->commit();
		}
		return $boolIsSuccess;
	}

	public function removeNewlyCreatedBatch( $objDatabase ) {

		$strSql = 'UPDATE
						vcr_utility_bills vub
					SET
					    utility_batch_id = NULL
					FROM
					    utility_batches ub
					WHERE
					    vub.utility_batch_id = ub.id
					    AND ub.property_id = ' . $this->getPropertyId() . '
					    AND ub.utility_batch_step_id = ' . CUtilityBatchStep::VALIDATE . ';';

		$strSql .= 'DELETE
					FROM
					    utility_batch_qc_escalations ubqe
					USING utility_batches ub
					WHERE
					    ubqe.utility_batch_id = ub.id
					    AND ub.property_id = ' . $this->getPropertyId() . '
					    AND ub.utility_batch_step_id = ' . CUtilityBatchStep::VALIDATE . ';';

		$strSql .= 'UPDATE
						utility_transactions ut
					SET
					    utility_batch_id = NULL
					FROM
					    utility_batches ub
					WHERE
					    ut.utility_batch_id = ub.id
					    AND ub.property_id = ' . $this->getPropertyId() . '
					    AND ub.utility_batch_step_id = ' . CUtilityBatchStep::VALIDATE . ';';

		$strSql .= 'DELETE
					FROM
					    utility_batch_activities uba
					USING utility_batches ub
					WHERE
					    uba.utility_batch_id = ub.id
					    AND ub.property_id = ' . $this->getPropertyId() . '
					    AND ub.utility_batch_step_id = ' . CUtilityBatchStep::VALIDATE . ';';

		$strSql .= 'DELETE
					    FROM
					        utility_batches
					    WHERE
					        property_id = ' . $this->getPropertyId() . '
					        AND utility_batch_step_id = ' . CUtilityBatchStep::VALIDATE;

		return fetchData( $strSql, $objDatabase );
	}

	public function updateUtilityPropertyUnits( $objUtilityPropertyUnit, $objUnitSpace, $objPropertyUnit, $objUnitAddress, $objPropertyBuilding ) {

		$objUtilityPropertyUnit->setSpaceNumber( $objUnitSpace->getSpaceNumber() );
		$objUtilityPropertyUnit->setUnitNumber( $objPropertyUnit->getUnitNumber() );

		if( true == valObj( $objUnitAddress, 'CUnitAddress' ) ) {
			$objUtilityPropertyUnit->setTimeZoneId( $objUnitAddress->getTimeZoneId() );
			$objUtilityPropertyUnit->setStreetLine1( $objUnitAddress->getStreetLine1() . ' ' . $objUnitAddress->getStreetLine2() );
			$objUtilityPropertyUnit->setStreetLine2( NULL );
			$objUtilityPropertyUnit->setStreetLine3( $objUnitAddress->getStreetLine3() );
			$objUtilityPropertyUnit->setCity( $objUnitAddress->getCity() );
			$objUtilityPropertyUnit->setStateCode( \Psi\CStringService::singleton()->strtoupper( $objUnitAddress->getStateCode() ) );
			$objUtilityPropertyUnit->setProvince( $objUnitAddress->getProvince() );
			$objUtilityPropertyUnit->setPostalCode( $objUnitAddress->getPostalCode() );
			$objUtilityPropertyUnit->setCountryCode( $objUnitAddress->getCountryCode() );
			$objUtilityPropertyUnit->setUtilityAccountAddressTypeId( $objUnitAddress->getAddressTypeId() );
		}

		if( true == valObj( $objPropertyBuilding, 'CPropertyBuilding' ) ) {
			$objUtilityPropertyUnit->setPropertyBuildingId( $objPropertyBuilding->getId() );
			$objUtilityPropertyUnit->setBuildingName( $objPropertyBuilding->getBuildingName() );
		}
	}

	public function getDateFromBillingMonthByDay( $intDay ) {
		if( 2 == $this->getBillingMonth() AND 30 <= $intDay ) {
			$objDate = new DateTime(date( 'm', strtotime( $this->getBillingMonth() ) ) . '/01/' . date( 'Y', strtotime( $this->getBillingMonth() ) ) );
			return $objDate->format('m/t/Y');
		}
		return date( 'm/d/Y', strtotime( date( 'm', strtotime( $this->getBillingMonth() ) ) . '/' . $intDay . '/' . date( 'Y', strtotime( $this->getBillingMonth() ) ) ) );
	}

	public function getCompanyEmployeeNames( $objAdminDatabase ) {

		$strDecisionMakerEmployees = $strCompanyEmployeeName = '';

		$objClient = CClients::fetchClientById( $this->getCid(), $objAdminDatabase );

		if( false == valObj( $objClient, 'CClient' ) ) {
			return $strDecisionMakerEmployees;
		}

		$objClientDatabase = $objClient->loadDatabase( CDatabaseUserType::JOBEXECUTOR );

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			return $strDecisionMakerEmployees;
		}

		$arrmixCompanyEmployees = ( array ) CCompanyEmployees::createService()->fetchCompanyEmployeesByIdsByCid( ( array ) $this->getDecisionMakerCompanyEmployeeIds(), $this->getCid(), $objClientDatabase, true, true, true );

		if( true == valArr( $arrmixCompanyEmployees ) ) {
			$intDecisionMakerCompanyEmployeeCount = 1;
			foreach( $arrmixCompanyEmployees as $arrmixCompanyEmployee ) {
				$strCompanyEmployeeName = $intDecisionMakerCompanyEmployeeCount . '. ' . $arrmixCompanyEmployee['namefull'];
				if( true == empty( $strDecisionMakerEmployees ) ) {
					$strDecisionMakerEmployees = $strCompanyEmployeeName;
				} else {
					$strDecisionMakerEmployees = $strDecisionMakerEmployees . ',' . $strCompanyEmployeeName;
				}
				$intDecisionMakerCompanyEmployeeCount++;
			}
		}

		return $strDecisionMakerEmployees;
	}

	public function updateScheduleExportDate( $strScheduledExportStartDate, $objDatabase ) {

		$boolSuccessFlag = true;
		if( true == is_null( $strScheduledExportStartDate ) ) {
			$boolSuccessFlag = false;
		}

		$arrintUtilityBillTypeIds = [ CUtilityBillType::ACTUAL, CUtilityBillType::VACANT_UNIT, CUtilityBillType::DIRECT_METERED, CUtilityBillType::SUMMARY ];

		$strSql = 'UPDATE
						utility_bills
					SET
						scheduled_export_date = now(),
						ps_product_id = ' . CPsProduct::UTILITY_EXPENSE_MANAGEMENT . '
					WHERE
						created_on::DATE >= \'' . $strScheduledExportStartDate . '\' AND
						property_id IN ( ' . $this->m_intPropertyId . ' ) AND
						is_historical = FALSE AND
						processed_on IS NOT NULL AND
						processed_again_by IS NOT NULL AND
						scheduled_export_date IS NULL AND
						exported_on IS NULL AND
						exported_by IS NULL AND
						ap_header_id IS NULL AND
						deleted_by IS NULL AND
						deleted_on IS NULL AND
						is_queue_ignored IS FALSE AND
						ap_exception_queue_item_id IS NULL AND
						utility_bill_type_id IN ( ' . implode( ',', $arrintUtilityBillTypeIds ) . ' ) AND
						( review_utility_bill_id IS NULL OR ( review_utility_bill_id IS NOT NULL AND reviewed_by IS NOT NULL AND reviewed_on IS NOT NULL ) )
						AND copy_utility_bill_id IS NULL AND
						ps_product_id = ' . CPsProduct::RESIDENT_UTILITY;

		if( true == is_bool( fetchData( $strSql, $objDatabase ) ) ) {
			$boolSuccessFlag = false;
		}
	return $boolSuccessFlag;
	}

	public function loadNewPropertyUtilitySettings( $intCid, $intPropertyId, $intPropertyUtilitySettingId ) {

		$this->m_objUtilityIntegrationSetting = new CUtilityIntegrationSetting();
		$this->m_objUtilityPreBillSetting     = new CUtilityPreBillSetting();
		$this->m_objUtilityUemSetting         = new CUtilityUemSetting();
		$this->m_objUtilityPropertySetting    = new CUtilityPropertySetting();
		$this->m_objUtilityInvoiceSetting     = new CUtilityInvoiceSetting();
		$this->m_objUtilityAutoBillingSetting = new CUtilityAutoBillingSetting();

		$this->m_objUtilityIntegrationSetting->setCid( $intCid );
		$this->m_objUtilityPreBillSetting->setCid( $intCid );
		$this->m_objUtilityUemSetting->setCid( $intCid );
		$this->m_objUtilityPropertySetting->setCid( $intCid );
		$this->m_objUtilityInvoiceSetting->setCid( $intCid );
		$this->m_objUtilityAutoBillingSetting->setCid( $intCid );

		$this->m_objUtilityIntegrationSetting->setPropertyId( $intPropertyId );
		$this->m_objUtilityPreBillSetting->setPropertyId( $intPropertyId );
		$this->m_objUtilityUemSetting->setPropertyId( $intPropertyId );
		$this->m_objUtilityPropertySetting->setPropertyId( $intPropertyId );
		$this->m_objUtilityInvoiceSetting->setPropertyId( $intPropertyId );
		$this->m_objUtilityAutoBillingSetting->setPropertyId( $intPropertyId );

		$this->m_objUtilityIntegrationSetting->setPropertyUtilitySettingId( $intPropertyUtilitySettingId );
		$this->m_objUtilityPreBillSetting->setPropertyUtilitySettingId( $intPropertyUtilitySettingId );
		$this->m_objUtilityUemSetting->setPropertyUtilitySettingId( $intPropertyUtilitySettingId );
		$this->m_objUtilityPropertySetting->setPropertyUtilitySettingId( $intPropertyUtilitySettingId );
		$this->m_objUtilityInvoiceSetting->setPropertyUtilitySettingId( $intPropertyUtilitySettingId );
		$this->m_objUtilityAutoBillingSetting->setPropertyUtilitySettingId( $intPropertyUtilitySettingId );
	}

	public function addImplementationEmail( $arrmixPropertyImplementationDetails ) {

		$strSubject = $arrmixPropertyImplementationDetails['ps_product_name'] . ' IC assigned for ' . $arrmixPropertyImplementationDetails['company_name'] . ' > ' . $arrmixPropertyImplementationDetails['property_name'] . ': ' . $arrmixPropertyImplementationDetails['implementation_consultant_name'];

		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setCid( $arrmixPropertyImplementationDetails['cid'] );
		$objSystemEmail->setPropertyId( $arrmixPropertyImplementationDetails['property_id'] );
		$objSystemEmail->setSubject( $strSubject );
		$objSystemEmail->setToEmailAddress( $arrmixPropertyImplementationDetails['sending_to_email'] );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::UTILITY_BILL_EMAIL_NOTIFICATION );

		$strAssignedText = 'assigned';

		if( true == $arrmixPropertyImplementationDetails['is_reassigned'] ) {
			$strAssignedText = 're-assigned';
		}

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );
		$objSmarty->assign( 'sending_to_name', $arrmixPropertyImplementationDetails['sending_to_name'] );
		$objSmarty->assign( 'implementation_consultant_name', $arrmixPropertyImplementationDetails['implementation_consultant_name'] );
		$objSmarty->assign( 'ps_product_name', $arrmixPropertyImplementationDetails['ps_product_name'] );
		$objSmarty->assign( 'company_name', $arrmixPropertyImplementationDetails['company_name'] );
		$objSmarty->assign( 'property_name', $arrmixPropertyImplementationDetails['property_name'] );
		$objSmarty->assign( 'assigned_text', $strAssignedText );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX', CONFIG_ENTRATA_LOGO_PREFIX );
		$objSmarty->assign( 'image_url', CONFIG_COMMON_PATH );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_RESIDENT_WORKS . 'private/utilities/utility_implementation_template.tpl', PATH_INTERFACES_ADMIN . 'common/layouts/utility_email.tpl' );

		$objSystemEmail->setHtmlContent( $strHtmlContent );

		$this->m_arrobjSystemEmails[] = $objSystemEmail;

	}

	public function sendPreparedEmails( $intUserId, $objEmailDatabase ) {

		if( false == valArr( $this->m_arrobjSystemEmails ) ) {
			return true;
		}

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			$this->addErrorMsg( new CErrorMsg( 'Failed to load email database.' ) );
			return false;
		}

		foreach( $this->m_arrobjSystemEmails AS $objSystemEmail ) {

			if( false == $objSystemEmail->validate( VALIDATE_INSERT ) || false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( '', 'Failed to send the email notification ' . $objSystemEmail->getSubject() ) );
			}

		}

	}

	public function getValidImplementationEmployeeId( $intEmployeeId, $intEmployeePreviousManagerId, $objAdminDatabase ) {

		$objEmployee = CEmployees::fetchEmployeeById( $intEmployeePreviousManagerId, $objAdminDatabase );

		if( true == valObj( $objEmployee, 'CEmployee' ) && false == $objEmployee->getIsTerminated() && true == in_array( $objEmployee->getDesignationId(), CDesignation::$c_arrintImplementationConsultantDesignationIds ) ) {
			return $intEmployeePreviousManagerId;
		}

		$objEmployee = current( ( array ) CEmployees::fetchActiveEmployeesByDesignationIds( [ CDesignation::DIRECTOR_ACCOUNTING_CONSULTING ], $objAdminDatabase ) );

		if( false == valObj( $objEmployee, 'CEmployee' ) ) {
			return $intEmployeeId;
		}

		return $objEmployee->getId();

	}

	public function updateImplementationConsultantEmployeeId( $intEmployeeId, $intEmployeePreviousManagerId, $objAdminDatabase, $objUtilitiesDatabase ) {

		$intEmployeePreviousManagerId = $this->getValidImplementationEmployeeId( $intEmployeeId, $intEmployeePreviousManagerId, $objAdminDatabase );

		$this->updatePropertyUtilitySettings( $intEmployeeId, $intEmployeePreviousManagerId, $objUtilitiesDatabase );

	}

	public function updatePropertyUtilitySettings( $intOldImplementationEmployeeId, $intNewImplementationEmployeeId, $objUtilitiesDatabase ) {

		if( false == valId( $intOldImplementationEmployeeId ) || false == valId( $intNewImplementationEmployeeId ) ) {
			return false;
		}

		$strSql = 'UPDATE property_utility_settings
					SET billing_implementation_employee_id = CASE
																WHEN is_in_implementation IS TRUE AND billing_implementation_employee_id = ' . ( int ) $intOldImplementationEmployeeId . ' 
																THEN ' . ( int ) $intNewImplementationEmployeeId . '
																ELSE billing_implementation_employee_id
															END,
						uem_implementation_employee_id = CASE
															WHEN is_uem_implementation IS TRUE AND uem_implementation_employee_id = ' . ( int ) $intOldImplementationEmployeeId . '
															THEN ' . ( int ) $intNewImplementationEmployeeId . '
															ELSE uem_implementation_employee_id
														END
					FROM
						utility_uem_settings
					WHERE
						property_utility_settings.property_id = utility_uem_settings.property_id
						AND ( is_in_implementation IS TRUE OR is_uem_implementation IS TRUE )
						AND ( billing_implementation_employee_id = ' . ( int ) $intOldImplementationEmployeeId . ' OR uem_implementation_employee_id = ' . ( int ) $intOldImplementationEmployeeId . ' );';

		return fetchData( $strSql, $objUtilitiesDatabase );

	}

	public function adjustDistributionAndVerifyInvoiceDay( $strCurrentDate, $arrmixPaidHolidays ) {
		$objCurrentDate = new DateTime( $strCurrentDate );

		$intDay = ( int ) $objCurrentDate->format( 'N' );

		if( true == in_array( $intDay, [ CPropertyHoliday::WEEKEND_SATURDAY, CPropertyHoliday::WEEKEND_SUNDAY ], true ) || ( true == valarr( $arrmixPaidHolidays ) && true == array_key_exists( $strCurrentDate, $arrmixPaidHolidays ) ) ) {
			$strCurrentDate = $objCurrentDate->sub( new DateInterval( 'P' . ( 1 ) . 'D' ) )->format( 'm/d/Y' );
			return $this->adjustDistributionAndVerifyInvoiceDay( $strCurrentDate, $arrmixPaidHolidays );
		} else {
			return $strCurrentDate;
		}
	}

	public function updatePropertyPreference( $intCurrentUserId, $intOldCompanyEmployeeId, $objClientDatabase ) {
		$arrstrPropertySettingKeys = [
			'VACANT_USAGE_EMAIL',
			'SUBMETER_NOTIFICATION',
			'RESIDENTS_NOT_SENT_INVOICE',
			'MISSING_UTILITY_BILL_EMAIL',
			'PRE_BILL_NOTIFICATION_EMAIL',
			'RETURNED_INVOICE_NOTIFICATION',
			'VACANT_COST_RECOVERY_NOTIFICATION'
		];

		$arrobjPropertySettingKeys = \Psi\Eos\Entrata\CPropertySettingKeys::createService()->fetchPropertySettingKeysByKeys( $arrstrPropertySettingKeys, $objClientDatabase, true );

		$arrobjCompanyEmployees = CCompanyEmployees::createService()->fetchCompanyEmployeesByIdsByCid( array_filter( [ $intOldCompanyEmployeeId, $this->getCompanyEmployeeId() ] ), $this->getCid(), $objClientDatabase );

		$objOldCompanyEmployee = getArrayElementByKey( $intOldCompanyEmployeeId, $arrobjCompanyEmployees );
		$objNewCompanyEmployee = getArrayElementByKey( $this->getCompanyEmployeeId(), $arrobjCompanyEmployees );

		$arrobjPropertyPreferences = rekeyObjects( 'Key', ( array ) CPropertyPreferences::fetchPropertyPreferencesByKeysByPropertyIdByCid( $arrstrPropertySettingKeys, $this->getPropertyId(), $this->getCid(), $objClientDatabase ) );

		foreach( $arrstrPropertySettingKeys as $strPropertySettingKey ) {

			$objPropertyPreference = getArrayElementByKey( $strPropertySettingKey, $arrobjPropertyPreferences );
			$objPropertySettingKey = getArrayElementByKey( $strPropertySettingKey, $arrobjPropertySettingKeys );

			if( false == valObj( $objPropertySettingKey, 'CPropertySettingKey' ) ) {
				continue;
			}

			if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {

				$arrstrEmailAddresses = array_flip( explode( ',', $objPropertyPreference->getValue() ) );

				if( true == valobj( $objOldCompanyEmployee, 'CCompanyEmployee' ) && true == array_key_exists( $objOldCompanyEmployee->getEmailAddress(), $arrstrEmailAddresses ) ) {
					unset( $arrstrEmailAddresses[$objOldCompanyEmployee->getEmailAddress()] );
				}

				if( true == valobj( $objNewCompanyEmployee, 'CCompanyEmployee' ) ) {
					$arrstrEmailAddresses[$objNewCompanyEmployee->getEmailAddress()] = $objNewCompanyEmployee->getEmailAddress();
				}

				$objPropertyPreference->setValue( implode( ',', array_unique( array_filter( array_keys( $arrstrEmailAddresses ) ) ) ) );

				$arrobjPropertyPreferences[$strPropertySettingKey] = $objPropertyPreference;
			} elseif( true == valObj( $objNewCompanyEmployee, 'CCompanyEmployee' ) ) {
				$objPropertyPreference = new CPropertyPreference();
				$objPropertyPreference->setCid( $this->getCid() );
				$objPropertyPreference->setPropertyId( $this->getPropertyId() );
				$objPropertyPreference->setPropertyPreferenceTypeId( CPropertyPreferenceType::STANDARD );
				$objPropertyPreference->setKey( $strPropertySettingKey );
				$objPropertyPreference->setValue( $objNewCompanyEmployee->getEmailAddress() );
				$objPropertyPreference->setPropertySettingKeyId( $objPropertySettingKey->getId() );
				$arrobjPropertyPreferences[$strPropertySettingKey] = $objPropertyPreference;
			}
		}

		$boolIsSuccess = true;
		foreach( $arrobjPropertyPreferences as $objPropertyPreference ) {
			if( false == $objPropertyPreference->insertOrUpdate( $intCurrentUserId, $objClientDatabase ) ) {
				$boolIsSuccess = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Unable to update property preferences' ) );
				break;
			}
		}

		return $boolIsSuccess;
	}

	public function createUpdateApPayeeAccounts( $intUserId, $intCompanyUserId, $objUtilitiesDatabase, $objClientDatabase ) {

		$arrstrWhereClause = $arrintApPayeeIds = $arrstrInvalidAccountIds = [];

		$arrobjUtilityBillAccounts = ( array ) \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchActiveUtilityBillAccountsByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );

		if( true == valArr( $arrobjUtilityBillAccounts ) ) {
			foreach( $arrobjUtilityBillAccounts as $objUtilityBillAccount ) {

				if( false == \Psi\Libraries\UtilFunctions\valId( $objUtilityBillAccount->getApPayeeId() ) || false == \Psi\Libraries\UtilFunctions\valId( $objUtilityBillAccount->getApPayeeLocationId() ) ) {
					$arrstrInvalidAccountIds[$objUtilityBillAccount->getId()] = $objUtilityBillAccount->getAccountNumber();
					continue;
				}

				$arrintUtilityBillAccountIds[] = $objUtilityBillAccount->getId();
				$arrintApPayeeIds[] = ( int ) $objUtilityBillAccount->getApPayeeId();
				$arrintApLocationIds[]	= ( int ) $objUtilityBillAccount->getApPayeeLocationId();
				$arrstrWhereClause[]	= '( ' . ( int ) $objUtilityBillAccount->getCid() . ', ' . ( int ) $objUtilityBillAccount->getApPayeeId() . ', ' . ( int ) $objUtilityBillAccount->getApPayeeLocationId() . ', upper ( \'' . str_replace( ' ', '', $objUtilityBillAccount->getAccountNumber() ) . '\' ) )';
			}
		}

		if( true == \Psi\Libraries\UtilFunctions\valArr( $arrstrInvalidAccountIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please set vendor or vendor location on the utility bill accounts - ' . sqlStrImplode( $arrstrInvalidAccountIds ) ) );
			return false;
		}

		$arrobjApPayees = ( array ) \Psi\Eos\Entrata\CApPayees::createService()->fetchApPayeesByIdsByCid( $arrintApPayeeIds, $this->getCid(), $objClientDatabase );
		$arrobjApRemittances	= rekeyObjects( 'ApPayeeLocationId', ( array ) \Psi\Eos\Entrata\CApRemittances::createService()->fetchDefaultApRemittancesByApPayeeLocationIdsByCid( $arrintApLocationIds, $this->getCid(), $objClientDatabase ) );
		$arrobjApPayeeAccounts	= rekeyObjects( 'AccountNumber', ( array ) \Psi\Eos\Entrata\CApPayeeAccounts::createService()->fetchApPayeeAccountsByCustomWhereClause( $arrstrWhereClause, $objClientDatabase ) );
		$arrobjRekeyedExistingApPayeeAccounts	= rekeyObjects( 'UtilityBillAccountId', ( array ) \Psi\Eos\Entrata\CApPayeeAccounts::createService()->fetchAllApPayeeAccountsByUtilityBillAccountIdsByCid( $arrintUtilityBillAccountIds, $this->getCid(), $objClientDatabase ), true );

		foreach( $arrobjUtilityBillAccounts as $objUtilityBillAccount ) {

			if( false == \Psi\Libraries\UtilFunctions\valId( $objUtilityBillAccount->getApPayeeId() ) ) {
				continue;
			}

			$objApPayee = \Psi\Libraries\UtilFunctions\getArrayElementByKey( $objUtilityBillAccount->getApPayeeId(), $arrobjApPayees );
			$objApRemittance = \Psi\Libraries\UtilFunctions\getArrayElementByKey( $objUtilityBillAccount->getApPayeeLocationId(), $arrobjApRemittances );
			$objApPayeeAccount = \Psi\Libraries\UtilFunctions\getArrayElementByKey( strtoupper( str_replace( ' ', '', $objUtilityBillAccount->getAccountNumber() ) ), $arrobjApPayeeAccounts );

			if( false == \Psi\Libraries\UtilFunctions\valObj( $objApPayeeAccount, 'CApPayeeAccount' ) && true == valObj( $objApPayee, 'CApPayee' ) ) {
				$objApPayeeAccount = new CApPayeeAccount();
				$objApPayeeAccount->setCid( $objUtilityBillAccount->getCid() );
				$objApPayeeAccount->setApPayeeId( $objApPayee->getId() );
				$objApPayeeAccount->setAccountNumber( str_replace( ' ', '', $objUtilityBillAccount->getAccountNumber() ) );
				$objApPayeeAccount->setApPayeeLocationId( $objUtilityBillAccount->getApPayeeLocationId() );
				$objApPayeeAccount->setDefaultPropertyId( $this->getPropertyId() );

				if( true == \Psi\Libraries\UtilFunctions\valObj( $objApRemittance, 'CApRemittance' ) ) {
					$objApPayeeAccount->setDefaultApRemittanceId( $objApRemittance->getId() );
				}
			}

			if( true == \Psi\Libraries\UtilFunctions\valObj( $objApPayeeAccount, 'CApPayeeAccount' ) ) {
				$objApPayeeAccount->setUtilityBillAccountId( $objUtilityBillAccount->getId() );
				$objApPayeeAccount->setIsDisabled( false );

				if( false == $objApPayeeAccount->insertOrUpdate( $intCompanyUserId, $objClientDatabase ) ) {
					$this->addErrorMsg( $objApPayeeAccount->getErrorMsgs() );
					return false;
				}
			}

			$arrobjExistingApPayeeAccounts = ( array ) getArrayElementByKey( $objUtilityBillAccount->getId(), $arrobjRekeyedExistingApPayeeAccounts );

			foreach( $arrobjExistingApPayeeAccounts as $objExistingApPayeeAccount ) {

				if( true == \Psi\Libraries\UtilFunctions\valObj( $objExistingApPayeeAccount, 'CApPayeeAccount' ) && ( $objUtilityBillAccount->getApPayeeId() != $objExistingApPayeeAccount->getApPayeeId() || $objUtilityBillAccount->getApPayeeLocationId() != $objExistingApPayeeAccount->getApPayeeLocationId() ) ) {
					$objExistingApPayeeAccount->setUtilityBillAccountId( NULL );

					if( false == $objExistingApPayeeAccount->update( $intCompanyUserId, $objClientDatabase ) ) {
						$this->addErrorMsg( $objExistingApPayeeAccount->getErrorMsgs() );
						return false;
					}
				}
			}

		}

		return true;
	}

	public function markApPayeeAsUtility( $intUserId, $objUtilitiesDatabase, $objClientDatabase ) {

		$arrintApPayeeIds = $arrstrInvalidAccountIds = [];

		$arrobjUtilityBillAccounts = ( array ) \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchActiveUtilityBillAccountsByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );

		foreach( $arrobjUtilityBillAccounts as $objUtilityBillAccount ) {
			if( false == \Psi\Libraries\UtilFunctions\valId( $objUtilityBillAccount->getApPayeeId() ) || false == \Psi\Libraries\UtilFunctions\valId( $objUtilityBillAccount->getApPayeeLocationId() ) ) {
				$arrstrInvalidAccountIds[$objUtilityBillAccount->getId()] = $objUtilityBillAccount->getAccountNumber();
				continue;
			}

			$arrintApPayeeIds[$objUtilityBillAccount->getApPayeeId()] = $objUtilityBillAccount->getApPayeeId();
		}

		if( true == \Psi\Libraries\UtilFunctions\valArr( $arrstrInvalidAccountIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please set vendor or vendor location on the utility bill accounts - ' . sqlStrImplode( $arrstrInvalidAccountIds ) ) );
			return false;
		}

		if( true == valArr( $arrintApPayeeIds ) ) {

			$arrobjApPayees = ( array ) \Psi\Eos\Entrata\CApPayees::createService()->fetchApPayeesByIdsByCid( $arrintApPayeeIds, $this->getCid(), $objClientDatabase );

			foreach( $arrobjApPayees as $objApPayee ) {

				$objApPayee->setIsUtilities( true );

				if( false == $objApPayee->update( $intUserId, $objClientDatabase ) ) {
					$this->addErrorMsg( $objApPayee->getErrorMsgs() );
					return false;
				}
			}
		}

		return true;

	}

	public function updateUtilityEmployee( $intOldEmployeeId, $boolIsRUEmployee, $objAdminDatabase, $objUtilitiesDatabase ) {

		if( false == valId( $intOldEmployeeId ) ) {
			return false;
		}

		$arrintAssociateDirector = [ CDesignation::ASSOCIATE_DIRECTOR_UTILITY_MANAGEMENT ];

		if( true == CStrings::strToBool( $boolIsRUEmployee ) ) {
			$arrintAssociateDirector = [ CDesignation::UTILITY_BILLING_ASSOCIATE_DIRECTOR ];
		}

		$intNewEmployeeId = $this->getValidEmployeeId( $intOldEmployeeId, $arrintAssociateDirector, $objAdminDatabase );

		if( true == CStrings::strToBool( $boolIsRUEmployee ) ) {

			$strSql = 'UPDATE property_utility_settings
							SET analyst_employee_id = ' . ( int ) $intNewEmployeeId . '
						WHERE
							analyst_employee_id  = ' . ( int ) $intOldEmployeeId;
		} else {

			$strSql = 'UPDATE utility_uem_settings
							SET uem_employee_id = ' . ( int ) $intNewEmployeeId . '
						WHERE
							uem_employee_id  = ' . ( int ) $intOldEmployeeId;

			$this->updateClientIpEmployees( $intNewEmployeeId, $intOldEmployeeId, $objUtilitiesDatabase );

		}

		return fetchData( $strSql, $objUtilitiesDatabase );

	}

	public function updateClientIpEmployees( $intNewEmployeeId, $intOldEmployeeId, $objUtilitiesDatabase ) {

		$arrobjClientIpEmployees = ( array ) \Psi\Eos\Utilities\CClientIpEmployees::createService()->fetchClientIpEmployeesByCidByEmployeeIds( [], [ $intOldEmployeeId ], $objUtilitiesDatabase );

		foreach( $arrobjClientIpEmployees as $objClientIpEmployee ) {
			$arrintEmployeeIds = [];
			foreach( $objClientIpEmployee->getEmployeeIds() as $intKey => $intEmployeeId ) {
				$arrintEmployeeIds[$intKey] = ( $intOldEmployeeId == $intEmployeeId ) ? $intNewEmployeeId : $intEmployeeId;
			}

			$objClientIpEmployee->setEmployeeIds( $arrintEmployeeIds );
			$objClientIpEmployee->Update( $intNewEmployeeId, $objUtilitiesDatabase );
		}
	}

	public function getValidEmployeeId( $intEmployeeId, $arrintAssociateDirector, $objAdminDatabase ) {

		$objEmployee = current( ( array ) \Psi\Eos\Admin\CEmployees::createService()->fetchActiveEmployeesByDesignationIds( $arrintAssociateDirector, $objAdminDatabase ) );

		if( true == valObj( $objEmployee, 'CEmployee' ) && false == $objEmployee->getIsTerminated() && $intEmployeeId != $objEmployee->getId() ) {
			return $objEmployee->getId();
		}

		$objEmployee = current( ( array ) \Psi\Eos\Admin\CEmployees::createService()->fetchActiveEmployeesByDesignationIds( [ CDesignation::DIRECTOR_OF_FULFILLMENT ], $objAdminDatabase ) );

		if( false == valObj( $objEmployee, 'CEmployee' ) ) {
			return $intEmployeeId;
		}

		return $objEmployee->getId();
	}

	public function getIsItUtilityExpenseManagement( $objAdminDatabase ) {

		$arrmixUtilityContractProperties = ( array ) \Psi\Eos\Admin\CContractProperties::createService()->fetchActiveAndTemporaryContractPropertiesByPsProductIds( [ CPsProduct::UTILITY_EXPENSE_MANAGEMENT ], $objAdminDatabase, [ $this->getCid() ], [ $this->getPropertyId() ] );

		if( false == valArr( $arrmixUtilityContractProperties ) ) {
			return false;
		}

		return ( true == valArr( current( $arrmixUtilityContractProperties ) ) ) ? true : false;
	}

	public function getIsItInvoiceProcessing( $objAdminDatabase ) {

		$arrmixUtilityContractProperties = ( array ) \Psi\Eos\Admin\CContractProperties::createService()->fetchActiveAndTemporaryContractPropertiesByPsProductIds( [ CPsProduct::INVOICE_PROCESSING ], $objAdminDatabase, [ $this->getCid() ], [ $this->getPropertyId() ] );

		return ( false == valArr( $arrmixUtilityContractProperties ) ) ? false : true;
	}

	public function getIsItResidentUtility( $objAdminDatabase ) {

		$arrmixUtilityContractProperties = ( array ) \Psi\Eos\Admin\CContractProperties::createService()->fetchActiveAndTemporaryContractPropertiesByPsProductIds( [ CPsProduct::RESIDENT_UTILITY ], $objAdminDatabase, [ $this->getCid() ], [ $this->getPropertyId() ] );

		if( false == valArr( $arrmixUtilityContractProperties ) ) {
			return false;
		}

		return ( false == valArr( $arrmixUtilityContractProperties ) ) ? false : true;
	}

	public function getIsItRuUemIp( $objAdminDatabase ) {

		$arrmixUtilityContractProperties = ( array ) \Psi\Eos\Admin\CContractProperties::createService()->fetchActiveAndTemporaryContractPropertiesByPsProductIds( CPsProduct::$c_arrintRuUemIpProductIds, $objAdminDatabase, [ $this->getCid() ], [ $this->getPropertyId() ] );

		if( true == valArr( $arrmixUtilityContractProperties ) ) {
			foreach( $arrmixUtilityContractProperties as $arrmixUtilityContractProperty ) {

				$intPsProductId = $arrmixUtilityContractProperty['ps_product_id'];

				switch( $intPsProductId ) {
					case CPsProduct::RESIDENT_UTILITY:
						$this->setIsRuProperty( true );
						break;

					case CPsProduct::UTILITY_EXPENSE_MANAGEMENT:
						$this->setIsUemProperty( true );
						break;

					case CPsProduct::INVOICE_PROCESSING:
						$this->setIsIpProperty( true );
						break;

					default:
						// Default case
						break;
				}
			}
		}
	}

	public function getPreBillDeadLine() {

		$arrmixPaidHoliday = rekeyArray( 'date', ( array ) \Psi\Eos\Admin\CPaidHolidays::createService()->fetchPaidHolidaysByCountryCodeByDateRange( date( date( 'Y' ) . '-01-01' ), date( date( 'Y' ) . '-12-31' ), CCountry::CODE_USA, $this->getAdminDatabase() ) );

		$objUtilityPreBillLibrary = new CUtilityPreBillLibrary();
		$objUtilityPreBillLibrary->setAdminDatabase( $this->getAdminDatabase() );
		$objUtilityPreBillLibrary->setPropertyUtilitySetting( $this );
		return $objUtilityPreBillLibrary->getPreBillDay( $arrmixPaidHoliday );
	}

	public function getIsFirstUtilityBatch() : bool {

		$boolIsFirstUtilityBatch = false;

		$intTotalCompletedBatchCount = \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchCompletedPreviousUtilityBatchIdByPropertyId( $this->getPropertyId(), $this->getDatabase() );

		if( true == valId( $intTotalCompletedBatchCount ) ) {
			$boolIsFirstUtilityBatch = true;
		}

		return $boolIsFirstUtilityBatch;
	}

	public function getIsActiveRuProperty( $objAdminDatabase ) {

		$boolIsRuContractedProperty = $this->getIsItResidentUtility( $objAdminDatabase );

		$boolIsActiveRuProperty = false;
		if( true == $boolIsRuContractedProperty ) {
			$boolIsActiveRuProperty = true;
			if( true == CStrings::strToBool( $this->getIsInImplementation() ) ) {
				$boolIsActiveRuProperty = false;
			}
		}

		return $boolIsActiveRuProperty;
	}

	public function loadUtilityBillAccountSetupDetail( $objUtilitiesDatabase ) {
		$arrmixUtilityBillAccounts = [
			'utility_bill_account_templates' => [],
			'rekeyed_utility_bill_accounts'  => [],
			'all_utility_bill_accounts'      => []
		];

		$arrmixPropertyUtilityTypes    = ( array ) \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchCustomPropertyUtilityTypesByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );
		$arrmixTempUtilityBillAccounts = ( array ) \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchCustomActiveUtilityBillAccountsByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );

		foreach( $arrmixTempUtilityBillAccounts as $arrmixTempUtilityBillAccount ) {
			if( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixTempUtilityBillAccount['is_template'] ) ) {
				$arrmixUtilityBillAccounts['utility_bill_account_templates'][$arrmixTempUtilityBillAccount['property_utility_type_id']][$arrmixTempUtilityBillAccount['utility_bill_type_id']] = $arrmixTempUtilityBillAccount;
			} else {
				$arrmixUtilityBillAccounts['all_utility_bill_accounts'][$arrmixTempUtilityBillAccount['property_utility_type_id']][]                                                            = $arrmixTempUtilityBillAccount;
				$arrmixUtilityBillAccounts['rekeyed_utility_bill_accounts'][$arrmixTempUtilityBillAccount['property_utility_type_id']][$arrmixTempUtilityBillAccount['utility_bill_type_id']][] = $arrmixTempUtilityBillAccount;
			}
		}

		$arrmixUtilityBillAccounts['billing_allocation_properties_count'] = \Psi\Eos\Utilities\CBillingAllocationProperties::createService()->fetchBillingAllocationCountByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );

		$this->setUtilityBillAccounts( $arrmixUtilityBillAccounts );
		$this->setPropertyUtilityTypeDeatils( $arrmixPropertyUtilityTypes );
	}

	public function checkUtilityBillAccountSetupErrors( $objAdminDatabase ) {
		// If there is no property utility types available for property then we will not check any validation.
		if( false == valArr( $this->getPropertyUtilityTypeDeatils() ) ) {
			return;
		}

		$arrstrUtilityBillAccountSetupErrors = [];
		$arrmixAllUtilityBillAccountSetupErrors = $this->getUtilityBillAccountSetupErrors();

		$boolIsUemContractedProperty = $this->getIsItUtilityExpenseManagement( $objAdminDatabase );

		foreach( $this->getPropertyUtilityTypeDeatils() as $intPropertyUtilityTypeId => $arrmixPropertyUtilityTypeDeatil ) {

			$strUtlityType				= $arrmixPropertyUtilityTypeDeatil['commodity_name'];
			$boolIsDirectMeterUtility	= \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['direct_meter_utility'] );

			$boolIsFixed			= \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['is_fixed'] );
			$boolHasVcrServices		= \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['has_vcr_service'] );
			$boolHasUemServices		= \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['has_uem_service'] );
			$boolHasBilligServices	= \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['has_billing_service'] );
			$boolIsDontRequireBills	= \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['dont_require_bills'] );

			// If a utility type Has Billing, VCR and UEM turned off then we do not need to validate.
			if( false == $boolHasBilligServices && false == $boolHasVcrServices && ( false == $boolIsUemContractedProperty || false == $boolHasUemServices ) ) {
				continue;
			}

			// If Don't Require Bills/Is Fixed is TRUE or utility type is CONVERGENT BILLING then we are not triggering validation.
			if( true == $boolIsDontRequireBills || true == $boolIsFixed || CUtilityType::CONVERGENT_BILLING == $arrmixPropertyUtilityTypeDeatil['utility_type_id'] ) {
				continue;
			}

			$arrmixPropertyUtilityTypeBillAccounts = ( array ) getArrayElementByKey( $intPropertyUtilityTypeId, getArrayElementByKey( 'rekeyed_utility_bill_accounts', $this->getUtilityBillAccounts() ) );
			$arrmixPropertyUtilityTypeBillTemplates = getArrayElementByKey( $intPropertyUtilityTypeId, getArrayElementByKey( 'utility_bill_account_templates', $this->getUtilityBillAccounts() ) );

			$intBillingAllocationPropertiesCount = getArrayElementByKey( 'billing_allocation_properties_count', $this->getUtilityBillAccounts() );

			// Check validation for VCR, direct meter and direct metered vcr.
			if( true == $boolHasVcrServices || ( true == $boolHasBilligServices && true == $boolIsDirectMeterUtility ) ) {

				// Check template for VCR.
				if( true == $boolHasVcrServices
					&& ( false == valArr( $arrmixPropertyUtilityTypeBillTemplates ) || false == array_key_exists( CUtilityBillType::VACANT_UNIT, $arrmixPropertyUtilityTypeBillTemplates ) ) ) {

					$arrstrUtilityBillAccountSetupErrors[$intPropertyUtilityTypeId][] = $strUtlityType . ' - Please configure at least one vacant utility bill account template.';
				}

				if( true == $boolHasBilligServices && true == $boolIsDirectMeterUtility
				    && ( false == valArr( $arrmixPropertyUtilityTypeBillTemplates ) || false == array_key_exists( CUtilityBillType::DIRECT_METERED, $arrmixPropertyUtilityTypeBillTemplates ) ) ) {

					$arrstrUtilityBillAccountSetupErrors[$intPropertyUtilityTypeId][] = $strUtlityType . ' - Please configure at least one direct metered utility bill account template.';
				}

				// We need to make sure that if has uem is ON or we have non direct metered utility, it must have one UBA for it.
			} elseif( 0 == $intBillingAllocationPropertiesCount && false == valArr( array_filter( $arrmixPropertyUtilityTypeBillAccounts ) ) ) {

				$arrstrUtilityBillAccountSetupErrors[$intPropertyUtilityTypeId][] = $strUtlityType . ' - You must configure at least one utility bill account.';
			}

		}

		if( true == valArr( $arrstrUtilityBillAccountSetupErrors ) ) {
			$arrmixAllUtilityBillAccountSetupErrors['errors'] = $arrstrUtilityBillAccountSetupErrors;
		}

		$this->setUtilityBillAccountSetupErrors( $arrmixAllUtilityBillAccountSetupErrors );
	}

	public function checkUtilityBillAccountSetupWarnings( $objClientDatabase ) {

		$arrobjPropertyUnits		= ( array ) \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchSimplePropertyUnitByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		$arrobjPropertyBuildings	= ( array ) \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchPropertyBuildingsByIdsByCid( array_filter( array_keys( rekeyObjects( 'PropertyBuildingId', $arrobjPropertyUnits ) ) ), $this->getCid(), $objClientDatabase );

		// If property unit not available in entrata, we are not triggering validation.
		if( false == valArr( $arrobjPropertyUnits ) ) {
			return;
		}

		$arrintPropertyUnitIds = array_keys( $arrobjPropertyUnits );
		$arrintAllPropertyBuildingIds = array_keys( $arrobjPropertyBuildings );

		$arrstrUtilityBillAccountSetupWarnings = [];
		$arrmixAllUtilityBillAccountSetupErrors = $this->getUtilityBillAccountSetupErrors();

		foreach( $this->getPropertyUtilityTypeDeatils() as $intPropertyUtilityTypeId => $arrmixPropertyUtilityTypeDeatil ) {
			$strUtlityType            = $arrmixPropertyUtilityTypeDeatil['name'];
			$boolIsDirectMeterUtility = \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['direct_meter_utility'] );

			$boolIsFixed            = \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['is_fixed'] );
			$boolHasVcrServices     = \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['has_vcr_service'] );
			$boolHasUemServices     = \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['has_uem_service'] );
			$boolIsBillByBuilding   = \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['bill_by_building'] );
			$boolHasBilligServices  = \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['has_billing_service'] );
			$boolIsDontRequireBills = \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $arrmixPropertyUtilityTypeDeatil['dont_require_bills'] );

			$boolHasUemServicesOnly = ( true == $boolHasUemServices && false == $boolHasBilligServices && false == $boolHasVcrServices );

			$arrmixPropertyUtilityTypeBillAccounts    = getArrayElementByKey( $intPropertyUtilityTypeId, getArrayElementByKey( 'rekeyed_utility_bill_accounts', $this->getUtilityBillAccounts() ) );
			$arrmixAllPropertyUtilityTypeBillAccounts = getArrayElementByKey( $intPropertyUtilityTypeId, getArrayElementByKey( 'all_utility_bill_accounts', $this->getUtilityBillAccounts() ) );

			// If Don't Require Bills/Is Fixed is TRUE or utility type is CONVERGENT BILLING.
			if( false == $boolHasUemServicesOnly
			    && ( true == $boolIsDontRequireBills || true == $boolIsFixed || CUtilityType::CONVERGENT_BILLING == $arrmixPropertyUtilityTypeDeatil['utility_type_id']
			         || ( true == $boolHasVcrServices && false == $boolHasBilligServices ) ) ) {

				continue;
			}

			/**
			 * For direct metered utility, each unit should have at least one UBA.
			 */
			if( true == $boolHasBilligServices && true == $boolIsDirectMeterUtility ) {

				$arrintDirectMeteredPropertyUnitIds = $arrstrNoDirectMeteredPropertyUnitNumbers = [];

				$arrmixDirectMeteredUtilityBillAccounts = getArrayElementByKey( CUtilityBillType::DIRECT_METERED, $arrmixPropertyUtilityTypeBillAccounts );

				if( true == valArr( $arrmixDirectMeteredUtilityBillAccounts ) ) {
					$arrintDirectMeteredPropertyUnitIds = array_filter( array_keys( rekeyArray( 'property_unit_id', $arrmixDirectMeteredUtilityBillAccounts ) ) );
				}

				$arrintNoDirectMeteredPropertyUnitIds = array_diff( $arrintPropertyUnitIds, $arrintDirectMeteredPropertyUnitIds );

				foreach( $arrintNoDirectMeteredPropertyUnitIds as $intPropertyUnitId ) {
					$strUnitNumber       = $arrobjPropertyUnits[$intPropertyUnitId]->getUnitNumber();
					$objPropertyBuilding = getArrayElementByKey( $arrobjPropertyUnits[$intPropertyUnitId]->getPropertyBuildingId(), $arrobjPropertyBuildings );

					$arrstrNoDirectMeteredPropertyUnitNumbers[] = ( true == valObj( $objPropertyBuilding, 'CPropertyBuilding' ) ) ? $objPropertyBuilding->getBuildingName() . ' - ' . $strUnitNumber : $strUnitNumber;
				}

				if( true == valArr( $arrstrNoDirectMeteredPropertyUnitNumbers ) ) {
					$arrstrUtilityBillAccountSetupWarnings[$intPropertyUtilityTypeId][] = $strUtlityType . ' - You are missing an direct metered account for the following units: ' . implode( ', ', $arrstrNoDirectMeteredPropertyUnitNumbers );
				}

				/**
				 * For Non direct metered utility, we need to check no of utility bill accounts v/s no of buildings in entrata
				 * (This one will not apply to TRASH.).
				 */
			} elseif( ( ( true == $boolHasUemServicesOnly && true == in_array( $arrmixPropertyUtilityTypeDeatil['utility_type_id'], [ CUtilityType::WATER, CUtilityType::SEWER ] ) ) || ( true == $boolHasBilligServices && CUtilityType::TRASH != $arrmixPropertyUtilityTypeDeatil['utility_type_id'] ) )
			          && ( false == valArr( $arrmixAllPropertyUtilityTypeBillAccounts ) || \Psi\Libraries\UtilFunctions\count( $arrmixAllPropertyUtilityTypeBillAccounts ) < \Psi\Libraries\UtilFunctions\count( $arrobjPropertyBuildings ) ) ) {

				$arrstrUtilityBillAccountSetupWarnings[$intPropertyUtilityTypeId][] = $strUtlityType . ' - Based on the number of buildings at this property we believe you are missing ' . ( \Psi\Libraries\UtilFunctions\count( $arrobjPropertyBuildings ) - \Psi\Libraries\UtilFunctions\count( $arrmixAllPropertyUtilityTypeBillAccounts ) ) . ' accounts.';

				/**
				 * If property utility type have Bill by buliding is ON then every building shoud have at least one UBA.
				 */
			} elseif( true == $boolHasBilligServices && true == $boolIsBillByBuilding ) {

				$arrintPropertyBuildingIds = $arrstrNoUbaBuildingNames = [];

				if( true == valArr( $arrmixAllPropertyUtilityTypeBillAccounts ) ) {
					$arrintPropertyBuildingIds = array_filter( array_keys( rekeyArray( 'property_building_id', $arrmixAllPropertyUtilityTypeBillAccounts ) ) );
				}

				$arrintPropertyBuildingIds = array_diff( $arrintAllPropertyBuildingIds, $arrintPropertyBuildingIds );

				foreach( $arrintPropertyBuildingIds as $intPropertyBuildingId ) {
					$arrstrNoUbaBuildingNames[] = $arrobjPropertyBuildings[$intPropertyBuildingId]->getBuildingName();
				}

				if( true == valArr( $arrstrNoUbaBuildingNames ) ) {
					$arrstrUtilityBillAccountSetupWarnings[$intPropertyUtilityTypeId][] = $strUtlityType . ' - You are missing an account for the following buildings: ' . implode( ', ', $arrstrNoUbaBuildingNames );
				}
			}
		}

		if( true == valArr( $arrstrUtilityBillAccountSetupWarnings ) ) {
			$arrmixAllUtilityBillAccountSetupErrors['warnings'] = $arrstrUtilityBillAccountSetupWarnings;
		}

		$this->setUtilityBillAccountSetupErrors( $arrmixAllUtilityBillAccountSetupErrors );
	}

	public function checkMissingDisableUemDocuments( $objUtilitiesDatabase ) {

		$arrintPropertyUtilityCount    = ( array ) \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchMissingDisableUemDocumentPropertyUtilityTypesCountByPropertyIdByCid( $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );

		if( 0 < $arrintPropertyUtilityCount['0'] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Commodity has been disabled and documents showing that the client requested this have not been uploaded. Please upload this documentation.' ) );
			return false;
		}
		return true;
	}

	public function updateDateWiseScheduleExportDate( $arrmixUbaAndUemGoLiveDetails, $objDatabase ) {

		$boolSuccessFlag = true;
		if( false == valArr( $arrmixUbaAndUemGoLiveDetails ) ) {
			$boolSuccessFlag = false;
		}

		$arrintUtilityBillTypeIds = CUtilityBillType::$c_arrintProcessingBillTypes;

		$strSubSql = '';

		foreach( $arrmixUbaAndUemGoLiveDetails as $intUtilityBillAccId => $strUemGoLiveDate ) {

			if( $strSubSql == '' ) {
				$strSubSql .= '( ( utility_bill_account_id = ' . ( int ) $intUtilityBillAccId . ' AND created_on::DATE >= \'' . $strUemGoLiveDate . '\' ) ';
			} else {
				$strSubSql .= 'OR ( utility_bill_account_id = ' . ( int ) $intUtilityBillAccId . ' AND created_on::DATE >= \'' . $strUemGoLiveDate . '\' ) ';
			}
		}

		if( $strSubSql != '' ) {
			$strSubSql .= ' )';
		}

		$strSql = 'UPDATE
						utility_bills
					SET
						scheduled_export_date = now(),
						ps_product_id = ' . CPsProduct::UTILITY_EXPENSE_MANAGEMENT . '
					WHERE
						' . $strSubSql . '
						AND property_id IN ( ' . $this->m_intPropertyId . ' ) AND
						is_historical = FALSE AND
						processed_on IS NOT NULL AND
						processed_again_by IS NOT NULL AND
						scheduled_export_date IS NULL AND
						exported_on IS NULL AND
						exported_by IS NULL AND
						ap_header_id IS NULL AND
						deleted_by IS NULL AND
						deleted_on IS NULL AND
						is_queue_ignored IS FALSE AND
						ap_exception_queue_item_id IS NULL AND
						utility_bill_type_id IN ( ' . implode( ',', $arrintUtilityBillTypeIds ) . ' ) AND
						( review_utility_bill_id IS NULL OR ( review_utility_bill_id IS NOT NULL AND reviewed_by IS NOT NULL AND reviewed_on IS NOT NULL ) )
						AND copy_utility_bill_id IS NULL AND
						ps_product_id = ' . CPsProduct::RESIDENT_UTILITY;

		if( true == is_bool( fetchData( $strSql, $objDatabase ) ) ) {
			$boolSuccessFlag = false;
		}
		return $boolSuccessFlag;
	}

	public function createOnlyDate( $strDateTime ) {

		$strNewlyCreatedDate = new DateTime( $strDateTime );

		$strFormattedDate = $strNewlyCreatedDate->format( 'm/d/Y' );

		return $strFormattedDate;
	}

	public function mergeUtilityPropertyUnit( $objUser, $objUtilitiesDatabase ) {

		$strSql = ' UPDATE 
						utility_accounts ua
					SET
						property_unit_id = upu.property_unit_id,
						updated_by = ' . $objUser->getId() . ',
						updated_on = NOW()
					FROM
						utility_property_units upu
					WHERE
						ua.cid = upu.cid
						AND ua.property_id = upu.property_id
						AND upu.unit_space_id = ANY ( ua.unit_space_ids )
						AND ua.cid = ' . ( int ) $this->getCid() . '
						AND ua.property_id = ' . ( int ) $this->getPropertyId();

		return fetchData( $strSql, $objUtilitiesDatabase );
	}

	public function checkUtilityProviderChangeOfAddressWarnings( $objUtilitiesDatabase ) {

		$arrintUtilityProviderIds = $arrintProviderIdsHavingGasOrElectricUtility = $arrintProviderIdsHavingOtherUtilities = [];

		$arrmixTempUtilityBillAccounts = ( array ) \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchActiveUtilityBillAccountProviderDetailsByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );

		if( false == valArr( $arrmixTempUtilityBillAccounts ) ) {
			return true;
		}

		foreach( $arrmixTempUtilityBillAccounts as $arrmixTempUtilityBillAccount ) {
			if( true == in_array( $arrmixTempUtilityBillAccount['utility_type_id'], CUtilityType::$c_arrintNJStateUtilityTypes ) ) {
				$arrintProviderIdsHavingGasOrElectricUtility[$arrmixTempUtilityBillAccount['utility_provider_id']] = $arrmixTempUtilityBillAccount['utility_provider_id'];
			} else {
				$arrintProviderIdsHavingOtherUtilities[$arrmixTempUtilityBillAccount['utility_provider_id']] = $arrmixTempUtilityBillAccount['utility_provider_id'];
			}
		}

		$arrintUtilityProviderIds = array_merge( $arrintProviderIdsHavingGasOrElectricUtility, $arrintProviderIdsHavingOtherUtilities );

		$arrmixPropertyUtilityProviderEventDetails = ( array ) \Psi\Eos\Utilities\CPropertyUtilityProviderEventDetails::createService()->fetchPropertyUtilityProviderEventDetailsByPropertyIdByUtilityProviderIds( $this->getPropertyId(), $arrintUtilityProviderIds, $objUtilitiesDatabase );

		$arrmixPropertyUtilityProviderEventDetails = rekeyArray( 'utility_provider_id', $arrmixPropertyUtilityProviderEventDetails );

		$arrintPropertyUtilityProviderIds = $arrstrUtilityProviderChangeOfAddressSetupWarnings = [];

		if( true == valArr( $arrmixPropertyUtilityProviderEventDetails ) ) {

			foreach( $arrmixPropertyUtilityProviderEventDetails as $intUtilityProviderId => $arrmixTempPropertyUtilityProviderEventDetails ) {
				$strUtlityProviderName = $arrmixTempPropertyUtilityProviderEventDetails['name'];

				$arrintEventTypeIds = explode( ',', $arrmixTempPropertyUtilityProviderEventDetails['provider_event_types'] );

				$arrintChangeofAddressItemsNotPresent = array_diff( CUtilityProviderEventType::$c_arrintLOAAndCOA, $arrintEventTypeIds );

				if( true == in_array( $intUtilityProviderId, $arrintProviderIdsHavingGasOrElectricUtility ) ) {
					$arrintChangeofAddressItemsNotPresent = array_diff( CUtilityProviderEventType::$c_arrintLOAAndCOAAndRTO, $arrintEventTypeIds );
				}

				if( true == valArr( $arrintChangeofAddressItemsNotPresent ) ) {
					foreach( $arrintChangeofAddressItemsNotPresent as $intKey => $intPropertyUtilityProviderEventTypeId ) {

						if( $intPropertyUtilityProviderEventTypeId == CUtilityProviderEventType::LETTER_OF_AUTHORIZATION ) {
							$arrstrUtilityProviderChangeOfAddressSetupWarnings[$intUtilityProviderId][] = 'Letter of Authorization was not done for Provider ' . $strUtlityProviderName;
						}

						if( $intPropertyUtilityProviderEventTypeId == CUtilityProviderEventType::CHANGE_OF_ADDRESS ) {
							$arrstrUtilityProviderChangeOfAddressSetupWarnings[$intUtilityProviderId][] = 'Change of Address Request was not done for Provider ' . $strUtlityProviderName;
						}

						if( $intPropertyUtilityProviderEventTypeId == CUtilityProviderEventType::REVERT_TO_OWNER_AGREEMENT ) {
							$arrstrUtilityProviderChangeOfAddressSetupWarnings[$intUtilityProviderId][] = 'Revert to Owner Agreement Address Request not done for Provider ' . $strUtlityProviderName;
						}
					}
				}

				$arrintPropertyUtilityProviderIds[] = $intUtilityProviderId;
			}

			if( count( $arrintPropertyUtilityProviderIds ) != count( $arrintUtilityProviderIds ) ) {
				$arrmixTempUtilityBillAccounts = rekeyArray( 'utility_provider_id', $arrmixTempUtilityBillAccounts );
				foreach( $arrmixTempUtilityBillAccounts as $intUtilityProviderId => $arrmixTempUtilityBillAccount ) {

					if( false == in_array( $intUtilityProviderId, $arrintPropertyUtilityProviderIds ) ) {
						$arrstrUtilityProviderChangeOfAddressSetupWarnings[$intUtilityProviderId][] = 'Letter of Authorization was not done and Change of Address Request was not done or/and Revert to Owner Agreement Address Request not done for Provider ' . $arrmixTempUtilityBillAccount['utility_ptovider_name'];
					}
				}
			}

		} else {
			$arrstrUtilityProviderChangeOfAddressSetupWarnings[0][] = 'Letter of Authorization was not done and Change of Address Request was not done or/and Revert to Owner Agreement Address Request not done for all Providers';
		}

		if( true == valArr( $arrstrUtilityProviderChangeOfAddressSetupWarnings ) ) {
			$arrmixAllUtilityProviderChangeOfAddressWarnings['change_of_address'] = $arrstrUtilityProviderChangeOfAddressSetupWarnings;
		}

		$this->setUtilityProviderChangeOfAddressWarnings( $arrmixAllUtilityProviderChangeOfAddressWarnings );
	}

	public function checkPropertyCommodities( $objUtilitiesDatabase ) {

		$arrobjBillingPropertyUtilityTypes = rekeyObjects( 'id', ( array ) \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchAllBillingPropertyUtilityTypesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objUtilitiesDatabase ) );

		$arrintUtilityBillAccountPropertyUtilityTypes = array_keys( rekeyArray( 'property_utility_type_id', ( array ) \Psi\Eos\Utilities\CUtilityBillAccountTemplateCharges::createService()->fetchUtilityBillAccountPropertyUtilityTypesByPropertyId(  $this->getPropertyId(), $objUtilitiesDatabase ) ) );

		$arrobjPropertyCommodities = ( array ) \Psi\Eos\Utilities\CPropertyCommodities::createService()->fetchPropertyCommoditiesByPropertyUtilityTypeIds( array_keys( $arrobjBillingPropertyUtilityTypes ), $objUtilitiesDatabase );

		$arrintRekeyedPropertyCommodityIds = array_keys( rekeyObjects( 'propertyUtilityTypeId', $arrobjPropertyCommodities ) );

		$arrintMissingCommoditiesUtilities = array_diff( array_keys( $arrobjBillingPropertyUtilityTypes ), $arrintRekeyedPropertyCommodityIds );

		foreach( $arrintMissingCommoditiesUtilities as $intPropertyUtilityTypeId ) {

			if( true == in_array( $intPropertyUtilityTypeId, $arrintUtilityBillAccountPropertyUtilityTypes ) ) {
				continue;
			}
			/** @var $objPropertyUtilityType \CPropertyUtilityType */
			$objPropertyUtilityType = getArrayElementByKey( $intPropertyUtilityTypeId, $arrobjBillingPropertyUtilityTypes );
			$this->m_arrstrPropertyCommodityWarnings[$intPropertyUtilityTypeId] = 'Billing Utility ' . $objPropertyUtilityType->getName(). ' does not have a commodity mapped with it.';
		}

		$arrmixCommodityValidation = ( array ) \Psi\Eos\Utilities\CUtilityBillAccountCommodities::createService()->fetchCommodityMappingWithPropertyUtilityTypeByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );

		foreach( $arrmixCommodityValidation as $arrmixCommodityValidationDetails ) {
			$strKey = 'commodity ~ ' . $arrmixCommodityValidationDetails['name'] ;
			$this->m_arrstrPropertyCommodityWarnings[$strKey] = 'Commodity ' . $arrmixCommodityValidationDetails['name'] . ' is not mapped with a Billing Utility.';
		}
	}

	public function getPropertyCommodityWarnings() {
		return $this->m_arrstrPropertyCommodityWarnings;
	}


}
?>