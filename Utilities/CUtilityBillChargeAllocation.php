<?php

class CUtilityBillChargeAllocation extends CBaseUtilityBillChargeAllocation {

	protected $m_intPropertyUtilityTypeId;

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['property_utility_type_id'] ) ) {
			$this->setPropertyUtilityTypeId( $arrmixValues['property_utility_type_id'] );
		}

    }

	public function unsetAllErrorMsgs() {
		$this->m_arrobjErrorMsgs = NULL;
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->m_intPropertyUtilityTypeId = $intPropertyUtilityTypeId;
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

}
?>