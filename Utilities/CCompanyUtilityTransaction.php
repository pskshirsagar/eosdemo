<?php

class CCompanyUtilityTransaction extends CBaseCompanyUtilityTransaction {

	protected $m_fltTransactionAmount;

    public function getTransactionAmount() {
    	return $this->m_fltTransactionAmount;
    }

    public function setTransactionAmount( $fltTransactionAmount ) {
    	$this->m_fltTransactionAmount = $fltTransactionAmount;
    }

}
?>