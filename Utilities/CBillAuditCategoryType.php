<?php

class CBillAuditCategoryType extends CBaseBillAuditCategoryType {

	const BILL_DATA		= 1;
	const CONSUMPTION	= 2;
	const FINANCIAL		= 3;

}
?>