<?php

class CComplianceJob extends CBaseComplianceJob {

	protected $m_intScreeningId;

	protected $m_strCompanyName;
	protected $m_strComplianceRuleIds;
	protected $m_strComplianceJobItemIds;
	protected $m_strComplianceStatusName;
	protected $m_strComplianceRulesetName;
	protected $m_strComplianceJobItemEntrataReferenceIds;

	protected $m_arrobjComplianceJobItems;

	const SCREENING_APPLICATION_TYPE_ID = 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );

			return $this->update( $intUserId, $objDatabase );
		}

		return parent::delete( $intUserId, $objDatabase );
	}

	public function createComplianceJobItem() {

		$objComplianceJobItem = new CComplianceJobItem();

		$objComplianceJobItem->setCid( $this->getCid() );
		$objComplianceJobItem->setPropertyId( $this->getPropertyId() );
		$objComplianceJobItem->setVendorInvitationId( $this->getVendorInvitationId() );
		$objComplianceJobItem->setComplianceJobId( $this->getId() );
		$objComplianceJobItem->setComplianceStatusId( CComplianceStatus::REQUESTED );

		return $objComplianceJobItem;
	}

	public function getComplianceRulesetName() {
		return $this->m_strComplianceRulesetName;
	}

	public function getComplianceStatusName() {
		return $this->m_strComplianceStatusName;
	}

	public function setComplianceRulesetName( $strComplianceRulesetName ) {
		$this->m_strComplianceRulesetName = $strComplianceRulesetName;
	}

	public function setComplianceStatusName( $strComplianceStatusName ) {
		$this->m_strComplianceStatusName = $strComplianceStatusName;
	}

	public function addComplianceJobItem( $objComplianceJobItem ) {
		$this->m_arrobjComplianceJobItems[] = $objComplianceJobItem;
	}

	public function getComplianceJobItems() {
		return $this->m_arrobjComplianceJobItems;
	}

	public function getComplianceJobItemIds() {
		return $this->m_strComplianceJobItemIds;
	}

	public function getComplianceRuleIds() {
		return $this->m_strComplianceRuleIds;
	}

	public function getScreeningId() {
		return $this->m_intScreeningId;
	}

	public function setComplianceJobItemIds( $strComplianceJobItemIds ) {
		$this->m_strComplianceJobItemIds = $strComplianceJobItemIds;
	}

	public function setComplianceRuleIds( $strComplianceRuleIds ) {
		$this->m_strComplianceRuleIds = $strComplianceRuleIds;
	}

	public function setScreeningId( $intScreeningId ) {
		$this->m_intScreeningId = ( int ) $intScreeningId;
	}

	public function getComplianceJobItemEntrataReferenceIds() {
		return $this->m_strComplianceJobItemEntrataReferenceIds;
	}

	public function setComplianceJobItemEntrataReferenceIds( $strComplianceJobItemEntrataReferenceIds ) {
		$this->m_strComplianceJobItemEntrataReferenceIds = $strComplianceJobItemEntrataReferenceIds;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['compliance_ruleset_name'] ) ) {
			$this->setComplianceRulesetName( $arrmixValues['compliance_ruleset_name'] );
		}

		if( true == isset( $arrmixValues['compliance_status_name'] ) ) {
			$this->setComplianceStatusName( $arrmixValues['compliance_status_name'] );
		}

		if( true == isset( $arrmixValues['compliance_job_item_ids'] ) ) {
			$this->setComplianceJobItemIds( $arrmixValues['compliance_job_item_ids'] );
		}

		if( true == isset( $arrmixValues['entrata_reference_ids'] ) ) {
			$this->setComplianceJobItemEntrataReferenceIds( $arrmixValues['entrata_reference_ids'] );
		}

		if( true == isset( $arrmixValues['compliance_rule_ids'] ) ) {
			$this->setComplianceRuleIds( $arrmixValues['compliance_rule_ids'] );
		}

		if( true == isset( $arrmixValues['company_name'] ) ) {
			$this->setCompanyName( $arrmixValues['company_name'] );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolReturnCustomSqlOnly = false ) {

		if( true == $boolReturnSqlOnly && $boolReturnCustomSqlOnly ) {

			$strId = ( true == is_null( $this->getId() ) ) ?    'nextval( \'public.compliance_jobs_id_seq\' )' : $this->sqlId();

			$strSql = 'INSERT INTO
                        public.compliance_jobs
                    VALUES ( ' .
			 $strId . ', ' .
			 $this->sqlCid() . ', ' .
			 $this->sqlVendorInvitationId() . ', ' .
			 $this->sqlPropertyId() . ', ' .
			 $this->sqlComplianceStatusId() . ', ' .
			 $this->sqlComplianceRulesetId() . ', ' .
			 $this->sqlApPayeeId() . ', ' .
			 $this->sqlVendorId() . ', ' .
			 $this->sqlApLegalEntityId() . ', ' .
			 $this->sqlVendorEntityId() . ', ' .
			 $this->sqlTaskId() . ', ' .
			 $this->sqlLastRequestedOn() . ', ' .
			 $this->sqlLastApprovedOn() . ', ' .
			 $this->sqlDeclinedBy() . ', ' .
			 $this->sqlDeclinedOn() . ', ' .
			 $this->sqlDeletedBy() . ', ' .
			 $this->sqlDeletedOn() . ', ' .
			 ( int ) $intCurrentUserId . ', ' .
			 $this->sqlUpdatedOn() . ', ' .
			 ( int ) $intCurrentUserId . ', ' .
			 $this->sqlCreatedOn() . ' ) RETURNING id;';

			return $strSql;
		}

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function createScreeningApplicationRequest( $intCompanyUserId, $objDatabase ) {

		$objScreeningApplicationRequest = new CScreeningApplicationRequest();

		$objScreeningApplicationRequest->setApplicationId( $this->getId() );
		$objScreeningApplicationRequest->setCid( $this->getCid() );
		$objScreeningApplicationRequest->setScreeningVendorId( CTransmissionVendor::RESIDENT_VERIFY );
		$objScreeningApplicationRequest->setRequestStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );
		$objScreeningApplicationRequest->setApplicationTypeId( self::SCREENING_APPLICATION_TYPE_ID );

		$objDatabase->begin();

		if( false == $objScreeningApplicationRequest->insert( $intCompanyUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Compliance Job [ID: ' . $this->getId() . '] : Failed to create screening application request.', NULL ) );
			$objDatabase->rollback();
			return false;
		}

		$objDatabase->commit();

		return $objScreeningApplicationRequest;
	}

	public function fetchScreeningApplicationRequest( $objDatabase ) {
		// @TODO: Add ApplicationType
		return CScreeningApplicationRequests::fetchScreeningApplicationRequestByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

}
?>
