<?php

class CTestUtilityBill extends CBaseTestUtilityBill {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTestCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTestUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestSubmittedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestSubmittedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>