<?php

class CUtilityTransmission extends CBaseUtilityTransmission {

	const VALIDATE_INSERT_FOR_BATCH_PROCESS = 'insert_for_batch_process';

	const LOW_USAGE_PER_DAY  				 = 20;
	const HIGH_USAGE_PER_DAY 			     = 200;
	const HIGH_USAGE_PER_DAY_FOR_VACANT_UNIT = 5;

	protected $m_intFileSize;
	protected $m_strFileError;
	protected $m_strTempFileName;
	protected $m_strPropertyName;
	protected $m_strUtilityTypeName;

	protected $m_boolIsError;

    public function __construct() {
        parent::__construct();
		$this->m_boolIsError = false;
    }

	/**
	 * Set Functions
	 */

	public function getUtilityTypeName() {
    	return $this->m_strUtilityTypeName;
    }

    public function getIsError() {
    	return $this->m_boolIsError;
    }

	public function setUtilityTypeName( $strUtilityTypeName ) {
    	$this->m_strUtilityTypeName = $strUtilityTypeName;
    	return $this->m_strUtilityTypeName;
    }

    public function setIsError( $boolIsError ) {
    	$this->m_boolIsError = $boolIsError;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['utility_type_name'] ) ) {
    		$this->setUtilityTypeName( $arrmixValues['utility_type_name'] );
	    }

    }

	public function valCid() {
		$boolIsValid = true;
		if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client', 'client is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
        }

        return $boolIsValid;
    }

	public function valPropertyUtilityTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyUtilityTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_property_type_id', 'Type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valUtilityTransmissionTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityTransmissionTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_transmission_type_id', ' Transmission type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valTransmissionDatetime() {

        $boolIsValid = true;

        if( true == is_null( $this->getTransmissionDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_datetime', ' Transmission date is required.' ) );
        } elseif( 1 !== CValidation::checkDate( $this->getTransmissionDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_datetime', 'Transmission date must be in mm/dd/yyyy form.' ) );
        }

        return $boolIsValid;
    }

    public function valUtilityTransmissionDateByPropertyIdByUtilityTypeId( $objDatabase ) {

    	$boolIsValid = true;

		$strCondition = '';
    	if( false == is_null( $this->getId() ) ) {
    		$strCondition = '  AND id <> ' . ( int ) $this->getId();
    	}

    	$strSql = 'WHERE
    					cid = ' . ( int ) $this->getCid() . '
    					AND property_id = ' . ( int ) $this->getPropertyId() . '
    					AND utility_transmission_type_id = ' . ( int ) $this->getUtilityTransmissionTypeId() . '
    					AND property_utility_type_id = ' . ( int ) $this->getPropertyUtilityTypeId() . '
    					AND to_char(transmission_datetime,\'DD\') = \'' . date( 'd', strtotime( $this->getTransmissionDatetime() ) ) . '\'
						AND to_char(transmission_datetime,\'MM\') = \'' . date( 'm', strtotime( $this->getTransmissionDatetime() ) ) . '\'
						AND to_char(transmission_datetime,\'YYYY\') = \'' . date( 'Y', strtotime( $this->getTransmissionDatetime() ) ) . '\'
						' . $strCondition;

    	$intCount = \Psi\Eos\Utilities\CUtilityTransmissions::createService()->fetchUtilityTransmissionCount( $strSql, $objDatabase );

    	if( 0 != $intCount ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  ' Transmission already exist for this property and property utility type.' ) );
    	}

    	return $boolIsValid;
    }

    public function valDependantInformation( $objDatabase ) {

    	$boolIsValid = true;

    	$intCount = \Psi\Eos\Utilities\CUtilityTransmissionDetails::createService()->fetchUtilityTransmissionDetailCount( 'WHERE utility_transmission_id = ' . $this->getId(), $objDatabase );

    	if( 0 != $intCount ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' Utility transmission id ' . $this->getId() . ' cannot be removed because utility transmission detail(s) depends on it.' ) );
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valPropertyUtilityTypeId();
				$boolIsValid &= $this->valUtilityTransmissionTypeId();
				$boolIsValid &= $this->valUtilityTransmissionDateByPropertyIdByUtilityTypeId( $objDatabase );
				break;

			case self::VALIDATE_INSERT_FOR_BATCH_PROCESS:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valPropertyUtilityTypeId();
				$boolIsValid &= $this->valUtilityTransmissionTypeId();
				break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

     /**
      * Create Functions
      */

    public function createUtilityTransmissionDetail() {

		$objUtilityTransmissionDetail = new CUtilityTransmissionDetail();
		$objUtilityTransmissionDetail->setCid( $this->getCid() );
		$objUtilityTransmissionDetail->setPropertyId( $this->getPropertyId() );
		$objUtilityTransmissionDetail->setUtilityTransmissionId( $this->getId() );
		$objUtilityTransmissionDetail->setTransmissionDatetime( $this->getTransmissionDatetime() );

		return $objUtilityTransmissionDetail;
    }

	public function createPropertyMeterDevice() {

		$objPropertyMeterDevice = new CPropertyMeterDevice();
		$objPropertyMeterDevice->setCid( $this->getCid() );
		$objPropertyMeterDevice->setPropertyId( $this->getPropertyId() );

		return $objPropertyMeterDevice;
    }

     /**
      * Fetch Functions
      */

    public function fetchUtilityTransmissionDetails( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransmissionDetails::createService()->fetchUtilityTransmissionDetailsByUtilityTransmissionId( $this->getId(), $objDatabase );
    }

}
?>