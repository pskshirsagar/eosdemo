<?php

class CComplianceJobLog extends CBaseComplianceJobLog {

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valPropertyId() {
		return true;
	}

	public function valVendorInvitationId() {
		return true;
	}

	public function valComplianceJobId() {
		return true;
	}

	public function valComplianceStatusId() {
		return true;
	}

	public function valComplianceRulesetId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valTaskId() {
		return true;
	}

	public function valLogDatetime() {
		return true;
	}

	public function valLastRequestedOn() {
		return true;
	}

	public function valLastApprovedOn() {
		return true;
	}

	public function valDeclinedBy() {
		return true;
	}

	public function valDeclinedOn() {
		return true;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>