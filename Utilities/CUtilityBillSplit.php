<?php

class CUtilityBillSplit extends CBaseUtilityBillSplit {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSplitPages() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomSplit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>