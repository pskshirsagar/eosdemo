<?php

class CUtilityProvider extends CBaseUtilityProvider {
	// Note: Changed sample bill provider name to dummy provider named as '--'
	const SAMPLE_UTILITY_PROVIDER = 588;

	protected $m_strUtilityTypeName;
	protected $m_strPhoneNumberTypeName;
	protected $m_strCountryName;
	protected $m_strStateName;
	protected $m_strCheckAccountTypeName;
	protected $m_strCheckAccountNumber;
	protected $m_objAdminDatabase;
	protected $m_objPaymentDatabase;

	protected $m_intPropertyUtilityTypeId;
	protected $m_intUtilityBatchId;

	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_strCheckNameOnAccount;
	protected $m_strCheckBankName;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;

	/**
	 * Get Functions
	 */

	public function getUtilityTypeName() {
		return $this->m_strUtilityTypeName;
	}

	public function getPhoneNumberTypeName() {
		return $this->m_strPhoneNumberTypeName;
	}

	public function getAdminDatabse() {
		return $this->m_objAdminDatabase;
	}

	public function getPaymentDatabse() {
		return $this->m_objPaymentDatabase;
	}

	public function getCheckAccountNumber() {
		if( true == is_null( $this->m_strCheckAccountNumber ) ) {
			return false;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = CStrings::strTrimDef( $strStreetLine1, 100, NULL, true );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = CStrings::strTrimDef( $strStreetLine2, 100, NULL, true );
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->m_strStreetLine3 = CStrings::strTrimDef( $strStreetLine3, 100, NULL, true );
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = CStrings::strTrimDef( $strCity, 50, NULL, true );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = CStrings::strTrimDef( $strStateCode, 2, NULL, true );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function setProvince( $strProvince ) {
		$this->m_strProvince = CStrings::strTrimDef( $strProvince, 50, NULL, true );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = CStrings::strTrimDef( $strPostalCode, 20, NULL, true );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = CStrings::strTrimDef( $strCountryCode, 2, NULL, true );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->m_strCheckNameOnAccount = CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->m_strCheckBankName = CStrings::strTrimDef( $strCheckBankName, 100, NULL, true );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->m_strCheckRoutingNumber = CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->m_strCheckAccountNumberEncrypted = CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function setStateName( $strStateName ) {
		$this->m_strStateName = CStrings::strTrimDef( $strStateName );
	}

	public function getStateName() {
		return $this->m_strStateName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_type_name'] ) ) {
			$this->setUtilityTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_type_name'] ) : $arrmixValues['utility_type_name'] );
		}
		if( true == isset( $arrmixValues['phone_number_type_name'] ) ) {
			$this->setPhoneNumberTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['phone_number_type_name'] ) : $arrmixValues['phone_number_type_name'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_id'] ) ) {
			$this->setPropertyUtilityTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_utility_type_id'] ) : $arrmixValues['property_utility_type_id'] );
		}
		if( true == isset( $arrmixValues['utility_batch_id'] ) ) {
			$this->setUtilityBatchId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_batch_id'] ) : $arrmixValues['utility_batch_id'] );
		}
	}

	public function setUtilityTypeName( $strUtilityTypeName ) {
		$this->m_strUtilityTypeName = $strUtilityTypeName;
	}

	public function setPhoneNumberTypeName( $strPhoneNumberTypeName ) {
		$this->m_strPhoneNumberTypeName = $strPhoneNumberTypeName;
	}

	public function setAdminDatabse( $objAdminDatabse ) {
		return $this->m_objAdminDatabase = $objAdminDatabse;
	}

	public function setPaymentDatabse( $objPaymentDatabse ) {
		return $this->m_objPaymentDatabase = $objPaymentDatabse;
	}

	public function valUtilityTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_type_id', 'Utility type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valProviderDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getProviderDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'provider_datetime', 'Provider date is required.' ) );
		}

		return $boolIsValid;
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->m_intPropertyUtilityTypeId = $intPropertyUtilityTypeId;
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->m_intUtilityBatchId = $intUtilityBatchId;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valWebsiteUrl() {
		$boolIsValid = true;

		if( false == is_null( $this->getWebsiteUrl() ) && false == CValidation::checkUrl( $this->getWebsiteUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_url', 'Invalid website url.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( false == is_null( $this->getNameFirst() ) && false == preg_match( '/^[a-zA-Z]*$/', $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Invalid first name.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( false == is_null( $this->getNameLast() ) && false == preg_match( '/^[a-zA-Z]*$/', $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Invalid last name.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == is_null( $this->getEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Invalid email address.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;

		if( false == is_null( $this->getPhoneNumber() ) ) {

			$intLength = strlen( str_replace( '-', '', $this->getPhoneNumber() ) );

			if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number must be between 10 and 15 numbers.' ) );
			} elseif( false == CValidation::validateFullPhoneNumber( $this->getPhoneNumber(), false ) || 0 == $this->getPhoneNumber() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Invalid phone number.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$intUtilityBillTemplatesCount = \Psi\Eos\Utilities\CUtilityBillTemplates::createService()->fetchUtilityBillTemplateCount( 'WHERE utility_provider_id = ' . ( int ) $this->getId(), $objDatabase );

			if( 0 < $intUtilityBillTemplatesCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' Utility provider \'' . $this->m_strName . '\' is already linked with utility bill template(s).' ) );
			}

			$intUtilityBillAccountDetails = \Psi\Eos\Utilities\CUtilityBillAccountDetails::createService()->fetchUtilityBillAccountDetailCount( 'WHERE utility_provider_id = ' . ( int ) $this->getId(), $objDatabase );

			if( 0 < $intUtilityBillAccountDetails ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' Utility provider \'' . $this->m_strName . '\' is already linked with utility bill account detail(s).' ) );
			}

		} else {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Invalid databse object.' ) );
		}

		return $boolIsValid;
	}

	public function valVendorCode( $objDatabase ) {
        $boolIsValid = true;
        if( false == is_null( $this->getVendorCode() ) ) {

            $arrobjUtilityProviders = \Psi\Eos\Utilities\CUtilityProviders::createService()->fetchUtilityProvidersByVendorCode( $this->getVendorCode(), $objDatabase, $this->getId() );

            if( true == valArr( $arrobjUtilityProviders ) ) {
                $boolIsValid = false;
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_code', 'Other Provider is already associated with vendor code:'. $this->getVendorCode() ) );
            }
        }

        return $boolIsValid;
    }

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valUtilityTypeId();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valProviderDatetime();
				$boolIsValid &= $this->valWebsiteUrl();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress();
                $boolIsValid &= $this->valVendorCode( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valIsRequiredSpecificLoaForm() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredSpecificCoaForm() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredSpecificRtoForm() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredAccountNumberForCoaRequests() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredServiceAddressForCoaRequests() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredAccountNumberForRtoRequests() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredServiceAddressForRtoRequests() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsContactPreferenceSameAsCoa() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredSeparateRtoRequest() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		$boolChangeFlag = false;

		if( false == $boolSkipLog && false == $boolReturnSqlOnly ) {
			$objUtilitySettingLog = new CUtilitySettingLog();
			$boolChangeFlag = $objUtilitySettingLog->createUpdateLog( 'utility_providers', $this, $intCurrentUserId, $objDatabase );
		}

		$boolUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolUpdateStatus && false == $boolSkipLog && true == $boolChangeFlag ) {

			return $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase );
		}

		return $boolUpdateStatus;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		$this->m_strProviderDatetime = date( 'm/d/Y H:i:s T' );

		if( false == $boolSkipLog && false == $boolReturnSqlOnly ) {
			$objUtilitySettingLog = new CUtilitySettingLog();
			$objUtilitySettingLog->createInsertLog( 'utility_providers', $this, $intCurrentUserId, $objDatabase );
		}

		$boolInsertStatus = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolInsertStatus && false == $boolSkipLog ) {

			$objUtilitySettingLog->setReferenceId( $this->getId() );
			return $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase );
		}

		return $boolInsertStatus;

	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		if( false == $boolSkipLog && false == $boolReturnSqlOnly ) {
			$objUtilitySettingLog = new CUtilitySettingLog();
			$objUtilitySettingLog->createDeleteLog( 'utility_providers', $this->getId(), $intCurrentUserId, $objDatabase );
		}

		$boolDeleteStatus = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolDeleteStatus && false == $boolSkipLog ) {

			return $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase );
		}

		return $boolDeleteStatus;
	}

}