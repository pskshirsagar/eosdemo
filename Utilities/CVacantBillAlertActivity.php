<?php

class CVacantBillAlertActivity extends CBaseVacantBillAlertActivity {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVacantBillAlertId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVacantBillAlertEventId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMemo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivityDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>