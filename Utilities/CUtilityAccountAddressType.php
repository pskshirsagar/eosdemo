<?php

class CUtilityAccountAddressType extends CBaseUtilityAccountAddressType {

	const PRIMARY 		= 1;
	const TEMPORARY 	= 2;
	const FORWARDING 	= 3;

}
?>