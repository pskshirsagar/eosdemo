<?php

class CUtilityAccountPhoneNumber extends CBaseUtilityAccountPhoneNumber {

    public function valPhoneNumberTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPhoneNumberTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_type_id', 'Phone number type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valMessageOperatorId() {
        $boolIsValid = true;

        if( CPhoneNumberType::MOBILE == $this->getPhoneNumberTypeId() && true == is_null( $this->getMessageOperatorId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_operator_id', 'Message Operator is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPhoneNumber() {

        $boolIsValid = true;
        $intLength = strlen( str_replace( '-', '', $this->getPhoneNumber() ) );

	    if( true == is_null( $this->getPhoneNumber() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
			return $boolIsValid;
	    }

		if( 0 == $this->getPhoneNumber() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Invalid phone number.' ) );
			return $boolIsValid;
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number must be between 10 and 15 characters.' ) );
		} elseif( false == CValidation::validateFullPhoneNumber( $this->getPhoneNumber(), false ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Invalid phone number.' ) );
		}

        return $boolIsValid;
    }

    public function checkDuplicatePhoneNumberTypeId( $intUtilityAccountId, $objUtilityDatabase ) {

    	if( true == is_null( $intUtilityAccountId ) || true == is_null( $objUtilityDatabase ) ) {
    		return true;
	    }

    	$boolIsValid = true;
    	$arrobjUtilityAccountPhoneNumbers 	= \Psi\Eos\Utilities\CUtilityAccountPhoneNumbers::createService()->fetchUtilityAccountPhoneNumbersByUtilityAccountId( $intUtilityAccountId, $objUtilityDatabase );
		$arrobjUtilityAccountPhoneNumbers 	= rekeyObjects( 'PhoneNumberTypeId', $arrobjUtilityAccountPhoneNumbers );

	    if( true == valArr( $arrobjUtilityAccountPhoneNumbers ) && true == array_key_exists( $this->m_intPhoneNumberTypeId, $arrobjUtilityAccountPhoneNumbers ) ) {

	    	$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_type_id', 'Phone number type already exists.' ) );
		}

		return $boolIsValid;
    }

    public function validate( $strAction, $objUtilityDatabase = NULL, $intUtilityAccountId = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valPhoneNumberTypeId();
            	$boolIsValid &= $this->valPhoneNumber();
            	$boolIsValid &= $this->valMessageOperatorId();

            	$boolIsValid &= $this->checkDuplicatePhoneNumberTypeId( $intUtilityAccountId, $objUtilityDatabase );
				break;

            case 'validate_insert_update':
            	$boolIsValid &= $this->valPhoneNumberTypeId();
            	$boolIsValid &= $this->valPhoneNumber();
            	$boolIsValid &= $this->valMessageOperatorId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>