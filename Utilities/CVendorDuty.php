<?php

class CVendorDuty extends CBaseVendorDuty {

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valVendorDutyTypeId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>