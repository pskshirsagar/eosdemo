<?php

class CBillEscalateType extends CBaseBillEscalateType {

	const ACCOUNT_NOT_SETUP							= 1;
	const NO_TEMPLATE_FOUND							= 2;
	const NO_CURRENT_CHARGES						= 3;
	const NO_ACCOUNT_NUMBER							= 4;
	const NO_DUE_DATE								= 5;
	const NO_SERVICE_PERIOD							= 7;
	const ALREADY_PROCESSED_FOR_THE_SERVICE_PERIOD	= 8;
	const CONTENT_NOT_READABLE						= 9;
	const OTHER										= 11;
	const UNABLE_TO_ASSOCIATE						= 12;
	const UTILITY_BILLS_NOT_ALLOWED_UNDER_IP		= 13;
	const PO_REQUIRED_PO_NOT_FOUND					= 14;
	const PDF_ENCRYPTED								= 15;
	const HOPPER_NUMBER_NOT_FOUND					= 16;
	const INVOICE_IS_A_STATEMENT					= 17;
	const NO_CONTRACT								= 18;
	const VENDOR_MISMATCH							= 19;
	const PROPERTY_MISMATCH							= 20;
	const POS_ASSOCIATED_TO_MULTIPLE_ACCOUNTS		= 21;
	const DUPLICATE									= 22;
	const NO_BILL_DATE_AND_DUE_DATE					= 23;
	const VENDOR_CODE_NOT_FOUND 					= 24;
	const ACCOUNT_NOT_ASSOCIATED 					= 25;
	const SUMMARY_PARENT_ACCOUNT_NOT_ASSOCIATED		= 26;
	const SUMMARY_CHILD_ACCOUNT_NOT_ASSOCIATED		= 27;
	const COMMODITY_CONFIGURATION 					= 28;
	const MISSING_PAGES								= 29;
	const UTILITY_BILL_PROCESSED_AS_IP				= 30;
	const ACCOUNT_INACTIVE          				= 31;

	public static $c_arrintFDGBillEscalateTypeIds = [
		self::ACCOUNT_NOT_ASSOCIATED,
		self::SUMMARY_PARENT_ACCOUNT_NOT_ASSOCIATED,
		self::SUMMARY_CHILD_ACCOUNT_NOT_ASSOCIATED,
	];

	public static function getBillEscalateTypeDetails( $arrintBillEscalateTypeIds = [] ) {

		$arrmixBillEscalateTypes = [
			self::ACCOUNT_NOT_SETUP                        => 'Account Not Setup',
			self::NO_TEMPLATE_FOUND                        => 'No Template Found',
			self::NO_CURRENT_CHARGES                       => 'No Current Charges',
			self::NO_ACCOUNT_NUMBER                        => 'No Account Number',
			self::ALREADY_PROCESSED_FOR_THE_SERVICE_PERIOD => 'Already Processed For The Service Period',
			self::OTHER                                    => 'Other',
			self::UNABLE_TO_ASSOCIATE                      => 'Unable to Associate',
			self::UTILITY_BILLS_NOT_ALLOWED_UNDER_IP       => 'Utility bills not allowed under IP',
			self::PDF_ENCRYPTED                            => '.pdf Encrypted',
			self::HOPPER_NUMBER_NOT_FOUND                  => 'Hopper Number not found',
			self::INVOICE_IS_A_STATEMENT                   => 'Invoice is a statement',
			self::NO_CONTRACT                              => 'No contract',
			self::VENDOR_MISMATCH                          => 'Vendor Mismatch',
			self::PROPERTY_MISMATCH                        => 'Property Mismatch',
			self::POS_ASSOCIATED_TO_MULTIPLE_ACCOUNTS      => 'Po\'s are for multiple accounts so we are unable to associate to PO.',
			self::CONTENT_NOT_READABLE                     => 'Content Not Readable',
			self::NO_DUE_DATE                              => 'No Due Date',
			self::NO_SERVICE_PERIOD                        => 'No Service Period',
			self::DUPLICATE                                => 'Duplicate',
			self::PO_REQUIRED_PO_NOT_FOUND                 => 'PO required, PO not found.',
			self::ACCOUNT_NOT_ASSOCIATED                   => 'Account Not Associated',
			self::SUMMARY_PARENT_ACCOUNT_NOT_ASSOCIATED    => 'Summary Parent Account Not Associated',
			self::SUMMARY_CHILD_ACCOUNT_NOT_ASSOCIATED     => 'Summary Child Account Not Associated',
			self::COMMODITY_CONFIGURATION                  => 'Commodity Configuration',
			self::ACCOUNT_INACTIVE                         => 'Account Inactive'
		];

		if( true == valArr( $arrintBillEscalateTypeIds ) ) {

			foreach( $arrmixBillEscalateTypes as $intId => $arrmixBillEscalateType ) {
				if( false == in_array( $intId, $arrintBillEscalateTypeIds ) ) {
					unset( $arrmixBillEscalateTypes[$intId] );
				}
			}
		}

		return $arrmixBillEscalateTypes;
	}

	public static function getBillEscalateTypeName( $intBillEscalateTypeId ) {

		$arrmixBillEscalateTypes = [
			self::ACCOUNT_NOT_SETUP                        => 'Account Not Setup',
			self::NO_TEMPLATE_FOUND                        => 'No Template Found',
			self::NO_CURRENT_CHARGES                       => 'No Current Charges',
			self::NO_ACCOUNT_NUMBER                        => 'No Account Number',
			self::ALREADY_PROCESSED_FOR_THE_SERVICE_PERIOD => 'Already Processed For The Service Period',
			self::OTHER                                    => 'Other',
			self::UNABLE_TO_ASSOCIATE                      => 'Unable to Associate',
			self::UTILITY_BILLS_NOT_ALLOWED_UNDER_IP       => 'Utility bills not allowed under IP',
			self::PDF_ENCRYPTED                            => '.pdf Encrypted',
			self::HOPPER_NUMBER_NOT_FOUND                  => 'Hopper Number not found',
			self::INVOICE_IS_A_STATEMENT                   => 'Invoice is a statement',
			self::NO_CONTRACT                              => 'No contract',
			self::VENDOR_MISMATCH                          => 'Vendor Mismatch',
			self::PROPERTY_MISMATCH                        => 'Property Mismatch',
			self::POS_ASSOCIATED_TO_MULTIPLE_ACCOUNTS      => 'Po\'s are for multiple accounts so we are unable to associate to PO.',
			self::CONTENT_NOT_READABLE                     => 'Content Not Readable',
			self::NO_DUE_DATE                              => 'No Due Date',
			self::DUPLICATE                                => 'Duplicate',
			self::PO_REQUIRED_PO_NOT_FOUND                 => 'PO required, PO not found.',
			self::ACCOUNT_NOT_ASSOCIATED                   => 'Account Not Associated',
			self::SUMMARY_PARENT_ACCOUNT_NOT_ASSOCIATED    => 'Summary Parent Account Not Associated',
			self::SUMMARY_CHILD_ACCOUNT_NOT_ASSOCIATED     => 'Summary Child Account Not Associated',
			self::NO_BILL_DATE_AND_DUE_DATE                => 'No Bill Date or Due Date',
			self::VENDOR_CODE_NOT_FOUND                    => 'Vendor code not found on provider',
			self::NO_SERVICE_PERIOD                        => 'No Service Period',
			self::COMMODITY_CONFIGURATION                  => 'Commodity Configuration',
            self::MISSING_PAGES                            => 'Missing Pages',
			self::UTILITY_BILL_PROCESSED_AS_IP             => 'Utility bill processed as IP',
			self::ACCOUNT_INACTIVE                         => 'Account Inactive'
		];

		return getArrayElementByKey( $intBillEscalateTypeId, $arrmixBillEscalateTypes );
	}

}
?>