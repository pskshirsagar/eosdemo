<?php

class CBillingAllocationProperty extends CBaseBillingAllocationProperty {

	protected $m_arrintUtilityTypeIds;

	public function getUtilityTypeIds() {
		return $this->m_arrintUtilityTypeIds;
	}

	public function setUtilityTypeIds( $arrintUtilityTypeIds ) {
		$this->set( 'm_arrintUtilityTypeIds', CStrings::strToArrIntDef( $arrintUtilityTypeIds, NULL ) );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityTypeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillingAllocationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllocationPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/**
	 * Validate Functions
	 */

	public function valUtilityTypeAllocation( $arrintPropertyIds, $objUtilitiesDatabase, $objAdminDatabase ) {

		$boolIsValidAllocation = $boolIsValidUtilityType = true;
		$arrmixUtilityTypes    = $arrstrUtilityTypes = $arrstrUtilityTypeAllocations = $arrstrPropertyNames = $arrstrPropertyNameAllocations = $arrstrAllocationNames = $arrobjPropertyUtilityTypes = [];

		$arrmixProperies = rekeyArray( 'property_id', \Psi\Eos\Admin\CContractProperties::createService()->fetchActiveAndTemporaryContractPropertiesByPsProductIds( [ CPsProduct::RESIDENT_UTILITY ], $objAdminDatabase, [ $this->getCid() ], [] ) );

		$arrobjAllPropertyUtilityTypes = \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchAllActivePropertyUtilityTypesByPropertyIds( array_keys( $arrmixProperies ), $objUtilitiesDatabase );

		foreach( $arrobjAllPropertyUtilityTypes as $objPropertyUtilityType ) {
			$arrobjPropertyUtilityTypes[$objPropertyUtilityType->getUtilityTypeId()] = $objPropertyUtilityType;
			if( true == in_array( $objPropertyUtilityType->getPropertyId(), $arrintPropertyIds ) ) {
				$arrmixUtilityTypes[$objPropertyUtilityType->getPropertyId()][$objPropertyUtilityType->getUtilityTypeName()] = $objPropertyUtilityType->getUtilityTypeId();
			}
		}

		$arrmixBillingAllocationProperties = \Psi\Eos\Utilities\CBillingAllocationProperties::createService()->fetchBillingAllocationPropertiesByCidByPropertyIdsByUtilityTypeIds( $this->getCid(), $arrintPropertyIds, $this->getUtilityTypeIds(), $objUtilitiesDatabase );

		foreach( $arrintPropertyIds as $intPropertyId ) {
			foreach( $this->getUtilityTypeIds() as $intUtilityTypeId ) {
				// Here validate the utility type.
				if( false == array_key_exists( $intPropertyId, $arrmixUtilityTypes ) || false == in_array( $intUtilityTypeId, $arrmixUtilityTypes[$intPropertyId] ) ) {
					$boolIsValidUtilityType                                                                 = false;
					$arrstrUtilityTypes[$arrobjPropertyUtilityTypes[$intUtilityTypeId]->getUtilityTypeName()] = $arrobjPropertyUtilityTypes[$intUtilityTypeId]->getUtilityTypeName();
					$arrstrPropertyNames[$arrmixProperies[$intPropertyId]['property_name']]                  = $arrmixProperies[$intPropertyId]['property_name'];
				}
				// Here validate the utility type with allocation.
				foreach( $arrmixBillingAllocationProperties as $arrmixBillingAllocationProperty ) {
					if( $this->getBillingAllocationId() != $arrmixBillingAllocationProperty['id'] && $intUtilityTypeId == $arrmixBillingAllocationProperty['utility_type_id'] ) {
						$boolIsValidAllocation                                                                                              = false;
						$arrstrUtilityTypeAllocations[$arrobjPropertyUtilityTypes[$intUtilityTypeId]->getUtilityTypeName()]                  = $arrobjPropertyUtilityTypes[$intUtilityTypeId]->getUtilityTypeName();
						$arrstrPropertyNameAllocations[$arrmixProperies[$arrmixBillingAllocationProperty['property_id']]['property_name']] = $arrmixProperies[$arrmixBillingAllocationProperty['property_id']]['property_name'];
						$arrstrAllocationNames[$arrmixBillingAllocationProperty['name']]                                                   = $arrmixBillingAllocationProperty['name'];
					}
				}
			}
		}

		if( false == $boolIsValidAllocation ) {
			$strMessage = 'Cannot select this utility ' . implode( ', ', $arrstrUtilityTypeAllocations ) .
			              ' as it is associated to allocation ' . implode( ', ', $arrstrAllocationNames ) . ' for ' . implode( ', ', $arrstrPropertyNameAllocations ) . '.';

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_type', $strMessage ) );
		}

		if( false == $boolIsValidUtilityType ) {
			$strMessage = 'Cannot select this utility ' . implode( ', ', $arrstrUtilityTypes ) . ' as it is missing on ' . implode( ', ', $arrstrPropertyNames ) . '.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_type_allocation', $strMessage ) );
		}

		return ( $boolIsValidUtilityType && $boolIsValidAllocation );
	}

	public function validate( $strAction, $arrintPropertyIds, $objUtilitiesDatabase, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			$boolIsValid &= $this->valUtilityTypeAllocation( $arrintPropertyIds, $objUtilitiesDatabase, $objAdminDatabase );
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>