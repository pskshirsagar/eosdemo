<?php

class CBillEscalateStep extends CBaseBillEscalateStep {

	const VERIFY_DATA                         = 1;
	const SEND_BACK_TO_QUEUE                  = 2;
	const SETUP_ACCOUNT                       = 3;
	const SETUP_TEMPLATE                      = 4;
	const ADD_NOTE                            = 5;
	const EMAIL_INVOICE_TO_CLIENT             = 6;
	const DELETE_INVOICE                      = 7;
	const PROCESS_INVOICE                     = 8;
	const ASSOCIATE_INVOICE                   = 9;
	const SEND_TO_INCOMPLETE_INVOICES         = 10;
	const NOT_A_DUPLICATE                     = 11;
	const UPDATE_VENDOR_CODE                  = 12;
	const ASSOCIATE_AND_UPDATE_ACCOUNT_NUMBER = 13;
	const ACCOUNT_INACTIVE                    = 14;
	const ADD_ACCOUNT                         = 15;
	const COMMODITY_CONFIGURATION     = 16;
	const SEND_BACK_TO_THIRD_PARTY            = 17;

	public static function getBillEscalateSteps() {

		return [
			self::VERIFY_DATA					=> 'Verify Data',
			self::SEND_BACK_TO_QUEUE			=> 'Send Back To Queue',
			self::SETUP_ACCOUNT					=> 'Setup Account',
			self::SETUP_TEMPLATE				=> 'Setup Template',
			self::ADD_NOTE						=> 'Add Note',
			self::EMAIL_INVOICE_TO_CLIENT		=> 'Email Invoice To Client',
			self::DELETE_INVOICE				=> 'Delete Invoice',
			self::PROCESS_INVOICE				=> 'Process Invoice',
			self::ASSOCIATE_INVOICE				=> 'Associate Invoice',
			self::SEND_TO_INCOMPLETE_INVOICES	=> 'Send To Incomplete Invoices',
			self::NOT_A_DUPLICATE	            => 'Not A Duplicate Invoice',
			self::SEND_BACK_TO_THIRD_PARTY	    => 'Send Back To Third Party'
		];
	}

}
?>