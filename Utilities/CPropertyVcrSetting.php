<?php

class CPropertyVcrSetting extends CBasePropertyVcrSetting {

	protected $m_arrobjPropertyVcrTypeRates;
	protected $m_intUtilityBillAccountDetailId;

	const PUT_VCR_TOTAL_VALIDATION_COUNT = 6;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjPropertyVcrTypeRates = [];
	}

	/**
	 * Set Functions
	 */

	public function setPropertyVcrTypeRates( $arrobjPropertyVcrTypeRates ) {
		$this->m_arrobjPropertyVcrTypeRates = $arrobjPropertyVcrTypeRates;
	}

	public function setUtilityBillAccountDetailId( $intUtilityBillAccountDetailId ) {
		$this->set( 'm_intUtilityBillAccountDetailId', CStrings::strToIntDef( $intUtilityBillAccountDetailId, NULL, false ) );
	}

	/**
	 * Get Functions
	 */

	public function getPropertyVcrTypeRates() {
		return $this->m_arrobjPropertyVcrTypeRates;
	}

	public function getUtilityBillAccountDetailId() {
		return $this->m_intUtilityBillAccountDetailId;
	}

	 /**
	 * Create Functions
	 */

	public function createUtilityBillAccountDetail() {

		$objUtilityBillAccountDetail = new CUtilityBillAccountDetail();
		$objUtilityBillAccountDetail->setCid( $this->getCid() );
		$objUtilityBillAccountDetail->setPropertyId( $this->getPropertyId() );

		return $objUtilityBillAccountDetail;
	}

	public function createPropertyVcrTypeRate() {
		$objPropertyVcrTypeRate = new CPropertyVcrTypeRate();

		$objPropertyVcrTypeRate->setCid( $this->getCid() );
		$objPropertyVcrTypeRate->setPropertyId( $this->getPropertyId() );
		$objPropertyVcrTypeRate->setPropertyVcrSettingId( $this->getId() );
		$objPropertyVcrTypeRate->setFirstFixedServiceFee( '50.00' );
		$objPropertyVcrTypeRate->setFirstVcrServiceFeeTypeId( CVcrServiceFeeType::FIXED_AMOUNT );
		$objPropertyVcrTypeRate->setUsageWaiverAmount( '5.00' );
		$objPropertyVcrTypeRate->setGraceDays( '1' );
		$objPropertyVcrTypeRate->setRapidLeaseDefinitionDays( '1' );
		$objPropertyVcrTypeRate->setWaiveUsageDuringGraceDays( false );

		return $objPropertyVcrTypeRate;
	}

	public function valPropertyUtilityTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyUtilityTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_utility_type_id', 'Utility type is required.' ) );
        }

        return $boolIsValid;
 	}

    public function valUsageArCodeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUsageArCodeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'usage_ar_code_id', 'Charge code for usage is required.' ) );
        }

        return $boolIsValid;
    }

    public function valServiceFeeArCodeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getServiceFeeArCodeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_fee_ar_code_id', 'Charge code for service fee is required.' ) );
        }

        return $boolIsValid;
    }

    public function valUtilityBillAccountDetailId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityBillAccountDetailId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_account_detail_id', 'Utility bill account detail is required.' ) );
        }

        return $boolIsValid;
    }

    public function valUtilityTemplateId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityTemplateId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_template_id', 'Utility template is required.' ) );
        }

        return $boolIsValid;
    }

    public function valVacantUnitBillAlertAmount( $boolIsNullable = false ) {
        $boolIsValid = true;

        if( true == $boolIsNullable && true == is_null( $this->getVacantUnitBillAlertAmount() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vacant_unit_bill_alert_amount', 'Vacant unit bill alert amount is required.' ) );
        }
        if( false == is_null( $this->getVacantUnitBillAlertAmount() ) && ( 0 == $this->getVacantUnitBillAlertAmount() || false == preg_match( '/^[0-9.]*$/', $this->getVacantUnitBillAlertAmount() ) ) ) {
			$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vacant_unit_bill_alert_amount', 'Invalid vacant unit bill alert amount.' ) );
        }

        return $boolIsValid;
    }

	public function valPropertyVcrTypeRatesByLeaseDates() {
		$boolIsValid = true;

		if( true == valArr( $this->getPropertyVcrTypeRates() ) ) {

			$arrobjPropertyVcrTypeRates = $this->getPropertyVcrTypeRates();

			$intCount = \Psi\Libraries\UtilFunctions\count( $arrobjPropertyVcrTypeRates );

			if( 1 == $intCount ) {

				if( false == is_null( $arrobjPropertyVcrTypeRates[0]->getEndDate() ) && strtotime( $arrobjPropertyVcrTypeRates[0]->getStartDate() ) > strtotime( $arrobjPropertyVcrTypeRates[0]->getEndDate() ) ) {
					$arrobjPropertyVcrTypeRates[0]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date must be smaller than end date.' ) );
					$boolIsValid = false;
				}

				if( false == is_null( $arrobjPropertyVcrTypeRates[0]->getEndDate() ) ) {
					$arrobjPropertyVcrTypeRates[0]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date_criteria', 'End date must be empty.' ) );
					$boolIsValid = false;
				}
			} else {

				for( $intFirstLoop = 0; $intFirstLoop < $intCount; $intFirstLoop++ ) {

					if( 0 == $intFirstLoop && false == is_null( $arrobjPropertyVcrTypeRates[$intFirstLoop]->getEndDate() ) && strtotime( $arrobjPropertyVcrTypeRates[$intFirstLoop]->getStartDate() ) > strtotime( $arrobjPropertyVcrTypeRates[$intFirstLoop]->getEndDate() ) ) {
						$arrobjPropertyVcrTypeRates[$intFirstLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date must be smaller than end date.' ) );
						$boolIsValid = false;
						break 1;
					}

					for( $intSecondLoop = $intFirstLoop + 1; $intSecondLoop < $intCount; $intSecondLoop++ ) {

						if( false == is_null( $arrobjPropertyVcrTypeRates[$intSecondLoop]->getEndDate() ) && strtotime( $arrobjPropertyVcrTypeRates[$intSecondLoop]->getStartDate() ) > strtotime( $arrobjPropertyVcrTypeRates[$intSecondLoop]->getEndDate() ) ) {
							$arrobjPropertyVcrTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date must be smaller than end date.' ) );
							$boolIsValid = false;
							break 2;
						}

						if( $intSecondLoop == ( \Psi\Libraries\UtilFunctions\count( $arrobjPropertyVcrTypeRates ) - 1 ) && false == is_null( $arrobjPropertyVcrTypeRates[$intSecondLoop]->getEndDate() ) ) {
							$arrobjPropertyVcrTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Last records end date must be null' ) );
							$boolIsValid = false;
							break 2;
						}

						if( true == is_null( $arrobjPropertyVcrTypeRates[$intFirstLoop]->getEndDate() ) && true == is_null( $arrobjPropertyVcrTypeRates[$intSecondLoop]->getStartDate() ) ) {
							$arrobjPropertyVcrTypeRates[$intFirstLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Enter end date.' ) );
							$boolIsValid = false;
							break 2;
						}

						if( false == is_null( $arrobjPropertyVcrTypeRates[$intSecondLoop]->getStartDate() ) && strtotime( $arrobjPropertyVcrTypeRates[$intFirstLoop]->getStartDate() ) > strtotime( $arrobjPropertyVcrTypeRates[$intSecondLoop]->getStartDate() ) ) {
							$arrobjPropertyVcrTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Start date must be greater the previous start date.' ) );
							$boolIsValid = false;
							break 2;
						} elseif( true == is_null( $arrobjPropertyVcrTypeRates[$intSecondLoop]->getStartDate() ) ) {
							$arrobjPropertyVcrTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Enter start date.' ) );
							$boolIsValid = false;
							break 2;
						}

						if( strtotime( $arrobjPropertyVcrTypeRates[$intFirstLoop]->getEndDate() ) >= strtotime( $arrobjPropertyVcrTypeRates[$intSecondLoop]->getStartDate() ) ) {
							$arrobjPropertyVcrTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Start date must be greater than previous end date.' ) );
							$boolIsValid = false;
							break 2;
						}

						// Check the LED and next LSD Difference. If its more than 1 day then throw the error.

						if( 0 < $intSecondLoop && false == is_null( $arrobjPropertyVcrTypeRates[$intFirstLoop]->getEndDate() ) && ( strtotime( $arrobjPropertyVcrTypeRates[$intSecondLoop]->getStartDate() ) - strtotime( $arrobjPropertyVcrTypeRates[$intFirstLoop]->getEndDate() ) ) > 86400 ) {
							$arrobjPropertyVcrTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'There should not be a single day gap between this start date and previous end date.' ) );
							$boolIsValid = false;
							break 2;
						} elseif( true == is_null( $arrobjPropertyVcrTypeRates[$intFirstLoop]->getEndDate() ) ) {
							$arrobjPropertyVcrTypeRates[$intFirstLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Enter end date.' ) );
							$boolIsValid = false;
							break 2;
						}

						$intFirstLoop++;

					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valIsImmediatePost( $objUtilitiesDatabase ) {

		$boolIsValid = true;

		$objPropertyUtilityType = \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchPropertyUtilityTypeByIdByPropertyIdByCid( $this->getPropertyUtilityTypeId(), $this->getPropertyId(), $this->getCid(), $objUtilitiesDatabase );
		$objUtilityPropertySetting = \Psi\Eos\Utilities\CUtilityPropertySettings::createService()->fetchUtilityPropertySettingByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );

		if( false == valObj( $objPropertyUtilityType, 'CPropertyUtilityType' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Property utility type failed to load.' ) );
		} elseif( true == valObj( $objUtilityPropertySetting, 'CUtilityPropertySetting' ) && true == $objUtilityPropertySetting->getIsVcrOnly() && false == $objPropertyUtilityType->getHasBillingService() && true == $objPropertyUtilityType->getHasVcrService() && false == $this->getIsImmediatePost() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_immediate_post', 'This property is configured for VCR Only and Immediate VCR Posting is required to be turned on. Please activate immediate vcr posting.' ) );
		}

		return $boolIsValid;
	}

    public function validate( $strAction, $objUtilitiesDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyUtilityTypeId();
				$boolIsValid &= $this->valUsageArCodeId();
				$boolIsValid &= $this->valServiceFeeArCodeId();
				$boolIsValid &= $this->valUtilityTemplateId();
				$boolIsValid &= $this->valVacantUnitBillAlertAmount( $boolIsNullable = true );
				$boolIsValid &= $this->valIsImmediatePost( $objUtilitiesDatabase );
				break;

			case 'validate_property_vcr_setting_in_utility_batch':
				$boolIsValid &= $this->valVacantUnitBillAlertAmount( $boolIsNullable = true );
				break;

	        case 'validate_property_vcr_type_rates':
		        $boolIsValid &= $this->valPropertyVcrTypeRatesByLeaseDates();
		        break;

			case 'validate_vcr_bill':
				$boolIsValid &= $this->valIsImmediatePost( $objUtilitiesDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
        }

        return $boolIsValid;
    }

    /**
    * Fetch Functions
    */

	public function fetchUtilityBillAccountDetail( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CUtilityBillAccountDetails::createService()->fetchUtilityBillAccountDetailById( $this->getUtilityBillAccountDetailId(), $objUtilitiesDatabase );
	}

	public function fetchUtilityBillAccountDetailById( $intUtilityBillAccountDetailId, $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CUtilityBillAccountDetails::createService()->fetchUtilityBillAccountDetailById( $intUtilityBillAccountDetailId, $objUtilitiesDatabase );
	}

	public function fetchActivePropertyVcrTypeRates( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyVcrTypeRates::createService()->fetchActivePropertyVcrTypeRatesByPropertyVcrSettingId( $this->getId(), $objUtilitiesDatabase );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$arrstrTempOriginalValues 	 = [];
		$arrstrOriginalValues 		 = [];
		$arrstrChangedOriginalValues = [];
		$boolIsChangedValue			 = false;

		$arrmixOriginalValues = fetchData( 'SELECT * FROM property_vcr_settings WHERE id = ' . ( int ) $this->getId(), $objDatabase );

		$arrstrTempOriginalValues = $arrmixOriginalValues[0];

		foreach( $arrstrTempOriginalValues as $strKey => $strMixOriginalValue ) {

			if( 'utility_bill_account_detail_id' == $strKey ) {
				continue;
			}

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			if( CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $strMixOriginalValue ) {
				$arrstrChangedOriginalValues[$strKey] 	= $this->$strFunctionName();
				$arrstrOriginalValues[$strKey] 			= $strMixOriginalValue;
				$boolIsChangedValue 					= true;
			}
		}

		$boolUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUpdateStatus ) {

			if( false != $boolIsChangedValue ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'property_vcr_settings' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrChangedOriginalValues ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}

			return $boolUpdateStatus;
		}

		return false;
	}

}
?>