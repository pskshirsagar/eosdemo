<?php

class CUtilityBillsAwsDetail extends CBaseUtilityBillsAwsDetail {

	protected $m_intCid;
	protected $m_intPropertyId;

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['cid'] ) ) {
			$this->setCid( $arrmixValues['cid'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	/**
	 * Get Functions
	 */

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>