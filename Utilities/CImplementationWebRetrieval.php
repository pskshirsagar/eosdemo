<?php

class CImplementationWebRetrieval extends CBaseImplementationWebRetrieval {

	public function valUsername() {
		$boolIsValid = true;
		if( true == is_null( $this->getUsername() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPasswordEncrypted() {
		$boolIsValid = true;
		if( true == is_null( $this->getPasswordEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_encrypted', 'Password is required.' ) );
		}
		return $boolIsValid;
	}

	public function valRetrievalInstructions() {
        $boolIsValid = true;
        if( true == is_null( $this->getRetrievalInstructions() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retrieval_instructions', 'Instruction is required.' ) );
        }

        return $boolIsValid;
    }

	public function valUrl() {
		$boolIsValid = true;
		if( true == is_null( $this->getURL() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'URL is required.' ) );
		} elseif( false == filter_var( $this->getUrl(), FILTER_VALIDATE_URL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Please enter valid URL.' ) );
		}
		return $boolIsValid;
	}

	public function valUtilityCredentialId() {

		$boolIsValid = true;
		if( true == is_null( $this->getUtilityCredentialId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_credential_id', 'Utility credential is required.' ) );

		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUtilityCredentialId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>