<?php

class CMeterManufacturer extends CBaseMeterManufacturer {

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUrl() {
		$boolIsValid = true;

		if( false == is_null( $this->getUrl() ) && false == CValidation::checkUrl( $this->getUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Invalid website.' ) );
		}

		return $boolIsValid;
	}

	public function valUniqueName( $objDatabase ) {

		$boolIsValid = true;

		$strWhereCondition = ' WHERE name = \'' . addslashes( $this->getName() ) . '\'';

		if( false == is_null( $this->getId() ) ) {
			$strWhereCondition .= ' AND id != ' . ( int ) $this->getId();
		}

		$intCount = \Psi\Eos\Utilities\CMeterManufacturers::createService()->fetchMeterManufacturerCount( $strWhereCondition, $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  ' Entered name is already used.' ) );
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		$intMeterPartTypeCount		 = \Psi\Eos\Utilities\CMeterPartTypes::createService()->fetchMeterPartTypeCount( 'WHERE meter_manufacturer_id = ' . ( int ) $this->getId(), $objDatabase );
		$intPropertyMeterDeviceCount = \Psi\Eos\Utilities\CPropertyMeterDevices::createService()->fetchPropertyMeterDeviceCount( 'WHERE meter_manufacturer_id = ' . ( int ) $this->getId(), $objDatabase );

		if( 0 < $intMeterPartTypeCount || 0 < $intPropertyMeterDeviceCount ) {
			$boolIsValid = false;
			if( 0 < $intMeterPartTypeCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  $this->m_strName . ' Meter manufacturer cannot be removed because meter part type(s) depends on it.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  $this->m_strName . ' Meter manufacturer cannot be removed because property meter device(s) depends on it.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase=NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valUrl();
				$boolIsValid &= $this->valUniqueName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchMeterPartTypes( $objDatabase ) {
		return \Psi\Eos\Utilities\CMeterPartTypes::createService()->fetchMeterPartTypesByMeterManufacturerId( $this->getId(), $objDatabase );
	}

	public function fetchPropertyMeterDevices( $objDatabase ) {
		return \Psi\Eos\Utilities\CPropertyMeterDevices::createService()->fetchPropertyMeterDevicesByMeterManufacturerId( $this->getId(), $objDatabase );
	}

}
?>