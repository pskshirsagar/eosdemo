<?php

class CUtilityBillType extends CBaseUtilityBillType {

	const SAMPLE 					= 1;
	const ESTIMATE 					= 2;
	const VACANT_UNIT 				= 3;
	const ACTUAL 					= 4;
	const DUMMY_BILL				= 5;
	const DIRECT_METERED			= 6;
	const SUMMARY					= 7;
	const NO_CHARGE_BILL			= 8;
	const ESTIMATED_DIRECT_METERED	= 9;
	const REVIEW_BILL	= 10;

	const TEXT_ACTUAL			= 'actual';
	const TEXT_DIRECT_METERED	= 'direct_metered';
	const TEXT_VACANT_UNIT		= 'vacant_unit';
	const TEXT_SUMMARY			= 'summary';

	public static $c_arrintNonProcessingBillTypes = [
		self::SAMPLE,
		self::DUMMY_BILL,
		self::REVIEW_BILL,
		self::SUMMARY
	];

	public static function getPublishedUtilityBillTypes( $arrintExcludeUtilityBillTypeIds = [] ) {

		$arrmixUtilityBillTypes = [
			self::SAMPLE					=> 'Sample',
			self::ESTIMATE					=> 'Estimate',
			self::VACANT_UNIT				=> 'Vacant Unit',
			self::ACTUAL					=> 'Actual',
			self::DUMMY_BILL				=> 'Dummy Bill',
			self::DIRECT_METERED			=> 'Direct Metered',
			self::SUMMARY					=> 'Summary',
			self::NO_CHARGE_BILL			=> 'No Charge Bill',
			self::ESTIMATED_DIRECT_METERED	=> 'Estimated Direct Metered',
			self::REVIEW_BILL				=> 'Review Bill'
		];

		foreach( $arrintExcludeUtilityBillTypeIds as $intExcludeUtilityBillTypeId ) {
			unset( $arrmixUtilityBillTypes[$intExcludeUtilityBillTypeId] );
		}

		return $arrmixUtilityBillTypes;
	}

	public static $c_arrintProcessingBillTypes = [
		self::VACANT_UNIT,
		self::ACTUAL,
		self::DIRECT_METERED,
		self::SUMMARY
	];

	public static $c_arrstrUnitWiseBillAccountTypes = [
		self::DIRECT_METERED,
		self::VACANT_UNIT
	];

	public function getUtilityBillTypeId( $strUtilityBillType ) {

		$intUtilityBillTypeId = NULL;
		switch( $strUtilityBillType ) {

			case self::TEXT_ACTUAL:
				$intUtilityBillTypeId = self::ACTUAL;
				break;

			case self::TEXT_DIRECT_METERED:
				$intUtilityBillTypeId = self::DIRECT_METERED;
				break;

			case self::TEXT_VACANT_UNIT:
				$intUtilityBillTypeId = self::VACANT_UNIT;
				break;

			case self::TEXT_SUMMARY:
				$intUtilityBillTypeId = self::SUMMARY;
				break;

			default:
				// default case
				break;
		}

		return $intUtilityBillTypeId;
	}

	public static function getUtilityBillType( $intUtilityBillTypeId ) {

		$strUtilityBillType = NULL;
		switch( $intUtilityBillTypeId ) {

			case self::ACTUAL:
				$strUtilityBillType = self::TEXT_ACTUAL;
				break;

			case self::DIRECT_METERED:
				$strUtilityBillType = self::TEXT_DIRECT_METERED;
				break;

			case self::VACANT_UNIT:
				$strUtilityBillType = self::TEXT_VACANT_UNIT;
				break;

			case self::SUMMARY:
				$strUtilityBillType = self::TEXT_SUMMARY;
				break;

			default:
				// default case
				break;
		}

		return $strUtilityBillType;
	}

	public static function getPublishedUtilityBillTypesTitle() {

		$arrmixUtilityBillTypes = [
			self::VACANT_UNIT				=> 'Vacant Unit',
			self::ACTUAL					=> 'Property',
			self::DIRECT_METERED			=> 'Unit',
			self::SUMMARY					=> 'Summary'
		];

		return $arrmixUtilityBillTypes;
	}

	public static $c_arrintDisabledBillAccountTypes = [
		self::ACTUAL,
		self::SUMMARY
	];

	public static $c_arrstrSmartBillAccountTypeIds = [
		self::ACTUAL,
		self::DIRECT_METERED,
		self::VACANT_UNIT,
		self::SUMMARY
	];

	public static $c_arrintUemValidateBillTypeIds = [
		self::ACTUAL,
		self::SUMMARY,
		self::DIRECT_METERED
	];

	public static $c_arrintExcludeUtilityBillTypeIds = [
		self::SAMPLE,
		self::DUMMY_BILL,
		self::REVIEW_BILL
	];

	public static $c_arrintFDGDefaultUtilityBillTypeIds = [
		self::ACTUAL,
		self::DIRECT_METERED,
		self::VACANT_UNIT
	];

}
?>
