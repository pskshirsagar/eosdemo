<?php

class CWebRetrieval extends CBaseWebRetrieval {

	protected $m_strWebRetrievalEscalationType;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['name'] ) ) {
			$this->setWebRetrievalEscalationType( $arrmixValues['name'] );
		}
	}

	public function setWebRetrievalEscalationType( $strWebRetrievalEscalationType ) {
		$this->m_strWebRetrievalEscalationType = $strWebRetrievalEscalationType;
	}

	public function getWebRetrievalEscalationType() {
		return $this->m_strWebRetrievalEscalationType;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objDatabase );
	}

}
?>