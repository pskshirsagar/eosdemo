<?php

class CVendorCategory extends CBaseVendorCategory {

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valVendorCategoryId() {
		return true;
	}

	public function valVendorCategoryTypeId( $objUtilitiesDatabase ) {

		$boolIsValid = true;

		if( 0 == $this->m_intVendorCategoryTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_category_type_id', 'Service(s) is required.' ) );
		} else {
			$objVendorCategoryType = \Psi\Eos\Utilities\CVendorCategoryTypes::createService()->fetchVendorCategoryTypeById( $this->m_intVendorCategoryTypeId,  $objUtilitiesDatabase );

			if( false == valObj( $objVendorCategoryType, 'CVendorCategoryType' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_category_type_id', 'Invalid services is provided.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valVendorCategoryTypeId( $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>