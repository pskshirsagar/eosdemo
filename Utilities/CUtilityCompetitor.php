<?php

class CUtilityCompetitor extends CBaseUtilityCompetitor {

	public function valName() {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Competitor Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valName();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>