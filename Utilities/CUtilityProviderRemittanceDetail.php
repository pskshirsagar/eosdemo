<?php

class CUtilityProviderRemittanceDetail extends CBaseUtilityProviderRemittanceDetail {

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    /**
     * Get Functions
     */

    public function getCheckAccountNumberEncrypted() {
    	if( true == is_null( $this->m_strCheckAccountNumberEncrypted ) ) {
    		return false;
	    }
    	return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
    }

    /**
     * Set Functions
     */

    public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {

    	if( true == is_null( $strCheckAccountNumberEncrypted ) || 0 == \Psi\CStringService::singleton()->strlen( $strCheckAccountNumberEncrypted ) ) {
    		return NULL;
	    }

    	$strCheckAccountNumberEncrypted = trim( $strCheckAccountNumberEncrypted );
    	if( true == \Psi\CStringService::singleton()->stristr( $strCheckAccountNumberEncrypted, 'X' ) || true == \Psi\CStringService::singleton()->stristr( $strCheckAccountNumberEncrypted, '+' ) || true == \Psi\CStringService::singleton()->stristr( $strCheckAccountNumberEncrypted, '=' ) ) {
    		$this->m_strCheckAccountNumberEncrypted = $strCheckAccountNumberEncrypted;
    		return NULL;
    	}
    	$strCheckAccountNumberEncrypted = CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 20, NULL, true );
    	$this->m_strCheckAccountNumberEncrypted = \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER );
    }

}
?>