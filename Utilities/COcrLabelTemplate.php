<?php

class COcrLabelTemplate extends CBaseOcrLabelTemplate {

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Please select company' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valLabelKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getLabelKey() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Please select Key' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valLabelValues() {
		$boolIsValid = true;

		if( true == is_null( $this->getLabelKey() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Please insert values' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLabelKey();
				$boolIsValid &= $this->valLabelValues();
				$boolIsValid &= $this->checkUniqueLabelTemplate( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	private function checkUniqueLabelTemplate( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getUtilityBillAccountId() ) ) {
			return $boolIsValid;
		}

		$strWhere = '';
		if( false == is_null( $this->getId() ) ) {
			$strWhere = ' AND id != ' . $this->getId();
		}

		$strSql = 'SELECT
						count(id)
					FROM
						ocr_label_templates
					WHERE
						cid = ' . $this->getCid() . '
						AND utility_bill_account_id = ' . $this->getUtilityBillAccountId() . '
		                AND label_key = \'' . $this->getLabelKey() . '\''
						 . $strWhere;
		$arrintResultCount = fetchData( $strSql, $objDatabase );

		if( 1 <= current( $arrintResultCount )['count'] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'This key already exists for this client or account please update the existing record.' ) );
			$boolIsValid = false;
		}

		return  $boolIsValid;
	}

}
?>