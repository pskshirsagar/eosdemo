<?php

class CComplianceJobItemDoc extends CBaseComplianceJobItemDoc {

	public function valId() {
		return true;
	}

	public function valComplianceJobItemId() {
		return true;
	}

	public function valComplianceDocId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>