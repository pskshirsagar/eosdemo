<?php

class CUtilityCredential extends CBaseUtilityCredential {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUrl() {

		$boolIsValid = true;

		if( true == is_null( $this->getUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Url required.' ) );
		} elseif( false == is_null( $this->getUrl() ) && false == CValidation::checkUrl( $this->getUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Invalid url.' ) );
		}

		return $boolIsValid;
	}

	public function valUsername() {

		$boolIsValid = true;
		if( true == is_null( $this->getUsername() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username required.' ) );

		}

		return $boolIsValid;
	}

	public function valPassword() {

		$boolIsValid = true;

		if( true == is_null( $this->getPassword() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_encrypted', 'Password required.' ) );
		}

		return $boolIsValid;
	}

	public function valInstructions() {

		$boolIsValid = true;

		if( true == is_null( $this->getInstructions() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'web_retrieval_instructions', 'Instructions are required .' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valUrl();
				$boolIsValid &= $this->valUsername();
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valInstructions();
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setPassword( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			$this->setPasswordEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPassword, CONFIG_SODIUM_KEY_BLACKLEDGER ) );
		}
	}

	public function getPassword() {
		if( true == valStr( $this->getPasswordEncrypted() ) ) {
			return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getPasswordEncrypted(), CONFIG_SODIUM_KEY_BLACKLEDGER, [ 'legacy_secret_key' => CONFIG_KEY_BLACKLEDGER ] );
		}
		return NULL;
	}

}
?>