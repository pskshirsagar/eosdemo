<?php

class CUemBillStatusType extends CBaseUemBillStatusType {

	const UNUSED        = 1;
	const IN_PROGRESS 	= 2;
	const COMPLETED  	= 3;

	const CUSTOM_AUTO_RESOLVED = 99;

	public static function populateSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'UNUSED',		self::UNUSED );
		$objSmarty->assign( 'IN_PROGRESS',	self::IN_PROGRESS );
		$objSmarty->assign( 'COMPLETED', 	self::COMPLETED );
	}

	public static function populateTemplateConstants( $arrmixTemplateParameters ) {
		$arrmixTemplateParameters['UNUSED']			= self::UNUSED;
		$arrmixTemplateParameters['IN_PROGRESS']	= self::IN_PROGRESS;
		$arrmixTemplateParameters['COMPLETED']		= self::COMPLETED;
		return $arrmixTemplateParameters;
	}

}
?>