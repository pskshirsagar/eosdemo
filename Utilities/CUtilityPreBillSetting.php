<?php

class CUtilityPreBillSetting extends CBaseUtilityPreBillSetting {

	public function validate( $strAction, $objUtilitiesDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNoBillingFee( $objUtilitiesDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		// Utility Pre Bill Setting
		$arrstrTempUtilityPreBillSettingOriginalValues = $arrstrUtilityPreBillSettingOriginalValueChanges = $arrstrUtilityPreBillSettingOriginalValues = [];
		$boolUtilityPreBillSettingChangeFlag = false;

		$arrmixUtilityPreBillSettingOriginalValues = fetchData( 'SELECT * FROM utility_pre_bill_settings WHERE property_utility_setting_id = ' . ( int ) $this->getPropertyUtilitySettingId(), $objDatabase );

		$arrstrTempUtilityPreBillSettingOriginalValues = $arrmixUtilityPreBillSettingOriginalValues[0];

		foreach( $arrstrTempUtilityPreBillSettingOriginalValues as $strKey => $mixUtilityPreBillSettingOriginalValue ) {

			if( true == in_array( $strKey, [ 'is_distribution_date_as_post_date', 'ps_subsidy_billing_fee', 'billed_ledger_ids', 'pre_bill_due_day', 'distribution_due_day', 'require_prebill_approval' ] ) ) {
				continue;
			}

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strFunctionName    = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			if( ( false == CStrings::strToBool( $mixUtilityPreBillSettingOriginalValue ) && '' == $this->$strFunctionName() ) || ( true == CStrings::strToBool( $mixUtilityPreBillSettingOriginalValue ) && '1' == $this->$strFunctionName() ) ) {
				continue;
			}

			if( CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $mixUtilityPreBillSettingOriginalValue ) {
				$arrstrUtilityPreBillSettingOriginalValueChanges[$strKey] = $this->$strFunctionName();
				$arrstrUtilityPreBillSettingOriginalValues[$strKey]       = $mixUtilityPreBillSettingOriginalValue;
				$boolUtilityPreBillSettingChangeFlag                      = true;
			}
		}

		$boolUtilityPreBillSettingUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUtilityPreBillSettingUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolUtilityPreBillSettingChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempUtilityPreBillSettingOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempUtilityPreBillSettingOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'utility_pre_bill_settings' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrUtilityPreBillSettingOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrUtilityPreBillSettingOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}

		}

		if( true == $boolUtilityPreBillSettingUpdateStatus ) {
			return true;
		} else {
			return false;
		}

	}

	public function valNoBillingFee( $objUtilitiesDatabase ) {
		$boolIsValid = true;

		$boolIsBillingFeeExist = $this->getIsBillingFeeExists( $objUtilitiesDatabase );

		if( true == $boolIsBillingFeeExist && true == $this->getIsNoBillingFee() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_billing_fee', 'There should not be a billing fee set up for this property. If No Billing Fee is checked.' ) );
		}

		return $boolIsValid;
	}

	public function getIsBillingFeeExists( $objUtilitiesDatabase ) {
		$arrintPropertyUtilityTypeIds = [];

		$arrobjPropertyUtilityTypes = \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchActiveBillablePropertyUtilityTypesByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );

		if( true == valArr( $arrobjPropertyUtilityTypes ) ) {
			$arrintPropertyUtilityTypeIds = array_keys( rekeyObjects( 'Id', $arrobjPropertyUtilityTypes ) );
		}

		$intBillingFeeCount = \Psi\Eos\Utilities\CPropertyUtilityTypeRates::createService()->fetchPropertyUtilityTypeRateCountHavingBillingFeeAmountByPropertyUtilityTypeIdsByCid( $arrintPropertyUtilityTypeIds, $this->getCid(), $objUtilitiesDatabase, true );

		if( 0 < $intBillingFeeCount ) {
			return true;
		} else {
			return false;
		}
	}

}
?>
