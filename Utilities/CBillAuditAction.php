<?php

class CBillAuditAction extends CBaseBillAuditAction {

	protected $m_intResolutionAuditStepId;

    public function setResolutionAuditStepId( $intResolutionAuditStepId ) {
    	$this->m_intResolutionAuditStepId = $intResolutionAuditStepId;
    }

    public function getResolutionAuditStepId() {
    	return $this->m_intResolutionAuditStepId;
    }

    public function valMemo() {
        $boolIsValid = true;

        if( true == is_null( $this->getMemo() ) && ( CBillAuditStep::VERIFIED == $this->getResolutionAuditStepId() || CBillAuditStep::CHECK_DATA_NEW == $this->getBillAuditStepId() ) ) {
        	$this->setMemo( 'No Memo.' );
        } elseif( true == is_null( $this->getMemo() ) ) {
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_audit_memo', 'Note is required' ) );
	        $boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valMemo();
        		break;

        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>