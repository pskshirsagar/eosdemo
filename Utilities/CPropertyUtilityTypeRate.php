<?php

class CPropertyUtilityTypeRate extends CBasePropertyUtilityTypeRate {

	protected $m_arrobjPropertyUtilityTypeFactorRules;
	protected $m_arrobjOldPropertyUtilityTypeFactorRules;
	protected $m_arrobjPropertyUtilityTypeUnitTypes;
	protected $m_arrobjPropertyUtilityTypeRateMethods;
	protected $m_arrobjOldPropertyUtilityTypeRateMethods;

	protected $m_objPropertyUtilityType;

	protected $m_strPropertyUtilityTypeName;
	protected $m_strStateCode;
	protected $m_strPostalCode;

	protected $m_intPropertyId;
	protected $m_intFallInCount;
	protected $m_intOldProeprtyUtilityTypeRateId;
	protected $m_intPropertyUtilityTypeRateMethodId;

	protected $m_fltPercentage;

	protected $m_boolIsOverlappedConsumption;
	const PUT_LEASE_TOTAL_VALIDATION_COUNT = 6;

	public static $c_arrstrPostalCode = [
		'20812', '20813', '20814', '20815', '20815', '20816', '20817', '20818', '20824', '20825', '20827', '20830',
		'20832', '20833', '20837','20838', '20839', '20841', '20842', '20847', '20848', '20849', '20850', '20851',
		'20852', '20853', '20854', '20855', '20859', '20860', '20861', '20862', '20866', '20868', '20871', '20872',
		'20874', '20875', '20876', '20877', '20878', '20879', '20880', '20882', '20883', '20884', '20885', '20886',
		'20891', '20895', '20896', '20898', '20901', '20902', '20903', '20904', '20905', '20906', '20907', '20908',
		'20910', '20911', '20912', '20913', '20914', '20915', '20916', '20918'
	];

	public function __construct() {
		parent::__construct();

		$this->m_intFallInCount = 0;

		$this->m_boolIsOverlappedConsumption = false;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {
		$this->setCommonAreaDeduction( 0 );
		$this->setMoveInFee( 0 );
		$this->setTerminationFee( 0 );
		$this->setFlatFeeAmount( 0 );
		$this->setBillingFeeAmount( 0 );
		$this->setBillingFeePercent( 0 );
		$this->setIgnoreRenewals( false );
		$this->setBudgetBillingAmount( NULL );
		$this->setRecoveryPercentage( NULL );
	}

	public function setPropertyUtilityType( $objPropertyUtilityType ) {
		$this->m_objPropertyUtilityType = $objPropertyUtilityType;
	}

	public function setIsOverlappedConsumption( $boolIsOverlappedConsumption ) {
		return $this->m_boolIsOverlappedConsumption = $boolIsOverlappedConsumption;
	}

	public function setOldPropertyUtilityTypeRateId( $intOldPropertyUtilitytypeRateId ) {
		return $this->m_intOldProeprtyUtilityTypeRateId = $intOldPropertyUtilitytypeRateId;
	}

	public function setPropertyUtilityTypeName( $strPropertyUtilityTypeName ) {
		$this->m_strPropertyUtilityTypeName = $strPropertyUtilityTypeName;
	}

	public function setFallInCount( $intFallInCount ) {
		$this->m_intFallInCount = $intFallInCount;
	}

	public function setPropertyUtilityTypeRateMethodId( $intPropertyUtilityTypeRateMethodId ) {
		$this->m_intPropertyUtilityTypeRateMethodId = $intPropertyUtilityTypeRateMethodId;
	}

	public function setPercentage( $fltPercentage ) {
		$this->m_fltPercentage = $fltPercentage;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_utility_type_name'] ) ) {
			$this->setPropertyUtilityTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_utility_type_name'] ) : $arrmixValues['property_utility_type_name'] );
		}

		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId );
	}

	/**
	 * Get Functions
	 */

	public function getPropertyUtilityTypeName() {
		return $this->m_strPropertyUtilityTypeName;
	}

	public function getFallInCount() {
		return $this->m_intFallInCount;
	}

	public function getPropertyUtilityTypeFactorRules() {
		return $this->m_arrobjPropertyUtilityTypeFactorRules;
	}

	public function getOldPropertyUtilityTypeFactorRules() {
		return $this->m_arrobjOldPropertyUtilityTypeFactorRules;
	}

	public function getPropertyUtilityTypeUnitTypes() {
		return $this->m_arrobjPropertyUtilityTypeUnitTypes;
	}

	public function getPropertyUtilityType() {
		return $this->m_objPropertyUtilityType;
	}

	public function getIsOverlappedConsumption() {
		return $this->m_boolIsOverlappedConsumption;
	}

	public function getOldPropertyUtilityTypeRateId() {
		return $this->m_intOldProeprtyUtilityTypeRateId;
	}

	public function getPropertyUtilityTypeRateMethodId() {
		return $this->m_intPropertyUtilityTypeRateMethodId;
	}

	public function getPercentage() {
		return $this->m_fltPercentage;
	}

	public function getPropertyUtilityTypeRateMethods() {
		return $this->m_arrobjPropertyUtilityTypeRateMethods;
	}

	public function getOldPropertyUtilityTypeRateMethods() {
		return $this->m_arrobjOldPropertyUtilityTypeRateMethods;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	/**
	 * Add Functions
	 */

	public function addPropertyUtilityTypeFactorRules( $objPropertyUtilityTypeFactorRule ) {

		if( CUtilityRubsFormula::SQUARE_FOOTAGE_WEIGHTED == $objPropertyUtilityTypeFactorRule->getUtilityRubsFormulaId() || CUtilityRubsFormula::SQUARE_FOOTAGE_FIXED == $objPropertyUtilityTypeFactorRule->getUtilityRubsFormulaId() ) {
			$this->m_arrobjPropertyUtilityTypeFactorRules[$objPropertyUtilityTypeFactorRule->getSquareFeet()] = $objPropertyUtilityTypeFactorRule;
		} else {
			$this->m_arrobjPropertyUtilityTypeFactorRules[$objPropertyUtilityTypeFactorRule->getCount()] = $objPropertyUtilityTypeFactorRule;
		}
	}

	public function addOldPropertyUtilityTypeFactorRules( $objPropertyUtilityTypeFactorRule ) {

		if( CUtilityRubsFormula::SQUARE_FOOTAGE_WEIGHTED == $objPropertyUtilityTypeFactorRule->getUtilityRubsFormulaId() ) {
			$this->m_arrobjOldPropertyUtilityTypeFactorRules[$objPropertyUtilityTypeFactorRule->getSquareFeet()] = $objPropertyUtilityTypeFactorRule;
		} else {
			$this->m_arrobjOldPropertyUtilityTypeFactorRules[$objPropertyUtilityTypeFactorRule->getCount()] = $objPropertyUtilityTypeFactorRule;
		}
	}

	public function addOldPropertyUtilityTypeRateMethods( $objPropertyUtilityTypeRateMethod ) {
			$this->m_arrobjOldPropertyUtilityTypeRateMethods[] = $objPropertyUtilityTypeRateMethod;
	}

	public function addPropertyUtilityTypeUnitTypes( $objPropertyUtilityTypeUnitType ) {
		$this->m_arrobjPropertyUtilityTypeUnitTypes[$objPropertyUtilityTypeUnitType->getUnitTypeId()] = $objPropertyUtilityTypeUnitType;
	}

	public function addPropertyUtilityTypeRateMethods( $objPropertyUtilityTypeRateMethod ) {
			$this->m_arrobjPropertyUtilityTypeRateMethods[] = $objPropertyUtilityTypeRateMethod;
	}

	/**
	 * Validate Functions
	 */

	public function valPropertyUtilityTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyUtilityTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_utility_type_id', 'Property utility type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFlatFeeAmount() {
		$boolIsValid = true;

		if( 0 > $this->getFlatFeeAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'flat_fee_amount', 'Enter valid Flat Fee Amount.' ) );
		}

		return $boolIsValid;
	}

	public function valCommonAreaDeduction() {
		$boolIsValid = true;

		if( 0 > $this->getCommonAreaDeduction() || 1 <= $this->getCommonAreaDeduction() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'common_deduction_percent', 'Enter valid common area deduction.' ) );
		}

		return $boolIsValid;
	}

	public function valBillingFeeAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getBillingFeeAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_fee_amount', 'Invalid billing fee amount.' ) );
		}

		return $boolIsValid;
	}

	public function valBillingFeePercent() {
		$boolIsValid = true;
		$intBillingFeePercent = $this->getBillingFeePercent();

		if( true == is_numeric( $intBillingFeePercent ) && ( 0 > $intBillingFeePercent || 1 < $intBillingFeePercent ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_fee_percent', 'Invalid billing fee percent.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityBatchStep( $objUtilitiesDatabase ) {
		$boolIsValid = true;

		$objClonePropertyUtilityType = new CPropertyUtilityType();
		$objClonePropertyUtilityType->setCid( $this->getCid() );
		$objClonePropertyUtilityType->setPropertyId( $this->getPropertyId() );

		$objUtilityBatch = $objClonePropertyUtilityType->fetchActiveUtilityBatch( $objUtilitiesDatabase );

		if( true == valObj( $objUtilityBatch, 'CUtilityBatch' ) && CUtilityBatchStep::CALCULATE_BILLING_DATA < $objUtilityBatch->getUtilityBatchStepId() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Lease bucket can not be inserted/updated as you have crossed the calculate billing data step.' ) );
		}
		return $boolIsValid;

	}

	public function valBillingFeeAsPerStateCode( $objUtilitiesDatabase ) {

		$boolIsValid = true;
		$arrstrStateCode = [ 'TX', 'FL', 'MD', 'OR' ];

		if( false == in_array( $this->getStateCode(), $arrstrStateCode ) ) {
			return $boolIsValid;
		}

		$objClonePropertyUtilityType = \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchPropertyUtilityTypeByIdByCid( $this->getPropertyUtilityTypeId(), $this->getCid(), $objUtilitiesDatabase );

		$intUtilityTypeId = $objClonePropertyUtilityType->getUtilityTypeId();

		switch( $this->getStateCode() ) {
			case 'TX':
			case 'FL':
				switch( $intUtilityTypeId ) {
					case CUtilityType::CONVERGENT_BILLING:
					case CUtilityType::WATER:
					case CUtilityType::SEWER:

						if( 0 <= $this->getFlatFeeAmount() && $this->getUtilityRubsFormulaId() == CUtilityRubsFormula::FLAT_FEE ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'flat_fee_amount', 'You cannot apply a flat billing fee in ' . $this->getStateCode() . '.' ) );
						}
						break 2;

					case CUtilityType::GAS:
						if( 'TX' == $this->getStateCode() && 3 < $this->getBillingFeeAmount() ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_fee_amount', 'You cannot charge more than $3 for a gas fee in TX.' ) );
						}
						break 2;

					default:
						// default case
						break;
				}

			case 'MD':

				switch( $intUtilityTypeId ) {

					case CUtilityType::CONVERGENT_BILLING:
					case CUtilityType::WATER:

                        // We are not triggering this validation if the property is Quest Management Group >> 1840 Apartments( changes done under task 2556308 - Quest 1840 Billing Fee exception )
					    if( true == $objClonePropertyUtilityType->getIsSubmetered() && true == in_array( $this->m_strPostalCode, self::$c_arrstrPostalCode ) ) {
                            $boolIsValid = true;
                            break 2;
                        }

                        if(1 < $this->getBillingFeeAmount()) {
                            $boolIsValid = false;
                            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_fee_amount', 'You cannot apply a billing fee of more than $1.' ) );
                        }
						break 2;

					default:
						// default case
						break;
				}

			case 'OR':
				switch( $intUtilityTypeId ) {

					case CUtilityType::CONVERGENT_BILLING:
					case CUtilityType::WATER:
						if( 0 < $this->getBillingFeeAmount() || 0 < $this->getBillingFeePercent() ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_fee_amount', 'You cannot charge a Water/Convergent billing fee in OR.' ) );
						}
						break 2;

					default:
						// default case
						break;
				}

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;

	}

	public function validate( $strAction, $objUtilitiesDatabase ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyUtilityTypeId();
				$boolIsValid &= $this->valCommonAreaDeduction();
				$boolIsValid &= $this->valBillingFeeAmount();
				$boolIsValid &= $this->valBillingFeePercent();
				$boolIsValid &= $this->valFlatFeeAmount();
				$boolIsValid &= $this->valUtilityBatchStep( $objUtilitiesDatabase );
				$boolIsValid &= $this->valBillingFeeAsPerStateCode( $objUtilitiesDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createPropertyUtilityTypeFactorRule() {

		$objPropertyUtilityTypeFactorRule = new CPropertyUtilityTypeFactorRule;

		$objPropertyUtilityTypeFactorRule->setCid( $this->getCid() );
		$objPropertyUtilityTypeFactorRule->setUtilityRubsFormulaId( $this->getUtilityRubsFormulaId() );
		$objPropertyUtilityTypeFactorRule->setPropertyUtilityTypeId( $this->getPropertyUtilityTypeId() );
		$objPropertyUtilityTypeFactorRule->setPropertyUtilityTypeRateId( $this->getId() );

		return $objPropertyUtilityTypeFactorRule;
	}

	public function createPropertyUtilityTypeUnitType() {

		$objPropertyUtilityTypeUnitType = new CPropertyUtilityTypeUnitType;

		$objPropertyUtilityTypeUnitType->setCid( $this->getCid() );
		$objPropertyUtilityTypeUnitType->setUtilityRubsFormulaId( $this->getUtilityRubsFormulaId() );
		$objPropertyUtilityTypeUnitType->setPropertyUtilityTypeId( $this->getPropertyUtilityTypeId() );
		$objPropertyUtilityTypeUnitType->setPropertyUtilityTypeRateId( $this->getId() );

		return $objPropertyUtilityTypeUnitType;
	}

	public function createPropertyUtilityTypeRateMethod( $intPropertyId ) {

		$objPropertyUtilityTypeRateMethod = new CPropertyUtilityTypeRateMethod();

		$objPropertyUtilityTypeRateMethod->setCid( $this->getCid() );
		$objPropertyUtilityTypeRateMethod->setPropertyId( $intPropertyId );
		$objPropertyUtilityTypeRateMethod->setPropertyUtilityTypeRateId( $this->getId() );

		return $objPropertyUtilityTypeRateMethod;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPropertyUtilityTypeFactorRules( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CPropertyUtilityTypeFactorRules::createService()->fetchPropertyUtilityTypeFactorRulesByPropertyUtilityTypeRateIdByUtilityRubsFormulaId( $this->getId(), $this->getUtilityRubsFormulaId(), $objUtilitiesDatabase );
	}

	public function fetchPropertyUtilityTypeUnitTypes( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CPropertyUtilityTypeUnitTypes::createService()->fetchPropertyUtilityTypeUnitTypesByPropertyUtilityTypeRateId( $this->getId(), $objUtilitiesDatabase );
	}

	public function fetchPropertyUtilityTypeRateMethods( $objUtilitiesDatabase ) {

		return \Psi\Eos\Utilities\CPropertyUtilityTypeRateMethods::createService()->fetchCustomPropertyUtilityTypeRateMethodsByPropertyUtilityTypeRateIdByCid( $this->getId(), $this->getCid(), $objUtilitiesDatabase );
	}

	/**
	 * Other Functions
	 */

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		$arrstrTempOriginalValues 	= [];
		$arrstrOriginalValues 		= [];
		$arrstrOriginalValueChanges = [];
		$boolChangeFlag				= false;

		$strSql = 'SELECT
						putr.*,
						put.property_id
					 FROM
						property_utility_type_rates putr
						JOIN property_utility_types put ON ( putr.property_utility_type_id = put.id )
					 WHERE
						putr.id = ' . ( int ) $this->getId();

		$arrmixOriginalValues = fetchData( $strSql, $objDatabase );

		$arrstrTempOriginalValues = $arrmixOriginalValues[0];

		foreach( $arrstrTempOriginalValues as $strKey => $strMixOriginalValue ) {

			if( true == in_array( $strKey, [ 'property_id', 'ps_move_in_fee', 'ps_termination_fee', 'ps_billing_fee_amount' ] ) ) {
				continue;
			}

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			if( CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $strMixOriginalValue ) {
				$arrstrOriginalValueChanges[$strKey] 	= $this->$strFunctionName();
				$arrstrOriginalValues[$strKey] 			= $strMixOriginalValue;
				$boolChangeFlag 						= true;
			}
		}

		$boolUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'property_utility_type_rates' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}

			return $boolUpdateStatus;
		}

		return false;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>