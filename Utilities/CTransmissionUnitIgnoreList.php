<?php

class CTransmissionUnitIgnoreList extends CBaseTransmissionUnitIgnoreList {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityTransmissionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityTransmissionDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIgnoredDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>