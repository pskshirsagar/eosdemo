<?php

class CPropertyUtilityProviderEvent extends CBasePropertyUtilityProviderEvent {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityProviderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityContactTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getUtilityContactTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_building_id', 'Contact type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSystemEmailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailToAddress() {
		$boolIsValid = true;

		$this->setEmailToAddress( getSanitizedEmailField( $this->getEmailToAddress() ) );

		if( false == valStr( $this->getEmailToAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_contact', 'Please enter to email address.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailSubject() {
		$boolIsValid = true;

		if( false == valStr( $this->getEmailSubject() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_contact', 'Please enter to email subject.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailContent() {
		$boolIsValid = true;

		if( false == valStr( $this->getEmailContent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_contact', 'Please enter to email Content.' ) );
		}

		return $boolIsValid;
	}

	public function valFaxNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailTo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailStreetLineOne() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailStreetLineTwo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailState() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneContact() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneIsInProgress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystemDocument() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validateContactTypeData() {
		$boolIsValid = true;

		switch( $this->getUtilityContactTypeId() ) {
			case CUtilityContactType::EMAIL:
				$boolIsValid &= $this->valEmailToAddress();
				$boolIsValid &= $this->valEmailSubject();
				$boolIsValid &= $this->valEmailContent();
				break;

			case CUtilityContactType::FAX:
				$boolIsValid &= $this->valFaxNumber();
				break;

			case CUtilityContactType::MAIL:
				$boolIsValid &= $this->valMailTo();
				$boolIsValid &= $this->valMailStreetLineOne();
				$boolIsValid &= $this->valMailCity();
				$boolIsValid &= $this->valMailState();
				$boolIsValid &= $this->valMailPostalCode();
				break;

			case CUtilityContactType::PHONE:
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valPhoneContact();
				break;

			default:
				// No default case.
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUtilityContactTypeId();
				$boolIsValid &= $this->validateContactTypeData();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>