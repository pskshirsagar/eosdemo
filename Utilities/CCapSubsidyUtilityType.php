<?php

class CCapSubsidyUtilityType extends CBaseCapSubsidyUtilityType {

   const OCCUPANCY 					= 1;
   const SQUARE_FOOTAGE 			= 2;
   const UNIT_TYPE 					= 3;
   const BEDROOMS 					= 4;
   const OCCUPANCY_SQUARE_FOOTAGE	= 5;
   const OCCUPANCY_UNIT_TYPE 		= 6;
   const OCCUPANCY_BEDROOMS 		= 7;

}
?>