<?php

class CPropertyMeterNotificationSetting extends CBasePropertyMeterNotificationSetting {

	protected $m_boolIsDelete;

	protected $m_intUtilityTypeId;
	protected $m_intPropertyMeteringSettingId;

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->m_intUtilityTypeId = ( int ) $intUtilityTypeId;
	}

	public function getIsDelete() {
		return $this->m_boolIsDelete;
	}

	public function setIsDelete( $boolIsDelete ) {
		$this->m_boolIsDelete = ( bool ) $boolIsDelete;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_type_id'] ) ) {
			$this->setUtilityTypeId( $arrmixValues['utility_type_id'] );
		}

	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>