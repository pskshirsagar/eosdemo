<?php

class CVacantBillAlertEvent extends CBaseVacantBillAlertEvent {

	const CONTACTED_PROVIDER_NO_RESOLUTION		= 1;
	const CONTACTED_PROVIDER_PENDING			= 2;
	const CONTACTED_CLIENT_NO_RESOLUTION		= 3;
	const CONTACTED_CLIENT_PENDING				= 4;
	const ACCOUNT_CREATED						= 5;
	const RESOLVED_WITHOUT_INVOICE				= 6;
	const RESOLVED_UPLOADED_BILL				= 7;
	const VACANT_BILL_ALERT_NEW					= 8;
	const RESOLVED_UTILITIES_IN_RESIDENT_NAME	= 9;
	const RESOLVED_WITHOUT_ACCOUNT				=10;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static $c_arrintDefaultVacantBillAlerts = [
		self::VACANT_BILL_ALERT_NEW,
		self::CONTACTED_CLIENT_PENDING,
		self::CONTACTED_PROVIDER_PENDING,
		self::CONTACTED_CLIENT_NO_RESOLUTION,
		self::CONTACTED_PROVIDER_NO_RESOLUTION
	];

	public static $c_arrintCompletedVacantBillAlertEventIds = [
		self::ACCOUNT_CREATED,
		self::RESOLVED_WITHOUT_INVOICE,
		self::RESOLVED_UPLOADED_BILL,
		self::RESOLVED_WITHOUT_ACCOUNT
	];

	public static function getVacantBillAlertEventContact( $strVacantBillAlertEventType ) : int {

		switch( $strVacantBillAlertEventType ) {

			case 'PROVIDER_PENDING':
				$intVacantBillAlertEventId = self::CONTACTED_PROVIDER_PENDING;
				break;

			case 'PROVIDER_NO_RESOLUTION':
				$intVacantBillAlertEventId = self::CONTACTED_PROVIDER_NO_RESOLUTION;
				break;

			case 'CLIENT_PENDING':
				$intVacantBillAlertEventId = self::CONTACTED_CLIENT_PENDING;
				break;

			case 'CLIENT_NO_RESOLUTION':
				$intVacantBillAlertEventId = self::CONTACTED_CLIENT_NO_RESOLUTION;
				break;

			default:
				$intVacantBillAlertEventId = self::CONTACTED_PROVIDER_PENDING;
				break;
		}

		return $intVacantBillAlertEventId;
	}

}
?>