<?php

class CUtilityLog extends CBaseUtilityLog {

	const BILL_EXPORT_FAILURE_REASON	= 'BILL_EXPORT_FAILURE_REASON';
	const NO_VENDOR 					= 'Vendor not set';
	const NO_VENDOR_LOCATION 			= 'Vendor location not set';
	const NO_VENDOR_ACCOUNT 			= 'The vendor that is set on the utility bill account no longer exists in Entrata.';
	const BILL_OVERLAP_AUDIT			= 'Bill is having Bill Overlap Audit';
	const EXPORT_BILL_SETTING_SET 		= 'Export Utility Bills Setting is off';
	const PROPERTY_GL_SETTING_NOT_FOUND	= 'Property GL Setting Not Found';
	const FAILED_TO_INSERT_AP_HEADER	= 'Failed to Insert Invoice. You would need to set gl_account either for ap_payee (vendor) or ap_payee account (vendor account) to get the invoice exported';
	const FAILED_TO_POST_FTP			= 'Failed to post invoice through FTP. Check FTP URL and Credentials';
	const FAILED_TO_APPROVAL_ROUTE		= 'Failed to route invoice';
	const LATE_FEE_AUDIT			    = 'Bill is having Late Fee Audit';
	const PAST_DUE_DATE_AUDIT			= 'Bill is having Past Due Date Audit';
	const BALANCE_FORWARD_AUDIT			= 'Bill is having Balance Forward Audit';
	const MULTIPLE_BILLS_IN_SAME_BILLING_PERIOD_AUDIT			= 'Bill is having Multiple Bills in same billing period Audit';

	const NO_VENDOR_IN_UTILITIES 		    = 'Vendor is not set on utility bill account.';
	const NO_VENDOR_LOCATION_IN_UTILITIES 	= 'Vendor location is not set on utility bill account';
	const NO_UTILITY_BILL_ACCOUNT           = 'Utility Bill Account does not exist in Entrata';
	const UTILITY_BILL_ACCOUNT_NOT_ADDED    = 'Utility Bill Account has not been added as a Vendor Account in Entrata';

	const UTILITY_BILL_AND_CHARGES_AMOUNT	= 'Utility Bill and Charges Total amount not matched';
	const UTILITY_BILL_DATE					= 'Utility Bill date is less Than Uem Go Live Date';
	const UTILITY_DUE_DATE					= 'Utility Bill ( Due date - 21 days ) is less Than Uem Go Live Date';
	const UTILITY_SERVICE_PERIOD			= 'Utility Bill Service period End date is less than Uem Go Live Date';
	const UTILITY_BILL_PDF					= 'Unable to create file for bill';

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valPropertyId() {
		return true;
	}

	public function valUtilityBillId() {
		return true;
	}

	public function valKey() {
		return true;
	}

	public function valValue() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>