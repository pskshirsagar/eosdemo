<?php

class CBillAuditLiabilityType extends CBaseBillAuditLiabilityType {

	const ENTRATA		= 1;
	const CLIENT		= 2;
	const PROVIDER		= 3;
	const VACANT_UNIT	= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getBillAuditLiabilityTypeName( $intBillAuditLiabilityTypeId ) {

		$strName = '';

		switch( $intBillAuditLiabilityTypeId ) {
			case self::ENTRATA:
				$strName = 'Entrata';
				break;

			case self::CLIENT:
				$strName = 'Client';
				break;

			case self::PROVIDER:
				$strName = 'Provider';
				break;

			case self::VACANT_UNIT:
				$strName = 'Vacant Unit';
				break;

			default:
				// Empty case
				break;
		}

		return $strName;
	}

}
?>