<?php

class CUtilityBillFdgDataLog extends CBaseUtilityBillFdgDataLog {

	protected $m_strFileName;

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFdgDataLogTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChildBillIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJsonData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valError() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFailed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>