<?php

class CBuyerLocation extends CBaseBuyerLocation {

	protected $m_intCid;
	protected $m_intStoreId;

	protected $m_strCompanyName;
	protected $m_strBuyerContactLastName;
	protected $m_strBuyerContactFirstName;
	protected $m_strBuyerContactPhoneNumber;
	protected $m_strBuyerContactEmailAddress;

	public function valId() {
		return true;
	}

	public function valBuyerId() {
		return true;
	}

	public function valPropertyId() {
		return true;
	}

	public function valLocationName() {
		return true;
	}

	public function valPhoneNumber() {
		return true;
	}

	public function valStreetLine1() {
		return true;
	}

	public function valStreetLine2() {
		return true;
	}

	public function valCity() {
		return true;
	}

	public function valStateCode() {
		return true;
	}

	public function valPostalCode() {
		return true;
	}

	public function valDisabledBy() {
		return true;
	}

	public function valDisabledOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['company_name'] ) && $boolDirectSet ) {
			$this->m_strCompanyName = trim( stripcslashes( $arrmixValues['company_name'] ) );
		} elseif( isset( $arrmixValues['company_name'] ) ) {
			$this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['company_name'] ) : $arrmixValues['company_name'] );
		}

		if( isset( $arrmixValues['name_last'] ) && $boolDirectSet ) {
			$this->m_strBuyerContactLastName = trim( stripcslashes( $arrmixValues['name_last'] ) );
		} elseif( isset( $arrmixValues['name_last'] ) ) {
			$this->setBuyerContactLastName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['name_last'] ) : $arrmixValues['name_last'] );
		}

		if( isset( $arrmixValues['name_first'] ) && $boolDirectSet ) {
			$this->m_strBuyerContactFirstName = trim( stripcslashes( $arrmixValues['name_first'] ) );
		} elseif( isset( $arrmixValues['name_first'] ) ) {
			$this->setBuyerContactFirstName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['name_first'] ) : $arrmixValues['name_first'] );
		}

		if( isset( $arrmixValues['phone_number'] ) && $boolDirectSet ) {
			$this->m_strBuyerContactPhoneNumber = trim( stripcslashes( $arrmixValues['phone_number'] ) );
		} elseif( isset( $arrmixValues['phone_number'] ) ) {
			$this->setBuyerContactPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['phone_number'] ) : $arrmixValues['phone_number'] );
		}

		if( isset( $arrmixValues['email_address'] ) && $boolDirectSet ) {
			$this->m_strBuyerContactEmailAddress = trim( stripcslashes( $arrmixValues['email_address'] ) );
		} elseif( isset( $arrmixValues['email_address'] ) ) {
			$this->setBuyerContactEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['email_address'] ) : $arrmixValues['email_address'] );
		}

		if( isset( $arrmixValues['store_id'] ) && $boolDirectSet ) {
			$this->m_intStoreId = trim( stripcslashes( $arrmixValues['store_id'] ) );
		} elseif( isset( $arrmixValues['store_id'] ) ) {
			$this->setStoreId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['store_id'] ) : $arrmixValues['store_id'] );
		}

		if( isset( $arrmixValues['cid'] ) && $boolDirectSet ) {
			$this->m_intCid = trim( stripcslashes( $arrmixValues['cid'] ) );
		} elseif( isset( $arrmixValues['cid'] ) ) {
			$this->setCid( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cid'] ) : $arrmixValues['cid'] );
		}
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = CStrings::strTrimDef( $strCompanyName, 100, NULL, true );
	}

	public function setBuyerContactEmailAddress( $strBuyerContactEmailAddress ) {
		$this->m_strBuyerContactEmailAddress = CStrings::strTrimDef( $strBuyerContactEmailAddress, 240, NULL, true );
	}

	public function setBuyerContactPhoneNumber( $strBuyerContactPhoneNumber ) {
		$this->m_strBuyerContactPhoneNumber = CStrings::strTrimDef( $strBuyerContactPhoneNumber, 30, NULL, true );
	}

	public function setBuyerContactFirstName( $strBuyerContactFirstName ) {
		$this->m_strBuyerContactFirstName = CStrings::strTrimDef( $strBuyerContactFirstName, 50, NULL, true );
	}

	public function setBuyerContactLastName( $strBuyerContactLastName ) {
		$this->m_strBuyerContactLastName = CStrings::strTrimDef( $strBuyerContactLastName, 50, NULL, true );
	}

	public function setStoreId( $intStoreId ) {
		$this->m_intStoreId = $intStoreId;
	}

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getBuyerContactEmailAddress() {
		return $this->m_strBuyerContactEmailAddress;
	}

	public function getBuyerContactPhoneNumber() {
		return $this->m_strBuyerContactPhoneNumber;
	}

	public function getBuyerContactFirstName() {
		return $this->m_strBuyerContactFirstName;
	}

	public function getBuyerContactLastName() {
		return $this->m_strBuyerContactLastName;
	}

	public function getStoreId() {
		return $this->m_intStoreId;
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

}
?>