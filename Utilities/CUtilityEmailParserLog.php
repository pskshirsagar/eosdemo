<?php

class CUtilityEmailParserLog extends CBaseUtilityEmailParserLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeLocationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFromEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubject() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFailed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityDocumentIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResolvedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResolvedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>