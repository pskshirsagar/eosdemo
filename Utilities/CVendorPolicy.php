<?php

class CVendorPolicy extends CBaseVendorPolicy {

	protected $m_intDeniedBy;

	protected $m_strDeniedOn;
	protected $m_strBrokerName;

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valBrokerId() {
		$boolIsValid = true;

		if( false == valId( $this->getBrokerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'borker_id', 'Brokerage name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valComplianceItemId() {
		$boolIsValid = true;

		if( false == valId( $this->getComplianceItemId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'borker_id', 'Policy type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPolicyDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getPolicyDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Policy description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPolicyNumber( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPolicyNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'policy_number', 'Policy number is required.' ) );
		} else {
			$intCountVendorPolicies = ( int ) \Psi\Eos\Utilities\CVendorPolicies::createService()->fetchActiveVendorPoliciesByPolicyNumberByComplianceItemIdByVendorId( $this->getPolicyNumber(), $this->getComplianceItemId(), $this->getVendorId(), $objDatabase, $this->getId() );
			if( 1 <= $intCountVendorPolicies ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'policy_number', 'Policy number for this policy type is already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valExpirationDate() {

		if( true == is_null( $this->getExpirationDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expired_date', 'Expiration date is required.' ) );
		} elseif( false == is_null( $this->getExpirationDate() ) && ( false == CValidation::validateDate( $this->getExpirationDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'A valid expiration date is required.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function valVendorEntityId() {
		$boolIsValid = true;

		if( false == valId( $this->getVendorEntityId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_entity_id', 'Vendor entity is required.' ) );
		}

		return $boolIsValid;
	}

	public function setBrokerName( $strBrokerName ) {
		$this->m_strBrokerName = CStrings::strTrimDef( $strBrokerName, 240, NULL, true );
	}

	public function getBrokerName() {
		return $this->m_strBrokerName;
	}

	public function setDeniedBy( $intDeniedBy ) {
		$this->set( 'm_intDeniedBy', CStrings::strToIntDef( $intDeniedBy, NULL, false ) );
	}

	public function getDeniedBy() {
		return $this->m_intDeniedBy;
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->set( 'm_strDeniedOn', CStrings::strTrimDef( $strDeniedOn, -1, NULL, true ) );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valComplianceItemId();
				$boolIsValid &= $this->valBrokerId();
				$boolIsValid &= $this->valPolicyDescription();
				$boolIsValid &= $this->valPolicyNumber( $objDatabase );
				$boolIsValid &= $this->valExpirationDate();
				$boolIsValid &= $this->valVendorEntityId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['broker_name'] ) && $boolDirectSet ) {
			$this->m_strBrokerName = trim( stripcslashes( $arrmixValues['broker_name'] ) );
		} elseif( isset( $arrmixValues['broker_name'] ) ) {
			$this->setBrokerName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['broker_name'] ) : $arrmixValues['broker_name'] );
		}

		if( isset( $arrmixValues['denied_by'] ) && $boolDirectSet ) {
			$this->set( 'm_intDeniedBy', trim( $arrmixValues['denied_by'] ) );
		} elseif( isset( $arrmixValues['denied_by'] ) ) {
			$this->setDeniedBy( $arrmixValues['denied_by'] );
		}

		if( isset( $arrmixValues['denied_on'] ) && $boolDirectSet ) {
			$this->set( 'm_strDeniedOn', trim( $arrmixValues['denied_on'] ) );
		} elseif( isset( $arrmixValues['denied_on'] ) ) {
			$this->setDeniedOn( $arrmixValues['denied_on'] );
		}
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

}

?>