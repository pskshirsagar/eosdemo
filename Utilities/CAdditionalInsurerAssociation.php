<?php

class CAdditionalInsurerAssociation extends CBaseAdditionalInsurerAssociation {

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valAdditionalInsurerId() {
		return true;
	}

	public function valPropertyGroupId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>