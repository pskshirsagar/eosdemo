<?php

class CPropertyCapSubsidy extends CBasePropertyCapSubsidy {

	public function valCapAmount() {
		$boolIsValid = true;

		if( false == is_null( $this->getCapAmount() ) && 0 >= $this->getCapAmount() ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cap_amount', 'Cap amount should be greater than 0.' ) );
      	}

		return $boolIsValid;
    }

    public function valSubsidyAmount() {
        $boolIsValid = true;

        if( false == is_null( $this->getSubsidyAmount() ) && 0 >= $this->getSubsidyAmount() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_amount', 'Subsidy amount should be greater than 0 .' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valSubsidyAmount();
        		$boolIsValid &= $this->valCapAmount();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>