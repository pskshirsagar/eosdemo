<?php

class CWorker extends CBaseWorker {

	const SUPER_ADMIN			= 1;
	const MAX_TAX_NUMBER		= 4;
	const MAX_PASSWORD_SIZE		= 8;
	const MIN_PHONE_NUMBER_SIZE	= 10;
	const MAX_PHONE_NUMBER_SIZE = 15;

	const TOTAL_LOGIN_ATTEMPTS		 = 5;
	// 5 minutes
	const LOGIN_ATTEMPT_PERIOD		 = 300;
	// 30 minutes
	const LOGIN_ATTEMPT_WAITING_TIME = 1800;

	// @TODO: temprory constanct, will remove it once migration of compliance tables done [task id: 2499522]
	const USER_ID_FLAG_TO_MIGRATE_COMPLIANCE_DATA = 1726;

	const VALIDATE_INITIAL_SETUP = 'validate_initial_setup';

	const VALIDATE_BUSINESS_OWNER = 'validate_business_owner';

	public static $c_intSystemVendorId = 1;

	protected $m_boolIsPSIUser;
	protected $m_boolIsPrimary;
	protected $m_boolIsNoneGiven;
	protected $m_boolIsSystemGeneratedPassword;

	protected $m_intStoreId;
	protected $m_intWorkerStoreId;
	protected $m_intDefaultVendorEntityId;
	protected $m_intVendorPreferenceValue;

	protected $m_strPoolName;
	protected $m_strPassword;
	protected $m_strSubDomain;
	protected $m_strConfirmPassword;
	protected $m_strCurrentPassword;

	public function valPassword( $boolIsNewPassword = false ) {

		$strMessage = '';

		if( true == $boolIsNewPassword ) {
			$strMessage = 'new ';
		}

		if( true == is_null( $this->m_strPassword ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'password', \Psi\CStringService::singleton()->ucfirst( $strMessage ) . __( 'Password is required.' ) ) );
		} elseif( false == is_null( $this->m_strPassword ) && CWorker::MAX_PASSWORD_SIZE > strlen( $this->m_strPassword ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'password', \Psi\CStringService::singleton()->ucfirst( $strMessage ) . __( 'Password cannot be less than {%d, 0} characters.', [ CWorker::MAX_PASSWORD_SIZE ] ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valConfirmPassword( $boolIsNewPassword = false ) {
		$boolIsValid = true;

		if( true == $this->getSystemGeneratedPassword() ) {
			return $boolIsValid;
		}

		$strMessageConfirmPasswordRequired = __( 'Confirm password is required.' );
		$strMessageConfirmPasswordNotMatched = __( 'Password does not match with confirm password.' );

		if( true == $boolIsNewPassword ) {
			$strMessageConfirmPasswordRequired = __( 'Confirm new password is required.' );
			$strMessageConfirmPasswordNotMatched = __( 'New password does not match with confirm password.' );
		}

		if( true == is_null( $this->m_strConfirmPassword ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'confirm_password', $strMessageConfirmPasswordRequired ) );
		} elseif( 0 != strcmp( $this->m_strPassword, $this->m_strConfirmPassword ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'confirm_password', $strMessageConfirmPasswordNotMatched ) );
		}
		return $boolIsValid;
	}

	public function valCurrentPassword( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getCurrentPassword() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_password', __( 'Current password is required.' ) ) );
		} else {
			$objWorker = \Psi\Eos\Utilities\CWorkers::createService()->fetchActiveWorkerByEmailAddressByPasswordBySubDomainOrByVendorId( $this->m_strEmailAddress, md5( $this->getCurrentPassword() ), $objDatabase, NULL, $this->getVendorId() );
			if( false == valObj( $objWorker, 'CWorker' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_password', __( 'Current password is not correct.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valTaxNumberEncrypted( $intDutyTypeId, $boolIsPsiUser = false ) {
		$boolIsValid = true;

		if( CDutyType::VENDOR == $intDutyTypeId ) {

			$strDecryptedTaxNumber = $this->getDecryptedTaxNumber();

			if( CVendorEntity::TAX_ID_NUMBER_FORMAT_ONE == $strDecryptedTaxNumber || CVendorEntity::TAX_ID_NUMBER_FORMAT_TWO == $strDecryptedTaxNumber ) {
				return $boolIsValid;
			}

			if( true == $boolIsPsiUser && true == $this->getIsPrincipal() && true == is_null( $this->m_strTaxNumberEncrypted ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax id is required.' ) ) );

			} elseif( false == is_null( $this->m_strTaxNumberEncrypted ) && false == preg_match( '/^[\d\-]+$/', $strDecryptedTaxNumber ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax id should be numeric and in XX-XXXXXXX or XXX-XX-XXXX format.' ) ) );

			} elseif( false == is_null( $this->m_strTaxNumberEncrypted ) && false == preg_match( '/^([\d]{2}-[\d]{7})$|^([\d]{3}-[\d]{2}-[\d]{4})$/', $strDecryptedTaxNumber ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax id should be in XX-XXXXXXX or XXX-XX-XXXX format.' ) ) );

			}
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'name_first', __( 'First name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valNameMiddle() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameMiddle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'name_middle', __( 'Middle name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valEmailAddress( $objDatabase ) {

		if( true == is_null( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is required.' ) ) );
		} elseif( true == isset( $this->m_strEmailAddress ) && $this->m_strEmailAddress != filter_var( $this->m_strEmailAddress, FILTER_VALIDATE_EMAIL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address does not appear to be valid.' ) ) );
		} elseif( 0 < strlen( $this->m_strEmailAddress ) && true == valObj( $objDatabase, 'CDatabase' ) && 0 < \Psi\Eos\Utilities\CWorkers::createService()->fetchWorkerCount( ' WHERE lower( email_address ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getEmailAddress() ) ) . '\' AND id != ' . ( int ) $this->getId() . ' AND deleted_by IS NULL AND deleted_on IS NULL', $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is already being used.' ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;

		if( true == is_null( $this->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'street_line1', __( 'Address is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'state_code', __( 'State code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {

		if( true == is_null( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'postal_code', __( 'Postal code is required.' ) ) );
		} elseif( false == CValidation::validatePostalCode( $this->getPostalCode(), CCountry::CODE_USA ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'postal_code', __( 'Valid Postal code is required.' ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valPhoneNumber1() {

		if( true == is_null( $this->getPhoneNumber1() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is required.' ) ) );
		} elseif( ( CWorker::MIN_PHONE_NUMBER_SIZE > strlen( $this->getPhoneNumber1() ) || CWorker::MAX_PHONE_NUMBER_SIZE < strlen( $this->getPhoneNumber1() ) ) || false == CValidation::validateFullPhoneNumber( $this->getPhoneNumber1(), false ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid.' ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valPhoneNumber2() {
		$boolIsValid = true;

		if( false == is_null( $this->getPhoneNumber2() ) && ( ( CWorker::MIN_PHONE_NUMBER_SIZE > strlen( $this->getPhoneNumber2() ) || CWorker::MAX_PHONE_NUMBER_SIZE < strlen( $this->getPhoneNumber2() ) ) || false == CValidation::validateFullPhoneNumber( $this->getPhoneNumber2(), false ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number2', __( 'Fax number is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCity() {

		if( true == is_null( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'city', __( 'City is required.' ) ) );
		} elseif( true == is_numeric( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'city', __( 'City can not be numeric.' ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valPasswordEncrypted() {
		return true;
	}

	public function valBirthDate() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strBirthDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'birth_date', __( 'Birth date is required.' ) ) );
		}
		if( true == isset( $this->m_strBirthDate ) && ( false == CValidation::checkBirthDateFormat( $this->m_strBirthDate ) || strtotime( $this->m_strBirthDate ) > time() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'birth_date', __( 'A valid birth date is required.' ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 */

	public function setPassword( $strPassword ) {
		$this->m_strPassword = CStrings::strTrimDef( $strPassword, 240, NULL, true );
		if( true == valStr( $strPassword ) ) {
			$this->m_strPasswordEncrypted = \Psi\Libraries\Cryptography\CCrypto::createService()->hash( $this->m_strPassword );
		}
	}

	public function setConfirmPassword( $strConfirmPassword ) {
		$this->m_strConfirmPassword = CStrings::strTrimDef( $strConfirmPassword, 240, NULL, true );
	}

	public function setCurrentPassword( $strCurrentPassword ) {
		$this->m_strCurrentPassword = CStrings::strTrimDef( $strCurrentPassword, 240, NULL, true );
	}

	public function setVendorPreferenceValue( $strVendorPreferenceValue ) {
		$this->m_intVendorPreferenceValue = $strVendorPreferenceValue;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['password'] ) ) {
			$this->setPassword( $arrmixValues['password'] );
		}
		if( true == isset( $arrmixValues['is_system_generated_password'] ) ) {
			$this->setSystemGeneratedPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_system_generated_password'] ) : $arrmixValues['is_system_generated_password'] );
		}

		if( true == isset( $arrmixValues['is_none_given'] ) ) {
			$this->setIsNoneGiven( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_none_given'] ) : $arrmixValues['is_none_given'] );
		}

		if( true == isset( $arrmixValues['worker_store_id'] ) ) {
			$this->setWorkerStoreId( $arrmixValues['worker_store_id'] );
		}

		if( true == isset( $arrmixValues['confirm_password'] ) ) {
			$this->setConfirmPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['confirm_password'] ) : $arrmixValues['confirm_password'] );
		}

		if( true == isset( $arrmixValues['current_password'] ) ) {
			$this->setCurrentPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['current_password'] ) : $arrmixValues['current_password'] );
		}

		if( true == isset( $arrmixValues['subdomain'] ) ) {
			$this->setSubdomain( $arrmixValues['subdomain'] );
		}

		if( true == isset( $arrmixValues['is_primary'] ) && $boolDirectSet ) {
			$this->m_boolIsPrimary = trim( stripcslashes( $arrmixValues['is_primary'] ) );
		} elseif( isset( $arrmixValues['is_primary'] ) ) {
			$this->setIsPrimary( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_primary'] ) : $arrmixValues['is_primary'] );
		}

		if( true == isset( $arrmixValues['default_vendor_entity_id'] ) ) {
			$this->setDefaultVendorEntityId( $arrmixValues['default_vendor_entity_id'] );
		}

		if( true == isset( $arrmixValues['store_id'] ) ) {
			$this->setStoreId( $arrmixValues['store_id'] );
		}

		if( true == isset( $arrmixValues['pool_name'] ) ) {
			$this->setPoolName( $arrmixValues['pool_name'] );
		}

		if( true == isset( $arrmixValues['vendor_preference_value'] ) ) {
			$this->setVendorPreferenceValue( $arrmixValues['vendor_preference_value'] );
		}

		$this->setIsPSIUser();
	}

	public function setIsPSIUser() {
		// As first 10000 records in company_users table are reserved for PSI users.
		$this->m_boolIsPSIUser = ( 10000 > $this->getId() ) ? true : false;
	}

	public function setIsPrimary( $boolIsPrimaryContact ) {
		$this->m_boolIsPrimary = CStrings::strToBool( $boolIsPrimaryContact );
	}

	public function setSubdomain( $strSubDomain ) {
		$this->m_strSubDomain = CStrings::strTrimDef( $strSubDomain, 240, NULL, true );
	}

	public function setWorkerStoreId( $strWorkerStoreId ) {
		$this->m_intWorkerStoreId = $strWorkerStoreId;
	}

	public function setDefaultVendorEntityId( $intDefaultVendorEntityId ) {
		$this->m_intDefaultVendorEntityId = $intDefaultVendorEntityId;
	}

	public function setStoreId( $intStoreId ) {
		$this->m_intStoreId = $intStoreId;
	}

	public function setIsNoneGiven( $boolIsNoneGiven ) {
		 $this->m_boolIsNoneGiven = CStrings::strToBool( $boolIsNoneGiven );
	}

	public function setPoolName( $strPoolName ) {
		$this->m_strPoolName = $strPoolName;
	}

	public function setSystemGeneratedPassword( $boolIsSystemGeneratePassword ) {
		$this->m_boolIsSystemGeneratedPassword = CStrings::strToBool( $boolIsSystemGeneratePassword );
	}

	public function setLoginAttemptData( $boolIsLoggedIn = false ) {
		if( true == $boolIsLoggedIn ) {
			$this->setLoginAttemptCount( 0 );
		} elseif( time() > ( strtotime( date( 'c', strtotime( $this->getLastLoginAttemptOn() ) ) ) + self::LOGIN_ATTEMPT_WAITING_TIME ) && 0 < $this->getLoginAttemptCount() ) {
			$this->setLoginAttemptCount( 1 );
		} else {
			$this->setLoginAttemptCount( $this->getLoginAttemptCount() + 1 );
		}

		$this->setLastLoginAttemptOn( date( 'c', time() ) );
	}

	/**
	 * Get Functions
	 */

	public function getDecryptedTaxNumber() {
		if( false == valStr( $this->m_strTaxNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function getTaxNumberMasked() {
		if( CWorker::MAX_TAX_NUMBER == strlen( $this->getDecryptedTaxNumber() ) ) {
			return $this->getTaxNumberEncrypted();
		}

		return CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $this->getDecryptedTaxNumber() ) );
	}

	public function encryptTaxNumber() {
		if( false == is_null( $this->getTaxNumberEncrypted() ) ) {
			$this->setTaxNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		}
	}

	public function getVendorPreferenceValue() {
		return $this->m_intVendorPreferenceValue;
	}

	public function getIsPSIUser() {

		if( false == isset( $this->m_boolIsPSIUser ) ) {
			$this->setIsPSIUser();
		}
		return $this->m_boolIsPSIUser;
	}

	public function getIsPrimary() {
		return $this->m_boolIsPrimary;
	}

	public function getSubdomain() {
		return $this->m_strSubDomain;
	}

	public function getConfirmPassword() {
		return $this->m_strConfirmPassword;
	}

	public function getCurrentPassword() {
		return $this->m_strCurrentPassword;
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function getWorkerStoreId() {
		return $this->m_intWorkerStoreId;
	}

	public function getDefaultVendorEntityId() {
		return $this->m_intDefaultVendorEntityId;
	}

	public function getStoreId() {
		return $this->m_intStoreId;
	}

	public function getIsNoneGiven() {
		return $this->m_boolIsNoneGiven;
	}

	public function getPoolName() {
		return $this->m_strPoolName;
	}

	public function getSystemGeneratedPassword() {
		return $this->m_boolIsSystemGeneratedPassword;
	}

	public function validate( $strAction, $objDatabase = NULL, $intDutyTypeId = NULL, $boolIsCheckPassword = false, $boolIsPsiUser = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress( $objDatabase );
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valConfirmPassword();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valPostalCode();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber1();
				$boolIsValid &= $this->valPhoneNumber2();
				$boolIsValid &= $this->valEmailAddress( $objDatabase );

				if( true == $boolIsCheckPassword ) {
					$boolIsValid &= $this->valCurrentPassword( $objDatabase );
					$boolIsValid &= $this->valPassword( true );
					$boolIsValid &= $this->valConfirmPassword( true );
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_new_worker':
				$boolIsValid &= $this->valTaxNumberEncrypted( $intDutyTypeId, $boolIsPsiUser );
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber1();
				$boolIsValid &= $this->valPhoneNumber2();
				$boolIsValid &= $this->valEmailAddress( $objDatabase );

				if( true == $this->getIsPrincipal() ) {

					if( false == $this->getIsNoneGiven() ) {
						$boolIsValid &= $this->valNameMiddle();
					}

					$boolIsValid &= $this->valBirthDate();
					$boolIsValid &= $this->valStreetLine1();
					$boolIsValid &= $this->valCity();
					$boolIsValid &= $this->valStateCode();
					$boolIsValid &= $this->valPostalCode();
				}

				if( true == $boolIsCheckPassword ) {
					$boolIsValid &= $this->valPassword();
					$boolIsValid &= $this->valConfirmPassword();
				}
				break;

			case 'update_password':
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valConfirmPassword();
				break;

			case self::VALIDATE_INITIAL_SETUP:
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress( $objDatabase );
				$boolIsValid &= $this->valPhoneNumber1();
				$boolIsValid &= $this->valPhoneNumber2();
				break;

			case self::VALIDATE_BUSINESS_OWNER:
				$boolIsValid = $this->validateBusinessOwner();
				break;

			case VALIDATE_SCREENING:
				$boolIsValid &= $this->valNameFirst();
				if( false == $this->getIsNoneGiven() ) {
					$boolIsValid &= $this->valNameMiddle();
				}
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valBirthDate();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valPostalCode();
				break;

			case 'validate_login_details':
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPassword();

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchWorkerPreferenceByKey( $strKey, $objDatabase ) {
		return \Psi\Eos\Utilities\CWorkerPreferences::createService()->fetchWorkerPreferenceByWorkerIdByVendorIdByKey( $this->getId(), $this->getVendorId(), $strKey, $objDatabase );
	}

	/**
	 * Create Functions
	 */

	public function createWorkerPreference( $strKey, $strValue ) {
		$objWorkerPreference = new CWorkerPreference();

		$objWorkerPreference->setKey( $strKey );
		$objWorkerPreference->setValue( $strValue );
		$objWorkerPreference->setWorkerId( $this->getId() );
		$objWorkerPreference->setVendorId( $this->getVendorId() );

		return $objWorkerPreference;
	}

	/**
	 * Other Functions
	 */

	public function login( $objDatabase ) {
		$objWorker = $this->fetchWorker( $objDatabase );

		if( false == valObj( $objWorker, CWorker::class ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', __( 'Invalid username and/or password.' ), 304 ) );
			return false;
		}

		if( false == \Psi\Libraries\Cryptography\CCrypto::createService()->verifyHash( $this->getPassword(), $objWorker->getPasswordEncrypted() ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', __( 'Invalid username and/or password.' ), 304 ) );
			return false;
		}

		return $objWorker;
	}

	public function createVendorDuty() {
		$objVendorDuty 	= new CVendorDuty();
		$objVendorDuty->setVendorId( $this->getId() );

		return $objVendorDuty;
	}

	public function createOrFetchWorkerToken( $objDatabase ) {

		$objWorkerToken = \Psi\Eos\Utilities\CWorkerTokens::createService()->fetchWorkerTokenByWorkerIdByVendorId( $this->getId(), $this->getVendorId(), $objDatabase );

		if( false == valObj( $objWorkerToken, 'CWorkerToken' ) ) {
			$objWorkerToken = new CWorkerToken();
			$objWorkerToken->setWorkerId( $this->getId() );
			$objWorkerToken->setVendorId( $this->getVendorId() );
		}

		// Always reset token to current Time.
		$objWorkerToken->setToken( md5( time() ) );

		if( ( false == $objWorkerToken->validate( VALIDATE_UPDATE ) ) || ( false == $objWorkerToken->insertOrUpdate( $this->getId(), $objDatabase ) ) ) {
			$this->addErrorMsg( __( 'Failed to insert or update company worker token.' ) );
			return NULL;
		}
		return $objWorkerToken;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objDatabase );
	}

	public function setTaxNumber( $strPlainTaxNumber ) {
		if( true == \Psi\CStringService::singleton()->stristr( $strPlainTaxNumber, '*' ) ) {
			return;
		}

		$this->setTaxNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( preg_replace( '/[^a-z0-9]/i', '', $strPlainTaxNumber ), CONFIG_SODIUM_KEY_TAX_NUMBER ) );
	}

	public function validateBusinessOwner() {

		$boolIsValid = true;

		if( true == is_null( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'business_owner_name_first', __( 'First name is required.' ) ) );
		}

		if( false == $this->getIsNoneGiven() && true == is_null( $this->getNameMiddle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'business_owner_name_middle', __( 'Middle name is required.' ) ) );
		}

		if( true == is_null( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'business_owner_name_last', __( ' Last name is required.' ) ) );
		}

		if( false == is_null( $this->getPostalCode() ) ) {
			if( false == CValidation::validatePostalCode( $this->getPostalCode(), CCountry::CODE_USA ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'business_owner_postal_code', __( 'Valid Postal code is required.' ) ) );
			} elseif( true == preg_match( '/[^a-zA-Z0-9\-]/', $this->getPostalCode() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'business_owner_postal_code', __( 'Special characters/space are not allowed in postal code.' ) ) );
			}
		}

		if( true == isset( $this->m_strBirthDate ) && ( false == CValidation::checkBirthDateFormat( $this->m_strBirthDate ) || strtotime( $this->m_strBirthDate ) > time() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'business_owner_birth_date', __( 'A valid birth date is required.' ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * @param $objDatabase
	 * @return \CWorker
	 */
	public function fetchWorker( $objDatabase ) {
		return \Psi\Eos\Utilities\CWorkers::createService()->fetchActiveWorkerByEmailAddressBySubDomainOrByVendorId( $this->getEmailAddress(), $objDatabase, $this->getSubdomain(), $this->getVendorId() );
	}

}
?>