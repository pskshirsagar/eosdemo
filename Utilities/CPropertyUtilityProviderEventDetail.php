<?php

class CPropertyUtilityProviderEventDetail extends CBasePropertyUtilityProviderEventDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUtilityProviderEventId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityProviderEventTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConfirmedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConfirmedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConfirmUtilityDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFollowUpDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConfirmedNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>