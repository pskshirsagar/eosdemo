<?php

class CComplianceDocLimit extends CBaseComplianceDocLimit {

	protected $m_intComplianceDocId;

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valComplianceDocPolicyId() {
		return true;
	}

	public function valComplianceItemId() {
		return true;
	}

	public function valAmount() {
		return true;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setComplianceDocId( $intComplianceDocId ) {
		$this->m_intComplianceDocId = $intComplianceDocId;
	}

	public function getComplianceDocId() {
		return $this->m_intComplianceDocId;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['compliance_doc_id'] ) ) {
			$this->setComplianceDocId( $arrmixValues['compliance_doc_id'] );
		}
	}

}
?>