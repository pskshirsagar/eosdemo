<?php

class CUtilityProviderEventType extends CBaseUtilityProviderEventType {

	const CHANGE_OF_ADDRESS = 1;
	const LETTER_OF_AUTHORIZATION = 2;
	const REVERT_TO_OWNER_AGREEMENT = 3;

	public static function getUtilityProviderEventTypeName( $intUtilityProviderEventTypeId ) {
		$strUtilityProviderEventTypeName = '';

		switch( $intUtilityProviderEventTypeId ) {
			case self::CHANGE_OF_ADDRESS:
				$strUtilityProviderEventTypeName = 'Change of Address';
				break;

			case self::LETTER_OF_AUTHORIZATION:
				$strUtilityProviderEventTypeName = 'Letter of Authorization';
				break;

			case self::REVERT_TO_OWNER_AGREEMENT:
				$strUtilityProviderEventTypeName = 'Revert to Owner Agreement';
				break;

			default:
				// No default value.
		}

		return $strUtilityProviderEventTypeName;
	}

	public static $c_arrintLOAAndCOA	= [
		self::CHANGE_OF_ADDRESS,
		self::LETTER_OF_AUTHORIZATION
	];

	public static $c_arrintLOAAndCOAAndRTO	= [
		self::CHANGE_OF_ADDRESS,
		self::LETTER_OF_AUTHORIZATION,
		self::REVERT_TO_OWNER_AGREEMENT
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>