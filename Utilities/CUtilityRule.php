<?php

class CUtilityRule extends CBaseUtilityRule {

	const MAXIMUM_LATE_FEE	= 71;
	const NUMBER_OF_RESIDENT_IS_ALLOWED_TO_PAY_BILL = 76;
	const CONVERGENT_MAXIMUM_RESIDENT_FEE = 77;
	const PERCENT_LATE_FEE = 103;
	const BILL_PERIOD_MUST_BE_SAME_AS_CONSUMPTION_PERIOD = 104;

	protected $m_objUtilityRuleType;

	protected $m_arrobjUtilityRuleAssociations;

	protected $m_strPostalCode;
	protected $m_strStateCode;
	protected $m_strUtilityName;
	protected $m_strUtilityRuleSetName;
	protected $m_strUtilityRuleTypeName;
	protected $m_strUtilityRuleTypeValueName;
	protected $m_strUtilityRuleSetStateCode;
	protected $m_strUtilityRuleTypeValueStringValue;

    protected $m_intUtilityRuleTypeValueMaxNumericValue;
    protected $m_intUtilityRuleTypeValueMinNumericValue;

    protected $m_fltUtilityRuleTypeValueMaxPercentageValue;
    protected $m_fltUtilityRuleTypeValueMinPercentageValue;

    protected $m_boolIsDeleteAssociation;
    protected $m_boolUtilityRuleTypeValueurtvBooleanValue;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_rule_type_name'] ) ) {
			$this->setUtilityRuleTypeName( $arrmixValues['utility_rule_type_name'] );
		}
		if( true == isset( $arrmixValues['utility_rule_type_value_name'] ) ) {
			$this->setUtilityRuleTypeValueName( $arrmixValues['utility_rule_type_value_name'] );
		}
		if( true == isset( $arrmixValues['utility_rule_input_option_id'] ) ) {
			$this->setUtilityRuleInputOptionId( $arrmixValues['utility_rule_input_option_id'] );
		}

		if( true == isset( $arrmixValues['urtv_string_value'] ) ) {
			$this->setUtilityRuleTypeValueStringValue( $arrmixValues['urtv_string_value'] );
		}
		if( true == isset( $arrmixValues['urtv_min_numeric_value'] ) ) {
			$this->setUtilityRuleTypeValueMinNumericValue( $arrmixValues['urtv_min_numeric_value'] );
		}
		if( true == isset( $arrmixValues['urtv_max_numeric_value'] ) ) {
			$this->setUtilityRuleTypeValueMaxNumericValue( $arrmixValues['urtv_max_numeric_value'] );
		}
		if( true == isset( $arrmixValues['urtv_min_percentage_value'] ) ) {
			$this->setUtilityRuleTypeValueMinPercentageValue( $arrmixValues['urtv_min_percentage_value'] );
		}
		if( true == isset( $arrmixValues['urtv_max_percentage_value'] ) ) {
			$this->setUtilityRuleTypeValueMaxPercentageValue( $arrmixValues['urtv_max_percentage_value'] );
		}
		if( true == isset( $arrmixValues['urtv_boolean_value'] ) ) {
			$this->setUtilityRuleTypeValueurtvBooleanValue( $arrmixValues['urtv_boolean_value'] );
		}
		if( true == isset( $arrmixValues['postal_code'] ) ) {
			$this->setPostalCode( $arrmixValues['postal_code'] );
		}
		if( true == isset( $arrmixValues['state_code'] ) ) {
			$this->setStateCode( $arrmixValues['state_code'] );
		}
		if( true == isset( $arrmixValues['utility_name'] ) ) {
			$this->setUtilityName( $arrmixValues['utility_name'] );
		}

	}

    public function valUtilityRuleTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityRuleTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_rule_type_id', 'Rule type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valUtilityRuleInputOptionId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getUtilityRuleInputOptionId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_rule_input_option_id', 'Data type is required.' ) );
    	}

    	return $boolIsValid;
    }

	public function valStringValue() {
        $boolIsValid = true;

		if( false == is_null( $this->getStringValue() ) && $this->getStringValue() != strip_tags( $this->getStringValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'string_value', 'Message cannot contain HTML tag or JS code.' ) );
		}

        return $boolIsValid;
    }

    public function valMinNumericValue( $intUtilityInputOptionId ) {
        $boolIsValid = true;

        if( true == is_null( $this->getMinNumericValue() ) || false == is_numeric( $this->getMinNumericValue() ) ) {

			$boolIsValid = false;
        	if( $intUtilityInputOptionId == CUtilityRuleInputOption::UTILITY_INPUT_OPTION_SINGLE_NUMBER ) {
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_numeric_value', 'Numeric value is required.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_numeric_value', 'Min numeric value is required.' ) );
			}

        }

        return $boolIsValid;
    }

    public function valMaxNumericValue() {
        $boolIsValid = true;

    	if( true == is_null( $this->getMaxNumericValue() ) ) {

    		$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_numeric_value', 'Max numeric value is required.' ) );

        } elseif( false == is_numeric( $this->getMinNumericValue() ) ) {

        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_numeric_value', 'Max numeric value must be numeric.' ) );

        } elseif( $this->getMinNumericValue() >= $this->getMaxNumericValue() ) {

        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_numeric_value', 'Min numeric value must be less than max numeric value.' ) );

        }

        return $boolIsValid;
    }

	public function valMinPercentageValue( $intUtilityInputOptionId ) {
        $boolIsValid = true;

        if( false == is_numeric( $this->getMinPercentageValue() ) || true == is_null( $this->getMinPercentageValue() ) ) {

			$boolIsValid = false;
        	if( $intUtilityInputOptionId == CUtilityRuleInputOption::UTILITY_INPUT_OPTION_SINGLE_PERCENTAGE ) {
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_percentage_value', 'Percentage value is required.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_percentage_value', 'Min percentage value is required.' ) );
			}

        } elseif( 100 < $this->getMinPercentageValue() ) {
        	$boolIsValid = false;
        	if( $intUtilityInputOptionId == CUtilityRuleInputOption::UTILITY_INPUT_OPTION_SINGLE_PERCENTAGE ) {
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_percentage_value', 'Percentage value must be less than or equal to 100.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_percentage_value', 'Min percentage value must be less than or equal to 100.' ) );
			}
        }

        return $boolIsValid;
    }

    public function valMaxPercentageValue() {
        $boolIsValid = true;

    	if( true == is_null( $this->getMaxPercentageValue() ) ) {

    		$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_percentage_value', 'Max percentage value is required.' ) );

        } elseif( false == is_numeric( $this->getMaxPercentageValue() ) ) {

        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_percentage_value', 'Max percentage value must be numeric.' ) );

        } elseif( 100 < $this->getMaxPercentageValue() ) {

        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_percentage_value', 'Max percentage value must be less than or equal to 100.' ) );

        } elseif( $this->getMinPercentageValue() >= $this->getMaxPercentageValue() ) {

        		$boolIsValid = false;
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_percentage_value', 'Min percentage value must be less than max percentage value.' ) );
        }

        return $boolIsValid;
    }

    public function valBooleanValue() {
        $boolIsValid = true;

        if( true == is_null( $this->getBooleanValue() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'boolean_value', 'Boolean value is required.' ) );
        } elseif( false == preg_match( '/^[0-1]*$/', $this->getBooleanValue() ) ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'boolean_value', 'Boolean value must be 0 or 1.' ) );
        }

        return $boolIsValid;
    }

 	public function valReferenceDescription() {
        $boolIsValid = true;

        if( true == is_null( $this->getReferenceDescription() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference_description', 'Reference description is required.' ) );
        } elseif( $this->getReferenceDescription() != strip_tags( $this->getReferenceDescription() ) ) {
            $boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference_description', 'Message cannot contain HTML tag or JS code.' ) );
        }

        return $boolIsValid;
    }

    public function valDataTypeValue() {

    	$boolIsValid = true;

    	switch( $this->getUtilityRuleInputOptionId() ) {

    		case CUtilityRuleInputOption::UTILITY_INPUT_OPTION_STRING:
    			$boolIsValid = $this->valStringValue();
    			break;

    		case CUtilityRuleInputOption::UTILITY_INPUT_OPTION_SINGLE_NUMBER:
    		case CUtilityRuleInputOption::UTILITY_INPUT_OPTION_MIN_MAX_NUMERIC_RANGE:

   				$boolIsValid = $this->valMinNumericValue( $this->getUtilityRuleInputOptionId() );

   				if( CUtilityRuleInputOption::UTILITY_INPUT_OPTION_MIN_MAX_NUMERIC_RANGE == $this->getUtilityRuleInputOptionId() ) {

   					$boolIsValid &= $this->valMaxNumericValue();
   				}
   				break;

			case CUtilityRuleInputOption::UTILITY_INPUT_OPTION_SINGLE_PERCENTAGE:
			case CUtilityRuleInputOption::UTILITY_INPUT_OPTION_MIN_MAX_PERCENTAGE_RANGE:

				$boolIsValid = $this->valMinPercentageValue( $this->getUtilityRuleInputOptionId() );

				if( CUtilityRuleInputOption::UTILITY_INPUT_OPTION_MIN_MAX_PERCENTAGE_RANGE == $this->getUtilityRuleInputOptionId() ) {

					$boolIsValid &= $this->valMaxPercentageValue();
					$boolIsValid &= $this->valBooleanValue();
				}
				break;

			default:
				$boolIsValid = true;
				break;
    	}

    	return $boolIsValid;

    }

	public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:

				$boolIsValid				&= $this->valUtilityRuleTypeId();
        		$boolIsValid				&= $this->valDataTypeValue();
				$boolIsValid				&= $this->valUtilityRuleInputOptionId();
            	$boolIsValid				&= $this->valReferenceDescription();
            	$boolIsValid                &= $this->valStringValue();
				break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valDependency( $objDatabase );
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function getUtilityRuleTypeName() {
		return $this->m_strUtilityRuleTypeName;
	}

	public function getUtilityRuleTypeValueName() {
		return $this->m_strUtilityRuleTypeValueName;
	}

	public function setUtilityRuleTypeName( $strName ) {
		$this->m_strUtilityRuleTypeName = $strName;
	}

	public function setUtilityRuleTypeValueName( $strName ) {
		$this->m_strUtilityRuleTypeValueName = $strName;
	}

	public function setUtilityRuleType( $objUtilityRuleType ) {
    	$this->m_objUtilityRuleType = $objUtilityRuleType;
    }

	public function getUtilityRuleType() {
    	return $this->m_objUtilityRuleType;
    }

	public function getIsDeleteAssociation() {
   		return $this->m_boolIsDeleteAssociation;
	}

	public function setIsDeleteAssociation( $boolIsDeleteAssociation ) {
   		$this->m_boolIsDeleteAssociation = $boolIsDeleteAssociation;
    }

    public function createUtilityRule() {

    	$this->setMaxNumericValue( NULL );
		$this->setMinNumericValue( NULL );
		$this->setMaxPercentageValue( NULL );
		$this->setMinPercentageValue( NULL );
		$this->setStringValue( NULL );
		$this->setBooleanValue( false );
    }

	public function addUtilityRuleAssociation( $objUtilityRuleAssociation ) {
		$this->m_arrobjUtilityRuleAssociations[$objUtilityRuleAssociation->getId()] = $objUtilityRuleAssociation;
	}

	public function getUtilityRuleAssociations() {
		return $this->m_arrobjUtilityRuleAssociations;
	}

	public function valDependency( $objDatabase ) {

		$boolIsValid = true;
		$intCount = \Psi\Eos\Utilities\CUtilityRuleAssociations::createService()->fetchUtilityRuleAssociationCount( ' WHERE utility_rule_id = ' . ( int ) $this->getId(), $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_rule_association', 'Failed to delete the utility rule, it is being associated with utility association(s).' ) );
		}

		$intCount = \Psi\Eos\Utilities\CUtilityRuleOverrides::createService()->fetchUtilityRuleOverrideCount( ' WHERE utility_rule_id = ' . ( int ) $this->getId(), $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_rule_override', 'Failed to delete the utility rule, it is being associated with utility rule override(s).' ) );
		}
		return $boolIsValid;

	}

	public function createUtilityRuleAssociation() {
		$objUtilityRuleAssociation = new CUtilityRuleAssociation();
		$objUtilityRuleAssociation->setUtilityRuleId( $this->getId() );
		return $objUtilityRuleAssociation;
	}

	public function createUtilityRuleUtilityType( $intUtilityTypeId ) {
		$objUtilityRuleUtilityType = new CUtilityRuleUtilityType();
		$objUtilityRuleUtilityType->setUtilityRuleId( $this->getId() );
		$objUtilityRuleUtilityType->setUtilityTypeId( $intUtilityTypeId );
		return $objUtilityRuleUtilityType;
	}

	public function fetchUniqueUtilityRuleAssociations( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityRuleAssociations::createService()->fetchUniqueUtilityRuleAssociationsByUtilityRuleId( $this->getId(), $objDatabase );
    }

    public function setUtilityRuleTypeValueMaxPercentageValue( $fltUtilityRuleTypeValueMaxPercentageValue ) {
		$this->m_fltUtilityRuleTypeValueMaxPercentageValue = CStrings::strToFloatDef( $fltUtilityRuleTypeValueMaxPercentageValue, NULL, false, 6 );
    }

    public function setUtilityRuleTypeValueMinPercentageValue( $fltUtilityRuleTypeValueMinPercentageValue ) {
		$this->m_fltUtilityRuleTypeValueMinPercentageValue = CStrings::strToFloatDef( $fltUtilityRuleTypeValueMinPercentageValue, NULL, false, 6 );
    }

    public function setUtilityRuleTypeValueMaxNumericValue( $intUtilityRuleTypeValueMinNumericValue ) {
		$this->m_intUtilityRuleTypeValueMinNumericValue = CStrings::strToIntDef( $intUtilityRuleTypeValueMinNumericValue );
    }

    public function setUtilityRuleTypeValueMinNumericValue( $intUtilityRuleTypeValueMaxNumericValue ) {
		$this->m_intUtilityRuleTypeValueMaxNumericValue = CStrings::strToIntDef( $intUtilityRuleTypeValueMaxNumericValue );

    }

    public function setUtilityRuleTypeValueStringValue( $strUtilityRuleTypeValueStringValue ) {
		$this->m_strUtilityRuleTypeValueStringValue = $strUtilityRuleTypeValueStringValue;
    }

    public function setUtilityRuleTypeValueurtvBooleanValue( $boolUtilityRuleTypeValueurtvBooleanValue ) {
		$this->m_boolUtilityRuleTypeValueurtvBooleanValue = $boolUtilityRuleTypeValueurtvBooleanValue;
    }

    public function getUtilityRuleTypeValueMaxPercentageValue() {
		return $this->m_fltUtilityRuleTypeValueMaxPercentageValue;
    }

    public function getUtilityRuleTypeValueMinPercentageValue() {
		return $this->m_fltUtilityRuleTypeValueMinPercentageValue;
    }

    public function getUtilityRuleTypeValueMaxNumericValue() {
		return $this->m_intUtilityRuleTypeValueMinNumericValue;
    }

    public function getUtilityRuleTypeValueMinNumericValue() {
		return $this->m_intUtilityRuleTypeValueMaxNumericValue;
    }

    public function getUtilityRuleTypeValueStringValue() {
		return $this->m_strUtilityRuleTypeValueStringValue;
    }

    public function getUtilityRuleTypeValueurtvBooleanValue() {
		return $this->m_boolUtilityRuleTypeValueurtvBooleanValue;
    }

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getUtilityName() {
		return $this->m_strUtilityName;
	}

	public function setUtilityName( $strUtilityName ) {
		$this->m_strUtilityName = $strUtilityName;
	}

	public function createUtilityRuleOverride( $objUser ) {
		$objUtilityRuleOverride = new CUtilityRuleOverride();

		$objUtilityRuleOverride->setCid( $this->getCid() );
		$objUtilityRuleOverride->setPropertyId( $this->getPropertyId() );
		$objUtilityRuleOverride->setUtilityRuleId( $this->getId() );
		$objUtilityRuleOverride->setOverrideCreatedOn( 'NOW()' );
		$objUtilityRuleOverride->setOverrideCreatedBy( $objUser->getId() );

		return $objUtilityRuleOverride;
	}

}
?>