<?php

class CUtilityBillAnomalyType extends CBaseUtilityBillAnomalyType {

	const BILL_MISSING 	= 1;
	const EXCESS_USAGE 	= 2;
	const OCR_FAILED 	= 3;

}
?>