<?php

class CUtilityUemSetting extends CBaseUtilityUemSetting {

	const AUDIT_COMPANY_CONTACT_IDS = 'audit_company_contact_ids';

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		// utility uem setting
		$arrstrTempUtilityUemSettingOriginalValues = $arrstrUtilityUemSettingOriginalValueChanges = $arrstrUtilityUemSettingOriginalValues = [];
		$boolUtilityUemSettingChangeFlag = false;

		$arrmixUtilityUemSettingOriginalValues = fetchData( 'SELECT * FROM utility_uem_settings WHERE property_utility_setting_id = ' . ( int ) $this->getPropertyUtilitySettingId(), $objDatabase );

		$arrstrTempUtilityUemSettingOriginalValues = $arrmixUtilityUemSettingOriginalValues[0];

		foreach( $arrstrTempUtilityUemSettingOriginalValues as $strKey => $mixUtilityUemSettingOriginalValue ) {

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strFunctionName    = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			if( ( false == CStrings::strToBool( $mixUtilityUemSettingOriginalValue ) && '' == $this->$strFunctionName() ) || ( true == CStrings::strToBool( $mixUtilityUemSettingOriginalValue ) && '1' == $this->$strFunctionName() ) ) {
				continue;
			}

			if( CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $mixUtilityUemSettingOriginalValue ) {
				$arrstrUtilityUemSettingOriginalValueChanges[$strKey] = $this->$strFunctionName();
				$arrstrUtilityUemSettingOriginalValues[$strKey]       = $mixUtilityUemSettingOriginalValue;
				$boolUtilityUemSettingChangeFlag                      = true;
			}
		}

		foreach( $arrstrUtilityUemSettingOriginalValueChanges as $strRequiredKey => $arrstrUtilityUemSettingOriginalValueChange ) {
			if( self::AUDIT_COMPANY_CONTACT_IDS == $strRequiredKey ) {
				$arrstrUtilityUemSettingOriginalValueChanges[$strRequiredKey] = implode( ',', $arrstrUtilityUemSettingOriginalValueChange );
			}
		}

		$boolUtilityUemSettingUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUtilityUemSettingUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolUtilityUemSettingChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempUtilityUemSettingOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempUtilityUemSettingOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'utility_uem_settings' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrUtilityUemSettingOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrUtilityUemSettingOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}

		}

		if( true == $boolUtilityUemSettingUpdateStatus ) {
			return true;
		} else {
			return false;
		}

	}

}
?>