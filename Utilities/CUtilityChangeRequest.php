<?php

class CUtilityChangeRequest extends CBaseUtilityChangeRequest {

	protected $m_strResidentWorksUrl;

	public function valUtilityChangeRequestTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityChangeRequestTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_change_request_type_id', 'Request type is reqiured.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Company is reqiured.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is reqiured.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_account_id', 'Utility account is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUtilityChangeRequestTypeId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valUtilityAccountId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
            	// default case
            	$boolIsValid = true;
            	break;
		}

		return $boolIsValid;
	}

	public function fetchPropertyUtilitySetting( $objDatabase ) {

		$objPropertyUtilitySetting = \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingByPropertyId( $this->getPropertyId(), $objDatabase );
		return $objPropertyUtilitySetting;
	}

	public function fetchUtilityAccount( $objDatabase ) {

		$objUtilityAccount = \Psi\Eos\Utilities\CUtilityAccounts::createService()->fetchUtilityAccountById( $this->getUtilityAccountId(), $objDatabase );
		return $objUtilityAccount;
	}

	public function sendUtilityChangeRequestEmailAlert( $arrstrEmailAddrresses, $arrobjUtilityChangeRequestTypes, $objUtilityAccount, $objCurrentEmployee, $objEmailDatabase, $strRwxSuffix, $objAdminDatabase ) {

		$objClient = CClients::fetchClientById( $this->getCid(), $objAdminDatabase );

		$strRwxSuffix 	= ( false == is_null( $strRwxSuffix ) ) ? $strRwxSuffix : CONFIG_RWX_LOGIN_SUFFIX;

		if( true == valObj( $objClient, 'CClient' ) ) {
			$this->m_strResidentWorksUrl = 'http://' . $objClient->getRwxDomain() . $strRwxSuffix . '/?module=utilities_system&utility_change_request_id=' . $this->getId() . '&is_from_email=1';
		}

		$strHtmlEmailOutput = $this->buildHtmlUtilityChangeRequestEmailContent( $arrobjUtilityChangeRequestTypes, $objUtilityAccount, $objCurrentEmployee );

		/**
		 * INSERT SYSTEM EMAILS
		 */

		if( false == valArr( $arrstrEmailAddrresses ) ) {
			return false;
		}

		foreach( $arrstrEmailAddrresses as $strEmailAddress ) {
			$objSystemEmail = new CSystemEmail();

			// set data
			$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_BILLING_EMAIL_ADDRESS );

			$strSubject = ' Utility Change Request Approval for #' . $this->getId();

			$objSystemEmail->setSubject( $strSubject );
			$objSystemEmail->setToEmailAddress( $strEmailAddress );
			$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
			$objSystemEmail->setCid( $this->getCid() );
			$objSystemEmail->setPropertyId( $this->getPropertyId() );
			$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::COMPANY_EMPLOYEE );

			if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
				trigger_error( 'System emailed failed to insert.', E_USER_ERROR );
				exit;
			}
		}

		return true;
	}

	public function buildHtmlUtilityChangeRequestEmailContent( $arrobjUtilityChangeRequestTypes, $objUtilityAccount, $objCurrentEmployee ) {

		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'utility_change_request',       $this );
		$objSmarty->assign( 'utility_change_request_types', $arrobjUtilityChangeRequestTypes );
		$objSmarty->assign( 'utility_account',              $objUtilityAccount );
		$objSmarty->assign( 'employee',                     $objCurrentEmployee );
		$objSmarty->assign( 'task_reuqest_uri', 			'http://' . basename( $_SERVER['HTTP_HOST'] ) );
		$objSmarty->assign( 'residentworks_url',            $this->m_strResidentWorksUrl );
		$objSmarty->assign( 'base_uri',		                CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX',   'entrata_' );
		$objSmarty->assign( 'image_url',	                CONFIG_COMMON_PATH );

		return $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'utilities/utility_change_requests/utility_change_request_email_format.tpl', PATH_INTERFACES_ADMIN . 'common/layouts/utility_email.tpl' );
	}

}
?>