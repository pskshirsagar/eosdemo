<?php

class CUtilityProviderContactDetail extends CBaseUtilityProviderContactDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityProviderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityProviderEventTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstructions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsIncludeRevertToOwnerAgreement() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsIncludeAccountList() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsIncludeContractCopy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityPreferedContactTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailToAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailSubject() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailContent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFaxNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailTo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailStreetLineOne() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailStreetLineTwo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailState() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneContact() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneInstructions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>