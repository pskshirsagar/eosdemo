<?php

class CUtilityBillOcrDataValue extends CBaseUtilityBillOcrDataValue {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrFieldId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCapturedLabel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCapturedValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLabelVertices() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValueVertices() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>