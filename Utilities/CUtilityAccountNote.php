<?php

class CUtilityAccountNote extends CBaseUtilityAccountNote {

    public function valEmployeeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getEmployeeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee is required.' ) );
        }

        return $boolIsValid;
    }

    public function valNote() {
        $boolIsValid = true;

        if( true == is_null( $this->getNote() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note is required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valEmployeeId();
            	$boolIsValid &= $this->valNote();
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>