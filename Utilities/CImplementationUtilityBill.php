<?php

class CImplementationUtilityBill extends CBaseImplementationUtilityBill {

	protected $m_strUtilityDocumentFilePath;
	protected $m_strUtilityDocumentFileName;

	const THIRD_PATRY_PROCESSING_VALIDATION = 'THIRD_PATRY_PROCESSING_VALIDATION';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationWebRetrievalId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSummaryImplementationUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSummaryImplementationUtilityBillPageNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewImplementationUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsQueueIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillProcessingDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartProcessDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndProcessDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastProcessedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedOnTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedAgainBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedAgainOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedAgainOnTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartReviewDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndReviewDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastReviewedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillProcessedSuccessfully() {

		$boolIsValid = true;

		if( false == is_null( $this->getProcessedAgainBy() ) && true == is_null( $this->getBillProcessingDetails() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Bill Procesing Details not set.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;

	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_DELETE:
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBillDateAndDueDate();
				break;

			case self::THIRD_PATRY_PROCESSING_VALIDATION:
				$boolIsValid &= $this->valBillProcessedSuccessfully();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valBillDateAndDueDate() : bool {
		$boolIsValid = true;
		$arrmixExistingBillProcessingDetails = json_decode( json_encode( $this->getBillProcessingDetails() ), 1 );

		$strBillDate        = $arrmixExistingBillProcessingDetails['bill_date'];
		$strBillDueDate     = $arrmixExistingBillProcessingDetails['bill_due_date'];

		if( false == empty( $strBillDate ) && false == empty( $strBillDueDate ) ) {
			if( strtotime( $strBillDate ) > strtotime( $strBillDueDate ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', 'Bill due date should not be less than bill date.' ) );
			}
		}

		if( true == empty( $strBillDate ) && true == empty( $strBillDueDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bill_date', 'Bill Date or Due Date is required.' ) );
		}

		return $boolIsValid;
	}

	public function updateProcessingTime( $objDatabase ) {
		$strColumnValue = '';

		if( true == is_null( $this->getProcessedOnTime() ) && false == is_null( $this->getProcessedOn() ) && false == is_null( $this->getProcessedBy() ) ) {
			$strColumnValue = 'processed_on_time = end_process_datetime - start_process_datetime';
		}

		if( true == is_null( $this->getProcessedAgainOnTime() ) && false == is_null( $this->getProcessedAgainOn() ) && false == is_null( $this->getProcessedAgainBy() ) ) {
			$strColumnValue .= ( true == valStr( $strColumnValue ) ) ? ', processed_again_on_time = end_process_datetime - start_process_datetime' : 'processed_again_on_time = end_process_datetime - start_process_datetime';
		}

		if( true == valStr( $strColumnValue ) && false == is_null( $this->getStartProcessDatetime() ) ) {
			if( false == fetchData( 'UPDATE implementation_utility_bills SET ' . $strColumnValue . ' WHERE id = ' . ( int ) $this->getId(), $objDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function fetchUtilityDocument( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityDocuments::createService()->fetchUtilityDocumentById( $this->getUtilityDocumentId(), $objDatabase );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_document_file_path'] ) ) {
			$this->setUtilityDocumentFilePath( $arrmixValues['utility_document_file_path'] );
		}
		if( true == isset( $arrmixValues['utility_document_file_name'] ) ) {
			$this->setUtilityDocumentFileName( $arrmixValues['utility_document_file_name'] );
		}
	}

	public function setUtilityDocumentFilePath( $strUtilityDocumentFilePath ) {
		if( false == is_null( $this->getCid() ) ) {
			$this->m_strUtilityDocumentFilePath = getMountsPath( $this->getCid(), PATH_MOUNTS_UTILITY_DOCUMENTS ) . $strUtilityDocumentFilePath;
		} else {
			$this->m_strUtilityDocumentFilePath = PATH_MOUNTS_GLOBAL_UTILITY_DOCUMENTS . $strUtilityDocumentFilePath;
		}
	}

	public function setUtilityDocumentFileName( $strUtilityDocumentFileName ) {
		$this->m_strUtilityDocumentFileName = $strUtilityDocumentFileName;
	}

	public function getUtilityDocumentFilePath() {
		return $this->m_strUtilityDocumentFilePath;
	}

	public function getUtilityDocumentFileName() {
		return $this->m_strUtilityDocumentFileName;
	}

}
?>