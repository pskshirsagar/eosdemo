<?php

class CUtilityBillCharge extends CBaseUtilityBillCharge {

	protected $m_strStateCode;
	protected $m_strPostalCode;

	protected $m_boolIsCredit;
	protected $m_boolShowMeterData;
	protected $m_boolIsSummaryBillCharge;
	protected $m_boolIsAccountCharge;

	protected $m_arrstrNotMatchedKeys;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsSummaryBillCharge = $this->m_boolIsAccountCharge = false;
	}

	public function setStateCode( $strStateCode ) {
        $this->m_strStateCode = $strStateCode;
	}

	public function setPostalCode( $strPostalCode ) {
        $this->m_strPostalCode = $strPostalCode;
	}

	public function setIsCredit( $boolIsCredit ) {
		$this->m_boolIsCredit = Cstrings::strToBool( $boolIsCredit );
	}

	public function setIsSummaryBillCharge( $boolIsSummaryBillCharge ) {
		$this->m_boolIsSummaryBillCharge = Cstrings::strToBool( $boolIsSummaryBillCharge );
	}

	public function setNotMatchedKeys( $arrstrNotMatchedKeys ) {
		$this->m_arrstrNotMatchedKeys = $arrstrNotMatchedKeys;
	}

	public function setIsAccountCharge( $boolIsAccountCharge ) {
		$this->m_boolIsAccountCharge = Cstrings::strToBool( $boolIsAccountCharge );
	}

    public function getStateCode() {
        return $this->m_strStateCode;
    }

	public function getPostalCode() {
        return $this->m_strPostalCode;
	}

	public function getIsCredit() {
		return $this->m_boolIsCredit;
	}

	public function getIsSummaryBillCharge() {
		return $this->m_boolIsSummaryBillCharge;
	}

	public function getNotMatchedKeys() {
		return $this->m_arrstrNotMatchedKeys;
	}

	public function getIsAccountCharge() {
		return $this->m_boolIsAccountCharge;
	}

    public function valName() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getName() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is a required Field.' ) );
    	}

    	return $boolIsValid;
    }

    public function valAmount() {
        $boolIsValid = true;

        if( true == is_null( $this->getAmount() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', $this->getName() . ' - Charge amount is required' ) );
        }

        return $boolIsValid;
    }

    public function valStartDateAndEndDate( $boolReturnOnly = false ) {
    	$boolIsValid = true;

		if( true == is_null( $this->getPropertyUtilityTypeId() ) || true == $this->getIsSummaryBillCharge() ) {
			return $boolIsValid;
		}

    	if( true == is_null( $this->getStartDate() ) || true == is_null( $this->getEndDate() ) ) {
	    	$boolIsValid &= false;
			if( false == $boolReturnOnly ) {
	    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Invalid start date or end date.' ) );
			}
    	}

    	if( ( true == $boolIsValid ) && ( 1 != strlen( CValidation::checkDate( $this->getStartDate() ) ) || 1 != strlen( CValidation::checkDate( $this->getEndDate() ) ) ) ) {
    		$boolIsValid &= false;
    		if( false == $boolReturnOnly ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date or end date is invalid and must be in mm/dd/yyyy format.' ) );
    		}
    	}

    	if( true == $boolIsValid && 0 > $this->daysDifference( $this->getEndDate(), $this->getStartDate() ) ) {
            $boolIsValid = false;
            if( false == $boolReturnOnly ) {
	            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Invalid start date or end date.' ) );
            }
    	}

    	return $boolIsValid;
    }

	public function daysDifference( $intEndDate, $intBeginDate ) {
	   $arrintDateParts1 = explode( '/', $intBeginDate );
	   $arrintDateParts2 = explode( '/', $intEndDate );
	   $intStartDate  = gregoriantojd( $arrintDateParts1[0], $arrintDateParts1[1], $arrintDateParts1[2] );
	   $intEndDate	  = gregoriantojd( $arrintDateParts2[0], $arrintDateParts2[1], $arrintDateParts2[2] );
	   return $intEndDate - $intStartDate;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

		switch( $strAction ) {
    		case VALIDATE_INSERT:
    		case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valAmount();
            	$boolIsValid &= $this->valStartDateAndEndDate();
            	$boolIsValid &= $this->valName();
        		break;

    		case VALIDATE_DELETE:
        		break;

    		default:
            	// default case
            	$boolIsValid = true;
        		break;
		}

        return $boolIsValid;
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrmixValues['is_credit'] ) ) {
			$this->setIsCredit( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_credit'] ) : $arrmixValues['is_credit'] );
		}

		if( true == isset( $arrmixValues['not_matched_keys'] ) ) {
			$this->setNotMatchedKeys( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['not_matched_keys'] ) : $arrmixValues['not_matched_keys'] );
		}
	}

	public function unsetAllErrorMsgs() {
    	$this->m_arrobjErrorMsgs = NULL;
	}

}
?>