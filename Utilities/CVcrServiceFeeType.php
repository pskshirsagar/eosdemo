<?php

class CVcrServiceFeeType extends CBaseVcrServiceFeeType {

	const FIXED_AMOUNT		= 1;
	const DAILY_PENALTY 	= 2;
	const NO_SERVICE_FEE 	= 3;
	const DAILY_AMOUNT 		= 4;

}
?>
