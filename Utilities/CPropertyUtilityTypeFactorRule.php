<?php

class CPropertyUtilityTypeFactorRule extends CBasePropertyUtilityTypeFactorRule {

	protected $m_intOldPropertyUtilityTypeFactorRuleId;

    public function setOldPropertyUtilityTypeFactorRuleId( $intOldPropertyUtilityTypeFactorRuleId ) {
    	return $this->m_intOldPropertyUtilityTypeFactorRuleId = $intOldPropertyUtilityTypeFactorRuleId;
    }

    public function getOldPropertyUtilityTypeFactorRuleId() {
    	return $this->m_intOldPropertyUtilityTypeFactorRuleId;
    }

    public function valFactor() {
        $boolIsValid = true;

        if( true == in_array( $this->getUtilityRubsFormulaId(), CUtilityRubsFormula::$c_arrintFixedFormulae ) ) {
		    return $boolIsValid;
	    }

        if( true == is_null( $this->getFactor() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'factor', 'Factor is required.' ) );
        }

        return $boolIsValid;
    }

    public function valFixedAmount() {
        $boolIsValid = true;

	    if( false == in_array( $this->getUtilityRubsFormulaId(), CUtilityRubsFormula::$c_arrintFixedFormulae ) ) {
		    return $boolIsValid;
	    }

        if( true == is_null( $this->getFixedAmount() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fixed_amount', 'Fixed amount is required' ) );
        }

   		if( false == preg_match( '/^[0-9.]*$/', $this->getFixedAmount() ) || 1 == $this->getFixedAmount() ) {
			$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fixed_amount', 'Invalid fixed amount.' ) );
   		}

        return $boolIsValid;
    }

   public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valFactor();
            	$boolIsValid &= $this->valFixedAmount();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
   }

}
?>