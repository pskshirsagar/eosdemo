<?php

class CBuyerSubAccount extends CBaseBuyerSubAccount {

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valStoreId() {
		return true;
	}

	public function valBuyerAccountId() {
		return true;
	}

	public function valApPayeeSubAccountId() {
		return true;
	}

	public function valAccountNumber() {
		return true;
	}

	public function valDescription() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>