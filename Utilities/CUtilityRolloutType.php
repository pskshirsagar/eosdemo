<?php

class CUtilityRolloutType extends CBaseUtilityRolloutType {

	const TAKEOVER = 1;
	const LEASE_UP = 2;
	const RAMP_UP = 3;
	const STUDENT_TAKEOVER = 4;
	const STUDENT_LEASE_UP = 5;
	const STUDENT_RAMP_UP = 6;

}
?>