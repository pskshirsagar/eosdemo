<?php

class CUtilityInvoiceType extends CBaseUtilityInvoiceType {

	const STANDARD 		        	 = 1;
	const MOVE_IN 			         = 2;
	const MOVE_OUT 		        	 = 3;
	const VACANCY_RECOVERY         	 = 4;
	const IMMEDIATE_VACANT_RECOVERY	 = 5;

}
?>