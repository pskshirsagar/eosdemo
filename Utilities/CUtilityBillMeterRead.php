<?php

class CUtilityBillMeterRead extends CBaseUtilityBillMeterRead {

	protected $m_intCommodityId;
	protected $m_intUtilityTypeId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intUtilityConsumptionTypeId;
	protected $m_intUtilityBillTemplateChargeId;
	protected $m_intUtilityBillAccountTemplateChargeId;

	protected $m_boolIsDemandMeter;
	protected $m_boolIsTimeOfUseMeter;
	protected $m_boolCaptureUsageTotalOnly;

	protected $m_arrstrNotMatchedKeys;

	protected $m_strMeterNumber;

	public function getUtilityBillTemplateChargeId() {
		return $this->m_intUtilityBillTemplateChargeId;
	}

	public function getUtilityConsumptionTypeId() {
		return $this->m_intUtilityConsumptionTypeId;
	}

	public function getMeterNumber() {
		return $this->m_strMeterNumber;
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function getUtilityBillAccountTemplateChargeId() {
		return $this->m_intUtilityBillAccountTemplateChargeId;
	}

	public function getIsDemandMeter() {
		return $this->m_boolIsDemandMeter;
	}

	public function getIsTimeOfUseMeter() {
		return $this->m_boolIsTimeOfUseMeter;
	}

	public function getCaptureUsageTotalOnly() {
		return $this->m_boolCaptureUsageTotalOnly;
	}

	public function getCommodityId() {
		return $this->m_intCommodityId;
	}

	public function setUtilityBillTemplateChargeId( $intUtilityBillTemplateChargeId ) {
		$this->m_intUtilityBillTemplateChargeId = $intUtilityBillTemplateChargeId;
	}

	public function setUtilityConsumptionTypeId( $intUtilityConsumptionTypeId ) {
		$this->m_intUtilityConsumptionTypeId = $intUtilityConsumptionTypeId;
	}

	public function setMeterNumber( $strMeterNumber ) {
		$this->m_strMeterNumber = $strMeterNumber;
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->m_intPropertyUtilityTypeId = $intPropertyUtilityTypeId;
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->m_intUtilityTypeId = $intUtilityTypeId;
	}

	public function setUtilityBillAccountTemplateChargeId( $intUtilityBillAccountTemplateChargeId ) {
		$this->m_intUtilityBillAccountTemplateChargeId = $intUtilityBillAccountTemplateChargeId;
	}

	public function setNotMatchedKeys( $arrstrNotMatchedKeys ) {
		$this->m_arrstrNotMatchedKeys = $arrstrNotMatchedKeys;
	}

	public function setIsDemandMeter( $boolIsDemandMeter ) {
		$this->m_boolIsDemandMeter = CStrings::strToBool( $boolIsDemandMeter );
	}

	public function setIsTimeOfUseMeter( $boolIsTimeOfUseMeter ) {
		$this->m_boolIsTimeOfUseMeter = CStrings::strToBool( $boolIsTimeOfUseMeter );
	}

	public function setCaptureUsageTotalOnly( $boolCaptureUsageTotalOnly ) {
		$this->m_boolCaptureUsageTotalOnly = CStrings::strToBool( $boolCaptureUsageTotalOnly );
	}

	public function setCommodityId( $intCommodityId ) {
		$this->m_intCommodityId = $intCommodityId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_bill_template_charge_id'] ) ) {
			$this->setUtilityBillTemplateChargeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_template_charge_id'] ) : $arrmixValues['utility_bill_template_charge_id'] );
		}
		if( true == isset( $arrmixValues['utility_consumption_type_id'] ) ) {
			$this->setUtilityConsumptionTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_consumption_type_id'] ) : $arrmixValues['utility_consumption_type_id'] );
		}
		if( true == isset( $arrmixValues['meter_number'] ) ) {
			$this->setMeterNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['meter_number'] ) : $arrmixValues['meter_number'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_id'] ) ) {
			$this->setPropertyUtilityTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_utility_type_id'] ) : $arrmixValues['property_utility_type_id'] );
		}
		if( true == isset( $arrmixValues['utility_bill_account_template_charge_id'] ) ) {
			$this->setUtilityBillAccountTemplateChargeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_account_template_charge_id'] ) : $arrmixValues['utility_bill_account_template_charge_id'] );
		}
		if( true == isset( $arrmixValues['is_partial_match'] ) ) {
			$this->setIsPartialMatch( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_partial_match'] ) : $arrmixValues['is_partial_match'] );
		}
		if( true == isset( $arrmixValues['utility_type_id'] ) ) {
			$this->setUtilityTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_type_id'] ) : $arrmixValues['utility_type_id'] );
		}

		if( true == isset( $arrmixValues['is_demand_meter'] ) ) {
			$this->setIsDemandMeter( $arrmixValues['is_demand_meter'] );
		}

		if( true == isset( $arrmixValues['is_time_of_use_meter'] ) ) {
			$this->setIsTimeOfUseMeter( $arrmixValues['is_time_of_use_meter'] );
		}

		if( true == isset( $arrmixValues['capture_usage_total_only'] ) ) {
			$this->setCaptureUsageTotalOnly( $arrmixValues['capture_usage_total_only'] );
		}

		if( true == isset( $arrmixValues['commodity_id'] ) ) {
			$this->setCommodityId( $arrmixValues['commodity_id'] );
		}
		return true;
	}

	public function getNotMatchedKeys() {
		return $this->m_arrstrNotMatchedKeys;
	}

    public function validate( $strAction, $objUtilityBill = NULL ) {
    	$boolIsValid = true;

    	switch( $strAction ) {
    		case VALIDATE_INSERT:
    		case VALIDATE_UPDATE:
    			$boolIsValid &= $this->valStartDateAndEndDate();
    			$boolIsValid &= $this->valMeterReading();
    			$boolIsValid &= $this->valUnitConsumptionAmount();
		        $boolIsValid &= $this->valDemand();
		        $boolIsValid &= $this->valconsumptionUnits();
    			break;

    		case VALIDATE_DELETE:
    			break;

    		default:
    			// default case
    			$boolIsValid = true;
    			break;
    	}

    	return $boolIsValid;
    }

    public function valMeterReading() {
    	$boolIsValid = true;

    	   	if( false == $this->getCaptureUsageTotalOnly() && ( true == is_null( $this->getStartRead() ) || true == is_null( $this->getEndRead() ) ) ) {
    	   		$boolIsValid &= false;
    	   		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_start_read', 'Start meter read or End meter is invalid.' ) );
    	   	}

    	   	if( 1 == $this->getStartRead() && false == is_null( $this->getEndRead() ) ) {
    	   		if( $this->getEndRead() < $this->getStartRead() ) {
    	   			$boolIsValid &= false;
    	   			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_start_read', 'Start meter read or End meter is invalid.' ) );
    	   		}
    	   	}

    	   	if( false == is_null( $this->getStartRead() ) && false == is_null( $this->getEndRead() ) && $this->getStartRead() > $this->getEndRead() ) {
    			$boolIsValid &= false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_start_read', 'Meter start read should be lower than meter end read.' ) );
    	   	}

    	return $boolIsValid;
    }

    public function valUnitConsumptionAmount() {
    	$boolIsValid = true;

    	if( false == $this->getCaptureUsageTotalOnly() && ( true == is_null( $this->getStartRead() ) && true == is_null( $this->getEndRead() ) ) ) {
    		$boolIsValid &= false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_consumption_amount', 'Unit consumption amount is invalid.' ) );
    	}
    	if( false == $this->getCaptureUsageTotalOnly() && ( true == is_null( $this->getUnitConsumptionAmount() ) || false == preg_match( '/^-?[0-9.]*$/', $this->getUnitConsumptionAmount() ) ) ) {
    		$boolIsValid &= false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_consumption_amount', 'Unit consumption amount is invalid.' ) );
    	}

    	return $boolIsValid;
    }

    public function valStartDateAndEndDate( $boolReturnOnly = false ) {
    	$boolIsValid = true;

    	if( true == is_null( $this->getStartDate() ) || true == is_null( $this->getEndDate() ) ) {
			$boolIsValid &= false;
    		if( false == $boolReturnOnly ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', $this->getStartDate() . 'Invalid start date or end date.' ) );
    		}
		}

 		if( ( true == $boolIsValid ) && ( 1 != strlen( CValidation::checkDate( $this->getStartDate() ) ) || 1 != strlen( CValidation::checkDate( $this->getEndDate() ) ) ) ) {
    		$boolIsValid &= false;
    	    if( false == $boolReturnOnly ) {
    	    	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date or end date is invalid and must be in mm/dd/yyyy format.' ) );
    	    }
		}

		if( true == $boolIsValid && 0 > $this->daysDifference( $this->getEndDate(), $this->getStartDate() ) ) {
            $boolIsValid = false;
            if( false == $boolReturnOnly ) {
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Invalid start date or end date.' ) );
            }
    	}

    	return $boolIsValid;
    }

	public function valDemand() {
		$boolIsValid = true;

		if( true == $this->getIsDemandMeter() && true == is_null( $this->getDemand() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'demand', 'Demand is required.' ) );
		}

		return $boolIsValid;
	}

	public function valconsumptionUnits() {
		$boolIsValid = true;

		if( true == $this->getCaptureUsageTotalOnly() && true == is_null( $this->getConsumptionUnits() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'consumption_units', 'Consumption Units is required.' ) );
		}

		return $boolIsValid;
	}

    public function daysDifference( $intEndDate, $intBeginDate ) {
    	$arrintBeginDateParts = explode( '/', $intBeginDate );
    	$arrintEndDateParts = explode( '/', $intEndDate );
    	$intStartDate  = gregoriantojd( $arrintBeginDateParts[0], $arrintBeginDateParts[1], $arrintBeginDateParts[2] );
    	$intEndDate	  = gregoriantojd( $arrintEndDateParts[0], $arrintEndDateParts[1], $arrintEndDateParts[2] );
    	return $intEndDate - $intStartDate;
    }

}
?>