<?php

class CUtilityNotice extends CBaseUtilityNotice {

	protected $m_intCheckUtilityNoticeType;

	protected $m_intUtilityBatchId;

	protected $m_strFileName;
	protected $m_strFilePath;

	public function getUtilityNoticeType() {
		return $this->m_intCheckUtilityNoticeType;
	}

	public function getFileName() {
        return $this->m_strFileName;
    }

  	public function getFilePath( $intCid = NULL ) {
  		if( false == is_null( $intCid ) ) {
  			return getMountsPath( $intCid, PATH_MOUNTS_UTILITY_DOCUMENTS ) . $this->m_strFilePath;
  		}

	    return PATH_MOUNTS_GLOBAL_UTILITY_DOCUMENTS . $this->m_strFilePath;
    }

    public function getUtilityBatchId() {
    	return $this->m_intUtilityBatchId;
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['check_utility_notice_type'] ) ) {
			$this->setUtilityNoticeType( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['check_utility_notice_type'] ) : $arrmixValues['check_utility_notice_type'] );
		}
		if( true == isset( $arrmixValues['file_name'] ) ) {
			$this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['file_name'] ) : $arrmixValues['file_name'] );
		}
		if( true == isset( $arrmixValues['file_path'] ) ) {
			$this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['file_path'] ) : $arrmixValues['file_path'] );
		}
		if( true == isset( $arrmixValues['utility_batch_id'] ) ) {
			$this->setUtilityBatchId( $arrmixValues['utility_batch_id'] );
		}
	}

	public function setUtilityNoticeType( $intCheckUtilityNoticeType ) {
		$this->m_intCheckUtilityNoticeType = $intCheckUtilityNoticeType;
	}

	public function setFileName( $strFileName ) {
        $this->m_strFileName = CStrings::strTrimDef( $strFileName, 240, NULL, true );
    }

 	public function setFilePath( $strFilePath ) {
        $this->m_strFilePath = CStrings::strTrimDef( $strFilePath, -1, NULL, true );
    }

    public function setUtilityBatchId( $intUtilityBatchId ) {
    	$this->m_intUtilityBatchId = $intUtilityBatchId;
    }

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedOn( 'NOW()' );
		$this->setDeletedBy( $intCurrentUserId );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function createUtilityDocument() {

		$objUtilityDocument = new CUtilityDocument();
		$objUtilityDocument->setDocumentDatetime( 'Now()' );

		return $objUtilityDocument;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required' ) );
		}

		if( false == is_null( $this->getTitle() ) && false == preg_match( '/^[a-zA-Z0-9\\\'\" ]+$/', $this->getTitle() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Invalid title.' ) );
		}
		return $boolIsValid;
	}

	public function valContent() {
		$boolIsValid = true;

		if( 1 == $this->getUtilityNoticeType() && true == is_null( $this->getContent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'content', 'Content is required' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateTitle( $objUtilitiesDatabase ) {

		$boolIsValid = true;
		$strCondition = '';

		if( true == is_numeric( $this->getId() ) ) {
			$strCondition = ' AND id <> ' . $this->getId();
		}

		$intCountTitle = \Psi\Eos\Utilities\CUtilityNotices::createService()->fetchUtilityNoticeCount( 'WHERE title = \'' . $this->getTitle() . '\'' . $strCondition . ' AND deleted_by IS NULL', $objUtilitiesDatabase );

		if( 0 != $intCountTitle ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $this->m_strTitle . ' utility notice is already exist.' ) );
		}

		return $boolIsValid;

	}

	public function valDependantInformation( $objUtilitiesDatabase ) {

		$boolIsValid = true;

		$intCount = \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchUtilityBatchCount( 'WHERE utility_notice_id = ' . $this->getId(), $objUtilitiesDatabase );

		if( 0 != $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  $this->m_strTitle . ' utility notice cannot be removed because property utility batch(s) depends on it.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objUtilitiesDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valContent();
				$boolIsValid &= $this->valDuplicateTitle( $objUtilitiesDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objUtilitiesDatabase );
				break;

			default:
            	// default case
            	$boolIsValid = true;
            	break;
		}

		return $boolIsValid;
	}

	public function fetchUtilityDocumentById( $intUtilityDocumentId, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityDocuments::createService()->fetchUtilityDocumentById( $intUtilityDocumentId, $objUtilitiesDatabase );
	}

}
?>