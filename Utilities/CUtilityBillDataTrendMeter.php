<?php

class CUtilityBillDataTrendMeter extends CBaseUtilityBillDataTrendMeter {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillMonthDataTrendId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAccountMeterId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpense() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConsumption() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>