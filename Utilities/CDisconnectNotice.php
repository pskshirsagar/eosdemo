<?php

class CDisconnectNotice extends CBaseDisconnectNotice {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisconnectNoticeDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResolvedNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResolvedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResolvedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>