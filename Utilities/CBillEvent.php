<?php

class CBillEvent extends CBaseBillEvent {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillEventTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillEventSubTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillEventReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>