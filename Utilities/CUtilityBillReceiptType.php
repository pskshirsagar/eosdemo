<?php

class CUtilityBillReceiptType extends CBaseUtilityBillReceiptType {

	const UTILITY_MAIL						= 1;
	const EMAIL								= 2;
	const FAX								= 3;
	const ENTRATA_UPLOAD					= 5;
	const WEB_RETRIEVAL						= 6;
	const ESTIMATED							= 8;
	const CLIENT_ADMIN_UPLOAD				= 9;
	const ENTRATA_AP						= 10;
	const VENDOR_INVOICE_IMPORT				= 13;
	const CON_SERVICE						= 12;
	const SIMPLEMENTATION_PORTAL_UPLOAD		= 14;
	const UTILITY_PARSER_EMAIL				= 15;
	const INVOICE_PROCESSING_PARSER_EMAIL	= 16;
	const UTILITY_BILL_CA_UPLOAD			= 17;
	const INVOICE_PROCESSING_BILL_CA_UPLOAD	= 18;
	const MISSING_BILL_ALERT_UPLOAD			= 19;
	const IP_MAIL                           = 20;

	public static $c_arrintAllUtilityBillTypes = [
		self::UTILITY_MAIL,
		self::EMAIL,
		self::ENTRATA_UPLOAD,
		self::WEB_RETRIEVAL,
		self::CLIENT_ADMIN_UPLOAD,
		self::ENTRATA_AP,
		self::UTILITY_PARSER_EMAIL,
		self::INVOICE_PROCESSING_PARSER_EMAIL,
		self::UTILITY_BILL_CA_UPLOAD,
		self::INVOICE_PROCESSING_BILL_CA_UPLOAD,
		self::MISSING_BILL_ALERT_UPLOAD,
		self::IP_MAIL
	];

	public static function populateTemplateConstants() {
		return [
			self::UTILITY_MAIL			=> 'Bill was received via the mail',
			self::EMAIL 				=> 'Bill was received via email',
			self::ENTRATA_UPLOAD		=> 'Bills was received via the client manually uploading them in Entrata',
			self::WEB_RETRIEVAL			=> 'Bills was received via web retrievals',
			self::CLIENT_ADMIN_UPLOAD	=> 'Bills was received via client admin upload',
			self::ENTRATA_AP			=> 'Bill was received via Entrata AP System',
			self::UTILITY_BILL_CA_UPLOAD => 'Bill uploaded in utility bill CA upload',
			self::INVOICE_PROCESSING_BILL_CA_UPLOAD => 'Bill uploaded in invoice processing bill CA upload',
			self::UTILITY_PARSER_EMAIL => 'Bill received via utility parser email',
			self::INVOICE_PROCESSING_PARSER_EMAIL => 'Bill received via invoice processing parser email',
			self::MISSING_BILL_ALERT_UPLOAD => 'Bill received via missing invoice alert upload'
		];
	}

	public static $c_arrstrUtilityBillReceiptTypeMessages = [
		self::UTILITY_MAIL        => 'Bill received via the mail',
		self::EMAIL               => 'Bill received via email',
		self::ENTRATA_UPLOAD      => 'Bills uploaded in Entrata',
		self::WEB_RETRIEVAL       => 'Bill uploaded in Web Retrieval',
		self::CLIENT_ADMIN_UPLOAD => 'Bill uploaded in Client Admin',
		self::ENTRATA_AP          => 'Bill auto imported from Entrata',
		self::FAX                 => 'Bill received via fax',
		self::ESTIMATED           => 'Bill Estimated',
		self::CON_SERVICE         => 'Bill added via Conservice file',
		self::UTILITY_BILL_CA_UPLOAD => 'Bill uploaded in utility bill CA upload',
		self::INVOICE_PROCESSING_BILL_CA_UPLOAD => 'Bill uploaded in invoice processing bill CA upload',
		self::UTILITY_PARSER_EMAIL => 'Bill received via utility parser email',
		self::INVOICE_PROCESSING_PARSER_EMAIL => 'Bill received via invoice processing parser email',
		self::MISSING_BILL_ALERT_UPLOAD => 'Bill received via missing invoice alert upload',
		self::IP_MAIL => 'Bill received via the IP mail'
	];

	public static $c_arrintWebRetrievalUtilityBillReceiptTypes = [
		self::WEB_RETRIEVAL
	];

}
?>