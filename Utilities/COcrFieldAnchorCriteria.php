<?php

class COcrFieldAnchorCriteria extends CBaseOcrFieldAnchorCriteria {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityProviderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrFieldId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAnchorTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLabels() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLabelVertices() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAnchors() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAnchorVertices() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>