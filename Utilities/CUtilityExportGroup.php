<?php

class CUtilityExportGroup extends CBaseUtilityExportGroup {

    public function valServicePeriodUtilityTypeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intServicePeriodUtilityTypeId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_period_utility_type_id', 'Please Select Utility Type.' ) );
        }

        return $boolIsValid;
    }

    public function valArCodeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intArCodeId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Please Select Ar Code.' ) );
        }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

        if( false == isset( $this->m_strName ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Utility Export Group Name is Required.' ) );
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

        if( false == isset( $this->m_strDescription ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Utility Export Group Description is Required.' ) );
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName();
            	$boolIsValid &= $this->valDescription();
            	$boolIsValid &= $this->valServicePeriodUtilityTypeId();
            	$boolIsValid &= $this->valArCodeId();
        		break;

            case VALIDATE_DELETE:
         		break;

        	default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>