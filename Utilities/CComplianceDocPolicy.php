<?php

class CComplianceDocPolicy extends CBaseComplianceDocPolicy {

	protected $m_intPropertyGroupId;

	protected $m_strComplianceItemName;

	protected $m_arrobjComplianceDocLimits;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjComplianceDocLimits = [];

	}

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valComplianceDocId() {
		return true;
	}

	public function valComplianceItemId() {
		$boolIsValid = true;

		if( 0 == ( int ) $this->getComplianceItemId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'compliance_item_id', 'Policy type is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPolicyDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getPolicyDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'policy_description', 'Policy description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPolicyNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getPolicyNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'policy_number', 'Policy number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valExpirationDate( $boolIsRequired = false ) {
		$boolIsValid = true;

		$strDate = $this->getExpirationDate();
		if( true == $boolIsRequired && true == is_null( $strDate ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expiration_date', 'Expiration date is required.' ) );
			return false;
		}

		if( false == is_null( $strDate ) && false == CValidation::validateDate( $strDate ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expiration_date', 'Expiration date is invalid.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valComplianceItemId();
				if( true == valId( $this->getVendorPolicyId() ) ) {
					$boolIsValid &= $this->valPolicyDescription();
					$boolIsValid &= $this->valPolicyNumber();
				}
				$boolIsValid &= $this->valExpirationDate( true );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createComplianceDocLimit() {

		$objComplianceDocLimit = new CComplianceDocLimit();
		$objComplianceDocLimit->setCid( $this->getCid() );
		$objComplianceDocLimit->setComplianceDocPolicyId( $this->getId() );

		return $objComplianceDocLimit;
	}

	public function setComplianceItemName( $strComplianceItemName ) {
		$this->m_strComplianceItemName = CStrings::strTrimDef( $strComplianceItemName, 50, NULL, true );
	}

	public function getComplianceItemName() {
		return $this->m_strComplianceItemName;
	}

	public function setPropertyGroupId( $intPropertyGroupId ) {
		$this->m_intPropertyGroupId = ( int ) $intPropertyGroupId;
	}

	public function getPropertyGroupId() {
		return $this->m_intPropertyGroupId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['compliance_item_name'] ) && $boolDirectSet ) {
			$this->m_strComplianceItemName = trim( stripcslashes( $arrmixValues['compliance_item_name'] ) );
		} elseif( isset( $arrmixValues['compliance_item_name'] ) ) {
			$this->setComplianceItemName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['compliance_item_name'] ) : $arrmixValues['compliance_item_name'] );
		}

		if( true == isset( $arrmixValues['property_group_id'] ) ) {
			$this->setPropertyGroupId( $arrmixValues['property_group_id'] );
		}
	}

	public function addComplianceDocLimit( $objComplianceDocLimit ) {
		$this->m_arrobjComplianceDocLimits[] = $objComplianceDocLimit;
	}

	public function getComplianceDocLimits() {
		return $this->m_arrobjComplianceDocLimits;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>