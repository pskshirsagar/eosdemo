<?php

class CPropertyMeteringSetting extends CBasePropertyMeteringSetting {

	const LAST_DAY_OF_MONTH = 28;
	const PUT_SUBMETER_TOTAL_VALIDATION_COUNT = 20;

	protected $m_intPropertyId;
	protected $m_intUtilityTypeId;
	protected $m_intUtilityConsumptionTypeId;

	protected $m_arrobjNormalMeterRates;
	protected $m_arrobjCareMeterRates;
	protected $m_strPropertyUtilityTypeName;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}

		if( true == isset( $arrmixValues['property_utility_type_name'] ) ) {
			$this->setPropertyUtilityTypeName( $arrmixValues['property_utility_type_name'] );
		}

		if( true == isset( $arrmixValues['utility_type_id'] ) ) {
			$this->setUtilityTypeId( $arrmixValues['utility_type_id'] );
		}

		if( true == isset( $arrmixValues['utility_consumption_type_id'] ) ) {
			$this->setUtilityConsumptionTypeId( $arrmixValues['utility_consumption_type_id'] );
		}

	}

    /**
     * Create Functions
     */

    public function createMeterRate() {

    	$objMeterRate = new CMeterRate();
    	$objMeterRate->setCid( $this->getCid() );
    	$objMeterRate->setPropertyUtilityTypeId( $this->getPropertyUtilityTypeId() );
    	$objMeterRate->setPropertyMeteringSettingId( $this->getId() );

    	return $objMeterRate;
    }

    public function addNormalMeterRates( $arrobjNormalMeterRates ) {
		$this->m_arrobjNormalMeterRates = $arrobjNormalMeterRates;
    }

    public function addCareMeterRates( $arrobjCareMeterRates ) {
    	$this->m_arrobjCareMeterRates = $arrobjCareMeterRates;
    }

 	public function getNormalMeterRates() {
		return $this->m_arrobjNormalMeterRates;
 	}

    public function getCareMeterRates() {
    	return $this->m_arrobjCareMeterRates;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function getPropertyUtilityTypeName() {
    	return $this->m_strPropertyUtilityTypeName;
    }

    public function setPropertyUtilityTypeName( $strPropertyUtilityTypeName ) {
    	$this->m_strPropertyUtilityTypeName = $strPropertyUtilityTypeName;
    }

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->m_intUtilityTypeId = $intUtilityTypeId;
	}

	public function getUtilityConsumptionTypeId() {
		return $this->m_intUtilityConsumptionTypeId;
	}

	public function setUtilityConsumptionTypeId( $intUtilityConsumptionTypeId ) {
		$this->m_intUtilityConsumptionTypeId = $intUtilityConsumptionTypeId;
	}

	public function valUtilityTransmissionTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityTransmissionTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_transmission_type_id', 'Utility transmission type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valInstallationCompanyName() {
        $boolIsValid = true;

        if( true == is_null( $this->getInstallationCompanyName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'installation_company_name', 'Installation company name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valTamperFee() {
        $boolIsValid = true;

        if( true == is_null( $this->getTamperFee() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tamper_fee', 'Tamper fee is required.' ) );
        }
        if( false == preg_match( '/^[0-9]*$/', $this->getTamperFee() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tamper_fee', 'Tamper fee must be an integer.' ) );
        }

        return $boolIsValid;
    }

	public function valRdlDccPhoneNumber() {
		$boolIsValid = true;

		if( false == is_null( $this->getRdlDccPhoneNumber() ) ) {

			if( false == CValidation::validateFullPhoneNumber( $this->getRdlDccPhoneNumber(), false ) || 0 == $this->getRdlDccPhoneNumber() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rdl_dcc_phone_number', 'Invalid rdl/dcc phone number.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valTransmissionExpectedDay() {

		$boolIsValid = true;

		if( false == is_null( $this->getTransmissionExpectedDay() ) ) {
			if( false == preg_match( '/^[0-9]*$/', $this->getTransmissionExpectedDay() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_expected_day', 'Transmission expected day must be an integer.' ) );
			} elseif( self::LAST_DAY_OF_MONTH < $this->getTransmissionExpectedDay() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_expected_day', 'Transmission expected day must be less than 28.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valTransmissionDeadlineDay() {
		$boolIsValid = true;

		if( false == is_null( $this->getTransmissionDeadlineDay() ) ) {
			if( false == preg_match( '/^[0-9]*$/', $this->getTransmissionDeadlineDay() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_deadline_day', 'Transmission deadline day must be an integer.' ) );
			} elseif( self::LAST_DAY_OF_MONTH < $this->getTransmissionDeadlineDay() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_deadline_day', 'Transmission deadline day must be less than 28.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valTransmissionEmergencyDay() {
		$boolIsValid = true;

		if( false == is_null( $this->getTransmissionEmergencyDay() ) ) {
			if( false == preg_match( '/^[0-9]*$/', $this->getTransmissionEmergencyDay() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_emergency_day', 'Transmission emergency day must be an integer.' ) );
			} elseif( self::LAST_DAY_OF_MONTH < $this->getTransmissionEmergencyDay() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_emergency_day', 'Transmission emergency day must be less than 28.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valExpectedInstallCompletion( $objUtilitiesDatabase ) {

		$boolIsValid = true;

		if( false == is_null( $this->getExpectedInstallCompletion() ) ) {
			if( true == is_null( $this->getId() ) && ( strtotime( $this->getExpectedInstallCompletion() ) < strtotime( date( 'm/d/Y', time() ) ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expected_install_completion', 'Expected install completion should not be less than today.' ) );
			}

			if( false == is_null( $this->getId() ) ) {
				$objPropertyMeteringSetting = \Psi\Eos\Utilities\CPropertyMeteringSettings::createService()->fetchPropertyMeteringSettingById( $this->getId(), $objUtilitiesDatabase );
				if( strtotime( $objPropertyMeteringSetting->getExpectedInstallCompletion() ) != strtotime( $this->getExpectedInstallCompletion() ) && strtotime( $this->getExpectedInstallCompletion() ) < strtotime( date( 'm/d/Y', time() ) ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expected_install_completion', 'Expected install completion should not be less than today.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valBtuFactorAndDailyAllowance() {

		$boolIsValid = true;

		if( CMeterRateType::BASELINE_RATE == $this->getMeterRateTypeId() && ( true == is_null( $this->getBtuFactor() ) || 0 >= $this->getBtuFactor() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'btu_factor', 'Please enter a valid btu factor.' ) );
		}

		if( CMeterRateType::BASELINE_RATE == $this->getMeterRateTypeId() && ( true == is_null( $this->getDailyAllowance() ) || 0 >= $this->getDailyAllowance() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'daily_allowance', 'Please enter a valid daily allowance.' ) );
		}

		return $boolIsValid;
	}

	public function valNormalMeterRates() {

		$boolIsValid = true;

		if( true == is_null( $this->getMeterRateTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'meter_rate_type_id', 'Please select normal meter rate type.' ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid ) {
			$arrobjNormalMeterRates = $this->getNormalMeterRates();

			if( false == valArr( $arrobjNormalMeterRates ) ) {
				return true;
			}

			$intNormalCount = \Psi\Libraries\UtilFunctions\count( $arrobjNormalMeterRates );

			foreach( $arrobjNormalMeterRates as $intKey => $objMeterRate ) {

				if( CMeterRateType::BASELINE_RATE != $this->getMeterRateTypeId() ) {

					if( true == is_null( $this->getMeterRateChangeDate() ) || strtotime( $this->getMeterRateChangeDate() ) < time() ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Please select future rate change date.' ) );
						$boolIsValid = false;
					}

					if( 0 == $intKey && 0 != $objMeterRate->getStartCriteria() ) {
						$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_criteria', 'First start criteria must be 0.' ) );
						$boolIsValid = false;
					}

					if( true == is_null( $objMeterRate->getStartCriteria() ) ) {
						$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_criteria', 'Please enter start criteria value.' ) );
						$boolIsValid = false;
					}

					if( $intKey != ( $intNormalCount - 1 ) && true == is_null( $objMeterRate->getEndCriteria() ) ) {
						$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_criteria', 'Please enter end criteria value.' ) );
						$boolIsValid = false;
					}

					if( $intKey == ( $intNormalCount - 1 ) && false == is_null( $objMeterRate->getEndCriteria() ) ) {
						$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_criteria', 'Please remove end criteria value.' ) );
						$boolIsValid = false;
					}

					if( true == is_null( $objMeterRate->getRate() ) ) {
						$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Please enter valid rate value.' ) );
						$boolIsValid = false;
					}
				} else {
					if( true == is_null( $objMeterRate->getRate() ) ) {
						if( 0 == $intKey ) {
							$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Please enter a valid baseline rate for normal rate.' ) );
						} else {
							$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Please enter a valid over baseline rate for normal rate.' ) );
						}
						$boolIsValid = false;
					}
				}

				if( 1 < $intNormalCount ) {

					if( false == isset( $arrobjNormalMeterRates[$intKey + 1] ) ) {
						continue;
					}

					$objNextMeterRate = $arrobjNormalMeterRates[$intKey + 1];

					if( CMeterRateType::BASELINE_RATE != $this->getMeterRateTypeId() && 1 != ( $objNextMeterRate->getStartCriteria() - $objMeterRate->getEndCriteria() ) ) {
						$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_diff', 'There should not be gap between end range and next start range.' ) );
						$boolIsValid = false;
					}
				}

				if( CMeterRateType::BASELINE_RATE != $this->getMeterRateTypeId() && false == is_null( $objMeterRate->getStartCriteria() ) && false == is_null( $objMeterRate->getEndCriteria() ) && $objMeterRate->getStartCriteria() > $objMeterRate->getEndCriteria() ) {
					$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_criteria', 'End range should be greater than start range	.' ) );
					$boolIsValid = false;
				}

				if( false == $boolIsValid ) {
					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function valCareMeterRates() {

		if( false == valArr( $this->getCareMeterRates() ) ) {
			return true;
		}

		$boolIsValid = true;

		$arrobjCareMeterRates = $this->getCareMeterRates();

		$intCareCount = \Psi\Libraries\UtilFunctions\count( $arrobjCareMeterRates );

		foreach( $arrobjCareMeterRates as $intKey => $objMeterRate ) {

			if( CMeterRateType::BASELINE_RATE != $this->getMeterCareRateTypeId() ) {

				if( true == is_null( $this->getMeterCareRateChangeDate() ) || strtotime( $this->getMeterCareRateChangeDate() ) < time() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Please select future care rate change date.' ) );
					$boolIsValid = false;
				}

				if( 0 == $intKey && 0 != $objMeterRate->getStartCriteria() ) {
					$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_criteria', 'First start criteria must be 0.' ) );
					$boolIsValid = false;
				}

				if( true == is_null( $objMeterRate->getStartCriteria() ) ) {
					$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_criteria', 'Please enter start criteria value.' ) );
					$boolIsValid = false;
				}

				if( $intKey != ( $intCareCount - 1 ) && true == is_null( $objMeterRate->getEndCriteria() ) ) {
					$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_criteria', 'Please enter end criteria value.' ) );
					$boolIsValid = false;
				}

				if( $intKey == ( $intCareCount - 1 ) && false == is_null( $objMeterRate->getEndCriteria() ) ) {
					$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_criteria', 'Please remove end criteria value.' ) );
					$boolIsValid = false;
				}

				if( true == is_null( $objMeterRate->getRate() ) ) {
					$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Please enter valid rate value.' ) );
					$boolIsValid = false;
				}
			} else {
					if( true == is_null( $objMeterRate->getRate() ) ) {
						if( 0 == $intKey ) {
							$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Please enter a valid baseline rate for care rate.' ) );
						} else {
							$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Please enter a valid over baseline rate for care rate.' ) );
						}
						$boolIsValid = false;
					}
			}

			if( 1 < $intCareCount && $intKey != $intCareCount ) {

				if( false == isset( $arrobjCareMeterRates[$intKey + 1] ) ) {
					continue;
				}

				$objNextMeterRate = $arrobjCareMeterRates[$intKey + 1];

				if( CMeterRateType::BASELINE_RATE != $this->getMeterCareRateTypeId() && 1 != ( $objNextMeterRate->getStartCriteria() - $objMeterRate->getEndCriteria() ) ) {
					$objMeterRate->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate_diff', 'The gap between end range and next start range should be 1.' ) );
					$boolIsValid = false;
				}
			}

			if( false == $boolIsValid ) {
				break;
			}
		}

		return $boolIsValid;
	}

	public function valLowUsageThreshhold() {
		$boolIsValid = true;

		if( false == is_null( $this->getLowUsageThreshhold() ) ) {
			if( 0 > $this->getLowUsageThreshhold() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'low_usage_threshhold', 'You cannot specify a negative value for Low Usage Threshold.' ) );
			} elseif( false == is_null( $this->getHighUsageThreshhold() ) && $this->getLowUsageThreshhold() > $this->getHighUsageThreshhold() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'low_usage_threshhold', 'Low Usage Threshold value should be lesser or equal to High Usage Threshold.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valHighUsageThreshhold() {
		$boolIsValid = true;

		if( false == is_null( $this->getHighUsageThreshhold() ) && 0 > $this->getHighUsageThreshhold() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'high_usage_threshhold', 'You cannot specify a negative value for High Usage Threshold.' ) );
		}

		return $boolIsValid;
	}

	public function valBilledConsumptionThreshold() {
		$boolIsValid = true;

		if( true == is_null( $this->getBilledConsumptionThreshold() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billed_consumptio_threshold', 'Billed Consumption Threshold is required.' ) );
		} elseif( 0 > $this->getBilledConsumptionThreshold() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billed_consumptio_threshold', 'You cannot specify a negative value for Billed Consumption Threshold.' ) );
		}

		return $boolIsValid;
	}

	public function valTrueUpPercent( $objUtilitiesDatabase ) {
		$boolIsValid = true;
		$objPropertyUtilitySetting = \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingByPropertyId( $this->getPropertyId(), $objUtilitiesDatabase );

		if( true == valObj( $objPropertyUtilitySetting, 'CPropertyUtilitySetting' ) && true == $objPropertyUtilitySetting->getIsInImpleMentation() && false == is_numeric( $this->getTrueUpPercent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'true_up_percent', 'Invalid true up value.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objUtilitiesDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUtilityTransmissionTypeId();
				$boolIsValid &= $this->valInstallationCompanyName();
				$boolIsValid &= $this->valTamperFee();
				$boolIsValid &= $this->valRdlDccPhoneNumber();
				$boolIsValid &= $this->valTransmissionExpectedDay();
				$boolIsValid &= $this->valTransmissionDeadlineDay();
				$boolIsValid &= $this->valTransmissionEmergencyDay();
				$boolIsValid &= $this->valExpectedInstallCompletion( $objUtilitiesDatabase );
				$boolIsValid &= $this->valNormalMeterRates();
				$boolIsValid &= $this->valCareMeterRates();
				$boolIsValid &= $this->valBtuFactorAndDailyAllowance();
				$boolIsValid &= $this->valLowUsageThreshhold();
				$boolIsValid &= $this->valHighUsageThreshhold();
				$boolIsValid &= $this->valBilledConsumptionThreshold();
				$boolIsValid &= $this->valTrueUpPercent( $objUtilitiesDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

        return $boolIsValid;
	}

    public function fetchMeterRates( $objUtilitiesDatabase ) {
    	return \Psi\Eos\Utilities\CMeterRates::createService()->fetchMeterRatesByPropertyMeteringSettingId( $this->getId(), $objUtilitiesDatabase );
    }

    public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	if( true == is_null( $this->getId() ) ) {
    		return parent::insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	    }

		$arrstrTempOriginalValues = $arrstrOriginalValues = $arrstrOriginalValueChanges = $arrmixOriginalValues = [];
		$boolChangeFlag				= false;

		$objPropertyMeteringSetting = new CPropertyMeteringSetting();
	    $arrmixOriginalValues = \Psi\Eos\Utilities\CPropertyMeteringSettings::createService()->fetchCustomPropertyMeteringSettingById( $this->getId(), $objDatabase, true );
		$arrstrTempOriginalValues = $arrmixOriginalValues[0];

		if( true == valArr( $arrstrTempOriginalValues ) ) {
			foreach( $arrstrTempOriginalValues as $strKey => $mixOriginalValue ) {

				$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
				$strGetFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
				$strSetFunctionName = 'set' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

				$objPropertyMeteringSetting->$strSetFunctionName( $mixOriginalValue );

				if( $this->$strGetFunctionName() != $objPropertyMeteringSetting->$strGetFunctionName() ) {

					$strChangedValue = $this->$strSqlFunctionName();
					if( 'NOW()' === $this->$strGetFunctionName() ) {
						$strChangedValue = date( 'Y-m-d H:i:s' );
					}

					$arrstrOriginalValueChanges[$this->getPropertyUtilityTypeName() . '~' . $strKey]	= $strChangedValue;
					$arrstrOriginalValues[$this->getPropertyUtilityTypeName() . '~' . $strKey]			= $objPropertyMeteringSetting->$strSqlFunctionName();
					$boolChangeFlag																		= true;
				}
			}
		}

    	$boolUpdateStatus = parent::insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

    	if( false != $boolUpdateStatus && true == valArr( $arrstrTempOriginalValues ) ) {

    		if( false != $boolChangeFlag ) {
    			$objUtilitySettingLog = new CUtilitySettingLog();

    			$objUtilitySettingLog->setCid( $arrstrTempOriginalValues['cid'] );
    			$objUtilitySettingLog->setPropertyId( ( int ) $this->getPropertyId() );
    			$objUtilitySettingLog->setTableName( 'property_metering_settings' );
    			$objUtilitySettingLog->setReferenceId( $this->getId() );
    			$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
    			$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValueChanges ) ) );
    			$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

    			if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
    				return false;
    			}
    		}

    		return $boolUpdateStatus;
    	}

    	return false;
    }

}
?>