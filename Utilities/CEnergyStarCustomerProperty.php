<?php

class CEnergyStarCustomerProperty extends CBaseEnergyStarCustomerProperty {

	public function valPropertyName() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyName() ) || 3 > strlen( trim( $this->getPropertyName() ) ) ) {
			if( 0 == strlen( trim( $this->getPropertyName() ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'Property name is required.' ) );
				$boolIsValid = false;
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'A property name must contain at least 3 characters.' ) );
				$boolIsValid = false;
			}
		}

		$strPropertyName = preg_replace( '/[^a-zA-Z0-9\s]/', '', trim( $this->getPropertyName() ) );

		if( 0 == strlen( $strPropertyName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'Invalid Property Name.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valAddressLine() {
		$boolIsValid = true;

		if( true == is_null( $this->getAddressLine() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_line', 'Address line is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;

		if( true == is_null( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', 'State name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;

		$intLength = strlen( str_replace( '-', '', $this->getPostalCode() ) );

		if( true == is_null( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal Code is required.' ) );
		} elseif( false == is_null( $this->getPostalCode() ) && 0 < $intLength && ( false == preg_match( '/^\d{5,5}?$/', $this->getPostalCode() ) && false == preg_match( '/^\d{5,5}-\d{4,4}?$/', $this->getPostalCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be 5 or 10 characters in XXXXX or XXXXX-XXXX format.' ) );
		}

		return $boolIsValid;
	}

	public function getYearBuilt() {

		if( $this->m_strYearBuilt == NULL || false === \Psi\CStringService::singleton()->strpos( $this->m_strYearBuilt, '/' ) ) {
			return $this->m_strYearBuilt;
		}

		$arrstrDateParts = explode( '/', $this->m_strYearBuilt );

		$intYear = NULL;

		if( 3 <= \Psi\Libraries\UtilFunctions\count( $arrstrDateParts ) ) {
			$intYear = $arrstrDateParts[2];
		}

		return $intYear;
	}

	public function setYearBuilt( $strYearBuilt ) {
		$strYearBuilt = trim( $strYearBuilt );

		if( is_numeric( $strYearBuilt ) ) {
			if( $strYearBuilt >= 0 && $strYearBuilt <= 99 ) {
				$strYearBuilt += 1900;
			}

			$strYearBuilt = $strYearBuilt ? sprintf( '%02d/%02d/%04d', 1, 1, ( int ) $strYearBuilt ) : NULL;
		}
		$this->m_strYearBuilt = CStrings::strTrimDef( $strYearBuilt, -1, NULL, true );
	}

	public function valYearBuilt() {
		$boolIsValid = true;

		$strUnformattedYearBuiltDate = $this->m_strYearBuilt;

		if( true == is_null( $strUnformattedYearBuiltDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year_built', 'Please add year built.' ) );
		} elseif( false == is_null( $strUnformattedYearBuiltDate ) && ( false == CValidation::validateDate( $strUnformattedYearBuiltDate ) ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year_built', 'Year built does not appear valid.' ) );
		}

		return $boolIsValid;
	}

	public function valGrossFloorArea() {
		$boolIsValid = true;

		if( true == is_null( $this->getGrossFloorArea() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gross_floor_area', 'Gross floor area is required.' ) );
		} elseif( false == is_numeric( $this->getGrossFloorArea() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gross_floor_area', 'Please entrer valid Gross floor area.' ) );
		}

		return $boolIsValid;
	}

	public function valOccupancyPercentage() {
		$boolIsValid = true;

		if( true == is_null( $this->getOccupancyPercentage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupancy_percentage', 'Occupancy percentage is required.' ) );
		} elseif( false == is_numeric( $this->getOccupancyPercentage() ) || 100 < $this->getOccupancyPercentage() || 0 >= $this->getOccupancyPercentage() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gross_floor_area', 'Please entrer valid occupancy percentage.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyName();
				$boolIsValid &= $this->valAddressLine();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valYearBuilt();
				$boolIsValid &= $this->valGrossFloorArea();
				$boolIsValid &= $this->valOccupancyPercentage();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createEnergyStarCustomerBuilding() {
		$objEnergyStarCustomerBuilding = new CEnergyStarCustomerBuilding();
		$objEnergyStarCustomerBuilding->setEnergyStarCustomerPropertyId( $this->getId() );
		return $objEnergyStarCustomerBuilding;
	}

}
?>