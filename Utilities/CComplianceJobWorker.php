<?php

class CComplianceJobWorker extends CBaseComplianceJobWorker {

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valVendorInvitationId() {
		return true;
	}

	public function valComplianceJobId() {
		return true;
	}

	public function valWorkerId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>