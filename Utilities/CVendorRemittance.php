<?php

class CVendorRemittance extends CBaseVendorRemittance {

	/**
	 * Get Functions
	 */

	public function getCheckAccountNumber() {
		if( false == valStr( $this->m_strCheckAccountNumberEncrypted ) ) {
			return false;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getCheckAccountNumberMasked() {
		if( false == valStr( $this->m_strCheckAccountNumberEncrypted ) ) {
			return false;
		}
		$strCheckAccountNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
		$intStringLength	   = strlen( $strCheckAccountNumber );
		$strLastFour		   = \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, -4 );

		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	/**
	 * Set Functions
	 */

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		$strCheckAccountNumber = trim( $strCheckAccountNumber );

		if( true == \Psi\CStringService::singleton()->strpos( $strCheckAccountNumber, 'X' ) ) {
			return;
		}

		$strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 20, NULL, true );
		if( true == valStr( $strCheckAccountNumber ) ) {
			$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
		}
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['check_account_number'] ) ) {
			$this->setCheckAccountNumber( $arrmixValues['check_account_number'] );
		}
	}

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valPaymentTypeId() {
		return true;
	}

	public function valCheckAccountTypeId() {
		return true;
	}

	public function valCheckNameOnAccount() {
		$boolIsValid = true;
		if( true == is_null( $this->getCheckNameOnAccount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', 'Name on account is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCheckBankName() {
		$boolIsValid = true;
		if( true == is_null( $this->getCheckBankName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', 'Bank name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCheckRoutingNumber() {

		$boolIsValid = true;

		if( true == is_null( $this->getCheckRoutingNumber() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number is required.' ) );
			return false;
		}

		if( 0 !== preg_match( '/[^0-9]/', $this->getCheckRoutingNumber() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be a number.' ) );
			return false;
		}

		if( 9 != strlen( $this->getCheckRoutingNumber() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must contain only 9 digits.' ) );
			return false;
		}

		return $boolIsValid;

	}

	public function valCheckAccountNumberEncrypted() {
		return true;
	}

	public function valStreetLine3() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valName() {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Remittance name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCheckAccountNumber() {
		$boolIsValid = true;
		if( true == is_null( $this->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', 'Account number is required.' ) );
		}
		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strStreetLine1 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', 'Address is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine2() {
		return true;
	}

	public function valCity( $boolIsRequired = true ) {

		if( true == $boolIsRequired && true == is_null( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City is required.' ) );
		} elseif( true == is_numeric( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City can not be numeric.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strStateCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', 'State code is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPostalCode( $boolIsCheckPostalCode = false ) {
		$boolIsValid = true;

		if( true == $boolIsCheckPostalCode && true == is_null( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code is required.' ) );
		}

		if( true == valStr( $this->getPostalCode() ) && false == CValidation::validatePostalCode( $this->getPostalCode(), CCountry::CODE_USA ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Valid postal code is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				if( CApPaymentType::ACH == $this->getPaymentTypeId() ) {
					$boolIsValid &= $this->valCheckBankName();
					$boolIsValid &= $this->valCheckNameOnAccount();
					$boolIsValid &= $this->valCheckRoutingNumber();
					$boolIsValid &= $this->valCheckAccountNumber();
					$boolIsValid &= $this->valStreetLine1();
					$boolIsValid &= $this->valCity();
					$boolIsValid &= $this->valStateCode();
					$boolIsValid &= $this->valPostalCode( true );
				} else {
					$boolIsValid &= $this->valPostalCode();
				}
				$boolIsValid &= $this->valCity( false );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function getDecryptedCheckAccountNumber() {
		if( false == valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function encryptAccountNumber() {
		if( false == valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}
		$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

}
?>