<?php

class CUtilityMoveOutType extends CBaseUtilityMoveOutType {

	const WITH_MOVE_OUT			= 1;
	const INDIVIDUAL_MOVE_OUT	= 2;
	const AUTOMATED_MOVE_OUT	= 3;
	const BULK_MOVE_OUT			= 4;

}
?>