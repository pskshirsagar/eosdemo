<?php

class COrderHeader extends CBaseOrderHeader {

	const TRANSACTION_AMOUNT_LIMIT = 1000000000;

	const VALIDATE_API_INVOICE_INSERT = 'validate_api_invoice_insert';

	protected $m_intBulkPropertyId;

	protected $m_strCompanyName;
	protected $m_strLocationName;
	protected $m_strAccountNumber;

	protected $m_arrobjOrderDetails;

	protected $m_arrintBuyerLocationIds;
	protected $m_arrintPoApHeaderIds;
	protected $m_arrintPoPropertyIds;

	protected $m_arrstrPoApHeaderNumbers;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjOrderDetails     = [];
		$this->m_arrintPoApHeaderIds    = [];
		$this->m_arrintBuyerLocationIds = [];
	}

	/**
	 * @return int
	 */
	public function getBulkPropertyId() {
		return $this->m_intBulkPropertyId;
	}

	public function setPoPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintPoPropertyIds = $arrintPropertyIds;
	}

	/**
	 * @param mixed $intBulkPropertyId
	 */
	public function setBulkPropertyId( $intBulkPropertyId ) {
		$this->m_intBulkPropertyId = $intBulkPropertyId;
	}

	/**
	 * @return mixed
	 */
	public function getPoApHeaderNumbers() {
		return $this->m_arrstrPoApHeaderNumbers;
	}

	/**
	 * @param mixed $arrstrPoApHeaderNumbers
	 */
	public function setPoApHeaderNumbers( $arrstrPoApHeaderNumbers ) {
		$this->m_arrstrPoApHeaderNumbers = $arrstrPoApHeaderNumbers;
	}

	/**
	 * @return array
	 */
	public function getPoApHeaderIds() : array {
		return $this->m_arrintPoApHeaderIds;
	}

	/**
	 * @param array $arrintPoApHeaderIds
	 */
	public function setPoApHeaderIds( array $arrintPoApHeaderIds ) {
		$this->m_arrintPoApHeaderIds = $arrintPoApHeaderIds;
	}

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valStoreId() {
		return true;
	}

	public function valBuyerId() {
		return true;
	}

	public function valBulkBuyerLocationId() {
		$boolIsValid = true;

		if( false == valId( $this->getBulkBuyerLocationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Please select any property.' ) );
		}

		return $boolIsValid;
	}

	public function valBuyerAccountId() {
		return true;
	}

	public function valVendorTermId() {
		return true;
	}

	public function valOrderTypeId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valApHeaderId() {
		return true;
	}

	public function valInvoicedApHeaderId() {
		return true;
	}

	public function valComplianceJobId() {
		return true;
	}

	public function valOrderHeaderIds() {
		return true;
	}

	public function valAssignedWorkerId() {
		return true;
	}

	public function valTransactionDatetime() {
		return true;
	}

	public function valPostDate() {

		if( false == valStr( $this->getPostDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'post_date', __( 'Invoice date is required.' ) ) );
			return $boolIsValid;
		} elseif( false == CValidation::validateDate( $this->getPostDate() ) || false == preg_match( '#^(((0?[0-9])|(1[012]))/(0?[0-9]|[0-2][0-9]|3[0-1])/[0-9]{4})$#', $this->getPostDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'post_date', __( 'Invoice date should be in mm/dd/yyyy format.' ) ) );
			return $boolIsValid;
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valDueDate() {

		if( false == valStr( $this->getDueDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', 'Invoice due date is required.' ) );
		} elseif( false == CValidation::validateDate( $this->getDueDate() )
		          || false == preg_match( '#^(((0?[0-9])|(1[012]))/(0?[0-9]|[0-2][0-9]|3[0-1])/[0-9]{4})$#', $this->getDueDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', 'Invoice due date should be in mm/dd/yyyy format.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valPostDateAndDueDate( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( false == ( int ) \Psi\CStringService::singleton()->substr( $this->getPostDate(), 0, 2 ) || false == valStr( $this->getDueDate() ) ) {
			return true;
		}

		$intPostDate = $this->getStrToTimeByDate( $this->getPostDate() );
		$intDueDate = $this->getStrToTimeByDate( $this->getDueDate() );

		if( $intPostDate > $intDueDate ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', __( 'Invoice due date should be greater than or equal to invoice date.' ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	private function getStrToTimeByDate( $strDate ) {

		return mktime( 0, 0, 0, ( int ) \Psi\CStringService::singleton()->substr( $strDate, 0, 2 ), ( int ) \Psi\CStringService::singleton()->substr( $strDate, 3, 2 ), ( int ) \Psi\CStringService::singleton()->substr( $strDate, 6, 4 ) );
	}

	public function valHeaderNumber( $objUtilitiesDatabase, $objClientDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_strHeaderNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Invoice Number is required.' ) );
		} elseif( true == valObj( $objClientDatabase, 'CDatabase' ) ) {
			$objCompanyPreference = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'ALLOW_DUPLICATE_INVOICE_NUMBERS_PER_VENDOR', $this->getCid(), $objClientDatabase );
			if( true == valObj( $objUtilitiesDatabase, 'CDatabase' ) && ( false == valObj( $objCompanyPreference, 'CCompanyPreference' ) || 1 != $objCompanyPreference->getValue() ) ) {
				$strWhereForOrderHeader = ' WHERE lower( header_number ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getHeaderNumber() ) ) . '\' AND id != ' . ( int ) $this->getId() . ' AND buyer_id = ' . ( int ) $this->getBuyerId() . ' AND vendor_id = ' . ( int ) $this->getVendorId() . ' AND ap_header_type_id = ' . ( int ) CApHeaderType::INVOICE . ' AND deleted_by IS NULL AND deleted_on IS NULL';
				$strWhereForApHeader    = NULL;

				// validating from ap_header table
				$objApPayeeAccount = CEndureInvoices::loadApPayeeAccount( $this->getBuyerAccountId(), $this->getCid(), $objClientDatabase );
				if( true == valObj( $objApPayeeAccount, 'CApPayeeAccount' ) ) {
					$objApPayee = CApPayees::fetchApPayeeByIdByCid( $objApPayeeAccount->getApPayeeId(), $this->getCid(), $objClientDatabase );

					if( true == valObj( $objApPayee, 'CApPayee' ) ) {
						$strWhereForApHeader = 'WHERE
													cid = ' . ( int ) $this->getCid() . '
													AND lower( header_number ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getHeaderNumber() ) ) . '\'
													AND ap_payee_id = ' . ( int ) $objApPayee->getId() . '
													AND id != ' . ( int ) $this->getApHeaderId() . '
													AND ap_header_type_id = ' . CApHeaderType::INVOICE . '
													AND gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE . '
													AND reversal_ap_header_id IS NULL
													AND EXISTS (
																SELECT NULL FROM
																	company_preferences cp
																WHERE
																	cp.cid = ' . ( int ) $this->getCid() . '
																	AND cp.key = \'ALLOW_DUPLICATE_INVOICE_NUMBERS_PER_VENDOR\'
																	AND cp.value = \'0\'
															)
													AND deleted_by IS NULL
													AND deleted_on IS NULL';
					}
				}

				if( 0 < strlen( $this->m_strHeaderNumber ) && ( 0 < \Psi\Eos\Utilities\COrderHeaders::createService()->fetchOrderHeaderCount( $strWhereForOrderHeader, $objUtilitiesDatabase ) || ( false == is_null( $strWhereForApHeader ) && 0 < CApHeaders::fetchApHeaderCount( $strWhereForApHeader, $objClientDatabase ) ) ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Invoice number already exists for the selected customer.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function validUrl( $strUrl ) {

		$boolIsValid = true;

		if( false == is_null( $strUrl ) && false == filter_var( $strUrl, FILTER_VALIDATE_URL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Invalid URL.' ) );
		}

		return $boolIsValid;
	}

	public function valHeaderMemo() {
		return true;
	}

	public function valControlTotal() {
		return true;
	}

	public function valTransactionAmount() {
		$boolIsValid = true;

		if( self::TRANSACTION_AMOUNT_LIMIT < abs( $this->getTransactionAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'Transaction amount should be between -$1,000,000,000 and $1,000,000,000.' ) );
		}

		return $boolIsValid;
	}

	public function valRequirePosForInvoices( $objUtilitiesDatabase, $objClientDatabase ) {
		$boolIsValid                = true;
		$arrstrRequiredPOProperties = $arrstrProperties = $arrintPropertyIds = $arrobjBuyerLocations = $arrintBuyerLocationIds = $arrintOrderDetailBuyerLocationIds = [];

		if( false == valObj( $objUtilitiesDatabase, 'CDatabase' ) || false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			return $boolIsValid;
		}
		$arrobjApPayeeAccounts = ( array ) CApPayeeAccounts::fetchApPayeeAccountsByBuyerAccountIdByCid( $this->getBuyerAccountId(), $this->getCid(), $objClientDatabase );
		$objApPayeeAccount     = array_pop( $arrobjApPayeeAccounts );

		$objApPayee = NULL;

		if( true == valObj( $objApPayeeAccount, 'CApPayeeAccount' ) ) {
			$objApPayee = CApPayees::fetchActiveApPayeeByIdByCid( $objApPayeeAccount->getApPayeeId(), $this->getCid(), $objClientDatabase );
		}

		if( true == valObj( $objApPayee, 'CApPayee' ) && true == $objApPayee->getRequirePoForInvoice() ) {
			if( true == valArr( $this->m_arrobjOrderDetails ) ) {
				foreach( $this->m_arrobjOrderDetails as $objOrderDetail ) {
					$intBuyerLocationId                          = $objOrderDetail->getBuyerLocationId();
					$arrintBuyerLocationIds[$intBuyerLocationId] = $intBuyerLocationId;

					if( true == valId( $objOrderDetail->getOrderDetailId() ) ) {
						$arrintOrderDetailBuyerLocationIds[$intBuyerLocationId] = $intBuyerLocationId;
					}
				}

				$arrintBuyerLocationIds = array_diff( $arrintBuyerLocationIds, $arrintOrderDetailBuyerLocationIds );
			}

			if( true == ( $arrintBuyerLocationIds = getIntValuesFromArr( $arrintBuyerLocationIds ) ) ) {
				$arrobjBuyerLocations = ( array ) \Psi\Eos\Utilities\CBuyerLocations::createService()->fetchBuyerLocationsByIds( $arrintBuyerLocationIds, $objUtilitiesDatabase );

				foreach( $arrobjBuyerLocations as $objBuyerLocation ) {
					$arrstrProperties[$objBuyerLocation->getPropertyId()]  = $objBuyerLocation->getLocationName();
					$arrintPropertyIds[$objBuyerLocation->getPropertyId()] = $objBuyerLocation->getPropertyId();
				}

				$arrmixCompanyPreferences = ( array ) CPropertyPreferences::fetchCustomPropertyPreferencesByKeyByPropertyIdsByCid( 'REQUIRE_POS_FOR_INVOICES', $arrintPropertyIds, $this->getCid(), $objClientDatabase );

				if( true == valArr( $arrmixCompanyPreferences ) ) {
					foreach( $arrmixCompanyPreferences as $arrmixCompanyPreference ) {
						if( 1 == $arrmixCompanyPreference['value'] ) {
							$arrstrRequiredPOProperties[] = $arrstrProperties[$arrmixCompanyPreference['property_id']];
						}
					}

					if( true == valArr( $arrstrRequiredPOProperties ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_pos', 'A PO is required to add an invoice for ap payee ' . \Psi\CStringService::singleton()->ucwords( $objApPayee->getCompanyName() ) . ' for property(s) ' . sqlStrImplode( $arrstrRequiredPOProperties ) . '.' ) );
						$boolIsValid = false;
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valPoTypes( $objUtilitiesDatabase ) {
		$boolIsValid          = true;
		$arrintOrderHeaderIds = rekeyObjects( 'ApHeaderSubTypeId', ( array ) \Psi\Eos\Utilities\COrderHeaders::createService()->fetchActiveOrderHeadersByIdsByVendorId( $this->getOrderHeaderIds(), $this->getVendorId(), $objUtilitiesDatabase ) );
		if( 1 < \Psi\Libraries\UtilFunctions\count( $arrintOrderHeaderIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_pos', 'Invoice can not be created for multiple PO types.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valBuyerLocations( $objUtilitiesDatabase ) {
		$boolIsValid = true;

		if( true == valObj( $objUtilitiesDatabase, 'CDatabase' ) ) {
			$arrintOrderHeaderBuyerLocationIds = ( array ) getObjectFieldValuesByFieldName( 'BuyerLocationId', $this->m_arrobjOrderDetails );

			// load buyer location ids if not set
			if( false == valArr( $this->m_arrintBuyerLocationIds ) ) {
				$this->loadBuyerLocationIds( $objUtilitiesDatabase );
			}
			$arrintUnAssociatedBuyerLocationIds = array_diff( $arrintOrderHeaderBuyerLocationIds, $this->m_arrintBuyerLocationIds );

			if( true == valArr( $arrintUnAssociatedBuyerLocationIds ) ) {
				$arrobjUnAssociatedBuyerLocations = ( array ) \Psi\Eos\Utilities\CBuyerLocations::createService()->fetchBuyerLocationsByIds( $arrintUnAssociatedBuyerLocationIds, $objUtilitiesDatabase );
				$arrstrBuyerLocationNames         = ( array ) getObjectFieldValuesByFieldName( 'LocationName', $arrobjUnAssociatedBuyerLocations );

				if( true == valArr( $arrstrBuyerLocationNames ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', ' The property(s) \'' . sqlStrImplode( $arrstrBuyerLocationNames ) . '\' are not associated with selected customer/account.' ) );
					$boolIsValid = false;
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', ' The property(s) are not associated with selected customer/account.' ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valTransactionAmountDue() {
		return true;
	}

	public function valTaxAmount() {
		return true;
	}

	public function valDiscountAmount() {
		return true;
	}

	public function valShippingAmount() {
		return true;
	}

	public function valInvoicedBy() {
		return true;
	}

	public function valInvoicedOn() {
		return true;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function valOrderDetails() : bool {
		$boolIsValid = true;

		if( false == valArr( $this->getOrderDetails() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_details', 'At least one detail line item should be required.' ) );
		}

		return $boolIsValid;
	}

	public function valOrderDetailsTotal() : bool {
		if( number_format( $this->getTransactionAmount(), 2, '.', '' ) != number_format( $this->getOrderDetailsTotal(), 2, '.', '' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Invoice total and the sum of the detail lines should be the same.' ) );
		} elseif( 0 == $this->getOrderDetailsTotal() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Sum of the detail line items should not be zero.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	private function getOrderDetailsTotal() {
		$fltTotalAmount = 0;
		foreach( $this->getOrderDetails() as $objOrderDetail ) {
			$fltTotalAmount = bcadd( $fltTotalAmount, $objOrderDetail->getTransactionAmount(), 2 );

		}

		return $fltTotalAmount;
	}

	/**
	 * Set Functions
	 */

	public function setOrderDetails( $arrobjOrderDetails ) {
		$this->m_arrobjOrderDetails = ( true == valArr( $arrobjOrderDetails ) ) ? $arrobjOrderDetails : [];
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = CStrings::strTrimDef( $strCompanyName, 100, NULL, true );
	}

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = CStrings::strTrimDef( $strLocationName, 100, NULL, true );
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = CStrings::strTrimDef( $strAccountNumber, 50, NULL, true );
	}

	public function setBuyerLocationIds( $arrintBuyerLocationIds ) {
		$this->m_arrintBuyerLocationIds = ( true == ( $arrintBuyerLocationIds = getIntValuesFromArr( $arrintBuyerLocationIds ) ) ) ? $arrintBuyerLocationIds : [];
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getBuyerLocationIds() {
		return $this->m_arrintBuyerLocationIds;
	}

	public function getOrderDetails() {
		return $this->m_arrobjOrderDetails;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['company_name'] ) && $boolDirectSet ) {
			$this->m_strCompanyName = trim( stripcslashes( $arrmixValues['company_name'] ) );
		} elseif( true == isset( $arrmixValues['company_name'] ) ) {
			$this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['company_name'] ) : $arrmixValues['company_name'] );
		}

		if( isset( $arrmixValues['location_name'] ) && $boolDirectSet ) {
			$this->m_strLocationName = trim( stripcslashes( $arrmixValues['location_name'] ) );
		} elseif( isset( $arrmixValues['location_name'] ) ) {
			$this->setLocationName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['location_name'] ) : $arrmixValues['location_name'] );
		}

		if( isset( $arrmixValues['account_number'] ) && $boolDirectSet ) {
			$this->m_strAccountNumber = trim( stripcslashes( $arrmixValues['account_number'] ) );
		} elseif( isset( $arrmixValues['account_number'] ) ) {
			$this->setAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_number'] ) : $arrmixValues['account_number'] );
		}

	}

	public function validate( $strAction, $objUtilitiesDatabase = NULL, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTransactionAmount();

				if( CApHeaderType::INVOICE == $this->getApHeaderTypeId() ) {
					$boolIsValid &= $this->valHeaderNumber( $objUtilitiesDatabase, $objClientDatabase );
					$boolIsValid &= $this->valPostDate();
					$boolIsValid &= $this->valDueDate();
					$boolIsValid &= $this->valPostDateAndDueDate( $objClientDatabase );
					$boolIsValid &= $this->valRequirePosForInvoices( $objUtilitiesDatabase, $objClientDatabase );
					$boolIsValid &= $this->valPoTypes( $objUtilitiesDatabase );
				}

				$boolIsValid &= $this->valBuyerLocations( $objUtilitiesDatabase );
				break;

			case self::VALIDATE_API_INVOICE_INSERT:
				$boolIsValid &= $this->valTransactionAmount();

				if( CApHeaderType::INVOICE == $this->getApHeaderTypeId() ) {
					$boolIsValid &= $this->valHeaderNumber( $objUtilitiesDatabase, $objClientDatabase );
					$boolIsValid &= $this->valPostDate();
					$boolIsValid &= $this->valDueDate();
					$boolIsValid &= $this->valPostDateAndDueDate( $objClientDatabase );
					$boolIsValid &= $this->valRequirePosForInvoices( $objUtilitiesDatabase, $objClientDatabase );
					$boolIsValid &= $this->valPoTypes( $objUtilitiesDatabase );
				}

				$boolIsValid &= $this->valBuyerLocations( $objUtilitiesDatabase );

				$boolIsValid &= $this->valOrderDetails();
				$boolIsValid &= $this->valOrderDetailsTotal();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objUtilitiesDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objUtilitiesDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objUtilitiesDatabase, $boolReturnSqlOnly );
	}

	public function createOrderDetail() {
		$objOrderDetail = new COrderDetail();
		$objOrderDetail->setCid( $this->getCid() );
		$objOrderDetail->setVendorId( $this->getVendorId() );

		return $objOrderDetail;
	}

	private function loadBuyerLocationIds( $objUtilitiesDatabase ) {
		$this->m_arrintBuyerLocationIds = array_keys( ( array ) \Psi\Eos\Utilities\CBuyerLocations::createService()->fetchBuyerLocationsByBuyerAccountIdByBuyerIdByVendorId( $this->getBuyerAccountId(), $this->getBuyerId(), $this->getVendorId(), $objUtilitiesDatabase ) );
	}

}

?>