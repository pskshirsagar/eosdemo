<?php

class CRadioPartType extends CBaseRadioPartType {

    public function valRadioManufacturerId() {
        $boolIsValid = true;

        if( true == is_null( $this->getRadioManufacturerId() ) ) {
           $boolIsValid = false;
           $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'radio_manufacturer_id', 'Please select radio manufacturer.' ) );
        }

        return $boolIsValid;
    }

    public function valName( $objDatabase ) {

        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
            return $boolIsValid;
        } elseif( 0 < preg_match( '@[^a-z0-9 ]+@i', $this->getName() ) ) {
           	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Special characters are not allowed in name.' ) );
            return $boolIsValid;
       	}

        $strSql = ' WHERE name = \'' . $this->getName() . '\'';
        if( 0 < $this->getId() ) {
        	$strSql .= ' AND id != ' . ( int ) $this->getId();
        }

        $intRadioPartTypeCount = \Psi\Eos\Utilities\CRadioPartTypes::createService()->fetchRadioPartTypeCount( $strSql, $objDatabase );

     	if( 0 < $intRadioPartTypeCount ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name already exists.' ) );
            return $boolIsValid;
     	}

        return $boolIsValid;
    }

 	public function valDependancy( $objDatabase = NULL ) {

		$boolIsValid = true;

		$intPropertyMeterDeviceCount = \Psi\Eos\Utilities\CPropertyMeterDevices::createService()->fetchPropertyMeterDeviceCount( 'WHERE radio_part_type_id = ' . ( int ) $this->getId(), $objDatabase );

	     if( 0 < $intPropertyMeterDeviceCount ) {
           $boolIsValid = false;
           $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Cannot delete radio part type ' . $this->getName() . ' as it depends on property meter device.' ) );
	     }

		return $boolIsValid;
 	}

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valRadioManufacturerId();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valDependancy( $objDatabase );
           		break;

           	default:
           		// default case
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }

}
?>