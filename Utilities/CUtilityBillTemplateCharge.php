<?php

class CUtilityBillTemplateCharge extends CBaseUtilityBillTemplateCharge {

	protected $m_boolIsIgnored;

 	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
 		 parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
      	 if( true == isset( $arrmixValues['is_ignored'] ) ) {
			$this->setIsIgnored( $arrmixValues['is_ignored'] );
		 }
    }

    public function setIsIgnored( $intIsIgnored ) {
    	$this->m_boolIsIgnored = CStrings::strToBool( $intIsIgnored );
    }

    public function getIsIgnored() {
    	return $this->m_boolIsIgnored;
    }

    public function valId( $objDatabase ) {
        $boolIsValid = true;

		$intUtilityBillChargeCount = \Psi\Eos\Utilities\CUtilityBillCharges::createService()->fetchUtilityBillChargeCount( ' WHERE utility_bill_template_charge_id = ' . ( int ) $this->getId(), $objDatabase );

        if( 0 < $intUtilityBillChargeCount ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Utility Bill Template Charge ( ' . $this->getName() . ' ) already linked with Bill Charges.' ) );
        }

        $intUtilityBillAccountTemplateChargeCount = \Psi\Eos\Utilities\CUtilityBillAccountTemplateCharges::createService()->fetchUtilityBillAccountTemplateChargeCount( ' WHERE utility_bill_template_charge_id = ' . ( int ) $this->getId(), $objDatabase );

        if( 0 < $intUtilityBillAccountTemplateChargeCount ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Utility Bill Template Charge ( ' . $this->getName() . ' ) already linked with Utility bill account template charge.' ) );
        }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Template charge name is required field.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId( $objDatabase );
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function createUtilityBillAccountTemplateCharge( $objProperty ) {
    	$objUtilityBillAccountTemplateCharge = new CUtilityBillAccounttemplateCharge();
    	$objUtilityBillAccountTemplateCharge->setDefaults();
    	$objUtilityBillAccountTemplateCharge->setCid( $objProperty->getCid() );
		$objUtilityBillAccountTemplateCharge->setPropertyId( $objProperty->getId() );
		return $objUtilityBillAccountTemplateCharge;
    }

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		$arrstrTempOriginalValues 	= $arrstrOriginalValues = $arrstrOriginalValueChanges = [];
		$boolChangeFlag				= false;

		$arrmixOriginalValues = fetchData( 'SELECT * FROM utility_bill_template_charges WHERE id = ' . ( int ) $this->getId(), $objDatabase );
		if( true == valArr( $arrmixOriginalValues ) ) {

			$arrstrTempOriginalValues = $arrmixOriginalValues[0];

			foreach( $arrstrTempOriginalValues as $strKey => $strMixOriginalValue ) {

				$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
				$strFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

				if( true == method_exists( $this, $strSqlFunctionName ) && CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $strMixOriginalValue ) {
					$arrstrOriginalValueChanges[$strKey] 	= $this->$strFunctionName();
					$arrstrOriginalValues[$strKey] 			= $strMixOriginalValue;
					$boolChangeFlag 						= true;
				}
			}
		}

		$boolUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setTableName( 'utility_bill_template_charges' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}

			return $boolUpdateStatus;
		}

		return true;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		$arrmixColumnNames = fetchData( 'select column_name from information_schema.columns where table_name = \'utility_bill_template_charges\' ', $objDatabase );

		foreach( $arrmixColumnNames as $arrmixColumnName ) {

			$strFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $arrmixColumnName['column_name'] ) ) );

			if( true == method_exists( $this, $strFunctionName ) ) {
				$arrstrOriginalValues[$arrmixColumnName['column_name']] = $this->$strFunctionName();
			}
		}

		$boolInsertStatus = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolInsertStatus && false == $boolSkipLog ) {

			$objUtilitySettingLog = new CUtilitySettingLog();
			$objUtilitySettingLog->setTableName( 'utility_bill_template_charges' );
			$objUtilitySettingLog->setReferenceId( $this->getId() );
			$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
			$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

			if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
				return false;
			}

			return $boolInsertStatus;
		}

		return true;

	}

	public function deleteUtilityBillTemplateCharge( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = false ) {

		if( false == $boolIsSoftDelete ) {
			return $this->delete( $intCurrentUserId, $objDatabase );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return $this->update( $intCurrentUserId, $objDatabase );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		$arrstrOriginalValues = [];
		$arrmixOriginalValues = fetchData( 'SELECT * FROM utility_bill_template_charges WHERE id = ' . ( int ) $this->getId(), $objDatabase );

		if( true == valArr( $arrmixOriginalValues ) ) {

			$arrstrTempOriginalValues	= $arrmixOriginalValues[0];
			foreach( $arrstrTempOriginalValues as $strKey => $strMixOriginalValue ) {

				$arrstrOriginalValues[$strKey]	= $strMixOriginalValue;
			}
		}

		$boolDeleteStatus = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolDeleteStatus ) {

			if( false == $boolSkipLog ) {

				$objUtilitySettingLog = new CUtilitySettingLog();
				$objUtilitySettingLog->setTableName( 'utility_bill_template_charges' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}

			return $boolDeleteStatus;
		}

		return true;
	}
    public function valCommodityId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

}
?>