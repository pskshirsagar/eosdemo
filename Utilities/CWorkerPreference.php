<?php

class CWorkerPreference extends CBaseWorkerPreference {

	const FORCE_CHANGE_PASSWORD				= 'FORCE_CHANGE_PASSWORD';
	const SHOW_PROFILE_CREATION_PROGRESS	= 'SHOW_PROFILE_CREATION_PROGRESS';

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valWorkerId() {
		return true;
	}

	public function valKey() {
		return true;
	}

	public function valValue() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>