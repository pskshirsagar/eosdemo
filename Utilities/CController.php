<?php

class CController extends CBaseController {

	const COMPANY_PROFILE	= 2;
	const ORDERS			= 3;
	const INVOICES			= 4;
	const CUSTOMERS			= 5;
	const COMPLIANCE		= 6;
	const CATALOGS          = 32;

	public function valId() {
		return true;
	}

	public function valParentControllerId() {
		return true;
	}

	public function valName() {
		return true;
	}

	public function valTitle() {
		return true;
	}

	public function valUrl() {
		return true;
	}

	public function valDescription() {
		return true;
	}

	public function valIsPublic() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valOrderNum() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function controllerIdToName( $intControllerId ) {

		$strControllerName = '';

		switch( $intControllerId ) {
			case self::ORDERS:
				$strControllerName = 'orders';
				break;

			case self::INVOICES:
				$strControllerName = 'invoices';
				break;

			case self::CUSTOMERS:
				$strControllerName = 'customers';
				break;

			case self::COMPLIANCE:
				$strControllerName = 'compliance';
				break;

			case self::COMPANY_PROFILE:
				$strControllerName = 'company_profile';
				break;

			default:
				// default case
				break;
		}

		return $strControllerName;
	}

}
?>