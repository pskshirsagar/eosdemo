<?php

class CMissingBillEscalation extends CBaseMissingBillEscalation {

	public function valCompanyUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyUserId() ) && false == $this->getIsGeneric() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', 'User is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDaysLateTrigger() {
		$boolIsValid = true;

		if( true == is_null( $this->getDaysLateTrigger() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_late_trigger', 'Trigger notification after bill days are required.' ) );
		}

		if( false == is_null( $this->getDaysLateTrigger() ) && ( 1 > $this->getDaysLateTrigger() || 28 < $this->getDaysLateTrigger() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_last_trigger', 'Trigger notification after bill days are in between 1 to 28.' ) );
		}

		return $boolIsValid;
	}

	public function valIsGenericTypes() {
		$boolIsValid = true;
		if( true == $this->getIsGeneric() && ( false == $this->getIsGenericCompany() && false == $this->getIsGenericRegion() && false == $this->getIsGenericProperty() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_generic_types', 'Atleast one send notification to is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMissingBillTypes() {
		$boolIsValid = true;

		if( false == $this->getIncludeVcrBills() && false == $this->getIncludeUtilityBills() && false == $this->getIncludeBillPayBills() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_generic_types', 'Atleast one type of bills is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDaysLateTrigger();
				$boolIsValid &= $this->valIsGenericTypes();
				$boolIsValid &= $this->valCompanyUserId();
				$boolIsValid &= $this->valMissingBillTypes();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $boolIsValid;
				break;

			default:
				trigger_error( 'Application error: Failed to load.', E_UER_ERROR );
		}

		return $boolIsValid;
	}

	public function sendMissingBillEscalationsEmailAlert( $arrstrEmailAddress, $objCurrentEmployee, $objEmailDatabase ) {

		$strHtmlEmailOutput = $this->buildHtmlMissingBillEscalationsEmailContent( $objCurrentEmployee );

		/**
		 * INSERT SYSTEM EMAILS
		 */

		$arrstrEmailAddress	= array_unique( $arrstrEmailAddress );

		if( false == valArr( $arrstrEmailAddress ) || 0 == valArr( $arrstrEmailAddress ) ) {
			return false;
		}

		foreach( $arrstrEmailAddress as $strEmailAddress ) {

			if( true == is_null( $strEmailAddress ) || 0 == strlen( trim( $strEmailAddress ) ) ) {
				continue;
			}

			$objSystemEmail = new CSystemEmail();

			// set data

			if( true == valObj( $objCurrentEmployee, 'CEmployees' ) && false == is_null( $objCurrentEmployee->getEmailAddress() ) ) {
				$objSystemEmail->setFromEmailAddress( trim( $objCurrentEmployee->getEmailAddress() ) );
			} else {
				$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
			}

			$strSubject = ' Missing Bill Esacalations for # ' . $this->getId();

			$objSystemEmail->setSubject( $strSubject );
			$objSystemEmail->setToEmailAddress( $strEmailAddress );
			$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
			$objSystemEmail->setCid( $this->getCid() );
			$objSystemEmail->setPropertyId( $this->getPropertyId() );
			$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::COMPANY_EMPLOYEE );

			if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
				trigger_error( 'System emailed failed to insert.', E_USER_ERROR );
				exit;
			}
		}
		return true;
	}

	public function buildHtmlMissingBillEscalationsEmailContent( $objCurrentEmployee ) {
		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		$objSmarty->assign_by_ref( 'missing_bill_escalation',  $this );
		$objSmarty->assign_by_ref( 'employee',                 $objCurrentEmployee );
		$objSmarty->assign( 'base_uri',		                   CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'image_url',	                   CONFIG_COMMON_PATH );

		$strHtmlTaskContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'ps_lead/utilities/company_utility_billing/company_missing_bill_escalations/missing_bill_escalations_email_format.tpl', PATH_INTERFACES_ADMIN . 'common/layouts/utility_email.tpl' );

		return $strHtmlTaskContent;
	}

}
?>