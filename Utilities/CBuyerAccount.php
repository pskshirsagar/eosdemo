<?php

class CBuyerAccount extends CBaseBuyerAccount {

	protected $m_intCid;
	protected $m_intLocationId;
	protected $m_intVendorCatalogId;
	protected $m_intBuyerSubAccountCount;
	protected $m_intDefaultVendorEntityId;

	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strLocationName;
	protected $m_strOrderHeaderNumber;
	protected $m_strOrderHeaderCreatedOn;
	protected $m_strBuyerSubAccountNumber;
	protected $m_strDefaultPropertyLocationName;

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valStoreId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getStoreId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'store_id', 'Location is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBuyerId( $objDatabase ) {
		$boolIsValid = true;
		if( false == is_numeric( $this->getBuyerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'buyer_id', 'Customer is required.' ) );
		} elseif( true == is_numeric( $this->getStoreId() ) ) {
			if( false == valObj( $objDatabase, 'CDatabase' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'buyer_id', 'Invalid database.' ) );
			} else {
				$intBuyerId = \Psi\Eos\Utilities\CBuyers::createService()->fetchActiveBuyerIdByIdByVendorIdByStoreId( $this->getBuyerId(), $this->getVendorId(), $this->getStoreId(), $objDatabase );
				if( false == valId( $intBuyerId ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'buyer_id', 'Location is not synced with customer.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valBuyerLocationId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getBuyerLocationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'buyer_location_id', 'Default property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valApPayeeAccountId() {
		return true;
	}

	public function valUtilityBillAccountId() {
		return true;
	}

	public function valReplacementBuyerAccountId() {
		return true;
	}

	public function valAuditFrequencyId() {
		return true;
	}

	public function valAccountNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', 'Account number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAccountDescription() {
		return true;
	}

	public function valAuditDay() {
		return true;
	}

	public function valLastInvoiceDate() {
		return true;
	}

	public function valAuditStartDate() {
		return true;
	}

	public function valAuditInvoice() {
		return true;
	}

	public function valIsDisabled() {
		return true;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAccountNumber();
				$boolIsValid &= $this->valStoreId();
				$boolIsValid &= $this->valBuyerId( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['location_name'] ) && $boolDirectSet ) {
			$this->m_strLocationName = trim( stripcslashes( $arrmixValues['location_name'] ) );
		} elseif( isset( $arrmixValues['location_name'] ) ) {
			$this->setLocationName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['location_name'] ) : $arrmixValues['location_name'] );
		}

		if( isset( $arrmixValues['name'] ) && $boolDirectSet ) {
			$this->m_strName = trim( stripcslashes( $arrmixValues['name'] ) );
		} elseif( isset( $arrmixValues['name'] ) ) {
			$this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['name'] ) : $arrmixValues['name'] );
		}

		if( isset( $arrmixValues['street_line1'] ) && $boolDirectSet ) {
			$this->m_strStreetLine1 = trim( stripcslashes( $arrmixValues['street_line1'] ) );
		} elseif( isset( $arrmixValues['street_line1'] ) ) {
			$this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['street_line1'] ) : $arrmixValues['street_line1'] );
		}

		if( isset( $arrmixValues['street_line2'] ) && $boolDirectSet ) {
			$this->m_strStreetLine2 = trim( stripcslashes( $arrmixValues['street_line2'] ) );
		} elseif( isset( $arrmixValues['street_line2'] ) ) {
			$this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['street_line2'] ) : $arrmixValues['street_line2'] );
		}

		if( isset( $arrmixValues['city'] ) && $boolDirectSet ) {
			$this->m_strCity = trim( stripcslashes( $arrmixValues['city'] ) );
		} elseif( isset( $arrmixValues['city'] ) ) {
			$this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['city'] ) : $arrmixValues['city'] );
		}

		if( isset( $arrmixValues['state_code'] ) && $boolDirectSet ) {
			$this->m_strStateCode = trim( stripcslashes( $arrmixValues['state_code'] ) );
		} elseif( isset( $arrmixValues['state_code'] ) ) {
			$this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['state_code'] ) : $arrmixValues['state_code'] );
		}

		if( isset( $arrmixValues['postal_code'] ) && $boolDirectSet ) {
			$this->m_strPostalCode = trim( stripcslashes( $arrmixValues['postal_code'] ) );
		} elseif( isset( $arrmixValues['postal_code'] ) ) {
			$this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['postal_code'] ) : $arrmixValues['postal_code'] );
		}

		if( isset( $arrmixValues['buyer_sub_account_count'] ) && $boolDirectSet ) {
			$this->m_intBuyerSubAccountCount = trim( stripcslashes( $arrmixValues['buyer_sub_account_count'] ) );
		} elseif( isset( $arrmixValues['buyer_sub_account_count'] ) ) {
			$this->setBuyerSubAccountCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['buyer_sub_account_count'] ) : $arrmixValues['buyer_sub_account_count'] );
		}

		if( isset( $arrmixValues['buyer_sub_account_number'] ) && $boolDirectSet ) {
			$this->m_strBuyerSubAccountNumber = trim( stripcslashes( $arrmixValues['buyer_sub_account_number'] ) );
		} elseif( isset( $arrmixValues['buyer_sub_account_number'] ) ) {
			$this->setBuyerSubAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['buyer_sub_account_number'] ) : $arrmixValues['buyer_sub_account_number'] );
		}

		if( isset( $arrmixValues['location_id'] ) && $boolDirectSet ) {
			$this->m_intLocationId = trim( stripcslashes( $arrmixValues['location_id'] ) );
		} elseif( isset( $arrmixValues['location_id'] ) ) {
			$this->setLocationId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['location_id'] ) : $arrmixValues['location_id'] );
		}

		if( isset( $arrmixValues['order_header_number'] ) && $boolDirectSet ) {
			$this->m_strOrderHeaderNumber = trim( stripcslashes( $arrmixValues['order_header_number'] ) );
		} elseif( isset( $arrmixValues['order_header_number'] ) ) {
			$this->setOrderHeaderNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['order_header_number'] ) : $arrmixValues['order_header_number'] );
		}

		if( isset( $arrmixValues['order_header_created_on'] ) && $boolDirectSet ) {
			$this->m_strOrderHeaderCreatedOn = trim( stripcslashes( $arrmixValues['order_header_created_on'] ) );
		} elseif( isset( $arrmixValues['order_header_created_on'] ) ) {
			$this->setOrderHeaderCreatedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['order_header_created_on'] ) : $arrmixValues['order_header_created_on'] );
		}

		if( true == isset( $arrmixValues['default_vendor_entity_id'] ) ) {
			$this->setDefaultVendorEntityId( $arrmixValues['default_vendor_entity_id'] );
		}

		if( true == isset( $arrmixValues['default_property_location_name'] ) ) {
			$this->setDefaultPropertyLocationName( $arrmixValues['default_property_location_name'] );
		}

		if( isset( $arrmixValues['cid'] ) && $boolDirectSet ) {
			$this->m_intCid = trim( stripcslashes( $arrmixValues['cid'] ) );
		} elseif( isset( $arrmixValues['cid'] ) ) {
			$this->setCid( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cid'] ) : $arrmixValues['cid'] );
		}

		if( true == isset( $arrmixValues['vendor_catalog_id'] ) ) {
			$this->setVendorCatalogId( $arrmixValues['vendor_catalog_id'] );
		}
	}

	/**
	 * Set Functions
	 */

	public function setCid( $intCid ) {
		$this->m_intCid = ( int ) $intCid;
	}

	public function setLocationId( $intLocationId ) {
		$this->m_intLocationId = $intLocationId;
	}

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = CStrings::strTrimDef( $strLocationName, 100, NULL, true );
	}

	public function setName( $strName ) {
		$this->m_strName = CStrings::strTrimDef( $strName, 200, NULL, true );
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = CStrings::strTrimDef( $strStreetLine1, 200, NULL, true );
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = CStrings::strTrimDef( $strStreetLine2, 200, NULL, true );
	}

	public function setCity( $strCity ) {
		$this->m_strCity = CStrings::strTrimDef( $strCity, 50, NULL, true );
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = CStrings::strTrimDef( $strStateCode, 2, NULL, true );
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = CStrings::strTrimDef( $strPostalCode, 20, NULL, true );
	}

	public function setBuyerSubAccountCount( $intBuyerSubAccountCount ) {
		$this->m_intBuyerSubAccountCount = CStrings::strToIntDef( $intBuyerSubAccountCount );
	}

	public function setBuyerSubAccountNumber( $strBuyerSubAccountNumber ) {
		$this->m_strBuyerSubAccountNumber = CStrings::strTrimDef( $strBuyerSubAccountNumber, 50, NULL, true );
	}

	public function setDefaultVendorEntityId( $intDefaultVendorEntityId ) {
		$this->m_intDefaultVendorEntityId = $intDefaultVendorEntityId;
	}

	public function setOrderHeaderNumber( $strOrderHeaderNumber ) {
		$this->m_strOrderHeaderNumber = CStrings::strTrimDef( $strOrderHeaderNumber, NULL, NULL, true );
	}

	public function setOrderHeaderCreatedOn( $strOrderHeaderCreatedOn ) {
		$this->m_strOrderHeaderCreatedOn = CStrings::strTrimDef( $strOrderHeaderCreatedOn, NULL, NULL, true );
	}

	public function setDefaultPropertyLocationName( $strDefaultPropertyLocationName ) {
		$this->m_strDefaultPropertyLocationName = CStrings::strTrimDef( $strDefaultPropertyLocationName, NULL, NULL, true );
	}

	public function setVendorCatalogId( $intVendorCatalogId ) {
		$this->m_intVendorCatalogId = ( int ) $intVendorCatalogId;
	}

	/**
	 * Get Functions
	 */

	public function getCid() {
		return $this->m_intCid;
	}

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getBuyerSubAccountCount() {
		return $this->m_intBuyerSubAccountCount;
	}

	public function getBuyerSubAccountNumber() {
		return $this->m_strBuyerSubAccountNumber;
	}

	public function getOrderHeaderNumber() {
		return $this->m_strOrderHeaderNumber;
	}

	public function getOrderHeaderCreatedOn() {
		return $this->m_strOrderHeaderCreatedOn;
	}

	public function getDefaultPropertyLocationName() {
		return $this->m_strDefaultPropertyLocationName;
	}

	public function getVendorCatalogId() {
		return $this->m_intVendorCatalogId;
	}

}
?>