<?php

class CUtilityBillOcrTemplateLog extends CBaseUtilityBillOcrTemplateLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOcrTemplateLabelId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemLabel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParsedLabel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConfidencePercentage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>