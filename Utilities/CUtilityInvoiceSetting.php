<?php

class CUtilityInvoiceSetting extends CBaseUtilityInvoiceSetting {

	const TX_INVOICE_MIN_DUE_DAYS 		= 16;
	const CA_NC_INVOICE_MIN_DUE_DAYS 	= 25;

	const VALIDATE_UPDATE_INVOICE_TAB = 'validate_update_invoice_tab';

	public function valNonResidentPortalUrl() {

		$boolIsValid = true;

		if( false == is_null( $this->getNonResidentPortalUrl() ) && false == filter_var( $this->getNonResidentPortalUrl(), FILTER_VALIDATE_URL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'non_resident_portal_url', 'Non resident portal url is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valInvoiceDueDay() {

		$boolIsValid = true;

		if( true == is_null( $this->getInvoiceDueDay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_due_day', 'Invoice Due Day is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNonResidentPortalUrl();
				$boolIsValid &= $this->valInvoiceDueDay();
				break;

			case self::VALIDATE_UPDATE_INVOICE_TAB:
				$boolIsValid &= $this->valInvoiceDueDay();
				$boolIsValid &= $this->valNonResidentPortalUrl();
				break;

			case VALIDATE_INSERT:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		// utility Invoice setting
		$arrstrTempUtilityInvoiceSettingOriginalValues = $arrstrUtilityInvoiceSettingOriginalValueChanges = $arrstrUtilityInvoiceSettingOriginalValues = [];
		$boolUtilityInvoiceSettingChangeFlag = false;

		$arrmixUtilityInvoiceSettingOriginalValues = fetchData( 'SELECT * FROM utility_invoice_settings WHERE property_utility_setting_id = ' . ( int ) $this->getPropertyUtilitySettingId(), $objDatabase );

		$arrstrTempUtilityInvoiceSettingOriginalValues = $arrmixUtilityInvoiceSettingOriginalValues[0];

		foreach( $arrstrTempUtilityInvoiceSettingOriginalValues as $strKey => $mixUtilityInvoiceSettingOriginalValue ) {

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strFunctionName    = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			if( ( false == CStrings::strToBool( $mixUtilityInvoiceSettingOriginalValue ) && '' == $this->$strFunctionName() ) || ( true == CStrings::strToBool( $mixUtilityInvoiceSettingOriginalValue ) && '1' == $this->$strFunctionName() ) ) {
				continue;
			}

			if( CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $mixUtilityInvoiceSettingOriginalValue ) {
				$arrstrUtilityInvoiceSettingOriginalValueChanges[$strKey] = $this->$strFunctionName();
				$arrstrUtilityInvoiceSettingOriginalValues[$strKey]       = $mixUtilityInvoiceSettingOriginalValue;
				$boolUtilityInvoiceSettingChangeFlag                      = true;
			}
		}

		$boolUtilityInvoiceSettingUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUtilityInvoiceSettingUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolUtilityInvoiceSettingChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempUtilityInvoiceSettingOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempUtilityInvoiceSettingOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'utility_invoice_settings' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrUtilityInvoiceSettingOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrUtilityInvoiceSettingOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		if( true == $boolUtilityInvoiceSettingUpdateStatus ) {
			return true;
		} else {
			return false;
		}

	}

}
?>