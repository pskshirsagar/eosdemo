<?php

class CBillAuditStep extends CBaseBillAuditStep {

	const CHECK_DATA																 = 1;
	const CALL_THE_PROVIDER_TO_FIND_OUT_WHY_THE_CHARGE_WAS_MISSING					 = 3;
	const CALL_THE_PROVIDER_TO_FIND_OUT_WHY_THE_BILLING_DATE_WAS_BEFORE_THE_END_DATE = 5;
	const CALL_PROVIDER_TO_VALIDATE_THAT_BOTH_ARE_ACTUAL_BILLS						 = 7;
	const CALL_PROVIDER_TO_FIND_OUT_WHY_THE_SERVICE_PERIOD_LENGTH_IS_DIFFERENT		 = 8;
	const CALL_THE_PROVIDER_TO_FIND_OUT_WHY_THAT_CHARGE_WAS_ADDED_TO_THE_BILL		 = 9;
	const CALL_PROVIDER_TO_GATHER_WHEN_THE_PAYMENT_WAS_MADE							 = 11;
	const CALL_PROVIDER_TO_FIND_OUT_WHY_THERE_IS_A_BALANCE_FORWARD					 = 13;
	const CALL_PROVIDER_TO_FIND_OUT_WHY_THERE_IS_A_CREDIT							 = 14;
	const CALL_PROVIDER_TO_SEE_WHY_THE_DUE_DATE_CHANGED								 = 15;
	const CALL_PROVIDER_TO_FIND_OUT_WHY_THERE_IS_AN_OVERLAP_IN_SERVICE_PERIOD		 = 17;
	const CALL_PROVIDER_TO_FIND_OUT_WHY_THERE_IS_AN_GAP_IN_SERVICE_PERIOD			 = 18;
	const CALL_PROVIDER_TO_FIND_OUT_WHY_THE_READ_IS_ESTIMATED						 = 19;
	const CALL_PROVIDER_TO_FIND_OUT_WHY_THE_CONSUMPTION_IS_ZERO						 = 21;
	const CHECK_PROVIDER_FOR_POSTED_RATE_SCHEDULE									 = 25;
	const CALL_PROVIDER_TO_VERIFY_RATE												 = 26;
	const CALL_PROVIDER_TO_VERIFY_READ												 = 29;
	const CALL_THE_PROVIDER_TO_FIND_OUT_IF_DEMAND_WAS_READ_CORRECTLY				 = 31;
	const RECEIVED_CORRECTED_BILL													 = 33;
	const VERIFIED 																	 = 35;
	const RESOLVED_BY_AUDIT															 = 36;
	const REMOVE_BILL																 = 37;
	const AUTO_RESOLVED																 = 41;

	const CHECK_DATA_NEW															 = 43;
	const CONTACT_PROVIDER															 = 44;
	const RECURRING_CHARGE															 = 45;
	const NON_RECURRING_CHARGE														 = 46;
	const NOT_CURRENT_CHARGE														 = 47;
	const CONTACT_CLIENT															 = 48;
	const BALANCE_FORWARD_IS_CREDITED_DUE_TO_OVER_PAYMENT							 = 49;
	const BALANCE_FORWARD_IS_CURRENT_CHARGE											 = 50;
	const BALANCE_FORWARD_IS_FROM_MISSING_INVOICE									 = 51;
	const BALANCE_FORWARD_IS_OR_INCLUDES_LATE_FEE									 = 52;
	const REQUEST_TO_HAVE_LATE_FEE_WAIVED											 = 54;
	const CONFIRMED_LATE_FEE_WAIVED													 = 55;
	const PROVIDER_EXTENDED_THE_DUE_DATE											 = 56;
	const MISSREAD_CONFIRMED_PROVIDER_WILL_REISSUE									 = 58;
	const DUPLICATE_INVOICE_FOUND_IN_SYSTEM											 = 63;
	const PROVIDER_ERROR															 = 64;
	const PROVIDER_ERROR_PROVIDER_WILL_REISSUE										 = 66;
	const METER_ERROR_PROVIDER_WILL_REISSUE											 = 68;
	const CREDIT_IS_RESULT_OF_PROVIDER_ERROR										 = 72;
	const PREVIOUS_BILL_MATCH_EXACTLY_WITH_CURRENT_BILL								 = 73;
	const VERIFIED_NEW													 		 	 = 75;
	const PROVIDER_MAILING_ISSUE													 = 80;
	const AUTO_RESOLVED_NEW															 = 86;
	const VERIFIED_BILL_WILL_EXPORT													 = 102;
	const VERIFIED_BILL_WILL_NOT_EXPORT												 = 103;

    public static function getBillAuditStepsForContactProvider() {

		return [
			self::CALL_PROVIDER_TO_FIND_OUT_WHY_THE_CONSUMPTION_IS_ZERO,
			self::CALL_PROVIDER_TO_FIND_OUT_WHY_THE_READ_IS_ESTIMATED,
			self::CALL_PROVIDER_TO_FIND_OUT_WHY_THE_SERVICE_PERIOD_LENGTH_IS_DIFFERENT,
			self::CALL_PROVIDER_TO_FIND_OUT_WHY_THERE_IS_A_CREDIT,
			self::CALL_PROVIDER_TO_FIND_OUT_WHY_THERE_IS_A_BALANCE_FORWARD,
			self::CALL_PROVIDER_TO_FIND_OUT_WHY_THERE_IS_AN_GAP_IN_SERVICE_PERIOD,
			self::CALL_PROVIDER_TO_FIND_OUT_WHY_THERE_IS_AN_OVERLAP_IN_SERVICE_PERIOD,
			self::CALL_PROVIDER_TO_GATHER_WHEN_THE_PAYMENT_WAS_MADE,
			self::CALL_PROVIDER_TO_SEE_WHY_THE_DUE_DATE_CHANGED,
			self::CALL_PROVIDER_TO_VALIDATE_THAT_BOTH_ARE_ACTUAL_BILLS,
			self::CALL_PROVIDER_TO_VERIFY_RATE,
			self::CALL_PROVIDER_TO_VERIFY_READ,
			self::CALL_THE_PROVIDER_TO_FIND_OUT_IF_DEMAND_WAS_READ_CORRECTLY,
			self::CALL_THE_PROVIDER_TO_FIND_OUT_WHY_THAT_CHARGE_WAS_ADDED_TO_THE_BILL,
			self::CALL_THE_PROVIDER_TO_FIND_OUT_WHY_THE_BILLING_DATE_WAS_BEFORE_THE_END_DATE,
			self::CALL_THE_PROVIDER_TO_FIND_OUT_WHY_THE_CHARGE_WAS_MISSING,
			self::CHECK_PROVIDER_FOR_POSTED_RATE_SCHEDULE
		];

	}

	public static function getBillAuditStepsToRemoveBill() {

		return [
			self::MISSREAD_CONFIRMED_PROVIDER_WILL_REISSUE,
			self::DUPLICATE_INVOICE_FOUND_IN_SYSTEM,
			self::PROVIDER_ERROR,
			self::PROVIDER_ERROR_PROVIDER_WILL_REISSUE,
			self::METER_ERROR_PROVIDER_WILL_REISSUE,
			self::PREVIOUS_BILL_MATCH_EXACTLY_WITH_CURRENT_BILL
		];

	}

}
?>