<?php

class CUtilityPropertyTaxId extends CBaseUtilityPropertyTaxId {

	public function validate( $strAction, $objClientDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTaxNumber( $objClientDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valTaxNumber( $objDatabase ) {

		$boolIsValid = true;

		$objPropertyAddress = $this->fetchPropertyAddress( $objDatabase );

		if( false == valStr( $this->getTaxId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_id', __( 'Tax ID is required.' ) ) );
		}

		if( true == $this->getIsFromEntrata() || ( false == $this->getIsFromEntrata() && false == $this->getIsFromMerchantAccount() ) ) {
			if( true == valStr( $this->getTaxId() ) && false == preg_match( '/^([\d]{2}-[\d]{7})$|([\d]{3}-[\d]{2}-[\d]{4})$/', $this->getTaxId() ) && CCountry::CODE_USA == $objPropertyAddress->getCountryCode() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_id', __( 'Tax ID Number should be in XX-XXXXXXX or XXX-XX-XXXX and should be numeric.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function setTaxId( $strPlainTaxNumber ) {
		if( true == valStr( $strPlainTaxNumber ) ) {

			if( true == $this->getIsFromMerchantAccount() ) {
				$strTaxNumber = CStrings::strTrimDef( $strPlainTaxNumber, 20, NULL, true );
				$this->setTaxIdEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strTaxNumber, CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER ) );
			} else {
				$this->setTaxIdEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
			}
		}
	}

	public function getTaxId() {
		if( false == valStr( $this->getTaxIdEncrypted() ) ) {
			return NULL;
		}

		if( true == valStr( $this->getIsFromMerchantAccount() ) ) {
			return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getTaxIdEncrypted(), CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CHECK_TAX_NUMBER, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
		}

		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getTaxIdEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function setMerchantTaxNumber( $strPlainTaxNumber ) {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $strPlainTaxNumber ) || true == stristr( $strPlainTaxNumber, 'X' ) ) return;

		$strTaxNumber = CStrings::strTrimDef( $strPlainTaxNumber, 20, NULL, true );
		$this->setTaxIdEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strTaxNumber, CONFIG_SODIUM_KEY_CHECK_TAX_NUMBER ) );
	}

	public function fetchPropertyAddress( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getPropertyId(), CAddressType::PRIMARY, $this->getCid(), $objDatabase );
	}

}
?>