<?php

class CPropertyCommodity extends CBasePropertyCommodity {

	const KEY_HAS_UEM_SERVICE					= 'has_uem_service';

	protected $m_boolDontRequireBills;
	protected $m_boolIsManual;
	protected $m_strCommodityName;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function updateCommodity( $strKey, $boolValue, $intUserId, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CCommodities::createService()->updatePropertyCommodityKeyByIdByCidByPropertyId( $strKey, $boolValue, $intUserId, $this->getId(), $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function getDontRequireBills() {
		return $this->m_boolDontRequireBills;
	}

	public function getIsManual() {
		return $this->m_boolIsManual;
	}

	public function setDontRequireBills( $boolDontRequireBills ) {
		$this->m_boolDontRequireBills = $boolDontRequireBills;
	}

	public function setIsManual( $boolIsManual ) {
		$this->m_boolIsManual = $boolIsManual;
	}

	public function createDummyUtilityBill( $intUtilityBatchId, $strStartDate = null, $strEndDate = null ) {

		$arrmixDummyUtilityBills = [];

		$objUtilityBill = new CUtilityBill();
		$objUtilityBill->setCid( $this->getCid() );
		$objUtilityBill->setPropertyId( $this->getPropertyId() );
		$objUtilityBill->setBillDatetime( 'NOW()' );
		$objUtilityBill->setUtilityBillTypeId( CUtilityBillType::DUMMY_BILL );
		$objUtilityBill->setUtilityBillReceiptTypeId( NULL );
		$objUtilityBill->setProcessedOn( 'NOW()' );
		$objUtilityBill->setProcessedAgainOn( 'NOW()' );

		$arrmixDummyUtilityBills['utility_bill'] = $objUtilityBill;

		$objUtilityBillAllocation = new CUtilityBillAllocation();
		$objUtilityBillAllocation->setCid( $this->getCid() );
		$objUtilityBillAllocation->setPropertyId( $this->getPropertyId() );
		$objUtilityBillAllocation->setUtilityBatchId( $intUtilityBatchId );
		$objUtilityBillAllocation->setStartDate( $strStartDate );
		$objUtilityBillAllocation->setEndDate( $strEndDate );
		$objUtilityBillAllocation->setBillableAmount( 0 );
		$objUtilityBillAllocation->setTotalAmount( 0 );

		$arrmixDummyUtilityBills['utility_bill_allocation'] = $objUtilityBillAllocation;

		$objUtilityBillCharge = new CUtilityBillCharge();
		$objUtilityBillCharge->setCid( $this->getCid() );
		$objUtilityBillCharge->setPropertyId( $this->getPropertyId() );
		$objUtilityBillCharge->setPropertyUtilityTypeId( $this->getPropertyUtilityTypeId() );
		$objUtilityBillCharge->setCommodityId( $this->getCommodityId() );
		$objUtilityBillCharge->setName( 'Dummy bill charge' );
		$objUtilityBillCharge->setAmount( 0 );

		$arrmixDummyUtilityBills['utility_bill_charge'] = $objUtilityBillCharge;

		$objUtilityBillChargeAllocation = new CUtilityBillChargeAllocation();
		$objUtilityBillChargeAllocation->setCid( $this->getCid() );
		$objUtilityBillChargeAllocation->setPropertyId( $this->getPropertyId() );
		$objUtilityBillChargeAllocation->setBillableAmount( 0 );
		$objUtilityBillChargeAllocation->setTotalAmount( 0 );

		$arrmixDummyUtilityBills['utility_bill_charge_allocation'] = $objUtilityBillChargeAllocation;

		return $arrmixDummyUtilityBills;

	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_manual'] ) ) {
			$this->setIsManual( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_manual'] ) : $arrmixValues['is_manual'] );
		}

		if( true == isset( $arrmixValues['dont_require_bills'] ) ) {
			$this->setDontRequireBills( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['dont_require_bills'] ) : $arrmixValues['dont_require_bills'] );
		}

	}

}
?>