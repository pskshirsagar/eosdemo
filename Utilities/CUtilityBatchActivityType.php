<?php

class CUtilityBatchActivityType extends CBaseUtilityBatchActivityType {

	const EMAIL		= 1;
	const PHONE		= 2;
	const NOTE		= 3;
	const FORWARD	= 4;
	const ROLLBACK	= 5;

	public function getNameByUtilityBatchActivityTypeId( $intUtilityBatchActivityTypeId ) {

		$strUtilityBatchActivityType = '';

		switch( $intUtilityBatchActivityTypeId ) {

			case CUtilityBatchActivityType::EMAIL:
				$strUtilityBatchActivityType = 'Email';
				break;

			case CUtilityBatchActivityType::PHONE:
				$strUtilityBatchActivityType = 'Phone';
				break;

			case CUtilityBatchActivityType::NOTE:
				$strUtilityBatchActivityType = 'Note';
				break;

			case CUtilityBatchActivityType::FORWARD:
				$strUtilityBatchActivityType = 'Forward';
				break;

			case CUtilityBatchActivityType::ROLLBACK:
				$strUtilityBatchActivityType = 'Rollback';
				break;

			default:
				// default case
				break;
		}

		return $strUtilityBatchActivityType;
	}

}
?>