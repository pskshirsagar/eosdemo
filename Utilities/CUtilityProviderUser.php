<?php

class CUtilityProviderUser extends CBaseUtilityProviderUser {

    public function valUserId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUserId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  'Please select the employee from available list.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valUserId();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>