<?php

class CDataEntryLog extends CBaseDataEntryLog {

	const SKIPPED_PROCESSING_LOG    = 'SKIPPED_PROCESSING_LOG';
	const SKIPPED_ASSOCIATE_LOG     = 'SKIPPED_ASSOCIATE_LOG';
	const SKIPPED_ASSOCIATE_REVIEW_LOG = 'SKIPPED_ASSOCIATE_REVIEW_LOG';
	const ASSOCIATION_ERROR_LOG     = 'ASSOCIATION_ERROR_LOG';

	const SKIPPED_BILL_REVIEW_LOG = 'SKIPPED_BILL_REVIEW_LOG';
	const BILL_REVIEW_ERROR_LOG = 'BILL_REVIEW_ERROR_LOG';

	const SKIPPED_DATA_ENTRY_AUDIT_LOG = 'SKIPPED_DATA_ENTRY_AUDIT_LOG';

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
