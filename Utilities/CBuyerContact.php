<?php

class CBuyerContact extends CBaseBuyerContact {

	const MAX_PHONE_NUMBER_SIZE = 14;

	protected $m_boolIsPrimary;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['is_primary'] ) && $boolDirectSet ) {
			$this->m_boolIsPrimary = trim( stripcslashes( $arrmixValues['is_primary'] ) );
		} elseif( isset( $arrmixValues['is_primary'] ) ) {
			$this->setIsPrimary( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_primary'] ) : $arrmixValues['is_primary'] );
		}
	}

	public function getIsPrimary() {
		return $this->m_boolIsPrimary;
	}

	public function setIsPrimary( $boolIsPrimary ) {
		$this->m_boolIsPrimary = CStrings::strToBool( $boolIsPrimary );
	}

	public function valId() {
		$boolIsValid = true;

		if( false == valId( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'buyer_contact_id', ' Unable to load contact.' ) );
		}

		return $boolIsValid;
	}

	public function valVendorId() {
		$boolIsValid = true;

		if( false == valId( $this->getVendorId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor', ' Unable to load vendor.' ) );
		}

		return $boolIsValid;
	}

	public function valBuyerId() {
		$boolIsValid = true;

		if( false == valId( $this->getBuyerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'buyer', ' Unable to load company.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {
		return true;
	}

	public function valEmailAddress( $objDatabase = NULL ) {

		if( true == is_null( $this->m_strEmailAddress ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is required. ' ) );
		} elseif( true == isset( $this->m_strEmailAddress ) && $this->m_strEmailAddress != filter_var( $this->m_strEmailAddress, FILTER_VALIDATE_EMAIL ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address does not appear to be valid.' ) );
		} elseif( 0 < strlen( $this->m_strEmailAddress ) && true == valObj( $objDatabase, 'CDatabase' ) && 0 < \Psi\Eos\Utilities\CBuyerContacts::createService()->fetchBuyerContactCount( ' WHERE lower( email_address ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getEmailAddress() ) ) . '\' AND id != ' . ( int ) $this->getId(), $objDatabase ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is already being used.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {

		if( true == is_null( $this->getPhoneNumber() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', ' Phone number is required. ' ) );
		} elseif( ( self::MAX_PHONE_NUMBER_SIZE < strlen( $this->getPhoneNumber() ) ) || false == CValidation::validateFullPhoneNumber( $this->getPhoneNumber(), false ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', ' Phone number is not valid.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valIsBuyerPrimary() {
		return true;
	}

	public function valIsBuyerContact() {
		return true;
	}

	public function valIsPrimary() {
		$boolIsValid = true;

		if( true == $this->getIsPrimary() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_buyer_contact', ' You can not disable primary contact.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmailAddress( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_disable':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valBuyerId();
				$boolIsValid &= $this->valVendorId();
				$boolIsValid &= $this->valIsPrimary();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>