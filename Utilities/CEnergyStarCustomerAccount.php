<?php

class CEnergyStarCustomerAccount extends CBaseEnergyStarCustomerAccount {

	public function valUsername() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strUsername ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Valid username is required.' ) );
		}

		$strUsernamePattern = '/^[A-Za-z]{1}[A-Za-z0-9._]{1,}$/';

		if( true == $boolIsValid && 1 != preg_match( $strUsernamePattern, $this->m_strUsername ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username should contain only alphabets and digits.' ) );
		}

		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;

		$boolIsValid = true;

		if( false == isset( $this->m_strPassword ) || 8 > strlen( $this->m_strPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password must be at least 8 characters.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strNameFirst ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strNameLast ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is required. ' ) );
		}

		if( false == is_null( $this->m_strEmailAddress ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email does not appear to be valid.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;

		if( false == is_null( $this->getPhoneNumber() ) && false == CValidation::checkPhoneNumberFullValidation( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Valid phone is required. ' ) );
		}

		return $boolIsValid;
	}

	public function encryptPassword() {
		if( true == valStr( trim( $this->getPassword() ) ) ) {
			return $this->setPassword( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( trim( $this->getPassword() ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function decryptPassword( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $strPassword, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
		}
		return NULL;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>