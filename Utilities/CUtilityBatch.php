<?php
use Psi\Eos\Admin\CContractProperties;
class CUtilityBatch extends CBaseUtilityBatch {

	const US_POSTAGE_STAMP_COST 				= .49;
	const UTILITY_BILLING_FEE_COST_AMOUNT  		= 1.50;
	const UTILITY_MOVE_IN_FEE_COST_AMOUNT 		= 1.50;
	const UTILITY_VCR_RECOVERY_FEE_COST_AMOUNT 	= 5.00;

	const UTILITY_PRE_BATCHES				= 1;
	const IN_PROGRESS_UTILITY_BATCHES		= 2;
	const IN_IMPLEMENTATION_UTILITY_BATCHES = 3;
	const COMPLETED_UTILITY_BATCHES			= 4;
	const VCR_ONLY							= 5;

	const PENDING_MANAGER_REGENERATE_LEASE_CHARGE_HRS = 24;

	protected $m_boolIsPreBillRejected;
	protected $m_boolIsLeaseChargesProperty;
	protected $m_boolIsManualChargesProperty;

	protected $m_strRequestUrl;
	protected $m_strCompletedOn;
	protected $m_strLastBatchDate;
	protected $m_strSecureBaseName;
	protected $m_strUtilityBatchStepName;
	protected $m_strScheduledChargesDueDate;
	protected $m_strPrebillRejectionReason;

	protected $m_intCompanyUserId;
	protected $m_intBatchTotalAmount;
	protected $m_intBatchInvoiceCount;
	protected $m_intUtilityDistributionStepId;

	protected $m_fltBillingFee;
	protected $m_fltUtilityCharges;
	protected $m_fltUtilityExpense;

	protected $m_arrintPropertyBuildingOccupiedUnitCount;

	protected $m_objUtilitiesDatabase;

	protected $m_arrstrBillingAllocationDetails;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['last_batch_date'] ) ) {
			$this->setLastBatchDate( $arrmixValues['last_batch_date'] );
		}
		if( true == isset( $arrmixValues['utility_batch_step_name'] ) ) {
			$this->setUtilityBatchStepName( $arrmixValues['utility_batch_step_name'] );
		}
		if( true == isset( $arrmixValues['batch_invoice_count'] ) ) {
			$this->setBatchInvoiceCount( $arrmixValues['batch_invoice_count'] );
		}
		if( true == isset( $arrmixValues['batch_total_amount'] ) ) {
			$this->setBatchTotalAmount( $arrmixValues['batch_total_amount'] );
		}
		if( true == isset( $arrmixValues['utility_expense'] ) ) {
			$this->setUtilityExpense( $arrmixValues['utility_expense'] );
		}
		if( true == isset( $arrmixValues['utility_charges'] ) ) {
			$this->setUtilityCharges( $arrmixValues['utility_charges'] );
		}
		if( true == isset( $arrmixValues['billing_fee'] ) ) {
			$this->setBillingFee( $arrmixValues['billing_fee'] );
		}
		if( true == isset( $arrmixValues['utility_distribution_step_id'] ) ) {
			$this->setUtilityDistributionStepId( $arrmixValues['utility_distribution_step_id'] );
		}
		if( true == isset( $arrmixValues['completed_on'] ) ) {
			$this->setCompletedOn( $arrmixValues['completed_on'] );
		}
		if( true == isset( $arrmixValues['company_user_id'] ) ) {
			$this->setUtilityDistributionStepId( $arrmixValues['company_user_id'] );
		}
		if( true == isset( $arrmixValues['scheduled_charges_due_date'] ) ) {
			$this->setScheduledChargesDueDate( $arrmixValues['scheduled_charges_due_date'] );
		}

	}

	public function getUtilityExpense() {
		return $this->m_fltUtilityExpense;
	}

	public function getUtilityCharges() {
		return $this->m_fltUtilityCharges;
	}

	public function getBillingFee() {
		return $this->m_fltBillingFee;
	}

	public function getLastBatchDate() {
		return $this->m_strLastBatchDate;
	}

	public function getUtilityBatchStepName() {
		return $this->m_strUtilityBatchStepName;
	}

	public function getBatchInvoiceCount() {
		return $this->m_intBatchInvoiceCount;
	}

	public function getBatchTotalAmount() {
		return $this->m_intBatchTotalAmount;
	}

	public function getPropertyBuildingOccupiedUnitCount() {
		return $this->m_arrintPropertyBuildingOccupiedUnitCount;
	}

	public function getUtilityDistributionStepId() {
		return $this->m_intUtilityDistributionStepId;
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getScheduledChargesDueDate() {
		return $this->m_strScheduledChargesDueDate;
	}

	public function getBillingAllocationDetails() {
		return $this->m_arrstrBillingAllocationDetails;
	}

	public function setUtilityExpense( $fltUtilityExpense ) {
		return $this->m_fltUtilityExpense = $fltUtilityExpense;
	}

	public function setUtilityCharges( $fltUtilityCharges ) {
		return $this->m_fltUtilityCharges = $fltUtilityCharges;
	}

	public function setPropertyBuildingOccupiedUnitCount( $arrintPropertyBuildingOccupiedUnitCount ) {
		$this->m_arrintPropertyBuildingOccupiedUnitCount = $arrintPropertyBuildingOccupiedUnitCount;
	}

	public function setBillingFee( $fltBillingFee ) {
		return $this->m_fltBillingFee = $fltBillingFee;
	}

	public function setLastBatchDate( $strLastBatchDate ) {
		$this->m_strLastBatchDate = $strLastBatchDate;
	}

	public function setUtilityBatchStepName( $strUtilityBatchStepName ) {
		$this->m_strUtilityBatchStepName = $strUtilityBatchStepName;
	}

	public function setBatchInvoiceCount( $intBatchInvoiceCount ) {
		$this->m_intBatchInvoiceCount = $intBatchInvoiceCount;
	}

	public function setBatchTotalAmount( $intBatchTotalAmount ) {
		$this->m_intBatchTotalAmount = $intBatchTotalAmount;
	}

	public function setUtilityDistributionStepId( $intUtilityDistributionStepId ) {
		$this->m_intUtilityDistributionStepId = $intUtilityDistributionStepId;
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = $intCompanyUserId;
	}

	public function setScheduledChargesDueDate( $strScheduledChargesDueDate ) {
		$this->set( 'm_strScheduledChargesDueDate', CStrings::strTrimDef( $strScheduledChargesDueDate, -1, NULL, true ) );
	}

	public function setIsPreBillRejected( $boolIsPreBillRejected ) {
		$this->m_boolIsPreBillRejected = $boolIsPreBillRejected;
	}

	public function setPrebillRejectionReason( $strPrebillRejectionReason ) {
		$this->m_strPrebillRejectionReason = $strPrebillRejectionReason;
	}

	public function setIsLeaseChargesProperty( $boolIsLeaseChargesProperty ) {
		$this->m_boolIsLeaseChargesProperty = $boolIsLeaseChargesProperty;
	}

	public function setIsManualChargesProperty( $boolIsManualChargesProperty ) {
		$this->m_boolIsManualChargesProperty = $boolIsManualChargesProperty;
	}

	public function setBillingAllocationDetails( $arrstrBillingAllocationDetails ) {
		$this->m_arrstrBillingAllocationDetails = $arrstrBillingAllocationDetails;
	}

	public function valDistributionDeadline() {
		$boolIsValid = true;

		if( true == is_null( $this->getDistributionDeadline() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'distribution_deadline', 'Please set Invoice Day in order to calculate Distribution deadline date.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityBatchStepId( $strAction, $intUtilityBatchStepId = NULL, $intPreviewInvoice = NULL ) {

		$boolIsValid = true;

		if( $strAction == VALIDATE_INSERT || $strAction == VALIDATE_UPDATE ) {

			if( false == is_numeric( $intUtilityBatchStepId ) ) {
				return true;
			}

			if( false == is_null( $intUtilityBatchStepId ) && $intUtilityBatchStepId != $this->getUtilityBatchStepId() && true == is_null( $intPreviewInvoice ) ) {
				$boolIsValid = false;
				if( true == is_null( $intPreviewInvoice ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_batch_step', 'The batch step has already been updated to another step. Please refresh and try again.' ) );
				}
			}
		}

		if( $strAction == VALIDATE_DELETE && $this->getUtilityBatchStepId() >= CUtilityBatchStep::BATCH_DISTRIBUTED ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_batch_step', 'You cannot delete this batch because it has been distributed.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intUtilityBatchStepId = NULL, $intPreviewInvoice = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUtilityBatchStepId( $strAction, $intUtilityBatchStepId, $intPreviewInvoice );
				$boolIsValid &= $this->valDistributionDeadline();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valUtilityBatchStepId( $strAction );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function getUtilityBatchStep() {
		return CUtilityBatchStep::getNameByUtilityBatchStepId( $this->getUtilityBatchStepId() );
	}

	public function getIsLeaseChargesProperty() {
		return $this->m_boolIsLeaseChargesProperty;
	}

	public function getIsManualChargesProperty() {
		return $this->m_boolIsManualChargesProperty;
	}

	public function fetchUtilityBillAllocations( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBillAllocations::createService()->fetchUtilityBillAllocationsByUtilityBatchId( $this->getId(), $objDatabase );
	}

	public function fetchVcrUtilityBills( $objDatabase ) {
		return \Psi\Eos\Utilities\CVcrUtilityBills::createService()->fetchVcrUtilityBillsByUtilityBatchId( $this->getId(), $objDatabase );
	}

	public function fetchUtilityTransactionsByUtilityInvoiceIds( $arrintUtilityInvoiceIds, $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUtilityTransactionsByUtilityBatchIdByUtilityInvoiceIds( $this->getId(), $arrintUtilityInvoiceIds, $objDatabase );
	}

	public function fetchUtilityTransactions( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUtilityTransactionsByUtilityBatchId( $this->getId(), $objDatabase );
	}

	public function fetchAllUtilityTransactions( $objDatabase, $boolIsManual ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchAllUtilityTransactionsByUtilityBatchId( $this, $objDatabase, $boolIsManual );
	}

	public function fetchUtilityTransactionsByUtilityTransactionTypeIds( $arrintUtilityTransactionTypeIds, $objDatabase, $boolIsManual = false ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUtilityTransactionsByUtilityBatchIdByUtilityTransactionTypeIds( $this, $arrintUtilityTransactionTypeIds, $objDatabase, $boolIsManual );
	}

	public function fetchAllUtilityTransactionsByUtilityTransactionTypeIds( $arrintUtilityTransactionTypeIds, $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchAllUtilityTransactionsByUtilityBatchIdByUtilityTransactionTypeIds( $this->getId(), $arrintUtilityTransactionTypeIds, $objDatabase );
	}

	public function fetchMeterTransmissionBatches( $objDatabase ) {
		return \Psi\Eos\Utilities\CMeterTransmissionBatches::createService()->fetchMeterTransmissionBatchesByUtilityBatch( $this, $objDatabase );
	}

	public function fetchUnverifiedUtilityInvoices( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchUnverifiedUtilityInvoicesByCidByPropertyIdByUtilityBatchId( $this->getCid(), $this->getPropertyId(), $this->getId(), $objDatabase );
	}

	public function fetchUnpostedUtilityTransactions( $objDatabase, $boolIsExcludeUtilityTax = false ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUnpostedUtilityTransactionsByUtilityBatchId( $this->getId(), $objDatabase, $boolIsExcludeUtilityTax );
	}

	public function fetchUnpostedUtilityTransactionsCount( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUnpostedUtilityTransactionsCountByUtilityBatchId( $this->getId(), $objDatabase );
	}

	public function fetchUtilityInvoices( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchUtilityInvoicesByUtilityBatchId( $this->getId(), $objDatabase );
	}

	public function fetchUnpostedUtilityInvoicesByUtilityBatchStepId( $intUtilityBatchStepId, $intIsConvergent, $objDatabase, $arrintExcludeUtilityTransactionTypeIds = NULL ) {
		return \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchUnpostedUtilityInvoicesByCidByPropertyIdByUtilityBatchIdByUtilityBatchStepId( $this->getCid(), $this->getPropertyId(), $this->getId(), $intUtilityBatchStepId, $intIsConvergent, $objDatabase, $arrintExcludeUtilityTransactionTypeIds );
	}

	// Post utility batch

	public function postBatch( $objPropertyUtilitySetting, $intUserId, $objUtilitiesDatabase, $objClientDatabase ) {

		$boolIsLateBilling = false;
		$strPostMonth = '';

		if( strtotime( date( 'm/1/Y' ) ) > strtotime( $objPropertyUtilitySetting->getBillingMonth() ) ) {
			$boolIsLateBilling = true;
		}

		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );

		$boolIsIntegratedProperty = $objProperty->isIntegrated( $objClientDatabase );

		// Validating post month of transactions.
		if( false == $boolIsIntegratedProperty ) {
			$objPropertyGlSetting	= CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $objPropertyUtilitySetting->getPropertyId(), $objPropertyUtilitySetting->getCid(), $objClientDatabase );
			$strPostMonth			= $this->getPostMonth( $objPropertyUtilitySetting->getIsNextMonthPostMonth() );

			$objPostMonthDate	= new DateTime( $strPostMonth );
			$objCurrentDate		= new DateTime( date( 'm/d/Y' ) );
			$intDayDifference	= $objCurrentDate->diff( $objPostMonthDate )->format( '%R%a' );

			// If calculated post month is greater than 31 days of current date then we are taking post month from property_gl_settings(old logic).
			if( 31 < $intDayDifference ) {
				$strPostMonth = '';
			} elseif( strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getArLockMonth() ) ) {
				// Show validation if post month is locked.
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month_error', 'Please change the setting so Post month would be next month' ) );
				return false;
			}
		}

		if( ( false == $boolIsIntegratedProperty && true == $boolIsLateBilling ) || true == $objPropertyUtilitySetting->getDontPostTempTransactions() ) {
			return $this->setArTransactionIdsOnUtilityTransactions( $intUserId, $objUtilitiesDatabase, $objClientDatabase );
		}

		// Post utility charges to entrata as temporary transactions
		if( true == $this->postArTransactions( $objPropertyUtilitySetting, $intUserId, $objUtilitiesDatabase, $objClientDatabase, $objProperty, $strPostMonth ) ) {
			return true;
		}

		return false;
	}

	public function setArTransactionIdsOnUtilityTransactions( $intUserId, $objUtilitiesDatabase, $objClientDatabase ) {

		$arrintUtilityTransactionTypeIds = [ CUtilityTransactionType::PRE_POSTED_CONVERGENT, CUtilityTransactionType::CONVERGENT, CUtilityTransactionType::TAX ];

		// fetch utility_transactions which are not already posted to RW database
		$arrobjUtilityTransactions = ( array ) $this->fetchUnpostedUtilityTransactions( $objUtilitiesDatabase );

		$objArTransaction = new CArTransaction();

		$objClientDatabase->begin();
		$objUtilitiesDatabase->begin();

		foreach( $arrobjUtilityTransactions as $objUtilityTransaction ) {

			if( true == in_array( $objUtilityTransaction->getUtilityTransactionTypeId(), $arrintUtilityTransactionTypeIds ) ) {
				continue;
			}

			$objArTransaction->fetchNextId( $objClientDatabase );
			$objUtilityTransaction->setArTransactionId( $objArTransaction->getId() );
			if( false == $objUtilityTransaction->update( $intUserId, $objUtilitiesDatabase ) ) {
				$this->addErrorMsgs( $objUtilityTransaction->getErrorMsgs() );
				$objClientDatabase->rollback();
				$objUtilitiesDatabase->rollback();
				return false;
			}
		}

		$objClientDatabase->commit();
		$objUtilitiesDatabase->commit();

		return true;
	}

	public function postTransactions( $objProperty, $objPropertyUtilitySetting, $arrobjAccounts, $intUserId, $objUtilitiesDatabase, $objAdminDatabase, $objUtilityDistributionLibrary  ) {

		$objClient	= CClients::fetchClientById( $objProperty->getCid(), $objAdminDatabase );
		$intPropertyUnitCount = 0;

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objClientDatabase = $objClient->loadDatabase( CDatabaseUserType::PS_PROPERTYMANAGER );
			$objClientDatabase->open();
			$intPropertyUnitCount = \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitCount( ' WHERE deleted_on IS NULL AND property_id=' . ( int ) $objProperty->getId() . ' AND cid = ' . ( int ) $objProperty->getCid(), $objClientDatabase );
			$objClientDatabase->close();
		}

		$objRuPricing = \Psi\Eos\Utilities\CRuPricings::createService()->fetchRuUemPricingsByPropertyId( $objProperty->getId(), $objUtilitiesDatabase );

		if( false == valObj( $objRuPricing, 'CRuPricing' ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Failed to load RU Pricing details' ) );

			return false;
		}

		$objAdminDatabase->begin();

		$arrobjPropertyUtilityTypes 	= ( array ) $objPropertyUtilitySetting->fetchPropertyUtilityTypes( $objUtilitiesDatabase );
		$arrobjPropertyUtilityTypeRates = \Psi\Eos\Utilities\CPropertyUtilityTypeRates::createService()->fetchAllPropertyUtilityTypeRatesByPropertyUtilityTypeIds( array_keys( $arrobjPropertyUtilityTypes ), $objUtilitiesDatabase, true );

		$objContractProperty = CContractProperties::createService()->fetchActiveContractPropertyByPropertyIdByPsProductId( $this->getPropertyId(), CPsProduct::RESIDENT_UTILITY, $objAdminDatabase );

		// Post the following to company ledger:
		// 1. Postage
		// 2. Mailed Invoice Fees
		// 3. Maintenance Fees
		// 4. Sales Tax
		// 5. Billing Fees
		// 6. Move-in Fees
		// 7. Termination Fees
		// 8. VCR Recovery
		// 9. Vacancy Analysis Fees
		// 10. Late Fees (property_utility_settings.ps_late_fee_percent)
		// 11. Service Credits for Emailed Bills

		// Find out how many move ins by property utility type rate - post one charge per property utility type rate
		// Find out how many move outs by property utility type rate - post one charge per property utility type rate
		// *Removed this logic and implement below one* Find out how many billing fees by property utility type rate - post one charge per property utility type rate **

		// Find out total invoice per lease bucker and post one Billing charge per property utility type rate

		$fltTaxableAmount = 0;
		$intTaxableTransactionCount = 0;

		$arrintUtilityTransactionTypeIds = [ CUtilityTransactionType::BILLING_FEE, CUtilityTransactionType::MOVE_IN_FEE, CUtilityTransactionType::TERMINATION_FEE ];

		// If transactions are merged then need to check for actual/estimated transactions count.
		if( true == $objPropertyUtilitySetting->getMergeBillingFee() ) {
			array_push( $arrintUtilityTransactionTypeIds, CUtilityTransactionType::ACTUAL_USAGE, CUtilityTransactionType::ESTIMATED_USAGE );
		}

		$strSql = 'SELECT
						property_utility_type_rate_id,
						utility_transaction_type_id,
						COUNT( id ) AS transaction_count,
						SUM( current_amount ) AS transaction_sum
					FROM
						utility_transactions
					WHERE
						utility_batch_id = ' . ( int ) $this->getId() . '
						AND cid = ' . ( int ) $this->getCid() . '
						AND utility_transaction_type_id IN ( ' . implode( ',', $arrintUtilityTransactionTypeIds ) . ' )
						AND original_utility_transaction_id IS NULL
						AND deleted_on IS NULL
					GROUP BY
						property_utility_type_rate_id,
						utility_transaction_type_id';

		$arrstrUtilityTransactions = fetchData( $strSql, $objUtilitiesDatabase );
		$arrstrBillingMoveInTerminationFeeUtilityTransactions = [];

		if( true == valArr( $arrstrUtilityTransactions ) ) {
			foreach( $arrstrUtilityTransactions as $arrstrUtilityTransaction ) {
				$arrstrBillingMoveInTerminationFeeUtilityTransactions[$arrstrUtilityTransaction['utility_transaction_type_id']][$arrstrUtilityTransaction['property_utility_type_rate_id']]['transaction_count'] = $arrstrUtilityTransaction['transaction_count'];
				$arrstrBillingMoveInTerminationFeeUtilityTransactions[$arrstrUtilityTransaction['utility_transaction_type_id']][$arrstrUtilityTransaction['property_utility_type_rate_id']]['transaction_sum'] = $arrstrUtilityTransaction['transaction_sum'];
			}
		}

		// Get transactions that aren't grouped by proeprty utility type rates
		$arrintFullScopeUtilityTransactionTypeIds = [ CUtilityTransactionType::VCR_LATE_FEE, CUtilityTransactionType::LATE_FEE ];

		$strSql = 'SELECT
						utility_transaction_type_id,
						COUNT( id ) AS transaction_count,
						SUM( current_amount ) AS transaction_sum
					FROM
						utility_transactions
					WHERE
						utility_batch_id = ' . ( int ) $this->getId() . '
						AND cid = ' . ( int ) $this->getCid() . '
						AND utility_transaction_type_id IN ( ' . implode( ',', $arrintFullScopeUtilityTransactionTypeIds ) . ' )
						AND original_utility_transaction_id IS NULL
						AND deleted_on IS NULL
					GROUP BY
						utility_transaction_type_id';

		$arrstrUtilityTransactions			= [];
		$arrstrLateFeeUtilityTransactions	= [];

		$arrstrUtilityTransactions = fetchData( $strSql, $objUtilitiesDatabase );

		if( true == isset( $arrstrUtilityTransactions ) && true == valArr( $arrstrUtilityTransactions ) ) {
			foreach( $arrstrUtilityTransactions as $arrstrUtilityTransaction ) {
				$arrstrLateFeeUtilityTransactions[$arrstrUtilityTransaction['utility_transaction_type_id']]['transaction_count'] = $arrstrUtilityTransaction['transaction_count'];
				$arrstrLateFeeUtilityTransactions[$arrstrUtilityTransaction['utility_transaction_type_id']]['transaction_sum'] = $arrstrUtilityTransaction['transaction_sum'];
			}
		}

		// Post VCR Penalty Fees
		$strSql = 'SELECT
						ut.property_utility_type_id,
						ut.property_vcr_type_rate_id,
						COUNT( ut.id ) AS transaction_count,
						SUM( ut.current_amount ) AS transaction_sum
					FROM
						utility_transactions ut
						LEFT JOIN property_vcr_type_rates pvtr ON ( pvtr.id = ut.property_vcr_type_rate_id )
					WHERE
						ut.utility_batch_id = ' . ( int ) $this->getId() . '
						AND ut.cid = ' . ( int ) $this->getCid() . '
						AND ut.utility_transaction_type_id = ' . CUtilityTransactionType::VCR_SERVICE_FEE . '
						AND ut.is_original IS TRUE
						AND ut.original_utility_transaction_id IS NOT NULL
						AND ut.deleted_on IS NULL
					GROUP BY
						ut.property_utility_type_id,
						ut.property_vcr_type_rate_id';

		$arrstrTempVCRPenaltiFeeUtilityTransactions = fetchData( $strSql, $objUtilitiesDatabase );

		$arrstrVCRPenaltiFeeUtilityTransactions = [];

		foreach( $arrstrTempVCRPenaltiFeeUtilityTransactions as $arrstrTempVCRPenaltiFeeUtilityTransaction ) {
			$arrstrVCRPenaltiFeeUtilityTransactions[$arrstrTempVCRPenaltiFeeUtilityTransaction['property_utility_type_id']][$arrstrTempVCRPenaltiFeeUtilityTransaction['property_vcr_type_rate_id']] = $arrstrTempVCRPenaltiFeeUtilityTransaction;
		}

		$objUtilityDistributionLibrary->setPropertyId( $objPropertyUtilitySetting->getPropertyId() );
		$intAccountId = $objUtilityDistributionLibrary->loadRecurringAccountId();

		$objBillingFeeAccount	= NULL;
		if( true == is_numeric( $intAccountId ) && true == array_key_exists( $intAccountId, $arrobjAccounts ) && true == valObj( $arrobjAccounts[$intAccountId], 'CAccount' ) ) {
			$objBillingFeeAccount = $arrobjAccounts[$intAccountId];
		}

		if( false == valObj( $objBillingFeeAccount, 'CAccount' ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'We cannot complete the distribution because a company account has not been added to the contract. Please contact the billing team.' ) );
			$objAdminDatabase->rollback();
			return false;
		}

		// Load the count of invoices that have to be mailed associated to this batch
		$arrstrMailedInvoiceCount = fetchData( 'SELECT count(id) FROM utility_invoices WHERE utility_batch_id = ' . ( int ) $this->getId() . ' AND mailed_on IS NOT NULL ', $objUtilitiesDatabase );

		$intMailedInvoiceCount	= ( true == isset( $arrstrMailedInvoiceCount[0]['count'] ) ) ? ( int ) $arrstrMailedInvoiceCount[0]['count'] : 0;
		$fltMailedInvoiceCharge	= $intMailedInvoiceCount * self::US_POSTAGE_STAMP_COST;

		// Post a transaction for postage
		if( 0 != $fltMailedInvoiceCharge && true == $objRuPricing->getChargeforPostage() ) {

			$objTransaction = $objBillingFeeAccount->createTransaction();
			$objTransaction->setReferenceNumber( $this->getId() );
			$objTransaction->setItemCount( $intMailedInvoiceCount );
			$objTransaction->setChargeCodeId( CChargeCode::UTILITY_POSTAGE_FEES );
			$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
			$objTransaction->setTransactionAmount( $fltMailedInvoiceCharge );
			$objTransaction->setMemo( ' Postage Fees for ' . $objProperty->getPropertyName() );
			$objTransaction->setPropertyId( $objProperty->getId() );

			if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
				$objTransaction->setContractPropertyId( $objContractProperty->getId() );
			}

			// Not taxable.
			$objTransaction->postCharge( $intUserId, $objAdminDatabase );
		}

		// Post Billing Fees * On the basis of invoice count, create the charges *
		// Get current batch invoices with utility accounts

		$arrobjAllCompanyUtilityTransactions = ( array ) \Psi\Eos\Utilities\CCompanyUtilityTransactions::createService()->fetchCompanyUtilityTransactionsByUtilityBatchId( $this->getId(), $objUtilitiesDatabase );

		$arrobjUemBillingCompanyUtilityTransactions = $arrobjBillingCompanyUtilityTransactions = $arrobjMoveInCompanyUtilityTransactions = $arrobjSubsidyCompanyUtilityTransactions = [];

		foreach( $arrobjAllCompanyUtilityTransactions as $objCompanyUtilityTransaction ) {
			if( CUtilityTransactionType::BILLING_FEE == $objCompanyUtilityTransaction->getUtilityTransactionTypeId() ) {
				if( true == is_null( $objCompanyUtilityTransaction->getPropertyUtilityTypeId() ) && true == is_null( $objCompanyUtilityTransaction->getPropertyUtilityTypeRateId() ) ) {
					$arrobjUemBillingCompanyUtilityTransactions[$objCompanyUtilityTransaction->getId()] = $objCompanyUtilityTransaction;
				} else {
					$arrobjBillingCompanyUtilityTransactions[$objCompanyUtilityTransaction->getId()] = $objCompanyUtilityTransaction;
				}
			} elseif( CUtilityTransactionType::MOVE_IN_FEE == $objCompanyUtilityTransaction->getUtilityTransactionTypeId() ) {
				$arrobjMoveInCompanyUtilityTransactions[$objCompanyUtilityTransaction->getId()] = $objCompanyUtilityTransaction;
			} elseif( CUtilityTransactionType::SUBSIDY_BILLING_FEE == $objCompanyUtilityTransaction->getUtilityTransactionTypeId() ) {
				$arrobjSubsidyCompanyUtilityTransactions[$objCompanyUtilityTransaction->getId()] = $objCompanyUtilityTransaction;
			}
		}

		if( true == valArr( $arrobjBillingCompanyUtilityTransactions ) ) {

			$strWhere = ' WHERE utility_batch_id = ' . $this->getId();

			$intBatchUtilityInvoiceCount = \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchUtilityInvoiceCount( $strWhere, $objUtilitiesDatabase );

			foreach( $arrobjBillingCompanyUtilityTransactions as $objCompanyUtilityTransaction ) {

				$objTransaction = $objBillingFeeAccount->createTransaction();
				$objTransaction->setReferenceNumber( $this->getId() );
				$objTransaction->setItemCount( $intBatchUtilityInvoiceCount );
				$objTransaction->setChargeCodeId( CChargeCode::UTILITY_BILLING_FEES );
				$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
				$objTransaction->setTransactionAmount( $objCompanyUtilityTransaction->getTotalCharge() );

				if( 0 < $objRuPricing->getConvergentBillingFeeAmount() ) {
					$objTransaction->setCostAmount( ( float ) ( $objCompanyUtilityTransaction->getTotalCharge() / $objRuPricing->getConvergentBillingFeeAmount() ) * self::UTILITY_BILLING_FEE_COST_AMOUNT );
				}

				$objTransaction->setMemo( 'Convergent Billing Fees for ' . $objProperty->getPropertyName() );
				$objTransaction->setPropertyId( $objProperty->getId() );

				if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
					$objTransaction->setContractPropertyId( $objContractProperty->getId() );
				}

				$objTransaction->postCharge( $intUserId, $objAdminDatabase );

				$fltTaxableAmount 			+= ( float ) $objTransaction->getTransactionAmount();
				$intTaxableTransactionCount += $objTransaction->getItemCount();

				$strSql = 'UPDATE
						 		utility_transactions SET transaction_id = ' . ( int ) $objTransaction->getId() . '
							WHERE
								utility_batch_id = ' . ( int ) $this->getId() . '
								AND cid = ' . ( int ) $this->getCid() . '
								AND property_utility_type_rate_id = ' . ( int ) $objCompanyUtilityTransaction->getPropertyUtilityTypeRateId() . '
								AND utility_transaction_type_id = ' . CUtilityTransactionType::BILLING_FEE . '
								AND deleted_on IS NULL';

				fetchData( $strSql, $objUtilitiesDatabase );

				$objCompanyUtilityTransaction->setTransactionId( $objTransaction->getId() );

				if( false == $objCompanyUtilityTransaction->update( $intUserId, $objUtilitiesDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Failed to update company utility transactions.' ) );
					$objAdminDatabase->rollback();
					return false;
				}
			}
		}

		// Post UEM Billing Fee if use_invoice_total is turned on.

		if( true == valArr( $arrobjUemBillingCompanyUtilityTransactions ) ) {
			$arrfltPropertiesTransactionalAmount = CContractProperties::createService()->fetchCustomContractPropertiesByContractStatusTypeIdsByPsProductIdsByPropertyIdsByCids( [ CContractStatusType::CONTRACT_APPROVED ], [ CPsProduct::UTILITY_EXPENSE_MANAGEMENT ], [ $this->getPropertyId() ], [ $this->getCid() ], $objAdminDatabase );
			if( true == valArr( $arrfltPropertiesTransactionalAmount ) && true == $objRuPricing->getIsPerRuInvoicePricing() && 0 < ( float ) $objRuPricing->getPerRuInvoiceAmount() ) {

				foreach( $arrobjUemBillingCompanyUtilityTransactions as $objCompanyUtilityTransaction ) {

					$intItemCount = ( int ) ( $objCompanyUtilityTransaction->getTotalCharge() / $objRuPricing->getPerRuInvoiceAmount() );
					$strMemo = 'Utility Expense Management Transactional - ' . $objProperty->getPropertyName() . ' - ' . $intItemCount . ' Invoices';

					$objTransaction = $objBillingFeeAccount->createTransaction();
					$objTransaction->setReferenceNumber( $this->getId() );
					$objTransaction->setItemCount( $intItemCount );
					$objTransaction->setChargeCodeId( CChargeCode::UTILITY_EXPENSE_MANAGEMENT_TRANSACTIONAL );
					$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
					$objTransaction->setTransactionAmount( $objCompanyUtilityTransaction->getTotalCharge() );
					$objTransaction->setMemo( $strMemo );
					$objTransaction->setPropertyId( $objProperty->getId() );

					if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
						$objTransaction->setContractPropertyId( $objContractProperty->getId() );
					}

					$objTransaction->postCharge( $intUserId, $objAdminDatabase );

					$objCompanyUtilityTransaction->setTransactionId( $objTransaction->getId() );

					if( false == $objCompanyUtilityTransaction->update( $intUserId, $objUtilitiesDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Failed to update company utility transactions.' ) );
						$objAdminDatabase->rollback();
						return false;
					}
				}
			}
		}

		// Post Move-in Fees
		if( true == valArr( $arrobjMoveInCompanyUtilityTransactions ) ) {

			foreach( $arrobjMoveInCompanyUtilityTransactions as $objCompanyUtilityTransaction ) {

				$objPropertyUtilityTypeRate = getArrayElementByKey( $objCompanyUtilityTransaction->getPropertyUtilityTypeRateId(), $arrobjPropertyUtilityTypeRates );

				if( false == valObj( $objPropertyUtilityTypeRate, 'CPropertyUtilityTypeRate' ) ) {
					$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Property utility type rate failed to load for property utility type rate id:' . $objCompanyUtilityTransaction->getPropertyUtilityTypeRateId() ) );
					$objAdminDatabase->rollback();
					return false;
				}

				$fltTransactionAmount 	= $objCompanyUtilityTransaction->getTotalCharge();
				$fltCostAmount			= ( float ) ( $objCompanyUtilityTransaction->getTotalCharge() / $objRuPricing->getMoveInFeeAmount() ) * self::UTILITY_MOVE_IN_FEE_COST_AMOUNT;

				$intItemCount = ( int ) ( $objCompanyUtilityTransaction->getTotalCharge() / $objRuPricing->getMoveInFeeAmount() );

				if( 0 != $fltTransactionAmount ) {

					$objTransaction = $objBillingFeeAccount->createTransaction();
					$objTransaction->setReferenceNumber( $this->getId() );
					$objTransaction->setItemCount( $intItemCount );
					$objTransaction->setChargeCodeId( CChargeCode::UTILITY_MOVE_IN_FEES );
					$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
					$objTransaction->setTransactionAmount( $fltTransactionAmount );
					$objTransaction->setCostAmount( $fltCostAmount );
					$objTransaction->setMemo( 'Move-in Fees for ' . $objProperty->getPropertyName() );
					$objTransaction->setPropertyId( $objProperty->getId() );

					if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
						$objTransaction->setContractPropertyId( $objContractProperty->getId() );
					}

					$objTransaction->postCharge( $intUserId, $objAdminDatabase );

					$fltTaxableAmount 			+= ( float ) $objTransaction->getTransactionAmount();
					$intTaxableTransactionCount += $objTransaction->getItemCount();

					$strSql = 'UPDATE
							 		utility_transactions SET transaction_id = ' . ( int ) $objTransaction->getId() . '
								WHERE
									utility_batch_id = ' . ( int ) $this->getId() . '
									AND cid = ' . ( int ) $this->getCid() . '
									AND property_utility_type_rate_id = ' . ( int ) $objPropertyUtilityTypeRate->getId() . '
									AND utility_transaction_type_id = ' . CUtilityTransactionType::MOVE_IN_FEE . '
									AND deleted_on IS NULL';

					fetchData( $strSql, $objUtilitiesDatabase );

					$objCompanyUtilityTransaction->setTransactionId( $objTransaction->getId() );

					if( false == $objCompanyUtilityTransaction->update( $intUserId, $objUtilitiesDatabase ) ) {
						$objAdminDatabase->rollback();
						$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Failed to update company utility transactions.' ) );
						return false;
					}
				}
			}
		}

		// Post ps subsidy billing fee
		if( true == valArr( $arrobjSubsidyCompanyUtilityTransactions ) ) {

			foreach( $arrobjSubsidyCompanyUtilityTransactions as $objSubsidyCompanyUtilityTransaction ) {
				$fltTransactionAmount = $objSubsidyCompanyUtilityTransaction->getTotalCharge();

				$intItemCount = ( int ) ( $objSubsidyCompanyUtilityTransaction->getTotalCharge() / $objRuPricing->getSubsidizedConvergentBillingFeeAmount() );

				if( 0 != $fltTransactionAmount ) {

					$objTransaction = $objBillingFeeAccount->createTransaction();
					$objTransaction->setReferenceNumber( $this->getId() );
					$objTransaction->setItemCount( $intItemCount );
					$objTransaction->setChargeCodeId( CChargeCode::UTILITY_SUBSIDY_BILLING_FEE );
					$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
					$objTransaction->setTransactionAmount( $fltTransactionAmount );
					$objTransaction->setMemo( 'Utility subsidy billing for ' . $objProperty->getPropertyName() );
					$objTransaction->setPropertyId( $objProperty->getId() );

					if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
						$objTransaction->setContractPropertyId( $objContractProperty->getId() );
					}

					$objTransaction->postCharge( $intUserId, $objAdminDatabase );

					$objSubsidyCompanyUtilityTransaction->setTransactionId( $objTransaction->getId() );

					if( false == $objSubsidyCompanyUtilityTransaction->update( $intUserId, $objUtilitiesDatabase ) ) {
						$objAdminDatabase->rollback();
						$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Failed to update company utility transactions.' ) );
						return false;
					}
				}
			}
		}

		// Post Termination Fees
		if( true == isset( $arrstrBillingMoveInTerminationFeeUtilityTransactions[CUtilityTransactionType::TERMINATION_FEE] ) && true == valArr( $arrstrBillingMoveInTerminationFeeUtilityTransactions[CUtilityTransactionType::TERMINATION_FEE] ) ) {
			foreach( $arrstrBillingMoveInTerminationFeeUtilityTransactions[CUtilityTransactionType::TERMINATION_FEE] as $intPropertyUtilityTypeRateId => $arrstrSummarizedUtilityTransaction ) {

				$objPropertyUtilityTypeRate = $arrobjPropertyUtilityTypeRates[$intPropertyUtilityTypeRateId];

				if( false == valObj( $objPropertyUtilityTypeRate, 'CPropertyUtilityTypeRate' ) ) {
					$objAdminDatabase->rollback();
					$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Property utility type rate failed to load for property utility type rate id:' . $intPropertyUtilityTypeRateId ) );
					return false;
				}

				$fltTransactionAmount = ( float ) $arrstrSummarizedUtilityTransaction['transaction_count'] * ( float ) $objRuPricing->getMoveOutFeeAmount();
				$intItemCount = ( int ) $arrstrSummarizedUtilityTransaction['transaction_count'];

				if( 0 != $fltTransactionAmount ) {
					$objTransaction = $objBillingFeeAccount->createTransaction();
					$objTransaction->setReferenceNumber( $this->getId() );
					$objTransaction->setItemCount( $intItemCount );
					$objTransaction->setChargeCodeId( CChargeCode::UTILITY_TERMINATION_FEES );
					$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
					$objTransaction->setTransactionAmount( $fltTransactionAmount );
					$objTransaction->setMemo( 'Utility Termination Fees for ' . $objProperty->getPropertyName() );
					$objTransaction->setPropertyId( $objProperty->getId() );

					if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
						$objTransaction->setContractPropertyId( $objContractProperty->getId() );
					}

					$objTransaction->postCharge( $intUserId, $objAdminDatabase );

					$fltTaxableAmount 			+= ( float ) $objTransaction->getTransactionAmount();
					$intTaxableTransactionCount += $objTransaction->getItemCount();

					$strSql = 'UPDATE
							 		utility_transactions SET transaction_id = ' . $objTransaction->getId() . '
								WHERE
									utility_batch_id = ' . ( int ) $this->getId() . '
									AND cid = ' . ( int ) $this->getCid() . '
									AND property_utility_type_rate_id = ' . ( int ) $objPropertyUtilityTypeRate->getId() . '
									AND utility_transaction_type_id = ' . CUtilityTransactionType::TERMINATION_FEE . '
									AND deleted_on IS NULL';

					fetchData( $strSql, $objUtilitiesDatabase );
				}
			}
		}

		if( true == isset( $arrstrLateFeeUtilityTransactions ) && true == valArr( $arrstrLateFeeUtilityTransactions ) ) {

			// Post Late Fees
			if( true == isset( $arrstrLateFeeUtilityTransactions[CUtilityTransactionType::LATE_FEE] ) ) {

				$fltTransactionAmount = ( float ) $arrstrLateFeeUtilityTransactions[CUtilityTransactionType::LATE_FEE]['transaction_sum'] * ( float ) $objPropertyUtilitySetting->getPsLateFeePercent();

				$intItemCount = ( int ) $arrstrLateFeeUtilityTransactions[CUtilityTransactionType::LATE_FEE]['transaction_sum'];

				if( 0 != $fltTransactionAmount ) {

					$objTransaction = $objBillingFeeAccount->createTransaction();
					$objTransaction->setReferenceNumber( $this->getId() );
					$objTransaction->setItemCount( $intItemCount );
					$objTransaction->setChargeCodeId( CChargeCode::UTILITY_LATE_FEES );
					$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
					$objTransaction->setTransactionAmount( $fltTransactionAmount );
					$objTransaction->setMemo( 'Utility Late Fees for ' . $objProperty->getPropertyName() );
					$objTransaction->setPropertyId( $objProperty->getId() );

					if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
						$objTransaction->setContractPropertyId( $objContractProperty->getId() );
					}

					$objTransaction->postCharge( $intUserId, $objAdminDatabase );

					$fltTaxableAmount 			+= ( float ) $objTransaction->getTransactionAmount();
					$intTaxableTransactionCount += $objTransaction->getItemCount();

					$strSql = 'UPDATE
							 		utility_transactions SET transaction_id = ' . ( int ) $objTransaction->getId() . '
								WHERE
									utility_batch_id = ' . ( int ) $this->getId() . '
									AND cid = ' . ( int ) $this->getCid() . '
									AND utility_transaction_type_id = ' . CUtilityTransactionType::LATE_FEE . '
									AND deleted_on IS NULL';

					fetchData( $strSql, $objUtilitiesDatabase );
				}
			}

			// Post VCR Late Fees
			if( true == isset( $arrstrLateFeeUtilityTransactions[CUtilityTransactionType::VCR_LATE_FEE] ) ) {

				$fltTransactionAmount = ( float ) $arrstrLateFeeUtilityTransactions[CUtilityTransactionType::VCR_LATE_FEE]['transaction_sum'] * ( float ) $objPropertyUtilitySetting->getPsLateFeePercent();

				$intItemCount = ( int ) $arrstrLateFeeUtilityTransactions[CUtilityTransactionType::VCR_LATE_FEE]['transaction_sum'];

				if( 0 != $fltTransactionAmount ) {

					$objTransaction = $objBillingFeeAccount->createTransaction();
					$objTransaction->setReferenceNumber( $this->getId() );
					$objTransaction->setItemCount( $intItemCount );
					$objTransaction->setChargeCodeId( CChargeCode::UTILITY_LATE_FEES );
					$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
					$objTransaction->setTransactionAmount( $fltTransactionAmount );
					$objTransaction->setMemo( 'VCR Late Fees for ' . $objProperty->getPropertyName() );
					$objTransaction->setPropertyId( $objProperty->getId() );

					if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
						$objTransaction->setContractPropertyId( $objContractProperty->getId() );
					}

					$objTransaction->postCharge( $intUserId, $objAdminDatabase );

					$fltTaxableAmount 			+= ( float ) $objTransaction->getTransactionAmount();
					$intTaxableTransactionCount += $objTransaction->getItemCount();

					$strSql = 'UPDATE
							 		utility_transactions SET transaction_id = ' . ( int ) $objTransaction->getId() . '
								WHERE
									utility_batch_id = ' . ( int ) $this->getId() . '
									AND cid = ' . ( int ) $this->getCid() . '
									AND utility_transaction_type_id = ' . CUtilityTransactionType::VCR_LATE_FEE . '
									AND deleted_on IS NULL';

					fetchData( $strSql, $objUtilitiesDatabase );
				}
			}
		}

		if( true == valArr( $arrstrVCRPenaltiFeeUtilityTransactions ) ) {

			foreach( $arrstrVCRPenaltiFeeUtilityTransactions as $intPropertyUtilityTypeId => $arrstrTempUtilityTransactions ) {

				if( true == array_key_exists( $intPropertyUtilityTypeId, $arrobjPropertyUtilityTypes ) ) {

					$objPropertyUtilityType = $arrobjPropertyUtilityTypes[$intPropertyUtilityTypeId];

					if( false == valObj( $objPropertyUtilityType, 'CPropertyUtilityType' ) ) {
						$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Property utility type failed to load.' ) );
						$objAdminDatabase->rollback();
						return false;
					}

					if( true != $objPropertyUtilityType->getHasVcrService() ) {
						continue;
					}

					$objPropertyVcrSetting = $objPropertyUtilityType->fetchPropertyVcrSetting( $objUtilitiesDatabase );

					if( false == valObj( $objPropertyVcrSetting, 'CPropertyVcrSetting' ) ) {
						$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Property vcr setting failed to load for property utility type id- ' . $objPropertyUtilityType->getId() ) );
						$objAdminDatabase->rollback();
						return false;
					}

					$fltTransactionAmount = $fltCostAmount = $intItemCount = 0;

					$arrobjPropertyVcrTypeRates = \Psi\Eos\Utilities\CPropertyVcrTypeRates::createService()->fetchPropertyVcrTypeRatesByIds( array_keys( $arrstrTempUtilityTransactions ), $objUtilitiesDatabase );

					// Looping transactions vcr_type_rate wise to get recovery fee.
					foreach( $arrstrTempUtilityTransactions as $arrstrUtilityTransaction ) {

						$objPropertyVcrTypeRate = getArrayElementByKey( $arrstrUtilityTransaction['property_vcr_type_rate_id'], $arrobjPropertyVcrTypeRates );

						if( false == valObj( $objPropertyVcrTypeRate, 'CPropertyVcrTypeRate' ) ) {
							$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Property vcr type rate failed to load for property utility type id- ' . $objPropertyUtilityType->getId() ) );
							$objAdminDatabase->rollback();
							return false;
						}

						$fltTransactionAmount 	+= ( int ) $arrstrUtilityTransaction['transaction_count'] * ( float ) $objRuPricing->getVcrServiceFeeAmount();
						$fltCostAmount			+= ( float ) $arrstrUtilityTransaction['transaction_count'] * self::UTILITY_VCR_RECOVERY_FEE_COST_AMOUNT;
						$intItemCount 			+= ( int ) $arrstrUtilityTransaction['transaction_count'];
					}

					if( 0 != $fltTransactionAmount ) {

						$objTransaction = $objBillingFeeAccount->createTransaction();
						$objTransaction->setReferenceNumber( $this->getId() );
						$objTransaction->setItemCount( $intItemCount );
						$objTransaction->setChargeCodeId( CChargeCode::UTILITY_VCR_RECOVERY_FEES );
						$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
						$objTransaction->setTransactionAmount( $fltTransactionAmount );
						$objTransaction->setCostAmount( $fltCostAmount );
						$objTransaction->setMemo( 'VCR Recovery Fees for ' . $objProperty->getPropertyName() );
						$objTransaction->setPropertyId( $objProperty->getId() );

						if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
							$objTransaction->setContractPropertyId( $objContractProperty->getId() );
						}

						$objTransaction->postCharge( $intUserId, $objAdminDatabase );

						$fltTaxableAmount 			+= ( float ) $objTransaction->getTransactionAmount();
						$intTaxableTransactionCount += $objTransaction->getItemCount();

						$strSql = 'UPDATE
								 		utility_transactions SET transaction_id = ' . ( int ) $objTransaction->getId() . '
									WHERE
										utility_batch_id = ' . ( int ) $this->getId() . '
										AND cid = ' . ( int ) $this->getCid() . '
										AND property_utility_type_id = ' . ( int ) $objPropertyUtilityType->getId() . '
										AND utility_transaction_type_id = ' . CUtilityTransactionType::VCR_SERVICE_FEE . '
										AND deleted_on IS NULL';

						fetchData( $strSql, $objUtilitiesDatabase );
					}
				}
			}
		}

		// Post VCR Per Unit Fees
		// We probably need to load property units by status type

		if( true == empty( $intPropertyUnitCount ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'Property unit count not configured in property utility setting.' ) );
			$objAdminDatabase->rollback();
			return false;
		}

		$boolHasVcrService = false;

		if( true == valArr( $arrobjPropertyUtilityTypes ) ) {
			foreach( $arrobjPropertyUtilityTypes as $objPropertyUtilityType ) {

				if( true != $objPropertyUtilityType->getHasVcrService() ) {
					continue;
				}

				$objPropertyVcrSetting = $objPropertyUtilityType->fetchPropertyVcrSetting( $objUtilitiesDatabase );

				$fltTransactionAmount 	= ( int ) $intPropertyUnitCount * ( float ) $objRuPricing->getVcmAmount();

				if( true == valObj( $objPropertyVcrSetting, 'CPropertyVcrSetting' ) && 0 != $fltTransactionAmount ) {
					$boolHasVcrService = true;
					break;
				}
			}
		}

		if( true == $boolHasVcrService ) {

			$fltCostAmount			= self::UTILITY_VCR_RECOVERY_FEE_COST_AMOUNT;

			$objTransaction = $objBillingFeeAccount->createTransaction();
			$objTransaction->setReferenceNumber( $this->getId() );
			$objTransaction->setItemCount( ( int ) $intPropertyUnitCount );
			$objTransaction->setChargeCodeId( CChargeCode::UTILITY_VCR_PER_UNIT_FEES );
			$objTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
			$objTransaction->setTransactionAmount( $fltTransactionAmount );
			$objTransaction->setCostAmount( $fltCostAmount );
			$objTransaction->setMemo( 'VCR Per Unit Fees for ' . $objProperty->getPropertyName() );
			$objTransaction->setPropertyId( $objProperty->getId() );

			if( true == valObj( $objContractProperty, 'CContractProperty' ) ) {
				$objTransaction->setContractPropertyId( $objContractProperty->getId() );
			}

			$objTransaction->postCharge( $intUserId, $objAdminDatabase );

		}

		$objAdminDatabase->commit();

		return true;
	}

	public function postArTransactions( $objPropertyUtilitySetting, $intUserId, $objUtilitiesDatabase, $objClientDatabase, $objProperty, $strPostMonth='' ) {

		$boolIsExcludeUtilityTax = false;

		if( false == $objProperty->isIntegrated( $objClientDatabase ) ) {
			$boolIsExcludeUtilityTax = true;
		}

		// fetch utility_transactions which are not already posted to RW database
		$arrobjUtilityTransactions = $this->fetchUnpostedUtilityTransactions( $objUtilitiesDatabase, $boolIsExcludeUtilityTax );

		if( false == valArr( $arrobjUtilityTransactions ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, '', 'There are no charges for posting.' ) );
			return false;
		}

		$arrobjExportedUtilityTransactions = [];

		$arrintUtilityTransactionIds = array_keys( $arrobjUtilityTransactions );
		$arrobjUtilityTransactions   = \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchGroupedUtilityTransactionsByIds( array_keys( $arrobjUtilityTransactions ), $objUtilitiesDatabase );

		$arrobjTempExportedUtilityTransactions = ( array ) \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchExportedUtilityTransactionsByExportGroupIdsByIds( array_filter( array_keys( rekeyObjects( 'UtilityExportGroupId', $arrobjUtilityTransactions ) ) ), $arrintUtilityTransactionIds, $objUtilitiesDatabase );

		$arrintArCodeIds  = ( true == valArr( $arrobjUtilityTransactions ) ) ? array_keys( rekeyObjects( 'ArCodeId', $arrobjUtilityTransactions ) ) : NULL;
		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setArCodeIds( $arrintArCodeIds );
		$arrobjArCodes = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodes( 'SELECT * FROM view_ar_codes WHERE cid = ' . $this->getCid() . ' AND property_id = ' . $this->getPropertyId(), $objClientDatabase );

		$arrintUtilityAccountIds	= array_keys( rekeyObjects( 'UtilityAccountId', $arrobjUtilityTransactions ) );
		$arrintScheduledChargeIds	= array_filter( array_keys( rekeyObjects( 'ScheduledChargeId', $arrobjUtilityTransactions ) ) );
		$arrobjUtilityAccounts		= $objPropertyUtilitySetting->fetchUtilityAccountsByIds( $arrintUtilityAccountIds, $objUtilitiesDatabase );

		$arrobjLeases = \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByCidByIds( $objProperty->getCid(), array_filter( array_keys( rekeyObjects( 'LeaseId', $arrobjUtilityAccounts ) ) ), $objClientDatabase );

		$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByIdsByCid( $arrintScheduledChargeIds, $objProperty->getCid(), $objClientDatabase );

		$arrintUtilityTypeIds = array_filter( array_keys( rekeyObjects( 'UtilityTypeId', $arrobjUtilityTransactions ) ) );
		array_push( $arrintUtilityTypeIds, CUtilityType::CUSTOM );

		$arrobjArTriggers = ( array ) \Psi\Eos\Entrata\CArTriggers::createService()->fetchArTriggersByUtilityTypeIds( $arrintUtilityTypeIds, $objClientDatabase );

		$arrobjArTriggers = rekeyObjects( 'UtilityTypeId', $arrobjArTriggers );

		foreach( $arrobjUtilityTransactions as $objUtilityTransaction ) {
			if( true == valArr( $arrobjUtilityAccounts ) && true == array_key_exists( $objUtilityTransaction->getUtilityAccountId(), $arrobjUtilityAccounts ) ) {

				$objUtilityAccount = getArrayElementByKey( $objUtilityTransaction->getUtilityAccountId(), $arrobjUtilityAccounts );

				$objLease = getArrayElementByKey( $objUtilityAccount->getLeaseId(), $arrobjLeases );

				if( false == valObj( $objLease, 'CLease' ) ) {
					continue;
				}

				if( false == is_null( $objUtilityTransaction->getScheduledChargeId() ) && false == array_key_exists( $objUtilityTransaction->getScheduledChargeId(), $arrobjScheduledCharges ) ) {
					continue;
				}

				$objScheduledCharge = getArrayElementByKey( $objUtilityTransaction->getScheduledChargeId(), $arrobjScheduledCharges );

				if( true == valObj( $objScheduledCharge, 'CScheduledCharge' ) && false == is_null( $objScheduledCharge->getLastPostedOn() ) && strtotime( date( 'm/d/Y' ) ) < strtotime( $objScheduledCharge->getLastPostedOn() ) ) {
					continue;
				}

				$objArTransaction = $arrobjUtilityAccounts[$objUtilityTransaction->getUtilityAccountId()]->createArTransaction( $objUtilityTransaction, $objClientDatabase );

				$objArTransaction->setLeaseIntervalId( $objLease->getActiveLeaseIntervalId() );

				if( true == $objProperty->isIntegrated( $objClientDatabase ) || true == valObj( $objScheduledCharge, 'CScheduledCharge' ) ) {
					$objArTransaction->setPostDate( date( 'm/d/Y h:i:s', mktime( 0, 0, 0, date( 'm' ) + 1, 1, date( 'Y' ) ) ) );
					$objArTransaction->setPostMonth( date( 'm/d/Y', strtotime( $objArTransaction->getPostDate() ) ) );
				} elseif( true == valStr( $strPostMonth ) ) {
					if( CUtilityTransactionType::UTILITY_TAX == $objUtilityTransaction->getUtilityTransactionTypeId() ) {
						continue;
					}
					$objArTransaction->setPostMonth( date( 'm/d/Y', strtotime( $strPostMonth ) ) );

					$objArTransaction->setPostDate( $this->getPostDate( $strPostMonth, $objUtilitiesDatabase, $objPropertyUtilitySetting ) );
				}

				$objArTransaction->setReportingPostDate( $objArTransaction->getPostDate() );

				if( true == valObj( $objArTransaction, 'CArTransaction' ) ) {
					$objArCode = getArrayElementByKey( $objArTransaction->getArCodeId(), $arrobjArCodes );

					if( true == valObj( $objArCode, 'CArCode' ) ) {
						$objArTransaction->setArCodeTypeId( $objArCode->getArCodeTypeId() );
						$objArTransaction->setArOriginId( $objArCode->getArOriginId() );
					}

					$intUtilityTypeId = $objUtilityTransaction->getUtilityTypeId();

					if( true == in_array( $intUtilityTypeId, [ CUtilityType::CUSTOM2, CUtilityType::CUSTOM3, CUtilityType::CUSTOM4, CUtilityType::CUSTOM5, CUtilityType::CUSTOM6 ] ) ) {
						$intUtilityTypeId = CUtilityType::CUSTOM;
					}

					$objArTrigger = getArrayElementByKey( $intUtilityTypeId, $arrobjArTriggers );

					if( CUtilityTransactionType::PRE_POSTED_CONVERGENT != $objUtilityTransaction->getUtilityTransactionTypeId() && true == valObj( $objArTrigger, 'CArTrigger' ) ) {
						$objArTransaction->setArTriggerId( $objArTrigger->getId() );
					}

					if( false == is_null( $objUtilityTransaction->getScheduledChargeId() ) && true == array_key_exists( $objUtilityTransaction->getScheduledChargeId(), $arrobjScheduledCharges ) ) {
						$objArTransaction->setArTriggerId( $objScheduledCharge->getArTriggerId() );
					}

					if( true == is_null( $objArTransaction->getArTriggerId() ) ) {
						continue;
					}

					$objArTransaction->setIsTemporary( true );
					$objUtilityTransaction->setArTransaction( $objArTransaction );
				}
			}
		}

		if( true == valArr( $arrobjTempExportedUtilityTransactions ) ) {
			foreach( $arrobjTempExportedUtilityTransactions as $objExportedUtilityTransaction ) {
				$arrobjExportedUtilityTransactions[$objExportedUtilityTransaction->getUtilityAccountId()][$objExportedUtilityTransaction->getId()] = $objExportedUtilityTransaction;
			}
		}

		switch( NULL ) {
            default:
				$boolIsValid = true;

	            $objClientDatabase->begin();
	            $objUtilitiesDatabase->begin();

            	foreach( $arrobjUtilityTransactions as $objUtilityTransaction ) {

            		$objArTransaction = $objUtilityTransaction->getArTransaction();

            		if( false == valObj( $objArTransaction, 'CArTransaction' ) ) {
            			continue;
		            }

            		if( 0 <= $objArTransaction->getTransactionAmount() ) {
						$boolIsValid &= $objArTransaction->validate( 'post_charge_ar_transaction', $objClientDatabase );
					} else {
						$boolIsValid &= $objArTransaction->validate( 'post_concession_ar_transaction', $objClientDatabase );
					}

            		if( false == $boolIsValid ) {
            			$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
            			$objClientDatabase->rollback();
            			$objUtilitiesDatabase->rollback();
						break 2;
					}

	            	if( false == $objArTransaction->postCharge( $this->m_intCompanyUserId, $objClientDatabase ) ) {
	            		$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
	            		$objClientDatabase->rollback();
	            		$objUtilitiesDatabase->rollback();
						break 2;
					}

					// Update utility charge with ar_transaction_id

					$strSql = ' UPDATE
									utility_transactions
								SET
									ar_transaction_id = ' . ( int ) $objArTransaction->getId() . ',
									updated_by = ' . ( int ) $intUserId . ',
									updated_on = \'NOW()\'
								WHERE
									id = ' . $objUtilityTransaction->getId();

					fetchData( $strSql, $objUtilitiesDatabase );

					if( false == is_null( $objUtilityTransaction->getUtilityExportGroupId() ) && true == valArr( $arrobjExportedUtilityTransactions ) ) {
						$arrobjUtilityAccountUtilityTransactions = ( array ) getArrayElementByKey( $objUtilityTransaction->getUtilityAccountId(), $arrobjExportedUtilityTransactions );

						if( true == valArr( $arrobjUtilityAccountUtilityTransactions ) ) {
							foreach( $arrobjUtilityAccountUtilityTransactions as $objTempUtilityTransaction ) {
								if( $objUtilityTransaction->getUtilityExportGroupId() == $objTempUtilityTransaction->getUtilityExportGroupId() && true == is_null( $objTempUtilityTransaction->getArTransactionId() ) ) {
									$strSql = ' UPDATE
													utility_Transactions
												SET
													ar_transaction_id = ' . ( int ) $objArTransaction->getId() . ',
													updated_by = ' . ( int ) $intUserId . ',
													updated_on = \'NOW()\'
												WHERE
													id = ' . $objTempUtilityTransaction->getId();

									fetchData( $strSql, $objUtilitiesDatabase );
								}
							}
						}
					}
            	}

            	$objClientDatabase->commit();
            	$objUtilitiesDatabase->commit();

				return true;
		}

		return false;
	}

	public function sendPreBillEmailAlert( $arrobjProperties, $arrstrEmailAddrresses, $strFromEmailAddress, $strReason, $strStatus, $strSuffix,  $objEmailDatabase,  $objDatabase, $strNote = NULL, $intPsLeadId, $objUtilitiesDatabase = NULL, $strFilePath = NULL, $strFileName = NULL ) {

		// Disable emails for Village at Merritt Park ( pid 253719 ).

		if( 253719 == $this->getPropertyId() && CClient::ID_HAMPSHIRE_ASSETS == $this->getCid() ) {
			return true;
		}

		$strPreBillDate = '';

		if( false == is_null( $strSuffix ) ) {
			$objClient = CClients::fetchClientById( $this->getCid(), $objDatabase );

			if( true == valObj( $objClient, 'CClient' ) ) {
					$this->m_strRequestUrl = 'http://' . $objClient->getRwxDomain() . $strSuffix . '/?module=utility_batches_dashboardxxx&utility_batch_id=' . ( int ) $this->getId() . '&is_from_email=1&ps_lead[id]=' . ( int ) $intPsLeadId;
			}
		} elseif( true == is_null( $strSuffix ) ) {
			$this->m_strRequestUrl = $this->m_strSecureBaseName . '/?module=ps_lead-new&ps_lead[id]=' . ( int ) $intPsLeadId . '&is_from_email=1';
		}

		if( 'is_rw_reject' == $strStatus || 'is_rw_approve' == $strStatus ) {
			$arrobjBatchActivities = ( array ) \Psi\Eos\Utilities\CUtilityBatchActivities::createService()->fetchPreBillDatesByUtilityBatchIds( [ $this->getId() ], $objUtilitiesDatabase );
			$objBatchActivity = current( $arrobjBatchActivities );
			$strPreBillDate = getArrayElementByKey( 'utility_batch_log_datetime', $objBatchActivity );
		}

		$strHtmlEmailOutput = $this->buildHtmlEmailContent( $arrobjProperties, $strReason, $strStatus, $strNote, $strPreBillDate );

		// insert system emails

		$arrstrEmailAddrresses = array_filter( $arrstrEmailAddrresses );
		if( false == valArr( $arrstrEmailAddrresses ) ) {
			return false;
		}

		foreach( $arrstrEmailAddrresses as $strEmailAddress ) {

			if( true == is_null( $strEmailAddress ) ) {
				continue;
			}

			$objSystemEmail = new CSystemEmail();

			$objSystemEmail->setFromEmailAddress( $strFromEmailAddress );

			if( 'is_ca_approve' == $strStatus ) {
				$objProperty = current( $arrobjProperties );
				$strSubject = ' Pre-bill for ' . $objProperty->getPropertyName() . ' is ready for review';
			} elseif( 'is_rw_reject' == $strStatus ) {
				$strSubject = ' Reject pre-bill batch ' . $this->getId();
			} elseif( 'is_rw_approve' == $strStatus ) {
				if( true == array_key_exists( $this->getPropertyId(), $arrobjProperties ) ) {
						$strPropertyName = $arrobjProperties[$this->getPropertyId()]->getPropertyName();
				}
				$strSubject = ' Pre-bill approved for ' . $strPropertyName;
			}

			if( false == is_null( $strFilePath ) && false == is_null( $strFileName ) ) {
				$objEmailAttachment = new CEmailAttachment();
				$objEmailAttachment->setFilePath( $strFilePath );
				$objEmailAttachment->setFileName( $strFileName );
				$objSystemEmail->addEmailAttachment( $objEmailAttachment );
			}

			$objSystemEmail->setSubject( $strSubject );
			$objSystemEmail->setToEmailAddress( $strEmailAddress );
			$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
			$objSystemEmail->setCid( $this->getCid() );
			$objSystemEmail->setPropertyId( $this->getPropertyId() );
			$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::COMPANY_EMPLOYEE );

			if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
				trigger_error( 'System emailed failed to insert.', E_USER_ERROR );
				exit;
			}
		}

		return true;
	}

	public function buildHtmlEmailContent( $arrobjProperties, $strReason, $strStatus, $strNote, $strPreBillDate = NULL ) {

		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );

		$objSmarty->assign_by_ref( 'utility_batch', $this );
		$objSmarty->assign_by_ref( 'properties', 	$arrobjProperties );
		$objSmarty->assign_by_ref( 'reason',      	$strReason );
		$objSmarty->assign_by_ref( 'status',   	    $strStatus );
		$objSmarty->assign_by_ref( 'note',   	    $strNote );
		$objSmarty->assign( 'pre_bill_date',        $strPreBillDate );
		$objSmarty->assign( 'task_request_url',  	$this->m_strRequestUrl );
		$objSmarty->assign( 'base_uri',		        CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX', 	CONFIG_ENTRATA_LOGO_PREFIX );
		$objSmarty->assign( 'image_url',	        CONFIG_COMMON_PATH );

		$strHtmlTaskContent = $objSmarty->nestedFetch( PATH_INTERFACES_RESIDENT_WORKS . 'private/utilities/utilities_dashboard_tab/utility_batches/utility_prebill_email_template.tpl', PATH_INTERFACES_ADMIN . 'common/layouts/utility_email.tpl' );

		return $strHtmlTaskContent;
	}

	public function fetchOriginalUtilityTransactionsWithoutMoveOut( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchOriginalUtilityTransactionsWithoutMoveOutByUtilityBatchId( $this->getId(), $objUtilitiesDatabase );
	}

	public function delete( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

		$arrintUtilityTransactionTypeIds = [ CUtilityTransactionType::VCR_USAGE_FEE, CUtilityTransactionType::VCR_SERVICE_FEE, CUtilityTransactionType::VCR_LATE_FEE ];

		$arrobjUtilityBillAllocations 		= $this->fetchUtilityBillAllocations( $objDatabase );
		$arrobjMeterTransmissionBatches 	= $this->fetchMeterTransmissionBatches( $objDatabase );
		$arrobjUtilityInvoices 				= $this->fetchUtilityInvoices( $objDatabase );
		$arrobjCompanyUtilityTransactions 	= ( array ) \Psi\Eos\Utilities\CCompanyUtilityTransactions::createService()->fetchCompanyUtilityTransactionsByUtilityBatchIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$arrobjVcrUtilityBills				= $this->fetchVcrUtilityBills( $objDatabase );
		$arrobjUtilityBatchActivity			= ( array ) \Psi\Eos\Utilities\CUtilityBatchActivities::createService()->fetchUtilityBatchActivitiesByUtilityBatchId( $this->getId(), $objDatabase );

		if( true == valArr( $arrobjUtilityInvoices ) ) {
			$arrintUtilityInvoiceIds = array_keys( $arrobjUtilityInvoices );
			$arrobjUtilityTransactions = $this->fetchUtilityTransactionsByUtilityInvoiceIds( $arrintUtilityInvoiceIds, $objDatabase );
		} else {
			$arrobjUtilityTransactions = $this->fetchUtilityTransactions( $objDatabase );
		}

		if( true == valArr( $arrobjUtilityTransactions ) ) {
			$arrintArTransactionIds = array_filter( array_keys( rekeyObjects( 'ArTransactionId', $arrobjUtilityTransactions ) ) );
			$arrobjArTransactions = \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByIdsByCid( $arrintArTransactionIds, $this->getCid(), $objClientDatabase );
		}

		$arrobjMoveOutUtilityTransactions = $this->fetchMoveOutUtilityTransactions( $objDatabase, false );

		if( true == valArr( $arrobjMoveOutUtilityTransactions ) ) {
			foreach( $arrobjMoveOutUtilityTransactions as $intUtilityTransactionId => $objUtilityTransaction ) {
				$arrobjUtilityTransactions[$intUtilityTransactionId] = $objUtilityTransaction;
			}
		}

		$boolIsValid = true;

		switch( NULL ) {
			default:

				$objDatabase->begin();
				$objClientDatabase->begin();

				if( true == valArr( $arrobjMeterTransmissionBatches ) ) {
					foreach( $arrobjMeterTransmissionBatches as $objMeterTransmissionBatches ) {
						if( false == $objMeterTransmissionBatches->delete( $intCurrentUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $objMeterTransmissionBatches->getErrorMsgs() );
							$objDatabase->rollback();
							$objClientDatabase->rollback();
							$boolIsValid &= false;
							break 2;
						}
					}
				}

				if( true == valArr( $arrobjUtilityTransactions ) ) {

					foreach( $arrobjUtilityTransactions as $objUtilityTransaction ) {
						if( true == is_null( $objUtilityTransaction->getOriginalUtilityTransactionId() ) ) {

							// We can only delete an ar transaction if the following conditions are met:
							// 1. The transaction is temporary
							// 2. The validate delete function returns true.
							// 3. Do not delete UTILITY_TRANSACTION_TYPE_CONVERGENT charges

							$objArTransaction = ( true == is_numeric( $objUtilityTransaction->getArTransactionId() ) && true == isset( $arrobjArTransactions[$objUtilityTransaction->getArTransactionId()] ) ) ? $arrobjArTransactions[$objUtilityTransaction->getArTransactionId()] : NULL;

							if( true == valObj( $objArTransaction, 'CArTransaction' )
									&& $objUtilityTransaction->getUtilityTransactionTypeId() != CUtilityTransactionType::CONVERGENT
									&& 1 == $objArTransaction->getIsTemporary()
									&& true == $objArTransaction->validate( VALIDATE_DELETE, $objClientDatabase ) ) {

								// Now we can attempt to delete this item from the database.
								if( false == $objArTransaction->delete( $intCurrentUserId, $objClientDatabase ) ) {
									$objClientDatabase->rollback();
									$objDatabase->rollback();
									$boolIsValid &= false;
									break 2;
								}
							}
						}

						if( $this->getId() == $objUtilityTransaction->getUtilityBatchId() && true == is_null( $objUtilityTransaction->getOriginalUtilityTransactionId() ) && ( true == in_array( $objUtilityTransaction->getUtilityTransactionTypeId(), $arrintUtilityTransactionTypeIds ) || 1 == $objUtilityTransaction->getIsMoveOut() ) ) {
							// update current batch original vcr transactions
							$objUtilityTransaction->setUtilityBatchId( NULL );

							if( false == $objUtilityTransaction->getIsMoveOut() ) {
								$objUtilityTransaction->setArTransactionId( NULL );
							}

							if( false == $objUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
								$this->addErrorMsgs( $objUtilityTransaction->getErrorMsgs() );
								$objClientDatabase->rollback();
								$objDatabase->rollback();
								$boolIsValid &= false;
								break 2;
							}
						} else {
							// delete cloned vcr transactions and cloned and original non vcr transactions
							if( false == $objUtilityTransaction->delete( $intCurrentUserId, $objDatabase ) ) {
								$this->addErrorMsgs( $objUtilityTransaction->getErrorMsgs() );
								$objClientDatabase->rollback();
								$objDatabase->rollback();
								$boolIsValid &= false;
								break 2;
							}
						}
					}
				}

				foreach( $arrobjCompanyUtilityTransactions as $objCompanyUtilityTransaction ) {

					if( false == $objCompanyUtilityTransaction->delete( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsgs( $objCompanyUtilityTransaction->getErrorMsgs() );
						$objDatabase->rollback();
						$objClientDatabase->rollback();
						$boolIsValid &= false;
						break 2;
					}
				}

				foreach( $arrobjUtilityBatchActivity as $objUtilityBatchActivity ) {

					if( false == $objUtilityBatchActivity->delete( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsgs( $objUtilityBatchActivity->getErrorMsgs() );
						$objDatabase->rollback();
						$objClientDatabase->rollback();
						$boolIsValid &= false;
						break 2;
					}
				}

				if( true == valArr( $arrobjUtilityInvoices ) ) {
					foreach( $arrobjUtilityInvoices as $objUtilityInvoice ) {
						if( false == $objUtilityInvoice->delete( $intCurrentUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $objUtilityInvoice->getErrorMsgs() );
							$objDatabase->rollback();
							$objClientDatabase->rollback();
							$boolIsValid &= false;
							break 2;
						}
					}
				}

				// code for removing dummy bills
				$strSql = 'SELECT
									ub.id
								FROM
									utility_bills ub
									JOIN utility_bill_allocations uba ON ( uba.utility_bill_id = ub.id )
								WHERE
									uba.utility_batch_id = ' . $this->getId() . '
									AND ub.utility_bill_type_id = ' . CUtilityBillType::DUMMY_BILL;

				$arrintUtilityBillIds = fetchData( $strSql, $objDatabase );

				if( true == valArr( $arrintUtilityBillIds ) ) {

					$arrintUtilityBillIds = array_keys( rekeyArray( 'id', $arrintUtilityBillIds ) );

					$strSql = 'DELETE
										FROM
											utility_bill_charge_allocations
										WHERE
											utility_bill_charge_id IN (
																		SELECT
																			id
																		FROM
																			utility_bill_charges
																		WHERE
																			utility_bill_id IN ( ' . implode( ', ', $arrintUtilityBillIds ) . ' )
											);';

					$strSql .= 'DELETE FROM utility_bill_charges WHERE utility_bill_id IN ( ' . implode( ', ', $arrintUtilityBillIds ) . ' );';

					$strSql .= 'DELETE FROM utility_bill_allocations WHERE utility_bill_id IN ( ' . implode( ', ', $arrintUtilityBillIds ) . ' );';

					$strSql .= 'DELETE FROM utility_bills WHERE id IN ( ' . implode( ', ', $arrintUtilityBillIds ) . ' );';

					if( false == fetchData( $strSql, $objDatabase ) ) {
						$objDatabase->rollback();
						break;
					}
				}

				if( true == valArr( $arrobjUtilityBillAllocations ) ) {
					foreach( $arrobjUtilityBillAllocations as $objUtilityBillAllocation ) {
						$objUtilityBillAllocation->setUtilityBatchId( NULL );
						if( false == $objUtilityBillAllocation->update( $intCurrentUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $objUtilityBillAllocation->getErrorMsgs() );
							$objDatabase->rollback();
							$objClientDatabase->rollback();
							$boolIsValid &= false;
							break 2;
						}
					}
				}

				if( true == valArr( $arrobjVcrUtilityBills ) ) {
					foreach( $arrobjVcrUtilityBills as $objVcrUtilityBill ) {

						$objVcrUtilityBill->setUtilityBatchId( NULL );

						if( false == $objVcrUtilityBill->update( $intCurrentUserId, $objDatabase ) ) {
							$objDatabase->rollback();
							$objClientDatabase->rollback();
							$boolIsValid &= false;
							break 2;
						}
					}
				}

				if( false == parent::delete( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $this->getErrorMsgs() );
					$objDatabase->rollback();
					$objClientDatabase->rollback();
					$boolIsValid &= false;
					break;
				}
		}

		if( true == $boolIsValid ) {
			$objDatabase->commit();
			$objClientDatabase->commit();
		}

		return $boolIsValid;

	}

	public function fetchClonedUtilityTransactionsByUtilityTransactionTypeIds( $arrintUtilityTransactionTypeIds, $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchClonedUtilityTransactionsByUtilityBatchIdByUtilityTranscationTypeIds( $this, $arrintUtilityTransactionTypeIds, $objDatabase );
	}

	public function fetchMoveOutUtilityTransactions( $objUtilitiesDatabase, $boolIsAmountOnly ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchMoveOutUtilityTransactionsByUtilityBatch( $this, $objUtilitiesDatabase, $boolIsAmountOnly );
	}

	public function fetchProcessedUtilityBills( $objDatabase, $boolIsDirectMeteredProperty = false ) {
		return \Psi\Eos\Utilities\CUtilityBills::createService()->fetchProcessedUtilityBillsByUtilityBatchIdsByPropertyIds( [ $this->getId() ], [ $this->getPropertyId() ], $objDatabase, $boolIsDirectMeteredProperty );
	}

	public function fetchUtilityAndSubsidyTransactionsByUtilityAccountStatusTypeIds( $arrintUtilityAccountStatusTypeIds, $objDatabase ) {

		$arrintUtilityTransactionTypeIds = [ CUtilityTransactionType::BASE_FEE, CUtilityTransactionType::ESTIMATED_USAGE, CUtilityTransactionType::ACTUAL_USAGE, CUtilityTransactionType::VCR_USAGE_FEE, CUtilityTransactionType::VCR_SERVICE_FEE ];

		$strSql = 'SELECT
						SUM( CASE WHEN ut.utility_transaction_type_id = ' . CUtilityTransactionType::SUBSIDY . ' THEN ut.current_amount END ) as subsidy_charges,
						SUM( CASE WHEN ut.utility_transaction_type_id IN ( ' . sqlIntImplode( $arrintUtilityTransactionTypeIds ) . ' ) THEN ut.current_amount END ) as utility_charges,
						SUM( CASE WHEN ut.utility_transaction_type_id = ' . CUtilityTransactionType::BILLING_FEE . ' THEN ut.current_amount END ) as billing_fee,
						ut.utility_account_id
					FROM utility_transactions ut
					    JOIN utility_accounts ua ON (ua.id = ut.utility_account_id)
					    JOIN utility_batches ub ON (ub.id = ut.utility_batch_id AND ub.utility_batch_step_id >= ' . CUtilityBatchStep::MANAGER_APPROVED . ' )
					WHERE ut.cid = ' . ( int ) $this->getCid() . ' AND
					     ut.property_id = ' . ( int ) $this->getPropertyId() . ' AND
					     ut.utility_batch_id  =  ' . ( int ) $this->getId() . '  AND
					     ut.current_amount <> 0 AND
					     ua.utility_account_status_type_id IN ( ' . sqlIntImplode( $arrintUtilityAccountStatusTypeIds ) . ' ) AND
					     ut.deleted_by IS NULL AND
					     ut.deleted_on IS NULL AND
					     ut.original_utility_transaction_id IS NULL
					GROUP BY
						ut.utility_account_id';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchPropertyUtilitySetting( $objDatabase ) {
		return \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingByPropertyId( $this->getPropertyId(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				$this->addErrorMsg( $objErrorMsg );
			}
		}
    }

    public function fechTotalConsumptionMeterTransmissionBatches( $objUtilitiesDatabase ) {

    	return \Psi\Eos\Utilities\CMeterTransmissionBatches::createService()->fetchTotalConsumtionMeterTransmissionBatchesByUtilityBatchId( $this->getId(), $objUtilitiesDatabase );
    }

    public function fetchTotalVacantUnitConsumptions( $objUtilitiesDatabase ) {

    	return \Psi\Eos\Utilities\CMeterTransmissionBatches::createService()->fetchTotalVacantUnitConsumptionByPropertyIdByUtilityBatchId( $this->getPropertyId(), $this->getId(), $objUtilitiesDatabase );
    }

    public function fetchPropertyUnitTotalVacantUnitConsumptions( $objUtilitiesDatabase ) {

    	return \Psi\Eos\Utilities\CMeterTransmissionBatches::createService()->fetchPropertyUnitTotalVacantUnitConsumptionByPropertyIdByUtilityBatchId( $this->getPropertyId(), $this->getId(), $objUtilitiesDatabase );
    }

    public function fetchUnExportedUtilityTransactions( $objUtilitiesDatabase ) {
    	return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUnExportedUtilityTransactionsByUtilityBatchId( $this->getId(), $objUtilitiesDatabase );
    }

    public function fetchMoveOutUtilityAccounts( $objUtilitiesDatabase ) {
    	return \Psi\Eos\Utilities\CUtilityAccounts::createService()->fetchMoveOutUtilityAccountsByUtilityBatchId( $this->getId(), $objUtilitiesDatabase );
    }

	public function getPostMonth( $boolIsNextMonthPostMonth ) {
		$strPostMonth = $this->getBillingMonth();

		// Generating post month from utility batch billing month.
		if( true == CStrings::strToBool( $boolIsNextMonthPostMonth ) ) {
			$strPostMonth = date( 'm/d/Y', strtotime( '+1 month', strtotime( $this->getBillingMonth() ) ) );
		}

		return $strPostMonth;
	}

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolUpdateBatchActivity = true ) {

		$strSql = 'SELECT utility_batch_step_id FROM utility_batches WHERE id = ' . ( int ) $this->getId();
    	$arrmixOriginalValues = fetchData( $strSql, $objDatabase );

	    $objPropertyUtilitySetting = \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingByPropertyId( $this->getPropertyId(), $objDatabase );

	    $boolIsSkipUtilityBatchActivity = false;

		if( true == in_array( $arrmixOriginalValues[0]['utility_batch_step_id'], [ CUtilityBatchStep::BILL_SELECTION, CUtilityBatchStep::BILLING_PERIOD ] ) && ( true == $this->getIsLeaseChargesProperty() || ( true == $this->getIsManualChargesProperty() && false == $objPropertyUtilitySetting->getDontApproveConvergentServicePeriod() ) ) ) {
			$boolIsSkipUtilityBatchActivity = true;
		} else if( CUtilityBatchStep::BILL_SELECTION == $arrmixOriginalValues[0]['utility_batch_step_id'] && true == $this->getIsManualChargesProperty() && true == $objPropertyUtilitySetting->getDontApproveConvergentServicePeriod() ) {
			$boolIsSkipUtilityBatchActivity = true;
		}

	    if( true == valArr( $arrmixOriginalValues ) && $arrmixOriginalValues[0]['utility_batch_step_id'] <> $this->getUtilityBatchStepId() ) {
		    $this->setLockedBy( $intCurrentUserId );
		    $this->setLockedOn( 'NOW()' );
	    }

		if( true == $boolUpdateBatchActivity && true == valArr( $arrmixOriginalValues ) ) {
			$objUtilityBatchActivity = new CUtilityBatchActivity;
			$objUtilityBatchActivity->setUtilityBatchId( $this->getId() );
			$objUtilityBatchActivity->setUtilityBatchStepId( $arrmixOriginalValues[0]['utility_batch_step_id'] );

			if( true == is_null( $this->getCompanyUserId() ) ) {
				$objUtilityBatchActivity->setEmployeeUserId( $intCurrentUserId );
				$objUtilityBatchActivity->setCreatedBy( $intCurrentUserId );

			} else {
				$objUtilityBatchActivity->setCompanyUserId( $this->getCompanyUserId() );
				$objUtilityBatchActivity->setCreatedBy( $this->getCompanyUserId() );
			}

			if( false == $boolIsSkipUtilityBatchActivity && $arrmixOriginalValues[0]['utility_batch_step_id'] > $this->getUtilityBatchStepId() ) {

				if( true == $this->m_boolIsPreBillRejected ) {
					$objUtilityBatchActivity->setMemo( $this->m_strPrebillRejectionReason );
				}

				$objUtilityBatchActivity->setUtilityBatchActivityTypeId( CUtilityBatchActivityType::ROLLBACK );
				$objUtilityBatchActivity->insert( $intCurrentUserId, $objDatabase );

			} elseif( false == $boolIsSkipUtilityBatchActivity && $arrmixOriginalValues[0]['utility_batch_step_id'] <= $this->getUtilityBatchStepId() ) {
				// forward
    			$objUtilityBatchActivity->setUtilityBatchActivityTypeId( CUtilityBatchActivityType::FORWARD );

    			if( CUser::ID_SCRIPTS_LAYER == $intCurrentUserId || CUser::ID_SYSTEM == $intCurrentUserId ) {
					$objUtilityBatchActivity->setApprovedBy( $intCurrentUserId );
					$objUtilityBatchActivity->setApprovedOn( 'NOW()' );
				}

				if( CUtilityBatchStep::COMPLETE == $this->getUtilityBatchStepId() ) {
					$objUtilityBatchActivity->setMemo( 'Utility batch completed.' );

					$objCompleteUtilityBatchActivity = $this->createCompleteUtilityBatchActivity( $intCurrentUserId );
				}

				if( CUtilityBatchStep::QC_APPROVED == $arrmixOriginalValues[0]['utility_batch_step_id'] ) {
					$objUtilityBatchActivity->setMemo( $this->getNotes() );
				}

				$objUtilityBatchActivity->insert( $intCurrentUserId, $objDatabase );

				if( CUtilityBatchStep::COMPLETE == $this->getUtilityBatchStepId() ) {
					$objCompleteUtilityBatchActivity->insert( $intCurrentUserId, $objDatabase );
				}
			}
		}

		$boolUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		return $boolUpdateStatus;
    }

    public function updateUtilityBatchDetails( $intCurrentUserId, $objDatabase ) {

	    $arrobjVcrUtilityBills				= ( array ) \Psi\Eos\Utilities\CVcrUtilityBills::createService()->fetchUnBatchedVcrUtilityBillsByPropertyId( $this->getPropertyId(), $objDatabase );
	    $arrobjMoveOutUtilityTransactions	= ( array ) \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUnBatchedMoveOutUtilityTransactionsByPropertyId( $this->getPropertyId(), $objDatabase );

	    foreach( $arrobjVcrUtilityBills as $objVcrUtilityBill ) {

		    $objVcrUtilityBill->setUtilityBatchId( $this->getId() );

		    if( false == $objVcrUtilityBill->update( $intCurrentUserId, $objDatabase ) ) {
			    $this->m_objUtilitiesDatabase->rollback();
			    break;
			}
	    }

	    foreach( $arrobjMoveOutUtilityTransactions as $objMoveOutUtilityTransaction ) {

		    $objMoveOutUtilityTransaction->setUtilityBatchId( $this->getId() );

		    if( false == $objMoveOutUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
			    $this->m_objUtilitiesDatabase->rollback();
			    break;
		    }
	    }
    }

	public function validateIsLeaseOrManualChargesProperty( $objUtilitiesDatabase ) {

		$intManualPropertyUtilityTypeRates = 0;
		$this->m_boolIsLeaseChargesProperty = $this->m_boolIsManualChargesProperty = false;

		$arrobjActivePropertyUtilityTypes = ( array ) \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchActiveBillablePropertyUtilityTypesByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );
		$arrobjActiveVcrPropertyUtilityTypes = ( array ) \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchActiveVcrPropertyUtilityTypesByPropertyId( $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );

		if( true == valArr( $arrobjActivePropertyUtilityTypes ) && 1 == \Psi\Libraries\UtilFunctions\count( $arrobjActivePropertyUtilityTypes ) && false == valArr( $arrobjActiveVcrPropertyUtilityTypes ) ) {
			foreach( $arrobjActivePropertyUtilityTypes as $objActivePropertyUtilityType ) {
				if( CUtilityType::CONVERGENT_BILLING == $objActivePropertyUtilityType->getUtilityTypeId() ) {
					$this->m_boolIsLeaseChargesProperty = true;
				}
			}
		}

		if( false == $this->m_boolIsLeaseChargesProperty && false == valArr( $arrobjActiveVcrPropertyUtilityTypes ) ) {
			$arrmixBillablePropertyUtilityTypeRates = ( array ) \Psi\Eos\Utilities\CPropertyUtilityTypeRates::createService()->fetchBillablePropertyUtilityTypeRatesByPropertyUtilityTypeIds( array_filter( array_keys( $arrobjActivePropertyUtilityTypes ) ), $objUtilitiesDatabase );

			if( true == valArr( $arrmixBillablePropertyUtilityTypeRates ) ) {
				foreach( $arrmixBillablePropertyUtilityTypeRates as $arrmixBillablePropertyUtilityTypeRate ) {
					if( CUtilityRubsFormula::MANUAL == $arrmixBillablePropertyUtilityTypeRate['utility_rubs_formula_id'] ) {
						$intManualPropertyUtilityTypeRates++;
					}
				}

				if( \Psi\Libraries\UtilFunctions\count( $arrmixBillablePropertyUtilityTypeRates ) == $intManualPropertyUtilityTypeRates ) {
					$this->m_boolIsManualChargesProperty = true;
				}
			}
		}

	}

	public function validatePostMonth( $intCurrentUserId, $strPostMonth, $objClientDatabase, $objUtilitiesDatabase ) {

		$arrstrClosedPostMonths = ( array ) CPeriods::fetchCustomClosedPostMonthsByPostMonthByPropertyIdByCid( $strPostMonth, $this->getPropertyId(), $this->getCid(), $objClientDatabase );

		$boolIsValidPostMonth = true;
		if( true == in_array( $strPostMonth, $arrstrClosedPostMonths ) ) {
			$boolIsValidPostMonth = false;
			$arrmixPropertyDetails = current( ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchCustomPropertyDetailsByPropertyIds( [ $this->getPropertyId() ], $objClientDatabase ) );
			$objUtilityBatchActivity = \Psi\Eos\Utilities\CUtilityBatchActivities::createService()->fetchUtilityBatchActivityByUtilityBatchStepIdByUtilityBatchId( CUtilityBatchStep::COMPLETE, $this->getId(), $objUtilitiesDatabase );

			if( true == valObj( $objUtilityBatchActivity, 'CUtilityBatchActivity' ) ) {

				$strCompanyName 	= '<span class="bold">' . str_replace( '"', '', str_replace( '\'', '', $arrmixPropertyDetails['company_name'] ) ) . '</span>';
				$strPropertyName 	= '<span class="bold">' . str_replace( '"', '', str_replace( '\'', '', $arrmixPropertyDetails['property_name'] ) ) . '</span>';

				$objUtilityBatchActivity->setErrorNote( 'We were unable to export transactions for ' . $strCompanyName . ' - ' . $strPropertyName . ' because the AR post month for <span class="bold">' . $strPostMonth . '</span> is closed. Please reach out to the client and ask them to re-open the post month.' );

				if( false == $objUtilityBatchActivity->update( $intCurrentUserId, $objUtilitiesDatabase ) ) {
					return false;
				}
			}
		}

		return $boolIsValidPostMonth;
	}

	public function validateChargesCodes( $intCurrentUserId, $objClientDatabase, $objUtilitiesDatabase ) {

		$strSql = 'SELECT
						COUNT(ut.id) as utility_transaction_count
					FROM
						utility_transactions ut 
						INNER JOIN utility_integration_settings uis ON( uis.property_id = ut.property_id )
					WHERE
						ut.utility_batch_id = ' . $this->getId() . '
						AND ut.utility_transaction_type_id NOT IN ( ' . CUtilityTransactionType::CONVERGENT . ',' . CUtilityTransactionType::PRE_POSTED_CONVERGENT . ',' . CUtilityTransactionType::TAX . '  )
						AND ut.is_move_out IS FALSE
                        AND ut.original_utility_transaction_id IS NULL
                        AND uis.is_export_batch_transactions IS FALSE';

		$arrintUtilityTransactionCount = fetchData( $strSql, $objUtilitiesDatabase );

		if( 0 == $arrintUtilityTransactionCount[0]['utility_transaction_count'] ) {
			return true;
		}

		$strSql = 'SELECT
						DISTINCT ut.ar_code_id,
						ut.property_utility_type_id,
						put.name
					FROM
						utility_transactions ut
						JOIN utility_accounts ua ON ( ua.id = ut.utility_account_id )
						JOIN property_utility_types put ON ( put.id = ut.property_utility_type_id )
					WHERE
						ut.cid = ' . $this->getCid() . '
						AND ut.property_id = ' . $this->getPropertyId() . '
						AND ut.utility_batch_id = ' . $this->getId() . '
						AND ua.utility_account_status_type_id IN ( ' . CUtilityAccountStatusType::CURRENT . ',' . CUtilityAccountStatusType::PENDING_MOVE_OUT . ' )
						AND ut.utility_transaction_type_id NOT IN ( ' . CUtilityTransactionType::CONVERGENT . ',' . CUtilityTransactionType::PRE_POSTED_CONVERGENT . ',' . CUtilityTransactionType::TAX . '  )
						AND ut.scheduled_export_date IS NOT NULL
						AND ut.is_move_out IS FALSE
						AND ut.exported_by IS NULL
						AND ut.exported_on IS NULL
						AND ut.original_utility_transaction_id IS NULL
					GROUP BY
						ut.ar_code_id,
						ut.property_utility_type_id,
						put.name
					ORDER BY
						ut.property_utility_type_id';

		$arrmixArCodesDetails = rekeyArray( 'ar_code_id', fetchData( $strSql, $objUtilitiesDatabase ) );

		$strErrorNote = '';
		$boolIsValidChargeCodes = true;

		$arrmixPropertyDetails = current( ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchCustomPropertyDetailsByPropertyIds( [ $this->getPropertyId() ], $objClientDatabase ) );
		$objUtilityBatchActivity = \Psi\Eos\Utilities\CUtilityBatchActivities::createService()->fetchUtilityBatchActivityByUtilityBatchStepIdByUtilityBatchId( CUtilityBatchStep::COMPLETE, $this->getId(), $objUtilitiesDatabase );

		$strCompanyName 	= '<span class="bold">' . str_replace( '"', '', str_replace( '\'', '', $arrmixPropertyDetails['company_name'] ) ) . '</span>';
		$strPropertyName 	= '<span class="bold">' . str_replace( '"', '', str_replace( '\'', '', $arrmixPropertyDetails['property_name'] ) ) . '</span>';

		if( false == valArr( $arrmixArCodesDetails ) ) {
			$boolIsValidChargeCodes = false;
			$strErrorNote = 'Batch <span class="bold">#' . $this->getId() . '</span> for ' . $strCompanyName . ' - ' . $strPropertyName . ' is completed and has no transactions which are scheduled to export. Please notify development immediately.';
		} else {
			$strSql = 'SELECT id, name FROM ar_codes WHERE id IN( ' . sqlIntImplode( array_keys( $arrmixArCodesDetails ) ) . ' ) AND cid = ' . $this->getCid() . ' AND is_disabled IS TRUE';

			$arrmixArCodes = rekeyArray( 'id', ( array ) fetchData( $strSql, $objClientDatabase ) );

			$arrmixArCodes = array_intersect( $arrmixArCodes, $arrmixArCodesDetails );

			if( true == valArr( $arrmixArCodes ) ) {

				$boolIsValidChargeCodes = false;
				foreach( $arrmixArCodes as $intArCodeId => $arrmixArCode ) {

					$arrmixArCodesDetail = getArrayElementByKey( $intArCodeId, $arrmixArCodesDetails );

					$strDataItem = 'data-cid="' . $arrmixPropertyDetails['cid'] . '"';
					$strDataItem .= 'data-property-id="' . $arrmixPropertyDetails['id'] . '"';
					$strDataItem .= 'data-ps-lead-id="' . $arrmixPropertyDetails['ps_lead_id'] . '"';
					$strDataItem .= 'data-property-utility-type-id="' . $arrmixArCodesDetail['property_utility_type_id'] . '"';
					$strLinkText = 'Please click <span class="update-utility-charge-code link text-blue bold" ' . $strDataItem . '>HERE</span> to assign a charge code.';
					$strErrorNote .= '<br>We were unable to export transactions for ' . $strCompanyName . ' - ' . $strPropertyName . ' because the charge code ' . $arrmixArCode['name'] . ' for utility type ' . $arrmixArCodesDetail['name'] . ' is no longer exists.' . $strLinkText;
				}
			}
		}

		if( true == valObj( $objUtilityBatchActivity, 'CUtilityBatchActivity' ) ) {
			$objUtilityBatchActivity->setErrorNote( $objUtilityBatchActivity->getErrorNote() . $strErrorNote );

			if( false == $objUtilityBatchActivity->update( $intCurrentUserId, $objUtilitiesDatabase ) ) {
				return false;
			}
		}

		return $boolIsValidChargeCodes;
	}

	public function createCompleteUtilityBatchActivity( $intCurrentUserId ) : CUtilityBatchActivity {

		$objUtilityBatchActivity = new CUtilityBatchActivity();

		$objUtilityBatchActivity->setCreatedBy( $intCurrentUserId );
		$objUtilityBatchActivity->setUtilityBatchId( $this->getId() );
		$objUtilityBatchActivity->setEmployeeUserId( $intCurrentUserId );
		$objUtilityBatchActivity->setUtilityBatchStepId( CUtilityBatchStep::COMPLETE );
		$objUtilityBatchActivity->setUtilityBatchActivityTypeId( CUtilityBatchActivityType::FORWARD );

		return $objUtilityBatchActivity;
	}

	private function getPostDate( $strPostMonth, $objUtilitiesDatabase, $objPropertyUtilitySetting ) {

		$strPostDate = date( 'm/d/Y h:i:s', mktime( 0, 0, 0, date( 'm' ) + 1, 1, date( 'Y' ) ) );

		if( true == empty( $strPostMonth ) ) {
			return $strPostDate;
		}

		if( CUtilityPostDateType::FIRST_OF_POST_MONTH == $objPropertyUtilitySetting->getUtilityPostDateTypeId() ) {

			$strPostDate = date( 'm/01/Y', strtotime( $strPostMonth ) );
		} elseif( CUtilityPostDateType::DISTRIBUTION_DATE == $objPropertyUtilitySetting->getUtilityPostDateTypeId() ) {

			$strPostDate = date( 'm/d/Y', strtotime( $this->getDistributionDeadline() ) );
		} elseif( CUtilityPostDateType::UTILITY_DATE == $objPropertyUtilitySetting->getUtilityPostDateTypeId() ) {

			$arrmixUtilityDueDates = ( array ) \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchUtilityInvoicesDueDatesByUtilityBatchIds( [ $this->getId() ], $objUtilitiesDatabase );
			$arrmixUtilityDueDates = rekeyArray( 'utility_batch_id', $arrmixUtilityDueDates );

			if( true == array_key_exists( $this->getId(), $arrmixUtilityDueDates ) ) {
				$strPostDate = date( 'm/d/Y', strtotime( $arrmixUtilityDueDates[$this->getId()]['invoice_due_date'] ) );
			}
		}

		return $strPostDate;
	}

}
?>
