<?php

class CVendorPaymentAccountVendorEntity extends CBaseVendorPaymentAccountVendorEntity {

	public function valId() {
		return true;
	}

	public function valVendorPaymentAccountId() {
		return true;
	}

	public function valVendorEntityId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>