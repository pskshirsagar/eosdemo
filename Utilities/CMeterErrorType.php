<?php

class CMeterErrorType extends CBaseMeterErrorType {

	const EXCEPTION		= 1;
	const LOW_BATTERY	= 2;
	const TAMPER		= 3;
	const INACTIVE		= 4;
	const RESET			= 5;
	const UNPLUGGED		= 6;
	const OK			= 7;

    /**
     * Get Functions
     */

    public static function getMeterErrorTypeByErrorCode( $strErrorCode ) {

    	$intMeterErrorTypeId = NULL;

    	switch( $strErrorCode ) {

    		case 'E':
    			$intMeterErrorTypeId = self::EXCEPTION;
    			break;

    		case 'B':
    			$intMeterErrorTypeId = self::LOW_BATTERY;
    			break;

    		case 'T':
    			$intMeterErrorTypeId = self::TAMPER;
    			break;

    		case 'I':
    			$intMeterErrorTypeId = self::INACTIVE;
    			break;

    		case 'R':
    			$intMeterErrorTypeId = self::RESET;
    			break;

    		case 'P':
    			$intMeterErrorTypeId = self::UNPLUGGED;
    			break;

    		case 'OK':
    			$intMeterErrorTypeId = self::OK;
    			break;

    		default:
    			// no action
    	}

    	return $intMeterErrorTypeId;
    }

}
?>