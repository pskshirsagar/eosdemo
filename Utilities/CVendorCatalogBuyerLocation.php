<?php

class CVendorCatalogBuyerLocation extends CBaseVendorCatalogBuyerLocation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVendorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBuyerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVendorCatalogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBuyerLocationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>