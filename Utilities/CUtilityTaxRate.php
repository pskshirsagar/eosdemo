<?php

class CUtilityTaxRate extends CBaseUtilityTaxRate {

	protected $m_arrobjUtilityTaxRateAssociations;
	protected $m_arrstrCounties;

	protected $m_boolIsDeleteAssociation;

	public function getCounties() {
		return $this->m_arrstrCounties;
	}

	public function getIsDeleteAssociation() {
		return $this->m_boolIsDeleteAssociation;
	}

	public function setIsDeleteAssociation( $boolIsDeleteAssociation ) {
		$this->m_boolIsDeleteAssociation = $boolIsDeleteAssociation;
	}

	public function addUtilityTaxRateAssociation( $objUtilityTaxRateAssociation ) {
		$this->m_arrobjUtilityTaxRateAssociations[$objUtilityTaxRateAssociation->getId()] = $objUtilityTaxRateAssociation;
	}

	public function getUtilityTaxRateAssociations() {
		return $this->m_arrobjUtilityTaxRateAssociations;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} else {

			$strSql = 'SELECT *  FROM utility_tax_rates WHERE name =\'' . $this->getName() . '\'';

			if( false == is_null( $this->getId() ) ) {
				$strSql .= ' AND id <>' . $this->getId();
			}

			$arrobjUtilityTaxRates = \Psi\Eos\Utilities\CUtilityTaxRates::createService()->fetchUtilityTaxRates( $strSql, $objDatabase );

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjUtilityTaxRates ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name aleady exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valTaxRatePercent() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaxRatePercent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_rate_percent', 'Tax rate percent is required.' ) );
		} elseif( 0 > $this->getTaxRatePercent() || $this->getTaxRatePercent() > 1 ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_rate_percent', 'Invalid tax rate percent.' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateCounty( $objDatabase ) {

		$boolIsValid = true;
		$strExistCounty = '';

		if( true == valArr( $this->getCounties() ) && false == is_null( $this->getTaxRatePercent() ) ) {
			$arrobjExistUtilityCounties = \Psi\Eos\Utilities\CUtilityTaxRates::createService()->fetchUtilityTaxRatesByStateCodeByCounties( $this->getStateCode(), $this->getCounties(), $objDatabase );

			if( true == valArr( $arrobjExistUtilityCounties ) ) {
				foreach( $arrobjExistUtilityCounties as $objExistUtilityCount ) {
					if( false == empty( $strExistCounty ) ) {
						$strExistCounty .= ',';
					}

					$strExistCounty .= \Psi\CStringService::singleton()->ucfirst( \Psi\CStringService::singleton()->strtolower( $objExistUtilityCount->getCounty() ) );
				}

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strExistCounty . ' utility tax rate coutny(s) is already exist.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {

			$strSql = ' ( SELECT utility_tax_rate_id FROM property_utility_types WHERE utility_tax_rate_id = ' . ( int ) $this->getId();
			$strSql .= ' ) UNION ( SELECT utility_tax_rate_id FROM utility_tax_rate_utility_types WHERE utility_tax_rate_id = ' . ( int ) $this->getId();
			$strSql .= ' ) UNION ( SELECT utility_tax_rate_id FROM utility_tax_rate_associations WHERE utility_tax_rate_id = ' . ( int ) $this->getId() . ')';

			$arrmixData = fetchData( $strSql, $objDatabase );

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixData ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to delete the utility tax rate, it is being associated with property utility type / utility tax rate utility type association / utility tax rate association.' ) );
			}
		} else {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' Invalid database object.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valTaxRatePercent();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;

	}

	public function createUtilityTaxRate() {

		$objUtilityTaxRate = new CUtilityTaxRate;
		$objUtilityTaxRate->setStateCode( $this->getStateCode() );
		$objUtilityTaxRate->setTaxRatePercent( $this->getTaxRatePercent() );
		$objUtilityTaxRate->setIsResidentRate( $this->getIsResidentRate() );

		return $objUtilityTaxRate;
	}

	public function createUtilityTaxRateAssociation() {
		$objUtilityTaxRateAssociation = new CUtilityTaxRateAssociation();
		$objUtilityTaxRateAssociation->setUtilityTaxRateId( $this->getId() );
		return $objUtilityTaxRateAssociation;
	}

	public function addCounties( $arrstrCounties ) {
		$this->m_arrstrCounties = $arrstrCounties;
	}

	public function fetchUtilityTaxRateAssociations( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTaxRateAssociations::createService()->fetchUtilityTaxRateAssociationsByUtilityTaxRateId( $this->getId(), $objUtilitiesDatabase );

	}

}
?>