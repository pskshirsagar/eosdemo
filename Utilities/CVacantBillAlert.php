<?php

class CVacantBillAlert extends CBaseVacantBillAlert {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_intCid ) || true == is_null( $this->m_intCid ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_intPropertyId ) || true == is_null( $this->m_intPropertyId ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_intPropertyUnitId ) || true == is_null( $this->m_intPropertyUnitId ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>