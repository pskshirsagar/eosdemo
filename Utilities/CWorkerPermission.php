<?php

class CWorkerPermission extends CBaseWorkerPermission {

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valWorkerId() {
		return true;
	}

	public function valControllerId() {
		return true;
	}

	public function valIsAllowed() {
		return true;
	}

	public function valIsInherited() {
		return true;
	}

	public function valOptions() {
		return true;
	}

	public function valOrderNum() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>