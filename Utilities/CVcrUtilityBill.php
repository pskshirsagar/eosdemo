<?php

class CVcrUtilityBill extends CBaseVcrUtilityBill {

	protected $m_intUtilityBillAmount;

	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strPropertyUtilityName;
	protected $m_strBillDatetime;
	protected $m_strBatchDatetime;

	protected $m_fltVacantUnitBillAlertAmount;


	/**
     * Set Methods
     */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_bill_amount'] ) ) {
			$this->setUtilityBillAmount( $arrmixValues['utility_bill_amount'] );
		}
		if( true == isset( $arrmixValues['start_date'] ) ) {
			$this->setStartDate( $arrmixValues['start_date'] );
		}
		if( true == isset( $arrmixValues['end_date'] ) ) {
			$this->setEndDate( $arrmixValues['end_date'] );
		}
		if( true == isset( $arrmixValues['vacant_unit_bill_alert_amount'] ) ) {
			$this->setVacantUnitBillAlertAmount( $arrmixValues['vacant_unit_bill_alert_amount'] );
		}

		if( true == isset( $arrmixValues['property_utility_name'] ) ) {
			$this->setPropertyUtilityName( $arrmixValues['property_utility_name'] );
		}
		if( true == isset( $arrmixValues['batch_datetime'] ) ) {
			$this->setBatchDatetime( $arrmixValues['batch_datetime'] );
		}
		if( true == isset( $arrmixValues['bill_datetime'] ) ) {
			$this->setBillDatetime( $arrmixValues['bill_datetime'] );
		}
	}

	public function setUtilityBillAmount( $intUtilityBillAmount ) {
		$this->m_intUtilityBillAmount = $intUtilityBillAmount;
	}

    public function setStartDate( $strStartDate ) {
        $this->m_strStartDate = CStrings::strTrimDef( $strStartDate, -1, NULL, true );
    }

	public function setEndDate( $strEndDate ) {
        $this->m_strEndDate = CStrings::strTrimDef( $strEndDate, -1, NULL, true );
    }

 	public function setVacantUnitBillAlertAmount( $fltVacantUnitBillAlertAmount ) {
        $this->m_fltVacantUnitBillAlertAmount = CStrings::strToFloatDef( $fltVacantUnitBillAlertAmount, NULL, false, 2 );
    }

    public function setPropertyUtilityName( $strPropertyUtilityName ) {
    	$this->m_strPropertyUtilityName = $strPropertyUtilityName;
    }

    public function setBatchDatetime( $strBatchDatetime ) {
    	$this->m_strBatchDatetime = $strBatchDatetime;
    }

    public function setBillDatetime( $strBillDatetime ) {
    	$this->m_strBillDatetime = CStrings::strTrimDef( $strBillDatetime, -1, NULL, true );
    }

    /**
     * Get Methods
     */

	public function getUtilityBillAmount() {
		return $this->m_intUtilityBillAmount;
	}

 	public function getStartDate() {
        return $this->m_strStartDate;
    }

    public function getEndDate() {
        return $this->m_strEndDate;
    }

	public function getVacantUnitBillAlertAmount() {
        return $this->m_fltVacantUnitBillAlertAmount;
    }

    public function getPropertyUtilityName() {
    	return $this->m_strPropertyUtilityName;
    }

    public function getBatchDatetime() {
    	return $this->m_strBatchDatetime;
    }

    public function getBillDatetime() {
    	return $this->m_strBillDatetime;
    }

    public function valDependencies( $objDatabase ) {
    	$boolIsValid = true;

    	$arrstrVcrUtilityBills = \Psi\Eos\Utilities\CVcrUtilityBills::createService()->fetchDistributedVcrUtilityBillById( $this->getId(), $objDatabase );
		$strVcrUtilityBill	   = array_shift( $arrstrVcrUtilityBills );
    	if( 0 < $strVcrUtilityBill['count'] ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '0', 'Vcr Utility bill ' . $this->getId() . ' is associated with the batch so cannot delete.' ) );
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valDependencies( $objDatabase );
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function createUtilityInvoice() {

		$objUtilityInvoice = new CUtilityInvoice();
		$objUtilityInvoice->setCid( $this->getCid() );
		$objUtilityInvoice->setPropertyId( $this->getPropertyId() );
		$objUtilityInvoice->setPropertyUnitId( $this->getPropertyUnitId() );
		$objUtilityInvoice->setUtilityAccountId( $this->getUtilityAccountId() );

		return $objUtilityInvoice;
	}

	public function fetchUtilityBill( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBills::createService()->fetchUtilityBillById( $this->getUtilityBillId(), $objDatabase );
	}

}
?>