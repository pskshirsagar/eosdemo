<?php

class COrderDetail extends CBaseOrderDetail {

	protected $m_strLocationName;
	protected $m_intPropertyId;
	protected $m_intPoApDetailId;

	/**
	 * @return int
	 */
	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	/**
	 * @param int $intPropertyId
	 */
	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = ( int ) $intPropertyId;
	}

	/**
	 * @return int
	 */
	public function getPoApDetailId() {
		return $this->m_intPoApDetailId;
	}

	/**
	 * @param int $intPoApDetailId
	 */
	public function setPoApDetailId( $intPoApDetailId ) {
		$this->m_intPoApDetailId = ( int ) $intPoApDetailId;
	}

	public function valBuyerLocationId() {

		$boolIsValid = true;

		if( false == valId( $this->getBuyerLocationId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'buyer_location_id', __( 'Please select the property.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valTransactionAmount() {

		$boolIsValid = true;

		if( 0 == $this->getTransactionAmount() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'transaction_amount', __( 'Please select the rate for line item.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBuyerLocationId();
				$boolIsValid &= $this->valTransactionAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = CStrings::strTrimDef( $strLocationName, 100, NULL, true );
	}

	public function setCatalogFields( $objApDetail, $arrobjApCodes, $arrobjApCatalogItems, $arrobjUnitOfMeasures ) {

		$objApCode = getArrayElementByKey( $objApDetail->getApCodeId(), $arrobjApCodes );
		if( true == valObj( $objApCode, CApCode::class ) ) {
			$this->setCatalogItemName( $objApCode->getName() );
		}

		$objApCatalogItem = getArrayElementByKey( $objApDetail->getApCatalogItemId(), $arrobjApCatalogItems );
		if( true == valObj( $objApCatalogItem, CApCatalogItem::class ) ) {
			$this->setVendorSku( $objApCatalogItem->getVendorSku() );
		}

		$objUnitOfMeasure = getArrayElementByKey( $objApDetail->getUnitOfMeasureId(), $arrobjUnitOfMeasures );
		if( true == valObj( $objUnitOfMeasure, CUnitOfMeasure::class ) ) {
			$this->setPurchasingUnitOfMeasure( $objUnitOfMeasure->getName() );
		}
	}

	/**
	 * Get Functions
	 *
	 */

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['location_name'] ) && $boolDirectSet ) {
			$this->m_strLocationName = trim( $arrmixValues['location_name'] );
		} elseif( isset( $arrmixValues['location_name'] ) ) {
			$this->setLocationName( $arrmixValues['location_name'] );
		} elseif( isset( $arrmixValues['po_ap_detail_id'] ) ) {
			$this->setPoApDetailId( $arrmixValues['po_ap_detail_id'] );
		} elseif( isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>