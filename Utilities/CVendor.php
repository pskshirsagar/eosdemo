<?php

class CVendor extends CBaseVendor {

	public static $c_intSystemVendorId = 1;

	const VALIDATE_INITIAL_SETUP	 = 'validate_initial_setup';
	const GOOGLE_CAPTCHA_SITE_KEY	 = '6Lebeh0UAAAAACQ1VHRm7MSHi7wvn_DpWsLBTA9G';
	const GOOGLE_CAPTCHA_SECRETE_KEY = '6Lebeh0UAAAAAI5mNiyYKffyMgMLjMeTBO1MzYNJ';

	// we have defined this constant to show maintenance supply punchout
	const ID_WILMAR_TEST_ACCOUNT   = 635;
	const ID_SHOPTEST_TEST_ACCOUNT = 636;
	const ID_BESTBUY_TEST_ACCOUNT = 6288;
	const ID_CONSTRUCTION_AHOY_TEST = 505;
	const ID_TESTSONALI123_RAPID_TEST = 1254;
	const ID_AMITUSERDBA_RAPID_TEST = 21012;
	const ID_AMIT_RAPID_USER = 34252;
	const ID_HD_SUPPLY_TEST = 34354;
	const ID_VIJAYA_BANK_TEST_ACCOUNT = 33877;

	protected $m_intWorkerId;

	protected $m_strCidEncrypted;
	protected $m_strLocationName;
	protected $m_strTaxNumberEncrypted;

	// Define array for the automation clients to skip the google captcha validation

	public static $c_arrintAutomationTestClientIds  = [ 10885 => 10885, 15359 => 15359, 17515 => 17515 ];

	public function valId() {
		return true;
	}

	public function valDefaultVendorRemittanceId() {
		return true;
	}

	public function valDefaultVendorEntityId() {
		$boolIsValid = true;

		if( false == valId( $this->getDefaultVendorEntityId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_entity_id', __( 'Entity type is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		return true;
	}

	public function valCompanyName() {

		$boolIsValid = true;

		if( false == valStr( $this->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'company_name', __( 'Business name is required.' ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * Validation Function : Google recaptcha for vendor sign up form
	 */

	public function verifyGoogleCaptcha( $strResponse, $strSecretKey ) {

		if( true == $this->isAutomationTestClientIds() ) {
			return true;
		}

		$strUrl = $this->getGoogleCaptchaUrl( $strSecretKey, $strResponse );

		$arrmixParameters = $this->setGoogleCaptchaUrlParameter( $strUrl );

		$objExternalRequest = $this->createObjExternalRequest();

		if( 'true' == $this->executeExternalRequest( $objExternalRequest, $arrmixParameters ) ) {
			$boolIsValid = true;
		} else {
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'g-recaptcha-response', __( 'It seems you are robot, please try submitting the form once again by selecting verification field!' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function isAutomationTestClientIds() {
		return isset( self::$c_arrintAutomationTestClientIds[$this->getCid()] );
	}

	public function getGoogleCaptchaUrl( $strSecretKey, $strResponse ) {
		return 'https://www.google.com/recaptcha/api/siteverify?secret=' . $strSecretKey . '&response=' . $strResponse;
	}

	public function setGoogleCaptchaUrlParameter( $strUrl ) {
		return [
			'url'				=> $strUrl,
			'execution_timeout'	=> 15,
			'ssl_verify_peer'	=> false,
			'ssl_verify_host'	=> false
		];
	}

	public function createObjExternalRequest() {
		return new CExternalRequest();
	}

	public function executeExternalRequest( $objExternalRequest, $arrmixParameters ) {

		$objExternalRequest->setParameters( $arrmixParameters );

		$strCurlData = $objExternalRequest->execute( true );

		unset( $objExternalRequest );

		$arrmixResult = json_decode( $strCurlData, true );

		return $arrmixResult['success'];
	}

	public function valSubdomain() {
		return true;
	}

	public function valCompanyLogo() {
		return true;
	}

	public function valWebsiteUrl() {

		$boolIsValid = true;

		if( false == is_null( $this->getWebsiteUrl() ) && false == preg_match( '@^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*$@i', $this->getWebsiteUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'website_url', __( 'Invalid website url format.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStartDate() {

		if( true == is_null( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is required.' ) ) );
		} elseif( false == preg_match( '/(?:(?:19|20)[0-9]{2})/', $this->getStartDate() ) || true == $this->getStartDate() > date( 'Y' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'start_date', __( 'The year entered should be in the range from {%s, 0} to {%s, 1}.', [ '1900', date( 'Y' ) ] ) ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valIsServiceProvider() {
		return true;
	}

	public function valDisabledBy() {
		return true;
	}

	public function valDisabledOn() {
		return true;
	}

	public function validate( $strAction, $strGoogleResponse = NULL, $strGoogleCaptchaSecreteKey = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->verifyGoogleCaptcha( $strGoogleResponse, $strGoogleCaptchaSecreteKey );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valWebsiteUrl();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valCompanyName();
				break;

			case self::VALIDATE_INITIAL_SETUP:
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valWebsiteUrl();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['location_name'] ) && $boolDirectSet ) {
			$this->m_strLocationName = trim( stripcslashes( $arrmixValues['location_name'] ) );
		} elseif( isset( $arrmixValues['location_name'] ) ) {
			$this->setLocationName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['location_name'] ) : $arrmixValues['location_name'] );
		}

		if( isset( $arrmixValues['tax_number_encrypted'] ) && $boolDirectSet ) {
			$this->m_strTaxNumberEncrypted = trim( stripcslashes( $arrmixValues['tax_number_encrypted'] ) );
		} elseif( isset( $arrmixValues['tax_number_encrypted'] ) ) {
			$this->setTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['tax_number_encrypted'] ) : $arrmixValues['tax_number_encrypted'] );
		}

		if( isset( $arrmixValues['worker_id'] ) && $boolDirectSet ) {
			$this->set( 'm_intWorkerId', trim( $arrmixValues['worker_id'] ) );
		} elseif( isset( $arrmixValues['worker_id'] ) ) {
			$this->setWorkerId( $arrmixValues['worker_id'] );
		}
	}

	/**
	 * Set Functions
	 */

	public function setCidEncrypted( $strCidEncrypted ) {
		$this->m_strCidEncrypted = $strCidEncrypted;
	}

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = CStrings::strTrimDef( $strLocationName, 100, NULL, true );
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->m_strTaxNumberEncrypted = CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true );
	}

	public function setWorkerId( $intWorkerId ) {
		$this->set( 'm_intWorkerId', CStrings::strToIntDef( $intWorkerId, NULL, false ) );
	}

	/**
	 * Get Functions
	 */

	public function getCidEncrypted() {
		return $this->m_strCidEncrypted;
	}

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function getTaxNumber() {
		if( false == valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function getTaxNumberMasked() {

		if( CWorker::MAX_TAX_NUMBER == strlen( $this->getTaxNumber() ) ) {
			return $this->getTaxNumberEncrypted();
		}

		return CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $this->getTaxNumber() ) );
	}

	public function getIsDisabled() {
		return ( false == is_null( $this->getDisabledBy() ) && false == is_null( $this->getDisabledOn() ) ) ? true: false;
	}

	public function getWorkerId() {
		return $this->m_intWorkerId;
	}

	/**
	 * Create Functions
	 */

	public function createPool() {
		$objPool = new CPool();
		$objPool->setVendorId( $this->getId() );
		return $objPool;
	}

	public function createPoolPermission() {
		$objPoolPermission = new CPoolPermission();
		$objPoolPermission->setVendorId( $this->getId() );
		return $objPoolPermission;
	}

	public function createWorkerPool() {
		$objWorkerPool = new CWorkerPool();
		$objWorkerPool->setVendorId( $this->getId() );
		return $objWorkerPool;
	}

	public function createVendorPreference() {
		$objVendorPreference = new CVendorPreference();
		$objVendorPreference->setVendorId( $this->getId() );
		return $objVendorPreference;
	}

	public function createComplianceDoc() {

		$objComplianceDoc = new CComplianceDoc();
		$objComplianceDoc->setVendorId( $this->getId() );
		$objComplianceDoc->setIsVendorDoc( true );
		$objComplianceDoc->setDocDatetime( 'NOW()' );

		return $objComplianceDoc;
	}

	public function isTestVendor() {
		return valId( $this->getCid() );
	}

}
?>