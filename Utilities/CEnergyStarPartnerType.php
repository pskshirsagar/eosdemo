<?php

class CEnergyStarPartnerType extends CBaseEnergyStarPartnerType {

	const ASSOCIATION 					= 1;
	const BUILDING_OWNERS_MANAGERS 		= 2;
	const SERVICE_PRODUCT_PROVIDERS 	= 3;
	const SMALL_BUSINESSES 				= 4;
	const UTILITY_ENERGY_SPONSORS 		= 5;

	public static $c_arrstrEnergyStarPartnerTypes = [
		self::ASSOCIATION					=> 'Association',
		self::BUILDING_OWNERS_MANAGERS		=> 'Organizations that Own/Manage/Lease Buildings & Plants',
		self::SERVICE_PRODUCT_PROVIDERS		=> 'Service and Product Providers',
		self::SMALL_BUSINESSES				=> 'Small Businesses',
		self::UTILITY_ENERGY_SPONSORS 		=> 'Utilities & Energy Efficiency Program Sponsors'
	];

}
?>