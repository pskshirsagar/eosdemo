<?php

class CComplianceDocPropertyGroup extends CBaseComplianceDocPropertyGroup {

	const ALL_PROPERTY_GROUP_ID_VALUE = 100000000;

	protected $m_strBuyerLocationName;

	public function setBuyerLocationName( $strBuyerLocationName ) {
		$this->m_strBuyerLocationName = CStrings::strTrimDef( $strBuyerLocationName, 240, NULL, true );
	}

	public function getBuyerLocationName() {
		return $this->m_strBuyerLocationName;
	}

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valApPayeeId() {
		return true;
	}

	public function valComplianceDocId() {
		return true;
	}

	public function valPropertyGroupId() {
		return true;
	}

	public function valVerifiedBy() {
		return true;
	}

	public function valVerifiedOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['buyer_location_name'] ) ) {
			$this->setBuyerLocationName( $arrmixValues['buyer_location_name'] );
		}
	}

	public static function getAllPropertyGroupIdByCid( $intCid ) {
		return self::ALL_PROPERTY_GROUP_ID_VALUE + $intCid;
	}

}
?>