<?php

class CBroker extends CBaseBroker {

	const MIN_PHONE_NUMBER_SIZE	= 10;
	const MAX_PHONE_NUMBER_SIZE = 15;

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Brokerage name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContact() {
		$boolIsValid = true;

		if( true == is_null( $this->getContact() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Contact name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {

		$intPhoneNumberLength = strlen( $this->getPhoneNumber() );

		if( true == is_null( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
		} elseif( ( CWorker::MIN_PHONE_NUMBER_SIZE > $intPhoneNumberLength || CWorker::MAX_PHONE_NUMBER_SIZE < $intPhoneNumberLength ) || false == CValidation::validateFullPhoneNumber( $this->getPhoneNumber(), false ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is not valid.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valEmailAddress( $objDatabase ) {

		if( true == is_null( $this->getEmailAddress() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is required.' ) );
		} elseif( false == is_null( $this->getEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address does not appear to be valid.' ) );
		} elseif( 0 < strlen( $this->getEmailAddress() ) && true == valObj( $objDatabase, 'CDatabase' )
				&& 0 < \Psi\Eos\Utilities\CBrokers::createService()->fetchBrokerCount( ' WHERE vendor_id = ' . $this->sqlVendorId() . ' AND deleted_by IS NULL AND deleted_on IS NULL AND lower( email_address ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getEmailAddress() ) ) . '\' AND id != ' . ( int ) $this->getId(), $objDatabase ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is already being used.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;

		if( true == is_null( $this->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', 'Brokerage address is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;

		if( true == is_null( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', 'State code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {

		if( true == is_null( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code is required.' ) );
		} elseif( false == CValidation::validatePostalCode( $this->getPostalCode(), CCountry::CODE_USA ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Valid Postal code is required.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valContact();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmailAddress( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>