<?php

class CUtilityBillAccountCommodity extends CBaseUtilityBillAccountCommodity {

	protected $m_strCommodityName;
	protected $m_strAccountNumber;
	protected $m_intPropertyCommodityId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_strBillingType;

	/**
	 * GET FUNCTIONS
	 */

	public function getCommodityName() {
		return $this->m_strCommodityName;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getPropertyCommodityId() {
		return $this->m_intPropertyCommodityId;
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function getBillingType() {
		return $this->m_strBillingType;
	}

	/**
	 * SET FUNCTIONS
	 */

	public function setCommodityName( $strCommodityName ) {
		$this->m_strCommodityName = $strCommodityName;
	}

	public function setAccountNumber( $strAccountName ) {
		$this->m_strAccountNumber = $strAccountName;
	}

	public function setPropertyCommodityId( $intPropertyCommodityId ) {
		$this->m_intPropertyCommodityId = $intPropertyCommodityId;
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->m_intPropertyUtilityTypeId = $intPropertyUtilityTypeId;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['commodity_name'] ) ) {
			$this->setCommodityName( $arrmixValues['commodity_name'] );
		}

		if( true == isset( $arrmixValues['property_commodity_id'] ) ) {
			$this->setPropertyCommodityId( $arrmixValues['property_commodity_id'] );
		}

		if( true == isset( $arrmixValues['property_utility_type_id'] ) ) {
			$this->setPropertyUtilityTypeId( $arrmixValues['property_utility_type_id'] );
		}

		if( true == isset( $arrmixValues['billing_type'] ) ) {
			$this->setBillingType( $arrmixValues['billing_type'] );
		}

	}

	public function setBillingType( $strBillingType ) {
		$this->m_strBillingType = $strBillingType;
	}
}
?>