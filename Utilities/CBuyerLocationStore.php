<?php

class CBuyerLocationStore extends CBaseBuyerLocationStore {

	protected $m_strStoreName;
	protected $m_strStreetLine1;

	public function valId() {
		return true;
	}

	public function valBuyerLocationId() {
		return true;
	}

	public function valStoreId() {
		return true;
	}

	public function setStoreName( $strStoreName ) {
		$this->m_strStoreName = CStrings::strTrimDef( $strStoreName, 100, NULL, true );
	}

	public function getStoreName() {
		return $this->m_strStoreName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['store_name'] ) && $boolDirectSet ) {
			$this->m_strStreetLine1 = trim( stripcslashes( $arrmixValues['store_name'] ) );
		} elseif( isset( $arrmixValues['store_name'] ) ) {
			$this->setStoreName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['store_name'] ) : $arrmixValues['store_name'] );
		}
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>