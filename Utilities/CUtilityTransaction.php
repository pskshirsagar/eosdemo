<?php

class CUtilityTransaction extends CBaseUtilityTransaction {

	const VALIDATE_INSERT_FROM_BATCH_PROCESS = 'insert_from_batch_process';

	protected $m_strUtilityConsumptionTypeName;
	protected $m_strPropertyUtilityTypeName;
	protected $m_strUtilityBillChargeName;
	protected $m_strUtilityTypeName;
	protected $m_strName;
	protected $m_strBatchDatetime;
	protected $m_strDescription;
	protected $m_strBillDatetime;

	protected $m_intVcrUtilityBillId;
	protected $m_intUtilityExportGroupId;

	protected $m_fltUtilityChargeAmount;
	protected $m_fltTotalCollected;
	protected $m_fltBillingFeeAmount;

	protected $m_objPropertyUtilitySetting;
	protected $m_objArTransaction;
	protected $m_objBillingFeeTransaction;

	protected $m_boolHasVcrService;
	protected $m_boolIsBaseFee;
	protected $m_boolIsGroupedTransactions;

	protected $m_intLeaseId;

	protected $m_arrintPropertyMeterDeviceIds;


	/**
     * Get Functions
     */

	public function getUtilityConsumptionTypeName() {
		return $this->m_strUtilityConsumptionTypeName;
	}

	public function getPropertyUtilityTypeName() {
		return $this->m_strPropertyUtilityTypeName;
	}

	public function getUtilityTypeName() {
    	return $this->m_strUtilityTypeName;
    }

	public function getUtilityBillChargeName() {
		return $this->m_strUtilityBillChargeName;
	}

 	public function getBillingFeeAmount() {
        return $this->m_fltBillingFeeAmount;
    }

    public function getVcrUtilityBillId() {
    	return $this->m_intVcrUtilityBillId;
    }

    public function getUtilityExportGroupId() {
    	return $this->m_intUtilityExportGroupId;
    }

	public function getPropertyUtilitySetting() {
    	return $this->m_objPropertyUtilitySetting;
    }

	public function getArTransaction() {
    	return $this->m_objArTransaction;
    }

    public function getUtilityChargeAmount() {
    	return $this->m_fltUtilityChargeAmount;
    }

    public function getTotalCollected() {
    	return $this->m_fltTotalCollected;

    }

    public function getHasVcrService() {
    	return $this->m_boolHasVcrService;
    }

    public function getBillingFeeTransaction() {
    	return $this->m_objBillingFeeTransaction;
    }

    public function getIsBaseFee() {
    	return $this->m_boolIsBaseFee;
    }

    public function getBatchDatetime() {
    	return $this->m_strBatchDatetime;
    }

    public function getDescription() {
    	return $this->m_strDescription;
    }

    public function getName() {
    	return $this->m_strName;
    }

    public function getLeaseId() {
    	return $this->m_intLeaseId;
    }

    public function getBillDatetime() {
    	return $this->m_strBillDatetime;
    }

	public function getIsGroupedTransactions() {
		return $this->m_boolIsGroupedTransactions;
	}

	public function getPropertyMeterDeviceIds() {
		return $this->m_arrintPropertyMeterDeviceIds;
	}

    /**
     * Set Functions
     */

	public function setUtilityConsumptionTypeName( $strUtilityConsumptionTypeName ) {
		$this->m_strUtilityConsumptionTypeName = $strUtilityConsumptionTypeName;
	}

	public function setPropertyUtilityTypeName( $strPropertyUtilityTypeName ) {
		$this->m_strPropertyUtilityTypeName = $strPropertyUtilityTypeName;
	}

    public function setUtilityTypeName( $strUtilityTypeName ) {
    	$this->m_strUtilityTypeName = $strUtilityTypeName;
    }

	public function setUtilityBillChargeName( $strUtilityBillChargeName ) {
		$this->m_strUtilityBillChargeName = $strUtilityBillChargeName;
	}

	public function setBillingFeeAmount( $fltBillingFeeAmount ) {
        $this->m_fltBillingFeeAmount = CStrings::strToFloatDef( $fltBillingFeeAmount, NULL, false, 2 );
    }

	public function setVcrUtilityBillId( $intVcrUtilityBillId ) {
    	 $this->m_intVcrUtilityBillId = $intVcrUtilityBillId;
    }

    public function setUtilityExportGroupId( $intUtilityExportGroupId ) {
    	$this->m_intUtilityExportGroupId = $intUtilityExportGroupId;
    }

	public function setPropertyUtilitySetting( $objPropertyUtilitySetting ) {
    	$this->m_objPropertyUtilitySetting = $objPropertyUtilitySetting;
    }

	public function setArTransaction( $objArTransaction ) {
    	$this->m_objArTransaction = $objArTransaction;
    }

	public function setUtilityChargeAmount( $fltUtilityChargeAmount ) {
    	$this->m_fltUtilityChargeAmount = $fltUtilityChargeAmount;
    }

	public function setTotalCollected( $fltTotalCollected ) {
    	$this->m_fltTotalCollected = $fltTotalCollected;

    }

	public function setHasVcrService( $boolHasVcrService ) {
    	$this->m_boolHasVcrService = CStrings::strToBool( $boolHasVcrService );
    }

    public function setIsBaseFee( $boolIsBaseFee ) {
    	$this->m_boolIsBaseFee = CStrings::strToBool( $boolIsBaseFee );
    }

    public function setBatchDatetime( $strBatchDatetime ) {
    	$this->m_strBatchDatetime = $strBatchDatetime;
    }

    public function setDescription( $strDescription ) {
    	$this->m_strDescription = $strDescription;
    }

 	public function setName( $strName ) {
    	$this->m_strName = $strName;
    }

    public function setLeaseId( $intLeaseId ) {
    	$this->m_intLeaseId = $intLeaseId;
    }

	public function setBillDatetime( $strBillDatetime ) {
        $this->m_strBillDatetime = CStrings::strTrimDef( $strBillDatetime, -1, NULL, true );
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_type_name'] ) ) {
			$this->setUtilityTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_type_name'] ) : $arrmixValues['utility_type_name'] );
		}
		if( true == isset( $arrmixValues['utility_bill_charge_name'] ) ) {
			$this->setUtilityBillChargeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_charge_name'] ) : $arrmixValues['utility_bill_charge_name'] );
		}

		if( true == isset( $arrmixValues['utility_consumption_type_name'] ) ) {
			$this->setUtilityConsumptionTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_consumption_type_name'] ) : $arrmixValues['utility_consumption_type_name'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_name'] ) ) {
			$this->setPropertyUtilityTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_utility_type_name'] ) : $arrmixValues['property_utility_type_name'] );
		}
		if( true == isset( $arrmixValues['utility_provider_name'] ) ) {
			$this->setUtilityProviderName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_provider_name'] ) : $arrmixValues['utility_provider_name'] );
		}
		if( true == isset( $arrmixValues['billing_fee_amount'] ) ) {
			$this->setBillingFeeAmount( $arrmixValues['billing_fee_amount'] );
		}
		if( true == isset( $arrmixValues['vcr_utility_bill_id'] ) ) {
			$this->setVcrUtilityBillId( $arrmixValues['vcr_utility_bill_id'] );
		}

		if( true == isset( $arrmixValues['utility_charge_amount'] ) ) {
			$this->setUtilityChargeAmount( $arrmixValues['utility_charge_amount'] );
		}
		if( true == isset( $arrmixValues['total_collected'] ) ) {
			$this->setTotalCollected( $arrmixValues['total_collected'] );
		}
		if( true == isset( $arrmixValues['has_vcr_service'] ) ) {
			$this->setHasVcrService( $arrmixValues['has_vcr_service'] );
		}
		if( true == isset( $arrmixValues['batch_datetime'] ) ) {
			$this->setBatchDatetime( $arrmixValues['batch_datetime'] );
		}
		if( true == isset( $arrmixValues['utility_export_group_id'] ) ) {
			$this->setUtilityExportGroupId( $arrmixValues['utility_export_group_id'] );
		}
		if( true == isset( $arrmixValues['description'] ) ) {
			$this->setDescription( $arrmixValues['description'] );
		}
		if( true == isset( $arrmixValues['name'] ) ) {
			$this->setName( $arrmixValues['name'] );
		}
		if( true == isset( $arrmixValues['lease_id'] ) ) {
			$this->setLeaseId( $arrmixValues['lease_id'] );
		}
		if( true == isset( $arrmixValues['bill_datetime'] ) ) {
			$this->setBillDatetime( $arrmixValues['bill_datetime'] );
		}

		if( true == isset( $arrmixValues['is_grouped_transactions'] ) ) {
			$this->setIsGroupedTransactions( $arrmixValues['is_grouped_transactions'] );
		}

		if( true == isset( $arrmixValues['property_meter_device_ids'] ) ) {
			$this->setPropertyMeterDeviceIds( $arrmixValues['property_meter_device_ids'] );
		}

		return true;
	}

 	public function addBillingFeeTransaction( $objBillingFeeTransaction ) {
 		$this->m_objBillingFeeTransaction = $objBillingFeeTransaction;
 	}

	public function setIsGroupedTransactions( $boolIsGroupedTransactions ) {
		$this->m_boolIsGroupedTransactions = CStrings::strToBool( $boolIsGroupedTransactions );
	}

	public function setPropertyMeterDeviceIds( $arrintPropertyMeterDeviceIds ) {
		$this->set( 'm_arrintPropertyMeterDeviceIds', CStrings::strToArrIntDef( $arrintPropertyMeterDeviceIds, NULL ) );
	}

	/**
	 * Validate Functions
	 */

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
        }

        return $boolIsValid;
    }

    public function valUtilityAccountId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityAccountId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_account_id', 'Utility account is required.' ) );
        }

        return $boolIsValid;
    }

    public function valArCodeId( $boolIsTerminationArCode = false, $objDatabase ) {
        $boolIsValid = true;

        if( true == $boolIsTerminationArCode && true == is_null( $this->getArCodeId() ) ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Please update termination charge code on property utility setting, before adding move-out charge.' ) );
        } elseif( true == is_null( $this->getArCodeId() ) ) {
            $boolIsValid = false;

            if( false == is_null( $this->getUtilityTransactionTypeId() ) ) {
            	$strUtilityTransactionTypeName = getArrayElementByKey( $this->getUtilityTransactionTypeId(), CUtilityTransactionType::getUtilityTransactionTypeNames() );
            }

            if( false == is_null( $this->getChargeName() ) ) {
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Charge code is required for transaction type ( ' . $strUtilityTransactionTypeName . ' ) of charge name ( ' . $this->getChargeName() . ' )' ) );
            } elseif( false == is_null( $this->getUtilityTypeName() ) ) {
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Charge code is required for transaction type ( ' . $strUtilityTransactionTypeName . ' ) of property utility type ( ' . $this->getUtilityTypeName() . ' )' ) );
            } else {
            	$objPropertyUtilityType = \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchPropertyUtilityTypeById( $this->getPropertyUtilityTypeId(), $objDatabase );
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Charge code is required for transaction type ( ' . $strUtilityTransactionTypeName . ' ) of property utility type ( ' . $objPropertyUtilityType->getUtilityTypeName() . ' )' ) );
            }
        }

        return $boolIsValid;
    }

    public function valChargeName() {
        $boolIsValid = true;

        if( true == is_null( $this->getChargeName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_name', 'Charge name is required for property utility type( ' . $this->getPropertyUtilityTypeId() . ' ) .' ) );
        }

        return $boolIsValid;
    }

    public function valTransactionDatetime() {
        $boolIsValid = true;

        if( true == is_null( $this->getTransactionDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', 'Transaction datetime is required.' ) );
        }

        return $boolIsValid;
    }

    public function valOriginalAmount() {
        $boolIsValid = true;

        if( true == is_null( $this->getOriginalAmount() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_amount', 'Original amount is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCurrentAmount() {
        $boolIsValid = true;

        if( false == is_null( $this->getOriginalAmount() ) && 0 != $this->getOriginalAmount() && true == is_null( $this->getCurrentAmount() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_amount', 'Current amount is required.' ) );
        }

        return $boolIsValid;
    }

    public function valUtilityInvoiceId( $objDatabase = NULL ) {
    	$boolIsValid = true;

    	if( false == is_null( $objDatabase ) && false == is_null( $this->getUtilityInvoiceId() ) ) {

    		$objUtilityTransactionType = \Psi\Eos\Utilities\CUtilityTransactionTypes::createService()->fetchUtilityTransactionTypeById( $this->getUtilityTransactionTypeId(), $objDatabase );

    		if( true == valObj( $objUtilityTransactionType, 'CUtilityTransactionType' ) && false != $objUtilityTransactionType->getIsPsiOriginated() ) {
    			$boolIsValid = false;
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_invoice_id', 'Deletion of invoiced PSI originated transaction is not allowed.' ) );
    		}
        }

        return $boolIsValid;
    }

	public function valAmount() {
        $boolIsValid = true;

        if( true == is_null( $this->getOriginalAmount() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_amount', 'Amount is required.' ) );
        }

        return $boolIsValid;
    }

    public function valTransactionType() {
    	$boolIsValid = true;

    	if( 0 == $this->getUtilityTransactionTypeId() ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_type_id', 'Transaction Type is required' ) );
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valUtilityAccountId();
            	$boolIsValid &= $this->valArCodeId( false, $objDatabase );
            	$boolIsValid &= $this->valChargeName();
            	$boolIsValid &= $this->valTransactionDatetime();
            	$boolIsValid &= $this->valTransactionType();
            	$boolIsValid &= $this->valOriginalAmount();
            	$boolIsValid &= $this->valCurrentAmount();
            	break;

            case self::VALIDATE_INSERT_FROM_BATCH_PROCESS:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valUtilityAccountId();
            	$boolIsValid &= $this->valArCodeId( false, $objDatabase );
            	$boolIsValid &= $this->valChargeName();
            	$boolIsValid &= $this->valAmount();
            	break;

            case 'VALIDATE_SOFT_DELETE':
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valUtilityAccountId();
            	$boolIsValid &= $this->valArCodeId( false, $objDatabase );
            	$boolIsValid &= $this->valChargeName();
            	$boolIsValid &= $this->valTransactionDatetime();
            	$boolIsValid &= $this->valOriginalAmount();
            	$boolIsValid &= $this->valCurrentAmount();
            	$boolIsValid &= $this->valDeletedBy();
            	$boolIsValid &= $this->valDeletedOn();
            	$boolIsValid &= $this->valUtilityInvoiceId( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valUtilityInvoiceId( $objDatabase );
            	break;

            case 'VALIDATE_MOVE_OUT_CHARGE':
            	$boolIsValid &= $this->valArCodeId( true, $objDatabase );
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>