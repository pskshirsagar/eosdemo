<?php

use Psi\Libraries\ExternalFileUpload\CFileUpload;

class CUtilityDocument extends CBaseUtilityDocument {

	const TYPE_BILL 		            = 'bill';
	const TYPE_NOTICE 		            = 'notice';
	const TYPE_TEMPLATE 	            = 'template';
	const TYPE_WEB_RETRIEVAL 	        = 'web_retrieval';
	const TYPE_REGISTRATION_UTILITY     = 'registration_utility';
	const TYPE_DISCONNECT_NOTICE        = 'disconnect_notice';
	const TYPE_DISABLE_UEM_DOCUMENTATION     = 'disable_uem_documentation';

	protected $m_intDocumentPageCount;
	protected $m_intPsProductId;

	protected $m_objUtilityBill;

	// Setter Methods

	public function setDocumentPageCount( $intDocumentPageCount ) {
		$this->m_intDocumentPageCount = $intDocumentPageCount;
	}

	public function setUtilityBill( $objUtilityBill ) {
		$this->m_objUtilityBill = $objUtilityBill;
	}

	// Gettter Methods

	public function getDocumentPageCount() {
		return $this->m_intDocumentPageCount;
	}

	public function getUtilityBill() {
		return $this->m_objUtilityBill;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = $intPsProductId;
	}

	public function valFileExtensionId( $objAdminDatabase, $arrmixFileDetails ) {

		$boolIsValid = true;

		if( true == array_key_exists( 'upload_file', $_FILES ) && false == empty( $_FILES['upload_file']['name'] ) && true == array_key_exists( 'file_path', $arrmixFileDetails ) ) {

			$intFileExtensionId = CFileExtension::APPLICATION_PDF;

			$objFileExtension 	= \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionById( $intFileExtensionId, $objAdminDatabase );
			$boolIsValid 		= false;

			$arrstrPathInfo = pathinfo( $arrmixFileDetails['file_path'] . $_FILES['upload_file']['name'] );

			if( $objFileExtension->getExtension() == $arrstrPathInfo['extension'] ) {
				$this->setFileExtensionId( $objFileExtension->getId() );
				$boolIsValid = true;
			}

			if( false == $boolIsValid ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type_id', ' Please upload pdf file. ' ) );
			}

		} elseif( true == array_key_exists( 'file_extention_id', $arrmixFileDetails ) && true == array_key_exists( 'file_size', $arrmixFileDetails ) ) {

			if( true == valArr( $arrmixFileDetails ) ) {

				// validate for extention to upload only PDF
				if( CFileExtension::APPLICATION_PDF != $arrmixFileDetails['file_extension_id'] ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Please upload PDF files. ' ) );
				}

				// validate for size
				if( 0 == $arrmixFileDetails['file_size'] ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' File Size should be more than 0 KB. ' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( true == is_null( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'A File is required to upload.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL, $arrmixFileDetails = [] ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFileName();
				if( false == empty( $arrmixFileDetails ) ) {
					$boolIsValid &= $this->valFileExtensionId( $objAdminDatabase, $arrmixFileDetails );
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function fetchFileExtension( $objDatabase ) {

		if( false == is_null( $this->getFileName() ) ) {
			$arrstrFileParts = pathinfo( $this->getFileName() );
			if( true == valArr( $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
				return \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
			}
		}

		return false;
	}

	public function createUtilityBillDocument( $objDatabase ) {

		$objUtilityBillDocument = new CUtilityBillDocument();
		$objUtilityBillDocuments = \Psi\Eos\Utilities\CUtilityBillDocuments::createService()->fetchLastOrderNum( $objDatabase );

		$intOrderNum = ( true == valObj( $objUtilityBillDocuments, 'CUtilityBillDocument' ) ) ? ( int ) $objUtilityBillDocuments->getOrderNum() + 1 : 1;

		$objUtilityBillDocument->setOrderNum( $intOrderNum );

		return $objUtilityBillDocument;
	}

 	public function insert( $intCurrentUserId, $objDatabase, $intCid = NULL ) {

 		if( true == isset( $_FILES ) && true == array_key_exists( 'utility_document_temp', $_FILES ) ) {
 			if( false == $this->uploadFiles( $intCid ) ) {
    			return false;
 			}
    	}

    	return parent::insert( $intCurrentUserId, $objDatabase );
   	}

 	public function uploadFiles( $intCid ) {

		if( false == is_dir( $this->getFullFilePath( $intCid ) ) && false == CFileIo::recursiveMakeDir( $this->getFullFilePath( $intCid ) ) ) {
			trigger_error( 'Couldn\'t create directory(' . $this->getFullFilePath( $intCid ) . ' ). ', E_USER_ERROR );
			return false;
		}

		$objFileUpload = new CFileUpload();
   		$objFileUpload->setAcceptableTypes( [ 'application/pdf' ] );
		$objFileUpload->setOverwriteMode( CFileUpload::MODE_OVERWRITE );

		if( false == is_string( $objFileUpload->upload( 'utility_document_temp', $this->getFullFilePath( $intCid ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Invalid file specified to upload.' ) );
    		return false;
    	}

    	return true;
   	}

	public function setFilePath( $strFilePath, $strDocumentType = NULL ) {

		if( false == is_null( $strDocumentType ) ) {

			$strDocumentType = \Psi\CStringService::singleton()->strtoupper( $strDocumentType );

			if( \Psi\CStringService::singleton()->strtoupper( self::TYPE_BILL ) == $strDocumentType ) {
				$strFilePath .= '/' . date( 'Y' ) . '/' . date( 'm' ) . '/';
			} else {
				$strFilePath = $strFilePath . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/';
			}

			eval( '$strFullUri = self::TYPE_' . $strDocumentType . ';' );

			$strFilePath = $strFullUri . '/' . $strFilePath;
		}

		parent::setFilePath( $strFilePath );
   	}

   	public function getFullFilePath( $intCid = NULL ) {

   		if( true == is_numeric( $intCid ) ) {
   			return getMountsPath( $intCid, PATH_MOUNTS_UTILITY_DOCUMENTS ) . $this->getFilePath();
   		}

	    return PATH_MOUNTS_GLOBAL_UTILITY_DOCUMENTS . $this->getFilePath();

    }

}
?>