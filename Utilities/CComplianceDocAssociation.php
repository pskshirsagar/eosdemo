<?php

class CComplianceDocAssociation extends CBaseComplianceDocAssociation {

	protected $m_intDeniedBy;

	protected $m_strDeniedOn;

	protected $m_strEntityName;

	protected $m_boolIsVendorDoc;

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valComplianceDocId() {
		return true;
	}

	public function valComplianceLevelId() {
		return true;
	}

	public function valEntrataReferenceId() {
		return true;
	}

	public function valVendorReferenceId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getEntityName() {
		return $this->m_strEntityName;
	}

	public function setEntityName( $strEntityName ) {
		$this->m_strEntityName = CStrings::strTrimDef( $strEntityName, 240, NULL, true );
	}

	public function getIsVendorDoc() {
		return $this->m_boolIsVendorDoc;
	}

	public function setIsVendorDoc( $boolIsVendorDoc ) {
		$this->m_boolIsVendorDoc = CStrings::strToBool( $boolIsVendorDoc );
	}

	public function setDeniedBy( $intDeniedBy ) {
		$this->set( 'm_intDeniedBy', CStrings::strToIntDef( $intDeniedBy, NULL, false ) );
	}

	public function getDeniedBy() {
		return $this->m_intDeniedBy;
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->set( 'm_strDeniedOn', CStrings::strTrimDef( $strDeniedOn, -1, NULL, true ) );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['entity_name'] ) && $boolDirectSet ) {
			$this->m_strEntityName = trim( stripcslashes( $arrmixValues['entity_name'] ) );
		} elseif( isset( $arrmixValues['entity_name'] ) ) {
			$this->setEntityName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['entity_name'] ) : $arrmixValues['entity_name'] );
		}

		if( isset( $arrmixValues['is_vendor_doc'] ) && $boolDirectSet ) {
			$this->m_boolIsVendorDoc = trim( stripcslashes( $arrmixValues['is_vendor_doc'] ) );
		} elseif( isset( $arrmixValues['is_vendor_doc'] ) ) {
			$this->setIsVendorDoc( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_vendor_doc'] ) : $arrmixValues['is_vendor_doc'] );
		}

		if( isset( $arrmixValues['denied_by'] ) && $boolDirectSet ) {
			$this->set( 'm_intDeniedBy', trim( $arrmixValues['denied_by'] ) );
		} elseif( isset( $arrmixValues['denied_by'] ) ) {
			$this->setDeniedBy( $arrmixValues['denied_by'] );
		}

		if( isset( $arrmixValues['denied_on'] ) && $boolDirectSet ) {
			$this->set( 'm_strDeniedOn', trim( $arrmixValues['denied_on'] ) );
		} elseif( isset( $arrmixValues['denied_on'] ) ) {
			$this->setDeniedOn( $arrmixValues['denied_on'] );
		}
	}

}
?>