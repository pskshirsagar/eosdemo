<?php

class CBuyerVendorEntity extends CBaseBuyerVendorEntity {

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valBuyerId() {
		return true;
	}

	public function valVendorEntityId() {
		return true;
	}

	public function valApLegalEntityId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>