<?php

class CUtilityBillAuditType extends CBaseUtilityBillAuditType {

	const SERVICE_PERIOD_OVERLAP	= 1;

	/**
	 * For now we are setting value as 0.
	 * Once new audit added, we need to set the actual values.
	 */
	const NEW_LINE_ITEM				= 0;
	const MISSING_LINE_ITEM			= 0;
	const SERVICE_PERIOD_GAP		= 0;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getUtilityBillAuditTemplateNames( $intUtilityBillAuditTypeId = NULL, $intUtilityBillAuditStatusTypeId = CUtilityBillAuditStatusType::NEW ) {

		$strTplName = 'edit_';

		if( CUtilityBillAuditStatusType::COMPLETE == $intUtilityBillAuditStatusTypeId ) {
			$strTplName = 'view_';
		}

		$arrstrTemplateNames = [
			self::SERVICE_PERIOD_OVERLAP => $strTplName . 'service_period_overlap_audit.tpl'
		];

		if( true == valId( $intUtilityBillAuditTypeId ) ) {
			return $arrstrTemplateNames[$intUtilityBillAuditTypeId];
		}

		return $arrstrTemplateNames;
	}

}
?>