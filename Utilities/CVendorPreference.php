<?php

class CVendorPreference extends CBaseVendorPreference {
	const ACCOUNT_ACTIVITY_DIGEST_EMAIL_FREQUENCY 				= 'ACCOUNT_ACTIVITY_DIGEST_EMAIL_FREQUENCY';
	const INSURANCE_POLICIES_EXPIRATION_NOTIFICATION_FREQUENCY	= 'INSURANCE_POLICIES_EXPIRATION_NOTIFICATION_FREQUENCY';

	const DAILY		= 1;
	const WEEKLY	= 7;
	const MONTHLY	= 30;

	const THIRTY_DAYS	= 30;
	const SIXTY_DAYS	= 60;
	const NINTY_DAYS	= 90;

	private $m_arrstrInsurancePoliciesExpires;
	private $m_arrstrActivityDigestEmailFrequencies;

	private $m_strNoNotice;

	public static $c_arrstrVendorPreferenceKeys = [
		self::INSURANCE_POLICIES_EXPIRATION_NOTIFICATION_FREQUENCY
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getActivityDigestEmailFrequencies() {
		if( true == valArr( $this->m_arrstrActivityDigestEmailFrequencies ) ) {
			return $this->m_arrstrActivityDigestEmailFrequencies;
		}

		$this->m_arrstrActivityDigestEmailFrequencies = [
			self::DAILY		=> __( 'Daily' ),
			self::WEEKLY	=> __( 'Weekly' ),
			self::MONTHLY	=> __( 'Monthly' )
		];

		return $this->m_arrstrActivityDigestEmailFrequencies;
	}

	public function getInsurancePoliciesExpires() {
		if( true == valArr( $this->m_arrstrInsurancePoliciesExpires ) ) {
			return $this->m_arrstrInsurancePoliciesExpires;
		}

		$this->m_arrstrInsurancePoliciesExpires = [
			self::THIRTY_DAYS	=> __( '{%d, 0} days', [ 30 ] ),
			self::SIXTY_DAYS	=> __( '{%d, 0} days', [ 60 ] ),
			self::NINTY_DAYS	=> __( '{%d, 0} days', [ 90 ] ),
		];

		return $this->m_arrstrInsurancePoliciesExpires;
	}

	public function getstrNoNotice() {
		if( true == valStr( $this->m_strNoNotice ) ) {
			return $this->m_strNoNotice;
		}

		return $this->m_strNoNotice = __( 'No notice' );
	}

	public function valId() {
		return true;
	}

	public function valVendorId() {
		return true;
	}

	public function valKey() {
		return true;
	}

	public function valValue() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>