<?php

use function Psi\Libraries\UtilFunctions\{valStr, valArr, count};

class CVendorCatalog extends CBaseVendorCatalog {

	const VENDOR_CATALOG_PUNCHOUT_CXML_KEY = 'cxml';

	const PUNCHOUT_SETUP_REQUEST_URL_LABEL = 'Catalog URL';
	const IDENTITY_LABEL                   = 'Catalog Identity';
	const SHARED_SECRET_LABEL              = 'Catalog SharedSecret';
	const ORDER_REQUEST_URL_LABEL          = 'Catalog OrderRequest URL';

	const PUNCHOUT_SETUP_REQUEST_URL = 'PunchoutSetupRequest URL';
	const IDENTITY                   = 'Identity';
	const SHARED_SECRET              = 'SharedSecret';
	const ORDER_REQUEST_URL          = 'OrderRequest URL';
	const TO_CREDENTIAL_IDENTITY     = 'To Credential Identity';
	const TO_CREDENTIAL_DOMAIN     = 'To Credential Domain';
	const FROM_CREDENTIAL_DOMAIN     = 'From Credential Domain';
	const SENDER_CREDENTIAL_DOMAIN     = 'Sender Credential Domain';
	const DUNS                         = 'DUNS';
	const NETWORK_ID                   = 'NetworkID';

	public static $c_arrstrVendorCatalogPunchoutKeys = [
		self::IDENTITY                   => self::IDENTITY_LABEL,
		self::SHARED_SECRET              => self::SHARED_SECRET_LABEL,
		self::PUNCHOUT_SETUP_REQUEST_URL => self::PUNCHOUT_SETUP_REQUEST_URL_LABEL,
		self::ORDER_REQUEST_URL          => self::ORDER_REQUEST_URL_LABEL,
		self::TO_CREDENTIAL_IDENTITY     => self::TO_CREDENTIAL_IDENTITY,
	];

	public static $c_arrstrUrlKeys = [
		self::PUNCHOUT_SETUP_REQUEST_URL => self::PUNCHOUT_SETUP_REQUEST_URL_LABEL,
		self::ORDER_REQUEST_URL => self::ORDER_REQUEST_URL_LABEL
	];

	public static $c_arrstrPunchoutConfigurationKeys = [
		self::TO_CREDENTIAL_IDENTITY => self::TO_CREDENTIAL_IDENTITY,
		self::TO_CREDENTIAL_DOMAIN => self::TO_CREDENTIAL_DOMAIN,
		self::FROM_CREDENTIAL_DOMAIN => self::FROM_CREDENTIAL_DOMAIN,
		self::SENDER_CREDENTIAL_DOMAIN => self::SENDER_CREDENTIAL_DOMAIN
	];

	public static $c_arrstrToCredentialDomainValues = [
		self::DUNS => self::DUNS,
		self::NETWORK_ID => self::NETWORK_ID
	];

	public static $c_arrstrAllVendorCatalogPunchoutKeys = [
		self::IDENTITY                   => self::IDENTITY_LABEL,
		self::SHARED_SECRET              => self::SHARED_SECRET_LABEL,
		self::PUNCHOUT_SETUP_REQUEST_URL => self::PUNCHOUT_SETUP_REQUEST_URL_LABEL,
		self::ORDER_REQUEST_URL          => self::ORDER_REQUEST_URL_LABEL,
		self::TO_CREDENTIAL_IDENTITY     => self::TO_CREDENTIAL_IDENTITY,
		self::TO_CREDENTIAL_DOMAIN       => self::TO_CREDENTIAL_DOMAIN,
	];

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valVendorId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBuyerAccountId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valVendorCatalogTypeId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDisabledBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDisabledOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid    = true;
		$strInValidKeys = NULL;
		$arrstrMissingRequiredFields = [];

		if( true == valArr( $arrmixDetails = json_decode( json_encode( $this->getDetails() ), true ) )
			&& true == isset( $arrmixDetails[self::VENDOR_CATALOG_PUNCHOUT_CXML_KEY] ) ) {

			foreach( $arrmixDetails[self::VENDOR_CATALOG_PUNCHOUT_CXML_KEY] as $strKey => $strValue ) {
				if( false == valStr( $strValue ) && true == isset( self::$c_arrstrAllVendorCatalogPunchoutKeys[$strKey] ) ) {
					$arrstrMissingRequiredFields[self::$c_arrstrAllVendorCatalogPunchoutKeys[$strKey]] = self::$c_arrstrAllVendorCatalogPunchoutKeys[$strKey];
					$boolIsValid &= false;
				}

				// check the key is of type URL then validate the URL format
				if( true == valStr( $strValue ) && true == valArr( preg_grep( '/' . $strKey . '/i', array_keys( self::$c_arrstrUrlKeys ) ) ) && false == CValidation::checkUrl( $strValue ) ) {
					$boolIsValid    &= false; echo
					$strInValidKeys .= self::$c_arrstrUrlKeys[$strKey] . ', ';
				}
			}

			if( true == valArr( $arrstrMissingRequiredFields ) ) {
				$strRequiredFields = implode( ', ', $arrstrMissingRequiredFields );
				$strMessage = ( 1 < count( $arrstrMissingRequiredFields ) ) ? ' are ' : ' is ';
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( ' {%s, 0} {%s, 1} required.', [ $strRequiredFields, $strMessage ] ) ) );
			}

			if( false == $boolIsValid && true == valStr( $strInValidKeys ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'A valid URL is required for {%s, 0}.', [ rtrim( $strInValidKeys, ', ' ) ] ) ) );
				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDetails();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>