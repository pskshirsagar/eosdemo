<?php

class CUtilityCredentialQuestionAnswer extends CBaseUtilityCredentialQuestionAnswer {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityCredentialId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuestion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAnswer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>