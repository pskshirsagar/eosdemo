<?php

class CVendorEntity extends CBaseVendorEntity {

	const TAX_ID_NUMBER_FORMAT_ONE = '000-00-0000';
	const TAX_ID_NUMBER_FORMAT_TWO = '00-0000000';

	const VA_ACCOUNT_VERIFICATION_TYPE_ID = 1;
	const VA_LEGAL_ENTITY_VERIFICATION_TYPE_ID = 2;

	const VA_ACCOUNT_VERIFICATION_TYPE = 'VA Account';
	const VA_LEGAL_ENTITY_VERIFICATION_TYPE = 'Legal Entity';

	protected $m_intComplianceDocId;

	protected $m_strCity;
	protected $m_strFileName;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strCompanyName;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strComplianceDocId;

	/**
	 * Get Functions
	 */

	public function getTaxNumber() {
		if( false == valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function getTaxNumberMasked() {
		if( CWorker::MAX_TAX_NUMBER == strlen( $this->getTaxNumber() ) ) {
			return $this->getTaxNumberEncrypted();
		}

		return CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $this->getTaxNumber() ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function getComplianceDocId() {
		return $this->m_intComplianceDocId;
	}

	/**
	 * Set Functions
	 */

	public function setTaxNumber( $strPlainTaxNumber ) {

		if( false == valStr( $strPlainTaxNumber ) || true == \Psi\CStringService::singleton()->stristr( $strPlainTaxNumber, '*' ) ) {
			return;
		}

		$this->setTaxNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );

		$this->setTaxNumberHash( $strPlainTaxNumber );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['tax_number'] ) ) {
			$this->setTaxNumber( $arrmixValues['tax_number'] );
		}

		if( isset( $arrmixValues['company_name'] ) && $boolDirectSet ) {
			$this->m_strCompanyName = trim( stripcslashes( $arrmixValues['company_name'] ) );
		} elseif( isset( $arrmixValues['company_name'] ) ) {
			$this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['company_name'] ) : $arrmixValues['company_name'] );
		}

		if( isset( $arrmixValues['street_line1'] ) && $boolDirectSet ) {
			$this->m_strStreetLine1 = trim( stripcslashes( $arrmixValues['street_line1'] ) );
		} elseif( isset( $arrmixValues['street_line1'] ) ) {
			$this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['street_line1'] ) : $arrmixValues['street_line1'] );
		}

		if( isset( $arrmixValues['street_line2'] ) && $boolDirectSet ) {
			$this->m_strStreetLine2 = trim( stripcslashes( $arrmixValues['street_line2'] ) );
		} elseif( isset( $arrmixValues['street_line2'] ) ) {
			$this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['street_line2'] ) : $arrmixValues['street_line2'] );
		}

		if( isset( $arrmixValues['street_line3'] ) && $boolDirectSet ) {
			$this->m_strStreetLine3 = trim( stripcslashes( $arrmixValues['street_line3'] ) );
		} elseif( isset( $arrmixValues['street_line3'] ) ) {
			$this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['street_line3'] ) : $arrmixValues['street_line3'] );
		}

		if( isset( $arrmixValues['city'] ) && $boolDirectSet ) {
			$this->m_strCity = trim( stripcslashes( $arrmixValues['city'] ) );
		} elseif( isset( $arrmixValues['city'] ) ) {
			$this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['city'] ) : $arrmixValues['city'] );
		}

		if( isset( $arrmixValues['state_code'] ) && $boolDirectSet ) {
			$this->m_strStateCode = trim( stripcslashes( $arrmixValues['state_code'] ) );
		} elseif( isset( $arrmixValues['state_code'] ) ) {
			$this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['state_code'] ) : $arrmixValues['state_code'] );
		}

		if( isset( $arrmixValues['postal_code'] ) && $boolDirectSet ) {
			$this->m_strPostalCode = trim( stripcslashes( $arrmixValues['postal_code'] ) );
		} elseif( isset( $arrmixValues['postal_code'] ) ) {
			$this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['postal_code'] ) : $arrmixValues['postal_code'] );
		}

		if( isset( $arrmixValues['file_name'] ) && $boolDirectSet ) {
			$this->m_strFileName = trim( stripcslashes( $arrmixValues['file_name'] ) );
		} elseif( isset( $arrmixValues['file_name'] ) ) {
			$this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['file_name'] ) : $arrmixValues['file_name'] );
		}

		if( isset( $arrmixValues['compliance_doc_id'] ) && $boolDirectSet ) {
			$this->m_strComplianceDocId = trim( stripcslashes( $arrmixValues['compliance_doc_id'] ) );
		} elseif( isset( $arrmixValues['compliance_doc_id'] ) ) {
			$this->setComplianceDocId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['compliance_doc_id'] ) : $arrmixValues['compliance_doc_id'] );
		}
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = CStrings::strTrimDef( $strCompanyName, 40, NULL, true );
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = CStrings::strTrimDef( $strStreetLine1, 100, NULL, true );
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = CStrings::strTrimDef( $strStreetLine2, 100, NULL, true );
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->m_strStreetLine3 = CStrings::strTrimDef( $strStreetLine3, 100, NULL, true );
	}

	public function setCity( $strCity ) {
		$this->m_strCity = CStrings::strTrimDef( $strCity, 50, NULL, true );
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = CStrings::strTrimDef( $strStateCode, 2, NULL, true );
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = CStrings::strTrimDef( $strPostalCode, 20, NULL, true );
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = CStrings::strTrimDef( $strFileName, 240, NULL, true );
	}

	public function setComplianceDocId( $intComplianceDocId ) {
		$this->m_intComplianceDocId = ( int ) $intComplianceDocId;
	}

	public function setTaxNumberHash( $strPlainTaxNumber ) {
		$intPlainTaxNumber = Psi\Libraries\UtilStrings\CStrings::createService()->strTrimDef( str_replace( '-', '', $strPlainTaxNumber ) );

		if( true == is_numeric( $intPlainTaxNumber ) ) {
			parent::setTaxNumberHash( \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $intPlainTaxNumber, CONFIG_SODIUM_KEY_BLIND_INDEX ) );
		} else {
			parent::setTaxNumberHash( $intPlainTaxNumber );
		}
	}

	/**
	 * Validate Methods
	 */

	public function valId( $objDatabase ) {
		$boolIsValid = true;
			if( true == valId( $this->getId() ) ) {

				$arrintCount = \Psi\Eos\Utilities\CVendorEntities::createService()->fetchVendorEntityAssociationCountByIdByVendorId( $this->getId(), $this->getVendorId(), $objDatabase );

				if( 0 != $arrintCount[0]['count'] ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_id', __( 'You can not delete this entity as it is associated with location(s).' ) ) );
				}
			}
		return $boolIsValid;
	}

	public function valDefaultOwnerTypeId( $objUtilitiesDatabase ) {

		$boolIsValid = true;

		if( 0 == $this->m_intDefaultOwnerTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_owner_type_id', __( 'Entity type is required.' ) ) );
		} else {

			$objDefaultOwnerType = \Psi\Eos\Utilities\CDefaultOwnerTypes::createService()->fetchDefaultOwnerTypeById( $this->m_intDefaultOwnerTypeId, $objUtilitiesDatabase );

			if( false == valObj( $objDefaultOwnerType, 'CDefaultOwnerType' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_owner_type_id', __( 'Entity owner type is provided.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valTaxNumberEncrypted( $intCid, $objDatabase ) {

		$boolIsValid = true;

		$strDecryptedTaxNumber = $this->getTaxNumber();

		if( CVendorEntity::TAX_ID_NUMBER_FORMAT_ONE == $strDecryptedTaxNumber || CVendorEntity::TAX_ID_NUMBER_FORMAT_TWO == $strDecryptedTaxNumber ) {
			return $boolIsValid;
		}

		if( true == valStr( $strDecryptedTaxNumber ) ) {
			if( false == preg_match( '/^[\d\-]+$/', $strDecryptedTaxNumber ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax number should be in XX-XXXXXXX or XXX-XX-XXXX and should be numeric.' ) ) );
			} elseif( false == preg_match( '/^([\d]{2}-[\d]{7})$|^([\d]{3}-[\d]{2}-[\d]{4})$/', $strDecryptedTaxNumber ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax number should be in XX-XXXXXXX or XXX-XX-XXXX.' ) ) );
			} elseif( 0 < \Psi\Eos\Utilities\CVendorEntities::createService()->fetchVendorEntityCountByIdByCidByTaxNumberHash( $this->getId(), $intCid, $this->getTaxNumberHash(), $objDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax number is already exists.' ) ) );
			}
		} else {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'tax_number_encrypted', __( 'Tax number is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valEntityName() {
		$boolIsValid = true;
		if( true == is_null( $this->getEntityName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'entity_name', __( 'Entity name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $intCid, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_create_account':
				$boolIsValid &= $this->valDefaultOwnerTypeId( $objDatabase );
				$boolIsValid &= $this->valTaxNumberEncrypted( $intCid, $objDatabase );
				$boolIsValid &= $this->valEntityName();
				break;

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEntityName();
				$boolIsValid &= $this->valTaxNumberEncrypted( $intCid, $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId( $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function isSynced( $objDatabase ) {

		$boolIsSynced = false;
		$strWhere = ' WHERE vendor_entity_id = ' . $this->getId() . ' AND vendor_id = ' . $this->getVendorId();
		$intRowCount = \Psi\Eos\Utilities\CBuyerVendorEntities::createService()->fetchBuyerVendorEntityCount( $strWhere, $objDatabase );

		if( 1 <= $intRowCount ) {
			$boolIsSynced = true;
		}

		return $boolIsSynced;
	}

	/**
	 * Create Functions
	 */

	public function createBuyer() {
		$objBuyer = new CBuyer();
		$objBuyer->setVendorId( $this->getVendorId() );
		return $objBuyer;
	}

	public function createBuyerVendorEntity() {
		$objBuyerVendorEntity = new CBuyerVendorEntity();
		$objBuyerVendorEntity->setVendorId( $this->getVendorId() );
		$objBuyerVendorEntity->setVendorEntityId( $this->getId() );
		return $objBuyerVendorEntity;
	}

}
?>