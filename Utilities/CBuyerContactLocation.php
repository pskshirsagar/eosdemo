<?php

class CBuyerContactLocation extends CBaseBuyerContactLocation {

	protected $m_intBuyerId;

	public function valId() {
		return true;
	}

	public function valBuyerContactId() {
		return true;
	}

	public function valBuyerLocationId() {
		return true;
	}

	public function valIsPrimary() {
		return true;
	}

	public function setBuyerId( $intBuyerId ) {
		$this->m_intBuyerId = $intBuyerId;
	}

	public function getBuyerId() {
		return $this->m_intBuyerId;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['buyer_id'] ) ) {
			$this->m_intBuyerId = $arrmixValues['buyer_id'];
		}
	}

}
?>