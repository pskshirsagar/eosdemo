<?php

class CPropertyMeterDevice extends CBasePropertyMeterDevice {

	protected $m_strUtilityTypeName;
	protected $m_strPropertyUnitName;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_type_name'] ) ) {
			$this->setUtilityTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_type_name'] ) : $arrmixValues['utility_type_name'] );
		}
		if( true == isset( $arrmixValues['property_unit_name'] ) ) {
			$this->setPropertyUnitName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_unit_name'] ) : $arrmixValues['property_unit_name'] );
		}
	}

	public function getUtilityTypeName() {
		return $this->m_strUtilityTypeName;
	}

	public function getPropertyUnitName() {
		return $this->m_strPropertyUnitName;
	}

	public function setUtilityTypeName( $strUtilityTypeName ) {
		$this->m_strUtilityTypeName = $strUtilityTypeName;
	}

	public function setPropertyUnitName( $strPropertyUnitName ) {
		$this->m_strPropertyUnitName = $strPropertyUnitName;
	}

    public function valUtilityTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getUtilityTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_type_id', 'Utility type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyUnitId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyUnitId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', 'Property unit is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDeviceNumber() {
        $boolIsValid = true;

        if( true == is_null( $this->getDeviceNumber() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'device_number', 'Device number is required.' ) );
        } elseif( false == preg_match( '/^[a-zA-Z0-9- ]*$/', $this->getDeviceNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'device_number', 'Only alphanumerics are allow in device number.' ) );
        }

        return $boolIsValid;
    }

    public function valUnitNumber() {
        $boolIsValid = true;

        if( true == is_null( $this->getUnitNumber() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_number', 'Unit number is required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objUtilityDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valPropertyUnitId();
            	$boolIsValid &= $this->valUtilityTypeId();
            	$boolIsValid &= $this->valDeviceNumber();
            	$boolIsValid &= $this->valUnitNumber();
            	$boolIsValid &= $this->checkDuplicateUtilityTypeIdWithPropertyUnitId( $objUtilityDatabase );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function checkDuplicateUtilityTypeIdWithPropertyUnitId( $objUtilityDatabase ) {
		if( true == is_null( $this->getUtilityTypeId() ) || true == is_null( $this->getPropertyUnitId() ) || true == is_null( $objUtilityDatabase ) ) {
			return true;
		}

		$boolIsValid = true;

		$strWhereSql = ' WHERE id <> ' . ( int ) $this->getId() . 'AND property_id = ' . ( int ) $this->getPropertyId() . ' AND utility_type_id=' . ( int ) $this->getUtilityTypeId() . ' AND property_unit_id=' . ( int ) $this->getPropertyUnitId();

		$intPropertyMeterDeviceCount = \Psi\Eos\Utilities\CPropertyMeterDevices::createService()->fetchPropertyMeterDeviceCount( $strWhereSql, $objUtilityDatabase );

		if( 0 < $intPropertyMeterDeviceCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_type_id', 'Meter for utility type ' . $this->getUtilityTypeName() . ' on unit ' . $this->getPropertyUnitName() . ' is already exists.' ) );
		}

		return $boolIsValid;
	}

}
?>