<?php

class CUtilityAccount extends CBaseUtilityAccount {

	const VALIDATE_TEXT_PATTERN = '/^[a-zA-Z]*$/';
	const VALIDATE_NUMBER_PATTERN = '/^[0-9]*$/';

	protected $m_objCustomer;
	protected $m_objLease;
	protected $m_objProperty;
	protected $m_objMoveInFeeUtilityTransaction;
	protected $m_objTerminationFeeUtilityTransaction;
	protected $m_objDepositGlAccount;
	protected $m_objRentGlAccount;

	protected $m_arrobjUtilityTransactions;
	protected $m_arrobjUtilityAccountPhoneNumbers;
	protected $m_arrobjUtilityPropertyUnits;
	protected $m_arrobjPropertyUtilityTypeRates;
	protected $m_arrobjMoveInFeeUtilityTransactions;
	protected $m_arrobjVcrUtilityTransactions;
	protected $m_arrobjTerminationFeeUtilityTransactions;
	protected $m_arrobjScheduledCharges;
	protected $m_arrobjLeaseCustomers;
	protected $m_arrobjLeaseIntervals;

	protected $m_arrmixPropertyUtilityTypeDetails;
	protected $m_arrmixPropertyUtilityTypeRateDetails;

	protected $m_intSquareFeet;
	protected $m_intUnitSpaceId;
	protected $m_intPropertyUtilityTypeRateId;
	protected $m_intPropertyUtilityTypeId;
	protected $m_intNumberOfBedRooms;
	protected $m_intUnitTypeId;
	protected $m_arrintTotalDays;
	protected $m_intIntegrationClientTypeId;

	protected $m_fltPreviousBalance = 0.0;
	protected $m_fltTotalAmount = 0.0;

	protected $m_intUtilityBatchId;

	protected $m_strUnitNumber;
	protected $m_strPropertyUtilityTypeName;
	protected $m_strUnitStatus;
	protected $m_strExceptionDetails;

	protected $m_arrstrIsEligible;
	protected $m_arrstrCoveredBySubsidies;
	protected $m_arrstrIsUtilityExcludeFromCalculations;
	protected $m_arrstrIsUtilityExcludeFromUsageFactor;

	protected $m_boolIsSelected;
	protected $m_boolIsLeaseIdChange;

	protected $m_boolIsFirstUtilityBill;
	protected $m_arrstrIsInUtilityConsumptionPeriod;

	protected $m_boolIsPropertyUtilityOverlappedConsumption;
	protected $m_arrboolIsUtilityFullCharges;

	public function __construct() {
		parent::__construct();
		$this->m_boolIsPropertyUtilityOverlappedConsumption = [];
		$this->m_arrboolIsUtilityFullCharges = $this->m_arrstrIsEligible = [];
		$this->m_boolIsLeaseIdChange = false;
	}

	/**
	 * Create Functions
	 */

	public function createUtilityChangeRequest() {

		$objUtilityChangeRequest = new CUtilityChangeRequest();
		$objUtilityChangeRequest->setCid( $this->getCid() );
		$objUtilityChangeRequest->setPropertyId( $this->getPropertyId() );
		$objUtilityChangeRequest->setUtilityAccountId( $this->getId() );
		$objUtilityChangeRequest->setIsClientRequest( true );

		return $objUtilityChangeRequest;
	}

	public function createUtilityAccountPhoneNumber() {

		$objUtilityAccountPhoneNumber = new CUtilityAccountPhoneNumber();
		$objUtilityAccountPhoneNumber->setCid( $this->getCid() );
		$objUtilityAccountPhoneNumber->setUtilityAccountId( $this->getId() );

		return $objUtilityAccountPhoneNumber;
	}

	public function createUtilityPropertyUnit() {

		$objUtilityPropertyUnit = new CUtilityPropertyUnit();
		$objUtilityPropertyUnit->setCid( $this->getCid() );
		$objUtilityPropertyUnit->setPropertyId( $this->getPropertyId() );
		$objUtilityPropertyUnit->setPropertyUnitId( $this->getPropertyUnitId() );
		$objUtilityPropertyUnit->setUnitSpaceId( $this->getUnitSpaceId() );

		return $objUtilityPropertyUnit;
	}

	public function createUtilityAccountAddress() {

		$objUtilityAccountAddress = new CUtilityAccountAddress();
		$objUtilityAccountAddress->setCid( $this->getCid() );
		$objUtilityAccountAddress->setUtilityAccountId( $this->getId() );

		return $objUtilityAccountAddress;
	}

	public function createUtilityAccountNote() {

		$objUtilityAccountNote = new CUtilityAccountNote();
		$objUtilityAccountNote->setCid( $this->getCid() );
		$objUtilityAccountNote->setUtilityAccountId( $this->getId() );
		$objUtilityAccountNote->setNoteDatetime( 'NOW()' );

		return $objUtilityAccountNote;
	}

	public function createUtilityAccountRecurringCharge() {

		$objUtilityAccountRecurringCharge = new CUtilityAccountRecurringCharge();
		$objUtilityAccountRecurringCharge->setDefaults();
		$objUtilityAccountRecurringCharge->setCid( $this->getCid() );
		$objUtilityAccountRecurringCharge->setUtilityAccountId( $this->getId() );

		return $objUtilityAccountRecurringCharge;
	}

	public function createUtilityInvoice( $arrobjUtilityTransactions = NULL ) {

		$objUtilityInvoice = new CUtilityInvoice();
		$objUtilityInvoice->setCid( $this->getCid() );
		$objUtilityInvoice->setPropertyId( $this->getPropertyId() );
		$objUtilityInvoice->setPropertyUnitId( $this->getPropertyUnitId() );
		$objUtilityInvoice->setUtilityAccountId( $this->getId() );

		if( true == valArr( $arrobjUtilityTransactions ) ) {
			$objUtilityInvoice->setUtilityTransactions( $arrobjUtilityTransactions );
		}

		return $objUtilityInvoice;
	}

	public function createUtilityTransaction( $objArCode = NULL ) {

		$objUtilityTransaction = new CUtilityTransaction();
		$objUtilityTransaction->setCid( $this->getCid() );
		$objUtilityTransaction->setPropertyId( $this->getPropertyId() );
		$objUtilityTransaction->setUtilityAccountId( $this->getId() );
		// TODO ::
		$objUtilityTransaction->setTransactionDatetime( 'NOW()' );

		if( true == valObj( $objArCode, 'CArCode' ) ) {
			$objUtilityTransaction->setArCodeId( $objArCode->getId() );
			$objUtilityTransaction->setChargeName( $objArCode->getName() );
			$objUtilityTransaction->setChargeDescription( $objArCode->getDescription() );
		}

		return $objUtilityTransaction;
	}

	public function createPropertyCapSubsidy() {

		$objPropertyCapSubsidy = new CPropertyCapSubsidy();
		$objPropertyCapSubsidy->setCid( $this->getCid() );
		$objPropertyCapSubsidy->setPropertyId( $this->getPropertyId() );
		$objPropertyCapSubsidy->setUtilityAccountId( $this->getId() );

		return $objPropertyCapSubsidy;
	}

	public function createArTransaction( $objUtilityTransaction, $objClientDatabase ) {

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$objArTransaction = new CArTransaction();
		$objArTransaction->setCid( $this->getCid() );
		$objArTransaction->setPropertyId( $this->getPropertyId() );
		$objArTransaction->setLeaseId( $this->getLeaseId() );
		$objArTransaction->setArCodeId( $objUtilityTransaction->getArCodeId() );
		$objArTransaction->setScheduledChargeId( $objUtilityTransaction->getScheduledChargeId() );
		$objArTransaction->setPostDate( date( 'm/d/Y h:i:s', time() ) );
		$objArTransaction->setReportingPostDate( date( 'm/d/Y h:i:s', time() ) );
		$objArTransaction->setTransactionAmount( $objUtilityTransaction->getCurrentAmount() );
		$objArTransaction->setMemo( $objUtilityTransaction->getChargeDescription() );
		$objArTransaction->setPostMonth( $this->getOrFetchPostMonth( $objUtilityTransaction, $objClientDatabase ) );
		$objArTransaction->setOverridePostMonthPermission( true );

		return $objArTransaction;
	}

	public function createUtilityBatchAccount() {

		$objUtilityBatchAccount = new CUtilityBatchAccount();
		$objUtilityBatchAccount->setCid( $this->getCid() );
		$objUtilityBatchAccount->setPropertyId( $this->getPropertyId() );
		$objUtilityBatchAccount->setUtilityAccountId( $this->getId() );
		$objUtilityBatchAccount->setPropertyUnitId( $this->getPropertyUnitId() );
		$objUtilityBatchAccount->setUnitSpaceIds( $this->getUnitSpaceIds() );
		$objUtilityBatchAccount->setOriginalLeaseStartDate( $this->getOriginalLeaseMoveInDate() );
		$objUtilityBatchAccount->setLeaseStartDate( $this->getLeaseStartDate() );
		$objUtilityBatchAccount->setOriginalMoveInDate( $this->getOriginalLeaseMoveInDate() );
		$objUtilityBatchAccount->setMoveInDate( $this->getMoveInDate() );
		$objUtilityBatchAccount->setMoveOutDate( $this->getMoveOutDate() );
		$objUtilityBatchAccount->setOccupantCount( $this->getOccupantCount() );
        $objUtilityBatchAccount->setSquareFeet( $this->getSquareFeet() );
        $objUtilityBatchAccount->setBedroomCount( $this->getNumberOfBedRooms() );
        $objUtilityBatchAccount->setUnitTypeId( $this->getUnitTypeId() );
		$objUtilityBatchAccount->setUpdatedOn( 'NOW()' );
		$objUtilityBatchAccount->setCreatedOn( 'NOW()' );
		$objUtilityBatchAccount->setExceptionDetails( $this->getExceptionDetails() );

		return $objUtilityBatchAccount;

	}

	public function getOrFetchPostMonth( $objUtilityTransaction, $objDatabase ) {

		$objPropertyGlSetting 		= CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		$arrobjPropertyHolidays		= rekeyObjects( 'PropertyId', ( array ) CPropertyHolidays::fetchPropertyHolidaysByCompanyHolidayDateByCid( date( 'm/d/Y' ), $this->getCid(), $objDatabase ) );

		if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			trigger_error( 'Property GL Setting could not be loaded.' );
			exit;
		}

		$strPostMonth = $objPropertyGlSetting->getArPostMonth();

		if( date( 'n', strtotime( $objUtilityTransaction->getScheduledExportDate() ) ) > date( 'n', strtotime( $objPropertyGlSetting->getArPostMonth() ) ) && date( 'Y', strtotime( $objUtilityTransaction->getScheduledExportDate() ) ) >= date( 'Y', strtotime( $objPropertyGlSetting->getArPostMonth() ) ) &&
			true == $objPropertyGlSetting->getArAutoAdvancePostMonth() && 1 == $objPropertyGlSetting->getIgnoreHolidaysAndWeekends() &&
		    ( true == in_array( ( int ) date( 'N', strtotime( date( 'm/d/Y' ) ) ), [ CPropertyHoliday::WEEKEND_SATURDAY, CPropertyHoliday::WEEKEND_SUNDAY ] ) || true == array_key_exists( $this->getPropertyId(), $arrobjPropertyHolidays ) ) ) {
			$strPostMonth = date( 'm/d/Y', strtotime( '+1 months', strtotime( $objPropertyGlSetting->getArPostMonth() ) ) );
		}

		return $strPostMonth;
	}

	public function getNameLast() {
		$strNameLast = $this->m_strNameLast;

		if( 'no name' == \Psi\CStringService::singleton()->strtolower( $strNameLast ) ) {
			$strNameLast = '';
		}

		return $strNameLast;

	}

	public function getNameFirst() {
		$strNameFirst = $this->m_strNameFirst;

		if( 'no name' == \Psi\CStringService::singleton()->strtolower( $this->m_strNameFirst ) ) {
			$strNameFirst = '';
		}

		return $strNameFirst;

	}

	/**
	 * Add Functions
	 */

	public function addUtilityAccountPhoneNumber( $objUtilityAccountPhoneNumber ) {
		$this->m_arrobjUtilityAccountPhoneNumbers[$objUtilityAccountPhoneNumber->getId()] = $objUtilityAccountPhoneNumber;
	}

	public function addPropertyUtilityTypeRates( $objPropertyUtilityTypeRate ) {
		$this->m_arrobjPropertyUtilityTypeRates[$objPropertyUtilityTypeRate->getId()] = $objPropertyUtilityTypeRate;
	}

	public function addMoveInFeeUtilityTransactions( $objMoveInFeeUtilityTransaction, $boolIsRekey = false ) {

		if( true == $boolIsRekey ) {
			$this->m_arrobjMoveInFeeUtilityTransactions[$objMoveInFeeUtilityTransaction->getPropertyUtilityTypeId()][$objMoveInFeeUtilityTransaction->getUtilityTransactionTypeId()][] = $objMoveInFeeUtilityTransaction;
		} elseif( 0 < $objMoveInFeeUtilityTransaction->getPropertyUtilityTypeRateId() ) {
			$this->m_arrobjMoveInFeeUtilityTransactions[$objMoveInFeeUtilityTransaction->getPropertyUtilityTypeRateId()] = $objMoveInFeeUtilityTransaction;
		}
	}

	public function addVcrUtilityTransactions( $objVcrUtilityTransaction ) {
		$this->m_arrobjVcrUtilityTransactions[$objVcrUtilityTransaction->getPropertyUtilityTypeId()][$objVcrUtilityTransaction->getUtilityTransactionTypeId()][] = $objVcrUtilityTransaction;
	}

	public function addTerminationFeeUtilityTransactions( $objTerminationFeeUtilityTransaction ) {
		if( 0 < $objTerminationFeeUtilityTransaction->getPropertyUtilityTypeRateId() ) {
			$this->m_arrobjTerminationFeeUtilityTransactions[$objTerminationFeeUtilityTransaction->getPropertyUtilityTypeRateId()] = $objTerminationFeeUtilityTransaction;
		}
	}

	public function addUtilityMoveOutTransactions( $objUtilityTransaction ) {
		if( false == valObj( $objUtilityTransaction, 'CUtilityTransaction' ) ) {
			return NULL;
		}
		$this->m_arrobjUtilityTransactions[$objUtilityTransaction->getUtilityTransactionTypeId()][$objUtilityTransaction->getPropertyUtilityTypeId() . '_' . $objUtilityTransaction->getUtilityBillChargeId()] = $objUtilityTransaction;
		ksort( $this->m_arrobjUtilityTransactions );
	}

	public function addUtilityTransactions( $objUtilityTransaction ) {

		if( false == valObj( $objUtilityTransaction, 'CUtilityTransaction' ) ) {
			return NULL;
		}

		$this->m_arrobjUtilityTransactions[$objUtilityTransaction->getPropertyUtilityTypeId()][$objUtilityTransaction->getUtilityTransactionTypeId()][] = $objUtilityTransaction;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_utility_type_rate_id'] ) ) {
			$this->setPropertyUtilityTypeRateId( $arrmixValues['property_utility_type_rate_id'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_id'] ) ) {
			$this->setPropertyUtilityTypeId( $arrmixValues['property_utility_type_id'] );
		}
		if( true == isset( $arrmixValues['utility_batch_id'] ) ) {
			$this->setUtilityBatchId( $arrmixValues['utility_batch_id'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_name'] ) ) {
			$this->setPropertyUtilityTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_utility_type_name'] ) : $arrmixValues['property_utility_type_name'] );
		}
		if( true == isset( $arrmixValues['square_feet'] ) ) {
			$this->setSquareFeet( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['square_feet'] ) : $arrmixValues['square_feet'] );
		}
		if( true == isset( $arrmixValues['unit_space_id'] ) ) {
			$this->setUnitSpaceId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unit_space_id'] ) : $arrmixValues['unit_space_id'] );
		}
		if( true == isset( $arrmixValues['exception_details'] ) ) {
			$this->setExceptionDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['exception_details'] ) : $arrmixValues['exception_details'] );
		}
		if( true == isset( $arrmixValues['unit_type_id'] ) ) {
			$this->setUnitTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unit_type_id'] ) : $arrmixValues['unit_type_id'] );
		}
		if( true == isset( $arrmixValues['bedroom_count'] ) ) {
			$this->setNumberOfBedRooms( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bedroom_count'] ) : $arrmixValues['bedroom_count'] );
		}
	}

	public function setDefaults() {
		$this->setUseTemporaryAddress( false );
	}

	public function setDepositGlAccount( $objDepositGlAccount ) {
		$this->m_objDepositGlAccount = $objDepositGlAccount;
	}

	public function setRentGlAccount( $objRentGlAccount ) {
		$this->m_objRentGlAccount = $objRentGlAccount;
	}

	public function setClosedOn( $strClosedOn = NULL ) {
		parent::setClosedOn( $strClosedOn );
	}

	public function setClosedBy( $strClosedBy = NULL ) {
		parent::setClosedBy( $strClosedBy );
	}

	public function setCustomer( $objCustomer ) {
		$this->m_objCustomer = $objCustomer;
	}

	public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function setLeaseIntervals( $arrobjLeaseIntervals ) {
		$this->m_arrobjLeaseIntervals = $arrobjLeaseIntervals;
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setUtilityAccountPhoneNumbers( $arrobjUtilityAccountPhoneNumbers ) {
		$this->m_arrobjUtilityAccountPhoneNumbers = $arrobjUtilityAccountPhoneNumbers;
	}

	public function setUtilityPropertyUnits( $arrobjUtilityPropertyUnits ) {
		$this->m_arrobjUtilityPropertyUnits = $arrobjUtilityPropertyUnits;
	}

	public function setMoveInFeeUtilityTransaction( $objMoveInFeeUtilityTransaction ) {
		$this->m_objMoveInFeeUtilityTransaction = $objMoveInFeeUtilityTransaction;
	}

	public function setTerminationFeeUtilityTransaction( $objTerminationFeeUtilityTransaction ) {
		$this->m_objTerminationFeeUtilityTransaction = $objTerminationFeeUtilityTransaction;
	}

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function setIsFirstUtilityBill( $boolIsFirstUtilityBill ) {
		$this->m_boolIsFirstUtilityBill = $boolIsFirstUtilityBill;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function setPropertyUtilityTypeName( $strPropertyUtilityTypeName ) {
		$this->m_strPropertyUtilityTypeName = $strPropertyUtilityTypeName;
	}

	public function setSquareFeet( $intSquareFeet ) {
		$this->m_intSquareFeet = $intSquareFeet;
	}

	public function setPropertyUtilityTypeRateId( $intPropertyUtilityTypeRateId ) {
		$this->m_intPropertyUtilityTypeRateId = $intPropertyUtilityTypeRateId;
	}

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->m_intPropertyUtilityTypeId = $intPropertyUtilityTypeId;
	}

	public function setNumberOfBedRooms( $intNumberOfBedRooms ) {
		$this->m_intNumberOfBedRooms = $intNumberOfBedRooms;
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->m_intUnitTypeId = $intUnitTypeId;
	}

	public function addScheduledCharges( $objScheduledCharge ) {
		$this->m_arrobjScheduledCharges[] = $objScheduledCharge;
	}

	public function setPropertyUtilityTypeDetails( $arrmixPropertyUtilityTypeDetails ) {
		$this->m_arrmixPropertyUtilityTypeDetails = $arrmixPropertyUtilityTypeDetails;
	}

	public function setLeaseCustomers( $arrobjLeaseCustomers ) {
		$this->m_arrobjLeaseCustomers = $arrobjLeaseCustomers;
	}

	public function setIsInUtilityConsumptionPeriods( $intPropertyUtilityTypeId, $boolIsInConsumption ) {
		$this->m_arrstrIsInUtilityConsumptionPeriod[$intPropertyUtilityTypeId] = $boolIsInConsumption;
	}

	public function setPreviousBalance( $fltPreviousalance ) {
		$this->m_fltPreviousBalance = $fltPreviousalance;
	}

	public function setTotalAmount( $fltTotalAmount ) {
		$this->m_fltTotalAmount = $fltTotalAmount;
	}

	public function setScheduledCharges( $arrobjScheduledCharges ) {
		$this->m_arrobjScheduledCharges = $arrobjScheduledCharges;
	}

	public function setUtilityTransactions( $arrobjUtilityTransactions ) {
		return $this->m_arrobjUtilityTransactions = $arrobjUtilityTransactions;
	}

	public function addTotalDays( $intProprtyUtilityTypeId, $intTotalDays ) {
		$this->m_arrintTotalDays[$intProprtyUtilityTypeId] = $intTotalDays;
	}

	public function getTotalDays() {
		return $this->m_arrintTotalDays;
	}

	public function addIsPropertyUtilityOverlappedConsumption( $intPropertyUtilityTypeId, $boolIsOverlappedConsumption ) {
		return $this->m_boolIsPropertyUtilityOverlappedConsumption[$intPropertyUtilityTypeId] = $boolIsOverlappedConsumption;
	}

	public function addIsUtilityFullCharges( $intPropertyUtilityTypeId, $boolIsGet ) {
		return $this->m_arrboolIsUtilityFullCharges[$intPropertyUtilityTypeId] = $boolIsGet;
	}

	public function setPropertyUtilityTypeRateDetails( $arrmixPropertyUtilityTypeRateDetails ) {
		$this->m_arrmixPropertyUtilityTypeRateDetails = $arrmixPropertyUtilityTypeRateDetails;
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->m_intUtilityBatchId = $intUtilityBatchId;
	}

	public function setIntegrationClientTypeId( $intIntegrationClientTypeId ) {
		$this->m_intIntegrationClientTypeId = $intIntegrationClientTypeId;
	}

	public function setUnitStatus( $strUnitStatus ) {
		$this->m_strUnitStatus = $strUnitStatus;
	}

	public function setIsLeaseIdChange( $boolIsLeaseIdChange ) {
		$this->m_boolIsLeaseIdChange = $boolIsLeaseIdChange;
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->m_intUnitSpaceId = $intUnitSpaceId;
	}

	public function setExceptionDetails( $strExceptionDetails ) {
		$this->m_strExceptionDetails = CStrings::strTrimDef( $strExceptionDetails, -1, NULL, true );
	}

	public function setCoveredBySubsidies( $intPropertyUtilityTypeId, $boolIsInConsumption ) {
		$this->m_arrstrCoveredBySubsidies[$intPropertyUtilityTypeId] = $boolIsInConsumption;
	}

	public function resetCoveredBySubsidies() {
		$this->m_arrstrCoveredBySubsidies = [];
	}

	public function setIsUtilityExcludeFromCalculations( $intPropertyUtilityTypeId, $boolIsInConsumption ) {
		$this->m_arrstrIsUtilityExcludeFromCalculations[$intPropertyUtilityTypeId] = $boolIsInConsumption;
	}

	public function setIsUtilityExcludeFromUsageFactor( $intPropertyUtilityTypeId, $boolIsInConsumption ) {

		$this->m_arrstrIsUtilityExcludeFromUsageFactor[$intPropertyUtilityTypeId] = $boolIsInConsumption;
	}

	public function setIsEligible( $intPropertyUtilityTypeId, $boolIsInConsumption ) {
		$this->m_arrstrIsEligible[$intPropertyUtilityTypeId] = $boolIsInConsumption;
	}

	/**
	 * Get Functions
	 */

	public function getDepositGlAccount() {
		return $this->m_objDepositGlAccount;
	}

	public function getRentGlAccount() {
		return $this->m_objRentGlAccount;
	}

	public function getScheduledCharges() {
		return $this->m_arrobjScheduledCharges;
	}

	public function getCustomer() {
		return $this->m_objCustomer;
	}

	public function getLease() {
		return $this->m_objLease;
	}

	public function getLeaseIntervals() {
		return $this->m_arrobjLeaseIntervals;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getUtilityAccountPhoneNumbers() {
		return $this->m_arrobjUtilityAccountPhoneNumbers;
	}

	public function getUtilityPropertyUnits() {
		return $this->m_arrobjUtilityPropertyUnits;
	}

	public function getPropertyUtilityTypeRates() {
		return $this->m_arrobjPropertyUtilityTypeRates;
	}

	public function getMoveInFeeUtilityTransactions() {
		return $this->m_arrobjMoveInFeeUtilityTransactions;
	}

	public function getVcrUtilityTransactions() {
		return $this->m_arrobjVcrUtilityTransactions;
	}

	public function getTerminationFeeUtilityTransactions() {
		return $this->m_arrobjTerminationFeeUtilityTransactions;
	}

	public function getUtilityTransactions() {
		return $this->m_arrobjUtilityTransactions;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getSquareFeet() {
		return $this->m_intSquareFeet;
	}

	public function getPropertyUtilityTypeRateId() {
		return $this->m_intPropertyUtilityTypeRateId;
	}

	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	public function getNumberOfBedRooms() {
		return $this->m_intNumberOfBedRooms;
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function getPropertyUtilityTypeName() {
		return $this->m_strPropertyUtilityTypeName;
	}

	public function getPropertyUtilityTypeDetails() {
		return $this->m_arrmixPropertyUtilityTypeDetails;
	}

	public function getLeaseCustomers() {
		return $this->m_arrobjLeaseCustomers;
	}

	public function getIsInUtilityConsumptionPeriods() {
		return $this->m_arrstrIsInUtilityConsumptionPeriod;
	}

	public function getPreviousBalance() {
		return $this->m_fltPreviousBalance;
	}

	public function getTotalAmount() {
		return $this->m_fltTotalAmount;
	}

	public function getIsPropertyUtilityOverlappedConsumption() {
		return $this->m_boolIsPropertyUtilityOverlappedConsumption;
	}

	public function getIsUtilityFullCharges() {
		return $this->m_arrboolIsUtilityFullCharges;
	}

	public function getIsFirstUtilityBill() {
		return $this->m_boolIsFirstUtilityBill;
	}

	public function getPropertyUtilityTypeRateDetails() {
		return $this->m_arrmixPropertyUtilityTypeRateDetails;
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function getIntegrationClientTypeId() {
		return $this->m_intIntegrationClientTypeId;
	}

	public function getUnitStatus() {
		return $this->m_strUnitStatus;
	}

	public function getIsLeaseIdChange() {
		return $this->m_boolIsLeaseIdChange;
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function getExceptionDetails() {
		return $this->m_strExceptionDetails;
	}

	public function getCoveredBySubsidies() {
		return $this->m_arrstrCoveredBySubsidies;
	}

	public function getIsUtilityExcludeFromCalculations() {
		return $this->m_arrstrIsUtilityExcludeFromCalculations;
	}

	public function getIsUtilityExcludeUsageFactor() {
		return $this->m_arrstrIsUtilityExcludeFromUsageFactor;
	}

	public function getIsEligible() {
		return $this->m_arrstrIsEligible;
	}

	/**
	 * Get Or Fetch Functions
	 */

	public function getOrFetchProperty( $objClientDatabase ) {

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			return $this->m_objProperty;
		}

		$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );

		return $this->m_objProperty;
	}

	public function getMoveInFeeUtilityTransaction() {
		return $this->m_objMoveInFeeUtilityTransaction;
	}

	public function getTerminationFeeUtilityTransaction() {
		return $this->m_objTerminationFeeUtilityTransaction;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

	/**
	 * Validate Functions
	 */

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Select property name.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyUnitId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', 'Select property unit.' ) );
		}

		return $boolIsValid;
	}

	public function valUnitSpaceIds() {
		$boolIsValid = true;

		if( false == valArr( $this->getUnitSpaceIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_id', 'Select unit space.' ) );
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', 'Select customer name.' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->getLeaseId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', 'Select lease number.' ) );
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getRemotePrimaryKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'remote_primary_key', 'Remote primary key field is empty.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOriginalLeaseStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getOriginalLeaseStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_lease_start_date', 'Valid original lease start date is required.' ) );
		} elseif( 1 !== CValidation::checkDate( $this->getOriginalLeaseStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_lease_start_date', 'Original lease start date must be in mm/dd/yyyy form.' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getLeaseStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', 'Valid lease start date is required.' ) );
		} elseif( 1 !== CValidation::checkDate( $this->getLeaseStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', 'Lease start date must be in mm/dd/yyyy form.' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseEndDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getLeaseEndDate() ) && 1 !== CValidation::checkDate( $this->getLeaseEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', 'Lease end date must be in mm/dd/yyyy form.' ) );
		}

		return $boolIsValid;
	}

	public function valMoveInDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getMoveInDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', 'Valid move in date is required.' ) );
		} elseif( 1 !== CValidation::checkDate( $this->getMoveInDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_lease_start_date', 'Move in date must be in mm/dd/yyyy form.' ) );
		}

		return $boolIsValid;
	}

	public function valMoveOutDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getMoveOutDate() ) && 1 !== CValidation::checkDate( $this->getMoveOutDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', 'Move out date must be in mm/dd/yyyy form.' ) );
		}

		return $boolIsValid;
	}

	public function valOriginalLeaseStartDateBeforeLeaseStartDate() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strOriginalLeaseStartDate ) && false == is_null( $this->m_strLeaseStartDate )
				&& strtotime( $this->m_strLeaseStartDate ) > strtotime( $this->m_strOriginalLeaseStartDate ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', 'Original lease start date cannot fall after the lease start date.' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseStartDateBeforeLeaseEndDate() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strLeaseStartDate ) && false == is_null( $this->m_strLeaseEndDate )
				&& strtotime( $this->m_strLeaseStartDate ) > strtotime( $this->m_strLeaseEndDate ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', 'Lease start date must fall before the move-in date.' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseStartDateBeforeMoveInDate() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strLeaseStartDate ) && false == is_null( $this->m_strMoveInDate )
				&& strtotime( $this->m_strLeaseStartDate ) > strtotime( $this->m_strMoveInDate ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', 'Lease start date must fall before the move-in date.' ) );
		}

		return $boolIsValid;
	}

	public function valMoveInDateBeforeMoveOutDate() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strMoveInDate ) && false == is_null( $this->m_strMoveOutDate )
				&& strtotime( $this->m_strMoveInDate ) > strtotime( $this->m_strMoveOutDate ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', 'Move-in date must fall before the move-out date.' ) );
		}

		return $boolIsValid;
	}

	public function valAdditionalEmailAddresses() {

		$boolIsValid	= true;
		$arrstrEmails	= $this->getAdditionalEmailAddresses();

		if( false == valArr( $arrstrEmails ) ) {
			return true;
		}

		foreach( $arrstrEmails as $strValue ) {

			if( false == CValidation::validateEmailAddress( $strValue ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'additional_email_addresses', 'Email address is invalid.' ) );
				break;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valUnitSpaceIds();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valOriginalLeaseStartDate();
				$boolIsValid &= $this->valLeaseStartDate();
				$boolIsValid &= $this->valLeaseEndDate();
				$boolIsValid &= $this->valMoveInDate();
				$boolIsValid &= $this->valMoveOutDate();
				$boolIsValid &= $this->valAdditionalEmailAddresses();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchIntegratedUtilityTransactions( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchIntegratedUtilityTransactionsByUtilityAccountId( $this->getId(), $objDatabase );
	}

	public function fetchUtilityTransactions( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUtilityTransactionsByUtilityAccountId( $this->getId(), $objDatabase );
	}

	public function fetchActiveUtilityTransactions( $objDatabase ) {

		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchActiveUtilityTransactionsByUtilityAccountId( $this->getId(), $objDatabase );
	}

	public function fetchOpenUtilityTransactions( $objDatabase ) {

		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchOpenUtilityTransactionsByUtilityAccountId( $this->getId(), $objDatabase );
	}

	public function fetchUtilityTransactionById( $intUtilityTransactionId, $objDatabase ) {

		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUtilityTransactionByUtilityAccountIdById( $this->getId(), $intUtilityTransactionId, $objDatabase );
	}

	public function fetchPropertyUtilityTypeRates( $objDatabase ) {

		return \Psi\Eos\Utilities\CPropertyUtilityTypeRates::createService()->fetchPropertyUtilityTypeRatesByUtilityAccountId( $this->getId(), $objDatabase );
	}

	public function fetchPaginatedUtilityAccountChangeRequests( $intPageNo, $intPageSize, $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityChangeRequests::createService()->fetchPaginatedUtilityChangeRequestsByUtilityAccountId( $intPageNo, $intPageSize, $this->getId(), $objDatabase );
	}

	public function fetchUtilityAccountChangeRequestsCount( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityChangeRequests::createService()->fetchUtilityChangeRequestCount( ' WHERE utility_account_id = ' . $this->getId(), $objDatabase );
	}

	public function fetchPropertyCapSubsidies( $objDatabase ) {
		return \Psi\Eos\Utilities\CPropertyCapSubsidies::createService()->fetchPropertyCapSubsidiesByUtilityAccountIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUtilityChangeRequestById( $intUtilityChangeRequestId, $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityChangeRequests::createService()->fetchUtilityChangeRequestById( $intUtilityChangeRequestId, $objDatabase );
	}

	public function fetchPropertyUnit( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchMoveOutCharges( $objDatabase ) {

		$arrobjUnexportedUtilityTransactions = [];

		if( CUtilityAccountStatusType::PAST == $this->getUtilityAccountStatusTypeId() ) {
			$arrobjUnexportedUtilityTransactions = \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchUnexportedUtilityTransactionsByUtilityAccountIds( [ $this->getId() ], $objDatabase );
		} else {
			$arrobjUnexportedUtilityTransactions = \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchMoveOutUtilityTransactionsByUtilityAccountIds( [ $this->getId() ], $objDatabase );
		}

		return $arrobjUnexportedUtilityTransactions;
	}

	public function fetchExportedMoveOutCharges( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityTransactions::createService()->fetchExportedUtilityTransactionsByUtilityAccountIds( [ $this->getId() ], $objDatabase );
	}

	public function fetchUtilityPropertyUnit( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityPropertyUnits::createService()->fetchUtilityPropertyUnitByPropertyUnitId( $this->getPropertyUnitId(), $objDatabase );
	}

	public function fetchUtilityAccountAddress( $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityAccountAddresses::createService()->fetchUtilityAccountAddressByUtilityAccountId( $this->getId(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function reverseMoveOutCharges( $intCompanyUserId, $objUtilityDatabase, $objClientDatabase ) {
		$arrobjUtilityTransactions = $this->fetchExportedMoveOutCharges( $objUtilityDatabase );

		if( false == valArr( $arrobjUtilityTransactions ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->m_intPropertyId, $this->m_intCid, CIntegrationService::REVERSE_UTILITY_TRANSACTIONS, $intCompanyUserId, $objClientDatabase );
		$objGenericWorker->setProperty( $this->getOrFetchProperty( $objClientDatabase ) );
		$objGenericWorker->setUtilityTransactions( $arrobjUtilityTransactions );
		$objGenericWorker->setUtilityDatabase( $objUtilityDatabase );

		return $objGenericWorker->process();
	}

	public function exportMoveOutCharges( $intCompanyUserId, $objUtilityDatabase, $objClientDatabase ) {
		$arrobjUtilityTransactions = $this->fetchMoveOutCharges( $objUtilityDatabase );

		if( false == valArr( $arrobjUtilityTransactions ) ) {
			return true;
		}

		foreach( $arrobjUtilityTransactions as $intKey => $objUtilityTransaction ) {
			if( false == is_null( $objUtilityTransaction->getRemotePrimaryKey() ) ) {
				unset( $arrobjUtilityTransactions[$intKey] );
			}
		}

		if( false == valArr( $arrobjUtilityTransactions ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->m_intPropertyId, $this->m_intCid, CIntegrationService::SEND_UTILITY_TRANSACTIONS, $intCompanyUserId, $objClientDatabase );
		$objGenericWorker->setProperty( $this->getOrFetchProperty( $objClientDatabase ) );
		$objGenericWorker->setUtilityTransactions( $arrobjUtilityTransactions );
		$objGenericWorker->setUtilityDatabase( $objUtilityDatabase );

		return $objGenericWorker->process();
	}

	// We are calling this function from the import customers transport classes

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $intUtilityAccountStatusTypeId = NULL, $intOccupantCount = NULL, $boolRequiresUpdate = false, $boolIsInsertNew = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->getProperty() ) || true == is_null( $this->getLease() ) || true == is_null( $this->getCustomer() ) || NULL == $intUtilityAccountStatusTypeId ) {

			return parent::insertOrUpdate( $intCurrentUserId, $objDatabase );
		} elseif( false == is_null( $this->getCustomer() ) && false == is_null( $this->getProperty() ) && false == is_null( $this->getLease() ) ) {

			$objProperty = $this->getProperty();

			if( false == is_null( $objProperty->getRemotePrimaryKey() ) ) {
				$this->setRemotePrimaryKey( $this->m_objLease->getRemotePrimaryKey() );
			} else {
				$this->setRemotePrimaryKey( NULL );
			}

			// If multiple intervals of type renewals only

			$arrobjLeaseIntervals = $this->getLeaseIntervals();
			$objLease = $this->getLease();

			$objActiveLeaseInterval = getArrayElementByKey( $objLease->getActiveLeaseIntervalId(), $arrobjLeaseIntervals );

			if( true == valObj( $objActiveLeaseInterval, 'CLeaseInterval' ) ) {
				unset( $arrobjLeaseIntervals[$objLease->getActiveLeaseIntervalId()] );
			}

			$arrintUtilityAccounts = [ 145608, 145606, 145599, 619101, 772281, 790534, 618895, 619015, 739235 ];

			if( true == $this->getIsLeaseIdChange() && false == in_array( $this->getId(), $arrintUtilityAccounts ) ) {
				$this->setOriginalLeaseStartDate( $objLease->getLeaseStartDate() );
				$this->setOriginalLeaseMoveInDate( $objLease->getMoveInDate() );
				$boolRequiresUpdate = true;
			}

			$boolUpdateOriginalDates = false;

			if( true == valArr( $arrobjLeaseIntervals ) ) {
				$objLeaseInterval = array_pop( $arrobjLeaseIntervals );
				$boolUpdateOriginalDates = true;
				$boolRequiresUpdate = true;
			}

			if( true == $boolUpdateOriginalDates && false == in_array( $this->getId(), $arrintUtilityAccounts ) ) {
				$this->setOriginalLeaseStartDate( $objLeaseInterval->getLeaseStartDate() );
				$this->setOriginalLeaseMoveInDate( $objLeaseInterval->getLeaseStartDate() );
			}

			if( true == is_null( $this->getId() ) || true == $boolIsInsertNew ) {

				$this->setUtilityAccountStatusTypeId( $intUtilityAccountStatusTypeId );
				$this->setOccupantCount( $intOccupantCount );

				if( false == $boolUpdateOriginalDates ) {
					$this->setOriginalLeaseMoveInDate( $this->m_objLease->getMoveInDate() );
				}

				$arrintUnitSpaceIds = ( true == valArr( $this->m_objLease->getLeaseUnitSpaceIds() ) ) ? $this->m_objLease->getLeaseUnitSpaceIds() : [ $this->m_objLease->getUnitSpaceId() ];

				$this->setUnitSpaceIds( $arrintUnitSpaceIds );

				$boolIsValid = $this->validate( VALIDATE_INSERT );

				if( false == $boolIsValid || false == $this->insert( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Property( ' . $this->m_objProperty->getId() . '): ' . $this->m_objProperty->getPropertyName() . ': Customer ( ' . $this->m_objCustomer->getNameFirst() . '' . $this->m_objCustomer->getNameLast() . '):: Failed to insert utility account.', NULL ) );
					trigger_error( 'Client: ' . $this->m_objProperty->getCid() . ': Property( ' . $this->m_objProperty->getId() . '): ' . $this->m_objProperty->getPropertyName() . ': Customer ( ' . $this->m_objCustomer->getNameFirst() . '' . $this->m_objCustomer->getNameLast() . '):: Failed to insert utility account.', E_USER_WARNING );
					return false;
				}

			} else {

				if( true == $this->getIsMovedOut() && $this->getUtilityAccountStatusTypeId() == CUtilityAccountStatusType::CURRENT ) {
					$this->setIsMovedOut( false );
					$boolRequiresUpdate = true;
				}

				if( $this->getPropertyUnitId() != $this->m_objLease->getPropertyUnitId() ) {
					$this->setPropertyUnitId( $this->m_objLease->getPropertyUnitId() );
					$boolRequiresUpdate = true;
				}

				// In case of non-integration we are setting unit space ids on lease object.
				if( true == valArr( $this->m_objLease->getLeaseUnitSpaceIds() ) ) {
                    $arrintChangedUnitSpaceIds = array_merge( array_diff( $this->m_objLease->getLeaseUnitSpaceIds(), $this->getUnitSpaceIds() ), array_diff( $this->getUnitSpaceIds(), $this->m_objLease->getLeaseUnitSpaceIds() ) );

                    if( true == valArr( $arrintChangedUnitSpaceIds ) ) {
                        $this->setUnitSpaceIds( $this->m_objLease->getLeaseUnitSpaceIds() );
                        $boolRequiresUpdate = true;
                    }
                } elseif( $this->m_objLease->getUnitSpaceId() != $this->getUnitSpaceId() ) {
                    $this->setUnitSpaceIds( [ $this->m_objLease->getUnitSpaceId() ] );
                    $boolRequiresUpdate = true;
                }

				if( $this->getInvoiceDeliveryTypeId() != $this->m_objCustomer->getInvoiceDeliveryTypeId() ) {
					$this->setInvoiceDeliveryTypeId( $this->m_objCustomer->getInvoiceDeliveryTypeId() );
					$boolRequiresUpdate = true;
				}

				if( $intUtilityAccountStatusTypeId != $this->getUtilityAccountStatusTypeId() ) {
					$this->setUtilityAccountStatusTypeId( $intUtilityAccountStatusTypeId );
					$boolRequiresUpdate = true;
				}

				if( $this->getCustomerId() != $this->m_objCustomer->getId() ) {
					$this->setCustomerId( $this->m_objCustomer->getId() );
					$boolRequiresUpdate = true;
				}

				if( $this->getLeaseId() != $this->m_objLease->getId() ) {
					$this->setLeaseId( $this->m_objLease->getId() );
					$boolRequiresUpdate = true;
				}

				if( true == valStr( $this->m_objCustomer->getNameFirst() ) && $this->getNameFirst() != $this->m_objCustomer->getNameFirst() && false == $this->getIsNotOverrideName() ) {
					$this->setNameFirst( $this->m_objCustomer->getNameFirst() );
					$boolRequiresUpdate = true;
				}

				if( false == is_null( $this->m_objCustomer->getNameLast() ) && $this->getNameLast() != $this->m_objCustomer->getNameLast() && false == $this->getIsNotOverrideName() ) {
					$this->setNameLast( $this->m_objCustomer->getNameLast() );
					$boolRequiresUpdate = true;
				}

				if( true == is_null( $this->m_objCustomer->getNameFirst() ) && true == is_null( $this->m_objCustomer->getNameLast() ) && $this->getNameFirst() != $this->m_objCustomer->getNameFull() && false == $this->getIsNotOverrideName() ) {
					$this->setNameFirst( $this->m_objCustomer->getNameFull() );
					$boolRequiresUpdate = true;
				}

				if( false == is_null( $this->m_objLease->getLeaseStartDate() ) && $this->getLeaseStartDate() != $this->m_objLease->getLeaseStartDate() && false == in_array( $this->getId(), $arrintUtilityAccounts ) ) {

					if( CIntegrationClientType::AMSI == $this->getIntegrationClientTypeId() && true == valStr( $this->getLeaseStartDate() ) && ( false == valStr( $this->getOriginalLeaseStartDate() ) || strtotime( $this->getLeaseStartDate() ) < strtotime( $this->m_objLease->getLeaseStartDate() ) ) ) {
						$this->setOriginalLeaseStartDate( $this->getLeaseStartDate() );
					}

					$this->setLeaseStartDate( $this->m_objLease->getLeaseStartDate() );
					$boolRequiresUpdate = true;
				}

				if( $this->getLeaseEndDate() != $this->m_objLease->getLeaseEndDate() ) {
					$this->setLeaseEndDate( $this->m_objLease->getLeaseEndDate() );
					$boolRequiresUpdate = true;
				}

				if( false == is_null( $this->m_objLease->getMoveInDate() ) && $this->getMoveInDate() != $this->m_objLease->getMoveInDate() && false == in_array( $this->getId(), $arrintUtilityAccounts ) ) {

					if( CIntegrationClientType::AMSI == $this->getIntegrationClientTypeId() && true == valStr( $this->getMoveInDate() ) && ( false == valStr( $this->getOriginalLeaseMoveInDate() ) || strtotime( $this->getMoveInDate() ) < strtotime( $this->m_objLease->getMoveInDate() ) ) ) {
						$this->setOriginalLeaseMoveInDate( $this->getMoveInDate() );
					}

					$this->setMoveInDate( $this->m_objLease->getMoveInDate() );
					$boolRequiresUpdate = true;
				}

				if( $this->getMoveOutDate() != $this->m_objLease->getMoveOutDate() ) {
					$this->setMoveOutDate( $this->m_objLease->getMoveOutDate() );
					$boolRequiresUpdate = true;
				}

				if( true == is_numeric( $intOccupantCount ) && 0 < $intOccupantCount && $this->getOccupantCount() != $intOccupantCount && false == $this->getIsLocked() ) {
					$this->setOccupantCount( $intOccupantCount );
					$boolRequiresUpdate = true;
				}

				if( true == $boolRequiresUpdate && false == $this->update( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Property( ' . $this->m_objProperty->getId() . '): ' . $this->m_objProperty->getPropertyName() . ': Customer ( ' . $this->m_objCustomer->getNameFirst() . '' . $this->m_objCustomer->getNameLast() . '):: Failed to update utility account.', NULL ) );
					$boolIsValid &= false;
					trigger_error( 'Client: ' . $this->m_objProperty->getCid() . ': Property( ' . $this->m_objProperty->getId() . '): ' . $this->m_objProperty->getPropertyName() . ': Customer ( ' . $this->m_objCustomer->getNameFirst() . '' . $this->m_objCustomer->getNameLast() . '):: Failed to update utility account.', E_USER_WARNING );
				}
			}

			// We will uncomment the code after standard release.
//			if( false == is_null( $this->m_objCustomer->getPrimaryPhoneNumberTypeId() ) && ( false == is_null( $this->getId() ) || true == $boolIsInsertNew ) ) {
//
//				// Insert/update utility account phone numbers.
//				$boolRequiresUpdateAccountPhoneNumber = false;
//
//				$objUtilityAccountPhoneNumber = getArrayElementByKey( $this->m_objCustomer->getPrimaryPhoneNumberTypeId(), $this->getUtilityAccountPhoneNumbers() );
//
//				if( false == valObj( $objUtilityAccountPhoneNumber, 'CUtilityAccountPhoneNumber' ) ) {
//					$objUtilityAccountPhoneNumber = $this->createUtilityAccountPhoneNumber();
//					$objUtilityAccountPhoneNumber->setPhoneNumberTypeId( $this->m_objCustomer->getPrimaryPhoneNumberTypeId() );
//					$boolRequiresUpdateAccountPhoneNumber = true;
//				}
//
//				$strPhoneNumber = NULL;
//				$intMessageOperatorId = NULL;
//
//				if( false == is_null( $this->m_objCustomer->getPhoneNumber() ) ) {
//					$strPhoneNumber = $this->m_objCustomer->getPhoneNumber();
//				} else {
//					$strPhoneNumber = $this->m_objCustomer->getFaxNumber();
//				}
//
//				if( $intMessageOperatorId != $objUtilityAccountPhoneNumber->getMessageOperatorId() ) {
//					$objUtilityAccountPhoneNumber->setMessageOperatorId( $intMessageOperatorId );
//					$boolRequiresUpdateAccountPhoneNumber = true;
//				}
//
//				if( 0 < strlen( trim( $strPhoneNumber ) ) && $strPhoneNumber != $objUtilityAccountPhoneNumber->getPhoneNumber() ) {
//					$objUtilityAccountPhoneNumber->setPhoneNumber( $strPhoneNumber );
//					$boolRequiresUpdateAccountPhoneNumber = true;
//				}
//
//				if( true == $boolRequiresUpdateAccountPhoneNumber && false == $objUtilityAccountPhoneNumber->insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
//					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Property( ' . $this->m_objProperty->getId() . '): ' . $this->m_objProperty->getPropertyName() . ': Customer ( ' . $this->m_objCustomer->getNameFirst() . '' . $this->m_objCustomer->getNameLast() . '):: Failed to add/update utility account phone number.', NULL ) );
//					trigger_error( 'Client: ' . $this->m_objProperty->getCid() . ': Property( ' . $this->m_objProperty->getId() . '): ' . $this->m_objProperty->getPropertyName() . ': Customer ( ' . $this->m_objCustomer->getNameFirst() . '' . $this->m_objCustomer->getNameLast() . '):: Failed to add/update utility account phone number.', E_USER_WARNING );
//					$boolIsValid &= false;
//				}
//			}

		} else {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	// Updating utility transactions with imported ledger

	public function updateUtilityTransactions( $intCurrentUserId, $objDatabase, $objClientDatabase, $arrobjArCodes, $arrobjStdArTransactions = NULL, $objClient = NULL, $objPaymentArCode = NULL ) {
		if( true == is_null( $this->getProperty() ) || true == is_null( $this->getLease() ) ) {
			return false;
		}

		$boolIsValid = true;

		$arrintVCRUtilityTransactionTypeIds = [ CUtilityTransactionType::VCR_USAGE_FEE, CUtilityTransactionType::VCR_SERVICE_FEE,  CUtilityTransactionType::VCR_LATE_FEE ];

		$arrobjUtilityTransactions = $this->getUtilityTransactions();

		if( false == valArr( $this->m_arrobjUtilityTransactions ) ) {
			$arrobjUtilityTransactions = $this->fetchActiveUtilityTransactions( $objDatabase );
		}

		// Process Non Integrated Property Info
		if( true == is_null( $this->m_objLease->getRemotePrimaryKey() ) && true == is_null( $this->getRemotePrimaryKey() ) ) {
			// Non integrated utility account

			$arrobjRekeyedUtilityTransactions = [];

			if( true == valArr( $arrobjUtilityTransactions ) ) {
				foreach( $arrobjUtilityTransactions as $objUtilityTransaction ) {

					if( ( $objUtilityTransaction->getUtilityTransactionTypeId() == CUtilityTransactionType::CONVERGENT || $objUtilityTransaction->getUtilityTransactionTypeId() == CUtilityTransactionType::TAX )
							&& true == is_null( $objUtilityTransaction->getArTransactionId() ) ) {

						$objUtilityTransaction->setCurrentAmount( 0 );

						if( false == $objUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction could not be updated.', NULL ) );
							trigger_error( 'Client: ' . $this->getCid() . ' : Utility account ( ' . $this->getId() . ' ) - Id [ ' . $objUtilityTransaction->getId() . ' ] [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] :: Charge transaction could not be updated. The ledger was not updated successfully.', E_USER_WARNING );
							return false;
						}
					}

					if( false == is_null( $objUtilityTransaction->getArTransactionId() ) && true == in_array( $objUtilityTransaction->getUtilityTransactionTypeId(), $arrintVCRUtilityTransactionTypeIds ) ) {
						$objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::CONVERGENT );
					}

					if( ( true == is_null( $objUtilityTransaction->getArTransactionId() ) ) && ( $objUtilityTransaction->getUtilityTransactionTypeId() != CUtilityTransactionType::CONVERGENT || $objUtilityTransaction->getUtilityTransactionTypeId() != CUtilityTransactionType::TAX )
								|| true == is_null( $objUtilityTransaction->getArTransactionId() ) ) {
						continue;
					}

					$arrobjRekeyedUtilityTransactions[$objUtilityTransaction->getArTransactionId()] = $objUtilityTransaction;
				}
			}

			$arrobjArTransactions = $this->m_objLease->getArTransactions();

			if( false == valArr( $arrobjArTransactions ) ) {

				$intLedgerFilterId = \Psi\Eos\Entrata\CLedgerFilters::createService()->fetchLedgerFilterIdByDefaultLedgerFilterIdByCid( CDefaultLedgerFilter::RESIDENT, $this->getCid(), $objClientDatabase );

				$objArTransactionsFilter = new CArTransactionsFilter();
				$objArTransactionsFilter->setDefaults( true );
				$objArTransactionsFilter->setShowOutstandingOnly( true );
				$objArTransactionsFilter->setHideTemporary( true );

				if( false == empty( $intLedgerFilterId ) ) {
					$objArTransactionsFilter->setLedgerFilterId( $intLedgerFilterId );
				}

				$arrobjArTransactions = $this->m_objLease->fetchArTransactions( $objArTransactionsFilter, $objClientDatabase );
			}

			if( true == valArr( $arrobjArTransactions ) ) {
				foreach( $arrobjArTransactions as $objArTransaction ) {

					if( true == $objArTransaction->getIsTemporary() ) {
						continue;
					}

					$objUtilityTransaction = getArrayElementByKey( $objArTransaction->getId(), $arrobjRekeyedUtilityTransactions );

					if( false == valObj( $objUtilityTransaction, 'CUtilityTransaction' ) ) {

						$objUtilityTransaction = $objArTransaction->createUtilityTransaction();
						$objArCode = getArrayElementByKey( $objArTransaction->getArCodeId(), $arrobjArCodes );

						if( false == valObj( $objArCode, 'CArCode' ) ) {
							continue;
						}

						$objUtilityTransaction->setChargeName( $objArCode->getName() );
						$objUtilityTransaction->setChargeDescription( $objArCode->getDescription() );
						$objUtilityTransaction->setUtilityAccountId( $this->getId() );
						$objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::CONVERGENT );

					} else {
						$objUtilityTransaction->setArTransactionId( $objArTransaction->getId() );
						$objUtilityTransaction->setCurrentAmount( $objArTransaction->getTransactionAmountDue() );
						unset( $arrobjRekeyedUtilityTransactions[$objArTransaction->getId()] );
					}

					if( true == is_null( $objArTransaction->getScheduledChargeId() ) ) {
						$objUtilityTransaction->setPostMonth( $objArTransaction->getPostMonth() );
					}

					if( false == $objUtilityTransaction->insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction could not be inserted/update.', NULL ) );
						trigger_error( 'Client: ' . $this->getCid() . ' : Utility account ( ' . $this->getId() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] :: Charge transaction could not be inserted/updated. The ledger was not updated successfully.', E_USER_WARNING );
						return false;
					}
				}
			}

			if( true == valArr( $arrobjRekeyedUtilityTransactions ) ) {
				foreach( $arrobjRekeyedUtilityTransactions as $objRekeyedUtilityTransaction ) {

					if( false == is_null( $objRekeyedUtilityTransaction->getScheduledExportDate() ) && true == is_null( $objRekeyedUtilityTransaction->getExportedOn() ) ) {
						continue;
					}

					$objRekeyedUtilityTransaction->setCurrentAmount( 0 );

					if( false == $objRekeyedUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction could not be updated.', NULL ) );
						trigger_error( 'Client: ' . $this->getCid() . ' : Utility account ( ' . $this->getId() . ' ) - Id [ ' . $objRekeyedUtilityTransaction->getId() . ' ] [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] :: Charge transaction could not be updated. The ledger was not updated successfully.', E_USER_WARNING );
						return false;
					}
				}
			}

			return $boolIsValid;
		}

		// Process Integrated Property Process Lease Charges
		$arrobjUtilityTransactionsReKeyed = [];

		if( true == valArr( $arrobjUtilityTransactions ) ) {
			foreach( $arrobjUtilityTransactions as $objUtilityTransaction ) {

				if( true == is_null( $objUtilityTransaction->getRemotePrimaryKey() ) ) {
					continue;
				}

				if( true == is_null( $objUtilityTransaction->getExportedOn() ) ) {
					// We should not delete utility charges (Created By PSI), we should update it.

					if( false == $objUtilityTransaction->delete( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Utility Transaction( ' . $objUtilityTransaction->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' failed to update.', NULL ) );
						trigger_error( 'Client: ' . $this->getCid() . ' : Utility Transaction( ' . $objUtilityTransaction->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' failed to update.', E_USER_WARNING );
						$boolIsValid &= false;
					}

				} else {
					// Remote primary key and exported on are not null - Exported utility transaction.

				    if( false == is_null( $objUtilityTransaction->getArTransactionId() ) && true == in_array( $objUtilityTransaction->getUtilityTransactionTypeId(), $arrintVCRUtilityTransactionTypeIds ) ) {

				        $objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::CONVERGENT );

				        if( false == $objUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
				            $this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction failed to update.', NULL ) );
				            trigger_error( 'Client: ' . $this->getCid() . ' : Utility Transaction( ' . $objUtilityTransaction->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' failed to update.', E_USER_WARNING );
				            $boolIsValid &= false;
				        }
				    }

					$arrobjUtilityTransactionsReKeyed[\Psi\CStringService::singleton()->strtolower( $objUtilityTransaction->getRemotePrimaryKey() )] = $objUtilityTransaction;
				}
			}
		}

		$arrobjArCodesReKeyed = [];

		if( true == valArr( $arrobjArCodes ) ) {
			foreach( $arrobjArCodes as $objArCode ) {
				$arrobjArCodesReKeyed[\Psi\CStringService::singleton()->strtolower( $objArCode->getRemotePrimaryKey() )] = $objArCode;
			}
		}

		if( true == valObj( $arrobjStdArTransactions, 'stdClass' ) ) {
			$arrobjStdArTransactions = [ $arrobjStdArTransactions ];
		}

		// Divide up the transactions into charges, payments, negative payments, and concessions.
		$arrobjStdCharges 				  	= [];
		$arrobjStdConcessions 			  	= [];
		$arrobjStdArTransactionPayments  	= [];
		$arrobjStdNegativePayments 	  		= [];

		if( true == valArr( $arrobjStdArTransactions ) ) {
			foreach( $arrobjStdArTransactions as $objStdArTransaction ) {

				if( true == isset( $objStdArTransaction->isHoldCharge ) && 1 == $objStdArTransaction->isHoldCharge ) {
					continue;
				}

				// Payments
				if( true == isset( $objStdArTransaction->isPaymentTransaction ) && 1 == $objStdArTransaction->isPaymentTransaction && 0 <= $objStdArTransaction->transactionAmount ) {
					$arrobjStdArTransactionPayments[] = $objStdArTransaction;

				// Negative Payments (nsf adjustments)
				} elseif( true == isset( $objStdArTransaction->isPaymentTransaction ) && 1 == $objStdArTransaction->isPaymentTransaction && 0 > $objStdArTransaction->transactionAmount ) {
					$arrobjStdNegativePayments[] = $objStdArTransaction;

				// Unpaid or partial or totally paid charges
				} elseif( true == isset( $objStdArTransaction->transactionAmount ) && 0 <= $objStdArTransaction->transactionAmount ) {
					$arrobjStdCharges[] = $objStdArTransaction;

				// Unallocated or partially or Fully allocated concessions
				} elseif( true == isset( $objStdArTransaction->transactionAmount ) && 0 > $objStdArTransaction->transactionAmount ) {
					$arrobjStdConcessions[] = $objStdArTransaction;

				// Error
				} else {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Transaction was imported in an unexpected format for client (' . $this->getCid() . ')', NULL ) );
					trigger_error( 'Client: ' . $this->getCid() . ' : Utility account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->m_objProperty->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' had a transaction imported in an unexpected format.  The ledger was not updated successfully.', E_USER_WARNING );
					$boolIsValid &= false;
				}
			}
		}

		// insert charges keyed by remote primary key
		if( true == valArr( $arrobjStdCharges ) ) {
			foreach( $arrobjStdCharges as $objStdCharge ) {

				$objUtilityTransaction = getArrayElementByKey( \Psi\CStringService::singleton()->strtolower( $objStdCharge->remotePrimaryKey ), $arrobjUtilityTransactionsReKeyed );

				if( true == valObj( $objUtilityTransaction, 'CUtilityTransaction' ) ) {
					$fltTransactionAmountDue = ( float ) $objStdCharge->transactionAmountDue;

					if( 0 < $fltTransactionAmountDue && $objUtilityTransaction->getCurrentAmount() != $fltTransactionAmountDue ) {
						$objUtilityTransaction->setCurrentAmount( $fltTransactionAmountDue );

						if( false == $objUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction failed to update.', NULL ) );
							trigger_error( 'Client: ' . $this->getCid() . ' : Utility account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' :: Charge transaction could not be updated. The ledger was not updated successfully.', E_USER_WARNING );
							$boolIsValid &= false;
						}
					}

					unset( $arrobjUtilityTransactionsReKeyed[\Psi\CStringService::singleton()->strtolower( $objStdCharge->remotePrimaryKey )] );
					continue;
				}

				$objArCode = getArrayElementByKey( \Psi\CStringService::singleton()->strtolower( $objStdCharge->chargeCodeRemotePrimaryKey ), $arrobjArCodesReKeyed );

				if( false == valObj( $objArCode, 'CArCode' ) ) {
					$objArCode = $objClient->fetchArCodeByRemotePrimaryKeyByIntegrationDatabaseId( $objStdCharge->chargeCodeRemotePrimaryKey, $this->m_objLease->getIntegrationDatabaseId(), $objClientDatabase );
				}

				if( false == valObj( $objArCode, 'CArCode' ) ) {
					$objArCode = $objClient->fetchArCodeByNameByIntegrationDatabaseId( $objStdCharge->transactionMemo, $this->m_objLease->getIntegrationDatabaseId(), $objClientDatabase );
				}

				if( false == valObj( $objArCode, 'CArCode' ) ) {
					$objArCode = $objClient->fetchArCodeByNameByIntegrationDatabaseIdNull( $objStdCharge->transactionMemo, $objClientDatabase );
				}

				if( false == valObj( $objArCode, 'CArCode' ) && 0 == strlen( trim( $objStdCharge->transactionMemo ) ) ) {
					continue;
				}

				// Insert or Update ArCode for Charge

				if( false == valObj( $objArCode, 'CArCode' ) && false == $this->insertOrUpdateArCode( $objClient, $intCurrentUserId, $objClientDatabase, $objStdCharge->transactionMemo, $objStdCharge->transactionMemo ) ) {
						continue;
				}

				$objUtilityTransaction = $this->createUtilityTransaction( $objArCode );

				$objUtilityTransaction->setRemotePrimaryKey( $objStdCharge->remotePrimaryKey );
				$objUtilityTransaction->setTransactionDatetime( $objStdCharge->transactionDatetime );
				$objUtilityTransaction->setOriginalAmount( $objStdCharge->transactionAmount );
				$objUtilityTransaction->setUnadjustedAmount( $objStdCharge->transactionAmount );
				$objUtilityTransaction->setCurrentAmount( $objStdCharge->transactionAmount );
				$objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::CONVERGENT );
				$objUtilityTransaction->setMemo( ( true == isset( $objStdCharge->transactionMemo ) ) ? $objStdCharge->transactionMemo : NULL );

				if( true == valArr( $this->getScheduledCharges() ) ) {
					foreach( $this->getScheduledCharges() as $objScheduledCharge ) {
						if( false == is_null( $objScheduledCharge->getLastPostedOn() ) && date( 'm/d/Y', strtotime( $objStdCharge->transactionDatetime ) ) <= date( 'm/d/Y', strtotime( $objScheduledCharge->getLastPostedOn() ) ) && ( float ) ( $objStdCharge->transactionAmountApplied + $objStdCharge->transactionAmountDue ) == ( float ) $objScheduledCharge->getChargeAmount()
							&& $objScheduledCharge->getArCodeId() == $objUtilityTransaction->getArCodeId() && date( 'd', strtotime( $objStdCharge->transactionDatetime ) ) == date( 'd', strtotime( $objScheduledCharge->getLastPostedOn() ) ) ) {
							$objUtilityTransaction->setScheduledChargeId( $objScheduledCharge->getId() );
							break;
						}
					}
				}

				if( false == $objUtilityTransaction->insert( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction could not be inserted.', NULL ) );
					trigger_error( 'Client: ' . $this->getCid() . ' : Utility account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' :: Charge transaction could not be inserted. The ledger was not updated successfully.', E_USER_WARNING );
					$boolIsValid &= false;
				}
			}
		}

		// insert concessions keyed by remote primary key
		if( true == valArr( $arrobjStdConcessions ) ) {
			foreach( $arrobjStdConcessions as $objStdConcession ) {

				$objUtilityTransaction = getArrayElementByKey( \Psi\CStringService::singleton()->strtolower( $objStdConcession->remotePrimaryKey ), $arrobjUtilityTransactionsReKeyed );

				if( true == valObj( $objUtilityTransaction, 'CUtilityTransaction' ) ) {
					$fltTransactionAmountDue = ( float ) $objStdConcession->transactionAmountDue;

					if( 0 < abs( $fltTransactionAmountDue ) && $objUtilityTransaction->getCurrentAmount() != $fltTransactionAmountDue ) {
						$objUtilityTransaction->setCurrentAmount( $fltTransactionAmountDue );

						if( false == $objUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction failed to update.', NULL ) );
							trigger_error( 'Client: ' . $this->getCid() . ' : Utility account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' :: Charge transaction could not be updated. The ledger was not updated successfully.', E_USER_WARNING );
							$boolIsValid &= false;
						}
					}

					unset( $arrobjUtilityTransactionsReKeyed[\Psi\CStringService::singleton()->strtolower( $objStdConcession->remotePrimaryKey )] );
					continue;
				}

				$objArCode = getArrayElementByKey( \Psi\CStringService::singleton()->strtolower( $objStdConcession->chargeCodeRemotePrimaryKey ), $arrobjArCodesReKeyed );

				if( false == valObj( $objArCode, 'CArCode' ) ) {
					$objArCode = $objClient->fetchArCodeByRemotePrimaryKeyByIntegrationDatabaseId( $objStdConcession->chargeCodeRemotePrimaryKey, $this->m_objLease->getIntegrationDatabaseId(), $objClientDatabase );
				}

				if( false == valObj( $objArCode, 'CArCode' ) ) {
					$objArCode = $objClient->fetchArCodeByNameByIntegrationDatabaseId( $objStdConcession->transactionMemo, $this->m_objLease->getIntegrationDatabaseId(), $objClientDatabase );
				}

				if( false == valObj( $objArCode, 'CArCode' ) ) {
					$objArCode = $objClient->fetchArCodeByNameByIntegrationDatabaseIdNull( $objStdConcession->transactionMemo, $objClientDatabase );
				}

				if( false == valObj( $objArCode, 'CArCode' ) && 0 == strlen( trim( $objStdConcession->transactionMemo ) ) ) {
					continue;
				}

				// Insert or Update ArCode for Concession
				if( false == valObj( $objArCode, 'CArCode' ) && false == $this->insertOrUpdateArCode( $objClient, $intCurrentUserId, $objClientDatabase, $objStdConcession->transactionMemo, $objStdConcession->transactionMemo ) ) {
						continue;
				}

				$objUtilityTransaction = $this->createUtilityTransaction( $objArCode );

				$objUtilityTransaction->setRemotePrimaryKey( $objStdConcession->remotePrimaryKey );
				$objUtilityTransaction->setTransactionDatetime( $objStdConcession->transactionDatetime );
				$objUtilityTransaction->setOriginalAmount( $objStdConcession->transactionAmount );
				$objUtilityTransaction->setUnadjustedAmount( $objStdConcession->transactionAmount );
				$objUtilityTransaction->setCurrentAmount( $objStdConcession->transactionAmount );
				$objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::CONVERGENT );
				$objUtilityTransaction->setMemo( ( true == isset( $objStdConcession->transactionMemo ) ) ? $objStdConcession->transactionMemo : NULL );

				if( true == valArr( $this->getScheduledCharges() ) ) {
					foreach( $this->getScheduledCharges() as $objScheduledCharge ) {
						if( false == is_null( $objScheduledCharge->getLastPostedOn() ) && date( 'm/d/Y', strtotime( $objStdConcession->transactionDatetime ) ) <= date( 'm/d/Y', strtotime( $objScheduledCharge->getLastPostedOn() ) ) && ( float ) ( $objStdConcession->transactionAmountApplied + $objStdConcession->transactionAmountDue ) == ( float ) $objScheduledCharge->getChargeAmount()
							&& $objScheduledCharge->getArCodeId() == $objUtilityTransaction->getArCodeId() && date( 'd', strtotime( $objStdConcession->transactionDatetime ) ) == date( 'd', strtotime( $objScheduledCharge->getLastPostedOn() ) ) ) {
							$objUtilityTransaction->setScheduledChargeId( $objScheduledCharge->getId() );
							break;
						}
					}
				}

				if( false == $objUtilityTransaction->insert( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Concession transaction could not be inserted.', NULL ) );
					trigger_error( 'Client: ' . $this->getCid() . ' : Utility Account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' :: Concession transaction could not be inserted. The ledger was not updated successfully.', E_USER_WARNING );
					return false;
				}
			}
		}

		if( true == valArr( $arrobjStdArTransactionPayments ) || true == valArr( $arrobjStdNegativePayments ) ) {

			if( false == valObj( $objPaymentArCode, 'CArCode' ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Payment charge code could not be retrieved from the database.', NULL ) );
				trigger_error( 'Client: ' . $this->getCid() . ' : Utility Account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' :: payment charge code could not be retrieved from the database. The ledger was not updated successfully.', E_USER_WARNING );
				return false;
			}
		}

		// insert payments keyed by remote primary key
		if( true == valArr( $arrobjStdArTransactionPayments ) ) {

			foreach( $arrobjStdArTransactionPayments as $objStdPayment ) {

				$objUtilityTransaction = getArrayElementByKey( \Psi\CStringService::singleton()->strtolower( $objStdPayment->remotePrimaryKey ), $arrobjUtilityTransactionsReKeyed );

				if( true == valObj( $objUtilityTransaction, 'CUtilityTransaction' ) ) {
					$fltTransactionAmountDue = ( float ) $objStdPayment->transactionAmountDue;

					if( 0 < abs( $fltTransactionAmountDue ) && $objUtilityTransaction->getCurrentAmount() != $fltTransactionAmountDue ) {
						$objUtilityTransaction->setCurrentAmount( $fltTransactionAmountDue );

						if( false == $objUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction failed to update.', NULL ) );
							trigger_error( 'Client: ' . $this->getCid() . ' : Utility account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' :: Charge transaction could not be updated. The ledger was not updated successfully.', E_USER_WARNING );
							$boolIsValid &= false;
						}
					}

					unset( $arrobjUtilityTransactionsReKeyed[\Psi\CStringService::singleton()->strtolower( $objStdPayment->remotePrimaryKey )] );
					continue;
				}

				$objUtilityTransaction = $this->createUtilityTransaction( $objPaymentArCode );

				$objUtilityTransaction->setRemotePrimaryKey( $objStdPayment->remotePrimaryKey );
				$objUtilityTransaction->setTransactionDatetime( $objStdPayment->transactionDatetime );
				$objUtilityTransaction->setOriginalAmount( $objStdPayment->transactionAmount * -1 );
				$objUtilityTransaction->setUnadjustedAmount( $objStdPayment->transactionAmount * -1 );
				$objUtilityTransaction->setCurrentAmount( $objStdPayment->transactionAmount * -1 );
				$objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::CONVERGENT );
				$objUtilityTransaction->setMemo( ( true == isset( $objStdPayment->transactionMemo ) && 0 < strlen( trim( $objStdPayment->transactionMemo ) ) ) ? trim( $objStdPayment->transactionMemo ) : 'Yardi Import' );

				if( true == isset( $objStdPayment->transactionMemo ) && 0 < strlen( trim( $objStdPayment->transactionMemo ) ) && false !== \Psi\CStringService::singleton()->stristr( $objStdPayment->transactionMemo, 'PSID' ) ) {
					$arrstrPaymentTransactionMemo = explode( ' ', $objStdPayment->transactionMemo );

					if( true == is_numeric( $arrstrPaymentTransactionMemo[2] ) ) {
						$objArPayment = $objClient->fetchArPaymentById( $arrstrPaymentTransactionMemo[2], $objClientDatabase );

						if( true == valObj( $objArPayment, 'CArPayment' ) ) {
							$objUtilityTransaction->setArPaymentId( $objArPayment->getId() );
						}
					}
				}

				if( true == valArr( $this->getScheduledCharges() ) ) {
					foreach( $this->getScheduledCharges() as $objScheduledCharge ) {
						if( false == is_null( $objScheduledCharge->getLastPostedOn() ) && date( 'm/d/Y', strtotime( $objStdPayment->transactionDatetime ) ) <= date( 'm/d/Y', strtotime( $objScheduledCharge->getLastPostedOn() ) ) && ( float ) ( $objStdPayment->transactionAmountApplied + $objStdPayment->transactionAmountDue ) == ( float ) $objScheduledCharge->getChargeAmount()
							&& $objScheduledCharge->getArCodeId() == $objUtilityTransaction->getArCodeId() && date( 'd', strtotime( $objStdPayment->transactionDatetime ) ) == date( 'd', strtotime( $objScheduledCharge->getLastPostedOn() ) ) ) {
							$objUtilityTransaction->setScheduledChargeId( $objScheduledCharge->getId() );
							break;
						}
					}
				}

				if( false == $objUtilityTransaction->insert( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Payment transaction could not be inserted.', NULL ) );
					trigger_error( 'Client: ' . $this->getCid() . ' : Utility Account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' :: Payment transaction could not be inserted. The ledger was not updated successfully.', E_USER_WARNING );
					return false;
				}
			}
		}

		// insert charges keyed by remote primary key
		if( true == valArr( $arrobjStdNegativePayments ) ) {

			foreach( $arrobjStdNegativePayments as $objStdNegativePaymentCharge ) {

				$objUtilityTransaction = getArrayElementByKey( \Psi\CStringService::singleton()->strtolower( $objStdNegativePaymentCharge->remotePrimaryKey ), $arrobjUtilityTransactionsReKeyed );

				if( true == valObj( $objUtilityTransaction, 'CUtilityTransaction' ) ) {
					$fltTransactionAmountDue = ( float ) $objStdNegativePaymentCharge->transactionAmountDue;

					if( 0 < abs( $fltTransactionAmountDue ) && $objUtilityTransaction->getCurrentAmount() != $fltTransactionAmountDue ) {
						$objUtilityTransaction->setCurrentAmount( $fltTransactionAmountDue );

						if( false == $objUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction failed to update.', NULL ) );
							trigger_error( 'Client: ' . $this->getCid() . ' : Utility account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' :: Charge transaction could not be updated. The ledger was not updated successfully.', E_USER_WARNING );
							$boolIsValid &= false;
						}
					}

					unset( $arrobjUtilityTransactionsReKeyed[\Psi\CStringService::singleton()->strtolower( $objStdNegativePaymentCharge->remotePrimaryKey )] );
					continue;
				}

				$objUtilityTransaction = $this->createUtilityTransaction( $objPaymentArCode );

				$objUtilityTransaction->setRemotePrimaryKey( $objStdNegativePaymentCharge->remotePrimaryKey );
				$objUtilityTransaction->setTransactionDatetime( $objStdNegativePaymentCharge->transactionDatetime );
				$objUtilityTransaction->setOriginalAmount( abs( $objStdNegativePaymentCharge->transactionAmount ) );
				$objUtilityTransaction->setUnadjustedAmount( abs( $objStdNegativePaymentCharge->transactionAmount ) );
				$objUtilityTransaction->setCurrentAmount( abs( $objStdNegativePaymentCharge->transactionAmount ) );
				$objUtilityTransaction->setUtilityTransactionTypeId( CUtilityTransactionType::CONVERGENT );
				$objUtilityTransaction->setMemo( $objStdNegativePaymentCharge->transactionMemo );

				if( true == valArr( $this->getScheduledCharges() ) ) {
					foreach( $this->getScheduledCharges() as $objScheduledCharge ) {
						if( false == is_null( $objScheduledCharge->getLastPostedOn() ) && date( 'm/d/Y', strtotime( $objStdNegativePaymentCharge->transactionDatetime ) ) <= date( 'm/d/Y', strtotime( $objScheduledCharge->getLastPostedOn() ) ) && ( float ) ( $objStdNegativePaymentCharge->transactionAmountApplied + $objStdNegativePaymentCharge->transactionAmountDue ) == ( float ) $objScheduledCharge->getChargeAmount()
							&& $objScheduledCharge->getArCodeId() == $objUtilityTransaction->getArCodeId() && date( 'd', strtotime( $objStdNegativePaymentCharge->transactionDatetime ) ) == date( 'd', strtotime( $objScheduledCharge->getLastPostedOn() ) ) ) {
							$objUtilityTransaction->setScheduledChargeId( $objScheduledCharge->getId() );
							break;
						}
					}
				}

				if( false == $objUtilityTransaction->insert( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction could not be inserted.', NULL ) );
					trigger_error( 'Client: ' . $this->getCid() . ' : Utility Account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' :: Charge transaction could not be inserted. The ledger was not updated successfully.', E_USER_WARNING );
					return false;
				}
			}
		}

		// Update unprocessed utility transactions - The transaction which we have exported to external system but didn't received in next call - It must be closed in external system.
		// Closing it at our end.
		if( true == valArr( $arrobjUtilityTransactionsReKeyed ) ) {
			foreach( $arrobjUtilityTransactionsReKeyed as $objUtilityTransaction ) {
				if( 0 != $objUtilityTransaction->getCurrentAmount() ) {
					$objUtilityTransaction->setCurrentAmount( 0 );

					if( false == $objUtilityTransaction->update( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Charge transaction failed to update.', NULL ) );
						trigger_error( 'Client: ' . $this->getCid() . ' : Utility account ( ' . $this->getRemotePrimaryKey() . ' ) [Property : ' . $this->m_objProperty->getPropertyName() . ' ID : ' . $this->m_objProperty->getId() . '] Key : ' . $this->getRemotePrimaryKey() . ' Name: ' . $this->getNameFirst() . ' ' . $this->getNameLast() . ' :: Charge transaction could not be updated. The ledger was not updated successfully.', E_USER_WARNING );
						$boolIsValid &= false;
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function insertOrUpdateArCode( $objClient, $intCurrentUserId, $objClientDatabase, $strName, $strDescription ) {

		$objArCode = $objClient->createArCode();
		$objArCode->setIntegrationDatabaseId( $this->m_objLease->getIntegrationDatabaseId() );
		$objArCode->setName( $strName );
		$objArCode->setDescription( $strDescription );
		$objArCode->setCreditGlAccountId( $this->m_objRentGlAccount->getId() );

		$objDefaultDebitGlAccount = CGlAccounts::fetchGlAccountBySystemCodeByGlAccountUsageTypeIdByCid( CGlAccount::SYSTEM_CODE_ACCOUNTS_RECEIVABLE, CGlAccountUsageType::ACCOUNTS_RECEIVABLE, $this->getCid(), $objClientDatabase );

		if( false == valObj( $objDefaultDebitGlAccount, 'CGlAccount' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Default debit gl account is not set.', NULL ) );
			trigger_error( 'Client: ' . $this->getCid() . ' : Default debit gl account is not set.', E_USER_WARNING );
			return false;
		}

		$objArCode->setDebitGlAccountId( $objDefaultDebitGlAccount->getId() );
		$objArCode->setArCodeSummaryTypeId( CArCodeSummaryType::NO_SUMMARY );
		$objArCode->setPriorityNum( \Psi\Eos\Entrata\CArCodes::createService()->fetchMaxPriorityNumByCid( $objClient->getId(), $objClientDatabase ) );

		// Please be careful with the string sequnce in below array, below in a loop we have break point at two level.
		$arrstrNameTypes = [ 'deposit', 'rent', 'concession', 'pet', 'garage', 'garbage', 'gas', 'water', 'electricity', 'energy', 'sewer', 'pest', 'trash', 'rubbish', 'permit', 'parking', 'furniture', 'carport', 'storage', 'washer', 'dryer' ];

		$intArOriginId		= CArOrigin::BASE;
		$intArCodeTypeId	= CArCodeType::OTHER_INCOME;
		$intGlAccountTypeId	= CGlAccountType::INCOME;

		foreach( $arrstrNameTypes as $strNameType ) {

			if( false === \Psi\CStringService::singleton()->strripos( $strName, $strNameType ) ) {
				continue;
			}

			switch( $strNameType ) {
				case 'pet':
					$intArOriginId		= CArOrigin::PET;
					break 2;

				case 'garage':
				case 'permit':
				case 'parking':
				case 'furniture':
				case 'carport':
				case 'storage':
				case 'washer':
				case 'dryer':
					$intArOriginId		= CArOrigin::ADD_ONS;
					break 2;

				case 'concession':
					$intArOriginId		= CArOrigin::SPECIAL;
					break 2;

				case 'rent':
					$intArCodeTypeId	= CArCodeType::RENT;
					break 2;

				case 'deposit':
					$intArCodeTypeId	= CArCodeType::DEPOSIT;
					$intGlAccountTypeId	= CGlAccountType::LIABILITIES;
					break 2;

				default:
					$intArCodeTypeId	= CArCodeType::OTHER_INCOME;
					$intGlAccountTypeId	= CGlAccountType::INCOME;
					$intArOriginId		= CArOrigin::BASE;
					break;
			}
		}

		$objArCode->setArOriginId( $intArOriginId );
		$objArCode->setArCodeTypeId( $intArCodeTypeId );
		$objArCode->setGlAccountTypeId( $intGlAccountTypeId );

		if( true == \Psi\CStringService::singleton()->stristr( $objArCode->getName(), 'Dep' ) || 'dep' == \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->substr( $objArCode->getName(), 0, 3 ) ) || 'SD' == $objArCode->getArCodeTypeId() ) {
			$objArCode->setCreditGlAccountId( $this->m_objDepositGlAccount->getId() );
			$objArCode->setGlAccountTypeId( CGlAccountType::LIABILITIES );
			$objArCode->setArCodeTypeId( CArCodeType::DEPOSIT );
		}

		if( false == $objArCode->insert( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Ar Code ( ' . $objStdConcession->transactionMemo . ' ) failed to insert.', NULL ) );
			trigger_error( 'Client: ' . $this->getCid() . ' : Ar Code ( ' . $objStdConcession->transactionMemo . ' ) failed to insert.', E_USER_WARNING );
			return false;
		}

		return true;
	}

}
?>