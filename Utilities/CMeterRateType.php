<?php

class CMeterRateType extends CBaseMeterRateType {

	const AVERAGE_ALLOCATION	= 1;
	const FULL_ALLOCATION		= 2;
	const RATE_ALLOCATION		= 3;
	const BASELINE_RATE			= 4;

	public static function getStaticMeterRateTypeName( $intMeterRateTypeId ) {

		switch( $intMeterRateTypeId ) {
			case self::AVERAGE_ALLOCATION:
				$strMeterRateTypeName = 'Average Allocation';
				break;

			case self::FULL_ALLOCATION:
				$strMeterRateTypeName = 'Full Allocation';
				break;

			case self::RATE_ALLOCATION:
				$strMeterRateTypeName = 'Rate Allocation';
				break;

			case self::BASELINE_RATE:
				$strMeterRateTypeName = 'Baseline Allocation';
				break;

			default:
				// no default case
		}

		return $strMeterRateTypeName;
	}

}
?>