<?php

class CUtilityBillUnit extends CBaseUtilityBillUnit {

	public function valPropertyUnitId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getPropertyUnitId() ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;

	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
					$boolIsValid &= $this->valPropertyUnitId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>