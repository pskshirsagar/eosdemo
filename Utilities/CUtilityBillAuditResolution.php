<?php

class CUtilityBillAuditResolution extends CBaseUtilityBillAuditResolution {

	const BILL_DATA_ENTRY_ERROR							 = 1;
	const BILL_ACCOUNT_SETUP_INCORRECT					 = 2;
	const NEW_SERVICE_PERFORMED							 = 3;
	const DUPLICATE_INVOICE								 = 4;
	const CORRECTED_FINAL_INVOICE_NO_NEW_CHARGES_CREDITS = 5;
	const CORRECTED_FINAL_INVOICE_NEW_CHARGES_CREDITS	 = 6;
	const PROVIDER_ERROR_IMMEDIATE_CREDIT				 = 7;
	const PROVIDER_ERROR_FUTURE_CREDIT					 = 8;
	const PROVIDER_ERROR_NEW_INVOICE_ISSUED				 = 9;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillAuditTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsClientFacing() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>