<?php

class CUtilityTransmissionMeterError extends CBaseUtilityTransmissionMeterError {

	protected $m_strMeterErrorTypeName;

    /**
     * Get Functions
     */

    public function getMeterErrorTypeName() {
    	return $this->m_strMeterErrorTypeName;
    }

    /**
     * Set Functions
     */

    public function setMeterErrorTypeName( $strMeterErrorTypeName ) {
    	$this->m_strMeterErrorTypeName = $strMeterErrorTypeName;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['meter_error_type_name'] ) ) {
    		$this->setMeterErrorTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['meter_error_type_name'] ) : $arrmixValues['meter_error_type_name'] );
	    }
    	return true;
    }

    /**
     * Validate Functions
     */

    public function valId() {
        return true;
    }

    public function valUtilityTransmissionDetailId() {
        return true;
    }

    public function valMeterErrorTypeId() {
	    return true;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>