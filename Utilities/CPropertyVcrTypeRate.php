<?php

class CPropertyVcrTypeRate extends CBasePropertyVcrTypeRate {

	const VCR_NOTICE_TEXT_LENGTH = 1547;

	protected $m_intPropertyUtilityTypeId;

	/**
	 * Get Methods
	 */
	public function getPropertyUtilityTypeId() {
		return $this->m_intPropertyUtilityTypeId;
	}

	/**
	 * Set Methods
	 */

	public function setPropertyUtilityTypeId( $intPropertyUtilityTypeId ) {
		$this->m_intPropertyUtilityTypeId = $intPropertyUtilityTypeId;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 == $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Company id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) || 0 == $this->getPropertyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyVcrSettingId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyVcrSettingId() ) || 0 == $this->getPropertyVcrSettingId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_vcr_setting_id', 'Property VCR setting id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRapidVcrServiceFeeTypeId() : bool {
		$boolIsValid = true;

		if( true == is_null( $this->getRapidVcrServiceFeeTypeId() ) || 0 == $this->getRapidVcrServiceFeeTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_vcr_service_fee_type_id', 'Rapid VCR service fee type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFirstVcrServiceFeeTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFirstVcrServiceFeeTypeId() ) || 0 == $this->getFirstVcrServiceFeeTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_vcr_service_fee_type_id', 'First VCR service fee type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valGraceDays() {
		$boolIsValid = true;

		if( true == is_null( $this->getGraceDays() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'grace_days', 'Grace days is required.' ) );
		}
		if( false == preg_match( '/^[0-9]*$/', $this->getGraceDays() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'grace_days', 'Invalid grace days.' ) );
		}

		return $boolIsValid;
	}

	public function valWaiveUsageDuringGraceDays() {
		$boolIsValid = true;

		if( true == is_null( $this->getRapidLeaseDefinitionDays() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_lease_definition_days', 'Rapid lease definition days is required.' ) );
		}
		if( false == preg_match( '/^[0-9]*$/', $this->getRapidLeaseDefinitionDays() ) || 29 < $this->getRapidLeaseDefinitionDays() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_lease_definition_days', 'Rapid lease definition days should be less than 28 days.' ) );
		}

		return $boolIsValid;
	}

	public function valFirstBillVcrSeparately() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillConsecutiveVcrViolations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRapidLeaseDefinitionDays() {
		$boolIsValid = true;

		if( true == is_null( $this->getRapidLeaseDefinitionDays() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_lease_definition_days', 'Rapid lease definition days is required.' ) );
		}
		if( false == preg_match( '/^[0-9]*$/', $this->getRapidLeaseDefinitionDays() ) || 29 < $this->getRapidLeaseDefinitionDays() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_lease_definition_days', 'Rapid lease definition days should be less than 28 days.' ) );
		}

		return $boolIsValid;
	}

	public function valMoveOutEstimatedChargesDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUsageWaiverAmount() {
		$boolIsValid = true;

		if( false == is_null( $this->getUsageWaiverAmount() ) && ( false == preg_match( '/^[0-9.]*$/', $this->getUsageWaiverAmount() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'usage_waiver_amount', 'Invalid usage waiver amount.' ) );
		}

		return $boolIsValid;
	}

	public function valFirstFixedServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::FIXED_AMOUNT == $this->getFirstVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getFirstFixedServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_fixed_service_fee', 'First fixed service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getFirstFixedServiceFee() ) || 0 == $this->getFirstFixedServiceFee() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_fixed_service_fee', 'Invalid first fixed service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valFirstDay1ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getFirstVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getFirstDay1ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day1_service_fee', 'First day 1 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getFirstDay1ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day1_service_fee', 'Invalid first day 1 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valFirstDay2ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getFirstVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getFirstDay2ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day2_service_fee', 'First day 2 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getFirstDay2ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day2_service_fee', 'Invalid first day 2 service fee.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valFirstDay3ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getFirstVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getFirstDay3ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day3_service_fee', 'First day 3 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getFirstDay3ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day3_service_fee', 'Invalid first day 3 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valFirstDay4ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getFirstVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getFirstDay4ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day4_service_fee', 'First day 4 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getFirstDay4ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day4_service_fee', 'Invalid first day 4 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valFirstDay5ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getFirstVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getFirstDay5ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day5_service_fee', 'First day 5 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getFirstDay5ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day5_service_fee', 'Invalid first day 5 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valFirstDay6ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getFirstVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getFirstDay6ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day6_service_fee', 'First day 6 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getFirstDay6ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day6_service_fee', 'Invalid first day 6 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valFirstDay7ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getFirstVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getFirstDay7ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day7_service_fee', 'First day 7 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getFirstDay7ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day7_service_fee', 'Invalid first day 7 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRapidFixedServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::FIXED_AMOUNT == $this->getRapidVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getRapidFixedServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_fixed_service_fee', 'Rapid fixed service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getRapidFixedServiceFee() ) || 0 == $this->getRapidFixedServiceFee() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_fixed_service_fee', 'Invalid Rapid fixed service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRapidDay1ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getRapidVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getRapidDay1ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day1_service_fee', 'Rapid day 1 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getRapidDay1ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day1_service_fee', 'Invalid Rapid day 1 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRapidDay2ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getRapidVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getRapidDay2ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day2_service_fee', 'Rapid day 2 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getRapidDay2ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day2_service_fee', 'Invalid Rapid day 2 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRapidDay3ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getRapidVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getRapidDay3ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day3_service_fee', 'Rapid day 3 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getRapidDay3ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day3_service_fee', 'Invalid Rapid day 3 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRapidDay4ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getRapidVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getRapidDay4ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day4_service_fee', 'Rapid day 4 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getRapidDay4ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day4_service_fee', 'Invalid Rapid day 4 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRapidDay5ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getRapidVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getRapidDay5ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day5_service_fee', 'Rapid day 5 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getRapidDay5ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day5_service_fee', 'Invalid Rapid day 5 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRapidDay6ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getRapidVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getRapidDay6ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day6_service_fee', 'Rapid day 6 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getRapidDay6ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day6_service_fee', 'Invalid Rapid day 6 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRapidDay7ServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_PENALTY == $this->getRapidVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getRapidDay7ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day7_service_fee', 'Rapid day 7 service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getRapidDay7ServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Rapid_day7_service_fee', 'Invalid Rapid day 7 service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valFirstDailyServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_AMOUNT == $this->getFirstVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getFirstDailyServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_daily_service_fee', 'First daily service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getFirstDailyServiceFee() ) || 0 == $this->getFirstDailyServiceFee() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_daily_service_fee', 'Invalid first daily service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRapidDailyServiceFee() {

		$boolIsValid = true;

		if( CVcrServiceFeeType::DAILY_AMOUNT == $this->getRapidVcrServiceFeeTypeId() ) {
			if( true == is_null( $this->getRapidDailyServiceFee() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_daily_service_fee', 'Rapid daily service fee is required.' ) );
			} elseif( false == preg_match( '/^[0-9.]*$/', $this->getRapidDailyServiceFee() ) || 0 == $this->getRapidDailyServiceFee() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rapid_daily_service_fee', 'Invalid Rapid daily service fee.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valMoveOutChargeEstimationDays() {
		$boolIsValid = true;
		if( false == $this->getIsHideMoveOutEstimatedCharges() ) {
			if( true == is_null( $this->getMoveOutEstimatedChargesDays() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_charge_estimation_days', 'Move out charge estimation days is required.' ) );
			}
			if( false == preg_match( '/^[0-9]*$/', $this->getRapidLeaseDefinitionDays() ) || 1 > $this->getMoveOutEstimatedChargesDays() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_charge_estimation_days', 'Move out charge estimation days should be between 1 to 99 day(s).' ) );
			}
		}
		return $boolIsValid;
	}

	public function valVcrNoticeText() {
		$boolIsValid = true;

		$intStart = \Psi\CStringService::singleton()->strpos( $this->getVcrNoticeText(), '<' );

		$strVcrNoticeText = \Psi\CStringService::singleton()->substr( $this->getVcrNoticeText(), $intStart );
		$strVcrNoticeText = str_replace( [ '<br />', '<br/>' ], '', $strVcrNoticeText );
		// xml requires one root node
		$strVcrNoticeText = '<div>' . $strVcrNoticeText . '</div>';

		libxml_use_internal_errors( true );
		libxml_clear_errors();
		simplexml_load_string( $strVcrNoticeText );

		if( 0 != \Psi\Libraries\UtilFunctions\count( libxml_get_errors() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vcr_notice_text', 'VCR Notice Text should contain valid HTML tags.' ) );
		}

		if( true == $boolIsValid && self::VCR_NOTICE_TEXT_LENGTH < \Psi\CStringService::singleton()->strlen( strip_tags( $this->getVcrNoticeText() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vcr_notice_text', 'VCR Notice Text should not be greater than ' . self::VCR_NOTICE_TEXT_LENGTH . ' characters.' ) );
		}

		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_begin_date', 'Service begin date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getEndDate() ) && strtotime( $this->getStartDate() ) >= strtotime( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_end_date', 'Service end date must be greater than service begin date.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFirstVcrServiceFeeTypeId();
				$boolIsValid &= $this->valRapidVcrServiceFeeTypeId();
//				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valUsageWaiverAmount();
				$boolIsValid &= $this->valGraceDays();
				$boolIsValid &= $this->valRapidLeaseDefinitionDays();
				$boolIsValid &= $this->valFirstFixedServiceFee();
				$boolIsValid &= $this->valFirstDay1ServiceFee();
				$boolIsValid &= $this->valFirstDay2ServiceFee();
				$boolIsValid &= $this->valFirstDay3ServiceFee();
				$boolIsValid &= $this->valFirstDay4ServiceFee();
				$boolIsValid &= $this->valFirstDay5ServiceFee();
				$boolIsValid &= $this->valFirstDay6ServiceFee();
				$boolIsValid &= $this->valFirstDay7ServiceFee();
				$boolIsValid &= $this->valRapidFixedServiceFee();
				$boolIsValid &= $this->valRapidDay1ServiceFee();
				$boolIsValid &= $this->valRapidDay2ServiceFee();
				$boolIsValid &= $this->valRapidDay3ServiceFee();
				$boolIsValid &= $this->valRapidDay4ServiceFee();
				$boolIsValid &= $this->valRapidDay5ServiceFee();
				$boolIsValid &= $this->valRapidDay6ServiceFee();
				$boolIsValid &= $this->valRapidDay7ServiceFee();
				$boolIsValid &= $this->valFirstDailyServiceFee();
				$boolIsValid &= $this->valRapidDailyServiceFee();
				$boolIsValid &= $this->valMoveOutChargeEstimationDays();
				$boolIsValid &= $this->valVcrNoticeText();
				break;

			case 'validate_property_vcr_type_rate_in_utility_batch':
				// $boolIsValid &= $this->valRecoveryFee();
				$boolIsValid &= $this->valUsageWaiverAmount();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_utility_type_id'] ) ) {
			$this->setPropertyUtilityTypeId( $arrmixValues['property_utility_type_id'] );
		}
	}

}
?>