<?php

class CUtilityAccountAddress extends CBaseUtilityAccountAddress {

	protected $m_objPropertyId;
	protected $m_objPropertyUnitId;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['property_unit_id'] ) ) {
			$this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		}
	}

    public function setPropertyId( $intPropertyUtilityId ) {
    	$this->m_objPropertyId = $intPropertyUtilityId;
    }

    public function setPropertyUnitId( $intPropertyUnitId ) {
    	$this->m_objPropertyUnitId = $intPropertyUnitId;
    }

    /**
     * Get Functions
     */

    public function getPropertyId() {
    	return $this->m_objPropertyId;
    }

    public function getPropertyUnitId() {
    	return $this->m_objPropertyUnitId;
    }

}
?>