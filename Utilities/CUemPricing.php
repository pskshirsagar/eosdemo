<?php

class CUemPricing extends CBaseUemPricing {

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPerInvoiceAccountAmount() {
		$boolIsValid = true;
		if( true == is_null( $this->getPerInvoiceAccountAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'per_ru_invoice_amount', 'Per Account Charge is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPerRuInvoiceAmount() {
		$boolIsValid = true;

		if( true == $this->getIsPerRuInvoicePricing() && true == is_null( $this->getPerRuInvoiceAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'per_ru_invoice_amount', 'Per RU Invoice Charge is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPerInvoiceAccountAmount();
				$boolIsValid &= $this->valPerRuInvoiceAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		$arrstrOriginalValues = $arrstrOriginalValueChanges = [];
		$boolChangeFlag				= false;

		$objRuPricing = new CUemPricing();
		$arrmixOriginalValues = fetchData( 'SELECT * FROM uem_pricings WHERE id = ' . ( int ) $this->getId(), $objDatabase );

		$arrstrTempOriginalValues = $arrmixOriginalValues[0];

		foreach( $arrstrTempOriginalValues as $strKey => $mixOriginalValue ) {

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strGetFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strSetFunctionName = 'set' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			$objRuPricing->$strSetFunctionName( $mixOriginalValue );

			if( $this->$strGetFunctionName() != $objRuPricing->$strGetFunctionName() ) {
				$arrstrOriginalValueChanges[$strKey] 	= $this->$strSqlFunctionName();
				$arrstrOriginalValues[$strKey] 			= $objRuPricing->$strSqlFunctionName();
				$boolChangeFlag 						= true;
			}
		}

		$boolUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'uem_pricings' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		return $boolUpdateStatus;

	}

}

?>