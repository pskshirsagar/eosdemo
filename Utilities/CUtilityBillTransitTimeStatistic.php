<?php

class CUtilityBillTransitTimeStatistic extends CBaseUtilityBillTransitTimeStatistic {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityProviderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityBillReceiptTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAverageTransitTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransitTimeStandardDeviation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>