<?php

class CUtilityRubsFormula extends CBaseUtilityRubsFormula {

	const FORMULA_100_OCCUPANCY 				  	     = 1;
	const FORMULA_100_SQUARE_FOOTAGE 			  	     = 2;
	const FORMULA_50_OCCUPANCY_50_SQUARE_FOOTAGE 	     = 3;
	const BEDROOM_WEIGHTED 				    		 	 = 4;
	const OCCUPANCY_WEIGHTED 				    		 = 5;
	const UNIT_WEIGHTED 					   	 		 = 6;
	const UNIT_TYPE_WEIGHTED 				   	 	 	 = 7;
	const MANUAL			 				    	 	 = 8;
	const PER_UNIT_BILLING			 	 	 			 = 9;
	const FLAT_FEE			 				     	 	 = 10;
	const DIRECT_METERED	 				     	 	 = 11;
	const DIRECT_METERED_SHOW_METER_DATA	     		 = 12;
	const SQUARE_FOOTAGE_WEIGHTED               		 = 13;
	const FORMULA_50_OCCUPANCY_WEIGHT_50_SQUARE_FOOTAGE	 = 14;
	const CUSTOM										 = 15;
	const BEDROOM_FIXED                                  = 16;
	const OCCUPANCY_FIXED                                = 17;
	const UNIT_TYPE_FIXED                                = 18;
	const SQUARE_FOOTAGE_FIXED                           = 19;
	const FORMULA_50_OCCUPANCY_FIXED_50_SQUARE_FOOTAGE   = 20;

	// Neeed to remove these 2 constants later on as getting used for test data( populate_test_data )
	const WATER_DEFAULT = 1;
	const ELECTRIC_DEFAULT = 2;

	public static $c_arrintNotShowInBatchFactorFormulae = [
		self::PER_UNIT_BILLING,
		self::FLAT_FEE,
		self::DIRECT_METERED,
		self::DIRECT_METERED_SHOW_METER_DATA
	];

	public static $c_arrintOccupancyFormulae = [
		self::FORMULA_100_OCCUPANCY,
		self::FORMULA_50_OCCUPANCY_50_SQUARE_FOOTAGE,
		self::FORMULA_50_OCCUPANCY_WEIGHT_50_SQUARE_FOOTAGE,
		self::OCCUPANCY_WEIGHTED
	];

	public static $c_arrintDirectMeteredRubsFormulaIds = [
		self::DIRECT_METERED,
		self::DIRECT_METERED_SHOW_METER_DATA
	];

	public static $c_arrintFixedFormulae = [
		self::BEDROOM_FIXED,
		self::OCCUPANCY_FIXED,
		self::UNIT_TYPE_FIXED,
		self::SQUARE_FOOTAGE_FIXED,
		self::FORMULA_50_OCCUPANCY_FIXED_50_SQUARE_FOOTAGE
	];
}

?>