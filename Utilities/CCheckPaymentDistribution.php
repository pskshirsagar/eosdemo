<?php

class CCheckPaymentDistribution extends CBaseCheckPaymentDistribution {

    public function valId() {
	    return true;
    }

    public function valCheckPaymentDistributionStepId() {
	    return true;
    }

    public function valPrintedOn() {
	    return true;
    }

    public function valMailedOn() {
	    return true;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>