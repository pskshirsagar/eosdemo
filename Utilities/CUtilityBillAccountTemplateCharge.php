<?php

class CUtilityBillAccountTemplateCharge extends CBaseUtilityBillAccountTemplateCharge {

	protected $m_boolIsMetered;

	protected $m_strMeterNumber;
	protected $m_strAccountNumber;

    public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Utility Bill Account Template Charges id does not appear valid.' ) );
    	}

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client id does not appear valid.' ) );
    	}

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

     	if( false == isset( $this->m_intPropertyId ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'proeprty_id', 'Property id does not appear valid.' ) );
    	}

        return $boolIsValid;
    }

    public function valUtilityBillAccountId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intUtilityBillAccountId ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_account_id', 'Utility Bill Account id does not appear valid.' ) );
    	}

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId();
            	break;

            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valUtilityBillAccountId();
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = false ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return $this->update( $intCurrentUserId, $objDatabase );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['account_number'] ) ) {
			$this->setAccountNumber( $arrmixValues['account_number'] );
		}

		if( true == isset( $arrmixValues['meter_number'] ) ) {
			$this->setMeterNumber( $arrmixValues['meter_number'] );
		}

		if( true == isset( $arrmixValues['is_metered'] ) ) {
			$this->setIsMetered( trim( stripcslashes( $arrmixValues['is_metered'] ) ) );
		}
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = $strAccountNumber;
	}

	public function setMeterNumber( $strMeterNumber ) {
		$this->m_strMeterNumber = $strMeterNumber;
	}

	public function setIsMetered( $boolIsMetered ) {
		$this->m_boolIsMetered = CStrings::strToBool( $boolIsMetered );
	}

	public function setMeterNumbers( $arrstrMeterNumbers ) {
		$this->m_arrstrMeterNumbers = $arrstrMeterNumbers;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getMeterNumber() {
		return $this->m_strMeterNumber;
	}

	public function getIsMetered() {
		return $this->m_boolIsMetered;
	}

	/**
	 * @return array
	 */
	public function getMeterNumbers() : array {
		return $this->m_arrstrMeterNumbers;
	}

    public function valCommodityId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

}
?>