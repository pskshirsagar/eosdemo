<?php

class CMissingBillAlertEvent extends CBaseMissingBillAlertEvent {

	const CONTACTED_PROVIDER_NO_RESOLUTION	= 1;
	const CONTACTED_PROVIDER_PENDING		= 2;
	const CONTACTED_CLIENT_NO_RESOLUTION	= 3;
	const CONTACTED_CLIENT_PENDING			= 4;
	const RESOLVED							= 5;
	const RESOLVED_WITHOUT_INVOICE			= 6;
	const OTHER								= 7;
	const RESOLVED_RECEVIED_TODAY			= 8;
	const MISSING_BILL_ALERT_NEW			= 9;
	const BILL_UPLOADED			            = 10;

	public static function populateTemplateConstants( $arrmixTemplateParameters ) {
		$arrmixTemplateParameters['MISSING_BILL_ALERT_EVENT_CONTACTED_PROVIDER_NO_RESOLUTION']	= self::CONTACTED_PROVIDER_NO_RESOLUTION;
		$arrmixTemplateParameters['MISSING_BILL_ALERT_EVENT_CONTACTED_PROVIDER_PENDING'] 		= self::CONTACTED_PROVIDER_PENDING;
		$arrmixTemplateParameters['MISSING_BILL_ALERT_EVENT_CONTACTED_CLIENT_NO_RESOLUTION'] 	= self::CONTACTED_CLIENT_NO_RESOLUTION;
		$arrmixTemplateParameters['MISSING_BILL_ALERT_EVENT_CONTACTED_CLIENT_PENDING'] 			= self::CONTACTED_CLIENT_PENDING;
		$arrmixTemplateParameters['MISSING_BILL_ALERT_EVENT_RESOLVED']							= self::RESOLVED;
		$arrmixTemplateParameters['MISSING_BILL_ALERT_EVENT_RESOLVED_WITHOUT_INVOICE']			= self::RESOLVED_WITHOUT_INVOICE;
		$arrmixTemplateParameters['MISSING_BILL_ALERT_EVENT_OTHER']								= self::OTHER;
		$arrmixTemplateParameters['RESOLVED_RECEVIED_TODAY']									= self::RESOLVED_RECEVIED_TODAY;
		$arrmixTemplateParameters['NEW']														= self::MISSING_BILL_ALERT_NEW;
		return $arrmixTemplateParameters;
	}

	public static function getMissingBillAlertEvents() {

		$arrmixMissingBillAlertEvents[self::CONTACTED_PROVIDER_NO_RESOLUTION]		= 'Contacted Provider No Resolution';
		$arrmixMissingBillAlertEvents[self::CONTACTED_PROVIDER_PENDING]				= 'Contacted Provider Pending';
		$arrmixMissingBillAlertEvents[self::CONTACTED_CLIENT_NO_RESOLUTION]			= 'Contacted Client No Resolution';
		$arrmixMissingBillAlertEvents[self::CONTACTED_CLIENT_PENDING]				= 'Contacted Client Pending';
		$arrmixMissingBillAlertEvents[self::RESOLVED]								= 'Resolved';
		$arrmixMissingBillAlertEvents[self::RESOLVED_WITHOUT_INVOICE]				= 'Resolved Without Invoice';
		$arrmixMissingBillAlertEvents[self::OTHER] 									= 'Other';
		$arrmixMissingBillAlertEvents[self::RESOLVED_RECEVIED_TODAY] 				= 'Resolved - Received today';
		$arrmixMissingBillAlertEvents[self::MISSING_BILL_ALERT_NEW] 				= 'New';
		return $arrmixMissingBillAlertEvents;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static $c_arrintDefaultMissingInvoiceAlerts = [
		self::OTHER,
		self::MISSING_BILL_ALERT_NEW,
		self::CONTACTED_CLIENT_PENDING,
		self::CONTACTED_PROVIDER_PENDING,
		self::CONTACTED_CLIENT_NO_RESOLUTION,
		self::CONTACTED_PROVIDER_NO_RESOLUTION
	];

	public static $c_arrintCompletedMissingBillAlertEventIds = [
		self::RESOLVED,
		self::RESOLVED_WITHOUT_INVOICE,
		self::RESOLVED_RECEVIED_TODAY
	];

	public static function getMissingInvoiceAlertEventContact( $strMissingInvoiceAlertEventType ) : int {

		switch( $strMissingInvoiceAlertEventType ) {

			case 'PROVIDER_PENDING':
				$intMissingBillAlertEventId = self::CONTACTED_PROVIDER_PENDING;
				break;

			case 'PROVIDER_NO_RESOLUTION':
				$intMissingBillAlertEventId = self::CONTACTED_PROVIDER_NO_RESOLUTION;
				break;

			case 'CLIENT_PENDING':
				$intMissingBillAlertEventId = self::CONTACTED_CLIENT_PENDING;
				break;

			case 'CLIENT_NO_RESOLUTION':
				$intMissingBillAlertEventId = self::CONTACTED_CLIENT_NO_RESOLUTION;
				break;

			default:
				$intMissingBillAlertEventId = self::CONTACTED_PROVIDER_PENDING;
				break;
		}

		return $intMissingBillAlertEventId;
	}

}
?>