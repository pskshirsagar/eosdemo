<?php

class CUtilityInvoiceBatch extends CBaseUtilityInvoiceBatch {

    public function valId() {
        return true;
    }

    public function valFileName() {
	    return true;
    }

    public function valFilePath() {
	    return true;
    }

    public function valPageCounts() {
	    return true;
    }

    public function valTransactionCounts() {
	    return true;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>