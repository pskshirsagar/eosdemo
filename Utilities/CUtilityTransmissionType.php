<?php

class CUtilityTransmissionType extends CBaseUtilityTransmissionType {

	const PROPERTY					= 1;
	const COLLECTOR					= 2;
	const UTILITY_COMPANY			= 3;
	const PSI						= 4;
	const INOVONICS_DATA_LOGGER		= 5;
	const INOVONICS_ECHO_STREAM		= 6;
	const HEXAGRAM					= 7;
	const SPEED_READ				= 8;
	const ESTIMATED					= 9;
	const MANUAL					= 10;
	const TAPWATCH_1				= 11;
	const TAPWATCH_2				= 12;
	const TAPWATCH_3				= 13;
	const TEHAMA					= 14;
	const NEXT_CENTURY				= 15;
	const CERENITI					= 16;

	public static function getUtilityTransmissionTypeName( $intUtilityTransmissionTypeId ) {

		$arrstrUtilityTransmissionTypeNames = [];

		$arrstrUtilityTransmissionTypeNames[self::PROPERTY] 				= 'Property';
		$arrstrUtilityTransmissionTypeNames[self::COLLECTOR] 				= 'Collector';
		$arrstrUtilityTransmissionTypeNames[self::UTILITY_COMPANY] 			= 'Utility Company';
		$arrstrUtilityTransmissionTypeNames[self::PSI]						= 'PSI';
		$arrstrUtilityTransmissionTypeNames[self::INOVONICS_DATA_LOGGER] 	= 'Inovonics Data Logger';
		$arrstrUtilityTransmissionTypeNames[self::INOVONICS_ECHO_STREAM] 	= 'Inovonics Echo Stream';
		$arrstrUtilityTransmissionTypeNames[self::HEXAGRAM] 				= 'Hexagram';
		$arrstrUtilityTransmissionTypeNames[self::SPEED_READ] 				= 'Speed Read';
		$arrstrUtilityTransmissionTypeNames[self::ESTIMATED] 				= 'Estimated';
		$arrstrUtilityTransmissionTypeNames[self::MANUAL]					= 'Manual';
		$arrstrUtilityTransmissionTypeNames[self::TAPWATCH_1] 				= 'Tapwatch 1';
		$arrstrUtilityTransmissionTypeNames[self::TAPWATCH_2] 				= 'Tapwatch 2';
		$arrstrUtilityTransmissionTypeNames[self::TAPWATCH_3] 				= 'Tapwatch 3';
		$arrstrUtilityTransmissionTypeNames[self::TEHAMA] 					= 'Tehama';
		$arrstrUtilityTransmissionTypeNames[self::NEXT_CENTURY] 			= 'Next Century';
		$arrstrUtilityTransmissionTypeNames[self::CERENITI] 				= 'Cereniti';
		if( true == array_key_exists( $intUtilityTransmissionTypeId, $arrstrUtilityTransmissionTypeNames ) ) {
			return $arrstrUtilityTransmissionTypeNames[$intUtilityTransmissionTypeId];
		}

		return NULL;
	}

}
?>