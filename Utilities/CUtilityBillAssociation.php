<?php

class CUtilityBillAssociation extends CBaseUtilityBillAssociation {

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityBillId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityBillId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_id', 'Utility Bill is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityBillAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityBillAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_account_id', 'Utility Bill Account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityBillAccountDetails() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityBillAccountDetails() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_account_details', 'Utility Bill Account Details is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valUtilityBillId();
				$boolIsValid &= $this->valUtilityBillAccountId();
				$boolIsValid &= $this->valUtilityBillAccountDetails();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>