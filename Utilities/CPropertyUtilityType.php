<?php

class CPropertyUtilityType extends CBasePropertyUtilityType {

	protected $m_objPropertyUtilityTypeRate;

	protected $m_arrobjPropertyUtilityTypeRates;

	protected $m_strEndDate;
	protected $m_strStartDate;
	protected $m_strStateCode;
	protected $m_strUtilityTypeName;
	protected $m_strLastTransmissionOn;
	protected $m_strUtilityCollectorName;
	protected $m_strUtilityBillPaymentTypeName;
	protected $m_strUtilityConsumptionTypeName;
	protected $m_strUtilityTransmissionReceiptTypeName;

	protected $m_boolIsTexas;
	protected $m_boolIsDisabled;
	protected $m_boolIsBaseFeeBorrowed;

	protected $m_intUtilityBillId;
	protected $m_intReadyBillsCount;
	protected $m_intLeaseBucketCount;
	protected $m_intUtilityBillAccountId;
	protected $m_intOutstandingBillsCount;
	protected $m_intPropertyVcrSettingsCount;
	protected $m_intUtilityBillAccountsCount;
	protected $m_intPropertyMeterSettingsCount;
	protected $m_intPropertyUtilityTypeRatesCount;

	protected $m_arrintUtilityTypeIds;
	protected $m_arrintUnconfiguredPvsPutIds;

	const VALIDATE_UPDATE_CUSTOM				= 'validate_update_custom';
	const VALIDATE_PROPERTY_UTILITY_TYPE_RATES	= 'validate_property_utility_type_rates';
	const VALIDATE_FRONT_END_VALIDATIONS		= 'validate_front_end_validations';
	const VALIDATE_GENERAL_BILLING_TAB			= 'validate_general_billing';
	const VALIDATE_FINANCIAL_TAB				= 'validate_general_billing';
	const VALIDATE_PUT_ON_SETTINGS				= 'validate_put_on_settings';

	const KEY_HAS_VCR_SERVICE					= 'has_vcr_service';
	const KEY_HAS_BILLING_SERVICE				= 'has_billing_service';
	const KEY_IS_SUBMETERED						= 'is_submetered';

	const PUT_TOTAL_VALIDATION_COUNT            = 20;
	const PUT_CONVERGENT_VALIDATION_COUNT       = 14;
	const PUT_BILL_BY_BUILDING_VALIDATION_COUNT = 1;

	const VACANT_UNIT_BILL_ALERT_AMOUNT         = 1.50;

	function __construct() {
		parent::__construct();

		$this->setIsBaseFeeBorrowed( false );

	}

	/**
	 * Create Functions
	 */

	public function createPropertyUtilityTypeRate() {

		$objPropertyUtilityTypeRate = new CPropertyUtilityTypeRate();
		$objPropertyUtilityTypeRate->setDefaults();
		$objPropertyUtilityTypeRate->setCid( $this->getCid() );
		$objPropertyUtilityTypeRate->setPropertyUtilityTypeId( $this->getId() );
		$objPropertyUtilityTypeRate->setPropertyUtilityType( $this );

		return $objPropertyUtilityTypeRate;
	}

	public function createPropertyMeteringSetting() {

		$objPropertyMeteringSetting = new CPropertyMeteringSetting();
		$objPropertyMeteringSetting->setCid( $this->getCid() );
		$objPropertyMeteringSetting->setPropertyUtilityTypeId( $this->getId() );

		return $objPropertyMeteringSetting;
	}

	public function createPropertyVcrSetting() {

		$objPropertyVcrSetting = new CPropertyVcrSetting();

		$objPropertyVcrSetting->setCid( $this->getCid() );
		$objPropertyVcrSetting->setPropertyId( $this->getPropertyId() );
		$objPropertyVcrSetting->setPropertyUtilityTypeId( $this->getId() );
		$objPropertyVcrSetting->setVacantUnitBillAlertAmount( self::VACANT_UNIT_BILL_ALERT_AMOUNT );

		$objPropertyVcrTypeRate = new CPropertyVcrTypeRate();

		$objPropertyVcrTypeRate->setCid( $this->getCid() );
		$objPropertyVcrTypeRate->setPropertyId( $this->getPropertyId() );
		$objPropertyVcrTypeRate->setFirstFixedServiceFee( '50.00' );
		$objPropertyVcrTypeRate->setFirstVcrServiceFeeTypeId( CVcrServiceFeeType::FIXED_AMOUNT );
		$objPropertyVcrTypeRate->setUsageWaiverAmount( '5.00' );
		$objPropertyVcrTypeRate->setGraceDays( '1' );
		$objPropertyVcrTypeRate->setRapidLeaseDefinitionDays( '1' );
		$objPropertyVcrTypeRate->setWaiveUsageDuringGraceDays( false );
		$objPropertyVcrTypeRate->setMoveOutEstimatedChargesDays( 15 );
		$objPropertyVcrTypeRate->setRapidVcrServiceFeeTypeId( CVcrServiceFeeType::FIXED_AMOUNT );
		$objPropertyVcrTypeRate->setRapidFixedServiceFee( '50.00' );

		$objPropertyVcrSetting->setPropertyVcrTypeRates( [ $objPropertyVcrTypeRate ] );

		return $objPropertyVcrSetting;
	}

	public function createUtilityBillAccountDetail() {

		$objUtilityBillAccountDetail = new CUtilityBillAccountDetail();
		$objUtilityBillAccountDetail->setCid( $this->getCid() );
		$objUtilityBillAccountDetail->setPropertyId( $this->getPropertyId() );

		return $objUtilityBillAccountDetail;
	}

	/**
	 * Get Functions
	 */

	public function getOutstandingBillsCount() {
		return $this->m_intOutstandingBillsCount;
	}

	public function getReadyBillsCount() {
		return $this->m_intReadyBillsCount;
	}

	public function getUtilityTypeName() {
		return $this->m_strUtilityTypeName;
	}

	public function getUtilityCollectorName() {
		return $this->m_strUtilityCollectorName;
	}

	public function getUtilityTransmissionReceiptTypeName() {
		return $this->m_strUtilityTransmissionReceiptTypeName;
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function getUtilityConsumptionTypeName() {
		return $this->m_strUtilityConsumptionTypeName;
	}

	public function getPropertyUtilityTypeRate() {
		return $this->m_objPropertyUtilityTypeRate;
	}

	public function getLastTransmissionOn() {
		return $this->m_strLastTransmissionOn;
	}

	public function getPropertyUtilityTypeRatesCount() {
		return $this->m_intPropertyUtilityTypeRatesCount;
	}

	public function getPropertyVcrSettingsCount() {
		return $this->m_intPropertyVcrSettingsCount;
	}

	public function getPropertyMeterSettingsCount() {
		return $this->m_intPropertyMeterSettingsCount;
	}

	public function getUtilityBillAccountsCount() {
		return $this->m_intUtilityBillAccountsCount;
	}

	public function getPropertyUtilityTypeRates() {
		return $this->m_arrobjPropertyUtilityTypeRates;
	}

	public function getUtilityBillId() {
		return $this->m_intUtilityBillId;
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function getLeaseBucketCount() {
		return $this->m_intLeaseBucketCount;
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function getIsTexas() {
		return $this->m_boolIsTexas;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getUtilityTypeIds() {
		return $this->m_arrintUtilityTypeIds;
	}

	public function getUnconfiguredPvsPutIds() {
		return $this->m_arrintUnconfiguredPvsPutIds;
	}

	/**
	 * Add Functions
	 */

	public function addPropertyUtilityTypeRate( $objPropertyUtilityTypeRate ) {
		$this->m_arrobjPropertyUtilityTypeRates[$objPropertyUtilityTypeRate->getId()] = $objPropertyUtilityTypeRate;
	}

	public function addPropertyUtilityTypeRates( $arrobjPropertyUtilityTypeRates ) {
		$this->m_arrobjPropertyUtilityTypeRates = $arrobjPropertyUtilityTypeRates;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {

		$this->setHasVcrService( false );
		$this->setIsSubmetered( false );

	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_type_name'] ) ) {
			$this->setUtilityTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_type_name'] ) : $arrmixValues['utility_type_name'] );
		}
		if( true == isset( $arrmixValues['utility_collector_name'] ) ) {
			$this->setUtilityCollectorName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_collector_name'] ) : $arrmixValues['utility_collector_name'] );
		}
		if( true == isset( $arrmixValues['utility_transmission_receipt_type_name'] ) ) {
			$this->setUtilityTransmissionReceiptTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_transmission_receipt_type_name'] ) : $arrmixValues['utility_transmission_receipt_type_name'] );
		}
		if( true == isset( $arrmixValues['utility_consumption_type_name'] ) ) {
			$this->setUtilityConsumptionTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_consumption_type_name'] ) : $arrmixValues['utility_consumption_type_name'] );
		}
		if( true == isset( $arrmixValues['last_transmission_on'] ) ) {
			$this->setLastTransmissionOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['last_transmission_on'] ) : $arrmixValues['last_transmission_on'] );
		}
		if( true == isset( $arrmixValues['property_utility_type_rates_count'] ) ) {
			$this->setPropertyUtilityTypeRatesCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_utility_type_rates_count'] ) : $arrmixValues['property_utility_type_rates_count'] );
		}
		if( true == isset( $arrmixValues['property_vcr_settings_count'] ) ) {
			$this->setPropertyVcrSettingsCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_vcr_settings_count'] ) : $arrmixValues['property_vcr_settings_count'] );
		}
		if( true == isset( $arrmixValues['property_meter_settings_count'] ) ) {
			$this->setPropertyMeterSettingsCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_meter_settings_count'] ) : $arrmixValues['property_meter_settings_count'] );
		}
		if( true == isset( $arrmixValues['utility_bill_accounts_count'] ) ) {
			$this->setUtilityBillAccountsCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_accounts_count'] ) : $arrmixValues['utility_bill_accounts_count'] );
		}
		if( true == isset( $arrmixValues['ready_bills_count'] ) ) {
			$this->setReadyBillsCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['ready_bills_count'] ) : $arrmixValues['ready_bills_count'] );
		}
		if( true == isset( $arrmixValues['is_disabled'] ) ) {
			$this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_disabled'] ) : $arrmixValues['is_disabled'] );
		}

		if( true == isset( $arrmixValues['utility_bill_id'] ) ) {
			$this->setUtilityBillId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_id'] ) : $arrmixValues['utility_bill_id'] );
		}

		if( true == isset( $arrmixValues['utility_bill_account_id'] ) ) {
			$this->setUtilityBillAccountId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_bill_account_id'] ) : $arrmixValues['utility_bill_account_id'] );
		}
		if( true == isset( $arrmixValues['start_date'] ) ) {
			$this->setStartDate( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['start_date'] ) : $arrmixValues['start_date'] );
		}
		if( true == isset( $arrmixValues['end_date'] ) ) {
			$this->setEndDate( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['end_date'] ) : $arrmixValues['start_date'] );
		}
		if( true == isset( $arrmixValues['outstanding_bills_count'] ) ) {
			$this->setOutStandingBillsCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['outstanding_bills_count'] ) : $arrmixValues['outstanding_bills_count'] );
		}
	}

	public function setUtilityTypeName( $strUtilityTypeName ) {
		$this->m_strUtilityTypeName = $strUtilityTypeName;
	}

	public function setReadyBillsCount( $intReadyBillsCount ) {
		$this->m_intReadyBillsCount = $intReadyBillsCount;
	}

	public function setUtilityCollectorName( $strUtilityCollectorName ) {
		$this->m_strUtilityCollectorName = $strUtilityCollectorName;
	}

	public function setUtilityTransmissionReceiptTypeName( $strUtilityTransmissionReceiptTypeName ) {
		$this->m_strUtilityTransmissionReceiptTypeName = $strUtilityTransmissionReceiptTypeName;
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->m_boolIsDisabled = CStrings::strToBool( $boolIsDisabled );
	}

	public function setUtilityConsumptionTypeName( $strUtilityConsumptionTypeName ) {
		$this->m_strUtilityConsumptionTypeName = $strUtilityConsumptionTypeName;
	}

	public function setPropertyUtilityTypeRate( $objPropertyUtilityTypeRate ) {
		$this->m_objPropertyUtilityTypeRate = $objPropertyUtilityTypeRate;
	}

	public function setLastTransmissionOn( $strLastTransmissionOn ) {
		$this->m_strLastTransmissionOn = $strLastTransmissionOn;
	}

	public function setPropertyUtilityTypeRatesCount( $intPropertyUtilityTypeRatesCount ) {
		$this->m_intPropertyUtilityTypeRatesCount = $intPropertyUtilityTypeRatesCount;
	}

	public function setPropertyVcrSettingsCount( $intPropertyVcrSettingsCount ) {
		$this->m_intPropertyVcrSettingsCount = $intPropertyVcrSettingsCount;
	}

	public function setPropertyMeterSettingsCount( $intPropertyMeterSettingsCount ) {
		$this->m_intPropertyMeterSettingsCount = $intPropertyMeterSettingsCount;
	}

	public function setUtilityBillAccountsCount( $intUtilityBillAccountsCount ) {
		$this->m_intUtilityBillAccountsCount = $intUtilityBillAccountsCount;
	}

	public function setUtilityBillId( $intUtilityBillId ) {
		$this->m_intUtilityBillId = $intUtilityBillId;
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->m_intUtilityBillAccountId = $intUtilityBillAccountId;
	}

	public function setLeaseBucketCount( $intLeaseBucketCount ) {
		$this->m_intLeaseBucketCount = $intLeaseBucketCount;
	}

	public function setStartDate( $strStartDate ) {
		$this->m_strStartDate = $strStartDate;
	}

	public function setEndDate( $strEndDate ) {
		$this->m_strEndDate = $strEndDate;
	}

	public function setIsTexas( $boolIsTexas ) {
		$this->m_boolIsTexas = $boolIsTexas;
	}

	public function setOutStandingBillsCount( $intOutstandingBillsCount ) {
		$this->m_intOutstandingBillsCount = $intOutstandingBillsCount;
	}

	public function setIsBaseFeeBorrowed( $boolIsBaseFeeBorrowed ) {
		$this->m_boolIsBaseFeeBorrowed = $boolIsBaseFeeBorrowed;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = CStrings::strTrimDef( $strStateCode, 2, NULL, true );
	}

	public function setUtilityTypeIds( $arrintUtilityTypeIds ) {
		$this->m_arrintUtilityTypeIds = $arrintUtilityTypeIds;
	}

	public function setUnconfiguredPvsPutIds( $arrintUnconfiguredPvsPutIds ) {
		$this->m_arrintUnconfiguredPvsPutIds = $arrintUnconfiguredPvsPutIds;
	}

	/**
	 * Validate Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_type_id', 'Utility type is required.' ) );
		}
		return  $boolIsValid;
	}

	public function valArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getArCodeId() ) && CUtilityType::CONVERGENT_BILLING != $this->getUtilityTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBillingFeeArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getBillingFeeArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_fee_ar_code_id', 'Billing fee charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMoveInArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMoveInArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_ar_code_id', 'Move-in charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTerminationArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTerminationArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_ar_code_id', 'Move-out charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCapArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCapArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cap_ar_code_id', 'Cap charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSubsidyArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSubsidyArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_ar_code_id', 'Subsidy charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valArCodeDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getChargeCodeDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_description', 'Charge code description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBillingFeeDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getBillingFeeDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_fee_description', 'billing fee description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMoveInFeeDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getMoveInFeeDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_fee_description', 'move in fee description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTerminationFeeDescription() {

		$boolIsValid = true;

		if( true == is_null( $this->getTerminationFeeDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_fee_description', 'move out description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCapDescription() {

		$boolIsValid = true;

		if( true == is_null( $this->getCapDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cap_description', 'cap description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSubsidyDescription() {

		$boolIsValid = true;

		if( true == is_null( $this->getSubsidyDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_description', 'subsidy description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTaxRateDescription() {

		$boolIsValid = true;

		if( false == is_null( $this->getUtilityTaxRateId() ) && true == is_null( $this->getTaxRateDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_rate_description', 'Tax rate description is required.' ) );
		}

		if( false == is_null( $this->getUtilityTaxRateId() ) && true == getValuesWithSpecialCharacters( $this->getTaxRateDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_rate_description', 'Special characters not allowed in tax rate description.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityConsumptionTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityConsumptionTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_consumption_type_id', 'Consumption type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objUtilitiesDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} elseif( false == is_null( $this->getPropertyId() ) && true == is_null( $this->getId() ) ) {
			$intPropertyUtilityTypeCount = \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchPropertyUtilityTypeCount( ' WHERE property_id=' . ( int ) $this->getPropertyId() . ' AND LOWER(name)= \'' . trim( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'  AND disabled_by IS NULL and deleted_by IS NULL ', $objUtilitiesDatabase );

			if( 0 < $intPropertyUtilityTypeCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Utility type with same name is already added. Choose different name . ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valBaseFeePerUnitChangeDate() {
		$boolIsValid = true;

		if( true == $this->getIsTexas() && CUtilityBaseFeeCalculationType::FIXED_BASE_FEES == $this->getUtilityBaseFeeCalculationTypeId() && false == is_null( $this->getBaseFeePerUnit() ) && ( true == is_null( $this->getBaseFeePerUnitChangeDate() ) || strtotime( $this->getBaseFeePerUnitChangeDate() ) < time() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_fee_per_unit_change_date', 'Base fee per unit change date should be future date.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUtilitySetting( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getPropertyId() ) ) {
			$intUtilitySettingCount = \Psi\Libraries\UtilFunctions\count( \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingsByPropertyId( $this->getPropertyId(), $objDatabase ) );
			if( 0 == $intUtilitySettingCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_setting', 'Property utility setting is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPropertyUtilityTypeRatesByLeaseDates( $objPropertyUtilitySetting, $objUtilitiesDatabase ) {
		$boolIsValid = true;

		$arrobjPropertyUtilityTypeRates = $this->fetchPropertyUtilityTypeRates( $objUtilitiesDatabase );

		if( true == valArr( $this->getPropertyUtilityTypeRates() ) ) {

			$arrobjPropertyUtilityTypeRates = $this->getPropertyUtilityTypeRates();

			$intCount = \Psi\Libraries\UtilFunctions\count( $arrobjPropertyUtilityTypeRates );

			if( 1 == $intCount ) {
                if( false == is_null( $arrobjPropertyUtilityTypeRates[0]->getLeaseStartDateCriteria() ) && false == \CValidation::validateDate( $arrobjPropertyUtilityTypeRates[0]->getLeaseStartDateCriteria() ) ) {
                    $arrobjPropertyUtilityTypeRates[0]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invalid_lease_start_date_criteria', 'Lease start date must be valid date( ' . $arrobjPropertyUtilityTypeRates[0]->getLeaseStartDateCriteria() . ' ).' ) );
                    $boolIsValid = false;
                } elseif( false == is_null( $arrobjPropertyUtilityTypeRates[0]->getLeaseEndDateCriteria() ) && false == \CValidation::validateDate( $arrobjPropertyUtilityTypeRates[0]->getLeaseEndDateCriteria() ) ) {
                    $arrobjPropertyUtilityTypeRates[0]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invalid_lease_end_date_criteria', 'Lease end date must be valid date( ' . $arrobjPropertyUtilityTypeRates[0]->getLeaseEndDateCriteria() . ' ).' ) );
                    $boolIsValid = false;
                } else {
                    if( false == is_null( $objPropertyUtilitySetting->getTerminationDate() ) && false == is_null( $arrobjPropertyUtilityTypeRates[0]->getLeaseEndDateCriteria() ) && strtotime( $arrobjPropertyUtilityTypeRates[0]->getLeaseStartDateCriteria() ) > strtotime( $arrobjPropertyUtilityTypeRates[0]->getLeaseEndDateCriteria() ) ) {
                        $arrobjPropertyUtilityTypeRates[0]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date_criteria', 'Lease start date must be smaller than lease end date.' ) );
                        $boolIsValid = false;
                    }

                    if( true == is_null( $objPropertyUtilitySetting->getTerminationDate() ) && false == is_null( $arrobjPropertyUtilityTypeRates[0]->getLeaseEndDateCriteria() ) ) {
                    	$arrobjPropertyUtilityTypeRates[0]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date_criteria', 'Lease end date must be empty as termination date is null.' ) );
                        $boolIsValid = false;
                    }

                    if( false == is_null( $objPropertyUtilitySetting->getTerminationDate() ) && false == is_null( $arrobjPropertyUtilityTypeRates[0]->getLeaseEndDateCriteria() ) && strtotime( $objPropertyUtilitySetting->getTerminationDate() ) != strtotime( $arrobjPropertyUtilityTypeRates[0]->getLeaseEndDateCriteria() ) ) {
                        $arrobjPropertyUtilityTypeRates[0]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date_criteria', 'Lease end date must be same as termination date( ' . $objPropertyUtilitySetting->getTerminationDate() . ' ).' ) );
                        $boolIsValid = false;
                    }
                }
			} else {

				for( $intFirstLoop = 0; $intFirstLoop < $intCount; $intFirstLoop++ ) {
                    if( false == is_null( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseStartDateCriteria() ) && false == \CValidation::validateDate( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseStartDateCriteria() ) ) {
                        $arrobjPropertyUtilityTypeRates[$intFirstLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invalid_lease_start_date_criteria', 'Lease start date must be valid date( ' . $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseStartDateCriteria() . ' ).' ) );
                        $boolIsValid = false;
                        break 1;
                    }
                    if( false == is_null( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseEndDateCriteria() ) && false == \CValidation::validateDate( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseEndDateCriteria() ) ) {
                        $arrobjPropertyUtilityTypeRates[$intFirstLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invalid_lease_end_date_criteria', 'Lease end date must be valid date( ' . $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseEndDateCriteria() . ' ).' ) );
                        $boolIsValid = false;
                        break 1;
                    }
					if( 0 == $intFirstLoop && false == is_null( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseEndDateCriteria() ) && strtotime( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseStartDateCriteria() ) > strtotime( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseEndDateCriteria() ) ) {
						$arrobjPropertyUtilityTypeRates[$intFirstLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date_criteria', 'Lease start date must be smaller than lease end date.' ) );
						$boolIsValid = false;
						break 1;
					}

					for( $intSecondLoop = $intFirstLoop + 1; $intSecondLoop < $intCount; $intSecondLoop++ ) {
                        if( false == is_null( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseStartDateCriteria() ) && false == \CValidation::validateDate( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseStartDateCriteria() ) ) {
                            $arrobjPropertyUtilityTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invalid_lease_start_date_criteria', 'Lease start date must be valid date( ' . $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseStartDateCriteria() . ' ).' ) );
                            $boolIsValid = false;
                            break 2;
                        }
                        if( false == is_null( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseEndDateCriteria() ) && false == \CValidation::validateDate( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseEndDateCriteria() ) ) {
                            $arrobjPropertyUtilityTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invalid_lease_end_date_criteria', 'Lease end date must be valid date( ' . $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseEndDateCriteria() . ' ).' ) );
                            $boolIsValid = false;
                            break 2;
                        }
						if( false == is_null( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseEndDateCriteria() ) && strtotime( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseStartDateCriteria() ) > strtotime( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseEndDateCriteria() ) ) {
							$arrobjPropertyUtilityTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date_criteria', 'Lease start date must be smaller than lease end date.' ) );
							$boolIsValid = false;
							break 2;
						}

						if( $intSecondLoop == ( \Psi\Libraries\UtilFunctions\count( $arrobjPropertyUtilityTypeRates ) - 1 ) && true == is_null( $objPropertyUtilitySetting->getTerminationDate() ) && false == is_null( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseEndDateCriteria() ) ) {
							$arrobjPropertyUtilityTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date_criteria', 'Last records lease end date must be null as termination date is null.' ) );
							$boolIsValid = false;
							break 2;
						}

						if( true == is_null( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseEndDateCriteria() ) && true == is_null( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseStartDateCriteria() ) ) {
							$arrobjPropertyUtilityTypeRates[$intFirstLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date_criteria', 'Enter lease end date.' ) );
							$boolIsValid = false;
							break 2;
						}

						if( false == is_null( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseStartDateCriteria() ) && strtotime( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseStartDateCriteria() ) > strtotime( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseStartDateCriteria() ) ) {
							$arrobjPropertyUtilityTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date_criteria', 'Lease start date must be greater the previous lease start date.' ) );
							$boolIsValid = false;
							break 2;
						} elseif( true == is_null( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseStartDateCriteria() ) ) {
							$arrobjPropertyUtilityTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date_criteria', 'Enter lease start date.' ) );
							$boolIsValid = false;
							break 2;
						}

						if( false == is_null( $objPropertyUtilitySetting->getTerminationDate() ) && strtotime( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseStartDateCriteria() ) > strtotime( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseEndDateCriteria() ) ) {
							$arrobjPropertyUtilityTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date_criteria', 'Enter lease end date as termination date is set ( ' . $objPropertyUtilitySetting->getTerminationDate() . ' ) .' ) );
							$boolIsValid = false;
							break 2;
						}

						if( strtotime( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseEndDateCriteria() ) >= strtotime( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseStartDateCriteria() ) ) {
							$arrobjPropertyUtilityTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date_criteria', 'Lease start date must be greater than previous lease end date.' ) );
							$boolIsValid = false;
							break 2;
						}

						// Check the LED and next LSD Difference. If its more than 1 day then throw the error.

						if( 0 < $intSecondLoop && false == is_null( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseEndDateCriteria() ) && ( strtotime( $arrobjPropertyUtilityTypeRates[$intSecondLoop]->getLeaseStartDateCriteria() ) - strtotime( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseEndDateCriteria() ) ) > 86400 ) {
							$arrobjPropertyUtilityTypeRates[$intSecondLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date_criteria', 'There should not be a single day gap between this lease start date and previous lease end date.' ) );
							$boolIsValid = false;
							break 2;
						} elseif( true == is_null( $arrobjPropertyUtilityTypeRates[$intFirstLoop]->getLeaseEndDateCriteria() ) ) {
							$arrobjPropertyUtilityTypeRates[$intFirstLoop]->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date_criteria', 'Enter lease end date.' ) );
							$boolIsValid = false;
							break 2;
						}

						$intFirstLoop++;

					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valServiceTypes() {
		$boolIsValid = true;

		if( true == $this->getIsManual() && true == $this->getIsFixed() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_maual_fixed', 'Select either manual or fixed billing.' ) );
			$boolIsValid = false;
		}

		if( true == $this->getIsManual() && true == $this->getHasVcrService() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_maual_fixed', 'Select either manual or VCR billing.' ) );
			$boolIsValid = false;
		}

		if( true == $this->getIsManual() && true == $this->getIsSubmetered() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_maual_fixed', 'Select either manual or submetered.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valChargeCodeDescriptions() {
		$boolIsValid = true;

		if( true == getValuesWithSpecialCharacters( $this->getChargeCodeDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_description', 'Special characters not allowed in charge code description.' ) );
		}

		if( true == getValuesWithSpecialCharacters( $this->getBillingFeeDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_fee_description', 'Special characters not allowed in billing fee description.' ) );
		}

		if( true == getValuesWithSpecialCharacters( $this->getMoveInFeeDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_fee_description', 'Special characters not allowed in move in fee description.' ) );
		}

		if( true == getValuesWithSpecialCharacters( $this->getTerminationFeeDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_fee_description', 'Special characters not allowed in termination description.' ) );
		}

		if( true == getValuesWithSpecialCharacters( $this->getCapDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cap_description', 'Special characters not allowed in cap description.' ) );
		}

		if( true == getValuesWithSpecialCharacters( $this->getSubsidyDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_description', 'Special characters not allowed in subsidy description.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityBillAccount( $objUtilitiesDatabase ) {
		$boolIsValid = true;

		$arrobjUtilityBillAccounts = \Psi\Eos\Utilities\CUtilityBillAccounts::createService()->fetchPropertyBuildingUtilityBillAccountsByPropertyUtilityTypeId( $this->getId(), $objUtilitiesDatabase );

		if( false == valArr( $arrobjUtilityBillAccounts ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_bill_account', 'System did not find any utility bill account that has property building set.' ) );
		}

		return $boolIsValid;
	}

	public function valBillingServicePeriodTypeId() {

		$boolIsValid = true;

		if( true == is_null( $this->getBillingServicePeriodTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_service_period_type_id', 'Billing Service Period Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBillingServicePropertyUtilityTypeId() {

		$boolIsValid = true;

		if( CBillingServicePeriodType::MATCH == $this->getBillingServicePeriodTypeId() && true == is_null( $this->getBillingServicePropertyUtilityTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_service_property_utility_type_id', 'Matching Utility Type required.' ) );
		}

		return $boolIsValid;
	}

	public function valBillingServiceMonth() {

		$boolIsValid = true;

		if( true == in_array( $this->getBillingServicePeriodTypeId(), [ CBillingServicePeriodType::FIRST_TO_END_OF_MONTH, CBillingServicePeriodType::FIRST_TO_FIRST, CBillingServicePeriodType::CUSTOM ] ) && true == is_null( $this->getBillingServiceMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_service_period_type_id', 'Billing Service Month is required.' ) );
		}

		return $boolIsValid;
	}

	public function valGrossRecaptureThreshold() {
		$boolIsValid = true;

		if( true == is_null( $this->getGrossRecaptureThreshold() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gross_recapture_threshold', 'Gross Recapture Threshold is required.' ) );
		}

		if( 0 > $this->getGrossRecaptureThreshold() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gross_recapture_threshold', 'You cannot specify a negative value for Gross Recapture Threshold.' ) );
		}

		return $boolIsValid;
	}

	public function valBillableRecaptureThreshold() {
		$boolIsValid = true;

		if( true == is_null( $this->getBillableRecaptureThreshold() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billable_recapture_threshold', 'Billable Recapture Threshold is required.' ) );
		}

		if( 0 > $this->getBillableRecaptureThreshold() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billable_recapture_threshold', 'You cannot specify a negative value for Billable Recapture Threshold.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityChargesThreshold() {
		$boolIsValid = true;

		if( true == is_null( $this->getUtilityChargesThreshold() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_charges_threshold', 'Utility Charges Threshold is required.' ) );
		}

		if( 0 > $this->getUtilityChargesThreshold() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'utility_charges_threshold', 'You cannot specify a negative value for Utility Charges Threshold.' ) );
		}

		return $boolIsValid;
	}

	public function valTaxChargeCode() {
		$boolIsValid = true;

		if( false == is_null( $this->getUtilityTaxRateId() ) && true == is_null( $this->getTaxRateArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Tax charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBillingServicePeriodStartDay() {
		$boolIsValid = true;

		if( true == ( CBillingServicePeriodType::CUSTOM == $this->getBillingServicePeriodTypeId() ) && true == is_null( $this->getBillingServicePeriodStartDay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_service_period_start_day', 'Billing Service Period Start Day is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBillingServicePeriodEndDay() {
		$boolIsValid = true;

		if( true == ( CBillingServicePeriodType::CUSTOM == $this->getBillingServicePeriodTypeId() ) && true == is_null( $this->getBillingServicePeriodEndDay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_service_period_end_day', 'Billing Service Period End Day is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUseBillAccount() {

		$boolIsValid = true;

		if( CBillingServicePeriodType::PROVIDER_SERVICE_PERIOD == $this->getBillingServicePeriodTypeId() && true == is_null( $this->getBillingServicePeriodUtilityBillAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_service_period_utility_bill_account_id', 'Use Bill Account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCustomMessages() {
		$boolIsValid = true;

		$boolIsValid &= $this->valRegistrationOrRenewalDates();
		$boolIsValid &= $this->valRenewalDate();

		return $boolIsValid;
	}

	public function valRegistrationOrRenewalDates() {

		$boolIsValid = true;

		if( ( ( CState::STATE_CODE_NC == $this->getStateCode() ) && true == in_array( $this->getUtilityTypeId(), CUtilityType::$c_arrintNCStateUtilityTypes ) ) ||
			( ( CState::STATE_CODE_NJ == $this->getStateCode() ) && true == in_array( $this->getUtilityTypeId(), CUtilityType::$c_arrintNJStateUtilityTypes ) ) ) {

			$strRegistrationDate	= $this->createFormattedDate( $this->getRegistrationDate() );
			$strRenewalDate			= $this->createFormattedDate( $this->getRenewalDate() );

			if( ( false == is_null( $this->getRegistrationDate() ) && 1 != strlen( CValidation::checkDate( $strRegistrationDate ) ) ) ||
			    ( false == is_null( $this->getRenewalDate() ) && 1 != strlen( CValidation::checkDate( $strRenewalDate ) ) ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_date', 'The registration or renewal dates are invalid.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRenewalDate() {

		$boolIsValid = true;

		if( ( ( CState::STATE_CODE_NC == $this->getStateCode() ) && true == in_array( $this->getUtilityTypeId(), CUtilityType::$c_arrintNCStateUtilityTypes ) ) ||
		    ( ( CState::STATE_CODE_NJ == $this->getStateCode() ) && true == in_array( $this->getUtilityTypeId(), CUtilityType::$c_arrintNJStateUtilityTypes ) ) ) {

			if( false == is_null( $this->getRegistrationDate() ) && false == is_null( $this->getRenewalDate() ) ) {

				$arrintRegistrationDateParts	= explode( '/', $this->getRegistrationDate() );
				$arrintRenewalDateParts			= explode( '/', $this->getRenewalDate() );

				$intRegistrationDate	= gregoriantojd( $arrintRegistrationDateParts[0], $arrintRegistrationDateParts[1], $arrintRegistrationDateParts[2] );
				$intRenewalDate			= gregoriantojd( $arrintRenewalDateParts[0], $arrintRenewalDateParts[1], $arrintRenewalDateParts[2] );
				$intDaysDifference		= $intRenewalDate - $intRegistrationDate;

				if( 0 > $intDaysDifference ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_date', 'The renewal date is invalid.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valFrontEndMessages() {
		$boolIsValid = true;

		$boolIsValid &= $this->valBillingServicePeriodStartDayEndDayDifference();
		$boolIsValid &= $this->valRegistrationOrRenewalDate();

		return $boolIsValid;
	}

	public function valBillingServicePeriodStartDayEndDayDifference() {

		$boolIsValid = true;

		if( true == ( CBillingServicePeriodType::CUSTOM == $this->getBillingServicePeriodTypeId() ) && false == is_null( $this->getBillingServicePeriodStartDay() ) && false == is_null( $this->getBillingServicePeriodEndDay() ) ) {

			$intBillingServicePeriodDayDifference = $this->getBillingServicePeriodEndDay() - $this->getBillingServicePeriodStartDay();

			if( -1 > $intBillingServicePeriodDayDifference || 1 < $intBillingServicePeriodDayDifference ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_service_period_day', 'Billing service Period Day Difference should not be greater than 1.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRegistrationOrRenewalDate() {

		$boolIsValid = true;

		if( ( ( CState::STATE_CODE_NC == $this->getStateCode() ) && true == in_array( $this->getUtilityTypeId(), CUtilityType::$c_arrintNCStateUtilityTypes ) ) ||
			( ( CState::STATE_CODE_NJ == $this->getStateCode() ) && true == in_array( $this->getUtilityTypeId(), CUtilityType::$c_arrintNJStateUtilityTypes ) ) ) {

			if( true == is_null( $this->getRegistrationDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'registration_date', 'Registration date is required.' ) );
			}

			if( true == is_null( $this->getRenewalDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_date', 'Renewal date is required.' ) );
			}

			if( false == is_null( $this->getRegistrationDate() ) && false == is_null( $this->getRenewalDate() ) ) {

				$arrintRegistrationDateParts	= explode( '/', $this->getRegistrationDate() );
				$arrintRenewalDateParts			= explode( '/', $this->getRenewalDate() );

				$intRegistrationDate	= gregoriantojd( $arrintRegistrationDateParts[0], $arrintRegistrationDateParts[1], $arrintRegistrationDateParts[2] );
				$intRenewalDate			= gregoriantojd( $arrintRenewalDateParts[0], $arrintRenewalDateParts[1], $arrintRenewalDateParts[2] );

				if( $intRenewalDate <= $intRegistrationDate ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_date', 'Registration date must be less than renewal date.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valBaseFeePerUnit() {

		$boolIsValid = true;

		if( CUtilityBaseFeeCalculationType::FIXED_BASE_FEES == $this->getUtilityBaseFeeCalculationTypeId() && true == is_null( $this->getBaseFeePerUnit() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_fee_per_unit', 'Base fee per unit fees is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objPropertyUtilitySetting, $objUtilitiesDatabase ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCustomMessages();

				$boolIsValid &= $this->valUtilityTypeId();
				$boolIsValid &= $this->valName( $objUtilitiesDatabase );
				$boolIsValid &= $this->valArCodeId();

				if( CUtilityType::CONVERGENT_BILLING != $this->getUtilityTypeId() ) {
					$boolIsValid &= $this->valUtilityConsumptionTypeId();
					$boolIsValid &= $this->valBaseFeePerUnitChangeDate();
					$boolIsValid &= $this->valServiceTypes();
					$boolIsValid &= $this->valBillingServicePeriodTypeId();
					$boolIsValid &= $this->valBillingServicePropertyUtilityTypeId();
					$boolIsValid &= $this->valBillingServicePeriodStartDay();
					$boolIsValid &= $this->valBillingServicePeriodEndDay();
					$boolIsValid &= $this->valBillingServiceMonth();
					if( VALIDATE_UPDATE == $strAction ) {
						$boolIsValid &= $this->valUseBillAccount();
					}

					if( true == $this->getBillByBuilding() ) {
						$boolIsValid &= $this->valUtilityBillAccount( $objUtilitiesDatabase );
					}

					$boolIsValid &= $this->valChargeCodeDescriptions();
					$boolIsValid &= $this->valGrossRecaptureThreshold();
					$boolIsValid &= $this->valBillableRecaptureThreshold();
					$boolIsValid &= $this->valUtilityChargesThreshold();
					$boolIsValid &= $this->valTaxChargeCode();
				}
				break;

			case self::VALIDATE_PROPERTY_UTILITY_TYPE_RATES:
				$boolIsValid &= $this->valPropertyUtilityTypeRatesByLeaseDates( $objPropertyUtilitySetting, $objUtilitiesDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $boolIsValid;
				break;

			case self::VALIDATE_UPDATE_CUSTOM:
				$boolIsValid &= $this->valCustomMessages();
				break;

			case self::VALIDATE_FRONT_END_VALIDATIONS:
				$boolIsValid &= $this->valFrontEndMessages();
				break;

			case self::VALIDATE_GENERAL_BILLING_TAB:
				if( CUtilityType::CONVERGENT_BILLING != $this->getUtilityTypeId() ) {

					$boolIsValid &= $this->valBaseFeePerUnit();
					$boolIsValid &= $this->valBaseFeePerUnitChangeDate();
					$boolIsValid &= $this->valGrossRecaptureThreshold();
					$boolIsValid &= $this->valBillableRecaptureThreshold();
					$boolIsValid &= $this->valUtilityChargesThreshold();
					$boolIsValid &= $this->valUtilityConsumptionTypeId();
					$boolIsValid &= $this->valServiceTypes();
					$boolIsValid &= $this->valBillingServicePeriodTypeId();
					$boolIsValid &= $this->valBillingServicePropertyUtilityTypeId();
					$boolIsValid &= $this->valBillingServicePeriodStartDay();
					$boolIsValid &= $this->valBillingServicePeriodEndDay();
					$boolIsValid &= $this->valBillingServiceMonth();
					$boolIsValid &= $this->valUseBillAccount();
					$boolIsValid &= $this->valRegistrationOrRenewalDates();
					$boolIsValid &= $this->valRenewalDate();

					if( true == $this->getBillByBuilding() ) {
						$boolIsValid &= $this->valUtilityBillAccount( $objUtilitiesDatabase );
					}
				}
				break;

			case self::VALIDATE_PUT_ON_SETTINGS:

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function validateFinancialTabDetail( $arrmixPropertyUtilityTypeDetails ) {
		$boolIsValid = true;

		if( false == valArr( $arrmixPropertyUtilityTypeDetails ) ) {
			return  $boolIsValid;
		}

		if( CUtilityType::CONVERGENT_BILLING <> $this->getUtilityTypeId() ) {
			$boolIsValid &= $this->valArCodeId();
			$boolIsValid &= $this->valArcodeDescription();
		}

		if( false == empty( $arrmixPropertyUtilityTypeDetails['billing_fee_amount'] ) || false == empty( $arrmixPropertyUtilityTypeDetails['billing_fee_percent'] ) ) {
			$boolIsValid &= $this->valBillingFeeArCodeId();
			$boolIsValid &= $this->valBillingFeeDescription();
		}

		if( false == empty( $arrmixPropertyUtilityTypeDetails['move_in_fee'] ) ) {
			$boolIsValid &= $this->valMoveInArCodeId();
			$boolIsValid &= $this->valMoveInFeeDescription();
		}

		if( false == empty( $arrmixPropertyUtilityTypeDetails['termination_fee'] ) ) {
			$boolIsValid &= $this->valTerminationArCodeId();
			$boolIsValid &= $this->valTerminationFeeDescription();
		}

		if( false == empty( $arrmixPropertyUtilityTypeDetails['cap_amount'] ) ) {
			$boolIsValid &= $this->valCapArCodeId();
			$boolIsValid &= $this->valCapDescription();
		}

		if( false == empty( $arrmixPropertyUtilityTypeDetails['subsidy_amount'] ) ) {
			$boolIsValid &= $this->valSubsidyArCodeId();
			$boolIsValid &= $this->valSubsidyDescription();
		}

		if( true == Cstrings::strToBool( $arrmixPropertyUtilityTypeDetails['is_integrated'] ) ) {
			$boolIsValid &= $this->valTaxChargeCode();
			$boolIsValid &= $this->valTaxRateDescription();
		}

		$boolIsValid &= $this->valChargeCodeDescriptions();

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function getIsInitiated() {

		return !( false == is_null( $this->getInitiationDate() ) && strtotime( $this->getInitiationDate() ) > time() );
	}

	public function determineHasTransmissionProblem() {
		if( false == is_null( $this->getLastTransmissionOn() ) && ( time() - strtotime( $this->getLastTransmissionOn() ) ) < 129600 ) {
			// 129600==18 hours
			return false;
		}
	}

	public function unsetAllErrorMsgs() {
		$this->m_arrobjErrorMsgs = NULL;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolChangeUtilityName = false ) {

		if( true == $boolChangeUtilityName ) {
			$this->setName( $this->getName() . '_old' );
		}
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$arrstrTempOriginalValues 	= [];
		$arrstrOriginalValues 		= [];
		$arrstrOriginalValueChanges = [];
		$boolChangeFlag				= false;

		$arrmixOriginalValues = fetchData( 'SELECT * FROM property_utility_types WHERE id = ' . ( int ) $this->getId(), $objDatabase );

		$arrstrTempOriginalValues = $arrmixOriginalValues[0];

		if( true == valArr( $arrstrTempOriginalValues ) ) {
			foreach( $arrstrTempOriginalValues as $strKey => $strMixOriginalValue ) {

				if( 'utility_documents_ids' == $strKey || 'details' == $strKey ) {
					continue;
				}

				$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
				$strFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

				if( CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $strMixOriginalValue ) {
					$arrstrOriginalValueChanges[$strKey] 	= $this->$strFunctionName();
					$arrstrOriginalValues[$strKey] 			= $strMixOriginalValue;
					$boolChangeFlag 						= true;
				}
			}
		}

		$boolUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUpdateStatus ) {

			if( false != $boolChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'property_utility_types' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}

			return $boolUpdateStatus;
		}

		return false;
	}

	public function getMeasurementUnit() {

		$strMeasurementUnit = NULL;

		switch( $this->getUtilityTypeId() ) {

			case CUtilityType::COLD_WATER:
			case CUtilityType::HOT_WATER:
			case CUtilityType::WATER:
			case CUtilityType::SEWER:
			case CUtilityType::HVAC:
				$strMeasurementUnit = 'Gallons';
				break;

			case CUtilityType::ELECTRIC:
				$strMeasurementUnit = 'KwH';
				break;

			case CUtilityType::GAS:
				$strMeasurementUnit = 'Therms';
				break;

			default:
				// default case
				break;
		}

		return $strMeasurementUnit;
	}

	public function generateBillingServicePeriod( $strConsumptionStartDate, $strConsumptionEndDate ) {

		// this is temporary fix, we will remove this in the month of feb.
		$strYear = date( 'Y' );
		if( strtotime( date( $this->getBillingServiceMonth() . '/01/Y' ) ) > strtotime( date( 'm/01/Y' ) ) ) {
			$strYear = date( 'Y', strtotime( '-1 year' ) );
		}

		switch( $this->getBillingServicePeriodTypeId() ) {
			case CBillingServicePeriodType::PROVIDER_SERVICE_PERIOD:
			case CBillingServicePeriodType::DIRECT_METERED:
			case CBillingServicePeriodType::MATCH:
				$this->setEndDate( $strConsumptionEndDate );
				$this->setStartDate( $strConsumptionStartDate );
				break;

			case CBillingServicePeriodType::FIRST_TO_END_OF_MONTH:
				$strFirstDay = date( 'm/d/Y', strtotime( date( $this->getBillingServiceMonth() . '/01/' . $strYear ) ) );
				$strLastDay = date( 'm/t/Y', strtotime( $strFirstDay ) );

				$this->setEndDate( $strLastDay );
				$this->setStartDate( $strFirstDay );
				break;

			case CBillingServicePeriodType::FIRST_TO_FIRST:
				$strFirstDay = date( 'm/d/Y', strtotime( date( $this->getBillingServiceMonth() . '/01/' . $strYear ) ) );
				$strLastDay = date( 'm/d/Y', strtotime( date( 'm/t/Y', strtotime( $strFirstDay ) ) . '+1 day' ) );

				$this->setEndDate( $strLastDay );
				$this->setStartDate( $strFirstDay );
				break;

			case CBillingServicePeriodType::MANUAL:
				if( true == valStr( $strConsumptionEndDate ) && 1 < strlen( $strConsumptionEndDate ) ) {
					$strConsumptionEndDate = date( 'm/d/Y', strtotime( $strConsumptionEndDate ) );
					$this->setStartDate( $strConsumptionEndDate );
				}
				break;

			case CBillingServicePeriodType::MANUAL_PLUS_ONE:
				if( true == valStr( $strConsumptionEndDate ) && 1 < strlen( $strConsumptionEndDate ) ) {
					$strConsumptionEndDate = date( 'm/d/Y', strtotime( $strConsumptionEndDate . '+1 day' ) );
					$this->setStartDate( $strConsumptionEndDate );
				}
				break;

			case CBillingServicePeriodType::CUSTOM:
				$strFirstDay = date( 'm/d/Y', strtotime( date( $this->getBillingServiceMonth() . '/' . $this->getBillingServicePeriodStartDay() . '/' . $strYear ) ) );

				$intNextBillingServiceMonth = $this->getBillingServiceMonth() + 1;

				if( $intNextBillingServiceMonth > 12 ) {
					$intNextBillingServiceMonth = 1;
					$strLastDay = date( 'm/d/Y', strtotime( date( $intNextBillingServiceMonth . '/' . $this->getBillingServicePeriodEndDay() . '/' . $strYear ) ) );
					$strLastDay = date( 'm/d/Y', strtotime( '+ 1 year', strtotime( $strLastDay ) ) );
				} else {
					$strLastDay = date( 'm/d/Y', strtotime( date( $intNextBillingServiceMonth . '/' . $this->getBillingServicePeriodEndDay() . '/' . $strYear ) ) );
				}

				$this->setEndDate( $strLastDay );
				$this->setStartDate( $strFirstDay );
				break;

			default:
				// default Case
				break;
		}

	}

	public function getIsBaseFeeBorrowed() {
		return $this->m_boolIsBaseFeeBorrowed;
	}

	 /**
	 * Fetch Functions
	 */

	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUtilityTypeRates( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyUtilityTypeRates::createService()->fetchPropertyUtilityTypeRatesByPropertyUtilityTypeId( $this->getId(), $objUtilitiesDatabase );
	}

	public function fetchUtilityConsumptionTypes( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityConsumptionTypes::createService()->fetchUtilityConsumptionTypesByUtilityTypeId( $this->getUtilityTypeId(), $objUtilitiesDatabase );
	}

	public function fetchPropertyMeteringSetting( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyMeteringSettings::createService()->fetchPropertyMeteringSettingByPropertyUtilityTypeId( $this->getId(), $objUtilitiesDatabase );
	}

	public function fetchPropertyVcrSetting( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyVcrSettings::createService()->fetchPropertyVcrSettingByPropertyUtilityTypeId( $this->getId(), $objUtilitiesDatabase );
	}

	public function fetchAllPropertyVcrSetting( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyVcrSettings::createService()->fetchAllPropertyVcrSettingByPropertyUtilityTypeId( $this->getId(), $objUtilitiesDatabase );
	}

	public function fetchActiveUtilityBatch( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityBatches::createService()->fetchActiveUtilityBatchByPropertyIdByManagementId( $this->getPropertyId(), $this->getCid(), $objUtilitiesDatabase );
	}

	public function fetchDirectMeteredVcrPropertyCapSubsidies( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyCapSubsidies::createService()->fetchVcrPropertyCapSubsidiesByPropertyUtilityTypeIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objUtilitiesDatabase );
	}

	public function updatePropertyUtilityType( $strKey, $boolValue, $intUserId, $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->updatePropertyUtilityTypeKeyByIdByCidByPropertyId( $strKey, $boolValue, $intUserId, $this->getId(), $this->getCid(), $this->getPropertyId(), $objUtilitiesDatabase );
	}

	public function createFormattedDate( $strDateTime ) {

		$strNewlyCreatedDate = new DateTime( $strDateTime );

		$strFormattedDate = $strNewlyCreatedDate->format( 'm/d/yy' );

		return $strFormattedDate;
	}

	public function insertDefaultCommodity( $intCurrentUserId, $objUtilitiesDatabase, $objAdminDatabase ) {
	    $objCommodity= \Psi\Eos\Utilities\CCommodities::createService()->fetchCommodityByIdsByUtilityTypeId( CCommodity::$c_arrintDefaultCommodities, $this->getUtilityTypeId(), $objUtilitiesDatabase );

        $objTestPropertyUtilitySetting = new CPropertyUtilitySetting();
        $objTestPropertyUtilitySetting->setCid( $this->getCid() );
        $objTestPropertyUtilitySetting->setPropertyId( $this->getPropertyId() );

        $boolIsUemContractedProperty	= $objTestPropertyUtilitySetting->getIsItUtilityExpenseManagement( $objAdminDatabase );

	    if( true == valObj( $objCommodity, 'CCommodity' ) ) {
            $objPropertyCommodity	= \Psi\Eos\Utilities\CPropertyCommodities::createService()->fetchPropertyCommodityByCommodityIdByPropertyIdByCid( $objCommodity->getId(), $this->getPropertyId(), $this->getCid(), $objUtilitiesDatabase );
            if( true == valObj( $objPropertyCommodity, 'CPropertyCommodity' ) ) {
                $objPropertyCommodity->setCid( $this->getCid() );
                $objPropertyCommodity->setPropertyId( $this->getPropertyId() );
                $objPropertyCommodity->setCommodityId( $objCommodity->getId() );
                $objPropertyCommodity->setPropertyUtilityTypeId( $this->getId() );
                if( true == $boolIsUemContractedProperty ) {
                    $objPropertyCommodity->setHasUemService( true );
                }
            } else {
                $objPropertyCommodity = new CPropertyCommodity();
                $objPropertyCommodity->setCid( $this->getCid() );
                $objPropertyCommodity->setPropertyId( $this->getPropertyId() );
                $objPropertyCommodity->setCommodityId( $objCommodity->getId() );
                $objPropertyCommodity->setPropertyUtilityTypeId( $this->getId() );
                if( true == $boolIsUemContractedProperty ) {
                    $objPropertyCommodity->setHasUemService( true );
                }
            }
            return $objPropertyCommodity->insertOrUpdate( $intCurrentUserId, $objUtilitiesDatabase );
        }
    }

	public function mapPropertyUtilityCommodity( $intCurrentUserId, $objUtilitiesDatabase, $boolIsUemProduct = false ) {
		$objCommodity= \Psi\Eos\Utilities\CCommodities::createService()->fetchCommodityByIdsByUtilityTypeId( CCommodity::$c_arrintDefaultCommodities, $this->getUtilityTypeId(), $objUtilitiesDatabase );
		$boolIsSuccess = true;

		if( true == valObj( $objCommodity, 'CCommodity' ) ) {
			$objPropertyCommodity	= \Psi\Eos\Utilities\CPropertyCommodities::createService()->fetchPropertyCommodityByCommodityIdByPropertyIdByCid( $objCommodity->getId(), $this->getPropertyId(), $this->getCid(), $objUtilitiesDatabase );

			if( false == valObj( $objPropertyCommodity, 'CPropertyCommodity' ) ) {
				$objPropertyCommodity = new CPropertyCommodity();
				$objPropertyCommodity->setCid( $this->getCid() );
				$objPropertyCommodity->setPropertyId( $this->getPropertyId() );
				$objPropertyCommodity->setCommodityId( $objCommodity->getId() );
				$objPropertyCommodity->setPropertyUtilityTypeId( $this->getId() );
				$objPropertyCommodity->setCreatedBy( $intCurrentUserId );
				$objPropertyCommodity->setCreatedOn( 'NOW()' );
			}

			if( true == $boolIsUemProduct ) {
				$objPropertyCommodity->setUemGoLiveDate( NULL );
				$objPropertyCommodity->setHasUemService( true );
			}

			$objPropertyCommodity->setUpdatedBy( $intCurrentUserId );
			$objPropertyCommodity->setUpdatedOn( 'NOW()' );

			if( false == $objPropertyCommodity->insertOrUpdate( $intCurrentUserId, $objUtilitiesDatabase ) ){
				$this->addErrorMsgs( $objPropertyCommodity->getErrorMsgs() );
				$boolIsSuccess = false;
			}

		}

		return $boolIsSuccess;
	}

}
?>