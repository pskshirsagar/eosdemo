<?php

class CComplianceExemptionPropertyGroup extends CBaseComplianceExemptionPropertyGroup {

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valComplianceExemptionId() {
		return true;
	}

	public function valPropertyGroupId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>