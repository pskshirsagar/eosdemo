<?php

class CUtilityPropertyPhase extends CBaseUtilityPropertyPhase {

	public function valName() {
        $boolIsValid = true;

        if( false == isset( $this->m_strName ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Utility property phase name is Required.' ) );
        }
        return $boolIsValid;
    }

    public function valUnitCount() {
        $boolIsValid = true;

        if( false == isset( $this->m_intUnitCount ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_count', 'Utility property phase unit count is Required.' ) );
        }
        return $boolIsValid;
    }

    public function valEstimatedPhaseCompletionDate( $intPropertyUtilitySettingId, $objUtilitiesDatabase ) {

    	$objPropertyUtilitySetting = \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingById( $intPropertyUtilitySettingId, $objUtilitiesDatabase );
    	$boolIsValid = true;

        if( true == is_null( $this->getEstimatedPhaseCompletionDate() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_phase_completion_date', 'Estimated Phase Completion date is required.' ) );
        } elseif( strtotime( $this->getEstimatedPhaseCompletionDate() ) <= strtotime( $objPropertyUtilitySetting->getInitiationDate() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_phase_completion_date', 'Estimated Phase Completion date should not be less than initiation date.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $intPropertyUtilitySettingId, $objUtilitiesDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valName();
        		$boolIsValid &= $this->valUnitCount();
        		$boolIsValid &= $this->valEstimatedPhaseCompletionDate( $intPropertyUtilitySettingId, $objUtilitiesDatabase );
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>