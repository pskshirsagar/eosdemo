<?php

class CDutyPermission extends CBaseDutyPermission {

	public function valId() {
		return true;
	}

	public function valDutyTypeId() {
		return true;
	}

	public function valControllerId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>