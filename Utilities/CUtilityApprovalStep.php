<?php

class CUtilityApprovalStep extends CBaseUtilityApprovalStep {

	protected $m_strUtilityApprovalStepTypeName;

	const PROPERTY_OR_PAYMENT_ADDRESS_IS_CORRECT_IN_ENTRATA                                       = 1;
	const PROPERTY_RULES_AND_NOTES                                                                = 2;
	const PROPERTY_LOGO_IS_UPLOADED_TO_ENTRATA                                                    = 3;
	const PROPERTY_LEASE_AND_UTILITY_ADENDUM_IS_SAVED_IN_CLIENT_FILE                              = 4;
	const BILLING_ANALYST_LISTED                                                                  = 5;
	const PREBILL_EMAILS_ARE_SET_UP_IN_ENTRATA                                                    = 6;
	const THE_SEND_UTILITY_TRANSACTIONS_HAS_BEEN_ADDED_UNDER_THE_COMPANY_INEGRATION               = 7;
	const HAVE_PROPERTY_ENABLE_PERMISSIONS_TO_VIEW_PREBILL_IN_ENTRATA                             = 8;
	const UNIT_ADDRESSES_ARE_CORRECT_MATCH_USPS_AND_CORRECT_IN_OUR_SYSTEM                         = 9;
	const SQFT_AND_BEDROOM_AMOUNTS_ARE_CORRECT                                                    = 10;
	const NON_BILLABLE_UNITS_ARE_MARKED                                                           = 11;
	const NO_VILATIONS_OF_STATE_REGULATIONS                                                       = 12;
	const TEXAS_CORRECT_CADS                                                                      = 13;
	const CHECK_IF_PROPERTY_IS_REGISTERED_WITH_TCEQ                                               = 14;
	const SALES_TAX                                                                               = 15;
	const TX_BASES_ARE_CREATED                                                                    = 16;
	const ALL_BILLING_METHODS_ARE_ENTERED_CORRECTLY_AND_CONFIRMED_BY_PREVIOUS_PROVIDER_AND_CLIENT = 17;
	const RAMP_UP_DATES_ARE_ENTERED                                                               = 18;
	const CADS_ARE_ENTERED_CORRECTLY                                                              = 19;
	const MOVEIN_FEE_IS_ENTERED                                                                   = 20;
	const CONVERGENT_BILLING_FEE_IS_ENTERED                                                       = 21;
	const PSI_FEE_ENTERED                                                                         = 22;
	const IF_SERVICE_FEE_IS_LINKED_TO_CONVEREGENT_BILLING_MAKE_SURE_IT_IS_SET_TO_IS_FIXED         = 23;
	const VCR_IS_SET_UP_FOR_APPLICABLE_UNTILITIES                                                 = 24;
	const VCR_FEES_ARE_SET_UP                                                                     = 25;
	const CO_ACCT_CHARGE_CODES_GRACE_PERIOD_WAIVER_AMT_AND_PROVIDER_ARE_SET_UP                    = 26;
	const HIGH_VACANT_USAGE_EMAIL_IS_SET_UP_IN_ENTRATA                                            = 27;
	const METERED_PROPERTIES                                                                      = 28;
	const ALL_TRANSPONDERS_ARE_IMPROTED_TO_UNITS                                                  = 29;
	const BEGINNING_AND_ENDING_READS_ARE_IMPORTED_AND_CORRECT                                     = 30;
	const MODEM_NUMBER_IS_RECORDED                                                                = 31;
	const IF_CITY_RATES_THEY_HAVE_BEEN_APPROVED_BY_KIM_COLE                                       = 32;
	const BILLING_DETAIL_IS_COMPLETE                                                              = 33;
	const PROPERTY_INFO_IS_COMPLETE                                                               = 34;
	const COMPANY_BILLING_SETTINGS_IS_COMPLETE                                                    = 35;
	const PREBILL_GENERATION_SETTINGS_ARE_COMPLETE                                                = 36;
	const INTEGRATION_SETTINGS_ARE_CORRECT                                                        = 37;
	const TAX_RATE_SETTINGS                                                                       = 38;
	const ROLLOUT_TYPE_SET                                                                        = 39;
	const COMPLETE_LIST_OF_PROVIDER_ACCOUNTS_AND_CONFIRMATION_FROM_PROPERTY                       = 40;
	const BILL_TEMPLATES_ARE_CREATED                                                              = 41;
	const ALL_PROVIDER_ACCOUNTS_ARE_CREATED                                                       = 42;
	const BILLS_FOR_THE_FIRST_MONTH_ARE_UPLOADED_AND_PROCESSED_CORRECTLY                          = 43;
	const EACH_UTILITY_HAS_CHARGE_CODE_ENTERED_CORRECTLY_IN_CLIENT_ADMIN                          = 44;
	const BASE_CHARGES_ARE_ENTERED_FOR_EACH_UTLILITY                                              = 45;
	const DEFAULT_BILL_DATES_ARE_SET                                                              = 46;

	const REQUIRE_LEGAL_APPROVAL                                                                  = 1;
	const REQUIRE_CLIENT_APPROVAL                                                                 = 1;
	const REQUIRE_DIRECTOR_OF_RU_APPROVAL                                                         = 1;

	public static $c_arrstrUtilityApprovalStepUrls = [

		self::PROPERTY_OR_PAYMENT_ADDRESS_IS_CORRECT_IN_ENTRATA                                       => '/?module=clients-new&action=login_user&return_url=/?module=property_system',
		self::PROPERTY_RULES_AND_NOTES                                                                => '/?module=clients-new&action=login_user&return_url=/?module=property_system',
		self::PROPERTY_LOGO_IS_UPLOADED_TO_ENTRATA                                                    => '/?module=clients-new&action=login_user&return_url=/?module=property_system',
		self::PROPERTY_LEASE_AND_UTILITY_ADENDUM_IS_SAVED_IN_CLIENT_FILE                              => '/?module=clients-new&action=login_user&return_url=/?module=property_system',
		self::BILLING_ANALYST_LISTED                                                                  => '/?module=ps_lead-new&load_tab=10&is_global=0',
		self::PREBILL_EMAILS_ARE_SET_UP_IN_ENTRATA                                                    => '/?module=clients-new&action=login_user&return_url=/?module=property_system',
		self::THE_SEND_UTILITY_TRANSACTIONS_HAS_BEEN_ADDED_UNDER_THE_COMPANY_INEGRATION               => '/?module=integration_results&action=search_integration_results&integration_result_filter[kill_session]=1',
		self::HAVE_PROPERTY_ENABLE_PERMISSIONS_TO_VIEW_PREBILL_IN_ENTRATA                             => '/?module=ps_lead-new&load_tab=10&is_global=0',
		self::UNIT_ADDRESSES_ARE_CORRECT_MATCH_USPS_AND_CORRECT_IN_OUR_SYSTEM                         => '/?module=ps_lead-new&load_tab=9&is_global=0',
		self::SQFT_AND_BEDROOM_AMOUNTS_ARE_CORRECT                                                    => '/?module=ps_lead-new&load_tab=9&is_global=0',
		self::NON_BILLABLE_UNITS_ARE_MARKED                                                           => '/?module=ps_lead-new&load_tab=9&is_global=0',
		self::NO_VILATIONS_OF_STATE_REGULATIONS                                                       => '',
		self::TEXAS_CORRECT_CADS                                                                      => '',
		self::CHECK_IF_PROPERTY_IS_REGISTERED_WITH_TCEQ                                               => '',
		self::SALES_TAX                                                                               => '',
		self::TX_BASES_ARE_CREATED                                                                    => '',
		self::ALL_BILLING_METHODS_ARE_ENTERED_CORRECTLY_AND_CONFIRMED_BY_PREVIOUS_PROVIDER_AND_CLIENT => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::RAMP_UP_DATES_ARE_ENTERED                                                               => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::CADS_ARE_ENTERED_CORRECTLY                                                              => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::MOVEIN_FEE_IS_ENTERED                                                                   => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::CONVERGENT_BILLING_FEE_IS_ENTERED                                                       => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::PSI_FEE_ENTERED                                                                         => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::IF_SERVICE_FEE_IS_LINKED_TO_CONVEREGENT_BILLING_MAKE_SURE_IT_IS_SET_TO_IS_FIXED         => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::VCR_IS_SET_UP_FOR_APPLICABLE_UNTILITIES                                                 => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::VCR_FEES_ARE_SET_UP                                                                     => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::CO_ACCT_CHARGE_CODES_GRACE_PERIOD_WAIVER_AMT_AND_PROVIDER_ARE_SET_UP                    => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::HIGH_VACANT_USAGE_EMAIL_IS_SET_UP_IN_ENTRATA                                            => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::METERED_PROPERTIES                                                                      => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::ALL_TRANSPONDERS_ARE_IMPROTED_TO_UNITS                                                  => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::BEGINNING_AND_ENDING_READS_ARE_IMPORTED_AND_CORRECT                                     => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::MODEM_NUMBER_IS_RECORDED                                                                => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::IF_CITY_RATES_THEY_HAVE_BEEN_APPROVED_BY_KIM_COLE                                       => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::BILLING_DETAIL_IS_COMPLETE                                                              => '/?module=ps_lead-new&load_tab=10&is_global=0',
		self::PROPERTY_INFO_IS_COMPLETE                                                               => '/?module=ps_lead-new&load_tab=10&is_global=0',
		self::COMPANY_BILLING_SETTINGS_IS_COMPLETE                                                    => '/?module=ps_lead-new&load_tab=10&is_global=0',
		self::PREBILL_GENERATION_SETTINGS_ARE_COMPLETE                                                => '/?module=ps_lead-new&load_tab=10&is_global=0',
		self::INTEGRATION_SETTINGS_ARE_CORRECT                                                        => '/?module=ps_lead-new&load_tab=10&is_global=0',
		self::TAX_RATE_SETTINGS                                                                       => '/?module=ps_lead-new&load_tab=10&is_global=0',
		self::ROLLOUT_TYPE_SET                                                                        => '/?module=ps_lead-new&load_tab=10&is_global=0',
		self::COMPLETE_LIST_OF_PROVIDER_ACCOUNTS_AND_CONFIRMATION_FROM_PROPERTY                       => '/?module=utility_providers-new&action=view_utility_providers',
		self::BILL_TEMPLATES_ARE_CREATED                                                              => '/?module=utility_bill_templates-new&action=view_utility_bill_templates',
		self::ALL_PROVIDER_ACCOUNTS_ARE_CREATED                                                       => '/?module=utility_providers-new&action=view_utility_providers',
		self::BILLS_FOR_THE_FIRST_MONTH_ARE_UPLOADED_AND_PROCESSED_CORRECTLY                          => '/?module=ps_lead-new&load_tab=4&is_global=0',
		self::EACH_UTILITY_HAS_CHARGE_CODE_ENTERED_CORRECTLY_IN_CLIENT_ADMIN                          => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::BASE_CHARGES_ARE_ENTERED_FOR_EACH_UTLILITY                                              => '/?module=ps_lead-new&load_tab=1&is_global=0',
		self::DEFAULT_BILL_DATES_ARE_SET                                                              => '/?module=ps_lead-new&load_tab=10&is_global=0'
	];

	/**
	 * Get Functions
	 */

	public function getUtilityApprovalStepTypeName() {
		return $this->m_strUtilityApprovalStepTypeName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['utility_approval_step_type_name'] ) ) {
			$this->setUtilityApprovalStepTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['utility_approval_step_type_name'] ) : $arrmixValues['utility_approval_step_type_name'] );
		}
	}

	public function setUtilityApprovalStepTypeName( $strUtilityApprovalStepTypeName ) {
		$this->m_strUtilityApprovalStepTypeName = $strUtilityApprovalStepTypeName;
	}

}
?>