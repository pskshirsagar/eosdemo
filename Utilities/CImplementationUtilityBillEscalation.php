<?php

class CImplementationUtilityBillEscalation extends CBaseImplementationUtilityBillEscalation {

	protected $m_strBillEscalateTypeName;
	protected $m_strMemo;

	/**
	 *Set Methods
	 */

	public function setValues( $arrmixValues,$boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['bill_escalate_type_name'] ) ) {
			$this->setBillEscalateTypeName( $arrmixValues['bill_escalate_type_name'] );
		}
		if( true == isset( $arrmixValues['memo'] ) ) {
			$this->setMemo( $arrmixValues['memo'] );
		}
	}

	public function setBillEscalateTypeName( $strBillEscalateTypeName ) {
		$this->m_strBillEscalateTypeName = $strBillEscalateTypeName;
	}

	public function setMemo( $strMemo ) {
		$this->m_strMemo = $strMemo;
	}

	/**
	 *Get Methods
	 */

	public function getBillEscalateTypeName() {
		return $this->m_strBillEscalateTypeName;
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationUtilityBillId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillEscalateTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEscalationNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEscalatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEscalatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletionNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>