<?php

class CFdgDataLogType extends CBaseFdgDataLogType {

	const SENT				= 1;
	const RECEIVED			= 2;
	const NOT_ASSOCIATED	= 3;
	const PARSED			= 4;
	const RETURNED_TO_FDG	= 5;
	const SENT_TO_MANUAL	= 6;

}
?>