<?php

define( 'CLONE_VALIDATE_INSERT', 'clone_insert' );

class CUtilityBillAccountUnit extends CBaseUtilityBillAccountUnit {

	protected $m_strAccountNumber;

	const CLONE_VALIDATE_INSERT = 'clone_insert';

	public function valPropertyUnitId() {
		$boolIsValid = true;
		if( true == is_null( $this->getPropertyUnitId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', 'Property unit is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case CLONE_VALIDATE_INSERT:
				$boolIsValid &= $this->valPropertyUnitId();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = $strAccountNumber;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['account_number'] ) ) {
			$this->setAccountNumber( $arrmixValues['account_number'] );
		}
	}

}
?>