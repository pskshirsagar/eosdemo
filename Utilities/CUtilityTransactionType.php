<?php

class CUtilityTransactionType extends CBaseUtilityTransactionType {

   const CONVERGENT				= 1;
   const ESTIMATED_USAGE		= 2;
   const ACTUAL_USAGE			= 3;
   const BILLING_FEE			= 4;
   const MOVE_IN_FEE			= 5;
   const TERMINATION_FEE		= 6;
   const VCR_USAGE_FEE			= 7;
   const VCR_SERVICE_FEE		= 8;
   const VCR_LATE_FEE			= 9;
   const LATE_FEE				= 10;
   const PRE_POSTED_CONVERGENT	= 11;
   const BASE_FEE				= 12;
   const SUBSIDY				= 13;
   const CAP					= 14;
   const MOVE_OUT_USAGE			= 15;
   const TAX					= 16;
   const UTILITY_TAX			= 17;
   const SUBSIDY_BILLING_FEE	= 18;

	public static function getUtilityTransactionTypeIds() {
		return [
			self::CONVERGENT, self::ESTIMATED_USAGE, self::ACTUAL_USAGE,
			self::BILLING_FEE, self::MOVE_IN_FEE, self::TERMINATION_FEE,
			self::VCR_USAGE_FEE, self::VCR_SERVICE_FEE, self::VCR_LATE_FEE,
			self::LATE_FEE, self::PRE_POSTED_CONVERGENT, self::BASE_FEE,
			self::SUBSIDY, self::CAP, self::TAX
		];
	}

	public static function getUtilityTransactionTypeNames() {
		$arrstrUtilityTransactionTypeNames = [];

		$arrstrUtilityTransactionTypeNames[self::CONVERGENT] 				= 'Convergent';
		$arrstrUtilityTransactionTypeNames[self::ESTIMATED_USAGE] 		= 'Estimated Usage';
		$arrstrUtilityTransactionTypeNames[self::ACTUAL_USAGE] 			= 'Actual Usage';
		$arrstrUtilityTransactionTypeNames[self::BILLING_FEE] 			= 'Billing Fee';
		$arrstrUtilityTransactionTypeNames[self::MOVE_IN_FEE] 			= 'Move In Fee';
		$arrstrUtilityTransactionTypeNames[self::TERMINATION_FEE] 		= 'Termination Fee';
		$arrstrUtilityTransactionTypeNames[self::VCR_USAGE_FEE] 			= 'VCR Usage Fee';
		$arrstrUtilityTransactionTypeNames[self::VCR_SERVICE_FEE] 		= 'VCR Service Fee';
		$arrstrUtilityTransactionTypeNames[self::VCR_LATE_FEE] 			= 'VCR Late Fee';
		$arrstrUtilityTransactionTypeNames[self::LATE_FEE] 				= 'Late Fee';
		$arrstrUtilityTransactionTypeNames[self::PRE_POSTED_CONVERGENT] 	= 'Pre Posted Convergent';
		$arrstrUtilityTransactionTypeNames[self::BASE_FEE] 				= 'Base Fee';
		$arrstrUtilityTransactionTypeNames[self::SUBSIDY] 				= 'Subsidy';
		$arrstrUtilityTransactionTypeNames[self::CAP] 					= 'Cap';
		$arrstrUtilityTransactionTypeNames[self::TAX] 					= 'Tax';

		return $arrstrUtilityTransactionTypeNames;
	}

	public static function getConvergentExcludeUtilityTransactionTypeIds() {
		return [
			self::CONVERGENT 				=> self::CONVERGENT,
			self::PRE_POSTED_CONVERGENT 	=> self::PRE_POSTED_CONVERGENT,
			self::TAX 		                => self::TAX
		];
	}

	public static $c_arrintVcrUtilityTransactionTypeIds = [
		self::VCR_USAGE_FEE,
		self::VCR_SERVICE_FEE
	];

	public static function getDeleteTransactionTypeIdsGeneratedInPreBill() {
		return [
			self::ESTIMATED_USAGE,
			self::ACTUAL_USAGE,
			self::BILLING_FEE,
			self::BASE_FEE,
			self::SUBSIDY,
			self::CAP,
			self::MOVE_IN_FEE,
			self::VCR_USAGE_FEE,
			self::VCR_SERVICE_FEE,
			self::UTILITY_TAX
		];
	}

}
?>
