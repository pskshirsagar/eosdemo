<?php

class CUtilityPropertyUnit extends CBaseUtilityPropertyUnit {

    protected $m_arrobjPropertyUnitUtilityTypes;
	protected $m_strPropertyUnitAddress;

    public function setPropertyUnitUtilityTypes( $arrobjPropertyUnitUtilityTypes ) {
    	$this->m_arrobjPropertyUnitUtilityTypes = $arrobjPropertyUnitUtilityTypes;
    }

    public function setPropertyUnitUtilityType( $objPropertyUnitUtilityType ) {
    	$this->m_arrobjPropertyUnitUtilityTypes[] = $objPropertyUnitUtilityType;
    }

	public function setPropertyUnitAddress( $strPropertyUnitAddress ) {
		$this->m_strPropertyUnitAddress = $strPropertyUnitAddress;
	}

	public function getPropertyUnitAddress() {
		return $this->m_strPropertyUnitAddress;
	}

    public function getPropertyUnitUtilityTypes() {
    	return $this->m_arrobjPropertyUnitUtilityTypes;
    }

	public function setValues( $arrmixValues, $boolStripSlashes = false, $boolDirectSet = false ) {
		if( true == isset( $arrmixValues['property_unit_address'] ) ) $this->setPropertyUnitAddress( $arrmixValues['property_unit_address'] );
		return parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	public function valPropertyUnitAddress() {

    	$boolIsValid = true;

    	if( ( true == is_null( $this->getStreetLine1() ) ) && ( true == is_null( $this->getStreetLine2() ) ) && ( true == is_null( $this->getStreetLine3() ) ) ) {

    		$boolIsValid &= false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_address', 'Please add atleast one street address.' ) );
    	}

    	if( true == is_null( $this->getCity() ) ) {
    		$boolIsValid &= false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'Please add city.' ) );
    	}

    	if( true == is_null( $this->getPostalCode() ) || 0 < strlen( $this->getPostalCode() ) ) {
    		$strZipCodeCheck = '/(^\d{5}$)|(^\d{5}-\d{4}$)/';
    		if( preg_match( $strZipCodeCheck, $this->getPostalCode() ) !== 1 ) {
    			$boolIsValid &= false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code',  'Enter valid postal code.' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case 'VALIDATE_INSERT_RESIDENT_ADDRESS':
        		$boolIsValid &= $this->valPropertyUnitAddress();
        		break;

            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>