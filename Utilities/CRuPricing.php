<?php

class CRuPricing extends CBaseRuPricing {

	protected $m_fltPerInvoiceAccountAmount;
	protected $m_fltPerRuInvoiceAmount;

	public function setPerInvoiceAccountAmount( $fltPerInvoiceAccountAmount ) {
		$this->m_fltPerInvoiceAccountAmount = $fltPerInvoiceAccountAmount;
	}

	public function setPerRuInvoiceAmount( $fltPerRuInvoiceAmount ) {
		$this->m_fltPerRuInvoiceAmount = $fltPerRuInvoiceAmount;
	}

	public function setIsPerRuInvoicePricing( $boolIsPerRuInvoicePricing ) {
		$this->set( 'm_boolIsPerRuInvoicePricing', CStrings::strToBool( $boolIsPerRuInvoicePricing ) );
	}

	public function getPerInvoiceAccountAmount() {
		return $this->m_fltPerInvoiceAccountAmount;
	}

	public function getPerRuInvoiceAmount() {
		return $this->m_fltPerRuInvoiceAmount;
	}

	public function getIsPerRuInvoicePricing() {
		return $this->m_boolIsPerRuInvoicePricing;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['per_invoice_account_amount'] ) ) {
			$this->setPerInvoiceAccountAmount( $arrmixValues['per_invoice_account_amount'] );
		}

		if( true == isset( $arrmixValues['per_ru_invoice_amount'] ) ) {
			$this->setPerRuInvoiceAmount( $arrmixValues['per_ru_invoice_amount'] );
		}

		if( true == isset( $arrmixValues['is_per_ru_invoice_pricing'] ) ) {
			$this->setIsPerRuInvoicePricing( $arrmixValues['is_per_ru_invoice_pricing'] );
		}

	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property id is required.' ) );
		}
		return $boolIsValid;
	}

	public function valConvergentBillingFeeAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidizedConvergentBillingFeeAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInFeeAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutFeeAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVcrServiceFeeAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVcmAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChargeForPostage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSkipLog = false ) {

		$arrstrOriginalValues = $arrstrOriginalValueChanges = [];
		$boolChangeFlag				= false;

		$objRuPricing = new CRuPricing();
		$arrmixOriginalValues = fetchData( 'SELECT * FROM ru_pricings WHERE id = ' . ( int ) $this->getId(), $objDatabase );

		$arrstrTempOriginalValues = $arrmixOriginalValues[0];

		foreach( $arrstrTempOriginalValues as $strKey => $mixOriginalValue ) {

			$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strGetFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
			$strSetFunctionName = 'set' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

			$objRuPricing->$strSetFunctionName( $mixOriginalValue );

			if( $this->$strGetFunctionName() != $objRuPricing->$strGetFunctionName() ) {
				$arrstrOriginalValueChanges[$strKey] 	= $this->$strSqlFunctionName();
				$arrstrOriginalValues[$strKey] 			= $objRuPricing->$strSqlFunctionName();
				$boolChangeFlag 						= true;
			}
		}

		$boolUpdateStatus = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUpdateStatus ) {

			if( false == $boolSkipLog && false != $boolChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( $arrstrTempOriginalValues['property_id'] );
				$objUtilitySettingLog->setTableName( 'ru_pricings' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		return $boolUpdateStatus;

	}

}
?>