<?php

class CBillEventReference extends CBaseBillEventReference {

	const ASSOCIATION = 1;
	const PROCESSING  = 2;
	const ESCALATION  = 3;
	const AUDITS      = 4;
	const EXPORT      = 5;
	const ESTIMATION  = 6;

	public static function getBillEventReferenceTitle() {

		return [
			self::ASSOCIATION => 'Association',
			self::PROCESSING  => 'Processing',
			self::ESCALATION  => 'Escalation',
			self::AUDITS      => 'Audits',
			self::EXPORT      => 'Export',
			self::ESTIMATION  => 'Estimation'
		];
	}

	public static function getBillEventReferenceMessage() {

		return [
			self::ASSOCIATION => 'Associated',
			self::PROCESSING  => 'Processed',
		];
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>