<?php

class CUtilityDistributionStep extends CBaseUtilityDistributionStep {

	const GENERATED			= 1;
   	const APPROVED			= 2;
   	const EMAILED			= 3;
	const MAILED			= 4;
   	const AUTO_BILLED		= 5;
   	const COMPLETED			= 6;

}
?>