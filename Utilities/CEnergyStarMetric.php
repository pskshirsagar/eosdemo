<?php

class CEnergyStarMetric extends CBaseEnergyStarMetric {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEnergyStarCustomerPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSiteTotal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSourceTotal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSiteIntensity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDesignMetrics() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetailMetrics() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNoScoreReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastSyncOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>