<?php

class CMeterRate extends CBaseMeterRate {

	protected $m_intPropertyId;

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId );
	}

	/**
	 * Get Functions
	 */

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	/**
	 * Other Functions
	 */

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getId() ) ) {
			return parent::insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$arrstrTempOriginalValues = $arrstrOriginalValues = $arrstrOriginalValueChanges = $arrmixOriginalValues = [];
		$boolChangeFlag				= false;

		$objPropertyUtilityType = \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchPropertyUtilityTypeById( $this->getPropertyUtilityTypeId(), $objDatabase );

		if( false == valObj( $objPropertyUtilityType, 'CPropertyUtilityType' ) ) {
			return false;
		}

		$objMeterRate = new CMeterRate();
		$arrmixOriginalValues = \Psi\Eos\Utilities\CMeterRates::createService()->fetchCustomMeterRateById( $this->getId(), $objDatabase, true );
		$arrstrTempOriginalValues = $arrmixOriginalValues[0];

		if( true == valArr( $arrstrTempOriginalValues ) ) {
			foreach( $arrstrTempOriginalValues as $strKey => $mixOriginalValue ) {

				$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
				$strGetFunctionName = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
				$strSetFunctionName = 'set' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

				$objMeterRate->$strSetFunctionName( $mixOriginalValue );

				if( $this->$strGetFunctionName() != $objMeterRate->$strGetFunctionName() ) {
					$arrstrOriginalValueChanges[$objPropertyUtilityType->getName() . '~' . $strKey]	= $this->$strSqlFunctionName();
					$arrstrOriginalValues[$objPropertyUtilityType->getName() . '~' . $strKey]		= $objMeterRate->$strSqlFunctionName();
					$boolChangeFlag																	= true;
				}
			}
		}

		$boolUpdateStatus = parent::insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false != $boolUpdateStatus && true == valArr( $arrstrTempOriginalValues ) ) {

			if( false != $boolChangeFlag ) {
				$objUtilitySettingLog = new CUtilitySettingLog();

				$objUtilitySettingLog->setCid( $arrstrTempOriginalValues['cid'] );
				$objUtilitySettingLog->setPropertyId( ( int ) $objPropertyUtilityType->getPropertyId() );
				$objUtilitySettingLog->setTableName( 'meter_rates' );
				$objUtilitySettingLog->setReferenceId( $this->getId() );
				$objUtilitySettingLog->setOldData( serialize( array_map( 'htmlentities', $arrstrOriginalValues ) ) );
				$objUtilitySettingLog->setNewData( serialize( array_map( 'htmlentities', $arrstrOriginalValueChanges ) ) );
				$objUtilitySettingLog->setCompanyUserId( $intCurrentUserId );

				if( false == $objUtilitySettingLog->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}

			return $boolUpdateStatus;
		}

		return false;
	}

}
?>