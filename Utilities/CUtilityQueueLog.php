<?php

class CUtilityQueueLog extends CBaseUtilityQueueLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityQueueId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valItemCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLateCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>