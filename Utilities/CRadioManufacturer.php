<?php

class CRadioManufacturer extends CBaseRadioManufacturer {

	public function valName( $objDatabase ) {

        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
            return $boolIsValid;

        } elseif( 0 < preg_match( '@[^a-z0-9 ]+@i', $this->getName() ) ) {
           	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Special characters are not allowed in name.' ) );
            return $boolIsValid;
       	}

        $strSql = ' WHERE name = \'' . $this->getName() . '\'';

        if( 0 < $this->getId() ) {
        	$strSql .= ' AND id != ' . ( int ) $this->getId();
        }

        $intRadioManufacturerCount = \Psi\Eos\Utilities\CRadioManufacturers::createService()->fetchRadioManufacturerCount( $strSql, $objDatabase );

        if( 0 < $intRadioManufacturerCount ) {
           	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name already exists.' ) );
            return $boolIsValid;
        }

        return $boolIsValid;
	}

	public function valDependancy( $objDatabase ) {

		$boolIsValid = true;
		$intRadioPartTypeCount 	 	 = \Psi\Eos\Utilities\CRadioPartTypes::createService()->fetchRadioPartTypeCount( 'WHERE radio_manufacturer_id = ' . ( int ) $this->getId(), $objDatabase );
		$intPropertyMeterDeviceCount = \Psi\Eos\Utilities\CPropertyMeterDevices::createService()->fetchPropertyMeterDeviceCount( 'WHERE radio_manufacturer_id = ' . ( int ) $this->getId(), $objDatabase );

     	if( 0 < $intRadioPartTypeCount ) {
           	$boolIsValid = false;
           	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Cannot delete radio manufacturer ' . $this->getName() . ' as it depends on radio part type.' ) );
     	}

		if( 0 < $intPropertyMeterDeviceCount ) {
           	$boolIsValid = false;
           	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Cannot delete radio manufacturer ' . $this->getName() . ' as it dependent on property meter device.' ) );
		}

		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valDependancy( $objDatabase );
           		break;

           	default:
           		// default case
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }

}
?>