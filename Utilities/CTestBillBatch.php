<?php

class CTestBillBatch extends CBaseTestBillBatch {

	protected $m_strBatchResult;

	const BATCH_STATUS_NEW         = 'New';
	const BATCH_STATUS_IN_PROGRESS = 'In Progress';
	const BATCH_STATUS_COMPLETE    = 'Complete';

	public function setBatchResult( $strBatchResult ) {
		$this->m_strBatchResult = $strBatchResult;
	}

	public function getBatchResult() {
		return $this->m_strBatchResult;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>