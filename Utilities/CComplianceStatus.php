<?php

class CComplianceStatus extends CBaseComplianceStatus {

	const REJECTED		= 1;
	const NON_COMPLIANT	= 2;
	const REQUIRED		= 3;
	const PENDING		= 4;
	const COMPLIANT		= 5;
	const INCOMPLETE	= 'Incomplete';
	const EXEMPT		= 6;

	public function valId() {
		return true;
	}

	public function valName() {
		return true;
	}

	public function valDescription() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valOrderNum() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function assignSmartyComplianceStatuses( $objSmarty ) {

		$objSmarty->assign( 'COMPLIANCE_STATUS_REJECTED', self::REJECTED );
		$objSmarty->assign( 'COMPLIANCE_STATUS_NON_COMPLIANT', self::NON_COMPLIANT );
		$objSmarty->assign( 'COMPLIANCE_STATUS_REQUIRED', self::REQUIRED );
		$objSmarty->assign( 'COMPLIANCE_STATUS_PENDING', self::PENDING );
		$objSmarty->assign( 'COMPLIANCE_STATUS_COMPLIANT', self::COMPLIANT );
		$objSmarty->assign( 'COMPLIANCE_STATUS_EXEMPT', self::EXEMPT );
	}

	public static function getComplianceStatusName( $intComplianceStatusId ) {

		$strComplianceStatusName = '';

		switch( $intComplianceStatusId ) {

			case self::REJECTED:
				$strComplianceStatusName = 'REJECTED';
				break;

			case self::NON_COMPLIANT:
				$strComplianceStatusName = 'NON-COMPLIANT ';
				break;

			case self::REQUIRED:
				$strComplianceStatusName = 'REQUIRED';
				break;

			case self::PENDING:
				$strComplianceStatusName = 'PENDING';
 				break;

			case self::COMPLIANT:
				$strComplianceStatusName = 'COMPLIANT';
				break;

			case self::EXEMPT:
				$strComplianceStatusName = 'EXEMPT';
				break;

			default:
		}
		return $strComplianceStatusName;
	}

	public static function loadComplianceStatusNames() {

		$arrstrComplianceStatusNames[self::REJECTED]		= 'Rejected';
		$arrstrComplianceStatusNames[self::NON_COMPLIANT]	= 'Non-compliant';
		$arrstrComplianceStatusNames[self::REQUIRED]		= 'Required';
		$arrstrComplianceStatusNames[self::PENDING]			= 'Pending';
		$arrstrComplianceStatusNames[self::COMPLIANT]		= 'Compliant';
		$arrstrComplianceStatusNames[self::EXEMPT]			= 'Exempt';

		return $arrstrComplianceStatusNames;
	}

}
?>