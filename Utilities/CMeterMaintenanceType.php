<?php

class CMeterMaintenanceType extends CBaseMeterMaintenanceType {

	protected $m_objMeterMaintenanceType;

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
			return $boolIsValid;
		} elseif( 0 < preg_match( '@[^a-z0-9 ]+@i', $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Special characters are not allowed in name.' ) );
			return $boolIsValid;
		}

		$this->m_objMeterMaintenanceType = \Psi\Eos\Utilities\CMeterMaintenanceTypes::createService()->fetchMeterMaintenanceTypeByName( $this->getName(), $objDatabase );

		if( true == valObj( $this->m_objMeterMaintenanceType, 'CMeterMaintenanceType' ) && $this->getId() != $this->m_objMeterMaintenanceType->getId() ) {
			$boolIsValid &= false;

			$strMsg = 'Name already exists.';

			if( false == is_null( $this->m_objMeterMaintenanceType->getDeletedBy() ) && false == is_null( $this->m_objMeterMaintenanceType->getDeletedOn() ) ) {

				$strMsg = 'Name already exists. Maintenance type already exist and its deleted, <a onclick="loadUpdateDialog(this);" id=' . $this->m_objMeterMaintenanceType->getId() . '>Click</a> here to edit and enable.';
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', $strMsg ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>