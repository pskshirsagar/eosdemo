<?php

class CComplianceExemption extends CBaseComplianceExemption {

	protected $m_intDatePart;

	private $m_arrstrExcemptionTimeFrames;

	const THIRTY_DAYS       = 30;
	const SIXTY_DAYS        = 60;
	const NINTY_DAYS        = 90;
	const SIX_MONTHS        = 6;
	const PERMANENT	        = 'PERMANENT';
	const CUSTOM_END_DATE   = 'CUSTOM_END_DATE';

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getExcemptionTimeFrames() {
		if( true == valArr( $this->m_arrstrExcemptionTimeFrames ) ) {
			return $this->m_arrstrExcemptionTimeFrames;
		}

		$this->m_arrstrExcemptionTimeFrames = [
			self::THIRTY_DAYS		=> __( '{%d, 0} days', [ 30 ] ),
			self::SIXTY_DAYS		=> __( '{%d, 0} days', [ 60 ] ),
			self::NINTY_DAYS		=> __( '{%d, 0} days', [ 90 ] ),
			self::SIX_MONTHS		=> __( '{%d, 0} months', [ 6 ] ),
			self::PERMANENT			=> __( 'Permanent' ),
			self::CUSTOM_END_DATE	=> __( 'Custom End Date' )
		];

		return $this->m_arrstrExcemptionTimeFrames;
	}

	public function setDatePart( $intDatePart ) {
		$this->m_intDatePart = ( int ) $intDatePart;
	}

	public function getDatePart() {
		return $this->m_intDatePart;
	}

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valApPayeeId() {
		return true;
	}

	public function valApLegalEntityId() {
		return true;
	}

	public function valPropertyId() {
		return true;
	}

	public function valExemptionDatetime() {
		return true;
	}

	public function valExemptUntilDate() {
		return true;
	}

	public function valExemptionReason() {
		$boolIsValid = true;
		if( true == is_null( $this->getExemptionReason() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'exemption_reason', 'Exemption reason is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valExemptionReason();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['date_part'] ) ) {
			$this->setDatePart( $arrmixValues['date_part'] );
		}
	}

	public function createComplianceExemptionPropertyGroup() {
		$objComplianceExemptionPropertyGroup = new CComplianceExemptionPropertyGroup();

		$objComplianceExemptionPropertyGroup->setCid( $this->getCid() );
		$objComplianceExemptionPropertyGroup->setComplianceExemptionId( $this->getId() );

		return $objComplianceExemptionPropertyGroup;
	}

	public function createComplianceExemptionEntity() {
		$objComplianceExemptionEntity = new CComplianceExemptionEntity();

		$objComplianceExemptionEntity->setCid( $this->getCid() );
		$objComplianceExemptionEntity->setComplianceExemptionId( $this->getId() );

		return $objComplianceExemptionEntity;
	}

	public function createComplianceExemptionComplianceItem() {
		$objComplianceExemptionComplianceItem = new CComplianceExemptionComplianceItem();

		$objComplianceExemptionComplianceItem->setCid( $this->getCid() );
		$objComplianceExemptionComplianceItem->setComplianceExemptionId( $this->getId() );

		return $objComplianceExemptionComplianceItem;
	}

	public function getExemptionTimeFrame() {

		if( true == valStr( $this->getExemptionDatetime() ) && true == is_null( $this->getExemptUntilDate() ) ) {
			return self::PERMANENT;
		}

		$intComplianceExemption = NULL;

		if( true == valStr( $this->getExemptionDatetime() ) && false == is_null( $this->getExemptUntilDate() ) ) {

			$strExemptionDatetime   = new DateTime( $this->getExemptionDatetime() );
			$strExemptUntilDate     = new DateTime( $this->getExemptUntilDate() );

			$objInterval    = $strExemptionDatetime->diff( $strExemptUntilDate );

			if( CComplianceExemption::SIX_MONTHS == $objInterval->m ) {
				return CComplianceExemption::SIX_MONTHS;
			}

			$intDayInterval = $objInterval->days;

			switch( $intDayInterval ) {

				case CComplianceExemption::THIRTY_DAYS:
					$intComplianceExemption = CComplianceExemption::THIRTY_DAYS;
					break;

				case CComplianceExemption::SIXTY_DAYS:
					$intComplianceExemption = CComplianceExemption::SIXTY_DAYS;
					break;

				case CComplianceExemption::NINTY_DAYS:
					$intComplianceExemption = CComplianceExemption::NINTY_DAYS;
					break;

				default:
					// default value
					$intComplianceExemption = CComplianceExemption::CUSTOM_END_DATE;
			}
		}

		return $intComplianceExemption;

	}

	public function getCustomExemptUntilDate( $strExemptionDateTime, $strCustomEndDate ) {

		$strExemptUntilDate = NULL;

		switch( $strExemptionDateTime ) {

			case CComplianceExemption::SIX_MONTHS:
				$strExemptUntilDate = date( 'Y-m-d H:i:s', strtotime( '+' . $strExemptionDateTime . ' months' ) );
				break;

			case CComplianceExemption::PERMANENT:
				$strExemptUntilDate = NULL;
				break;

			case CComplianceExemption::THIRTY_DAYS:
			case CComplianceExemption::SIXTY_DAYS:
			case CComplianceExemption::NINTY_DAYS:
				$strExemptUntilDate = date( 'Y-m-d H:i:s', strtotime( '+' . $strExemptionDateTime . ' days' ) );
				break;

			case CComplianceExemption::CUSTOM_END_DATE:
				$strExemptUntilDate = $strCustomEndDate;
				break;

			default:
				// default value
				break;
		}

		return $strExemptUntilDate;

	}

}
?>