<?php

class CScreeningPackageApplicantTypeAssociation extends CBaseScreeningPackageApplicantTypeAssociation {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScreeningPackageId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScreeningApplicantTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsActive() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function archiveScreeningPackageApplicantTypeAssociation( $intCid, $intPropertyId, $intUserId, $objScreeningDatabase ) {

		$boolIsValid = true;

    	$strSql = 'UPDATE
				       screening_package_applicant_type_associations
				   SET
				       is_active = 0,
				       updated_by = ' . ( int ) $intUserId . ',
				       updated_on = NOW ( )
				   WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		$objScreeningDatabase->begin();

		$boolIsValid = fetchData( $strSql, $objScreeningDatabase );

		if( false == $boolIsValid ) $objScreeningDatabase->rollback();

		$objScreeningDatabase->commit();

    	return $boolIsValid;
    }

}
?>