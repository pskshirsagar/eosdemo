<?php

use Psi\Eos\Admin\CWebsiteTemplates;
use Psi\Eos\Entrata\CWebsites;

use Psi\Eos\Admin\CWebsiteTemplateSlots;

class CWebsiteTemplate extends CBaseWebsiteTemplate {

	const SEQUOIA_PREMIUM = 370;
	const PARCELS_ONLY    = 468;
	const RESIDENTS       = 469;
	const LOBBY_DISPLAY_A = 287;
	const LOBBY_DISPLAY_B = 288;

	protected $m_intWebsiteCount;
	protected $m_intWebsiteTemplateTypeId;
	protected $m_boolIsAuthorizedUser;

	protected $m_strTemplateDisplayTypes;

	protected $m_arrobjWebsiteTemplateSlots;
	protected $m_arrobjWebsiteTemplateCounts;

	public function __construct() {
		parent::__construct();
		$this->setAllowDifferentialUpdate( true );
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrintValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrintValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrintValues['website_count'] ) ) {
			$this->setWebsiteCount( $arrintValues['website_count'] );
		}
		if( true == isset( $arrintValues['website_template_type_id'] ) ) {
			$this->setWebsiteTemplateTypeId( $arrintValues['website_template_type_id'] );
		}
		if( true == isset( $arrintValues['template_display_types'] ) ) {
			$this->setTemplateDisplayTypes( $arrintValues['template_display_types'] );
		}

		return;
	}

	public function setWebsiteTemplateSlots( $arrobjWebsiteTemplateSlots ) {
		$this->m_arrobjWebsiteTemplateSlots = $arrobjWebsiteTemplateSlots;
	}

	public function setWebsiteCount( $intWebsiteCount ) {
		$this->m_intWebsiteCount = $intWebsiteCount;
	}

	public function setTemplateDisplayTypes( $strTemplateDisplayTypes ) {
		$this->m_strTemplateDisplayTypes = $strTemplateDisplayTypes;
	}

	public function setWebsiteTemplateTypeId( $intWebsiteTemplateTypeId ) {
		$this->m_intWebsiteTemplateTypeId = $intWebsiteTemplateTypeId;
	}

	public function setAuthorizedUser( $boolAuthorizedUser ) {
		$this->m_boolIsAuthorizedUser = $boolAuthorizedUser;
	}

	/**
	 * Get Functions
	 */

	public function getWebsiteCount() {
		return $this->m_intWebsiteCount;
	}

	public function getTemplateDisplayTypes() {
		return $this->m_strTemplateDisplayTypes;
	}

	public function getWebsiteTemplateTypeId() {
		return $this->m_intWebsiteTemplateTypeId;
	}

	public function getWebsiteTemplateSlots() {
		return $this->m_arrobjWebsiteTemplateSlots;
	}

	public function getWebsiteTemplateCleanName() {
		// remove whitespaces and template name in camel case.
		return preg_replace( '/\s+/', '', \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) );
	}

	public function getAuthorizedUser() {
		return $this->m_boolIsAuthorizedUser;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchWebsites( $objDatabase ) {
		return CWebsites::createService()->fetchCustomActiveClientsWebsitesByWebsiteTemplateId( $this->m_intId, $objDatabase );
	}

	public function fetchTemplateAssociateWebsites( $objDatabase, $intCid = NULL ) {
		return CWebsites::createService()->fetchCustomTemplateAssociateActiveClientsWebsitesByWebsiteTemplateId( $this->m_intId, $objDatabase, $intCid );
	}

	public function fetchCustomSearchTemplateAssociateWebsites( $intWebsitesId = NULL, $strWebsitesName = NULL, $mixWebsitesCompanyName = NULL, $strWebsitesDomainType = NULL, $objDatabase ) {
		return CWebsites::createService()->fetchCustomSearchTemplateAssociateActiveClientsWebsitesByWebsiteTemplateId( $this->m_intId, $intWebsitesId, $strWebsitesName, $mixWebsitesCompanyName, $strWebsitesDomainType, $objDatabase );
	}

	public function fetchTemplateNavigationOptions( $objDatabase ) {
		return CTemplateNavigationOptions::fetchTemplateNavigationOptionsByWebsiteTemplateId( $this->m_intId, $objDatabase );
	}

	public function fetchWebsiteTemplateById( $intWebsiteTemplateId, $objDatabase ) {
		return CWebsiteTemplates::createService()->fetchWebsiteTemplateById( $intWebsiteTemplateId, $objDatabase );
	}

	public function fetchWebsiteTemplateSlotById( $intWebsiteTemplateSlotId, $objAdminDatabase ) {
		return CWebsiteTemplateSlots::createService()->fetchWebsiteTemplateSlotById( $intWebsiteTemplateSlotId, $objAdminDatabase );
	}

	public function fetchWebsiteTemplateSlots( $objAdminDatabase ) {
		return CWebsiteTemplateSlots::createService()->fetchWebsiteTemplateSlotsByWebsiteTemplateId( $this->m_intId, $objAdminDatabase );
	}

	public function fetchPublishedWebsiteTemplateSlots( $objAdminDatabase ) {
		return CWebsiteTemplateSlots::createService()->fetchPublishedWebsiteTemplateSlotsByWebsiteTemplateId( $this->m_intId, $objAdminDatabase );
	}

	public function fetchPublishedOldWebsiteTemplateSlots( $intWebsiteTemplateId, $objAdminDatabase ) {
		return CWebsiteTemplateSlots::createService()->fetchPublishedWebsiteTemplateSlotsByWebsiteTemplateId( $intWebsiteTemplateId, $objAdminDatabase );
	}

	public function fetchWebsiteTemplateSlotsByTemplateSlotTypeId( $objAdminDatabase, $intTemplateSlotTypeId = NULL ) {
		return CWebsiteTemplateSlots::createService()->fetchWebsiteTemplateSlotsByWebsiteTemplateIdByTemplateSlotTypeId( $this->m_intId, $objAdminDatabase, $intTemplateSlotTypeId );
	}

	public function fetchPublishedWebsiteTemplateSlotByKey( $strKey, $objAdminDatabase ) {
		return CWebsiteTemplateSlots::createService()->fetchPublishedWebsiteTemplateSlotByWebsiteTemplateIdByKey( $this->m_intId, $strKey, $objAdminDatabase );
	}

	/**
	 * Get Or Fetch Functions
	 */

	// This is a special function written to load slots in every app layer PBY.

	public function getOrFetchWebsiteTemplateSlots( $objAdminDatabase, $intTemplateSlotTypeId = NULL ) {

		if( true == valArr( $this->m_arrobjWebsiteTemplateSlots ) ) {
			return $this->m_arrobjWebsiteTemplateSlots;
		} else {
			$this->setWebsiteTemplateSlots( $this->fetchWebsiteTemplateSlotsByTemplateSlotTypeId( $objAdminDatabase, $intTemplateSlotTypeId ) );

			return $this->getWebsiteTemplateSlots();
		}
	}

	public function getOrFetchCachedWebsiteTemplateSlots( $objDatabase ) {

		if( false == valArr( $this->m_arrobjWebsiteTemplateSlots ) ) {

			$this->m_arrobjWebsiteTemplateSlots = CCache::fetchObject( 'CWebsiteTemplateSlots_' . $this->getId() );

			if( false === $this->m_arrobjWebsiteTemplateSlots || false == valArr( $this->m_arrobjWebsiteTemplateSlots ) ) {

				$this->setWebsiteTemplateSlots( $this->fetchWebsiteTemplateSlotsByTemplateSlotTypeId( $objDatabase, $intTemplateSlotTypeId = NULL ) );
				CCache::storeObject( 'CWebsiteTemplateSlots_' . $this->getId(), $this->m_arrobjWebsiteTemplateSlots, $intSpecificLifetime = 3600, $arrstrTags = [] );
			}
		}

		return $this->m_arrobjWebsiteTemplateSlots;
	}

	/**
	 * Create Functions
	 */

	public function createWebsiteTemplateSlot() {
		$objWebsiteTemplateSlot = new CWebsiteTemplateSlot();
		$objWebsiteTemplateSlot->setWebsiteTemplateId( $this->m_intId );

		return $objWebsiteTemplateSlot;
	}

	public function createTemplateWidget() {
		$objTemplateWidget = new CDefaultTemplateWidget();
		$objTemplateWidget->setWebsiteTemplateId( $this->m_intId );

		return $objTemplateWidget;
	}

	public function createWebsiteSettingKey() {
		$objWebsiteSettingKey = new CWebsiteSettingKey();
		$objWebsiteSettingKey->setWebsiteTemplateId( $this->getId() );
		$objWebsiteSettingKey->setWebsiteSettingGroupId( CWebsiteSettingGroup::TEMPLATE_SPECIFIC_SETTINGS );

		return $objWebsiteSettingKey;
	}

	/**
	 * Validate Functions
	 */

	public function valName( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Design name is required.' ) );
		}

		if( true == isset( $objDatabase ) ) {
			$objPreexistingWebsiteTemplate = CWebsiteTemplates::createService()->fetchWebsiteTemplateByName( $this->getId(), $this->m_strName, $objDatabase );

			if( true == valObj( $objPreexistingWebsiteTemplate, 'CWebsiteTemplate' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'template_name', 'A design with this name already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPath( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'path', 'File path is required.' ) );
		}

		if( true == isset( $objDatabase ) ) {
			$intCountPreexistingWebsiteTemplatePath = CWebsiteTemplates::createService()->fetchWebsiteTemplateCountByPath( $this->getId(), $this->m_strPath, $objDatabase );

			if( 0 < $intCountPreexistingWebsiteTemplatePath ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'template_path', 'A design with this file path already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeprecatedOn() {

		$boolIsValid        = true;

		if( false == is_null( $this->getDeprecatedOn() ) && ( date( 'Y-m-d', strtotime( $this->getDeprecatedOn() ) ) < date( 'Y-m-d' ) ) ) {

			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deprecated_on', 'End of Life will not be less than current date.' ) );

		}
		return $boolIsValid;
	}

	public function valIsPublished( $objDatabase = NULL ) {

		$arrobjWebsiteTemplateCounts     = [];
		$boolIsValid                     = true;
		$objWebsiteTemplateCurrentStatus = CWebsiteTemplates::createService()->fetchWebsiteTemplateById( $this->getId(), $objDatabase );

		if( true == isset( $objDatabase ) ) {
			$arrobjClientDatabases = \Psi\Eos\Connect\CDatabases::createService()->fetchClientDatabasesByDatabaseUserTypeIdByDatabaseTypeId( CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseType::CLIENT, CDatabases::createConnectDatabase() );

			if( true == valArr( $arrobjClientDatabases ) ) {

				foreach( $arrobjClientDatabases as $objClientDatabase ) {

					$objClientDatabase->open();

					if( false == valArr( $arrobjWebsiteTemplateCounts ) ) {
						$arrobjWebsiteTemplateCounts = CWebsiteTemplates::createService()->fetchCustomNewAllSortedWebsiteTemplates( 'ASC', 'id', '', '', '( wt.is_published = 1 )', $objClientDatabase, $strTemplateloadfunctions = NULL, $boolWebsiteStatus = true );

					} else {
						$arrobjWebsiteTemplateCountsTemp = CWebsiteTemplates::createService()->fetchCustomNewAllSortedWebsiteTemplates( 'ASC', 'id', '', '', '( wt.is_published = 1 )', $objClientDatabase, $strTemplateloadfunctions = NULL, $boolWebsiteStatus = true );
						if( true == valArr( $arrobjWebsiteTemplateCountsTemp ) ) {
							foreach( $arrobjWebsiteTemplateCounts as $objWebsiteTemplate ) {
								if( true == array_key_exists( $objWebsiteTemplate->getId(), $arrobjWebsiteTemplateCountsTemp ) && true == valObj( $arrobjWebsiteTemplateCountsTemp[$objWebsiteTemplate->getId()], 'CWebsiteTemplate' ) && 0 < $arrobjWebsiteTemplateCountsTemp[$objWebsiteTemplate->getId()]->getWebsiteCount() ) {
									$arrobjWebsiteTemplateCounts[$objWebsiteTemplate->getId()]->setWebsiteCount( $arrobjWebsiteTemplateCounts[$objWebsiteTemplate->getId()]->getWebsiteCount() + $arrobjWebsiteTemplateCountsTemp[$objWebsiteTemplate->getId()]->getWebsiteCount() );
								}
							}
						}
					}

					$objClientDatabase->close();
				}
			}

			if( 0 == $objWebsiteTemplateCurrentStatus->getIsPublished() && 0 == $this->m_intIsPublished ) {
				$boolIsValid = true;

			} elseif( true == isset( $arrobjWebsiteTemplateCounts[$this->getId()] ) && 0 < $arrobjWebsiteTemplateCounts[$this->getId()]->getWebsiteCount() && 0 == $this->m_intIsPublished ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', 'This website design is associated to enabled or disabled company website(s). You cannot unpublish it.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valPath( $objDatabase );
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valDeprecatedOn();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valPath( $objDatabase );
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valDeprecatedOn();
				$boolIsValid &= $this->valIsPublished( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function updateSequence( $objClientDatabase ) {
		$intNextSeqId = $this->getId() + 1;
		$arrmixData   = fetchData( 'SELECT setval( \'public.website_templates_id_seq\', ' . ( int ) $intNextSeqId . ' , FALSE );', $objClientDatabase );

		if( 0 >= $arrmixData[0]['setval'] ) {
			return false;
		}

		return true;
	}

	public function fetchCustomWebsiteTemplateById( $intWebsiteTemplateId = NULL, $objDatabase ) {

		$strSql = '
				SELECT
				wt.*
				FROM
				website_templates wt
				WHERE wt.id = ' . ( int ) $intWebsiteTemplateId;

		return CWebsiteTemplates::createService()->fetchWebsiteTemplate( $strSql, $objDatabase );
	}

	public function fetchCustomDefaultTemplateWidget( $objDatabase ) {
		return \Psi\Eos\Entrata\CDefaultTemplateWidgets::createService()->fetchCustomDefaultTemplateWidgetByTemplateId( $this->getId(), $objDatabase );
	}

	public function fetchCustomWebsiteTemplateWithWebsiteUsageWithClientByTemplateIdByCid( $intWebsiteTemplateId = NULL, $intCid = NULL, $objDatabase ) {

		$strSql = '
				SELECT
					wt.*,
						(
							SELECT
								count ( w.id )
							FROM
								websites w
								JOIN clients c ON ( w.website_template_id = wt.id AND w.cid = c.id AND c.id = ' . ( int ) $intCid . ' )
						) website_count,
						CASE
							WHEN wt.is_responsive = 1  THEN  btrim(\'Responsive,\' || replace(dt_ids.template_display_types,\'Premium\',\'\'),\',\')
							ELSE dt_ids.template_display_types
						END as template_display_types
				 FROM
					website_templates wt
				 		LEFT JOIN (
							SELECT
								array_to_string ( array_agg ( DISTINCT ( wdt.name ) ), \',\' ) AS template_display_types,
								tdt.website_template_id
							FROM
								template_display_types tdt JOIN website_display_types wdt ON ( tdt.website_display_type_id = wdt.id AND tdt.website_template_id = ' . ( int ) $intWebsiteTemplateId . ' )
							GROUP BY tdt.website_template_id
						) as dt_ids ON ( dt_ids.website_template_id = wt.id )

				WHERE wt.id = ' . ( int ) $intWebsiteTemplateId;

		return CWebsiteTemplates::createService()->fetchWebsiteTemplate( $strSql, $objDatabase );
	}

	public function fetchAllActiveWebsiteWigetsByWebsiteIdBySupportedType( $intWebsiteId, $intSupportedType, $intCid, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteWidgets::createService()->fetchAllActiveWebsiteWigetsByWebsiteIdBySupportedTypeByWebsiteTemplateIdByCid( $intWebsiteId, $intSupportedType, $this->getId(), $intCid, $objDatabase );
	}

	public function getInsertSql( $intCurrentUserId ) {
		return $this->insert( $intCurrentUserId, NULL, true );
	}

	public function getUpdateSql( $intCurrentUserId ) {
		return $this->update( $intCurrentUserId, NULL, true );
	}

}
