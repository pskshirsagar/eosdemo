<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CAlertMessages
 * Do not add any new functions to this class.
 */

class CAlertMessages extends CBaseAlertMessages {

	public static function fetchAlertMessagesByCompanyUserIdsByCids( $arrintCompanyUserIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCompanyUserIds ) || false == valArr( $arrintCids ) ) return;

		$strSql = '
				SELECT
					company_user_id,
					cid,
					MAX( sent_on ) AS sent_on,
					MAX( message ) AS message
				FROM
					alert_messages
				WHERE
					cid IN ( ' . implode( ', ', $arrintCids ) . ' )
					AND company_user_id IN ( ' . implode( ', ', $arrintCompanyUserIds ) . ')
					AND sent_on IS NOT NULL
				GROUP BY
					company_user_id,
					cid';

		return self::fetchAlertMessages( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchAlertMessageByCallId( $intCallId, $objVoipDatabase ) {
		return self::fetchAlertMessage( 'SELECT * FROM alert_messages WHERE call_id = ' . ( int ) $intCallId . ' AND message IS NOT NULL LIMIT 1', $objVoipDatabase );
	}

	public static function fetchAlertMessageByCompanyUserIdByCidByChatId( $intCompanyUserId, $intCid, $intChatId, $objDatabase ) {

		$strSql = 'SELECT
					company_user_id,
					cid,
					message
				FROM
					alert_messages
				WHERE
					cid = ' . ( int ) $intCid . '
					AND company_user_id = ' . ( int ) $intCompanyUserId . '
					AND message LIKE \'%' . $intChatId . '%\'
				ORDER BY id DESC
				LIMIT 1';

		return self::fetchAlertMessage( $strSql, $objDatabase );
	}

}
?>