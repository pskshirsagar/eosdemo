<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallPriorities
 * Do not add any new functions to this class.
 */

class CCallPriorities extends CBaseCallPriorities {

	public static function fetchCallPriority( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallPriority', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>