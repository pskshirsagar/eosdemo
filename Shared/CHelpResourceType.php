<?php

class CHelpResourceType extends CBaseHelpResourceType {

	const ASSESSMENTS               = 1;
	const TOOL_TIPS                 = 2;
	const HELP_TIPS                 = 3;
	const VIDEO                     = 4;
	const ARTICLE                   = 5;
	const FAQ                       = 6;
	const STEP_BY_STEP_INSTRUCTIONS = 7;
	const SIMULATION                = 8;
	const COURSES                   = 9;
	const ADMIN_GUIDE               = 10;
	const INTERACTIVE_COURSE        = 11;
	const CUSTOM_COURSE             = 12;
	const HELP_SECTION              = 13;
	const HELP_TEXT                 = 14;
	const HELP_IMAGE                = 15;
	const HELP_LINK                 = 16;
	const HELP_FILE                 = 17;
	const LEARNING_TRACK            = 18;

	public static $c_arrintHelpResourceTypeIds = [ 'article' => self::ARTICLE, 'admin_guide' => self::ADMIN_GUIDE, 'video' => self::VIDEO ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	* If you are making any changes in loadSmartyConstants function then
	* please make sure the same changes would be applied to loadTemplateConstants function also.
	*/

	public static function loadSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'HELP_RESOURCE_TYPE_ASSESSMENTS',					self::ASSESSMENTS );
		$objSmarty->assign( 'HELP_RESOURCE_TYPE_TOOL_TIPS',						self::TOOL_TIPS );
		$objSmarty->assign( 'HELP_RESOURCE_TYPE_HELP_TIPS',						self::HELP_TIPS );
		$objSmarty->assign( 'HELP_RESOURCE_TYPE_VIDEO',							self::VIDEO );
		$objSmarty->assign( 'HELP_RESOURCE_TYPE_ARTICLE',						self::ARTICLE );
		$objSmarty->assign( 'HELP_RESOURCE_TYPE_FAQ',							self::FAQ );
		$objSmarty->assign( 'HELP_RESOURCE_TYPE_STEP_BY_STEP_INSTRUCTIONS',		self::STEP_BY_STEP_INSTRUCTIONS );
		$objSmarty->assign( 'HELP_RESOURCE_TYPE_SIMULATION',					self::SIMULATION );
		$objSmarty->assign( 'HELP_RESOURCE_TYPE_COURSES',						self::COURSES );
		$objSmarty->assign( 'HELP_RESOURCE_TYPE_ADMIN_GUIDE',					self::ADMIN_GUIDE );
		$objSmarty->assign( 'HELP_RESOURCE_TYPE_INTERACTIVE_COURSE',			self::INTERACTIVE_COURSE );
	}

	public static function loadTemplateConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_ASSESSMENTS']					= self::ASSESSMENTS;
		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_TOOL_TIPS']					= self::TOOL_TIPS;
		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_HELP_TIPS']					= self::HELP_TIPS;
		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_VIDEO']						= self::VIDEO;
		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_ARTICLE']						= self::ARTICLE;
		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_FAQ']							= self::FAQ;
		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_STEP_BY_STEP_INSTRUCTIONS']	= self::STEP_BY_STEP_INSTRUCTIONS;
		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_SIMULATION']					= self::SIMULATION;
		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_COURSES']						= self::COURSES;
		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_ADMIN_GUIDE']					= self::ADMIN_GUIDE;
		$arrmixTemplateParameters['HELP_RESOURCE_TYPE_INTERACTIVE_COURSE']			= self::INTERACTIVE_COURSE;
		$arrmixTemplateParameters['HELP_RESOURCE_HELP_TEXT']						= self::HELP_TEXT;
		$arrmixTemplateParameters['HELP_RESOURCE_HELP_IMAGE']						= self::HELP_IMAGE;
		$arrmixTemplateParameters['HELP_RESOURCE_HELP_LINK']						= self::HELP_LINK;
		$arrmixTemplateParameters['HELP_RESOURCE_HELP_FILE']						= self::HELP_FILE;

		return $arrmixTemplateParameters;
	}

}
?>