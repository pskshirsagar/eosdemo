<?php

class CHelpTopicModule extends CBaseHelpTopicModule {

	protected $m_strHelpTopicTitle;

    public function getHelpTopicTitle() {
    	return $this->m_strHelpTopicTitle;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['title'] ) ) $this->setHelpTopicTitle( $arrmixValues['title'] );

    	return;
    }

    public function setHelpTopicTitle( $strHelpTopicTitle ) {
    	$this->m_strHelpTopicTitle = $strHelpTopicTitle;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valModuleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHelpTopicId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	$this->setDeletedBy( $intCurrentUserId );
    	$this->setDeletedOn( 'NOW()' );

    	return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    }

}
?>