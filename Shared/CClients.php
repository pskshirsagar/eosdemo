<?php

use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see        \Psi\Eos\Admin\CClients
 * Do not add any new functions to this class.
 */
class CClients extends CBaseClients {

	public static $c_arrintMacroScriptCids = [ 2004, 3170, 4027, 1557, 3692, 2981, 2680, 1092, 2138, 9779, 10150, 10151, 10152, 4715, 235 ];
	public static $c_arrintPreScreeningFixChargeCids = [ 4396, 4924, 7292 ];
	public static $c_intNullCid = 1;

	// commenting this function because this table is dynamic and we are unnecessary storing data in cache by using this function in other functions.[DevOps]
	// public static function fetchClients( $strSql, $objDatabase ) {
	//	return self::fetchCachedObjects( $strSql, 'CClient', $objDatabase, DATA_CACHE_MEMORY );
	// }

	// public static function fetchClient( $strSql, $objDatabase ) {
	//	return self::fetchCachedObject( $strSql, 'CClient', $objDatabase, DATA_CACHE_MEMORY );
	// }

	public static function fetchEligibleClientsByPsLeadId( $intPsLeadId, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON ( lower( mc.company_name ), mc.id )
						mc.*
					FROM
						clients mc
					WHERE
						mc.ps_lead_id = ' . ( int ) $intPsLeadId . '
					ORDER BY
						lower( mc.company_name ), mc.id';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchEligibleClientsByPsLeadIdsByStatusTypeIds( $arrintPsLeadIds, $arrintCompanyStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintPsLeadIds ) || false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						DISTINCT ON ( lower( mc.company_name ), mc.id )
						mc.*
					FROM
						clients mc
					WHERE
						mc.ps_lead_id IN ( ' . implode( ',', $arrintPsLeadIds ) . ' )
						AND mc.database_id = ' . ( int ) $objDatabase->getId() . '
						AND mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
					ORDER BY
						lower( mc.company_name ), mc.id';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsByScriptId( $intScriptId, $objDatabase ) {
		$strSql = 'SELECT
						mc.*
					FROM
						clients mc,
						company_scripts cc
					WHERE
						mc.id=cc.cid
						AND cc.script_id =' . ( int ) $intScriptId;

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchActiveClientsByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						mc.*
					FROM
						clients mc,
						ps_lead_details pld
					WHERE
						mc.id = pld.cid
						AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND ( pld.sales_employee_id = ' . ( int ) $intEmployeeId . ' OR pld.support_employee_id = ' . ( int ) $intEmployeeId . ' )';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsByCompanyStatusTypeId( $intCompanyStatusTypeId, $objDatabase, $strOrderBy = 'ASC' ) {
		$strSql = 'SELECT * FROM clients WHERE company_status_type_id = ' . ( int ) $intCompanyStatusTypeId . ' ORDER BY lower( company_name ) ' . $strOrderBy . '';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsIdsByCompanyStatusTypeId( $intCompanyStatusTypeId, $objDatabase, $strOrderBy = 'ASC' ) {

		$strSql = 'SELECT id
					FROM clients
					WHERE company_status_type_id = ' . ( int ) $intCompanyStatusTypeId . '
					ORDER BY lower( company_name ) ' . $strOrderBy . '';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsByCompanyStatusTypeIds( $arrintCompanyStatusTypeIds, $objDatabase ) {
		$strSql = 'SELECT * FROM clients WHERE company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' ) ORDER BY lower( company_name ) ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsByPsProductId( $intPsProductId, $objDatabase, $boolLiveCompaniesOnly = false ) {

		$strSql = 'SELECT
							*
						FROM
							( SELECT
								DISTINCT ON (mc.id) mc.*
								FROM
									clients mc,
									contracts c,
									contract_properties cp
								WHERE
									mc.id = c.cid
									AND c.id = cp.contract_id ';

		if( true == $boolLiveCompaniesOnly ) {
			$strSql .= '			AND mc.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT . ' ';
		}

		$strSql .= ' AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
					AND cp.ps_product_id = ' . ( int ) $intPsProductId . ' ) AS sub_query1
						ORDER BY lower( company_name ) ASC ';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchActiveAndTemporaryClientsByPsProductId( $intPsProductId, $objDatabase, $boolLiveCompaniesOnly = false ) {
		if( false == valId( $intPsProductId ) ) {
			return NULL;
		}

		$strCondition = '';

		if( true == $boolLiveCompaniesOnly ) {
			$strCondition = ' AND mc.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT;
		}

		$strSql = ' SELECT
						*
					FROM
						( 
							(	SELECT
									DISTINCT ON (mc.id) mc.*
								FROM
									clients mc
									JOIN contracts c ON( mc.id = c.cid )
									JOIN contract_properties cp ON( c.id = cp.contract_id AND c.cid = cp.cid )
								WHERE
									c.contract_status_type_id = ' . ( int ) CContractStatusType::CONTRACT_APPROVED . '
									AND cp.ps_product_id = ' . ( int ) $intPsProductId . $strCondition . '
							)
							UNION
							(	SELECT
									DISTINCT ON (mc.id) mc.*
								FROM
									clients mc
									JOIN simple_contracts sc ON( mc.id = sc.cid )
								WHERE
									sc.ps_product_id = ' . ( int ) $intPsProductId . $strCondition . '
									AND ( sc.expire_on > NOW() OR sc.expire_on IS NULL )
							)
						) AS sub_query
					ORDER BY 
						lower( company_name ) ASC ';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchTestClientsByPsProductId( $intPsProductId, $arrintCompanyStatusTypeIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT (mc.id), mc.company_name
					FROM
						clients mc
					JOIN
						contract_properties cp on (mc.id = cp.cid)
					WHERE
						mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						AND cp.ps_product_id = ' . ( int ) $intPsProductId . '
					ORDER BY mc.company_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsByPsProductIds( $arrintPsProductIds, $objDatabase, $boolLiveCompaniesOnly = false, $boolIsIncludeTerminated = false ) {

		if( false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$arrintCompanyStatusTypeIds[]  = CCompanyStatusType::CLIENT;
		$arrintContractStatusTypeIds[] = CContractStatusType::CONTRACT_APPROVED;

		if( true == $boolIsIncludeTerminated ) {
			$arrintCompanyStatusTypeIds[]  = CCompanyStatusType::TERMINATED;
			$arrintContractStatusTypeIds[] = CContractStatusType::CONTRACT_TERMINATED;
		}

		$strSql = 'SELECT
							*
						FROM
							( SELECT
								DISTINCT ON (mc.id) mc.*
								FROM
									clients mc,
									contracts c
								WHERE
									EXISTS ( select 1 from contract_properties cp where c.id = cp.contract_id AND cp.cid = mc.id AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) )
									AND mc.id = c.cid ';

		if( true == $boolLiveCompaniesOnly ) {
			$strSql .= ' AND mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ')';
 		}

		$strSql .= ' AND c.contract_status_type_id IN (' . implode( ',', $arrintContractStatusTypeIds ) . ') ) AS sub_query1
						ORDER BY lower( company_name ) ASC ';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientByRwxDomain( $strRwxDomain, $objDatabase ) {
		$strSql = 'SELECT * FROM clients WHERE lower( rwx_domain ) = \'' . ( string ) addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strRwxDomain ) ) ) . '\'';

		return self::fetchClient( $strSql, $objDatabase );
	}

	public static function fetchClientsByStatusTypeId( $intStatusTypeId, $objDatabase ) {
		return self::fetchClients( sprintf( 'SELECT * FROM %s WHERE company_status_type_id = \'%d\' ORDER BY company_name ASC', 'clients', ( int ) $intStatusTypeId ), $objDatabase );
	}

	public static function fetchClientsByStatusTypeIds( $arrintCompanyStatusTypeIds, $objDatabase ) {
		return self::fetchClients( sprintf( 'SELECT * FROM %s WHERE company_status_type_id IN ( %s ) ORDER BY company_name ASC', 'clients', implode( ',', $arrintCompanyStatusTypeIds ) ), $objDatabase );
	}

	public static function fetchClientsByDatabaseIdByStatusTypeId( $intDatabaseId, $intStatusTypeId, $objDatabase ) {
		$strSql = 'SELECT
						id,
						company_name,
						rwx_domain
					FROM
						clients
					WHERE
						database_id = ' . ( int ) $intDatabaseId . '
						AND company_status_type_id = ' . ( int ) $intStatusTypeId . ' ORDER BY company_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsByDatabaseIdByStatusTypeIds( $intDatabaseId, $arrintCompanyStatusTypeIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT * FROM clients WHERE database_id = ' . ( int ) $intDatabaseId . ' AND company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' ) ORDER BY company_name ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchCustomClientsByPropertyIds( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT DISTINCT ON ( mc.id ) mc.* FROM clients mc, properties cp WHERE cp.cid = mc.id AND cp.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchCustomClientByPropertyId( $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT mc.* FROM clients mc, properties cp WHERE cp.cid = mc.id AND cp.id = ' . ( int ) $intPropertyId;

		return self::fetchClient( $strSql, $objDatabase );
	}

	public static function fetchClientsByIdsByCompanyStatusTypeIds( $arrintCids, $arrintCompanyStatusTypeIds, $objDatabase ) {
		if( false == ( $arrintCids = getIntValuesFromArr( $arrintCids ) ) || false == ( $arrintCompanyStatusTypeIds = getIntValuesFromArr( $arrintCompanyStatusTypeIds ) ) ) {
			return NULL;
		}

		return self::fetchClients( 'SELECT * FROM clients WHERE id IN ( ' . implode( ',', $arrintCids ) . ' ) AND company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' ) ORDER BY lower( company_name ) ASC', $objDatabase );
	}

	public static function fetchClientsByIds( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSelectClause  = '';
		$strJoinClause    = '';
		$strClientIdsList = implode( ',', $arrintCids );
		$strClientIdsList = trim( $strClientIdsList, ',' );

		if( true == empty( $strClientIdsList ) ) {
			return NULL;
		}

		if( CDatabaseType::ADMIN == $objDatabase->getDatabaseTypeId() ) {
			$strSelectClause = ', pl.id AS ps_lead_id ';
			$strJoinClause   = ' JOIN ps_leads pl ON( mc.ps_lead_id = pl.id AND pl.deleted_by IS NULL ) ';
		}

		$strSql = 'SELECT
						mc.* ' . $strSelectClause . '
					FROM
						clients mc ' . $strJoinClause . '
					WHERE
						mc.id IN ( ' . $strClientIdsList . ' )
					ORDER BY
						lower( mc.company_name ) ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsByIdsOrderedById( $arrintCids, $objDatabase ) {
		$strSql = 'SELECT
						mc.*
					FROM
						clients mc
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
					ORDER BY
						mc.id ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsByIdsByCompanyStatusIdOrderedById( $arrintCids, $intCompanyStatusId, $objDatabase ) {
		$strSql = 'SELECT
						mc.*
					FROM
						clients mc
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
					AND
						mc.company_status_type_id = ' . ( int ) $intCompanyStatusId . '
					ORDER BY
						mc.id ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientById( $intCid, $objDatabase ) {
		return parent::fetchClient( 'SELECT * FROM clients WHERE id =' . ( int ) $intCid, $objDatabase );
	}

	public static function fetchClientByIds( $arrintCids, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						clients
					WHERE
						id IN ( ' . implode( ',', $arrintCids ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCachedClientById( $intCid, $objDatabase ) {

		$objClient = CCache::fetchObject( 'CClient_' . $intCid );

		if( false === $objClient || false == valObj( $objClient, 'CClient' ) ) {
			$objClient = self::fetchClientById( $intCid, $objDatabase );
			CCache::storeObject( 'CClient_' . $intCid, $objClient, $intSpecificLifetime = 3600, $arrstrTags = [] );
		}

		return $objClient;
	}

	public static function fetchClientByInvoiceId( $intInvoiceId, $objDatabase ) {

		$strSql = 'SELECT
						mc.*
					FROM
						clients as mc
					JOIN invoices as inv ON ( inv.cid = mc.id	AND inv.id = ' . ( int ) $intInvoiceId . ' )';

		return self::fetchClient( $strSql, $objDatabase );
	}

	public static function fetchAllClients( $objDatabase ) {
		$strSql = 'SELECT * FROM clients ORDER BY company_name';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchAllClientsByStatusTypeIds( $arrintCompanyStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						*
					FROM
						clients
					WHERE company_status_type_id NOT IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' ) ORDER BY company_name ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchAllClientIdsAndNames( $objDatabase ) {
		$strSql = 'SELECT id,company_name FROM clients ORDER BY company_name ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchAllClientlists( $objDatabase ) {
		$strSql = '	SELECT
						id,
						company_name
					FROM
						clients
					ORDER BY company_name';

		$arrmixClientlists = fetchData( $strSql, $objDatabase );

		return $arrmixClientlists;
	}

	public static function fetchSearchedSimpleClientsByCompanyName( $strCompanyName, $objDatabase, $intLimit = 10 ) {
		$strSql = 'SELECT id, company_name FROM clients WHERE company_name ILIKE \'%' . $strCompanyName . '%\' ORDER BY company_name LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientlistByStatusTypeIds( $arrintCompanyStatusTypeIds, $objDatabase, $arrintDatabaseIds = NULL ) {
		$strSql = '	SELECT
						id,
						company_name
					FROM
						clients
					WHERE
						company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )';
		if( false != valArr( $arrintDatabaseIds ) ) {
			$strSql .= ' AND database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )';
		}
		$strSql .= 'ORDER BY company_name';

		$arrmixClientlist = fetchData( $strSql, $objDatabase );

		return $arrmixClientlist;
	}

	public static function fetchCustomClientsWithScheduledApTransactionsToProcess( $strDateToProcess, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						clients mc
					WHERE
						id IN ( SELECT
									DISTINCT sath.cid
								FROM
									scheduled_ap_transaction_headers sath
								WHERE
									sath.next_post_date <= \'' . $strDateToProcess . '\'
									AND sath.deleted_on IS NULL
									AND ( sath.is_on_hold IS NULL OR sath.is_on_hold = 0 )
									AND sath.approved_on IS NOT NULL
									AND sath.approved_by IS NOT NULL
								)
					ORDER BY
						mc.id ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchCustomClientsByActivateStandardPosting( $objClientDatabase, $strCustomPgsWhereCondition = '' ) {

		$strSql = '
			SELECT
				mc.*
			FROM
				clients mc
			WHERE
				EXISTS (
					SELECT
						NULL
					FROM
						property_gl_settings pgs
						JOIN properties p ON pgs.cid = p.cid AND pgs.property_id = p.id AND p.is_disabled = 0 AND p.is_test = 0
					WHERE
						pgs.cid = mc.id
						AND pgs.activate_standard_posting = true
						' . $strCustomPgsWhereCondition . '
				)
			ORDER BY
				mc.id';

		return self::fetchClients( $strSql, $objClientDatabase );
	}

	public static function fetchProcessingClients( $boolClientOnly, $objPaymentDatabase ) {

		$strSql = 'SELECT mc.* FROM clients mc,
					company_merchant_accounts cma
					WHERE
					mc.id = cma.cid
					AND cma.is_disabled != 1 ';

		if( true == $boolClientOnly ) {
			$strSql .= ' AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '';
		}
		$strSql .= ' ORDER BY mc.company_name';

		return self::fetchClients( $strSql, $objPaymentDatabase );
	}

	public static function fetchAllClientsOnPaymentSearch( $boolClientOnly, $objPaymentDatabase ) {

		$strSql = 'SELECT c.* FROM clients c ';

		if( true == $boolClientOnly ) {
			$strSql .= ' WHERE c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '';
		}
		$strSql .= ' ORDER BY c.company_name';

		return self::fetchClients( $strSql, $objPaymentDatabase );
	}

	public static function fetchClientsByExportBatchId( $intExportBatchId, $objDatabase ) {

		// load summarized clients

		$strSql = 'SELECT
						mc.id,
						mc.entity_id,
						mc.company_name,
						sum( t.transaction_amount ) as unexported_charges
					FROM
						clients mc,
						transactions t
					WHERE
						mc.id = t.cid
						AND t.export_batch_id = ' . ( int ) $intExportBatchId . '
					GROUP BY
						mc.id,
						mc.entity_id,
						mc.company_name
					ORDER BY
						mc.company_name';

		$arrobjClients = self::fetchClients( $strSql, $objDatabase );

		// load summarized accounts

		$strSql = 'SELECT
						a.id,
						a.account_name,
						a.cid,
						sum( t.transaction_amount ) as unexported_charges
					FROM
						clients mc,
						transactions t,
						accounts a
					WHERE
						mc.id = t.cid
						AND a.id = t.account_id
						AND t.export_batch_id = ' . ( int ) $intExportBatchId . '
					GROUP BY
						a.id,
						a.account_name,
						a.cid
					ORDER BY
						a.account_name';

		$arrobjAccounts = \Psi\Eos\Admin\CAccounts::createService()->fetchAccounts( $strSql, $objDatabase );

		// load summarized data per charge code per account

		$strSql = 'SELECT
						a.id AS account_id,
						a.cid,
						a.account_name AS account_name,
						t.charge_code_id,
						cc.name as charge_code_name,
						sum( t.transaction_amount ) as unexported_charges
					FROM
						clients mc,
						transactions t,
						accounts a,
						charge_codes cc
					WHERE
						mc.id = t.cid
						AND a.id = t.account_id
						AND cc.id = t.charge_code_id
						AND t.export_batch_id = ' . ( int ) $intExportBatchId . '
					GROUP BY
						a.id,
						a.account_name,
						a.cid,
						t.charge_code_id,
						cc.name
					ORDER BY
						cc.name';

		$arrstrChargeCodeExportsData = fetchData( $strSql, $objDatabase );

		CObjectModifiers::createService()->nestObjects( $arrobjAccounts, $arrobjClients );

		if( true == valArr( $arrstrChargeCodeExportsData ) ) {
			foreach( $arrstrChargeCodeExportsData as $arrstrChargeCodeExportData ) {
				$arrobjAccounts[$arrstrChargeCodeExportData['account_id']]->addChargeCodeExportData( $arrstrChargeCodeExportData );
			}
		}

		return $arrobjClients;
	}

	public static function fetchCustomePaginatedClients( $intPageNo = 0, $strPageSize = 'ALL', $strWhere = NULL, $intCompanyStatusTypeId = NULL, $strMegaSearch = '', $objDatabase ) {

		$strLimit  = '';
		$strOffset = '';

		if( true == valId( $strPageSize ) ) {
			$strLimit  = ' LIMIT ' . ( int ) $strPageSize;
			$strOffset = ' OFFSET ' . ( 0 < $intPageNo ) ? ( int ) $strPageSize * ( $intPageNo - 1 ) : 0;
		}

		if( true == valStr( $strWhere ) && 0 < strlen( $strWhere ) ) {
			if( true == isset( $intCompanyStatusTypeId ) && true == is_numeric( $intCompanyStatusTypeId ) ) {
				$strWhere .= ' AND company_status_type_id = ' . ( int ) $intCompanyStatusTypeId;
			}

			if( CDatabaseType::CLIENT == $objDatabase->getDatabaseTypeId() ) {

				$strSql = 'SELECT
								id,
								company_name,
								company_status_type_id,
								database_id
							FROM
								(
									SELECT
										DISTINCT ON ( mc.id ) mc.*
									FROM
										clients mc ' . $strMegaSearch . ' ' . $strWhere . '
								) AS sub_query1
							ORDER BY
								LOWER ( company_name ) ' . $strOffset . $strLimit;

			} else {
				$strSql = 'SELECT
								id,
								entity_id,
								company_name,
								company_status_type_id,
								database_id
							FROM (
								SELECT DISTINCT ON ( mc.id )
									mc.* FROM clients mc
									LEFT JOIN persons p ON ( p.cid = mc.id AND p.is_disabled = 0 )
									LEFT JOIN person_contact_preferences pcp ON ( p.id = pcp.person_id ) ' . $strMegaSearch . ' ' . $strWhere . ' ) as sub_query1
							ORDER BY LOWER( company_name ) ' . $strOffset . $strLimit;

			}
		} else {
			if( true == isset( $intCompanyStatusTypeId ) && true == is_numeric( $intCompanyStatusTypeId ) ) {
				$strWhere = ' WHERE company_status_type_id = ' . ( int ) $intCompanyStatusTypeId;
			}
			$strSql = 'SELECT
								id,
								entity_id,
								company_name,
								company_status_type_id,
								database_id
							FROM
								clients ' . $strWhere . '
							ORDER BY
								LOWER ( company_name ) ' . $strOffset . $strLimit;
		}
		$arrobjClients = self::fetchClients( $strSql, $objDatabase );

		return $arrobjClients;
	}

	public static function fetchCustomClientCount( $strWhere = NULL, $intCompanyStatusTypeId = NULL, $boolIsMegaSearch = false, $objDatabase ) {

		if( true == isset( $strWhere ) && 0 < strlen( $strWhere ) ) {

			$strMegaSearch = '';

			if( true == $boolIsMegaSearch ) {

				$strMegaSearch = '
						LEFT OUTER JOIN properties cp ON ( mc.id = cp.cid )
						LEFT OUTER JOIN property_addresses pa ON ( cp.id = pa.property_id and cp.cid = pa.cid AND pa.is_alternate = false )
						LEFT OUTER JOIN website_domains cwd ON ( mc.id = cwd.cid ) ';
			} else {
				$strMegaSearch = ' LEFT OUTER JOIN accounts a ON ( a.cid = mc.id ) ';
			}

			if( true == isset( $intCompanyStatusTypeId ) && true == is_numeric( $intCompanyStatusTypeId ) ) {
				$strWhere .= ' AND company_status_type_id = ' . ( int ) $intCompanyStatusTypeId;
			}

			$strSql = ' SELECT
						count(id)
					FROM (
						SELECT DISTINCT ON ( mc.id )
							mc.* FROM clients mc
							JOIN persons p ON ( p.cid = mc.id )
							JOIN person_contact_preferences pcp ON ( p.id = pcp.person_id ) ' . $strMegaSearch . ' ' . $strWhere . ' ) as sub_query1';

		} else {

			if( true == isset( $intCompanyStatusTypeId ) && true == is_numeric( $intCompanyStatusTypeId ) ) {
				$strWhere = ' WHERE company_status_type_id = ' . ( int ) $intCompanyStatusTypeId;
			}

			$strSql = 'SELECT count(id) FROM clients ' . $strWhere . ' ';
		}

		$arrmixClientCount = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixClientCount ) ) {
			return $arrmixClientCount[0]['count'];
		}

		return 0;
	}

	public static function fetchSimpleClientById( $intCid, $objDatabase ) {
		if( false == is_numeric( $intCid ) ) {
			return NULL;
		}
		$strSql = 'SELECT id, database_id, company_name, rwx_domain, company_status_type_id FROM clients WHERE id = ' . ( int ) $intCid;

		return self::fetchClient( $strSql, $objDatabase );
	}

	public static function fetchCustomIntegratedClientsByCompanyStatusTypeIds( $arrintCompanyStatusTypeIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
							distinct on (mc.company_name) mc.*
						FROM
							clients AS mc,
							integration_databases AS id
						WHERE
							mc.id = id.cid
							AND mc.company_status_type_id IN( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						ORDER BY mc.company_name ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchCustomIntegratedClientsByIntegrationClientTypeIdsByCompanyStatusTypeIds( $arrintIntegrationClientTypeIds, $arrintCompanyStatusTypeIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyStatusTypeIds ) || false == valArr( $arrintIntegrationClientTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
							distinct on (mc.company_name) mc.*
						FROM
							clients AS mc,
							integration_databases AS id
						WHERE
							mc.id = id.cid
							AND id.integration_client_type_id IN ( ' . implode( ',', $arrintIntegrationClientTypeIds ) . ' )
							AND mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						ORDER BY mc.company_name ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchIntegratedClientsByCompanyStatusTypeIdsByIds( $arrintCompanyStatusTypeIds, $arrintIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
							distinct on (mc.company_name) mc.*
						FROM
							clients AS mc,
							integration_databases AS id
						WHERE
							mc.id = id.cid
							AND mc.id IN( ' . implode( ',', $arrintIds ) . ' )
							AND mc.company_status_type_id IN( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						ORDER BY mc.company_name ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchIsIntegratedClientByCid( $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON ( mc.id ) mc.*
					FROM
						integration_databases id
						JOIN clients mc ON ( mc.id = id.cid )
					WHERE
						mc.id = ' . ( int ) $intCid;

		$objClient = self::fetchClient( $strSql, $objClientDatabase );

		if( true == valObj( $objClient, 'CClient' ) ) {
			return true;
		}

		return false;
	}

	public static function fetchAssociatedCompaniesByCommissionStructureId( $intCommissionStructureId, $objDatabase ) {
		$strSql = 'SELECT
						cid,
						account_id
					FROM
						commission_rate_associations cra,
						commission_structures cs
					WHERE
						cra.commission_structure_id = cs.id
						AND cra.deleted_on IS NULL
						AND cs.deleted_on IS NULL
						AND cs.is_published = 1
						AND cs.id = ' . ( int ) $intCommissionStructureId . '
					GROUP BY
						cid, account_id
					ORDER BY cid DESC';

		$arrintAssociatedCompanyIds = fetchData( $strSql, $objDatabase );

		$arrobjClients = [];

		if( true == valArr( $arrintAssociatedCompanyIds ) ) {
			foreach( $arrintAssociatedCompanyIds as $arrintAssociatedCid ) {
				$objClient = parent::fetchClientById( $arrintAssociatedCid['cid'], $objDatabase );

				if( true == isset( $arrintAssociatedCid['account_id'] ) && 0 < $arrintAssociatedCid['account_id'] ) {
					$objAccount = $objClient->fetchAccount( $arrintAssociatedCid['account_id'], $objDatabase );
					if( false == is_null( $objAccount ) ) {
						$objClient->addAccount( $objAccount );
					}
				}
				$arrobjClients[$objClient->getId()] = $objClient;
			}
		}

		return $arrobjClients;
	}

	public static function fetchClientsWithCommissions( $objDatabase ) {

		$strSql = 'SELECT
						mc.*
					FROM
						clients mc
					WHERE
						mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND mc.id IN (
										SELECT
											DISTINCT cid
										FROM
											commission_rate_associations cra WHERE cra.deleted_on IS NULL
											AND cra.start_date <= \'' . date( 'm/d/Y' ) . '\'
											AND cra.end_date > \'' . date( 'm/d/Y' ) . '\'
									) ORDER BY company_name';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsByCompanyStatusTypeIdsByPsProductIds( $arrintCompanyStatusTypeIds, $arrintPsProductIds, $objDatabase ) {

		if( false == valArr( $arrintPsProductIds ) || false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
							*
						FROM
							( SELECT
								DISTINCT ON (mc.id) mc.*
								FROM
									clients mc,
									contracts c,
									contract_properties cp
								WHERE
									mc.id = c.cid
									AND c.id = cp.contract_id
									AND mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
									AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
									AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . ') AS sub_query1
						ORDER BY lower( company_name ) ASC ';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchPaginatedClientsByCompanyStatusTypeIdsByPsProductIds( $intPageNo, $intPageSize, $arrintCompanyStatusTypeIds, $arrintPsProductIds, $objAdminDatabase ) {

		if( false == valArr( $arrintPsProductIds ) || false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$intPageSize = ( int ) $intPageSize;
		$intOffset   = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql = 'SELECT
						DISTINCT( mc.id ),
						mc.*
					FROM
						clients mc
						JOIN contracts c ON ( mc.id = c.cid )
						JOIN contract_properties cp ON ( c.id = cp.contract_id )
					WHERE
						mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
						ORDER BY mc.company_name ASC OFFSET ' . ( int ) $intOffset;
		if( true == ( 0 < $intPageSize ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intPageSize;
		}

		return parent::fetchClients( $strSql, $objAdminDatabase );
	}

	public static function fetchPaginatedClientsByIdsByPsProductIds( $intPageNo, $intPageSize, $arrintIds, $arrintPsProductIds, $objAdminDatabase ) {

		if( false == valArr( $arrintIds ) || false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$intPageSize = ( int ) $intPageSize;
		$intOffset   = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql = 'SELECT
						DISTINCT( mc.id ),
						mc.*
					FROM
						clients mc
						JOIN contracts c ON ( mc.id = c.cid )
						JOIN contract_properties cp ON ( c.id = cp.contract_id )
					WHERE
						mc.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
						ORDER BY mc.company_name ASC OFFSET ' . ( int ) $intOffset;
		if( true == ( 0 < $intPageSize ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intPageSize;
		}

		return parent::fetchClients( $strSql, $objAdminDatabase );
	}

	public static function fetchTotalClientsCountByCompanyStatusTypeIdsByPsProductIds( $arrintCompanyStatusTypeIds, $arrintPsProductIds, $objAdminDatabase ) {

		if( false == valArr( $arrintPsProductIds ) || false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT( DISTINCT( mc.id ) )
					FROM
						clients mc
						JOIN contracts c ON ( mc.id = c.cid )
						JOIN contract_properties cp ON ( c.id = cp.contract_id )
					WHERE
						mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED;

		$arrintResponseCount = fetchData( $strSql, $objAdminDatabase );

		return ( false == is_null( $arrintResponseCount[0]['count'] ) ) ? $arrintResponseCount[0]['count'] : NULL;

	}

	public static function fetchPermissionedClientsByPsProductIds( $arrintPsProductIds, $objDatabase, $boolLiveCompaniesOnly = false, $strTimePeriod = NULL ) {

		$strPeriodSql = '';
		if( true == valStr( $strTimePeriod ) ) {
			if( $strTimePeriod == 'year_to_date' ) {
				$strPeriodSql = 'OR cp.termination_date >= date_trunc( \'year\', NOW() )';
			} elseif( $strTimePeriod == 'last_thirty_days' ) {
				$strPeriodSql = " OR cp.termination_date > ( NOW() - interval '1 month' )";
			}
		}

		$strSql = ' SELECT
						*
					FROM
						( SELECT
							DISTINCT ON (mc.id) mc.*
						FROM
							clients mc,
							contracts c,
							contract_properties cp
						WHERE
							mc.id = c.cid
							AND c.id = cp.contract_id ';

		if( true == $boolLiveCompaniesOnly ) {
			$strSql .= ' AND mc.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT . ' ';
		}

		$strSql .= 'AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
					AND ( cp.termination_date IS NULL OR cp.termination_date > NOW() ' . $strPeriodSql . ' )
					AND cp.renewal_contract_property_id IS NULL
					AND cp.is_last_contract_record = TRUE
					AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
					) AS sub_query1
				ORDER BY lower( company_name ) ASC ';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchSimpleClientsByPsProductIdByDatabaseId( $intPsProductId, $intDatabaseId, $objDatabase, $boolLiveCompaniesOnly = false ) {

		$arrintClientIds = [];

		$strSql = 'SELECT
							id
					FROM
						( SELECT
								DISTINCT ON (mc.id) mc.id
							FROM
								clients mc,
								contracts c,
								contract_properties cp
							WHERE
								mc.id = c.cid
								AND c.id = cp.contract_id ';

		if( true == $boolLiveCompaniesOnly ) {
			$strSql .= '		AND mc.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT . ' ';
		}

		$strSql .= ' 			AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
								AND mc.database_id = ' . ( int ) $intDatabaseId . '
								AND cp.ps_product_id = ' . ( int ) $intPsProductId . '
						) AS sub_query1 ';

		$arrmixResponses = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponses ) ) {
			foreach( $arrmixResponses as $arrmixResponse ) {
				$arrintClientIds[$arrmixResponse['id']] = $arrmixResponse['id'];
			}
		}

		return $arrintClientIds;
	}

	public static function fetchClientsByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchClients( sprintf( 'SELECT * FROM clients WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

	public static function fetchClientByCompanyPhonenumber( $intPhoneNumber, $objDatabase ) {

		$strSql = 'SELECT
						mc.*
					FROM
						clients mc,
						company_phone_numbers cpn
					WHERE
						mc.id=cpn.cid
						AND cpn.phone_number =\'' . $intPhoneNumber . '\' LIMIT 1';

		return self::fetchClient( $strSql, $objDatabase );
	}

	public static function fetchPaginatedClientsDetailsByCids( $arrintCids, $arrstrCompanyFilter, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$intOffset       = ( true == isset( $arrstrCompanyFilter['page_no'] ) && true == isset( $arrstrCompanyFilter['page_size'] ) ) ? $arrstrCompanyFilter['page_size'] * ( $arrstrCompanyFilter['page_no'] - 1 ) : 0;
		$intLimit        = ( true == isset( $arrstrCompanyFilter['page_size'] ) ) ? $arrstrCompanyFilter['page_size'] : '';
		$strOrderByField = ' mc.company_name';
		$strOrderByType  = ' ASC';

		if( true == valArr( $arrstrCompanyFilter ) ) {
			if( true == isset( $arrstrCompanyFilter['order_by_field'] ) ) {

				switch( $arrstrCompanyFilter['order_by_field'] ) {

					case 'company_name':
						$strOrderByField = ' mc.company_name';
						break;

					case 'total_revenue':
						$strOrderByField = ' total_revenue';
						break;

					case 'revenue_per_unit':
						$strOrderByField = ' revenue_per_unit';
						break;

					case 'unit':
						$strOrderByField = ' total_units';
						break;

					case 'unit_in_portfolio':
						$strOrderByField = ' unit_in_portfolio';
						break;

					default:
						$strOrderByField = ' mc.company_name';
						break;
				}
			}

			if( true == isset( $arrstrCompanyFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrCompanyFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}
		}

		$strSql = 'SELECT
						mc.id,
						mc.company_name,
						pl.id AS ps_lead_id,
						MAX( pl.number_of_units ) AS unit_in_portfolio,
						MAX( company_total_units.total_units ) AS total_units,
						SUM( i.invoice_amount ) AS total_revenue,
						CASE
							WHEN
								SUM(i.invoice_amount) > 0 AND MAX( company_total_units.total_units ) > 0
							THEN
								SUM(i.invoice_amount) / MAX( company_total_units.total_units ) ::FLOAT
							ELSE NULL
						END AS revenue_per_unit
					FROM
						clients mc
						JOIN ps_leads pl ON ( mc.ps_lead_id = pl.id AND pl.deleted_by IS NULL )
						JOIN ps_lead_details pld ON ( mc.id = pld.cid )
						LEFT JOIN invoices i ON ( mc.id = i.cid AND i.invoice_batch_id IS NOT NULL AND i.utility_batch_id IS NULL AND i.invoice_date BETWEEN DATE_TRUNC( \'month\', ( NOW() - INTERVAL \'1 months\' ) ) AND DATE_TRUNC( \'month\', NOW() ) - INTERVAL \'1 day\' )
						LEFT JOIN
						(
							SELECT
								property_units.cid,
								sum( property_units.number_of_units ) AS total_units
							FROM
							(
								SELECT
									c.cid,
									COALESCE( p.number_of_units, 0 ) AS number_of_units
								FROM contracts c
									JOIN clients mc ON ( mc.id = c.cid )
									JOIN contract_properties cp ON ( c.id = cp.contract_id AND ( cp.termination_date IS NULL OR cp.termination_date >= DATE_TRUNC( \'month\', NOW() - INTERVAL \'1 months\' ) ) )
									JOIN properties p ON ( cp.property_id = p.id AND mc.id = p.cid )
								WHERE
									mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
									AND cp.renewal_contract_property_id IS NULL
									AND cp.is_last_contract_record = TRUE
									AND c.contract_start_date <= DATE_TRUNC( \'month\', NOW() ) - INTERVAL \'1 day\'
									AND	( c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . ' OR mc.id = c.cid )
								GROUP BY
									mc.id,
									p.id,
									c.id,
									p.number_of_units
								) AS property_units
							WHERE property_units.cid IN ( ' . implode( ',', $arrintCids ) . ' )
							GROUP BY
								property_units.cid
						) AS company_total_units ON ( company_total_units.cid = mc.id )
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
					GROUP BY
						mc.id,
						mc.company_name,
						pl.id
					ORDER BY
						' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		$arrstrTempClientsDetails = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrstrTempClientsDetails ) ) {
			return NULL;
		}

		$arrstrClientsDetails = [];

		foreach( $arrstrTempClientsDetails as $arrstrManagementPsLeadDetails ) {
			$arrstrClientsDetails[$arrstrManagementPsLeadDetails['id']] = $arrstrManagementPsLeadDetails;
		}

		return $arrstrClientsDetails;
	}

	public static function fetchCustomPaginatedActiveClientsByEmployeeId( $intEmployeeId, $arrstrCompanyFilter, $objDatabase, $boolIsClientExecutiveEmployee = false, $boolIsForNewSales = false, $boolIsDefaultDashboardRequest = false ) {

		$intOffset           = ( true == isset( $arrstrCompanyFilter['page_no'] ) && true == isset( $arrstrCompanyFilter['page_size'] ) ) ? $arrstrCompanyFilter['page_size'] * ( $arrstrCompanyFilter['page_no'] - 1 ) : 0;
		$intLimit            = ( true == isset( $arrstrCompanyFilter['page_size'] ) ) ? $arrstrCompanyFilter['page_size'] : '';
		$strOrderByField     = ' mc.company_name';
		$strOrderByType      = ' ASC';
		$strContractClosedOn = '';

		if( true == valArr( $arrstrCompanyFilter ) ) {
			if( true == isset( $arrstrCompanyFilter['order_by_field'] ) ) {

				switch( $arrstrCompanyFilter['order_by_field'] ) {

					case NULL:
					case 'company_name':
						$strOrderByField = ' mc.company_name';
						break;

					case 'setup_revenue':
						$strOrderByField = ' setup_revenue';
						break;

					case 'recurring_revenue':
						$strOrderByField = ' recuring_revenue';
						break;

					case 'transactional_revenue':
						$strOrderByField = ' transactional_revenue';
						break;

					case 'close_date':
						$strOrderByField = ' min ( cp.close_date ) ';
						break;

					default:
						$strOrderByField = ' mc.company_name';
						break;
				}
			}

			if( true == isset( $arrstrCompanyFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrCompanyFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}

			if( true == isset( $arrstrCompanyFilter['close_date'] ) ) {

				switch( $arrstrCompanyFilter['close_date'] ) {
					case NULL:
					case 30:
						$strContractClosedOn = 'today - 30 days';
						break;

					case 60:
						$strContractClosedOn = 'today - 60 days';
						break;

					case 90:
						$strContractClosedOn = 'today - 90 days';
						break;

					case 12:
						$strContractClosedOn = 'today - 12 months';
						break;

					default:
						$strContractClosedOn = 'today - 30 days';
						break;
				}
			}

			if( true == isset( $arrstrCompanyFilter['my_revenue_summary'] ) ) {
				$strContractClosedOn = 'today - ' . $arrstrCompanyFilter['my_revenue_summary'] . ' days';
			}
		}

		$strFromClause   = '';
		$strSelectClause = '';
		$strGroupBy      = '';
		$strCondition    = '';

		$strFromClause = ' JOIN ps_lead_details pld ON ( pld.cid = mc.id ) ';

		$strCondition = ' AND c.contract_type_id <> ' . CContractType::RENEWAL . '
							AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED;

		$strCondition .= ' AND pld.sales_employee_id = ' . ( int ) $intEmployeeId . ' ';

		if( true == isset( $arrstrCompanyFilter['is_from_product'] ) && true == $arrstrCompanyFilter['is_from_product'] ) {
			$strCondition = ' AND c.contract_type_id <> ' . CContractType::RENEWAL . '
							AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . $strCondition;
		}

		if( true == $boolIsClientExecutiveEmployee ) {
			$strFromClause = '	JOIN ps_lead_details pld ON ( pld.cid = mc.id ) ';
			$strCondition  = ' AND c.contract_start_date <= NOW() AND ( cp.close_date <= NOW() OR ( c.contract_type_id = ' . CContractType::RENEWAL . ' ) ) ';
			if( false == $boolIsDefaultDashboardRequest ) {
				$strCondition .= 'AND pld.support_employee_id = ' . ( int ) $intEmployeeId;
			}

			$strSelectClause = ' , count( DISTINCT cp.property_id) as property_count ';
		}

		if( true == $boolIsForNewSales ) {

			$strSelectClause = ', sum(cp.implementation_amount) as setup_revenue,
								sum(cp.monthly_recurring_amount) as recuring_revenue,
								sum(cp.transactional_amount)as transactional_revenue,
								min ( cp.close_date ) as close_date,
								c.id as contract_id,
								c.ps_lead_id,
								c.id as opportunity_id ';

			$strFromClause = ' JOIN properties p ON (p.id = cp.property_id AND p.termination_date IS NULL) ';

			$strCondition = ' AND c.contract_type_id <> ' . CContractType::RENEWAL . '
								AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
								AND cp.close_date <= \'' . date( 'Y-m-d' ) . '\'
								AND cp.close_date >= \'' . date( 'Y-m-d', strtotime( $strContractClosedOn ) ) . '\' ';

			$strCondition .= ' AND c.sales_employee_id = ' . ( int ) $intEmployeeId;

			if( true == isset( $arrstrCompanyFilter['is_from_product'] ) && true == $arrstrCompanyFilter['is_from_product'] ) {
				$strCondition .= ' AND pld.sales_employee_id = ' . ( int ) $intEmployeeId . ' ';
			}

			if( true == isset( $arrstrCompanyFilter['my_revenue_summary'] ) ) {
				$strCondition .= ' AND c.contract_start_date <= NOW() ';
			}

			$strGroupBy = ', c.id,
								c.ps_lead_id ';
		}

		if( true == isset( $arrstrCompanyFilter['search_val'] ) && true == valStr( $arrstrCompanyFilter['search_val'] ) && 0 < strlen( trim( $arrstrCompanyFilter['search_val'] ) ) ) {
			$strCondition .= ' AND mc.company_name ILIKE \'%' . trim( addslashes( $arrstrCompanyFilter['search_val'] ) ) . '%\' ';
		}

		$strSql = 'SELECT
						mc.id, mc.company_name,pl.number_of_units, pl.id AS ps_lead_id ' . $strSelectClause . '
					FROM
						clients mc
						JOIN ps_leads pl ON( mc.ps_lead_id = pl.id AND pl.deleted_by IS NULL )
						JOIN contracts c ON (mc.id = c.cid)
						JOIN contract_properties cp ON (cp.contract_id = c.id)
						' . $strFromClause . '
					WHERE
						mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::TERMINATED . ',' . CCompanyStatusType::TEMPLATE . ' ) AND
						cp.renewal_contract_property_id IS NULL AND
						cp.is_last_contract_record = TRUE AND
						( cp.termination_date IS NULL OR cp.termination_date > NOW() ) ' . $strCondition . '
					GROUP BY
						mc.id,
						mc.company_name,
						pl.number_of_units,
						pl.id ' . $strGroupBy . '
					ORDER BY ' . $strOrderByField . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( false == is_null( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		if( true == $boolIsForNewSales ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchClients( $strSql, $objDatabase );
		}
	}

	public static function fetchCustomPaginatedActiveClientsCountByEmployeeId( $intEmployeeId, $arrstrCompanyFilter, $objDatabase, $boolIsClientExecutiveEmployee = false, $boolIsForNewSales = false, $boolIsDefaultDashboardRequest = false ) {

		$strContractClosedOn = '';

		if( true == valArr( $arrstrCompanyFilter ) ) {
			if( true == isset( $arrstrCompanyFilter['close_date'] ) ) {

				switch( $arrstrCompanyFilter['close_date'] ) {

					case NULL:
					case 30:
						$strContractClosedOn = 'today - 30 days';
						break;

					case 60:
						$strContractClosedOn = 'today - 60 days';
						break;

					case 90:
						$strContractClosedOn = 'today - 90 days';
						break;

					case 12:
						$strContractClosedOn = 'today - 12 months';
						break;

					default:
						$strContractClosedOn = 'today - 30 days';
						break;
				}
			}
		}

		$strFromClause = '';

		$strSelectClause = ' count( DISTINCT mc.id ) ';
		$strCondition    = ' AND c.contract_type_id <> ' . CContractType::RENEWAL . '
							AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
							AND c.sales_employee_id = ' . ( int ) $intEmployeeId;

		if( true == isset( $arrstrCompanyFilter['is_from_product'] ) && true == $arrstrCompanyFilter['is_from_product'] ) {
			$strFromClause .= ' JOIN ps_lead_details pld ON ( pld.cid = mc.id ) ';
			$strCondition  = ' AND c.contract_type_id <> ' . CContractType::RENEWAL . '
								 AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
								 AND pld.sales_employee_id = ' . ( int ) $intEmployeeId;
		}

		if( true == $boolIsClientExecutiveEmployee ) {
			$strFromClause = ' JOIN ps_lead_details pld ON ( pld.cid = mc.id ) ';
			$strCondition  = ' AND ( cp.close_date <= NOW() OR ( c.contract_type_id = ' . CContractType::RENEWAL . ' ) )
									AND c.contract_start_date <= NOW() ';
			if( false == $boolIsDefaultDashboardRequest ) {
				$strCondition .= 'AND pld.support_employee_id = ' . ( int ) $intEmployeeId;
			}

		}

		if( true == $boolIsForNewSales ) {

			$strSelectClause = ' count( DISTINCT c.id ) ';
			$strFromClause   = ' JOIN properties p ON (p.id = cp.property_id AND p.termination_date IS NULL ) ';
			$strCondition    = ' AND c.contract_type_id <> ' . CContractType::RENEWAL . '
								AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
								AND cp.close_date <= \' ' . date( 'Y-m-d' ) . '\'
								AND cp.close_date >= \'' . date( 'Y-m-d', strtotime( $strContractClosedOn ) ) . '\'
								AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED;

			$strCondition .= ' AND c.sales_employee_id = ' . ( int ) $intEmployeeId;

			if( true == isset( $arrstrCompanyFilter['is_from_product'] ) && true == $arrstrCompanyFilter['is_from_product'] ) {
				$strCondition .= ' AND pld.sales_employee_id = ' . ( int ) $intEmployeeId . ' ';
			}
		}

		if( true == isset( $arrstrCompanyFilter['search_val'] ) && true == valStr( $arrstrCompanyFilter['search_val'] ) && 0 < strlen( trim( $arrstrCompanyFilter['search_val'] ) ) ) {
			$strCondition .= ' AND mc.company_name ILIKE \'%' . trim( addslashes( $arrstrCompanyFilter['search_val'] ) ) . '%\'';
		}

		$strSql = 'SELECT
						' . $strSelectClause . '
					FROM
						clients mc
						JOIN contracts c ON (mc.id = c.cid)
						JOIN contract_properties cp ON (cp.contract_id = c.id)
						' . $strFromClause . '
					WHERE
						mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::TERMINATED . ',' . CCompanyStatusType::TEMPLATE . ' ) AND
						cp.renewal_contract_property_id IS NULL AND
						cp.is_last_contract_record = TRUE AND
						( cp.termination_date IS NULL OR cp.termination_date > NOW() ) ' . $strCondition;

		$arrintClientsCount = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintClientsCount[0]['count'] ) ) {
			return $arrintClientsCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchActiveClientsPropertyCountByEmployeeIdByPsProductIds( $intEmployeeId, $arrintPsProductIds, $objDatabase, $boolIsClientExecutiveEmployee = false, $boolIsDefaultDashboardRequest = false, $arrintCids = NULL ) {

		$strPsProductIds = implode( ',', $arrintPsProductIds );

		$strSubSqlJoinCondition = '';
		$strSubSqlCondition     = '';
		$strCondition           = '';

		if( true == $boolIsClientExecutiveEmployee ) {

			if( true == valArr( $arrintCids ) ) {
				$strCondition           .= ' AND mc.id IN( ' . implode( ',', $arrintCids ) . ' )';
				$strSubSqlCondition     = ' AND p.cid IN( ' . implode( ',', $arrintCids ) . ' )';
				$strSubSqlJoinCondition = ' AND from_properties.cid IN( ' . implode( ',', $arrintCids ) . ' )';
			}

			if( false == $boolIsDefaultDashboardRequest ) {
				$strCondition .= ' AND pld.support_employee_id = ' . ( int ) $intEmployeeId;
			}

		}

		$strSql = 'SELECT
						ps_product_id,
						company_name,
						cid,
						total_property_count,
						contract_property_count
					FROM (
							SELECT count(p.id) as total_property_count, p.cid
							FROM properties p
							WHERE p.termination_date IS NULL
							' . $strSubSqlCondition . '
							GROUP BY p.cid
						) as from_properties join (
							SELECT
								mc.id ,
								mc.company_name as company_name,
								count(DISTINCT cp.property_id) as contract_property_count,
								cp.ps_product_id as ps_product_id
								FROM clients mc
								JOIN contracts c ON ( mc.id = c.cid )
								JOIN contract_properties cp ON ( cp.contract_id = c.id AND cp.cid = c.cid )
								JOIN ps_lead_details pld ON ( pld.cid = mc.id )
								JOIN properties p ON ( p.id = cp.property_id AND p.cid = cp.cid AND p.termination_date IS NULL )
							WHERE
								mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::TERMINATED . ',' . CCompanyStatusType::TEMPLATE . ' )
								' . $strCondition . '
								AND cp.renewal_contract_property_id IS NULL
								AND cp.is_last_contract_record = TRUE
								AND ( cp.close_date <= NOW() OR ( c.contract_type_id = ' . CContractType::RENEWAL . ' ) )
								AND ( cp.termination_date IS NULL OR cp.termination_date > NOW() )
								AND c.contract_start_date <= NOW()
								AND cp.ps_product_id IN (' . $strPsProductIds . ')
							GROUP BY
								mc.id,
								cp.ps_product_id,
								mc.company_name
						)as from_contract on ( from_contract.id = from_properties.cid ' . $strSubSqlJoinCondition . ' )
					GROUP BY
						cid,
						total_property_count,
						contract_property_count,
						company_name,
						ps_product_id
					ORDER BY company_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsBySupportEmployeeId( $intSupportEmployeeId, $objDatabase ) {

		if( true == is_null( $intSupportEmployeeId ) || false == is_numeric( $intSupportEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						mc.id AS cid,
						mc.company_name
					FROM clients mc
						JOIN ps_lead_details pld ON ( mc.id = pld.cid )
					WHERE
						mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND pld.support_employee_id = ' . ( int ) $intSupportEmployeeId . '
					ORDER BY mc.company_name';

		$arrmixCompaniesData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixCompaniesData ) ) {
			return NULL;
		}

		$arrstrClients = [];

		foreach( $arrmixCompaniesData as $arrstrCompanyData ) {
			$arrstrClients[$arrstrCompanyData['cid']] = $arrstrCompanyData['company_name'];
		}

		return $arrstrClients;

	}

	public static function fetchCustomActiveClientsPropertyAndUnitCountAndRevenueByEmployeeIds( $arrintEmployeeIds, $arrstrCompanyFilter, $objAdminDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strOrderByField = ' company_name';
		$strOrderByType  = ' ASC';
		$strCondition    = '';

		if( true == valArr( $arrstrCompanyFilter ) ) {
			if( true == isset( $arrstrCompanyFilter['order_by_field'] ) ) {

				switch( $arrstrCompanyFilter['order_by_field'] ) {

					case NULL:
					case 'company_name':
						$strOrderByField = ' company_name ';
						break;

					case 'unit':
						$strOrderByField = ' unit_count ';
						break;

					case 'reveue':
						$strOrderByField = ' monthly_recurring_amount ';
						break;

					case 'property':
						$strOrderByField = ' property_count ';
						break;

					case 'recurring_transactional_revenue':
						$strOrderByField = ' recurring_transactional_revenue ';
						break;

					default:
						$strOrderByField = ' company_name';
						break;
				}
			}

			if( true == isset( $arrstrCompanyFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrCompanyFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}
		}

		if( true == isset( $arrstrCompanyFilter['search_val'] ) && true == valStr( $arrstrCompanyFilter['search_val'] ) && 0 < strlen( trim( $arrstrCompanyFilter['search_val'] ) ) ) {
			$strCondition .= ' AND mc.company_name ILIKE \'%' . trim( addslashes( $arrstrCompanyFilter['search_val'] ) ) . '%\' ';
		}

		$strCondition .= ' AND pld.sales_employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ';

		$strSql = ' SELECT
						id,
						company_name,
						COUNT ( DISTINCT property_id ) AS contract_property_count,
						COALESCE(sum( DISTINCT unit_count),0) AS unit_count,
						sum(monthly_recurring_amount) AS monthly_recurring_amount,
						sum(recurring_transactional_revenue) AS recurring_transactional_revenue,
						state_name,
						property_count,
						number_of_units,
						city
					FROM (
							SELECT
								mc.id ,
								mc.company_name,
								SUM(COALESCE(cp.monthly_recurring_amount, 0)) AS monthly_recurring_amount,
								SUM(COALESCE(cp.transactional_amount, 0)) AS recurring_transactional_revenue,
								cp.property_id AS property_id,
								MAX(p.number_of_units) AS unit_count,
								s.name AS state_name,
								pl.property_count,
								pl.number_of_units,
								ca.city AS city
							FROM
								contracts c
								JOIN contract_properties cp ON ( cp.contract_id = c.id )
								JOIN properties p ON ( cp.property_id = p.id and p.termination_date IS NULL and p.is_disabled = 0 )
								RIGHT JOIN clients mc ON ( mc.id = c.cid )
								JOIN ps_leads pl ON( mc.ps_lead_id = pl.id )
								JOIN ps_lead_details pld ON ( pl.id = pld.ps_lead_id )
								JOIN company_addresses ca ON ( mc.id = ca.cid )
								LEFT JOIN states s ON ( ca.state_code = s.code )
							WHERE
								mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::TERMINATED . ',' . CCompanyStatusType::TEMPLATE . ' ) AND
								(c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . ' OR c.contract_type_id = ' . CContractType::RENEWAL . ' )
								AND cp.termination_date IS NULL' . $strCondition . '
							GROUP BY c.id,
									cp.property_id,
									mc.id,
									s.name,
									pl.property_count,
									pl.number_of_units,
									ca.city,
									mc.company_name
							) as sub_query1
					GROUP BY
							id,
							state_name,
							property_count,
							number_of_units,
							city ,
							company_name
					ORDER BY
							' . $strOrderByField . $strOrderByType;

		$arrstrTempClientsDetails = fetchData( $strSql, $objAdminDatabase );

		if( false == valArr( $arrstrTempClientsDetails ) ) {
			return NULL;
		}

		$arrstrClientsDetails = [];

		foreach( $arrstrTempClientsDetails as $arrstrManagementPsLeadDetails ) {
			$arrstrClientsDetails[$arrstrManagementPsLeadDetails['id']] = $arrstrManagementPsLeadDetails;
		}

		return $arrstrClientsDetails;

	}

	public static function fetchAllKeyClients( $objDatabase ) {

		$strSql = 'SELECT
						mc.id,
						mc.company_name
					FROM
						ps_lead_details pld
						JOIN clients mc ON ( mc.id = pld.cid )
					WHERE
						 COALESCE( pld.key_client_override_rank, pld.key_client_acv_rank ) BETWEEN ' . CPsLeadDetail::HIGH . ' AND ' . CPsLeadDetail::LOW . '
					ORDER BY
						mc.company_name ASC ';

		return self::fetchClients( $strSql, $objDatabase );

	}

	public static function fetchCustomPaginatedClientsTerminatedContractsAndContractPropertiesByEmployeeIds( $arrintEmployeeIds = NULL, $arrstrTaskFilter, $objDatabase, $arrintCids = NULL ) {

		if( false == valArr( $arrstrTaskFilter ) || false == valStr( $arrstrTaskFilter['employee_type'] ) ) {
			return NULL;
		}

		if( 'sales' == $arrstrTaskFilter['employee_type'] ) {
			if( true == is_null( $arrintEmployeeIds ) || false == valArr( $arrintEmployeeIds ) ) {
				return NULL;
			}
		} elseif( 'general' == $arrstrTaskFilter['employee_type'] ) {
			if( false == valArr( $arrintCids ) ) {
				return NULL;
			}
		}

		$intOffset                        = ( true == isset( $arrstrTaskFilter['page_no'] ) && true == isset( $arrstrTaskFilter['page_size'] ) ) ? $arrstrTaskFilter['page_size'] * ( $arrstrTaskFilter['page_no'] - 1 ) : 0;
		$intLimit                         = ( true == isset( $arrstrTaskFilter['page_size'] ) ) ? $arrstrTaskFilter['page_size'] : '';
		$strOrderByField                  = ' ctr.termination_date';
		$strOrderByType                   = ' DESC';
		$strWhereCondition                = '';
		$strTerminationDateCondition      = ' ctr.request_datetime <= NOW() ';
		$strSelectDataCondition           = '';
		$strJoinCondition                 = '';
		$strWhereConditionPropertyProduct = '';
		$strDeniedGroupByCondition        = '';
		$strNullsLastCondition            = '';

		if( true == valArr( $arrstrTaskFilter ) ) {
			if( true == isset( $arrstrTaskFilter['order_by_field'] ) ) {

				switch( $arrstrTaskFilter['order_by_field'] ) {

					case NULL:
					case 'contract_termination_request_id':
						$strOrderByField = ' ctr.id';
						break;

					case 'recuring_revenue':
						$strOrderByField = ' recuring_revenue';
						break;

					case 'transactional_revenue':
						$strOrderByField = ' transactional_amount';
						break;

					case 'termination_date':
						$strOrderByField = ' ctr.termination_date';
						break;

					case 'reason_for_terminating':
						$strOrderByField = ' ctre.name';
						break;

					case 'additional_comments':
						$strOrderByField = ' ctr.termination_reason';
						break;

					case 'sales_approved_on':
						$strOrderByField = ' ctr.sales_approved_on';
						break;

					default:
						$strOrderByField = ' ctr.termination_date';
						break;
				}
			}

			$strDeniedNullsLast = '';

			$strWhereApprovedUnapprovedRequestStatus = 'AND ctr.contract_termination_request_status_id NOT IN ( ' . CContractTerminationRequestStatus::CANCELLED . ',' . CContractTerminationRequestStatus::DENIED . ',' . CContractTerminationRequestStatus::REVERTED . ' ) ';

			$strWhereDeniedTerminationRequestStatus = 'AND ctr.contract_termination_request_status_id IN ( ' . CContractTerminationRequestStatus::DENIED . ', ' . CContractTerminationRequestStatus::CANCELLED . ',' . CContractTerminationRequestStatus::REVERTED . ' ) ';

			$strJoinApproveUnapprovedCondition = 'JOIN contract_termination_requests ctr ON ( ctr.id = cp.contract_termination_request_id ' . $strWhereApprovedUnapprovedRequestStatus . ' AND ' . $strTerminationDateCondition . ' )';

			$strJoinDeniedCondition = 'JOIN contract_termination_histories cths ON ( cths.contract_property_id = cp.id )
										JOIN contract_termination_requests ctr ON ( ctr.id = cths.contract_termination_request_id ' . $strWhereDeniedTerminationRequestStatus . ' AND ' . $strTerminationDateCondition . ' )';

			$strApprovedUnapprovedNullsLast = ', ctr.posted_on NULLS LAST';

			$strDeniedSelectData = ', cths.contract_termination_request_id,
									MAX( cths.denied_on ) AS denied_on';

			$strDeniedGroupBy = ', cths.contract_termination_request_id';

			$strApprovedUnapprovedSelectData = ', MAX( ctr.is_dismissed ) AS is_dismissed';

			if( true == isset( $arrstrTaskFilter['order_by_type'] ) ) {
				$strOrderByType = ( 'asc' == $arrstrTaskFilter['order_by_type'] || '1' == $arrstrTaskFilter['order_by_type'] ) ? ' ASC' : ' DESC';
			}

			if( 'sales' == $arrstrTaskFilter['employee_type'] ) {
				$strWhereCondition = ' pld.sales_employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND';
			} elseif( 'general' == $arrstrTaskFilter['employee_type'] ) {
				$strWhereCondition = ' mc.id IN ( ' . implode( ',', $arrintCids ) . ' ) AND';
			}

			if( !valArr( $arrintCids ) ) {
				$arrintClientCompanyStatusTypeIds = CCompanyStatusType::$c_arrintStdTerminationDashboardExcludeCompanyStatusTypeIds;
			} else {
				$arrintClientCompanyStatusTypeIds = CCompanyStatusType::$c_arrintClientTerminationDashboardExcludeCompanyStatusTypeIds;
				$arrstrTaskFilter['is_for_admin'] = 1;
			}

			if( ( 'sales' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) )
			    || ( 'general' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) && false == $arrstrTaskFilter['is_for_admin'] ) ) {
				if( 'approved' == $arrstrTaskFilter['status'] ) {
					$strWhereCondition .= ' ctr.sales_approved_on IS NOT NULL ';
				} elseif( 'unapproved' == $arrstrTaskFilter['status'] ) {
					$strWhereCondition .= ' ctr.sales_approved_on IS NULL ';
				} elseif( 'denied' == $arrstrTaskFilter['status'] ) {
					$strWhereCondition .= ' cths.denied_on IS NOT NULL ';
				} elseif( 'new' == $arrstrTaskFilter['status'] ) {
					$strWhereCondition .= ' ctr.is_dismissed = 0 AND ctr.posted_on IS NULL ';
				} elseif( 'dismissed' == $arrstrTaskFilter['status'] ) {
					$strWhereCondition .= ' ctr.is_dismissed = 1 AND ctr.posted_on IS NULL ';
				} else {
					$strWhereCondition .= ' ctr.sales_approved_on IS NULL ';
				}
			}

			if( ( 'approves_terminations' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) )
			    || ( 'general' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) && true == $arrstrTaskFilter['is_for_admin'] ) ) {
				if( 'approved' == $arrstrTaskFilter['status'] ) {
					$strWhereCondition .= ' ctr.posted_on IS NOT NULL ';
				} elseif( 'unapproved' == $arrstrTaskFilter['status'] ) {
					$strWhereCondition .= ' ctr.posted_on IS NULL ';
				} elseif( 'denied' == $arrstrTaskFilter['status'] ) {
					$strWhereCondition .= ' cths.denied_on IS NOT NULL ';
				} elseif( 'new' == $arrstrTaskFilter['status'] ) {
					$strWhereCondition .= ' ctr.is_dismissed = 0 AND ctr.posted_on IS NULL ';
				} elseif( 'dismissed' == $arrstrTaskFilter['status'] ) {
					$strWhereCondition .= ' ctr.is_dismissed = 1 AND ctr.posted_on IS NULL ';
				} else {
					$strWhereCondition .= ' ctr.posted_on IS NULL ';
				}
			}

			if( ( 'sales' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) )
			    || ( 'general' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) && false == $arrstrTaskFilter['is_for_admin'] )
			    || ( 'approves_terminations' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) )
			    || ( 'general' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) && true == $arrstrTaskFilter['is_for_admin'] ) ) {

				if( 'approved' == $arrstrTaskFilter['status'] || 'unapproved' == $arrstrTaskFilter['status'] ) {
					$strJoinCondition       = $strJoinApproveUnapprovedCondition;
					$strNullsLastCondition  = $strApprovedUnapprovedNullsLast;
					$strSelectDataCondition = $strApprovedUnapprovedSelectData;
				} elseif( 'denied' == $arrstrTaskFilter['status'] ) {
					$strJoinCondition          = $strJoinDeniedCondition;
					$strNullsLastCondition     = $strDeniedNullsLast;
					$strDeniedGroupByCondition = $strDeniedGroupBy;
					$strSelectDataCondition    = $strDeniedSelectData;
				} else {
					$strJoinCondition       = $strJoinApproveUnapprovedCondition;
					$strNullsLastCondition  = $strApprovedUnapprovedNullsLast;
					$strSelectDataCondition = $strApprovedUnapprovedSelectData;
				}
			}

			if( true == valArr( $arrstrTaskFilter['property'] ) ) {
				$arrintPropertyIdLists            = implode( ',', $arrstrTaskFilter['property'] );
				$strWhereConditionPropertyProduct .= ' AND cp.property_id IN ( ' . $arrintPropertyIdLists . ' ) ';
			}

			if( true == valArr( $arrstrTaskFilter['product'] ) ) {
				$arrintProductIdLists             = implode( ',', $arrstrTaskFilter['product'] );
				$strWhereConditionPropertyProduct .= ' AND cp.ps_product_id IN ( ' . $arrintProductIdLists . ' ) ';
			}

		}

		$strSql = 'SELECT
						MAX( mc.id ) AS cid,
						MAX( mc.company_name ) AS company_name,
						SUM( cp.monthly_recurring_amount ) AS recuring_revenue,
						SUM( cp.transactional_amount ) AS transactional_amount,
						MAX ( e.name_full ) AS rvp_name,
						ctr.sales_approved_on,
						ctr.posted_on,
						ctr.termination_reason AS contract_termination_reason,
						ctr.termination_note AS contract_termination_note,
						ctr.deactivation_date,
						ctr.termination_date,
						ctr.id AS contract_termination_request_id,
						ctr.contract_termination_request_status_id,
						ctre.name AS contract_termination_reason_name,
						ctrs.name as contract_termination_request_status
						' . $strSelectDataCondition . '
					FROM
						contracts c
						JOIN clients mc ON (mc.id = c.cid AND mc.company_status_type_id NOT IN ( ' . sqlIntImplode( $arrintClientCompanyStatusTypeIds ) . ' ) AND c.contract_status_type_id IN ( ' . CContractStatusType::CONTRACT_APPROVED . ',' . CContractStatusType::CONTRACT_TERMINATED . ' ) )
						JOIN ps_lead_details pld ON ( pld.cid = mc.id)
						JOIN contract_properties cp ON ( cp.contract_id = c.id )
						' . $strJoinCondition . '
						JOIN properties p ON ( p.id = cp.property_id )
						JOIN contract_termination_request_statuses ctrs ON ctrs.id = ctr.contract_termination_request_status_id
						JOIN contract_termination_reasons ctre ON ctre.id = ctr.contract_termination_reason_id
						JOIN employees e ON ( e.id = pld.sales_employee_id )
					WHERE
						' . $strWhereCondition . $strWhereConditionPropertyProduct . '
					GROUP BY
						ctr.id,
						ctr.sales_approved_on,
						ctr.posted_on,
						ctr.termination_reason,
						ctr.termination_date,
						ctr.request_datetime,
						ctrs.name,
						ctre.name
						' . $strDeniedGroupByCondition . '
					ORDER BY ' . $strOrderByField . $strOrderByType . $strNullsLastCondition . '
					OFFSET ' . ( int ) $intOffset;

		if( false == is_null( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPaginatedClientsTerminatedContractsAndContractPropertiesCountByEmployeeIds( $arrintEmployeeIds = NULL, $arrstrTaskFilter, $objDatabase, $arrintCids = NULL ) {
		$strWhereCondition = '';

		$strTerminationDateCondition = ' ctr.request_datetime <= NOW() ';

		if( false == valArr( $arrstrTaskFilter ) || false == isset( $arrstrTaskFilter['employee_type'] ) || false == valStr( $arrstrTaskFilter['employee_type'] ) ) {
			return NULL;
		}

		$strWhereConditionPropertyProduct        = '';
		$strWhereApprovedUnapprovedRequestStatus = 'AND ctr.contract_termination_request_status_id NOT IN ( ' . CContractTerminationRequestStatus::CANCELLED . ',' . CContractTerminationRequestStatus::DENIED . ',' . CContractTerminationRequestStatus::REVERTED . ' ) ';
		$strWhereDeniedTerminationRequestStatus  = 'AND ctr.contract_termination_request_status_id IN ( ' . CContractTerminationRequestStatus::DENIED . ', ' . CContractTerminationRequestStatus::CANCELLED . ',' . CContractTerminationRequestStatus::REVERTED . ' ) ';

		$strJoinApproveUnapprovedCondition = 'JOIN contract_termination_requests ctr ON ( ctr.id = cp.contract_termination_request_id ' . $strWhereApprovedUnapprovedRequestStatus . ' AND ' . $strTerminationDateCondition . ' )';
		$strJoinDeniedCondition            = 'JOIN contract_termination_histories cths ON ( cths.contract_property_id = cp.id )
										JOIN contract_termination_requests ctr ON ( ctr.id = cths.contract_termination_request_id ' . $strWhereDeniedTerminationRequestStatus . ' AND ' . $strTerminationDateCondition . ' )';

		$strJoinCondition = '';

		if( true == isset( $arrstrTaskFilter['order_by_type'] ) ) {
			$strOrderByType = ( 'asc' == $arrstrTaskFilter['order_by_type'] || '1' == $arrstrTaskFilter['order_by_type'] ) ? ' ASC' : ' DESC';
		}

		if( 'sales' == $arrstrTaskFilter['employee_type'] ) {
			$strWhereCondition = ' pld.sales_employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND';
		} elseif( 'general' == $arrstrTaskFilter['employee_type'] ) {
			$strWhereCondition = ' mc.id IN ( ' . implode( ',', $arrintCids ) . ' ) AND';
		}
		if( !valArr( $arrintCids ) ) {
			$arrintClientCompanyStatusTypeIds = CCompanyStatusType::$c_arrintStdTerminationDashboardExcludeCompanyStatusTypeIds;
		} else {
			$arrintClientCompanyStatusTypeIds = CCompanyStatusType::$c_arrintClientTerminationDashboardExcludeCompanyStatusTypeIds;
			$arrstrTaskFilter['is_for_admin'] = 1;
		}
		if( ( 'sales' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) )
		    || ( 'general' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) && false == $arrstrTaskFilter['is_for_admin'] ) ) {
			if( 'approved' == $arrstrTaskFilter['status'] ) {
				$strWhereCondition .= ' ctr.sales_approved_on IS NOT NULL ';
			} elseif( 'unapproved' == $arrstrTaskFilter['status'] ) {
				$strWhereCondition .= ' ctr.sales_approved_on IS NULL ';
			} elseif( 'denied' == $arrstrTaskFilter['status'] ) {
				$strWhereCondition .= ' cths.denied_on IS NOT NULL ';
			} elseif( 'new' == $arrstrTaskFilter['status'] ) {
				$strWhereCondition .= ' ctr.is_dismissed = 0 AND ctr.posted_on IS NULL ';
			} elseif( 'dismissed' == $arrstrTaskFilter['status'] ) {
				$strWhereCondition .= ' ctr.is_dismissed = 1 AND ctr.posted_on IS NULL ';
			} else {
				$strWhereCondition .= ' ctr.sales_approved_on IS NULL ';
			}
		}

		if( ( 'approves_terminations' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) )
		    || ( 'general' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) && true == $arrstrTaskFilter['is_for_admin'] ) ) {

			if( 'approved' == $arrstrTaskFilter['status'] ) {
				$strWhereCondition .= ' ctr.posted_on IS NOT NULL ';
			} elseif( 'unapproved' == $arrstrTaskFilter['status'] ) {
				$strWhereCondition .= ' ctr.posted_on IS NULL ';
			} elseif( 'denied' == $arrstrTaskFilter['status'] ) {
				$strWhereCondition .= ' cths.denied_on IS NOT NULL ';
			} elseif( 'new' == $arrstrTaskFilter['status'] ) {
				$strWhereCondition .= ' ctr.is_dismissed = 0 AND ctr.posted_on IS NULL ';
			} elseif( 'dismissed' == $arrstrTaskFilter['status'] ) {
				$strWhereCondition .= ' ctr.is_dismissed = 1 AND ctr.posted_on IS NULL ';
			} else {
				$strWhereCondition .= ' ctr.posted_on IS NULL ';
			}
		}

		if( ( 'sales' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) )
		    || ( 'general' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) && false == $arrstrTaskFilter['is_for_admin'] )
		    || ( 'approves_terminations' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) )
		    || ( 'general' == $arrstrTaskFilter['employee_type'] && true == isset( $arrstrTaskFilter['status'] ) && true == $arrstrTaskFilter['is_for_admin'] ) ) {

			if( 'approved' == $arrstrTaskFilter['status'] || 'unapproved' == $arrstrTaskFilter['status'] ) {
				$strJoinCondition = $strJoinApproveUnapprovedCondition;
			} elseif( 'denied' == $arrstrTaskFilter['status'] ) {
				$strJoinCondition = $strJoinDeniedCondition;
			} else {
				$strJoinCondition = $strJoinApproveUnapprovedCondition;
			}
		}

		if( true == valArr( $arrstrTaskFilter['property'] ) ) {
			$arrintPropertyIdLists            = implode( ',', $arrstrTaskFilter['property'] );
			$strWhereConditionPropertyProduct .= ' AND cp.property_id IN ( ' . $arrintPropertyIdLists . ' ) ';
		}

		if( true == valArr( $arrstrTaskFilter['product'] ) ) {
			$arrintProductIdLists             = implode( ',', $arrstrTaskFilter['product'] );
			$strWhereConditionPropertyProduct .= ' AND cp.ps_product_id IN ( ' . $arrintProductIdLists . ' ) ';
		}

		$strSql = 'SELECT
						count( contract_termination_request_id )
					FROM (
							SELECT
								ctr.id AS contract_termination_request_id
							FROM
								contracts c
								JOIN clients mc ON (mc.id = c.cid AND mc.company_status_type_id NOT IN ( ' . sqlIntImplode( $arrintClientCompanyStatusTypeIds ) . ' ) AND c.contract_status_type_id IN ( ' . CContractStatusType::CONTRACT_APPROVED . ',' . CContractStatusType::CONTRACT_TERMINATED . ' ))
								JOIN ps_lead_details pld ON ( pld.cid = mc.id)
								JOIN contract_properties cp ON ( cp.contract_id = c.id )
								' . $strJoinCondition . '
								JOIN properties p ON ( p.id = cp.property_id )
							WHERE
								' . $strWhereCondition . $strWhereConditionPropertyProduct . '
							GROUP BY
								ctr.id
						) as subSql';

		$arrintClientsCount = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintClientsCount[0]['count'] ) ) {
			return $arrintClientsCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchSearchClients( $arrmixFilteredExplodedSearch, $objDatabase, $boolIsGlobalSearch = false ) {

		$strCidCondition = ( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) ? ' to_char( mc.id, \'99999999\' ) LIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR ' : '';

		$strSql = '
			SELECT
				mc.id,
				mc.company_name,
				mc.database_id,
				cs.name,
				pld.primary_website_urls AS website_url,
				CASE WHEN COALESCE( pld.key_client_override_rank, pld.key_client_acv_rank ) BETWEEN ' . CPsLeadDetail::HIGH . ' AND ' . CPsLeadDetail::LOW . ' THEN 1 ELSE 0 END AS is_key_client,
				pld.ps_lead_id AS ps_lead_id,
				CASE
					WHEN mc.company_name ILIKE E\'' . implode( '%\' AND mc.company_name ILIKE E\'%', $arrmixFilteredExplodedSearch ) . '%\' THEN 1
					WHEN mc.rwx_domain ILIKE E\'' . implode( '%\' AND mc.rwx_domain ILIKE E\'%', $arrmixFilteredExplodedSearch ) . '%\' THEN 1
				END AS order_first,
				CASE
					WHEN cs.id = ' . CCompanyStatusType::CLIENT . ' THEN 1
					WHEN cs.id = ' . CCompanyStatusType::SALES_DEMO . ' THEN 2
					WHEN cs.id = ' . CCompanyStatusType::TEST_DATABASE . ' THEN 3
					WHEN cs.id = ' . CCompanyStatusType::TERMINATED . ' THEN 4
				END AS order_second
			FROM
				clients mc
				JOIN ps_lead_details pld ON ( pld.cid = mc.id )
				LEFT JOIN company_status_types cs ON ( mc.company_status_type_id = cs.id )
				LEFT JOIN company_addresses ca ON ( mc.id = ca.cid )
				LEFT JOIN ps_leads pl ON ( mc.ps_lead_id = pl.id AND pl.deleted_by IS NULL )
			WHERE
				(
					' . $strCidCondition . ' mc.company_name ILIKE \'%' . implode( '%\' AND mc.company_name ILIKE \'%', $arrmixFilteredExplodedSearch ) . '%\'
					OR mc.rwx_domain ILIKE \'%' . implode( '%\' AND mc.rwx_domain ILIKE \'%', $arrmixFilteredExplodedSearch ) . '%\'
					OR ca.city ILIKE \'%' . implode( '%\' AND ca.city ILIKE \'%', $arrmixFilteredExplodedSearch ) . '%\'
					OR ca.state_code ILIKE \'%' . implode( '%\' AND ca.state_code ILIKE \'%', $arrmixFilteredExplodedSearch ) . '%\'
					OR ca.postal_code ILIKE \'%' . implode( '%\' AND ca.postal_code ILIKE \'%', $arrmixFilteredExplodedSearch ) . '%\'
					OR pld.primary_website_urls ILIKE \'%' . implode( '%\' AND pld.primary_website_urls ILIKE \'%', $arrmixFilteredExplodedSearch ) . '%\'
				)
				AND mc.company_name NOT LIKE \'%_delete%\' ';
		if( true == $boolIsGlobalSearch ) {
			$strSql .= 'AND mc.company_status_type_id <> ' . CCompanyStatusType::CANCELLED;
		}
		$strSql .= '
			ORDER BY
				order_first, order_second, mc.id
			LIMIT
				10';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchDatabaseDetailsForOutageSystem( $objConnectDatabase ) {
		$strSql          = "SELECT
						id AS database_id,
						database_name,
						ip_address,
						'NA' as short_description,
						'NA' as full_description
					FROM
						databases
					ORDER BY
						id ASC";
		$arrmixDatabases = fetchData( $strSql, $objConnectDatabase );

		return $arrmixDatabases;
	}

	public static function fetchPersonDetailsForOutageSystem( $objAdminDatabase ) {

		$strSql = "(SELECT
						p.id,
						array_to_string ( ARRAY [ p.name_first, p.name_last ], ' ' ) AS NAME,
						0 AS person_association_type_id,
						NULL AS person_association_type,
						pcf.person_contact_type_id AS person_type_id,
						pct.name AS person_type,
						p.email_address,
						0 AS designation_id,
						NULL::text AS designation,
						0 AS department_id,
						NULL::text AS department_name,
						mc.id AS cid,
						mc.company_name,
						pl.company_status_type_id AS is_client,
						p.country_code,
						CASE
							WHEN pcf1.person_contact_type_id = 12 THEN 1
							WHEN pcf1.person_contact_type_id = 13 THEN 2
						END AS outage_notification_status,
						mc.database_id,
						NULL AS phone_number2
					FROM
						persons p
						JOIN person_contact_preferences pcf1 ON ( p.id = pcf1.person_id AND pcf1.person_contact_type_id IN ( 12, 13 ) )
						JOIN person_contact_preferences pcf ON ( p.id = pcf.person_id AND pcf.person_contact_type_id NOT IN ( 12, 13 ) )
						JOIN person_contact_types pct ON ( pcf.person_contact_type_id = pct.id )
						JOIN ps_leads pl ON ( pl.id = p.ps_lead_id )
						JOIN clients mc ON ( mc.ps_lead_id = pl.id )
					WHERE
						p.id IN (
									SELECT
										p.id
									FROM
										persons p
										JOIN person_contact_preferences pcf ON ( p.id = pcf.person_id )
										JOIN person_contact_types pct ON ( pcf.person_contact_type_id = pct.id AND pcf.person_contact_type_id IN ( 12, 13 ) )
									WHERE
										p.deleted_by IS NULL
										AND p.is_disabled = 0
						)
						AND pl.company_status_type_id = 4
					UNION
						(
							SELECT
								p.id,
								array_to_string ( ARRAY [ p.name_first, p.name_last ], ' ' ) AS NAME,
								0 AS person_association_type_id,
								NULL AS person_association_type,
								1 AS person_type_id,
								'Primary' AS person_type,
								p.email_address,
								0 AS designation_id,
								NULL::text AS designation,
								0 AS department_id,
								NULL::text AS department_name,
								mc.id AS cid,
								mc.company_name,
								pl.company_status_type_id AS is_client,
								p.country_code,
								CASE
									WHEN pcf.person_contact_type_id = 12 THEN 1
									WHEN pcf.person_contact_type_id = 13 THEN 2
								END AS outage_notification_status,
								mc.database_id,
								NULL AS phone_number2
							FROM
								persons p
								JOIN person_contact_preferences pcf ON ( p.id = pcf.person_id )
								JOIN person_contact_types pct ON ( pcf.person_contact_type_id = pct.id )
								JOIN ps_leads pl ON ( pl.id = p.ps_lead_id )
								JOIN clients mc ON ( mc.ps_lead_id = pl.id )
							WHERE
								p.deleted_by IS NULL
								AND pcf.person_contact_type_id IN ( 12, 13 )
								AND p.is_primary = 1
								AND pl.company_status_type_id = 4
								AND p.is_disabled = 0
						))
					UNION
					(SELECT
						e.id,
						preferred_name AS NAME,
						0 AS person_association_type_id,
						NULL AS person_association_type,
						0 AS person_type_id,
						NULL AS person_type,
						e.email_address,
						e.designation_id,
						dn.name AS designation,
						d.id AS department_id,
						d.name AS department_name,
						0 AS cid,
						NULL AS company_name,
						0 AS is_client,
						ea.country_code,
						CASE
							WHEN 51 = ur.role_id AND 52 = ur.role_id THEN 3
							WHEN 51 = ur.role_id THEN 1
							WHEN 52 = ur.role_id THEN 2
						ELSE 0
							END AS outage_notification_status,
						0 AS database_id,
						epn.phone_number AS phone_number2
					FROM
						employees e
						JOIN departments d ON e.department_id = d.id
						JOIN employee_phone_numbers epn ON epn.employee_id = e.id
						JOIN employee_addresses ea ON ea.employee_id = e.id
						JOIN designations dn ON e.designation_id = dn.id
						JOIN users u ON u.employee_id = e.id
						JOIN user_roles ur ON u.id = ur.user_id
					WHERE
						epn.phone_number IS NOT NULL
						AND e.employee_status_type_id = " . CEmployeeStatusType::CURRENT . '
						AND ea.address_type_id = ' . CAddressType::PRIMARY . '
						AND epn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . '
						AND ur.role_id IN ( 51, 52 )
						AND ur.deleted_on IS NULL)';

		$arrmixPersons = fetchData( $strSql, $objAdminDatabase );

		return $arrmixPersons;
	}

	public static function fetchClientByIdByKeys( $intCid, $arrstrKeys, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						count( DISTINCT( mc.id ) ),
						mc.id,
						pp.key
					FROM
						clients mc
						JOIN properties p ON ( p.cid = mc.id )
						JOIN property_preferences pp ON ( pp.cid = mc.id AND pp.property_id = p.id )
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND pp.key IN (\'' . implode( "','", $arrstrKeys ) . '\')
						AND pp.value IS NOT NULL
						AND p.is_disabled = 0
						GROUP BY mc.id, pp.key';

		$arrstrResponseData                   = fetchData( $strSql, $objDatabase );
		$arrstrClientPropertyPreferencesCount = [];

		if( true == valArr( $arrstrResponseData ) ) {
			foreach( $arrstrResponseData as $arrstrResponse ) {
				$arrstrClientPropertyPreferencesCount[$arrstrResponse['key']] = $arrstrResponse['count'];
			}
		}

		return $arrstrClientPropertyPreferencesCount;
	}

	public static function fetchCustomActiveClientsUnitCountByEmployeeIdByPsProductIds( $intEmployeeId, $arrintPsProductIds, $objDatabase, $arrstrCompanyFilter = NULL ) {

		$strPsProductIds = implode( ',', $arrintPsProductIds );

		$strCondition = ' AND pld.sales_employee_id = ' . ( int ) $intEmployeeId;

		if( true == isset( $arrstrCompanyFilter['search_val'] ) && true == valStr( $arrstrCompanyFilter['search_val'] ) && 0 < strlen( $arrstrCompanyFilter['search_val'] ) ) {
			$strCondition .= ' AND mc.company_name ILIKE \'%' . trim( addslashes( $arrstrCompanyFilter['search_val'] ) ) . '%\' ';
		}

		$strSql = 'SELECT
						ps_product_id,
						company_name,
						cid,
						total_unit_count

					FROM (
							SELECT
								p.cid
							FROM
								properties p
							WHERE
								p.termination_date IS NULL
								AND p.is_disabled = 0
							GROUP BY
								p.cid
						) as from_properties join (
							SELECT
								mc.id ,
								mc.company_name as company_name,
								sum(p.number_of_units) as total_unit_count,
								cp.ps_product_id as ps_product_id
								FROM clients mc
								JOIN ps_lead_details pld ON ( pld.cid = mc.id )
								JOIN contracts c ON (mc.id = c.cid)
								JOIN contract_properties cp ON (cp.contract_id = c.id AND cp.cid = c.cid)
								JOIN properties p ON (p.id = cp.property_id AND p.cid = cp.cid AND p.termination_date IS NULL AND p.is_disabled = 0 )
							WHERE
								mc.company_status_type_id NOT IN ( ' . CCompanyStatusType::SALES_DEMO . ',' . CCompanyStatusType::TEST_DATABASE . ',' . CCompanyStatusType::TERMINATED . ',' . CCompanyStatusType::TEMPLATE . ' )
								AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
								AND c.contract_type_id <> ' . CContractType::RENEWAL . $strCondition . '
								AND cp.termination_date IS NULL
								AND cp.ps_product_id IN (' . $strPsProductIds . ')
							GROUP BY
								mc.id,
								cp.ps_product_id,
								mc.company_name
						)as from_contract on ( from_contract.id = from_properties.cid )
					GROUP BY
						cid,
						total_unit_count,
						company_name,
						ps_product_id
					ORDER BY company_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientByPsLeadId( $intPsLeadId, $objDatabase ) {
		return parent::fetchClient( sprintf( 'SELECT * FROM clients WHERE ps_lead_id = %d ', ( int ) $intPsLeadId ), $objDatabase );
	}

	public static function fetchCustomClientsWithSystemCompanyUser( $objDatabase ) {

		$strSql = 'SELECT
						mc.id
					FROM
						clients mc
					WHERE
						mc.id != 1
						AND EXISTS ( SELECT
											1
										FROM
											public.company_users cu
										WHERE
											cu.cid = mc.id
											AND cu.id = 1
									)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleClientsByIds( $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						clients
					WHERE
						id IN ( ' . implode( ',', $arrintCids ) . ' )
					ORDER BY
						lower( company_name ) ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchSimpleClientsByPsProductIdsByDatabaseId( $arrintPsProductIds, $intDatabaseId, $objDatabase, $boolIsClientCompanies ) {

		if( false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strClientCompaniesCondition = ( true == $boolIsClientCompanies ) ? 'AND mc.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT : '';

		$strSql = 'SELECT sub_query.*,
						CASE
							WHEN product_id = ' . CPsProduct::RESIDENT_INSURE . ' THEN ' . CPsProduct::RESIDENT_INSURE . '
							ELSE ' . CPsProduct::RESIDENT_PORTAL . '
						END AS product_id
					FROM
						( SELECT
								DISTINCT ON (mc.id) mc.*,
								MAX(cp.ps_product_id) over( partition BY mc.id ) as product_id
							FROM
								clients mc
								JOIN contract_properties cp ON mc.id = cp.cid
								JOIN contracts c ON ( c.id = cp.contract_id AND c.cid = mc.id )
							WHERE
								c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
								' . $strClientCompaniesCondition . '
								AND mc.database_id = ' . ( int ) $intDatabaseId . '
								AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) ' . '
						) sub_query';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsDetailsByIdsByCompanyStatusTypeIds( $arrintCids, $arrintCompanyStatusTypeIds, $objDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSelectClause = '';
		$strJoinClause   = '';

		if( CDatabaseType::ADMIN == $objDatabase->getDatabaseTypeId() ) {
			$strSelectClause = ', pl.id AS ps_lead_id ';
			$strJoinClause   = ' JOIN ps_leads pl ON( mc.ps_lead_id = pl.id AND pl.deleted_by IS NULL ) ';
		}

		$strSql = 'SELECT
						mc.* ' . $strSelectClause . '
					FROM
						clients mc ' . $strJoinClause . '
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
						AND mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
					ORDER BY
						lower( mc.company_name ) ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsWithInvoiceCountByInvoiceBatchIdByInvoiceMethodId( $intInvoiceBatchId, $intInvoiceMethodId, $objAdminDatabase ) {
		$strSql = 'SELECT
						mc.id, mc.ps_lead_id, mc.company_name, count(i.id) AS number_of_invoices
					FROM clients mc
						JOIN invoices i ON i.cid = mc.id
						JOIN accounts a ON a.id = i.account_id
					WHERE a.invoice_method_id = ' . ( int ) $intInvoiceMethodId . '
						AND i.invoice_batch_id = ' . ( int ) $intInvoiceBatchId . '
						AND i.invoice_status_type_id <> ' . CInvoiceStatusType::SALES_TAX_PENDING . '
						GROUP BY mc.id, mc.ps_lead_id, mc.company_name';

		return self::fetchClients( $strSql, $objAdminDatabase );
	}

	public static function fetchClientNameById( $intCid, $objDatabase ) {
		return self::fetchClient( 'SELECT company_name, rwx_domain FROM clients WHERE id = ' . ( int ) $intCid, $objDatabase );
	}

	public static function fetchCustomClientsByIntegrationClientTypeIds( $arrintIntegrationClientTypeIds, $objDatabase ) {

		$strSql = 'SELECT mc.id FROM clients AS mc, integration_clients AS ic WHERE mc.id = ic.cid AND ic.integration_client_type_id IN (' . implode( ',', $arrintIntegrationClientTypeIds ) . ') GROUP BY mc.id';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsByName( $strClientName, $objDatabase ) {

		$strSql = 'SELECT * FROM clients WHERE company_name ILIKE \'%' . $strClientName . '%\'';

		return self::fetchClients( $strSql, $objDatabase );

	}

	public static function fetchClientsByPsProductIdByDatabaseId( $intPsProductId, $intDatabaseId, $objDatabase, $boolLiveCompaniesOnly = false ) {
		$strSql = 'SELECT
						*
					FROM
						( SELECT
								DISTINCT ON (mc.id) mc.*
							FROM
								clients mc,
								contracts c,
								contract_properties cp
							WHERE
								mc.id = c.cid
								AND c.id = cp.contract_id ';

		if( true == $boolLiveCompaniesOnly ) {
			$strSql .= 'AND mc.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT;
		}

		$strSql .= 'AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
					AND mc.database_id = ' . ( int ) $intDatabaseId . '
					AND cp.ps_product_id = ' . ( int ) $intPsProductId . ' ) AS sub_query1
					ORDER BY lower( company_name ) ASC ';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsByPsProductIdByDatabaseIds( $intPsProductId, $arrintDatabaseIds, $objDatabase, $boolLiveCompaniesOnly = false ) {
		$strSql = 'SELECT
						*
					FROM
						( SELECT
								DISTINCT ON (mc.id) mc.*
							FROM
								clients mc,
								contracts c,
								contract_properties cp
							WHERE
								mc.id = c.cid
								AND c.id = cp.contract_id ';

		if( true == $boolLiveCompaniesOnly ) {
			$strSql .= 'AND mc.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT;
		}

		$strSql .= 'AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
					AND mc.database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )
					AND cp.ps_product_id = ' . ( int ) $intPsProductId . ' 
					AND cp.deactivation_date IS NULL
					AND cp.termination_date IS NULL
					AND cp.renewal_contract_property_id IS NULL
					AND cp.is_last_contract_record = TRUE
					AND cp.contract_termination_request_id IS NULL
					AND cp.close_date IS NOT NULL ) AS sub_query1
					ORDER BY lower( company_name ) ASC ';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchTestClientsByPsProductIdByDatabaseId( $intPsProductId, $intDatabaseId, $arrintCompanyStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT (mc.id), mc.company_name
					FROM
						clients mc
					JOIN
						contract_properties cp on (mc.id = cp.cid)
					WHERE
						mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						AND mc.database_id = ' . ( int ) $intDatabaseId . '
						AND cp.ps_product_id = ' . ( int ) $intPsProductId . '
					ORDER BY mc.company_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsByDatabaseIds( $arrintDatabaseIds, $objDatabase ) {

		if( false == valArr( $arrintDatabaseIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						clients
					WHERE
						database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' ) ';

		return self::fetchClients( $strSql, $objDatabase );

	}

	public static function fetchClientIdsByDatabaseIds( $arrintDatabaseIds, $objDatabase ) {
		if( false == valArr( $arrintDatabaseIds ) ) {
			return NULL;
		}

		$strSql          = 'SELECT id FROM clients WHERE database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' );';
		$arrstrClients   = ( array ) fetchData( $strSql, $objDatabase );
		$arrintClientIds = [];

		foreach( $arrstrClients as $arrstrClient ) {
			$arrintClientIds[] = $arrstrClient['id'];
		}

		return $arrintClientIds;
	}

	public static function fetchClientIdsByCompanyStatusTypeIds( $arrintCompanyStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql          = 'SELECT id FROM clients WHERE company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' );';
		$arrstrClients   = ( array ) fetchData( $strSql, $objDatabase );
		$arrintClientIds = [];

		foreach( $arrstrClients as $arrstrClient ) {
			$arrintClientIds[] = $arrstrClient['id'];
		}

		return $arrintClientIds;
	}

	public static function fetchClientByIdForAwaitingResponseNotifications( $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						company_name,
						rwx_domain,
						ps_lead_id
					FROM
						clients
					WHERE
						id = ' . ( int ) $intCid;

		return parent::fetchClient( $strSql, $objDatabase );
	}

	public static function fetchContractBasedClientsByPsProductIdsByDatabaseId( $arrintPsProductIds, $intDatabaseId, $objDatabase, $boolLiveCompaniesOnly = false, $intCid = NULL ) {

		$strCondition = '';

		if( true == $boolLiveCompaniesOnly ) {
			$strCondition = ' AND mc.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT;
		}

		$strSql = ' ( SELECT
							DISTINCT ON ( mc.id, lower( company_name ) ) mc.*
						FROM
							clients mc
							JOIN contracts c ON( mc.id = c.cid )
							JOIN contract_properties cp ON( c.id = cp.contract_id AND c.cid = cp.cid )
						WHERE
							mc.database_id = ' . ( int ) $intDatabaseId . $strCondition . '
							AND c.contract_status_type_id = ' . ( int ) CContractStatusType::CONTRACT_APPROVED;

		if( false == is_null( $intCid ) && true == valId( $intCid ) ) {
			$strSql .= ' AND mc.id = ' . ( int ) $intCid;
		}

		$strSql .= ' AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						ORDER BY
							lower( company_name )
					)
					UNION
					( SELECT
							DISTINCT ON ( mc.id, lower( company_name ) ) mc.*
						FROM
							clients mc
							JOIN simple_contracts sc ON( mc.id = sc.cid )
						WHERE
							mc.database_id = ' . ( int ) $intDatabaseId . $strCondition;

		if( false == is_null( $intCid ) && true == valId( $intCid ) ) {
			$strSql .= ' AND mc.id = ' . ( int ) $intCid;
		}

		$strSql .= ' AND sc.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
							AND ( sc.expire_on > NOW() OR sc.expire_on IS NULL )
						ORDER BY
							lower( company_name )
					 )';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchContractBasedClientsByPsProductIdsByCompanyStatusTypeIdsByDatabaseIds( $arrintPsProductIds, $arrintCompanyStatusTypeIds, $arrintDatabaseIds, $objDatabase ) {

		$strCondition = '';

		if( valArr( $arrintCompanyStatusTypeIds ) ) {
			$strCondition = ' AND mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' ) ';
		}

		$strSql = ' ( SELECT
							DISTINCT ON ( mc.id, lower( company_name ) ) mc.*
						FROM
							clients mc
							JOIN contracts c ON( mc.id = c.cid )
							JOIN contract_properties cp ON( c.id = cp.contract_id AND c.cid = cp.cid )
						WHERE
							mc.database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )' . $strCondition . '
							AND c.contract_status_type_id = ' . ( int ) CContractStatusType::CONTRACT_APPROVED;

		$strSql .= ' AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						ORDER BY
							lower( company_name )
					)
					UNION
					( SELECT
							DISTINCT ON ( mc.id, lower( company_name ) ) mc.*
						FROM
							clients mc
							JOIN simple_contracts sc ON( mc.id = sc.cid )
						WHERE
							mc.database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )' . $strCondition;

		$strSql .= ' AND sc.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
							AND ( sc.expire_on > NOW() OR sc.expire_on IS NULL )
						ORDER BY
							lower( company_name )
					 )';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchCustomClientsByIds( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id, company_name
					FROM
						clients
					WHERE
						id IN ( ' . implode( ',', $arrintCids ) . ' )
					ORDER BY
						lower( company_name ) ASC';

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * @param $arrintCids
	 * @param $objDatabase
	 * @return array|null
	 */
	public static function fetchTestClientsByIds( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id
					FROM
						clients
					WHERE
						id IN ( ' . implode( ',', $arrintCids ) . ' )
						AND company_status_type_id =' . CCompanyStatusType::TEST_DATABASE;

		$arrintClients = fetchData( $strSql, $objDatabase );

		$arrintReorderedClients = [];

		if( true == valArr( $arrintClients ) ) {
			foreach( $arrintClients as $arrintClient ) {
				$arrintReorderedClients[] = $arrintClient['id'];
			}
		}

		return $arrintReorderedClients;
	}

	public static function fetchSimpleClientsByIdsByFieldNames( $arrstrFieldNames, $intCompanyStatusTypeId, $objDatabase ) {

		if( false == valArr( $arrstrFieldNames ) ) {
			return NULL;
		}

		$strSql = 'SELECT ' . implode( ',', $arrstrFieldNames ) . '
					FROM
						clients
					WHERE
						company_status_type_id = ' . ( int ) $intCompanyStatusTypeId . '
						--AND company_name ILIKE E\'%lake%\'
					ORDER BY
						lower ( company_name );';

		$arrstrClients = fetchData( $strSql, $objDatabase );

		$arrstrReorderedClients = [];

		if( true == valArr( $arrstrClients ) ) {
			foreach( $arrstrClients as $arrstrClient ) {
				$arrstrReorderedClients[$arrstrClient['id']] = $arrstrClient;
			}
		}

		return $arrstrReorderedClients;
	}

	public static function fetchClientsByProductTypeIdByCompanyStatusTypeId( $arrintCIds, $intCompanyStatusId, $intPsProductId, $objAdminDatabase ) {

		if( false == valArr( $arrintCIds ) || true == is_null( $intCompanyStatusId ) || true == is_null( $intPsProductId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						mc.id,
						mc.company_name AS company_name
					FROM
						clients mc
						LEFT JOIN contract_products cp ON ( mc.id = cp.cid )
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCIds ) . ' )
						AND mc.company_status_type_id = ' . ( int ) $intCompanyStatusId . '
						AND cp.ps_product_id = ' . ( int ) $intPsProductId;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchClientsByInvoiceBatchIds( $arrintInvoiceBatchIds, $objAdminDatabase ) {

		if( false == valArr( $arrintInvoiceBatchIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						c.id,
						c.company_name
					FROM
						clients c
						JOIN invoices i ON i.cid = c.id
					WHERE
						i.invoice_batch_id IN ( ' . implode( ',', $arrintInvoiceBatchIds ) . ' )
					GROUP BY
						c.id;';

		return parent::fetchClients( $strSql, $objAdminDatabase );
	}

	public static function fetchIsTestClientsByClientIdByCompanyStatusTypeIds( $intClientId, $arrintCompanyStatusTypeIds, $objDatabase ) {

		$boolIsTestClient = false;

		if( true == is_null( $intClientId ) || false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return $boolIsTestClient;
		}

		$strSql = '	SELECT
						id
					FROM
						clients
					WHERE
						id = ' . ( int ) $intClientId . '
						AND company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )';

		$arrintClientId = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintClientId ) ) {
			$boolIsTestClient = true;
		}

		return $boolIsTestClient;
	}

	public static function refreshCachedAccountsBalancesByCid( $intCid, $objAdminDatabase ) {

		if( false == valId( $intCid ) ) {
			return false;
		}

		$strSql = 'SELECT * FROM rebuild_accounts_balances( ' . ( int ) $intCid . ', ' . 'ARRAY[]::INT[]' . ' );';

		$arrmixResult = fetchData( $strSql, $objAdminDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return false;
		}

		return true;
	}

	public static function fetchClientByBluemoonRemotePrimayKey( $intBluemoonRemotePrimayKey, $objAdminDatabase ) {

		$strSql = 'SELECT
						c.*
					FROM
						clients c
						JOIN ps_leads pl ON ( pl.id = c.ps_lead_id )
					WHERE
						pl.bluemoon_remote_primary_key = ' . ( int ) $intBluemoonRemotePrimayKey;

		return parent::fetchClient( $strSql, $objAdminDatabase );
	}

	public static function fetchBluemoonClientsByIdsByCompanyStatusTypeIds( $arrintCids, $arrintCompanyStatusTypeIds, $objAdminDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						c.id,
						c.company_name,
						c.rwx_domain,
						pl.id ps_lead_id
					FROM
						clients c
						JOIN ps_leads pl ON ( pl.id = c.ps_lead_id )
					WHERE
						pl.bluemoon_remote_primary_key IS NOT NULL
						AND c.id IN ( ' . implode( ',', $arrintCids ) . ' )
						AND c.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )';

		return parent::fetchClients( $strSql, $objAdminDatabase );
	}

	public static function fetchClientsByIdsByCompanyStatusTypeId( $arrintCids, $intCompanyStatusTypeId, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						mc.* ,
						pl.id AS ps_lead_id
					FROM
						clients mc
						JOIN ps_leads pl ON( mc.ps_lead_id = pl.id AND pl.deleted_by IS NULL )
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
						AND mc.company_status_type_id = ' . ( int ) $intCompanyStatusTypeId . '
					ORDER BY
						lower( mc.company_name ) ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchNaaBlueMoonIntegratedClientsByPsProductId( $intPsProductId, $objDatabase ) {

		$strSql = 'SELECT
					 *
					FROM
						( SELECT
							DISTINCT ( c.id ),
							c.*
							FROM clients c
								JOIN contract_properties cp ON ( c.id = cp.cid )
								JOIN ps_leads pl ON ( c.ps_lead_id = pl.id AND pl.bluemoon_remote_primary_key IS NOT NULL )
								JOIN ps_lead_remote_properties plrp ON ( c.id = plrp.cid )
							WHERE cp.ps_product_id = ' . ( int ) $intPsProductId . '
								AND plrp.bluemoon_license_number IS NOT NULL
								AND plrp.is_disabled = 0
						UNION
							SELECT
								DISTINCT ( c.id ),
								c.*
							FROM clients c
								JOIN ps_leads pl ON ( c.ps_lead_id = pl.id AND pl.bluemoon_remote_primary_key IS NOT NULL )
								JOIN ps_lead_remote_properties plrp ON ( c.id = plrp.cid )
								JOIN simple_contracts sc on ( c.id = sc.cid )
							WHERE sc.ps_product_id = ' . ( int ) $intPsProductId . '
								AND plrp.bluemoon_license_number IS NOT NULL
								AND plrp.is_disabled = 0
						 ) as subSql
					ORDER BY subSql.company_name';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function checkIsClientExistsById( $intCid, $objDatabase ) {

		$strSql = 'SELECT EXISTS ( SELECT * FROM clients WHERE id = ' . ( int ) $intCid . ' ) AS is_client_exists';

		$arrstrResult = fetchData( $strSql, $objDatabase );

		if( 't' == $arrstrResult[0]['is_client_exists'] ) {
			return true;
		}

		return false;
	}

	public static function fetchClientWithAddressInformationByCid( $intCId, $objDatabase ) {

		$strSql = 'SELECT
						c.id AS client_id,
						c.entity_id,
						c.database_id,
						cst.name AS company_status_type,
						c.company_name,
						ca.street_line1,
						ca.city,
						ca.state_code,
						ca.postal_code,
						ca.country_code,
						ca.longitude,
						ca.latitude,
						c.rwx_domain
					FROM
						clients AS c
						JOIN company_status_types AS cst on cst.id = c.company_status_type_id
						LEFT JOIN company_addresses AS ca on ca.cid = c.id AND ca.address_type_id = ' . CAddressType::PRIMARY . '
					WHERE
						c.id = ' . ( int ) $intCId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchIntegratedClients( $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT( c.id ),
						c.*
					FROM
						clients c
						RIGHT JOIN integration_databases id ON( c.id = id.cid )
					ORDER BY
						c.company_name';

		return self::fetchClients( $strSql, $objClientDatabase );
	}

	public static function fetchDeligentClientsByIdsByCompanyStatusTypeId( $intCompanyStatusTypeId, $objDatabase ) {

		if( false == is_numeric( $intCompanyStatusTypeId ) ) {
			return false;
		}

		// For delinquency, we need to consider only those clients whose currency code is USD as of now
		$strSql = 'SELECT
						c.id
					FROM
						clients c
						JOIN ps_lead_details pld ON ( c.id = pld.cid AND pld.delinquency_start_date IS NOT NULL AND pld.delinquency_start_date <= now()::DATE )
					WHERE
						 c.company_status_type_id = ' . ( int ) $intCompanyStatusTypeId . '
						 AND c.currency_code = \'' . CCurrency::CURRENCY_CODE_USD . '\'
					ORDER BY
						c.id ASC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchClientsForFilesExport( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT c.id,
						c.company_name
					FROM
						file_exports fe
						JOIN clients c ON ( fe.cid = c.id )
					WHERE
						is_exported = 0
						AND file_export_type_id = ' . ( int ) CFileExportType::BULK_DOCUMENT_EXPORT;

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsForOneTimeExportsAndRecurringBackup( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT c.id,
						c.company_name
					FROM
						file_exports fe
						JOIN clients c ON ( fe.cid = c.id )
					WHERE
						is_exported = 0
						AND file_export_type_id IN ( ' . CFileExportType::ONE_TIME_EXPORT . ', ' . CFileExportType::RECURRING_BACKUP . ' )';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchLatestInsertedClientByDatabaseId( $arrintDatabaseIds, $objDatabase ) {

		$strSql = 'SELECT
						database_id
					FROM
						clients
					WHERE
						database_id IN (' . implode( ',', $arrintDatabaseIds ) . ')
					ORDER BY
						id DESC
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsCountByACVRank( $mixClientNameOrClientId, $strKeyClientACVRank, $intRisk, $arrintDatabaseIds, $objDatabase ) {

		$strWhere = 'c.company_status_type_id = ' . CCompanyStatusType::CLIENT;
		if( false == valStr( $mixClientNameOrClientId ) && false == valStr( $strKeyClientACVRank ) && false == valArr( $arrintDatabaseIds ) ) {
			$strWhere .= ' AND
						( CASE
							WHEN ( pld.key_client_acv_rank BETWEEN 1 AND 10 ) OR ( pld.key_client_override_rank BETWEEN 1 AND 10 ) THEN TRUE
							ELSE FALSE
						  END )';
		}

		$strWhere .= ( true == valStr( $mixClientNameOrClientId ) ) ? ' AND ( LOWER( c.company_name ) like \'%' . \Psi\CStringService::singleton()->strtolower( $mixClientNameOrClientId ) . '%\' OR c.id = ' . ( int ) $mixClientNameOrClientId . ' ) ' : '';
		$strWhere .= ( true == valStr( $strKeyClientACVRank ) ) ? ' AND ( CASE WHEN pld.key_client_acv_rank IN ( ' . $strKeyClientACVRank . ' ) OR pld.key_client_override_rank IN ( ' . $strKeyClientACVRank . ' ) THEN TRUE ELSE FALSE END )' : '';
		$strWhere .= ( true == valId( $intRisk ) ) ? ( '5' != $intRisk ? ' AND pld.client_risk_status = ' . $intRisk : ' AND ( pld.client_risk_status NOT IN ( 2,3,4 ) ) ' ) : '';
		$strWhere .= ( true == valArr( $arrintDatabaseIds ) ) ? ' AND c.database_id in ( ' . implode( ',', $arrintDatabaseIds ) . ' ) ' : '';

		$strSql = '
				SELECT
					count(c.id) as total
				FROM
					(
						SELECT
							count( p.id ) AS total_active_properties,
							SUM( p.number_of_units ) AS total_active_units,
							p.cid
						FROM
							properties p
							JOIN clients c ON ( p.cid = c.id )
							LEFT JOIN ps_leads pl ON ( c.ps_lead_id = pl.id )
							JOIN 
							(
								SELECT
									cp.property_id,
									cp.cid,
									rank( ) OVER ( PARTITION BY cp.property_id ORDER BY cp.close_date ASC, cp.commission_bucket_id ASC, cp.id )
								FROM
									contract_properties cp
									JOIN contracts c ON ( cp.contract_id = c.id AND cp.cid = c.cid )
									LEFT JOIN contract_termination_requests ctr ON ( ctr.cid = cp.cid AND ctr.id = cp.contract_termination_request_id )
								WHERE
									cp.commission_bucket_id <> ' . CCommissionBucket::RENEWAL . '
									AND c.contract_status_type_id IN ( ' . implode( ',', CContractStatusType::$c_arrintCompletedContractStatusTypeIds ) . ' )
									AND COALESCE ( ctr.contract_termination_reason_id, 0 ) <> ' . CContractTerminationReason::DUPLICATE_CONTRACT . '
							) AS cp_active ON ( cp_active.property_id = p.id AND cp_active.cid = p.cid AND rank = 1 )
						WHERE
							p.is_disabled <> 1
							AND p.is_test <> 1
							AND p.termination_date IS NULL
						GROUP BY
							p.cid
					) AS property_details
				RIGHT JOIN clients c ON ( property_details.cid = c.id )
				JOIN ps_lead_details pld ON ( c.id = pld.cid AND ( pld.key_client_override_rank IS NOT NULL OR pld.key_client_acv_rank IS NOT NULL OR pld.client_risk_status IS NOT NULL ) )
				JOIN ps_leads pl ON c.ps_lead_id = pl.id
				WHERE ' . $strWhere;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintCount ) && true == isset( $arrintCount[0]['total'] ) && 0 < $arrintCount[0]['total'] ) ? $arrintCount[0]['total'] : 0;
	}

	public static function fetchTopACVClientsDetails( $mixClientNameOrClientId, $strKeyClientACVRank, $intRisk, $arrintDatabaseIds, $strOrderBy, $strOrderDirection, $intOffSet = 0, $intLimit = 100, $objDatabase ) {
		$strOrderByClause = ( true == valStr( $strOrderBy ) && true == valStr( $strOrderDirection ) ) ? ( 'risk' == $strOrderBy ? 'pld.client_risk_status' : $strOrderBy ) . ' ' . ( ( 'risk' == $strOrderBy ) ? ( 'ASC' == $strOrderDirection ? 'DESC' : 'ASC' ) : $strOrderDirection ) : 'key_client_acv_rank ASC,pld.client_risk_status DESC';

		$strWhere = 'c.company_status_type_id = ' . CCompanyStatusType::CLIENT;
		if( false == valStr( $mixClientNameOrClientId ) && false == valStr( $strKeyClientACVRank ) && false == valArr( $arrintDatabaseIds ) ) {
			$strWhere .= ' AND
						( CASE
							WHEN ( pld.key_client_acv_rank BETWEEN 1 AND 10 ) OR ( pld.key_client_override_rank BETWEEN 1 AND 10 ) THEN TRUE
							ELSE FALSE
						END )';
		}

		$strWhere .= ( true == valStr( $mixClientNameOrClientId ) ) ? ' AND ( LOWER( c.company_name ) like \'%' . \Psi\CStringService::singleton()->strtolower( $mixClientNameOrClientId ) . '%\' OR c.id = ' . ( int ) $mixClientNameOrClientId . ' ) ' : '';
		$strWhere .= ( true == valStr( $strKeyClientACVRank ) ) ? ' AND ( CASE WHEN pld.key_client_acv_rank IN ( ' . $strKeyClientACVRank . ' ) OR pld.key_client_override_rank IN ( ' . $strKeyClientACVRank . ' ) THEN TRUE ELSE FALSE END )' : '';
		$strWhere .= ( true == valId( $intRisk ) ) ? ( CTopACVClientsReport::NO_RISK != $intRisk ? ' AND pld.client_risk_status = ' . $intRisk : ' AND ( pld.client_risk_status NOT IN ( ' . CTopACVClientsReport::LOW_RISK . ',' . CTopACVClientsReport::MEDIUM_RISK . ',' . CTopACVClientsReport::HIGH_RISK . ' ) ) ' ) : '';
		$strWhere .= ( true == valArr( $arrintDatabaseIds ) ) ? ' AND c.database_id in ( ' . implode( ',', $arrintDatabaseIds ) . ' ) ' : '';

		$strSql = ' SELECT
						c.id,
						c.company_name,
						c.database_id,
						( CASE WHEN property_details.total_active_properties IS NULL THEN 0 ELSE property_details.total_active_properties END ) as property_count,
						( CASE WHEN property_details.total_active_units IS NULL THEN 0 ELSE property_details.total_active_units END ) as number_of_units,
						( CASE
							WHEN pld.key_client_override_rank BETWEEN 1 AND 10 THEN pld.key_client_override_rank
							ELSE pld.key_client_acv_rank
						END ) AS key_client_acv_rank,
						( CASE
							WHEN pld.client_risk_status = ' . CTopACVClientsReport::HIGH_RISK . ' THEN \'High Risk\'
							WHEN pld.client_risk_status = ' . CTopACVClientsReport::MEDIUM_RISK . ' THEN \'Medium Risk\'
							WHEN pld.client_risk_status = ' . CTopACVClientsReport::LOW_RISK . ' THEN \'Low Risk\'
							ELSE \'No Risk\'
						END ) AS risk
					FROM
						(
							SELECT
								count ( p.id ) AS total_active_properties,
								SUM ( p.number_of_units ) AS total_active_units,
								p.cid
							FROM
								properties p
								JOIN clients c ON ( p.cid = c.id )
								LEFT JOIN ps_leads pl ON ( c.ps_lead_id = pl.id )
								JOIN 
								(
									SELECT
										cp.property_id,
										cp.cid,
										rank ( ) OVER ( PARTITION BY cp.property_id ORDER BY cp.close_date ASC, cp.commission_bucket_id ASC, cp.id )
									FROM
										contract_properties cp
										JOIN contracts c ON ( cp.contract_id = c.id AND cp.cid = c.cid )
										LEFT JOIN contract_termination_requests ctr ON ( ctr.cid = cp.cid AND ctr.id = cp.contract_termination_request_id )
									WHERE
										cp.commission_bucket_id <> ' . CCommissionBucket::RENEWAL . '
										AND c.contract_status_type_id IN ( ' . implode( ',', CContractStatusType::$c_arrintCompletedContractStatusTypeIds ) . ' )
										AND COALESCE ( ctr.contract_termination_reason_id, 0 ) <> ' . CContractTerminationReason::DUPLICATE_CONTRACT . '
							) AS cp_active ON ( cp_active.property_id = p.id AND cp_active.cid = p.cid AND rank = 1 )
						WHERE
							p.is_disabled <> 1
							AND p.is_test <> 1
							AND p.termination_date IS NULL
						GROUP BY
							p.cid
						) AS property_details
						RIGHT JOIN clients c ON ( property_details.cid = c.id )
						JOIN ps_lead_details pld ON ( c.id = pld.cid AND ( pld.key_client_override_rank IS NOT NULL OR pld.key_client_acv_rank IS NOT NULL OR pld.client_risk_status IS NOT NULL ) )
						JOIN ps_leads pl ON ( c.ps_lead_id = pl.id )
					WHERE
						' . $strWhere . '
					ORDER BY
						' . $strOrderByClause . '
					OFFSET ' . ( int ) $intOffSet . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsByCustomerManagerEmployeeId( $intCustomerManagerEmployeeId, $objDatabase ) {

		if( true == is_null( $intCustomerManagerEmployeeId ) || false == is_numeric( $intCustomerManagerEmployeeId ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						mc.id,
						mc.company_name AS company_name,
						pl.id AS ps_lead_id,
						SUM(spl.total_new_active_acv) as net_acv,
						MAX( to_char( ea.association_datetime ::DATE, \'YYYY/MM/DD\' ) ) AS association_datetime
					FROM
						clients mc
						JOIN ps_leads pl ON( mc.ps_lead_id = pl.id AND pl.deleted_on IS NULL AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
						JOIN stats_ps_leads spl ON ( pl.id = spl.ps_lead_id )
						JOIN ps_lead_details pld ON ( spl.ps_lead_id = pld.ps_lead_id AND pld.support_employee_id= ' . ( int ) $intCustomerManagerEmployeeId . ' )
						JOIN employee_associations ea ON( pld.ps_lead_id = ea.ps_lead_id AND pld.support_employee_id =ea.employee_id)
					GROUP BY
						mc.id,
						mc.company_name,
						pl.id
					ORDER BY
						mc.company_name';

		$arrstrImplementationManagerCompaniesDetailsTemp = fetchData( $strSql, $objDatabase );

		$arrstrImplementationManagerCompaniesDetails = [];

		if( true == valArr( $arrstrImplementationManagerCompaniesDetailsTemp ) ) {
			foreach( $arrstrImplementationManagerCompaniesDetailsTemp as $arrstrImplementationManagerCompany ) {
				$arrstrImplementationManagerCompaniesDetails[$arrstrImplementationManagerCompany['id']] = $arrstrImplementationManagerCompany;
			}
		}

		return $arrstrImplementationManagerCompaniesDetails;
	}

	public static function fetchClientIdsByCompanyStatusTypeIdsByDatabaseIds( $arrintCompanyStatusTypeIds, $arrintDatabaseIds, $objDatabase ) {
		if( false == valArr( $arrintCompanyStatusTypeIds ) || false == valArr( $arrintDatabaseIds ) ) {
			return NULL;
		}

		$strSql          = 'SELECT id FROM clients WHERE company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' ) AND database_id IN (' . implode( ',', $arrintDatabaseIds ) . ')';
		$arrstrClients   = ( array ) fetchData( $strSql, $objDatabase );
		$arrintClientIds = [];

		foreach( $arrstrClients as $arrstrClient ) {
			$arrintClientIds[] = $arrstrClient['id'];
		}

		return $arrintClientIds;
	}

	public static function fetchClientsByPsProductIdsByCompanyStatusTypeIdsByDatabaseIds( $arrintPsProductId, $arrintCompanyStatusTypeId, $arrintDatabaseId, $objDatabase ) {

		if( false == ( $arrintPsProductId = getIntValuesFromArr( $arrintPsProductId ) ) || false == ( $arrintCompanyStatusTypeId = getIntValuesFromArr( $arrintCompanyStatusTypeId ) ) || false == ( $arrintDatabaseId = getIntValuesFromArr( $arrintDatabaseId ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON (mc.id) mc.*
				FROM
					clients mc,
					contracts c,
					contract_properties cp
					WHERE
						mc.id = c.cid
						AND c.id = cp.contract_id ';

		$strSql .= ' AND mc.company_status_type_id IN ( ' . sqlIntImplode( $arrintCompanyStatusTypeId ) . ' )
					AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
					AND mc.database_id IN ( ' . sqlIntImplode( $arrintDatabaseId ) . ' )
					AND cp.ps_product_id IN ( ' . sqlIntImplode( $arrintPsProductId ) . ' )';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsHavingContractByCompanyStatusTypeIdsByPsProductIds( $arrintCompanyStatusTypeIds, $arrintPsProductIds, $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyStatusTypeIds ) || false == valArr( $arrintPsProductIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT c.id, 
						c.*
					FROM
						clients as c
					JOIN 
						contract_properties as cp on c.id = cp.cid
					WHERE
						c.id = cp.cid 
						AND c.company_status_type_id IN (' . implode( ', ', $arrintCompanyStatusTypeIds ) . ') 
						AND cp.termination_date IS NULL 
						AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND cp.property_id NOT IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
					ORDER BY c.company_name';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchAllActiveClientsByPsProductId( $intPsProductId, $objDatabase ) {
		if( false == is_numeric( $intPsProductId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						mc.*
					FROM
						clients AS mc
						JOIN contracts AS c ON (c.cid = mc.id )
						JOIN contract_properties AS cp ON ( cp.contract_id = c.id )
					WHERE
						mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND cp.ps_product_id = ' . ( int ) $intPsProductId . ' 
 						AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
						AND ( cp.termination_date IS NULL OR cp.termination_date > now() )
					GROUP BY
						mc.id
					ORDER BY mc.company_name';

		return self::fetchClients( $strSql, $objDatabase );

	}

	public static function fetchAllActiveClients( $objDatabase ) {

		$strSql = 'SELECT
						mc.id,
						mc.company_name
					FROM
						clients AS mc
						JOIN contracts AS c ON (c.cid = mc.id )
						JOIN contract_properties AS cp ON ( cp.contract_id = c.id )
						LEFT JOIN implementation_dates AS id on ( id.contract_property_id = cp.id )
					WHERE
						mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
 						AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
						AND ( cp.termination_date IS NULL OR cp.termination_date > now() )
					GROUP BY
						mc.id
					ORDER BY mc.company_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomAllActiveClients( $objDatabase ) {

		$strSql = 'SELECT
						mc.id,
						mc.company_name
					FROM
						clients AS mc
						JOIN contracts AS c ON (c.cid = mc.id )
						JOIN contract_properties AS cp ON ( cp.contract_id = c.id )
						LEFT JOIN implementation_dates AS id on ( id.contract_property_id = cp.id )
					WHERE
 						c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
						AND ( cp.termination_date IS NULL OR cp.termination_date > now() )
					GROUP BY
						mc.id
					ORDER BY mc.company_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveClientsByPsProductIdsByCompanyStatusTypeIdsByIds( $arrintPsProductIds, $arrintCompanyStatusTypeIds, $arrintIds, $objDatabase ) {

		if( false == valArr( $arrintPsProductIds ) || false == valArr( $arrintCompanyStatusTypeIds ) || false == valArr( $arrintIds ) ) {
			return;
		}

		$strSql = '( SELECT
						mc.*
					FROM
						clients AS mc
						JOIN contracts AS c ON ( c.cid = mc.id )
						JOIN contract_properties AS cp ON ( cp.contract_id = c.id )
					WHERE
						mc.id IN( ' . sqlIntImplode( $arrintIds ) . ' )
						AND mc.company_status_type_id IN ( ' . sqlIntImplode( $arrintCompanyStatusTypeIds ) . ' )
						AND cp.ps_product_id IN ( ' . sqlIntImplode( $arrintPsProductIds ) . ' )
 						AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
					GROUP BY
						mc.id
					ORDER BY mc.company_name )
					UNION ALL
					( SELECT 
						mc.*
					FROM
						clients AS mc
						JOIN simple_contracts sc ON ( sc.cid = mc.id )
						JOIN properties p ON ( p.cid = sc.cid AND p.id = sc.property_id)
					WHERE 
						mc.id IN ( ' . sqlIntImplode( $arrintIds ) . ' )
						AND mc.company_status_type_id IN ( ' . sqlIntImplode( $arrintCompanyStatusTypeIds ) . ' )
						AND sc.ps_product_id IN ( ' . sqlIntImplode( $arrintPsProductIds ) . ' )
						AND ( sc.expire_on IS NULL OR sc.expire_on > NOW() )
					GROUP BY
						mc.id
					ORDER BY
						mc.company_name )';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetClientsCsmDetailsByCids( $arrstrClientIds, $objDatabase ) {

		$strSql = 'SELECT
						c.id AS client_id,
						c.company_name AS client_name,
						cst.id as company_status_type_id,
						cst.name AS company_status,
						e.name_full AS CSM,
						e.email_address
					FROM
						ps_lead_details pld
					LEFT JOIN employees e ON pld.support_employee_id = e.id
					JOIN clients c ON c.id = pld.cid
					JOIN company_status_types cst on c.company_status_type_id = cst.id
					WHERE
						c.id IN (' . implode( ', ', $arrstrClientIds ) . ')
					ORDER BY cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsByIdsOrderedByName( $arrintCids, $objDatabase, $boolIsFetchObject = true ) {

		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						mc.*
					FROM
						clients mc
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
					ORDER BY
						mc.company_name ASC';

		if( true == $boolIsFetchObject ) {
			return self::fetchClients( $strSql, $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchIsBelongsToTopAcvClientsByCidByLimit( $intCid, $intLimit = 50, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						( CASE
						WHEN ' . ( int ) $intCid . ' = ANY ( array_agg ( subSql.cid ) ) THEN 1
						ELSE 0
						END ) AS is_belongs_to_top_acv_client
					FROM
						(
							SELECT
								spl.cid,
								SUM ( spl.total_new_active_acv ) AS total_acv
							FROM
								stats_ps_leads AS spl
								JOIN clients AS c ON ( c.id = spl.cid AND c.company_status_type_id = 4 )
							GROUP BY
								spl.cid
							ORDER BY
								total_acv DESC
							Limit ' . ( int ) $intLimit . '
						) AS subSql';

		$arrmixTopAcvClient = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixTopAcvClient ) && isset( $arrmixTopAcvClient[0]['is_belongs_to_top_acv_client'] ) ) {
			return $arrmixTopAcvClient[0]['is_belongs_to_top_acv_client'];
		}

		return 0;
	}

	public static function fetchClientDetailsByContractDraftStatusIds( $arrintContractDraftStatusIds, $objDatabase ) {

		if( false == valArr( $arrintContractDraftStatusIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						c.id,
						c.company_name
					FROM
						clients c
						JOIN contract_drafts cd ON ( cd.cid = c.id )
					WHERE
						cd.contract_draft_status_id IN ( ' . implode( ',', $arrintContractDraftStatusIds ) . ' )
					ORDER BY
						c.company_name';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchDatabaseIdByCids( $arrintCids, $objDatabase ) {

		$strSql = 'SELECT 
						id,
						database_id 
					FROM 
						clients 
					WHERE 
						id IN ( ' . implode( ',', $arrintCids ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientByPropertyId( $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						c.*
					FROM
						clients as c
					JOIN properties p ON ( p.cid = c.id AND p.id = ' . ( int ) $intPropertyId . ' )';

		return self::fetchClient( $strSql, $objDatabase );
	}

	public static function fetchClientsByTransmissionTypeId( $intTransmissionTypeId, $objDatabase ) {
		if( false == valId( $intTransmissionTypeId ) ) {
			return;
		}
		$strSql = 'SELECT
             DISTINCT c.*
           FROM
             clients c 
             JOIN company_transmission_vendors ctv  ON (c.id = ctv.cid )
             WHERE ctv.transmission_type_id  = ' . ( int ) $intTransmissionTypeId . '
             ORDER BY c.company_name;';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsWithCallPhoneNumbersByStatusTypeIds( $arrintCompanyStatusTypeIds, $objDatabase ) {
		$strWhereSql = NULL;
		if( true == valArr( $arrintCompanyStatusTypeIds ) ) {
			$strWhereSql = ' AND c . company_status_type_id IN( ' . implode( ', ', $arrintCompanyStatusTypeIds ) . ' ) ';
		}

		$strSql = '	SELECT
						c.id,
						c.company_name
					FROM
						clients AS c 
						JOIN call_phone_numbers AS cpn ON ( c.id = cpn.cid )
					WHERE
						cpn.phone_number IS NOT NULL
						' . $strWhereSql . '
					ORDER BY company_name';

		$arrmixClients = fetchData( $strSql, $objDatabase );

		return $arrmixClients;
	}

	public static function fetchAllClientsByCompanyStatusTypeIdsByPsProductIds( $arrintCompanyStatusTypeIds, $arrintPsProductIds, $objDatabase ) {

		if( false == valArr( $arrintPsProductIds ) || false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT mc.id,
						mc.company_name,
						mc.database_id,
						c.contract_start_date as contract_start_date,
						cp.property_id as property_id
					FROM
						clients mc,
						contracts c,
						contract_properties cp
					WHERE
						mc.id = c.cid
						AND c.id = cp.contract_id
						AND mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . '
						AND cp.renewal_contract_property_id IS NULL
						AND cp.is_last_contract_record = TRUE
						AND ( cp.deactivation_date::date > NOW()::date OR cp.deactivation_date IS NULL )

					UNION

					SELECT
						DISTINCT sc.cid,
						mc.company_name,
						mc.database_id,
						sc.created_on as contract_start_date,
						sc.property_id as property_id
					FROM
						clients mc
						JOIN simple_contracts sc ON ( sc.cid = mc.id )
						JOIN ps_products as pp ON ( pp.id = sc.ps_product_id )
						JOIN users as u ON ( u.id = sc.created_by )
						JOIN employees as e ON ( e.id = u.employee_id )
					WHERE
						mc.id = sc.cid
						AND sc.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						AND ( sc.expire_on::date > NOW()::date OR sc.expire_on IS NULL )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedClientsByExportPartnerIdByExportObjectType( $intExportPartnerId, $strExportObjectType, $objDatabase, $arrmixParameters = NULL, $boolIsCount = false, $objPagination = NULL ) {
		if( false == valId( $intExportPartnerId ) || false == valStr( $strExportObjectType ) ) {
			return NULL;
		}

		$strFilterStatusCondition     = '';
		$strFilterClientTypeCondition = ' c.company_status_type_id = ' . CCompanyStatusType::CLIENT;
		$strFilterDateCondition       = " AND (c.created_on >= date_trunc( 'month', NOW() ) - INTERVAL '5 months' OR eo.exported_on >= date_trunc( 'month', NOW() ) - INTERVAL '5 months' )";
		$strOffsetLimitCondition      = '';
		$strSortDirection             = 'DESC';
		$strOrderByCondition          = 'id';
		$strCidCondition              = '';
		$strCreatedOn                 = '';
		$strExportedOn                = '';

		if( true == valArr( $arrmixParameters ) ) {
			if( true == valStr( $arrmixParameters['status_type'] ) ) {
				if( 1 == $arrmixParameters['status_type'] ) {
					$strFilterStatusCondition = ' AND eo.export_status_type_id = ' . CExportStatusType::NETSUITE_COMPLETED;
				} elseif( 2 == $arrmixParameters['status_type'] ) {
					$strFilterStatusCondition = ' AND eo.export_status_type_id IS NULL';
				} elseif( 3 == $arrmixParameters['status_type'] ) {
					$strFilterStatusCondition = ' AND eo.export_status_type_id IN ( ' . CExportStatusType::NETSUITE_FAILED . ',' . CExportStatusType::NETSUITE_INCOMPLETE . ' )';
				}
			}

			if( true == valId( $arrmixParameters['client_type'] ) ) {
				$strFilterClientTypeCondition = ' c.company_status_type_id = ' . $arrmixParameters['client_type'];
			} else {
				$strFilterClientTypeCondition = ' c.company_status_type_id IN (' . CCompanyStatusType::TERMINATED . ', ' . CCompanyStatusType::CLIENT . ')';
			}

			if( true == valStr( $arrmixParameters['time_period'] ) ) {
				if( 6 == $arrmixParameters['time_period'] ) {
					$strFilterDateCondition = " >= date_trunc( 'month', NOW() ) - INTERVAL '5 months' ";
					$strCreatedOn           = ' c.created_on ';
					$strExportedOn          = ' eo.exported_on ';
				} else {
					$strFilterDateCondition = ' = ' . $arrmixParameters['time_period'];
					$strCreatedOn           = " date_part('year',c.created_on) ";
					$strExportedOn          = " date_part('year',eo.exported_on) ";
				}

				if( 0 == $arrmixParameters['status_type'] || 2 == $arrmixParameters['status_type'] ) {
					$strFilterDateCondition = ' AND ( (' . $strCreatedOn . $strFilterDateCondition . ') OR ( ' . $strExportedOn . $strFilterDateCondition . '))';
				} else {
					$strFilterDateCondition = ' AND ' . $strExportedOn . $strFilterDateCondition;
				}
			}

			if( true == valStr( $arrmixParameters['sort_direction'] ) ) {
				$strSortDirection = $arrmixParameters['sort_direction'];
			}

			if( true == valStr( $arrmixParameters['order_by'] ) && 'undefined' != $arrmixParameters['order_by'] ) {
				$strOrderByCondition = $arrmixParameters['order_by'];
			}

			if( true == valStr( $arrmixParameters['custom_id'] ) ) {
				$strCidCondition = ' AND c.id = ' . ( int ) $arrmixParameters['custom_id'];
			}
		}

		if( false == $boolIsCount ) {
			$strOffsetLimitCondition = ' ORDER BY ' . $strOrderByCondition . ' ' . $strSortDirection;

			if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
				$strOffsetLimitCondition .= ' OFFSET ' . ( int ) $objPagination->getOffset() . '
											 LIMIT ' . ( int ) $objPagination->getPageSize();
			}

			$strSelectFields = ' c.id,
								c.company_name,
								c.created_on,
								eo.reference_id,
								eo.export_status_type_id,
								eo.exported_on ';
		} else {
			$strSelectFields = ' count( c.id ) ';
		}

		$strSql = ' SELECT' .
		          $strSelectFields . '
 					FROM
 						clients c
 					LEFT JOIN export_objects eo ON ( c.id = eo.export_object_type_key::int AND eo.export_partner_id = ' . $intExportPartnerId . ' AND eo.export_object_type =\'' . $strExportObjectType . '\' )
 					WHERE 
 					' . $strFilterClientTypeCondition . '
 					' . $strCidCondition . '
					' . $strFilterStatusCondition . '
					' . $strFilterDateCondition . '
					' . $strOffsetLimitCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsInvoicesStatusCountByCids( $arrintCids, $objAdminDatabase, $boolIsCreditMemo = false ) {
		if( true == $boolIsCreditMemo ) {
			$strWhere        = ' AND i.invoice_amount < 0 ';
			$strExportObject = CExportObject::CREDIT_MEMO;
		} else {
			$strWhere        = ' AND i.is_credit_memo = 0
						AND i.invoice_amount >= 0
						AND i.invoice_status_type_id <> ' . CInvoiceStatusType::SALES_TAX_PENDING;
			$strExportObject = CExportObject::INVOICE;
		}

		$strSql = ' SELECT
						c.id,
						COUNT( DISTINCT i.id ) AS total_invoices_count,
						COUNT( DISTINCT CASE WHEN export_status_type_id = 1 OR export_status_type_id = 3 THEN i.id END ) AS mapped_invoices,
						COUNT( DISTINCT CASE WHEN export_status_type_id IS NULL THEN i.id END ) AS unmapped_invoices,
						COUNT( DISTINCT CASE WHEN export_status_type_id = 2 THEN i.id END ) AS failed_invoices
					FROM
						clients c
						JOIN invoices i ON ( c.id = i.cid ' . $strWhere . ' )
						JOIN transactions t ON ( t.cid = i.cid AND t.invoice_id = i.id AND t.charge_code_id NOT IN ( ' . CChargeCode::PAYMENT_RECEIVED . ', ' . CChargeCode::SALES_TAX . ' ) )
						JOIN accounts a ON (a.id = i.account_id AND a.cid = i.cid AND a.account_type_id NOT IN ( ' . implode( ',', CAccountType::$c_arrintNetSuiteNotAllowedAccountIds ) . ' ) )
						LEFT JOIN export_objects eo ON ( i.cid = eo.cid AND i.id = eo.export_object_type_key::int AND eo.export_object_type = \'' . $strExportObject . '\' )
					WHERE
						i.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ( i.created_on >= date_trunc ( \'month\', NOW ( ) )
							OR eo.exported_on >= date_trunc ( \'month\', NOW ( ) ) )
					GROUP BY c.id';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchClientsPaymentsStatusCountByCids( $arrintCids, $objDatabase, $boolIsRefundPayments = false ) {

		if( true == $boolIsRefundPayments ) {
			$strWhere            = 'AND cp.payment_amount < 0';
			$strExportObjectType = CExportObject::RETURN_PAYMENT;
		} else {
			$strWhere            = 'AND cp.payment_amount > 0';
			$strExportObjectType = CExportObject::PAYMENT;
		}

		$strSql = 'SELECT
						c.id,
						COUNT( cp.id ) AS total_count,
						SUM( CASE WHEN eo.export_status_type_id = ' . CExportStatusType::NETSUITE_COMPLETED . ' OR eo.export_status_type_id = ' . CExportStatusType::NETSUITE_INCOMPLETE . ' THEN 1 ELSE 0 END ) AS mapped_count,
						SUM( CASE WHEN eo.export_status_type_id IS NULL THEN 1 ELSE 0 END ) AS unmapped_count,
						SUM( CASE WHEN eo.export_status_type_id = ' . CExportStatusType::NETSUITE_FAILED . ' THEN 1 ELSE 0 END ) AS failed_count
					FROM
						clients c
						JOIN company_payments cp ON ( c.id = cp.cid AND cp.deposit_id IS NOT NULL )
						JOIN accounts a ON ( cp.account_id = a.id AND a.account_type_id NOT IN ( ' . implode( ',', CAccountType::$c_arrintNetSuiteNotAllowedAccountIds ) . ' ))
						LEFT JOIN export_objects eo ON ( cp.cid = eo.cid AND cp.id = eo.export_object_type_key::int AND eo.export_object_type = \'' . $strExportObjectType . '\' )
					WHERE
						cp.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ' . $strWhere . '
						AND ( cp.created_on >= date_trunc ( \'month\', NOW ( ) )
							OR eo.exported_on >= date_trunc ( \'month\', NOW ( ) ) )
					GROUP BY c.id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveTemporaryAndDemoClientsByIdsByPsProductId( $arrintCids, $intPsProductId, $objDatabase ) {
		if( false == valId( $intPsProductId ) ) {
			return NULL;
		}

		$strCondition = '';

		if( true == valArr( $arrintCids ) ) {
			$strCondition = ' AND mc.id NOT IN ( ' . implode( ',', $arrintCids ) . ')';
		}

		$strSql = ' SELECT
						*
					FROM
						( 
							(	SELECT
									DISTINCT ON (mc.id) mc.*
								FROM
									clients mc
									JOIN contracts c ON( mc.id = c.cid )
									JOIN contract_properties cp ON( c.id = cp.contract_id AND c.cid = cp.cid )
								WHERE
									c.contract_status_type_id = ' . ( int ) CContractStatusType::CONTRACT_APPROVED . '
									AND mc.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT . '
									AND cp.ps_product_id = ' . ( int ) $intPsProductId . $strCondition . '
							)
							UNION
							(	SELECT
									DISTINCT ON (mc.id) mc.*
								FROM
									clients mc
									JOIN simple_contracts sc ON( mc.id = sc.cid )
								WHERE
									sc.ps_product_id = ' . ( int ) $intPsProductId . $strCondition . '
									AND mc.company_status_type_id = ' . ( int ) CCompanyStatusType::CLIENT . '
									AND ( sc.expire_on > NOW() OR sc.expire_on IS NULL )
							)
							UNION
							(	SELECT
									mc.*
								FROM
									clients mc
								WHERE
									mc.id IN ( ' . implode( ',', CClient::$c_arrintDemoSalesCids ) . ' ) ' . $strCondition . '
							)
						) AS sub_query
					ORDER BY
						lower( company_name ) ASC ';

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsByExportPartnerIdByExportObjectType( $intExportPartnerId, $strExportObjectType, $objDatabase, $arrmixFilteredExplodedSearch = NULL, $arrmixParameters = NULL ) {
		$strFilterStatusCondition     = '';
		$strFilterClientTypeCondition = ' AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT;
		$strFilterDateCondition       = " AND (c.created_on >= date_trunc( 'month', NOW() ) - INTERVAL '5 months' OR eo.exported_on >= date_trunc( 'month', NOW() ) - INTERVAL '5 months' )";
		$strExportedOn                = '';
		$strCreatedOn                 = '';

		if( true == valArr( $arrmixParameters ) ) {
			if( true == valStr( $arrmixParameters['status_type'] ) ) {
				if( 1 == $arrmixParameters['status_type'] ) {
					$strFilterStatusCondition = ' AND eo.export_status_type_id = ' . CExportStatusType::NETSUITE_COMPLETED;
				} elseif( 2 == $arrmixParameters['status_type'] ) {
					$strFilterStatusCondition = ' AND eo.export_status_type_id IS NULL';
				} elseif( 3 == $arrmixParameters['status_type'] ) {
					$strFilterStatusCondition = ' AND eo.export_status_type_id IN ( ' . CExportStatusType::NETSUITE_FAILED . ',' . CExportStatusType::NETSUITE_INCOMPLETE . ' )';
				}
			}

			if( true == valId( $arrmixParameters['client_type'] ) ) {
				$strFilterClientTypeCondition = ' AND c.company_status_type_id = ' . $arrmixParameters['client_type'];
			} else {
				$strFilterClientTypeCondition = ' AND c.company_status_type_id IN (' . CCompanyStatusType::TERMINATED . ', ' . CCompanyStatusType::CLIENT . ')';
			}

			if( true == valStr( $arrmixParameters['time_period'] ) ) {
				if( 6 == $arrmixParameters['time_period'] ) {
					$strFilterDateCondition = " >= date_trunc( 'month', NOW() ) - INTERVAL '5 months ' ";
					$strCreatedOn           = ' c.created_on ';
					$strExportedOn          = ' eo.exported_on ';
				} else {
					$strFilterDateCondition = ' = ' . $arrmixParameters['time_period'];
					$strCreatedOn           = " date_part('year',c.created_on) ";
					$strExportedOn          = " date_part('year',eo.exported_on) ";
				}

				if( 0 == $arrmixParameters['status_type'] || 2 == $arrmixParameters['status_type'] ) {
					$strFilterDateCondition = ' AND ( (' . $strCreatedOn . $strFilterDateCondition . ') OR ( ' . $strExportedOn . $strFilterDateCondition . '))';
				} else {
					$strFilterDateCondition = ' AND ' . $strExportedOn . $strFilterDateCondition;
				}
			}
		}

		if( false == valId( $intExportPartnerId ) || false == valStr( $strExportObjectType ) ) {
			return NULL;
		}

		$strClientsIdCondition = ( 1 <= \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) ? ' to_char( c.id, \'99999999\' ) LIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR ' : '';

		$strSql = ' SELECT
						c.id,
						c.company_name
					FROM
						clients c
					LEFT JOIN export_objects eo ON ( c.id = eo.export_object_type_key::int AND eo.export_partner_id = ' . $intExportPartnerId . ' AND eo.export_object_type = \'' . $strExportObjectType . '\' )
					WHERE 
						1=1 
						' . $strFilterClientTypeCondition . '
						' . $strFilterStatusCondition . '
						' . $strFilterDateCondition .
		          ' AND (' . $strClientsIdCondition . ' c.company_name ILIKE \'%' . implode( '%\' AND c.company_name ILIKE \'%', $arrmixFilteredExplodedSearch ) . '%\')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientsForPricingMarketGrowth( $objDatabase ) {
		$strSql = sprintf( 'SELECT DISTINCT
							              c.id
							       FROM clients AS c
							            JOIN properties AS p ON p.cid = c.id AND p.is_disabled = 0 AND
							              p.is_test = 0 AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
							            JOIN property_types AS pt ON pt.id = p.property_type_id AND pt.occupancy_type_id IN ( ' . COccupancyType::CONVENTIONAL . ', ' . COccupancyType::STUDENT . ' )
							            LEFT JOIN property_products AS pp ON pp.ps_product_id IN ( ' . CPsProduct::PRICING . ',' . CPsProduct::PRICING_STUDENT . ' ) AND
							              pp.property_id = p.id
							            LEFT JOIN property_transmission_vendors AS ptv ON ptv.cid = c.id AND
							              ptv.property_id = p.id
							            LEFT JOIN company_transmission_vendors AS ctv ON ctv.id =
							              ptv.company_transmission_vendor_id
							            GROUP BY c.id' );

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchLocaleCodeById( $intId, $objDatabase ) {

		$strSql = 'SELECT
						locale_code
					FROM
						clients
					WHERE
						id = ' . ( int ) $intId;

		return parent::fetchColumn( $strSql, 'locale_code', $objDatabase );
	}

	public static function fetchClientsByPsProductIdsByCompanyStatusTypeIds( $arrintPsProductId, $arrintCompanyStatusTypeId, $objDatabase, $arrintCids = NULL ) {

		if( false == ( $arrintPsProductId = getIntValuesFromArr( $arrintPsProductId ) ) || false == ( $arrintCompanyStatusTypeId = getIntValuesFromArr( $arrintCompanyStatusTypeId ) ) ) {
			return NULL;
		}

		$strSql = ' SELECT  
						DISTINCT c.*
					FROM
						clients c
					JOIN property_products pp ON ( c.id = pp.cid )
					WHERE 
						c.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeId ) . ' ) 
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductId ) . ' )';

		if( true == valArr( $arrintCids ) ) {
			$strSql .= ' AND c.id IN ( ' . implode( ',', $arrintCids ) . ' )';
		}

		return self::fetchClients( $strSql, $objDatabase );
	}

	public static function fetchClientsByCompanyStatusTypeIdsByCountryCodes( $arrintCompanyStatusTypeIds, $arrstrCountryCodes, $objDatabase ) {
		$strSql = 'SELECT * FROM clients WHERE company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' ) AND country_code IN ( \'' . implode( '\',\'', $arrstrCountryCodes ) . '\' ) ORDER BY lower( company_name ) ASC';

		return self::fetchClients( $strSql, $objDatabase );
	}

}

?>
