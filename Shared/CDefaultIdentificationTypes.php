<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultIdentificationTypes
 * Do not add any new functions to this class.
 */

class CDefaultIdentificationTypes extends CBaseDefaultIdentificationTypes {

	public static function fetchDefaultIdentificationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultIdentificationType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultIdentificationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultIdentificationType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>