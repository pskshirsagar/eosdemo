<?php

class CPropertyValidator {

	protected $m_objProperty;

	public function __construct() {

	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
    }

	/**
	 * Validation Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objProperty->getCid() ) || 0 >= ( int ) $this->m_objProperty->getCid() ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Property client id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyName( $objDatabase = NULL, $boolIsDuplicatePropertyName = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_objProperty->getPropertyName() ) || 3 > strlen( trim( $this->m_objProperty->getPropertyName() ) ) ) {
			if( 0 == strlen( trim( $this->m_objProperty->getPropertyName() ) ) ) {
				$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', __( 'Property name is required.' ) ) );
				return false;
			} else {
				$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', __( 'A property name must contain at least 3 characters.' ) ) );
				return false;
			}
		}

		if( CLanguage::ENGLISH == CLocaleContainer::createService()->getTargetLocaleCode() ) {
			$strPropertyName = \Psi\CStringService::singleton()->preg_replace( '/[^a-zA-Z0-9\s]/', '', trim( $this->m_objProperty->getPropertyName() ) );

			if( 0 == strlen( $strPropertyName ) ) {
				$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', __( 'Invalid Property Name.' ) ) );
				return false;
			}
		}

		if( true == $boolIsDuplicatePropertyName && true == is_null( $this->m_objProperty->getId() ) && false == $this->m_objProperty->valCheckDuplicationByPropertyName( $objDatabase ) ) {
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', __( 'Property name already exists.' ) ) );
			$boolIsValid = false;
		}

		$strPropertyName = \Psi\CStringService::singleton()->preg_replace( '/(\s\s+)/', ' ', $this->m_objProperty->getPropertyName() );
		$this->m_objProperty->setPropertyName( $strPropertyName );
		return $boolIsValid;
	}

	public function valPropertyTypeId() {
		if( false == is_int( $this->m_objProperty->getPropertyTypeId() ) ) {
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_type_id', __( 'Property type is required.' ), '' ) );
			return false;
		}

		return true;
	}

	public function valNumberOfUnits( $boolIsRequired = false ) {
		$boolIsValid = true;

		if( false == is_null( $this->m_objProperty->getNumberOfUnits() ) && ( 0 > ( int ) $this->m_objProperty->getNumberOfUnits() || 10 < strlen( $this->m_objProperty->getNumberOfUnits() ) ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_units', __( 'Number of units is not a valid number.' ) ) );
		}

		if( true == $boolIsRequired ) {

			if( true == is_null( $this->m_objProperty->getNumberOfUnits() ) ) {
				$boolIsValid = false;
				$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_units', __( 'Number of units is required.' ) ) );
			}

			if( ( 0 == $this->m_objProperty->getNumberOfUnits() || 10 < strlen( $this->m_objProperty->getNumberOfUnits() ) ) && true == $boolIsValid ) {
				$boolIsValid = false;
				$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_units', __( 'Number of units is not a valid number.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valYearBuilt() {

		$boolIsValid = true;

		$strUnformattedYearBuiltDate = $this->m_objProperty->getUnformattedYearBuilt();

		if( false == is_null( $strUnformattedYearBuiltDate ) && ( false == CValidation::validateDate( $strUnformattedYearBuiltDate, true )
			|| ( '1800' > \Psi\CStringService::singleton()->substr( $strUnformattedYearBuiltDate, 6, 4 ) || ( date( 'Y' ) + 5 ) < \Psi\CStringService::singleton()->substr( $strUnformattedYearBuiltDate, 6, 4 ) ) ) ) {

			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year_built', __( 'Year built does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valYearRemodeled() {

		$boolIsValid = true;

		$strUnformattedYearRemodeledDate = $this->m_objProperty->getUnformattedYearRemodeled();

		if( false == is_null( $strUnformattedYearRemodeledDate ) && ( false == CValidation::validateDate( $strUnformattedYearRemodeledDate, true )
				|| ( '1900' > \Psi\CStringService::singleton()->substr( $strUnformattedYearRemodeledDate, 6, 4 ) || ( date( 'Y' ) + 5 ) < \Psi\CStringService::singleton()->substr( $strUnformattedYearRemodeledDate, 6, 4 ) ) ) ) {

			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year_remodeled', __( 'Year remodeled does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valShortDescription() {
		$boolIsValid = $this->m_objProperty->valShortDescription();

		if( true == is_null( trim( $this->m_objProperty->getShortDescription() ) ) || 0 == strlen( trim( $this->m_objProperty->getShortDescription() ) ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'short_description', __( 'Short Description is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFullDescription() {
		$boolIsValid = $this->m_objProperty->valFullDescription();

		if( true == is_null( trim( $this->m_objProperty->getFullDescription() ) ) || 0 == strlen( trim( $this->m_objProperty->getFullDescription() ) ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'full_description', __( 'Long Description is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLookupCode( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $objDatabase ) || false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( __( 'Database object CDatabase was not sent to the validation fucntion on property' ), $objDatabase );
		}

		if( true == valStr( $this->m_objProperty->getLookupCode() ) ) {

			$strWhere = ' WHERE lower(lookup_code) = \'' . trim( \Psi\CStringService::singleton()->strtolower( $this->m_objProperty->getLookupCode() ) ) . '\' AND cid = ' . $this->m_objProperty->getCid();
			if( false == is_null( $this->m_objProperty->getId() ) ) $strWhere .= ' AND id != ' . ( int ) $this->m_objProperty->getId();

			$intPropertyCount = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyCount( $strWhere, $objDatabase );

			if( 0 < $intPropertyCount ) {
				$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lookup_code', __( 'The look up code you entered is already being used on another property.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valVaultwareNumber( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $objDatabase ) || false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( __( 'Database object CDatabase was not sent to the validation fucntion on property' ), $objDatabase );
		}

		if( false == is_null( $this->m_objProperty->getVaultwareNumber() ) && 0 < strlen( $this->m_objProperty->getVaultwareNumber() ) ) {

			$strWhere = ' WHERE lower(vaultware_number) = \'' . trim( \Psi\CStringService::singleton()->strtolower( $this->m_objProperty->getVaultwareNumber() ) ) . '\' AND cid = ' . $this->m_objProperty->getCid();

			if( false == is_null( $this->m_objProperty->getId() ) ) $strWhere .= ' AND id != ' . ( int ) $this->m_objProperty->getId();

			$intPropertyCount = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyCount( $strWhere, $objDatabase );

			if( 0 < $intPropertyCount ) {
				$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_number', __( 'The vaultware number(External Feed Id) you entered is already being used on another property.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valOwnerId() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objProperty->getOwnerId() ) ) {

			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'owner_id', __( 'Owner is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLongitudeAndLatitude() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objProperty->getLongitude() ) || true == is_null( $this->m_objProperty->getLatitude() ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invalid_latitude_longitude', __( 'Valid address is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyPhotosCount() {

		$boolIsValid = true;

		if( 3 > $this->m_objProperty->getPropertyPhotosCount() ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_photos_count', __( 'At least three photos required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyFloorplansCount() {

		$boolIsValid = true;

		if( 1 > $this->m_objProperty->getPropertyFloorplansCount() ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floor_plans_count', __( 'At least one floor plan required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCommunityAmenitiesCount() {

		$boolIsValid = true;

		if( 3 > $this->m_objProperty->getCommunityAmenitiesCount() ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'community_amenities_count', __( 'At least three amenities required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valOfficePhoneNumber() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objProperty->getOfficePhoneNumber() ) || 0 == strlen( $this->m_objProperty->getOfficePhoneNumber() ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_phone_number', __( 'Phone number is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyTypeApartment() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objProperty->getPropertyTypeId() ) || 1 != strlen( $this->m_objProperty->getPropertyTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_type_apartment', __( 'Property type must be Apartment.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCity() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objProperty->getCity() ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine1() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objProperty->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Street Line 1 is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {

		$boolIsValid = true;

		$intLength = strlen( str_replace( '-', '', $this->getPostalCode() ) );

		if( true == is_null( $this->m_objProperty->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Postal Code is required.' ) ) );
		} elseif( false == is_null( $this->getPostalCode() ) && 0 < $intLength && ( ( false == preg_match( '/^\d{5,5}?$/', $this->getPostalCode() ) && false == preg_match( '/^\d{5,5}-\d{4,4}?$/', $this->getPostalCode() ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Postal code must be 5 or 10 characters in XXXXX or XXXXX-XXXX format.' ) ) );
		}

		return $boolIsValid;
	}

	public function valShortOrFullDescription() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objProperty->getShortDescription() ) && true == is_null( $this->m_objProperty->getFullDescription() ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'short_or_full_description', __( 'Either Short or Full Description is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryEmailAddress() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objProperty->getPrimaryEmailAddress() ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_email_address', __( 'Email Address is required.' ) ) );

		} elseif( false == CValidation::validateEmailAddresses( $this->m_objProperty->getPrimaryEmailAddress() ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_email_address', __( 'The email address does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTerminationReason( $boolIsUnterminate = false ) {
        $boolIsValid = true;

        if( true == $boolIsUnterminate ) {

        	if( false == is_null( $this->m_objProperty->getTerminationReason() ) ) {
            	$boolIsValid = false;
            	$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_reason', __( 'Termination reason is not required.' ) ) );
       	 	}

       	 	return $boolIsValid;
        }

        if( false == is_null( $this->m_objProperty->getTerminationDate() ) && true == is_null( $this->m_objProperty->getTerminationReason() ) ) {
            $boolIsValid = false;
            $this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_reason', __( 'Termination reason is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valTerminationDate( $boolIsUnterminate = false ) {
        $boolIsValid = true;

    	if( true == $boolIsUnterminate ) {

        	if( false == is_null( $this->m_objProperty->getTerminationDate() ) ) {
            	$boolIsValid = false;
            	$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_reason', __( 'Termination date is not required.' ) ) );
       	 	}

       	 	return $boolIsValid;
        }

        if( false == is_null( $this->m_objProperty->getTerminationReason() ) && true == is_null( $this->m_objProperty->getTerminationDate() ) ) {
            $boolIsValid = false;
            $this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_date', __( 'Termination date is required.' ) ) );
        }

        return $boolIsValid;
    }

	public function valCheckDuplicationByPropertyName( $objDatabase ) {

		$strSql = 'SELECT id FROM properties WHERE lower( property_name ) = \'' . trim( \Psi\CStringService::singleton()->strtolower( addslashes( $this->m_objProperty->getPropertyName() ) ) ) . '\' AND cid = ' . ( int ) $this->m_objProperty->getCid() . ' AND is_disabled = 0';

		$arrintProeprtyId	= fetchData( $strSql, $objDatabase );
		$boolIsValid		= valArr( $arrintProeprtyId );

		return ( true == $boolIsValid )?false:true;
	}

	public function valPropertyProduct( $objDatabase ) {

		$boolIsValid = true;

		$arrobjPropertyProducts = rekeyObjects( 'PsProductId', ( array ) $this->m_objProperty->fetchPropertyProducts( $objDatabase ) );

		if( valArr( $arrobjPropertyProducts ) && !( \Psi\Libraries\UtilFunctions\count( $arrobjPropertyProducts ) == 1 && isset( $arrobjPropertyProducts[CPsProduct::HISTORICAL_ACCESS] ) ) ) {
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_date', __( 'You cannot disable a property that is attached to an active contract.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valAccountId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false != is_null( $this->m_objProperty->getAccountId() ) ) {
			$boolIsValid = false;
			$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', __( 'Billing account is required for property {%s,0}', [ $this->m_objProperty->getPropertyName() ] ) ) );
		} elseif( false != valObj( $objDatabase, 'CDatabase' ) && 0 == $this->m_objProperty->getAllowMultipleAccountProperties() ) {
			$strWhere = ' WHERE account_id = ' . ( int ) $this->m_objProperty->getAccountId() . ' AND cid = ' . ( int ) $this->m_objProperty->getCid() . ' AND id != ' . ( int ) $this->m_objProperty->getId();
			$intPropertyCount = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyCount( $strWhere, $objDatabase );

			if( 0 < $intPropertyCount ) {
				$this->m_objProperty->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', __( 'The billing account you selected is already being used on another property.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsRequired = false, $boolIsDuplicatePropertyName = false ) {

		$boolIsValid = true;

		switch( $strAction ) {

			case 'validate_owner':
				$boolIsValid &= $this->valOwnerId();

			case VALIDATE_INSERT:
				// $boolIsValid &= $this->valId();
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyName( $objDatabase, $boolIsDuplicatePropertyName );
				$boolIsValid &= $this->valNumberOfUnits( $boolIsRequired );
				$boolIsValid &= $this->valYearBuilt();
				$boolIsValid &= $this->valYearRemodeled();
				$boolIsValid &= $this->valPropertyTypeId();
				$boolIsValid &= $this->valLookupCode( $objDatabase );
				$boolIsValid &= $this->valVaultwareNumber( $objDatabase );
				break;

			case 'validate_property_group_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyTypeId();
				break;

			case 'validate_advanced_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valYearBuilt();
				$boolIsValid &= $this->valYearRemodeled();
				break;

			case 'validate_property_descriptions_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valShortOrFullDescription();
				break;

			case 'contract_insert':
				$boolIsValid &= $this->valCheckDuplicationByPropertyName( $objDatabase );
				break;

			case 'contract_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyTypeId();
				$boolIsValid &= $this->valPropertyName( $objDatabase );
				$boolIsValid &= $this->valNumberOfUnits();
				$boolIsValid &= $this->valVaultwareNumber( $objDatabase );
				break;

			case 'hawaii_enroll':
				$boolIsValid &= $this->valNumberOfUnits( $boolIsRequired = true );
				break;

			case VALIDATE_DELETE:
				// $boolIsValid &= $this->valId();
				break;

			case 'validate_property_approval':
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valLongitudeAndLatitude();
				$boolIsValid &= $this->valShortOrFullDescription();
				$boolIsValid &= $this->valPropertyPhotosCount();
				$boolIsValid &= $this->valPropertyFloorplansCount();
				$boolIsValid &= $this->valCommunityAmenitiesCount();
				$boolIsValid &= $this->valOfficePhoneNumber();
				$boolIsValid &= $this->valPrimaryEmailAddress();
				$boolIsValid &= $this->valPropertyTypeApartment();
				$boolIsValid &= $this->valPropertyTypeId();
				break;

			case 'vacancy_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyTypeId();
				$boolIsValid &= $this->valPropertyName( $objDatabase );
				break;

			case 'validate_property_termination':
				$boolIsValid &= $this->valTerminationReason();
				$boolIsValid &= $this->valTerminationDate();
				break;

			case 'validate_property_untermination':
				$boolIsValid &= $this->valTerminationReason( $boolIsUnterminate = true );
				$boolIsValid &= $this->valTerminationDate( $boolIsUnterminate = true );
				break;

			case 'validate_property_is_disable':
				$boolIsValid &= $this->valPropertyProduct( $objDatabase );
				break;

			case 'billing_account_usage':
				$boolIsValid &= $this->valAccountId( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>